			
/*
--查询程序文件大小
select fu_filename,datalength(fu_pkg) from ABSys_Org_Function order by datalength(fu_pkg) desc 

--清除数据库中的程序包
update ABSys_Org_Function set Fu_Pkg=null,FU_Bitmap=null,Fu_Version='0'

--去除加密狗检测
update ABSys_Org_Parameter set Pa_Value=0 where Pa_Name='CheckSoftDog' 
*/

set nocount on
declare @tempSQL1 nvarchar(4000)
declare @tempSQL2 nvarchar(4000)
declare @tempCurDatabaseName nvarchar(100)
set @tempCurDatabaseName=db_name()

print convert(nvarchar(100),getdate(),121)+' '+'开始'

--停止所有触发器 
exec Proc_DisableTrigger @tempCurDatabaseName 

declare @tempBeginTime datetime 
declare @tempEndTime datetime 
declare @i int
set @i=0

declare @tempTableName nvarchar(100)
declare SysTableCursor cursor for 
select name 
from sysobjects 
where xtype in ('U') and  dbo.Func_InExcludeTableName(name)=0
order by Name

open SysTableCursor
fetch next from SysTableCursor into @tempTableName 
while @@fetch_status = 0
begin
	set @tempBeginTime =getdate() 
  set @i=@i+1 

  if charindex(','+@tempTableName+',',',ABSys_Org_ConnList,')>0
  begin
    delete ABSys_Org_Table where Ta_CL_Guid in (select CL_Guid from ABSys_Org_ConnList where CL_Name<>'Main')
    delete ABSys_Org_ConnList where CL_Name<>'Main'
    print '表['+@tempTableName+']单独处理'
  end
  else if charindex(','+@tempTableName+',',',ABSys_Org_Tree,')>0
  begin
    delete ABSys_Org_Tree where isnull(Tr_Group,'')<>'框架使用'
    print '表['+@tempTableName+']单独处理'
  end
  else if charindex(','+@tempTableName+',',',ABSys_Org_Function,')>0
  begin
    delete ABSys_Org_Function where Fu_SubPath='ABSoft_Set\ABClientP\Flows\'
    print '表['+@tempTableName+']单独处理'
  end
  else if charindex(','+@tempTableName+',',',ABSys_Org_TreeItem,')>0
  begin
		declare @tempTr_Guid nvarchar(100)
		declare @tempTi_Guid nvarchar(100)

		set @tempTr_Guid=(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = 'Enterprise Dir')
    if isnull(@tempTr_Guid,'')<>''
    begin
			delete ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid

			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order,Ti_Varchar1)
			values(@tempTr_Guid,'0',newid(),'Enterprise','***公司',newid(),20,'Enterprise')

			set @tempTi_Guid=(select Ti_Guid from ABSys_Org_TreeItem where Ti_Tr_Guid=@tempTr_Guid and Ti_Code='Enterprise' and Ti_ParentGuid='0')
			if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Plan')
				insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order,Ti_Varchar1)
				values(@tempTr_Guid,@tempTi_Guid,newid(),'Plan','计划部',newid(),10,'Dept')
			if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Sale')
				insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order,Ti_Varchar1)
				values(@tempTr_Guid,@tempTi_Guid,newid(),'Sale','销售部',newid(),20,'Dept')
			if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Production')
				insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order,Ti_Varchar1)
				values(@tempTr_Guid,@tempTi_Guid,newid(),'Production','生产部',newid(),30,'Dept')
			if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Storage')
				insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order,Ti_Varchar1)
				values(@tempTr_Guid,@tempTi_Guid,newid(),'Storage','仓储部',newid(),40,'Dept')
			if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Buy')
				insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order,Ti_Varchar1)
				values(@tempTr_Guid,@tempTi_Guid,newid(),'Buy','采购部',newid(),50,'Dept')
			if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Finance')
				insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order,Ti_Varchar1)
				values(@tempTr_Guid,@tempTi_Guid,newid(),'Finance','财务部',newid(),60,'Dept')
			if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='info')
				insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order,Ti_Varchar1)
				values(@tempTr_Guid,@tempTi_Guid,newid(),'info','信息部',newid(),70,'Dept')
		end

		set @tempTr_Guid=(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = 'Function Dir')
    if isnull(@tempTr_Guid,'')<>''
    begin
      delete ABSys_Org_Function where FU_Ti_Guid in (select Ti_Guid from ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid and isnull(Ti_Group,'')<>'框架使用')
		  delete ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid and isnull(Ti_Group,'')<>'框架使用'
    end
		set @tempTr_Guid=(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = 'Table Dir')
    if isnull(@tempTr_Guid,'')<>''
    begin
      delete ABSys_Org_Table where Ta_Ti_Guid in (select Ti_Guid from ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid and isnull(Ti_Group,'')<>'框架使用')
		  delete ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid and isnull(Ti_Group,'')<>'框架使用'
    end
		set @tempTr_Guid=(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = 'Extend ReportType')
    if isnull(@tempTr_Guid,'')<>''
    begin
      delete ABSys_Org_ExtendReport
		  delete ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid
		  insert into ABSys_Org_TreeItem(Ti_Tr_Guid  ,Ti_ParentGuid,Ti_Guid,Ti_Code     ,Ti_Name      ,PubID   ,Ti_Order)
			          select @tempTr_Guid a,'0' b,newid() c,'1' d,'扩展报表' e ,newid() f,10 g
    end
		set @tempTr_Guid=(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = 'CustomObject')
    if isnull(@tempTr_Guid,'')<>''
    begin
      delete ABSys_Right_KeyCustomObject
		  delete ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid
		  insert into ABSys_Org_TreeItem(Ti_Tr_Guid  ,Ti_ParentGuid,Ti_Guid,Ti_Code     ,Ti_Name      ,PubID   ,Ti_Order,Ti_Varchar1,Ti_Bit1)
			          select @tempTr_Guid a,'0' b,newid() c,'1' d,'功能点示例1' e ,newid() f,10 g,'FuncPoint' h ,1 i
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'2'  ,'功能点示例2'   ,newid()  ,20  ,'FuncPoint'   ,1       
    end
		set @tempTr_Guid=(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = 'CustomObjectType')
    if isnull(@tempTr_Guid,'')<>''
    begin
		  delete ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid
		  insert into ABSys_Org_TreeItem(Ti_Tr_Guid  ,Ti_ParentGuid,Ti_Guid,Ti_Code     ,Ti_Name      ,PubID   ,Ti_Order)
			          select @tempTr_Guid a,'0' b,newid() c,'FuncPoint' d,'功能点'e ,newid() f,10 g
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'BusinessOrder'  ,'业务单据'  ,newid()  ,20         
    end
		set @tempTr_Guid=(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = 'ClientType')
    if isnull(@tempTr_Guid,'')<>''
    begin
		  delete ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid
		  insert into ABSys_Org_TreeItem(Ti_Tr_Guid  ,Ti_ParentGuid,Ti_Guid,Ti_Code     ,Ti_Name      ,PubID   ,Ti_Order)
			          select @tempTr_Guid a,'0' b,newid() c,'1' d,'类型1' e ,newid() f,10 g
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'2'  ,'类型2'   ,newid()  ,20         
    end
		set @tempTr_Guid=(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = 'CustomCustName')
    if isnull(@tempTr_Guid,'')<>''
    begin
		  delete ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid
		  insert into ABSys_Org_TreeItem(Ti_Tr_Guid  ,Ti_ParentGuid,Ti_Guid,Ti_Code     ,Ti_Name      ,PubID   ,Ti_Order)
			          select @tempTr_Guid a,'0' b,newid() c,'1' d,'定制1' e ,newid() f,10 g
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'2'  ,'定制2'   ,newid()  ,20         
    end
		set @tempTr_Guid=(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = 'BankName')
    if isnull(@tempTr_Guid,'')<>''
    begin
		  delete ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid
		  insert into ABSys_Org_TreeItem(Ti_Tr_Guid  ,Ti_ParentGuid,Ti_Guid,Ti_Code     ,Ti_Name      ,PubID   ,Ti_Order)
			          select @tempTr_Guid a,'0' b,newid() c,'1' d,'中国建设银行' e ,newid() f,10 g
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'2'  ,'中国农业银行'   ,newid()  ,20         
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'3'  ,'中国交通银行'   ,newid()  ,30         
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'4'  ,'中国招商银行'   ,newid()  ,40         
    end
		set @tempTr_Guid=(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = 'CreditCardType')
    if isnull(@tempTr_Guid,'')<>''
    begin
		  delete ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid
		  insert into ABSys_Org_TreeItem(Ti_Tr_Guid  ,Ti_ParentGuid,Ti_Guid,Ti_Code     ,Ti_Name      ,PubID   ,Ti_Order)
			          select @tempTr_Guid a,'0' b,newid() c,'1' d,'建行龙卡' e ,newid() f,10 g
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'2'  ,'邮政绿卡'   ,newid()  ,20         
    end
		set @tempTr_Guid=(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = 'CurrencyType')
    if isnull(@tempTr_Guid,'')<>''
    begin
		  delete ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid
		  insert into ABSys_Org_TreeItem(Ti_Tr_Guid  ,Ti_ParentGuid,Ti_Guid,Ti_Code     ,Ti_Name      ,PubID   ,Ti_Order)
			          select @tempTr_Guid a,'0' b,newid() c,'1' d,'人民币' e ,newid() f,10 g
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'2'  ,'美元'   ,newid()  ,20         
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'3'  ,'港元'   ,newid()  ,30         
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'4'  ,'日元'   ,newid()  ,40         
    end
		set @tempTr_Guid=(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = 'Appellation')
    if isnull(@tempTr_Guid,'')<>''
    begin
		  delete ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid
		  insert into ABSys_Org_TreeItem(Ti_Tr_Guid  ,Ti_ParentGuid,Ti_Guid,Ti_Code     ,Ti_Name      ,PubID   ,Ti_Order)
			          select @tempTr_Guid a,'0' b,newid() c,'1' d,'博士' e ,newid() f,10 g
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'2'  ,'先生'   ,newid()  ,20         
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'3'  ,'小姐'   ,newid()  ,30        
    end
		set @tempTr_Guid=(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = 'DrivingLicenceType')
    if isnull(@tempTr_Guid,'')<>''
    begin
		  delete ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid
		  insert into ABSys_Org_TreeItem(Ti_Tr_Guid  ,Ti_ParentGuid,Ti_Guid,Ti_Code     ,Ti_Name      ,PubID   ,Ti_Order)
			          select @tempTr_Guid a,'0' b,newid() c,'A' d,'A照' e ,newid() f,10 g
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'B'  ,'B照'   ,newid()  ,20         
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'C'  ,'C照'   ,newid()  ,30        
    end
		set @tempTr_Guid=(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = 'Position')
    if isnull(@tempTr_Guid,'')<>''
    begin
		  delete ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid
		  insert into ABSys_Org_TreeItem(Ti_Tr_Guid  ,Ti_ParentGuid,Ti_Guid,Ti_Code     ,Ti_Name      ,PubID   ,Ti_Order)
			          select @tempTr_Guid a,'0' b,newid() c,'1' d,'销售' e ,newid() f,10 g
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'2'  ,'开发'   ,newid()  ,20         
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'3'  ,'生产'   ,newid()  ,30        
    end
		set @tempTr_Guid=(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = 'SkillGrade')
    if isnull(@tempTr_Guid,'')<>''
    begin
		  delete ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid
		  insert into ABSys_Org_TreeItem(Ti_Tr_Guid  ,Ti_ParentGuid,Ti_Guid,Ti_Code     ,Ti_Name      ,PubID   ,Ti_Order)
			          select @tempTr_Guid a,'0' b,newid() c,'1' d,'工程师' e ,newid() f,10 g
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'2'  ,'高级工程师'   ,newid()  ,20    
    end
		set @tempTr_Guid=(SELECT Top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code = 'BusinessOrderState')
    if isnull(@tempTr_Guid,'')<>''
    begin
		  delete ABSys_Org_TreeItem where Ti_Tr_Guid =@tempTr_Guid
		  insert into ABSys_Org_TreeItem(Ti_Tr_Guid  ,Ti_ParentGuid,Ti_Guid,Ti_Code     ,Ti_Name      ,PubID   ,Ti_Order)
			          select @tempTr_Guid a,'0' b,newid() c,'Input' d,'已录入' e ,newid() f,10 g
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'ApprovedIng'  ,'审批中'   ,newid()  ,20       
      union all select @tempTr_Guid  ,'0'  ,newid()  ,'Finish'  ,'已完成'   ,newid()  ,30      
    end

    print '表['+@tempTableName+']单独处理'
  end
  else  if charindex(','+@tempTableName+',',',ABSys_Org_Parameter,')>0
  begin
		--将参数恢复到默认值
		update ABSys_Org_Parameter
		set PA_ExpandValue=null,
		    Pa_Value= 
		  case 
				when Pa_Name=ltrim(rtrim('RegName                             ')) then 'ABFrameWork' --注册名称
				when Pa_Name=ltrim(rtrim('Logo                                ')) then 'Logo' --Logo图标
				when Pa_Name=ltrim(rtrim('BegUseDate                          ')) then convert(varchar(10),getdate(),121) --开始使用日期
				when Pa_Name=ltrim(rtrim('SoftName                            ')) then 'ABFrameWork' --软件名称
				when Pa_Name=ltrim(rtrim('ServiceDisplayName                  ')) then 'ABFrameWork三层服务器'--三层方式下提供数据库访问的服务
				when Pa_Name=ltrim(rtrim('Developer                           ')) then '郭仁俊(15921625470 QQ:16187001 QQ群:307455944)'--软件开发者
				when Pa_Name=ltrim(rtrim('DevelopDateTime                     ')) then '2005年1~2025年月1月' --开发时间
				when Pa_Name=ltrim(rtrim('VersionNo                           ')) then 'V1.0'--系统版本号  
				when Pa_Name=ltrim(rtrim('CopyRight                           ')) then '[ABParams_SoftName] [ABParams_VersionNo] 版权属[ABParams_Developer]所有.发布给[ABParams_RegName]使用,除[ABParams_RegName]之外,任何其它单位或个人的复制或传播,任何形式的反编译或修改都属于侵权行为,触犯版权法和相关公约,将受到法律的严厉制裁.' --版权说明
				when Pa_Name=ltrim(rtrim('Heartbeat                           ')) then '60'  --客户端心跳(秒)
				when Pa_Name=ltrim(rtrim('UpdateFTP                           ')) then ''    --下载更新的FTP地址
				when Pa_Name=ltrim(rtrim('AutoDownPkg                         ')) then 'Auto'--Down 强制下载  UnDown 强制不下载  Auto 根据本地参数设置来决定
				when Pa_Name=ltrim(rtrim('CheckSoftDog                        ')) then '0'   --是否检测加密狗
				when Pa_Name=ltrim(rtrim('FuncUpdateCount                     ')) then '0'   --功能模块更新计数,客户端启动时检测本地值和数据库值,如客户端值比数据库值小则启动更新并用数据库值替换 功能模块有更新时,数据库值小加1
				when Pa_Name=ltrim(rtrim('ActiveFlow                          ')) then ''    --当前使用的流程图
				when Pa_Name=ltrim(rtrim('ActiveFlowBackColor                 ')) then ''    --当前使用的流程图背景色
		  else Pa_Value
		  end 
    print '表['+@tempTableName+']单独处理'
  end
  --清空数据的表
  else  if charindex(','+@tempTableName+',',','+
										'ABSys_Right_KeyCustomObject'+','+
										'ABSys_Right_OperatorKey'+','+
										'ABSys_Right_KeyTable'+','+
										'ABSys_Right_KeyTableRow'+','+
										'ABSys_Right_KeyFunction'+','+
										'ABSys_Right_KeyField'+','+
										'ABSys_Right_KeyExtendReport'+','+
										
										'ABSys_Approved_Bill'+','+
										'ABSys_Approved_ConditionParam'+','+
										'ABSys_Approved_Message'+','+
										'ABSys_Approved_PassRuleOperator'+','+
										'ABSys_Approved_Rule'+','+
										'ABSys_Approved_Step'+','+
										'ABSys_Approved_StepOperator'+','+

										'ABSys_Log_CurLink'+','+
										'ABSys_Log_CustomEvent'+','+
										'ABSys_Log_ErrorInfo'+','+
										'ABSys_Log_FieldChange'+','+
										'ABSys_Log_Login'+','+ 
										
										'ABSys_Org_ExtendReport'+','+
										'ABSys_Org_ExtendReportDatasets'+','+
										'ABSys_Org_HistoryData'+','+
										'ABSys_Org_Key'+','+
										'ABSys_Org_Mail'+','+
										'ABSys_Org_MailFile'+','+
										'ABSys_Org_Operator'+','+
										'ABSys_Org_BillInput'+','+
										'ABSys_Org_DownGroup'+','
                     )>0
  begin
		exec('delete '+@tempTableName)
    print '表['+@tempTableName+']清空数据'
  end 
  else  
  --保留数据的表
  begin
    print '表['+@tempTableName+']保留数据'
  end

  set @tempEndTime =getdate() 
  --print convert(nvarchar(100),getdate(),121)+' '+'-------第'+cast(@i as varchar)+'张表['+@tempTableName+']已处理,耗时'+cast(DATEDIFF(second,@tempBeginTime,@tempEndTime) as nvarchar)+'秒-------'

	fetch next from SysTableCursor into @tempTableName
end
--关闭与释放游标
close SysTableCursor
deallocate SysTableCursor

update ABSys_Org_Parameter
set Pa_Value='' 
where Pa_Name in 
( 
'ClientType',    --客户端类型
'CustomCustName' --定制的客户名称
)

if exists(
				  select DatabaseName,TableName,caption,count(*) 
					from ABSys_Temp_GetFieldInfo
				  where caption<>''  
					group by DatabaseName,TableName,caption
				  having count(*)>1
			   ) or 
    exists(
				  select DatabaseName,caption,count(*) 
					from ABSys_Temp_GetTableInfo
				  where caption<>''  
					group by DatabaseName,caption
				  having count(*)>1
			   )
begin
  select DatabaseName,caption,count(*) 
	from ABSys_Temp_GetTableInfo
  where caption<>''  
	group by DatabaseName,caption
  having count(*)>1
  order by   count(*)  desc

  select DatabaseName,TableName,caption,count(*) 
	from ABSys_Temp_GetFieldInfo
  where caption<>''  
	group by DatabaseName,TableName,caption
  having count(*)>1
  order by   count(*)  desc
end
else 
begin
	exec Proc_ParseDictionary ''
end 

--启动所有触发器 
exec Proc_EnableTrigger @tempCurDatabaseName 
--清除日志
if dbo.Func_GetSQLServerMainVersion()<10 
  exec('DUMP TRANSACTION '+@tempCurDatabaseName+' WITH NO_LOG')

print convert(varchar(24),getdate(),121)+' 完成'

set nocount OFF


 