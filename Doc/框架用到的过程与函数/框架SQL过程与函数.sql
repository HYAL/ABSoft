exec Proc_CreateABSys_TempAfterEditStructure
go

--统一增加与删除列的新增/修改/删除-->日志表的触发器(字段中必须要有PUBID字段,默认值为newid())
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_SetFieldChangeLog]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_SetFieldChangeLog]
go
CREATE PROCEDURE [dbo].[Proc_SetFieldChangeLog]
(
@IsCreate bit,
@DatabaseNames varchar(8000), --为空时表示所有库，否则为用逗号分隔的字串
@TableNames varchar(8000) --为空时表示所有表，否则为用逗号分隔的字串
) 
AS
begin
  set nocount on
/*
	declare @DatabaseNames varchar(100)
	declare @TableNames varchar(100)
	declare @IsCreate bit
	set @DatabaseNames=''
	set @TableNames=''
	set @IsCreate=1
*/
	declare @tempMainDatabaseName varchar(100)
  set @tempMainDatabaseName=db_name()
  update ABSys_Org_ConnList set CL_DatabaseName=@tempMainDatabaseName where CL_Name='Main' and CL_DatabaseName<>@tempMainDatabaseName

  set @DatabaseNames =isnull(@DatabaseNames,'')
  set @TableNames =isnull(@TableNames,'')
	declare @tempDatabaseName varchar(100)
	declare @tempTableName varchar(100)
	declare @tempSqltext varchar(8000)
	declare @tempTrigBaseConst varchar(8000)
	declare @tempFieldNames varchar(8000)
	declare @tempFieldValue varchar(8000)
	declare @tempFieldValues varchar(8000)
	declare @tempFieldNames_bin varchar(8000)
	declare @tempFieldNames_Varchar varchar(8000)

	declare @tempFieldNames_500 varchar(8000) 

	--取出所有可创建日志触发器的用户表
  --除日志表、临时表、下拉组别表且有PubID字段、ABSys_Org_Table表设置为记录日志的表才创建日志触发器
	declare SysTableCursor cursor for 
    select CL_DatabaseName,Ta_Name
    from ABSys_Org_ConnList a left join ABSys_Org_Table   b on b.Ta_CL_Guid=a.CL_Guid
    where (CL_DatabaseName<>DB_Name() or 
           Ta_Name not in ('ABSys_Log_FieldChange','ABSys_Log_CurLink','ABSys_Log_CustomEvent','ABSys_Log_ErrorInfo','ABSys_Log_Login','ABSys_Org_DownGroup') and
           dbo.Func_InExcludeTableName(Ta_Name)=0 
           ) and  
          (@DatabaseNames='' or charindex(','+CL_DatabaseName+',',','+@DatabaseNames+',')>0) and 
          (@TableNames='' or charindex(','+Ta_Name+',',','+@TableNames+',')>0) and 
          (@IsCreate=0 or isnull(Ta_IsTail,0)=1) and 
          exists(select *  from ABSys_Temp_GetFieldInfo c where a.CL_DatabaseName=c.DatabaseName and b.Ta_Name=c.TableName and c.Name='PubID')  
    order by CL_DatabaseName,Ta_Name

	open SysTableCursor 
	fetch next from SysTableCursor into @tempDatabaseName,@tempTableName
	while @@fetch_status = 0
	begin
		if @IsCreate=0
		begin
			--删除表的日志触发器
			set @tempSqltext=
			' IF EXISTS (SELECT name FROM sysobjects WHERE name = '+''''+@tempTableName+'_Insert_ChangLog' +''''+' AND type = ''TR'')'+CHAR(10)+
			'   drop TRIGGER '+@tempTableName+'_Insert_ChangLog '+CHAR(10)+
			' IF EXISTS (SELECT name FROM sysobjects WHERE name = '+''''+@tempTableName+'_Update_ChangLog' +''''+' AND type = ''TR'')'+CHAR(10)+
			'   drop TRIGGER '+@tempTableName+'_Update_ChangLog '+CHAR(10)+
			' IF EXISTS (SELECT name FROM sysobjects WHERE name = '+''''+@tempTableName+'_Delete_ChangLog' +''''+' AND type = ''TR'')'+CHAR(10)+
			'   drop TRIGGER '+@tempTableName+'_Delete_ChangLog'
	    exec Proc_ExecSQL @tempSqltext,@tempDatabaseName  
      --print @tempSqltext
		end
		else if @IsCreate=1
		begin
		  set @tempFieldNames_bin=''
		  set @tempFieldNames_Varchar=''
		  set @tempFieldNames=''
		  set @tempFieldValue=''
		  set @tempFieldValues=''


			SELECT 
        @tempFieldNames_Varchar=
        case 
          when type in ('varchar','Nvarchar')  then @tempFieldNames_Varchar+','+Name
          else @tempFieldNames_Varchar
        end,

        @tempFieldNames=
        case  
          when @tempFieldNames='' then Name
          else @tempFieldNames+','+Name
        end, 
        @tempFieldValue=
        case 
          when type in ('varchar','Nvarchar')  then '[''+isnull('+Name+','''')+'']' 
          else '+[''+cast(isnull('+Name+',0) as varchar)+'']' 
        end,
        @tempFieldValues=
        case 
          when @tempFieldValues='' then @tempFieldValue
          else @tempFieldValues+@tempFieldValue
        end 

			FROM ABSys_Temp_GetFieldInfo
			where DatabaseName=@tempDatabaseName and TableName=@tempTableName and type not in ('text','image','ntext','table')
      order by DatabaseName,TableName,ID

		  set  @tempFieldValues=''''+ @tempFieldValues +''''

			SELECT  @tempFieldNames_bin=@tempFieldNames_bin+','+Name
			FROM ABSys_Temp_GetFieldInfo
			where DatabaseName=@tempDatabaseName and TableName=@tempTableName and type in ('text','image','ntext','table')
      order by DatabaseName,TableName,ID

      if len(@tempFieldNames)<=2000 and len(@tempFieldValues)<=2000 
      begin
		    set @tempFieldNames_500=substring(@tempFieldNames,1,500)

				set @tempTrigBaseConst=  
					'   set nocount on '+CHAR(10)+		  
					'   declare @PubPC varchar(100) '+CHAR(10)+
					'   declare @PubSPID varchar(500) '+CHAR(10)+
					'   declare @PubOp_Name varchar(100) '+CHAR(10)+
					'   select @PubOp_Name=Cl_Op_Name,@PubPC=Cl_PC ,@PubSPID=cast(Cl_SPID as varchar(500))  '+CHAR(10)+
					'   from '+@tempMainDatabaseName+'.dbo.ABSys_log_CurLink '+CHAR(10)+
					'   where charindex(''[HostName:''+host_name()+'']'',Cl_PC)>0   '+CHAR(10)+ 
					'   set @PubOp_Name=isnull(@PubOp_Name,user_name()) '+CHAR(10)+ 
					'   set @PubSPID=isnull(@PubSPID,cast(@@SPID as varchar(500))) '+CHAR(10)+ 
					'   set @PubPC=isnull(@PubPC,host_name()) '+CHAR(10)
				
				--创建新增数据到日志表的触发器,只记录PUBID字段,其它字段放弃(只记录单条插入的日志，一次性插入多条时不记录日志) 
				set @tempSqltext= 
					' CREATE TRIGGER '+@tempTableName+'_Insert_ChangLog'+' ON [dbo].['+@tempTableName+'] '+CHAR(10)+ 
					' FOR INSERT '+CHAR(10)+ 
					' AS '+CHAR(10)+ 
				  ' if (select count(*) from inserted where isnull(PubID,'''')<>'''') =1   '+CHAR(10) +
				  ' begin '+CHAR(10) +
				   @tempTrigBaseConst+ 
				  '   insert '+@tempMainDatabaseName+'.dbo.ABSys_Log_FieldChange(Fc_Guid,Fc_Op_Name,Fc_DateTime,Fc_PC,Fc_Type,Fc_PubIDValue,Fc_TableName,Fc_FieldName,Fc_NewValue,PubID) '+CHAR(10)+
					'   select  newid(),@PubOp_Name,getdate(),@PubPC,'+CHAR(10)+
					'           ''Insert'',PubID,'+''''+@tempTableName+''''+',''PubID'',PubID ,newid() '+CHAR(10)+
				  '   from inserted '+CHAR(10) +
					'    '+CHAR(10) +
					'   set nocount off '+CHAR(10)+   
				  ' end '
		    exec Proc_ExecSQL @tempSqltext,@tempDatabaseName  
	      --print @tempSqltext
	
	      --删除时记录数据字典中设置段的值(所有设置字段合为一条记录,字段名称或字段值超过500个字符则放弃,只记录单条删除的日志，一次性删除多条时不记录日志)
				set @tempSqltext= 
					' CREATE TRIGGER '+@tempTableName+'_Delete_ChangLog'+' ON [dbo].['+@tempTableName+'] '+CHAR(10)+
					' FOR DELETE '+CHAR(10)+
					' AS '+CHAR(10)+ 
		      ' if  (select count(*) from deleted  where isnull(PubID,'''')<>'''' ) =1   '+CHAR(10) +
		      ' begin '+CHAR(10) +
		        @tempTrigBaseConst+ 
		      '   insert '+@tempMainDatabaseName+'.dbo.ABSys_Log_FieldChange(Fc_Guid,Fc_Op_Name,Fc_DateTime,Fc_PC,Fc_Type,Fc_PubIDValue,Fc_TableName,Fc_FieldName,Fc_OldValue,PubID) '+CHAR(10)+
		  		'   select  newid(),@PubOp_Name,getdate(),@PubPC,'+CHAR(10)+
		      '           ''Delete'',PubID,'+''''+@tempTableName+''''+','''+@tempFieldNames_500+''',substring('+@tempFieldValues+',1,500),newid() '+CHAR(10)+
		      '   from deleted   '+CHAR(10) +
					'   set nocount off '+CHAR(10)+   
		      ' end '	 
		    exec Proc_ExecSQL @tempSqltext,@tempDatabaseName  
	      --print @tempSqltext
	
	      --创建更新数据到日志表的触发器 (所有设置字段合为一条记录,字段名称或字段值超过500个字符则放弃,只记录单条更新的日志，一次性更新多条时不记录日志)
				set @tempSqltext= 
				' CREATE TRIGGER '+@tempTableName+'_Update_ChangLog'+' ON [dbo].['+@tempTableName+'] '+CHAR(10)+
				' FOR UPDATE'+CHAR(10)+
				' AS '+CHAR(10)+ 
	      ' if (select count(*) from inserted) =1 and (select count(*) from deleted  where isnull(PubID,'''')<>'''' ) =1  '+CHAR(10) +
	      ' begin '+ CHAR(10) +
	        @tempTrigBaseConst+ 
				'   declare @object_id bigint'+CHAR(10)+
				'   declare @tempFieldName varchar(100)'+CHAR(10)+
				'   declare @tempFieldNames varchar(8000)'+CHAR(10)+
				'   declare @tempFieldValue varchar(8000)'+CHAR(10)+
				'   declare @tempFieldValues varchar(8000)'+CHAR(10)+
				'   declare @i int'+CHAR(10)+
				'   set @tempFieldNames='''' '+CHAR(10)+
				'   set @tempFieldValues='''' '+CHAR(10)+
				'   set @object_id=object_id(N'+''''+@tempTableName+''''+') '+CHAR(10)+
				'   set @i=1 '+CHAR(10)+
				'   while @i>0 '+CHAR(10)+
			  '   begin '+CHAR(10)+
		  	'     set @tempFieldName=COL_NAME(@object_id, @i)'+CHAR(10)+
				'     if isnull(@tempFieldName,'''')=''''    '+CHAR(10)+
				'     begin '+CHAR(10)+
				'       BREAK '+CHAR(10)+
				'     end '+CHAR(10)+
				'     else if CHARINDEX('',''+@tempFieldName+'','','+''','+@tempFieldNames_bin+','')<=0    '+CHAR(10)+
				'     begin '+CHAR(10)+
				'       if substring(columns_updated(),(@i-1)/8+1,1) & power(2,(@i-1)%8)> 0 '+CHAR(10)+
				'       begin '+CHAR(10)+
				'         if @tempFieldNames='''' '+CHAR(10)+
				'           set @tempFieldNames= @tempFieldName '+CHAR(10)+
				'         else '+CHAR(10)+
				'           set @tempFieldNames=@tempFieldNames+'',''+ @tempFieldName '+CHAR(10)+
	
				'         if CHARINDEX('',''+@tempFieldName+'','','+''','+@tempFieldNames_Varchar+','')>0    '+CHAR(10)+
				'           set @tempFieldValue=''''''[''''+isnull(''+@tempFieldName+'','''''''')+'''']'''''' '+CHAR(10)+
				'         else '+CHAR(10)+
				'           set @tempFieldValue=''''''[''''''+''+cast(isnull(''+@tempFieldName+'',0) as varchar)+''+'''''']'''''' '+CHAR(10)+

				'         if @tempFieldValues='''' '+CHAR(10)+
				'           set @tempFieldValues=@tempFieldValue '+CHAR(10)+
				'         else '+CHAR(10)+
				'           set @tempFieldValues=@tempFieldValues+''+'' +@tempFieldValue  '+CHAR(10)+
				'       end '+CHAR(10)+
				'     end '+CHAR(10)+
				'     set @i=@i+1 '+CHAR(10)+
				'   end '+CHAR(10)+
				'   if @tempFieldNames<>'''' '+CHAR(10)+
				'   begin '+CHAR(10)+
				'     select '+@tempFieldNames+ ' into #tempdeleted from deleted '+CHAR(10)+
				'     select '+@tempFieldNames+ ' into #tempinserted from inserted '+CHAR(10)+
				'     declare @tempFieldValues_Old varchar(8000)'+CHAR(10)+
				'     declare @tempFieldValues_New varchar(8000)'+CHAR(10)+
				'     declare @tempSQL Nvarchar(4000) '+CHAR(10)+
				'     set @tempSQL=''select @tempFieldValues_Old=''+@tempFieldValues+'' from #tempdeleted '' '+CHAR(10)+
				'     exec  sp_executesql @tempSQL ,N'' @tempFieldValues_Old varchar(8000) OUTPUT'',@tempFieldValues_Old  OUTPUT '+CHAR(10)+
				'     set @tempSQL=''select @tempFieldValues_New=''+@tempFieldValues+'' from #tempinserted'' '+CHAR(10)+
				'     exec  sp_executesql @tempSQL,N'' @tempFieldValues_New varchar(8000) OUTPUT'',@tempFieldValues_New  OUTPUT '+CHAR(10)+
				'     insert '+@tempMainDatabaseName+'.dbo.ABSys_Log_FieldChange(Fc_Guid,Fc_Op_Name,Fc_DateTime,Fc_PC,Fc_Type,Fc_PubIDValue,Fc_TableName,Fc_FieldName,Fc_OldValue,Fc_NewValue,PubID) '+CHAR(10)+
				'     select  newid(),@PubOp_Name,getdate(),@PubPC,'+CHAR(10)+
				'             ''Edit'',(select PubID from deleted),'+''''+@tempTableName+''''+','''+@tempFieldNames_500+''',substring(@tempFieldValues_Old,1,500),substring(@tempFieldValues_New,1,500),newid()  '+CHAR(10)+
				'   end '+CHAR(10)+
				'   set nocount off '+ CHAR(10)+  
	      ' end '
		    exec Proc_ExecSQL @tempSqltext,@tempDatabaseName  
      end
      else 
      begin
        print '表字段太多'+'['+@tempDatabaseName+':'+@tempTableName+']['+@tempFieldNames+']'
      end
		end
		fetch next from SysTableCursor into  @tempDatabaseName,@tempTableName
	end
	--关闭与释放游标
	close SysTableCursor
	deallocate SysTableCursor
	set nocount off
end
GO
/*
exec Proc_SetFieldChangeLog 0,'',''
exec Proc_SetFieldChangeLog 1,'',''
*/
  

go

--用游标分析与填充数据字典表的存储过程
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_ParseDictionary]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_ParseDictionary]
go
CREATE PROCEDURE [dbo].[Proc_ParseDictionary] 
(
@DatabaseNames varchar(8000) --为空时表示所有库，否则为用逗号分隔的字串
) 
AS
begin
	set nocount on
  exec Proc_CreateABSys_TempAfterEditStructure
  exec Proc_SetFieldChangeLog 0,@DatabaseNames,''

  update ABSys_Org_ConnList set CL_DatabaseName=db_name() where CL_Name='Main' and CL_DatabaseName<>db_name() 

	--定义变量
	declare @DatabaseName varchar(100)
	declare @Type varchar(100)
	declare @TableName varchar(100)
	declare @TableGuid varchar(100)
	declare @tempWhere varchar(8000)
	declare @tempUpdateFieldAndValue varchar(8000)
	declare @tempType int

	declare tempUpdateTypeCursor cursor for 
    select * 
    from (
	--(修改表类型)如在数据库中创建一表，删除，又创建一同名的视图
          select  'Ta_Type= xType' tempUpdateFieldAndValue,' and TableName=Ta_Name and xType<>Ta_Type and ('''+@DatabaseNames+'''='''' or '''+@DatabaseNames+'''=CL_DatabaseName)  ' tempWhere ,1 tempType
	--(修改表ID)如在数据库中删除了表又增加,则表的ID就会与数据字典中的表ID不一样了
    union select  'Ta_ID=TableID' tempUpdateFieldAndValue,' and TableName=Ta_Name and TableID<>Ta_ID and ('''+@DatabaseNames+'''='''' or '''+@DatabaseNames+'''=CL_DatabaseName) ' tempWhere ,2 tempType
	--(修改表名)如在数据库中修改了表名,则根据表ID更新一下
    union select  'Ta_Name=TableName' tempUpdateFieldAndValue,' and TableID=Ta_ID and TableName<> Ta_Name and ('''+@DatabaseNames+'''='''' or '''+@DatabaseNames+'''=CL_DatabaseName) ' tempWhere ,3 tempType
  --(增加表)如在数据库中增加表，则字典中的表数据要同时补上
    union select  '' tempUpdateFieldAndValue,' and ('''+@DatabaseNames+'''='''' or '''+@DatabaseNames+'''=CL_DatabaseName)  ' tempWhere,4 tempType
	--(删除表)如在数据库中删除表，则字典中的表数据就多余了
    union select  '' tempUpdateFieldAndValue,' and ('''+@DatabaseNames+'''='''' or '''+@DatabaseNames+'''=CL_DatabaseName)  ' tempWhere,5 tempType

	--(修改字段序号)如在数据库中改变了字段的前后序号而字段名没有变
    union select  'Fi_Order=ID' tempUpdateFieldAndValue,' and Fi_Name=Name and Fi_Order<>ID and ('''+@DatabaseNames+'''='''' or '''+@DatabaseNames+'''=CL_DatabaseName) ' tempWhere ,11 tempType
	--(修改字段名)如在数据库中修改了字段名而序号没有改变
    union select  'Fi_Name=Name' tempUpdateFieldAndValue,' and Fi_Order=ID and Fi_Name<>Name and ('''+@DatabaseNames+'''='''' or '''+@DatabaseNames+'''=CL_DatabaseName) ' tempWhere ,12 tempType
	--(修改字段标题)如在数据库中修改了修改字段标题而字段名及序号没有改变,如果此时字典中还未有标题时,则更新字段标题
    union select  'Fi_Caption=case when isnull(Caption,'''')='''' then Name else Caption end,
                   Fi_Hint   =case when isnull(Caption,'''')='''' then Name else Caption end
                   ' tempUpdateFieldAndValue,' and  Fi_Name=Name and Fi_Order=ID and isnull(Fi_Caption,Fi_Name)=Fi_Name and ('''+@DatabaseNames+'''='''' or '''+@DatabaseNames+'''=CL_DatabaseName) ' tempWhere ,13 tempType
  --(增加字段)如在数据库中增加字段，则字典中的字段数据要同时补上
    union select  '' tempUpdateFieldAndValue,' and ('''+@DatabaseNames+'''='''' or '''+@DatabaseNames+'''=CL_DatabaseName)  ' tempWhere,14 tempType
	--(删除字段)如在数据库中已删除字段，则字典中的字段数据就多余了
    union select  '' tempUpdateFieldAndValue,' and ('''+@DatabaseNames+'''='''' or '''+@DatabaseNames+'''=CL_DatabaseName)  ' tempWhere,15 tempType
      ) aa
    order by tempType

	open tempUpdateTypeCursor 
	fetch next from tempUpdateTypeCursor into @tempUpdateFieldAndValue,@tempWhere,@tempType
	while @@fetch_status = 0
	begin
    if @tempType=1 or @tempType=2 or @tempType=3
    begin
      exec('
						if exists(select * 
									    from ABSys_Temp_GetTableInfo a,ABSys_Org_ConnList b,ABSys_Org_Table c
                      where a.DatabaseName=b.CL_DatabaseName and c.ta_CL_Guid=b.CL_Guid '+@tempWhere+' 
			                )
							update c
							set  '+@tempUpdateFieldAndValue+'
							from ABSys_Temp_GetTableInfo a,ABSys_Org_ConnList b,ABSys_Org_Table c
                      where a.DatabaseName=b.CL_DatabaseName and c.ta_CL_Guid=b.CL_Guid '+@tempWhere+' 
            ')
    end
    else if @tempType=4
    begin
      exec('
						declare @tempTr_Guid  varchar(100)
						declare @tempTi_Guid1 varchar(100)
						set @tempTr_Guid=(select Tr_Guid from ABSys_Org_Tree where Tr_Code=''Table Dir'')
						set @tempTi_Guid1=(select Ti_Guid from ABSys_Org_TreeItem where Ti_Tr_Guid=@tempTr_Guid and Ti_Code=''Framkwork'' and Ti_ParentGuid=''0'')

						if exists(select * 
									    from ABSys_Temp_GetTableInfo a 
									         left join ABSys_Org_ConnList b on a.DatabaseName=b.CL_DatabaseName
									         left join ABSys_Org_Table c on a.TableName=c.Ta_Name and c.ta_CL_Guid=b.CL_Guid
                      where Ta_Name is null '+@tempWhere+'
			                )
							insert into ABSys_Org_Table(
								Ta_Ti_Guid,
								Ta_CL_Guid,
								Ta_Guid,
								Ta_Type,
								Ta_ID,
								Ta_Name,
								Ta_Caption,
								Ta_Order,
								Ta_IsTail,Ta_CanInsert,Ta_CanDelete,Ta_CanEdit,Ta_CanPrint, 
								PubID
							)
							select 
								case
								when left(a.TableName,9) =''ABSys_Log''      then (SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid1 and Ti_Code=''Framkwork_Log'' )
								when left(a.TableName,14)=''ABSys_Org_Func'' then (SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid1 and Ti_Code=''Framkwork_Func'' )
								when left(a.TableName,9) =''ABSys_Org''      then (SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid1 and Ti_Code=''Framkwork_Org'' )
								when left(a.TableName,11)=''ABSys_Right''    then (SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid1 and Ti_Code=''Framkwork_Right'' )
								when left(a.TableName,11)=''ABSys_Approved'' then (SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid1 and Ti_Code=''Framkwork_Approve'' )
								when left(a.TableName,5) =''ABSys''          then (SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid1 and Ti_Code=''Framkwork_Other'' )
								else                                              (SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Tr_Guid   =@tempTr_Guid  and Ti_Code=''Other Info'' )
								end as a,
					      CL_Guid,
								newid(),
					      Ltrim(Rtrim(a.xType)),
								a.TableID,a.TableName,case when isnull(a.Caption,'''')<>'''' then a.Caption else a.TableName end,10,
								0,1,1,1,1,newid()
				    from ABSys_Temp_GetTableInfo a 
								         left join ABSys_Org_ConnList b on a.DatabaseName=b.CL_DatabaseName
								         left join ABSys_Org_Table c on a.TableName=c.Ta_Name and c.ta_CL_Guid=b.CL_Guid 
	                  where Ta_Name is null '+@tempWhere+'
            ')
    end
    else if @tempType=5
    begin
      exec('
						if exists(select * 
									    from ABSys_Temp_GetTableInfo a 
									         left join ABSys_Org_ConnList b on a.DatabaseName=b.CL_DatabaseName
									         right join ABSys_Org_Table c on a.TableName=c.Ta_Name and c.ta_CL_Guid=b.CL_Guid  
                      where DatabaseName is null '+@tempWhere+'
			                )
							delete c
							from ABSys_Temp_GetTableInfo a,ABSys_Org_ConnList b,ABSys_Org_Table c 
              where a.DatabaseName=b.CL_DatabaseName and a.TableName=c.Ta_Name and c.ta_CL_Guid=b.CL_Guid and  DatabaseName is null '+@tempWhere+'
            ')
    end
    
    else if @tempType=11 or @tempType=12 or @tempType=13
    begin
      exec('
						if exists(select * 
						from ABSys_Temp_GetFieldInfo a,ABSys_Org_ConnList b ,ABSys_Org_Table c, ABSys_Org_Field d
						where a.DatabaseName=b.CL_DatabaseName and TableName= Ta_Name and c.ta_CL_Guid=b.CL_Guid  and 
						      Fi_Ta_Guid= Ta_Guid  '+@tempWhere+' 
			                )
							update d
							set  '+@tempUpdateFieldAndValue+'
							from ABSys_Temp_GetFieldInfo a,ABSys_Org_ConnList b ,ABSys_Org_Table c, ABSys_Org_Field d
							where a.DatabaseName=b.CL_DatabaseName and TableName= Ta_Name and c.ta_CL_Guid=b.CL_Guid  and 
							      Fi_Ta_Guid= Ta_Guid   '+@tempWhere+' 
            ')
    end
    else if @tempType=14
    begin
      exec('
						if exists(select * 
									    from ABSys_Temp_GetFieldInfo a 
									         left join ABSys_Org_ConnList b on a.DatabaseName=b.CL_DatabaseName
									         left join ABSys_Org_Table c on a.TableName=c.Ta_Name and c.ta_CL_Guid=b.CL_Guid 
									         left join ABSys_Org_Field d on Fi_Ta_Guid= Ta_Guid and Fi_Name=Name 
                      where    Fi_Name is null '+@tempWhere+'
			                )
							insert into ABSys_Org_Field (
								Fi_Ta_Guid     ,Fi_Guid      ,Fi_Name      ,Fi_Update,Fi_Copy,Fi_BatchEdit,
								Fi_Caption     ,Fi_Hint      ,Fi_Order    ,
								Fi_IsQueryView    ,Fi_IsGridView    ,Fi_IsInputPanelView   ,
								PubID      ,
								Fi_CanNull     ,Fi_CanEditInEdit   ,Fi_CanEditInInsert   ,
								Fi_BegEndQuery,Fi_CanSelectParent,Fi_DefaultValue,FI_ControlType,FI_CacleDataset    
								)
							select Ta_Guid ,newid()  ,a.Name ,1,1,1,
								case when isnull(a.Caption,'''')='''' then a.Name else a.Caption end,
								case when isnull(a.Caption,'''')='''' then a.Name else a.Caption end,
								a.ID ,
								case when a.Name=''PubID'' or substring(a.Name,3,100)=''_Guid'' then 0 else 0 end,
								case when a.Name=''PubID'' or substring(a.Name,3,100)=''_Guid'' then 0 else 0 end,
								case when a.Name=''PubID'' or substring(a.Name,3,100)=''_Guid'' then 0 else 1 end, 
								newid(),1,1,1,0,1,
								case 
				        when substring(a.Name,3,100)=''_Guid'' then ''Guid'' 
				        when substring(a.Name,3,100)=''_Order'' then ''Recordcount*10'' 
				        else null 
				        end ,
								case 
				        when substring(a.Name,3,100)=''_Order'' then ''TABcxDBFieldOrder'' 
				        else null 
				        end,
                1  
				
							from ABSys_Temp_GetFieldInfo a 
					         left join ABSys_Org_ConnList b on a.DatabaseName=b.CL_DatabaseName
					         left join ABSys_Org_Table c on a.TableName=c.Ta_Name and c.ta_CL_Guid=b.CL_Guid 
					         left join ABSys_Org_Field d on Fi_Ta_Guid= Ta_Guid and Fi_Name=Name 
              where    Fi_Name is null '+@tempWhere+'
            ')
    end
    else if @tempType=15
    begin
      exec('
						if exists(select * 
									    from ABSys_Temp_GetFieldInfo a 
									         left join ABSys_Org_ConnList b on a.DatabaseName=b.CL_DatabaseName
									         left join ABSys_Org_Table c on a.TableName=c.Ta_Name and c.ta_CL_Guid=b.CL_Guid 
									         right join ABSys_Org_Field d on Fi_Ta_Guid= Ta_Guid and Fi_Name=Name 
                      where    DatabaseName is null '+@tempWhere+'
			                )
							delete d
							from ABSys_Temp_GetFieldInfo a ,ABSys_Org_ConnList b,ABSys_Org_Table c,ABSys_Org_Field d  
              where  a.DatabaseName=b.CL_DatabaseName and  a.TableName=c.Ta_Name and c.ta_CL_Guid=b.CL_Guid  and Fi_Ta_Guid= Ta_Guid and Fi_Name=Name and DatabaseName is null '+@tempWhere+'
            ')
    end
    

		fetch next from tempUpdateTypeCursor into  @tempUpdateFieldAndValue,@tempWhere,@tempType
	end
	close tempUpdateTypeCursor
	deallocate tempUpdateTypeCursor

  exec Proc_SetFieldChangeLog 1,@DatabaseNames,''
	set nocount off
end

GO

if exists(
				  select DatabaseName,TableName,caption,count(*) 
					from ABSys_Temp_GetFieldInfo
				  where caption<>''  
					group by DatabaseName,TableName,caption
				  having count(*)>1
			   ) or 
    exists(
				  select DatabaseName,caption,count(*) 
					from ABSys_Temp_GetTableInfo
				  where caption<>''  
					group by DatabaseName,caption
				  having count(*)>1
			   )
begin
  select DatabaseName,caption,count(*) 
	from ABSys_Temp_GetTableInfo
  where caption<>''  
	group by DatabaseName,caption
  having count(*)>1
  order by   count(*)  desc

  select DatabaseName,TableName,caption,count(*) 
	from ABSys_Temp_GetFieldInfo
  where caption<>''  
	group by DatabaseName,TableName,caption
  having count(*)>1
  order by   count(*)  desc
end
else 
begin
	exec Proc_ParseDictionary ''
end

go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_InitABSoftInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_InitABSoftInfo]
go
CREATE PROCEDURE [dbo].[Proc_InitABSoftInfo] 
AS
begin
	set nocount on
	declare @tempTr_Guid varchar(100)
	declare @tempTi_Guid varchar(100)
	declare @tempTi_Guid1 varchar(100) 

	--初始化表分类
	if not exists(SELECT * FROM ABSys_Org_Tree where Tr_Code='Table Dir')
		insert into ABSys_Org_Tree(Tr_Guid,Tr_Code,Tr_Name,Tr_SysUse,PubID,Tr_Order) 
		values(newid(),'Table Dir','企业机构分类',1,newid(),10)

	set @tempTr_Guid=(select Tr_Guid from ABSys_Org_Tree where Tr_Code='Table Dir')
	if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_Tr_Guid=@tempTr_Guid and Ti_Code='Other Info' and Ti_ParentGuid='0')
		insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order) 
		values(@tempTr_Guid,'0',newid(),'Other Info','其它信息表',newid(),10)

	if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_Tr_Guid=@tempTr_Guid and Ti_Code='Framkwork' and Ti_ParentGuid='0')
		insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order)
		values(@tempTr_Guid,'0',newid(),'Framkwork','框架表',newid(),20)

	set @tempTi_Guid1=(select Ti_Guid from ABSys_Org_TreeItem where Ti_Tr_Guid=@tempTr_Guid and Ti_Code='Framkwork' and Ti_ParentGuid='0')
	if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid1 and Ti_Code='Framkwork_Org')
		insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order)
		values(@tempTr_Guid,@tempTi_Guid1,newid(),'Framkwork_Org','系统表',newid(),10)

	set @tempTi_Guid1=(select Ti_Guid from ABSys_Org_TreeItem where Ti_Tr_Guid=@tempTr_Guid and Ti_Code='Framkwork' and Ti_ParentGuid='0')
	if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid1 and Ti_Code='Framkwork_Func')
		insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order)
		values(@tempTr_Guid,@tempTi_Guid1,newid(),'Framkwork_Func','功能表',newid(),20)

	if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid1 and Ti_Code='Framkwork_Right')
		insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order)
		values(@tempTr_Guid,@tempTi_Guid1,newid(),'Framkwork_Right','权限表',newid(),30)

	if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid1 and Ti_Code='Framkwork_Approve')
		insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order)
		values(@tempTr_Guid,@tempTi_Guid1,newid(),'Framkwork_Approve','审批表',newid(),40)

	if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid1 and Ti_Code='Framkwork_Log')
		insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order)
		values(@tempTr_Guid,@tempTi_Guid1,newid(),'Framkwork_Log','日志表',newid(),50)

	if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid1 and Ti_Code='Framkwork_Other')
		insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order)
		values(@tempTr_Guid,@tempTi_Guid1,newid(),'Framkwork_Other','其它表',newid(),60)

	--初始化扩展报表分类
	if not exists(SELECT * FROM ABSys_Org_Tree where Tr_Code='Extend ReportType')
		insert into ABSys_Org_Tree(Tr_Guid,Tr_Code,Tr_Name,Tr_SysUse,PubID,Tr_Order)
		values(newid(),'Extend ReportType','报表分类',1,newid(),10)

	--初始化扩展分类
	if not exists(SELECT * FROM ABSys_Org_Tree where Tr_Code='Extend Type')
		insert into ABSys_Org_Tree(Tr_Guid,Tr_Code,Tr_Name,Tr_SysUse,PubID,Tr_Order)
		values(newid(),'Extend Type','扩展分类',1,newid(),10)

	set @tempTr_Guid=(select Tr_Guid from ABSys_Org_Tree where Tr_Code='Extend Type')
	if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_Tr_Guid=@tempTr_Guid and Ti_Code='Admin' and Ti_ParentGuid='0')
		insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order)
		values(@tempTr_Guid,'0',newid(),'Admin','系统管理员',newid(),20)

	set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Code='Admin' and Ti_Tr_Guid=@tempTr_Guid )
	if not exists(SELECT * FROM ABSys_Org_Operator where Op_code='admin')
		insert into ABSys_Org_Operator(Op_Ti_Guid,Op_Guid,Op_Code,Op_Name,Op_PassWord,OP_UseEndDateTime,Op_Order,PubID)
		values(@tempTi_Guid,newid(),'admin','Admin','000187000193000215000237000228000248000245000249000249000253000247000072',getdate()+3650,10,newid())
	if not exists(SELECT * FROM ABSys_Org_Operator where Op_code='sysuser')
		insert into ABSys_Org_Operator(Op_Ti_Guid,Op_Guid,Op_Code,Op_Name,Op_PassWord,OP_UseEndDateTime,Op_Order,PubID)
		values(@tempTi_Guid,newid(),'sysuser','sysuser','',getdate()+3650,20,newid())
	 
	--初始化企业机构
	if not exists(SELECT * FROM ABSys_Org_Tree where Tr_Code='Enterprise Dir')
		insert into ABSys_Org_Tree(Tr_Guid,Tr_Code,Tr_Name,Tr_SysUse,PubID,Tr_Order)
		values(newid(),'Enterprise Dir','企业机构分类',0,newid(),10)

	set @tempTr_Guid=(select Tr_Guid from ABSys_Org_Tree where Tr_Code='Enterprise Dir')
	if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_Tr_Guid=@tempTr_Guid)
	BEGIN
		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_Tr_Guid=@tempTr_Guid and Ti_Code='Enterprise' and Ti_ParentGuid='0')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order,Ti_Varchar1)
			values(@tempTr_Guid,'0',newid(),'Enterprise','***公司',newid(),20,'Enterprise')
			set @tempTi_Guid=(select Ti_Guid from ABSys_Org_TreeItem where Ti_Tr_Guid=@tempTr_Guid and Ti_Code='Enterprise' and Ti_ParentGuid='0')

		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Plan')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order,Ti_Varchar1)
			values(@tempTr_Guid,@tempTi_Guid,newid(),'Plan','计划部',newid(),10,'Dept')
		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Sale')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order,Ti_Varchar1)
			values(@tempTr_Guid,@tempTi_Guid,newid(),'Sale','销售部',newid(),20,'Dept')
		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Production')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order,Ti_Varchar1)
			values(@tempTr_Guid,@tempTi_Guid,newid(),'Production','生产部',newid(),30,'Dept')
		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Storage')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order,Ti_Varchar1)
			values(@tempTr_Guid,@tempTi_Guid,newid(),'Storage','仓储部',newid(),40,'Dept')
		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Buy')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order,Ti_Varchar1)
			values(@tempTr_Guid,@tempTi_Guid,newid(),'Buy','采购部',newid(),50,'Dept')
		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Finance')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order,Ti_Varchar1)
			values(@tempTr_Guid,@tempTi_Guid,newid(),'Finance','财务部',newid(),60,'Dept')
		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='info')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,PubID,Ti_Order,Ti_Varchar1)
			values(@tempTr_Guid,@tempTi_Guid,newid(),'info','信息部',newid(),70,'Dept')
	end

	--初始化菜单分类
	if not exists(SELECT * FROM ABSys_Org_Tree where Tr_Code='Function Dir')
		insert into ABSys_Org_Tree(Tr_Guid,Tr_Code,Tr_Name,Tr_SysUse,PubID,Tr_Order)
		values(newid(),'Function Dir','菜单分类',1,newid(),10)
	set @tempTr_Guid=(select Tr_Guid from ABSys_Org_Tree where Tr_Code='Function Dir')
	if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_Tr_Guid=@tempTr_Guid)
	BEGIN
		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_Tr_Guid=@tempTr_Guid and Ti_Code='Report Query')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,TI_bit1,PubID,Ti_Order)
			values(@tempTr_Guid,'0',newid(),'Report Query','报表管理',1,newid(),20)

		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_Tr_Guid=@tempTr_Guid and Ti_Code='Help Other')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,TI_bit1,PubID,Ti_Order)
			values(@tempTr_Guid,@tempTi_Guid,newid(),'Help Other','帮助与其它',1,newid(),30)

		set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Tr_Guid=@tempTr_Guid and Ti_Code='Help Other')
		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Tools')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,TI_bit1,PubID,Ti_Order)
			values(@tempTr_Guid,@tempTi_Guid,newid(),'Tools','工具',1,newid(),10)

		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_Tr_Guid=@tempTr_Guid and Ti_Code='Framkwork' and Ti_ParentGuid='0')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,TI_bit1,PubID,Ti_Order)
			values(@tempTr_Guid,'0',newid(),'Framkwork','框架',1,newid(),10)
		set @tempTi_Guid=(select Ti_Guid from ABSys_Org_TreeItem where Ti_Tr_Guid=@tempTr_Guid and Ti_Code='Framkwork' and Ti_ParentGuid='0')

		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Public')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,TI_bit1,PubID,Ti_Order)
			values(@tempTr_Guid,@tempTi_Guid,newid(),'Public','共用包',0,newid(),10)
		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='System')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,TI_bit1,PubID,Ti_Order)
			values(@tempTr_Guid,@tempTi_Guid,newid(),'System','系统管理',1,newid(),20)
		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Function')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,TI_bit1,PubID,Ti_Order)
			values(@tempTr_Guid,@tempTi_Guid,newid(),'Function','功能管理',1,newid(),30)
		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Right')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,TI_bit1,PubID,Ti_Order)
			values(@tempTr_Guid,@tempTi_Guid,newid(),'Right','权限管理',1,newid(),40)
		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Approve')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,TI_bit1,PubID,Ti_Order)
			values(@tempTr_Guid,@tempTi_Guid,newid(),'Approve','审批管理',1,newid(),50)
		if not exists(SELECT * FROM ABSys_Org_TreeItem where Ti_ParentGuid=@tempTi_Guid and Ti_Code='Log')
			insert into ABSys_Org_TreeItem(Ti_Tr_Guid,Ti_ParentGuid,Ti_Guid,Ti_Code,Ti_Name,TI_bit1,PubID,Ti_Order)
			values(@tempTr_Guid,@tempTi_Guid,newid(),'Log','日志管理',1,newid(),60)
	END

	--初始化菜单明细
	set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Code='Report Query' and Ti_Tr_Guid=@tempTr_Guid and Ti_ParentGuid='0')
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_ExtendReportDesignG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'报表设计',1,1,0,0,'ABSys_Org_ExtendReportDesignG.bpl',null,null,10,0,1,'ABSys_Org_ExtendReportDesignG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_ExtendReportPrintG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'报表打印',1,1,0,0,'ABSys_Org_ExtendReportPrintG.bpl',null,null,20,1,1,'ABSys_Org_ExtendReportPrintG',newid())

	set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Code='Help Other' and Ti_Tr_Guid=@tempTr_Guid and Ti_ParentGuid='0')
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='MainHelp.chm' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'帮助',1,1,0,0,'MainHelp.chm',null,null,10,0,1,'MainHelp.chm',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_UpdateG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'下载更新',1,1,0,0,'ABSys_Org_UpdateG.bpl',null,null,20,0,1,'ABSys_Org_UpdateG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_RegisteredG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'软件注册',1,1,0,0,'ABSys_Org_RegisteredG.bpl',null,null,30,0,1,'ABSys_Org_RegisteredG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_AboutG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'关于',1,1,0,0,'ABSys_Org_AboutG.bpl',null,null,40,1,1,'ABSys_Org_AboutG',newid())

	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='Run:http://www.aibosoft.cn' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'访问网站',1,1,0,0,'Run:http://www.aibosoft.cn',null,null,50,0,1,'Run:http://www.aibosoft.cn',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='Run:http://www.aibosoft.cn/QuestionAnswer.htm' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'常见问题',1,1,0,0,'Run:http://www.aibosoft.cn/QuestionAnswer.htm',null,null,60,0,1,'Run:http://www.aibosoft.cn/QuestionAnswer.htm',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='Run:http://www.aibosoft.cn/leaveWord/index.asp' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'客户留言',1,1,0,0,'Run:http://www.aibosoft.cn/leaveWord/index.asp',null,null,70,0,1,'Run:http://www.aibosoft.cn/leaveWord/index.asp',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='Run:http://www.aibosoft.cn/LinkMe.htm' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'联系我们',1,1,0,0,'Run:http://www.aibosoft.cn/LinkMe.htm',null,null,80,1,1,'Run:http://www.aibosoft.cn/LinkMe.htm',newid())

	set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Code='Framkwork' and Ti_Tr_Guid=@tempTr_Guid and Ti_ParentGuid='0')
	set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Code='Public' and Ti_ParentGuid=@tempTi_Guid)

	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABClient.exe' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'ABClient.exe',0,1,1,0,'ABClient.exe',null,null,10,0,1,'ABClient',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABClientP.exe' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'ABClientP.exe',0,1,1,0,'ABClientP.exe',null,null,20,1,1,'ABClientP',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABPubG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'ABPubG.bpl',0,1,1,0,'ABPubG.bpl',null,null,30,0,1,'ABPubG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSQLDMOG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'ABSQLDMOG.bpl',0,1,1,0,'ABSQLDMOG.bpl',null,null,40,0,1,'ABSQLDMOG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABThirdPubG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'ABThirdPubG.bpl',0,1,1,0,'ABThirdPubG.bpl',null,null,50,0,1,'ABThirdPubG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABFramkWorkG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'ABFramkWorkG.bpl',0,1,1,0,'ABFramkWorkG.bpl',null,null,70,0,1,'ABFramkWorkG',newid())

	set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Code='Framkwork' and Ti_Tr_Guid=@tempTr_Guid and Ti_ParentGuid='0')
	set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Code='System' and Ti_ParentGuid=@tempTi_Guid)
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_FieldG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'数据字典管理',1,1,0,0,'ABSys_Org_FieldG.bpl',null,null,10,0,1,'ABSys_Org_FieldG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_DownListG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'下拉列项管理',1,1,0,0,'ABSys_Org_DownListG.bpl',null,null,20,0,1,'ABSys_Org_DownListG',newid())


	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_ParameterG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'系统参数管理',1,1,0,0,'ABSys_Org_ParameterG.bpl',null,null,30,1,1,'ABSys_Org_ParameterG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_ConnG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'数据连接管理',1,1,0,0,'ABSys_Org_ConnG.bpl',null,null,40,0,1,'ABSys_Org_ConnG',newid())

	if not exists(SELECT * FROM /*ABFramkWork.dbo.*/ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_historyDataG.bpl' )
		insert into /*ABFramkWork.dbo.*/ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'历史数据清除',1,1,0,0,'ABSys_Org_historyDataG.bpl',null,null,50,1,1,'ABSys_Org_historyDataG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_InputOutputG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'导入导出管理',1,1,0,0,'ABSys_Org_InputOutputG.bpl',null,null,60,0,1,'ABSys_Org_InputOutputG',newid())

	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_MailG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'邮件管理',1,1,0,0,'ABSys_Org_MailG.bpl',null,null,70,1,1,'ABSys_Org_MailG',newid())

	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_BillInputG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'单据导入管理',1,1,0,0,'ABSys_Org_BillInputG.bpl',null,null,70,1,1,'ABSys_Org_BillInputG',newid())

	set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Code='Framkwork' and Ti_Tr_Guid=@tempTr_Guid and Ti_ParentGuid='0')
	set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Code='Function' and Ti_ParentGuid=@tempTi_Guid)
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_FunctionG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'功能菜单管理',1,1,0,0,'ABSys_Org_FunctionG.bpl',null,null,60,1,1,'ABSys_Org_FunctionG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_FuncSQLG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'功能SQL管理',1,1,0,0,'ABSys_Org_FuncSQLG.bpl',null,null,80,0,1,'ABSys_Org_FuncSQLG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_FuncControlPropertyG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'功能控件属性管理',1,1,0,0,'ABSys_Org_FuncControlPropertyG.bpl',null,null,100,0,1,'ABSys_Org_FuncControlPropertyG',newid())

	set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Code='Framkwork' and Ti_Tr_Guid=@tempTr_Guid and Ti_ParentGuid='0')
	set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Code='Right' and Ti_ParentGuid=@tempTi_Guid)
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_EnterpriseG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'企业机构管理',1,1,0,0,'ABSys_Org_EnterpriseG.bpl',null,null,10,0,1,'ABSys_Org_EnterpriseG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Org_KeyG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'角色管理',1,1,0,0,'ABSys_Org_KeyG.bpl',null,null,20,1,1,'ABSys_Org_KeyG',newid())

	set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Code='Framkwork' and Ti_Tr_Guid=@tempTr_Guid and Ti_ParentGuid='0')
	set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Code='Approve' and Ti_ParentGuid=@tempTi_Guid)
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Approved_BillG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'审批单据管理',1,1,0,0,'ABSys_Approved_BillG.bpl',null,null,5,0,1,'ABSys_Approved_BillG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Approved_BillG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'审批规则管理',1,1,0,0,'ABSys_Approved_RuleG.bpl',null,null,10,0,1,'ABSys_Approved_RuleG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Approved_MessageG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'审批消息管理',1,1,0,0,'ABSys_Approved_MessageG.bpl',null,null,20,1,1,'ABSys_Approved_MessageG',newid())

	set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Code='Framkwork' and Ti_Tr_Guid=@tempTr_Guid and Ti_ParentGuid='0')
	set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Code='Log' and Ti_ParentGuid=@tempTi_Guid)
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Log_CurLinkG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'当前连接日志',1,1,0,0,'ABSys_Log_CurLinkG.bpl',null,null,10,0,1,'ABSys_Log_CurLinkG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Log_LoginG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'登录日志',1,1,0,0,'ABSys_Log_LoginG.bpl',null,null,20,1,1,'ABSys_Log_LoginG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Log_SQLMonitorG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'SQL监控',1,1,0,0,'ABSys_Log_SQLMonitorG.bpl',null,null,30,0,1,'ABSys_Log_SQLMonitorG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Log_FieldChangeG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'数据修改日志',1,1,0,0,'ABSys_Log_FieldChangeG.bpl',null,null,40,0,1,'ABSys_Log_FieldChangeG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Log_ErrorInfoG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'程序异常日志',1,1,0,0,'ABSys_Log_ErrorInfoG.bpl',null,null,50,0,1,'ABSys_Log_ErrorInfoG',newid())
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName='ABSys_Log_CustomEventG.bpl' )
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID)
		values(@tempTi_Guid,newid(),'自定义日志',1,1,0,0,'ABSys_Log_CustomEventG.bpl',null,null,60,0,1,'ABSys_Log_CustomEventG',newid())
	set nocount off
end
go
exec Proc_InitABSoftInfo
go


--将更新表与字段的标题

--扩展属性由三个系统存储过程进行管理：
--将新扩展属性添加到数据库对象中。
--sp_addextendedproperty
--将新扩展属性更新到数据库对象中。
--sp_updateextendedproperty
--除去现有的扩展属性。
--sp_dropextendedproperty 

--检索现有扩展属性的值。
--系统函数 FN_LISTEXTENDEDPROPERTY 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_UpdateTableAndFieldCaption]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_UpdateTableAndFieldCaption]
go
CREATE PROCEDURE Proc_UpdateTableAndFieldCaption(
                                          @Type int , --=0 框架表标题更新到数据库中，=1 数据库中标题更新到框架表 
																@DatabaseNames varchar(8000), --为空时表示所有库，否则为用逗号分隔的字串
																@TableNames varchar(8000) --为空时表示所有表，否则为用逗号分隔的字串
																)
AS
begin
  set nocount on
  declare @tempMainDatabaseName varchar(100)
  set @tempMainDatabaseName=db_name()
  update ABSys_Org_ConnList set CL_DatabaseName=@tempMainDatabaseName where CL_Name='Main' and CL_DatabaseName<>@tempMainDatabaseName

  set @DatabaseNames =isnull(@DatabaseNames,'')
  set @TableNames =isnull(@TableNames,'')

	declare @tempSqltext varchar(8000)
  declare @tempDatabaseName varchar(100)
  declare @tempTableName varchar(100)
  declare @tempTableCaption varchar(100) 

  declare @tempFieldName varchar(100)
  declare @tempFieldCaption varchar(100)
  declare @tempGuid varchar(100)

  declare @tempTable table (DatabaseName varchar(100),TableName varchar(100),TableCaption varchar(100),Guid varchar(100))
  declare @tempField table (DatabaseName varchar(100),TableName varchar(100),FieldName varchar(100),FieldCaption varchar(100),Guid varchar(100))

  insert into @tempTable(DatabaseName,TableName,TableCaption,Guid)
  select CL_DatabaseName,Ta_Name,Ta_Caption,Ta_Guid
  from ABSys_Org_ConnList b 
       left join ABSys_Org_Table c on c.ta_CL_Guid=b.CL_Guid 
  where (@DatabaseNames='' or charindex(','+CL_DatabaseName+',',','+@DatabaseNames+',')>0) and 
        (@TableNames='' or charindex(','+Ta_Name+',',','+@TableNames+',')>0)   
 
  insert into @tempField(DatabaseName,TableName,FieldName,FieldCaption,Guid)
  select CL_DatabaseName,Ta_Name,Fi_Name,Fi_Caption,Fi_Guid 
  from ABSys_Org_ConnList b 
       left join ABSys_Org_Table c on c.ta_CL_Guid=b.CL_Guid 
       left join ABSys_Org_Field d on Fi_Ta_Guid= Ta_Guid 
  where (@DatabaseNames='' or charindex(','+CL_DatabaseName+',',','+@DatabaseNames+',')>0) and 
        (@TableNames='' or charindex(','+Ta_Name+',',','+@TableNames+',')>0)   

  declare tempTableCursor cursor for select DatabaseName,TableName,TableCaption,Guid from @tempTable
  open tempTableCursor
  fetch next from tempTableCursor into @tempDatabaseName,@tempTableName,@tempTableCaption,@tempGuid
  while @@fetch_status = 0
  begin
    set @tempSqltext='use '+@tempDatabaseName

    if @Type=0 
    begin
	    if (@tempTableCaption<>'') and (@tempTableName<>@tempTableCaption)
	    begin
	      set @tempSqltext=@tempSqltext+ '
             declare @tempName nvarchar(1000)
             declare @tempvalue nvarchar(1000)
             SELECT @tempvalue=cast(value as nvarchar(1000)),@tempName=Name FROM ::fn_listextendedproperty (NULL, N''user'', N''dbo'', N''table'','''+@tempTableName+''',null,null)
             set @tempName=isnull(@tempName,'''')
             set @tempvalue=isnull(@tempvalue,'''')
             if @tempvalue<>'''+@tempTableCaption+'''
             begin
	             if @tempName<>''''
	             begin
					       EXECUTE sp_updateextendedproperty N''MS_Description'', '''+@tempTableCaption+''', N''user'', N''dbo'', N''table'', N'''+@tempTableName+''',null,null
	             end
	             else 
	             begin
				         EXECUTE sp_addextendedproperty    N''MS_Description'', '''+@tempTableCaption+''', N''user'', N''dbo'', N''table'', N'''+@tempTableName+''',null,null
	             end
             end
	                                  ' 
	    end 
	    else 
	    begin
	      set @tempSqltext=@tempSqltext+ '
	             if EXISTS(SELECT * FROM ::fn_listextendedproperty (NULL, N''user'', N''dbo'', N''table'', '''+@tempTableName+''',null,null))
	               EXECUTE sp_dropextendedproperty    N''MS_Description'',N''user'', N''dbo'', N''table'', N'''+@tempTableName+''',null,null
	                                 '
	    end
    end
    else 
    begin
      set @tempSqltext=@tempSqltext+ '
             declare @tempvalue nvarchar(1000)
             SELECT @tempvalue=cast(value as nvarchar(1000)) FROM ::fn_listextendedproperty (NULL, N''user'', N''dbo'', N''table'','''+@tempTableName+''',null,null)
             set @tempvalue=isnull(@tempvalue,'''')
             if @tempvalue<>''''
      	       update '+@tempMainDatabaseName+'.dbo.ABSys_Org_Table set Ta_Caption=@tempvalue where  Ta_Guid='''+@tempGuid+'''
                                       '
    end
    exec(@tempSqltext)
--print @tempSqltext
--print @tempDatabaseName
--print @tempTableName
    fetch next from tempTableCursor into @tempDatabaseName,@tempTableName,@tempTableCaption,@tempGuid
  end
  --关闭与释放游标
  close tempTableCursor
  deallocate tempTableCursor

  declare tempFieldCursor cursor for select DatabaseName,TableName,FieldName,FieldCaption,Guid from  @tempField
  open tempFieldCursor
  fetch next from tempFieldCursor into @tempDatabaseName,@tempTableName,@tempFieldName,@tempFieldCaption,@tempGuid
  while @@fetch_status = 0
  begin
    set @tempSqltext='use '+@tempDatabaseName

    if @Type=0 
    begin
	    if (@tempFieldCaption<>'') and (@tempFieldName<>@tempFieldCaption)
	    begin
	      set @tempSqltext=@tempSqltext+ '
             declare @tempName nvarchar(1000)
             declare @tempvalue nvarchar(1000)
             SELECT @tempvalue=cast(value as nvarchar(1000)),@tempName=Name FROM ::fn_listextendedproperty (NULL, N''user'', N''dbo'', N''table'','''+@tempTableName+''',N''column'', '''+@tempFieldName+''')
             set @tempName=isnull(@tempName,'''')
             set @tempvalue=isnull(@tempvalue,'''')
             if @tempvalue<>'''+@tempFieldCaption+'''
             begin
	             if @tempName<>''''
	             begin
					       EXECUTE sp_updateextendedproperty N''MS_Description'', '''+@tempFieldCaption+''', N''user'', N''dbo'', N''table'', N'''+@tempTableName+''',N''column'', N'''+@tempFieldName+'''
	             end
	             else 
	             begin
				         EXECUTE sp_addextendedproperty    N''MS_Description'', '''+@tempFieldCaption+''', N''user'', N''dbo'', N''table'', N'''+@tempTableName+''',N''column'', N'''+@tempFieldName+'''
	             end
             end
	                                  ' 
	    end 
	    else 
	    begin
	      set @tempSqltext=@tempSqltext+ '
					     if EXISTS(SELECT * FROM ::fn_listextendedproperty (NULL, N''user'', N''dbo'', N''table'', '''+@tempTableName+''',N''column'', '''+@tempFieldName+'''))
				         EXECUTE sp_dropextendedproperty N''MS_Description'',N''user'', N''dbo'', N''table'', N'''+@tempTableName+''',N''column'', N'''+@tempFieldName+'''
	                                 '
	    end
    end
    else 
    begin
      set @tempSqltext=@tempSqltext+ '
             declare @tempvalue nvarchar(1000)
             SELECT @tempvalue=cast(value as nvarchar(1000)) FROM ::fn_listextendedproperty (NULL, N''user'', N''dbo'', N''table'','''+@tempTableName+''',N''column'', '''+@tempFieldName+''')
             set @tempvalue=isnull(@tempvalue,'''')
             if @tempvalue<>''''
      	       update '+@tempMainDatabaseName+'.dbo.ABSys_Org_Field set Fi_Caption=@tempvalue,FI_Hint=@tempvalue where Fi_Guid='''+@tempGuid+'''
                                       '
    end
    exec(@tempSqltext)
    
--print @tempSqltext
--print @tempDatabaseName
--print @tempTableName
--print @tempFieldName
    fetch next from tempFieldCursor into @tempDatabaseName,@tempTableName,@tempFieldName,@tempFieldCaption,@tempGuid
  end
  close tempFieldCursor
  deallocate tempFieldCursor

  set nocount off
end
go 

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_UpdateTableAndFieldCaption_FramkworkToDatabase]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_UpdateTableAndFieldCaption_FramkworkToDatabase]
go
CREATE PROCEDURE Proc_UpdateTableAndFieldCaption_FramkworkToDatabase(
																					@DatabaseNames varchar(8000), --为空时表示所有库，否则为用逗号分隔的字串
																					@TableNames varchar(8000) --为空时表示所有表，否则为用逗号分隔的字串
																					)
AS
begin
  exec Proc_UpdateTableAndFieldCaption 0,@DatabaseNames,@TableNames
  exec Proc_CreateABSys_TempAfterEditStructure
end
go 

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_UpdateTableAndFieldCaption_DatabaseToFramkwork]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_UpdateTableAndFieldCaption_DatabaseToFramkwork]
go
CREATE PROCEDURE Proc_UpdateTableAndFieldCaption_DatabaseToFramkwork(
																					@DatabaseNames varchar(8000), --为空时表示所有库，否则为用逗号分隔的字串
																					@TableNames varchar(8000) --为空时表示所有表，否则为用逗号分隔的字串
																					)
AS
begin
  exec Proc_UpdateTableAndFieldCaption 1,@DatabaseNames,@TableNames
end
/*
--框架表标题更新到数据库中
exec Proc_UpdateTableAndFieldCaption_FramkworkToDatabase '',''
--数据库中标题更新到框架表 
exec Proc_UpdateTableAndFieldCaption_DatabaseToFramkwork '',''
*/

go
--客户端刚启动时执行的操作,在数据库服务器端检测授权的情况，
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_CheckServiceLicenseFile]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_CheckServiceLicenseFile]
go
--@UserName=输入的用户名
--@Password=输入的密码
--@HostName=登录客户端名称
--@HostIP=登录客户端IP
--@MacAddress=登录客户端MAC
--@SPID=登录识别标志（如主程序名）
--@UseFuncRight=登录用户授权文件中是否检测菜单功能
--@FuncRight=登录用户的菜单功能
--@CustomFunc=登录用户的附加功能
--@OutMsg=为空时表示通过服务器的授权检测,不为空时表示授权不满足返回的是描述字串
CREATE PROCEDURE [dbo].[Proc_CheckServiceLicenseFile] (
@HostName NVarchar(100),
@HostIP NVarchar(100),
@MacAddress NVarchar(100),
@SPID NVarchar(100),
@UseFuncRight bit,
@FuncRight NVarchar(4000),
@CustomFunc NVarchar(4000),
@OutMsg  NVarchar(100) output
)
AS
begin
  set @OutMsg=''
end 
go
/*
declare @tempOutMsg NVarchar(100)
exec Proc_CheckServiceLicenseFile '','','','192.168.0.1','','',1,'','', @tempOutMsg output
select @tempOutMsg
*/
go


--新增或更新共用包到框架中 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_SetPublicPkgNewVersion]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_SetPublicPkgNewVersion]
go
CREATE PROCEDURE Proc_SetPublicPkgNewVersion(@FileName nvarchar (500),@SubPath nvarchar (500))
AS
begin
  set nocount on 
	declare @tempTr_Guid nvarchar(100)
	declare @tempTi_Guid nvarchar(100)
	set @tempTr_Guid=(select Tr_Guid from ABSys_Org_Tree where Tr_Code='Function Dir')
	set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Code='Framkwork' and Ti_Tr_Guid=@tempTr_Guid and Ti_ParentGuid='0')
	set @tempTi_Guid=(SELECT top 1 Ti_Guid FROM ABSys_Org_TreeItem where Ti_Code='Public' and Ti_ParentGuid=@tempTi_Guid)
	
	if not exists(SELECT * FROM ABSys_Org_Function where Fu_Ti_Guid=@tempTi_Guid and Fu_FileName=@FileName)
		insert into ABSys_Org_Function
		(Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_IsView,FU_IsUpdate,Fu_Public,Fu_Version,Fu_FileName,Fu_Pkg,Fu_Bitmap,Fu_Order,Fu_IsBeginGroup,Fu_RunAVI,Fu_Code,PubID,Fu_SubPath)
		values(@tempTi_Guid,newid(),@FileName,0,1,1,0,@FileName,null,null,10,0,1,@FileName,newid(),@SubPath)
  else 
    update ABSys_Org_Function set FU_Version= dbo.Func_GetNewVersionNo(FU_Version) where FU_FileName=@FileName

  update ABSys_Org_Parameter set Pa_Value=isnull(Pa_Value,0)+1 where  Pa_Name='FuncUpdateCount'

  set nocount off
end
	 
go
--exec Proc_SetPublicPkgNewVersion  'ABClient.exe','' 
--select * from ABSys_Org_Function where FU_FileName = 'ABClient.exe'
 

--增加或删除角色限制
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_AddOrDelKeySetup]') and OBJECTPROPERTY(id,N'IsProcedure') = 1)
drop procedure [dbo].[Proc_AddOrDelKeySetup]
go
CREATE  PROCEDURE  Proc_AddOrDelKeySetup(
@Type varchar(100),
@IsDel bit                  ,
@IsAdd bit                  ,

@Ke_Guid varchar(8000),

@KF_FU_Guid varchar(8000),
@KR_ER_Guid varchar(8000),
@KC_TI_Guid varchar(8000)
)
AS
  set nocount on
  if @Type='Func' 
  begin
	  if @IsDel=1
	  begin
		  delete ABSys_Right_KeyFunction
	    where (@Ke_Guid='' or charindex(','+cast(KF_KE_Guid as nvarchar(100))+',',','+@Ke_Guid+',')>0) and 
	          (@KF_FU_Guid='' or charindex(','+cast(KF_FU_Guid as nvarchar(100))+',',','+@KF_FU_Guid+',')>0)  
	  end
	  if @IsAdd=1
	  begin
		  insert into ABSys_Right_KeyFunction(KF_KE_Guid,KF_FU_Guid,KF_Guid,KF_Order,Pubid)
		  select Ke_Guid,Fu_Guid,Newid(),FU_Order,newid()
		  from ABSys_Org_Function,ABSys_Org_Key 
		  where (@Ke_Guid='' or charindex(','+cast(KE_Guid as nvarchar(100))+',',','+@Ke_Guid+',')>0) and 
	          (@KF_FU_Guid='' or charindex(','+cast(FU_Guid as nvarchar(100))+',',','+@KF_FU_Guid+',')>0)  and
	          KE_Guid+FU_Guid not in (select KF_KE_Guid+KF_FU_Guid from ABSys_Right_KeyFunction)	
	  end
  end
  else if @Type='ExtendReport' 
  begin
	  if @IsDel=1
	  begin
		  delete ABSys_Right_KeyExtendReport
	    where (@Ke_Guid='' or charindex(','+cast(KR_KE_Guid as nvarchar(100))+',',','+@Ke_Guid+',')>0) and 
	          (@KR_ER_Guid='' or charindex(','+cast(KR_ER_Guid as nvarchar(100))+',',','+@KR_ER_Guid+',')>0)  
	  end
	  if @IsAdd=1
	  begin
		  insert into ABSys_Right_KeyExtendReport(KR_KE_Guid,KR_ER_Guid,KR_Guid,KR_Order,Pubid)
		  select Ke_Guid,ER_Guid,Newid(),ER_Order,newid()
		  from ABSys_Org_ExtendReport,ABSys_Org_Key 
		  where (@Ke_Guid='' or charindex(','+cast(KE_Guid as nvarchar(100))+',',','+@Ke_Guid+',')>0) and 
	          (@KR_ER_Guid='' or charindex(','+cast(ER_Guid as nvarchar(100))+',',','+@KR_ER_Guid+',')>0)  and
	          KE_Guid+ER_Guid not in (select KR_KE_Guid+KR_ER_Guid from ABSys_Right_KeyExtendReport)	
	  end
  end
  else if @Type='CustomObject' 
  begin 
	  if @IsDel=1
	  begin
		  delete ABSys_Right_KeyCustomObject
	    where (@Ke_Guid='' or charindex(','+cast(KC_KE_Guid as nvarchar(100))+',',','+@Ke_Guid+',')>0) and 
	          (@KC_TI_Guid='' or charindex(','+cast(KC_TI_Guid as nvarchar(100))+',',','+@KC_TI_Guid+',')>0)  
	  end
	  if @IsAdd=1
	  begin
		  insert into ABSys_Right_KeyCustomObject(KC_KE_Guid,KC_TI_Guid,KC_Guid,KC_Order,Pubid)
		  select Ke_Guid,@KC_TI_Guid,Newid(),Ke_Order,newid()
		  from ABSys_Org_TreeItem ,ABSys_Org_Key 
		  where Ti_TR_Guid=dbo.Func_GetTreeGuid('CustomObject') and 
            (@Ke_Guid='' or charindex(','+cast(KE_Guid as nvarchar(100))+',',','+@Ke_Guid+',')>0) and 
	          (@KC_TI_Guid='' or charindex(','+cast(TI_Guid as nvarchar(100))+',',','+@KC_TI_Guid+',')>0)  and
	          KE_Guid+Ti_Guid not in (select KC_KE_Guid+KC_TI_Guid from ABSys_Right_KeyCustomObject)	
	  end
  end
  set nocount OFF 
GO

/*
exec Proc_AddOrDelKeySetup @Te_Codes,@AW_Codes_AreaWord,@Bn_Codes_AreaWord,@TimeParts_AreaWord,1,0,0
*/

--返回系统版本号
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetSysVersion]') and xtype in (N'FN', N'IF', N'TF'))
drop function[dbo].[Func_GetSysVersion]
exec(
' create function  Func_GetSysVersion() '+
' RETURNS Nvarchar(100)  '+
' AS '+ 
' begin'+
'   declare @tempResult Nvarchar(100)  '+
'   set  @tempResult=isnull((select Pa_value from ABSys_Org_Parameter where pa_name=''VersionNo''),''V1.0'')   '+
'   RETURN ( @tempResult)   '+
' end ' )
--select dbo.Func_GetSysVersion()
go



if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetTreeFieldValue]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetTreeFieldValue
go
create function Func_GetTreeFieldValue ( @aTreeLocalFieldValue Nvarchar(100),
                                      @aTreeItemLocalFieldValue Nvarchar(100),
                                      @aResultField Nvarchar(100),
                                      @aTreeLocalFieldName Nvarchar(100)='Tr_Code',
                                      @aTreeItemLocalFieldName Nvarchar(100)='Ti_Code'
                                      )
RETURNS varchar(100)
AS
BEGIN
  declare @tempStr1 varchar(1000)
	declare @obj int,@con Nvarchar(1000) 
	set @con=N'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog='+replace(db_name(),N'''',N'''''')
  exec sp_oacreate 'adodb.recordset',@obj out

  if substring(@aResultField,1,3)='Ti_' 
  begin
	  set @tempStr1=' declare @tempStr1 varchar(100) select @tempStr1=Tr_Guid  from  ABSys_Org_Tree  where  '+@aTreeLocalFieldName+'='+''''+@aTreeLocalFieldValue+''''+
	                ' select  '+@aResultField+'  from  ABSys_Org_TreeItem  where  Ti_Tr_Guid=@tempStr1 and '+
                           @aTreeItemLocalFieldName+'='+''''+@aTreeItemLocalFieldValue+''''
	  --用OLE查询 
		exec sp_oamethod @obj,'open',null,@tempStr1,@con
		exec sp_oagetproperty @obj,N'fields(0).value',@tempStr1 output
  end
  else 
  begin
	  set @tempStr1=' select @ResultStr='+@aResultField+'  from  ABSys_Org_Tree where  '+@aTreeLocalFieldName+'='+''''+@aTreeLocalFieldValue+''''
		exec sp_oamethod @obj,'open',null,@tempStr1,@con
		exec sp_oagetproperty @obj,N'fields(0).value',@tempStr1 output
  end 
  exec sp_oadestroy @obj
  RETURN(@tempStr1)
end

go 
--print dbo.Func_GetTreeFieldValue('帐户类型','集体帐户','Ti_Code',default,default)
--根据库名取库表GUID值
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetDatabseGuid]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetDatabseGuid
go
create function Func_GetDatabseGuid(@DatabseName Nvarchar(100))
RETURNS Nvarchar(100)
AS
BEGIN
	DECLARE  @tempGuid Nvarchar(100)
	set @tempGuid= (select CL_Guid from ABSys_Org_ConnList where CL_DatabaseName=@DatabseName)
  RETURN(@tempGuid)
end  
go
--根据表名取字典表中表GUID值
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetTableGuid]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetTableGuid
go
create function Func_GetTableGuid(@TableName Nvarchar(100))
RETURNS Nvarchar(100)
AS
BEGIN
	DECLARE  @tempGuid Nvarchar(100)
	set @tempGuid= (select TA_Guid from absys_org_table where Ta_Name=@TableName)
  RETURN(@tempGuid)
end 
 
--select dbo.Func_GetTableGuid('absys_org_table')
go
--根据表名和字段名取字典表中字段GUID值
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetFieldGuid]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetFieldGuid
go
create function Func_GetFieldGuid (@TableName Nvarchar(100),@FieldName Nvarchar(100) )
RETURNS varchar(100)
AS
BEGIN
  declare @tempStr1 varchar(100)
  select @tempStr1=Fi_Guid
  FROM ABSys_Org_Field
  WHERE Fi_Name=@FieldName and
        Fi_Ta_Guid=(select top 1 Ta_Guid FROM absys_org_table WHERE Ta_Name=@TableName)
  RETURN(@tempStr1)
end
--select dbo.Func_GetFieldGuid('absys_org_table','Ta_Name')
go
--根据模块文件名取功能表中文件GUID值
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetFunctionGuid]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetFunctionGuid
go
create function Func_GetFunctionGuid(@FileName Nvarchar(100))
RETURNS Nvarchar(100)
AS
BEGIN
	DECLARE  @tempGuid Nvarchar(100)
	set @tempGuid= (select Fu_Guid from ABSys_Org_Function where Fu_Code=@FileName)
  RETURN(@tempGuid)
end 
 
--select dbo.Func_GetFunctionGuid('ABClientP.exe')
go

--根据主列项编号取主列项GUID值 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetTreeGuid]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetTreeGuid
go
create function Func_GetTreeGuid (@aCode Nvarchar(100) )
RETURNS varchar(100)
AS
BEGIN
  declare @tempStr1 varchar(100)
  select @tempStr1=Tr_Guid
  FROM ABSys_Org_Tree
  WHERE Tr_Code=@aCode

  RETURN(@tempStr1)
end
go

--根据主从列项编号取从列项GUID值 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetTreeItemGuid]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetTreeItemGuid
go
create function Func_GetTreeItemGuid (@aTr_Code Nvarchar(100),@aTi_Code Nvarchar(100) )
RETURNS varchar(100)
AS
BEGIN
  declare @tempStr1 varchar(100)
  select @tempStr1=Ti_Guid
  FROM ABSys_Org_TreeItem
  WHERE Ti_Code=@aTr_Code and
        Ti_Tr_Guid=(select top 1 Tr_Guid FROM ABSys_Org_Tree WHERE Tr_Code=@aTi_Code)
  RETURN(@tempStr1)
end

go
--根据连接名得到表库名和所有者前缀
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetDatabaseAndOwnerByConnName]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetDatabaseAndOwnerByConnName
go
create function Func_GetDatabaseAndOwnerByConnName (@aConnName Nvarchar(100) )
RETURNS varchar(100)
AS
BEGIN
  declare @tempStr1 varchar(100)
  if isnull(@aConnName,'')=''
    set @aConnName='Main'

  select @tempStr1=CL_DatabaseName+'.dbo.'
  FROM ABSys_Org_ConnList
  WHERE CL_Name=@aConnName
  RETURN(@tempStr1)
end

GO
--将SQL中的连接名替换为数据库名
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_ReplaceConnNameToDatabaseName]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_ReplaceConnNameToDatabaseName]
go
CREATE PROCEDURE [dbo].[Proc_ReplaceConnNameToDatabaseName]
(
@SQL varchar(8000) output
)   
AS
	set nocount ON
	select @SQL= REPLACE(@SQL,'['+Cl_Name+']',CL_DatabaseName) 
	FROM ABSys_Org_ConnList  
	

	set nocount Off
GO
/*
declare @tempStr1 varchar(100)
SET @tempStr1='[Main]+[ERP]+[aaaa]+'
EXEC Proc_ReplaceConnNameToDatabaseName @tempStr1 OUTPUT
PRINT @tempStr1
*/
--根据连接名得到表库名和所有者前缀
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Func_GetDatabaseAndOwnerAndTableNameByTableGuid]') and xtype in (N'FN', N'IF', N'TF'))
drop function Func_GetDatabaseAndOwnerAndTableNameByTableGuid
go
create function Func_GetDatabaseAndOwnerAndTableNameByTableGuid (@aTableGuid Nvarchar(100) )
RETURNS varchar(100)
AS
BEGIN
  declare @tempStr1 varchar(100)

  select @tempStr1=CL_DatabaseName+'.dbo.Ta_Name '
  FROM ABSys_Org_Table 
       left join  ABSys_Org_ConnList on Ta_CL_Guid=CL_Guid        
  WHERE Ta_Guid=@aTableGuid
  RETURN(@tempStr1)
end

go


--检测用户的撤消单据权限
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_CheckCancelBill]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_CheckCancelBill]
go
CREATE PROCEDURE [dbo].[Proc_CheckCancelBill]
(
@BillCode varchar(100),
@MainGuid varchar(100),
@ResultStr varchar(100) output
)   
AS
	set nocount ON

	declare @tempSQL1 Nvarchar(4000)
	declare @tempStr1 Nvarchar(100)
	declare @tempBi_CancelApprovedCheckSQL	nvarchar(2000)
	declare @tempBi_CancelApprovedExecSQL	nvarchar(2000)
  set @ResultStr=''  

	select 
		@tempBi_CancelApprovedCheckSQL   =Bi_CancelApprovedCheckSQL,	 
		@tempBi_CancelApprovedExecSQL    =Bi_CancelApprovedExecSQL	 
	from ABSys_Approved_Bill  
	where Bi_Code=@BillCode 

  SET @tempBi_CancelApprovedCheckSQL=REPLACE(@tempBi_CancelApprovedCheckSQL,'[MainGuid]',@MainGuid)
  SET @tempBi_CancelApprovedExecSQL=REPLACE(@tempBi_CancelApprovedExecSQL,'[MainGuid]',@MainGuid)

	set @tempBi_CancelApprovedCheckSQL   =isnull(@tempBi_CancelApprovedCheckSQL   ,'')
	set @tempBi_CancelApprovedExecSQL   =isnull(@tempBi_CancelApprovedExecSQL   ,'')

  if (@tempBi_CancelApprovedCheckSQL<>'')           
  begin
		EXEC Proc_ReplaceConnNameToDatabaseName @tempBi_CancelApprovedCheckSQL OUTPUT
	  exec Proc_GetSQLValue @tempBi_CancelApprovedCheckSQL,@ResultStr output
    SET @ResultStr= isnull(@ResultStr,'')
  end

  if (@ResultStr='') 
  BEGIN
    if (exists (select * from  ABSys_Approved_Message where  ME_FromMainGuid=@MainGuid and ME_State<>'Cancel'))
      update ABSys_Approved_Message set ME_State='Cancel' where  ME_FromMainGuid=@MainGuid and ME_State<>'Cancel'
  	
	  if (@tempBi_CancelApprovedExecSQL<>'')           
	  begin
			EXEC Proc_ReplaceConnNameToDatabaseName @tempBi_CancelApprovedExecSQL OUTPUT
	    exec(@tempBi_CancelApprovedExecSQL)
	  end
  END 

	set nocount Off
   
go



--单据审批
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Proc_ApprovedBill]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Proc_ApprovedBill]
go
CREATE PROCEDURE [dbo].[Proc_ApprovedBill]
(
@BillCode varchar(100),
@MainGuid varchar(100),
@OutIsApprove bit output
)   
AS
	set nocount ON

  SET @OutIsApprove=0
	declare @tempSQL1 Nvarchar(4000)
	declare @tempSQLIntValue1 int
	declare @tempSQLStrValue1	nvarchar(100)

	declare @tempObject	nvarchar(100)

	declare @tempSumQuantity	decimal(18,4)
	declare @tempMinPrice	decimal(18,4)
	declare @tempMinDiscount	decimal(18,4)
	declare @tempMaxPrice	decimal(18,4)
	declare @tempMaxDiscount	decimal(18,4)
	declare @tempSumMoney	decimal(18,4)

	declare @tempOperator	nvarchar(100)
	declare @tempState	nvarchar(100)

	declare @tempBi_Guid	nvarchar(100)
	declare @tempBi_Name	nvarchar(100)
	declare @tempBi_GetCheckValueSQL	nvarchar(4000)
	declare @tempBi_BeginApprovedExecSQL	nvarchar(2000)
	
	declare @tempRU_Guid	nvarchar(100)
	declare @tempRU_Name	nvarchar(100)
	
	declare @tempCP_RequestTitle	nvarchar(100)
	declare @tempCP_ObjectValue	nvarchar(100)

	declare @tempCP_ThanQuantity	decimal(18,4)
	declare @tempCP_ThanPrice	decimal(18,4)
	declare @tempCP_ThanDiscount	decimal(18,4)
	declare @tempCP_ThanMoney	decimal(18,4)

	declare @tempCP_LessQuantity	decimal(18,4)
	declare @tempCP_LessPrice	decimal(18,4)
	declare @tempCP_LessDiscount	decimal(18,4)
	declare @tempCP_LessMoney	decimal(18,4)

	declare @tempSt_Guid	nvarchar(100)

	declare @tempME_ReMark	nvarchar(500)
  SET @tempME_ReMark=''
  
	select 
		@tempBi_Guid                   =Bi_Guid                  ,
		@tempBi_Name                   =Bi_Name                  ,
		@tempBi_GetCheckValueSQL       =Bi_GetCheckValueSQL	     ,
		@tempBi_BeginApprovedExecSQL   =Bi_BeginApprovedExecSQL	 

	from ABSys_Approved_Bill  
	where Bi_IsActive=1 and
	      Bi_Code=@BillCode 

	set @tempBi_Guid                   =isnull(@tempBi_Guid                   ,'')
	set @tempBi_Name                   =isnull(@tempBi_Name                   ,'')
	set @tempBi_GetCheckValueSQL	     =isnull(@tempBi_GetCheckValueSQL       ,'')
	set @tempBi_BeginApprovedExecSQL   =isnull(@tempBi_BeginApprovedExecSQL   ,'')
  --定义了活动单据
  if @tempBi_Guid<>'' and @tempBi_GetCheckValueSQL<>''
  begin
    --取检测值SQL
    SET @tempBi_GetCheckValueSQL=REPLACE(@tempBi_GetCheckValueSQL,'[MainGuid]',@MainGuid)
		EXEC Proc_ReplaceConnNameToDatabaseName @tempBi_GetCheckValueSQL OUTPUT
    exec sp_executesql @tempBi_GetCheckValueSQL,
												N'@tempObject nvarchar(100) OUTPUT ,@tempSumQuantity decimal(18,4) OUTPUT,@tempMinPrice decimal(18,4) OUTPUT ,@tempMinDiscount decimal(18,4) OUTPUT, 
												  @tempMaxPrice decimal(18,4) OUTPUT ,@tempMaxDiscount decimal(18,4) OUTPUT ,@tempSumMoney decimal(18,4) OUTPUT ,@tempState nvarchar(100) OUTPUT ,
												  @tempOperator nvarchar(100) OUTPUT ',
                         @tempObject=@tempObject OUTPUT,@tempSumQuantity=@tempSumQuantity OUTPUT,@tempMinPrice=@tempMinPrice OUTPUT,@tempMinDiscount=@tempMinDiscount OUTPUT,
                         @tempMaxPrice=@tempMaxPrice OUTPUT,@tempMaxDiscount=@tempMaxDiscount OUTPUT,@tempSumMoney=@tempSumMoney OUTPUT,@tempState=@tempState OUTPUT,
                         @tempOperator=@tempOperator OUTPUT

    IF @tempObject<>''
    BEGIN
		  set @tempSQL1=' select CU_Name from '+dbo.Func_GetDatabaseAndOwnerByConnName('ERP')+'ERP_Base_Cust where CU_Guid='''+@tempObject+''''
		  exec Proc_GetSQLValue @tempSQL1,@tempSQLStrValue1 output
	    if isnull(@tempSQLStrValue1,'')=''
      BEGIN
			  set @tempSQL1=' select SU_Name from '+dbo.Func_GetDatabaseAndOwnerByConnName('ERP')+'ERP_Base_Supplier where SU_Guid='''+@tempObject+''''
			  exec Proc_GetSQLValue @tempSQL1,@tempSQLStrValue1 output
      end
	    if isnull(@tempSQLStrValue1,'')<>''
        set @tempME_ReMark=case when @tempME_ReMark='' then @tempME_ReMark else @tempME_ReMark+char(10)+char(13) end+'主体对象['+@tempSQLStrValue1+']'	
    end
    IF @tempSumQuantity>0
      set @tempME_ReMark=case when @tempME_ReMark='' then @tempME_ReMark else @tempME_ReMark+char(10)+char(13) end+'总数量['+cast(@tempSumQuantity as varchar)+']'
    IF @tempMinPrice>0
      set @tempME_ReMark=case when @tempME_ReMark='' then @tempME_ReMark else @tempME_ReMark+char(10)+char(13) end+'最小价格['+cast(@tempMinPrice as varchar)+']'
    IF @tempMinDiscount>0
      set @tempME_ReMark=case when @tempME_ReMark='' then @tempME_ReMark else @tempME_ReMark+char(10)+char(13) end+'最小折扣['+cast(@tempMinDiscount as varchar)+']'
    IF @tempMaxPrice>0
      set @tempME_ReMark=case when @tempME_ReMark='' then @tempME_ReMark else @tempME_ReMark+char(10)+char(13) end+'最大价格['+cast(@tempMaxPrice as varchar)+']'
    IF @tempMaxDiscount>0
      set @tempME_ReMark=case when @tempME_ReMark='' then @tempME_ReMark else @tempME_ReMark+char(10)+char(13) end+'最大折扣['+cast(@tempMaxDiscount as varchar)+']'
    IF @tempSumMoney>0
      set @tempME_ReMark=case when @tempME_ReMark='' then @tempME_ReMark else @tempME_ReMark+char(10)+char(13) end+'总金额['+cast(@tempSumMoney as varchar)+']'
    IF @tempState<>''
      set @tempME_ReMark=case when @tempME_ReMark='' then @tempME_ReMark else @tempME_ReMark+char(10)+char(13) end+'状态['+isnull((select Ti_Name 
																											  from ABSys_Org_TreeItem 
																											  where Ti_Tr_Guid=(select Tr_Guid 
                                                                          from ABSys_Org_Tree 
                                                                          where Tr_Code='BusinessOrderState') and 
                                                              Ti_Code=@tempState),'')+']'
    IF @tempOperator<>''
      set @tempME_ReMark=case when @tempME_ReMark='' then @tempME_ReMark else @tempME_ReMark+char(10)+char(13) end+'操作员['+isnull((select OP_Name from ABSys_Org_Operator where OP_Guid=@tempOperator),'')+']'

    --请求审批的数据处于已录入状态
    --防止一操作员请求审批后另一操作员没刷新又对同一单据请求了审批
    if isnull(@tempState,'')='Input'
    begin
	    --取出单据定义的所有审批规则
			declare tempUpdateTypeCursor cursor for 
				select
		       RU_Guid          ,
				   RU_Name          ,
				   CP_RequestTitle  ,
				   CP_ObjectValue   ,
				   CP_ThanQuantity      ,
				   CP_ThanPrice         ,
				   CP_ThanDiscount      ,
				   CP_ThanMoney         ,
				   CP_LessQuantity      ,
				   CP_LessPrice         ,
				   CP_LessDiscount      ,
				   CP_LessMoney          
				from ABSys_Approved_Rule
				     left join ABSys_Approved_ConditionParam on Ru_Guid=CP_RU_Guid
				where RU_IsActive=1 and CP_Bi_Guid=@tempBi_Guid 
	
			open tempUpdateTypeCursor 
			fetch next from tempUpdateTypeCursor into  @tempRU_Guid         ,@tempRU_Name         ,@tempCP_RequestTitle ,@tempCP_ObjectValue  ,
                                                 @tempCP_ThanQuantity ,@tempCP_ThanPrice    ,@tempCP_ThanDiscount ,@tempCP_ThanMoney    ,  
                                                 @tempCP_LessQuantity ,@tempCP_LessPrice    ,@tempCP_LessDiscount ,@tempCP_LessMoney      
		  while @@fetch_status = 0
			begin
			  set @tempRU_Guid         =isnull(@tempRU_Guid        ,'')
			  set @tempRU_Name         =isnull(@tempRU_Name        ,'')
			  set @tempCP_RequestTitle =isnull(@tempCP_RequestTitle,'')
			  set @tempCP_ObjectValue  =isnull(@tempCP_ObjectValue ,'')

			  set @tempCP_ThanQuantity     =isnull(@tempCP_ThanQuantity ,0 )
			  set @tempCP_ThanPrice        =isnull(@tempCP_ThanPrice    ,0 )
			  set @tempCP_ThanDiscount     =isnull(@tempCP_ThanDiscount ,0 )
			  set @tempCP_ThanMoney        =isnull(@tempCP_ThanMoney    ,0 )

			  set @tempCP_LessQuantity     =isnull(@tempCP_LessQuantity ,0 )
			  set @tempCP_LessPrice        =isnull(@tempCP_LessPrice    ,0 )
			  set @tempCP_LessDiscount     =isnull(@tempCP_LessDiscount ,0 )
			  set @tempCP_LessMoney        =isnull(@tempCP_LessMoney    ,0 )

			  if @tempRU_Guid<>''
			  begin
	  		  --单据录入人是否在例外列表中
	        if @tempOperator not in (select RO_OP_Guid from ABSys_Approved_PassRuleOperator where RO_RU_Guid=@tempRU_Guid)
	        begin
   		      --判断审批的条件是否成立
            if (@tempCP_ObjectValue<>'' and @tempCP_ObjectValue=@tempObject) or 

               (@tempCP_ThanQuantity>0     and @tempSumQuantity>=@tempCP_ThanQuantity) or 
               (@tempCP_ThanPrice>0        and @tempMaxPrice   >=@tempCP_ThanPrice   ) or 
               (@tempCP_ThanDiscount>0     and @tempMaxDiscount>=@tempCP_ThanDiscount) or
               (@tempCP_ThanMoney>0        and @tempSumMoney   >=@tempCP_ThanMoney   ) or 

               (@tempCP_LessQuantity>0     and @tempSumQuantity<=@tempCP_LessQuantity) or 
               (@tempCP_LessPrice>0        and @tempMinPrice   <=@tempCP_LessPrice   ) or 
               (@tempCP_LessDiscount>0     and @tempMinDiscount<=@tempCP_LessDiscount) or
               (@tempCP_LessMoney>0        and @tempSumMoney   <=@tempCP_LessMoney   )   

            begin
              --取第一审批步骤生成审批消息
			        select top 1  
                 @tempSt_Guid=St_Guid
					    from ABSys_Approved_Step  
					    where ST_IsActive=1 and St_Ru_Guid=@tempRU_Guid AND ST_NeedPassCount>0
			        order by ST_order asc

			        set @tempSt_Guid         =isnull(@tempSt_Guid        ,'')
						  if @tempRU_Guid<>''
						  begin
							  if exists (select * from  ABSys_Approved_Message where  ME_FromMainGuid=@MainGuid and ME_State<>'Cancel')
						      delete ABSys_Approved_Message where  ME_FromMainGuid=@MainGuid and ME_State<>'Cancel'

							  insert into ABSys_Approved_Message( Me_Guid,ME_RequestOp_Guid,ME_RequestDatetime,ME_RequestTitle,ME_ApprovedOp_Guid,ME_State,
							                                      ME_Bi_Guid,ME_ST_Guid,ME_FromMainGuid,PubID,ME_ReMark) 
							  select  newid(),@tempOperator,getdate(),@tempCP_RequestTitle,SO_OP_Guid,'WaitFor',
                        @tempBi_Guid,@tempSt_Guid,@MainGuid,newid(),@tempME_ReMark
						    from  ABSys_Approved_StepOperator    
						    where So_St_Guid  = @tempSt_Guid and So_IsActive=1
 
                IF @tempBi_BeginApprovedExecSQL<>''
                begin
							    SET @tempBi_BeginApprovedExecSQL=REPLACE(@tempBi_BeginApprovedExecSQL,'[MainGuid]',@MainGuid)
		              EXEC Proc_ReplaceConnNameToDatabaseName @tempBi_BeginApprovedExecSQL OUTPUT
							    exec(@tempBi_BeginApprovedExecSQL)
                end
                IF @OutIsApprove=0
                  SET @OutIsApprove=1
              end
            end 
	        end		
		    end
				fetch next from tempUpdateTypeCursor into  @tempRU_Guid         ,@tempRU_Name         ,@tempCP_RequestTitle ,@tempCP_ObjectValue  ,
                                                 @tempCP_ThanQuantity ,@tempCP_ThanPrice    ,@tempCP_ThanDiscount ,@tempCP_ThanMoney    ,  
                                                 @tempCP_LessQuantity ,@tempCP_LessPrice    ,@tempCP_LessDiscount ,@tempCP_LessMoney      
			end
			close tempUpdateTypeCursor
			deallocate tempUpdateTypeCursor

    end
  end
	set nocount off

go




--审批步骤触发器
IF EXISTS (SELECT name FROM sysobjects WHERE name = 'ABSys_Approved_Message_Update' AND type = 'TR')
  drop TRIGGER ABSys_Approved_Message_Update
go
CREATE TRIGGER  ABSys_Approved_Message_Update    ON [dbo].[ABSys_Approved_Message]  
FOR  UPDATE    
AS 
	set nocount on

	declare @tempME_RequestOp_Guid     nvarchar(100)
	declare @tempME_RequestDatetime    datetime
	declare @tempME_RequestTitle       nvarchar(100)
	declare @tempME_ApprovedOp_Guid    nvarchar(100)
	declare @tempME_State              nvarchar(100)
	declare @tempME_NoAgreeWhy         nvarchar(100)
	declare @tempME_Datetime           datetime
	declare @tempME_Bi_Guid            nvarchar(100)
	declare @tempME_ST_Guid            nvarchar(100)
	declare @tempME_FromMainGuid       nvarchar(100)
	declare @tempME_ReMark             nvarchar(500)

	declare @tempST_RU_Guid	      nvarchar(100)
	declare @tempST_Name          nvarchar(100)
	declare @tempST_NeedPassCount int
	declare @tempST_IsActive      bit
	declare @tempST_Order	        int
		
	declare @tempBi_EndApprovedExecSQL       nvarchar(4000)
	declare @tempBi_ApprovedNoAgreeExecSQL   nvarchar(4000)


	declare @tempRealPassCount int
 

  --取得当前审批消息、当前审批步骤、当前审批单据的信息
  select 
		@tempME_RequestOp_Guid    =ME_RequestOp_Guid   ,
		@tempME_RequestDatetime   =ME_RequestDatetime  ,
		@tempME_RequestTitle      =ME_RequestTitle     ,
		@tempME_ApprovedOp_Guid   =ME_ApprovedOp_Guid  ,
		@tempME_State             =ME_State            ,
		@tempME_NoAgreeWhy        =ME_NoAgreeWhy       ,
		@tempME_Datetime          =ME_Datetime         ,
		@tempME_Bi_Guid           =ME_Bi_Guid          ,
		@tempME_ST_Guid           =ME_ST_Guid          ,
		@tempME_FromMainGuid      =ME_FromMainGuid     ,
		@tempME_ReMark            =ME_ReMark
  from inserted 

	set @tempME_RequestOp_Guid    =isnull(@tempME_RequestOp_Guid   ,'');
	set @tempME_RequestDatetime   =isnull(@tempME_RequestDatetime  ,0 );
	set @tempME_RequestTitle      =isnull(@tempME_RequestTitle     ,'');
	set @tempME_ApprovedOp_Guid   =isnull(@tempME_ApprovedOp_Guid  ,'');
	set @tempME_State             =isnull(@tempME_State            ,'');
	set @tempME_NoAgreeWhy        =isnull(@tempME_NoAgreeWhy       ,'');
	set @tempME_Datetime          =isnull(@tempME_Datetime         ,0 );
	set @tempME_Bi_Guid           =isnull(@tempME_Bi_Guid          ,'');
	set @tempME_ST_Guid           =isnull(@tempME_ST_Guid          ,'');
	set @tempME_FromMainGuid      =isnull(@tempME_FromMainGuid     ,'');
	set @tempME_ReMark            =isnull(@tempME_ReMark           ,'');

  SELECT
		@tempST_RU_Guid	        = ST_RU_Guid         , 
		@tempST_Name            = ST_Name            ,
		@tempST_NeedPassCount   = ST_NeedPassCount   ,
		@tempST_IsActive        = ST_IsActive        ,
		@tempST_Order	          = ST_Order
  from  ABSys_Approved_Step 
  where St_Guid=@tempME_ST_Guid 
	
	set @tempST_RU_Guid	         =isnull(@tempST_RU_Guid	     ,''); 
	set @tempST_Name             =isnull(@tempST_Name          ,'');
	set @tempST_NeedPassCount    =isnull(@tempST_NeedPassCount ,0);
	set @tempST_IsActive         =isnull(@tempST_IsActive      ,0);
	set @tempST_Order	           =isnull(@tempST_Order	       ,0);
	
  select 
		@tempBi_EndApprovedExecSQL      = Bi_EndApprovedExecSQL      ,
		@tempBi_ApprovedNoAgreeExecSQL  = Bi_ApprovedNoAgreeExecSQL  		
  from ABSys_Approved_Bill 
  where  Bi_Guid=@tempME_Bi_Guid
 
	set  @tempBi_EndApprovedExecSQL     =isnull( @tempBi_EndApprovedExecSQL     ,'');
	set  @tempBi_ApprovedNoAgreeExecSQL =isnull( @tempBi_ApprovedNoAgreeExecSQL ,'');
	

  if (UPDATE(Me_State)) 
  begin
	  if (@tempME_State in ('Approve')) 
	  begin  
	    --当前审批步骤必须审批的人员是否都已审批同意
	    if NOT exists(
					     select *
					     from  ABSYS_Approved_StepOperator 
					     where So_St_Guid=@tempME_ST_Guid and So_IsNeed=1 and So_IsActive=1 and 
					           SO_OP_Guid NOT in ( select ME_ApprovedOp_Guid 
		                                     from  ABSys_Approved_Message  
		                                     where Me_State='Approve' and  Me_St_Guid=@tempME_ST_Guid and ME_FromMainGuid=@tempME_FromMainGuid
		                                     ) 
	                 )
	    begin
	      --当前审批步骤是否达到最少人数
	      select @tempRealPassCount=count(*) 
        from  ABSys_Approved_Message 
        where Me_State='Approve' and  Me_St_Guid=@tempME_ST_Guid and ME_FromMainGuid=@tempME_FromMainGuid
        SET @tempRealPassCount=ISNULL(@tempRealPassCount,0)

	      if @tempRealPassCount>=@tempST_NeedPassCount  
	      begin
          --转到下一审批步骤
          SET @tempME_ST_Guid=
          ISNULL((
							    select top 1 St_Guid  
				          from ABSys_Approved_Step    
							    where St_IsActive=1 and St_Ru_Guid=@tempST_RU_Guid and St_order>@tempST_Order 
						      order by St_order asc),'')
		
          --转到下一审批步骤
		      if @tempME_ST_Guid<>''
	        begin   
					  if exists (select * from  ABSys_Approved_Message where  ME_FromMainGuid=@tempME_FromMainGuid AND ME_ST_Guid=@tempME_ST_Guid and ME_State<>'Cancel')
				      delete ABSys_Approved_Message where  ME_FromMainGuid=@tempME_FromMainGuid AND ME_ST_Guid=@tempME_ST_Guid and ME_State<>'Cancel'

					  insert into ABSys_Approved_Message( Me_Guid,ME_RequestOp_Guid,ME_RequestDatetime,ME_RequestTitle,ME_ApprovedOp_Guid,ME_State,
					                                      ME_Bi_Guid,ME_ST_Guid,ME_FromMainGuid,PubID,ME_ReMark) 
					  select  newid(),@tempME_RequestOp_Guid,getdate(),@tempME_RequestTitle,SO_OP_Guid,'WaitFor',
                    @tempME_Bi_Guid,@tempME_ST_Guid,@tempME_FromMainGuid,newid(),@tempME_ReMark
				    from  ABSys_Approved_StepOperator    
				    where So_St_Guid  = @tempME_ST_Guid and So_IsActive=1
	        END
          --没有下一审批审批步骤了则结束审批
	        ELSE if @tempME_ST_Guid ='' 
	        begin  
            IF @tempBi_EndApprovedExecSQL<>''
            begin
					    SET @tempBi_EndApprovedExecSQL=REPLACE(@tempBi_EndApprovedExecSQL,'[MainGuid]',@tempME_FromMainGuid)
		          EXEC Proc_ReplaceConnNameToDatabaseName @tempBi_EndApprovedExecSQL OUTPUT
					    exec(@tempBi_EndApprovedExecSQL)
            END
	        end   
		    end 
		  end
	  end
	  ELSE if (@tempME_State in ('Dissent')) 
	  begin
      IF @tempBi_ApprovedNoAgreeExecSQL<>''
      begin
		    SET @tempBi_ApprovedNoAgreeExecSQL=REPLACE(@tempBi_ApprovedNoAgreeExecSQL,'[MainGuid]',@tempME_FromMainGuid)
		    SET @tempBi_ApprovedNoAgreeExecSQL=REPLACE(@tempBi_ApprovedNoAgreeExecSQL,'[NoAgreeMessage]','第一次审批退回:'+@tempME_NoAgreeWhy+CHAR(10)+CHAR(13))
        EXEC Proc_ReplaceConnNameToDatabaseName @tempBi_ApprovedNoAgreeExecSQL OUTPUT
		    exec(@tempBi_ApprovedNoAgreeExecSQL)
      END
      update ABSys_Approved_Message set ME_State='Cancel' where  ME_FromMainGuid=@tempME_FromMainGuid and ME_State<>'Cancel'
	  end
  end
	set nocount off

go

 
declare @BegSize varchar(100)  
declare @EndSize varchar(100) 
declare @tempDatabase varchar(100) 
set @tempDatabase =DB_Name()
exec Proc_ClearLog @tempDatabase,@BegSize,@EndSize

 