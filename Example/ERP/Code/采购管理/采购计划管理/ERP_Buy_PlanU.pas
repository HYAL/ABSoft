unit ERP_Buy_PlanU;

interface

uses
  ABFramkWorkFuncFormU,
  ABPubDBU,
  ABFramkWorkFuncU,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, dxStatusBar,
  ABFramkWorkControlU, ABFramkWorkDBPanelU, Vcl.ExtCtrls, ABPubPanelU,
  ABFramkWorkDBNavigatorU, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, ABFramkWorkcxGridU, cxGrid, ABFramkWorkQueryU,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, ABThirdDBU, ABThirdCustomQueryU,
  ABThirdQueryU, ABFramkWorkDictionaryQueryU, dxBarBuiltInMenu, cxPC, cxSplitter,
  cxContainer, cxImage, cxDBEdit, Vcl.ImgList, Vcl.StdCtrls,
  ABFramkWorkQuerySelectFieldPanelU, cxGroupBox;

type
  TERP_Buy_PlanForm = class(TABFuncForm)
    ABDBStatusBar1: TABdxDBStatusBar;
    ABcxPageControl1: TABcxPageControl;
    cxTabSheet1: TcxTabSheet;
    Panel1: TPanel;
    Panel3: TPanel;
    ABcxPageControl2: TABcxPageControl;
    cxTabSheet_11: TcxTabSheet;
    ABDBPanel1: TABDBPanel;
    ABcxPageControl3: TABcxPageControl;
    cxTabSheet1_1: TcxTabSheet;
    ABcxGrid2: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    ABDBNavigator2: TABDBNavigator;
    ABcxSplitter2: TABcxSplitter;
    ABcxSplitter1: TABcxSplitter;
    ABDBNavigator1: TABDBNavigator;
    cxTabSheet2: TcxTabSheet;
    Panel2: TPanel;
    Panel4: TPanel;
    ABcxPageControl4: TABcxPageControl;
    cxTabSheet4: TcxTabSheet;
    ABDBPanel2: TABDBPanel;
    ABcxPageControl5: TABcxPageControl;
    cxTabSheet5: TcxTabSheet;
    ABcxGrid3: TABcxGrid;
    ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView;
    cxGridLevel2: TcxGridLevel;
    ABDBNavigator4: TABDBNavigator;
    ABcxSplitter3: TABcxSplitter;
    ABcxGrid4: TABcxGrid;
    ABcxGridDBBandedTableView3: TABcxGridDBBandedTableView;
    cxGridLevel3: TcxGridLevel;
    ABcxSplitter4: TABcxSplitter;
    cxTabSheet3: TcxTabSheet;
    Panel5: TPanel;
    Panel6: TPanel;
    ABcxPageControl6: TABcxPageControl;
    cxTabSheet6: TcxTabSheet;
    ABDBPanel3: TABDBPanel;
    ABcxPageControl7: TABcxPageControl;
    cxTabSheet7: TcxTabSheet;
    ABcxGrid5: TABcxGrid;
    ABcxGridDBBandedTableView4: TABcxGridDBBandedTableView;
    cxGridLevel4: TcxGridLevel;
    ABDBNavigator5: TABDBNavigator;
    ABcxSplitter5: TABcxSplitter;
    ABcxGrid6: TABcxGrid;
    ABcxGridDBBandedTableView5: TABcxGridDBBandedTableView;
    cxGridLevel5: TcxGridLevel;
    ABcxSplitter6: TABcxSplitter;
    ABDBNavigator3: TABDBNavigator;
    ABDatasource1: TABDatasource;
    ABQuery1_1: TABQuery;
    ABDatasource1_1: TABDatasource;
    ABDatasource2_1: TABDatasource;
    ABQuery2_1: TABQuery;
    ABDatasource2: TABDatasource;
    ABQuery2: TABQuery;
    ABDatasource3_1: TABDatasource;
    ABQuery3_1: TABQuery;
    ABDatasource3: TABDatasource;
    ABQuery3: TABQuery;
    ABQuery1: TABQuery;
    Panel7: TPanel;
    ABcxGrid1: TABcxGrid;
    ABcxGrid1ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    ABcxGrid1Level1: TcxGridLevel;
    Panel8: TPanel;
    ABQuerySelectFieldPanel1: TABQuerySelectFieldPanel;
    ABDBNavigator7: TABDBNavigator;
    procedure ABcxPageControl1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ERP_Buy_PlanForm: TERP_Buy_PlanForm;

implementation
{$R *.dfm}
procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :='TERP_Buy_PlanForm';
end;

exports
   ABRegister ;

procedure TERP_Buy_PlanForm.ABcxPageControl1Change(Sender: TObject);
begin
  ABInitFormPageControl(
                        ABcxPageControl1,

                        ABDBNavigator3,
                        ABDBStatusBar1,

                        [ABDatasource1,ABDatasource2,ABDatasource3],
                        [[ABDBPanel1],[ABDBPanel2],[ABDBPanel3]],
                        [[ABcxGrid1ABcxGridDBBandedTableView1],[ABcxGridDBBandedTableView3],[ABcxGridDBBandedTableView5]],
                        [true,true,False],
                        [true,true,True],
                        [true,true,True],

                        [[ABDatasource1_1],[ABDatasource2_1],[ABDatasource3_1]],
                        [[[]],[[]],[[]]],
                        [[[ABcxGridDBBandedTableView1]],[[ABcxGridDBBandedTableView2]],[[ABcxGridDBBandedTableView4]]],
                        [[true],[true],[true]],
                        [[true],[true],[true]],
                        [[true],[true],[true]],

                        [true,true,False]
                        );
end;

procedure TERP_Buy_PlanForm.FormCreate(Sender: TObject);
begin
  ABcxPageControl1Change(ABcxPageControl1);
end;

initialization
  RegisterClass(TERP_Buy_PlanForm);
finalization
  UnRegisterClass(TERP_Buy_PlanForm);
end.
