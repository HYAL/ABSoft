object ERP_Base_MaterielForm: TERP_Base_MaterielForm
  Left = 0
  Top = 0
  Caption = #29289#26009#31649#29702
  ClientHeight = 550
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TABcxSplitter
    Left = 221
    Top = 0
    Width = 8
    Height = 531
    HotZoneClassName = 'TcxMediaPlayer8Style'
    InvertDirection = True
    Control = Panel1
  end
  object ABDBStatusBar1: TABdxDBStatusBar
    Left = 0
    Top = 531
    Width = 750
    Height = 19
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 50
      end>
    PaintStyle = stpsUseLookAndFeel
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    StopUpdate = False
    DataSource = ABDatasource1
  end
  object Panel5: TPanel
    Left = 229
    Top = 0
    Width = 521
    Height = 531
    Align = alClient
    Caption = 'Panel5'
    TabOrder = 1
    object ABcxPageControl1: TABcxPageControl
      Left = 1
      Top = 27
      Width = 519
      Height = 503
      Align = alClient
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentBackground = False
      ParentColor = False
      ParentFont = False
      TabOrder = 0
      Properties.ActivePage = cxTabSheet1
      Properties.CustomButtons.Buttons = <>
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
      ActivePageIndex = 0
      ClientRectBottom = 502
      ClientRectLeft = 1
      ClientRectRight = 518
      ClientRectTop = 21
      object cxTabSheet1: TcxTabSheet
        Caption = #21015#34920#25968#25454
        ImageIndex = 0
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object ABcxGrid1: TABcxGrid
          Left = 0
          Top = 0
          Width = 517
          Height = 481
          Align = alClient
          TabOrder = 0
          object ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
            PopupMenu.AutoHotkeys = maManual
            PopupMenu.CloseFootStr = False
            PopupMenu.LinkTableView = ABcxGridDBBandedTableView1
            PopupMenu.AutoApplyBestFit = True
            PopupMenu.AutoCreateAllItem = True
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = ABDatasource1
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Filter.AutoDataSetFilter = True
            DataController.Filter.TranslateBetween = True
            DataController.Filter.TranslateIn = True
            DataController.Filter.TranslateLike = True
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.AlwaysShowEditor = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.DataRowSizing = True
            OptionsData.Deleting = False
            OptionsData.Inserting = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsSelection.MultiSelect = True
            OptionsView.CellAutoHeight = True
            OptionsView.GroupByBox = False
            OptionsView.BandHeaders = False
            Bands = <
              item
              end>
            ExtPopupMenu.AutoHotkeys = maManual
            ExtPopupMenu.CloseFootStr = False
            ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView1
            ExtPopupMenu.AutoApplyBestFit = True
            ExtPopupMenu.AutoCreateAllItem = True
          end
          object cxGridLevel1: TcxGridLevel
            GridView = ABcxGridDBBandedTableView1
          end
        end
      end
      object cxTabSheet2: TcxTabSheet
        Caption = #35814#32454#25968#25454
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 517
          Height = 481
          Align = alClient
          BevelInner = bvLowered
          BorderWidth = 1
          Caption = 'Panel2'
          Ctl3D = True
          ParentCtl3D = False
          TabOrder = 0
          object ABcxPageControl2: TABcxPageControl
            Left = 3
            Top = 3
            Width = 511
            Height = 475
            Align = alClient
            TabOrder = 0
            Properties.ActivePage = Main_Sheet1
            Properties.CustomButtons.Buttons = <>
            LookAndFeel.Kind = lfFlat
            LookAndFeel.NativeStyle = False
            ActivePageIndex = 0
            ClientRectBottom = 474
            ClientRectLeft = 1
            ClientRectRight = 510
            ClientRectTop = 21
            object Main_Sheet1: TcxTabSheet
              Caption = #29289#26009#20449#24687
              ImageIndex = 0
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object ABcxPageControl3: TABcxPageControl
                Left = 0
                Top = 0
                Width = 509
                Height = 453
                Align = alClient
                TabOrder = 0
                Properties.ActivePage = cxTabSheet3
                Properties.CustomButtons.Buttons = <>
                Properties.Rotate = True
                Properties.TabPosition = tpLeft
                LookAndFeel.Kind = lfFlat
                ActivePageIndex = 0
                ClientRectBottom = 452
                ClientRectLeft = 45
                ClientRectRight = 508
                ClientRectTop = 1
                object cxTabSheet3: TcxTabSheet
                  Caption = #22522#26412
                  ImageIndex = 0
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object Main_Panel1: TABDBPanel
                    Left = 0
                    Top = 0
                    Width = 463
                    Height = 451
                    Align = alClient
                    BevelOuter = bvNone
                    ShowCaption = False
                    TabOrder = 0
                    ReadOnly = False
                    DataSource = ABDatasource1
                    AddAnchors_akRight = True
                    AddAnchors_akBottom = True
                    AutoHeight = True
                    AutoWidth = True
                  end
                end
                object cxTabSheet4: TcxTabSheet
                  Caption = #20215#26684
                  ImageIndex = 1
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object Main_Panel2: TABDBPanel
                    Left = 0
                    Top = 0
                    Width = 463
                    Height = 451
                    Align = alClient
                    BevelOuter = bvNone
                    ShowCaption = False
                    TabOrder = 0
                    ReadOnly = False
                    DataSource = ABDatasource1
                    AddAnchors_akRight = True
                    AddAnchors_akBottom = True
                    AutoHeight = True
                    AutoWidth = True
                  end
                end
                object cxTabSheet5: TcxTabSheet
                  Caption = #20179#24211
                  ImageIndex = 2
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object Main_Panel3: TABDBPanel
                    Left = 0
                    Top = 0
                    Width = 463
                    Height = 451
                    Align = alClient
                    BevelOuter = bvNone
                    ShowCaption = False
                    TabOrder = 0
                    ReadOnly = False
                    DataSource = ABDatasource1
                    AddAnchors_akRight = True
                    AddAnchors_akBottom = True
                    AutoHeight = True
                    AutoWidth = True
                  end
                end
              end
            end
            object Detail_Sheet1: TcxTabSheet
              Caption = #29289#26009#21333#20301
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Detail_Grid1: TABcxGrid
                Left = 0
                Top = 26
                Width = 509
                Height = 427
                Align = alClient
                TabOrder = 1
                object Detail_TableView1: TABcxGridDBBandedTableView
                  PopupMenu.AutoHotkeys = maManual
                  PopupMenu.CloseFootStr = False
                  PopupMenu.LinkTableView = Detail_TableView1
                  PopupMenu.AutoApplyBestFit = True
                  PopupMenu.AutoCreateAllItem = True
                  Navigator.Buttons.CustomButtons = <>
                  DataController.DataSource = ABDatasource1_1
                  DataController.Filter.Options = [fcoCaseInsensitive]
                  DataController.Filter.AutoDataSetFilter = True
                  DataController.Filter.TranslateBetween = True
                  DataController.Filter.TranslateIn = True
                  DataController.Filter.TranslateLike = True
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsBehavior.AlwaysShowEditor = True
                  OptionsBehavior.FocusCellOnTab = True
                  OptionsBehavior.GoToNextCellOnEnter = True
                  OptionsCustomize.ColumnsQuickCustomization = True
                  OptionsCustomize.DataRowSizing = True
                  OptionsData.Deleting = False
                  OptionsData.Inserting = False
                  OptionsSelection.HideFocusRectOnExit = False
                  OptionsSelection.MultiSelect = True
                  OptionsView.CellAutoHeight = True
                  OptionsView.GroupByBox = False
                  OptionsView.BandHeaders = False
                  Bands = <
                    item
                    end>
                  ExtPopupMenu.AutoHotkeys = maManual
                  ExtPopupMenu.CloseFootStr = False
                  ExtPopupMenu.LinkTableView = Detail_TableView1
                  ExtPopupMenu.AutoApplyBestFit = True
                  ExtPopupMenu.AutoCreateAllItem = True
                end
                object Detail_Level1: TcxGridLevel
                  GridView = Detail_TableView1
                end
              end
              object Detail_Navigator1: TABDBNavigator
                Left = 0
                Top = 0
                Width = 509
                Height = 26
                Align = alTop
                BevelOuter = bvNone
                ShowCaption = False
                TabOrder = 0
                BigGlyph = False
                ImageLayout = blGlyphLeft
                DataSource = ABDatasource1_1
                VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
                ButtonRangeType = RtDetail
                BtnCustom1ImageIndex = -1
                BtnCustom2ImageIndex = -1
                BtnCustom3ImageIndex = -1
                BtnCustom4ImageIndex = -1
                BtnCustom5ImageIndex = -1
                BtnCustom6ImageIndex = -1
                BtnCustom7ImageIndex = -1
                BtnCustom8ImageIndex = -1
                BtnCustom9ImageIndex = -1
                BtnCustom10ImageIndex = -1
                BtnCustom1Caption = #33258#23450#20041'1'
                BtnCustom2Caption = #33258#23450#20041'2'
                BtnCustom3Caption = #33258#23450#20041'3'
                BtnCustom4Caption = #33258#23450#20041'4'
                BtnCustom5Caption = #33258#23450#20041'5'
                BtnCustom6Caption = #33258#23450#20041'6'
                BtnCustom7Caption = #33258#23450#20041'7'
                BtnCustom8Caption = #33258#23450#20041'8'
                BtnCustom9Caption = #33258#23450#20041'9'
                BtnCustom10Caption = #33258#23450#20041'10'
                BtnCustom1Kind = cxbkStandard
                BtnCustom2Kind = cxbkStandard
                BtnCustom3Kind = cxbkStandard
                BtnCustom4Kind = cxbkStandard
                BtnCustom5Kind = cxbkStandard
                BtnCustom6Kind = cxbkStandard
                BtnCustom7Kind = cxbkStandard
                BtnCustom8Kind = cxbkStandard
                BtnCustom9Kind = cxbkStandard
                BtnCustom10Kind = cxbkStandard
                ApprovedRollbackButton = nbCustom1
                ApprovedCommitButton = nbCustom1
                ButtonControlType = ctSingle
              end
            end
            object Main_Sheet2: TcxTabSheet
              Caption = #29289#26009#29256#26412
              ImageIndex = 0
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel4: TPanel
                Left = 0
                Top = 26
                Width = 509
                Height = 427
                Align = alClient
                TabOrder = 0
                object Detail_Grid2: TABcxGrid
                  Left = 1
                  Top = 1
                  Width = 156
                  Height = 425
                  Align = alClient
                  TabOrder = 0
                  object Detail_TableView2: TABcxGridDBBandedTableView
                    PopupMenu.AutoHotkeys = maManual
                    PopupMenu.CloseFootStr = False
                    PopupMenu.LinkTableView = Detail_TableView2
                    PopupMenu.AutoApplyBestFit = True
                    PopupMenu.AutoCreateAllItem = True
                    Navigator.Buttons.CustomButtons = <>
                    DataController.DataSource = ABDatasource1_2
                    DataController.Filter.Options = [fcoCaseInsensitive]
                    DataController.Filter.AutoDataSetFilter = True
                    DataController.Filter.TranslateBetween = True
                    DataController.Filter.TranslateIn = True
                    DataController.Filter.TranslateLike = True
                    DataController.Summary.DefaultGroupSummaryItems = <>
                    DataController.Summary.FooterSummaryItems = <>
                    DataController.Summary.SummaryGroups = <>
                    OptionsBehavior.AlwaysShowEditor = True
                    OptionsBehavior.FocusCellOnTab = True
                    OptionsBehavior.GoToNextCellOnEnter = True
                    OptionsCustomize.ColumnsQuickCustomization = True
                    OptionsCustomize.DataRowSizing = True
                    OptionsData.Deleting = False
                    OptionsData.Inserting = False
                    OptionsSelection.HideFocusRectOnExit = False
                    OptionsSelection.MultiSelect = True
                    OptionsView.CellAutoHeight = True
                    OptionsView.GroupByBox = False
                    OptionsView.BandHeaders = False
                    Bands = <
                      item
                      end>
                    ExtPopupMenu.AutoHotkeys = maManual
                    ExtPopupMenu.CloseFootStr = False
                    ExtPopupMenu.LinkTableView = Detail_TableView2
                    ExtPopupMenu.AutoApplyBestFit = True
                    ExtPopupMenu.AutoCreateAllItem = True
                  end
                  object Detail_Level2: TcxGridLevel
                    GridView = Detail_TableView2
                  end
                end
                object ABcxSplitter1: TABcxSplitter
                  Left = 157
                  Top = 1
                  Width = 8
                  Height = 425
                  HotZoneClassName = 'TcxMediaPlayer8Style'
                  AlignSplitter = salRight
                  InvertDirection = True
                  Control = Panel3
                end
                object Panel3: TPanel
                  Left = 165
                  Top = 1
                  Width = 343
                  Height = 425
                  Align = alRight
                  BevelOuter = bvNone
                  TabOrder = 2
                  object Detail_Navigator4: TABDBNavigator
                    Left = 0
                    Top = 258
                    Width = 343
                    Height = 26
                    Align = alTop
                    BevelOuter = bvNone
                    ShowCaption = False
                    TabOrder = 0
                    BigGlyph = False
                    ImageLayout = blGlyphLeft
                    DataSource = ABDatasource1_2_2
                    VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
                    ButtonRangeType = RtDetail
                    BtnCustom1ImageIndex = -1
                    BtnCustom2ImageIndex = -1
                    BtnCustom3ImageIndex = -1
                    BtnCustom4ImageIndex = -1
                    BtnCustom5ImageIndex = -1
                    BtnCustom6ImageIndex = -1
                    BtnCustom7ImageIndex = -1
                    BtnCustom8ImageIndex = -1
                    BtnCustom9ImageIndex = -1
                    BtnCustom10ImageIndex = -1
                    BtnCustom1Caption = #33258#23450#20041'1'
                    BtnCustom2Caption = #33258#23450#20041'2'
                    BtnCustom3Caption = #33258#23450#20041'3'
                    BtnCustom4Caption = #33258#23450#20041'4'
                    BtnCustom5Caption = #33258#23450#20041'5'
                    BtnCustom6Caption = #33258#23450#20041'6'
                    BtnCustom7Caption = #33258#23450#20041'7'
                    BtnCustom8Caption = #33258#23450#20041'8'
                    BtnCustom9Caption = #33258#23450#20041'9'
                    BtnCustom10Caption = #33258#23450#20041'10'
                    BtnCustom1Kind = cxbkStandard
                    BtnCustom2Kind = cxbkStandard
                    BtnCustom3Kind = cxbkStandard
                    BtnCustom4Kind = cxbkStandard
                    BtnCustom5Kind = cxbkStandard
                    BtnCustom6Kind = cxbkStandard
                    BtnCustom7Kind = cxbkStandard
                    BtnCustom8Kind = cxbkStandard
                    BtnCustom9Kind = cxbkStandard
                    BtnCustom10Kind = cxbkStandard
                    ApprovedRollbackButton = nbCustom1
                    ApprovedCommitButton = nbCustom1
                    ButtonControlType = ctSingle
                  end
                  object Detail_Grid4: TABcxGrid
                    Left = 0
                    Top = 284
                    Width = 343
                    Height = 141
                    Align = alClient
                    TabOrder = 1
                    ExplicitHeight = 106
                    object Detail_TableView4: TABcxGridDBBandedTableView
                      PopupMenu.AutoHotkeys = maManual
                      PopupMenu.CloseFootStr = False
                      PopupMenu.LinkTableView = Detail_TableView4
                      PopupMenu.AutoApplyBestFit = True
                      PopupMenu.AutoCreateAllItem = True
                      Navigator.Buttons.CustomButtons = <>
                      DataController.DataSource = ABDatasource1_2_2
                      DataController.Filter.Options = [fcoCaseInsensitive]
                      DataController.Filter.AutoDataSetFilter = True
                      DataController.Filter.TranslateBetween = True
                      DataController.Filter.TranslateIn = True
                      DataController.Filter.TranslateLike = True
                      DataController.Summary.DefaultGroupSummaryItems = <>
                      DataController.Summary.FooterSummaryItems = <>
                      DataController.Summary.SummaryGroups = <>
                      OptionsBehavior.AlwaysShowEditor = True
                      OptionsBehavior.FocusCellOnTab = True
                      OptionsBehavior.GoToNextCellOnEnter = True
                      OptionsCustomize.ColumnsQuickCustomization = True
                      OptionsCustomize.DataRowSizing = True
                      OptionsData.Deleting = False
                      OptionsData.Inserting = False
                      OptionsSelection.HideFocusRectOnExit = False
                      OptionsSelection.MultiSelect = True
                      OptionsView.CellAutoHeight = True
                      OptionsView.GroupByBox = False
                      OptionsView.BandHeaders = False
                      Bands = <
                        item
                        end>
                      ExtPopupMenu.AutoHotkeys = maManual
                      ExtPopupMenu.CloseFootStr = False
                      ExtPopupMenu.LinkTableView = Detail_TableView4
                      ExtPopupMenu.AutoApplyBestFit = True
                      ExtPopupMenu.AutoCreateAllItem = True
                    end
                    object Detail_Level4: TcxGridLevel
                      GridView = Detail_TableView4
                    end
                  end
                  object Panel9: TPanel
                    Left = 0
                    Top = 239
                    Width = 343
                    Height = 19
                    Align = alTop
                    BevelOuter = bvNone
                    BiDiMode = bdLeftToRight
                    Caption = #24403#21069#29256#26412#30340#22270#32440#29031#29255#20449#24687
                    Color = clMedGray
                    ParentBiDiMode = False
                    TabOrder = 2
                  end
                  object ABcxSplitter2: TABcxSplitter
                    Left = 0
                    Top = 231
                    Width = 343
                    Height = 8
                    HotZoneClassName = 'TcxMediaPlayer8Style'
                    AlignSplitter = salTop
                    InvertDirection = True
                    Control = Panel7
                  end
                  object Panel7: TPanel
                    Left = 0
                    Top = 0
                    Width = 343
                    Height = 231
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 4
                    object Panel11: TPanel
                      Left = 0
                      Top = 0
                      Width = 343
                      Height = 19
                      Align = alTop
                      BevelOuter = bvNone
                      BiDiMode = bdLeftToRight
                      Caption = #24403#21069#29256#26412#30340'BOM'#21015#34920
                      Color = clMedGray
                      ParentBiDiMode = False
                      TabOrder = 0
                    end
                    object Detail_Navigator3: TABDBNavigator
                      Left = 0
                      Top = 19
                      Width = 343
                      Height = 26
                      Align = alTop
                      BevelOuter = bvNone
                      ShowCaption = False
                      TabOrder = 1
                      BigGlyph = False
                      ImageLayout = blGlyphLeft
                      DataSource = ABDatasource1_2_1
                      VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
                      ButtonRangeType = RtDetail
                      BtnCustom1ImageIndex = -1
                      BtnCustom2ImageIndex = -1
                      BtnCustom3ImageIndex = -1
                      BtnCustom4ImageIndex = -1
                      BtnCustom5ImageIndex = -1
                      BtnCustom6ImageIndex = -1
                      BtnCustom7ImageIndex = -1
                      BtnCustom8ImageIndex = -1
                      BtnCustom9ImageIndex = -1
                      BtnCustom10ImageIndex = -1
                      BtnCustom1Caption = #33258#23450#20041'1'
                      BtnCustom2Caption = #33258#23450#20041'2'
                      BtnCustom3Caption = #33258#23450#20041'3'
                      BtnCustom4Caption = #33258#23450#20041'4'
                      BtnCustom5Caption = #33258#23450#20041'5'
                      BtnCustom6Caption = #33258#23450#20041'6'
                      BtnCustom7Caption = #33258#23450#20041'7'
                      BtnCustom8Caption = #33258#23450#20041'8'
                      BtnCustom9Caption = #33258#23450#20041'9'
                      BtnCustom10Caption = #33258#23450#20041'10'
                      BtnCustom1Kind = cxbkStandard
                      BtnCustom2Kind = cxbkStandard
                      BtnCustom3Kind = cxbkStandard
                      BtnCustom4Kind = cxbkStandard
                      BtnCustom5Kind = cxbkStandard
                      BtnCustom6Kind = cxbkStandard
                      BtnCustom7Kind = cxbkStandard
                      BtnCustom8Kind = cxbkStandard
                      BtnCustom9Kind = cxbkStandard
                      BtnCustom10Kind = cxbkStandard
                      ApprovedRollbackButton = nbCustom1
                      ApprovedCommitButton = nbCustom1
                      ButtonControlType = ctSingle
                    end
                    object Detail_Grid3: TABcxGrid
                      Left = 0
                      Top = 45
                      Width = 343
                      Height = 186
                      Align = alClient
                      TabOrder = 2
                      object Detail_TableView3: TABcxGridDBBandedTableView
                        PopupMenu.AutoHotkeys = maManual
                        PopupMenu.CloseFootStr = False
                        PopupMenu.LinkTableView = Detail_TableView3
                        PopupMenu.AutoApplyBestFit = True
                        PopupMenu.AutoCreateAllItem = True
                        Navigator.Buttons.CustomButtons = <>
                        DataController.DataSource = ABDatasource1_2_1
                        DataController.Filter.Options = [fcoCaseInsensitive]
                        DataController.Filter.AutoDataSetFilter = True
                        DataController.Filter.TranslateBetween = True
                        DataController.Filter.TranslateIn = True
                        DataController.Filter.TranslateLike = True
                        DataController.Summary.DefaultGroupSummaryItems = <>
                        DataController.Summary.FooterSummaryItems = <>
                        DataController.Summary.SummaryGroups = <>
                        OptionsBehavior.AlwaysShowEditor = True
                        OptionsBehavior.FocusCellOnTab = True
                        OptionsBehavior.GoToNextCellOnEnter = True
                        OptionsCustomize.ColumnsQuickCustomization = True
                        OptionsCustomize.DataRowSizing = True
                        OptionsData.Deleting = False
                        OptionsData.Inserting = False
                        OptionsSelection.HideFocusRectOnExit = False
                        OptionsSelection.MultiSelect = True
                        OptionsView.CellAutoHeight = True
                        OptionsView.GroupByBox = False
                        OptionsView.BandHeaders = False
                        Bands = <
                          item
                          end>
                        ExtPopupMenu.AutoHotkeys = maManual
                        ExtPopupMenu.CloseFootStr = False
                        ExtPopupMenu.LinkTableView = Detail_TableView3
                        ExtPopupMenu.AutoApplyBestFit = True
                        ExtPopupMenu.AutoCreateAllItem = True
                      end
                      object Detail_Level3: TcxGridLevel
                        GridView = Detail_TableView3
                      end
                    end
                  end
                end
              end
              object Detail_Navigator2: TABDBNavigator
                Left = 0
                Top = 0
                Width = 509
                Height = 26
                Align = alTop
                BevelOuter = bvNone
                ShowCaption = False
                TabOrder = 1
                BigGlyph = False
                ImageLayout = blGlyphLeft
                DataSource = ABDatasource1_2
                VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
                ButtonRangeType = RtDetail
                BtnCustom1ImageIndex = -1
                BtnCustom2ImageIndex = -1
                BtnCustom3ImageIndex = -1
                BtnCustom4ImageIndex = -1
                BtnCustom5ImageIndex = -1
                BtnCustom6ImageIndex = -1
                BtnCustom7ImageIndex = -1
                BtnCustom8ImageIndex = -1
                BtnCustom9ImageIndex = -1
                BtnCustom10ImageIndex = -1
                BtnCustom1Caption = #33258#23450#20041'1'
                BtnCustom2Caption = #33258#23450#20041'2'
                BtnCustom3Caption = #33258#23450#20041'3'
                BtnCustom4Caption = #33258#23450#20041'4'
                BtnCustom5Caption = #33258#23450#20041'5'
                BtnCustom6Caption = #33258#23450#20041'6'
                BtnCustom7Caption = #33258#23450#20041'7'
                BtnCustom8Caption = #33258#23450#20041'8'
                BtnCustom9Caption = #33258#23450#20041'9'
                BtnCustom10Caption = #33258#23450#20041'10'
                BtnCustom1Kind = cxbkStandard
                BtnCustom2Kind = cxbkStandard
                BtnCustom3Kind = cxbkStandard
                BtnCustom4Kind = cxbkStandard
                BtnCustom5Kind = cxbkStandard
                BtnCustom6Kind = cxbkStandard
                BtnCustom7Kind = cxbkStandard
                BtnCustom8Kind = cxbkStandard
                BtnCustom9Kind = cxbkStandard
                BtnCustom10Kind = cxbkStandard
                ApprovedRollbackButton = nbCustom1
                ApprovedCommitButton = nbCustom1
                ButtonControlType = ctSingle
              end
            end
          end
        end
      end
    end
    object ABDBNavigator1: TABDBNavigator
      Left = 1
      Top = 1
      Width = 519
      Height = 26
      Align = alTop
      BevelOuter = bvNone
      ShowCaption = False
      TabOrder = 1
      BigGlyph = False
      ImageLayout = blGlyphLeft
      DataSource = ABDatasource1
      VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel, nbQuerySpacer, nbQuery, nbReport, nbExitSpacer, nbExit]
      ButtonRangeType = RtMain
      BtnCustom1ImageIndex = -1
      BtnCustom2ImageIndex = -1
      BtnCustom3ImageIndex = -1
      BtnCustom4ImageIndex = -1
      BtnCustom5ImageIndex = -1
      BtnCustom6ImageIndex = -1
      BtnCustom7ImageIndex = -1
      BtnCustom8ImageIndex = -1
      BtnCustom9ImageIndex = -1
      BtnCustom10ImageIndex = -1
      BtnCustom1Caption = #33258#23450#20041'1'
      BtnCustom2Caption = #33258#23450#20041'2'
      BtnCustom3Caption = #33258#23450#20041'3'
      BtnCustom4Caption = #33258#23450#20041'4'
      BtnCustom5Caption = #33258#23450#20041'5'
      BtnCustom6Caption = #33258#23450#20041'6'
      BtnCustom7Caption = #33258#23450#20041'7'
      BtnCustom8Caption = #33258#23450#20041'8'
      BtnCustom9Caption = #33258#23450#20041'9'
      BtnCustom10Caption = #33258#23450#20041'10'
      BtnCustom1Kind = cxbkStandard
      BtnCustom2Kind = cxbkStandard
      BtnCustom3Kind = cxbkStandard
      BtnCustom4Kind = cxbkStandard
      BtnCustom5Kind = cxbkStandard
      BtnCustom6Kind = cxbkStandard
      BtnCustom7Kind = cxbkStandard
      BtnCustom8Kind = cxbkStandard
      BtnCustom9Kind = cxbkStandard
      BtnCustom10Kind = cxbkStandard
      ApprovedRollbackButton = nbCustom1
      ApprovedCommitButton = nbCustom1
      ButtonControlType = ctSingle
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 221
    Height = 531
    Align = alLeft
    Caption = 'Panel1'
    TabOrder = 3
    object ABDBNavigator2: TABDBNavigator
      Left = 1
      Top = 1
      Width = 219
      Height = 26
      Align = alTop
      BevelOuter = bvNone
      ShowCaption = False
      TabOrder = 0
      BigGlyph = False
      ImageLayout = blGlyphLeft
      DataSource = ABDatasource_0
      VisibleButtons = [nbQuerySpacer, nbQuery, nbReport]
      ButtonRangeType = RtCustom
      BtnCustom1ImageIndex = -1
      BtnCustom2ImageIndex = -1
      BtnCustom3ImageIndex = -1
      BtnCustom4ImageIndex = -1
      BtnCustom5ImageIndex = -1
      BtnCustom6ImageIndex = -1
      BtnCustom7ImageIndex = -1
      BtnCustom8ImageIndex = -1
      BtnCustom9ImageIndex = -1
      BtnCustom10ImageIndex = -1
      BtnCustom1Caption = #33258#23450#20041'1'
      BtnCustom2Caption = #33258#23450#20041'2'
      BtnCustom3Caption = #33258#23450#20041'3'
      BtnCustom4Caption = #33258#23450#20041'4'
      BtnCustom5Caption = #33258#23450#20041'5'
      BtnCustom6Caption = #33258#23450#20041'6'
      BtnCustom7Caption = #33258#23450#20041'7'
      BtnCustom8Caption = #33258#23450#20041'8'
      BtnCustom9Caption = #33258#23450#20041'9'
      BtnCustom10Caption = #33258#23450#20041'10'
      BtnCustom1Kind = cxbkStandard
      BtnCustom2Kind = cxbkStandard
      BtnCustom3Kind = cxbkStandard
      BtnCustom4Kind = cxbkStandard
      BtnCustom5Kind = cxbkStandard
      BtnCustom6Kind = cxbkStandard
      BtnCustom7Kind = cxbkStandard
      BtnCustom8Kind = cxbkStandard
      BtnCustom9Kind = cxbkStandard
      BtnCustom10Kind = cxbkStandard
      ApprovedRollbackButton = nbCustom1
      ApprovedCommitButton = nbCustom1
      ButtonControlType = ctSingle
      DesignSize = (
        219
        26)
      object ABDBLabels1: TABDBLabels
        Left = 317
        Top = 7
        Width = 474
        Height = 17
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        BiDiMode = bdRightToLeft
        ParentBiDiMode = False
        Transparent = True
        DataSource = ABDatasource1
        ExplicitWidth = 438
      end
    end
    object ABcxDBTreeView1: TABcxDBTreeView
      Left = 1
      Top = 27
      Width = 219
      Height = 503
      Align = alClient
      Bands = <
        item
          Options.OnlyOwnColumns = True
        end>
      DataController.DataSource = ABDatasource_0
      DataController.ParentField = 'MS_ParentGuid'
      DataController.KeyField = 'MS_Guid'
      DragMode = dmAutomatic
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ExpandOnIncSearch = True
      OptionsBehavior.IncSearch = True
      OptionsBehavior.IncSearchItem = ABcxDBTreeView1cxDBTreeListColumn1
      OptionsData.Deleting = False
      OptionsSelection.CellSelect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.Headers = False
      PopupMenu.DefaultParentValue = '0'
      RootValue = -1
      TabOrder = 1
      Active = False
      ExtFullExpand = False
      CanSelectParent = True
      object ABcxDBTreeView1cxDBTreeListColumn1: TcxDBTreeListColumn
        DataBinding.FieldName = 'MS_Name'
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object ABcxDBTreeView1cxDBTreeListColumn2: TcxDBTreeListColumn
        Visible = False
        DataBinding.FieldName = 'MS_Order'
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        SortOrder = soAscending
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
  end
  object ABDatasource1_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_1
    Left = 168
    Top = 246
  end
  object ABDatasource1_2: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_2
    Left = 168
    Top = 300
  end
  object ABDatasource1_2_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_2_1
    Left = 168
    Top = 354
  end
  object ABDatasource1_2_2: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_2_2
    Left = 168
    Top = 408
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 168
    Top = 190
  end
  object ABQuery_0: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select * '
      'from ERP_Base_MaterielSort')
    SqlUpdateDatetime = 42219.673458113420000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_MaterielSort')
    IndexListDefs = <
      item
        Name = 'IX_ABSys_Org_TreeItem'
        Fields = 'TI_Name;TI_TR_Guid'
        Options = [ixUnique]
      end
      item
        Name = 'IX_ABSys_Org_TreeItem_1'
        Fields = 'TI_Order;TI_ParentGuid;TI_TR_Guid'
      end
      item
        Name = 'IX_ABSys_Org_TreeItem_2'
        Fields = 'TI_Code;TI_TR_Guid'
        Options = [ixUnique]
      end
      item
        Name = 'PK_ABSys_Org_TreeItem'
        Fields = 'TI_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Base_MaterielSort')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 48
    Top = 47
  end
  object ABDatasource_0: TABDatasource
    AutoEdit = False
    DataSet = ABQuery_0
    Left = 168
    Top = 48
  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    AfterScroll = ABQuery1AfterScroll
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource_0
    MasterFields = 'MS_Guid'
    DetailFields = 'Ma_MS_Guid'
    SQL.Strings = (
      'select * from ERP_Base_Materiel'
      'where Ma_MS_Guid=:MS_Guid')
    SqlUpdateDatetime = 42219.675681967600000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_Materiel')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ERP_Base_Materiel')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    OnFieldChange = ABQuery1FieldChange
    AddTableRowQueryRight = True
    Left = 72
    Top = 191
    ParamData = <
      item
        Name = 'MS_GUID'
        ParamType = ptInput
      end>
  end
  object ABQuery1_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    MasterFields = 'Ma_Guid'
    DetailFields = 'MU_Ma_Guid'
    SQL.Strings = (
      'select * from ERP_Base_MaterielUnit'
      'where MU_Ma_Guid=:Ma_Guid')
    SqlUpdateDatetime = 42214.274519386580000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_MaterielUnit')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ERP_Base_MaterielUnit')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 72
    Top = 247
    ParamData = <
      item
        Name = 'MA_GUID'
        ParamType = ptInput
      end>
  end
  object ABQuery1_2: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    MasterFields = 'MA_Guid'
    DetailFields = 'MV_MA_Guid'
    SQL.Strings = (
      'select * from ERP_Base_MaterielVersion'
      'where MV_MA_Guid=:MA_Guid')
    SqlUpdateDatetime = 42214.277048865740000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_MaterielVersion')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ERP_Base_MaterielVersion')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 72
    Top = 303
    ParamData = <
      item
        Name = 'MA_GUID'
        ParamType = ptInput
      end>
  end
  object ABQuery1_2_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1_2
    MasterFields = 'MV_Guid'
    DetailFields = 'MB_MV_Guid'
    SQL.Strings = (
      'select * from ERP_Base_MaterielVersionBom'
      'where MB_MV_Guid=:MV_Guid')
    SqlUpdateDatetime = 42214.277675127320000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_MaterielVersionBom')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ERP_Base_MaterielVersionBom')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 72
    Top = 359
    ParamData = <
      item
        Name = 'MV_GUID'
        ParamType = ptInput
      end>
  end
  object ABQuery1_2_2: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1_2
    MasterFields = 'MV_Guid'
    DetailFields = 'MD_MV_Guid'
    SQL.Strings = (
      'select  '
      'GetNoBigDateFieldNames('#39'ERP_Base_MaterielVersionDocument'#39')'
      'from ERP_Base_MaterielVersionDocument'
      'where MD_MV_Guid=:MV_Guid')
    SqlUpdateDatetime = 42233.316142905090000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_MaterielVersionDocument')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ERP_Base_MaterielVersionDocument')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 72
    Top = 415
    ParamData = <
      item
        Name = 'MV_GUID'
        ParamType = ptInput
      end>
  end
end
