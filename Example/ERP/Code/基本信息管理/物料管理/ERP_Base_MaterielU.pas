unit ERP_Base_MaterielU;

interface

uses
  ABThirdConnU,
  ABPubFuncU,
  ABPubDBU,
  ABFramkWorkFuncFormU,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, dxStatusBar,
  ABFramkWorkControlU, ABFramkWorkDBPanelU, Vcl.ExtCtrls, ABPubPanelU,
  ABFramkWorkDBNavigatorU, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, ABFramkWorkcxGridU, cxGrid, ABFramkWorkQueryU,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, ABThirdDBU, ABThirdCustomQueryU,
  ABThirdQueryU, ABFramkWorkDictionaryQueryU, dxBarBuiltInMenu, cxPC, cxSplitter,
  cxContainer, cxImage, cxDBEdit, cxTL, cxMaskEdit, cxTLdxBarBuiltInMenu,
  Vcl.StdCtrls, ABPubDBLabelsU, cxInplaceContainer, cxDBTL, cxTLData;

type
  TERP_Base_MaterielForm = class(TABFuncForm)
    ABDatasource1: TABDatasource;
    ABcxPageControl1: TABcxPageControl;
    cxTabSheet1: TcxTabSheet;
    ABDBStatusBar1: TABdxDBStatusBar;
    cxTabSheet2: TcxTabSheet;
    Panel2: TPanel;
    ABcxPageControl2: TABcxPageControl;
    ABcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    Main_Sheet1: TcxTabSheet;
    Main_Sheet2: TcxTabSheet;
    Detail_Sheet1: TcxTabSheet;
    Detail_Navigator1: TABDBNavigator;
    ABDatasource1_1: TABDatasource;
    Detail_Grid1: TABcxGrid;
    Detail_TableView1: TABcxGridDBBandedTableView;
    Detail_Level1: TcxGridLevel;
    ABDatasource1_2: TABDatasource;
    ABDatasource1_2_1: TABDatasource;
    ABDatasource1_2_2: TABDatasource;
    Panel4: TPanel;
    Detail_Grid2: TABcxGrid;
    Detail_TableView2: TABcxGridDBBandedTableView;
    Detail_Level2: TcxGridLevel;
    Panel5: TPanel;
    Detail_Navigator2: TABDBNavigator;
    Splitter1: TABcxSplitter;
    Panel1: TPanel;
    ABDBNavigator2: TABDBNavigator;
    ABDBLabels1: TABDBLabels;
    ABDBNavigator1: TABDBNavigator;
    ABQuery_0: TABQuery;
    ABDatasource_0: TABDatasource;
    ABQuery1: TABQuery;
    ABQuery1_1: TABQuery;
    ABQuery1_2: TABQuery;
    ABQuery1_2_1: TABQuery;
    ABQuery1_2_2: TABQuery;
    ABcxPageControl3: TABcxPageControl;
    cxTabSheet3: TcxTabSheet;
    cxTabSheet4: TcxTabSheet;
    cxTabSheet5: TcxTabSheet;
    Main_Panel1: TABDBPanel;
    Main_Panel2: TABDBPanel;
    Main_Panel3: TABDBPanel;
    ABcxDBTreeView1: TABcxDBTreeView;
    ABcxDBTreeView1cxDBTreeListColumn1: TcxDBTreeListColumn;
    ABcxDBTreeView1cxDBTreeListColumn2: TcxDBTreeListColumn;
    ABcxSplitter1: TABcxSplitter;
    Panel3: TPanel;
    Detail_Navigator4: TABDBNavigator;
    Detail_Grid4: TABcxGrid;
    Detail_TableView4: TABcxGridDBBandedTableView;
    Detail_Level4: TcxGridLevel;
    Panel9: TPanel;
    ABcxSplitter2: TABcxSplitter;
    Panel7: TPanel;
    Panel11: TPanel;
    Detail_Navigator3: TABDBNavigator;
    Detail_Grid3: TABcxGrid;
    Detail_TableView3: TABcxGridDBBandedTableView;
    Detail_Level3: TcxGridLevel;
    procedure FormCreate(Sender: TObject);
    procedure ABQuery1AfterScroll(DataSet: TDataSet);
    procedure ABQuery1FieldChange(Sender: TField);
  private
    procedure CheckMa_From(aField: TField);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ERP_Base_MaterielForm: TERP_Base_MaterielForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :='TERP_Base_MaterielForm';
end;

exports
   ABRegister ;

procedure TERP_Base_MaterielForm.ABQuery1AfterScroll(
  DataSet: TDataSet);
begin
  CheckMa_From(ABQuery1.fieldbyname('Ma_From'));
end;

procedure TERP_Base_MaterielForm.ABQuery1FieldChange(Sender: TField);
begin
  CheckMa_From(Sender);
end;

procedure TERP_Base_MaterielForm.CheckMa_From(aField: TField);
begin
  if (AnsiCompareText(aField.FieldName,'Ma_From')=0) then
  begin
    if (AnsiCompareText('Make',aField.AsString)=0) or
       (AnsiCompareText('MakeAndBuy',aField.AsString)=0) then
    begin
      Main_Sheet2.TabVisible:=True;
    end
    else
    begin
      if (ABcxPageControl2.ActivePageIndex=2) then
        ABcxPageControl2.ActivePageIndex:=0;
      Main_Sheet2.TabVisible:=False;
    end;
  end;
end;

procedure TERP_Base_MaterielForm.FormCreate(Sender: TObject);
begin
  ABInitFormDataSet([],[],ABQuery_0);
  ABInitFormDataSet([Main_Panel1,Main_Panel2,Main_Panel3],[ABcxGridDBBandedTableView1],ABQuery1);
  ABInitFormDataSet([],[Detail_TableView1],ABQuery1_1);
  ABInitFormDataSet([],[Detail_TableView2],ABQuery1_2);
  ABInitFormDataSet([],[Detail_TableView3],ABQuery1_2_1);
  ABInitFormDataSet([],[Detail_TableView4],ABQuery1_2_2);
end;

initialization
  RegisterClass(TERP_Base_MaterielForm);

finalization
  UnRegisterClass(TERP_Base_MaterielForm);

end.
