unit ERP_Base_CustU;

interface

uses
  ABFramkWorkFuncFormU,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, dxStatusBar,
  ABFramkWorkControlU, ABFramkWorkDBPanelU, Vcl.ExtCtrls, ABPubPanelU,
  ABFramkWorkDBNavigatorU, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, ABFramkWorkcxGridU, cxGrid, ABFramkWorkQueryU,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, ABThirdDBU, ABThirdCustomQueryU,
  ABThirdQueryU, ABFramkWorkDictionaryQueryU, dxBarBuiltInMenu, cxPC, cxSplitter,
  cxContainer, cxImage, cxDBEdit;

type
  TERP_Base_CustForm = class(TABFuncForm)
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABDBStatusBar1: TABdxDBStatusBar;
    Panel1: TPanel;
    Panel3: TPanel;
    ABDBNavigator1: TABDBNavigator;
    ABQuery1_1: TABQuery;
    ABDatasource1_1: TABDatasource;
    ABcxPageControl1: TABcxPageControl;
    cxTabSheet1: TcxTabSheet;
    ABcxPageControl2: TABcxPageControl;
    cxTabSheet_11: TcxTabSheet;
    cxTabSheet_12: TcxTabSheet;
    cxTabSheet_13: TcxTabSheet;
    cxTabSheet_14: TcxTabSheet;
    ABcxPageControl3: TABcxPageControl;
    cxTabSheet1_1: TcxTabSheet;
    cxTabSheet1_2: TcxTabSheet;
    cxTabSheet1_3: TcxTabSheet;
    cxTabSheet1_4: TcxTabSheet;
    ABDBPanel1: TABDBPanel;
    ABcxGrid2: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    ABcxGrid1: TABcxGrid;
    ABcxGrid1ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    ABcxGrid1Level1: TcxGridLevel;
    ABcxSplitter1: TABcxSplitter;
    ABcxSplitter2: TABcxSplitter;
    ABDBNavigator2: TABDBNavigator;
    ABDBNavigator3: TABDBNavigator;
    ABDBNavigator4: TABDBNavigator;
    ABDBNavigator5: TABDBNavigator;
    ABDBPanel2: TABDBPanel;
    ABDBPanel3: TABDBPanel;
    ABDBPanel4: TABDBPanel;
    ABcxGrid3: TABcxGrid;
    ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView;
    cxGridLevel2: TcxGridLevel;
    ABcxGrid4: TABcxGrid;
    ABcxGridDBBandedTableView3: TABcxGridDBBandedTableView;
    cxGridLevel3: TcxGridLevel;
    ABcxGrid5: TABcxGrid;
    ABcxGridDBBandedTableView4: TABcxGridDBBandedTableView;
    cxGridLevel4: TcxGridLevel;
    ABDatasource1_2: TABDatasource;
    ABQuery1_2: TABQuery;
    ABDatasource1_3: TABDatasource;
    ABQuery1_3: TABQuery;
    ABDatasource1_4: TABDatasource;
    ABQuery1_4: TABQuery;
    ABQuery1_1_1: TABQuery;
    ABDatasource1_1_1: TABDatasource;
    ABcxDBImage1: TABcxDBImage;
    ABcxSplitter3: TABcxSplitter;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ERP_Base_CustForm: TERP_Base_CustForm;

implementation
{$R *.dfm}
procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :='TERP_Base_CustForm';
end;

exports
   ABRegister ;

procedure TERP_Base_CustForm.FormCreate(Sender: TObject);
begin
  ABInitFormDataSet([ABDBPanel1,ABDBPanel2,ABDBPanel3,ABDBPanel4],[ABcxGrid1ABcxGridDBBandedTableView1],ABQuery1);
  ABInitFormDataSet([],[ABcxGridDBBandedTableView1],ABQuery1_1);
  ABInitFormDataSet([],[],ABQuery1_1_1);
  ABInitFormDataSet([],[ABcxGridDBBandedTableView2],ABQuery1_2);
  ABInitFormDataSet([],[ABcxGridDBBandedTableView3],ABQuery1_3);
  ABInitFormDataSet([],[ABcxGridDBBandedTableView4],ABQuery1_4);
end;

initialization
  RegisterClass(TERP_Base_CustForm);
finalization
  UnRegisterClass(TERP_Base_CustForm);
end.
