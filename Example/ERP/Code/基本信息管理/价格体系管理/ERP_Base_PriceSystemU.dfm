object ERP_Base_PriceSystemForm: TERP_Base_PriceSystemForm
  Left = 0
  Top = 0
  Caption = #20215#26684#20307#31995#31649#29702
  ClientHeight = 550
  ClientWidth = 761
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ABDBNavigator3: TABDBNavigator
    Left = 0
    Top = 0
    Width = 761
    Height = 25
    Align = alTop
    BevelOuter = bvNone
    Caption = 'ABDBNavigator1'
    ShowCaption = False
    TabOrder = 0
    BigGlyph = False
    ImageLayout = blGlyphLeft
    DataSource = ABDatasource1
    VisibleButtons = [nbReport, nbExitSpacer, nbExit]
    ButtonRangeType = RtReportAndExit
    BtnCustom1ImageIndex = -1
    BtnCustom2ImageIndex = -1
    BtnCustom3ImageIndex = -1
    BtnCustom4ImageIndex = -1
    BtnCustom5ImageIndex = -1
    BtnCustom6ImageIndex = -1
    BtnCustom7ImageIndex = -1
    BtnCustom8ImageIndex = -1
    BtnCustom9ImageIndex = -1
    BtnCustom10ImageIndex = -1
    BtnCustom1Caption = #33258#23450#20041'1'
    BtnCustom2Caption = #33258#23450#20041'2'
    BtnCustom3Caption = #33258#23450#20041'3'
    BtnCustom4Caption = #33258#23450#20041'4'
    BtnCustom5Caption = #33258#23450#20041'5'
    BtnCustom6Caption = #33258#23450#20041'6'
    BtnCustom7Caption = #33258#23450#20041'7'
    BtnCustom8Caption = #33258#23450#20041'8'
    BtnCustom9Caption = #33258#23450#20041'9'
    BtnCustom10Caption = #33258#23450#20041'10'
    BtnCustom1Kind = cxbkStandard
    BtnCustom2Kind = cxbkStandard
    BtnCustom3Kind = cxbkStandard
    BtnCustom4Kind = cxbkStandard
    BtnCustom5Kind = cxbkStandard
    BtnCustom6Kind = cxbkStandard
    BtnCustom7Kind = cxbkStandard
    BtnCustom8Kind = cxbkStandard
    BtnCustom9Kind = cxbkStandard
    BtnCustom10Kind = cxbkStandard
    ApprovedRollbackButton = nbCustom1
    ApprovedCommitButton = nbCustom1
    ButtonControlType = ctSingle
  end
  object ABcxPageControl1: TABcxPageControl
    Left = 0
    Top = 25
    Width = 761
    Height = 506
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = cxTabSheet1
    Properties.CustomButtons.Buttons = <>
    LookAndFeel.Kind = lfFlat
    OnChange = ABcxPageControl1Change
    ActivePageIndex = 0
    ClientRectBottom = 505
    ClientRectLeft = 1
    ClientRectRight = 760
    ClientRectTop = 21
    object cxTabSheet1: TcxTabSheet
      Caption = #24050#24405#20837
      ImageIndex = 0
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 25
        Width = 759
        Height = 459
        Align = alClient
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel3: TPanel
          Left = 211
          Top = 1
          Width = 547
          Height = 457
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object ABcxPageControl2: TABcxPageControl
            Left = 0
            Top = 0
            Width = 547
            Height = 185
            Align = alTop
            TabOrder = 0
            Properties.ActivePage = cxTabSheet_11
            Properties.CustomButtons.Buttons = <>
            Properties.HideTabs = True
            LookAndFeel.Kind = lfFlat
            ActivePageIndex = 0
            ClientRectBottom = 184
            ClientRectLeft = 1
            ClientRectRight = 546
            ClientRectTop = 1
            object cxTabSheet_11: TcxTabSheet
              Caption = #22522#26412#20449#24687
              ImageIndex = 0
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object ABDBPanel1: TABDBPanel
                Left = 0
                Top = 0
                Width = 545
                Height = 183
                Align = alClient
                BevelOuter = bvNone
                Caption = 'ABDBPanel1'
                ShowCaption = False
                TabOrder = 0
                ReadOnly = False
                DataSource = ABDatasource1
                AddAnchors_akRight = True
                AddAnchors_akBottom = True
                AutoHeight = True
                AutoWidth = True
              end
            end
          end
          object ABcxPageControl3: TABcxPageControl
            Left = 0
            Top = 193
            Width = 547
            Height = 264
            Align = alClient
            TabOrder = 1
            Properties.ActivePage = cxTabSheet1_1
            Properties.CustomButtons.Buttons = <>
            Properties.HideTabs = True
            LookAndFeel.Kind = lfFlat
            ActivePageIndex = 0
            ClientRectBottom = 263
            ClientRectLeft = 1
            ClientRectRight = 546
            ClientRectTop = 1
            object cxTabSheet1_1: TcxTabSheet
              Caption = #32852#31995#20154#20449#24687
              ImageIndex = 0
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object ABcxGrid2: TABcxGrid
                Left = 0
                Top = 25
                Width = 545
                Height = 237
                Align = alClient
                TabOrder = 0
                object ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
                  PopupMenu.AutoHotkeys = maManual
                  PopupMenu.CloseFootStr = False
                  PopupMenu.LinkTableView = ABcxGridDBBandedTableView1
                  PopupMenu.AutoApplyBestFit = True
                  PopupMenu.AutoCreateAllItem = True
                  Navigator.Buttons.CustomButtons = <>
                  DataController.DataSource = ABDatasource1_1
                  DataController.Filter.Options = [fcoCaseInsensitive]
                  DataController.Filter.AutoDataSetFilter = True
                  DataController.Filter.TranslateBetween = True
                  DataController.Filter.TranslateIn = True
                  DataController.Filter.TranslateLike = True
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsBehavior.AlwaysShowEditor = True
                  OptionsBehavior.FocusCellOnTab = True
                  OptionsBehavior.GoToNextCellOnEnter = True
                  OptionsCustomize.ColumnsQuickCustomization = True
                  OptionsCustomize.DataRowSizing = True
                  OptionsSelection.MultiSelect = True
                  OptionsView.GroupByBox = False
                  OptionsView.Indicator = True
                  Bands = <
                    item
                    end>
                  ExtPopupMenu.AutoHotkeys = maManual
                  ExtPopupMenu.CloseFootStr = False
                  ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView1
                  ExtPopupMenu.AutoApplyBestFit = True
                  ExtPopupMenu.AutoCreateAllItem = True
                end
                object cxGridLevel1: TcxGridLevel
                  GridView = ABcxGridDBBandedTableView1
                end
              end
              object ABDBNavigator2: TABDBNavigator
                Left = 0
                Top = 0
                Width = 545
                Height = 25
                Align = alTop
                BevelOuter = bvNone
                BiDiMode = bdLeftToRight
                Caption = 'ABDBNavigator1'
                ParentBiDiMode = False
                ShowCaption = False
                TabOrder = 1
                BigGlyph = False
                ImageLayout = blGlyphLeft
                DataSource = ABDatasource1_1
                VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
                ButtonRangeType = RtCustom
                BtnCustom1ImageIndex = 1
                BtnCustom2ImageIndex = -1
                BtnCustom3ImageIndex = -1
                BtnCustom4ImageIndex = -1
                BtnCustom5ImageIndex = -1
                BtnCustom6ImageIndex = -1
                BtnCustom7ImageIndex = -1
                BtnCustom8ImageIndex = -1
                BtnCustom9ImageIndex = -1
                BtnCustom10ImageIndex = -1
                BtnCustom1Caption = #33258#23450#20041'1'
                BtnCustom2Caption = #33258#23450#20041'2'
                BtnCustom3Caption = #33258#23450#20041'3'
                BtnCustom4Caption = #33258#23450#20041'4'
                BtnCustom5Caption = #33258#23450#20041'5'
                BtnCustom6Caption = #33258#23450#20041'6'
                BtnCustom7Caption = #33258#23450#20041'7'
                BtnCustom8Caption = #33258#23450#20041'8'
                BtnCustom9Caption = #33258#23450#20041'9'
                BtnCustom10Caption = #33258#23450#20041'10'
                BtnCustom1Kind = cxbkStandard
                BtnCustom2Kind = cxbkStandard
                BtnCustom3Kind = cxbkStandard
                BtnCustom4Kind = cxbkStandard
                BtnCustom5Kind = cxbkStandard
                BtnCustom6Kind = cxbkStandard
                BtnCustom7Kind = cxbkStandard
                BtnCustom8Kind = cxbkStandard
                BtnCustom9Kind = cxbkStandard
                BtnCustom10Kind = cxbkStandard
                ApprovedRollbackButton = nbCustom1
                ApprovedCommitButton = nbCustom1
                ButtonControlType = ctSingle
              end
            end
          end
          object ABcxSplitter2: TABcxSplitter
            Left = 0
            Top = 185
            Width = 547
            Height = 8
            HotZoneClassName = 'TcxMediaPlayer8Style'
            AlignSplitter = salTop
            InvertDirection = True
            Control = ABcxPageControl2
          end
        end
        object ABcxSplitter1: TABcxSplitter
          Left = 203
          Top = 1
          Width = 8
          Height = 457
          HotZoneClassName = 'TcxMediaPlayer8Style'
          AlignSplitter = salRight
          InvertDirection = True
          Control = Panel3
        end
        object Panel7: TPanel
          Left = 1
          Top = 1
          Width = 202
          Height = 457
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object ABcxGrid1: TABcxGrid
            Left = 0
            Top = 0
            Width = 202
            Height = 457
            Align = alClient
            TabOrder = 0
            object ABcxGrid1ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
              PopupMenu.AutoHotkeys = maManual
              PopupMenu.CloseFootStr = False
              PopupMenu.LinkTableView = ABcxGrid1ABcxGridDBBandedTableView1
              PopupMenu.AutoApplyBestFit = True
              PopupMenu.AutoCreateAllItem = True
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = ABDatasource1
              DataController.Filter.Options = [fcoCaseInsensitive]
              DataController.Filter.AutoDataSetFilter = True
              DataController.Filter.TranslateBetween = True
              DataController.Filter.TranslateIn = True
              DataController.Filter.TranslateLike = True
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsBehavior.AlwaysShowEditor = True
              OptionsBehavior.FocusCellOnTab = True
              OptionsBehavior.GoToNextCellOnEnter = True
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsCustomize.DataRowSizing = True
              OptionsSelection.MultiSelect = True
              OptionsView.GroupByBox = False
              OptionsView.Indicator = True
              Bands = <
                item
                end>
              ExtPopupMenu.AutoHotkeys = maManual
              ExtPopupMenu.CloseFootStr = False
              ExtPopupMenu.LinkTableView = ABcxGrid1ABcxGridDBBandedTableView1
              ExtPopupMenu.AutoApplyBestFit = True
              ExtPopupMenu.AutoCreateAllItem = True
            end
            object ABcxGrid1Level1: TcxGridLevel
              GridView = ABcxGrid1ABcxGridDBBandedTableView1
            end
          end
        end
      end
      object ABDBNavigator1: TABDBNavigator
        Left = 0
        Top = 0
        Width = 759
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        Caption = 'ABDBNavigator1'
        ShowCaption = False
        TabOrder = 1
        BigGlyph = False
        ImageLayout = blGlyphLeft
        DataSource = ABDatasource1
        VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel, nbQuerySpacer, nbQuery, nbCustom1]
        ButtonRangeType = RtCustom
        BtnCustom1ImageIndex = -1
        BtnCustom2ImageIndex = -1
        BtnCustom3ImageIndex = -1
        BtnCustom4ImageIndex = -1
        BtnCustom5ImageIndex = -1
        BtnCustom6ImageIndex = -1
        BtnCustom7ImageIndex = -1
        BtnCustom8ImageIndex = -1
        BtnCustom9ImageIndex = -1
        BtnCustom10ImageIndex = -1
        BtnCustom1Caption = #35831#27714#23457#25209
        BtnCustom2Caption = #33258#23450#20041'2'
        BtnCustom3Caption = #33258#23450#20041'3'
        BtnCustom4Caption = #33258#23450#20041'4'
        BtnCustom5Caption = #33258#23450#20041'5'
        BtnCustom6Caption = #33258#23450#20041'6'
        BtnCustom7Caption = #33258#23450#20041'7'
        BtnCustom8Caption = #33258#23450#20041'8'
        BtnCustom9Caption = #33258#23450#20041'9'
        BtnCustom10Caption = #33258#23450#20041'10'
        BtnCustom1Kind = cxbkStandard
        BtnCustom2Kind = cxbkStandard
        BtnCustom3Kind = cxbkStandard
        BtnCustom4Kind = cxbkStandard
        BtnCustom5Kind = cxbkStandard
        BtnCustom6Kind = cxbkStandard
        BtnCustom7Kind = cxbkStandard
        BtnCustom8Kind = cxbkStandard
        BtnCustom9Kind = cxbkStandard
        BtnCustom10Kind = cxbkDropDown
        ApproveBill = 'ERP_Base_PriceSystem'
        ApproveMainGuidField = 'PS_Guid'
        ApproveStateField = 'PS_State'
        ApprovedRollbackButton = nbNull
        ApprovedCommitButton = nbCustom1
        ButtonControlType = ctSingle
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = #23457#25209#20013
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 759
        Height = 484
        Align = alClient
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel4: TPanel
          Left = 211
          Top = 1
          Width = 547
          Height = 482
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object ABcxPageControl4: TABcxPageControl
            Left = 0
            Top = 0
            Width = 547
            Height = 185
            Align = alTop
            TabOrder = 0
            Properties.ActivePage = cxTabSheet4
            Properties.CustomButtons.Buttons = <>
            Properties.HideTabs = True
            LookAndFeel.Kind = lfFlat
            ActivePageIndex = 0
            ClientRectBottom = 184
            ClientRectLeft = 1
            ClientRectRight = 546
            ClientRectTop = 1
            object cxTabSheet4: TcxTabSheet
              Caption = #22522#26412#20449#24687
              ImageIndex = 0
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object ABDBPanel2: TABDBPanel
                Left = 0
                Top = 0
                Width = 545
                Height = 183
                Align = alClient
                BevelOuter = bvNone
                Caption = 'ABDBPanel1'
                ShowCaption = False
                TabOrder = 0
                ReadOnly = False
                DataSource = ABDatasource2
                AddAnchors_akRight = True
                AddAnchors_akBottom = True
                AutoHeight = True
                AutoWidth = True
              end
            end
          end
          object ABcxPageControl5: TABcxPageControl
            Left = 0
            Top = 193
            Width = 547
            Height = 289
            Align = alClient
            TabOrder = 1
            Properties.ActivePage = cxTabSheet5
            Properties.CustomButtons.Buttons = <>
            Properties.HideTabs = True
            LookAndFeel.Kind = lfFlat
            ActivePageIndex = 0
            ClientRectBottom = 288
            ClientRectLeft = 1
            ClientRectRight = 546
            ClientRectTop = 1
            object cxTabSheet5: TcxTabSheet
              Caption = #32852#31995#20154#20449#24687
              ImageIndex = 0
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object ABcxGrid3: TABcxGrid
                Left = 0
                Top = 25
                Width = 545
                Height = 262
                Align = alClient
                TabOrder = 0
                object ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView
                  PopupMenu.AutoHotkeys = maManual
                  PopupMenu.CloseFootStr = False
                  PopupMenu.LinkTableView = ABcxGridDBBandedTableView2
                  PopupMenu.AutoApplyBestFit = True
                  PopupMenu.AutoCreateAllItem = True
                  Navigator.Buttons.CustomButtons = <>
                  DataController.DataSource = ABDatasource2_1
                  DataController.Filter.Options = [fcoCaseInsensitive]
                  DataController.Filter.AutoDataSetFilter = True
                  DataController.Filter.TranslateBetween = True
                  DataController.Filter.TranslateIn = True
                  DataController.Filter.TranslateLike = True
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsBehavior.AlwaysShowEditor = True
                  OptionsBehavior.FocusCellOnTab = True
                  OptionsBehavior.GoToNextCellOnEnter = True
                  OptionsCustomize.ColumnsQuickCustomization = True
                  OptionsCustomize.DataRowSizing = True
                  OptionsSelection.MultiSelect = True
                  OptionsView.GroupByBox = False
                  OptionsView.Indicator = True
                  Bands = <
                    item
                    end>
                  ExtPopupMenu.AutoHotkeys = maManual
                  ExtPopupMenu.CloseFootStr = False
                  ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView2
                  ExtPopupMenu.AutoApplyBestFit = True
                  ExtPopupMenu.AutoCreateAllItem = True
                end
                object cxGridLevel2: TcxGridLevel
                  GridView = ABcxGridDBBandedTableView2
                end
              end
              object ABDBNavigator4: TABDBNavigator
                Left = 0
                Top = 0
                Width = 545
                Height = 25
                Align = alTop
                BevelOuter = bvNone
                BiDiMode = bdLeftToRight
                Caption = 'ABDBNavigator1'
                ParentBiDiMode = False
                ShowCaption = False
                TabOrder = 1
                BigGlyph = False
                ImageLayout = blGlyphLeft
                DataSource = ABDatasource2_1
                VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
                ButtonRangeType = RtCustom
                BtnCustom1ImageIndex = 1
                BtnCustom2ImageIndex = -1
                BtnCustom3ImageIndex = -1
                BtnCustom4ImageIndex = -1
                BtnCustom5ImageIndex = -1
                BtnCustom6ImageIndex = -1
                BtnCustom7ImageIndex = -1
                BtnCustom8ImageIndex = -1
                BtnCustom9ImageIndex = -1
                BtnCustom10ImageIndex = -1
                BtnCustom1Caption = #33258#23450#20041'1'
                BtnCustom2Caption = #33258#23450#20041'2'
                BtnCustom3Caption = #33258#23450#20041'3'
                BtnCustom4Caption = #33258#23450#20041'4'
                BtnCustom5Caption = #33258#23450#20041'5'
                BtnCustom6Caption = #33258#23450#20041'6'
                BtnCustom7Caption = #33258#23450#20041'7'
                BtnCustom8Caption = #33258#23450#20041'8'
                BtnCustom9Caption = #33258#23450#20041'9'
                BtnCustom10Caption = #33258#23450#20041'10'
                BtnCustom1Kind = cxbkStandard
                BtnCustom2Kind = cxbkStandard
                BtnCustom3Kind = cxbkStandard
                BtnCustom4Kind = cxbkStandard
                BtnCustom5Kind = cxbkStandard
                BtnCustom6Kind = cxbkStandard
                BtnCustom7Kind = cxbkStandard
                BtnCustom8Kind = cxbkStandard
                BtnCustom9Kind = cxbkStandard
                BtnCustom10Kind = cxbkStandard
                ApprovedRollbackButton = nbCustom1
                ApprovedCommitButton = nbCustom1
                ButtonControlType = ctSingle
              end
            end
          end
          object ABcxSplitter3: TABcxSplitter
            Left = 0
            Top = 185
            Width = 547
            Height = 8
            HotZoneClassName = 'TcxMediaPlayer8Style'
            AlignSplitter = salTop
            InvertDirection = True
            Control = ABcxPageControl4
          end
        end
        object ABcxGrid4: TABcxGrid
          Left = 1
          Top = 1
          Width = 202
          Height = 482
          Align = alClient
          TabOrder = 1
          object ABcxGridDBBandedTableView3: TABcxGridDBBandedTableView
            PopupMenu.AutoHotkeys = maManual
            PopupMenu.CloseFootStr = False
            PopupMenu.LinkTableView = ABcxGridDBBandedTableView3
            PopupMenu.AutoApplyBestFit = True
            PopupMenu.AutoCreateAllItem = True
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = ABDatasource2
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Filter.AutoDataSetFilter = True
            DataController.Filter.TranslateBetween = True
            DataController.Filter.TranslateIn = True
            DataController.Filter.TranslateLike = True
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.AlwaysShowEditor = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.DataRowSizing = True
            OptionsSelection.MultiSelect = True
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
            Bands = <
              item
              end>
            ExtPopupMenu.AutoHotkeys = maManual
            ExtPopupMenu.CloseFootStr = False
            ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView3
            ExtPopupMenu.AutoApplyBestFit = True
            ExtPopupMenu.AutoCreateAllItem = True
          end
          object cxGridLevel3: TcxGridLevel
            GridView = ABcxGridDBBandedTableView3
          end
        end
        object ABcxSplitter4: TABcxSplitter
          Left = 203
          Top = 1
          Width = 8
          Height = 482
          HotZoneClassName = 'TcxMediaPlayer8Style'
          AlignSplitter = salRight
          InvertDirection = True
          Control = Panel4
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = #24050#23436#25104
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel5: TPanel
        Left = 0
        Top = 25
        Width = 759
        Height = 459
        Align = alClient
        BevelOuter = bvLowered
        TabOrder = 0
        object Panel6: TPanel
          Left = 211
          Top = 1
          Width = 547
          Height = 457
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object ABcxPageControl6: TABcxPageControl
            Left = 0
            Top = 0
            Width = 547
            Height = 185
            Align = alTop
            TabOrder = 0
            Properties.ActivePage = cxTabSheet6
            Properties.CustomButtons.Buttons = <>
            Properties.HideTabs = True
            LookAndFeel.Kind = lfFlat
            ActivePageIndex = 0
            ClientRectBottom = 184
            ClientRectLeft = 1
            ClientRectRight = 546
            ClientRectTop = 1
            object cxTabSheet6: TcxTabSheet
              Caption = #22522#26412#20449#24687
              ImageIndex = 0
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object ABDBPanel3: TABDBPanel
                Left = 0
                Top = 0
                Width = 545
                Height = 183
                Align = alClient
                BevelOuter = bvNone
                Caption = 'ABDBPanel1'
                ShowCaption = False
                TabOrder = 0
                ReadOnly = False
                DataSource = ABDatasource3
                AddAnchors_akRight = True
                AddAnchors_akBottom = True
                AutoHeight = True
                AutoWidth = True
              end
            end
          end
          object ABcxPageControl7: TABcxPageControl
            Left = 0
            Top = 193
            Width = 547
            Height = 264
            Align = alClient
            TabOrder = 1
            Properties.ActivePage = cxTabSheet7
            Properties.CustomButtons.Buttons = <>
            Properties.HideTabs = True
            LookAndFeel.Kind = lfFlat
            ActivePageIndex = 0
            ClientRectBottom = 263
            ClientRectLeft = 1
            ClientRectRight = 546
            ClientRectTop = 1
            object cxTabSheet7: TcxTabSheet
              Caption = #32852#31995#20154#20449#24687
              ImageIndex = 0
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object ABcxGrid5: TABcxGrid
                Left = 0
                Top = 25
                Width = 545
                Height = 237
                Align = alClient
                TabOrder = 0
                object ABcxGridDBBandedTableView4: TABcxGridDBBandedTableView
                  PopupMenu.AutoHotkeys = maManual
                  PopupMenu.CloseFootStr = False
                  PopupMenu.LinkTableView = ABcxGridDBBandedTableView4
                  PopupMenu.AutoApplyBestFit = True
                  PopupMenu.AutoCreateAllItem = True
                  Navigator.Buttons.CustomButtons = <>
                  DataController.DataSource = ABDatasource3_1
                  DataController.Filter.Options = [fcoCaseInsensitive]
                  DataController.Filter.AutoDataSetFilter = True
                  DataController.Filter.TranslateBetween = True
                  DataController.Filter.TranslateIn = True
                  DataController.Filter.TranslateLike = True
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  OptionsBehavior.AlwaysShowEditor = True
                  OptionsBehavior.FocusCellOnTab = True
                  OptionsBehavior.GoToNextCellOnEnter = True
                  OptionsCustomize.ColumnsQuickCustomization = True
                  OptionsCustomize.DataRowSizing = True
                  OptionsSelection.MultiSelect = True
                  OptionsView.GroupByBox = False
                  OptionsView.Indicator = True
                  Bands = <
                    item
                    end>
                  ExtPopupMenu.AutoHotkeys = maManual
                  ExtPopupMenu.CloseFootStr = False
                  ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView4
                  ExtPopupMenu.AutoApplyBestFit = True
                  ExtPopupMenu.AutoCreateAllItem = True
                end
                object cxGridLevel4: TcxGridLevel
                  GridView = ABcxGridDBBandedTableView4
                end
              end
              object ABDBNavigator5: TABDBNavigator
                Left = 0
                Top = 0
                Width = 545
                Height = 25
                Align = alTop
                BevelOuter = bvNone
                BiDiMode = bdLeftToRight
                Caption = 'ABDBNavigator1'
                ParentBiDiMode = False
                ShowCaption = False
                TabOrder = 1
                BigGlyph = False
                ImageLayout = blGlyphLeft
                DataSource = ABDatasource3_1
                VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
                ButtonRangeType = RtCustom
                BtnCustom1ImageIndex = 1
                BtnCustom2ImageIndex = -1
                BtnCustom3ImageIndex = -1
                BtnCustom4ImageIndex = -1
                BtnCustom5ImageIndex = -1
                BtnCustom6ImageIndex = -1
                BtnCustom7ImageIndex = -1
                BtnCustom8ImageIndex = -1
                BtnCustom9ImageIndex = -1
                BtnCustom10ImageIndex = -1
                BtnCustom1Caption = #33258#23450#20041'1'
                BtnCustom2Caption = #33258#23450#20041'2'
                BtnCustom3Caption = #33258#23450#20041'3'
                BtnCustom4Caption = #33258#23450#20041'4'
                BtnCustom5Caption = #33258#23450#20041'5'
                BtnCustom6Caption = #33258#23450#20041'6'
                BtnCustom7Caption = #33258#23450#20041'7'
                BtnCustom8Caption = #33258#23450#20041'8'
                BtnCustom9Caption = #33258#23450#20041'9'
                BtnCustom10Caption = #33258#23450#20041'10'
                BtnCustom1Kind = cxbkStandard
                BtnCustom2Kind = cxbkStandard
                BtnCustom3Kind = cxbkStandard
                BtnCustom4Kind = cxbkStandard
                BtnCustom5Kind = cxbkStandard
                BtnCustom6Kind = cxbkStandard
                BtnCustom7Kind = cxbkStandard
                BtnCustom8Kind = cxbkStandard
                BtnCustom9Kind = cxbkStandard
                BtnCustom10Kind = cxbkStandard
                ApprovedRollbackButton = nbCustom1
                ApprovedCommitButton = nbCustom1
                ButtonControlType = ctSingle
              end
            end
          end
          object ABcxSplitter5: TABcxSplitter
            Left = 0
            Top = 185
            Width = 547
            Height = 8
            HotZoneClassName = 'TcxMediaPlayer8Style'
            AlignSplitter = salTop
            InvertDirection = True
            Control = ABcxPageControl6
          end
        end
        object ABcxGrid6: TABcxGrid
          Left = 1
          Top = 1
          Width = 202
          Height = 457
          Align = alClient
          TabOrder = 1
          object ABcxGridDBBandedTableView5: TABcxGridDBBandedTableView
            PopupMenu.AutoHotkeys = maManual
            PopupMenu.CloseFootStr = False
            PopupMenu.LinkTableView = ABcxGridDBBandedTableView5
            PopupMenu.AutoApplyBestFit = True
            PopupMenu.AutoCreateAllItem = True
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = ABDatasource3
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Filter.AutoDataSetFilter = True
            DataController.Filter.TranslateBetween = True
            DataController.Filter.TranslateIn = True
            DataController.Filter.TranslateLike = True
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.AlwaysShowEditor = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.DataRowSizing = True
            OptionsSelection.MultiSelect = True
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
            Bands = <
              item
              end>
            ExtPopupMenu.AutoHotkeys = maManual
            ExtPopupMenu.CloseFootStr = False
            ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView5
            ExtPopupMenu.AutoApplyBestFit = True
            ExtPopupMenu.AutoCreateAllItem = True
          end
          object cxGridLevel5: TcxGridLevel
            GridView = ABcxGridDBBandedTableView5
          end
        end
        object ABcxSplitter6: TABcxSplitter
          Left = 203
          Top = 1
          Width = 8
          Height = 457
          HotZoneClassName = 'TcxMediaPlayer8Style'
          AlignSplitter = salRight
          InvertDirection = True
          Control = Panel6
        end
      end
      object ABDBNavigator7: TABDBNavigator
        Left = 0
        Top = 0
        Width = 759
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        Caption = 'ABDBNavigator1'
        ShowCaption = False
        TabOrder = 1
        BigGlyph = False
        ImageLayout = blGlyphLeft
        DataSource = ABDatasource3
        VisibleButtons = [nbQuerySpacer, nbQuery, nbCustomSpacer, nbCustom1]
        ButtonRangeType = RtCustom
        BtnCustom1ImageIndex = -1
        BtnCustom2ImageIndex = -1
        BtnCustom3ImageIndex = -1
        BtnCustom4ImageIndex = -1
        BtnCustom5ImageIndex = -1
        BtnCustom6ImageIndex = -1
        BtnCustom7ImageIndex = -1
        BtnCustom8ImageIndex = -1
        BtnCustom9ImageIndex = -1
        BtnCustom10ImageIndex = -1
        BtnCustom1Caption = ' '#25764#28040' '
        BtnCustom2Caption = #33258#23450#20041'2'
        BtnCustom3Caption = #33258#23450#20041'3'
        BtnCustom4Caption = #33258#23450#20041'4'
        BtnCustom5Caption = #33258#23450#20041'5'
        BtnCustom6Caption = #33258#23450#20041'6'
        BtnCustom7Caption = #33258#23450#20041'7'
        BtnCustom8Caption = #33258#23450#20041'8'
        BtnCustom9Caption = #33258#23450#20041'9'
        BtnCustom10Caption = #33258#23450#20041'10'
        BtnCustom1Kind = cxbkStandard
        BtnCustom2Kind = cxbkCommandLink
        BtnCustom3Kind = cxbkStandard
        BtnCustom4Kind = cxbkStandard
        BtnCustom5Kind = cxbkStandard
        BtnCustom6Kind = cxbkStandard
        BtnCustom7Kind = cxbkStandard
        BtnCustom8Kind = cxbkStandard
        BtnCustom9Kind = cxbkStandard
        BtnCustom10Kind = cxbkDropDown
        ApproveBill = 'ERP_Base_PriceSystem'
        ApproveMainGuidField = 'PS_Guid'
        ApproveStateField = 'PS_State'
        ApprovedRollbackButton = nbCustom1
        ApprovedCommitButton = nbNull
        ButtonControlType = ctSingle
      end
    end
  end
  object ABDBStatusBar1: TABdxDBStatusBar
    Left = 0
    Top = 531
    Width = 761
    Height = 19
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 50
      end>
    PaintStyle = stpsUseLookAndFeel
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    StopUpdate = False
    DataSource = ABDatasource1
  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    SQL.Strings = (
      'select * '
      'from ERP_Base_PriceSystem'
      'where PS_State='#39'Input'#39
      '')
    SqlUpdateDatetime = 42230.674298333330000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_PriceSystem')
    IndexListDefs = <
      item
        Name = 'IX_ERP_Base_Cust'
        Fields = 'Cu_Order'
      end
      item
        Name = 'IX_Cu_Code'
        Fields = 'Cu_Code'
        Options = [ixUnique]
      end
      item
        Name = 'IX_Cu_Name'
        Fields = 'Cu_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Cu_Guid'
        Fields = 'Cu_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Base_PriceSystem')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 248
    Top = 296
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 320
    Top = 296
  end
  object ABQuery1_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    MasterSource = ABDatasource1
    MasterFields = 'PS_GUID'
    DetailFields = 'Pi_PS_GUID'
    SQL.Strings = (
      'select *'
      'from ERP_Base_PriceSystemItem'
      'where Pi_PS_GUID=:PS_GUID')
    SqlUpdateDatetime = 42213.516102511560000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_PriceSystemItem')
    IndexListDefs = <
      item
        Name = 'IX_ERP_Base_Cust'
        Fields = 'Cu_Order'
      end
      item
        Name = 'IX_Cu_Code'
        Fields = 'Cu_Code'
        Options = [ixUnique]
      end
      item
        Name = 'IX_Cu_Name'
        Fields = 'Cu_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Cu_Guid'
        Fields = 'Cu_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Base_PriceSystemItem')
    InitActiveOK = False
    BlobFieldDefs = <
      item
        FieldName = 'Cl_Picture'
      end>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 410
    Top = 296
    ParamData = <
      item
        Name = 'PS_GUID'
        ParamType = ptInput
      end>
  end
  object ABDatasource1_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_1
    Left = 490
    Top = 296
  end
  object ABDatasource2_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery2_1
    Left = 488
    Top = 360
  end
  object ABDatasource3_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery3_1
    Left = 488
    Top = 432
  end
  object ABQuery3_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    MasterSource = ABDatasource3
    MasterFields = 'PS_GUID'
    DetailFields = 'Pi_PS_GUID'
    SQL.Strings = (
      'select *'
      'from ERP_Base_PriceSystemItem'
      'where Pi_PS_GUID=:PS_GUID')
    SqlUpdateDatetime = 42230.674794745370000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_PriceSystemItem')
    IndexListDefs = <
      item
        Name = 'IX_ERP_Base_Cust'
        Fields = 'Cu_Order'
      end
      item
        Name = 'IX_Cu_Code'
        Fields = 'Cu_Code'
        Options = [ixUnique]
      end
      item
        Name = 'IX_Cu_Name'
        Fields = 'Cu_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Cu_Guid'
        Fields = 'Cu_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Base_PriceSystemItem')
    InitActiveOK = False
    BlobFieldDefs = <
      item
        FieldName = 'Cl_Picture'
      end>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 408
    Top = 432
    ParamData = <
      item
        Name = 'PS_GUID'
        ParamType = ptInput
      end>
  end
  object ABQuery2_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    MasterSource = ABDatasource2
    MasterFields = 'PS_GUID'
    DetailFields = 'Pi_PS_GUID'
    SQL.Strings = (
      'select *'
      'from ERP_Base_PriceSystemItem'
      'where Pi_PS_GUID=:PS_GUID')
    SqlUpdateDatetime = 42230.674704872680000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_PriceSystemItem')
    IndexListDefs = <
      item
        Name = 'IX_ERP_Base_Cust'
        Fields = 'Cu_Order'
      end
      item
        Name = 'IX_Cu_Code'
        Fields = 'Cu_Code'
        Options = [ixUnique]
      end
      item
        Name = 'IX_Cu_Name'
        Fields = 'Cu_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Cu_Guid'
        Fields = 'Cu_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Base_PriceSystemItem')
    InitActiveOK = False
    BlobFieldDefs = <
      item
        FieldName = 'Cl_Picture'
      end>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 408
    Top = 360
    ParamData = <
      item
        Name = 'PS_GUID'
        ParamType = ptInput
      end>
  end
  object ABDatasource3: TABDatasource
    AutoEdit = False
    DataSet = ABQuery3
    Left = 320
    Top = 432
  end
  object ABQuery3: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    SQL.Strings = (
      'select * '
      'from ERP_Base_PriceSystem'
      'where PS_State='#39'Finish'#39)
    SqlUpdateDatetime = 42230.674443009260000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_PriceSystem')
    IndexListDefs = <
      item
        Name = 'IX_ABERP_Base_PriceSystem'
        Fields = 'Ps_Datetime'
      end
      item
        Name = 'IX_Ps_Name'
        Fields = 'Ps_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Ps_Guid'
        Fields = 'Ps_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Base_PriceSystem')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 248
    Top = 432
  end
  object ABQuery2: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    SQL.Strings = (
      'select * '
      'from ERP_Base_PriceSystem'
      'where PS_State='#39'ApprovedIng'#39)
    SqlUpdateDatetime = 42230.847078275460000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'ERP'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ERP_Base_PriceSystem')
    IndexListDefs = <
      item
        Name = 'IX_ABERP_Base_PriceSystem'
        Fields = 'Ps_Datetime'
      end
      item
        Name = 'IX_Ps_Name'
        Fields = 'Ps_Name'
        Options = [ixUnique]
      end
      item
        Name = 'PK_Ps_Guid'
        Fields = 'Ps_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ERP_Base_PriceSystem')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 248
    Top = 360
  end
  object ABDatasource2: TABDatasource
    AutoEdit = False
    DataSet = ABQuery2
    Left = 320
    Top = 360
  end
end
