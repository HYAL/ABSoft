unit ERP_Base_ProducePlantU;

interface

uses
  ABFramkWorkFuncFormU,
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, dxStatusBar,
  ABFramkWorkControlU, ABFramkWorkDBPanelU, Vcl.ExtCtrls, ABPubPanelU,
  ABFramkWorkDBNavigatorU, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, ABFramkWorkcxGridU, cxGrid, ABFramkWorkQueryU,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, ABThirdDBU, ABThirdCustomQueryU,
  ABThirdQueryU, ABFramkWorkDictionaryQueryU, dxBarBuiltInMenu, cxPC, cxSplitter,
  cxContainer, cxImage, cxDBEdit;

type
  TERP_Base_ProducePlantForm = class(TABFuncForm)
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABDBStatusBar1: TABdxDBStatusBar;
    ABDBPanel1: TABDBPanel;
    ABDBNavigator1: TABDBNavigator;
    ABcxGrid1: TABcxGrid;
    ABcxGrid1ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    ABcxGrid1Level1: TcxGridLevel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ERP_Base_ProducePlantForm: TERP_Base_ProducePlantForm;

implementation
{$R *.dfm}
procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :='TERP_Base_ProducePlantForm';
end;

exports
   ABRegister ;

procedure TERP_Base_ProducePlantForm.FormCreate(Sender: TObject);
begin
  ABInitFormDataSet([ABDBPanel1],[ABcxGrid1ABcxGridDBBandedTableView1],ABQuery1);
end;

initialization
  RegisterClass(TERP_Base_ProducePlantForm);
finalization
  UnRegisterClass(TERP_Base_ProducePlantForm);
end.
