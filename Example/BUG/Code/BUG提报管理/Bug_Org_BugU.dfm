object Bug_Org_BugForm: TBug_Org_BugForm
  Left = 0
  Top = 0
  Caption = 'BUG'#25552#25253#31649#29702
  ClientHeight = 550
  ClientWidth = 800
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ABDBStatusBar1: TABdxDBStatusBar
    Left = 0
    Top = 531
    Width = 800
    Height = 19
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 50
      end>
    PaintStyle = stpsUseLookAndFeel
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    StopUpdate = False
  end
  object Sheet1_Splitter1: TABcxSplitter
    Left = 672
    Top = 0
    Width = 8
    Height = 492
    HotZoneClassName = 'TcxMediaPlayer8Style'
    AlignSplitter = salRight
    InvertDirection = True
    Control = Panel1
    ExplicitTop = 26
    ExplicitHeight = 466
  end
  object Panel1: TPanel
    Left = 680
    Top = 0
    Width = 120
    Height = 492
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 2
    object ABcxLabel4: TABcxLabel
      Left = 0
      Top = 0
      Align = alTop
      Caption = #25552#25253#38468#20214
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -14
      Style.Font.Name = #23435#20307
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Properties.Alignment.Horz = taCenter
      Properties.Alignment.Vert = taVCenter
      Transparent = True
      AnchorX = 60
      AnchorY = 9
    end
    object Panel4: TPanel
      Left = 0
      Top = 467
      Width = 120
      Height = 25
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitTop = 441
      object SpeedButton1: TABcxButton
        Left = 4
        Top = 1
        Width = 60
        Height = 22
        Caption = #22686#21152#38468#20214
        LookAndFeel.Kind = lfFlat
        TabOrder = 0
        OnClick = SpeedButton1Click
        ShowProgressBar = False
      end
      object SpeedButton2: TABcxButton
        Left = 65
        Top = 1
        Width = 60
        Height = 22
        Caption = #21024#38500#38468#20214
        LookAndFeel.Kind = lfFlat
        TabOrder = 1
        OnClick = SpeedButton2Click
        ShowProgressBar = False
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 492
    Width = 800
    Height = 39
    Align = alBottom
    TabOrder = 3
    DesignSize = (
      800
      39)
    object ABcxButton1: TABcxButton
      Left = 21
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #36864#20986
      LookAndFeel.Kind = lfFlat
      TabOrder = 0
      ShowProgressBar = False
    end
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    Left = 40
    Top = 190
  end
  object ABDatasource2: TABDatasource
    AutoEdit = False
    Left = 41
    Top = 247
  end
end
