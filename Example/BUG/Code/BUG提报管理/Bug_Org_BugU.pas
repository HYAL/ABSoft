unit Bug_Org_BugU;

interface

uses
  ABPubU,ABControlU,
  ABThirdControlU,
  ABFWPubU,ABFWConnU,ABFWDBExpress_ClientU ,ABFWControlU ,ABFwFuncFormU,ABImportDataU,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBClient, cxPC, cxControls, cxNavigator, cxDBNavigator, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, ComCtrls,
  StdCtrls, DBCtrls, ABFWBarDBNavigatorFrameU, cxGridBandedTableView,
  cxGridDBBandedTableView, cxButtonEdit, dxStatusBar, cxContainer, cxLabel,
  cxSplitter, Menus, cxLookAndFeelPainters, cxButtons, cxLookAndFeels,
  ABFramkWorkQueryU, ABFramkWorkControlU;

type
  TBug_Org_BugForm = class(TABFwFuncForm)
    ABDatasource1: TABDatasource;
    ABDBStatusBar1: TABdxDBStatusBar;
    Sheet1_Splitter1: TABcxSplitter;
    Panel1: TPanel;
    ABcxLabel4: TABcxLabel;
    ABDatasource2: TABDatasource;
    Panel4: TPanel;
    SpeedButton1: TABcxButton;
    SpeedButton2: TABcxButton;
    Panel3: TPanel;
    ABcxButton1: TABcxButton;
    procedure FormCreate(Sender: TObject);
    procedure ABcxGridDBBandedTableView2CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure ABBarDBNavigator1AfterBtnCustom1Click(Sender: TObject);
    procedure ABClientDataSet1AfterScroll(DataSet: TDataSet);
    procedure ABBarDBNavigator1AfterBtnCustom2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Bug_Org_BugForm: TBug_Org_BugForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :='TBug_Org_BugForm';
end;

exports
   ABRegister ;


procedure TBug_Org_BugForm.FormCreate(Sender: TObject);
begin
  ABInitFormDataSet([],[ABcxGridDBBandedTableView1],ABClientDataSet1);
  ABInitFormDataSet([],[ABcxGridDBBandedTableView2],ABClientDataSet2);
end;

procedure TBug_Org_BugForm.ABBarDBNavigator1AfterBtnCustom1Click(
  Sender: TObject);
begin
  ABClientDataset1.ABEdit;
  ABClientDataset1.FindField('Bu_State').asstring:='处理中';
  ABClientDataset1.FindField('Bu_OKDatetime').AsDateTime:=ABGetServerDateTime(ABMainConn);
  ABClientDataset1.ABPost;
end;

procedure TBug_Org_BugForm.ABBarDBNavigator1AfterBtnCustom2Click(
  Sender: TObject);
begin
  ABClientDataset1.ABEdit;
  ABClientDataset1.FindField('Bu_State').asstring:='已处理';
  ABClientDataset1.FindField('Bu_OKDatetime').AsDateTime:=ABGetServerDateTime(ABMainConn);
  ABClientDataset1.ABPost;
end;

//下载附件
procedure TBug_Org_BugForm.ABClientDataSet1AfterScroll(DataSet: TDataSet);
begin
  ABBarDBNavigator1.Frame.btnCustom1.Enabled:=
    AnsiCompareText(ABClientDataset1.FindField('Bu_OKOp_Name').AsString,ABPubUser.Name)=0;
  ABBarDBNavigator1.Frame.btnCustom2.Enabled:=ABBarDBNavigator1.Frame.btnCustom1.Enabled;
end;

procedure TBug_Org_BugForm.ABcxGridDBBandedTableView2CellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
var
  tempStr1:string;
begin
  tempStr1:=ABSaveFile('',ABClientDataset2.FindField('BF_FileName').AsString);
  if (tempStr1<>EmptyStr) and
     (ABClientDataset2.Active) and
     (not ABDatasetIsEmpty(ABClientDataset2)) then
  begin
    ABDownToFileFromField(ABMainConn,
               'Bug_Org_BugFile','Bf_File',
               'BF_Guid='+ABQuotedStr(ABClientDataset2.FindField('BF_Guid').AsString),
               tempStr1);
  end;
end;

//增加附件
procedure TBug_Org_BugForm.SpeedButton1Click(Sender: TObject);
var
  tempStr1:string;
begin
  tempStr1:=ABSelectFile;
  if ABCheckFileExists(tempStr1) then
  begin
    ABClientDataset2.Append;
    ABClientDataset2.FindField('BF_FileName').AsString:=ABGetFilename(tempStr1,false);
    ABClientDataset2.ABPost;
    ABUpFileToField(ABMainConn,
               'Bug_Org_BugFile','Bf_File',
               'BF_Guid='+ABQuotedStr(ABClientDataset2.FindField('BF_Guid').AsString),
               tempStr1);
  end;
end;

//删除附件
procedure TBug_Org_BugForm.SpeedButton2Click(Sender: TObject);
begin
  if (ABClientDataset2.Active) and
     (not ABDatasetIsEmpty(ABClientDataset2) ) then
  begin
    ABClientDataset2.ABDelete;
  end;
end;


initialization
  RegisterClass(TBug_Org_BugForm);

finalization
  UnRegisterClass(TBug_Org_BugForm);

end.
