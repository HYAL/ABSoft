unit Bug_Org_WaitJobU;

interface

uses
  ABPubU,ABControlU,
  ABThirdControlU,
  ABFWPubU,ABFWConnU,ABFWDBExpress_ClientU ,ABFWControlU ,ABFwFuncFormU,ABImportDataU,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBClient, cxPC, cxControls, cxNavigator, cxDBNavigator, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, ComCtrls,
  StdCtrls, DBCtrls, ABFWBarDBNavigatorFrameU, cxGridBandedTableView,
  cxGridDBBandedTableView, cxButtonEdit, dxStatusBar, cxContainer, cxLabel,
  cxSplitter, Menus, cxLookAndFeelPainters, cxButtons, cxLookAndFeels,
  ABFramkWorkQueryU, ABFramkWorkControlU;

type
  TBug_Org_WaitJobForm = class(TABFwFuncForm)
    ABDatasource1: TABDatasource;
    ABDBStatusBar1: TABdxDBStatusBar;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Bug_Org_WaitJobForm: TBug_Org_WaitJobForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :='TBug_Org_WaitJobForm';
end;

exports
   ABRegister ;


procedure TBug_Org_WaitJobForm.FormCreate(Sender: TObject);
begin
  ABInitFormDataSet([],[ABcxGridDBBandedTableView1],ABClientDataSet1);
end;


initialization
  RegisterClass(TBug_Org_WaitJobForm);

finalization
  UnRegisterClass(TBug_Org_WaitJobForm);

end.

