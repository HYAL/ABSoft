unit frxrcExports;

interface
{$I frx.inc}
implementation
uses frxRes;
const resXML =
'<?xml version="1.1" encoding="utf-8"?><Resources CodePage="1252">  <StrRes Name='+
'"8000" Text="Export to Excel"/>  <StrRes Name="8001" Text="Styles"/>  <StrRes Na'+
'me="8002" Text="Pictures"/>  <StrRes Name="8003" Text="Merge cells"/>  <StrRes N'+
'ame="8004" Text="Fast export"/>  <StrRes Name="8005" Text="WYSIWYG"/>  <StrRes N'+
'ame="8006" Text="As text"/>  <StrRes Name="8007" Text="Background"/>  <StrRes Na'+
'me="8008" Text="Open Excel after export"/>  <StrRes Name="8009" Text="Excel file'+
' (*.xls)|*.xls"/>  <StrRes Name="8010" Text=".xls"/>  <StrRes Name="8100" Text="'+
'Export to Excel"/>  <StrRes Name="8101" Text="Styles"/>  <StrRes Name="8102" Tex'+
't="WYSIWYG"/>  <StrRes Name="8103" Text="Background"/>  <StrRes Name="8104" Text'+
'="Open Excel after export"/>  <StrRes Name="8105" Text="XML Excel file (*.xls)|*'+
'.xls"/>  <StrRes Name="8106" Text=".xls"/>  <StrRes Name="8200" Text="Export to '+
'HTML table"/>  <StrRes Name="8201" Text="Open after export"/>  <StrRes Name="820'+
'2" Text="Styles"/>  <StrRes Name="8203" Text="Pictures"/>  <StrRes Name="8204" T'+
'ext="All in one folder"/>  <StrRes Name="8205" Text="Fixed width"/>  <StrRes Nam'+
'e="8206" Text="Page navigator"/>  <StrRes Name="8207" Text="Multipage"/>  <StrRe'+
's Name="8208" Text="Mozilla browser"/>  <StrRes Name="8209" Text="Background"/> '+
' <StrRes Name="8210" Text="HTML file (*.html)|*.html"/>  <StrRes Name="8211" Tex'+
't=".html"/>  <StrRes Name="8300" Text="Export to text (dot-matrix printer)"/>  <'+
'StrRes Name="8301" Text="Preview on/off"/>  <StrRes Name="8302" Text=" Export pr'+
'operties  "/>  <StrRes Name="8303" Text="Page breaks"/>  <StrRes Name="8304" Tex'+
't="OEM codepage"/>  <StrRes Name="8305" Text="Empty lines"/>  <StrRes Name="8306'+
'" Text="Lead spaces"/>  <StrRes Name="8307" Text="Page numbers"/>  <StrRes Name='+
'"8308" Text="Enter numbers and/or page ranges, separated by commas. For example:'+
' 1,3,5-12"/>  <StrRes Name="8309" Text=" Scaling "/>  <StrRes Name="8310" Text="'+
'X Scale"/>  <StrRes Name="8311" Text="Y Scale"/>  <StrRes Name="8312" Text=" Fra'+
'mes "/>  <StrRes Name="8313" Text="None"/>  <StrRes Name="8314" Text="Simple"/> '+
' <StrRes Name="8315" Text="Graphic"/>  <StrRes Name="8316" Text="Only with OEM c'+
'odepage"/>  <StrRes Name="8317" Text="Print after export"/>  <StrRes Name="8318"'+
' Text="Save settings"/>  <StrRes Name="8319" Text=" Preview "/>  <StrRes Name="8'+
'320" Text="Width:"/>  <StrRes Name="8321" Text="Height:"/>  <StrRes Name="8322" '+
'Text="Page"/>  <StrRes Name="8323" Text="Zoom in"/>  <StrRes Name="8324" Text="Z'+
'oom out"/>  <StrRes Name="8325" Text="Text file (dot-matrix printer)(*.prn)|*.pr'+
'n"/>  <StrRes Name="8326" Text=".prn"/>  <StrRes Name="8400" Text="Print"/>  <St'+
'rRes Name="8401" Text="Printer"/>  <StrRes Name="8402" Text="Name"/>  <StrRes Na'+
'me="8403" Text="Setup..."/>  <StrRes Name="8404" Text="Copies"/>  <StrRes Name="'+
'8405" Text="Number of copies"/>  <StrRes Name="8406" Text=" Printer init setup "'+
'/>  <StrRes Name="8407" Text="Printer type"/>  <StrRes Name="8408" Text=".fpi"/>'+
'  <StrRes Name="8409" Text="Printer init template (*.fpi)|*.fpi"/>  <StrRes Name'+
'="8410" Text=".fpi"/>  <StrRes Name="8411" Text="Printer init template (*.fpi)|*'+
'.fpi"/>  <StrRes Name="8500" Text="Export to RTF"/>  <StrRes Name="8501" Text="P'+
'ictures"/>  <StrRes Name="8502" Text="WYSIWYG"/>  <StrRes Name="8503" Text="Open'+
' after export"/>  <StrRes Name="8504" Text="RTF file (*.rtf)|*.rtf"/>  <StrRes N'+
'ame="8505" Text=".rtf"/>  <StrRes Name="8600" Text="Export Settings"/>  <StrRes '+
'Name="8601" Text=" Image settings "/>  <StrRes Name="8602" Text="JPEG quality"/>'+
'  <StrRes Name="8603" Text="Resolution (dpi)"/>  <StrRes Name="8604" Text="Separ'+
'ate files"/>  <StrRes Name="8605" Text="Crop pages"/>  <StrRes Name="8606" Text='+
'"Monochrome"/>  <StrRes Name="8700" Text="Export to PDF"/>  <StrRes Name="8701" '+
'Text="Compressed"/>  <StrRes Name="8702" Text="Embedded fonts"/>  <StrRes Name="'+
'8703" Text="Print optimized"/>  <StrRes Name="8704" Text="Outline"/>  <StrRes Na'+
'me="8705" Text="Background"/>  <StrRes Name="8706" Text="Open after export"/>  <'+
'StrRes Name="8707" Text="Adobe PDF file (*.pdf)|*.pdf"/>  <StrRes Name="8708" Te'+
'xt=".pdf"/>  <StrRes Name="RTFexport" Text="RTF file"/>  <StrRes Name="BMPexport'+
'" Text="BMP image"/>  <StrRes Name="JPEGexport" Text="JPEG image"/>  <StrRes Nam'+
'e="TIFFexport" Text="TIFF image"/>  <StrRes Name="PNGexport" Text="PNG image"/> '+
' <StrRes Name="EMFexport" Text="EMF image"/>  <StrRes Name="TextExport" Text="Te'+
'xt (matrix printer)"/>  <StrRes Name="XlsOLEexport" Text="Excel table (OLE)"/>  '+
'<StrRes Name="HTMLexport" Text="HTML file"/>  <StrRes Name="XlsXMLexport" Text="'+
'Excel table (XML)"/>  <StrRes Name="PDFexport" Text="PDF file"/>  <StrRes Name="'+
'ProgressWait" Text="Please wait"/>  <StrRes Name="ProgressRows" Text="Setting up'+
' rows"/>  <StrRes Name="ProgressColumns" Text="Setting up columns"/>  <StrRes Na'+
'me="ProgressStyles" Text="Setting up styles"/>  <StrRes Name="ProgressObjects" T'+
'ext="Exporting objects"/>  <StrRes Name="TIFFexportFilter" Text="Tiff image (*.t'+
'if)|*.tif"/>  <StrRes Name="BMPexportFilter" Text="BMP image (*.bmp)|*.bmp"/>  <'+
'StrRes Name="JPEGexportFilter" Text="Jpeg image (*.jpg)|*.jpg"/>  <StrRes Name="'+
'PNGexportFilter" Text="PNG image (*.png)|*.png"/>  <StrRes Name="EMFexportFilter'+
'" Text="EMF image (*.emf)|*.emf"/>  <StrRes Name="HTMLNavFirst" Text="First"/>  '+
'<StrRes Name="HTMLNavPrev" Text="Prev"/>  <StrRes Name="HTMLNavNext" Text="Next"'+
'/>  <StrRes Name="HTMLNavLast" Text="Last"/>  <StrRes Name="HTMLNavRefresh" Text'+
'="Refresh"/>  <StrRes Name="HTMLNavPrint" Text="Print"/>  <StrRes Name="HTMLNavT'+
'otal" Text="Total pages"/>  <StrRes Name="8800" Text="Export to Text"/>  <StrRes'+
' Name="8801" Text="Text file (*.txt)|*.txt"/>  <StrRes Name="8802" Text=".txt"/>'+
'  <StrRes Name="SimpleTextExport" Text="Text file"/>  <StrRes Name="8850" Text="'+
'Export to CSV"/>  <StrRes Name="8851" Text="CSV file (*.csv)|*.csv"/>  <StrRes N'+
'ame="8852" Text=".csv"/>  <StrRes Name="8853" Text="Separator"/>  <StrRes Name="'+
'CSVExport" Text="CSV file"/>  <StrRes Name="8900" Text="Send by E-mail"/>  <StrR'+
'es Name="8901" Text="E-mail"/>  <StrRes Name="8902" Text="Account"/>  <StrRes Na'+
'me="8903" Text="Connection"/>  <StrRes Name="8904" Text="Address"/>  <StrRes Nam'+
'e="8905" Text="Attachment"/>  <StrRes Name="8906" Text="Format"/>  <StrRes Name='+
'"8907" Text="From Address"/>  <StrRes Name="8908" Text="From Name"/>  <StrRes Na'+
'me="8909" Text="Host"/>  <StrRes Name="8910" Text="Login"/>  <StrRes Name="8911"'+
' Text="Mail"/>  <StrRes Name="8912" Text="Message"/>  <StrRes Name="8913" Text="'+
'Text"/>  <StrRes Name="8914" Text="Organization"/>  <StrRes Name="8915" Text="Pa'+
'ssword"/>  <StrRes Name="8916" Text="Port"/>  <StrRes Name="8917" Text="Remember'+
' properties"/>  <StrRes Name="8918" Text="Required fields are Empty"/>  <StrRes '+
'Name="8919" Text="Advanced export settings"/>  <StrRes Name="8920" Text="Signatu'+
're"/>  <StrRes Name="8921" Text="Build"/>  <StrRes Name="8922" Text="Subject"/> '+
' <StrRes Name="8923" Text="Best regards"/>  <StrRes Name="8924" Text="Send mail '+
'to"/>  <StrRes Name="EmailExport" Text="E-mail"/>  <StrRes Name="FastReportFile"'+
' Text="FastReport file"/>  <StrRes Name="GifexportFilter" Text="Gif file (*.gif)'+
'|*.gif"/>  <StrRes Name="GIFexport" Text="Gif image"/>  <StrRes Name="8950" Text'+
'="Continuous"/>  <StrRes Name="8951" Text="Page Header/Footer"/>  <StrRes Name="'+
'8952" Text="Text"/>  <StrRes Name="8953" Text="Header/Footer"/>  <StrRes Name="8'+
'954" Text="None"/>  <StrRes Name="ODSExportFilter" Text="Open Document Spreadshe'+
'et file (*.ods)|*.ods"/>  <StrRes Name="ODSExport" Text="Open Document Spreadshe'+
'et"/>  <StrRes Name="ODTExportFilter" Text="Open Document Text file (*.odt)|*.od'+
't"/>  <StrRes Name="ODTExport" Text="Open Document Text"/>  <StrRes Name="8960" '+
'Text=".ods"/>  <StrRes Name="8961" Text=".odt"/>  <StrRes Name="8962" Text="Secu'+
'rity"/>  <StrRes Name="8963" Text="Security settings"/>  <StrRes Name="8964" Tex'+
't="Owner password"/>  <StrRes Name="8965" Text="User password"/>  <StrRes Name="'+
'8966" Text="Print the document"/>  <StrRes Name="8967" Text="Modify the document'+
'"/>  <StrRes Name="8968" Text="Copy of text and graphics"/>  <StrRes Name="8969"'+
' Text="Add or modify text annotations"/>  <StrRes Name="8970" Text="PDF Security'+
'"/>  <StrRes Name="8971" Text="Document information"/>  <StrRes Name="8972" Text'+
'="Information"/>  <StrRes Name="8973" Text="Title"/>  <StrRes Name="8974" Text="'+
'Author"/>  <StrRes Name="8975" Text="Subject"/>  <StrRes Name="8976" Text="Keywo'+
'rds"/>  <StrRes Name="8977" Text="Creator"/>  <StrRes Name="8978" Text="Producer'+
'"/>  <StrRes Name="8979" Text="Authentification"/>  <StrRes Name="8980" Text="Pe'+
'rmissions"/>  <StrRes Name="8981" Text="Viewer"/>  <StrRes Name="8982" Text="Vie'+
'wer preferences"/>  <StrRes Name="8983" Text="Hide toolbar"/>  <StrRes Name="898'+
'4" Text="Hide menubar"/>  <StrRes Name="8985" Text="Hide window user interface"/'+
'>  <StrRes Name="8986" Text="Fit window"/>  <StrRes Name="8987" Text="Center win'+
'dow"/>  <StrRes Name="8988" Text="Print scaling"/>  <StrRes Name="8989" Text="Co'+
'nfirmation Reading"/>  <StrRes Name="9000" Text="Rows count:"/>  <StrRes Name="9'+
'001" Text="Split To Sheet"/>  <StrRes Name="9002" Text="Don''t split"/>  <StrRes '+
'Name="9003" Text="Use report pages"/>  <StrRes Name="9004" Text="Use print on pa'+
'rent"/>  <StrRes Name="9101" Text="Export to DBF"/>  <StrRes Name="9102" Text="d'+
'Base (DBF) export"/>  <StrRes Name="9103" Text=".dbf"/>  <StrRes Name="9104" Tex'+
't="Failed to load the file"/>  <StrRes Name="9105" Text="Failure"/>  <StrRes Nam'+
'e="9106" Text="Field names"/>  <StrRes Name="9107" Text="Automatically"/>  <StrR'+
'es Name="9108" Text="Manually"/>  <StrRes Name="9109" Text="Load from file"/>  <'+
'StrRes Name="9110" Text="Text files (*.txt)|*.txt|All files|*.*"/>  <StrRes Name'+
'="9111" Text="DBF files (*.dbf)|*.dbf|All files|*.*"/>  <StrRes Name="9151" Text'+
'="Excel 97/2000/XP file"/>  <StrRes Name="9152" Text="Auto create file"/>  <StrR'+
'es Name="9153" Text="Options"/>  <StrRes Name="9154" Text="Page"/>  <StrRes Name'+
'="9155" Text="Grid Lines"/>  <StrRes Name="9156" Text="All in one page"/>  <StrR'+
'es Name="9157" Text="Data Grouping"/>  <StrRes Name="9158" Text="Like the report'+
'"/>  <StrRes Name="9159" Text="Chunks. Each chunk has (rows):"/>  <StrRes Name="'+
'9160" Text="Chunk size must be a non negative integer value."/>  <StrRes Name="9'+
'161" Text="SingleSheet must be False when exporting to chunks"/>  <StrRes Name="'+
'9162" Text="Author"/>  <StrRes Name="9163" Text="Comment"/>  <StrRes Name="9164"'+
' Text="Keywords"/>  <StrRes Name="9165" Text="Revision"/>  <StrRes Name="9166" T'+
'ext="Version"/>  <StrRes Name="9167" Text="Application"/>  <StrRes Name="9168" T'+
'ext="Subject"/>  <StrRes Name="9169" Text="Category"/>  <StrRes Name="9170" Text'+
'="Company"/>  <StrRes Name="9171" Text="Title"/>  <StrRes Name="9172" Text="Mana'+
'ger"/>  <StrRes Name="9173" Text="General"/>  <StrRes Name="9174" Text="Informat'+
'ion"/>  <StrRes Name="9175" Text="Protection"/>  <StrRes Name="9176" Text="Passw'+
'ord"/>  <StrRes Name="9177" Text="If you set a non empty password string, the ge'+
'nerated document will be protected with this password. The password is always wr'+
'itten in Unicode characters and must be shorter than 256 Unicode characters."/> '+
' <StrRes Name="9178" Text="Confirmation"/>  <StrRes Name="9179" Text="Password c'+
'onfirmation does not match the password. Type the password again."/>  <StrRes Na'+
'me="9180" Text="Attempting to set a password of %d characters. The maximum allow'+
'ed length of a password is %d characters."/>  <StrRes Name="9181" Text="Adjust p'+
'age size"/>  <StrRes Name="9182" Text="Export to Excel 97/2000/XP"/>  <StrRes Na'+
'me="9183" Text="Delete empty rows"/>  <StrRes Name="9184" Text="Export formulas"'+
'/>    <StrRes Name="BiffCol" Text="Resizing columns"/>  <StrRes Name="BiffRow" T'+
'ext="Resizing rows"/>  <StrRes Name="BiffCell" Text="Exporting cells"/>  <StrRes'+
' Name="BiffImg" Text="Exporting pictures"/>    <StrRes Name="9200" Text="Microso'+
'ft Excel 2007 XML"/>  <StrRes Name="9201" Text="Microsoft PowerPoint 2007 XML"/>'+
'  <StrRes Name="9203" Text="Microsoft Word 2007 XML"/>    <StrRes Name="9300" Te'+
'xt="HTML Layered"/>  <StrRes Name="9301" Text="HTML files (*.html)|*.html|All fi'+
'les|*.*"/>  <StrRes Name="9302" Text="HTML Layered Export"/>  <StrRes Name="9303'+
'" Text="HTML5 Layered"/>  <StrRes Name="9304" Text="HTML4 Layered"/>    <StrRes '+
'Name="9400" Text="Reordering components"/>  <StrRes Name="9401" Text="Rendering '+
'components"/>  <StrRes Name="9402" Text="Adjusting intersecting components"/>  <'+
'StrRes Name="9403" Text="Removing empty rows"/>  <StrRes Name="9404" Text="Adjus'+
'ting frames"/>  <StrRes Name="9405" Text="Splitting big cells"/>  <StrRes Name="'+
'9406" Text="Composing BIFF file"/>    <StrRes Name="xProto" Text="Export Prototy'+
'pe"/>  <StrRes Name="xBlank" Text="Blank Export"/>  <StrRes Name="expMailAddr" T'+
'ext="Address or addresses delimited by comma"/></Resources>'+
'';
initialization
    frxResources.AddXML(Utf8Encode(resXML));

end.
