unit frxrcDesgn;

interface
{$I frx.inc}
implementation
uses frxRes;
const resXML =
'<?xml version="1.1" encoding="utf-8"?><Resources CodePage="1252">  <StrRes Name='+
'"2000" Text="Object Inspector"/>  <StrRes Name="oiProp" Text="Properties"/>  <St'+
'rRes Name="oiEvent" Text="Events"/>  <StrRes Name="2100" Text="Data Tree"/>  <St'+
'rRes Name="2101" Text="Data"/>  <StrRes Name="2102" Text="Variables"/>  <StrRes '+
'Name="2103" Text="Functions"/>  <StrRes Name="2104" Text="Create field"/>  <StrR'+
'es Name="2105" Text="Create caption"/>  <StrRes Name="2106" Text="Classes"/>  <S'+
'trRes Name="dtNoData" Text="No data available."/>  <StrRes Name="dtNoData1" Text'+
'="Go to the &#38;#34;Report|Data...&#38;#34; menu to add existing datasets to yo'+
'ur report, or switch to the &#38;#34;Data&#38;#34; tab and create new datasets."'+
'/>  <StrRes Name="dtData" Text="Data"/>  <StrRes Name="dtSysVar" Text="System va'+
'riables"/>  <StrRes Name="dtVar" Text="Variables"/>  <StrRes Name="dtFunc" Text='+
'"Functions"/>  <StrRes Name="2200" Text="Report Tree"/>  <StrRes Name="2300" Tex'+
't="Open Script File"/>  <StrRes Name="2301" Text="Save Script to File"/>  <StrRe'+
's Name="2302" Text="Run Script"/>  <StrRes Name="2303" Text="Trace Into"/>  <Str'+
'Res Name="2304" Text="Terminate Script"/>  <StrRes Name="2305" Text="Evaluate"/>'+
'  <StrRes Name="2306" Text="Language:"/>  <StrRes Name="2307" Text="Align"/>  <S'+
'trRes Name="2308" Text="Align Left"/>  <StrRes Name="2309" Text="Align Middle"/>'+
'  <StrRes Name="2310" Text="Align Right"/>  <StrRes Name="2311" Text="Align Top"'+
'/>  <StrRes Name="2312" Text="Align Center"/>  <StrRes Name="2313" Text="Align B'+
'ottom"/>  <StrRes Name="2314" Text="Space Horizontally"/>  <StrRes Name="2315" T'+
'ext="Space Vertically"/>  <StrRes Name="2316" Text="Center Horizontally in Band"'+
'/>  <StrRes Name="2317" Text="Center Vertically in Band"/>  <StrRes Name="2318" '+
'Text="Same Width"/>  <StrRes Name="2319" Text="Same Height"/>  <StrRes Name="232'+
'0" Text="Text"/>  <StrRes Name="2321" Text="Style"/>  <StrRes Name="2322" Text="'+
'Font Name"/>  <StrRes Name="2323" Text="Font Size"/>  <StrRes Name="2324" Text="'+
'Bold"/>  <StrRes Name="2325" Text="Italic"/>  <StrRes Name="2326" Text="Underlin'+
'e"/>  <StrRes Name="2327" Text="Font Color"/>  <StrRes Name="2328" Text="Highlig'+
'ht"/>  <StrRes Name="2329" Text="Text Rotation"/>  <StrRes Name="2330" Text="Ali'+
'gn Left"/>  <StrRes Name="2331" Text="Align Center"/>  <StrRes Name="2332" Text='+
'"Align Right"/>  <StrRes Name="2333" Text="Justify"/>  <StrRes Name="2334" Text='+
'"Align Top"/>  <StrRes Name="2335" Text="Align Middle"/>  <StrRes Name="2336" Te'+
'xt="Align Bottom"/>  <StrRes Name="2337" Text="Frame"/>  <StrRes Name="2338" Tex'+
't="Top Line"/>  <StrRes Name="2339" Text="Bottom Line"/>  <StrRes Name="2340" Te'+
'xt="Left Line"/>  <StrRes Name="2341" Text="Right Line"/>  <StrRes Name="2342" T'+
'ext="All Frame Lines"/>  <StrRes Name="2343" Text="No Frame"/>  <StrRes Name="23'+
'44" Text="Edit Frame"/>  <StrRes Name="2345" Text="Background Color"/>  <StrRes '+
'Name="2346" Text="Frame Color"/>  <StrRes Name="2347" Text="Frame Style"/>  <Str'+
'Res Name="2348" Text="Frame Width"/>  <StrRes Name="2349" Text="Standard"/>  <St'+
'rRes Name="2350" Text="New Report"/>  <StrRes Name="2351" Text="Open Report"/>  '+
'<StrRes Name="2352" Text="Save Report"/>  <StrRes Name="2353" Text="Preview"/>  '+
'<StrRes Name="2354" Text="New Report Page"/>  <StrRes Name="2355" Text="New Dial'+
'og Page"/>  <StrRes Name="2356" Text="Delete Page"/>  <StrRes Name="2357" Text="'+
'Page Settings"/>  <StrRes Name="2358" Text="Variables"/>  <StrRes Name="2359" Te'+
'xt="Cut"/>  <StrRes Name="2360" Text="Copy"/>  <StrRes Name="2361" Text="Paste"/'+
'>  <StrRes Name="2362" Text="Copy Formatting"/>  <StrRes Name="2363" Text="Undo"'+
'/>  <StrRes Name="2364" Text="Redo"/>  <StrRes Name="2365" Text="Group"/>  <StrR'+
'es Name="2366" Text="Ungroup"/>  <StrRes Name="2367" Text="Show Grid"/>  <StrRes'+
' Name="2368" Text="Align to Grid"/>  <StrRes Name="2369" Text="Fit to Grid"/>  <'+
'StrRes Name="2370" Text="Zoom"/>  <StrRes Name="2371" Text="Extra Tools"/>  <Str'+
'Res Name="2372" Text="Select Tool"/>  <StrRes Name="2373" Text="Hand Tool"/>  <S'+
'trRes Name="2374" Text="Zoom Tool"/>  <StrRes Name="2375" Text="Edit Text Tool"/'+
'>  <StrRes Name="2376" Text="Copy Format Tool"/>  <StrRes Name="2377" Text="Inse'+
'rt Band"/>  <StrRes Name="2378" Text="&amp;File"/>  <StrRes Name="2379" Text="&a'+
'mp;Edit"/>  <StrRes Name="2380" Text="Find..."/>  <StrRes Name="2381" Text="Find'+
' Next"/>  <StrRes Name="2382" Text="Replace..."/>  <StrRes Name="2383" Text="&am'+
'p;Report"/>  <StrRes Name="2384" Text="Data..."/>  <StrRes Name="2385" Text="Opt'+
'ions..."/>  <StrRes Name="2386" Text="Styles..."/>  <StrRes Name="2387" Text="&a'+
'mp;View"/>  <StrRes Name="2388" Text="Toolbars"/>  <StrRes Name="2389" Text="Sta'+
'ndard"/>  <StrRes Name="2390" Text="Text"/>  <StrRes Name="2391" Text="Frame"/> '+
' <StrRes Name="2392" Text="Alignment Palette"/>  <StrRes Name="2393" Text="Extra'+
' Tools"/>  <StrRes Name="2394" Text="Object Inspector"/>  <StrRes Name="2395" Te'+
'xt="Data Tree"/>  <StrRes Name="2396" Text="Report Tree"/>  <StrRes Name="2397" '+
'Text="Rulers"/>  <StrRes Name="2398" Text="Guides"/>  <StrRes Name="2399" Text="'+
'Delete Guides"/>  <StrRes Name="2400" Text="Options..."/>  <StrRes Name="2401" T'+
'ext="&amp;Help"/>  <StrRes Name="2402" Text="Help Contents..."/>  <StrRes Name="'+
'2403" Text="About FastReport..."/>  <StrRes Name="2404" Text="Tab Order..."/>  <'+
'StrRes Name="2405" Text="Undo"/>  <StrRes Name="2406" Text="Redo"/>  <StrRes Nam'+
'e="2407" Text="Cut"/>  <StrRes Name="2408" Text="Copy"/>  <StrRes Name="2409" Te'+
'xt="Paste"/>  <StrRes Name="2410" Text="Group"/>  <StrRes Name="2411" Text="Ungr'+
'oup"/>  <StrRes Name="2412" Text="Delete"/>  <StrRes Name="2413" Text="Delete Pa'+
'ge"/>  <StrRes Name="2414" Text="Select All"/>  <StrRes Name="2415" Text="Edit..'+
'."/>  <StrRes Name="2416" Text="Bring to Front"/>  <StrRes Name="2417" Text="Sen'+
'd to Back"/>  <StrRes Name="2418" Text="New..."/>  <StrRes Name="2419" Text="New'+
' Report"/>  <StrRes Name="2420" Text="New Page"/>  <StrRes Name="2421" Text="New'+
' Dialog"/>  <StrRes Name="2422" Text="Open..."/>  <StrRes Name="2423" Text="Save'+
'"/>  <StrRes Name="2424" Text="Save As..."/>  <StrRes Name="2425" Text="Variable'+
's..."/>  <StrRes Name="2426" Text="Page Settings..."/>  <StrRes Name="2427" Text'+
'="Preview"/>  <StrRes Name="2428" Text="Exit"/>  <StrRes Name="2429" Text="Repor'+
't Title"/>  <StrRes Name="2430" Text="Report Summary"/>  <StrRes Name="2431" Tex'+
't="Page Header"/>  <StrRes Name="2432" Text="Page Footer"/>  <StrRes Name="2433"'+
' Text="Header"/>  <StrRes Name="2434" Text="Footer"/>  <StrRes Name="2435" Text='+
'"Master Data"/>  <StrRes Name="2436" Text="Detail Data"/>  <StrRes Name="2437" T'+
'ext="Subdetail Data"/>  <StrRes Name="2438" Text="Data 4th level"/>  <StrRes Nam'+
'e="2439" Text="Data 5th level"/>  <StrRes Name="2440" Text="Data 6th level"/>  <'+
'StrRes Name="2441" Text="Group Header"/>  <StrRes Name="2442" Text="Group Footer'+
'"/>  <StrRes Name="2443" Text="Child"/>  <StrRes Name="2444" Text="Column Header'+
'"/>  <StrRes Name="2445" Text="Column Footer"/>  <StrRes Name="2446" Text="Overl'+
'ay"/>  <StrRes Name="2447" Text="Vertical bands"/>  <StrRes Name="2448" Text="He'+
'ader"/>  <StrRes Name="2449" Text="Footer"/>  <StrRes Name="2450" Text="Master D'+
'ata"/>  <StrRes Name="2451" Text="Detail Data"/>  <StrRes Name="2452" Text="Subd'+
'etail Data"/>  <StrRes Name="2453" Text="Group Header"/>  <StrRes Name="2454" Te'+
'xt="Group Footer"/>  <StrRes Name="2455" Text="Child"/>  <StrRes Name="2456" Tex'+
't="0��"/>  <StrRes Name="2457" Text="45��"/>  <StrRes Name="2458" Text="90��"/>  <S'+
'trRes Name="2459" Text="180��"/>  <StrRes Name="2460" Text="270��"/>  <StrRes Name'+
'="2461" Text="Font Settings"/>  <StrRes Name="2462" Text="Bold"/>  <StrRes Name='+
'"2463" Text="Italic"/>  <StrRes Name="2464" Text="Underline"/>  <StrRes Name="24'+
'65" Text="SuperScript"/>  <StrRes Name="2466" Text="SubScript"/>  <StrRes Name="'+
'2467" Text="Condensed"/>  <StrRes Name="2468" Text="Wide"/>  <StrRes Name="2469"'+
' Text="12 cpi"/>  <StrRes Name="2470" Text="15 cpi"/>  <StrRes Name="2471" Text='+
'"Report (*.fr3)|*.fr3"/>  <StrRes Name="2472" Text="Pascal files (*.pas)|*.pas|C'+
'++ files (*.cpp)|*.cpp|JavaScript files (*.js)|*.js|Basic files (*.vb)|*.vb|All '+
'files|*.*"/>  <StrRes Name="2473" Text="Pascal files (*.pas)|*.pas|C++ files (*.'+
'cpp)|*.cpp|JavaScript files (*.js)|*.js|Basic files (*.vb)|*.vb|All files|*.*"/>'+
'  <StrRes Name="2474" Text="Connections..."/>  <StrRes Name="2475" Text="Languag'+
'e"/>  <StrRes Name="2476" Text="Toggle breakpoint"/>  <StrRes Name="2477" Text="'+
'Run to cursor"/>  <StrRes Name="2478" Text="!Add child band"/>  <StrRes Name="24'+
'79" Text="Edit Fill"/>  <StrRes Name="dsCm" Text="Centimeters"/>  <StrRes Name="'+
'dsInch" Text="Inches"/>  <StrRes Name="dsPix" Text="Pixels"/>  <StrRes Name="dsC'+
'hars" Text="Characters"/>  <StrRes Name="dsCode" Text="Code"/>  <StrRes Name="ds'+
'Data" Text="Data"/>  <StrRes Name="dsPage" Text="Page"/>  <StrRes Name="dsRepFil'+
'ter" Text="Report (*.fr3)|*.fr3"/>  <StrRes Name="dsComprRepFilter" Text="Compre'+
'ssed report (*.fr3)|*.fr3"/>  <StrRes Name="dsSavePreviewChanges" Text="Save cha'+
'nges to preview page?"/>  <StrRes Name="dsSaveChangesTo" Text="Save changes to "'+
'/>  <StrRes Name="dsCantLoad" Text="Couldn''t load file"/>  <StrRes Name="dsStyle'+
'File" Text="Style"/>  <StrRes Name="dsCantFindProc" Text="Could not locate the m'+
'ain proc"/>  <StrRes Name="dsClearScript" Text="This will clear all code. Do you'+
' want to continue?"/>  <StrRes Name="dsNoStyle" Text="No style"/>  <StrRes Name='+
'"dsStyleSample" Text="Style sample"/>  <StrRes Name="dsTextNotFound" Text="Text '+
'''%s'' not found"/>  <StrRes Name="dsReplace" Text="Replace this occurence of ''%s'''+
'?"/>  <StrRes Name="2600" Text="About FastReport"/>  <StrRes Name="2601" Text="V'+
'isit our webpage for more info:"/>  <StrRes Name="2602" Text="Sales:"/>  <StrRes'+
' Name="2603" Text="Support:"/>  <StrRes Name="2700" Text="Page Options"/>  <StrR'+
'es Name="2701" Text="Paper"/>  <StrRes Name="2702" Text="Width"/>  <StrRes Name='+
'"2703" Text="Height"/>  <StrRes Name="2704" Text="Size"/>  <StrRes Name="2705" T'+
'ext="Orientation"/>  <StrRes Name="2706" Text="Left"/>  <StrRes Name="2707" Text'+
'="Top"/>  <StrRes Name="2708" Text="Right"/>  <StrRes Name="2709" Text="Bottom"/'+
'>  <StrRes Name="2710" Text="Margins"/>  <StrRes Name="2711" Text="Paper Source"'+
'/>  <StrRes Name="2712" Text="First page"/>  <StrRes Name="2713" Text="Other pag'+
'es"/>  <StrRes Name="2714" Text="Portrait"/>  <StrRes Name="2715" Text="Landscap'+
'e"/>  <StrRes Name="2716" Text="Other options"/>  <StrRes Name="2717" Text="Colu'+
'mns"/>  <StrRes Name="2718" Text="Number"/>  <StrRes Name="2719" Text="Width"/> '+
' <StrRes Name="2720" Text="Positions"/>  <StrRes Name="2721" Text="Other"/>  <St'+
'rRes Name="2722" Text="Duplex"/>  <StrRes Name="2723" Text="Print to previous pa'+
'ge"/>  <StrRes Name="2724" Text="Mirror margins"/>  <StrRes Name="2725" Text="La'+
'rge height in design mode"/>  <StrRes Name="2726" Text="Endless page width"/>  <'+
'StrRes Name="2727" Text="Endless page height"/>  <StrRes Name="2800" Text="Selec'+
't Report Datasets"/>  <StrRes Name="2900" Text="Edit Variables"/>  <StrRes Name='+
'"2901" Text="Category"/>  <StrRes Name="2902" Text="Variable"/>  <StrRes Name="2'+
'903" Text="Edit"/>  <StrRes Name="2904" Text="Delete"/>  <StrRes Name="2905" Tex'+
't="List"/>  <StrRes Name="2906" Text="Load"/>  <StrRes Name="2907" Text="Save"/>'+
'  <StrRes Name="2908" Text="Expression:"/>  <StrRes Name="2909" Text="Dictionary'+
' (*.fd3)|*.fd3"/>  <StrRes Name="2910" Text="Dictionary (*.fd3)|*.fd3"/>  <StrRe'+
's Name="vaNoVar" Text="(no variables defined)"/>  <StrRes Name="vaVar" Text="Var'+
'iables"/>  <StrRes Name="vaDupName" Text="Duplicate name"/>  <StrRes Name="3000"'+
' Text="Designer Options"/>  <StrRes Name="3001" Text="Grid"/>  <StrRes Name="300'+
'2" Text="Type"/>  <StrRes Name="3003" Text="Size"/>  <StrRes Name="3004" Text="D'+
'ialog form:"/>  <StrRes Name="3005" Text="Other"/>  <StrRes Name="3006" Text="Fo'+
'nts"/>  <StrRes Name="3007" Text="Code window"/>  <StrRes Name="3008" Text="Memo'+
' editor"/>  <StrRes Name="3009" Text="Size"/>  <StrRes Name="3010" Text="Size"/>'+
'  <StrRes Name="3011" Text="Colors"/>  <StrRes Name="3012" Text="Gap between ban'+
'ds:"/>  <StrRes Name="3013" Text="cm"/>  <StrRes Name="3014" Text="in"/>  <StrRe'+
's Name="3015" Text="pt"/>  <StrRes Name="3016" Text="pt"/>  <StrRes Name="3017" '+
'Text="pt"/>  <StrRes Name="3018" Text="Centimeters:"/>  <StrRes Name="3019" Text'+
'="Inches:"/>  <StrRes Name="3020" Text="Pixels:"/>  <StrRes Name="3021" Text="Sh'+
'ow grid"/>  <StrRes Name="3022" Text="Align to Grid"/>  <StrRes Name="3023" Text'+
'="Show editor after insert"/>  <StrRes Name="3024" Text="Use object''s font setti'+
'ngs"/>  <StrRes Name="3025" Text="Workspace"/>  <StrRes Name="3026" Text="Tool w'+
'indows"/>  <StrRes Name="3027" Text="LCD grid color"/>  <StrRes Name="3028" Text'+
'="Free bands placement"/>  <StrRes Name="3029" Text="Show drop-down fields list"'+
'/>  <StrRes Name="3030" Text="Show startup screen"/>  <StrRes Name="3031" Text="'+
'Restore defaults"/>  <StrRes Name="3032" Text="Show band captions"/>  <StrRes Na'+
'me="3100" Text="Select DataSet"/>  <StrRes Name="3101" Text="Number of records:"'+
'/>  <StrRes Name="3102" Text="Dataset"/>  <StrRes Name="3103" Text="Filter"/>  <'+
'StrRes Name="dbNotAssigned" Text="[not assigned]"/>  <StrRes Name="3200" Text="G'+
'roup"/>  <StrRes Name="3201" Text="Break on"/>  <StrRes Name="3202" Text="Option'+
's"/>  <StrRes Name="3203" Text="Data field"/>  <StrRes Name="3204" Text="Express'+
'ion"/>  <StrRes Name="3205" Text="Keep group together"/>  <StrRes Name="3206" Te'+
'xt="Start new page"/>  <StrRes Name="3207" Text="Show in outline"/>  <StrRes Nam'+
'e="3300" Text="System Memo"/>  <StrRes Name="3301" Text="Data band"/>  <StrRes N'+
'ame="3302" Text="DataSet"/>  <StrRes Name="3303" Text="DataField"/>  <StrRes Nam'+
'e="3304" Text="Function"/>  <StrRes Name="3305" Text="Expression"/>  <StrRes Nam'+
'e="3306" Text="Aggregate value"/>  <StrRes Name="3307" Text="System variable"/> '+
' <StrRes Name="3308" Text="Count invisible bands"/>  <StrRes Name="3309" Text="T'+
'ext"/>  <StrRes Name="3310" Text="Running total"/>  <StrRes Name="agAggregate" T'+
'ext="Insert Aggregate"/>  <StrRes Name="vt1" Text="[DATE]"/>  <StrRes Name="vt2"'+
' Text="[TIME]"/>  <StrRes Name="vt3" Text="[PAGE#]"/>  <StrRes Name="vt4" Text="'+
'[TOTALPAGES#]"/>  <StrRes Name="vt5" Text="[PAGE#] of [TOTALPAGES#]"/>  <StrRes '+
'Name="vt6" Text="[LINE#]"/>  <StrRes Name="3400" Text="OLE object"/>  <StrRes Na'+
'me="3401" Text="Insert..."/>  <StrRes Name="3402" Text="Edit..."/>  <StrRes Name'+
'="3403" Text="Close"/>  <StrRes Name="olStretched" Text="Stretched"/>  <StrRes N'+
'ame="3500" Text="Barcode Editor"/>  <StrRes Name="3501" Text="Code"/>  <StrRes N'+
'ame="3502" Text="Type of Bar"/>  <StrRes Name="3503" Text="Zoom:"/>  <StrRes Nam'+
'e="3504" Text="Options"/>  <StrRes Name="3505" Text="Rotation"/>  <StrRes Name="'+
'3506" Text="Calc Checksum"/>  <StrRes Name="3507" Text="Text"/>  <StrRes Name="3'+
'508" Text="0��"/>  <StrRes Name="3509" Text="90��"/>  <StrRes Name="3510" Text="18'+
'0��"/>  <StrRes Name="3511" Text="270��"/>  <StrRes Name="bcCalcChecksum" Text="Ca'+
'lc Checksum"/>  <StrRes Name="bcShowText" Text="Show Text"/>  <StrRes Name="3600'+
'" Text="Edit Aliases"/>  <StrRes Name="3601" Text="Press Enter to edit item"/>  '+
'<StrRes Name="3602" Text="Dataset alias"/>  <StrRes Name="3603" Text="Field alia'+
'ses"/>  <StrRes Name="3604" Text="Reset"/>  <StrRes Name="3605" Text="Update"/> '+
' <StrRes Name="alUserName" Text="User name"/>  <StrRes Name="alOriginal" Text="O'+
'riginal name"/>  <StrRes Name="3700" Text="Parameters Editor"/>  <StrRes Name="q'+
'pName" Text="Name"/>  <StrRes Name="qpDataType" Text="Data Type"/>  <StrRes Name'+
'="qpValue" Text="Value"/>  <StrRes Name="3800" Text="Master-Detail Link"/>  <Str'+
'Res Name="3801" Text="Detail fields"/>  <StrRes Name="3802" Text="Master fields"'+
'/>  <StrRes Name="3803" Text="Linked fields"/>  <StrRes Name="3804" Text="Add"/>'+
'  <StrRes Name="3805" Text="Clear"/>  <StrRes Name="3900" Text="Memo"/>  <StrRes'+
' Name="3901" Text="Insert Expression"/>  <StrRes Name="3902" Text="Insert Aggreg'+
'ate"/>  <StrRes Name="3903" Text="Insert Formatting"/>  <StrRes Name="3904" Text'+
'="Word Wrap"/>  <StrRes Name="3905" Text="Text"/>  <StrRes Name="3906" Text="For'+
'mat"/>  <StrRes Name="3907" Text="Highlight"/>  <StrRes Name="4000" Text="Pictur'+
'e"/>  <StrRes Name="4001" Text="Load"/>  <StrRes Name="4002" Text="Copy"/>  <Str'+
'Res Name="4003" Text="Paste"/>  <StrRes Name="4004" Text="Clear"/>  <StrRes Name'+
'="piEmpty" Text="Empty"/>  <StrRes Name="4100" Text="Chart Editor"/>  <StrRes Na'+
'me="4101" Text="Add Series"/>  <StrRes Name="4102" Text="Delete Series"/>  <StrR'+
'es Name="4103" Text="Edit Chart"/>  <StrRes Name="4104" Text="Band source"/>  <S'+
'trRes Name="4105" Text="Fixed data"/>  <StrRes Name="4106" Text="DataSet"/>  <St'+
'rRes Name="4107" Text="Data Source"/>  <StrRes Name="4108" Text="Values"/>  <Str'+
'Res Name="4109" Text="Select the chart series or add a new one."/>  <StrRes Name'+
'="4114" Text="Other options"/>  <StrRes Name="4115" Text="TopN values"/>  <StrRe'+
's Name="4116" Text="TopN caption"/>  <StrRes Name="4117" Text="Sort order"/>  <S'+
'trRes Name="4126" Text="X Axis"/>  <StrRes Name="ch3D" Text="3D View"/>  <StrRes'+
' Name="chAxis" Text="Show Axis"/>  <StrRes Name="chsoNone" Text="None"/>  <StrRe'+
's Name="chsoAscending" Text="Ascending"/>  <StrRes Name="chsoDescending" Text="D'+
'escending"/>  <StrRes Name="chxtText" Text="Text"/>  <StrRes Name="chxtNumber" T'+
'ext="Numeric"/>  <StrRes Name="chxtDate" Text="Date"/>  <StrRes Name="4200" Text'+
'="Rich Editor"/>  <StrRes Name="4201" Text="Open File"/>  <StrRes Name="4202" Te'+
'xt="Save File"/>  <StrRes Name="4203" Text="Undo"/>  <StrRes Name="4204" Text="F'+
'ont"/>  <StrRes Name="4205" Text="Insert Expression"/>  <StrRes Name="4206" Text'+
'="Bold"/>  <StrRes Name="4207" Text="Italic"/>  <StrRes Name="4208" Text="Underl'+
'ine"/>  <StrRes Name="4209" Text="Left Align"/>  <StrRes Name="4210" Text="Cente'+
'r Align"/>  <StrRes Name="4211" Text="Right Align"/>  <StrRes Name="4212" Text="'+
'Justify"/>  <StrRes Name="4213" Text="Bullets"/>  <StrRes Name="4300" Text="Cros'+
's-tab Editor"/>  <StrRes Name="4301" Text="Source data"/>  <StrRes Name="4302" T'+
'ext="Dimensions"/>  <StrRes Name="4303" Text="Rows"/>  <StrRes Name="4304" Text='+
'"Columns"/>  <StrRes Name="4305" Text="Cells"/>  <StrRes Name="4306" Text="Cross'+
'-tab structure"/>  <StrRes Name="4307" Text="Row header"/>  <StrRes Name="4308" '+
'Text="Column header"/>  <StrRes Name="4309" Text="Row grand total"/>  <StrRes Na'+
'me="4310" Text="Column grand total"/>  <StrRes Name="4311" Text="Swap rows/colum'+
'ns"/>  <StrRes Name="4312" Text="!Select style"/>  <StrRes Name="4313" Text="!Sa'+
've current style..."/>  <StrRes Name="4314" Text="!Show title"/>  <StrRes Name="'+
'4315" Text="!Show corner"/>  <StrRes Name="4316" Text="!Reprint headers on new p'+
'age"/>  <StrRes Name="4317" Text="!Auto size"/>  <StrRes Name="4318" Text="!Bord'+
'er around cells"/>  <StrRes Name="4319" Text="!Print down then across"/>  <StrRe'+
's Name="4320" Text="!Side-by-side cells"/>  <StrRes Name="4321" Text="!Join equa'+
'l cells"/>  <StrRes Name="4322" Text="None"/>  <StrRes Name="4323" Text="Sum"/> '+
' <StrRes Name="4324" Text="Min"/>  <StrRes Name="4325" Text="Max"/>  <StrRes Nam'+
'e="4326" Text="Average"/>  <StrRes Name="4327" Text="Count"/>  <StrRes Name="432'+
'8" Text="Ascending (A-Z)"/>  <StrRes Name="4329" Text="Descending (Z-A)"/>  <Str'+
'Res Name="4330" Text="No Sort"/>  <StrRes Name="crStName" Text="!Enter the style'+
' name:"/>  <StrRes Name="crResize" Text="!To resize a cross-tab, set its &#38;#3'+
'4;AutoSize&#38;#34; property to False."/>  <StrRes Name="crSubtotal" Text="Subto'+
'tal"/>  <StrRes Name="crNone" Text="None"/>  <StrRes Name="crSum" Text="Sum"/>  '+
'<StrRes Name="crMin" Text="Min"/>  <StrRes Name="crMax" Text="Max"/>  <StrRes Na'+
'me="crAvg" Text="Avg"/>  <StrRes Name="crCount" Text="Count"/>  <StrRes Name="cr'+
'Asc" Text="A-Z"/>  <StrRes Name="crDesc" Text="Z-A"/>  <StrRes Name="4400" Text='+
'"Expression Editor"/>  <StrRes Name="4401" Text="Expression:"/>  <StrRes Name="4'+
'500" Text="Display Format"/>  <StrRes Name="4501" Text="Category"/>  <StrRes Nam'+
'e="4502" Text="Format"/>  <StrRes Name="4503" Text="Format string:"/>  <StrRes N'+
'ame="4504" Text="Decimal separator:"/>  <StrRes Name="fkText" Text="Text (no for'+
'matting)"/>  <StrRes Name="fkNumber" Text="Number"/>  <StrRes Name="fkDateTime" '+
'Text="Date/Time"/>  <StrRes Name="fkBoolean" Text="Boolean"/>  <StrRes Name="fkN'+
'umber1" Text="1234.5;%g"/>  <StrRes Name="fkNumber2" Text="1234.50;%2.2f"/>  <St'+
'rRes Name="fkNumber3" Text="1,234.50;%2.2n"/>  <StrRes Name="fkNumber4" Text="$1'+
',234.50;%2.2m"/>  <StrRes Name="fkDateTime1" Text="11.28.2002;mm.dd.yyyy"/>  <St'+
'rRes Name="fkDateTime2" Text="28 nov 2002;dd mmm yyyy"/>  <StrRes Name="fkDateTi'+
'me3" Text="November 28, 2002;mmmm dd, yyyy"/>  <StrRes Name="fkDateTime4" Text="'+
'02:14;hh:mm"/>  <StrRes Name="fkDateTime5" Text="02:14am;hh:mm am/pm"/>  <StrRes'+
' Name="fkDateTime6" Text="02:14:00;hh:mm:ss"/>  <StrRes Name="fkDateTime7" Text='+
'"02:14am, November 28, 2002;hh:mm am/pm, mmmm dd, yyyy"/>  <StrRes Name="fkBoole'+
'an1" Text="0,1;0,1"/>  <StrRes Name="fkBoolean2" Text="No,Yes;No,Yes"/>  <StrRes'+
' Name="fkBoolean3" Text="_,x;_,x"/>  <StrRes Name="fkBoolean4" Text="False,True;'+
'False,True"/>  <StrRes Name="4600" Text="Highlight Editor"/>  <StrRes Name="4601'+
'" Text="Conditions"/>  <StrRes Name="4602" Text="Add"/>  <StrRes Name="4603" Tex'+
't="Delete"/>  <StrRes Name="4604" Text="Edit"/>  <StrRes Name="4605" Text="Style'+
'"/>  <StrRes Name="4606" Text="Frame"/>  <StrRes Name="4607" Text="Fill"/>  <Str'+
'Res Name="4608" Text="Font"/>  <StrRes Name="4609" Text="Visible"/>  <StrRes Nam'+
'e="4700" Text="Report Settings"/>  <StrRes Name="4701" Text="General"/>  <StrRes'+
' Name="4702" Text="Printer settings"/>  <StrRes Name="4703" Text="Copies"/>  <St'+
'rRes Name="4704" Text="General"/>  <StrRes Name="4705" Text="Password"/>  <StrRe'+
's Name="4706" Text="Collate copies"/>  <StrRes Name="4707" Text="Double pass"/> '+
' <StrRes Name="4708" Text="Print if empty"/>  <StrRes Name="4709" Text="Descript'+
'ion"/>  <StrRes Name="4710" Text="Name"/>  <StrRes Name="4711" Text="Description'+
'"/>  <StrRes Name="4712" Text="Picture"/>  <StrRes Name="4713" Text="Author"/>  '+
'<StrRes Name="4714" Text="Major"/>  <StrRes Name="4715" Text="Minor"/>  <StrRes '+
'Name="4716" Text="Release"/>  <StrRes Name="4717" Text="Build"/>  <StrRes Name="'+
'4718" Text="Created"/>  <StrRes Name="4719" Text="Modified"/>  <StrRes Name="472'+
'0" Text="Description"/>  <StrRes Name="4721" Text="Version"/>  <StrRes Name="472'+
'2" Text="Browse..."/>  <StrRes Name="4723" Text="Inheritance settings"/>  <StrRe'+
's Name="4724" Text="Select the option:"/>  <StrRes Name="4725" Text="Don''t chang'+
'e"/>  <StrRes Name="4726" Text="Detach the base report"/>  <StrRes Name="4727" T'+
'ext="Inherit from base report:"/>  <StrRes Name="4728" Text="Inheritance"/>  <St'+
'rRes Name="4729" Text="Templates path :"/>  <StrRes Name="rePrnOnPort" Text="on"'+
'/>  <StrRes Name="riNotInherited" Text="This report is not inherited."/>  <StrRe'+
's Name="riInherited" Text="This report is inherited from base report: %s"/>  <St'+
'rRes Name="4800" Text="Lines"/>  <StrRes Name="4900" Text="SQL"/>  <StrRes Name='+
'"4901" Text="Query Builder"/>  <StrRes Name="5000" Text="Password"/>  <StrRes Na'+
'me="5001" Text="Enter the password:"/>  <StrRes Name="5100" Text="Style Editor"/'+
'>  <StrRes Name="5101" Text="Color..."/>  <StrRes Name="5102" Text="Font..."/>  '+
'<StrRes Name="5103" Text="Frame..."/>  <StrRes Name="5104" Text="Add"/>  <StrRes'+
' Name="5105" Text="Delete"/>  <StrRes Name="5106" Text="Edit"/>  <StrRes Name="5'+
'107" Text="Load"/>  <StrRes Name="5108" Text="Save"/>  <StrRes Name="5200" Text='+
'"Frame Editor"/>  <StrRes Name="5201" Text="Frame"/>  <StrRes Name="5202" Text="'+
'Line"/>  <StrRes Name="5203" Text="Shadow"/>  <StrRes Name="5211" Text="Style:"/'+
'>  <StrRes Name="5214" Text="Color:"/>  <StrRes Name="5215" Text="Width:"/>  <St'+
'rRes Name="5216" Text="Choose the line style, then choose the line to apply the '+
'style."/>  <StrRes Name="5300" Text="New Item"/>  <StrRes Name="5301" Text="Item'+
's"/>  <StrRes Name="5302" Text="Templates"/>  <StrRes Name="5303" Text="Inherit '+
'the report"/>  <StrRes Name="5400" Text="Tab Order"/>  <StrRes Name="5401" Text='+
'"Controls listed in tab order:"/>  <StrRes Name="5402" Text="Move Up"/>  <StrRes'+
' Name="5403" Text="Move Down"/>  <StrRes Name="5500" Text="Evaluate"/>  <StrRes '+
'Name="5501" Text="Expression"/>  <StrRes Name="5502" Text="Result"/>  <StrRes Na'+
'me="5600" Text="Report Wizard"/>  <StrRes Name="5601" Text="Data"/>  <StrRes Nam'+
'e="5602" Text="Fields"/>  <StrRes Name="5603" Text="Groups"/>  <StrRes Name="560'+
'4" Text="Layout"/>  <StrRes Name="5605" Text="Style"/>  <StrRes Name="5606" Text'+
'="Step 1. Select the dataset."/>  <StrRes Name="5607" Text="Step 2. Select the f'+
'ields to display."/>  <StrRes Name="5608" Text="Step 3. Create groups (optional)'+
'."/>  <StrRes Name="5609" Text="Step 4. Define the page orientation and data lay'+
'out."/>  <StrRes Name="5610" Text="Step 5. Choose the report style."/>  <StrRes '+
'Name="5611" Text="Add &#62;"/>  <StrRes Name="5612" Text="Add all &#62;&#62;"/> '+
' <StrRes Name="5613" Text="&#60; Remove"/>  <StrRes Name="5614" Text="&#60;&#60;'+
' Remove all"/>  <StrRes Name="5615" Text="Add &#62;"/>  <StrRes Name="5616" Text'+
'="&#60; Remove"/>  <StrRes Name="5617" Text="Selected fields:"/>  <StrRes Name="'+
'5618" Text="Available fields:"/>  <StrRes Name="5619" Text="Groups:"/>  <StrRes '+
'Name="5620" Text="Orientation"/>  <StrRes Name="5621" Text="Layout"/>  <StrRes N'+
'ame="5622" Text="Portrait"/>  <StrRes Name="5623" Text="Landscape"/>  <StrRes Na'+
'me="5624" Text="Tabular"/>  <StrRes Name="5625" Text="Columnar"/>  <StrRes Name='+
'"5626" Text="Fit fields to page width"/>  <StrRes Name="5627" Text="&#60;&#60; B'+
'ack"/>  <StrRes Name="5628" Text="Next &#62;&#62;"/>  <StrRes Name="5629" Text="'+
'Finish"/>  <StrRes Name="5630" Text="New table..."/>  <StrRes Name="5631" Text="'+
'New query..."/>  <StrRes Name="5632" Text="Select database connection:"/>  <StrR'+
'es Name="5633" Text="Select a table:"/>  <StrRes Name="5634" Text="or"/>  <StrRe'+
's Name="5635" Text="Create a query..."/>  <StrRes Name="5636" Text="Configure co'+
'nnections"/>  <StrRes Name="wzStd" Text="Standard Report Wizard"/>  <StrRes Name'+
'="wzDMP" Text="Dot-Matrix Report Wizard"/>  <StrRes Name="wzStdEmpty" Text="Stan'+
'dard Report"/>  <StrRes Name="wzDMPEmpty" Text="Dot-Matrix Report"/>  <StrRes Na'+
'me="5700" Text="Connection Wizard"/>  <StrRes Name="5701" Text="Connection"/>  <'+
'StrRes Name="5702" Text="Choose the connection type:"/>  <StrRes Name="5703" Tex'+
't="Choose the database:"/>  <StrRes Name="5704" Text="Login"/>  <StrRes Name="57'+
'05" Text="Password"/>  <StrRes Name="5706" Text="Prompt login"/>  <StrRes Name="'+
'5707" Text="Use login/password:"/>  <StrRes Name="5708" Text="Table"/>  <StrRes '+
'Name="5709" Text="Choose the table name:"/>  <StrRes Name="5710" Text="Filter re'+
'cords:"/>  <StrRes Name="5711" Text="Query"/>  <StrRes Name="5712" Text="SQL sta'+
'tement:"/>  <StrRes Name="5713" Text="Query Builder"/>  <StrRes Name="5714" Text'+
'="Edit Query Parameters"/>  <StrRes Name="ftAllFiles" Text="All Files"/>  <StrRe'+
's Name="ftPictures" Text="Pictures"/>  <StrRes Name="ftDB" Text="Databases"/>  <'+
'StrRes Name="ftRichFile" Text="RichText file"/>  <StrRes Name="ftTextFile" Text='+
'"Text file"/>  <StrRes Name="prNotAssigned" Text="(Not assigned)"/>  <StrRes Nam'+
'e="prInvProp" Text="Invalid property value"/>  <StrRes Name="prDupl" Text="Dupli'+
'cate name"/>  <StrRes Name="prPict" Text="(Picture)"/>  <StrRes Name="mvExpr" Te'+
'xt="Allow Expressions"/>  <StrRes Name="mvStretch" Text="Stretch"/>  <StrRes Nam'+
'e="mvStretchToMax" Text="Stretch to Max Height"/>  <StrRes Name="mvShift" Text="'+
'Shift"/>  <StrRes Name="mvShiftOver" Text="Shift When Overlapped"/>  <StrRes Nam'+
'e="mvVisible" Text="Visible"/>  <StrRes Name="mvPrintable" Text="Printable"/>  <'+
'StrRes Name="mvFont" Text="Font..."/>  <StrRes Name="mvFormat" Text="Display For'+
'mat..."/>  <StrRes Name="mvClear" Text="Clear Contents"/>  <StrRes Name="mvAutoW'+
'idth" Text="Auto Width"/>  <StrRes Name="mvWWrap" Text="Word Wrap"/>  <StrRes Na'+
'me="mvSuppress" Text="Suppress Repeated Values"/>  <StrRes Name="mvHideZ" Text="'+
'Hide Zeros"/>  <StrRes Name="mvHTML" Text="Allow HTML Tags"/>  <StrRes Name="lvD'+
'iagonal" Text="Diagonal"/>  <StrRes Name="pvAutoSize" Text="Auto Size"/>  <StrRe'+
's Name="pvCenter" Text="Center"/>  <StrRes Name="pvAspect" Text="Keep Aspect Rat'+
'io"/>  <StrRes Name="bvSplit" Text="Allow Split"/>  <StrRes Name="bvKeepChild" T'+
'ext="Keep Child Together"/>  <StrRes Name="bvPrintChild" Text="Print Child If In'+
'visible"/>  <StrRes Name="bvStartPage" Text="Start New Page"/>  <StrRes Name="bv'+
'PrintIfEmpty" Text="Print If Detail Empty"/>  <StrRes Name="bvKeepDetail" Text="'+
'Keep Detail Together"/>  <StrRes Name="bvKeepFooter" Text="Keep Footer Together"'+
'/>  <StrRes Name="bvReprint" Text="Reprint On New Page"/>  <StrRes Name="bvOnFir'+
'st" Text="Print On First Page"/>  <StrRes Name="bvOnLast" Text="Print On Last Pa'+
'ge"/>  <StrRes Name="bvKeepGroup" Text="Keep Together"/>  <StrRes Name="bvFooter'+
'AfterEach" Text="Footer After Each Row"/>  <StrRes Name="bvDrillDown" Text="Dril'+
'l-Down"/>  <StrRes Name="bvResetPageNo" Text="Reset Page Numbers"/>  <StrRes Nam'+
'e="srParent" Text="Print On Parent"/>  <StrRes Name="bvKeepHeader" Text="Keep He'+
'ader Together"/>  <StrRes Name="obCatDraw" Text="Draw"/>  <StrRes Name="obCatOth'+
'er" Text="Other objects"/>  <StrRes Name="obCatOtherControls" Text="Other contro'+
'ls"/>  <StrRes Name="obDiagLine" Text="Diagonal line"/>  <StrRes Name="obRect" T'+
'ext="Rectangle"/>  <StrRes Name="obRoundRect" Text="Rounded rectangle"/>  <StrRe'+
's Name="obEllipse" Text="Ellipse"/>  <StrRes Name="obTrian" Text="Triangle"/>  <'+
'StrRes Name="obDiamond" Text="Diamond"/>  <StrRes Name="obLabel" Text="Label con'+
'trol"/>  <StrRes Name="obEdit" Text="Edit control"/>  <StrRes Name="obMemoC" Tex'+
't="Memo control"/>  <StrRes Name="obButton" Text="Button control"/>  <StrRes Nam'+
'e="obChBoxC" Text="CheckBox control"/>  <StrRes Name="obRButton" Text="RadioButt'+
'on control"/>  <StrRes Name="obLBox" Text="ListBox control"/>  <StrRes Name="obC'+
'Box" Text="ComboBox control"/>  <StrRes Name="obDateEdit" Text="DateEdit control'+
'"/>  <StrRes Name="obImageC" Text="Image control"/>  <StrRes Name="obPanel" Text'+
'="Panel control"/>  <StrRes Name="obGrBox" Text="GroupBox control"/>  <StrRes Na'+
'me="obBBtn" Text="BitBtn control"/>  <StrRes Name="obSBtn" Text="SpeedButton con'+
'trol"/>  <StrRes Name="obMEdit" Text="MaskEdit control"/>  <StrRes Name="obChLB"'+
' Text="CheckListBox control"/>  <StrRes Name="obDBLookup" Text="DBLookupComboBox'+
' control"/>  <StrRes Name="obBevel" Text="Bevel object"/>  <StrRes Name="obShape'+
'" Text="Shape object"/>  <StrRes Name="obText" Text="Text object"/>  <StrRes Nam'+
'e="obSysText" Text="System text"/>  <StrRes Name="obLine" Text="Line object"/>  '+
'<StrRes Name="obPicture" Text="Picture object"/>  <StrRes Name="obBand" Text="Ba'+
'nd object"/>  <StrRes Name="obDataBand" Text="Data band"/>  <StrRes Name="obSubR'+
'ep" Text="Subreport object"/>  <StrRes Name="obDlgPage" Text="Dialog form"/>  <S'+
'trRes Name="obRepPage" Text="Report page"/>  <StrRes Name="obReport" Text="Repor'+
't object"/>  <StrRes Name="obRich" Text="RichText object"/>  <StrRes Name="obOLE'+
'" Text="OLE object"/>  <StrRes Name="obChBox" Text="CheckBox object"/>  <StrRes '+
'Name="obChart" Text="Chart object"/>  <StrRes Name="obBarC" Text="Barcode object'+
'"/>  <StrRes Name="obCross" Text="Cross-tab object"/>  <StrRes Name="obDBCross" '+
'Text="DB Cross-tab object"/>  <StrRes Name="obGrad" Text="Gradient object"/>  <S'+
'trRes Name="obDMPText" Text="Dot-matrix Text object"/>  <StrRes Name="obDMPLine"'+
' Text="Dot-matrix Line object"/>  <StrRes Name="obDMPCmd" Text="Dot-matrix Comma'+
'nd object"/>  <StrRes Name="obBDEDB" Text="BDE Database"/>  <StrRes Name="obBDET'+
'b" Text="BDE Table"/>  <StrRes Name="obBDEQ" Text="BDE Query"/>  <StrRes Name="o'+
'bBDEComps" Text="BDE components"/>  <StrRes Name="obIBXDB" Text="IBX Database"/>'+
'  <StrRes Name="obIBXTb" Text="IBX Table"/>  <StrRes Name="obIBXQ" Text="IBX Que'+
'ry"/>  <StrRes Name="obIBXComps" Text="IBX Components"/>  <StrRes Name="obADODB"'+
' Text="ADO Database"/>  <StrRes Name="obADOTb" Text="ADO Table"/>  <StrRes Name='+
'"obADOQ" Text="ADO Query"/>  <StrRes Name="obADOComps" Text="ADO Components"/>  '+
'<StrRes Name="obDBXDB" Text="DBX Database"/>  <StrRes Name="obDBXTb" Text="DBX T'+
'able"/>  <StrRes Name="obDBXQ" Text="DBX Query"/>  <StrRes Name="obDBXComps" Tex'+
't="DBX Components"/>  <StrRes Name="obFIBDB" Text="FIB Database"/>  <StrRes Name'+
'="obFIBTb" Text="FIB Table"/>  <StrRes Name="obFIBQ" Text="FIB Query"/>  <StrRes'+
' Name="obFIBComps" Text="FIB Components"/>  <StrRes Name="obDataBases" Text="Dat'+
'aBases"/>  <StrRes Name="obTables" Text="Tables"/>  <StrRes Name="obQueries" Tex'+
't="Queries"/>  <StrRes Name="ctString" Text="String"/>  <StrRes Name="ctDate" Te'+
'xt="Date and Time"/>  <StrRes Name="ctConv" Text="Conversions"/>  <StrRes Name="'+
'ctFormat" Text="Formatting"/>  <StrRes Name="ctMath" Text="Mathematical"/>  <Str'+
'Res Name="ctOther" Text="Other"/>  <StrRes Name="IntToStr" Text="Converts an int'+
'eger value to a string"/>  <StrRes Name="FloatToStr" Text="Converts a float valu'+
'e to a string"/>  <StrRes Name="DateToStr" Text="Converts a date to a string"/> '+
' <StrRes Name="TimeToStr" Text="Converts a time to a string"/>  <StrRes Name="Da'+
'teTimeToStr" Text="Converts a date-and-time value to a string"/>  <StrRes Name="'+
'VarToStr" Text="Converts a variant value to a string"/>  <StrRes Name="StrToInt"'+
' Text="Converts a string to an integer value"/>  <StrRes Name="StrToInt64" Text='+
'"Converts a string to an Int64 value"/>  <StrRes Name="StrToFloat" Text="Convert'+
's a string to a floating-point value"/>  <StrRes Name="StrToDate" Text="Converts'+
' a string to a date format"/>  <StrRes Name="StrToTime" Text="Converts a string '+
'to a time format"/>  <StrRes Name="StrToDateTime" Text="Converts a string to a d'+
'ate-and-time format"/>  <StrRes Name="Format" Text="Returns formatted string ass'+
'embled from a series of array arguments"/>  <StrRes Name="FormatFloat" Text="For'+
'mats a floating-point value"/>  <StrRes Name="FormatDateTime" Text="Formats a da'+
'te-and-time value"/>  <StrRes Name="FormatMaskText" Text="Returns a string forma'+
'tted using an edit mask"/>  <StrRes Name="EncodeDate" Text="Returns a TDateTime '+
'type for a specified Year, Month, and Day"/>  <StrRes Name="DecodeDate" Text="Br'+
'eaks TDateTime into Year, Month, and Day values"/>  <StrRes Name="EncodeTime" Te'+
'xt="Returns a TDateTime type for a specified Hour, Min, Sec, and MSec"/>  <StrRe'+
's Name="DecodeTime" Text="Breaks TDateTime into hours, minutes, seconds, and mil'+
'liseconds"/>  <StrRes Name="Date" Text="Returns current date"/>  <StrRes Name="T'+
'ime" Text="Returns current time"/>  <StrRes Name="Now" Text="Return current date'+
' and time"/>  <StrRes Name="DayOfWeek" Text="Returns the day of the week for a s'+
'pecified date"/>  <StrRes Name="IsLeapYear" Text="Indicates whether a specified '+
'year is a leap year"/>  <StrRes Name="DaysInMonth" Text="Returns a number of day'+
's in a specified month"/>  <StrRes Name="Length" Text="Returns a length of a str'+
'ing"/>  <StrRes Name="Copy" Text="Returns a substring of a string"/>  <StrRes Na'+
'me="Pos" Text="Returns a position of a substring in a string"/>  <StrRes Name="D'+
'elete" Text="Removes a substring from a string"/>  <StrRes Name="Insert" Text="I'+
'nserts a substring into a string"/>  <StrRes Name="Uppercase" Text="Converts all'+
' character in a string to upper case"/>  <StrRes Name="Lowercase" Text="Converts'+
' all character in a string to lower case"/>  <StrRes Name="Trim" Text="Trims all'+
' trailing and leading spaces in a string"/>  <StrRes Name="NameCase" Text="Conve'+
'rts first character of a word to upper case"/>  <StrRes Name="CompareText" Text='+
'"Compares two strings"/>  <StrRes Name="Chr" Text="Converts an integer value to '+
'a char"/>  <StrRes Name="Ord" Text="Converts a character value to an integer"/> '+
' <StrRes Name="SetLength" Text="Sets the length of a string"/>  <StrRes Name="Ro'+
'und" Text="Rounds a floating-point value to the nearest whole number"/>  <StrRes'+
' Name="Trunc" Text="Truncates a floating-point value to an integer"/>  <StrRes N'+
'ame="Int" Text="Returns the integer part of a real number"/>  <StrRes Name="Frac'+
'" Text="Returns the fractional part of a real number"/>  <StrRes Name="Sqrt" Tex'+
't="Returns the square root of a specified number"/>  <StrRes Name="Abs" Text="Re'+
'turns an absolute value"/>  <StrRes Name="Sin" Text="Returns the sine of an angl'+
'e (in radians)"/>  <StrRes Name="Cos" Text="Returns the cosine of an angle (in r'+
'adians)"/>  <StrRes Name="ArcTan" Text="Returns the arctangent"/>  <StrRes Name='+
'"Tan" Text="Returns the tangent"/>  <StrRes Name="Exp" Text="Returns the exponen'+
'tial"/>  <StrRes Name="Ln" Text="Returns the natural log of a real expression"/>'+
'  <StrRes Name="Pi" Text="Returns the 3.1415926... number"/>  <StrRes Name="Inc"'+
' Text="Increments a value"/>  <StrRes Name="Dec" Text="Decrements a value"/>  <S'+
'trRes Name="RaiseException" Text="Raises an exception"/>  <StrRes Name="ShowMess'+
'age" Text="Shows a message box"/>  <StrRes Name="Randomize" Text="Starts the ran'+
'dom numbers generator"/>  <StrRes Name="Random" Text="Returns a random number"/>'+
'  <StrRes Name="ValidInt" Text="Returns True if specified string contains a vali'+
'd integer"/>  <StrRes Name="ValidFloat" Text="Returns True if specified string c'+
'ontains a valid float"/>  <StrRes Name="ValidDate" Text="Returns True if specifi'+
'ed string contains a valid date"/>  <StrRes Name="IIF" Text="Returns TrueValue i'+
'f specified Expr is True, otherwise returns FalseValue"/>  <StrRes Name="Get" Te'+
'xt="For internal use only"/>  <StrRes Name="Set" Text="For internal use only"/> '+
' <StrRes Name="InputBox" Text="Displays an input dialog box that enables the use'+
'r to enter a string"/>  <StrRes Name="InputQuery" Text="Displays an input dialog'+
' box that enables the user to enter a string"/>  <StrRes Name="MessageDlg" Text='+
'"Shows a message box"/>  <StrRes Name="CreateOleObject" Text="Creates an OLE obj'+
'ect"/>  <StrRes Name="VarArrayCreate" Text="Creates a variant array"/>  <StrRes '+
'Name="VarType" Text="Return a type of the variant value"/>  <StrRes Name="DayOf"'+
' Text="Returns day number (1..31) of given Date"/>  <StrRes Name="MonthOf" Text='+
'"Returns month number (1..12) of given Date"/>  <StrRes Name="YearOf" Text="Retu'+
'rns year of given Date"/>  <StrRes Name="ctAggregate" Text="Aggregate"/>  <StrRe'+
's Name="Sum" Text="Calculates the sum of the Expr for the Band datarow"/>  <StrR'+
'es Name="Avg" Text="Calculates the average of the Expr for the Band datarow"/>  '+
'<StrRes Name="Min" Text="Calculates the minimum of the Expr for the Band datarow'+
'"/>  <StrRes Name="Max" Text="Calculates the maximum of the Expr for the Band da'+
'tarow"/>  <StrRes Name="Count" Text="Calculates the number of datarows"/>  <StrR'+
'es Name="wzDBConn" Text="New Connection Wizard"/>  <StrRes Name="wzDBTable" Text'+
'="New Table Wizard"/>  <StrRes Name="wzDBQuery" Text="New Query Wizard"/>  <StrR'+
'es Name="5800" Text="Connections"/>  <StrRes Name="5801" Text="New"/>  <StrRes N'+
'ame="5802" Text="Delete"/>  <StrRes Name="cpName" Text="Name"/>  <StrRes Name="c'+
'pConnStr" Text="Connection string"/>  <StrRes Name="startCreateNew" Text="Create'+
' new report"/>  <StrRes Name="startCreateBlank" Text="Create blank report"/>  <S'+
'trRes Name="startOpenReport" Text="Open report"/>  <StrRes Name="startOpenLast" '+
'Text="Open last report"/>  <StrRes Name="startEditAliases" Text="Edit connection'+
' aliases"/>  <StrRes Name="startHelp" Text="Help"/>  <StrRes Name="5900" Text="W'+
'atches"/>  <StrRes Name="5901" Text="Add Watch"/>  <StrRes Name="5902" Text="Del'+
'ete Watch"/>  <StrRes Name="5903" Text="Edit Watch"/>  <StrRes Name="6000" Text='+
'"Inherit error"/>  <StrRes Name="6001" Text="Base and inherited reports have dup'+
'licate objects. What should we do?"/>  <StrRes Name="6002" Text="Delete duplicat'+
'es"/>  <StrRes Name="6003" Text="Rename duplicates"/>  <StrRes Name="6004" Text='+
'"Sort by Name"/>  <StrRes Name="crGroup" Text="Group"/>  <StrRes Name="4331" Tex'+
't="Grouping"/>  <StrRes Name="dsColorOth" Text="Other..."/>  <StrRes Name="6100"'+
' Text="Fill Editor"/>  <StrRes Name="6101" Text="Brush"/>  <StrRes Name="6102" T'+
'ext="Gradient"/>  <StrRes Name="6103" Text="Glass"/>  <StrRes Name="6104" Text="'+
'Brush style:"/>  <StrRes Name="6105" Text="Background color:"/>  <StrRes Name="6'+
'106" Text="Foreground color:"/>  <StrRes Name="6107" Text="Gradient style:"/>  <'+
'StrRes Name="6108" Text="Start color:"/>  <StrRes Name="6109" Text="End color:"/'+
'>  <StrRes Name="6110" Text="Orientation:"/>  <StrRes Name="6111" Text="Color:"/'+
'>  <StrRes Name="6112" Text="Blend:"/>  <StrRes Name="6113" Text="Show hatch"/> '+
' <StrRes Name="6113" Text="Show hatch"/>  <StrRes Name="bsSolid" Text="Solid"/> '+
' <StrRes Name="bsClear" Text="Clear"/>  <StrRes Name="bsHorizontal" Text="Horizo'+
'ntal"/>  <StrRes Name="bsVertical" Text="Vertical"/>  <StrRes Name="bsFDiagonal"'+
' Text="Forward Diagonal"/>  <StrRes Name="bsBDiagonal" Text="Backward Diagonal"/'+
'>  <StrRes Name="bsCross" Text="Cross"/>  <StrRes Name="bsDiagCross" Text="Diago'+
'nal cross"/>  <StrRes Name="gsHorizontal" Text="Horizontal"/>  <StrRes Name="gsV'+
'ertical" Text="Vertical"/>  <StrRes Name="gsElliptic" Text="Elliptic"/>  <StrRes'+
' Name="gsRectangle" Text="Rectangle"/>  <StrRes Name="gsVertCenter" Text="Vertic'+
'al Center"/>  <StrRes Name="gsHorizCenter" Text="Horizontal Center"/>  <StrRes N'+
'ame="foVertical" Text="Vertical"/>  <StrRes Name="foHorizontal" Text="Horizontal'+
'"/>  <StrRes Name="foVerticalMirror" Text="Vertical Mirror"/>  <StrRes Name="foH'+
'orizontalMirror" Text="Horizontal Mirror"/>  <StrRes Name="6200" Text="Hyperlink'+
' Editor"/>  <StrRes Name="6201" Text="Hyperlink kind"/>  <StrRes Name="6202" Tex'+
't="URL"/>  <StrRes Name="6203" Text="Page number"/>  <StrRes Name="6204" Text="A'+
'nchor"/>  <StrRes Name="6205" Text="Report"/>  <StrRes Name="6206" Text="Report '+
'page"/>  <StrRes Name="6207" Text="Custom"/>  <StrRes Name="6208" Text="Properti'+
'es"/>  <StrRes Name="6209" Text="Specify an URL (for example: http://www.url.com'+
'):"/>  <StrRes Name="6210" Text="or enter the expression that returns an URL:"/>'+
'  <StrRes Name="6211" Text="Specify a page number:"/>  <StrRes Name="6212" Text='+
'"or enter the expression that returns a page number:"/>  <StrRes Name="6213" Tex'+
't="Specify an anchor name:"/>  <StrRes Name="6214" Text="or enter the expression'+
' that returns an anchor name:"/>  <StrRes Name="6215" Text="Report name:"/>  <St'+
'rRes Name="6216" Text="Report variable:"/>  <StrRes Name="6217" Text="Specify a '+
'variable value:"/>  <StrRes Name="6218" Text="or enter the expression that retur'+
'ns a variable value:"/>  <StrRes Name="6219" Text="Report page:"/>  <StrRes Name'+
'="6220" Text="Specify a hyperlink value:"/>  <StrRes Name="6221" Text="or enter '+
'the expression that returns a hyperlink value:"/>  <StrRes Name="6222" Text="Wha'+
't will happen if you click this object in the preview window:"/>  <StrRes Name="'+
'6223" Text="Specified URL will be opened."/>  <StrRes Name="6224" Text="You will'+
' go to the specified page."/>  <StrRes Name="6225" Text="You will go to the obje'+
'ct that contains specified anchor."/>  <StrRes Name="6226" Text="Specified repor'+
't will be generated and opened in a separate preview tab."/>  <StrRes Name="6227'+
'" Text="Specified page will be generated and opened in a separate preview tab."/'+
'>  <StrRes Name="6228" Text="You should create OnClick event handler to define a'+
' custom action."/>  <StrRes Name="6229" Text="Modify the object''s appearance so '+
'it will look like a clickable link"/>  <StrRes Name="mvHyperlink" Text="Hyperlin'+
'k..."/></Resources>'+
'';
initialization
    frxResources.AddXML(Utf8Encode(resXML));

end.
