﻿ /* (1533) */ // CodeGear C++Builder
 /* (1534) */ // Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
 /* (1535) */ // All rights reserved
 /* (1540) */ 
// (DO NOT EDIT: machine generated header) 'frx20.dpk' rev: 27.00 (Windows)
 /* (1542) */ 
#ifndef Frx20HPP
 /* (1543) */ #define Frx20HPP
 /* (1545) */ 
#pragma delphiheader begin
 /* (1547) */ #pragma option push
 /* (1558) */ #pragma option -w-      // All warnings off
 /* (1560) */ #pragma option -Vx      // Zero-length empty class member 
 /* (1561) */ #pragma pack(push,8)
 /* (1110) */ #include <System.hpp>	// Pascal unit
 /* (1110) */ #include <SysInit.hpp>	// Pascal unit
 /* (1110) */ #include <frxAggregate.hpp>	// Pascal unit
 /* (1110) */ #include <frxChm.hpp>	// Pascal unit
 /* (1110) */ #include <frxClass.hpp>	// Pascal unit
 /* (1110) */ #include <frxClassRTTI.hpp>	// Pascal unit
 /* (1110) */ #include <frxCtrls.hpp>	// Pascal unit
 /* (1110) */ #include <frxDialogForm.hpp>	// Pascal unit
 /* (1110) */ #include <frxDMPClass.hpp>	// Pascal unit
 /* (1110) */ #include <frxDMPExport.hpp>	// Pascal unit
 /* (1110) */ #include <frxDock.hpp>	// Pascal unit
 /* (1110) */ #include <frxEngine.hpp>	// Pascal unit
 /* (1110) */ #include <frxGraphicUtils.hpp>	// Pascal unit
 /* (1110) */ #include <frxPassw.hpp>	// Pascal unit
 /* (1110) */ #include <frxPictureCache.hpp>	// Pascal unit
 /* (1110) */ #include <frxPreview.hpp>	// Pascal unit
 /* (1110) */ #include <frxPreviewPages.hpp>	// Pascal unit
 /* (1110) */ #include <frxPreviewPageSettings.hpp>	// Pascal unit
 /* (1110) */ #include <frxPrintDialog.hpp>	// Pascal unit
 /* (1110) */ #include <frxPrinter.hpp>	// Pascal unit
 /* (1110) */ #include <frxProgress.hpp>	// Pascal unit
 /* (1110) */ #include <frxrcClass.hpp>	// Pascal unit
 /* (1110) */ #include <frxRes.hpp>	// Pascal unit
 /* (1110) */ #include <frxSearchDialog.hpp>	// Pascal unit
 /* (1110) */ #include <frxUnicodeUtils.hpp>	// Pascal unit
 /* (1110) */ #include <frxUtils.hpp>	// Pascal unit
 /* (1110) */ #include <frxVariables.hpp>	// Pascal unit
 /* (1110) */ #include <frxXML.hpp>	// Pascal unit
 /* (1110) */ #include <frxXMLSerializer.hpp>	// Pascal unit
 /* (1110) */ #include <frxmd5.hpp>	// Pascal unit
 /* (1110) */ #include <frxAbout.hpp>	// Pascal unit
 /* (1110) */ #include <frxCodeUtils.hpp>	// Pascal unit
 /* (1110) */ #include <frxConnEditor.hpp>	// Pascal unit
 /* (1110) */ #include <frxConnItemEdit.hpp>	// Pascal unit
 /* (1110) */ #include <frxCustomEditors.hpp>	// Pascal unit
 /* (1110) */ #include <frxDataTree.hpp>	// Pascal unit
 /* (1110) */ #include <frxDesgn.hpp>	// Pascal unit
 /* (1110) */ #include <frxDesgnCtrls.hpp>	// Pascal unit
 /* (1110) */ #include <frxDesgnEditors.hpp>	// Pascal unit
 /* (1110) */ #include <frxDesgnWorkspace.hpp>	// Pascal unit
 /* (1110) */ #include <frxDesgnWorkspace1.hpp>	// Pascal unit
 /* (1110) */ #include <frxDsgnIntf.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditAliases.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditDataBand.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditExpr.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditFormat.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditFrame.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditGroup.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditHighlight.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditMemo.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditOptions.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditPage.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditPicture.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditReport.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditReportData.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditStrings.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditStyle.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditSysMemo.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditTabOrder.hpp>	// Pascal unit
 /* (1110) */ #include <frxEditVar.hpp>	// Pascal unit
 /* (1110) */ #include <frxEvaluateForm.hpp>	// Pascal unit
 /* (1110) */ #include <frxInheritError.hpp>	// Pascal unit
 /* (1110) */ #include <frxInsp.hpp>	// Pascal unit
 /* (1110) */ #include <frxNewItem.hpp>	// Pascal unit
 /* (1110) */ #include <frxPopupForm.hpp>	// Pascal unit
 /* (1110) */ #include <frxrcDesgn.hpp>	// Pascal unit
 /* (1110) */ #include <frxrcInsp.hpp>	// Pascal unit
 /* (1110) */ #include <frxReportTree.hpp>	// Pascal unit
 /* (1110) */ #include <frxStdWizard.hpp>	// Pascal unit
 /* (1110) */ #include <frxSynMemo.hpp>	// Pascal unit
 /* (1110) */ #include <frxUnicodeCtrls.hpp>	// Pascal unit
 /* (1110) */ #include <frxWatchForm.hpp>	// Pascal unit
 /* (1110) */ #include <frxBarcod.hpp>	// Pascal unit
 /* (1110) */ #include <frxBarcode.hpp>	// Pascal unit
 /* (1110) */ #include <frxBarcodeEditor.hpp>	// Pascal unit
 /* (1110) */ #include <frxBarcodeRTTI.hpp>	// Pascal unit
 /* (1110) */ #include <frxChBox.hpp>	// Pascal unit
 /* (1110) */ #include <frxChBoxRTTI.hpp>	// Pascal unit
 /* (1110) */ #include <frxCross.hpp>	// Pascal unit
 /* (1110) */ #include <frxCrossEditor.hpp>	// Pascal unit
 /* (1110) */ #include <frxCrossRTTI.hpp>	// Pascal unit
 /* (1110) */ #include <frxGradient.hpp>	// Pascal unit
 /* (1110) */ #include <frxGradientRTTI.hpp>	// Pascal unit
 /* (1110) */ #include <frxOLE.hpp>	// Pascal unit
 /* (1110) */ #include <frxOLEEditor.hpp>	// Pascal unit
 /* (1110) */ #include <frxOLERTTI.hpp>	// Pascal unit
 /* (1110) */ #include <frxRich.hpp>	// Pascal unit
 /* (1110) */ #include <frxRichEdit.hpp>	// Pascal unit
 /* (1110) */ #include <frxRichEditor.hpp>	// Pascal unit
 /* (1110) */ #include <frxRichRTTI.hpp>	// Pascal unit
 /* (1110) */ #include <frxGZip.hpp>	// Pascal unit
 /* (1110) */ #include <frxZLib.hpp>	// Pascal unit
 /* (1110) */ #include <frxCrypt.hpp>	// Pascal unit
 /* (1110) */ #include <rc_AlgRef.hpp>	// Pascal unit
 /* (1110) */ #include <rc_ApiRef.hpp>	// Pascal unit
 /* (1110) */ #include <rc_Crypt.hpp>	// Pascal unit
 /* (1110) */ #include <Winapi.Windows.hpp>	// Pascal unit
 /* (1110) */ #include <System.Internal.ExcUtils.hpp>	// Pascal unit
 /* (1110) */ #include <System.SysUtils.hpp>	// Pascal unit
 /* (1110) */ #include <System.VarUtils.hpp>	// Pascal unit
 /* (1110) */ #include <System.Variants.hpp>	// Pascal unit
 /* (1110) */ #include <System.AnsiStrings.hpp>	// Pascal unit
 /* (1110) */ #include <System.Math.hpp>	// Pascal unit
 /* (1110) */ #include <System.TimeSpan.hpp>	// Pascal unit
 /* (1110) */ #include <System.SyncObjs.hpp>	// Pascal unit
 /* (1110) */ #include <System.Generics.Defaults.hpp>	// Pascal unit
 /* (1110) */ #include <System.Rtti.hpp>	// Pascal unit
 /* (1110) */ #include <System.TypInfo.hpp>	// Pascal unit
 /* (1110) */ #include <System.Classes.hpp>	// Pascal unit
 /* (1110) */ #include <Winapi.ShellAPI.hpp>	// Pascal unit
 /* (1110) */ #include <System.DateUtils.hpp>	// Pascal unit
 /* (1110) */ #include <System.IOUtils.hpp>	// Pascal unit
 /* (1110) */ #include <System.Win.Registry.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Graphics.hpp>	// Pascal unit
 /* (1110) */ #include <System.Actions.hpp>	// Pascal unit
 /* (1110) */ #include <Winapi.UxTheme.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.ActnList.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.GraphUtil.hpp>	// Pascal unit
 /* (1110) */ #include <System.Win.ComObj.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Controls.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.StdCtrls.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.ExtCtrls.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Themes.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Menus.hpp>	// Pascal unit
 /* (1110) */ #include <System.HelpIntfs.hpp>	// Pascal unit
 /* (1110) */ #include <Winapi.FlatSB.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Clipbrd.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.ComCtrls.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Forms.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Printers.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Dialogs.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Buttons.hpp>	// Pascal unit
 /* (1110) */ #include <fs_iconst.hpp>	// Pascal unit
 /* (1110) */ #include <fs_itools.hpp>	// Pascal unit
 /* (1110) */ #include <fs_iinterpreter.hpp>	// Pascal unit
 /* (1110) */ #include <fs_iclassesrtti.hpp>	// Pascal unit
 /* (1110) */ #include <fs_igraphicsrtti.hpp>	// Pascal unit
 /* (1110) */ #include <fs_iformsrtti.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Imaging.jpeg.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Imaging.pngimage.hpp>	// Pascal unit
 /* (1110) */ #include <fs_ipascal.hpp>	// Pascal unit
 /* (1110) */ #include <fs_icpp.hpp>	// Pascal unit
 /* (1110) */ #include <fs_ibasic.hpp>	// Pascal unit
 /* (1110) */ #include <fs_ijs.hpp>	// Pascal unit
 /* (1110) */ #include <fs_idialogsrtti.hpp>	// Pascal unit
 /* (1110) */ #include <fs_iinirtti.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.OleCtnrs.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.CheckLst.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

 /* (1266) */ namespace Frx20
 /* (1267) */ {
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
 /* (1272) */ }	/* namespace Frx20 */
 /* (1293) */ #if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_FRX20)
 /* (1294) */ using namespace Frx20;
 /* (1295) */ #endif
 /* (1615) */ #pragma pack(pop)
 /* (1618) */ #pragma option pop
 /* (1619) */ 
#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
 /* (1622) */ #endif	// Frx20HPP
