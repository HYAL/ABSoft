﻿ /* (1533) */ // CodeGear C++Builder
 /* (1534) */ // Copyright (c) 1995, 2014 by Embarcadero Technologies, Inc.
 /* (1535) */ // All rights reserved
 /* (1540) */ 
// (DO NOT EDIT: machine generated header) 'fs20.dpk' rev: 27.00 (Windows)
 /* (1542) */ 
#ifndef Fs20HPP
 /* (1543) */ #define Fs20HPP
 /* (1545) */ 
#pragma delphiheader begin
 /* (1547) */ #pragma option push
 /* (1558) */ #pragma option -w-      // All warnings off
 /* (1560) */ #pragma option -Vx      // Zero-length empty class member 
 /* (1561) */ #pragma pack(push,8)
 /* (1110) */ #include <System.hpp>	// Pascal unit
 /* (1110) */ #include <SysInit.hpp>	// Pascal unit
 /* (1110) */ #include <fs_ipascal.hpp>	// Pascal unit
 /* (1110) */ #include <fs_icpp.hpp>	// Pascal unit
 /* (1110) */ #include <fs_ijs.hpp>	// Pascal unit
 /* (1110) */ #include <fs_ibasic.hpp>	// Pascal unit
 /* (1110) */ #include <fs_iclassesrtti.hpp>	// Pascal unit
 /* (1110) */ #include <fs_iconst.hpp>	// Pascal unit
 /* (1110) */ #include <fs_idialogsrtti.hpp>	// Pascal unit
 /* (1110) */ #include <fs_idisp.hpp>	// Pascal unit
 /* (1110) */ #include <fs_ievents.hpp>	// Pascal unit
 /* (1110) */ #include <fs_iexpression.hpp>	// Pascal unit
 /* (1110) */ #include <fs_iextctrlsrtti.hpp>	// Pascal unit
 /* (1110) */ #include <fs_iformsrtti.hpp>	// Pascal unit
 /* (1110) */ #include <fs_igraphicsrtti.hpp>	// Pascal unit
 /* (1110) */ #include <fs_iilparser.hpp>	// Pascal unit
 /* (1110) */ #include <fs_iinirtti.hpp>	// Pascal unit
 /* (1110) */ #include <fs_iinterpreter.hpp>	// Pascal unit
 /* (1110) */ #include <fs_iparser.hpp>	// Pascal unit
 /* (1110) */ #include <fs_isysrtti.hpp>	// Pascal unit
 /* (1110) */ #include <fs_imenusrtti.hpp>	// Pascal unit
 /* (1110) */ #include <fs_itools.hpp>	// Pascal unit
 /* (1110) */ #include <fs_xml.hpp>	// Pascal unit
 /* (1110) */ #include <fs_synmemo.hpp>	// Pascal unit
 /* (1110) */ #include <fs_tree.hpp>	// Pascal unit
 /* (1110) */ #include <Winapi.Windows.hpp>	// Pascal unit
 /* (1110) */ #include <System.Internal.ExcUtils.hpp>	// Pascal unit
 /* (1110) */ #include <System.SysUtils.hpp>	// Pascal unit
 /* (1110) */ #include <System.VarUtils.hpp>	// Pascal unit
 /* (1110) */ #include <System.Variants.hpp>	// Pascal unit
 /* (1110) */ #include <System.AnsiStrings.hpp>	// Pascal unit
 /* (1110) */ #include <System.Math.hpp>	// Pascal unit
 /* (1110) */ #include <System.TimeSpan.hpp>	// Pascal unit
 /* (1110) */ #include <System.SyncObjs.hpp>	// Pascal unit
 /* (1110) */ #include <System.Generics.Defaults.hpp>	// Pascal unit
 /* (1110) */ #include <System.Rtti.hpp>	// Pascal unit
 /* (1110) */ #include <System.TypInfo.hpp>	// Pascal unit
 /* (1110) */ #include <System.Classes.hpp>	// Pascal unit
 /* (1110) */ #include <Winapi.ShellAPI.hpp>	// Pascal unit
 /* (1110) */ #include <System.DateUtils.hpp>	// Pascal unit
 /* (1110) */ #include <System.IOUtils.hpp>	// Pascal unit
 /* (1110) */ #include <System.Win.Registry.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Graphics.hpp>	// Pascal unit
 /* (1110) */ #include <System.Actions.hpp>	// Pascal unit
 /* (1110) */ #include <Winapi.UxTheme.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.ActnList.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.GraphUtil.hpp>	// Pascal unit
 /* (1110) */ #include <System.Win.ComObj.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Controls.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.StdCtrls.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.ExtCtrls.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Themes.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Menus.hpp>	// Pascal unit
 /* (1110) */ #include <System.HelpIntfs.hpp>	// Pascal unit
 /* (1110) */ #include <Winapi.FlatSB.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Clipbrd.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.ComCtrls.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Forms.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Printers.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Dialogs.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.Buttons.hpp>	// Pascal unit
 /* (1110) */ #include <Vcl.CheckLst.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

 /* (1266) */ namespace Fs20
 /* (1267) */ {
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
 /* (1272) */ }	/* namespace Fs20 */
 /* (1293) */ #if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_FS20)
 /* (1294) */ using namespace Fs20;
 /* (1295) */ #endif
 /* (1615) */ #pragma pack(pop)
 /* (1618) */ #pragma option pop
 /* (1619) */ 
#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
 /* (1622) */ #endif	// Fs20HPP
