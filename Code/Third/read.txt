1.需要修改的三方单元
unit dxSkinsForm;
function dxSkinGetControllerClassForWindow(AWnd: HWND): TdxSkinWinControllerClass;
     {
      if AControl.ClassType = TPanel then
        Result := TdxSkinPanelController;
      Edit By Grj 2009-4-17
      解决应用皮肤时以下三类的空白部分是白色,而不是皮肤背景的问题
      dxSkinController1有皮肤打开时,FastReport不能看到设计界面,关闭皮肤就正常了
      所以 排队AControl.Owner.ClassName='TfrxDesignerForm',以便dxSkinController1有皮肤打开时,也能看到FastReport的设计界面

      if (AControl is TCustomPanel) then
      begin
        if (Assigned(AControl.Owner)) and
           (AControl.Owner.ClassName='TfrxDesignerForm') then
        begin

        end
        else
          Result := TdxSkinPanelController;
      end;
      }

//FastReport新版本中已修改了这个问题，所以不用再更改了
unit frxDBSet;
function TfrxDBDataset.GetDisplayText(Index: String): WideString;

      (*
{$IFDEF Delphi5}
        if TField(Fields.Objects[i]) is TWideStringField then
          s := VarToWideStr(TField(Fields.Objects[i]).Value)
        else
{$ENDIF}
        *)
         //edit by grj 2010 01-17  解决显示与保存是不同字段时,显示的不是text问题
         s := TField(Fields.Objects[i]).DisplayText;

2.编译三方包时需要在Tools下Option菜单开窗后在Library Path加入的路径
  编译完成后为人提高速度，再将这些路径删除
D:\ABSoft\Code\Third\FastReport\FastScript;D:\ABSoft\Code\Third\FastReport\FastQB;D:\ABSoft\Code\Third\FastReport\Source\ADO;D:\ABSoft\Code\Third\FastReport\Source\DBX;D:\ABSoft\Code\Third\FastReport\Source\ExportPack;D:\ABSoft\Code\Third\FastReport\Source;D:\ABSoft\Code\Third\DevExpressVCL\ExpressCore Library\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressCore Library\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressBars\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressBars\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressCommon Library\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressCommon Library\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressDataController\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressDataController\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressDBTree Suite\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressDBTree Suite\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressDocking Library\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressDocking Library\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressEditors Library\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressEditors Library\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressExport Library\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressFlowChart\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressFlowChart\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressGDI+ Library\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressGDI+ Library\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressLayout Control\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressLayout Control\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressLibrary\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressLibrary\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressMemData\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressExport Library\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressMemData\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressNavBar\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressNavBar\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressOrgChart\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressOrgChart\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressPageControl\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressPageControl\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressPivotGrid\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressPivotGrid\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressPrinting System\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressPrinting System\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressQuantumGrid\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressQuantumGrid\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressQuantumTreeList\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressQuantumTreeList\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressScheduler\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressScheduler\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressSkins Library\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressSkins Library\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressSpreadSheet\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressSpreadSheet\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressSpreadSheet (deprecated)\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressVerticalGrid\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressVerticalGrid\Sources;D:\ABSoft\Code\Third\DevExpressVCL\XP Theme Manager\Packages;D:\ABSoft\Code\Third\DevExpressVCL\XP Theme Manager\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressSpellChecker\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressSpellChecker\Sources;D:\ABSoft\Code\Third\DevExpressVCL\ExpressTile Control\Packages;D:\ABSoft\Code\Third\DevExpressVCL\ExpressTile Control\Sources