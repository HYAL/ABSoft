inherited cxGridWizardLayoutViewOptionsInterfaceElementsPageFrame: TcxGridWizardLayoutViewOptionsInterfaceElementsPageFrame
  inherited lcMain: TdxLayoutControl
    object pnPreviewGrid: TPanel [0]
      Left = 10
      Top = 10
      Width = 345
      Height = 430
      BevelOuter = bvNone
      Caption = 'pnPreviewGrid'
      TabOrder = 0
    end
    object chbRecordCaption: TcxCheckBox [1]
      Left = 361
      Top = 10
      Caption = 'Record caption'
      Properties.OnChange = RefreshPreviewGrid
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 1
      Transparent = True
      Width = 248
    end
    object chbNavigator: TcxCheckBox [2]
      Left = 361
      Top = 33
      Caption = 'Navigator'
      Properties.OnChange = RefreshPreviewGrid
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 2
      Transparent = True
      Width = 248
    end
    inherited lcMainGroup_Root: TdxLayoutGroup
      LayoutDirection = ldHorizontal
      Index = -1
    end
    object lciPreviewGrid: TdxLayoutItem
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'Panel1'
      CaptionOptions.Visible = False
      Parent = lcMainGroup_Root
      Control = pnPreviewGrid
      ControlOptions.AutoColor = True
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciRecordCaption: TdxLayoutItem
      CaptionOptions.Text = 'cxCheckBox1'
      CaptionOptions.Visible = False
      Parent = lcMainGroup1
      Control = chbRecordCaption
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object lciNavigator: TdxLayoutItem
      CaptionOptions.Text = 'cxCheckBox3'
      CaptionOptions.Visible = False
      Parent = lcMainGroup1
      Control = chbNavigator
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object lcMainGroup1: TdxLayoutGroup
      CaptionOptions.Text = 'Hidden Group'
      Parent = lcMainGroup_Root
      SizeOptions.Width = 269
      ButtonOptions.Buttons = <>
      Hidden = True
      ShowBorder = False
      Index = 1
    end
  end
end
