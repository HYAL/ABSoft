{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressBars components                                   }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSBARS AND ALL ACCOMPANYING VCL  }
{   CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.                  }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxBarBuiltInMenu;

{$I cxVer.inc}

interface

uses
  Windows, Menus, Classes, Graphics, ImgList, Contnrs, dxCore, cxLookAndFeels, dxBar, dxBuiltInPopupMenu, cxGraphics;

type
  TdxBarBuiltInMenuItemType = (bmitDefault, bmitChecked, bmitSubItem);

  { TdxBarBuiltInMenuHelper }

  TdxBarBuiltInMenuHelper = class
  private
    FBarManager: TdxBarManager;
    FItems: TObjectList;
    FPopupMenu: TdxBarPopupMenu;

    function GetImages: TCustomImageList;
    function GetLookAndFeel: TcxLookAndFeel;
    procedure SetImages(const Value: TCustomImageList);
    procedure SetLookAndFeel(const Value: TcxLookAndFeel);
  public
    constructor Create;
    destructor Destroy; override;
    function CreateMenuItem(AOwner: TComponent; const ACaption: string; ACommand: Integer; AEnabled: Boolean = True;
      AItemType: TdxBarBuiltInMenuItemType = bmitDefault; AChecked: Boolean = False; AImageIndex: Integer = -1;
      AWithSeparator: Boolean = False; AGlyph: TBitmap = nil; AClickEvent: TNotifyEvent = nil): TComponent;
    procedure CreatePopupMenu;
    procedure DestroyPopupMenu;

    property BarManager: TdxBarManager read FBarManager;
    property LookAndFeel: TcxLookAndFeel read GetLookAndFeel write SetLookAndFeel;
    property PopupMenu: TdxBarPopupMenu read FPopupMenu;
    property Images: TCustomImageList read GetImages write SetImages;
  end;

  { TdxBarBuiltInPopupMenuAdapter }

  TdxBarBuiltInPopupMenuAdapter = class(TdxCustomBuiltInPopupMenuAdapter)
  private
    FSeparatorRequired: Boolean;
  protected
    function GetPopupMenu: TComponent; override;
  public
    function Add(const ACaption: string; AClickEvent: TNotifyEvent; ATag: TdxNativeInt = 0;
      AImageIndex: TcxImageIndex = -1; AEnabled: Boolean = True): TComponent; override;
    procedure AddSeparator; override;
    procedure Clear; override;
    procedure SetChecked(AItem: TComponent; AValue: Boolean); override;
    procedure SetDefault(AItem: TComponent; AValue: Boolean); override;
    procedure SetImages(AImages: TCustomImageList); override;
    procedure SetLookAndFeel(AValue: TcxLookAndFeel); override;
  end;

function dxBarPopupHelper: TdxBarBuiltInMenuHelper;
implementation

uses
  SysUtils, Controls, Forms;

var
  FFakeForm: TCustomForm;
  FFakeBarManager: TdxBarManager;
  FBarPopupHelper: TdxBarBuiltInMenuHelper;

function GetFakeForm: TCustomForm;
begin
  if FFakeForm = nil then
    FFakeForm := TCustomForm.CreateNew(nil);
  Result := FFakeForm;
end;

function GetFakeBarManager: TdxBarManager;
begin
  if FFakeBarManager = nil then
    FFakeBarManager := TdxBarManager.Create(GetFakeForm);
  Result := FFakeBarManager;
end;

function dxBarPopupHelper: TdxBarBuiltInMenuHelper;
begin
  if FBarPopupHelper = nil then
    FBarPopupHelper := TdxBarBuiltInMenuHelper.Create;
  Result := FBarPopupHelper;
end;

{ TdxBarBuiltInMenuHelper }

constructor TdxBarBuiltInMenuHelper.Create;
begin
  inherited Create;
  FBarManager := GetFakeBarManager;
  FBarManager.ImageOptions.StretchGlyphs := False;
  FBarManager.Style := bmsUseLookAndFeel;
end;

destructor TdxBarBuiltInMenuHelper.Destroy;
begin
  DestroyPopupMenu;
  inherited Destroy;
end;

function TdxBarBuiltInMenuHelper.CreateMenuItem(AOwner: TComponent; const ACaption: string; ACommand: Integer;
  AEnabled: Boolean = True; AItemType: TdxBarBuiltInMenuItemType = bmitDefault; AChecked: Boolean = False;
  AImageIndex: Integer = -1; AWithSeparator: Boolean = False; AGlyph: TBitmap = nil; AClickEvent: TNotifyEvent = nil): TComponent;
const
  ItemClassMap: array [Boolean] of TdxBarItemClass = (TdxBarButton, TdxBarSubItem);
var
  ALinksOwner: IdxBarLinksOwner;
  ALink: TdxBarItemLink;
begin
  if Supports(AOwner, IdxBarLinksOwner, ALinksOwner) and (ALinksOwner.GetItemLinks <> nil) then
  begin
    ALink := ALinksOwner.GetItemLinks.AddItem(ItemClassMap[AItemType = bmitSubItem]);
    ALink.BeginGroup := AWithSeparator;
    Result := Alink.Item;
    BarDesignController.AddInternalItem(TdxBarItem(Result), FItems);
    TdxBarItem(Result).Enabled := AEnabled;
    TdxBarItem(Result).Caption := ACaption;
    TdxBarItem(Result).Tag := ACommand;
    TdxBarItem(Result).OnClick := AClickEvent;
    TdxBarItem(Result).ImageIndex := AImageIndex;
    TdxBarItem(Result).Glyph := AGlyph;

    if AItemType <> bmitSubItem then
    begin
      if AItemType = bmitChecked then
        TdxBarButton(Result).ButtonStyle := bsChecked;
      TdxBarButton(Result).Down := AChecked;
    end;
  end
  else
    Result := nil;
end;

procedure TdxBarBuiltInMenuHelper.CreatePopupMenu;
begin
  DestroyPopupMenu;
  FPopupMenu := TdxBarPopupMenu.Create(nil);
  FPopupMenu.BarManager := FBarManager;
  FItems := TObjectList.Create;
end;

procedure TdxBarBuiltInMenuHelper.DestroyPopupMenu;
begin
  FreeAndNil(FItems);
  FreeAndNil(FPopupMenu);
end;

function TdxBarBuiltInMenuHelper.GetImages: TCustomImageList;
begin
  Result := FBarManager.Images;
end;

function TdxBarBuiltInMenuHelper.GetLookAndFeel: TcxLookAndFeel;
begin
  Result := FBarManager.LookAndFeel;
end;

procedure TdxBarBuiltInMenuHelper.SetImages(const Value: TCustomImageList);
begin
  FBarManager.Images := Value;
end;

procedure TdxBarBuiltInMenuHelper.SetLookAndFeel(const Value: TcxLookAndFeel);
begin
  FBarManager.LookAndFeel.MasterLookAndFeel := Value;
end;

{ TdxBarBuiltInPopupMenuAdapter }

function TdxBarBuiltInPopupMenuAdapter.Add(const ACaption: string; AClickEvent: TNotifyEvent;
  ATag: TdxNativeInt = 0; AImageIndex: TcxImageIndex = -1; AEnabled: Boolean = True): TComponent;
begin
  if dxBarPopupHelper.PopupMenu = nil then
    dxBarPopupHelper.CreatePopupMenu;
  Result := dxBarPopupHelper.CreateMenuItem(dxBarPopupHelper.PopupMenu, ACaption,
    ATag, AEnabled, bmitDefault, False, AImageIndex, FSeparatorRequired, nil, AClickEvent);
  FSeparatorRequired := False;
end;

procedure TdxBarBuiltInPopupMenuAdapter.AddSeparator;
begin
  FSeparatorRequired := True;
end;

procedure TdxBarBuiltInPopupMenuAdapter.Clear;
begin
  dxBarPopupHelper.DestroyPopupMenu;
end;

procedure TdxBarBuiltInPopupMenuAdapter.SetChecked(AItem: TComponent; AValue: Boolean);
var
  AButtom: TdxBarButton;
begin
  AButtom := AItem as TdxBarButton;
  AButtom.ButtonStyle := bsChecked;
  AButtom.Down := AValue;
end;

procedure TdxBarBuiltInPopupMenuAdapter.SetDefault(AItem: TComponent; AValue: Boolean);
begin
  // do nothing
end;

procedure TdxBarBuiltInPopupMenuAdapter.SetImages(AImages: TCustomImageList);
begin
  dxBarPopupHelper.Images := AImages;
end;

procedure TdxBarBuiltInPopupMenuAdapter.SetLookAndFeel(AValue: TcxLookAndFeel);
begin
  dxBarPopupHelper.LookAndFeel := AValue;
end;

function TdxBarBuiltInPopupMenuAdapter.GetPopupMenu: TComponent;
begin
  Result := dxBarPopupHelper.PopupMenu;
end;

initialization
  TdxBuiltInPopupMenuAdapterManager.Register(TdxBarBuiltInPopupMenuAdapter);

finalization
  TdxBuiltInPopupMenuAdapterManager.Unregister(TdxBarBuiltInPopupMenuAdapter);
  FreeAndNil(FBarPopupHelper);
  FreeAndNil(FFakeBarManager);
  FreeAndNil(FFakeForm);
end.
