{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressBars components                                   }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSBARS AND ALL ACCOMPANYING VCL  }
{   CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.                  }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxRibbonCustomizationFormHelper;
 
{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
   Windows, SysUtils, Classes, Types, Controls, Graphics, Dialogs, Math, dxCore, cxClasses, cxGeometry, cxGraphics,
   cxTL, dxBar, dxRibbon, cxDropDownEdit, cxLookAndFeelPainters, dxBarStrs;

const
  dxrcfContextLevel = 0;
  dxrcfTabLevel = 1;
  dxrcfGroupLevel = 2;
  dxrcfItemLevel = 3;

  dxrcfCommandsCommandsNotInTheRibbon = 0;
  dxrcfCommandsAllCommands = 1;
  dxrcfCommandsAllTabs = 2;
  dxrcfCommandsMainTabs = 3;
  dxrcfCommandsToolTabs = 4;
  dxrcfCommandsCustomTabsAndGroups = 5;

  dxrcfRibbonAllTabs = 0;
  dxrcfRibbonMainTabs = 1;
  dxrcfRibbonToolTabs = 2;

type
  { TdxRibbonCustomizationFormItemData }

  TdxRibbonCustomizationFormItemData = class
  private
    FItem: TdxBarItem;
    FItemLink: TdxBarItemLink;
  public
    constructor Create(AItem: TdxBarItem; AItemLink: TdxBarItemLink); reintroduce;
    function Clone: TdxRibbonCustomizationFormItemData;

    property Item: TdxBarItem read FItem;
    property ItemLink: TdxBarItemLink read FItemLink write FItemLink;
  end;

  { TdxCustomRibbonCustomizationFormHelper }

  TdxCustomRibbonCustomizationFormHelper = class
  private
    FCommandsComboBox: TcxComboBox;
    FCommandsTreeList: TcxTreeList;
    FLockCount: Integer;
    FMovedNode: TcxTreeListNode;
    FMovedNodeLevel: Integer;
    FRibbon: TdxCustomRibbon;
    FRibbonComboBox: TcxComboBox;
    FRibbonTreeList: TcxTreeList;

    function GetFocusedNode: TcxTreeListNode;
  protected
    procedure AfterNodeMoved(const AConvertNodeData: Boolean); virtual; abstract;
    procedure ConvertItemNodeToNew(ANode: TcxTreeListNode);
    procedure CopyInnerTree(ASourceNode, ATargetNode: TcxTreeListNode);
    procedure CopyNode(ASourceNode, ATargetNode: TcxTreeListNode; ACopyInnerTree: Boolean; ACopyData: Boolean = True);
    procedure DoMoveNode(ASourceNode, ATargetNode: TcxTreeListNode; const AInsertBeforeTargetNode: Boolean;
      const ATargetNodeIsParent: Boolean = False); virtual; abstract;
    function GetItemData(AItemNode: TcxTreeListNode): TdxRibbonCustomizationFormItemData;
    function GetNodeFor(AData: TPersistent; AParentNode: TcxTreeListNode): TcxTreeListNode;
    function GetTargetNodeForMovingDown(ASourceNode: TcxTreeListNode): TcxTreeListNode; virtual; abstract;
    function GetTargetNodeForMovingUp(ASourceNode: TcxTreeListNode): TcxTreeListNode; virtual; abstract;
    procedure PopulateAllItems(ASource: TdxBarManager; ATargetTreeList: TcxTreeList; const AOnlyMissingItems: Boolean = False);
    procedure SetFocusedNode(ANode: TcxTreeListNode; const AExpandNodeParents: Boolean = False); virtual;
    procedure LockContentUpdating;
    procedure UnlockContentUpdating;

    function IsRibbonContainsItem(AItem: TdxBarItem): Boolean;
    function IsSameElement(ASourceNode, ATargetNode: TcxTreeListNode): Boolean; overload;
    function IsSameElement(ANode: TcxTreeListNode; AData: TPersistent): Boolean; overload;
    function IsSourceNodeValid(ASourceNode: TcxTreeListNode): Boolean; virtual;
    function IsTargetNodeValid(ASourceNode, ATargetNode: TcxTreeListNode): Boolean; virtual;

    property CommandsComboBox: TcxComboBox read FCommandsComboBox;
    property CommandsTreeList: TcxTreeList read FCommandsTreeList;
    property FocusedNode: TcxTreeListNode read GetFocusedNode;
    property Ribbon: TdxCustomRibbon read FRibbon;
    property RibbonComboBox: TcxComboBox read FRibbonComboBox;
    property RibbonTreeList: TcxTreeList read FRibbonTreeList;
  public
    constructor Create(ARibbon: TdxCustomRibbon; ACommandsTreeList, ARibbonTreeList: TcxTreeList;
      ACommandsComboBox, ARibbonComboBox: TcxComboBox); reintroduce; virtual;

    procedure BeginApplyChanges; virtual; abstract;
    procedure CreateAddedElements; virtual; abstract;
    procedure DeleteRemovedElements; virtual; abstract;
    procedure EndApplyChanges; virtual; abstract;
    procedure ReorderElements; virtual; abstract;
    procedure SynchronizeElementCaptions; virtual; abstract;

    procedure AddNode(ANode: TcxTreeListNode);
    procedure DropMovedNodeTo(ATargetNode: TcxTreeListNode; const ADropPointY: Integer); virtual; abstract;
    procedure FreeNodeData(ANode: TcxTreeListNode);
    function GetFirstVisibleTab: TdxRibbonTab;
    function GetNodeLevel(ANode: TcxTreeListNode): Integer; virtual; abstract;
    procedure InitializeMovedNode(ANode: TcxTreeListNode);
    procedure MoveFocusedNodeDown; virtual; abstract;
    procedure MoveFocusedNodeUp; virtual; abstract;
    procedure PopulateCommandsComboBoxContent; virtual; abstract;
    procedure PopulateCommandsTreeListContent; virtual; abstract;
    procedure PopulateRibbonComboBoxContent; virtual; abstract;
    procedure PopulateRibbonTreeListContent(const AIsReseting: Boolean = False); virtual; abstract;
    procedure ReleaseMovedNode;
    procedure RemoveNode(ANode: TcxTreeListNode); virtual; abstract;
    procedure RenameNode(ANode: TcxTreeListNode; ANewCaption: string); virtual;

    function DrawItemNode(Sender: TcxCustomTreeList; ACanvas: TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo;
      ALookAndFeelPainter: TcxCustomLookAndFeelPainter): Boolean;

    function CanMoveFocusedNodeDown: Boolean; virtual;
    function CanMoveFocusedNodeUp: Boolean; virtual;
    function CanMoveNodeTo(ASourceNode, ATargetNode: TcxTreeListNode): Boolean; virtual; abstract;
    function CanRemoveNode(ANode: TcxTreeListNode): Boolean; virtual;
    function CanRenameNode(ANode: TcxTreeListNode): Boolean;
    function CanUpdateContent: Boolean;

    property MovedNode: TcxTreeListNode read FMovedNode;
  end;

  { TdxRibbonCustomizationFormHelper }

  TdxRibbonCustomizationFormHelper = class(TdxCustomRibbonCustomizationFormHelper)
  protected
    function AddNewNodeToFocusedNode(const ATargetLevel: Integer; ACaption: string): TcxTreeListNode;
    procedure AfterNodeMoved(const AConvertNodeData: Boolean); override;
    procedure ConvertGroupNodeToNew(ANode: TcxTreeListNode);
    procedure ConvertTabNodeToNew(ANode: TcxTreeListNode);
    function CreateContextNode(AContext: TdxRibbonContext; ATargetTreeList: TcxTreeList; const AIsReseting: Boolean = False): TcxTreeListNode;
    function CreateTabNode(ATab: TdxRibbonTab; AContextNode: TcxTreeListNode; ATargetTreeList: TcxTreeList; const AIsReseting: Boolean = False): TcxTreeListNode;
    procedure DoMoveNode(ASourceNode, ATargetNode: TcxTreeListNode; const AInsertBeforeTargetNode: Boolean;
      const ATargetNodeIsParent: Boolean = False); override;
    function GetContextNode(ASourceNode: TcxTreeListNode): TcxTreeListNode;
    function GetTargetNodeForMovingDown(ASourceNode: TcxTreeListNode): TcxTreeListNode; override;
    function GetTargetNodeForMovingUp(ASourceNode: TcxTreeListNode): TcxTreeListNode; override;
    procedure PopulateCustomTabsAndGroups(ATargetTreeList: TcxTreeList);
    procedure PopulateGroupNodeContent(ANode: TcxTreeListNode; ATargetTreeList: TcxTreeList);
    procedure PopulateTabNodeContent(ANode: TcxTreeListNode; ATargetTreeList: TcxTreeList; const AIsReseting: Boolean = False);
    procedure RestoreGroupNodesForSharedToolBar(ASenderGroup: TdxRibbonTabGroup);
    procedure RestoreTabNodeExpanding(ANode: TcxTreeListNode; AStorage: TList);
    procedure SetFocusedNode(ANode: TcxTreeListNode; const AExpandNodeParents: Boolean = False); override;
    procedure StoreTabNodeExpanding(ANode: TcxTreeListNode; AStorage: TList);
    procedure SynchronizeMatchingGroupNodesWith(ANode: TcxTreeListNode);

    procedure CheckAndAddContextNodeToRibbon(ANode: TcxTreeListNode);
    procedure CheckAndAddGroupNodeToRibbon(ANode: TcxTreeListNode);
    procedure CheckAndAddItemNodeToRibbon(ANode: TcxTreeListNode);
    procedure CheckAndAddTabNodeToRibbon(ANode: TcxTreeListNode);
    procedure CheckAndDeleteContextsFromRibbon;
    procedure CheckAndDeleteGroupsFromRibbon(AGroups: TdxRibbonTabGroups; ATabNode: TcxTreeListNode);
    procedure CheckAndDeleteItemLinksFromRibbon(AItemLinks: TdxBarItemLinks; AGroupNode: TcxTreeListNode);
    procedure CheckAndDeleteTabsFromRibbon(AContext: TdxRibbonContext);

    function IsGroupCustomizingAllowed(ANode: TcxTreeListNode): Boolean;
    function IsGroupHiddenInTab(AGroupNode, ATabNode: TcxTreeListNode): Boolean;
    function IsParentTabTheSame(ASourceGroupNode, ATargetNode: TcxTreeListNode): Boolean;
    function IsRibbonGroupNodeWithSharedToolBar(ANode: TcxTreeListNode): Boolean;
    function IsSourceNodeValid(ASourceNode: TcxTreeListNode): Boolean; override;
    function IsTargetNodeValid(ASourceNode, ATargetNode: TcxTreeListNode): Boolean; override;
  public
    procedure BeginApplyChanges; override;
    procedure CreateAddedElements; override;
    procedure DeleteRemovedElements; override;
    procedure EndApplyChanges; override;
    procedure ReorderElements; override;
    procedure SynchronizeElementCaptions; override;

    procedure AddNewContext;
    procedure AddNewGroup;
    procedure AddNewTab;
    procedure DropMovedNodeTo(ATargetNode: TcxTreeListNode; const ADropPointY: Integer); override;
    procedure ExpandAllContexts;
    function GetNodeLevel(ANode: TcxTreeListNode): Integer; override;
    function IsTabsInCommands: Boolean;
    procedure MoveFocusedNodeDown; override;
    procedure MoveFocusedNodeUp; override;
    procedure PopulateCommandsComboBoxContent; override;
    procedure PopulateCommandsTreeListContent; override;
    procedure PopulateRibbonComboBoxContent; override;
    procedure PopulateRibbonTreeListContent(const AIsReseting: Boolean = False); override;
    procedure RemoveNode(ANode: TcxTreeListNode); override;
    procedure RenameNode(ANode: TcxTreeListNode; ANewCaption: string); override;
    procedure ResetTabNode(ANode: TcxTreeListNode);
    procedure UpdateContextsVisibility;

    function CanMoveFocusedNodeDown: Boolean; override;
    function CanMoveFocusedNodeUp: Boolean; override;
    function CanMoveNodeTo(ASourceNode, ATargetNode: TcxTreeListNode): Boolean; override;
    function CanRemoveNode(ANode: TcxTreeListNode): Boolean; override;
    function CanResetFocusedNode: Boolean;

    function DrawContextNode(Sender: TcxCustomTreeList; ACanvas: TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo;
      ALookAndFeelPainter: TcxCustomLookAndFeelPainter): Boolean;
  end;

  { TdxRibbonQATCustomizationFormHelper }

  TdxRibbonQATCustomizationFormHelper = class(TdxCustomRibbonCustomizationFormHelper)
  protected
    procedure AfterNodeMoved(const AConvertNodeData: Boolean); override;
    procedure DoMoveNode(ASourceNode, ATargetNode: TcxTreeListNode; const AInsertBeforeTargetNode: Boolean;
      const ATargetNodeIsParent: Boolean = False); override;
    function GetTargetNodeForMovingDown(ASourceNode: TcxTreeListNode): TcxTreeListNode; override;
    function GetTargetNodeForMovingUp(ASourceNode: TcxTreeListNode): TcxTreeListNode; override;
    procedure PopulateContextTabs(AContext: TdxRibbonContext; AItems: TStrings);
    procedure PopulateTabItems(ATab: TdxRibbonTab; ATargetTreeList: TcxTreeList);
  public
    procedure BeginApplyChanges; override;
    procedure CreateAddedElements; override;
    procedure DeleteRemovedElements; override;
    procedure EndApplyChanges; override;
    procedure ReorderElements; override;
    procedure SynchronizeElementCaptions; override;

    procedure ChangeQATPosition(const AShowBelowRibbon: Boolean);
    procedure DropMovedNodeTo(ATargetNode: TcxTreeListNode; const ADropPointY: Integer); override;
    function GetNodeLevel(ANode: TcxTreeListNode): Integer; override;
    function IsQATBelowRibbon: Boolean;
    procedure MoveFocusedNodeDown; override;
    procedure MoveFocusedNodeUp; override;
    procedure PopulateCommandsComboBoxContent; override;
    procedure PopulateCommandsTreeListContent; override;
    procedure PopulateRibbonComboBoxContent; override;
    procedure PopulateRibbonTreeListContent(const AIsReseting: Boolean = False); override;
    procedure RemoveNode(ANode: TcxTreeListNode); override;

    function CanMoveNodeTo(ASourceNode, ATargetNode: TcxTreeListNode): Boolean; override;
  end;

implementation

type
  TcxTreeListNodeAccess = class(TcxTreeListNode);
  TdxRibbonTabAccess = class(TdxRibbonTab);
  TdxCustomRibbonAccess = class(TdxCustomRibbon);
  TdxRibbonTabGroupAccess = class(TdxRibbonTabGroup);

{ TdxRibbonCustomizationFormItemData }

constructor TdxRibbonCustomizationFormItemData.Create(AItem: TdxBarItem; AItemLink: TdxBarItemLink);
begin
  inherited Create;
  FItem := AItem;
  FItemLink := AItemLink;
end;

function TdxRibbonCustomizationFormItemData.Clone: TdxRibbonCustomizationFormItemData;
begin
  Result := TdxRibbonCustomizationFormItemData.Create(FItem, FItemLink);
end;

{ TdxCustomRibbonCustomizationFormHelper }

constructor TdxCustomRibbonCustomizationFormHelper.Create(ARibbon: TdxCustomRibbon;
  ACommandsTreeList, ARibbonTreeList: TcxTreeList; ACommandsComboBox, ARibbonComboBox: TcxComboBox);
begin
  inherited Create;
  FRibbon := ARibbon;
  FCommandsComboBox := ACommandsComboBox;
  FCommandsTreeList := ACommandsTreeList;
  FRibbonComboBox := ARibbonComboBox;
  FRibbonTreeList := ARibbonTreeList;
end;

procedure TdxCustomRibbonCustomizationFormHelper.AddNode(ANode: TcxTreeListNode);
begin
  InitializeMovedNode(ANode);
  DropMovedNodeTo(FocusedNode, RibbonTreeList.Height);  
  ReleaseMovedNode;
  if ANode.getNextSibling <> nil then
    ANode.getNextSibling.Focused := True;
end;

procedure TdxCustomRibbonCustomizationFormHelper.FreeNodeData(ANode: TcxTreeListNode);
begin
  if TObject(ANode.Data) is TdxRibbonCustomizationFormItemData then
  begin
    TObject(ANode.Data).Free;
    ANode.Data := nil;
  end;
end;

function TdxCustomRibbonCustomizationFormHelper.GetFirstVisibleTab: TdxRibbonTab;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to Ribbon.TabCount - 1 do
    if TdxRibbonTabAccess(Ribbon.Tabs[I]).IsVisible then
    begin
      Result := Ribbon.Tabs[I];
      Break;
    end;
end;

procedure TdxCustomRibbonCustomizationFormHelper.InitializeMovedNode(ANode: TcxTreeListNode);
begin
  FMovedNode := ANode;
  FMovedNodeLevel := GetNodeLevel(ANode);
  LockContentUpdating;
end;

procedure TdxCustomRibbonCustomizationFormHelper.ReleaseMovedNode;
begin
  FMovedNode := nil;
  FMovedNodeLevel := -1;
  UnlockContentUpdating;
end;

procedure TdxCustomRibbonCustomizationFormHelper.RenameNode(ANode: TcxTreeListNode; ANewCaption: string);
begin
  ANode.Texts[0] := ANewCaption;
end;

function TdxCustomRibbonCustomizationFormHelper.DrawItemNode(Sender: TcxCustomTreeList; ACanvas: TcxCanvas;
  AViewInfo: TcxTreeListEditCellViewInfo; ALookAndFeelPainter: TcxCustomLookAndFeelPainter): Boolean;
var
  ARect: TRect;
  AItem: TdxBarItem;
  AImageWidth: Integer;
begin
  AItem := GetItemData(AViewInfo.Node).Item;
  ARect := AViewInfo.BoundsRect;
  AImageWidth := cxRectHeight(ARect);
  cxDrawImage(ACanvas, cxRectSetWidth(ARect, AImageWidth), AItem.Glyph, AItem.BarManager.Images, AItem.ImageIndex, ifmFit);
  Inc(ARect.Left, AImageWidth);
  ARect := cxRectInflate(ARect, -cxHeaderTextOffset);
  ARect := cxRectCenterVertically(ARect, cxTextHeight(ACanvas.Handle));
  cxDrawText(ACanvas, AViewInfo.Node.Texts[0], ARect, DT_LEFT);
  Result := True;
end;

function TdxCustomRibbonCustomizationFormHelper.CanMoveFocusedNodeDown: Boolean;
begin
  Result := IsSourceNodeValid(FocusedNode) and ((FocusedNode.getNextSibling <> nil) or (GetTargetNodeForMovingDown(FocusedNode) <> nil));
end;

function TdxCustomRibbonCustomizationFormHelper.CanMoveFocusedNodeUp: Boolean;
begin
  Result := IsSourceNodeValid(FocusedNode) and ((FocusedNode.getPrevSibling <> nil) or (GetTargetNodeForMovingUp(FocusedNode) <> nil));
end;

function TdxCustomRibbonCustomizationFormHelper.CanRemoveNode(ANode: TcxTreeListNode): Boolean;
begin
  Result := ANode <> nil;
end;

function TdxCustomRibbonCustomizationFormHelper.CanRenameNode(ANode: TcxTreeListNode): Boolean;
begin
  Result := (ANode <> nil) and ANode.Enabled and ((GetNodeLevel(ANode) <> dxrcfContextLevel) or
    not SameText(ANode.Texts[0], cxGetResourceString(@sdxRibbonCustomizationFormMainTabs)));
end;

function TdxCustomRibbonCustomizationFormHelper.CanUpdateContent: Boolean;
begin
  Result := FLockCount = 0;
end;

procedure TdxCustomRibbonCustomizationFormHelper.ConvertItemNodeToNew(ANode: TcxTreeListNode);
begin
  GetItemData(ANode).ItemLink := nil;
  ANode.Enabled := True;
end;

procedure TdxCustomRibbonCustomizationFormHelper.CopyInnerTree(ASourceNode, ATargetNode: TcxTreeListNode);
var
  I: Integer;
begin
  if GetNodeLevel(ASourceNode) < dxrcfItemLevel then
    for I := 0 to ASourceNode.Count - 1 do
      CopyNode(ASourceNode.Items[I], ATargetNode.AddChild, GetNodeLevel(ASourceNode) < dxrcfGroupLevel);
end;

procedure TdxCustomRibbonCustomizationFormHelper.CopyNode(
  ASourceNode, ATargetNode: TcxTreeListNode; ACopyInnerTree: Boolean; ACopyData: Boolean = True);
begin
  LockContentUpdating;
  try
    ATargetNode.Assign(ASourceNode);
    if ACopyData then
      if GetNodeLevel(ASourceNode) = dxrcfItemLevel then
        ATargetNode.Data := GetItemData(ASourceNode).Clone
      else
        ATargetNode.Data := ASourceNode.Data;
    if ACopyInnerTree then
      CopyInnerTree(ASourceNode, ATargetNode);
  finally
    UnlockContentUpdating;
  end;
end;

function TdxCustomRibbonCustomizationFormHelper.GetItemData(AItemNode: TcxTreeListNode): TdxRibbonCustomizationFormItemData;
begin
  Result := TdxRibbonCustomizationFormItemData(AItemNode.Data);
end;

function TdxCustomRibbonCustomizationFormHelper.GetNodeFor(AData: TPersistent; AParentNode: TcxTreeListNode): TcxTreeListNode;
begin
  Result := AParentNode;
  if Result <> nil then
  begin
    Result := AParentNode.getFirstChild;
    while (Result <> nil) and not IsSameElement(Result, AData) do
      Result := Result.getNextSibling;
  end;
end;

procedure TdxCustomRibbonCustomizationFormHelper.PopulateAllItems(
  ASource: TdxBarManager; ATargetTreeList: TcxTreeList; const AOnlyMissingItems: Boolean = False);
var
  AItem: TdxBarItem;
  I: Integer;
begin
  for I := 0 to ASource.ItemCount - 1 do
  begin
    AItem := ASource.Items[I];
    if not AOnlyMissingItems or not IsRibbonContainsItem(AItem) then
      ATargetTreeList.Add(nil, TdxRibbonCustomizationFormItemData.Create(AItem, nil)).Texts[0] := AItem.Caption;
  end;
end;

procedure TdxCustomRibbonCustomizationFormHelper.SetFocusedNode(ANode: TcxTreeListNode; const AExpandNodeParents: Boolean = False);
begin
  ANode.Focused := True;
end;

procedure TdxCustomRibbonCustomizationFormHelper.LockContentUpdating;
begin
  Inc(FLockCount);
end;

procedure TdxCustomRibbonCustomizationFormHelper.UnlockContentUpdating;
begin
  Dec(FLockCount);
end;

function TdxCustomRibbonCustomizationFormHelper.IsRibbonContainsItem(AItem: TdxBarItem): Boolean;
var
  ATabIndex, AGroupIndex: Integer;
  ATab: TdxRibbonTab;
  AToolBar: TdxBar;
begin
  Result := False;
  ATabIndex := 0;
  while not Result and (ATabIndex < Ribbon.TabCount) do
  begin
    ATab := Ribbon.Tabs[ATabIndex];
    AGroupIndex := 0;
    while not Result and (AGroupIndex < ATab.Groups.Count) do
    begin
      AToolBar := ATab.Groups[AGroupIndex].ToolBar;
      Result := (AToolBar <> nil) and AToolBar.ItemLinks.HasItem(AItem);
      Inc(AGroupIndex);
    end;
    Inc(ATabIndex);
  end;
end;

function TdxCustomRibbonCustomizationFormHelper.IsSameElement(ASourceNode, ATargetNode: TcxTreeListNode): Boolean;
begin
  Result := (ASourceNode.Data <> nil) and (ATargetNode.Data <> nil);
  case GetNodeLevel(ATargetNode) of
    dxrcfContextLevel, dxrcfTabLevel:
      Result := ASourceNode.Data = ATargetNode.Data;
    dxrcfGroupLevel:
      Result := Result and (TdxRibbonTabGroup(ASourceNode.Data).ToolBar = TdxRibbonTabGroup(ATargetNode.Data).ToolBar);
    dxrcfItemLevel:
      Result := Result and (GetItemData(ASourceNode).Item = GetItemData(ATargetNode).Item);
  end;
end;

function TdxCustomRibbonCustomizationFormHelper.IsSameElement(ANode: TcxTreeListNode; AData: TPersistent): Boolean;
begin
  Result := (ANode.Data <> nil) and (AData <> nil);
  case GetNodeLevel(ANode) of
    dxrcfContextLevel, dxrcfTabLevel:
      Result := TPersistent(ANode.Data) = AData;
    dxrcfGroupLevel:
      Result := Result and (TdxRibbonTabGroup(ANode.Data).ToolBar = TdxRibbonTabGroup(AData).ToolBar);
    dxrcfItemLevel:
      Result := Result and (GetItemData(ANode).Item = TdxBarItem(AData));
  end;
end;

function TdxCustomRibbonCustomizationFormHelper.IsSourceNodeValid(ASourceNode: TcxTreeListNode): Boolean;
begin
  Result := ASourceNode <> nil;
end;

function TdxCustomRibbonCustomizationFormHelper.IsTargetNodeValid(ASourceNode, ATargetNode: TcxTreeListNode): Boolean;
begin
  Result := ATargetNode <> nil;
end;

function TdxCustomRibbonCustomizationFormHelper.GetFocusedNode: TcxTreeListNode;
begin
  Result := RibbonTreeList.FocusedNode;
  if (Result = nil) and (RibbonTreeList.SelectionCount > 0) then
    Result := RibbonTreeList.Selections[0];
end;

{ TdxRibbonCustomizationFormHelper }

procedure TdxRibbonCustomizationFormHelper.BeginApplyChanges;
begin
  Ribbon.BeginUpdate;
end;

procedure TdxRibbonCustomizationFormHelper.CreateAddedElements;
var
  ANode: TcxTreeListNode;
  I: Integer;
begin
  for I := 1 to RibbonTreeList.AbsoluteCount - 1 do
  begin
    ANode := RibbonTreeList.AbsoluteItems[I];
    case ANode.Level of
      dxrcfContextLevel:
        CheckAndAddContextNodeToRibbon(ANode);
      dxrcfTabLevel:
        CheckAndAddTabNodeToRibbon(ANode);
      dxrcfGroupLevel:
        CheckAndAddGroupNodeToRibbon(ANode);
      dxrcfItemLevel:
        CheckAndAddItemNodeToRibbon(ANode);
    end;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.DeleteRemovedElements;
begin
  CheckAndDeleteContextsFromRibbon;
end;

procedure TdxRibbonCustomizationFormHelper.EndApplyChanges;
begin
  TdxCustomRibbonAccess(Ribbon).RecalculateBars;
  if Ribbon.ActiveTab <> nil then
    TdxRibbonTabAccess(Ribbon.ActiveTab).Activate;
  Ribbon.EndUpdate;
end;

procedure TdxRibbonCustomizationFormHelper.ReorderElements;
var
  ANode: TcxTreeListNode;
  I: Integer;
begin
  for I := 1 to RibbonTreeList.AbsoluteCount - 1 do
  begin
    ANode := RibbonTreeList.AbsoluteItems[I];
    case ANode.Level of
      dxrcfContextLevel:
        TdxRibbonContext(ANode.Data).Index := ANode.Index - 1;
      dxrcfTabLevel:
        begin
          TdxRibbonTab(ANode.Data).Visible := ANode.Checked;
          TdxRibbonTab(ANode.Data).Index := ANode.Index;
        end;
      dxrcfGroupLevel:
        TdxRibbonTabGroup(ANode.Data).Index := ANode.Index;
      dxrcfItemLevel:
        GetItemData(ANode).ItemLink.Index := ANode.Index;
    end;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.SynchronizeElementCaptions;
var
  ANode: TcxTreeListNode;
  I: Integer;
begin
  for I := 1 to RibbonTreeList.AbsoluteCount - 1 do
  begin
    ANode := RibbonTreeList.AbsoluteItems[I];
    case ANode.Level of
      dxrcfContextLevel:
        TdxRibbonContext(ANode.Data).Caption := ANode.Texts[0];
      dxrcfTabLevel:
        TdxRibbonTab(ANode.Data).Caption := ANode.Texts[0];
      dxrcfGroupLevel:
        TdxRibbonTabGroup(ANode.Data).Caption := ANode.Texts[0];
      dxrcfItemLevel:
        GetItemData(ANode).ItemLink.UserCaption := ANode.Texts[0];
    end;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.AddNewContext;   
var
  ANewContextNode: TcxTreeListNode;
begin
  LockContentUpdating;
  try
    ANewContextNode := RibbonTreeList.Root.AddChild;
    ANewContextNode.Texts[0] := cxGetResourceString(@sdxRibbonCustomizationFormNewContext);
    ANewContextNode.Focused := True;
    ANewContextNode.CheckGroupType := ncgCheckGroup;
    AddNewTab;
    ANewContextNode.Expand(True);
  finally
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.AddNewGroup;
begin
  LockContentUpdating;
  try
    AddNewNodeToFocusedNode(dxrcfGroupLevel, cxGetResourceString(@sdxRibbonCustomizationFormNewGroup));
  finally
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.AddNewTab;
var
  ANewTabNode: TcxTreeListNode;
begin
  LockContentUpdating;
  try
    ANewTabNode := AddNewNodeToFocusedNode(dxrcfTabLevel, cxGetResourceString(@sdxRibbonCustomizationFormNewTab));
    AddNewGroup;
    ANewTabNode.Expand(True);
  finally
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.DropMovedNodeTo(ATargetNode: TcxTreeListNode; const ADropPointY: Integer);
var
  AViewData: TcxTreeListNodeViewData;
begin
  if CanMoveNodeTo(MovedNode, ATargetNode) then
  begin
    while GetNodeLevel(ATargetNode) > FMovedNodeLevel do
      ATargetNode := ATargetNode.Parent;
    AViewData := TcxTreeListNodeAccess(ATargetNode).ViewData;
    if GetNodeLevel(ATargetNode) = FMovedNodeLevel then
      DoMoveNode(MovedNode, ATargetNode, (AViewData <> nil) and (ADropPointY < cxRectCenter(AViewData.GetRealBounds).Y))
    else
      DoMoveNode(MovedNode, ATargetNode, False, True);
  end;
end;

procedure TdxRibbonCustomizationFormHelper.ExpandAllContexts;
var
  I: Integer;
begin
  for I := 0 to RibbonTreeList.Root.Count - 1 do
    RibbonTreeList.Root.Items[I].Expand(False);
  if IsTabsInCommands then
    for I := 0 to CommandsTreeList.Root.Count - 1 do
      CommandsTreeList.Root.Items[I].Expand(False);
end;

function TdxRibbonCustomizationFormHelper.GetNodeLevel(ANode: TcxTreeListNode): Integer;
begin
  if ANode = nil then
    Result := -1
  else
    if ANode.TreeList = RibbonTreeList then
      Result := ANode.Level
    else
      if CommandsComboBox.ItemIndex in [dxrcfCommandsCommandsNotInTheRibbon, dxrcfCommandsAllCommands] then
        Result := dxrcfItemLevel
      else
        if CommandsComboBox.ItemIndex <> dxrcfCommandsCustomTabsAndGroups then
          Result := ANode.Level
        else
          Result := ANode.Level + 1;
end;

function TdxRibbonCustomizationFormHelper.IsTabsInCommands: Boolean;
begin
  Result := CommandsComboBox.ItemIndex in [dxrcfCommandsAllTabs, dxrcfCommandsMainTabs, dxrcfCommandsToolTabs];
end;

procedure TdxRibbonCustomizationFormHelper.MoveFocusedNodeDown;
var
  ASourceNode, ATargetNode: TcxTreeListNode;
begin
  ASourceNode := FocusedNode;
  ATargetNode := ASourceNode.getNextSibling;
  if ATargetNode <> nil then
    DoMoveNode(ASourceNode, ATargetNode, False)
  else
  begin
    ATargetNode := GetTargetNodeForMovingDown(ASourceNode);
    if ATargetNode.Count > 0 then
      DoMoveNode(ASourceNode, ATargetNode.getFirstChild, True)
    else
      DoMoveNode(ASourceNode, ATargetNode, False, True);
  end;
end;

procedure TdxRibbonCustomizationFormHelper.MoveFocusedNodeUp;
var
  ASourceNode, ATargetNode: TcxTreeListNode;
begin
  ASourceNode := FocusedNode;
  ATargetNode := ASourceNode.getPrevSibling;
  if ATargetNode <> nil then
    DoMoveNode(ASourceNode, ATargetNode, True)
  else
  begin
    ATargetNode := GetTargetNodeForMovingUp(ASourceNode);
    if ATargetNode.Level = ASourceNode.Level then
      DoMoveNode(ASourceNode, ATargetNode, False)
    else
      if ATargetNode.Count > 0 then
        DoMoveNode(ASourceNode, ATargetNode.GetLastChild, False)
      else
        DoMoveNode(ASourceNode, ATargetNode, True, True);
  end;
end;

procedure TdxRibbonCustomizationFormHelper.PopulateCommandsComboBoxContent;
var
  AItems: TStrings;
begin
  AItems := CommandsComboBox.Properties.Items;
  LockContentUpdating;
  AItems.BeginUpdate;
  try
    AItems.Clear;
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormCommandsNotInTheRibbon));
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormAllCommands));
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormAllTabs));
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormMainTabs));
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormToolTabs));
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormCustomTabsAndGroups));
    CommandsComboBox.ItemIndex := dxrcfCommandsAllTabs;
  finally
    AItems.EndUpdate;
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.PopulateCommandsTreeListContent;
var
  I: Integer;
begin
  LockContentUpdating;
  CommandsTreeList.BeginUpdate;
  try
    CommandsTreeList.Clear;
    CommandsTreeList.OptionsView.ShowRoot := False;
    CommandsTreeList.OptionsBehavior.IncSearch := CommandsComboBox.ItemIndex in [dxrcfCommandsCommandsNotInTheRibbon, dxrcfCommandsAllCommands];
    case CommandsComboBox.ItemIndex of
      dxrcfCommandsCommandsNotInTheRibbon:
        PopulateAllItems(Ribbon.BarManager, CommandsTreeList, True);
      dxrcfCommandsAllCommands:
        PopulateAllItems(Ribbon.BarManager, CommandsTreeList);
      dxrcfCommandsAllTabs:
        begin
          CreateContextNode(nil, CommandsTreeList);
          for I := 0 to Ribbon.Contexts.Count - 1 do
            CreateContextNode(Ribbon.Contexts[I], CommandsTreeList);
        end;
      dxrcfCommandsMainTabs:
        CreateContextNode(nil, CommandsTreeList);
      dxrcfCommandsToolTabs:
        for I := 0 to Ribbon.Contexts.Count - 1 do
          CreateContextNode(Ribbon.Contexts[I], CommandsTreeList);
      dxrcfCommandsCustomTabsAndGroups:
        PopulateCustomTabsAndGroups(CommandsTreeList);
    end;
  finally
    CommandsTreeList.EndUpdate;
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.PopulateRibbonComboBoxContent;
var
  AItems: TStrings;
begin
  AItems := RibbonComboBox.Properties.Items;
  LockContentUpdating;
  AItems.BeginUpdate;
  try
    AItems.Clear;
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormAllTabs));
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormMainTabs));
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormToolTabs));
    RibbonComboBox.ItemIndex := dxrcfRibbonAllTabs;
  finally
    AItems.EndUpdate;
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.PopulateRibbonTreeListContent(const AIsReseting: Boolean = False);
var
  I: Integer;
begin
  LockContentUpdating;
  RibbonTreeList.BeginUpdate;
  try
    RibbonTreeList.Clear;
    CreateContextNode(nil, RibbonTreeList, AIsReseting);
    for I := 0 to Ribbon.Contexts.Count - 1 do
      CreateContextNode(Ribbon.Contexts[I], RibbonTreeList, AIsReseting);
    UpdateContextsVisibility;
  finally
    RibbonTreeList.EndUpdate;
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.RemoveNode(ANode: TcxTreeListNode);
var
  AGroupNode: TcxTreeListNode;
begin
  ANode.DeleteChildren;
  if GetNodeLevel(ANode) = dxrcfItemLevel then
    AGroupNode := ANode.Parent
  else
    AGroupNode := nil;
  ANode.Delete;
  if AGroupNode <> nil then
    SynchronizeMatchingGroupNodesWith(AGroupNode);
end;

procedure TdxRibbonCustomizationFormHelper.RenameNode(ANode: TcxTreeListNode; ANewCaption: string);
begin
  inherited RenameNode(ANode, ANewCaption);
  case ANode.Level of
    dxrcfGroupLevel:
      SynchronizeMatchingGroupNodesWith(ANode);
    dxrcfItemLevel:
      SynchronizeMatchingGroupNodesWith(ANode.Parent);
  end;
end;

procedure TdxRibbonCustomizationFormHelper.ResetTabNode(ANode: TcxTreeListNode);
var
  AExpandedNodesList: TList;
  I: Integer;
begin
  LockContentUpdating;
  AExpandedNodesList := TList.Create;
  StoreTabNodeExpanding(ANode, AExpandedNodesList);
  try
    ANode.Texts[0] := TdxRibbonTab(ANode.Data).Caption;
    ANode.DeleteChildren;
    PopulateTabNodeContent(ANode, RibbonTreeList, True);
    for I := 0 to ANode.Count - 1 do
      SynchronizeMatchingGroupNodesWith(ANode.Items[I]);
    ANode.Checked := TdxRibbonTab(ANode.Data).Visible;
    ANode.Expand(False);
    SetFocusedNode(ANode);
  finally
    RestoreTabNodeExpanding(ANode, AExpandedNodesList);
    AExpandedNodesList.Free;
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.UpdateContextsVisibility;
var
  I: Integer;
begin
  if CanUpdateContent and (RibbonTreeList.Root.Count > 0) then
  begin
    RibbonTreeList.Root.Items[0].Visible := RibbonComboBox.ItemIndex in [dxrcfRibbonAllTabs, dxrcfRibbonMainTabs];
    for I := 1 to RibbonTreeList.Root.Count - 1 do
      RibbonTreeList.Root.Items[I].Visible := RibbonComboBox.ItemIndex in [dxrcfRibbonAllTabs, dxrcfRibbonToolTabs];
    RibbonTreeList.SelectionList.Clear;
    RibbonTreeList.FocusedNode := nil;
    ExpandAllContexts;
  end;
end;

function TdxRibbonCustomizationFormHelper.CanMoveFocusedNodeDown: Boolean;
var                            
  ATargetNode: TcxTreeListNode;
begin
  Result := inherited CanMoveFocusedNodeDown;
  if Result then
  begin
    ATargetNode := FocusedNode.getNextSibling;
    if ATargetNode = nil then
      ATargetNode := GetTargetNodeForMovingDown(FocusedNode);
    Result := (RibbonComboBox.ItemIndex <> dxrcfRibbonMainTabs) or (GetContextNode(ATargetNode) = RibbonTreeList.Root.Items[0]);
  end;
end;

function TdxRibbonCustomizationFormHelper.CanMoveFocusedNodeUp: Boolean;
var                            
  ATargetNode: TcxTreeListNode;
begin
  Result := inherited CanMoveFocusedNodeUp;
  if Result then
  begin
    ATargetNode := FocusedNode.getPrevSibling;
    if ATargetNode = nil then
      ATargetNode := GetTargetNodeForMovingUp(FocusedNode);
    Result := (RibbonComboBox.ItemIndex <> dxrcfRibbonToolTabs) or (GetContextNode(ATargetNode) <> RibbonTreeList.Root.Items[0]);
  end;
end;

function TdxRibbonCustomizationFormHelper.CanMoveNodeTo(ASourceNode, ATargetNode: TcxTreeListNode): Boolean;
var
  AActualParentNode: TcxTreeListNode;
  ASourceNodeLevel, I: Integer;
begin
  Result := IsSourceNodeValid(ASourceNode) and IsTargetNodeValid(ASourceNode, ATargetNode) and
    (not IsRibbonGroupNodeWithSharedToolBar(ASourceNode) or IsParentTabTheSame(ASourceNode, ATargetNode));
  if Result then
  begin
    ASourceNodeLevel := GetNodeLevel(ASourceNode);
    AActualParentNode := ATargetNode;
    while (AActualParentNode.Level > ASourceNodeLevel - 1) do
      AActualParentNode := AActualParentNode.Parent;
    if ((ASourceNode.TreeList = RibbonTreeList) or (ASourceNodeLevel = dxrcfItemLevel)) and
      (AActualParentNode <> ASourceNode.Parent) then
      for I := 0 to AActualParentNode.Count - 1 do
        if IsSameElement(ASourceNode, AActualParentNode.Items[I]) then
        begin
          Result := False;
          Break;
        end;
  end;
end;

function TdxRibbonCustomizationFormHelper.CanRemoveNode(ANode: TcxTreeListNode): Boolean;
begin
  Result := inherited CanRemoveNode(ANode);
  if Result then
    case GetNodeLevel(ANode) of
      dxrcfContextLevel:
        Result := (ANode.Data = nil) and (ANode.Index <> 0);
      dxrcfTabLevel:
        Result := (ANode.Data = nil) or not TdxRibbonTab(ANode.Data).IsPredefined;
      dxrcfGroupLevel:
        Result := (ANode.Data = nil) or not TdxRibbonTabGroup(ANode.Data).IsToolBarShared;
      dxrcfItemLevel:
        Result := ANode.Enabled and (GetItemData(ANode).ItemLink = nil);
    end;
end;

function TdxRibbonCustomizationFormHelper.CanResetFocusedNode: Boolean;
var
  AFocusedNode: TcxTreeListNode;
begin
  AFocusedNode := FocusedNode;
  Result := (AFocusedNode <> nil) and (AFocusedNode.Level > dxrcfContextLevel);
  if Result then
  begin
    while AFocusedNode.Level > dxrcfTabLevel do
      AFocusedNode := AFocusedNode.Parent;
    Result := AFocusedNode.Data <> nil;
  end;
end;

function TdxRibbonCustomizationFormHelper.DrawContextNode(Sender: TcxCustomTreeList; ACanvas: TcxCanvas;
  AViewInfo: TcxTreeListEditCellViewInfo; ALookAndFeelPainter: TcxCustomLookAndFeelPainter): Boolean;
var
  ATextRect: TRect;
begin
  ALookAndFeelPainter.DrawGalleryGroupHeader(ACanvas, AViewInfo.BoundsRect);
  ACanvas.Font.Style := ACanvas.Font.Style + [fsBold];
  ACanvas.Font.Color := ALookAndFeelPainter.GetGalleryGroupTextColor;
  ATextRect := cxRectCenterVertically(AViewInfo.BoundsRect, cxTextHeight(ACanvas.Handle));
  ATextRect := cxRectInflate(ATextRect, -cxHeaderTextOffset, 0);
  cxDrawText(ACanvas, AViewInfo.Node.Texts[0], ATextRect, DT_LEFT);
  Result := True;
end;

function TdxRibbonCustomizationFormHelper.AddNewNodeToFocusedNode(const ATargetLevel: Integer; ACaption: string): TcxTreeListNode;
var
  AParentNode: TcxTreeListNode;
  ANodeIndex: Integer;
begin
  ANodeIndex := -1;
  AParentNode := FocusedNode;
  while AParentNode.Level >= ATargetLevel do
  begin
    ANodeIndex := AParentNode.Index;
    AParentNode := AParentNode.Parent;
  end;
  if (ANodeIndex >= 0) and (ANodeIndex < AParentNode.Count - 1) then
    Result := RibbonTreeList.Insert(AParentNode.Items[ANodeIndex + 1])
  else
    Result := RibbonTreeList.AddChild(AParentNode);
  Result.Texts[0] := ACaption;
  Result.Checked := (Result.Level <> dxrcfGroupLevel) or AParentNode.Checked;
  Result.MakeVisible;
  SetFocusedNode(Result);
end;

procedure TdxRibbonCustomizationFormHelper.AfterNodeMoved(const AConvertNodeData: Boolean);
var
  AFocusedNode: TcxTreeListNode;
begin
  AFocusedNode := FocusedNode;
  case AFocusedNode.Level of
    dxrcfTabLevel:
      if AConvertNodeData then
        ConvertTabNodeToNew(AFocusedNode);
    dxrcfGroupLevel:
      begin
        AFocusedNode.Checked := AFocusedNode.Parent.Checked;
        if AConvertNodeData then
          ConvertGroupNodeToNew(AFocusedNode);
      end;
    dxrcfItemLevel:
      begin
        if AConvertNodeData then
          ConvertItemNodeToNew(AFocusedNode);
        SynchronizeMatchingGroupNodesWith(AFocusedNode.Parent);
      end;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.ConvertGroupNodeToNew(ANode: TcxTreeListNode);
var
  I: Integer;
begin
  for I := 0 to ANode.Count - 1 do
    ConvertItemNodeToNew(ANode.Items[I]);
  ANode.Data := nil;
  ANode.Texts[0] := ANode.Texts[0] + cxGetResourceString(@sdxRibbonCustomizationFormCustomElementSuffix);
  ANode.Enabled := True;
end;

procedure TdxRibbonCustomizationFormHelper.ConvertTabNodeToNew(ANode: TcxTreeListNode);
var
  I: Integer;
begin
  for I := 0 to ANode.Count - 1 do
    ConvertGroupNodeToNew(ANode.Items[I]);
  ANode.Data := nil;
  ANode.Texts[0] := ANode.Texts[0] + cxGetResourceString(@sdxRibbonCustomizationFormCustomElementSuffix);
end;

function TdxRibbonCustomizationFormHelper.CreateContextNode(AContext: TdxRibbonContext; ATargetTreeList: TcxTreeList;
  const AIsReseting: Boolean = False): TcxTreeListNode;
var
  I: Integer;
begin
  Result := ATargetTreeList.Add(nil, AContext);
  if AContext <> nil then
    Result.Texts[0] := AContext.Caption
  else
    Result.Texts[0] := cxGetResourceString(@sdxRibbonCustomizationFormMainTabs);
  for I := 0 to Ribbon.TabCount - 1 do
    if (Ribbon.Tabs[I].Context = AContext) and ((ATargetTreeList = RibbonTreeList) or Ribbon.Tabs[I].IsPredefined) then
      CreateTabNode(Ribbon.Tabs[I], Result, ATargetTreeList, AIsReseting);
  ATargetTreeList.AbsoluteItems[Result.AbsoluteIndex].Expand(False);
  Result.CheckGroupType := ncgCheckGroup;
end;

function TdxRibbonCustomizationFormHelper.CreateTabNode(ATab: TdxRibbonTab; AContextNode: TcxTreeListNode;
  ATargetTreeList: TcxTreeList; const AIsReseting: Boolean = False): TcxTreeListNode;
begin
  Result := ATargetTreeList.AddChild(AContextNode, ATab);
  Result.Texts[0] := ATab.Caption;
  PopulateTabNodeContent(Result, ATargetTreeList, AIsReseting);
  Result.Checked := ATab.Visible;
  Result.Expanded := ATab.Active;
end;

procedure TdxRibbonCustomizationFormHelper.DoMoveNode(ASourceNode, ATargetNode: TcxTreeListNode;
  const AInsertBeforeTargetNode: Boolean; const ATargetNodeIsParent: Boolean = False);
var
  ANewNode: TcxTreeListNode;
  AGroupWasHidden: Boolean;
begin
  if ATargetNodeIsParent then
    ANewNode := ATargetNode.AddChild
  else
    if AInsertBeforeTargetNode then
      ANewNode := ATargetNode.Parent.InsertChild(ATargetNode)
    else
      if ATargetNode.getNextSibling = nil then
        ANewNode := ATargetNode.Parent.AddChild
      else
        ANewNode := ATargetNode.Parent.InsertChild(ATargetNode.getNextSibling);
  AGroupWasHidden := IsGroupHiddenInTab(ASourceNode, ANewNode.Parent);
  CopyNode(ASourceNode, ANewNode, True);
  SetFocusedNode(ANewNode, True);
  AfterNodeMoved((ASourceNode.TreeList <> RibbonTreeList) and not AGroupWasHidden);
  if ASourceNode.TreeList = RibbonTreeList then
    RemoveNode(ASourceNode);
end;

function TdxRibbonCustomizationFormHelper.GetContextNode(ASourceNode: TcxTreeListNode): TcxTreeListNode;
begin
  Result := ASourceNode;
  while Result.Level > dxrcfContextLevel do
    Result := Result.Parent;
end;

function TdxRibbonCustomizationFormHelper.GetTargetNodeForMovingDown(ASourceNode: TcxTreeListNode): TcxTreeListNode;
var
  ALevel: Integer;
begin
  Result := ASourceNode;
  if Result <> nil then
  begin
    ALevel := Result.Level;
    repeat
      Result := Result.GetNext;
      while (Result <> nil) and ((Result.Level + 1) <> ALevel) do
        Result := Result.GetNext;
    until (Result = nil) or CanMoveNodeTo(ASourceNode, Result);
  end;
end;

function TdxRibbonCustomizationFormHelper.GetTargetNodeForMovingUp(ASourceNode: TcxTreeListNode): TcxTreeListNode;
var
  ALevel: Integer;
begin
  Result := ASourceNode;
  if Result <> nil then
  begin
    ALevel := Result.Level;
    repeat
      Result := Result.GetPrev;
      while (Result <> nil) and (Result.Level <> ALevel) and (((Result.Level + 1) <> ALevel) or (Result = ASourceNode.Parent)) do
        Result := Result.GetPrev;
    until (Result = nil) or CanMoveNodeTo(ASourceNode, Result);
  end;
end;

procedure TdxRibbonCustomizationFormHelper.PopulateCustomTabsAndGroups(ATargetTreeList: TcxTreeList);
var
  ACustomGroupsNode: TcxTreeListNode;
  ATab: TdxRibbonTab; 
  AGroup: TdxRibbonTabGroup;
  I, J: Integer;
begin
  ATargetTreeList.OptionsView.ShowRoot := True;
  ACustomGroupsNode := ATargetTreeList.Add(nil);
  ACustomGroupsNode.Texts[0] := cxGetResourceString(@sdxRibbonCustomizationFormCustomGroups);
  for I := 0 to Ribbon.TabCount - 1 do
  begin
    ATab := Ribbon.Tabs[I];
    if not ATab.IsPredefined then
      CreateTabNode(ATab, nil, ATargetTreeList).Expand(False);
    for J := 0 to ATab.Groups.Count - 1 do
    begin
      AGroup := ATab.Groups[J];
      if (AGroup.ToolBar <> nil) and not AGroup.ToolBar.IsPredefined then
        PopulateGroupNodeContent(ATargetTreeList.AddChild(ACustomGroupsNode, AGroup), ATargetTreeList);
    end;
  end;
  ACustomGroupsNode.MoveTo(ATargetTreeList.Root.Items[ATargetTreeList.Root.Count - 1], tlamAdd);
  ACustomGroupsNode.Expand(False);
end;

procedure TdxRibbonCustomizationFormHelper.PopulateGroupNodeContent(ANode: TcxTreeListNode; ATargetTreeList: TcxTreeList);
var
  AItemNode: TcxTreeListNode;
  AItemLinks: TdxBarItemLinks;
  AItemLink: TdxBarItemLink;
  AGroup: TdxRibbonTabGroup;
  I: Integer;
begin
  AGroup := TdxRibbonTabGroup(ANode.Data);
  ANode.Texts[0] := AGroup.Caption;
  ANode.Enabled := IsGroupCustomizingAllowed(ANode);
  AItemLinks := AGroup.ToolBar.ItemLinks;
  for I := 0 to AItemLinks.Count - 1 do
  begin
    AItemLink := AItemLinks[I];
    AItemNode := ATargetTreeList.AddChild(ANode, TdxRibbonCustomizationFormItemData.Create(AItemLink.Item, AItemLink));
    AItemNode.Texts[0] := AItemLink.Caption;
    AItemNode.Enabled := ANode.Enabled;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.PopulateTabNodeContent(ANode: TcxTreeListNode; ATargetTreeList: TcxTreeList;
  const AIsReseting: Boolean = False);
var
  ATab: TdxRibbonTab;
  AGroup: TdxRibbonTabGroup;
  I: Integer;
begin
  ATab := TdxRibbonTab(ANode.Data);
  for I := 0 to ATab.Groups.Count - 1 do
  begin
    AGroup := ATab.Groups[I];
    if (AGroup.ToolBar = nil) or ((ATargetTreeList = CommandsTreeList) and not AGroup.ToolBar.IsPredefined) or
      ((ATargetTreeList = RibbonTreeList) and ((AIsReseting and not AGroup.ToolBar.IsPredefined) or
      not TdxRibbonTabGroupAccess(AGroup).IsActuallyVisible)) then
      Continue;
    PopulateGroupNodeContent(ATargetTreeList.AddChild(ANode, AGroup), ATargetTreeList);
    if (ATargetTreeList = RibbonTreeList) and AIsReseting and AGroup.IsToolBarShared then
      RestoreGroupNodesForSharedToolBar(AGroup);
  end;
end;

procedure TdxRibbonCustomizationFormHelper.RestoreGroupNodesForSharedToolBar(ASenderGroup: TdxRibbonTabGroup);
var
  ATabIndex, AGroupIndex: Integer;
  ATab: TdxRibbonTab;
  AGroup: TdxRibbonTabGroup;
  AContextNode, ATabNode: TcxTreeListNode;
begin
  RibbonTreeList.BeginUpdate;
  try
    for ATabIndex := 0 to Ribbon.TabCount - 1 do
    begin
      ATab := Ribbon.Tabs[ATabIndex];
      AContextNode := GetNodeFor(ATab.Context, RibbonTreeList.Root);
      ATabNode := GetNodeFor(ATab, AContextNode);
      if (ATabNode <> nil) and (ATab <> ASenderGroup.Tab) and ATab.Groups.ContainsToolBar(ASenderGroup.ToolBar) and
        (GetNodeFor(ASenderGroup, ATabNode) = nil) then
        for AGroupIndex := 0 to ATab.Groups.Count - 1 do
        begin
          AGroup := ATab.Groups[AGroupIndex];
          if (AGroup.ToolBar <> nil) and (AGroup.ToolBar = ASenderGroup.ToolBar) then
          begin
            PopulateGroupNodeContent(RibbonTreeList.AddChild(ATabNode, AGroup), RibbonTreeList);
            Break;
          end;
        end;
    end;
  finally
    RibbonTreeList.EndUpdate;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.RestoreTabNodeExpanding(ANode: TcxTreeListNode; AStorage: TList);
var
  AGroupNode: TcxTreeListNode;
  I: Integer;
begin
  for I := 0 to ANode.Count - 1 do
  begin
    AGroupNode := ANode.Items[I];
    if AStorage.IndexOf(AGroupNode.Data) <> -1 then
      AGroupNode.Expand(False);
  end;
  AStorage.Clear;
end;

procedure TdxRibbonCustomizationFormHelper.SetFocusedNode(ANode: TcxTreeListNode; const AExpandNodeParents: Boolean = False);
var
  AParentNode: TcxTreeListNode;
begin
  inherited SetFocusedNode(ANode, AExpandNodeParents);
  if AExpandNodeParents then
  begin
    AParentNode := ANode;
    while GetNodeLevel(AParentNode) > dxrcfContextLevel do
    begin
      AParentNode := AParentNode.Parent;
      AParentNode.Expand(False);
    end;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.StoreTabNodeExpanding(ANode: TcxTreeListNode; AStorage: TList);
var
  AGroupNode: TcxTreeListNode;
  I: Integer;
begin
  for I := 0 to ANode.Count - 1 do
  begin
    AGroupNode := ANode.Items[I];
    if (AGroupNode.Data <> nil) and AGroupNode.Expanded then
      AStorage.Add(AGroupNode.Data);
  end;
end;

procedure TdxRibbonCustomizationFormHelper.SynchronizeMatchingGroupNodesWith(ANode: TcxTreeListNode);
var
  AContextIndex, ATabIndex, AGroupIndex: Integer;
  AFocusedNode, AContextNode, ATabNode, AGroupNode: TcxTreeListNode;
  AIsGroupExpanded: Boolean;
begin
  AFocusedNode := FocusedNode;
  RibbonTreeList.BeginUpdate;
  try
    for AContextIndex := 0 to RibbonTreeList.Root.Count - 1 do
    begin
      AContextNode := RibbonTreeList.Root.Items[AContextIndex];
      for ATabIndex := 0 to AContextNode.Count - 1 do
      begin
        ATabNode := AContextNode.Items[ATabIndex];
        for AGroupIndex := 0 to ATabNode.Count - 1 do
        begin
          AGroupNode := ATabNode.Items[AGroupIndex];
          if IsSameElement(AGroupNode, ANode) and (AGroupNode <> ANode) then
          begin
            AIsGroupExpanded := AGroupNode.Expanded;
            AGroupNode.DeleteChildren;
            CopyNode(ANode, AGroupNode, True, False);
            AGroupNode.Expanded := AIsGroupExpanded;
          end;
        end;
      end;
    end;
  finally
    RibbonTreeList.EndUpdate;
  end;
  AFocusedNode.Focused := True;
end;

procedure TdxRibbonCustomizationFormHelper.CheckAndAddContextNodeToRibbon(ANode: TcxTreeListNode);
var
  AContext: TdxRibbonContext;
begin
  if ANode.Data = nil then
  begin
    AContext := Ribbon.Contexts.Add;
    AContext.Visible := True;
    ANode.Data := AContext;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.CheckAndAddGroupNodeToRibbon(ANode: TcxTreeListNode);
var
  AGroup: TdxRibbonTabGroup;
  ATab: TdxRibbonTab;
begin
  ATab := TdxRibbonTab(ANode.Parent.Data);
  AGroup := TdxRibbonTabGroup(ANode.Data);
  if AGroup = nil then
  begin
    AGroup := ATab.Groups.Add;
    AGroup.ToolBar := Ribbon.BarManager.AddToolBar;
    ANode.Data := AGroup;
  end
  else
    TdxRibbonTabGroupAccess(AGroup).MoveToTab(ATab);
  AGroup.ToolBar.Visible := True;
end;

procedure TdxRibbonCustomizationFormHelper.CheckAndAddItemNodeToRibbon(ANode: TcxTreeListNode);
var
  AItemLinks: TdxBarItemLinks;
  AItem: TdxBarItem;
begin
  AItemLinks := TdxRibbonTabGroup(ANode.Parent.Data).ToolBar.ItemLinks;
  AItem := GetItemData(ANode).Item;
  if (GetItemData(ANode).ItemLink = nil) or not AItemLinks.HasItem(AItem) then
  begin
    GetItemData(ANode).ItemLink := AItemLinks.Add(AItem);
    SynchronizeMatchingGroupNodesWith(ANode.Parent);
  end;
end;

procedure TdxRibbonCustomizationFormHelper.CheckAndAddTabNodeToRibbon(ANode: TcxTreeListNode);
var
  AContext: TdxRibbonContext;
  ATab: TdxRibbonTab;
begin
  AContext := TdxRibbonContext(ANode.Parent.Data);
  ATab := TdxRibbonTab(ANode.Data);
  if ATab = nil then
    ATab := Ribbon.Tabs.Add;
  ATab.Context := AContext;
  ATab.Visible := ANode.Checked;
  ANode.Data := ATab;
end;

procedure TdxRibbonCustomizationFormHelper.CheckAndDeleteContextsFromRibbon;
var
  AContexts: TdxRibbonContexts;
  I: Integer;
begin
  AContexts := Ribbon.Contexts;
  AContexts.BeginUpdate;
  try
    for I := AContexts.Count - 1 downto 0 do
    begin
      CheckAndDeleteTabsFromRibbon(AContexts[I]);
      if GetNodeFor(AContexts[I], RibbonTreeList.Root) = nil then
        AContexts.Delete(I);
    end;
    CheckAndDeleteTabsFromRibbon(nil);
  finally
    AContexts.EndUpdate;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.CheckAndDeleteGroupsFromRibbon(AGroups: TdxRibbonTabGroups; ATabNode: TcxTreeListNode);
var
  AGroupNode: TcxTreeListNode;
  AGroup: TdxRibbonTabGroup;
  I: Integer;
begin
  AGroups.BeginUpdate;
  try
    for I := AGroups.Count - 1 downto 0 do
    begin
      AGroup := AGroups[I];
      if AGroup.ToolBar = nil then
        Continue;
      AGroupNode := GetNodeFor(AGroup, ATabNode);
      if AGroupNode <> nil then
        CheckAndDeleteItemLinksFromRibbon(AGroup.ToolBar.ItemLinks, AGroupNode)
      else
        AGroup.ToolBar.Visible := False;
    end;
  finally
    AGroups.EndUpdate;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.CheckAndDeleteItemLinksFromRibbon(AItemLinks: TdxBarItemLinks; AGroupNode: TcxTreeListNode);
var
  I: Integer;
begin
  AItemLinks.BeginUpdate;
  try
    for I := AItemLinks.Count - 1 downto 0 do
      if GetNodeFor(AItemLinks[I].Item, AGroupNode) = nil then
        AItemLinks.Delete(I);
  finally
    AItemLinks.EndUpdate;
  end;
end;

procedure TdxRibbonCustomizationFormHelper.CheckAndDeleteTabsFromRibbon(AContext: TdxRibbonContext);
var
  ATabs: TdxRibbonTabCollection;
  AContextNode, ATabNode: TcxTreeListNode;
  ATab: TdxRibbonTab;
  I: Integer;
begin
  ATabs := Ribbon.Tabs;
  AContextNode := GetNodeFor(AContext, RibbonTreeList.Root);
  ATabs.BeginUpdate;
  try
    for I := ATabs.Count - 1 downto 0 do
    begin
      ATab := ATabs[I];
      if ATab.Context = AContext then
      begin
        ATabNode := GetNodeFor(ATab, AContextNode);
        CheckAndDeleteGroupsFromRibbon(ATab.Groups, ATabNode);
        if ATabNode = nil then
          ATabs.Delete(I);
      end;
    end;
  finally
    ATabs.EndUpdate;
  end;
end;

function TdxRibbonCustomizationFormHelper.IsGroupCustomizingAllowed(ANode: TcxTreeListNode): Boolean;
begin
  Result := (ANode <> nil) and ((ANode.Data = nil) or (TdxRibbonTabGroup(ANode.Data).ToolBar = nil) or
    TdxRibbonTabGroup(ANode.Data).ToolBar.AllowCustomizing);
end;

function TdxRibbonCustomizationFormHelper.IsGroupHiddenInTab(AGroupNode, ATabNode: TcxTreeListNode): Boolean;
var
  AGroup: TdxRibbonTabGroup;
  ATab: TdxRibbonTab;
  I: Integer;
begin
  Result := (AGroupNode.Level = dxrcfGroupLevel) and (ATabNode.Level = dxrcfTabLevel) and (AGroupNode.Data <> nil) and
    (ATabNode.Data <> nil);
  if Result then
  begin
    AGroup := TdxRibbonTabGroup(AGroupNode.Data);
    ATab := TdxRibbonTab(ATabNode.Data);
    Result := (AGroup.ToolBar <> nil) and ATab.Groups.ContainsToolBar(AGroup.ToolBar);
    if Result then
      for I := 0 to ATabNode.Count - 1 do
        if IsSameElement(ATabNode.Items[I], AGroup) then
        begin
          Result := False;
          Break;
        end;
  end;
end;

function TdxRibbonCustomizationFormHelper.IsParentTabTheSame(ASourceGroupNode, ATargetNode: TcxTreeListNode): Boolean;
var
  ATargetTabNode: TcxTreeListNode;
begin
  ATargetTabNode := ATargetNode;
  while (GetNodeLevel(ATargetTabNode) <> dxrcfTabLevel) do
    ATargetTabNode := ATargetTabNode.Parent;
  Result := ASourceGroupNode.Parent = ATargetTabNode;
end;

function TdxRibbonCustomizationFormHelper.IsRibbonGroupNodeWithSharedToolBar(ANode: TcxTreeListNode): Boolean;
begin
  Result := (ANode.TreeList = RibbonTreeList) and (GetNodeLevel(ANode) = dxrcfGroupLevel) and (ANode.Data <> nil) and
    TdxRibbonTabGroup(ANode.Data).IsToolBarShared;
end;

function TdxRibbonCustomizationFormHelper.IsSourceNodeValid(ASourceNode: TcxTreeListNode): Boolean;
begin
  Result := inherited IsSourceNodeValid(ASourceNode) and (GetNodeLevel(ASourceNode) <> dxrcfContextLevel) and
    ((GetNodeLevel(ASourceNode) <> dxrcfItemLevel) or (ASourceNode.TreeList <> RibbonTreeList) or ASourceNode.Enabled) and
    not ((ASourceNode.TreeList = CommandsTreeList) and (CommandsComboBox.ItemIndex = dxrcfCommandsCustomTabsAndGroups) and
    (ASourceNode.Data = nil));
end;

function TdxRibbonCustomizationFormHelper.IsTargetNodeValid(ASourceNode, ATargetNode: TcxTreeListNode): Boolean;
begin
  Result := inherited IsTargetNodeValid(ASourceNode, ATargetNode) and
    (GetNodeLevel(ATargetNode) >= GetNodeLevel(ASourceNode) - 1) and
    (ATargetNode.Enabled or (GetNodeLevel(ASourceNode) = dxrcfGroupLevel));
end;

{ TdxRibbonQATCustomizationFormHelper }

procedure TdxRibbonQATCustomizationFormHelper.BeginApplyChanges;
begin
  Ribbon.BarManager.BeginUpdate;
end;

procedure TdxRibbonQATCustomizationFormHelper.CreateAddedElements;
var
  ANode: TcxTreeListNode;
  AItemLinks: TdxBarItemLinks;
  AItem: TdxBarItem;
  I: Integer;
begin
  for I := 0 to RibbonTreeList.Root.Count - 1 do
  begin
    ANode := RibbonTreeList.Root.Items[I];
    AItemLinks := Ribbon.QuickAccessToolbar.Toolbar.ItemLinks;
    AItem := GetItemData(ANode).Item;
    if (GetItemData(ANode).ItemLink = nil) or not AItemLinks.HasItem(AItem) then
      GetItemData(ANode).ItemLink := AItemLinks.Add(AItem);
  end;
end;

procedure TdxRibbonQATCustomizationFormHelper.DeleteRemovedElements;
var        
  AItemLinks: TdxBarItemLinks;
  I: Integer;
begin            
  AItemLinks := Ribbon.QuickAccessToolbar.Toolbar.ItemLinks;
  AItemLinks.BeginUpdate;
  try
    for I := AItemLinks.Count - 1 downto 0 do
      if GetNodeFor(AItemLinks[I].Item, RibbonTreeList.Root) = nil then
        AItemLinks.Delete(I);
  finally
    AItemLinks.EndUpdate;
  end;
end;

procedure TdxRibbonQATCustomizationFormHelper.EndApplyChanges;
begin
  Ribbon.BarManager.EndUpdate;
end;

procedure TdxRibbonQATCustomizationFormHelper.ReorderElements;
var
  ANode: TcxTreeListNode;
  I: Integer;
begin
  for I := 0 to RibbonTreeList.Root.Count - 1 do
  begin
    ANode := RibbonTreeList.Root.Items[I];
    GetItemData(ANode).ItemLink.Index := ANode.Index;
  end;
end;

procedure TdxRibbonQATCustomizationFormHelper.SynchronizeElementCaptions;
begin
  //do nothing
end;

procedure TdxRibbonQATCustomizationFormHelper.ChangeQATPosition(const AShowBelowRibbon: Boolean);
const
  QATPositionMap: array [Boolean] of TdxQuickAccessToolbarPosition = (qtpAboveRibbon, qtpBelowRibbon);
begin
  Ribbon.QuickAccessToolbar.Position := QATPositionMap[AShowBelowRibbon];
end;

procedure TdxRibbonQATCustomizationFormHelper.DropMovedNodeTo(ATargetNode: TcxTreeListNode; const ADropPointY: Integer);
var
  AViewData: TcxTreeListNodeViewData;
begin
  if CanMoveNodeTo(MovedNode, ATargetNode) then
    if IsTargetNodeValid(MovedNode, ATargetNode) then
    begin
      AViewData := TcxTreeListNodeAccess(ATargetNode).ViewData;
      DoMoveNode(MovedNode, ATargetNode, (AViewData <> nil) and (ADropPointY < cxRectCenter(AViewData.GetRealBounds).Y));
    end
    else
      DoMoveNode(MovedNode, ATargetNode, False, True);
end;

function TdxRibbonQATCustomizationFormHelper.GetNodeLevel(ANode: TcxTreeListNode): Integer;
begin
  Result := dxrcfItemLevel;
end;

function TdxRibbonQATCustomizationFormHelper.IsQATBelowRibbon: Boolean;
begin
  Result := Ribbon.QuickAccessToolbar.Position = qtpBelowRibbon;
end;

procedure TdxRibbonQATCustomizationFormHelper.MoveFocusedNodeDown;
var
  ASourceNode, ATargetNode: TcxTreeListNode;
begin
  ASourceNode := FocusedNode;
  ATargetNode := GetTargetNodeForMovingDown(ASourceNode);
  if ATargetNode <> nil then
    DoMoveNode(ASourceNode, ATargetNode, False);
end;

procedure TdxRibbonQATCustomizationFormHelper.MoveFocusedNodeUp;
var
  ASourceNode, ATargetNode: TcxTreeListNode;
begin
  ASourceNode := FocusedNode;
  ATargetNode := GetTargetNodeForMovingUp(ASourceNode);
  if ATargetNode <> nil then
    DoMoveNode(ASourceNode, ATargetNode, True);
end;

procedure TdxRibbonQATCustomizationFormHelper.PopulateCommandsComboBoxContent;
var
  AItems: TStrings;
  I: Integer;
begin
  AItems := CommandsComboBox.Properties.Items;
  LockContentUpdating;
  AItems.BeginUpdate;
  try
    AItems.Clear;
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormCommandsNotInTheRibbon));
    AItems.Add(cxGetResourceString(@sdxRibbonCustomizationFormAllCommands));
    PopulateContextTabs(nil, AItems);
    for I := 0 to Ribbon.Contexts.Count - 1 do
      PopulateContextTabs(Ribbon.Contexts[I], AItems);
    CommandsComboBox.ItemIndex := dxrcfCommandsAllCommands;
  finally
    AItems.EndUpdate;
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonQATCustomizationFormHelper.PopulateCommandsTreeListContent;
begin
  LockContentUpdating;
  CommandsTreeList.BeginUpdate;
  try
    CommandsTreeList.Clear;
    CommandsTreeList.OptionsBehavior.IncSearch := True;
    case CommandsComboBox.ItemIndex of
      dxrcfCommandsCommandsNotInTheRibbon:
        PopulateAllItems(Ribbon.BarManager, CommandsTreeList, True);
      dxrcfCommandsAllCommands:
        PopulateAllItems(Ribbon.BarManager, CommandsTreeList);
      else
        PopulateTabItems(TdxRibbonTab(CommandsComboBox.ItemObject), CommandsTreeList);
    end;
  finally
    CommandsTreeList.EndUpdate;
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonQATCustomizationFormHelper.PopulateRibbonComboBoxContent;
begin
  //do nothing 
end;

procedure TdxRibbonQATCustomizationFormHelper.PopulateRibbonTreeListContent(const AIsReseting: Boolean = False);
var
  AItemLinks: TdxBarItemLinks;
  AItemLink: TdxBarItemLink;
  AItemNode: TcxTreeListNode;
  I: Integer;
begin
  LockContentUpdating;
  RibbonTreeList.BeginUpdate;
  try
    RibbonTreeList.Clear;
    AItemLinks := Ribbon.QuickAccessToolbar.Toolbar.ItemLinks;
    for I := 0 to AItemLinks.Count - 1 do
    begin
      AItemLink := AItemLinks[I];
      AItemNode := RibbonTreeList.AddChild(nil, TdxRibbonCustomizationFormItemData.Create(AItemLink.Item, AItemLink));
      AItemNode.Texts[0] := AItemLink.Caption;
    end;
  finally
    RibbonTreeList.EndUpdate;
    UnlockContentUpdating;
  end;
end;

procedure TdxRibbonQATCustomizationFormHelper.RemoveNode(ANode: TcxTreeListNode);
begin
  ANode.Delete;
end;

function TdxRibbonQATCustomizationFormHelper.CanMoveNodeTo(ASourceNode, ATargetNode: TcxTreeListNode): Boolean;
var
  I: Integer;
begin
  Result := IsSourceNodeValid(ASourceNode) and ((ATargetNode = nil) or IsTargetNodeValid(ASourceNode, ATargetNode));
  if Result then
    for I := 0 to RibbonTreeList.Root.Count - 1 do
      if IsSameElement(ASourceNode, RibbonTreeList.Root.Items[I]) then
      begin
        Result := False;
        Break;
      end;
end;

procedure TdxRibbonQATCustomizationFormHelper.AfterNodeMoved(const AConvertNodeData: Boolean);
begin
  if AConvertNodeData then
    ConvertItemNodeToNew(FocusedNode);
end;

procedure TdxRibbonQATCustomizationFormHelper.DoMoveNode(ASourceNode, ATargetNode: TcxTreeListNode;
  const AInsertBeforeTargetNode: Boolean; const ATargetNodeIsParent: Boolean = False);
var
  ANewNode: TcxTreeListNode;
begin
  if ATargetNode <> nil then
    if AInsertBeforeTargetNode then
      ANewNode := ATargetNode.Parent.InsertChild(ATargetNode)
    else
      if ATargetNode.getNextSibling = nil then
        ANewNode := ATargetNode.Parent.AddChild
      else
        ANewNode := ATargetNode.Parent.InsertChild(ATargetNode.getNextSibling)
  else
    ANewNode := RibbonTreeList.Root.AddChild;
  CopyNode(ASourceNode, ANewNode, False);
  SetFocusedNode(ANewNode);
  AfterNodeMoved(ASourceNode.TreeList <> RibbonTreeList);
  if ASourceNode.TreeList = RibbonTreeList then
    RemoveNode(ASourceNode);
end;

function TdxRibbonQATCustomizationFormHelper.GetTargetNodeForMovingDown(ASourceNode: TcxTreeListNode): TcxTreeListNode;
begin
  Result := ASourceNode.getNextSibling;
end;

function TdxRibbonQATCustomizationFormHelper.GetTargetNodeForMovingUp(ASourceNode: TcxTreeListNode): TcxTreeListNode;
begin
  Result := ASourceNode.getPrevSibling;
end;

procedure TdxRibbonQATCustomizationFormHelper.PopulateContextTabs(AContext: TdxRibbonContext; AItems: TStrings);
var
  AContextPrefix, ATabSuffix: string;
  I: Integer;
begin
  if AContext <> nil then
    AContextPrefix := AContext.Caption + cxGetResourceString(@sdxRibbonCustomizationFormDelimiterContextTab)
  else
    AContextPrefix := EmptyStr;
  ATabSuffix := cxGetResourceString(@sdxRibbonCustomizationFormTabSuffix);
  for I := 0 to Ribbon.TabCount - 1 do
    if Ribbon.Tabs[I].Context = AContext then
      AItems.AddObject(AContextPrefix + Ribbon.Tabs[I].Caption + ATabSuffix, Ribbon.Tabs[I]);
end;

procedure TdxRibbonQATCustomizationFormHelper.PopulateTabItems(ATab: TdxRibbonTab; ATargetTreeList: TcxTreeList);
var
  AGroup: TdxRibbonTabGroup;
  AItemLink: TdxBarItemLink;
  I, J: Integer;
begin
  for I := 0 to ATab.Groups.Count - 1 do
  begin
    AGroup := ATab.Groups[I];
    for J := 0 to AGroup.ToolBar.ItemLinks.Count - 1 do
    begin
      AItemLink := AGroup.ToolBar.ItemLinks[J];
      ATargetTreeList.Add(nil, TdxRibbonCustomizationFormItemData.Create(AItemLink.Item, AItemLink)).Texts[0] := AItemLink.Caption;
    end;
  end;
end;

end.
