{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressGaugeControl                                      }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSGAUGECONTROL AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGaugeCustomScale;

{$I cxVer.inc}

interface

uses
  Windows, Graphics, Classes,
  cxClasses, dxCore, cxGeometry, cxGraphics, dxCoreGraphics, dxGDIPlusClasses, dxXMLDoc, dxCompositeShape,
  dxShapePrimitives;

const
  dxGaugeAfricaSunsetStyleName = 'AfricaSunset';
  dxGaugeCleanWhiteStyleName = 'CleanWhite';
  dxGaugeDarkNightStyleName = 'DarkNight';
  dxGaugeDeepFireStyleName = 'DeepFire';
  dxGaugeIceColdZoneStyleName = 'IceColdZone';
  dxGaugeMechanicalStyleName = 'Mechanical';
  dxGaugeMilitaryStyleName = 'Military';
  dxGaugeSportCarStyleName = 'SportCar';
  dxGaugeWhiteStyleName = 'White';

type
  TdxGaugeCustomScale = class;
  TdxGaugeCustomScaleClass = class of TdxGaugeCustomScale;
  TdxGaugeCustomScaleDefaultParameters = class;
  TdxGaugeCustomScaleDefaultParametersClass = class of TdxGaugeCustomScaleDefaultParameters;
  TdxGaugeCustomScaleParameters = class;
  TdxGaugeCustomScaleParametersClass = class of TdxGaugeCustomScaleParameters;
  TdxGaugeCustomScaleViewInfo = class;
  TdxGaugeCustomScaleViewInfoClass = class of TdxGaugeCustomScaleViewInfo;
  TdxGaugeScaleStyle = class;
  TdxGaugeScaleStyleClass = class of TdxGaugeScaleStyle;
  TdxGaugeCustomScaleStyleReader = class;
  TdxGaugeCustomScaleStyleReaderClass = class of TdxGaugeCustomScaleStyleReader;

  TdxGaugeScaleCalculatableLayer = (sctAllLayersWithoutChecks, sctAllLayers, sctBackgroundLayer, sctElementsLayer,
    sctValueIndicatorLayer, sctSpindleCapLayer);
  TdxGaugeScaleCalculatableLayers = set of TdxGaugeScaleCalculatableLayer;

  TdxGaugeElementType = (etBackground1, etBackground2, etDigitalBackgroundStart, etDigitalBackgroundMiddle,
    etDigitalBackgroundEnd, etTick1, etTick2, etTick3, etTick4, etTick5, etSpindleCap, etNeedle);
  TdxGaugeScalePositionType = (sptFactor, sptPixels);
  TdxGaugeScaleType = (stCircularScale, stDigitalScale);

  TdxGaugeScaleLayerDrawProc = procedure(AGPGraphics: TdxGPGraphics) of object;

  { TdxGaugeScaleTickDrawParameters }

  TdxGaugeScaleTickDrawParameters = record
    Image: TGraphic;
    Offset: Single;
    ScaleFactor: TdxPointF;
    ShowLabel: Boolean;
    ShowTick: Boolean;
    Size: TdxSizeF;
  end;

  { TdxGaugeCustomScaleDefaultParameters }

  TdxGaugeCustomScaleDefaultParameters = class(TObject);

  { TdxGaugeCustomScaleParameters }

  TdxGaugeCustomScaleParameters = class
  public
    Value: Variant; 

    AnchorScaleIndex: Integer;
    LayerCount: Integer;
    ShowBackground: Boolean;
    Visible: Boolean;

    CenterPosition: TPoint;
    CenterPositionFactor: TdxPointF;
    CenterPositionType: TdxGaugeScalePositionType;

    Height: Integer;
    HeightFactor: Single;
    Width: Integer;
    WidthFactor: Single;
    CachBounds: TRect;
    CalculatableLayers: TdxGaugeScaleCalculatableLayers;

    procedure Assign(ASource: TdxGaugeCustomScaleParameters); virtual;
  end;

  { TdxGaugeCustomScaleViewInfo }

  TdxGaugeCustomScaleViewInfo = class
  private
    FBackground: TdxCompositeShape;
    FScaleFactor: TdxPointF;
    FScaleParameters: TdxGaugeCustomScaleParameters;
    FStyle: TdxGaugeScaleStyle;

    procedure SetStyle(AStyle: TdxGaugeScaleStyle);

    function CanDraw: Boolean;
    function GetCenter(const ABounds: TdxRectF): TdxPointF;
    function GetScaleDefaultParameters: TdxGaugeCustomScaleDefaultParameters;
    procedure CachingBackgroundLayer;
    procedure CalculateScaleFactor(const ABounds: TdxRectF);
    procedure CheckBounds(const ABounds: TdxRectF);
    procedure CreateStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string);
    procedure RecreateStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string);
  protected
    FBackgroundLayer: TcxBitmap32;
    FBounds: TdxRectF;
    FPreviousBounds: TdxRectF;
    FPreviousCachBounds: TdxRectF;

    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; virtual;
    function GetScaleSize(const ABounds: TdxRectF): TdxSizeF; virtual;
    procedure Calculate(AScaleParameters: TdxGaugeCustomScaleParameters; const ABounds: TdxRectF); virtual;
    procedure CalculateBounds(const ABounds: TdxRectF); virtual;
    procedure DestroyCachedLayers; virtual;
    procedure DrawBackground(AGPGraphics: TdxGPGraphics); virtual;
    procedure LoadScaleElements; virtual;

    function CachingLayer(ADrawProc: TdxGaugeScaleLayerDrawProc): TcxBitmap32;
    function GetElementImage(AType: TdxGaugeElementType): TGraphic;
    function GetElementImageSize(AImage: TGraphic; AImageScaleFactor: TdxPointF): TdxSizeF;
    function NeedCreateLayerCach(AIsShowableLayer: Boolean): Boolean;
    function NeedRecreateLayerCach(ALayer: TdxGaugeScaleCalculatableLayer): Boolean;
    procedure DrawBackgroundLayer(AGPGraphics: TdxGPGraphics);
    procedure DrawCachedLayer(AGPGraphics: TdxGPGraphics; ALayerBitmap: TcxBitmap32);

    property Bounds: TdxRectF read FBounds;
    property ScaleDefaultParameters: TdxGaugeCustomScaleDefaultParameters read GetScaleDefaultParameters;
    property ScaleFactor: TdxPointF read FScaleFactor;
    property ScaleParameters: TdxGaugeCustomScaleParameters read FScaleParameters;
    property Style: TdxGaugeScaleStyle read FStyle write SetStyle;
  public
    constructor Create(AScaleType: TdxGaugeScaleType; const AStyleName: string); virtual;
    destructor Destroy; override;
  end;

  { TdxGaugeCustomScale }

  TdxGaugeCustomScale = class(TcxComponentCollectionItem)
  private
    FStyleName: string;

    function GetAnchorScaleIndex: Integer;
    function GetBounds: TdxRectF;
    function GetCenterPositionType: TdxGaugeScalePositionType;
    function GetCenterPositionX: Integer;
    function GetCenterPositionFactorX: Single;
    function GetCenterPositionY: Integer;
    function GetCenterPositionFactorY: Single;
    function GetLayerCount: Integer;
    function GetHeight: Integer;
    function GetHeightFactor: Single;
    function GetShowBackground: Boolean;
    function GetVisible: Boolean;
    function GetWidth: Integer;
    function GetWidthFactor: Single;
    procedure SetAnchorScaleIndex(const AValue: Integer);
    procedure SetCenterPositionType(const AValue: TdxGaugeScalePositionType);
    procedure SetCenterPositionX(const AValue: Integer);
    procedure SetCenterPositionFactorX(const AValue: Single);
    procedure SetCenterPositionY(const AValue: Integer);
    procedure SetCenterPositionFactorY(const AValue: Single);
    procedure SetHeight(const AValue: Integer);
    procedure SetHeightFactor(const AValue: Single);
    procedure SetShowBackground(const AValue: Boolean);
    procedure SetStyleName(const AValue: string);
    procedure SetVisible(const AValue: Boolean);
    procedure SetWidth(const AValue: Integer);
    procedure SetWidthFactor(const AValue: Single);

    procedure DoAssign(AScale: TdxGaugeCustomScale);

    function IsCenterPositionFactorXStored: Boolean;
    function IsCenterPositionFactorYStored: Boolean;
    function IsHeightFactorStored: Boolean;
    function IsWidthFactorStored: Boolean;
  protected
    FScaleParameters: TdxGaugeCustomScaleParameters;
    FViewInfo: TdxGaugeCustomScaleViewInfo;

    function GetCollectionFromParent(AParent: TComponent): TcxComponentCollection; override;

    class function GetScaleType: TdxGaugeScaleType; virtual;
    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; virtual;
    function GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass; virtual;
    procedure ApplyStyleParameters; virtual;
    procedure Calculate(const AScaleBounds: TdxRectF; const ACachBounds: TRect); virtual;
    procedure InternalDrawLayer(AGPGraphics: TdxGPGraphics; AIndex: Integer); virtual;
    procedure InternalRestoreStyleParameters; virtual;

    procedure DrawBackgroundLayer(AGPGraphics: TdxGPGraphics);
    procedure DrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer);
    procedure ScaleChanged(ACalculatableLayer: TdxGaugeScaleCalculatableLayer = sctAllLayers);

    property AnchorScaleIndex: Integer read GetAnchorScaleIndex write SetAnchorScaleIndex default -1;
    property Bounds: TdxRectF read GetBounds;
    property CenterPositionX: Integer read GetCenterPositionX write SetCenterPositionX default 0;
    property CenterPositionFactorX: Single read GetCenterPositionFactorX write SetCenterPositionFactorX
      stored IsCenterPositionFactorXStored;
    property CenterPositionY: Integer read GetCenterPositionY write SetCenterPositionY default 0;
    property CenterPositionFactorY: Single read GetCenterPositionFactorY write SetCenterPositionFactorY
      stored IsCenterPositionFactorYStored;
    property CenterPositionType: TdxGaugeScalePositionType read GetCenterPositionType
      write SetCenterPositionType default sptFactor;
    property Height: Integer read GetHeight write SetHeight default 0;
    property HeightFactor: Single read GetHeightFactor write SetHeightFactor stored IsHeightFactorStored;
    property LayerCount: Integer read GetLayerCount;
    property ShowBackground: Boolean read GetShowBackground write SetShowBackground default True;
    property StyleName: string read FStyleName write SetStyleName;
    property Visible: Boolean read GetVisible write SetVisible default True;
    property Width: Integer read GetWidth write SetWidth default 0;
    property WidthFactor: Single read GetWidthFactor write SetWidthFactor stored IsWidthFactorStored;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Assign(ASource: TPersistent); override;
    procedure LoadStyleFromFile(const AFileName: string);
    procedure RestoreStyleParameters;
  end;

  { TdxGaugeScaleCollection }

  TdxGaugeScaleCollection = class(TcxComponentCollection)
  private
    function GetScale(AIndex: Integer): TdxGaugeCustomScale;
    procedure SetScale(AIndex: Integer; const AValue: TdxGaugeCustomScale);
  public
    function GetItemPrefixName: string; override;
    procedure Assign(ASource: TPersistent); override;

    function Add(AScaleClass: TdxGaugeCustomScaleClass): TdxGaugeCustomScale;

    property Scales[Index: Integer]: TdxGaugeCustomScale read GetScale write SetScale; default;
  end;

  { TdxGaugeScaleStyle }

  TdxGaugeScaleStyle = class
  private
    FDefaultParameters: TdxGaugeCustomScaleDefaultParameters;
    FElements: array[TdxGaugeElementType] of TdxCompositeShape;
    FName: string;
    FScaleType: TdxGaugeScaleType;
    FShapes: TdxCompositeShape;
  protected
    property DefaultParameters: TdxGaugeCustomScaleDefaultParameters read FDefaultParameters write FDefaultParameters;
    property Name: string read FName write FName;
    property Shapes: TdxCompositeShape read FShapes write FShapes;
    property ScaleType: TdxGaugeScaleType read FScaleType;
  public
    constructor Create(AScaleType: TdxGaugeScaleType; const AStyleName: string);
    destructor Destroy; override;

    function GetElement(AElementType: TdxGaugeElementType): TdxCompositeShape;
  end;

  { TdxGaugeScaleStyleInfo }

  TdxGaugeScaleStyleInfo = class
  public
    Name: string;
    ResourceName: string;
    IsExternalStyle: Boolean;
    ScaleType: TdxGaugeScaleType;
  end;

  { TdxGaugeCustomScaleStyleReader }

  TdxGaugeCustomScaleStyleReader = class
  private
    function GetStyleRootNode(ADocument: TdxXMLDocument): TdxXMLNode;
    function ReadCompositeShape(ADocument: TdxXMLDocument): TdxCompositeShape;
    function ReadDefaultParameters(ADocument: TdxXMLDocument): TdxGaugeCustomScaleDefaultParameters;
  protected
    function GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass; virtual; abstract;
    procedure ReadParameters(ANode: TdxXMLNode; AParameters: TdxGaugeCustomScaleDefaultParameters); virtual; abstract;

    function GetAttributeValueAsAlphaColor(ANode: TdxXMLNode; const AAttributeName: AnsiString): TdxAlphaColor;
    function GetAttributeValueAsBoolean(ANode: TdxXMLNode; const AAttributeName: AnsiString): Boolean;
    function GetAttributeValueAsColor(ANode: TdxXMLNode; const AAttributeName: AnsiString): TColor;
    function GetAttributeValueAsDouble(ANode: TdxXMLNode; const AAttributeName: AnsiString): Double;
    function GetAttributeValueAsElementType(ANode: TdxXMLNode; const AAttributeName: AnsiString): TdxGaugeElementType;
    function GetAttributeValueAsInteger(ANode: TdxXMLNode; const AAttributeName: AnsiString): Integer;
    function GetAttributeValueAsPointF(ANode: TdxXMLNode; const AAttributeName: AnsiString): TdxPointF;
    function GetAttributeValueAsScaleType(ANode: TdxXMLNode; const AAttributeName: AnsiString): TdxGaugeScaleType;
    function GetAttributeValueAsString(ANode: TdxXMLNode; const AAttributeName: AnsiString): string;

    function GetChildNode(ANode: TdxXMLNode; const AChildNodeName: AnsiString): TdxXMLNode; overload;
    function GetChildNode(ANode: TdxXMLNode; const AChildNodeName: AnsiString; out AChildNode: TdxXMLNode): Boolean; overload;
    function GetElementType(const ATypeName: string): TdxGaugeElementType;
    procedure ReadFontParameters(ANode: TdxXMLNode; AParameters: TdxGaugeCustomScaleDefaultParameters);
  public
    function GetStyle(ADocument: TdxXMLDocument): TdxGaugeScaleStyle;
  end;

  { TdxGaugeScaleStyleLoader }

  TdxGaugeScaleStyleLoader = class
  private
    function GetReaderClass(AScaleType: TdxGaugeScaleType): TdxGaugeCustomScaleStyleReaderClass;
    function GetScaleType(ADocument: TdxXMLDocument): TdxGaugeScaleType;
    function IsDocumentAvailable(ADocument: TdxXMLDocument): Boolean;
    procedure LoadFromStream(AStream: TStream; out AStyle: TdxGaugeScaleStyle);
  protected
    function GetScaleStyleInfo(const AFileName: string): TdxGaugeScaleStyleInfo;
    function LoadFromFile(const AFileName: string): TdxGaugeScaleStyle;
    function LoadFromResource(AInstance: THandle; const AResName: string): TdxGaugeScaleStyle;
    function LoadStyle(AStyleInfo: TdxGaugeScaleStyleInfo): TdxGaugeScaleStyle;
  end;

 { TdxGaugeScaleStyleFactory }

  TdxGaugeScaleStyleFactory = class
  private
    FCircularScaleStyles: TStringList;
    FDigitalScaleStyles: TStringList;
    FStyleLoader: TdxGaugeScaleStyleLoader;

    function GetStyleInfo(AStyles: TStringList; const AStyleName: string): TdxGaugeScaleStyleInfo;
    procedure DeleteStyle(AStyles: TStringList; AIndex: Integer);
    procedure InternalRegisterStyle(AStyles: TStringList; AStyleInfo: TdxGaugeScaleStyleInfo);
    procedure InternalUnregisterStyle(AStyles: TStringList; const AStyleName: string);
  protected
    function CreateStyleFromFile(const AFileName: string): TdxGaugeScaleStyle;
    function GetFirstStyleName(AScaleType: TdxGaugeScaleType): string;
    function GetStyles(AScaleType: TdxGaugeScaleType): TStringList;
    function HasStyles(AScaleType: TdxGaugeScaleType): Boolean;
    procedure UnregisterStyles;
  public
    constructor Create;
    destructor Destroy; override;

    function GetStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string): TdxGaugeScaleStyle;
    procedure RegisterStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string); overload;
    procedure RegisterStyle(const AFileName: string); overload;
    procedure UnregisterStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string);
  end;

function dxGaugeScaleStyleFactory: TdxGaugeScaleStyleFactory;
procedure dxGaugeUnregisterStyles;

implementation

uses
  Types, SysUtils, Math, dxGaugeControl, dxGaugeQuantitativeScale, dxGaugeCircularScale, dxGaugeDigitalScale,
  dxGaugeUtils;

{$R GaugeStyles.res}

const
  dxGaugeCircularScaleTypeName = 'Circular';
  dxGaugeDigitalScaleTypeName = 'Digital';
  dxGaugeBackground1 = 'BackgroundLayer1';
  dxGaugeBackground2 = 'BackgroundLayer2';
  dxGaugeDigitalBackgroundStart = 'Back_Start';
  dxGaugeDigitalBackgroundMiddle = 'Back';
  dxGaugeDigitalBackgroundEnd = 'Back_End';
  dxGaugeTick1 = 'Tick1';
  dxGaugeTick2 = 'Tick2';
  dxGaugeTick3 = 'Tick3';
  dxGaugeTick4 = 'Tick4';
  dxGaugeTick5 = 'Tick5';
  dxGaugeSpindleCap = 'SpindleCap';
  dxGaugeNeedle = 'Needle';
  dxStyleElementNameMap: array[TdxGaugeElementType] of string = (dxGaugeBackground1, dxGaugeBackground2,
    dxGaugeDigitalBackgroundStart, dxGaugeDigitalBackgroundMiddle, dxGaugeDigitalBackgroundEnd,
    dxGaugeTick1, dxGaugeTick2, dxGaugeTick3, dxGaugeTick4, dxGaugeTick5, dxGaugeSpindleCap, dxGaugeNeedle);
  dxGaugeDefaultValue = 0;
  dxGaugeScaleLayerCount = 4;
  dxGaugeScaleWidthFactor = 1;
  dxGaugeScaleHeightFactor = 1;
  dxGaugeScaleCenterPositionFactor = 0.5;

  sdxGaugeStyleReaderInvalidScaleType = 'Invalid scale type';
  sdxGaugeStyleReaderInvalidFileFormat = 'Invalid file format';

type
  TdxCompositeShapeAccess = class(TdxCompositeShape);

var
  GaugeScaleStyleFactory: TdxGaugeScaleStyleFactory;

function dxGaugeScaleStyleFactory: TdxGaugeScaleStyleFactory;
begin
  if GaugeScaleStyleFactory = nil then
    GaugeScaleStyleFactory := TdxGaugeScaleStyleFactory.Create;
  Result := GaugeScaleStyleFactory;
end;

procedure dxGaugeUnregisterStyles;
begin
  if GaugeScaleStyleFactory <> nil then
    GaugeScaleStyleFactory.UnregisterStyles;
end;

function dxGaugeScaleTypeName(AType: TdxGaugeScaleType): string;
begin
  case AType of
    stCircularScale:
      Result := dxGaugeCircularScaleTypeName;
    stDigitalScale:
      Result := dxGaugeDigitalScaleTypeName;
    else
      Result := '';
  end;
end;

function dxGaugeScaleTypeByName(const ATypeName: string): TdxGaugeScaleType;
begin
  if SameText(ATypeName, dxGaugeCircularScaleTypeName) then
    Result := stCircularScale
  else
    Result := stDigitalScale;
end;

procedure DoError(const AFormatString: string; const AArguments: array of const);
begin
  raise EdxException.Create(Format(AFormatString, AArguments));
end;

{ TdxGaugeCustomScaleParameters }

procedure TdxGaugeCustomScaleParameters.Assign(ASource: TdxGaugeCustomScaleParameters);

  procedure dxGaugeCopyFieldValues;
  begin
    cxCopyData(ASource, Self, SizeOf(ASource) + SizeOf(Variant), SizeOf(Self) + SizeOf(Variant),
      ASource.InstanceSize -  SizeOf(ASource) - SizeOf(Variant));
  end;

begin
  dxGaugeCopyFieldValues;
  Value := ASource.Value;
end;

{ TdxGaugeCustomScaleViewInfo }

constructor TdxGaugeCustomScaleViewInfo.Create(AScaleType: TdxGaugeScaleType; const AStyleName: string);
begin
  if dxGaugeScaleStyleFactory.HasStyles(AScaleType) then
  begin
    inherited Create;
    FScaleFactor := dxPointF(1, 1);
    FScaleParameters := GetScaleParametersClass.Create;
    CreateStyle(AScaleType, AStyleName);
  end
  else
    raise EdxException.Create('dxGaugeScaleStyleFactory.Count = 0');
end;

destructor TdxGaugeCustomScaleViewInfo.Destroy;
begin
  DestroyCachedLayers;
  FreeAndNil(FStyle);
  FreeAndNil(FScaleParameters);
  inherited Destroy;
end;

function TdxGaugeCustomScaleViewInfo.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeCustomScaleParameters;
end;

function TdxGaugeCustomScaleViewInfo.GetScaleSize(const ABounds: TdxRectF): TdxSizeF;
begin
  if FScaleParameters.Width <> 0 then
    Result.cx := FScaleParameters.Width
  else
    Result.cx := FScaleParameters.WidthFactor * cxRectWidth(ABounds);
  if FScaleParameters.Height <> 0 then
    Result.cy := FScaleParameters.Height
  else
    Result.cy := FScaleParameters.HeightFactor * cxRectHeight(ABounds);
end;

procedure TdxGaugeCustomScaleViewInfo.Calculate(AScaleParameters: TdxGaugeCustomScaleParameters; const ABounds: TdxRectF);
begin
  FScaleParameters.Assign(AScaleParameters);
  CalculateBounds(ABounds);
  CalculateScaleFactor(FBounds);
  CheckBounds(ABounds);
  CachingBackgroundLayer;
end;

procedure TdxGaugeCustomScaleViewInfo.CalculateBounds(const ABounds: TdxRectF);
var
  ACenter: TdxPointF;
  ASize: TdxSizeF;
  R: TdxRectF;
begin
  R := cxRectContent(ABounds, cxRectF(0, 0, 1, 1));
  ACenter := GetCenter(R);
  ASize := GetScaleSize(R);
  ASize := dxSizeF(ASize.cx / 2, ASize.cy / 2);
  FPreviousBounds := FBounds;
  FBounds := cxRectInflate(cxRectF(ACenter, ACenter), dxRectF(ASize.cx, ASize.cy, ASize.cx, ASize.cy));
end;

procedure TdxGaugeCustomScaleViewInfo.DestroyCachedLayers;
begin
  FreeAndNil(FBackgroundLayer);
end;

procedure TdxGaugeCustomScaleViewInfo.DrawBackground(AGPGraphics: TdxGPGraphics);
begin
  dxGaugeDrawImage(AGPGraphics, FBackground, FBounds);
end;

procedure TdxGaugeCustomScaleViewInfo.LoadScaleElements;
begin
  FBackground := FStyle.GetElement(etBackground1);
end;

function TdxGaugeCustomScaleViewInfo.CachingLayer(ADrawProc: TdxGaugeScaleLayerDrawProc): TcxBitmap32;
var
  AGPGraphics: TdxGPGraphics;
begin
  Result := TcxBitmap32.CreateSize(FScaleParameters.CachBounds, True);
  AGPGraphics := dxGpBeginPaint(Result.Canvas.Handle, FScaleParameters.CachBounds);
  try
    AGPGraphics.SmoothingMode := smAntiAlias;
    ADrawProc(AGPGraphics);
  finally
    dxGpEndPaint(AGPGraphics);
  end;
end;

function TdxGaugeCustomScaleViewInfo.GetElementImage(AType: TdxGaugeElementType): TGraphic;
begin
  Result := FStyle.GetElement(AType);
end;

function TdxGaugeCustomScaleViewInfo.GetElementImageSize(AImage: TGraphic; AImageScaleFactor: TdxPointF): TdxSizeF;
begin
  Result.cx := AImage.Width * FScaleFactor.X * AImageScaleFactor.X;
  Result.cy := Max(AImage.Height * FScaleFactor.X * AImageScaleFactor.Y, 1);
end;

function TdxGaugeCustomScaleViewInfo.NeedCreateLayerCach(AIsShowableLayer: Boolean): Boolean;
begin
  Result := FScaleParameters.Visible and AIsShowableLayer;
end;

function TdxGaugeCustomScaleViewInfo.NeedRecreateLayerCach(ALayer: TdxGaugeScaleCalculatableLayer): Boolean;
begin
  Result := ((sctAllLayersWithoutChecks in FScaleParameters.CalculatableLayers) or
    (sctAllLayers in FScaleParameters.CalculatableLayers) or (ALayer in FScaleParameters.CalculatableLayers)) and CanDraw;
end;

procedure TdxGaugeCustomScaleViewInfo.DrawBackgroundLayer(AGPGraphics: TdxGPGraphics);
begin
  DrawCachedLayer(AGPGraphics, FBackgroundLayer);
end;

procedure TdxGaugeCustomScaleViewInfo.DrawCachedLayer(AGPGraphics: TdxGPGraphics; ALayerBitmap: TcxBitmap32);
begin
  if IsImageAssigned(ALayerBitmap) then
    dxGaugeDrawBitmap(AGPGraphics, ALayerBitmap, cxRectF(FScaleParameters.CachBounds));
end;

procedure TdxGaugeCustomScaleViewInfo.SetStyle(AStyle: TdxGaugeScaleStyle);
begin
  FreeAndNil(FStyle);
  FStyle := AStyle;
  LoadScaleElements;
end;

function TdxGaugeCustomScaleViewInfo.CanDraw: Boolean;
begin
  Result := ((FScaleParameters.Height > 0) or (FScaleParameters.HeightFactor > 0)) and
    ((FScaleParameters.Width > 0) or (FScaleParameters.WidthFactor > 0));
end;

function TdxGaugeCustomScaleViewInfo.GetCenter(const ABounds: TdxRectF): TdxPointF;
begin
  if FScaleParameters.CenterPositionType = sptFactor then
  begin
    Result.X := ABounds.Left + FScaleParameters.CenterPositionFactor.X * cxRectWidth(ABounds);
    Result.Y := ABounds.Top + FScaleParameters.CenterPositionFactor.Y * cxRectHeight(ABounds);
  end
  else
  begin
    Result.X := ABounds.Left + FScaleParameters.CenterPosition.X;
    Result.Y := ABounds.Top + FScaleParameters.CenterPosition.Y;
  end;
end;

function TdxGaugeCustomScaleViewInfo.GetScaleDefaultParameters: TdxGaugeCustomScaleDefaultParameters;
begin
  Result := FStyle.DefaultParameters;
end;

procedure TdxGaugeCustomScaleViewInfo.CachingBackgroundLayer;
begin
  if NeedRecreateLayerCach(sctBackgroundLayer) then
  begin
    FreeAndNil(FBackgroundLayer);
    if NeedCreateLayerCach(FScaleParameters.ShowBackground) then
      FBackgroundLayer := CachingLayer(DrawBackground);
  end;
end;

procedure TdxGaugeCustomScaleViewInfo.CalculateScaleFactor(const ABounds: TdxRectF);
begin
  FScaleFactor.X := cxRectWidth(ABounds) / dxGaugeControlWidth;
  FScaleFactor.Y := cxRectHeight(ABounds) / dxGaugeControlHeigth;
end;

procedure TdxGaugeCustomScaleViewInfo.CheckBounds(const ABounds: TdxRectF);

  function RectSizeIsEqual(const R, R1: TdxRectF): Boolean;
  begin
    Result := SameValue(cxRectWidth(R), cxRectWidth(R1)) and
      SameValue(cxRectHeight(R), cxRectHeight(R1));
  end;

  function RectBoundsIsEqual(const R, R1: TdxRectF): Boolean;
  begin
    Result := SameValue(R.Left, R1.Left) and SameValue(R.Top, R1.Top) and RectSizeIsEqual(R, R1);
  end;

  function IsBoundsChanged: Boolean;
  begin
    Result := RectBoundsIsEqual(FBounds, FPreviousBounds) and
      RectSizeIsEqual(cxRectF(FScaleParameters.CachBounds), FPreviousCachBounds)
  end;

begin
  if not(IsBoundsChanged or (sctAllLayersWithoutChecks in FScaleParameters.CalculatableLayers)) then
  begin
    FScaleParameters.CalculatableLayers := [];
    Include(FScaleParameters.CalculatableLayers, sctAllLayers);
  end;
  FPreviousCachBounds := cxRectF(FScaleParameters.CachBounds);
end;

procedure TdxGaugeCustomScaleViewInfo.CreateStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string);
begin
  FStyle := dxGaugeScaleStyleFactory.GetStyle(AScaleType, AStyleName);
  LoadScaleElements;
end;

procedure TdxGaugeCustomScaleViewInfo.RecreateStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string);
begin
  FreeAndNil(FStyle);
  CreateStyle(AScaleType, AStyleName);
end;

{ TdxGaugeCustomScale }

constructor TdxGaugeCustomScale.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FScaleParameters := GetScaleParametersClass.Create;
  FScaleParameters.AnchorScaleIndex := -1;  
  FScaleParameters.WidthFactor := dxGaugeScaleWidthFactor;
  FScaleParameters.HeightFactor := dxGaugeScaleHeightFactor;
  FScaleParameters.CenterPositionFactor.X := dxGaugeScaleCenterPositionFactor;
  FScaleParameters.CenterPositionFactor.Y := dxGaugeScaleCenterPositionFactor;
  FScaleParameters.LayerCount := dxGaugeScaleLayerCount;
  FScaleParameters.Value := dxGaugeDefaultValue;
  FScaleParameters.ShowBackground := True;
  FScaleParameters.Visible := True;
  FStyleName := dxGaugeScaleStyleFactory.GetFirstStyleName(GetScaleType);
  FViewInfo := GetViewInfoClass.Create(GetScaleType, FStyleName);
  ApplyStyleParameters;
end;

destructor TdxGaugeCustomScale.Destroy;
begin
  FreeAndNil(FViewInfo);
  FreeAndNil(FScaleParameters);
  inherited Destroy;
end;

procedure TdxGaugeCustomScale.Assign(ASource: TPersistent);
begin
  if ASource is TdxGaugeCustomScale then
  begin
    Collection.BeginUpdate;
    try
      DoAssign(TdxGaugeCustomScale(ASource));
    finally
      Collection.EndUpdate;
    end;
  end
  else
    inherited Assign(ASource);  
end;

procedure TdxGaugeCustomScale.LoadStyleFromFile(const AFileName: string);
begin
  FStyleName := '';
  FViewInfo.Style := dxGaugeScaleStyleFactory.CreateStyleFromFile(AFileName);
  ScaleChanged(sctAllLayersWithoutChecks);
end;

procedure TdxGaugeCustomScale.RestoreStyleParameters;
begin
  InternalRestoreStyleParameters;
end;

function TdxGaugeCustomScale.GetCollectionFromParent(AParent: TComponent): TcxComponentCollection;
begin
  Result := (AParent as TdxCustomGaugeControl).Scales;
end;

class function TdxGaugeCustomScale.GetScaleType: TdxGaugeScaleType;
begin
  Result := stCircularScale;
end;

function TdxGaugeCustomScale.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeCustomScaleParameters;
end;

function TdxGaugeCustomScale.GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass;
begin
  Result := TdxGaugeCustomScaleViewInfo;
end;

procedure TdxGaugeCustomScale.ApplyStyleParameters;
begin
//do nothing
end;

procedure TdxGaugeCustomScale.Calculate(const AScaleBounds: TdxRectF; const ACachBounds: TRect);
begin
  Collection.BeginUpdate;
  ApplyStyleParameters;
  Collection.EndUpdate(False);
  FScaleParameters.CachBounds := ACachBounds;
  FViewInfo.Calculate(FScaleParameters, AScaleBounds);
  FScaleParameters.CalculatableLayers := [];
end;

procedure TdxGaugeCustomScale.InternalDrawLayer(AGPGraphics: TdxGPGraphics; AIndex: Integer);
begin
  DrawBackgroundLayer(AGPGraphics);
end;

procedure TdxGaugeCustomScale.InternalRestoreStyleParameters;
begin
  ScaleChanged(sctAllLayersWithoutChecks);
end;

procedure TdxGaugeCustomScale.DrawBackgroundLayer(AGPGraphics: TdxGPGraphics);
begin
  FViewInfo.DrawBackgroundLayer(AGPGraphics);
end;

procedure TdxGaugeCustomScale.DrawLayer(AGPGraphics: TdxGPGraphics; ALayerIndex: Integer);
begin
  if Visible then
    InternalDrawLayer(AGPGraphics, ALayerIndex);
end;

procedure TdxGaugeCustomScale.ScaleChanged(ACalculatableLayer: TdxGaugeScaleCalculatableLayer = sctAllLayers);
begin
  if ACalculatableLayer = sctAllLayersWithoutChecks then
    FScaleParameters.CalculatableLayers := [];
  if not (sctAllLayersWithoutChecks in FScaleParameters.CalculatableLayers) then
    Include(FScaleParameters.CalculatableLayers, ACalculatableLayer);
  Changed(False);
end;

function TdxGaugeCustomScale.GetAnchorScaleIndex: Integer;
begin
  Result := FScaleParameters.AnchorScaleIndex;
end;

function TdxGaugeCustomScale.GetBounds: TdxRectF;
begin
  Result := FViewInfo.Bounds;
end;

function TdxGaugeCustomScale.GetCenterPositionType: TdxGaugeScalePositionType;
begin
  Result := FScaleParameters.CenterPositionType;
end;

function TdxGaugeCustomScale.GetCenterPositionX: Integer;
begin
  Result := FScaleParameters.CenterPosition.X;
end;

function TdxGaugeCustomScale.GetCenterPositionFactorX: Single;
begin
  Result := FScaleParameters.CenterPositionFactor.X;
end;

function TdxGaugeCustomScale.GetCenterPositionY: Integer;
begin
  Result := FScaleParameters.CenterPosition.Y;
end;

function TdxGaugeCustomScale.GetCenterPositionFactorY: Single;
begin
  Result := FScaleParameters.CenterPositionFactor.Y;
end;

function TdxGaugeCustomScale.GetLayerCount: Integer;
begin
  Result := FScaleParameters.LayerCount;
end;

function TdxGaugeCustomScale.GetHeight: Integer;
begin
  Result := FScaleParameters.Height;
end;

function TdxGaugeCustomScale.GetHeightFactor: Single;
begin
  Result := FScaleParameters.HeightFactor;
end;

function TdxGaugeCustomScale.GetShowBackground: Boolean;
begin
  Result := FScaleParameters.ShowBackground;
end;

function TdxGaugeCustomScale.GetVisible: Boolean;
begin
  Result := FScaleParameters.Visible;
end;

function TdxGaugeCustomScale.GetWidth: Integer;
begin
  Result := FScaleParameters.Width;
end;

function TdxGaugeCustomScale.GetWidthFactor: Single;
begin
  Result := FScaleParameters.WidthFactor;
end;

procedure TdxGaugeCustomScale.SetAnchorScaleIndex(const AValue: Integer);
begin
  if FScaleParameters.AnchorScaleIndex <> AValue then
  begin
    FScaleParameters.AnchorScaleIndex := Min(Max(AValue, -1), 0);
    ScaleChanged;
  end;
end;

procedure TdxGaugeCustomScale.SetCenterPositionType(const AValue: TdxGaugeScalePositionType);
begin
  if FScaleParameters.CenterPositionType <> AValue then
  begin
    FScaleParameters.CenterPositionType := AValue;
    ScaleChanged;
  end;
end;

procedure TdxGaugeCustomScale.SetCenterPositionX(const AValue: Integer);
begin
  if FScaleParameters.CenterPosition.X <> AValue then
  begin
    FScaleParameters.CenterPosition := Point(AValue, FScaleParameters.CenterPosition.Y);
    ScaleChanged;
  end;
end;

procedure TdxGaugeCustomScale.SetCenterPositionFactorX(const AValue: Single);
begin
  if not SameValue(FScaleParameters.CenterPositionFactor.X, AValue) then
  begin
    FScaleParameters.CenterPositionFactor := dxPointF(AValue, FScaleParameters.CenterPositionFactor.Y);
    ScaleChanged;
  end;
end;

procedure TdxGaugeCustomScale.SetCenterPositionY(const AValue: Integer);
begin
  if FScaleParameters.CenterPosition.Y <> AValue then
  begin
    FScaleParameters.CenterPosition := Point(FScaleParameters.CenterPosition.X, AValue);
    ScaleChanged;
  end;
end;

procedure TdxGaugeCustomScale.SetCenterPositionFactorY(const AValue: Single);
begin
  if not SameValue(FScaleParameters.CenterPositionFactor.Y, AValue) then
  begin
    FScaleParameters.CenterPositionFactor := dxPointF(FScaleParameters.CenterPositionFactor.X, AValue);
    ScaleChanged;
  end;
end;

procedure TdxGaugeCustomScale.SetHeight(const AValue: Integer);
begin
  if (FScaleParameters.Height <> AValue) and (AValue >= 0) then
  begin
    FScaleParameters.Height := AValue;
    ScaleChanged;
  end;
end;

procedure TdxGaugeCustomScale.SetHeightFactor(const AValue: Single);
begin
  if (FScaleParameters.HeightFactor <> AValue) and (AValue >= 0) then
  begin
    FScaleParameters.HeightFactor := AValue;
    ScaleChanged;
  end;
end;

procedure TdxGaugeCustomScale.SetShowBackground(const AValue: Boolean);
begin
  if FScaleParameters.ShowBackground <> AValue then
  begin
    FScaleParameters.ShowBackground := AValue;
    ScaleChanged(sctBackgroundLayer);
  end;
end;

procedure TdxGaugeCustomScale.SetStyleName(const AValue: string);
begin
  if not SameText(FStyleName, AValue) then
  begin
    FStyleName := AValue;
    Collection.BeginUpdate;
    try
      FViewInfo.RecreateStyle(GetScaleType, FStyleName);
      ApplyStyleParameters;
    finally
      Collection.EndUpdate(False);
    end;
    ScaleChanged(sctAllLayersWithoutChecks);
  end;
end;

procedure TdxGaugeCustomScale.SetVisible(const AValue: Boolean);
begin
  if FScaleParameters.Visible <> AValue then
  begin
    FScaleParameters.Visible := AValue;
    ScaleChanged;
  end;
end;

procedure TdxGaugeCustomScale.SetWidth(const AValue: Integer);
begin
  if (FScaleParameters.Width <> AValue) and (AValue >= 0) then
  begin
    FScaleParameters.Width := AValue;
    ScaleChanged;
  end;
end;

procedure TdxGaugeCustomScale.SetWidthFactor(const AValue: Single);
begin
  if not SameValue(FScaleParameters.WidthFactor, AValue) and (AValue > 0) then
  begin
    FScaleParameters.WidthFactor := AValue;
    ScaleChanged;
  end;
end;

procedure TdxGaugeCustomScale.DoAssign(AScale: TdxGaugeCustomScale);
begin
  StyleName := AScale.StyleName;
  FScaleParameters.Assign(AScale.FScaleParameters);
end;

function TdxGaugeCustomScale.IsCenterPositionFactorXStored: Boolean;
begin
  Result := not SameValue(FScaleParameters.CenterPositionFactor.X, 0.5);
end;

function TdxGaugeCustomScale.IsCenterPositionFactorYStored: Boolean;
begin
  Result := not SameValue(FScaleParameters.CenterPositionFactor.Y, 0.5);
end;

function TdxGaugeCustomScale.IsHeightFactorStored: Boolean;
begin
  Result := not SameValue(FScaleParameters.HeightFactor, 1);
end;

function TdxGaugeCustomScale.IsWidthFactorStored: Boolean;
begin
  Result := not SameValue(FScaleParameters.WidthFactor, 1);
end;

{ TdxGaugeScaleCollection }

function TdxGaugeScaleCollection.GetItemPrefixName: string;
begin
  Result := 'TdxGauge';
end;

procedure TdxGaugeScaleCollection.Assign(ASource: TPersistent);
var
  I: Integer;
  AScale: TdxGaugeCustomScale;
begin
  if ASource is TdxGaugeScaleCollection then
  begin
    BeginUpdate;
    try
      Clear;
      for I := 0 to TdxGaugeScaleCollection(ASource).Count - 1 do
      begin
        AScale := TdxGaugeScaleCollection(ASource).Scales[I];
        Add(TdxGaugeCustomScaleClass(AScale.ClassType)).Assign(AScale);
      end;
    finally
      EndUpdate;
    end;
  end
  else
    inherited Assign(ASource);
end;

function TdxGaugeScaleCollection.Add(AScaleClass: TdxGaugeCustomScaleClass): TdxGaugeCustomScale;
begin
  Result := AScaleClass.Create(ParentComponent.Owner);
  Result.SetParentComponent(ParentComponent);
  SetItemName(Result);
end;

function TdxGaugeScaleCollection.GetScale(AIndex: Integer): TdxGaugeCustomScale;
begin
  Result := inherited GetItem(AIndex) as TdxGaugeCustomScale;
end;

procedure TdxGaugeScaleCollection.SetScale(AIndex: Integer; const AValue: TdxGaugeCustomScale);
begin
  inherited SetItem(AIndex, AValue);
end;

{ TdxGaugeScaleStyle }

constructor TdxGaugeScaleStyle.Create(AScaleType: TdxGaugeScaleType; const AStyleName: string);
begin
  inherited Create;
  FName := AStyleName;
  FScaleType := AScaleType;
end;

destructor TdxGaugeScaleStyle.Destroy;
var
  I: TdxGaugeElementType;
begin
  for I := Low(FElements) to High(FElements) do
  begin
    FElements[I].Free;
    FElements[I] := nil;
  end;
  FreeAndNil(FDefaultParameters);
  FreeAndNil(FShapes);
  inherited Destroy;
end;

function TdxGaugeScaleStyle.GetElement(AElementType: TdxGaugeElementType): TdxCompositeShape;
begin
  if FElements[AElementType] = nil then
  begin
    FElements[AElementType] := TdxCompositeShape.Create;
    TdxCompositeShapeAccess(FElements[AElementType]).LoadFromShape(FShapes, dxStyleElementNameMap[AElementType]);
  end;
  Result := FElements[AElementType];
end;

{ TdxGaugeCustomScaleStyleReader }

function TdxGaugeCustomScaleStyleReader.GetStyle(ADocument: TdxXMLDocument): TdxGaugeScaleStyle;

  function ReadStyleName: string;
  begin
    Result := GetAttributeValueAsString(ADocument.Root.First, 'Name');
  end;

  function ReadScaleType: TdxGaugeScaleType;
  begin
    Result := GetAttributeValueAsScaleType(ADocument.Root.First, 'Type');
  end;

begin
  Result := TdxGaugeScaleStyle.Create(ReadScaleType, ReadStyleName);
  Result.Shapes := ReadCompositeShape(ADocument);
  Result.DefaultParameters := ReadDefaultParameters(ADocument);
end;

function TdxGaugeCustomScaleStyleReader.GetAttributeValueAsAlphaColor(ANode: TdxXMLNode;
  const AAttributeName: AnsiString): TdxAlphaColor;
begin
  Result := dxColorNameToAlphaColor(ANode.Attributes.GetValueAsString(AAttributeName));
end;

function TdxGaugeCustomScaleStyleReader.GetAttributeValueAsBoolean(ANode: TdxXMLNode;
  const AAttributeName: AnsiString): Boolean;
begin
  Result := ANode.Attributes.GetValueAsBoolean(AAttributeName);
end;

function TdxGaugeCustomScaleStyleReader.GetAttributeValueAsColor(ANode: TdxXMLNode;
  const AAttributeName: AnsiString): TColor;
begin
  Result := dxAlphaColorToColor(dxColorNameToAlphaColor(ANode.Attributes.GetValueAsString(AAttributeName)));
end;

function TdxGaugeCustomScaleStyleReader.GetAttributeValueAsDouble(ANode: TdxXMLNode;
  const AAttributeName: AnsiString): Double;
begin
  Result := ANode.Attributes.GetValueAsFloat(AAttributeName);
end;

function TdxGaugeCustomScaleStyleReader.GetAttributeValueAsElementType(ANode: TdxXMLNode;
  const AAttributeName: AnsiString): TdxGaugeElementType;
begin
  Result := GetElementType(ANode.Attributes.GetValueAsString(AAttributeName));
end;

function TdxGaugeCustomScaleStyleReader.GetAttributeValueAsInteger(ANode: TdxXMLNode;
  const AAttributeName: AnsiString): Integer;
begin
  Result := ANode.Attributes.GetValueAsInteger(AAttributeName);
end;

function TdxGaugeCustomScaleStyleReader.GetAttributeValueAsPointF(ANode: TdxXMLNode;
  const AAttributeName: AnsiString): TdxPointF;
begin
  Result := dxStrToPointF(ANode.Attributes.GetValueAsString(AAttributeName));
end;

function TdxGaugeCustomScaleStyleReader.GetAttributeValueAsScaleType(ANode: TdxXMLNode;
  const AAttributeName: AnsiString): TdxGaugeScaleType;
begin
  Result := dxGaugeScaleTypeByName(ANode.Attributes.GetValueAsString(AAttributeName));
end;

function TdxGaugeCustomScaleStyleReader.GetAttributeValueAsString(ANode: TdxXMLNode;
  const AAttributeName: AnsiString): string;
begin
  Result := ANode.Attributes.GetValueAsString(AAttributeName);
end;

function TdxGaugeCustomScaleStyleReader.GetChildNode(ANode: TdxXMLNode; const AChildNodeName: AnsiString;
  out AChildNode: TdxXMLNode): Boolean;
begin
  AChildNode := GetChildNode(ANode, AChildNodeName);
  Result := AChildNode <> nil;
end;

function TdxGaugeCustomScaleStyleReader.GetChildNode(ANode: TdxXMLNode; const AChildNodeName: AnsiString): TdxXMLNode;
var
  AChildNode: TdxXMLNode;
begin
  if ANode.FindChild(AChildNodeName, AChildNode) then
    Result := AChildNode
  else
    Result := nil;
end;

function TdxGaugeCustomScaleStyleReader.GetElementType(const ATypeName: string): TdxGaugeElementType;
var
  I: TdxGaugeElementType;
begin
  Result := etBackground1;
  for I := Low(dxStyleElementNameMap) to High(dxStyleElementNameMap) do
    if SameText(dxStyleElementNameMap[I], ATypeName) then
    begin
      Result := I;
      Break;
    end;
end;

procedure TdxGaugeCustomScaleStyleReader.ReadFontParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeCustomScaleDefaultParameters);
var
  AFontNode: TdxXMLNode;
  AScaleDefaultParameters: TdxGaugeQuantitativeScaleDefaultParameters;
begin
  if GetChildNode(ANode, 'Font', AFontNode) then
  begin
    AScaleDefaultParameters := AParameters as TdxGaugeQuantitativeScaleDefaultParameters;
    AScaleDefaultParameters.FontColor := GetAttributeValueAsColor(GetChildNode(AFontNode, 'Color'), 'Value');
    AScaleDefaultParameters.FontName := GetAttributeValueAsString(GetChildNode(AFontNode, 'Name'), 'Value');
    AScaleDefaultParameters.FontSize := GetAttributeValueAsInteger(GetChildNode(AFontNode, 'Size'), 'Value');
  end;
end;

function TdxGaugeCustomScaleStyleReader.GetStyleRootNode(ADocument: TdxXMLDocument): TdxXMLNode;
begin
  Result := GetChildNode(ADocument.Root, 'ScaleStyle');
end;

function TdxGaugeCustomScaleStyleReader.ReadCompositeShape(ADocument: TdxXMLDocument): TdxCompositeShape;
var
  ACompositeShapeNode: TdxXMLNode;
begin
  if GetChildNode(GetStyleRootNode(ADocument), 'CompositeShape', ACompositeShapeNode) then
  begin
    Result := TdxCompositeShape.Create;
    TdxCompositeShapeAccess(Result).LoadFromNode(ACompositeShapeNode);
  end
  else
    Result := nil;
end;

function TdxGaugeCustomScaleStyleReader.ReadDefaultParameters(ADocument: TdxXMLDocument):
  TdxGaugeCustomScaleDefaultParameters;
var
  ADefaultParametersNode: TdxXMLNode;
begin
  if GetChildNode(GetStyleRootNode(ADocument), 'DefaultParameters', ADefaultParametersNode) then
  begin
    Result := GetDefaultParametersClass.Create;
    ReadParameters(ADefaultParametersNode, Result);
  end
  else
    Result := nil;
end;

{ TdxGaugeScaleStyleLoader }

function TdxGaugeScaleStyleLoader.GetScaleStyleInfo(const AFileName: string): TdxGaugeScaleStyleInfo;
var
  AStyle: TdxGaugeScaleStyle;
begin
  try
    AStyle := LoadFromFile(AFileName);
    Result := TdxGaugeScaleStyleInfo.Create;
    Result.Name := AStyle.Name;
    Result.ScaleType := AStyle.ScaleType;
    Result.ResourceName := AFileName;
    Result.IsExternalStyle := True;
  finally
    FreeAndNil(AStyle);
  end;
end;

function TdxGaugeScaleStyleLoader.LoadFromFile(const AFileName: string): TdxGaugeScaleStyle;
var
  AStream: TStream;
begin
  AStream := TFileStream.Create(AFileName, fmOpenRead or fmShareDenyNone);
  try
    LoadFromStream(AStream, Result);
  finally
    AStream.Free
  end;
end;

function TdxGaugeScaleStyleLoader.LoadFromResource(AInstance: THandle; const AResName: string): TdxGaugeScaleStyle;
var
  AStream: TStream;
begin
  AStream := TResourceStream.Create(AInstance, AResName, RT_RCDATA);
  try
    LoadFromStream(AStream, Result);
  finally
    AStream.Free;
  end;
end;

function TdxGaugeScaleStyleLoader.LoadStyle(AStyleInfo: TdxGaugeScaleStyleInfo): TdxGaugeScaleStyle;
begin
  if AStyleInfo <> nil then
    if not AStyleInfo.IsExternalStyle then
      Result := LoadFromResource(HInstance, AStyleInfo.ResourceName)
    else
      Result := LoadFromFile(AStyleInfo.ResourceName)
  else
    Result := nil;
end;

function TdxGaugeScaleStyleLoader.GetReaderClass(AScaleType: TdxGaugeScaleType): TdxGaugeCustomScaleStyleReaderClass;
begin
  case AScaleType of
    stCircularScale:
      Result := TdxGaugeCircularScaleStyleReader;
    stDigitalScale:
      Result := TdxGaugeDigitalScaleStyleReader;
    else
      Result := nil;
  end;
end;

function TdxGaugeScaleStyleLoader.GetScaleType(ADocument: TdxXMLDocument): TdxGaugeScaleType;
var
  ANode: TdxXMLNode;
  AAttribute: TdxXMLNodeAttribute;
begin
  Result := stCircularScale;
  if IsDocumentAvailable(ADocument) and ADocument.Root.FindChild('ScaleStyle', ANode) and
      ANode.Attributes.Find('Type', AAttribute) then
  begin
    if SameText(AAttribute.ValueAsString, dxGaugeCircularScaleTypeName) then
      Result := stCircularScale
    else
      if SameText(AAttribute.ValueAsString, dxGaugeDigitalScaleTypeName) then
        Result := stDigitalScale
      else
        DoError(sdxGaugeStyleReaderInvalidScaleType, []);
  end
  else
    DoError(sdxGaugeStyleReaderInvalidFileFormat, []);
end;

function TdxGaugeScaleStyleLoader.IsDocumentAvailable(ADocument: TdxXMLDocument): Boolean;
begin
  Result := (ADocument <> nil) and (ADocument.Root <> nil);
end;

procedure TdxGaugeScaleStyleLoader.LoadFromStream(AStream: TStream; out AStyle: TdxGaugeScaleStyle);
var
  AXMLDocument: TdxXMLDocument;
  AReader: TdxGaugeCustomScaleStyleReader;
begin
  AXMLDocument := TdxXMLDocument.Create(nil);
  try
    AXMLDocument.LoadFromStream(AStream);
    if IsDocumentAvailable(AXMLDocument) then
    begin
      AReader := GetReaderClass(GetScaleType(AXMLDocument)).Create;
      AStyle := AReader.GetStyle(AXMLDocument);
    end;
  finally
    FreeAndNil(AReader);
    FreeAndNil(AXMLDocument);
  end;
end;

{ TdxGaugeScaleStyleFactory }

constructor TdxGaugeScaleStyleFactory.Create;
begin
  inherited Create;
  FCircularScaleStyles := TStringList.Create;
  FDigitalScaleStyles := TStringList.Create;
  FCircularScaleStyles.Sorted := True;
  FCircularScaleStyles.Duplicates := dupError;
  FDigitalScaleStyles.Sorted := True;
  FDigitalScaleStyles.Duplicates := dupError;
  FStyleLoader := TdxGaugeScaleStyleLoader.Create;
end;

destructor TdxGaugeScaleStyleFactory.Destroy;
begin
  UnregisterStyles;
  FreeAndNil(FStyleLoader);
  FreeAndNil(FDigitalScaleStyles);
  FreeAndNil(FCircularScaleStyles);
  inherited Destroy;
end;

function TdxGaugeScaleStyleFactory.GetStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string): TdxGaugeScaleStyle;
begin
  Result := FStyleLoader.LoadStyle(GetStyleInfo(GetStyles(AScaleType), AStyleName))
end;

procedure TdxGaugeScaleStyleFactory.RegisterStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string);

  function CreateStyleInfo: TdxGaugeScaleStyleInfo;
  begin
    Result := TdxGaugeScaleStyleInfo.Create;
    Result.Name := AStyleName;
    Result.ResourceName := dxGaugeScaleTypeName(AScaleType) + AStyleName;
    Result.IsExternalStyle := False;
    Result.ScaleType := AScaleType;
  end;

begin
  InternalRegisterStyle(GetStyles(AScaleType), CreateStyleInfo);
end;

procedure TdxGaugeScaleStyleFactory.RegisterStyle(const AFileName: string);
var
  AStyleInfo: TdxGaugeScaleStyleInfo;
begin
  AStyleInfo := FStyleLoader.GetScaleStyleInfo(AFileName);
  InternalRegisterStyle(GetStyles(AStyleInfo.ScaleType), AStyleInfo);
end;

procedure TdxGaugeScaleStyleFactory.UnregisterStyle(AScaleType: TdxGaugeScaleType; const AStyleName: string);
begin
  InternalUnregisterStyle(GetStyles(AScaleType), AStyleName);
end;

function TdxGaugeScaleStyleFactory.CreateStyleFromFile(const AFileName: string): TdxGaugeScaleStyle;
begin
  Result := FStyleLoader.LoadFromFile(AFileName);
end;

function TdxGaugeScaleStyleFactory.GetFirstStyleName(AScaleType: TdxGaugeScaleType): string;
begin
  Result := GetStyles(AScaleType).Names[0];
end;

function TdxGaugeScaleStyleFactory.GetStyles(AScaleType: TdxGaugeScaleType): TStringList;
begin
  case AScaleType of
    stCircularScale:
      Result := FCircularScaleStyles;
    stDigitalScale:
      Result := FDigitalScaleStyles;
    else
      Result := nil;
  end;
end;

function TdxGaugeScaleStyleFactory.HasStyles(AScaleType: TdxGaugeScaleType): Boolean;
begin
  Result := GetStyles(AScaleType).Count > 0;
end;

procedure TdxGaugeScaleStyleFactory.UnregisterStyles;

  procedure UnregistryStyles(AStyles: TStringList);
  begin
    if AStyles <> nil then
      while AStyles.Count > 0 do
        DeleteStyle(AStyles, 0);
  end;

begin
  UnregistryStyles(FCircularScaleStyles);
  UnregistryStyles(FDigitalScaleStyles);  
end;

function TdxGaugeScaleStyleFactory.GetStyleInfo(AStyles: TStringList; const AStyleName: string): TdxGaugeScaleStyleInfo;
var
  AIndex: Integer;
begin
  if AStyles <> nil then
    if AStyles.Find(AStyleName, AIndex) then
      Result := TdxGaugeScaleStyleInfo(AStyles.Objects[AIndex])
    else
      Result := TdxGaugeScaleStyleInfo(AStyles.Objects[0])
  else
    Result := nil;
end;

procedure TdxGaugeScaleStyleFactory.DeleteStyle(AStyles: TStringList; AIndex: Integer);
begin
  if AStyles <> nil then
  begin
    AStyles.Objects[AIndex].Free;
    AStyles.Delete(AIndex);
  end;
end;

procedure TdxGaugeScaleStyleFactory.InternalRegisterStyle(AStyles: TStringList; AStyleInfo: TdxGaugeScaleStyleInfo);
begin
  AStyles.AddObject(AStyleInfo.Name, TObject(AStyleInfo));
end;

procedure TdxGaugeScaleStyleFactory.InternalUnregisterStyle(AStyles: TStringList; const AStyleName: string);
var
  AIndex: Integer;
begin
  if AStyles.Find(AStyleName, AIndex) then
    DeleteStyle(AStyles, AIndex);
end;

initialization

finalization
  FreeAndNil(GaugeScaleStyleFactory);

end.
