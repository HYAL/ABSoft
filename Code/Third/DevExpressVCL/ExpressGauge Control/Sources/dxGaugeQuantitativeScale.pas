{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressGaugeControl                                      }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSGAUGECONTROL AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGaugeQuantitativeScale;

{$I cxVer.inc}

interface

uses
  SysUtils, Classes, Graphics, Windows, cxGeometry, cxGraphics, dxGDIPlusClasses, dxCompositeShape, dxXMLDoc,
  dxGaugeCustomScale;

type
  TdxGaugeQuantitativeScale = class;
  TdxGaugeQuantitativeScaleClass = class of TdxGaugeQuantitativeScale;
  TdxGaugeQuantitativeScaleParameters = class;
  TdxGaugeQuantitativeScaleParametersClass = class of TdxGaugeQuantitativeScaleParameters;
  TdxGaugeQuantitativeScaleViewInfo = class;
  TdxGaugeQuantitativeScaleViewInfoClass = class of TdxGaugeQuantitativeScaleViewInfo;

  TdxGaugeQuantitativeScaleAssignedValue = (savFontColor, savFontName, savFontSize, savShowFirstTick, savShowLabels,
    savShowLastTick, savShowTicks, savMajorTickCount, savMinorTickCount);
  TdxGaugeQuantitativeScaleAssignedValues = set of TdxGaugeQuantitativeScaleAssignedValue;

  { TdxGaugeQuantitativeScaleDefaultParameters }

  TdxGaugeQuantitativeScaleDefaultParameters = class(TdxGaugeCustomScaleDefaultParameters)
  public
    FontColor: TColor;
    FontName: string;
    FontSize: Integer;
    LabelOffset: Single;
    AllowOverlapMajorTicks: Boolean;
    ShowFirstTick: Boolean;
    ShowLabels: Boolean;
    ShowLastTick: Boolean;
    ShowTicks: Boolean;    
    MajorTickCount: Integer;
    MajorTickOffset: Single;
    MajorTickScaleFactor: TdxPointF;
    MajorTickType: TdxGaugeElementType;
    MinorTickCount: Integer;
    MinorTickOffset: Single;
    MinorTickScaleFactor: TdxPointF;
    MinorTickType: TdxGaugeElementType;
    SpindleCapSize: Single;
    ValueIndicatorStartOffset: Single;
    ValueIndicatorEndOffset: Single;
  end;

  { TdxGaugeQuantitativeScaleParameters }

  TdxGaugeQuantitativeScaleParameters = class(TdxGaugeCustomScaleParameters)
  public
    MajorTickCount: Integer;
    MaxValue: Single;
    MinValue: Single;
    MinorTickCount: Integer;
    ShowFirstTick: Boolean;
    ShowLabels: Boolean;
    ShowLastTick: Boolean;
    ShowTicks: Boolean;
    ShowValueIndicator: Boolean;
    Font: Pointer;
    LabelOffset: Single;
  end;

  { TdxGaugeQuantitativeScaleViewInfo }

  TdxGaugeQuantitativeScaleViewInfo = class(TdxGaugeCustomScaleViewInfo)
  private
    FFont: TFont;
    FLabelPrecision: Integer;
    FMajorTick: TdxCompositeShape;
    FMinorTick: TdxCompositeShape;

    function GetQuantitativeScaleDefaultParameters: TdxGaugeQuantitativeScaleDefaultParameters;
    function GetScaleParameters: TdxGaugeQuantitativeScaleParameters;
    function GetMaxLabelValue: Single;
    procedure CalculateLabelPrecision;
  protected
    FElementsLayer: TcxBitmap32;
    FValueIndicatorLayer: TcxBitmap32;

    FLabelOffset: Single;
    FMaxLabelRectSize: TdxSizeF;

    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; override;
    procedure Calculate(AScaleParameters: TdxGaugeCustomScaleParameters; const ABounds: TdxRectF); override;
    procedure DestroyCachedLayers; override;
    procedure LoadScaleElements; override;

    procedure DrawLabel(AGPGraphics: TdxGPGraphics; AValue, AOffset: Single); virtual; abstract;
    procedure DrawTick(AGPGraphics: TdxGPGraphics; AValue: Single; AAngle: Single;
      ATickDrawParameters: TdxGaugeScaleTickDrawParameters); virtual; abstract;
    procedure DrawTicks(AGPGraphics: TdxGPGraphics); virtual; abstract;
    procedure DrawValueIndicator(AGPGraphics: TdxGPGraphics); virtual; abstract;

    function IsMajorTickVisible(AIndex: Integer): Boolean;
    function GetElementValue(AElementIndex, AElementCount: Integer): Single;
    function GetLabelText(AValue: Single): string;
    function GetScaleDefaultParameters: TdxGaugeQuantitativeScaleDefaultParameters;
    function GetTickDrawParameters(AIsMajorTicks: Boolean): TdxGaugeScaleTickDrawParameters;
    function GetValueRange: Single;
    function GetValueStep: Single;
    procedure CachingElementsLayer;
    procedure CachingValueIndicatorLayer;
    procedure CalculateLabelMaxSize(AGPGraphics: TdxGPGraphics);
    procedure CalculateLabelOffset;
    procedure DrawElementsLayer(AGPGraphics: TdxGPGraphics);
    procedure DrawValueIndicatorLayer(AGPGraphics: TdxGPGraphics);

    property Font: TFont read FFont;
    property DefaultParameters: TdxGaugeQuantitativeScaleDefaultParameters read GetQuantitativeScaleDefaultParameters;
  public
    constructor Create(AScaleType: TdxGaugeScaleType; const AStyleName: string); override;
    destructor Destroy; override;
  end;

  { TdxGaugeQuantitativeScale }

  TdxGaugeQuantitativeScale = class(TdxGaugeCustomScale)
  private
    FAssignedValues: TdxGaugeQuantitativeScaleAssignedValues;
    FFont: TFont;
    FNeedUpdateAssignedValues: Boolean;

    function GetMajorTickCount: Integer;
    function GetMaxValue: Single;
    function GetMinorTickCount: Integer;
    function GetMinValue: Single;
    function GetShowFirstTick: Boolean;
    function GetShowLabels: Boolean;
    function GetShowLastTick: Boolean;
    function GetShowTicks: Boolean;
    function GetShowValueIndicator: Boolean;
    function GetValue: Single;
    procedure SetFont(const AValue: TFont);
    procedure SetMajorTickCount(const AValue: Integer);
    procedure SetMaxValue(const AValue: Single);
    procedure SetMinorTickCount(const AValue: Integer);
    procedure SetMinValue(const AValue: Single);
    procedure SetShowFirstTick(const AValue: Boolean);
    procedure SetShowLabels(const AValue: Boolean);
    procedure SetShowLastTick(const AValue: Boolean);
    procedure SetShowTicks(const AValue: Boolean);
    procedure SetShowValueIndicator(const AValue: Boolean);
    procedure SetValue(const AValue: Single);

    function GetScaleParameters: TdxGaugeQuantitativeScaleParameters;
    function GetViewInfo: TdxGaugeQuantitativeScaleViewInfo;
    procedure ApplyStyleFontParameters;
    procedure DrawIndicatorsLayer(AGPGraphics: TdxGPGraphics);
    procedure DrawScaleElementsLayer(AGPGraphics: TdxGPGraphics);
    procedure FontChangeHandler(ASender: TObject);
    procedure SetScaleValue(AValue: Single);

    function IsFontStored: Boolean;
    function IsMajorTickCountStored: Boolean;
    function IsMaxValueStored: Boolean;
    function IsMinorTickCountStored: Boolean;
    function IsShowFirstTickStored: Boolean;
    function IsShowLabelsStored: Boolean;
    function IsShowLastTickStored: Boolean;
    function IsShowTicksStored: Boolean;
  protected
    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; override;
    function GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass; override;

    procedure ApplyStyleParameters; override;
    procedure Calculate(const AScaleBounds: TdxRectF; const ACachBounds: TRect); override;
    procedure InternalDrawLayer(AGPGraphics: TdxGPGraphics; AIndex: Integer); override;
    procedure InternalRestoreStyleParameters; override;

    property Font: TFont read FFont write SetFont stored IsFontStored;
    property MajorTickCount: Integer read GetMajorTickCount write SetMajorTickCount stored IsMajorTickCountStored;
    property MinorTickCount: Integer read GetMinorTickCount write SetMinorTickCount stored IsMinorTickCountStored;
    property ShowFirstTick: Boolean read GetShowFirstTick write SetShowFirstTick stored IsShowFirstTickStored default True;
    property ShowLabels: Boolean read GetShowLabels write SetShowLabels stored IsShowLabelsStored default True;
    property ShowLastTick: Boolean read GetShowLastTick write SetShowLastTick stored IsShowLastTickStored default True;
    property ShowTicks: Boolean read GetShowTicks write SetShowTicks stored IsShowTicksStored default True;
    property ShowValueIndicator: Boolean read GetShowValueIndicator write SetShowValueIndicator default True;
    property ViewInfo: TdxGaugeQuantitativeScaleViewInfo read GetViewInfo;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property MaxValue: Single read GetMaxValue write SetMaxValue stored IsMaxValueStored;
    property MinValue: Single read GetMinValue write SetMinValue;
    property Value: Single read GetValue write SetValue;
  end;

  { TdxGaugeQuantitativeScaleStyleReader }

  TdxGaugeQuantitativeScaleStyleReader = class(TdxGaugeCustomScaleStyleReader)
  protected
    procedure ReadLabelsParameters(ANode: TdxXMLNode; AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
    procedure ReadMajorTicksParameters(ANode: TdxXMLNode; AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
    procedure ReadMinorTicksParameters(ANode: TdxXMLNode; AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
    procedure ReadTicksVisibility(ANode: TdxXMLNode; AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
  protected
    function GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass; override;
    procedure ReadParameters(ANode: TdxXMLNode; AParameters: TdxGaugeCustomScaleDefaultParameters); override;
  end;

implementation

uses
  Types, Math, dxCore, cxFormats, dxGaugeUtils;

type
  TdxGaugeScaleStyleAccess = class(TdxGaugeScaleStyle);

{ TdxGaugeQuantitativeScaleViewInfo }

constructor TdxGaugeQuantitativeScaleViewInfo.Create(AScaleType: TdxGaugeScaleType; const AStyleName: string);
begin
  inherited Create(AScaleType, AStyleName);
  FFont := TFont.Create;
end;

destructor TdxGaugeQuantitativeScaleViewInfo.Destroy;
begin
  FreeAndNil(FFont);
  inherited Destroy;
end;

function TdxGaugeQuantitativeScaleViewInfo.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeQuantitativeScaleParameters;
end;

procedure TdxGaugeQuantitativeScaleViewInfo.Calculate(AScaleParameters: TdxGaugeCustomScaleParameters; const ABounds: TdxRectF);
begin
  FFont.Assign((AScaleParameters as TdxGaugeQuantitativeScaleParameters).Font);
  inherited Calculate(AScaleParameters, ABounds);
  CalculateLabelPrecision;
  Font.Size := Max(Round(Font.Size * Min(ScaleFactor.X, ScaleFactor.Y)), 1);
  CachingElementsLayer;
  CachingValueIndicatorLayer;
end;

procedure TdxGaugeQuantitativeScaleViewInfo.DestroyCachedLayers;
begin
  FreeAndNil(FElementsLayer);
  FreeAndNil(FValueIndicatorLayer);
  inherited DestroyCachedLayers;
end;

procedure TdxGaugeQuantitativeScaleViewInfo.LoadScaleElements;
begin
  inherited LoadScaleElements;
  FMajorTick := Style.GetElement(GetQuantitativeScaleDefaultParameters.MajorTickType);
  FMinorTick := Style.GetElement(GetQuantitativeScaleDefaultParameters.MinorTickType);
end;

function TdxGaugeQuantitativeScaleViewInfo.IsMajorTickVisible(AIndex: Integer): Boolean;
var
  AScaleParameters: TdxGaugeQuantitativeScaleParameters;
  ACanDrawLastTick, ACanDrawFirstTick: Boolean;
begin
  AScaleParameters := GetScaleParameters;
  ACanDrawFirstTick := (AIndex = 0) and AScaleParameters.ShowFirstTick;
  ACanDrawLastTick := (AIndex = AScaleParameters.MajorTickCount - 1) and AScaleParameters.ShowLastTick;
  Result := ((AIndex <> 0) or ACanDrawFirstTick) and ((AIndex <> AScaleParameters.MajorTickCount - 1) or ACanDrawLastTick);
end;

function TdxGaugeQuantitativeScaleViewInfo.GetElementValue(AElementIndex, AElementCount: Integer): Single;
begin
  if AElementCount > 0 then
    Result := GetScaleParameters.MinValue + AElementIndex * GetValueRange / AElementCount
  else
    Result := -1;
end;

function TdxGaugeQuantitativeScaleViewInfo.GetLabelText(AValue: Single): string;
begin
  Result := Format('%*.*f', [Pos(dxFormatSettings.DecimalSeparator,
    dxFloatToStr(AValue, dxFormatSettings.DecimalSeparator)), FLabelPrecision, AValue]);
end;

function TdxGaugeQuantitativeScaleViewInfo.GetScaleDefaultParameters: TdxGaugeQuantitativeScaleDefaultParameters;
begin
  Result := TdxGaugeScaleStyleAccess(Style).DefaultParameters as TdxGaugeQuantitativeScaleDefaultParameters;
end;

function TdxGaugeQuantitativeScaleViewInfo.GetTickDrawParameters(AIsMajorTicks: Boolean): TdxGaugeScaleTickDrawParameters;
var
  AScaleDefaultParameters: TdxGaugeQuantitativeScaleDefaultParameters;
begin
  AScaleDefaultParameters := GetScaleDefaultParameters;
  Result.ShowLabel := AIsMajorTicks;
  Result.ShowTick := GetScaleParameters.ShowTicks;
  if AIsMajorTicks then
  begin
    Result.Image := FMajorTick;
    Result.Offset := AScaleDefaultParameters.MajorTickOffset;
    Result.ScaleFactor := AScaleDefaultParameters.MajorTickScaleFactor;
    Result.Size := GetElementImageSize(Result.Image, AScaleDefaultParameters.MajorTickScaleFactor);
  end
  else
  begin
    Result.Image := FMinorTick;
    Result.Offset := AScaleDefaultParameters.MinorTickOffset;
    Result.ScaleFactor := AScaleDefaultParameters.MinorTickScaleFactor;
    Result.Size := GetElementImageSize(Result.Image, AScaleDefaultParameters.MinorTickScaleFactor);
  end;
end;

function TdxGaugeQuantitativeScaleViewInfo.GetValueRange: Single;
var
  AParameters: TdxGaugeQuantitativeScaleParameters;
begin
  AParameters := GetScaleParameters;
  Result := Abs(AParameters.MaxValue - AParameters.MinValue)
end;

function TdxGaugeQuantitativeScaleViewInfo.GetValueStep: Single;
begin
  Result := GetValueRange / (GetScaleParameters.MajorTickCount - 1);
end;

procedure TdxGaugeQuantitativeScaleViewInfo.CachingElementsLayer;
var
  AScaleParameters: TdxGaugeQuantitativeScaleParameters;
begin
  if NeedRecreateLayerCach(sctElementsLayer) then
  begin
    FreeAndNil(FElementsLayer);
    AScaleParameters := GetScaleParameters;
    if NeedCreateLayerCach(AScaleParameters.ShowTicks or AScaleParameters.ShowLabels) then
      FElementsLayer := CachingLayer(DrawTicks);
  end;
end;

procedure TdxGaugeQuantitativeScaleViewInfo.CachingValueIndicatorLayer;
begin
  if NeedRecreateLayerCach(sctValueIndicatorLayer) then
  begin
    FreeAndNil(FValueIndicatorLayer);
    if NeedCreateLayerCach(GetScaleParameters.ShowValueIndicator) then
      FValueIndicatorLayer := CachingLayer(DrawValueIndicator);
  end;
end;

procedure TdxGaugeQuantitativeScaleViewInfo.CalculateLabelMaxSize(AGPGraphics: TdxGPGraphics);
var
  ATextRect: TdxRectF;
begin
  dxGPGetTextRect(AGPGraphics, GetLabelText(GetMaxLabelValue), FFont, False, cxRectF(cxNullRect), ATextRect);
  FMaxLabelRectSize := dxSizeF(cxRectWidth(ATextRect), cxRectHeight(ATextRect));
end;

procedure TdxGaugeQuantitativeScaleViewInfo.CalculateLabelOffset;
var
  ASize: Single;
begin
  ASize := FMaxLabelRectSize.cx;
  if GetScaleParameters.LabelOffset < 0 then
    ASize := -ASize;
  FLabelOffset := ASize / 2 + GetScaleParameters.LabelOffset * ScaleFactor.X;
end;

procedure TdxGaugeQuantitativeScaleViewInfo.DrawElementsLayer(AGPGraphics: TdxGPGraphics);
begin
  DrawCachedLayer(AGPGraphics, FElementsLayer);
end;

procedure TdxGaugeQuantitativeScaleViewInfo.DrawValueIndicatorLayer(AGPGraphics: TdxGPGraphics);
begin
  DrawCachedLayer(AGPGraphics, FValueIndicatorLayer);
end;

function TdxGaugeQuantitativeScaleViewInfo.GetQuantitativeScaleDefaultParameters: TdxGaugeQuantitativeScaleDefaultParameters;
begin
  Result := ScaleDefaultParameters as TdxGaugeQuantitativeScaleDefaultParameters;
end;

function TdxGaugeQuantitativeScaleViewInfo.GetScaleParameters: TdxGaugeQuantitativeScaleParameters;
begin
  Result := ScaleParameters as TdxGaugeQuantitativeScaleParameters;
end;

function TdxGaugeQuantitativeScaleViewInfo.GetMaxLabelValue: Single;
begin
  Result := RoundTo((GetScaleParameters.MaxValue + GetValueStep) / 10, -4);
end;

procedure TdxGaugeQuantitativeScaleViewInfo.CalculateLabelPrecision;
var
  ARoundingValue: string;
  AFormatSettings: TFormatSettings;
  ADecimalSeparatorPosition: Integer;
begin
  dxGetLocaleFormatSettings(dxGetInvariantLocaleID, AFormatSettings);
  ARoundingValue := dxFloatToStr(RoundTo((GetScaleParameters.MaxValue + GetValueStep) / 10, -4),
    AFormatSettings.DecimalSeparator);
  ADecimalSeparatorPosition := Pos(AFormatSettings.DecimalSeparator, ARoundingValue);
  if ADecimalSeparatorPosition > 0 then
    FLabelPrecision := Length(Copy(ARoundingValue, ADecimalSeparatorPosition + 1, MaxInt)) - 1
  else
    FLabelPrecision := 0;
end;

{ TdxGaugeQuantitativeScale }

constructor TdxGaugeQuantitativeScale.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FFont := TFont.Create;
  FFont.OnChange := FontChangeHandler;
  ApplyStyleFontParameters;
  FNeedUpdateAssignedValues := True;
  GetScaleParameters.MaxValue := 100;
  GetScaleParameters.ShowLabels := True;
  GetScaleParameters.ShowValueIndicator := True;
end;

destructor TdxGaugeQuantitativeScale.Destroy;
begin
  FreeAndNil(FFont);
  inherited Destroy;
end;

function TdxGaugeQuantitativeScale.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeQuantitativeScaleParameters;
end;

function TdxGaugeQuantitativeScale.GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass;
begin
  Result := TdxGaugeQuantitativeScaleViewInfo;
end;

procedure TdxGaugeQuantitativeScale.ApplyStyleParameters;
var
  AParameters: TdxGaugeQuantitativeScaleParameters;
  ADefaultParameters: TdxGaugeQuantitativeScaleDefaultParameters;
begin
  ApplyStyleFontParameters;
  AParameters := GetScaleParameters;
  ADefaultParameters := ViewInfo.DefaultParameters;
  if not(savShowFirstTick in FAssignedValues) then
    AParameters.ShowFirstTick := ADefaultParameters.ShowFirstTick;
  if not(savShowLabels in FAssignedValues) then
    AParameters.ShowLabels := ADefaultParameters.ShowLabels;
  AParameters.LabelOffset := ADefaultParameters.LabelOffset;
  if not(savShowLastTick in FAssignedValues) then
    AParameters.ShowLastTick := ADefaultParameters.ShowLastTick;
  if not(savShowTicks in FAssignedValues) then
    AParameters.ShowTicks := ADefaultParameters.ShowTicks;
  if not(savMajorTickCount in FAssignedValues) then
    AParameters.MajorTickCount := ADefaultParameters.MajorTickCount;
  if not(savMinorTickCount in FAssignedValues) then
    AParameters.MinorTickCount := ADefaultParameters.MinorTickCount;
end;

procedure TdxGaugeQuantitativeScale.Calculate(const AScaleBounds: TdxRectF; const ACachBounds: TRect);
begin
  GetScaleParameters.Font := FFont;
  inherited Calculate(AScaleBounds, ACachBounds);
end;

procedure TdxGaugeQuantitativeScale.InternalDrawLayer(AGPGraphics: TdxGPGraphics; AIndex: Integer);
begin
  case AIndex of
    0:
      DrawBackgroundLayer(AGPGraphics);
    1:
      DrawScaleElementsLayer(AGPGraphics);
    2:
      DrawIndicatorsLayer(AGPGraphics);
  end;
end;

procedure TdxGaugeQuantitativeScale.InternalRestoreStyleParameters;
begin
  FAssignedValues := [];
  GetScaleParameters.ShowValueIndicator := True;
  GetScaleParameters.ShowBackground := True;
  inherited InternalRestoreStyleParameters;
end;

function TdxGaugeQuantitativeScale.GetMajorTickCount: Integer;
begin
  Result := GetScaleParameters.MajorTickCount;
end;

function TdxGaugeQuantitativeScale.GetMaxValue: Single;
begin
  Result := GetScaleParameters.MaxValue;
end;

function TdxGaugeQuantitativeScale.GetMinorTickCount: Integer;
begin
  Result := GetScaleParameters.MinorTickCount;
end;

function TdxGaugeQuantitativeScale.GetMinValue: Single;
begin
  Result := GetScaleParameters.MinValue;
end;

function TdxGaugeQuantitativeScale.GetShowFirstTick: Boolean;
begin
  Result := GetScaleParameters.ShowFirstTick;
end;

function TdxGaugeQuantitativeScale.GetShowLabels: Boolean;
begin
  Result := GetScaleParameters.ShowLabels;
end;

function TdxGaugeQuantitativeScale.GetShowLastTick: Boolean;
begin
  Result := GetScaleParameters.ShowLastTick;
end;

function TdxGaugeQuantitativeScale.GetShowTicks: Boolean;
begin
  Result := GetScaleParameters.ShowTicks;
end;

function TdxGaugeQuantitativeScale.GetShowValueIndicator: Boolean;
begin
  Result := GetScaleParameters.ShowValueIndicator;
end;

function TdxGaugeQuantitativeScale.GetValue: Single;
begin
  Result := FScaleParameters.Value;
end;

procedure TdxGaugeQuantitativeScale.SetFont(const AValue: TFont);
begin
  FFont.Assign(AValue);
end;

procedure TdxGaugeQuantitativeScale.SetMajorTickCount(const AValue: Integer);
begin
  if GetScaleParameters.MajorTickCount <> AValue then
  begin
    GetScaleParameters.MajorTickCount := Max(AValue, 2);
    if GetScaleParameters.MajorTickCount = ViewInfo.DefaultParameters.MajorTickCount then
      Exclude(FAssignedValues, savMajorTickCount)
    else
      Include(FAssignedValues, savMajorTickCount);
    ScaleChanged(sctElementsLayer);
  end;
end;

procedure TdxGaugeQuantitativeScale.SetMaxValue(const AValue: Single);
var
  AParameters: TdxGaugeQuantitativeScaleParameters;
begin
  AParameters := GetScaleParameters;
  if (AParameters.MaxValue <> AValue) and (AValue > AParameters.MinValue) then
  begin
    AParameters.MaxValue := AValue;
    SetScaleValue(Value);
    ScaleChanged(sctElementsLayer);
  end;
end;

procedure TdxGaugeQuantitativeScale.SetMinorTickCount(const AValue: Integer);
begin
  if (GetScaleParameters.MinorTickCount <> AValue) and (AValue >= 0) then
  begin
    GetScaleParameters.MinorTickCount := AValue;
    if GetScaleParameters.MinorTickCount = ViewInfo.DefaultParameters.MinorTickCount then
      Exclude(FAssignedValues, savMinorTickCount)
    else
      Include(FAssignedValues, savMinorTickCount);
    ScaleChanged(sctElementsLayer);
  end;
end;

procedure TdxGaugeQuantitativeScale.SetMinValue(const AValue: Single);
var
  AParameters: TdxGaugeQuantitativeScaleParameters;
begin
  AParameters := GetScaleParameters;
  if (AParameters.MinValue <> AValue) and (AValue < AParameters.MaxValue) then
  begin
    AParameters.MinValue := AValue;
    SetScaleValue(Value);
    ScaleChanged(sctElementsLayer);
  end;
end;

procedure TdxGaugeQuantitativeScale.SetShowFirstTick(const AValue: Boolean);
begin
  if GetScaleParameters.ShowFirstTick <> AValue then
  begin
    GetScaleParameters.ShowFirstTick := AValue;
    if GetScaleParameters.ShowFirstTick = ViewInfo.DefaultParameters.ShowFirstTick then
      Exclude(FAssignedValues, savShowFirstTick)
    else
      Include(FAssignedValues, savShowFirstTick);
    ScaleChanged(sctElementsLayer);
  end;
end;

procedure TdxGaugeQuantitativeScale.SetShowLabels(const AValue: Boolean);
begin
  if GetScaleParameters.ShowLabels <> AValue then
  begin
    GetScaleParameters.ShowLabels := AValue;
    if GetScaleParameters.ShowLabels = ViewInfo.DefaultParameters.ShowLabels then
      Exclude(FAssignedValues, savShowLabels)
    else
      Include(FAssignedValues, savShowLabels);
    ScaleChanged(sctElementsLayer);
  end;
end;

procedure TdxGaugeQuantitativeScale.SetShowLastTick(const AValue: Boolean);
begin
  if GetScaleParameters.ShowLastTick <> AValue then
  begin
    GetScaleParameters.ShowLastTick := AValue;
    if GetScaleParameters.ShowLastTick = ViewInfo.DefaultParameters.ShowLastTick then
      Exclude(FAssignedValues, savShowLastTick)
    else
      Include(FAssignedValues, savShowLastTick);
    ScaleChanged(sctElementsLayer);
  end;
end;

procedure TdxGaugeQuantitativeScale.SetShowTicks(const AValue: Boolean);
begin
  if GetScaleParameters.ShowTicks <> AValue then
  begin
    GetScaleParameters.ShowTicks := AValue;
    if GetScaleParameters.ShowTicks = ViewInfo.DefaultParameters.ShowLastTick then
      Exclude(FAssignedValues, savShowTicks)
    else
      Include(FAssignedValues, savShowTicks);
    ScaleChanged(sctElementsLayer);
  end;
end;

procedure TdxGaugeQuantitativeScale.SetShowValueIndicator(const AValue: Boolean);
begin
  if GetScaleParameters.ShowValueIndicator <> AValue then
  begin
    GetScaleParameters.ShowValueIndicator := AValue;
    ScaleChanged(sctValueIndicatorLayer);
  end;
end;

procedure TdxGaugeQuantitativeScale.SetValue(const AValue: Single);
begin
  if (FScaleParameters.Value <> AValue) then
  begin
    SetScaleValue(AValue);
    ScaleChanged(sctValueIndicatorLayer);
  end;
end;

function TdxGaugeQuantitativeScale.GetScaleParameters: TdxGaugeQuantitativeScaleParameters;
begin
  Result := FScaleParameters as TdxGaugeQuantitativeScaleParameters;
end;

function TdxGaugeQuantitativeScale.GetViewInfo: TdxGaugeQuantitativeScaleViewInfo;
begin
  Result := FViewInfo as TdxGaugeQuantitativeScaleViewInfo;
end;

procedure TdxGaugeQuantitativeScale.ApplyStyleFontParameters;
begin
  if Assigned(FFont) then
  begin
    FNeedUpdateAssignedValues := False;
    if not(savFontName in FAssignedValues) then
      FFont.Name := ViewInfo.DefaultParameters.FontName;
    if not(savFontSize in FAssignedValues) then
      FFont.Size := ViewInfo.DefaultParameters.FontSize;
    if not(savFontColor in FAssignedValues) then
      FFont.Color := ViewInfo.DefaultParameters.FontColor;
    FNeedUpdateAssignedValues := True;
  end;
end;

procedure TdxGaugeQuantitativeScale.DrawIndicatorsLayer(AGPGraphics: TdxGPGraphics);
begin
  ViewInfo.DrawValueIndicatorLayer(AGPGraphics);
end;

procedure TdxGaugeQuantitativeScale.DrawScaleElementsLayer(AGPGraphics: TdxGPGraphics);
begin
  ViewInfo.DrawElementsLayer(AGPGraphics);
end;

procedure TdxGaugeQuantitativeScale.FontChangeHandler(ASender: TObject);
begin
  if FFont.Name = ViewInfo.DefaultParameters.FontName then
    Exclude(FAssignedValues, savFontName)
  else
    if FNeedUpdateAssignedValues then
      Include(FAssignedValues, savFontName);
  if FFont.Color = ViewInfo.DefaultParameters.FontColor then
    Exclude(FAssignedValues, savFontColor)
  else
    if FNeedUpdateAssignedValues then
      Include(FAssignedValues, savFontColor);
  if FFont.Size = ViewInfo.DefaultParameters.FontSize then
    Exclude(FAssignedValues, savFontSize)
  else
    if FNeedUpdateAssignedValues then
      Include(FAssignedValues, savFontSize);
  ScaleChanged(sctElementsLayer);
end;

procedure TdxGaugeQuantitativeScale.SetScaleValue(AValue: Single);
begin
  FScaleParameters.Value := Min(Max(AValue, GetScaleParameters.MinValue), GetScaleParameters.MaxValue);
  Include(FScaleParameters.CalculatableLayers, sctValueIndicatorLayer);
end;

function TdxGaugeQuantitativeScale.IsFontStored: Boolean;
begin
  Result := (savFontColor in FAssignedValues) or (savFontName in FAssignedValues) or (savFontSize in FAssignedValues);
end;

function TdxGaugeQuantitativeScale.IsMajorTickCountStored: Boolean;
begin
  Result := savMajorTickCount in FAssignedValues;
end;

function TdxGaugeQuantitativeScale.IsMaxValueStored: Boolean;
begin
  Result := not SameValue(GetScaleParameters.MaxValue, 100);
end;

function TdxGaugeQuantitativeScale.IsMinorTickCountStored: Boolean;
begin
  Result := savMinorTickCount in FAssignedValues;
end;

function TdxGaugeQuantitativeScale.IsShowFirstTickStored: Boolean;
begin
  Result := savShowFirstTick in FAssignedValues;
end;

function TdxGaugeQuantitativeScale.IsShowLabelsStored: Boolean;
begin
  Result := savShowLabels in FAssignedValues;
end;

function TdxGaugeQuantitativeScale.IsShowLastTickStored: Boolean;
begin
  Result := savShowLastTick in FAssignedValues;
end;

function TdxGaugeQuantitativeScale.IsShowTicksStored: Boolean;
begin
  Result := savShowTicks in FAssignedValues;
end;

{ TdxGaugeCircularScaleStyleReader }

function TdxGaugeQuantitativeScaleStyleReader.GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass;
begin
  Result := TdxGaugeQuantitativeScaleDefaultParameters;
end;

procedure TdxGaugeQuantitativeScaleStyleReader.ReadParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeCustomScaleDefaultParameters);
var
  AScaleParameters: TdxGaugeQuantitativeScaleDefaultParameters;
begin
  AScaleParameters := AParameters as TdxGaugeQuantitativeScaleDefaultParameters;
  ReadFontParameters(ANode, AScaleParameters);
  ReadLabelsParameters(ANode, AScaleParameters);
  ReadTicksVisibility(ANode, AScaleParameters);
  ReadMajorTicksParameters(ANode, AScaleParameters);
  ReadMinorTicksParameters(ANode, AScaleParameters);
end;

procedure TdxGaugeQuantitativeScaleStyleReader.ReadLabelsParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
var
  ALabelsNode: TdxXMLNode;
begin
  if GetChildNode(ANode, 'Labels', ALabelsNode) then
  begin
    AParameters.LabelOffset := GetAttributeValueAsDouble(GetChildNode(ALabelsNode, 'Offset'), 'Value');
    AParameters.ShowLabels := GetAttributeValueAsBoolean(GetChildNode(ALabelsNode, 'ShowLabels'), 'Value');
  end;
end;

procedure TdxGaugeQuantitativeScaleStyleReader.ReadMajorTicksParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
var
  AMajorTicksNode: TdxXMLNode;
begin
  if GetChildNode(ANode, 'MajorTicks', AMajorTicksNode) then
  begin
    AParameters.MajorTickCount := GetAttributeValueAsInteger(GetChildNode(AMajorTicksNode, 'Count'), 'Value');
    AParameters.MajorTickOffset := GetAttributeValueAsDouble(GetChildNode(AMajorTicksNode, 'Offset'), 'Value');
    AParameters.MajorTickScaleFactor := GetAttributeValueAsPointF(GetChildNode(AMajorTicksNode, 'ScaleFactor'), 'Value');
    AParameters.MajorTickType := GetAttributeValueAsElementType(GetChildNode(AMajorTicksNode, 'TickType'), 'Value');
  end;
end;

procedure TdxGaugeQuantitativeScaleStyleReader.ReadMinorTicksParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
var
  AMinorTicksNode: TdxXMLNode;
begin
  if GetChildNode(ANode, 'MinorTicks', AMinorTicksNode) then
  begin
    AParameters.MinorTickCount := GetAttributeValueAsInteger(GetChildNode(AMinorTicksNode, 'Count'), 'Value');
    AParameters.MinorTickOffset := GetAttributeValueAsDouble(GetChildNode(AMinorTicksNode, 'Offset'), 'Value');
    AParameters.MinorTickScaleFactor := GetAttributeValueAsPointF(GetChildNode(AMinorTicksNode, 'ScaleFactor'), 'Value');
    AParameters.MinorTickType := GetAttributeValueAsElementType(GetChildNode(AMinorTicksNode, 'TickType'), 'Value');
  end;
end;

procedure TdxGaugeQuantitativeScaleStyleReader.ReadTicksVisibility(ANode: TdxXMLNode;
  AParameters: TdxGaugeQuantitativeScaleDefaultParameters);
var
  ATicksVisibilityNode: TdxXMLNode;
begin
  if GetChildNode(ANode, 'TicksVisibility', ATicksVisibilityNode) then
  begin
    AParameters.AllowOverlapMajorTicks := GetAttributeValueAsBoolean(GetChildNode(ATicksVisibilityNode,
      'AllowOverlapMajorTicks'), 'Value');
    AParameters.ShowFirstTick := GetAttributeValueAsBoolean(GetChildNode(ATicksVisibilityNode, 'ShowFirstTick'), 'Value');
    AParameters.ShowLastTick := GetAttributeValueAsBoolean(GetChildNode(ATicksVisibilityNode, 'ShowLastTick'), 'Value');
    AParameters.ShowTicks := GetAttributeValueAsBoolean(GetChildNode(ATicksVisibilityNode, 'ShowTicks'), 'Value');
  end;
end;

end.
