{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressGaugeControl                                      }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSGAUGECONTROL AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGaugeDigitalScale;

{$I cxVer.inc}

interface

uses
  Classes, Generics.Collections, cxGeometry, dxCoreGraphics, cxGraphics, dxGDIPlusClasses, dxXMLDoc, dxCompositeShape,
  dxGaugeCustomScale;

type
  TdxGaugeDigitalScaleDisplayMode = (dsdmSevenSegment);
  TdxGaugeDigitalScaleCustomCharset = class;
  TdxGaugeDigitalScaleCustomCharsetClass = class of TdxGaugeDigitalScaleCustomCharset;

  { TdxGaugeDigitalScaleDefaultParameters }

  TdxGaugeDigitalScaleDefaultParameters = class(TdxGaugeCustomScaleDefaultParameters)
  public
    SegmentColorOff: TdxAlphaColor;
    SegmentColorOn: TdxAlphaColor;
  end;

  { TdxGaugeDigitalScaleParameters }

  TdxGaugeDigitalScaleParameters = class(TdxGaugeCustomScaleParameters)
  public
    DigitCount: Integer;
    Stretch: Boolean;
    SegmentColorOff: TdxAlphaColor;
    SegmentColorOn: TdxAlphaColor;
  end;

  { TdxGaugeDigitalScaleViewInfo }

  TdxGaugeDigitalScaleViewInfo = class(TdxGaugeCustomScaleViewInfo)
  private
    FBackgroundScaleFactor: TdxPointF;

    FBackgroundLeftPartBounds: TdxRectF;
    FBackgroundMiddlePartBounds: TdxRectF;
    FBackgroundRightPartBounds: TdxRectF;

    FCharset: TdxGaugeDigitalScaleCustomCharset;
    FDigitSections: array of TdxRectF; 

    FSectionsLayer: TcxBitmap32;

    FDigit: TdxCompositeShape;
    FBackgroundLeftPart: TdxCompositeShape;
    FBackgroundMiddlePart: TdxCompositeShape;
    FBackgroundRightPart: TdxCompositeShape;

    function GetDisplayModeCharsetClass(AMode: TdxGaugeDigitalScaleDisplayMode): TdxGaugeDigitalScaleCustomCharsetClass;

    function GetBackgroundOriginalSize: TdxSizeF;
    function GetBackgroundPartWidth: Single;
    function GetBitState(const AFlags: Integer; AIndex: Integer): Boolean;
    function GetDefaultParameters: TdxGaugeDigitalScaleDefaultParameters;
    function GetDigitResourceName(ADisplayMode: TdxGaugeDigitalScaleDisplayMode): string;
    function GetDigitSize: TdxSizeF;
    function GetScaledBounds: TdxRectF;
    function GetScaleParameters: TdxGaugeDigitalScaleParameters;
    function GetSectionMask(ANextChar, AChar, APrevChar: Char): Integer;
    function GetSectionOffset: Single;
    function GetSectionOriginalSize: TdxSizeF;
    function GetSectionScaledSize: TdxSizeF;
    function GetSegmentColorOff: TdxAlphaColor;
    function GetSegmentColorOn: TdxAlphaColor;
    function GetSegmentCount: Integer;
    function GetSegmentShapeName(ASegmentIndex: Integer): string;
    function SetBitState(const AFlags: Integer; AIndex: Integer): Integer;
    procedure CachingSegmentsLayer;
    procedure CalculateBackgroundScaleFactor;
    procedure CalculateBackgroundMiddlePartBounds;
    procedure CalculateProportionalBackground;
    procedure CalculateStretchableBackground;
    procedure CalculateSections;
    procedure ColorizeDigitSection(const ASectionMask: Integer);
    procedure CreateCharset(ADisplayMode: TdxGaugeDigitalScaleDisplayMode);
    procedure LoadDigit(ADisplayMode: TdxGaugeDigitalScaleDisplayMode);
    procedure DrawSegments(AGPGraphics: TdxGPGraphics);
  protected
    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; override;
    procedure Calculate(AScaleParameters: TdxGaugeCustomScaleParameters; const ABounds: TdxRectF); override;
    procedure CalculateBounds(const ABounds: TdxRectF); override;
    procedure DestroyCachedLayers; override;
    procedure DrawBackground(AGPGraphics: TdxGPGraphics); override;
    procedure LoadScaleElements; override;

    procedure ReloadDigit(ADisplayMode: TdxGaugeDigitalScaleDisplayMode);
    procedure DrawSectionsLayer(AGPGraphics: TdxGPGraphics);

    property DefaultParameters: TdxGaugeDigitalScaleDefaultParameters read GetDefaultParameters;
  public
    constructor Create(AScaleType: TdxGaugeScaleType; const AStyleName: string); override;
    destructor Destroy; override;
  end;

  { TdxGaugeDigitalScale }

  TdxGaugeDigitalScale = class(TdxGaugeCustomScale)
  private
    function GetDigitCount: Integer;
    function GetSegmentColorOff: TdxAlphaColor;
    function GetSegmentColorOn: TdxAlphaColor;
    function GetStretch: Boolean;
    function GetValue: string;
    procedure SetDigitCount(const AValue: Integer);
    procedure SetSegmentColorOff(const AValue: TdxAlphaColor);
    procedure SetSegmentColorOn(const AValue: TdxAlphaColor);
    procedure SetStretch(const AValue: Boolean);
    procedure SetValue(const AValue: string);

    function GetScaleDefaultParameters: TdxGaugeDigitalScaleDefaultParameters;
    function GetScaleParameters: TdxGaugeDigitalScaleParameters;
    function GetViewInfo: TdxGaugeDigitalScaleViewInfo;
    procedure DrawSectionsLayer(AGPGraphics: TdxGPGraphics);

    function IsDefaultColor(const AColor: TdxAlphaColor): Boolean;
    function IsSegmentColorOffStored: Boolean;
    function IsSegmentColorOnStored: Boolean;
    function IsValueStored: Boolean;

    property ScaleParameters: TdxGaugeDigitalScaleParameters read GetScaleParameters;
  protected
    class function GetScaleType: TdxGaugeScaleType; override;
    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; override;
    function GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass; override;

    procedure InternalDrawLayer(AGPGraphics: TdxGPGraphics; AIndex: Integer); override;
    procedure InternalRestoreStyleParameters; override;

    property ViewInfo: TdxGaugeDigitalScaleViewInfo read GetViewInfo;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property AnchorScaleIndex;
    property DigitCount: Integer read GetDigitCount write SetDigitCount default 5;
    property CenterPositionX;
    property CenterPositionFactorX;
    property CenterPositionY;
    property CenterPositionFactorY;
    property CenterPositionType;
    property Height;
    property HeightFactor;
    property Stretch: Boolean read GetStretch write SetStretch default False;
    property SegmentColorOff: TdxAlphaColor read GetSegmentColorOff write SetSegmentColorOff stored IsSegmentColorOffStored
      default dxacDefault;
    property SegmentColorOn: TdxAlphaColor read GetSegmentColorOn write SetSegmentColorOn stored IsSegmentColorOnStored
      default dxacDefault;
    property ShowBackground;
    property StyleName;
    property Width;
    property WidthFactor;
    property Value: string read GetValue write SetValue stored IsValueStored;
    property Visible;
  end;

  { TdxGaugeDigitalScaleStyleReader }

  TdxGaugeDigitalScaleStyleReader = class(TdxGaugeCustomScaleStyleReader)
  protected
    function GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass; override;
    procedure ReadParameters(ANode: TdxXMLNode; AParameters: TdxGaugeCustomScaleDefaultParameters); override;
  end;

  { TdxGaugeDigitalScaleCustomCharset }

  TdxGaugeDigitalScaleCustomCharset = class
  private
    FDictionary: TDictionary<Char, Integer>;
  protected
    function GetApostropheBit: Integer; virtual; abstract;
    function GetDecimalSeparatorBit: Integer; virtual; abstract;
    procedure GetDatetimeSeparatorBits(AIsNextChar: Boolean; out ABit1, ABit2: Integer); virtual; abstract;
    procedure InitDictionary; virtual; abstract;

    property Dictionary: TDictionary<Char, Integer> read FDictionary;
  public
    constructor Create;
    destructor Destroy; override;
  end;

  { TdxGaugeDigitalScaleSevenSegmentModeCharset }

  TdxGaugeDigitalScaleSevenSegmentModeCharset = class(TdxGaugeDigitalScaleCustomCharset)
  protected
    function GetApostropheBit: Integer; override;
    function GetDecimalSeparatorBit: Integer; override;
    procedure GetDatetimeSeparatorBits(AIsNextChar: Boolean; out ABit1, ABit2: Integer); override;
    procedure InitDictionary; override;
  end;

implementation

uses
  Types, SysUtils, Math, dxCore, cxFormats, dxGaugeUtils;

{$R DigitalSegments.res}

const
  dxGaugeDigitalScaleDigitCount = 5;
  dxGaugeDigitalScaleDigitOffset = 0.35;
  dxGaugeDigitalScaleSevenSegmentModeSegmentCount = 11;

type
  TdxCompositeShapeAccess = class(TdxCompositeShape);
  TdxGaugeScaleStyleAccess = class(TdxGaugeScaleStyle);  

function IsDateTimeSeparator(AChar: Char): Boolean;
begin
  Result := AChar = dxFormatSettings.TimeSeparator;
end;

function IsApostrophe(AChar: Char): Boolean;
begin
  Result := (AChar = '''') or (AChar = '"');
end;

function IsDecimalSeparator(AChar: Char): Boolean;
begin
  Result := (AChar = dxFormatSettings.DecimalSeparator) or (AChar = dxFormatSettings.DateSeparator);
end;

{ TdxGaugeDigitalScaleViewInfo }

constructor TdxGaugeDigitalScaleViewInfo.Create(AScaleType: TdxGaugeScaleType; const AStyleName: string);
begin
  inherited Create(AScaleType, AStyleName);
  LoadDigit(dsdmSevenSegment);
end;

destructor TdxGaugeDigitalScaleViewInfo.Destroy;
begin
  FreeAndNil(FCharset);
  FreeAndNil(FDigit);
  inherited Destroy;
end;

function TdxGaugeDigitalScaleViewInfo.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeDigitalScaleParameters;
end;

procedure TdxGaugeDigitalScaleViewInfo.Calculate(AScaleParameters: TdxGaugeCustomScaleParameters;
  const ABounds: TdxRectF);
begin
  inherited Calculate(AScaleParameters, ABounds);
  CachingSegmentsLayer;
end;

procedure TdxGaugeDigitalScaleViewInfo.CalculateBounds(const ABounds: TdxRectF);
begin
  inherited CalculateBounds(ABounds);
  CalculateBackgroundScaleFactor;
  if not GetScaleParameters.Stretch then
    CalculateProportionalBackground
  else
    CalculateStretchableBackground;
  CalculateSections;
end;

procedure TdxGaugeDigitalScaleViewInfo.DestroyCachedLayers;
begin
  FreeAndNil(FSectionsLayer);
  inherited DestroyCachedLayers;
end;

procedure TdxGaugeDigitalScaleViewInfo.DrawBackground(AGPGraphics: TdxGPGraphics);
begin
  dxGaugeDrawImage(AGPGraphics, FBackgroundLeftPart, FBackgroundLeftPartBounds);
  dxGaugeDrawImage(AGPGraphics, FBackgroundMiddlePart, FBackgroundMiddlePartBounds);
  dxGaugeDrawImage(AGPGraphics, FBackgroundRightPart, FBackgroundRightPartBounds);
end;

procedure TdxGaugeDigitalScaleViewInfo.LoadScaleElements;
begin
  inherited LoadScaleElements;
  FBackgroundLeftPart := Style.GetElement(etDigitalBackgroundStart);
  FBackgroundMiddlePart := Style.GetElement(etDigitalBackgroundMiddle);
  FBackgroundRightPart := Style.GetElement(etDigitalBackgroundEnd);
end;

procedure TdxGaugeDigitalScaleViewInfo.ReloadDigit(ADisplayMode: TdxGaugeDigitalScaleDisplayMode);
begin
  FreeAndNil(FDigit);
  LoadDigit(ADisplayMode);
end;

procedure TdxGaugeDigitalScaleViewInfo.DrawSectionsLayer(AGPGraphics: TdxGPGraphics);
begin
  DrawCachedLayer(AGPGraphics, FSectionsLayer);
end;

function TdxGaugeDigitalScaleViewInfo.GetDisplayModeCharsetClass(AMode: TdxGaugeDigitalScaleDisplayMode):
  TdxGaugeDigitalScaleCustomCharsetClass;
begin
  case AMode of
    dsdmSevenSegment:
      Result := TdxGaugeDigitalScaleSevenSegmentModeCharset;
    else
      Result := nil;
  end;
end;

function TdxGaugeDigitalScaleViewInfo.GetBackgroundOriginalSize: TdxSizeF;
begin
  Result.cx := GetDigitSize.cx * GetScaleParameters.DigitCount + GetSectionOffset;
  Result.cy := GetSectionOriginalSize.cy;
end;

function TdxGaugeDigitalScaleViewInfo.GetBackgroundPartWidth: Single;
begin
  Result := GetSectionScaledSize.cx * dxGaugeDigitalScaleDigitOffset * GetDigitSize.cx / GetDigitSize.cy;
end;

function TdxGaugeDigitalScaleViewInfo.GetBitState(const AFlags: Integer; AIndex: Integer): Boolean;
begin
  Result := (AFlags and (1 shl AIndex)) <> 0;
end;

function TdxGaugeDigitalScaleViewInfo.GetDefaultParameters: TdxGaugeDigitalScaleDefaultParameters;
begin
  Result := TdxGaugeScaleStyleAccess(Style).DefaultParameters as TdxGaugeDigitalScaleDefaultParameters;
end;

function TdxGaugeDigitalScaleViewInfo.GetDigitResourceName(ADisplayMode: TdxGaugeDigitalScaleDisplayMode): string;
begin
  case ADisplayMode of
    dsdmSevenSegment:
      Result := 'DigitalScaleSevenSegments';
    else
      Result := '';
  end;
end;

function TdxGaugeDigitalScaleViewInfo.GetDigitSize: TdxSizeF;
begin
  Result.cx := TdxCompositeShapeAccess(FDigit).WidthF;
  Result.cy := TdxCompositeShapeAccess(FDigit).HeightF;
end;

function TdxGaugeDigitalScaleViewInfo.GetScaledBounds: TdxRectF;
var
  ABoundsSize: TdxSizeF;
begin
  ABoundsSize.cx := GetBackgroundOriginalSize.cx * FBackgroundScaleFactor.X;
  ABoundsSize.cy := GetBackgroundOriginalSize.cy * FBackgroundScaleFactor.Y;
  Result := cxRectFBounds(cxRectCenter(FBounds).X - ABoundsSize.cx / 2,
    cxRectCenter(FBounds).Y - ABoundsSize.cy / 2, ABoundsSize.cx, ABoundsSize.cy);
end;

function TdxGaugeDigitalScaleViewInfo.GetScaleParameters: TdxGaugeDigitalScaleParameters;
begin
  Result := ScaleParameters as TdxGaugeDigitalScaleParameters;
end;

function TdxGaugeDigitalScaleViewInfo.GetSectionMask(ANextChar, AChar, APrevChar: Char): Integer;

  procedure ProcessDecimalSeparator(AChar: Char; var ASectionMask: Integer);
  begin
    if IsDecimalSeparator(AChar) then
      ASectionMask := SetBitState(ASectionMask, FCharset.GetDecimalSeparatorBit);
  end;

  procedure ProcessDatetimeSeparator(AChar: Char; AIsNextChar: Boolean; var ASectionMask: Integer);
  var
    ABit1, ABit2: Integer;
  begin
    if IsDateTimeSeparator(AChar) then
    begin
      FCharset.GetDatetimeSeparatorBits(AIsNextChar, ABit1, ABit2);
      ASectionMask := SetBitState(ASectionMask, ABit1);
      ASectionMask := SetBitState(ASectionMask, ABit2);
    end;
  end;

begin
  Result := 0;
  FCharset.Dictionary.TryGetValue(AChar, Result);

  ProcessDecimalSeparator(APrevChar, Result);

  ProcessDatetimeSeparator(APrevChar, False, Result);
  ProcessDatetimeSeparator(AChar, True, Result);
  ProcessDatetimeSeparator(ANextChar, True, Result);
end;

function TdxGaugeDigitalScaleViewInfo.GetSectionOffset: Single;
begin
  Result := 2 * GetDigitSize.cy * dxGaugeDigitalScaleDigitOffset;
end;

function TdxGaugeDigitalScaleViewInfo.GetSectionOriginalSize: TdxSizeF;
begin
  Result.cx := GetDigitSize.cx + GetSectionOffset;
  Result.cy := GetDigitSize.cy + GetSectionOffset;
end;

function TdxGaugeDigitalScaleViewInfo.GetSectionScaledSize: TdxSizeF;
begin
  Result.cx := GetSectionOriginalSize.cx * FBackgroundScaleFactor.X;
  Result.cy := GetSectionOriginalSize.cy * FBackgroundScaleFactor.Y;
end;

function TdxGaugeDigitalScaleViewInfo.GetSegmentColorOff: TdxAlphaColor;
var
  AScaleParameters: TdxGaugeDigitalScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  if AScaleParameters.SegmentColorOff = dxacDefault then
    Result := DefaultParameters.SegmentColorOff
  else
    Result := AScaleParameters.SegmentColorOff;
end;

function TdxGaugeDigitalScaleViewInfo.GetSegmentColorOn: TdxAlphaColor;
var
  AScaleParameters: TdxGaugeDigitalScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  if AScaleParameters.SegmentColorOn = dxacDefault then
    Result := DefaultParameters.SegmentColorOn
  else
    Result := AScaleParameters.SegmentColorOn;
end;

function TdxGaugeDigitalScaleViewInfo.GetSegmentCount: Integer;
begin
  Result := dxGaugeDigitalScaleSevenSegmentModeSegmentCount;
end;

function TdxGaugeDigitalScaleViewInfo.GetSegmentShapeName(ASegmentIndex: Integer): string;
const
  SevenSegmentShapeNameMap: array[0..dxGaugeDigitalScaleSevenSegmentModeSegmentCount] of string =
    ('Path_0', 'Path_1', 'Path_2', 'Path_3', 'Path_4', 'Path_5', 'Path_6', 'Path_7', 'Path_8_1', 'Path_8_2',
    'Path_9_1', 'Path_9_2');

begin
  Result := SevenSegmentShapeNameMap[ASegmentIndex];
end;

function TdxGaugeDigitalScaleViewInfo.SetBitState(const AFlags: Integer; AIndex: Integer): Integer;
begin
  Result := AFlags or (1 shl AIndex);
end;

procedure TdxGaugeDigitalScaleViewInfo.CachingSegmentsLayer;
begin
  if NeedRecreateLayerCach(sctValueIndicatorLayer) then
  begin
    FreeAndNil(FSectionsLayer);
    if NeedCreateLayerCach(True) then
      FSectionsLayer := CachingLayer(DrawSegments);
  end;
end;

procedure TdxGaugeDigitalScaleViewInfo.CalculateBackgroundScaleFactor;
var
  AOriginalSize: TdxSizeF;
begin
  AOriginalSize := GetBackgroundOriginalSize;
  FBackgroundScaleFactor.X := cxRectWidth(FBounds) / AOriginalSize.cx;
  FBackgroundScaleFactor.Y := cxRectHeight(FBounds) / AOriginalSize.cy;
  if not GetScaleParameters.Stretch then
    FBackgroundScaleFactor := dxPointF(Min(FBackgroundScaleFactor.X, FBackgroundScaleFactor.Y),
      Min(FBackgroundScaleFactor.X, FBackgroundScaleFactor.Y))
end;

procedure TdxGaugeDigitalScaleViewInfo.CalculateBackgroundMiddlePartBounds;
begin
  FBackgroundMiddlePartBounds := GetScaledBounds;
  FBackgroundMiddlePartBounds.Left := FBackgroundLeftPartBounds.Right - 1;
  FBackgroundMiddlePartBounds.Right := FBackgroundRightPartBounds.Left + 1;
end;

procedure TdxGaugeDigitalScaleViewInfo.CalculateProportionalBackground;

  function GetBackgroundPartOriginalSizeRatio(ABackgroundPart: TdxCompositeShape): Single;
  begin
    Result := TdxCompositeShapeAccess(ABackgroundPart).WidthF / TdxCompositeShapeAccess(ABackgroundPart).HeightF;
  end;



begin

  FBackgroundMiddlePartBounds := GetScaledBounds;
  FBackgroundMiddlePartBounds.Left := FBackgroundMiddlePartBounds.Left + GetBackgroundPartWidth;
  FBackgroundMiddlePartBounds.Right := FBackgroundMiddlePartBounds.Right - GetBackgroundPartWidth;

  FBackgroundLeftPartBounds := FBackgroundMiddlePartBounds;
  FBackgroundLeftPartBounds.Left := FBackgroundLeftPartBounds.Left - GetScaledBounds.Height *
    GetBackgroundPartOriginalSizeRatio(FBackgroundLeftPart);

  FBackgroundLeftPartBounds.Right := FBackgroundMiddlePartBounds.Left + 1;

  FBackgroundRightPartBounds := FBackgroundMiddlePartBounds;
  FBackgroundRightPartBounds.Right := FBackgroundRightPartBounds.Right + GetScaledBounds.Height *
    GetBackgroundPartOriginalSizeRatio(FBackgroundRightPart);

  FBackgroundRightPartBounds.Left := FBackgroundMiddlePartBounds.Right - 1;
end;

procedure TdxGaugeDigitalScaleViewInfo.CalculateStretchableBackground;
var
  ABounds: TdxRectF;
begin
  ABounds := GetScaledBounds;
  FBackgroundLeftPartBounds := ABounds;
  FBackgroundLeftPartBounds.Right := FBackgroundLeftPartBounds.Left + GetBackgroundPartWidth;

  FBackgroundRightPartBounds := ABounds;
  FBackgroundRightPartBounds.Left := FBackgroundRightPartBounds.Right - GetBackgroundPartWidth;

  FBackgroundMiddlePartBounds := GetScaledBounds;
  FBackgroundMiddlePartBounds.Left := FBackgroundLeftPartBounds.Right - 1;
  FBackgroundMiddlePartBounds.Right := FBackgroundRightPartBounds.Left + 1;
end;

procedure TdxGaugeDigitalScaleViewInfo.CalculateSections;
var
  I: Integer;
  ASize: TdxSizeF;
  AMiddlePartBounds: TdxRectF;
begin
  AMiddlePartBounds := cxRectContent(FBackgroundMiddlePartBounds, cxRectF(1, 0, 1, 0));
  ASize.cx := cxRectWidth(AMiddlePartBounds) / GetScaleParameters.DigitCount;
  if not GetScaleParameters.Stretch then
    ASize.cy := ASize.cx / GetDigitSize.cx * GetDigitSize.cy
  else
    ASize.cy := GetDigitSize.cy * FBackgroundScaleFactor.Y;
  SetLength(FDigitSections, GetScaleParameters.DigitCount);
  for I := Low(FDigitSections) to High(FDigitSections) do
  begin
    FDigitSections[I].Top := cxRectCenter(AMiddlePartBounds).Y - ASize.cy / 2;
    FDigitSections[I].Left := AMiddlePartBounds.Left + ASize.cx * I;
    FDigitSections[I].Right := FDigitSections[I].Left + ASize.cx;
    FDigitSections[I].Bottom := FDigitSections[I].Top + ASize.cy;
  end;
end;

procedure TdxGaugeDigitalScaleViewInfo.ColorizeDigitSection(const ASectionMask: Integer);
var
  I: Integer;
  AColor, AColorOn, AColorOff: TdxAlphaColor;
begin
  AColorOn := GetSegmentColorOn;
  AColorOff := GetSegmentColorOff;
  for I := 0 to GetSegmentCount do
  begin
    if GetBitState(ASectionMask, I) then
      AColor := AColorOn
    else
      AColor := AColorOff;
    TdxCompositeShapeAccess(FDigit).SetShapesColor(AColor, GetSegmentShapeName(I));
  end;
end;

procedure TdxGaugeDigitalScaleViewInfo.CreateCharset(ADisplayMode: TdxGaugeDigitalScaleDisplayMode);
begin
  FreeAndNil(FCharset);
  FCharset := GetDisplayModeCharsetClass(ADisplayMode).Create;
end;

procedure TdxGaugeDigitalScaleViewInfo.LoadDigit(ADisplayMode: TdxGaugeDigitalScaleDisplayMode);
var
  AStream: TResourceStream;
begin
  AStream := TResourceStream.Create(HInstance, GetDigitResourceName(ADisplayMode), RT_RCDATA);
  try
    FDigit := TdxCompositeShape.Create;
    FDigit.LoadFromStream(AStream);
    TdxCompositeShapeAccess(FDigit).NeedNormalize := True;
    CreateCharset(ADisplayMode);
  finally
    FreeAndNil(AStream);
  end;
end;

procedure TdxGaugeDigitalScaleViewInfo.DrawSegments(AGPGraphics: TdxGPGraphics);

  function GetChar(const S: string; AIndex: Integer): Char;
  begin
    if (AIndex > 0) and (AIndex <= Length(S)) then
      Result := S[AIndex]
    else
      Result := ' ';
  end;

var
  I, ACharIndex: Integer;
  AChar, APrevChar, ANextChar: Char;
  S: string;
begin
  S := UpperCase(dxVariantToString(ScaleParameters.Value));
  ACharIndex := Length(S);
  for I := High(FDigitSections) downto Low(FDigitSections) do
  begin
    AChar := GetChar(S, ACharIndex);
    if IsDecimalSeparator(AChar) or IsDateTimeSeparator(AChar) then
      Dec(ACharIndex);
    AChar := GetChar(S, ACharIndex);
    APrevChar := GetChar(S, ACharIndex + 1);
    ANextChar := GetChar(S, ACharIndex - 1);
    ColorizeDigitSection(GetSectionMask(ANextChar, AChar, APrevChar));
    Dec(ACharIndex);
    dxGaugeDrawImage(AGPGraphics, FDigit, FDigitSections[I]);
  end;
end;

{ TdxGaugeDigitalScale }

constructor TdxGaugeDigitalScale.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  ScaleParameters.SegmentColorOff := dxacDefault;
  ScaleParameters.SegmentColorOn := dxacDefault;
  ScaleParameters.DigitCount := dxGaugeDigitalScaleDigitCount;
end;

class function TdxGaugeDigitalScale.GetScaleType: TdxGaugeScaleType;
begin
  Result := stDigitalScale;
end;

function TdxGaugeDigitalScale.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeDigitalScaleParameters;
end;

function TdxGaugeDigitalScale.GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass;
begin
  Result := TdxGaugeDigitalScaleViewInfo;
end;

procedure TdxGaugeDigitalScale.InternalDrawLayer(AGPGraphics: TdxGPGraphics; AIndex: Integer);
begin
  case AIndex of
    0:
      DrawBackgroundLayer(AGPGraphics);
    1:
      DrawSectionsLayer(AGPGraphics);
  end;
end;

procedure TdxGaugeDigitalScale.InternalRestoreStyleParameters;
begin
  if not IsDefaultColor(ScaleParameters.SegmentColorOff) then
    GetScaleParameters.SegmentColorOff := dxacDefault;
  if not IsDefaultColor(ScaleParameters.SegmentColorOn) then
    GetScaleParameters.SegmentColorOn := dxacDefault;
  inherited InternalRestoreStyleParameters;
end;

function TdxGaugeDigitalScale.GetDigitCount: Integer;
begin
  Result := ScaleParameters.DigitCount;
end;

function TdxGaugeDigitalScale.GetSegmentColorOff: TdxAlphaColor;
begin
  Result := ScaleParameters.SegmentColorOff;
end;

function TdxGaugeDigitalScale.GetSegmentColorOn: TdxAlphaColor;
begin
  Result := ScaleParameters.SegmentColorOn;
end;

function TdxGaugeDigitalScale.GetStretch: Boolean;
begin
  Result := ScaleParameters.Stretch;
end;

function TdxGaugeDigitalScale.GetValue: string;
begin
  Result := dxVariantToString(ScaleParameters.Value);
end;

procedure TdxGaugeDigitalScale.SetDigitCount(const AValue: Integer);
begin
  if ScaleParameters.DigitCount <> AValue then
  begin
    ScaleParameters.DigitCount := Max(Min(AValue, 15), 1);
    ScaleChanged;
  end;
end;

procedure TdxGaugeDigitalScale.SetSegmentColorOff(const AValue: TdxAlphaColor);
begin
  if ScaleParameters.SegmentColorOff <> AValue then
  begin
    ScaleParameters.SegmentColorOff := AValue;
    ScaleChanged(sctValueIndicatorLayer);
  end;
end;

procedure TdxGaugeDigitalScale.SetSegmentColorOn(const AValue: TdxAlphaColor);
begin
  if ScaleParameters.SegmentColorOn <> AValue then
  begin
    ScaleParameters.SegmentColorOn := AValue;
    ScaleChanged(sctValueIndicatorLayer);
  end;
end;

procedure TdxGaugeDigitalScale.SetStretch(const AValue: Boolean);
begin
  if ScaleParameters.Stretch <> AValue then
  begin
    ScaleParameters.Stretch := AValue;
    ScaleChanged;
  end;
end;

procedure TdxGaugeDigitalScale.SetValue(const AValue: string);
begin
  if not SameText(GetValue, AValue) then
  begin
    ScaleParameters.Value := AValue;
    ScaleChanged(sctValueIndicatorLayer);
  end;
end;

function TdxGaugeDigitalScale.GetScaleDefaultParameters: TdxGaugeDigitalScaleDefaultParameters;
begin
  Result := TdxGaugeScaleStyleAccess(ViewInfo.Style).DefaultParameters as TdxGaugeDigitalScaleDefaultParameters;
end;

function TdxGaugeDigitalScale.GetScaleParameters: TdxGaugeDigitalScaleParameters;
begin
  Result := FScaleParameters as TdxGaugeDigitalScaleParameters;
end;

function TdxGaugeDigitalScale.GetViewInfo: TdxGaugeDigitalScaleViewInfo;
begin
  Result := FViewInfo as TdxGaugeDigitalScaleViewInfo;
end;

procedure TdxGaugeDigitalScale.DrawSectionsLayer(AGPGraphics: TdxGPGraphics);
begin
  ViewInfo.DrawSectionsLayer(AGPGraphics);
end;

function TdxGaugeDigitalScale.IsDefaultColor(const AColor: TdxAlphaColor): Boolean;
begin
  Result := AColor = dxacDefault;
end;

function TdxGaugeDigitalScale.IsSegmentColorOffStored: Boolean;
begin
  Result := not IsDefaultColor(ScaleParameters.SegmentColorOff) and
    (ScaleParameters.SegmentColorOff <> GetScaleDefaultParameters.SegmentColorOff);
end;

function TdxGaugeDigitalScale.IsSegmentColorOnStored: Boolean;
begin
  Result := not IsDefaultColor(ScaleParameters.SegmentColorOn) and
    (ScaleParameters.SegmentColorOn <> GetScaleDefaultParameters.SegmentColorOn);
end;

function TdxGaugeDigitalScale.IsValueStored: Boolean;
begin
  Result := not SameText(dxVariantToString(GetScaleParameters.Value), '0');
end;

{ TdxGaugeDigitalScaleStyleReader }

function TdxGaugeDigitalScaleStyleReader.GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass;
begin
  Result := TdxGaugeDigitalScaleDefaultParameters;
end;

procedure TdxGaugeDigitalScaleStyleReader.ReadParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeCustomScaleDefaultParameters);
var
  AScaleParameters: TdxGaugeDigitalScaleDefaultParameters;
begin
  AScaleParameters := AParameters as TdxGaugeDigitalScaleDefaultParameters;
  AScaleParameters.SegmentColorOff := GetAttributeValueAsAlphaColor(GetChildNode(ANode, 'SegmentColorOff'), 'Value');
  AScaleParameters.SegmentColorOn := GetAttributeValueAsAlphaColor(GetChildNode(ANode, 'SegmentColorOn'), 'Value');
end;

{ TdxGaugeDigitalScaleCustomCharset }

constructor TdxGaugeDigitalScaleCustomCharset.Create;
begin
  inherited Create;
  FDictionary := TDictionary<Char, Integer>.Create;
  InitDictionary;
end;

destructor TdxGaugeDigitalScaleCustomCharset.Destroy;
begin
  FreeAndNil(FDictionary);
  inherited Destroy;
end;

{ TdxGaugeDigitalScaleSevenSegmentModeCharset }

function TdxGaugeDigitalScaleSevenSegmentModeCharset.GetDecimalSeparatorBit: Integer;
begin
  Result := 7;
end;

function TdxGaugeDigitalScaleSevenSegmentModeCharset.GetApostropheBit: Integer;
begin
  Result := -1;
end;

procedure TdxGaugeDigitalScaleSevenSegmentModeCharset.GetDatetimeSeparatorBits(AIsNextChar: Boolean;
  out ABit1, ABit2: Integer);
begin
  if AIsNextChar then
  begin
    ABit1 := 8;
    ABit2 := 9;
  end
  else
  begin
    ABit1 := 10;
    ABit2 := 11;
  end;
end;

procedure TdxGaugeDigitalScaleSevenSegmentModeCharset.InitDictionary;
begin
  FDictionary.AddOrSetValue('0', $7E); 
  FDictionary.AddOrSetValue('1', $30); 
  FDictionary.AddOrSetValue('2', $6D); 
  FDictionary.AddOrSetValue('3', $79); 
  FDictionary.AddOrSetValue('4', $33); 
  FDictionary.AddOrSetValue('5', $5B); 
  FDictionary.AddOrSetValue('6', $5F); 
  FDictionary.AddOrSetValue('7', $70); 
  FDictionary.AddOrSetValue('8', $7F); 
  FDictionary.AddOrSetValue('9', $7B); 
  FDictionary.AddOrSetValue('-', $1);  
end;

initialization
  dxGaugeScaleStyleFactory.RegisterStyle(stDigitalScale, dxGaugeCleanWhiteStyleName);
  dxGaugeScaleStyleFactory.RegisterStyle(stDigitalScale, dxGaugeAfricaSunsetStyleName);
  dxGaugeScaleStyleFactory.RegisterStyle(stDigitalScale, dxGaugeDarkNightStyleName);
  dxGaugeScaleStyleFactory.RegisterStyle(stDigitalScale, dxGaugeDeepFireStyleName);
  dxGaugeScaleStyleFactory.RegisterStyle(stDigitalScale, dxGaugeIceColdZoneStyleName);
  dxGaugeScaleStyleFactory.RegisterStyle(stDigitalScale, dxGaugeMechanicalStyleName);
  dxGaugeScaleStyleFactory.RegisterStyle(stDigitalScale, dxGaugeMilitaryStyleName);
  dxGaugeScaleStyleFactory.RegisterStyle(stDigitalScale, dxGaugeSportCarStyleName);
  dxGaugeScaleStyleFactory.RegisterStyle(stDigitalScale, dxGaugeWhiteStyleName);

finalization
  dxGaugeUnregisterStyles;

end.
