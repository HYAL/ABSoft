{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressGaugeControl                                      }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSGAUGECONTROL AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGaugeCircularScale;

{$I cxVer.inc}

interface

uses
  Classes, Graphics, cxGeometry, cxGraphics, dxXMLDoc, dxGDIPlusClasses, dxCompositeShape, dxGaugeCustomScale,
  dxGaugeQuantitativeScale;

type
  TdxGaugeCircularScaleAssignedValue = (savAngleEnd, savAngleStart);
  TdxGaugeCircularScaleAssignedValues = set of TdxGaugeCircularScaleAssignedValue;

  { TdxGaugeCircularScaleDefaultParameters }

  TdxGaugeCircularScaleDefaultParameters = class(TdxGaugeQuantitativeScaleDefaultParameters)
  public
    AngleStart: Integer;
    AngleEnd: Integer;
    ScaleArcRadiusFactor: Single;
  end;

  { TdxGaugeCircularScaleParameters }

  TdxGaugeCircularScaleParameters = class(TdxGaugeQuantitativeScaleParameters)
  public
    AngleStart: Integer;
    AngleEnd: Integer;
    ShowSpindleCap: Boolean;
    Radius: Single;
    RadiusFactor: Single;
  end;

  { TdxGaugeCircularScaleViewInfo }

  TdxGaugeCircularScaleViewInfo = class(TdxGaugeQuantitativeScaleViewInfo)
  private
    FNeedle: TdxCompositeShape;
    FSpindleCap: TdxCompositeShape;

    function GetAngleRange: Single;
    function GetMajorTicksAngleStep: Single;
    function GetMinorTicksAngleStep: Single;
    function GetScaleParameters: TdxGaugeCircularScaleParameters;
    function GetScaleDefaultParameters: TdxGaugeCircularScaleDefaultParameters;
    function GetScaleValue: Single;
    function ValueToAngle(AValue: Single): Single;
    procedure CalculateScaleArcRadius;
  protected
    FSpindleCapLayer: TcxBitmap32;

    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; override;

    function GetScaleSize(const ABounds: TdxRectF): TdxSizeF; override;
    procedure Calculate(AScaleParameters: TdxGaugeCustomScaleParameters; const ABounds: TdxRectF); override;
    procedure CalculateBounds(const ABounds: TdxRectF); override;
    procedure DestroyCachedLayers; override;
    procedure DrawLabel(AGPGraphics: TdxGPGraphics; AValue, AOffset: Single); override;
    procedure DrawTick(AGPGraphics: TdxGPGraphics; AValue: Single; AAngle: Single;
      ATickDrawParameters: TdxGaugeScaleTickDrawParameters); override;
    procedure DrawTicks(AGPGraphics: TdxGPGraphics); override;
    procedure DrawValueIndicator(AGPGraphics: TdxGPGraphics); override;
    procedure LoadScaleElements; override;

    procedure CalculateSpindleCapLayer;
    procedure DrawMajorTick(AGPGraphics: TdxGPGraphics; ATickIndex: Integer; AValue, AAngle: Single;
      ATickDrawParameters: TdxGaugeScaleTickDrawParameters);
    procedure DrawMinorTicks(AGPGraphics: TdxGPGraphics; AMajorTickIndex: Integer; AMajorTickAngle: Single;
      ATickDrawParameters: TdxGaugeScaleTickDrawParameters);
    procedure DrawSpindleCap(AGPGraphics: TdxGPGraphics);
    procedure DrawSpindleCapLayer(AGPGraphics: TdxGPGraphics);

    property ScaleDefaultParameters: TdxGaugeCircularScaleDefaultParameters read GetScaleDefaultParameters;
  end;

  { TdxGaugeCircularScale }

  TdxGaugeCircularScale = class(TdxGaugeQuantitativeScale)
  private
    FAssignedValues: TdxGaugeCircularScaleAssignedValues;

    function GetAngleEnd: Integer;
    function GetAngleStart: Integer;
    function GetRadius: Integer;
    function GetRadiusFactor: Single;
    function GetScaleParameters: TdxGaugeCircularScaleParameters;
    function GetShowNeedle: Boolean;
    function GetShowSpindleCap: Boolean;
    function GetViewInfo: TdxGaugeCircularScaleViewInfo;
    procedure SetAngleEnd(const AValue: Integer);
    procedure SetAngleStart(const AValue: Integer);
    procedure SetRadius(const AValue: Integer);
    procedure SetRadiusFactor(const AValue: Single);
    procedure SetShowNeedle(const AValue: Boolean);
    procedure SetShowSpindleCap(const AValue: Boolean);

    function IsAngleEndStored: Boolean;
    function IsAngleStartStored: Boolean;
    function IsRadiusFactorStored: Boolean;
    procedure DrawSpindleCapsLayer(AGPGraphics: TdxGPGraphics);
  protected
    class function GetScaleType: TdxGaugeScaleType; override;
    function GetScaleParametersClass: TdxGaugeCustomScaleParametersClass; override;
    function GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass; override;

    procedure ApplyStyleParameters; override;
    procedure InternalDrawLayer(AGPGraphics: TdxGPGraphics; AIndex: Integer); override;
    procedure InternalRestoreStyleParameters; override;

    property ViewInfo: TdxGaugeCircularScaleViewInfo read GetViewInfo;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property AnchorScaleIndex;
    property AngleEnd: Integer read GetAngleEnd write SetAngleEnd stored IsAngleEndStored;
    property AngleStart: Integer read GetAngleStart write SetAngleStart stored IsAngleStartStored;
    property CenterPositionX;
    property CenterPositionFactorX;
    property CenterPositionY;
    property CenterPositionFactorY;
    property CenterPositionType;
    property Font;
    property MajorTickCount;
    property MinorTickCount;
    property Radius: Integer read GetRadius write SetRadius default 0;
    property RadiusFactor: Single read GetRadiusFactor write SetRadiusFactor stored IsRadiusFactorStored;
    property ShowBackground;
    property ShowFirstTick;
    property ShowLabels;
    property ShowLastTick;
    property ShowNeedle: Boolean read GetShowNeedle write SetShowNeedle default True;
    property ShowSpindleCap: Boolean read GetShowSpindleCap write SetShowSpindleCap default True;
    property ShowTicks;
    property StyleName;
    property Visible;
  end;

  { TdxGaugeCircularScaleStyleReader }

  TdxGaugeCircularScaleStyleReader = class(TdxGaugeQuantitativeScaleStyleReader)
  private
    procedure ReadCircularScaleParameters(ANode: TdxXMLNode; AParameters: TdxGaugeCircularScaleDefaultParameters);
    procedure ReadNeedleParameters(ANode: TdxXMLNode; AParameters: TdxGaugeCircularScaleDefaultParameters);
    procedure ReadSpindleCapParameters(ANode: TdxXMLNode; AParameters: TdxGaugeCircularScaleDefaultParameters);
  protected
    function GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass; override;
    procedure ReadParameters(ANode: TdxXMLNode; AParameters: TdxGaugeCustomScaleDefaultParameters); override;
  end;

implementation

uses
  SysUtils, Math, dxGaugeUtils;

type
  TdxGaugeScaleStyleAccess = class(TdxGaugeScaleStyle);

{ TdxGaugeCircularScaleViewInfo }

function TdxGaugeCircularScaleViewInfo.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeCircularScaleParameters;
end;

function TdxGaugeCircularScaleViewInfo.GetScaleSize(const ABounds: TdxRectF): TdxSizeF;
var
  AScaleParameters: TdxGaugeCircularScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  if AScaleParameters.Radius <> 0 then
  begin
    Result.cx := AScaleParameters.Radius * 2;
    Result.cy := Result.cx;
  end
  else
  begin
    Result.cx := AScaleParameters.RadiusFactor * cxRectWidth(ABounds) * 2;
    Result.cy := AScaleParameters.RadiusFactor * cxRectHeight(ABounds) * 2;
  end;
end;

procedure TdxGaugeCircularScaleViewInfo.Calculate(AScaleParameters: TdxGaugeCustomScaleParameters;
  const ABounds: TdxRectF);
begin
  inherited Calculate(AScaleParameters, ABounds);
  CalculateSpindleCapLayer;
end;

procedure TdxGaugeCircularScaleViewInfo.CalculateBounds(const ABounds: TdxRectF);
var
  ASize: Single;
begin
  inherited CalculateBounds(ABounds);
  ASize := Min(cxRectWidth(FBounds) / 2, cxRectHeight(FBounds) / 2);
  FBounds := cxRectInflate(cxRectF(cxRectCenter(FBounds), cxRectCenter(FBounds)), ASize, ASize);
  CalculateScaleArcRadius;
end;

procedure TdxGaugeCircularScaleViewInfo.DestroyCachedLayers;
begin
  FreeAndNil(FSpindleCapLayer);
  inherited DestroyCachedLayers;
end;

procedure TdxGaugeCircularScaleViewInfo.DrawLabel(AGPGraphics: TdxGPGraphics; AValue, AOffset: Single);
begin
  if GetScaleParameters.ShowLabels then
    dxGaugeDrawText(AGPGraphics, GetLabelText(AValue), FBounds, AOffset, ValueToAngle(AValue), Font);
end;

procedure TdxGaugeCircularScaleViewInfo.DrawTick(AGPGraphics: TdxGPGraphics; AValue: Single; AAngle: Single;
  ATickDrawParameters: TdxGaugeScaleTickDrawParameters);
var
  AScaleParameters: TdxGaugeCircularScaleParameters;
begin
  AScaleParameters := GetScaleParameters;
  if ATickDrawParameters.ShowTick then
    dxGaugeRotateAndDrawImage(AGPGraphics, ATickDrawParameters.Image, ATickDrawParameters.Size, FBounds,
      AScaleParameters.Radius + ATickDrawParameters.Offset * ScaleFactor.X, AAngle);

  if ATickDrawParameters.ShowLabel and AScaleParameters.ShowLabels then
    DrawLabel(AGPGraphics, AValue, AScaleParameters.Radius + FLabelOffset);
end;

procedure TdxGaugeCircularScaleViewInfo.DrawTicks(AGPGraphics: TdxGPGraphics);
var
  I: Integer;
  AAngle, AAngleStep, AValue, AValueStep: Single;
  AScaleParameters: TdxGaugeCircularScaleParameters;
  AMajorTickDrawParameters, AMinorTickDrawParameters: TdxGaugeScaleTickDrawParameters;
begin
  AScaleParameters := GetScaleParameters;
  if AScaleParameters.ShowTicks or AScaleParameters.ShowLabels then
  begin
    CalculateLabelMaxSize(AGPGraphics);
    CalculateLabelOffset;
    AAngle := AScaleParameters.AngleStart;
    AAngleStep := GetMajorTicksAngleStep;
    AValue := AScaleParameters.MinValue;
    AValueStep := GetValueStep;
    AMajorTickDrawParameters := GetTickDrawParameters(True);
    AMinorTickDrawParameters := GetTickDrawParameters(False);
    for I := 0 to GetScaleParameters.MajorTickCount - 1 do
    begin
      DrawMajorTick(AGPGraphics, I, AValue, AAngle, AMajorTickDrawParameters);
      DrawMinorTicks(AGPGraphics, I, AAngle, AMinorTickDrawParameters);
      AAngle := AAngle - AAngleStep;
      AValue := AValue + AValueStep;
    end;
  end;
end;

procedure TdxGaugeCircularScaleViewInfo.DrawValueIndicator(AGPGraphics: TdxGPGraphics);

  function GetStartOffset: Single;
  begin
    Result := ScaleDefaultParameters.ValueIndicatorStartOffset * ScaleFactor.X;
  end;

  function GetImageSize(AImage: TGraphic): TdxSizeF;
  begin
    Result.cx := GetScaleParameters.Radius - ScaleDefaultParameters.ValueIndicatorEndOffset *
      ScaleFactor.X - GetStartOffset;
    Result.cy := AImage.Height * Result.cx / AImage.Width;
  end;

begin
  dxGaugeRotateAndDrawImage(AGPGraphics, FNeedle, GetImageSize(FNeedle), FBounds, GetStartOffset,
    ValueToAngle(GetScaleValue));
end;

procedure TdxGaugeCircularScaleViewInfo.LoadScaleElements;
begin
  inherited LoadScaleElements;
  FNeedle := Style.GetElement(etNeedle);
  FSpindleCap := Style.GetElement(etSpindleCap);
end;

procedure TdxGaugeCircularScaleViewInfo.DrawMajorTick(AGPGraphics: TdxGPGraphics; ATickIndex: Integer; AValue, AAngle: Single;
  ATickDrawParameters: TdxGaugeScaleTickDrawParameters);
begin
  if IsMajorTickVisible(ATickIndex) then
    DrawTick(AGPGraphics, AValue, AAngle, ATickDrawParameters);
end;

procedure TdxGaugeCircularScaleViewInfo.CalculateSpindleCapLayer;
begin
  if NeedRecreateLayerCach(sctSpindleCapLayer) then
  begin
    FreeAndNil(FSpindleCapLayer);
    if NeedCreateLayerCach(GetScaleParameters.ShowSpindleCap and not FSpindleCap.Empty) then
      FSpindleCapLayer := CachingLayer(DrawSpindleCap);
  end;
end;

procedure TdxGaugeCircularScaleViewInfo.DrawMinorTicks(AGPGraphics: TdxGPGraphics; AMajorTickIndex: Integer;
  AMajorTickAngle: Single; ATickDrawParameters: TdxGaugeScaleTickDrawParameters);

  function GetAngle: Single;
  begin
    if ScaleDefaultParameters.AllowOverlapMajorTicks then
      Result := AMajorTickAngle
    else
      Result := AMajorTickAngle - GetMinorTicksAngleStep;
  end;

var
  I: Integer;
  AAngle, AAngleStep: Single;
begin
  AAngle := GetAngle;
  AAngleStep := GetMinorTicksAngleStep;
  if AMajorTickIndex < GetScaleParameters.MajorTickCount - 1 then
    for I := 0 to GetScaleParameters.MinorTickCount - 1 do
    begin
      DrawTick(AGPGraphics, 0, AAngle, ATickDrawParameters);
      AAngle := AAngle - AAngleStep;
    end;
  if ScaleDefaultParameters.AllowOverlapMajorTicks then
    DrawTick(AGPGraphics, 0, AAngle, ATickDrawParameters);
end;

procedure TdxGaugeCircularScaleViewInfo.DrawSpindleCap(AGPGraphics: TdxGPGraphics);
var
  R: TdxRectF;
  AScaleDefaultParameters: TdxGaugeCircularScaleDefaultParameters;
begin
  AScaleDefaultParameters := GetScaleDefaultParameters;
  R := cxRectF(cxRectCenter(FBounds), cxRectCenter(FBounds));
  R := cxRectInflate(R, AScaleDefaultParameters.SpindleCapSize / 2 * ScaleFactor.X,
    AScaleDefaultParameters.SpindleCapSize / 2 * ScaleFactor.X);
  dxGaugeDrawImage(AGPGraphics, FSpindleCap, R);
end;

procedure TdxGaugeCircularScaleViewInfo.DrawSpindleCapLayer(AGPGraphics: TdxGPGraphics);
begin
  DrawCachedLayer(AGPGraphics, FSpindleCapLayer);
end;

function TdxGaugeCircularScaleViewInfo.GetAngleRange: Single;
begin
  Result := GetScaleParameters.AngleStart - GetScaleParameters.AngleEnd;
end;

function TdxGaugeCircularScaleViewInfo.GetMajorTicksAngleStep: Single;
begin
  Result := GetAngleRange / (GetScaleParameters.MajorTickCount - 1);
end;

function TdxGaugeCircularScaleViewInfo.GetMinorTicksAngleStep: Single;
begin
  Result := GetMajorTicksAngleStep / (GetScaleParameters.MinorTickCount + 1);
end;

function TdxGaugeCircularScaleViewInfo.GetScaleParameters: TdxGaugeCircularScaleParameters;
begin
  Result := ScaleParameters as TdxGaugeCircularScaleParameters;
end;

function TdxGaugeCircularScaleViewInfo.GetScaleDefaultParameters: TdxGaugeCircularScaleDefaultParameters;
begin
  Result := TdxGaugeScaleStyleAccess(Style).DefaultParameters as TdxGaugeCircularScaleDefaultParameters;
end;

function TdxGaugeCircularScaleViewInfo.GetScaleValue: Single;
begin
  Result := GetScaleParameters.Value;
end;

function TdxGaugeCircularScaleViewInfo.ValueToAngle(AValue: Single): Single;
begin
  Result := GetScaleParameters.AngleStart - (AValue - GetScaleParameters.MinValue) * GetAngleRange / GetValueRange;
end;

procedure TdxGaugeCircularScaleViewInfo.CalculateScaleArcRadius;
var
  ADefaultParameters: TdxGaugeCircularScaleDefaultParameters;
begin
  ADefaultParameters := GetScaleDefaultParameters;
  GetScaleParameters.Radius := Min(cxRectWidth(FBounds) / 2 * ADefaultParameters.ScaleArcRadiusFactor,
    cxRectHeight(FBounds) / 2 * ADefaultParameters.ScaleArcRadiusFactor);
end;

{ TdxGaugeCircularScale }

constructor TdxGaugeCircularScale.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  GetScaleParameters.RadiusFactor := 0.5;
  GetScaleParameters.ShowSpindleCap := True;  
end;

class function TdxGaugeCircularScale.GetScaleType: TdxGaugeScaleType;
begin
  Result := stCircularScale;
end;

function TdxGaugeCircularScale.GetScaleParametersClass: TdxGaugeCustomScaleParametersClass;
begin
  Result := TdxGaugeCircularScaleParameters;
end;

function TdxGaugeCircularScale.GetViewInfoClass: TdxGaugeCustomScaleViewInfoClass;
begin
  Result := TdxGaugeCircularScaleViewInfo;
end;

procedure TdxGaugeCircularScale.ApplyStyleParameters;
var
  AParameters: TdxGaugeCircularScaleParameters;
  ADefaultParameters: TdxGaugeCircularScaleDefaultParameters;
begin
  inherited ApplyStyleParameters;
  AParameters := GetScaleParameters;
  ADefaultParameters := GetViewInfo.GetScaleDefaultParameters;
  if not(savAngleEnd in FAssignedValues) then
    AParameters.AngleEnd := ADefaultParameters.AngleEnd;
  if not(savAngleStart in FAssignedValues) then
    AParameters.AngleStart := ADefaultParameters.AngleStart;
end;

procedure TdxGaugeCircularScale.InternalDrawLayer(AGPGraphics: TdxGPGraphics; AIndex: Integer);
begin
  inherited InternalDrawLayer(AGPGraphics, AIndex);
  if AIndex = 3 then
    DrawSpindleCapsLayer(AGPGraphics);
end;

procedure TdxGaugeCircularScale.InternalRestoreStyleParameters;
begin
  FAssignedValues := [];
  GetScaleParameters.ShowSpindleCap := True;  
  inherited InternalRestoreStyleParameters;
end;

function TdxGaugeCircularScale.GetAngleEnd: Integer;
begin
  Result := GetScaleParameters.AngleEnd;
end;

function TdxGaugeCircularScale.GetAngleStart: Integer;
begin
  Result := GetScaleParameters.AngleStart;
end;

function TdxGaugeCircularScale.GetRadius: Integer;
begin
  Result := Round(GetScaleParameters.Radius);
end;

function TdxGaugeCircularScale.GetRadiusFactor: Single;
begin
  Result := GetScaleParameters.RadiusFactor;
end;

function TdxGaugeCircularScale.GetScaleParameters: TdxGaugeCircularScaleParameters;
begin
  Result := FScaleParameters as TdxGaugeCircularScaleParameters;
end;

function TdxGaugeCircularScale.GetShowNeedle: Boolean;
begin
  Result := ShowValueIndicator;
end;

function TdxGaugeCircularScale.GetShowSpindleCap: Boolean;
begin
  Result := GetScaleParameters.ShowSpindleCap;
end;

function TdxGaugeCircularScale.GetViewInfo: TdxGaugeCircularScaleViewInfo;
begin
  Result := FViewInfo as TdxGaugeCircularScaleViewInfo;
end;

procedure TdxGaugeCircularScale.SetAngleEnd(const AValue: Integer);
begin
  if GetScaleParameters.AngleEnd <> AValue then
  begin
    GetScaleParameters.AngleEnd := AValue;
    if GetScaleParameters.AngleStart = GetScaleParameters.AngleEnd then
      GetScaleParameters.AngleEnd := GetScaleParameters.AngleEnd - 360;
    if GetScaleParameters.AngleEnd = ViewInfo.ScaleDefaultParameters.AngleEnd then
      Exclude(FAssignedValues, savAngleEnd)
    else
      Include(FAssignedValues, savAngleEnd);
    Include(FScaleParameters.CalculatableLayers, sctValueIndicatorLayer);
    ScaleChanged(sctElementsLayer);
  end;
end;

procedure TdxGaugeCircularScale.SetAngleStart(const AValue: Integer);
begin
  if GetScaleParameters.AngleStart <> AValue then
  begin
    GetScaleParameters.AngleStart := AValue;
    if GetScaleParameters.AngleStart = GetScaleParameters.AngleEnd then
      GetScaleParameters.AngleStart := GetScaleParameters.AngleStart - 360;
    if GetScaleParameters.AngleStart = ViewInfo.ScaleDefaultParameters.AngleStart then
      Exclude(FAssignedValues, savAngleStart)
    else
      Include(FAssignedValues, savAngleStart);
    Include(FScaleParameters.CalculatableLayers, sctValueIndicatorLayer);      
    ScaleChanged(sctElementsLayer);
  end;
end;

procedure TdxGaugeCircularScale.SetRadius(const AValue: Integer);
begin
  if (GetScaleParameters.Radius <> AValue) and (AValue >= 0) then
  begin
    GetScaleParameters.Radius := AValue;
    ScaleChanged;
  end;
end;

procedure TdxGaugeCircularScale.SetRadiusFactor(const AValue: Single);
begin
  if not SameValue(GetScaleParameters.RadiusFactor, AValue) and (AValue > 0) then
  begin
    GetScaleParameters.RadiusFactor := AValue;
    ScaleChanged;
  end;
end;

procedure TdxGaugeCircularScale.SetShowNeedle(const AValue: Boolean);
begin
  if GetScaleParameters.ShowValueIndicator <> AValue then
  begin
    GetScaleParameters.ShowValueIndicator := AValue;
    ScaleChanged(sctValueIndicatorLayer);
  end;
end;

procedure TdxGaugeCircularScale.SetShowSpindleCap(const AValue: Boolean);
begin
  if GetScaleParameters.ShowSpindleCap <> AValue then
  begin
    GetScaleParameters.ShowSpindleCap := AValue;
    ScaleChanged(sctSpindleCapLayer);
  end;
end;

function TdxGaugeCircularScale.IsAngleEndStored: Boolean;
begin
  Result := savAngleEnd in FAssignedValues;
end;

function TdxGaugeCircularScale.IsAngleStartStored: Boolean;
begin
  Result := savAngleStart in FAssignedValues;
end;

function TdxGaugeCircularScale.IsRadiusFactorStored: Boolean;
begin
  Result := not SameValue(GetScaleParameters.RadiusFactor, 0.5);
end;

procedure TdxGaugeCircularScale.DrawSpindleCapsLayer(AGPGraphics: TdxGPGraphics);
begin
  ViewInfo.DrawSpindleCapLayer(AGPGraphics);
end;

{ TdxGaugeCircularScaleStyleReader }

function TdxGaugeCircularScaleStyleReader.GetDefaultParametersClass: TdxGaugeCustomScaleDefaultParametersClass;
begin
  Result := TdxGaugeCircularScaleDefaultParameters;
end;

procedure TdxGaugeCircularScaleStyleReader.ReadParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeCustomScaleDefaultParameters);
begin
  inherited ReadParameters(ANode, AParameters);
  ReadCircularScaleParameters(ANode, AParameters as TdxGaugeCircularScaleDefaultParameters);
end;

procedure TdxGaugeCircularScaleStyleReader.ReadCircularScaleParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeCircularScaleDefaultParameters);
var
  ACircularScaleNode: TdxXMLNode;
begin
  if GetChildNode(ANode, 'CircularScale', ACircularScaleNode) then
  begin
    ReadNeedleParameters(ACircularScaleNode, AParameters);
    ReadSpindleCapParameters(ACircularScaleNode, AParameters);
    AParameters.AngleEnd := GetAttributeValueAsInteger(GetChildNode(ACircularScaleNode, 'AngleEnd'), 'Value');
    AParameters.AngleStart := GetAttributeValueAsInteger(GetChildNode(ACircularScaleNode, 'AngleStart'), 'Value');
    AParameters.ScaleArcRadiusFactor := GetAttributeValueAsDouble(GetChildNode(ACircularScaleNode,
      'ArcRadiusFactor'), 'Value');
  end;
end;

procedure TdxGaugeCircularScaleStyleReader.ReadNeedleParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeCircularScaleDefaultParameters);
var
  ANeedleNode: TdxXMLNode;
begin
  if GetChildNode(ANode, 'Needle', ANeedleNode) then
  begin
    AParameters.ValueIndicatorStartOffset := GetAttributeValueAsDouble(GetChildNode(ANeedleNode, 'StartOffset'), 'Value');
    AParameters.ValueIndicatorEndOffset := GetAttributeValueAsDouble(GetChildNode(ANeedleNode, 'EndOffset'), 'Value');
  end;
end;

procedure TdxGaugeCircularScaleStyleReader.ReadSpindleCapParameters(ANode: TdxXMLNode;
  AParameters: TdxGaugeCircularScaleDefaultParameters);
var
  ASpindleCapNode: TdxXMLNode;
begin
  if GetChildNode(ANode, 'SpindleCap', ASpindleCapNode) then
    AParameters.SpindleCapSize := GetAttributeValueAsDouble(GetChildNode(ASpindleCapNode, 'Size'), 'Value');
end;

initialization
  dxGaugeScaleStyleFactory.RegisterStyle(stCircularScale, dxGaugeCleanWhiteStyleName);
  dxGaugeScaleStyleFactory.RegisterStyle(stCircularScale, dxGaugeAfricaSunsetStyleName);
  dxGaugeScaleStyleFactory.RegisterStyle(stCircularScale, dxGaugeDarkNightStyleName);
  dxGaugeScaleStyleFactory.RegisterStyle(stCircularScale, dxGaugeDeepFireStyleName);
  dxGaugeScaleStyleFactory.RegisterStyle(stCircularScale, dxGaugeIceColdZoneStyleName);
  dxGaugeScaleStyleFactory.RegisterStyle(stCircularScale, dxGaugeMechanicalStyleName);
  dxGaugeScaleStyleFactory.RegisterStyle(stCircularScale, dxGaugeMilitaryStyleName);
  dxGaugeScaleStyleFactory.RegisterStyle(stCircularScale, dxGaugeSportCarStyleName);
  dxGaugeScaleStyleFactory.RegisterStyle(stCircularScale, dxGaugeWhiteStyleName);

finalization
  dxGaugeUnregisterStyles;

end.
