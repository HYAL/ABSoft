{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressGaugeControl                                      }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSGAUGECONTROL AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGaugeControl;

{$I cxVer.inc}

interface

uses
  SysUtils, Classes, Windows, Graphics,
  cxClasses, cxControls, cxGeometry, cxLookAndFeels,
  dxGaugeCustomScale;

const
  dxGaugeControlWidth = 250;
  dxGaugeControlHeigth = 250;

type
  { TdxCustomGaugeControl }

  TdxCustomGaugeControl = class(TcxControl)
  private
    FLayerCount: Integer;
    FLockCount: Integer;
    FScales: TdxGaugeScaleCollection;
    FTransparent: Boolean;

    procedure SetTransparent(const AValue: Boolean);
    procedure ScaleChangeHandler(Sender: TObject; AItem: TcxComponentCollectionItem;
      AAction: TcxComponentCollectionNotification);

    function CanCalculate(const ABounds: TRect): Boolean;
    function GetScaleBounds(AScaleIndex: Integer): TdxRectF;
    function GetScaleClass(AScaleType: TdxGaugeScaleType): TdxGaugeCustomScaleClass;
    procedure Calculate;
    procedure CalculateScales;
    procedure Changed;
  protected
    function HasBackground: Boolean; override;
    function IsTransparentBackground: Boolean; override;
    function NeedRedrawOnResize: Boolean; override;
    procedure ChangeScale(M, D: Integer); override;    
    procedure DoPaint; override;
    procedure Loaded; override;
    procedure LookAndFeelChanged(Sender: TcxLookAndFeel; AChangedValues: TcxLookAndFeelValues); override;
    procedure Resize; override;

    function GetScales: TdxGaugeScaleCollection;
    function GetScaleIndex(AScale: TdxGaugeCustomScale): Integer;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure Assign(ASource: TPersistent); override;
    procedure GetChildren(AProc: TGetChildProc; ARoot: TComponent); override;

    function AddScale(AType: TdxGaugeScaleType): TdxGaugeCustomScale;
    procedure DeleteScale(AIndex: Integer);
    procedure Clear;

    procedure BeginUpdate;
    procedure CancelUpdate;
    procedure EndUpdate;

    property Scales: TdxGaugeScaleCollection read GetScales;
    property Transparent: Boolean read FTransparent write SetTransparent default False;
  end;

  { TdxGaugeControl }

  TdxGaugeControl = class(TdxCustomGaugeControl)
  published
    property Align;
    property Anchors;
    property BorderStyle default cxcbsDefault;
    property Color default clWindow;
    property Constraints;
    property LookAndFeel;
    property ParentColor default False;
    property Scales;
    property ShowHint default False;
    property Transparent;
    property Visible;

    property OnClick;
    property OnDblClick;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
  end;

implementation

uses
  Types, Math, dxGDIPlusClasses, dxGaugeQuantitativeScale, dxGaugeCircularScale, dxGaugeDigitalScale;

type
  TdxGaugeCustomScaleAccess = class(TdxGaugeCustomScale);
  TdxGaugeQuantitativeScaleAccess = class(TdxGaugeQuantitativeScale);

{ TdxCustomGaugeControl }

constructor TdxCustomGaugeControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  BorderStyle := cxcbsDefault;
  Color := clWindow;
  DoubleBuffered := True;  
  ParentColor := False;
  Width := dxGaugeControlWidth;
  Height := dxGaugeControlHeigth;

  FScales := TdxGaugeScaleCollection.Create(Self, TdxGaugeCustomScale);
  FScales.OnChange := ScaleChangeHandler;
end;

destructor TdxCustomGaugeControl.Destroy;
begin
  FreeAndNil(FScales);
  inherited Destroy;
end;

procedure TdxCustomGaugeControl.Assign(ASource: TPersistent);
begin
  if ASource is TdxCustomGaugeControl then
  begin
    BeginUpdate;
    Scales.Assign(TdxCustomGaugeControl(ASource).Scales);
    Transparent := TdxCustomGaugeControl(ASource).Transparent;
    EndUpdate;
  end
  else
    inherited Assign(ASource);
end;

procedure TdxCustomGaugeControl.GetChildren(AProc: TGetChildProc; ARoot: TComponent);
var
  I: Integer;
begin
  for I := 0 to Scales.Count - 1 do
    if Scales[I].Owner = ARoot then
      AProc(Scales[I]);
end;

function TdxCustomGaugeControl.AddScale(AType: TdxGaugeScaleType): TdxGaugeCustomScale;
begin
  Result := FScales.Add(GetScaleClass(AType));
end;

procedure TdxCustomGaugeControl.DeleteScale(AIndex: Integer);
begin
  FScales.Delete(AIndex);
end;

procedure TdxCustomGaugeControl.Clear;
begin
  FScales.Clear;
end;

procedure TdxCustomGaugeControl.BeginUpdate;
begin
  Inc(FLockCount);
end;

procedure TdxCustomGaugeControl.CancelUpdate;
begin
  Dec(FLockCount);
end;

procedure TdxCustomGaugeControl.EndUpdate;
begin
  Dec(FLockCount);
  if FLockCount = 0 then
    Changed;
end;

function TdxCustomGaugeControl.HasBackground: Boolean;
begin
  Result := True;
end;

function TdxCustomGaugeControl.IsTransparentBackground: Boolean;
begin
  Result := Transparent;
end;

function TdxCustomGaugeControl.NeedRedrawOnResize: Boolean;
begin
  Result := True;
end;

procedure TdxCustomGaugeControl.ChangeScale(M, D: Integer);
var
  I: Integer;
begin
  inherited ChangeScale(M, D);
  for I := 0 to Scales.Count - 1 do
    if Scales[I] is TdxGaugeQuantitativeScale then
      TdxGaugeQuantitativeScaleAccess(Scales[I]).Font.Height := MulDiv(TdxGaugeQuantitativeScaleAccess(Scales[I]).Font.Height, M, D);
end;

procedure TdxCustomGaugeControl.DoPaint;
var
  I, J: Integer;
  AGPGraphics: TdxGPGraphics;
begin
  inherited DoPaint;
  if FLockCount = 0 then
  begin
    AGPGraphics := dxGpBeginPaint(Canvas.Handle, ClientBounds);
    try
      AGPGraphics.SmoothingMode := smAntiAlias;
      for I := 0 to FLayerCount - 1 do
        for J := 0 to FScales.Count - 1 do
          TdxGaugeCustomScaleAccess(FScales[J]).DrawLayer(AGPGraphics, I);
    finally
      dxGpEndPaint(AGPGraphics);
    end;
  end;
end;

procedure TdxCustomGaugeControl.Loaded;
begin
  inherited Loaded;
  Changed;
end;

procedure TdxCustomGaugeControl.LookAndFeelChanged(Sender: TcxLookAndFeel; AChangedValues: TcxLookAndFeelValues);
begin
  Invalidate;
end;

procedure TdxCustomGaugeControl.Resize;
begin
  inherited Resize;
  Changed;
end;

function TdxCustomGaugeControl.GetScales: TdxGaugeScaleCollection;
begin
  Result := FScales;
end;

function TdxCustomGaugeControl.GetScaleIndex(AScale: TdxGaugeCustomScale): Integer;
begin
  Result := FScales.IndexOf(AScale);
end;

procedure TdxCustomGaugeControl.SetTransparent(const AValue: Boolean);
begin
  if FTransparent <> AValue then
  begin
    FTransparent := AValue;
    Changed;
  end;
end;

procedure TdxCustomGaugeControl.ScaleChangeHandler(Sender: TObject; AItem: TcxComponentCollectionItem;
  AAction: TcxComponentCollectionNotification);
begin
  Changed;
end;

function TdxCustomGaugeControl.CanCalculate(const ABounds: TRect): Boolean;
begin
  Result := (cxRectWidth(ABounds) > 0) and (cxRectHeight(ABounds) > 0);
end;

function TdxCustomGaugeControl.GetScaleBounds(AScaleIndex: Integer): TdxRectF;
begin
  if (TdxGaugeCustomScaleAccess(FScales[AScaleIndex]).AnchorScaleIndex = -1) or (AScaleIndex = 0) then
    Result := cxRectF(ClientBounds)
  else
    Result := TdxGaugeCustomScaleAccess(FScales[0]).Bounds;
end;

function TdxCustomGaugeControl.GetScaleClass(AScaleType: TdxGaugeScaleType): TdxGaugeCustomScaleClass;
begin
  case AScaleType of
    stCircularScale:
      Result := TdxGaugeCircularScale;
    stDigitalScale:
      Result := TdxGaugeDigitalScale;
    else
      Result := nil;
  end;
end;

procedure TdxCustomGaugeControl.Calculate;
begin
  if HandleAllocated then
    CalculateScales;
end;

procedure TdxCustomGaugeControl.CalculateScales;
var
  I: Integer;
begin
  FLayerCount := 0;
  if CanCalculate(ClientBounds) then
    for I := 0 to FScales.Count - 1 do
    begin
      TdxGaugeCustomScaleAccess(FScales[I]).Calculate(GetScaleBounds(I), ClientBounds);
      FLayerCount := Max(FLayerCount, TdxGaugeCustomScaleAccess(FScales[I]).LayerCount);
    end;
end;

procedure TdxCustomGaugeControl.Changed;
begin
  if FLockCount = 0 then
  begin
    Calculate;
    Invalidate;
  end;
end;

end.

