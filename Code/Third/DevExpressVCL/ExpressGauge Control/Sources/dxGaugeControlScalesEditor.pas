{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressGaugeControl                                      }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSGAUGECONTROL AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGaugeControlScalesEditor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxComponentCollectionEditor, ImgList, cxGraphics, Menus,
  ActnList, ComCtrls, ToolWin, ExtCtrls,
  dxGaugeCustomScale;

type
  TfrmGaugeControlScalesEditor = class(TfrmComponentCollectionEditor)
    pmAddCircularScale: TMenuItem;
    pmAddDigitalScale: TMenuItem;
    N2: TMenuItem;
    acChangeStyle: TAction;
    miRestoreStyle: TMenuItem;
    acRestoreStyleParameters: TAction;
    procedure acRestoreStyleExecute(Sender: TObject);
  private
    function GetScales: TdxGaugeScaleCollection;
  protected
    procedure AddItem(AItemClassIndex: Integer); override;
  public
    property Scales: TdxGaugeScaleCollection read GetScales;
  end;

var
  frmGaugeControlScalesEditor: TfrmGaugeControlScalesEditor;

implementation

{$R *.dfm}

uses
  dxGaugeCircularScale, dxGaugeDigitalScale, DesignWindows;

const
  dxGaugeScaleCount = 2;
  dxGaugeScaleClasses: array [0..dxGaugeScaleCount - 1] of TdxGaugeCustomScaleClass = (TdxGaugeCircularScale,
    TdxGaugeDigitalScale);

{ TfrmGaugeControlScalesEditor }

procedure TfrmGaugeControlScalesEditor.AddItem(AItemClassIndex: Integer);
begin
  Scales.Add(dxGaugeScaleClasses[AItemClassIndex]);
end;

function TfrmGaugeControlScalesEditor.GetScales: TdxGaugeScaleCollection;
begin
  Result := Collection as TdxGaugeScaleCollection;
end;

procedure TfrmGaugeControlScalesEditor.acRestoreStyleExecute(Sender: TObject);
begin
  Scales[ListView1.Selected.Index].RestoreStyleParameters;
  Designer.Modified;
end;

end.
