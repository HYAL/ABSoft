{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressGaugeControl                                      }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSGAUGECONTROL AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGaugeUtils;

{$I cxVer.inc}

interface

uses
  Windows, Graphics, dxCoreGraphics, cxGeometry, dxGDIPlusAPI, dxGDIPlusClasses;

procedure dxGaugeDrawText(AGPGraphics: TdxGPGraphics; const AText: string; const ARect: TdxRectF; ARadius, AAngle: Single;
  AFont: TFont); overload;
procedure dxGaugeDrawText(AGPGraphics: TdxGPGraphics; const AText: string; const ATextRectSize: TdxSizeF;
  const ARect: TdxRectF; ARadius, AAngle: Single; AFont: TFont); overload;

procedure dxGaugeDrawBitmap(AGPGraphics: TdxGPGraphics; ABitmap: TBitmap; const ARect: TdxRectF);
procedure dxGaugeDrawImage(AGPGraphics: TdxGPGraphics; AImage: TGraphic; const ARect: TdxRectF);
procedure dxGaugeRotateAndDrawImage(AGPGraphics: TdxGPGraphics; AImage: TGraphic; const ADestImageSize: TdxSizeF;
  const ABounds: TdxRectF; ARadius, ARotateAngle: Single); overload;
procedure dxGaugeRotateAndDrawImage(AGPGraphics: TdxGPGraphics; AImage: TGraphic; const ADestImageSize: TdxSizeF;
  ARotatePoint: TdxPointF; ARadius, ARotateAngle: Single); overload;

implementation

uses
  Types, Classes, Math, dxCore, cxGraphics, dxCompositeShape;

type
  TdxCompositeShapeAccess = class(TdxCompositeShape);

procedure dxGaugeDrawText(AGPGraphics: TdxGPGraphics; const AText: string; const ARect: TdxRectF; ARadius, AAngle: Single;
  AFont: TFont);
var
  ATextRect: TdxRectF;
begin
  dxGPGetTextRect(AGPGraphics, AText, AFont, False, cxRectF(cxNullRect), ATextRect);
  dxGaugeDrawText(AGPGraphics, AText, dxSizeF(cxRectWidth(ATextRect), cxRectHeight(ATextRect)), ARect, ARadius, AAngle,
    AFont);
end;

procedure dxGaugeDrawText(AGPGraphics: TdxGPGraphics; const AText: string; const ATextRectSize: TdxSizeF;
  const ARect: TdxRectF; ARadius, AAngle: Single; AFont: TFont);
var
  P: TdxPointF;
  ASize: TdxSizeF;
  ATextRect: TdxRectF;  
begin
  ASize := dxSizeF(ATextRectSize.cx / 2, ATextRectSize.cy / 2); 
  P := dxRingPoint(cxRectCenter(ARect), ARadius, DegToRad(AAngle));
  ATextRect := cxRectF(P.X - ASize.cx, P.Y - ASize.cy, P.X + ASize.cx, P.Y + ASize.cy);
  dxGPDrawText(AGPGraphics, AText, ATextRect, AFont, dxColorToAlphaColor(AFont.Color), taCenter, taVerticalCenter, True,
    TextRenderingHintAntiAlias)
end;

procedure dxGaugeDrawBitmap(AGPGraphics: TdxGPGraphics; ABitmap: TBitmap; const ARect: TdxRectF);
var
  AImage: TdxGPImage;
begin
  AImage := TdxGPImage.CreateFromBitmap(ABitmap);
  AGPGraphics.Draw(AImage, cxRectOffsetF(ARect, dxPointF(-1, -1)));
  AImage.Free;
end;

procedure dxGaugeDrawImage(AGPGraphics: TdxGPGraphics; AImage: TGraphic; const ARect: TdxRectF);
var
  ATempBitmap: TcxBitmap32;
begin
  if AImage is TdxCompositeShape then
    TdxCompositeShapeAccess(AImage).DrawF(AGPGraphics, ARect)
  else
  begin
    ATempBitmap := TcxBitmap32.CreateSize(AImage.Width, AImage.Height, True);
    dxGaugeDrawBitmap(AGPGraphics, ATempBitmap, ARect);
    ATempBitmap.Free;
  end;
end;

procedure dxGaugeRotateAndDrawImage(AGPGraphics: TdxGPGraphics; AImage: TGraphic; const ADestImageSize: TdxSizeF;
  const ABounds: TdxRectF; ARadius, ARotateAngle: Single);
begin
  dxGaugeRotateAndDrawImage(AGPGraphics, AImage, ADestImageSize, cxRectCenter(ABounds), ARadius, ARotateAngle);
end;

procedure dxGaugeRotateAndDrawImage(AGPGraphics: TdxGPGraphics; AImage: TGraphic; const ADestImageSize: TdxSizeF;
  ARotatePoint: TdxPointF; ARadius, ARotateAngle: Single);
var
  P: TdxPointF;
  APreviousWorldTransform: TdxGPMatrix;
begin
  ARotateAngle := -ARotateAngle;
  P := dxRingPoint(ARotatePoint, ARadius, DegToRad(-ARotateAngle));
  APreviousWorldTransform := AGPGraphics.GetWorldTransform;
  AGPGraphics.RotateWorldTransform(ARotateAngle, P);
  dxGaugeDrawImage(AGPGraphics, AImage,
    cxRectF(P.X, P.Y - ADestImageSize.cy / 2, P.X + ADestImageSize.cx, P.Y + ADestImageSize.cy / 2));
  AGPGraphics.SetWorldTransform(APreviousWorldTransform);
end;

end.

