{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressGaugeControl                                      }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSGAUGECONTROL AND ALL           }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGaugeControlReg;

interface

{$I cxVer.inc}

uses
  Classes, DesignEditors, DesignIntf, 
  dxCoreReg, cxComponentCollectionEditor, dxGaugeControl;

const
  dxGaugeControlProductName  = 'ExpressGaugeControl';

type
  { TdxGaugeControlComponentEditor }

  TdxGaugeControlComponentEditor = class(TdxComponentEditor)
  private
    function GetGaugeControl: TdxCustomGaugeControl;
  protected
    function GetProductName: string; override;
    function InternalGetVerb(AIndex: Integer): string; override;
    function InternalGetVerbCount: Integer; override;
    procedure InternalExecuteVerb(AIndex: Integer); override;
  public
    property GaugeControl: TdxCustomGaugeControl read GetGaugeControl;
  end;

  { TdxGaugeControlScalesEditor }

  TdxGaugeControlScalesEditor = class(TcxComponentCollectionProperty)
  public
    function GetEditorClass: TcxComponentCollectionEditorClass; override;
  end;

  TdxGaugeStyleNamePropertyEditor = class(TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    function GetValue: string; override;
    procedure GetValues(AProc: TGetStrProc); override;
  end;

procedure Register;

implementation

uses
  cxDesignWindows, dxGaugeControlScalesEditor,
  dxGaugeCustomScale, dxGaugeCircularScale, dxGaugeDigitalScale;

type
  TdxGaugeCustomScaleAccess = class(TdxGaugeCustomScale);
  TdxGaugeScaleStyleFactoryAccess = class(TdxGaugeScaleStyleFactory);

{ TdxGaugeControlComponentEditor }

function TdxGaugeControlComponentEditor.GetProductName: string;
begin
  Result := dxGaugeControlProductName;
end;

procedure TdxGaugeControlComponentEditor.InternalExecuteVerb(AIndex: Integer);
begin
  ShowFormEditorClass(Designer, Component, GaugeControl.Scales, 'Scales', TfrmGaugeControlScalesEditor);
end;

function TdxGaugeControlComponentEditor.InternalGetVerb(AIndex: Integer): string;
begin
  Result := 'Scales Editor...';
end;

function TdxGaugeControlComponentEditor.InternalGetVerbCount: Integer;
begin
  Result := 1;
end;

function TdxGaugeControlComponentEditor.GetGaugeControl: TdxCustomGaugeControl;
begin
  Result := Component as TdxCustomGaugeControl;
end;

{ TdxGaugeControlScalesEditor }

function TdxGaugeControlScalesEditor.GetEditorClass: TcxComponentCollectionEditorClass;
begin
  Result := TfrmGaugeControlScalesEditor;
end;

{ TdxGaugeStyleNamePropertyEditor }

function TdxGaugeStyleNamePropertyEditor.GetAttributes: TPropertyAttributes;
begin
  Result := inherited GetAttributes;
  Result := Result - [paSubProperties, paReadOnly] +
    [paValueList, paSortList, paRevertable, paVolatileSubProperties];
end;

function TdxGaugeStyleNamePropertyEditor.GetValue: string;
begin
  Result := TdxGaugeCustomScaleAccess(GetComponent(0)).StyleName;
end;

procedure TdxGaugeStyleNamePropertyEditor.GetValues(AProc: TGetStrProc);

  function GetScaleType: TdxGaugeScaleType;
  begin
    Result := TdxGaugeCustomScaleAccess(GetComponent(0)).GetScaleType;
  end;

var
  I: Integer;
  AStyleName: string;
  AStyles: TStringList;
begin
  AStyles := TdxGaugeScaleStyleFactoryAccess(dxGaugeScaleStyleFactory).GetStyles(GetScaleType);
  for I := 0 to AStyles.Count - 1 do
  begin
    AStyleName := AStyles[I];
    AProc(AStyleName);
  end;
end;

procedure Register;
begin
{$IFDEF DELPHI9}
  ForceDemandLoadState(dlDisable);
{$ENDIF}
  RegisterComponents(dxCoreLibraryProductPage, [TdxGaugeControl]);
  RegisterNoIcon([TdxGaugeCustomScale, TdxGaugeCircularScale, TdxGaugeDigitalScale]);
  RegisterClasses([TdxCustomGaugeControl, TdxGaugeCustomScale, TdxGaugeCircularScale, TdxGaugeDigitalScale]);
  RegisterComponentEditor(TdxGaugeControl, TdxGaugeControlComponentEditor);
  RegisterPropertyEditor(TypeInfo(TdxGaugeScaleCollection), TdxCustomGaugeControl, 'Scales', TdxGaugeControlScalesEditor);
  RegisterPropertyEditor(TypeInfo(string), TdxGaugeCustomScale, 'StyleName', TdxGaugeStyleNamePropertyEditor);
end;

end.
