inherited frmGaugeControlScalesEditor: TfrmGaugeControlScalesEditor
  Left = 737
  Top = 217
  Width = 234
  Caption = 'frmGaugeControlScalesEditor'
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlItems: TPanel
    Width = 218
    inherited ToolBar1: TToolBar
      Width = 216
      inherited ToolButton1: TToolButton
        Style = tbsDropDown
      end
      inherited ToolButton2: TToolButton
        Left = 39
      end
      inherited ToolButton3: TToolButton
        Left = 63
      end
      inherited ToolButton4: TToolButton
        Left = 71
      end
      inherited ToolButton5: TToolButton
        Left = 95
      end
    end
    inherited Panel: TPanel
      Width = 216
      inherited ListView1: TListView
        Width = 216
      end
    end
  end
  inherited ActionList: TActionList
    object acChangeStyle: TAction
      Caption = 'acChangeStyle'
      OnUpdate = SelectionUpdate
    end
    object acRestoreStyleParameters: TAction
      Caption = 'acRestoreStyleParameters'
      OnExecute = acRestoreStyleExecute
      OnUpdate = SelectionUpdate
    end
  end
  inherited PopupMenu1: TPopupMenu
    object N2: TMenuItem [5]
      Caption = '-'
    end
    object miRestoreStyle: TMenuItem [6]
      Action = acRestoreStyleParameters
      Caption = 'Restore Style Parameters'
    end
  end
  inherited ilActions: TcxImageList
    FormatVersion = 1
  end
  inherited ilToolBar: TcxImageList
    FormatVersion = 1
  end
  inherited ilToolBarDisabled: TcxImageList
    FormatVersion = 1
  end
  inherited pmItemClasses: TPopupMenu
    object pmAddCircularScale: TMenuItem
      Caption = 'Circular'
      Hint = 'Add Circular Scale'
      ImageIndex = 0
      OnClick = acAddExecute
    end
    object pmAddDigitalScale: TMenuItem
      Tag = 1
      Caption = 'Digital'
      Hint = 'Add Digital Scale'
      ImageIndex = 0
      OnClick = acAddExecute
    end
  end
end
