inherited frmScreenTipRepositoryEditor: TfrmScreenTipRepositoryEditor
  Width = 600
  Height = 421
  BorderStyle = bsSizeable
  Caption = 'frmScreenTipRepositoryEditor'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 161
    Top = 0
    Height = 345
  end
  inherited pnlItems: TPanel
    Width = 161
    Height = 345
    Align = alLeft
    inherited ToolBar1: TToolBar
      Width = 159
    end
    inherited Panel: TPanel
      Width = 159
      Height = 315
      inherited ListView1: TListView
        Width = 159
        Height = 315
      end
    end
  end
  object pnlButtons: TPanel [2]
    Left = 0
    Top = 345
    Width = 584
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object btnClose: TButton
      Left = 497
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Cancel = True
      Caption = 'Close'
      Default = True
      TabOrder = 0
      OnClick = btnCloseClick
    end
    object btnShowOptions: TButton
      Left = 412
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akRight, akBottom]
      Caption = 'Hide options'
      TabOrder = 1
      OnClick = btnShowOptionsClick
    end
  end
  object pnlPreviewAndOptions: TPanel [3]
    Left = 164
    Top = 0
    Width = 420
    Height = 345
    ParentShowHint = False
    ShowHint = False
    TabOrder = 2
    object pnlPreview: TPanel
      Left = 1
      Top = 1
      Width = 418
      Height = 90
      Align = alClient
      BevelOuter = bvNone
      Constraints.MinHeight = 50
      TabOrder = 0
      OnResize = pnlPreviewResize
      object gbPreview: TGroupBox
        Left = 5
        Top = 0
        Width = 408
        Height = 86
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = 'Preview'
        TabOrder = 0
        object PaintBox1: TPaintBox
          Left = 163
          Top = 13
          Width = 74
          Height = 62
          OnPaint = PaintBox1Paint
        end
      end
    end
    object pnlOptions: TPanel
      Left = 1
      Top = 91
      Width = 418
      Height = 253
      Align = alBottom
      BevelOuter = bvNone
      Constraints.MinHeight = 205
      Constraints.MinWidth = 414
      TabOrder = 1
      object pcOptions: TPageControl
        Left = 0
        Top = 0
        Width = 418
        Height = 253
        ActivePage = tsScreenTipOptions
        Align = alClient
        TabOrder = 0
        object tsScreenTipOptions: TTabSheet
          Caption = 'ScreenTip'
          object gbScreenTipInfo: TGroupBox
            Left = 4
            Top = 3
            Width = 400
            Height = 215
            Anchors = [akLeft, akTop, akRight, akBottom]
            Ctl3D = True
            ParentCtl3D = False
            TabOrder = 0
            object Label1: TLabel
              Left = 279
              Top = 190
              Width = 32
              Height = 13
              Anchors = [akLeft]
              Caption = 'Width:'
            end
            object btnHeaderRichTextEdit: TSpeedButton
              Left = 370
              Top = 50
              Width = 22
              Height = 22
              Anchors = [akRight, akBottom]
              Caption = '...'
              OnClick = btnDescriptionRichTextEditClick
            end
            object btnDescriptionRichTextEdit: TSpeedButton
              Tag = 1
              Left = 370
              Top = 98
              Width = 22
              Height = 22
              Anchors = [akRight, akBottom]
              Caption = '...'
              OnClick = btnDescriptionRichTextEditClick
            end
            object btnFooterRichTextEdit: TSpeedButton
              Tag = 2
              Left = 370
              Top = 148
              Width = 22
              Height = 22
              Anchors = [akRight, akBottom]
              Caption = '...'
              OnClick = btnDescriptionRichTextEditClick
            end
            object edtHeader: TEdit
              Left = 30
              Top = 51
              Width = 251
              Height = 21
              Anchors = [akLeft, akRight, akBottom]
              TabOrder = 0
              OnChange = edtHeaderChange
            end
            object edtDescription: TMemo
              Tag = 1
              Left = 48
              Top = 86
              Width = 233
              Height = 49
              Anchors = [akLeft, akRight, akBottom]
              TabOrder = 1
              OnChange = edtHeaderChange
            end
            object edtFooter: TEdit
              Tag = 2
              Left = 30
              Top = 149
              Width = 251
              Height = 21
              Anchors = [akLeft, akRight, akBottom]
              TabOrder = 2
              OnChange = edtHeaderChange
            end
            object chbUseHintAsHeader: TCheckBox
              Left = 8
              Top = 189
              Width = 114
              Height = 17
              Anchors = [akLeft]
              Caption = 'UseHintAsHeader'
              TabOrder = 3
              OnClick = chbUseHintAsHeaderClick
            end
            object chbUseStandardFooter: TCheckBox
              Left = 133
              Top = 189
              Width = 114
              Height = 17
              Anchors = [akLeft]
              Caption = 'UseStandardFooter'
              TabOrder = 4
              OnClick = chbUseStandardFooterClick
            end
            object UpDown1: TUpDown
              Left = 376
              Top = 187
              Width = 16
              Height = 21
              Anchors = [akLeft]
              Associate = edtWidth
              Max = 30000
              TabOrder = 5
            end
            object edtWidth: TEdit
              Left = 319
              Top = 187
              Width = 57
              Height = 21
              Anchors = [akLeft]
              TabOrder = 6
              Text = '0'
              OnChange = edtWidthChange
              OnExit = edtWidthExit
            end
            object Panel1: TPanel
              Left = 8
              Top = 94
              Width = 34
              Height = 34
              Anchors = [akLeft, akBottom]
              BevelOuter = bvLowered
              TabOrder = 7
              object pbDescription: TPaintBox
                Tag = 1
                Left = 1
                Top = 1
                Width = 32
                Height = 32
                Align = alClient
                PopupMenu = PopupMenu3
                OnContextPopup = pbHeaderContextPopup
                OnDblClick = pbHeaderClick
                OnPaint = pbHeaderPaint
              end
            end
            object Panel2: TPanel
              Left = 7
              Top = 151
              Width = 17
              Height = 17
              Anchors = [akLeft, akBottom]
              BevelOuter = bvLowered
              TabOrder = 8
              object pbFooter: TPaintBox
                Tag = 2
                Left = 1
                Top = 1
                Width = 15
                Height = 15
                Align = alClient
                PopupMenu = PopupMenu3
                OnContextPopup = pbHeaderContextPopup
                OnDblClick = pbHeaderClick
                OnPaint = pbHeaderPaint
              end
            end
            object Panel3: TPanel
              Left = 7
              Top = 53
              Width = 17
              Height = 17
              Anchors = [akLeft, akBottom]
              BevelOuter = bvLowered
              TabOrder = 9
              object pbHeader: TPaintBox
                Left = 1
                Top = 1
                Width = 15
                Height = 15
                Align = alClient
                PopupMenu = PopupMenu3
                OnContextPopup = pbHeaderContextPopup
                OnDblClick = pbHeaderClick
                OnPaint = pbHeaderPaint
              end
            end
            object chbDescriptionPlain: TCheckBox
              Tag = 1
              Left = 293
              Top = 101
              Width = 67
              Height = 17
              Anchors = [akRight, akBottom]
              Caption = 'Plain text'
              TabOrder = 10
              OnClick = chbPlainTextClick
            end
            object chbFooterPlain: TCheckBox
              Tag = 2
              Left = 293
              Top = 152
              Width = 67
              Height = 17
              Anchors = [akRight, akBottom]
              Caption = 'Plain text'
              TabOrder = 11
              OnClick = chbPlainTextClick
            end
            object chbHeaderPlain: TCheckBox
              Left = 293
              Top = 53
              Width = 67
              Height = 17
              Anchors = [akRight, akBottom]
              Caption = 'Plain text'
              TabOrder = 12
              OnClick = chbPlainTextClick
            end
          end
        end
        object tsRepositoryOptions: TTabSheet
          Caption = 'Repository'
          ImageIndex = 1
          object gbScreenTipsInfo: TGroupBox
            Left = 4
            Top = 3
            Width = 400
            Height = 215
            Anchors = [akLeft, akTop, akRight, akBottom]
            TabOrder = 0
            object Label2: TLabel
              Left = 8
              Top = 16
              Width = 81
              Height = 13
              Caption = 'Standard footer:'
            end
            object btnHeaderFont: TSpeedButton
              Left = 370
              Top = 134
              Width = 22
              Height = 22
              Anchors = [akRight, akBottom]
              Caption = '...'
              OnClick = btnHeaderFontClick
            end
            object btnDescriptionFont: TSpeedButton
              Tag = 1
              Left = 370
              Top = 160
              Width = 22
              Height = 22
              Anchors = [akRight, akBottom]
              Caption = '...'
              OnClick = btnDescriptionFontClick
            end
            object btnFooterFont: TSpeedButton
              Tag = 2
              Left = 370
              Top = 186
              Width = 22
              Height = 22
              Anchors = [akRight, akBottom]
              Caption = '...'
              OnClick = btnFooterFontClick
            end
            object lblHeaderFontInfo: TLabel
              Left = 72
              Top = 142
              Width = 39
              Height = 13
              Anchors = [akLeft, akBottom]
              Caption = 'Header:'
            end
            object lblDescriptionFontInfo: TLabel
              Tag = 1
              Left = 89
              Top = 168
              Width = 57
              Height = 13
              Anchors = [akLeft, akBottom]
              Caption = 'Description:'
            end
            object lblFooterFontInfo: TLabel
              Tag = 2
              Left = 67
              Top = 194
              Width = 36
              Height = 13
              Anchors = [akLeft, akBottom]
              Caption = 'Footer:'
            end
            object btnStdFooterRichTextEdit: TSpeedButton
              Tag = 3
              Left = 370
              Top = 42
              Width = 22
              Height = 22
              Anchors = [akRight, akBottom]
              Caption = '...'
              OnClick = btnDescriptionRichTextEditClick
            end
            object chbShowDescription: TCheckBox
              Left = 8
              Top = 114
              Width = 114
              Height = 17
              Anchors = [akLeft, akBottom]
              Caption = 'Show description'
              TabOrder = 0
              OnClick = chbShowDescriptionClick
            end
            object edtStandardFooter: TEdit
              Tag = 3
              Left = 29
              Top = 43
              Width = 251
              Height = 21
              Anchors = [akLeft, akRight, akBottom]
              TabOrder = 1
              OnChange = edtStandardFooterChange
            end
            object Panel4: TPanel
              Left = 7
              Top = 45
              Width = 17
              Height = 17
              Anchors = [akLeft, akBottom]
              BevelOuter = bvLowered
              TabOrder = 2
              object pbStandardFooter: TPaintBox
                Tag = 3
                Left = 1
                Top = 1
                Width = 15
                Height = 15
                Align = alClient
                PopupMenu = PopupMenu3
                OnContextPopup = pbHeaderContextPopup
                OnDblClick = pbStandardFooterClick
                OnPaint = pbStandardFooterPaint
              end
            end
            object chbHeaderFont: TCheckBox
              Left = 8
              Top = 142
              Width = 58
              Height = 13
              Anchors = [akLeft, akBottom]
              Caption = 'Header:'
              TabOrder = 3
              OnClick = chbHeaderFontClick
            end
            object chbDescriptionFont: TCheckBox
              Tag = 1
              Left = 8
              Top = 168
              Width = 81
              Height = 13
              Anchors = [akLeft, akBottom]
              Caption = 'Description:'
              TabOrder = 4
              OnClick = chbHeaderFontClick
            end
            object chbFooterFont: TCheckBox
              Tag = 2
              Left = 8
              Top = 194
              Width = 58
              Height = 13
              Anchors = [akLeft, akBottom]
              Caption = 'Footer:'
              TabOrder = 5
              OnClick = chbHeaderFontClick
            end
            object chbPlainText: TCheckBox
              Tag = 3
              Left = 293
              Top = 45
              Width = 67
              Height = 17
              Anchors = [akRight, akBottom]
              Caption = 'Plain text'
              TabOrder = 6
              OnClick = chbPlainTextClick
            end
          end
        end
      end
    end
  end
  inherited ActionList: TActionList
    object actLeftDescriptionAlign: TAction
      AutoCheck = True
      Caption = 'actLeftDescriptionAlign'
      Checked = True
    end
  end
  inherited ilActions: TcxImageList
    FormatVersion = 1
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Filter = 'Bitmaps (*.bmp)|*.bmp'
    Left = 72
    Top = 72
  end
  object PopupMenu3: TPopupMenu
    OnPopup = PopupMenu3Popup
    Left = 40
    Top = 72
    object Open1: TMenuItem
      Caption = 'Open...'
      OnClick = Open1Click
    end
    object Clear1: TMenuItem
      Caption = 'Clear'
      OnClick = Clear1Click
    end
    object FixedWidth1: TMenuItem
      Caption = 'Fixed Width'
      OnClick = FixedWidth1Click
    end
    object Align1: TMenuItem
      Caption = 'Align'
      object Left1: TMenuItem
        Tag = 1
        Caption = 'Left'
        GroupIndex = 1
        RadioItem = True
        OnClick = Right1Click
      end
      object Right1: TMenuItem
        Caption = 'Right'
        GroupIndex = 1
        RadioItem = True
        OnClick = Right1Click
      end
    end
  end
  object FontDialog1: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Left = 8
    Top = 72
  end
end
