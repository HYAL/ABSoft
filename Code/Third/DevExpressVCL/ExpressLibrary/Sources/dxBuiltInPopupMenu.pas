{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           Express Cross Platform Library classes                   }
{                                                                    }
{           Copyright (c) 2000-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCROSSPLATFORMLIBRARY AND ALL   }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxBuiltInPopupMenu;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  UITypes,
{$ENDIF}
  Types, Menus, Classes, cxControls, cxClasses, dxCore, ImgList, cxGraphics, cxLookAndFeels;

type

  { TdxCustomBuiltInPopupMenuAdapter }

  TdxCustomBuiltInPopupMenuAdapterClass = class of TdxCustomBuiltInPopupMenuAdapter;
  TdxCustomBuiltInPopupMenuAdapter = class(TComponent)
  protected
    function GetPopupMenu: TComponent; virtual; abstract;
  public
    function Add(const ACaption: string; AClickEvent: TNotifyEvent; ATag: TdxNativeInt = 0;
      AImageIndex: TcxImageIndex = -1; AEnabled: Boolean = True): TComponent; virtual; abstract;
    procedure AddSeparator; virtual; abstract;
    procedure Clear; virtual; abstract;
    procedure SetChecked(AItem: TComponent; AValue: Boolean); virtual; abstract;
    procedure SetDefault(AItem: TComponent; AValue: Boolean); virtual; abstract;
    procedure SetImages(AImages: TCustomImageList); virtual; abstract;
    procedure SetLookAndFeel(AValue: TcxLookAndFeel); virtual;
    function Popup(const P: TPoint): Boolean; virtual;
    //
    property PopupMenu: TComponent read GetPopupMenu;
  end;

  { TdxStandardBuiltInPopupMenuAdapter }

  TdxStandardBuiltInPopupMenuAdapter = class(TdxCustomBuiltInPopupMenuAdapter)
  private
    FPopupMenu: TComponent;
  protected
    function GetPopupMenu: TComponent; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function Add(const ACaption: string; AClickEvent: TNotifyEvent; ATag: TdxNativeInt = 0;
      AImageIndex: TcxImageIndex = -1; AEnabled: Boolean = True): TComponent; override;
    procedure AddSeparator; override;
    procedure Clear; override;
    procedure SetChecked(AItem: TComponent; AValue: Boolean); override;
    procedure SetDefault(AItem: TComponent; AValue: Boolean); override;
    procedure SetImages(AImages: TCustomImageList); override;
  end;

  { TdxBuiltInPopupMenuAdapterManager }

  TdxBuiltInPopupMenuAdapterManager = class
  public
    class function GetActualAdapterClass: TdxCustomBuiltInPopupMenuAdapterClass;
    class function IsActualAdapterStandard: Boolean;
    class procedure Register(AClass: TdxCustomBuiltInPopupMenuAdapterClass);
    class procedure Unregister(AClass: TdxCustomBuiltInPopupMenuAdapterClass);
  end;

implementation

uses
  SysUtils;

var
  FRegisteredClasses: TList;

{ TdxCustomBuiltInPopupMenuAdapter }

procedure TdxCustomBuiltInPopupMenuAdapter.SetLookAndFeel(AValue: TcxLookAndFeel);
begin
  // do nothing
end;

function TdxCustomBuiltInPopupMenuAdapter.Popup(const P: TPoint): Boolean;
begin
  Result := ShowPopupMenu(nil, PopupMenu, P);
end;

{ TdxStandardBuiltInPopupMenuAdapter }

constructor TdxStandardBuiltInPopupMenuAdapter.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FPopupMenu := TPopupMenu.Create(Self);
end;

destructor TdxStandardBuiltInPopupMenuAdapter.Destroy;
begin
  FreeAndNil(FPopupMenu);
  inherited Destroy;
end;

function TdxStandardBuiltInPopupMenuAdapter.Add(const ACaption: string; AClickEvent: TNotifyEvent;
  ATag: TdxNativeInt = 0; AImageIndex: TcxImageIndex = -1; AEnabled: Boolean = True): TComponent;
var
  AMenuItem: TMenuItem;
begin
  AMenuItem := TMenuItem.Create(PopupMenu);
  AMenuItem.Caption := ACaption;
  AMenuItem.Tag := ATag;
  AMenuItem.Enabled := AEnabled;
  AMenuItem.ImageIndex := AImageIndex;
  AMenuItem.OnClick := AClickEvent;
  TPopupMenu(PopupMenu).Items.Add(AMenuItem);
  Result := AMenuItem;
end;

procedure TdxStandardBuiltInPopupMenuAdapter.AddSeparator;
begin
  Add('-', nil);
end;

procedure TdxStandardBuiltInPopupMenuAdapter.Clear;
begin
  TPopupMenu(PopupMenu).Items.Clear
end;

procedure TdxStandardBuiltInPopupMenuAdapter.SetChecked(AItem: TComponent; AValue: Boolean);
begin
  (AItem as TMenuItem).Checked := AValue;
end;

procedure TdxStandardBuiltInPopupMenuAdapter.SetDefault(AItem: TComponent; AValue: Boolean);
begin
  (AItem as TMenuItem).Default := AValue;
end;

procedure TdxStandardBuiltInPopupMenuAdapter.SetImages(AImages: TCustomImageList);
begin
  TPopupMenu(PopupMenu).Images := AImages;
end;

function TdxStandardBuiltInPopupMenuAdapter.GetPopupMenu: TComponent;
begin
  Result := FPopupMenu;
end;

{ TdxBuiltInPopupMenuAdapterManager }

class function TdxBuiltInPopupMenuAdapterManager.GetActualAdapterClass: TdxCustomBuiltInPopupMenuAdapterClass;
begin
  Result := TdxCustomBuiltInPopupMenuAdapterClass(FRegisteredClasses.Last);
end;

class function TdxBuiltInPopupMenuAdapterManager.IsActualAdapterStandard: Boolean;
begin
  Result := GetActualAdapterClass = TdxStandardBuiltInPopupMenuAdapter;
end;

class procedure TdxBuiltInPopupMenuAdapterManager.Register(AClass: TdxCustomBuiltInPopupMenuAdapterClass);
begin
  if FRegisteredClasses = nil then
    FRegisteredClasses := TList.Create;
  FRegisteredClasses.Add(AClass);
end;

class procedure TdxBuiltInPopupMenuAdapterManager.Unregister(AClass: TdxCustomBuiltInPopupMenuAdapterClass);
begin
  FRegisteredClasses.Remove(AClass);
  if FRegisteredClasses.Count = 0 then
    FreeAndNil(FRegisteredClasses);
end;

initialization
  TdxBuiltInPopupMenuAdapterManager.Register(TdxStandardBuiltInPopupMenuAdapter);

finalization
  TdxBuiltInPopupMenuAdapterManager.Unregister(TdxStandardBuiltInPopupMenuAdapter);
end.
