{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressCoreLibrary                                       }
{                                                                    }
{           Copyright (c) 1998-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSCORELIBRARY AND ALL            }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxHashUtils;

{$I cxVer.inc}

interface

uses
  Windows, SysUtils, Classes, Math, dxCore;

const
  dxHashTableSize = 16843;

type
  TdxMD5Byte16 = array[0..15] of Byte;
  TdxMD5Byte64 = array[0..63] of Byte;
  PdxMD5Byte64 = ^TdxMD5Byte64;

  TdxMD5Int2 = array[0..1] of Cardinal;
  TdxMD5Int4 = array[0..3] of Cardinal;
  TdxMD5Int16 = array[0..15] of Cardinal;
  PdxMD5Int16 = ^TdxMD5Int16;

  TdxMD5Context = record
    State: TdxMD5Int4;
    Count: TdxMD5Int2;
    Buffer: TdxMD5Byte64;
    BufferLen: Cardinal;
  end;

{$IFDEF DELPHI14}
  TdxDynamicItemList = class;
  TdxHashTableItem = class;

  { TdxDynamicListItem }

  TdxDynamicListItemClass = class of TdxDynamicListItem;
  TdxDynamicListItem = class
  protected
    FIndex: Integer;
    FNext: TdxDynamicListItem;
    FOwner: TdxDynamicItemList;
    FPrev: TdxDynamicListItem;
    procedure ShiftIndex(ADelta: Integer); virtual;
  public
    constructor Create(AOwner: TdxDynamicItemList; AIndex: Integer); virtual;
    destructor Destroy; override;
    procedure Assign(ASource: TdxDynamicListItem); virtual;

    property Index: Integer read FIndex;
  end;

  { TdxDynamicItemList }

  TdxDynamicItemListForEachProcRef = reference to procedure (AItem: TdxDynamicListItem; AData: Pointer);

  TdxDynamicItemList = class
  private
    function GetCount: Integer;
    function GetFirstIndex: Integer;
    function GetItem(AIndex: Integer): TdxDynamicListItem; inline;
    function GetLastIndex: Integer;
    procedure SetItem(AIndex: Integer; const AValue: TdxDynamicListItem); inline;
  protected
    FCurrentItem: TdxDynamicListItem;
    FFirst: TdxDynamicListItem;
    FIsDeletion: Boolean;
    FLast: TdxDynamicListItem;
    function CreateItem(const AIndex: Integer): TdxDynamicListItem; overload; inline;
    function CreateItem(const AIndex: Integer; out AIsNewlyCreated: Boolean): TdxDynamicListItem; overload;
    procedure DeleteItem(const AItem: TdxDynamicListItem);
    function FindItem(const AIndex: Integer): TdxDynamicListItem;
    procedure ForEach(AProc: TdxDynamicItemListForEachProcRef; AGoForward: Boolean = True; AData: Pointer = nil); overload;
    procedure ForEach(AProc: TdxDynamicItemListForEachProcRef; AStartIndex: Integer;
      AFinishIndex: Integer; AGoForward: Boolean = True; AData: Pointer = nil); overload;
    function GetItemClass: TdxDynamicListItemClass; virtual;
    procedure InsertItem(const ANewItem, ANeighborItem: TdxDynamicListItem);
    function ItemNeeded(const AIndex: Integer): TdxDynamicListItem;
    procedure ShiftIndexex(AStartFromIndex, ADelta: Integer);
  public
    destructor Destroy; override;
    procedure Clear;

    property Count: Integer read GetCount;
    property FirstIndex: Integer read GetFirstIndex;
    property Items[Index: Integer]: TdxDynamicListItem read GetItem write SetItem; default;
    property LastIndex: Integer read GetLastIndex;
  end;

  { TdxHashTable }

  TdxHashTable = class
  protected
    FCount: Integer;
    FTable: array of TdxDynamicItemList;
    FUniqueCount: Integer;
    procedure CheckAndAddItem(var AItem);
    procedure DeleteItem(AItem: TdxHashTableItem); virtual;
    procedure ForEach(AProc: TdxDynamicItemListForEachProcRef; AData: Pointer = nil);
  public
    constructor Create; virtual;
    destructor Destroy; override;
    procedure Clear; virtual;

    property Count: Integer read FCount;
    property UniqueCount: Integer read FUniqueCount;
  end;

  { TdxHashTableItem }

  TdxHashTableItem = class(TdxDynamicListItem)
  private
    FRefCount: Integer;
    function GetKey: Cardinal; inline;
    procedure SetKey(const AValue: Cardinal); inline;
  protected
    FHashTable: TdxHashTable;

    procedure AddValueToString(var AStringValue: AnsiString; const AValue; AValueSize: Integer);
    procedure CalculateHash; virtual;
    function CompareItem(const AItem: TdxHashTableItem): Integer; virtual;

    property HashTable: TdxHashTable read FHashTable;
  public
    destructor Destroy; override;
    procedure AddRef;
    function IsEqual(const AItem: TdxHashTableItem): Boolean;
    procedure Release;

    property Key: Cardinal read GetKey write SetKey;
    property RefCount: Integer read FRefCount;
  end;
{$ENDIF}

function dxCRC32(AData: PByte; ACount: Integer): Cardinal;

function dxElfHash(const S: AnsiString; ALangID: Cardinal = CP_ACP): Integer; overload;
function dxElfHash(const S: WideString; ALangID: Cardinal = CP_ACP): Integer; overload;
function dxElfHash(P: PWideChar; ALength: Integer; ALangID: Cardinal = CP_ACP): Integer; overload;
function dxElfHash(P: PWideChar; ALength: Integer; AUpperCaseBuffer: PWideChar;
  AUpperCaseBufferLength: Integer; ALangID: Cardinal = CP_ACP): Integer; overload;

function dxMD5CalcStr(const S: string): string;
function dxMD5Compare(const ADigits1, ADigits2: TdxMD5Byte16): Boolean;
function dxMD5DigestToString(const ADigits: TdxMD5Byte16): string;
procedure dxMD5Calc(AInput: PByteArray; AInputLength: Integer; var ADigits: TdxMD5Byte16);
procedure dxMD5Final(var AContext: TdxMD5Context; var ADigits: TdxMD5Byte16);
procedure dxMD5Init(var AContext: TdxMD5Context);
procedure dxMD5Update(var AContext: TdxMD5Context; AInput: PByteArray; AInputLength: Integer); overload;
procedure dxMD5Update(var AContext: TdxMD5Context; const S: AnsiString); overload;

implementation

const
  CRCTable: array[0..255] of Cardinal =
   ($00000000, $77073096, $EE0E612C, $990951BA, $076DC419, $706AF48F, $E963A535, $9E6495A3,
    $0EDB8832, $79DCB8A4, $E0D5E91E, $97D2D988, $09B64C2B, $7EB17CBD, $E7B82D07, $90BF1D91,
    $1DB71064, $6AB020F2, $F3B97148, $84BE41DE, $1ADAD47D, $6DDDE4EB, $F4D4B551, $83D385C7,
    $136C9856, $646BA8C0, $FD62F97A, $8A65C9EC, $14015C4F, $63066CD9, $FA0F3D63, $8D080DF5,
    $3B6E20C8, $4C69105E, $D56041E4, $A2677172, $3C03E4D1, $4B04D447, $D20D85FD, $A50AB56B,
    $35B5A8FA, $42B2986C, $DBBBC9D6, $ACBCF940, $32D86CE3, $45DF5C75, $DCD60DCF, $ABD13D59,
    $26D930AC, $51DE003A, $C8D75180, $BFD06116, $21B4F4B5, $56B3C423, $CFBA9599, $B8BDA50F,
    $2802B89E, $5F058808, $C60CD9B2, $B10BE924, $2F6F7C87, $58684C11, $C1611DAB, $B6662D3D,

    $76DC4190, $01DB7106, $98D220BC, $EFD5102A, $71B18589, $06B6B51F, $9FBFE4A5, $E8B8D433,
    $7807C9A2, $0F00F934, $9609A88E, $E10E9818, $7F6A0DBB, $086D3D2D, $91646C97, $E6635C01,
    $6B6B51F4, $1C6C6162, $856530D8, $F262004E, $6C0695ED, $1B01A57B, $8208F4C1, $F50FC457,
    $65B0D9C6, $12B7E950, $8BBEB8EA, $FCB9887C, $62DD1DDF, $15DA2D49, $8CD37CF3, $FBD44C65,
    $4DB26158, $3AB551CE, $A3BC0074, $D4BB30E2, $4ADFA541, $3DD895D7, $A4D1C46D, $D3D6F4FB,
    $4369E96A, $346ED9FC, $AD678846, $DA60B8D0, $44042D73, $33031DE5, $AA0A4C5F, $DD0D7CC9,
    $5005713C, $270241AA, $BE0B1010, $C90C2086, $5768B525, $206F85B3, $B966D409, $CE61E49F,
    $5EDEF90E, $29D9C998, $B0D09822, $C7D7A8B4, $59B33D17, $2EB40D81, $B7BD5C3B, $C0BA6CAD,

    $EDB88320, $9ABFB3B6, $03B6E20C, $74B1D29A, $EAD54739, $9DD277AF, $04DB2615, $73DC1683,
    $E3630B12, $94643B84, $0D6D6A3E, $7A6A5AA8, $E40ECF0B, $9309FF9D, $0A00AE27, $7D079EB1,
    $F00F9344, $8708A3D2, $1E01F268, $6906C2FE, $F762575D, $806567CB, $196C3671, $6E6B06E7,
    $FED41B76, $89D32BE0, $10DA7A5A, $67DD4ACC, $F9B9DF6F, $8EBEEFF9, $17B7BE43, $60B08ED5,
    $D6D6A3E8, $A1D1937E, $38D8C2C4, $4FDFF252, $D1BB67F1, $A6BC5767, $3FB506DD, $48B2364B,
    $D80D2BDA, $AF0A1B4C, $36034AF6, $41047A60, $DF60EFC3, $A867DF55, $316E8EEF, $4669BE79,
    $CB61B38C, $BC66831A, $256FD2A0, $5268E236, $CC0C7795, $BB0B4703, $220216B9, $5505262F,
    $C5BA3BBE, $B2BD0B28, $2BB45A92, $5CB36A04, $C2D7FFA7, $B5D0CF31, $2CD99E8B, $5BDEAE1D,

    $9B64C2B0, $EC63F226, $756AA39C, $026D930A, $9C0906A9, $EB0E363F, $72076785, $05005713,
    $95BF4A82, $E2B87A14, $7BB12BAE, $0CB61B38, $92D28E9B, $E5D5BE0D, $7CDCEFB7, $0BDBDF21,
    $86D3D2D4, $F1D4E242, $68DDB3F8, $1FDA836E, $81BE16CD, $F6B9265B, $6FB077E1, $18B74777,
    $88085AE6, $FF0F6A70, $66063BCA, $11010B5C, $8F659EFF, $F862AE69, $616BFFD3, $166CCF45,
    $A00AE278, $D70DD2EE, $4E048354, $3903B3C2, $A7672661, $D06016F7, $4969474D, $3E6E77DB,
    $AED16A4A, $D9D65ADC, $40DF0B66, $37D83BF0, $A9BCAE53, $DEBB9EC5, $47B2CF7F, $30B5FFE9,
    $BDBDF21C, $CABAC28A, $53B39330, $24B4A3A6, $BAD03605, $CDD70693, $54DE5729, $23D967BF,
    $B3667A2E, $C4614AB8, $5D681B02, $2A6F2B94, $B40BBE37, $C30C8EA1, $5A05DF1B, $2D02EF8D);

function dxCRC32(AData: PByte; ACount: Integer): Cardinal;
var
  I: Integer;
begin
  Result := $FFFFFFFF;
  for I := 0 to ACount - 1 do
  begin
    Result := (Result shr 8) xor CRCTable[Byte(Result) xor AData^];
    Inc(AData);
  end;
  Result := Result xor $FFFFFFFF;
end;

function dxElfHash(const S: AnsiString; ALangID: Cardinal = CP_ACP): Integer;
begin
  Result := dxElfHash(dxAnsiStringToWideString(S, ALangID), ALangID);
end;

function dxElfHash(const S: WideString; ALangID: Cardinal = CP_ACP): Integer;
begin
  Result := dxElfHash(PWideChar(S), Length(S), ALangID);
end;

function dxElfHash(P: PWideChar; ALength: Integer; ALangID: Cardinal = CP_ACP): Integer;
var
  ATempBuffer: PWideChar;
begin
  ATempBuffer := AllocMem(ALength * SizeOf(WideChar));
  try
    Result := dxElfHash(P, ALength, ATempBuffer, ALength, ALangID);
  finally
    FreeMem(ATempBuffer);
  end;
end;

function dxElfHash(P: PWideChar; ALength: Integer; AUpperCaseBuffer: PWideChar;
  AUpperCaseBufferLength: Integer; ALangID: Cardinal = CP_ACP): Integer;

{$IFNDEF DELPHI12}
  function StrLen(P: PWideChar): Cardinal;
  begin
    Result := 0;
    while P^ <> #0 do
    begin
      Inc(Result);
      Inc(P);
    end;
  end;
{$ENDIF}

var
  AIndex, I: Integer;
begin
  Result := 0;
  if P <> nil then
  begin
    if ALength <= 0 then
      ALength := StrLen(P);

    if AUpperCaseBuffer <> nil then
    begin
      ALength := LCMapStringW(ALangID, LCMAP_UPPERCASE, P, ALength, AUpperCaseBuffer, AUpperCaseBufferLength);
      P := AUpperCaseBuffer;
    end;

    for I := 0 to ALength - 1 do
    begin
      Result := (Result shl 4) + Ord(P^);
      AIndex := Result and $F0000000;
      if AIndex <> 0 then
        Result := Result xor (AIndex shr 24);
      Result := Result and (not AIndex);
      Inc(P);
    end;
  end;
end;

const
  FFTable: array[0..15] of LongWord = (
    $d76aa478, $e8c7b756, $242070db, $c1bdceee,
    $f57c0faf, $4787c62a, $a8304613, $fd469501,
    $698098d8, $8b44f7af, $ffff5bb1, $895cd7be,
    $6b901122, $fd987193, $a679438e, $49b40821
  );

  GGTable: array[0..15] of LongWord = (
    $f61e2562, $c040b340, $265e5a51, $e9b6c7aa,
    $d62f105d, $02441453, $d8a1e681, $e7d3fbc8,
    $21e1cde6, $c33707d6, $f4d50d87, $455a14ed,
    $a9e3e905, $fcefa3f8, $676f02d9, $8d2a4c8a
  );

  HHTable: array[0..15] of LongWord = (
    $fffa3942, $8771f681, $6d9d6122, $fde5380c,
    $a4beea44, $4bdecfa9, $f6bb4b60, $bebfbc70,
    $289b7ec6, $eaa127fa, $d4ef3085, $04881d05,
    $d9d4d039, $e6db99e5, $1fa27cf8, $c4ac5665
  );

  IITable: array[0..15] of LongWord = (
    $f4292244, $432aff97, $ab9423a7, $fc93a039,
    $655b59c3, $8f0ccc92, $ffeff47d, $85845dd1,
    $6fa87e4f, $fe2ce6e0, $a3014314, $4e0811a1,
    $f7537e82, $bd3af235, $2ad7d2bb, $eb86d391
  );

function ROL(X, N: LongWord): LongWord;
begin
  Result := (X shl N) or (X shr (32 - N));
end;

function FF(A, B, C, D, X, S, AC: LongWord): LongWord;
begin
  Result := ROL(A + X + AC + (B and C or not B and D), S) + B;
end;

function GG(A, B, C, D, X, S, AC: LongWord): LongWord;
begin
  Result := ROL(A + X + AC + (B and D or C and not D), S) + B;
end;

function HH(A, B, C, D, X, S, AC: LongWord): LongWord;
begin
  Result := ROL(A + X + AC + (B xor C xor D), S) + B;
end;

function II(A, B, C, D, X, S, AC: LongWord): LongWord;
begin
  Result := ROL(A + X + AC + (C xor (B or not D)), S) + B;
end;

procedure dxMD5Init(var AContext: TdxMD5Context);
begin
  ZeroMemory(@AContext, SizeOf(AContext));
  AContext.State[0] := $67452301;
  AContext.State[1] := $EFCDAB89;
  AContext.State[2] := $98BADCFE;
  AContext.State[3] := $10325476;
end;

procedure dxMD5Transform(var AAccumulator: TdxMD5Int4; const ABuf: PdxMD5Int16);

  function CalcPos(ARow, AOffset: Integer): Integer;
  begin
    Result := ARow * 4 + AOffset;
    if Result < 0 then
      Inc(Result, 16);
    if Result >= 16 then
      Dec(Result, 16);
  end;

var
  A, B, C, D: LongWord;
  I: Integer;
begin
  A := AAccumulator[0];
  B := AAccumulator[1];
  C := AAccumulator[2];
  D := AAccumulator[3];

  for I := 0 to 3 do
  begin
    A := FF(A, B, C, D, ABuf^[CalcPos(I, 0)],  7, FFTable[4 * I + 0]);
    D := FF(D, A, B, C, ABuf^[CalcPos(I, 1)], 12, FFTable[4 * I + 1]);
    C := FF(C, D, A, B, ABuf^[CalcPos(I, 2)], 17, FFTable[4 * I + 2]);
    B := FF(B, C, D, A, ABuf^[CalcPos(I, 3)], 22, FFTable[4 * I + 3]);
  end;

  for I := 0 to 3 do
  begin
    A := GG(A, B, C, D, ABuf^[CalcPos(I, 1)],   5, GGTable[4 * I + 0]);
    D := GG(D, A, B, C, ABuf^[CalcPos(I, 6)],   9, GGTable[4 * I + 1]);
    C := GG(C, D, A, B, ABuf^[CalcPos(I, 11)], 14, GGTable[4 * I + 2]);
    B := GG(B, C, D, A, ABuf^[CalcPos(I, 0)],  20, GGTable[4 * I + 3]);
  end;

  for I := 0 to 3 do
  begin
    A := HH(A, B, C, D, ABuf^[CalcPos(-I, 5)],   4, HHTable[4 * I + 0]);
    D := HH(D, A, B, C, ABuf^[CalcPos(-I, 8)],  11, HHTable[4 * I + 1]);
    C := HH(C, D, A, B, ABuf^[CalcPos(-I, 11)], 16, HHTable[4 * I + 2]);
    B := HH(B, C, D, A, ABuf^[CalcPos(-I, 14)], 23, HHTable[4 * I + 3]);
  end;

  for I := 0 to 3 do
  begin
    A := II(A, B, C, D, ABuf^[CalcPos(-I, 0)],   6, IITable[4 * I + 0]);
    D := II(D, A, B, C, ABuf^[CalcPos(-I, 7)],  10, IITable[4 * I + 1]);
    C := II(C, D, A, B, ABuf^[CalcPos(-I, 14)], 15, IITable[4 * I + 2]);
    B := II(B, C, D, A, ABuf^[CalcPos(-I, 5)],  21, IITable[4 * I + 3]);
  end;

  Inc(AAccumulator[0], A);
  Inc(AAccumulator[1], B);
  Inc(AAccumulator[2], C);
  Inc(AAccumulator[3], D);
end;

procedure dxMD5Update(var AContext: TdxMD5Context; const S: AnsiString);
begin
  dxMD5Update(AContext, @S[1], Length(S));
end;

procedure dxMD5Update(var AContext: TdxMD5Context; AInput: PByteArray; AInputLength: Integer);
const
  SplitPartSize = 64;
var
  AIndex: Integer;
  ALeft: Integer;
begin
  AIndex := 0;
  if Integer(AInputLength shl 3) < 0 then
    Inc(AContext.Count[1]);

  Inc(AContext.Count[0], AInputLength shl 3);
  Inc(AContext.Count[1], AInputLength shr 29);

  if AContext.BufferLen > 0 then
  begin
    ALeft := Min(AInputLength, SplitPartSize - AContext.BufferLen);
    Move(AInput^[AIndex], AContext.Buffer[AContext.BufferLen], ALeft);
    Inc(AContext.BufferLen, ALeft);
    Inc(AIndex, ALeft);
    if AContext.BufferLen < SplitPartSize then
      Exit;
    dxMD5Transform(AContext.State, @AContext.Buffer[0]);
    AContext.BufferLen := 0;
  end;

  while AIndex + SplitPartSize <= AInputLength do
  begin
    dxMD5Transform(AContext.State, @AInput^[AIndex]);
    Inc(AIndex, SplitPartSize);
  end;

  if AInputLength > AIndex then
  begin
    AContext.BufferLen := AInputLength - AIndex;
    Move(AInput^[AIndex], AContext.Buffer[0], AContext.BufferLen);
  end;
end;

procedure dxMD5Final(var AContext: TdxMD5Context; var ADigits: TdxMD5Byte16);
var
  AWorkBuf: TdxMD5Byte64;
  AWorkLen: Cardinal;
begin
  ADigits := TdxMD5Byte16(AContext.State);
  Move(AContext.Buffer, AWorkBuf, AContext.BufferLen);
  AWorkBuf[AContext.BufferLen] := $80;
  AWorkLen := AContext.BufferLen + 1;
  if AWorkLen > 56 then
  begin
    FillChar(AWorkBuf[AWorkLen], 64 - AWorkLen, 0);
    dxMD5Transform(TdxMD5Int4(ADigits), @AWorkBuf[0]);
    AWorkLen := 0
  end;
  FillChar(AWorkBuf[AWorkLen], 56 - AWorkLen, 0);
  TdxMD5Int16(AWorkBuf)[14] := AContext.Count[0];
  TdxMD5Int16(AWorkBuf)[15] := AContext.Count[1];
  dxMD5Transform(TdxMD5Int4(ADigits), @AWorkBuf[0]);
  FillChar(AContext, SizeOf(AContext), 0);
end;

procedure dxMD5Calc(AInput: PByteArray; AInputLength: Integer; var ADigits: TdxMD5Byte16);
var
  AContext: TdxMD5Context;
begin
  dxMD5Init(AContext);
  dxMD5Update(AContext, AInput, AInputLength);
  dxMD5Final(AContext, ADigits);
end;

function dxMD5CalcStr(const S: string): string;
var
  AAnsiString: AnsiString;
  ADigits: TdxMD5Byte16;
begin
  AAnsiString := dxStringToAnsiString(S);
  dxMD5Calc(@AAnsiString[1], Length(AAnsiString), ADigits);
  Result := dxMD5DigestToString(ADigits);
end;

function dxMD5Compare(const ADigits1, ADigits2: TdxMD5Byte16): Boolean;
begin
  Result := CompareMem(@ADigits1[0], @ADigits2[0], SizeOf(TdxMD5Byte16));
end;

function dxMD5DigestToString(const ADigits: TdxMD5Byte16): string;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to High(ADigits) do
    Result := Result + IntToHex(ADigits[I], 2);
end;

{$IFDEF DELPHI14}
{ TdxDynamicListItem }

constructor TdxDynamicListItem.Create(AOwner: TdxDynamicItemList; AIndex: Integer);
begin
  inherited Create;
  FOwner := AOwner;
  FIndex := AIndex;
end;

destructor TdxDynamicListItem.Destroy;
begin
  if FOwner <> nil then
    FOwner.DeleteItem(Self);
  inherited Destroy;
end;

procedure TdxDynamicListItem.Assign(ASource: TdxDynamicListItem);
begin
end;

procedure TdxDynamicListItem.ShiftIndex(ADelta: Integer);
begin
  Inc(FIndex, ADelta);
end;

{ TdxDynamicItemList }

destructor TdxDynamicItemList.Destroy;
begin
  Clear;
  inherited Destroy;
end;

procedure TdxDynamicItemList.Clear;
var
  AItem: TdxDynamicListItem;
begin
  AItem := FFirst;
  FFirst := nil;
  FLast := nil;
  if AItem = nil then Exit;
  FIsDeletion := True;
  try
    while AItem.FNext <> nil do
    begin
      AItem := AItem.FNext;
      AItem.FPrev.Free;
    end;
    AItem.Free;
  finally
    FIsDeletion := False;
  end;
end;

function TdxDynamicItemList.CreateItem(const AIndex: Integer): TdxDynamicListItem;
var
  AIsNewlyCreated: Boolean;
begin
  Result := CreateItem(AIndex, AIsNewlyCreated);
end;

function TdxDynamicItemList.CreateItem(const AIndex: Integer; out AIsNewlyCreated: Boolean): TdxDynamicListItem;
begin
  Result := FindItem(AIndex);
  AIsNewlyCreated := (Result = nil) or (Result.Index <> AIndex);
  if AIsNewlyCreated then
  begin
    FCurrentItem := GetItemClass.Create(Self, AIndex);
    InsertItem(FCurrentItem, Result);
    Result := FCurrentItem;
  end;
end;

procedure TdxDynamicItemList.DeleteItem(const AItem: TdxDynamicListItem);
begin
  if FCurrentItem = AItem then
    FCurrentItem := nil;
  if FIsDeletion then Exit;

  if AItem.FPrev <> nil then
    AItem.FPrev.FNext := AItem.FNext
  else
  begin
    FFirst := AItem.FNext;
    if FFirst <> nil then
      FFirst.FPrev := nil;
  end;

  if AItem.FNext <> nil then
    AItem.FNext.FPrev := AItem.FPrev
  else
  begin
    FLast := AItem.FPrev;
    if FLast <> nil then
      FLast.FNext := nil;
  end;

  AItem.FNext := nil;
  AItem.FPrev := nil;
end;

function TdxDynamicItemList.FindItem(const AIndex: Integer): TdxDynamicListItem;
var
  AFirst, ALast: TdxDynamicListItem;
begin
  Result := nil;
  AFirst := FFirst;
  ALast := FLast;
  if (AFirst = nil) or ((AFirst.Index > AIndex) or (ALast.Index < AIndex)) then
    Exit;

  if FCurrentItem <> nil then
  begin
    if FCurrentItem.Index > AIndex then
      ALast := FCurrentItem
    else
      AFirst := FCurrentItem;
  end;

  if AIndex > (AFirst.Index + ALast.Index) div 2 then
  begin
    Result := ALast;
    while (Result <> nil) and (Result.Index > AIndex) do
      Result := Result.FPrev;
  end
  else
    Result := AFirst;

  while (Result <> nil) and (Result.Index < AIndex) do
    Result := Result.FNext;
  while (Result <> nil) and (Result.FPrev <> nil) and (Result.FPrev.Index = AIndex) do
    Result := Result.FPrev;

  if Result <> nil then
    FCurrentItem := Result;
end;

procedure TdxDynamicItemList.ForEach(AProc: TdxDynamicItemListForEachProcRef; AGoForward: Boolean = True; AData: Pointer = nil);
begin
  ForEach(AProc, FirstIndex, LastIndex, AGoForward, AData);
end;

procedure TdxDynamicItemList.ForEach(AProc: TdxDynamicItemListForEachProcRef;
  AStartIndex, AFinishIndex: Integer; AGoForward: Boolean = True; AData: Pointer = nil);
var
  AItem: TdxDynamicListItem;
  AItemNext: TdxDynamicListItem;
begin
  if AGoForward then
  begin
    AItem := FFirst;
    while (AItem <> nil) and (AItem.Index < AStartIndex) do
      AItem := AItem.FNext;
    while (AItem <> nil) and (AItem.Index <= AFinishIndex) do
    begin
      AItemNext := AItem.FNext;
      AProc(AItem, AData);
      AItem := AItemNext;
    end;
  end
  else
  begin
    AItem := FLast;
    while (AItem <> nil) and (AItem.Index > AFinishIndex) do
      AItem := AItem.FPrev;
    while (AItem <> nil) and (AItem.Index >= AStartIndex) do
    begin
      AItemNext := AItem.FPrev;
      AProc(AItem, AData);
      AItem := AItemNext;
    end;
  end;
end;

function TdxDynamicItemList.GetItemClass: TdxDynamicListItemClass;
begin
  Result := TdxDynamicListItem;
end;

procedure TdxDynamicItemList.InsertItem(const ANewItem, ANeighborItem: TdxDynamicListItem);

  procedure DoInsertItem(const AFirst, ASecond: TdxDynamicListItem);
  begin
    if AFirst.FNext <> nil then
    begin
      AFirst.FNext.FPrev := ASecond;
      ASecond.FNext := AFirst.FNext;
    end;
    AFirst.FNext := ASecond;
    if ASecond.FPrev <> nil then
    begin
      AFirst.FPrev := ASecond.FPrev;
      ASecond.FPrev.FNext := AFirst;
    end;
    ASecond.FPrev := AFirst;
    if AFirst.FPrev = nil then
      FFirst := AFirst;
    if ASecond.FNext = nil then
      FLast := ASecond;
  end;

begin
  if FFirst = nil then
  begin
    FFirst := ANewItem;
    FLast := ANewItem;
  end
  else

  if FFirst.Index > ANewItem.Index then
    DoInsertItem(ANewItem, FFirst)
  else

  if FLast.Index < ANewItem.Index then
    DoInsertItem(FLast, ANewItem)
  else

  if ANeighborItem = nil then
    raise EdxException.Create('TdxDynamicItemList.InsertItem')
  else

  if ANeighborItem.Index > ANewItem.Index then
    DoInsertItem(ANewItem, ANeighborItem)
  else
    DoInsertItem(ANeighborItem, ANewItem);
end;

function TdxDynamicItemList.ItemNeeded(const AIndex: Integer): TdxDynamicListItem;
begin
  Result := Items[AIndex];
  if Result = nil then
    Result := CreateItem(AIndex);
end;

procedure TdxDynamicItemList.ShiftIndexex(AStartFromIndex, ADelta: Integer);
var
  AItem: TdxDynamicListItem;
begin
  if ADelta > 0 then
  begin
    AItem := FLast;
    while (AItem <> nil) and (AItem.Index >= AStartFromIndex) do
    begin
      AItem.ShiftIndex(ADelta);
      AItem := AItem.FPrev;
    end;
  end
  else
    if ADelta < 0 then
    begin
      AItem := FFirst;
      while (AItem <> nil) and (AItem.Index < AStartFromIndex) do
        AItem := AItem.FNext;
      while (AItem <> nil) and (AItem.Index >= AStartFromIndex) do
      begin
        AItem.ShiftIndex(ADelta);
        AItem := AItem.FNext;
      end;
    end;
end;

function TdxDynamicItemList.GetCount: Integer;
begin
  Result := LastIndex + 1;
end;

function TdxDynamicItemList.GetFirstIndex: Integer;
begin
  if FFirst <> nil then
    Result := FFirst.Index
  else
    Result := 0;
end;

function TdxDynamicItemList.GetItem(AIndex: Integer): TdxDynamicListItem;
begin
  Result := FindItem(AIndex);
  if (Result <> nil) and (Result.Index <> AIndex) then
    Result := nil;
end;

function TdxDynamicItemList.GetLastIndex: Integer;
begin
  if FLast <> nil then
    Result := FLast.Index
  else
    Result := -1;
end;

procedure TdxDynamicItemList.SetItem(AIndex: Integer; const AValue: TdxDynamicListItem);
begin
  ItemNeeded(AIndex).Assign(AValue);
end;

{ TdxHashTable }

constructor TdxHashTable.Create;
begin
  SetLength(FTable, dxHashTableSize);
  FillChar(FTable[0], dxHashTableSize * SizeOf(TdxDynamicItemList), 0);
end;

destructor TdxHashTable.Destroy;
begin
  Clear;
  inherited Destroy;
end;

procedure TdxHashTable.Clear;
var
  I: Integer;
begin
  try
    for I := 0 to dxHashTableSize - 1 do
      FTable[I].Free;
  finally
    FillChar(FTable[0], dxHashTableSize * SizeOf(TdxDynamicItemList), 0);
    FCount := 0;
    FUniqueCount := 0;
  end;
end;

procedure TdxHashTable.CheckAndAddItem(var AItem);
var
  ACandidate: TdxHashTableItem;
  AIndex: Integer;
  AItemObject: TdxHashTableItem;
  AItems: TdxDynamicItemList;
begin
  AItemObject := TdxHashTableItem(AItem);
  AIndex := AItemObject.Key mod dxHashTableSize;
  AItems := FTable[AIndex];
  Inc(FUniqueCount);
  if AItems = nil then
  begin
    AItems := TdxDynamicItemList.Create;
    FTable[AIndex] := AItems;
  end;
  AItemObject.FOwner := AItems;

  ACandidate := TdxHashTableItem(AItems.FindItem(AItemObject.Index));
  if ACandidate = nil then
    AItems.InsertItem(AItemObject, nil)
  else
  begin
    while ACandidate.Index = AItemObject.Index do
    begin
      if ACandidate.IsEqual(AItemObject) then
      begin
        TdxHashTableItem(AItem) := ACandidate;
        Dec(FUniqueCount);
        if ACandidate <> AItemObject then
        begin
          AItemObject.FOwner := nil;
          FreeAndNil(AItemObject);
        end;
        Break;
      end;
      if ACandidate.FNext <> nil then
        ACandidate := TdxHashTableItem(ACandidate.FNext)
      else
        Break;
    end;
    if TdxHashTableItem(AItem) <> ACandidate then
      AItems.InsertItem(AItemObject, ACandidate);
  end;
  TdxHashTableItem(AItem).FHashTable := Self;
end;

procedure TdxHashTable.DeleteItem(AItem: TdxHashTableItem);
begin
end;

procedure TdxHashTable.ForEach(AProc: TdxDynamicItemListForEachProcRef; AData: Pointer = nil);
var
  AList: TdxDynamicItemList;
  I: Integer;
begin
  for I := 0 to dxHashTableSize - 1 do
  begin
    AList := FTable[I];
    if AList <> nil then
      AList.ForEach(AProc, True, AData);
  end;
end;

{ TdxHashTableItem }

destructor TdxHashTableItem.Destroy;
begin
  if HashTable <> nil then
  begin
    Dec(HashTable.FUniqueCount);
    HashTable.DeleteItem(Self);
  end;
  inherited Destroy;
end;

procedure TdxHashTableItem.AddRef;
begin
  Inc(FRefCount);
  if (HashTable <> nil) then
    Inc(HashTable.FCount);
end;

procedure TdxHashTableItem.AddValueToString(var AStringValue: AnsiString; const AValue; AValueSize: Integer);
begin
  if AValueSize > 0 then
  begin
    SetLength(AStringValue, Length(AStringValue) + AValueSize);
    Move(AValue, AStringValue[Length(AStringValue) - AValueSize + 1], AValueSize);
  end;
end;

procedure TdxHashTableItem.CalculateHash;
begin
end;

function TdxHashTableItem.CompareItem(const AItem: TdxHashTableItem): Integer;
begin
  Result := Byte(Self) - Byte(AItem);
end;

function TdxHashTableItem.IsEqual(const AItem: TdxHashTableItem): Boolean;
begin
  Result := CompareItem(AItem) = 0;
end;

procedure TdxHashTableItem.Release;
begin
  if (FRefCount > 0) and (HashTable <> nil) then
    Dec(HashTable.FCount);
  Dec(FRefCount);
  if FRefCount <= 0 then
    Free;
end;

function TdxHashTableItem.GetKey: Cardinal;
begin
  Result := Cardinal(FIndex);
end;

procedure TdxHashTableItem.SetKey(const AValue: Cardinal);
begin
  FIndex := Integer(AValue);
end;

{$ENDIF}

end.
