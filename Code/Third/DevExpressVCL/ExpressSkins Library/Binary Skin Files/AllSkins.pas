unit AllSkins;

interface

uses
  Classes, dxCore, dxCoreGraphics, dxGDIPlusApi, cxLookAndFeelPainters, dxSkinsCore, dxSkinsLookAndFeelPainter;

type

  { TdxSkinBlackPainter }

  TdxSkinBlackPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinBluePainter }

  TdxSkinBluePainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinCaramelPainter }

  TdxSkinCaramelPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinCoffeePainter }

  TdxSkinCoffeePainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinDarkroomPainter }

  TdxSkinDarkroomPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinDarkSidePainter }

  TdxSkinDarkSidePainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinFoggyPainter }

  TdxSkinFoggyPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinGlassOceansPainter }

  TdxSkinGlassOceansPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkiniMaginaryPainter }

  TdxSkiniMaginaryPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinLilianPainter }

  TdxSkinLilianPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinLiquidSkyPainter }

  TdxSkinLiquidSkyPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinLondonLiquidSkyPainter }

  TdxSkinLondonLiquidSkyPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinMcSkinPainter }

  TdxSkinMcSkinPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinMoneyTwinsPainter }

  TdxSkinMoneyTwinsPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinOffice2007BlackPainter }

  TdxSkinOffice2007BlackPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinOffice2007BluePainter }

  TdxSkinOffice2007BluePainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinOffice2007GreenPainter }

  TdxSkinOffice2007GreenPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinOffice2007PinkPainter }

  TdxSkinOffice2007PinkPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinOffice2007SilverPainter }

  TdxSkinOffice2007SilverPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinPumpkinPainter }

  TdxSkinPumpkinPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinSevenPainter }

  TdxSkinSevenPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinSharpPainter }

  TdxSkinSharpPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinSilverPainter }

  TdxSkinSilverPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinSpringtimePainter }

  TdxSkinSpringtimePainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinStardustPainter }

  TdxSkinStardustPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinSummer2008Painter }

  TdxSkinSummer2008Painter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinValentinePainter }

  TdxSkinValentinePainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinXmas2008BluePainter }

  TdxSkinXmas2008BluePainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinOffice2010BlackPainter }

  TdxSkinOffice2010BlackPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinOffice2010BluePainter }

  TdxSkinOffice2010BluePainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinOffice2010SilverPainter }

  TdxSkinOffice2010SilverPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinBlueprintPainter }

  TdxSkinBlueprintPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinDevExpressDarkStylePainter }

  TdxSkinDevExpressDarkStylePainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinDevExpressStylePainter }

  TdxSkinDevExpressStylePainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinHighContrastPainter }

  TdxSkinHighContrastPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinSevenClassicPainter }

  TdxSkinSevenClassicPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinSharpPlusPainter }

  TdxSkinSharpPlusPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinTheAsphaltWorldPainter }

  TdxSkinTheAsphaltWorldPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinVS2010Painter }

  TdxSkinVS2010Painter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinWhiteprintPainter }

  TdxSkinWhiteprintPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinOffice2013WhitePainter }

  TdxSkinOffice2013WhitePainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinOffice2013LightGrayPainter }

  TdxSkinOffice2013LightGrayPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinOffice2013DarkGrayPainter }

  TdxSkinOffice2013DarkGrayPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinMetropolisDarkPainter }

  TdxSkinMetropolisDarkPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

  { TdxSkinMetropolisPainter }

  TdxSkinMetropolisPainter = class(TdxSkinLookAndFeelPainter)
  public
    function LookAndFeelName: string; override;
  end;

implementation

{$R AllSkins.res}

const
  SkinsCount = 45;
  SkinNames: array[0..SkinsCount - 1] of string = (
    'Black',
    'Blue',
    'Caramel',
    'Coffee',
    'Darkroom',
    'DarkSide',
    'Foggy',
    'GlassOceans',
    'iMaginary',
    'Lilian',
    'LiquidSky',
    'LondonLiquidSky',
    'McSkin',
    'MoneyTwins',
    'Office2007Black',
    'Office2007Blue',
    'Office2007Green',
    'Office2007Pink',
    'Office2007Silver',
    'Pumpkin',
    'Seven',
    'Sharp',
    'Silver',
    'Springtime',
    'Stardust',
    'Summer2008',
    'Valentine',
    'Xmas2008Blue',
    'Office2010Black',
    'Office2010Blue',
    'Office2010Silver',
    'Blueprint',
    'DevExpressDarkStyle',
    'DevExpressStyle',
    'HighContrast',
    'SevenClassic',
    'SharpPlus',
    'TheAsphaltWorld',
    'VS2010',
    'Whiteprint',
    'Office2013White',
    'Office2013LightGray',
    'Office2013DarkGray',
    'MetropolisDark',
    'Metropolis'
  );
  SkinPainters: array[0..SkinsCount - 1] of TdxSkinLookAndFeelPainterClass = (
    TdxSkinBlackPainter,
    TdxSkinBluePainter,
    TdxSkinCaramelPainter,
    TdxSkinCoffeePainter,
    TdxSkinDarkroomPainter,
    TdxSkinDarkSidePainter,
    TdxSkinFoggyPainter,
    TdxSkinGlassOceansPainter,
    TdxSkiniMaginaryPainter,
    TdxSkinLilianPainter,
    TdxSkinLiquidSkyPainter,
    TdxSkinLondonLiquidSkyPainter,
    TdxSkinMcSkinPainter,
    TdxSkinMoneyTwinsPainter,
    TdxSkinOffice2007BlackPainter,
    TdxSkinOffice2007BluePainter,
    TdxSkinOffice2007GreenPainter,
    TdxSkinOffice2007PinkPainter,
    TdxSkinOffice2007SilverPainter,
    TdxSkinPumpkinPainter,
    TdxSkinSevenPainter,
    TdxSkinSharpPainter,
    TdxSkinSilverPainter,
    TdxSkinSpringtimePainter,
    TdxSkinStardustPainter,
    TdxSkinSummer2008Painter,
    TdxSkinValentinePainter,
    TdxSkinXmas2008BluePainter,
    TdxSkinOffice2010BlackPainter,
    TdxSkinOffice2010BluePainter,
    TdxSkinOffice2010SilverPainter,
    TdxSkinBlueprintPainter,
    TdxSkinDevExpressDarkStylePainter,
    TdxSkinDevExpressStylePainter,
    TdxSkinHighContrastPainter,
    TdxSkinSevenClassicPainter,
    TdxSkinSharpPlusPainter,
    TdxSkinTheAsphaltWorldPainter,
    TdxSkinVS2010Painter,
    TdxSkinWhiteprintPainter,
    TdxSkinOffice2013WhitePainter,
    TdxSkinOffice2013LightGrayPainter,
    TdxSkinOffice2013DarkGrayPainter,
    TdxSkinMetropolisDarkPainter,
    TdxSkinMetropolisPainter
  );


{ TdxSkinBlackPainter }

function TdxSkinBlackPainter.LookAndFeelName: string;
begin
  Result := 'Black';
end;

{ TdxSkinBluePainter }

function TdxSkinBluePainter.LookAndFeelName: string;
begin
  Result := 'Blue';
end;

{ TdxSkinCaramelPainter }

function TdxSkinCaramelPainter.LookAndFeelName: string;
begin
  Result := 'Caramel';
end;

{ TdxSkinCoffeePainter }

function TdxSkinCoffeePainter.LookAndFeelName: string;
begin
  Result := 'Coffee';
end;

{ TdxSkinDarkroomPainter }

function TdxSkinDarkroomPainter.LookAndFeelName: string;
begin
  Result := 'Darkroom';
end;

{ TdxSkinDarkSidePainter }

function TdxSkinDarkSidePainter.LookAndFeelName: string;
begin
  Result := 'DarkSide';
end;

{ TdxSkinFoggyPainter }

function TdxSkinFoggyPainter.LookAndFeelName: string;
begin
  Result := 'Foggy';
end;

{ TdxSkinGlassOceansPainter }

function TdxSkinGlassOceansPainter.LookAndFeelName: string;
begin
  Result := 'GlassOceans';
end;

{ TdxSkiniMaginaryPainter }

function TdxSkiniMaginaryPainter.LookAndFeelName: string;
begin
  Result := 'iMaginary';
end;

{ TdxSkinLilianPainter }

function TdxSkinLilianPainter.LookAndFeelName: string;
begin
  Result := 'Lilian';
end;

{ TdxSkinLiquidSkyPainter }

function TdxSkinLiquidSkyPainter.LookAndFeelName: string;
begin
  Result := 'LiquidSky';
end;

{ TdxSkinLondonLiquidSkyPainter }

function TdxSkinLondonLiquidSkyPainter.LookAndFeelName: string;
begin
  Result := 'LondonLiquidSky';
end;

{ TdxSkinMcSkinPainter }

function TdxSkinMcSkinPainter.LookAndFeelName: string;
begin
  Result := 'McSkin';
end;

{ TdxSkinMoneyTwinsPainter }

function TdxSkinMoneyTwinsPainter.LookAndFeelName: string;
begin
  Result := 'MoneyTwins';
end;

{ TdxSkinOffice2007BlackPainter }

function TdxSkinOffice2007BlackPainter.LookAndFeelName: string;
begin
  Result := 'Office2007Black';
end;

{ TdxSkinOffice2007BluePainter }

function TdxSkinOffice2007BluePainter.LookAndFeelName: string;
begin
  Result := 'Office2007Blue';
end;

{ TdxSkinOffice2007GreenPainter }

function TdxSkinOffice2007GreenPainter.LookAndFeelName: string;
begin
  Result := 'Office2007Green';
end;

{ TdxSkinOffice2007PinkPainter }

function TdxSkinOffice2007PinkPainter.LookAndFeelName: string;
begin
  Result := 'Office2007Pink';
end;

{ TdxSkinOffice2007SilverPainter }

function TdxSkinOffice2007SilverPainter.LookAndFeelName: string;
begin
  Result := 'Office2007Silver';
end;

{ TdxSkinPumpkinPainter }

function TdxSkinPumpkinPainter.LookAndFeelName: string;
begin
  Result := 'Pumpkin';
end;

{ TdxSkinSevenPainter }

function TdxSkinSevenPainter.LookAndFeelName: string;
begin
  Result := 'Seven';
end;

{ TdxSkinSharpPainter }

function TdxSkinSharpPainter.LookAndFeelName: string;
begin
  Result := 'Sharp';
end;

{ TdxSkinSilverPainter }

function TdxSkinSilverPainter.LookAndFeelName: string;
begin
  Result := 'Silver';
end;

{ TdxSkinSpringtimePainter }

function TdxSkinSpringtimePainter.LookAndFeelName: string;
begin
  Result := 'Springtime';
end;

{ TdxSkinStardustPainter }

function TdxSkinStardustPainter.LookAndFeelName: string;
begin
  Result := 'Stardust';
end;

{ TdxSkinSummer2008Painter }

function TdxSkinSummer2008Painter.LookAndFeelName: string;
begin
  Result := 'Summer2008';
end;

{ TdxSkinValentinePainter }

function TdxSkinValentinePainter.LookAndFeelName: string;
begin
  Result := 'Valentine';
end;

{ TdxSkinXmas2008BluePainter }

function TdxSkinXmas2008BluePainter.LookAndFeelName: string;
begin
  Result := 'Xmas2008Blue';
end;

{ TdxSkinOffice2010BlackPainter }

function TdxSkinOffice2010BlackPainter.LookAndFeelName: string;
begin
  Result := 'Office2010Black';
end;

{ TdxSkinOffice2010BluePainter }

function TdxSkinOffice2010BluePainter.LookAndFeelName: string;
begin
  Result := 'Office2010Blue';
end;

{ TdxSkinOffice2010SilverPainter }

function TdxSkinOffice2010SilverPainter.LookAndFeelName: string;
begin
  Result := 'Office2010Silver';
end;

{ TdxSkinBlueprintPainter }

function TdxSkinBlueprintPainter.LookAndFeelName: string;
begin
  Result := 'Blueprint';
end;

{ TdxSkinDevExpressDarkStylePainter }

function TdxSkinDevExpressDarkStylePainter.LookAndFeelName: string;
begin
  Result := 'DevExpressDarkStyle';
end;

{ TdxSkinDevExpressStylePainter }

function TdxSkinDevExpressStylePainter.LookAndFeelName: string;
begin
  Result := 'DevExpressStyle';
end;

{ TdxSkinHighContrastPainter }

function TdxSkinHighContrastPainter.LookAndFeelName: string;
begin
  Result := 'HighContrast';
end;

{ TdxSkinSevenClassicPainter }

function TdxSkinSevenClassicPainter.LookAndFeelName: string;
begin
  Result := 'SevenClassic';
end;

{ TdxSkinSharpPlusPainter }

function TdxSkinSharpPlusPainter.LookAndFeelName: string;
begin
  Result := 'SharpPlus';
end;

{ TdxSkinTheAsphaltWorldPainter }

function TdxSkinTheAsphaltWorldPainter.LookAndFeelName: string;
begin
  Result := 'TheAsphaltWorld';
end;

{ TdxSkinVS2010Painter }

function TdxSkinVS2010Painter.LookAndFeelName: string;
begin
  Result := 'VS2010';
end;

{ TdxSkinWhiteprintPainter }

function TdxSkinWhiteprintPainter.LookAndFeelName: string;
begin
  Result := 'Whiteprint';
end;

{ TdxSkinOffice2013WhitePainter }

function TdxSkinOffice2013WhitePainter.LookAndFeelName: string;
begin
  Result := 'Office2013White';
end;

{ TdxSkinOffice2013LightGrayPainter }

function TdxSkinOffice2013LightGrayPainter.LookAndFeelName: string;
begin
  Result := 'Office2013LightGray';
end;

{ TdxSkinOffice2013DarkGrayPainter }

function TdxSkinOffice2013DarkGrayPainter.LookAndFeelName: string;
begin
  Result := 'Office2013DarkGray';
end;

{ TdxSkinMetropolisDarkPainter }

function TdxSkinMetropolisDarkPainter.LookAndFeelName: string;
begin
  Result := 'MetropolisDark';
end;

{ TdxSkinMetropolisPainter }

function TdxSkinMetropolisPainter.LookAndFeelName: string;
begin
  Result := 'Metropolis';
end;

//

procedure RegisterPainters;
var
  I: Integer;
begin
  if CheckGdiPlus then
  begin
    for I := 0 to SkinsCount - 1 do
      cxLookAndFeelPaintersManager.Register(SkinPainters[I].Create(SkinNames[I], HInstance));
  end;
end;

procedure UnregisterPainters;
var
  I: Integer;
begin
  if cxLookAndFeelPaintersManager <> nil then
  begin
    for I := 0 to SkinsCount - 1 do
      cxLookAndFeelPaintersManager.Unregister(SkinNames[I]);
  end;
end;

{$IFNDEF DXSKINDYNAMICLOADING}
initialization
  dxUnitsLoader.AddUnit(@RegisterPainters, @UnregisterPainters);
finalization
  dxUnitsLoader.RemoveUnit(@UnregisterPainters);
{$ENDIF}
end.
