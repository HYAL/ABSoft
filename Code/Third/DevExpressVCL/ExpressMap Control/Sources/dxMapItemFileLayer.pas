{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressMapControl                                        }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSMAPCONTROL AND ALL             }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxMapItemFileLayer;

interface

{$I cxVer.inc}

uses
  Types, Classes, Graphics, SysUtils,
  cxGraphics, cxGeometry, dxCore, dxCoreClasses, dxGDIPlusClasses, dxCoreGraphics,
  dxMapControlTypes, dxMapLayer, dxCustomMapItemLayer, dxMapItem;

type
  TdxMapItemFileLayer = class(TdxCustomMapItemLayer)
  private
    FActive: Boolean;
    FLoadedActive: Boolean;
    FFileName: TFileName;
    procedure SetActive(const Value: Boolean);
    procedure SetFileName(const Value: TFileName);
  protected
    procedure Loaded; override;

    procedure DoActivate; override;
    procedure DoAssign(Source: TPersistent); override;
    procedure DoDeactivate; override;
  public
    procedure LoadFromFile(const AFileName: string = '');
    procedure LoadFromStream(AFileStream: TStream);
  published
    property Active: Boolean read FActive write SetActive default False;
    property AllowHotTrack;
    property FileName: TFileName read FFileName write SetFileName;
    property InitialMapSize;
    property ItemStyle;
    property ItemStyleHot;
    property ItemStyleSelected;
  end;

implementation

uses
  dxMapControlKmlFileLoader;

{ TdxMapItemFileLayer }

procedure TdxMapItemFileLayer.LoadFromFile(const AFileName: string = '');
var
  AFileStream: TFileStream;
begin
  Active := False;
  if AFileName <> '' then
    FFileName := AFileName;
  AFileStream := TFileStream.Create(FFileName, fmOpenRead or fmShareDenyNone);
  try
    LoadFromStream(AFileStream);
  finally
    AFileStream.Free;
  end;
end;

procedure TdxMapItemFileLayer.LoadFromStream(AFileStream: TStream);
begin
  try
    with TdxMapControlKmlFileLoader.Create(Self) do
    try
      LoadFromStream(AFileStream);
    finally
      Free;
    end;
    FActive := True;
  except
    MapItems.Clear;
  end;
end;

procedure TdxMapItemFileLayer.Loaded;
begin
  inherited Loaded;
  Active := FLoadedActive;
end;

procedure TdxMapItemFileLayer.DoActivate;
begin
  LoadFromFile;
end;

procedure TdxMapItemFileLayer.DoAssign(Source: TPersistent);
begin
  inherited DoAssign(Source);
  if Source is TdxCustomMapItemLayer then
  begin
    FileName := TdxMapItemFileLayer(Source).FileName;
    Active := TdxMapItemFileLayer(Source).Active;
  end;
end;

procedure TdxMapItemFileLayer.DoDeactivate;
begin
  MapItems.Clear;
end;

procedure TdxMapItemFileLayer.SetActive(const Value: Boolean);
begin
  if csLoading in ComponentState then
    FLoadedActive := Value
  else
    if FActive <> Value then
    begin
      FActive := Value;
      if not (csDesigning in ComponentState) then
        if FActive then
          DoActivate
        else
          DoDeactivate;
    end;
end;

procedure TdxMapItemFileLayer.SetFileName(const Value: TFileName);
begin
  if FileName <> Value then
  begin
    FFileName := Value;
    if not (csLoading in ComponentState) then
       Active := False;
  end;
end;

end.
