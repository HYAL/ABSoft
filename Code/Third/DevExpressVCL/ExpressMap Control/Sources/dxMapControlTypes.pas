{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressMapControl                                        }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSMAPCONTROL AND ALL             }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxMapControlTypes;

interface

{$I cxVer.inc}

uses
  Math, Classes, Types,
  dxCoreClasses, dxCore;

type
  TdxMapControlTileIndex = record
    X: Integer;
    Y: Integer;
    Level: Integer;
  end;

  TdxMapControlTileRange = record
    Max: TdxMapControlTileIndex;
    Min: TdxMapControlTileIndex;
  end;

  TdxMapControlGeoPoint = record
    Latitude: Double;
    Longitude: Double;
  end;

  TdxMapControlGeoLocation = class(TcxOwnedPersistent)
  private
    FGeoPoint: TdxMapControlGeoPoint;
    FOnChanged: TNotifyEvent;
    function GetLatitude: Double;
    function GetLongitude: Double;
    procedure SetGeoPoint(const Value: TdxMapControlGeoPoint);
    procedure SetLatitude(const Value: Double);
    procedure SetLongitude(const Value: Double);
  protected
    procedure Changed;
    procedure DoAssign(Source: TPersistent); override;
  public
    property GeoPoint: TdxMapControlGeoPoint read FGeoPoint write SetGeoPoint;
    property OnChanged: TNotifyEvent read FOnChanged write FOnChanged;
  published
    property Longitude: Double read GetLongitude write SetLongitude;
    property Latitude: Double read GetLatitude write SetLatitude;
  end;

const
  dxMapControlInvalidTileIndex: TdxMapControlTileIndex = (X: -1; Y: -1; Level: -1);

function dxMapControlTileRangeIsEqual(const ARange1, ARange2: TdxMapControlTileRange): Boolean; inline;
function dxMapControlTileIndex(X, Y, ALevel: Integer): TdxMapControlTileIndex; inline;
function dxMapControlTileIndexIsEqual(const AIndex1, AIndex2: TdxMapControlTileIndex): Boolean; inline;
function dxMapControlTileRange(const AMin, AMax: TdxMapControlTileIndex): TdxMapControlTileRange; inline;
function dxMapControlGeoPoint(ALatitude, ALongitude: Double): TdxMapControlGeoPoint; inline;
function dxMapControlGeoPointIsEqual(const AGeoPoint1, AGeoPoint2: TdxMapControlGeoPoint): Boolean; inline;

implementation

function dxMapControlTileRangeIsEqual(const ARange1, ARange2: TdxMapControlTileRange): Boolean;
begin
  Result := dxMapControlTileIndexIsEqual(ARange1.Max, ARange2.Max) and
    dxMapControlTileIndexIsEqual(ARange1.Min, ARange2.Min);
end;

function dxMapControlTileIndex(X, Y, ALevel: Integer): TdxMapControlTileIndex;
begin
  Result.X := X;
  Result.Y := Y;
  Result.Level := ALevel;
end;

function dxMapControlTileIndexIsEqual(const AIndex1, AIndex2: TdxMapControlTileIndex): Boolean;
begin
  Result := (AIndex1.X = AIndex2.X) and
    (AIndex1.Y = AIndex2.Y) and (AIndex1.Level = AIndex2.Level);
end;

function dxMapControlTileRange(const AMin, AMax: TdxMapControlTileIndex): TdxMapControlTileRange;
begin
  Result.Max := AMax;
  Result.Min := AMin;
end;

function dxMapControlGeoPoint(ALatitude, ALongitude: Double): TdxMapControlGeoPoint;
begin
  Result.Latitude := ALatitude;
  Result.Longitude := ALongitude;
end;

function dxMapControlGeoPointIsEqual(const AGeoPoint1, AGeoPoint2: TdxMapControlGeoPoint): Boolean;
begin
  Result := (AGeoPoint1.Latitude = AGeoPoint2.Latitude) and
   (AGeoPoint1.Longitude = AGeoPoint2.Longitude);
end;

  { TdxMapControlGeoLocation }

procedure TdxMapControlGeoLocation.Changed;
begin
  dxCallNotify(FOnChanged, Self);
end;

procedure TdxMapControlGeoLocation.DoAssign(Source: TPersistent);
begin
  inherited;
  if Source is TdxMapControlGeoLocation then
    GeoPoint := TdxMapControlGeoLocation(Source).GeoPoint;
end;

function TdxMapControlGeoLocation.GetLatitude: Double;
begin
  Result := FGeoPoint.Latitude;
end;

function TdxMapControlGeoLocation.GetLongitude: Double;
begin
  Result := FGeoPoint.Longitude;
end;

procedure TdxMapControlGeoLocation.SetGeoPoint(
  const Value: TdxMapControlGeoPoint);
begin
  if not dxMapControlGeoPointIsEqual(FGeoPoint, Value) then
  begin
    FGeoPoint := Value;
    Changed;
  end;
end;

procedure TdxMapControlGeoLocation.SetLatitude(const Value: Double);
begin
  if FGeoPoint.Latitude <> Value then
  begin
    FGeoPoint.Latitude := Value;
    Changed;
  end;
end;

procedure TdxMapControlGeoLocation.SetLongitude(const Value: Double);
begin
  if FGeoPoint.Longitude <> Value then
  begin
    FGeoPoint.Longitude := Value;
    Changed;
  end;
end;

end.
