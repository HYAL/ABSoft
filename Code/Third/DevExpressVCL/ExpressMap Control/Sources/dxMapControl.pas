{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressMapControl                                        }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSMAPCONTROL AND ALL             }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxMapControl;

interface

{$I cxVer.inc}

uses
  SysUtils, Graphics, Classes, Types, RTLConsts, Forms, Math, Windows, Controls,
  cxClasses, cxControls, cxGeometry, dxAnimation, dxCoreClasses, cxLookAndFeels,
  cxGraphics, cxLookAndFeelPainters, dxGdiPlusClasses, dxTypeHelpers, dxScreenTip,
  dxMapControlTypes, dxMapControlViewInfo, dxMapLayer, dxMapImageTileLayer,
  dxCustomMapItemLayer, dxMapItemLayer, dxMapItemFileLayer, dxMapItem;

type
  TdxCustomMapControl = class;

  TdxMapItemSelectMode = (mismNone, mismSingle, mismMultiple, mismExtended);

  TdxMapControlNavigationPanel = class(TcxOwnedPersistent)
  private
    FHeight: Integer;
    FMapControl: TdxCustomMapControl;
    FShowCoordinates: Boolean;
    FShowKilometersScale: Boolean;
    FShowMilesScale: Boolean;
    FShowScrollButtons: Boolean;
    FShowZoomTrackBar: Boolean;
    FVisible: Boolean;
    procedure SetHeight(AValue: Integer);
    procedure SetShowCoordinates(AValue: Boolean);
    procedure SetShowKilometersScale(AValue: Boolean);
    procedure SetShowMilesScale(AValue: Boolean);
    procedure SetShowScrollButtons(AValue: Boolean);
    procedure SetShowZoomTrackBar(AValue: Boolean);
    procedure SetVisible(AValue: Boolean);
  protected
    procedure Changed;
    procedure DoAssign(Source: TPersistent); override;
  public
    constructor Create(AOwner: TPersistent); override;
  published
    property Height: Integer read FHeight write SetHeight default 80;
    property ShowCoordinates: Boolean read FShowCoordinates write SetShowCoordinates default True;
    property ShowKilometersScale: Boolean read FShowKilometersScale write SetShowKilometersScale default True;
    property ShowMilesScale: Boolean read FShowMilesScale write SetShowMilesScale default True;
    property ShowScrollButtons: Boolean read FShowScrollButtons write SetShowScrollButtons default True;
    property ShowZoomTrackBar: Boolean read FShowZoomTrackBar write SetShowZoomTrackBar default True;
    property Visible: Boolean read FVisible write SetVisible default True;
  end;

  TdxMapControlOptionsBehavior = class(TcxOwnedPersistent)
  private
    FAnimation: Boolean;
    FMapControl: TdxCustomMapControl;
    FMapItemSelectMode: TdxMapItemSelectMode;
    FScrolling: Boolean;
    FZooming: Boolean;
    procedure Changed;
    procedure SetAnimation(const Value: Boolean);
    procedure SetMapItemSelectMode(const Value: TdxMapItemSelectMode);
    procedure SetScrolling(const Value: Boolean);
    procedure SetZooming(const Value: Boolean);
  protected
    procedure DoAssign(Source: TPersistent); override;
  public
    constructor Create(AOwner: TPersistent); override;
  published
    property Animation: Boolean read FAnimation write SetAnimation default True;
    property MapItemSelectMode: TdxMapItemSelectMode read FMapItemSelectMode write SetMapItemSelectMode default mismExtended;
    property Scrolling: Boolean read FScrolling write SetScrolling default True;
    property Zooming: Boolean read FZooming write SetZooming default True;
  end;

  TdxMapControlAnimationController = class
  private
    FMapControl: TdxCustomMapControl;
    FZoomAnimation: TdxAnimationTransition;
    FZoomAnimationAnchorPoint: TdxPointDouble;
    FZoomAnimationStartLevel: Double;
    FZoomLevel: Double;
    FZoomPositionScale: Double;
    FNestedZoom: Boolean;
    FFinishZoomAnimation: Boolean;
    FNewLevel: Double;
    FAnchorPoint: TdxPointDouble;
    FWithAnimation: Boolean;
    procedure DoZoom(AZoomLevel: Double);
    procedure DoZoomAnimation(Sender: TdxAnimationTransition; var APosition: Integer;
      var AFinished: Boolean);
  public
    constructor Create(AOwner: TdxCustomMapControl); virtual;
    destructor Destroy; override;
    procedure StartZoomAnimation;
    procedure Zoom(ANewZoomValue: Double; const AAnchorPoint: TdxPointDouble;
      AWithAnimation: Boolean = True);

    property ZoomLevel: Double read FZoomLevel;
  end;

  TdxMapControlSelectionController = class
  private
    FMapControl: TdxCustomMapControl;
    FSelection: TdxFastObjectList;
    procedure AddSelection(AItem: TdxMapItem);
    function GetCount: Integer;
    function GetItems(Index: Integer): TdxMapItem;
  public
    constructor Create(AOwner: TdxCustomMapControl); virtual;
    destructor Destroy; override;
    procedure ClearSelection;
    procedure Deselect(AItem: TdxMapItem);
    procedure Select(AItem: TdxMapItem; AShift: TShiftState);

    property Count: Integer read GetCount;
    property Items[Index: Integer]: TdxMapItem read GetItems;
  end;

  TdxMapControlDragAndDropObject = class(TcxDragAndDropObject)
  protected
    function GetDragAndDropCursor(Accepted: Boolean): TCursor; override;
    function ProcessKeyDown(AKey: Word; AShiftState: TShiftState): Boolean; override;
  end;

  TdxMapControlSelectionChangingEvent = procedure(Sender: TObject; AItem: TdxMapItem; var AAllow: Boolean) of object;
  TdxMapControlZoomLevelChangingEvent = procedure(Sender: TObject; ANewZoomLevel: Double; var AAllow: Boolean) of object;

  TdxMapControlDragAction = (mcdaNone, mcdaScroll, mcdaSelect, mcdaZoom);

  TdxCustomMapControl = class(TcxControl, IdxScreenTipProvider)
  private
    FAnimationController: TdxMapControlAnimationController;
    FCenterPoint: TdxMapControlGeoLocation;
    FController: TdxMapControlController;
    FDragMode: TdxMapControlDragAction;
    FIsViewInfoDirty: Boolean;
    FIsViewPortValid: Boolean;
    FLayers: TdxMapLayers;
    FLoadedZoomLevel: Double;
    FLockCount: Integer;
    FMapItemLayerSelectionController: TdxMapControlSelectionController;
    FNavigationPanel: TdxMapControlNavigationPanel;
    FOptionsBehavior: TdxMapControlOptionsBehavior;
    FPainter: TdxMapControlPainter;
    FSelectedRegionBounds: TRect;
    FShift: TShiftState;
    FStartCenterPoint: TdxMapControlGeoPoint;
    FViewInfo: TdxMapControlViewInfo;
    FZoomLevel: Double;
    FOnSelectionChanged: TNotifyEvent;
    FOnSelectionChanging: TdxMapControlSelectionChangingEvent;
    FOnZoomLevelChanged: TNotifyEvent;
    FOnZoomLevelChanging: TdxMapControlZoomLevelChangingEvent;
    function AllowZoomLevelChanges(ALevel: Double): Boolean;
    procedure CenterPointChanged(ASender: TObject);
    procedure CheckZoomLevel(var ALevel: Double);
    procedure DoZoomLevelChanged;
    function GetActualCenterPoint: TdxMapControlGeoPoint;
    function GetActualZoomLevel: Double;
    function GetBaseLayer: TdxMapLayer;
    function GetHitTest: TdxMapControlHitTest;
    function GetSelectedMapItemCount: Integer;
    function GetSelectedMapItems(Index: Integer): TdxMapItem;
    function IsZoomLevelStored: Boolean;
    procedure SetCenterPoint(AValue: TdxMapControlGeoLocation);
    procedure SetLayers(const Value: TdxMapLayers);
    procedure SetNavigationPanel(AValue: TdxMapControlNavigationPanel);
    procedure SetOptionsBehavior(AValue: TdxMapControlOptionsBehavior);
    procedure SetZoomLevel(Value: Double);
    procedure UpdateSelectedRegion(const R: TRect);
  protected
    procedure BoundsChanged; override;
    function DoMouseWheel(Shift: TShiftState; WheelDelta: Integer;
      MousePos: TPoint): Boolean; override;
    procedure DoPaint; override;
    procedure DragAndDrop(const P: TPoint; var Accepted: Boolean); override;
    procedure EndDragAndDrop(Accepted: Boolean); override;
    function GetDragAndDropObjectClass: TcxDragAndDropObjectClass; override;
    procedure InitControl; override;
    function IsDoubleBufferedNeeded: Boolean; override;
    procedure Loaded; override;
    procedure LookAndFeelChanged(Sender: TcxLookAndFeel; AChangedValues: TcxLookAndFeelValues); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseLeave(AControl: TControl); override; // to do
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    function StartDragAndDrop(const P: TPoint): Boolean; override;

    // IdxScreenTipProvider
    function IdxScreenTipProvider.GetAction = GetMapAction;
    function GetMapAction: TBasicAction;
    function GetScreenTip: TdxScreenTip;
    function GetShortCut: string;

    function AllowSelectionChanging(AItem: TdxMapItem): Boolean;
    function CanAnimate: Boolean;
    function CanScroll: Boolean;
    function CanSelect: Boolean;
    function CanZoom: Boolean;
    procedure Changed;
    procedure CheckChanges;
    procedure CheckViewPort;
    procedure DoSelectionChanged;
    function GetKilometersScale(ACenterPoint: TdxMapControlGeoPoint; AScreenDistance: Double): Double;
    function GetMaxZoomLevel: Integer;
    procedure InvalidateViewPort;
    procedure LayersChanged(Sender: TObject; AItem: TcxComponentCollectionItem;
      AAction: TcxComponentCollectionNotification); virtual;
    function ScreenPointToGeoPoint(APoint: TdxPointDouble): TdxMapControlGeoPoint;
    procedure ScrollMap(X, Y: Integer);
    procedure SelectMapItemsInRegion(const ARect: TRect);
    procedure UpdateViewPort;
    procedure Zoom(AZoomLevel: Double; const AAnchorPoint: TdxPointDouble; AAnimated: Boolean); overload;
    procedure ZoomToRegion(const ARect: TRect);

    function CreateAnimationController: TdxMapControlAnimationController; virtual;
    function CreateController: TdxMapControlController; virtual;
    function CreateMapItemsLayerSelectionController: TdxMapControlSelectionController; virtual;
    function CreateNavigationPanelOptions: TdxMapControlNavigationPanel; virtual;
    function CreateOptionsBehavior: TdxMapControlOptionsBehavior; virtual;
    procedure CreateSubclasses;
    procedure DestroySubclasses;
    function CreatePainter: TdxMapControlPainter; virtual;
    function CreateViewInfo: TdxMapControlViewInfo; virtual;

    property Controller: TdxMapControlController read FController;
    property Painter: TdxMapControlPainter read FPainter;
    property ViewInfo: TdxMapControlViewInfo read FViewInfo;

    property SelectedRegionBounds: TRect read FSelectedRegionBounds;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;

    function AddImageTileLayer: TdxMapImageTileLayer;
    function AddItemFileLayer: TdxMapItemFileLayer;
    function AddItemLayer: TdxMapItemLayer;
    procedure BeginUpdate;
    procedure ClearSelection;
    procedure Deselect(AMapItem: TdxMapItem);
    procedure EndUpdate;
    function IsLocked: Boolean;
    procedure Select(AMapItem: TdxMapItem; AShiftState: TShiftState = [];
      ACheckSelectionMode: Boolean = True);
    procedure Zoom(AZoomLevel: Double; AAnimated: Boolean); overload;
    procedure ZoomIn;
    procedure ZoomOut;

    property ActualCenterPoint: TdxMapControlGeoPoint read GetActualCenterPoint;
    property ActualZoomLevel: Double read GetActualZoomLevel;
    property CenterPoint: TdxMapControlGeoLocation read FCenterPoint write SetCenterPoint;
    property HitTest: TdxMapControlHitTest read GetHitTest;
    property Layers: TdxMapLayers read FLayers write SetLayers;
    property NavigationPanel: TdxMapControlNavigationPanel read FNavigationPanel write SetNavigationPanel;
    property OptionsBehavior: TdxMapControlOptionsBehavior read FOptionsBehavior write SetOptionsBehavior;
    property SelectedMapItemCount: Integer read GetSelectedMapItemCount;
    property SelectedMapItems[Index: Integer]: TdxMapItem read GetSelectedMapItems;
    property ZoomLevel: Double read FZoomLevel write SetZoomLevel stored IsZoomLevelStored;
    property OnSelectionChanged: TNotifyEvent read FOnSelectionChanged write FOnSelectionChanged;
    property OnSelectionChanging: TdxMapControlSelectionChangingEvent read FOnSelectionChanging write FOnSelectionChanging;
    property OnZoomLevelChanged: TNotifyEvent read FOnZoomLevelChanged write FOnZoomLevelChanged;
    property OnZoomLevelChanging: TdxMapControlZoomLevelChangingEvent read FOnZoomLevelChanging write FOnZoomLevelChanging;
  end;

  TdxMapControl = class(TdxCustomMapControl)
  published
    property Align;
    property Anchors;
    property BorderStyle default cxcbsDefault;
    property CenterPoint;
    property Constraints;
    property Cursor;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Font;
    property HelpContext;
    property HelpKeyword;
    property HelpType;
    property Layers;
    property LookAndFeel;
    property NavigationPanel;
    property OptionsBehavior;
    property ParentColor;
    property ParentFont;
    property PopupMenu;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    property ZoomLevel;
    property OnCanResize;
    property OnClick;
    property OnConstrainedResize;
    property OnContextPopup;
    property OnDblClick;
    property OnDockDrop;
    property OnDockOver;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnGetSiteInfo;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnMouseWheel;
    property OnMouseWheelDown;
    property OnMouseWheelUp;
    property OnResize;
    property OnSelectionChanged;
    property OnSelectionChanging;
    property OnStartDock;
    property OnStartDrag;
    property OnUnDock;
    property OnZoomLevelChanged;
    property OnZoomLevelChanging;
  end;

implementation

const
  dxMapControlMaxZoomLevel = 20;
  dxMapControlZoomAnimationInterval = 500;
  dxMapControlZoomAnimationFrameCount = 100;

{ TdxMapControlNavigationPanelOptions }

constructor TdxMapControlNavigationPanel.Create(AOwner: TPersistent);
begin
  inherited Create(AOwner);
  FMapControl := AOwner as TdxCustomMapControl;
  FHeight := 80;
  FShowCoordinates := True;
  FShowKilometersScale := True;
  FShowMilesScale := True;
  FShowScrollButtons := True;
  FShowZoomTrackBar := True;
  FVisible := True;
end;

procedure TdxMapControlNavigationPanel.Changed;
begin
  FMapControl.Changed;
end;

procedure TdxMapControlNavigationPanel.DoAssign(Source: TPersistent);
var
  ANavigationPanelOptions: TdxMapControlNavigationPanel;
begin
  inherited;
  if Source is TdxMapControlNavigationPanel then
  begin
    ANavigationPanelOptions := TdxMapControlNavigationPanel(Source);
    FHeight := ANavigationPanelOptions.Height;
    FShowCoordinates := ANavigationPanelOptions.ShowCoordinates;
    FShowKilometersScale := ANavigationPanelOptions.ShowKilometersScale;
    FShowMilesScale := ANavigationPanelOptions.ShowMilesScale ;
    FShowScrollButtons := ANavigationPanelOptions.ShowScrollButtons;
    FShowZoomTrackBar := ANavigationPanelOptions.ShowZoomTrackBar;
    FVisible := ANavigationPanelOptions.Visible;
    Changed;
  end;
end;

procedure TdxMapControlNavigationPanel.SetHeight(AValue: Integer);
begin
  if FHeight <> AValue then
  begin
    FHeight := AValue;
    Changed;
  end;
end;

procedure TdxMapControlNavigationPanel.SetShowCoordinates(
  AValue: Boolean);
begin
  if FShowCoordinates <> AValue then
  begin
    FShowCoordinates := AValue;
    Changed;
  end;
end;

procedure TdxMapControlNavigationPanel.SetShowKilometersScale(
  AValue: Boolean);
begin
  if FShowKilometersScale <> AValue then
  begin
    FShowKilometersScale := AValue;
    Changed;
  end;
end;

procedure TdxMapControlNavigationPanel.SetShowMilesScale(
  AValue: Boolean);
begin
  if FShowMilesScale <> AValue then
  begin
    FShowMilesScale := AValue;
    Changed;
  end;
end;

procedure TdxMapControlNavigationPanel.SetShowScrollButtons(
  AValue: Boolean);
begin
  if FShowScrollButtons <> AValue then
  begin
    FShowScrollButtons := AValue;
    Changed;
  end;
end;

procedure TdxMapControlNavigationPanel.SetShowZoomTrackBar(
  AValue: Boolean);
begin
  if FShowZoomTrackBar <> AValue then
  begin
    FShowZoomTrackBar := AValue;
    Changed;
  end;
end;

procedure TdxMapControlNavigationPanel.SetVisible(AValue: Boolean);
begin
  if FVisible <> AValue then
  begin
    FVisible := AValue;
    Changed;
  end;
end;

{ TdxMapControlOptionsBehavior }

constructor TdxMapControlOptionsBehavior.Create(AOwner: TPersistent);
begin
  inherited Create(AOwner);
  FMapControl := AOwner as TdxCustomMapControl;
  FAnimation := True;
  FScrolling := True;
  FMapItemSelectMode := mismExtended;
  FZooming := True;
end;

procedure TdxMapControlOptionsBehavior.DoAssign(Source: TPersistent);
begin
  inherited DoAssign(Source);
  if Source is TdxMapControlOptionsBehavior then
  begin
    FAnimation := TdxMapControlOptionsBehavior(Source).Animation;
    FMapItemSelectMode := TdxMapControlOptionsBehavior(Source).MapItemSelectMode;
    FScrolling := TdxMapControlOptionsBehavior(Source).Scrolling;
    FZooming := TdxMapControlOptionsBehavior(Source).Zooming;
    Changed;
  end;
end;

procedure TdxMapControlOptionsBehavior.Changed;
begin
  FMapControl.Changed;
end;

procedure TdxMapControlOptionsBehavior.SetAnimation(const Value: Boolean);
begin
  FAnimation := Value;
end;

procedure TdxMapControlOptionsBehavior.SetMapItemSelectMode(
  const Value: TdxMapItemSelectMode);
begin
  if Value <> FMapItemSelectMode then
  begin
    FMapItemSelectMode := Value;
    FMapControl.ClearSelection;
  end;
end;

procedure TdxMapControlOptionsBehavior.SetScrolling(const Value: Boolean);
begin
  if FScrolling <> Value then
  begin
    FScrolling := Value;
    Changed;
  end;
end;

procedure TdxMapControlOptionsBehavior.SetZooming(const Value: Boolean);
begin
  if FZooming <> Value then
  begin
    FZooming := Value;
    Changed;
  end;
end;

{ TdxMapControlAnimationController }

constructor TdxMapControlAnimationController.Create(
  AOwner: TdxCustomMapControl);
begin
  inherited Create;
  FMapControl := AOwner;
  FZoomLevel := 1;
end;

destructor TdxMapControlAnimationController.Destroy;
begin
  FreeAndNil(FZoomAnimation);
  inherited;
end;

procedure TdxMapControlAnimationController.StartZoomAnimation;
begin
  FZoomAnimationAnchorPoint := FAnchorPoint;
  FZoomAnimationStartLevel := FZoomLevel;

  FZoomPositionScale := (FNewLevel - FZoomLevel) / dxMapControlZoomAnimationFrameCount;

  FZoomAnimation := TdxAnimationTransition.Create(dxMapControlZoomAnimationInterval,
    ateAccelerateDecelerate, dxMapControlZoomAnimationFrameCount);
  try
    FZoomAnimation.FreeOnTerminate := False;
    FZoomAnimation.OnAnimate := DoZoomAnimation;
    FZoomAnimation.ImmediateAnimation;
  finally
    FreeAndNil(FZoomAnimation);
  end;
  if FNestedZoom then
  begin
    FNestedZoom := False;
    if FWithAnimation then
      StartZoomAnimation
    else
      DoZoom(FNewLevel);
  end;
end;

procedure TdxMapControlAnimationController.Zoom(ANewZoomValue: Double;
  const AAnchorPoint: TdxPointDouble; AWithAnimation: Boolean = True);
begin
  FNewLevel := ANewZoomValue;
  FAnchorPoint := AAnchorPoint;
  FWithAnimation := AWithAnimation;
  if FZoomAnimation <> nil then
  begin
    FFinishZoomAnimation := True;
    FNestedZoom := True;
    FZoomAnimation.OnAnimate := nil;
    Exit;
  end;

  if FWithAnimation then
    StartZoomAnimation
  else
    DoZoom(FNewLevel);
end;

procedure TdxMapControlAnimationController.DoZoom(AZoomLevel: Double);

  procedure UpdateZoomLevel;
  begin
    FZoomLevel := AZoomLevel;
    FMapControl.InvalidateViewPort;
  end;

var
  ANewCenterPoint: TdxMapControlGeoPoint;
begin
  if FZoomLevel <> AZoomLevel then
  begin
    FMapControl.BeginUpdate;
    try
      if FMapControl.GetBaseLayer <> nil then
        ANewCenterPoint := FMapControl.GetBaseLayer.MoveAndZoom(FMapControl.CenterPoint.GeoPoint,
          FAnchorPoint, FZoomLevel, AZoomLevel)
      else
        ANewCenterPoint := FMapControl.CenterPoint.GeoPoint;
      UpdateZoomLevel;
      FMapControl.CenterPoint.GeoPoint := ANewCenterPoint;
      FMapControl.CheckViewPort;
    finally
      FMapControl.EndUpdate;
    end;
    if FWithAnimation then
      Application.ProcessMessages;
  end;
end;

procedure TdxMapControlAnimationController.DoZoomAnimation(
  Sender: TdxAnimationTransition; var APosition: Integer;
  var AFinished: Boolean);
var
  AZoomDelta: Double;
begin
  AZoomDelta := APosition * FZoomPositionScale;
  DoZoom(FZoomAnimationStartLevel + AZoomDelta);
  if FFinishZoomAnimation then
  begin
    AFinished := True;
    FFinishZoomAnimation := False;
  end;
end;

{ TdxCustomMapItemLayerSelectionController }

constructor TdxMapControlSelectionController.Create(
  AOwner: TdxCustomMapControl);
begin
  inherited Create;
  FMapControl := AOwner;
  FSelection := TdxFastObjectList.Create(False);
end;

destructor TdxMapControlSelectionController.Destroy;
begin
  FreeAndNil(FSelection);
  inherited;
end;

procedure TdxMapControlSelectionController.ClearSelection;
var
  I: Integer;
begin
  for I := FSelection.Count - 1 downto 0 do
    Deselect(FSelection[I] as TdxMapItem);
end;

procedure TdxMapControlSelectionController.Deselect(
  AItem: TdxMapItem);
begin
  if not FMapControl.AllowSelectionChanging(AItem) then
    Exit;
  AItem.ViewInfo.State := mcesNormal;
  FSelection.Remove(AItem);
  FMapControl.DoSelectionChanged;
end;

procedure TdxMapControlSelectionController.Select(
  AItem: TdxMapItem; AShift: TShiftState);
begin
  if ssCtrl in AShift then
    if FSelection.IndexOf(AItem) = -1 then
      AddSelection(AItem)
    else
      Deselect(AItem)
  else
  begin
    ClearSelection;
    AddSelection(AItem);
  end;
end;

procedure TdxMapControlSelectionController.AddSelection(
  AItem: TdxMapItem);
begin
  if not FMapControl.AllowSelectionChanging(AItem) then
    Exit;
  AItem.ViewInfo.State := mcesSelected;
  FSelection.Add(AItem);
  FMapControl.DoSelectionChanged;
end;

function TdxMapControlSelectionController.GetCount: Integer;
begin
  Result := FSelection.Count;
end;

function TdxMapControlSelectionController.GetItems(
  Index: Integer): TdxMapItem;
begin
  Result := FSelection[Index] as TdxMapItem;
end;

{ TdxMapControlDragAndDropObject }

function TdxMapControlDragAndDropObject.GetDragAndDropCursor(
  Accepted: Boolean): TCursor;
begin
  if Accepted then
    Result := crSizeAll
  else
    Result := inherited GetDragAndDropCursor(Accepted);
end;

function TdxMapControlDragAndDropObject.ProcessKeyDown(AKey: Word;
  AShiftState: TShiftState): Boolean;
begin
  Result := AKey in [VK_SHIFT, VK_CONTROL];
end;

{ TdxMapControl }

constructor TdxCustomMapControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FIsViewInfoDirty := True;
  FZoomLevel := 1;
  FCenterPoint := TdxMapControlGeoLocation.Create(Self);
  FCenterPoint.OnChanged := CenterPointChanged;
  CreateSubclasses;
  BorderStyle := cxcbsDefault;
  Width := 400;
  Height := 200;
end;

destructor TdxCustomMapControl.Destroy;
begin
  DestroySubclasses;
  FreeAndNil(FCenterPoint);
  inherited;
end;

function TdxCustomMapControl.AddImageTileLayer: TdxMapImageTileLayer;
begin
  Result := Layers.Add(TdxMapImageTileLayer) as TdxMapImageTileLayer;
end;

function TdxCustomMapControl.AddItemFileLayer: TdxMapItemFileLayer;
begin
  Result := Layers.Add(TdxMapItemFileLayer) as TdxMapItemFileLayer;
end;

function TdxCustomMapControl.AddItemLayer: TdxMapItemLayer;
begin
  Result := Layers.Add(TdxMapItemLayer) as TdxMapItemLayer;
end;

procedure TdxCustomMapControl.BeginUpdate;
begin
  Inc(FLockCount);
end;

procedure TdxCustomMapControl.ClearSelection;
begin
  FMapItemLayerSelectionController.ClearSelection;
end;

procedure TdxCustomMapControl.Deselect(AMapItem: TdxMapItem);
begin
  FMapItemLayerSelectionController.Deselect(AMapItem);
end;

procedure TdxCustomMapControl.EndUpdate;
begin
  Dec(FLockCount);
  if FLockCount = 0 then
    Changed;
end;

function TdxCustomMapControl.IsLocked: Boolean;
begin
  Result := (FLockCount > 0) or IsLoading or IsDestroying or not HandleAllocated;
end;

procedure TdxCustomMapControl.Select(AMapItem: TdxMapItem; AShiftState: TShiftState = [];
  ACheckSelectionMode: Boolean = True);
begin
  if not ACheckSelectionMode or CanSelect then
  begin
    if ACheckSelectionMode and (OptionsBehavior.MapItemSelectMode = mismSingle) then
      AShiftState := [];
    FMapItemLayerSelectionController.Select(AMapItem, AShiftState);
  end;
end;

procedure TdxCustomMapControl.Zoom(AZoomLevel: Double; AAnimated: Boolean);
begin
  Zoom(AZoomLevel, dxPointDouble(Width / 2, Height / 2), AAnimated);
end;

procedure TdxCustomMapControl.ZoomIn;
begin
  ZoomLevel := ZoomLevel + 1;
end;

procedure TdxCustomMapControl.ZoomOut;
begin
  ZoomLevel := ZoomLevel - 1;
end;

procedure TdxCustomMapControl.BoundsChanged;
begin
  inherited BoundsChanged;
  UpdateViewPort;
  Changed;
end;

function TdxCustomMapControl.DoMouseWheel(Shift: TShiftState; WheelDelta: Integer;
  MousePos: TPoint): Boolean;
begin
  Result := inherited DoMouseWheel(Shift, WheelDelta, MousePos);
  if not Result and (WheelDelta <> 0) and CanZoom then
  begin
    Zoom(ZoomLevel + WheelDelta / WHEEL_DELTA, dxPointDouble(ScreenToClient(MousePos)), CanAnimate);
    Result := True;
   end;
end;

procedure TdxCustomMapControl.DoPaint;
begin
  inherited DoPaint;
  ViewInfo.Paint(Canvas);
end;

procedure TdxCustomMapControl.DragAndDrop(const P: TPoint; var Accepted: Boolean);
var
  AOffset: TPoint;
  ASelectedRegion: TRect;
begin
  Accepted := (FDragMode = mcdaScroll) and CanScroll or (FDragMode in [mcdaSelect, mcdaZoom]);
  inherited DragAndDrop(P, Accepted);
  if Accepted then
  begin
    AOffset := Point(MouseDownPos.X - P.X, MouseDownPos.Y - P.Y);
    case FDragMode of
      mcdaScroll:
        if GetBaseLayer <> nil then
          CenterPoint.GeoPoint := GetBaseLayer.Move(FStartCenterPoint, AOffset);
    else
      cxRectIntersect(ASelectedRegion, cxPointsBox([MouseDownPos, P]), ClientBounds);
      UpdateSelectedRegion(ASelectedRegion);
    end;
  end;
end;

procedure TdxCustomMapControl.EndDragAndDrop(Accepted: Boolean);
var
  ASelectedRegion: TRect;
  ADragMode: TdxMapControlDragAction;
begin
  inherited;
  ASelectedRegion := FSelectedRegionBounds;
  ADragMode := FDragMode;
  UpdateSelectedRegion(cxEmptyRect);
  FDragMode := mcdaNone;
  if ADragMode = mcdaSelect then
    SelectMapItemsInRegion(ASelectedRegion)
  else
    if ADragMode = mcdaZoom then
      ZoomToRegion(ASelectedRegion);
end;

function TdxCustomMapControl.GetDragAndDropObjectClass: TcxDragAndDropObjectClass;
begin
  Result := TdxMapControlDragAndDropObject;
end;

procedure TdxCustomMapControl.InitControl;
begin
  inherited InitControl;
  CheckViewPort;
  CheckChanges;
end;

function TdxCustomMapControl.IsDoubleBufferedNeeded: Boolean;
begin
  Result := True;
end;

procedure TdxCustomMapControl.Loaded;
begin
  inherited Loaded;
  UpdateViewPort;
  ZoomLevel := FLoadedZoomLevel;
  CheckChanges;
end;

procedure TdxCustomMapControl.LookAndFeelChanged(Sender: TcxLookAndFeel;
  AChangedValues: TcxLookAndFeelValues);
begin
  ViewInfo.ClearCache;
  Changed;
end;

procedure TdxCustomMapControl.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  FShift := Shift;
  inherited;
  FController.MouseDown(Button, Shift, X, Y);
end;

procedure TdxCustomMapControl.MouseLeave(AControl: TControl);
begin
  inherited;
  FController.MouseLeave;
end;

procedure TdxCustomMapControl.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  inherited MouseMove(Shift, X, Y);
  FController.MouseMove(Shift, X, Y);
end;

procedure TdxCustomMapControl.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited;
  FController.MouseUp(Button, Shift, X, Y);
end;

function TdxCustomMapControl.StartDragAndDrop(const P: TPoint): Boolean;
begin
  Result := not Controller.HitTest.HitAtNavigationPanel and (GetBaseLayer <> nil);
  if Result then
  begin
    if (FOptionsBehavior.MapItemSelectMode = mismExtended) and
      (FShift * [ssShift, ssCtrl] = [ssShift]) then
      FDragMode := mcdaSelect
    else
      if CanZoom and (FShift * [ssShift, ssCtrl] = [ssShift, ssCtrl]) then
        FDragMode := mcdaZoom
      else
      begin
        FStartCenterPoint := CenterPoint.GeoPoint;
        FDragMode := mcdaScroll;
      end;
  end;
end;

// IdxScreenTipProvider
function TdxCustomMapControl.GetMapAction: TBasicAction;
begin
  Result := nil;
end;

function TdxCustomMapControl.GetScreenTip: TdxScreenTip;
begin
  Result := Controller.GetScreenTip;
end;

function TdxCustomMapControl.GetShortCut: string;
begin
  Result := '';
end;

function TdxCustomMapControl.AllowSelectionChanging(AItem: TdxMapItem): Boolean;
begin
  Result := True;
  if Assigned(FOnSelectionChanging) then
    FOnSelectionChanging(Self, AItem, Result);
end;

function TdxCustomMapControl.CanAnimate: Boolean;
begin
  Result := not IsDesigning and FOptionsBehavior.Animation;
end;

function TdxCustomMapControl.CanScroll: Boolean;
begin
  Result := FOptionsBehavior.Scrolling;
end;

function TdxCustomMapControl.CanSelect: Boolean;
begin
  Result := FOptionsBehavior.MapItemSelectMode <> mismNone;
end;

function TdxCustomMapControl.CanZoom: Boolean;
begin
  Result := FOptionsBehavior.Zooming;
end;

procedure TdxCustomMapControl.Changed;
begin
  if IsLocked then
  begin
    FIsViewInfoDirty := True;
    Exit;
  end;
  FViewInfo.Calculate;
  Invalidate;
  FIsViewInfoDirty := False;
end;

procedure TdxCustomMapControl.CheckChanges;
begin
  if FIsViewInfoDirty then
    Changed;
end;

procedure TdxCustomMapControl.CheckViewPort;
begin
  if not FIsViewPortValid then
    UpdateViewPort;
end;

procedure TdxCustomMapControl.DoSelectionChanged;
begin
  if not IsDestroying then
    if Assigned(FOnSelectionChanged) then
      FOnSelectionChanged(Self);
end;

function TdxCustomMapControl.GetKilometersScale(
  ACenterPoint: TdxMapControlGeoPoint; AScreenDistance: Double): Double;
begin
  if GetBaseLayer <> nil then
    Result := GetBaseLayer.GetKilometersScale(ACenterPoint, AScreenDistance)
  else
    Result := 0;
end;

function TdxCustomMapControl.GetSelectedMapItemCount: Integer;
begin
  Result := FMapItemLayerSelectionController.Count;
end;

function TdxCustomMapControl.GetSelectedMapItems(Index: Integer): TdxMapItem;
begin
  Result := FMapItemLayerSelectionController.Items[Index];
end;

function TdxCustomMapControl.GetMaxZoomLevel: Integer;
begin
  Result := dxMapControlMaxZoomLevel;
end;

procedure TdxCustomMapControl.InvalidateViewPort;
begin
  FIsViewPortValid := False;
end;

procedure TdxCustomMapControl.LayersChanged(Sender: TObject; AItem: TcxComponentCollectionItem;
  AAction: TcxComponentCollectionNotification);
begin
  Changed;
end;

function TdxCustomMapControl.ScreenPointToGeoPoint(
  APoint: TdxPointDouble): TdxMapControlGeoPoint;
begin
  if GetBaseLayer <> nil then
    Result := GetBaseLayer.ScreenPointToGeoPoint(APoint)
  else
    Result := dxMapControlGeoPoint(0, 0);
end;

procedure TdxCustomMapControl.ScrollMap(X, Y: Integer);
begin
  if GetBaseLayer <> nil then
    CenterPoint.GeoPoint := GetBaseLayer.Move(CenterPoint.GeoPoint, cxPoint(X, Y));
end;

procedure TdxCustomMapControl.SelectMapItemsInRegion(const ARect: TRect);
var
  I: Integer;
  AMapItems: TList;
begin
  ClearSelection;
  AMapItems := TList.Create;
  try
    for I := 0 to Layers.Count - 1 do
    begin
      if Layers[I] is TdxCustomMapItemLayer then
        TdxCustomMapItemLayer(Layers[I]).GetItemsInRegion(ARect, AMapItems);
    end;
    for I := 0 to AMapItems.Count -1 do
      Select(TdxMapItem(AMapItems[I]), [ssCtrl]);
  finally
    AMapItems.Free;
  end;
end;

procedure TdxCustomMapControl.UpdateViewPort;
begin
  if not IsLoading then
  begin
    Layers.UpdateViewPort;
    FIsViewPortValid := True;
  end;
end;

procedure TdxCustomMapControl.Zoom(AZoomLevel: Double;
  const AAnchorPoint: TdxPointDouble; AAnimated: Boolean);
begin
  if not AllowZoomLevelChanges(AZoomLevel) then
    Exit;
  CheckZoomLevel(AZoomLevel);
  if FZoomLevel <> AZoomLevel then
  begin
    FZoomLevel := AZoomLevel;
    FAnimationController.Zoom(AZoomLevel, AAnchorPoint, AAnimated and not IsLocked);
    DoZoomLevelChanged;
  end;
end;

procedure TdxCustomMapControl.ZoomToRegion(const ARect: TRect);
var
  ALayer: TdxMapLayer;
  AMinPoint, AMaxPoint, ARegionTopLeft, ARegionBottomRight: TdxPointDouble;
  AAspectRatio, ARegionLength, AViewLength, AZoomDelta: Double;
  AViewPortSize, ASelectedRegionSize: TdxSizeDouble;
begin
  ALayer := GetBaseLayer;
  if (ALayer <> nil) and not ARect.IsEmpty then
  begin
    AMinPoint := ALayer.ScreenPointToMapUnit(dxNullPointDouble);
    AMaxPoint := ALayer.ScreenPointToMapUnit(dxPointDouble(ALayer.ViewportInPixels.cx, ALayer.ViewportInPixels.cy));
    AViewPortSize := dxSizeDouble(AMaxPoint.X - AMinPoint.X, AMaxPoint.Y - AMinPoint.Y);
    ARegionTopLeft := ALayer.ScreenPointToMapUnit(dxPointDouble(ARect.TopLeft));
    ARegionBottomRight := ALayer.ScreenPointToMapUnit(dxPointDouble(ARect.BottomRight));

    ASelectedRegionSize := dxSizeDouble(ARegionBottomRight.X - ARegionTopLeft.X,
      ARegionBottomRight.Y - ARegionTopLeft.Y);
    AAspectRatio := AViewPortSize.Width / AViewPortSize.Height;
    if ASelectedRegionSize.Width > ASelectedRegionSize.Height * AAspectRatio then
    begin
      ARegionLength := ASelectedRegionSize.Width;
      AViewLength := AViewPortSize.Width;
    end
    else
    begin
      ARegionLength := ASelectedRegionSize.Height;
      AViewLength := AViewPortSize.Height;
    end;
    AZoomDelta := Log2(AViewLength / ARegionLength);

    CenterPoint.GeoPoint := ALayer.ScreenPointToGeoPoint(dxPointDouble(cxRectCenter(ARect)));
    ZoomLevel := Round(ZoomLevel + AZoomDelta);
  end;
end;

function TdxCustomMapControl.CreateAnimationController: TdxMapControlAnimationController;
begin
  Result := TdxMapControlAnimationController.Create(Self);
end;

function TdxCustomMapControl.CreateController: TdxMapControlController;
begin
  Result := TdxMapControlController.Create(Self);
end;

function TdxCustomMapControl.CreateMapItemsLayerSelectionController: TdxMapControlSelectionController;
begin
  Result := TdxMapControlSelectionController.Create(Self);
end;

function TdxCustomMapControl.CreateNavigationPanelOptions: TdxMapControlNavigationPanel;
begin
  Result := TdxMapControlNavigationPanel.Create(Self);
end;

function TdxCustomMapControl.CreateOptionsBehavior: TdxMapControlOptionsBehavior;
begin
  Result := TdxMapControlOptionsBehavior.Create(Self);
end;

procedure TdxCustomMapControl.CreateSubclasses;
begin
  FController := CreateController;
  FPainter := CreatePainter;
  FViewInfo := CreateViewInfo;
  FMapItemLayerSelectionController := CreateMapItemsLayerSelectionController;
  FNavigationPanel := CreateNavigationPanelOptions;
  FAnimationController := CreateAnimationController;
  FOptionsBehavior := CreateOptionsBehavior;
  FLayers := TdxMapLayers.Create(Self, TdxMapLayer);
  FLayers.OnChange := LayersChanged;
end;

function TdxCustomMapControl.CreatePainter: TdxMapControlPainter;
begin
  Result := TdxMapControlPainter.Create(Self);
end;

procedure TdxCustomMapControl.DestroySubclasses;
begin
  FreeAndNil(FLayers);
  FreeAndNil(FOptionsBehavior);
  FreeAndNil(FAnimationController);
  FreeAndNil(FNavigationPanel);
  FreeAndNil(FMapItemLayerSelectionController);
  FreeAndNil(FViewInfo);
  FreeAndNil(FPainter);
  FreeAndNil(FController);
end;

function TdxCustomMapControl.CreateViewInfo: TdxMapControlViewInfo;
begin
  Result := TdxMapControlViewInfo.Create(Self);
end;

function TdxCustomMapControl.AllowZoomLevelChanges(ALevel: Double): Boolean;
begin
  Result := True;
  if Assigned(FOnZoomLevelChanging) then
    FOnZoomLevelChanging(Self, ALevel, Result);
end;

procedure TdxCustomMapControl.CenterPointChanged(ASender: TObject);
begin
  UpdateViewPort;
  Changed;
end;

procedure TdxCustomMapControl.CheckZoomLevel(var ALevel: Double);
begin
  ALevel := Min(Max(ALevel, 1), dxMapControlMaxZoomLevel);
end;

procedure TdxCustomMapControl.DoZoomLevelChanged;
begin
  if Assigned(FOnZoomLevelChanged) then
    FOnZoomLevelChanged(Self);
end;

function TdxCustomMapControl.GetActualCenterPoint: TdxMapControlGeoPoint;
begin
  Result := FCenterPoint.GeoPoint;
end;

function TdxCustomMapControl.GetActualZoomLevel: Double;
begin
  Result := FAnimationController.ZoomLevel;
end;

function TdxCustomMapControl.GetBaseLayer: TdxMapLayer;
begin
  Result := Layers.GetBaseLayer;
end;

procedure TdxCustomMapControl.GetChildren(Proc: TGetChildProc; Root: TComponent);
var
  I: Integer;
begin
  for I := 0 to Layers.Count - 1 do
    if Layers[I].Owner = Root then Proc(Layers[I]);
end;

function TdxCustomMapControl.GetHitTest: TdxMapControlHitTest;
begin
  Result := Controller.HitTest;
end;

function TdxCustomMapControl.IsZoomLevelStored: Boolean;
begin
  Result := FZoomLevel <> 1;
end;

procedure TdxCustomMapControl.SetCenterPoint(AValue: TdxMapControlGeoLocation);
begin
  FCenterPoint.Assign(AValue);
end;

procedure TdxCustomMapControl.SetLayers(const Value: TdxMapLayers);
begin
  FLayers.Assign(Value);
end;

procedure TdxCustomMapControl.SetNavigationPanel(AValue: TdxMapControlNavigationPanel);
begin
  FNavigationPanel.Assign(AValue);
end;

procedure TdxCustomMapControl.SetOptionsBehavior(
  AValue: TdxMapControlOptionsBehavior);
begin
  FOptionsBehavior.Assign(AValue);
end;

procedure TdxCustomMapControl.SetZoomLevel(Value: Double);
begin
  if IsLoading then
    FLoadedZoomLevel := Value
  else
    Zoom(Value, CanAnimate);
end;

procedure TdxCustomMapControl.UpdateSelectedRegion(const R: TRect);
begin
  cxInvalidateRect(Handle, FSelectedRegionBounds);
  FSelectedRegionBounds := R;
  cxInvalidateRect(Handle, R);
end;

end.
