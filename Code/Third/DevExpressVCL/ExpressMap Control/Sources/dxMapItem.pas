{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressMapControl                                        }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSMAPCONTROL AND ALL             }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxMapItem;

interface

{$I cxVer.inc}

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Types, Classes, Graphics, SysUtils, Math, Windows, Controls,
  dxCore, dxCoreClasses, dxCoreGraphics,cxGraphics, cxGeometry, cxClasses,
  dxGDIPlusClasses, dxGDIPlusApi, cxLookAndFeelPainters, dxScreenTip,
  dxMapLayer, dxMapControlTypes, dxMapItemStyle, dxMapControlElementViewInfo;

type
  TdxMapItem = class;
  TdxMapPointer = class;
  TdxMapPath = class;
  TdxMapPathSegment = class;
  TdxMapDot = class;

  TdxMapItemViewInfo = class(TdxMapControlElementViewInfo)
  private
    FStyle: TdxMapItemStyle;
    FIsCachedBoundsValid: Boolean;
    FItem: TdxMapItem;
    FCachedRenderScale, FCachedRenderOffset: TdxPointDouble;
    procedure CalculateCachedBounds;
    function GetBorderColor: TdxAlphaColor;
    function GetBorderWidth: Integer;
    function GetColor: TdxAlphaColor;
    function GetLookAndFeelPainter: TcxCustomLookAndFeelPainter;
    function GetStyle: TdxMapItemStyle;
    function GetTextColor: TdxAlphaColor;
    function GetTextGlowColor: TdxAlphaColor;
    procedure ResetRenderCache;
  protected
    procedure CalculateScreenBounds; virtual;
    procedure DoCalculateCachedBounds; virtual;
    function DoCanBeVisible: Boolean; virtual;
    procedure DoElementDestroying; override;
    procedure DoPaint; virtual;
    function GetForbiddenStates: TdxMapControlElementStates; override;
    function GetHitTestIndex: Integer; override;
    procedure CapturePaintCanvas;
    procedure Invalidate; override;
    function IsBoundsScalable: Boolean; virtual;
    function NeedCalculateScreenBoundsToDetermineVisibility: Boolean; virtual;
    procedure ReleasePaintCanvas;
    property LookAndFeelPainter: TcxCustomLookAndFeelPainter read GetLookAndFeelPainter;
  public
    constructor Create(AOwner: TdxMapItem); reintroduce; virtual;
    destructor Destroy; override;
    procedure CalculateBounds; override;
    function CanBeVisible: Boolean; virtual;
    procedure ClearCache; override;
    function GetScreenTip: TdxScreenTip; override;
    function IsIntersect(const ARect: TRect): Boolean; virtual;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Paint(ACanvas: TcxCanvas); override;

    property Item: TdxMapItem read FItem;
    property Style: TdxMapItemStyle read GetStyle;
  end;

  TdxMapItem = class(TcxComponentCollectionItem)
  private
 //   FAllowHotTrack: Boolean;
    FLayer: TdxMapLayer;
    FStyle: TdxMapItemStyle;
    FViewInfo: TdxMapItemViewInfo;
    FScreenTip: TdxScreenTip;
    FStyleHot: TdxMapItemStyle;
    FStyleSelected: TdxMapItemStyle;
    FVisible: Boolean;
    function GetSelected: Boolean;
    procedure SetSelected(const Value: Boolean);
    procedure SetStyle(Value: TdxMapItemStyle);
    procedure SetStyleHot(const Value: TdxMapItemStyle);
    procedure SetStyleSelected(const Value: TdxMapItemStyle);
    procedure SetVisible(const Value: Boolean);
    procedure StyleChanged(ASender: TObject);
  protected
    function CreateViewInfo: TdxMapItemViewInfo; virtual;
    procedure DoAssign(Source: TPersistent); virtual;
    function GetCollectionFromParent(AParent: TComponent): TcxComponentCollection; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SelectionChanged(AShift: TShiftState);

    property Layer: TdxMapLayer read FLayer write FLayer;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function GetStyle(AState: TdxMapControlElementState): TdxMapItemStyle;
    procedure SetParentComponent(Value: TComponent); override;

    //property Attributes: TdxMapItemAttributes read FAttributes;
    property Selected: Boolean read GetSelected write SetSelected;
    property ViewInfo: TdxMapItemViewInfo read FViewInfo;
  published
  //  property AllowHotTrack: Boolean read FAllowHotTrack write FAllowHotTrack default False;
    property ScreenTip: TdxScreenTip read FScreenTip write FScreenTip;
    property Style: TdxMapItemStyle read FStyle write SetStyle;
    property StyleHot: TdxMapItemStyle read FStyleHot write SetStyleHot;
    property StyleSelected: TdxMapItemStyle read FStyleSelected write SetStyleSelected;
    property Visible: Boolean read FVisible write SetVisible default True;
  end;

  TdxMapItemClass = class of TdxMapItem;

  TdxMapItems = class(TcxComponentCollection)
  private
    function GetItem(Index: Integer): TdxMapItem;
    procedure SetItem(Index: Integer; const Value: TdxMapItem);
  protected
    function GetItemPrefixName: string; override;
    procedure SetItemName(AItem: TcxComponentCollectionItem); override;
  public
    function Add(AMapItemClass: TdxMapItemClass): TdxMapItem;
    procedure Assign(Source: TPersistent); override;

    property Items[Index: Integer]: TdxMapItem read GetItem write SetItem; default;
  end;

  TdxMapSinglePointItem = class(TdxMapItem)
  private
    FLocation: TdxMapControlGeoLocation;
    procedure LocationChanged(ASender: TObject);
    procedure SetLocation(Value: TdxMapControlGeoLocation);
  protected
    procedure DoAssign(Source: TPersistent); override;
    property Location: TdxMapControlGeoLocation read FLocation write SetLocation;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

  TdxMapDotShapeKind = (mcskCircle, mcskRectangle);

  TdxMapDotViewInfo = class(TdxMapItemViewInfo)
  private
    FMapLocation: TdxPointDouble;
    FRadius: Double;
    FRelativeBounds: TdxRectDouble;
    function GetMapDot: TdxMapDot;
  protected
    procedure CalculateScreenBounds; override;
    procedure DoCalculateCachedBounds; override;
    function DoCanBeVisible: Boolean; override;
    procedure DoPaint; override;
  public
    property MapDot: TdxMapDot read GetMapDot;
  end;

  TdxMapDot = class(TdxMapSinglePointItem)
  private
    FShapeKind: TdxMapDotShapeKind;
    FSize: Integer;
    procedure SetShapeKind(Value: TdxMapDotShapeKind);
    procedure SetSize(Value: Integer);
  protected
    function CreateViewInfo: TdxMapItemViewInfo; override;
    procedure DoAssign(Source: TPersistent); override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property Location;
    property ShapeKind: TdxMapDotShapeKind read FShapeKind write SetShapeKind default mcskCircle;
    property Size: Integer read FSize write SetSize default 5;
  end;

  TdxMapRectangleViewInfo = class(TdxMapItemViewInfo)
  private
    FMapBounds: TdxRectDouble;
    FMapPoint1, FMapPoint2: TdxPointDouble;
  protected
    procedure CalculateScreenBounds; override;
    procedure DoCalculateCachedBounds; override;
    function DoCanBeVisible: Boolean; override;
    procedure DoPaint; override;
    function IsBoundsScalable: Boolean; override;
  end;

  TdxMapRectangle = class(TdxMapSinglePointItem)
  private
    FHeight: Double;
    FWidth: Double;
    procedure SetHeight(Value: Double);
    procedure SetWidth(Value: Double);
  protected
    function CreateViewInfo: TdxMapItemViewInfo; override;
    procedure DoAssign(Source: TPersistent); override;
  published
    property Height: Double read FHeight write SetHeight;
    property Location;
    property Width: Double read FWidth write SetWidth;
  end;

  TdxMapEllipseViewInfo = class(TdxMapRectangleViewInfo)
  protected
    procedure DoPaint; override;
    function PtInElement(const APoint: TPoint): Boolean; override;
  end;

  TdxMapEllipse = class(TdxMapRectangle)
  protected
    function CreateViewInfo: TdxMapItemViewInfo; override;
  end;

  TdxMapControlGeoPointItem = class(TCollectionItem)
  private
    FGeoPoint: TdxMapControlGeoPoint;
    function GetLatitude: Double;
    function GetLongitude: Double;
    procedure SetGeoPoint(const Value: TdxMapControlGeoPoint);
    procedure SetLatitude(const Value: Double);
    procedure SetLongitude(const Value: Double);
  public
    procedure Assign(Source: TPersistent); override;
    property GeoPoint: TdxMapControlGeoPoint read FGeoPoint write SetGeoPoint;
  published
    property Longitude: Double read GetLongitude write SetLongitude;
    property Latitude: Double read GetLatitude write SetLatitude;
  end;

  TdxMapControlGeoPointCollection = class(TCollection)
  private
    FOnChanged: TNotifyEvent;
    function GetItem(Index: Integer): TdxMapControlGeoPointItem;
    procedure SetItem(Index: Integer; const Value: TdxMapControlGeoPointItem);
  protected
    procedure Update(Item: TCollectionItem); override;
  public
    function Add: TdxMapControlGeoPointItem;

    property Items[Index: Integer]: TdxMapControlGeoPointItem read GetItem write SetItem; default;
    property OnChanged: TNotifyEvent read FOnChanged write FOnChanged;
  end;

  TdxMapPolylineViewInfo = class(TdxMapItemViewInfo)
  private
    FMapBounds: TdxRectDouble;
    FMapPoints: array of TdxPointDouble;
    FScreenPoints: array of TPoint;
  protected
    procedure CalculateScreenBounds; override;
    function CanAddPoint: Boolean; virtual;
    function CreatePath: TdxGpPath; virtual;
    procedure DoCalculateCachedBounds; override;
    function DoCanBeVisible: Boolean; override;
    procedure DoPaint; override;
    function IsBoundsScalable: Boolean; override;
    function NeedCalculateScreenBoundsToDetermineVisibility: Boolean; override;
    function PtInElement(const APoint: TPoint): Boolean; override;
  public
    function IsIntersect(const ARect: TRect): Boolean; override;
  end;

  TdxMapPolyline = class(TdxMapItem)
  private
    FGeoPoints: TdxMapControlGeoPointCollection;
    procedure SetGeoPoints(const Value: TdxMapControlGeoPointCollection);
  protected
    function CreateViewInfo: TdxMapItemViewInfo; override;
    procedure DoAssign(Source: TPersistent); override;
    procedure GeoPointsChanged(Sender: TObject); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property GeoPoints: TdxMapControlGeoPointCollection read FGeoPoints write SetGeoPoints;
  end;

  TdxMapPolygonViewInfo = class(TdxMapPolylineViewInfo)
  protected
    function CanAddPoint: Boolean; override;
    function CreatePath: TdxGpPath; override;
    function DoCanBeVisible: Boolean; override;
    procedure DoPaint; override;
    function NeedCalculateScreenBoundsToDetermineVisibility: Boolean; override;
    function PtInElement(const APoint: TPoint): Boolean; override;
  end;

  TdxMapPolygon = class(TdxMapPolyline)
  protected
    function CreateViewInfo: TdxMapItemViewInfo; override;
  end;

  TdxMapPathSegmentViewInfo = class(TdxMapItemViewInfo)
  private
    FMapBounds: TdxRectDouble;
    FMapPoints: array of TdxPointDouble;
    FScreenPoints: array of TPoint;
    FSegment: TdxMapPathSegment;
  protected
    procedure AddToPath(APath: TdxGPPath);
    procedure CalculateScreenBounds; override;
    procedure DoCalculateCachedBounds; override;
    function DoCanBeVisible: Boolean; override;
    function GetHitTestIndex: Integer; override;
    function IsBoundsScalable: Boolean; override;
    property Segment: TdxMapPathSegment read FSegment;
  public
    constructor Create(AOwner: TdxMapPathSegment); reintroduce; virtual;
  end;

  TdxMapPathSegment = class(TCollectionItem)
  private
    FGeoPoints: TdxMapControlGeoPointCollection;
    FViewInfo: TdxMapPathSegmentViewInfo;
    FVisible: Boolean;
    procedure SetGeoPoints(const Value: TdxMapControlGeoPointCollection);
    procedure SetVisible(const Value: Boolean);
  protected
    procedure GeoPointsChanged(Sender: TObject);
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    property ViewInfo: TdxMapPathSegmentViewInfo read FViewInfo;
  published
    property GeoPoints: TdxMapControlGeoPointCollection read FGeoPoints write SetGeoPoints;
    property Visible: Boolean read FVisible write SetVisible default True;
  end;

  TdxMapPathSegments = class(TOwnedCollection)
  private
    FOnChanged: TNotifyEvent;
    function GetItem(Index: Integer): TdxMapPathSegment;
    procedure SetItem(Index: Integer; const Value: TdxMapPathSegment);
  protected
    procedure Update(Item: TCollectionItem); override;
  public
    function Add: TdxMapPathSegment;

    property Items[Index: Integer]: TdxMapPathSegment read GetItem write SetItem; default;
    property OnChanged: TNotifyEvent read FOnChanged write FOnChanged;
  end;

  TdxMapPathViewInfo = class(TdxMapItemViewInfo)
  private
    function GetMapPath: TdxMapPath;
    function GetSegmentInfo(Index: Integer): TdxMapPathSegmentViewInfo;
  protected
    procedure AddVisibleElements; override;
    function CreatePath: TdxGpPath;
    procedure DoPaint; override;
    function PtInElement(const APoint: TPoint): Boolean; override;
  public
    procedure CalculateBounds; override;
    function IsIntersect(const ARect: TRect): Boolean; override;
    property MapPath: TdxMapPath read GetMapPath;
    property SegmentInfo[Index: Integer]: TdxMapPathSegmentViewInfo read GetSegmentInfo;
  end;

  TdxMapPath = class(TdxMapItem)
  private
    FSegments: TdxMapPathSegments;
    procedure SetSegments(const Value: TdxMapPathSegments);
  protected
    function CreateViewInfo: TdxMapItemViewInfo; override;
    procedure DoAssign(Source: TPersistent); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property Segments: TdxMapPathSegments read FSegments write SetSegments;
  end;

  TdxMapPointerViewInfo = class(TdxMapItemViewInfo)
  private
    FCachedImage: GpCachedBitmap;
    FImageRect: TRect;
    FIsCachedImageValid: Boolean;
    FTextBounds: TRect;
    FRelativeImageRect: TRect;
    FRelativeTextRect: TRect;
    FRelativeBounds: TRect;
    FMapLocation: TdxPointDouble;
    function CheckCachedImageCreated(AGpCanvas: TdxGPCanvas): Boolean;
    function IsImageVisible: Boolean;
    function GetMapPointer: TdxMapPointer;
  protected
    procedure CalculateScreenBounds; override;
    procedure DoCalculateCachedBounds; override;
    function DoCanBeVisible: Boolean; override;
    procedure DoPaint; override;
    function GetImageOrigin: TdxPointDouble; virtual;
    function PtInElement(const APoint: TPoint): Boolean; override;
    function UsePushpinImageAsDefault: Boolean; virtual;
  public
    destructor Destroy; override;

    property MapPointer: TdxMapPointer read GetMapPointer;
  end;

  TdxMapPointer = class(TdxMapSinglePointItem)
  private
    FImage: TdxSmartImage;
    FImageOrigin: TdxPointDoublePersistent;
    FImageVisible: Boolean;
    FText: string;
    procedure ImageChanged(ASender: TObject);
    procedure ImageOriginChanged(ASender: TObject);
    procedure SetImage(AValue: TdxSmartImage);
    procedure SetImageOrigin(Value: TdxPointDoublePersistent);
    procedure SetImageVisible(const Value: Boolean);
    procedure SetText(const Value: string);
    function IsImageOriginStored: Boolean;
  protected
    function CreateViewInfo: TdxMapItemViewInfo; override;
    procedure DoAssign(Source: TPersistent); override;

    property Image: TdxSmartImage read FImage write SetImage;
    property ImageOrigin: TdxPointDoublePersistent read FImageOrigin write SetImageOrigin stored IsImageOriginStored;
    property ImageVisible: Boolean read FImageVisible write SetImageVisible default True;
    property Text: string read FText write SetText;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

  TdxMapPushpin = class(TdxMapPointer)
  published
    property Location;
  end;

  TdxMapCustomElementViewInfo = class(TdxMapPointerViewInfo)
  protected
    procedure DoCalculateCachedBounds; override;
    procedure DoPaint; override;
  end;

  TdxMapCustomElement = class(TdxMapPointer)
  protected
    function CreateViewInfo: TdxMapItemViewInfo; override;
  published
    property Image;
    property ImageOrigin;
    property ImageVisible;
    property Location;
    property Text;
  end;

implementation

uses
  dxCustomMapItemLayer, dxMapControlViewInfo, dxMapControl;

type
  TdxCustomMapControlAccess = class(TdxCustomMapControl);

const
  dxMapPointerDefaultImageOrigin: TdxPointDouble = (X: 0.5; Y: 0.5);

function GetMapControl(AMapControl: TComponent): TdxCustomMapControlAccess;
begin
  Result := TdxCustomMapControlAccess(AMapControl);
end;

{ TdxMapItemViewInfo }

constructor TdxMapItemViewInfo.Create(AOwner: TdxMapItem);
begin
  inherited Create;
  FItem := AOwner;
  ResetRenderCache;
  FStyle := TdxMapItemStyle.Create(nil);
end;

destructor TdxMapItemViewInfo.Destroy;
begin
  FreeAndNil(FStyle);
  inherited Destroy;
end;

procedure TdxMapItemViewInfo.CalculateBounds;
var
  ABorderWidth: Integer;
begin
  if not FIsCachedBoundsValid then
  begin
    ResetRenderCache;
    CalculateCachedBounds;
  end;
  if not dxPointDoubleIsEqual(FCachedRenderScale, Item.Layer.RenderScale) or
    not dxPointDoubleIsEqual(FCachedRenderOffset, Item.Layer.RenderOffset) then
  begin
    CalculateScreenBounds;
    ABorderWidth := Style.BorderWidth;
    Bounds := cxRectInflate(Bounds, ABorderWidth, ABorderWidth);
    FCachedRenderScale := Item.Layer.RenderScale;
    FCachedRenderOffset := Item.Layer.RenderOffset;
  end;
end;

function TdxMapItemViewInfo.CanBeVisible: Boolean;
begin
  Result := Item.Visible;
  if Result then
  begin
    if NeedCalculateScreenBoundsToDetermineVisibility then
      CalculateBounds
    else
      if not FIsCachedBoundsValid then
        CalculateCachedBounds;
    Result := DoCanBeVisible;
  end;
end;

procedure TdxMapItemViewInfo.ClearCache;
begin
  inherited ClearCache;
  FIsCachedBoundsValid := False;
end;

function TdxMapItemViewInfo.GetScreenTip: TdxScreenTip;
begin
  Result := Item.ScreenTip;
end;

function TdxMapItemViewInfo.IsIntersect(const ARect: TRect): Boolean;
begin
  Result := cxRectIntersect(ARect, Bounds);
end;

procedure TdxMapItemViewInfo.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  Item.SelectionChanged(Shift);
end;

procedure TdxMapItemViewInfo.Paint(ACanvas: TcxCanvas);
begin
  dxGPPaintCanvas.BeginPaint(ACanvas.Handle, Bounds);
  try
    dxGPPaintCanvas.SmoothingMode := smAntiAlias;
    if dxGpIsRectVisible(dxGPPaintCanvas.Handle, Bounds) then
      DoPaint;
  finally
    dxGPPaintCanvas.EndPaint;
  end;
end;

procedure TdxMapItemViewInfo.CalculateScreenBounds;
begin
end;

procedure TdxMapItemViewInfo.DoCalculateCachedBounds;
begin
end;

function TdxMapItemViewInfo.DoCanBeVisible: Boolean;
begin
  Result := True;
end;

procedure TdxMapItemViewInfo.DoElementDestroying;
begin
  GetMapControl(Item.Layer.GetParentComponent).Controller.ElementDestroying(Self);
end;

procedure TdxMapItemViewInfo.DoPaint;
begin
end;

function TdxMapItemViewInfo.GetForbiddenStates: TdxMapControlElementStates;
begin
  Result := inherited GetForbiddenStates + [mcesPressed, mcesDisabled];
  if not (Item.Layer as TdxCustomMapItemLayer).AllowHotTrack then
    Result := Result + [mcesHot];
end;

function TdxMapItemViewInfo.GetHitTestIndex: Integer;
begin
  Result := mchtMapItem;
end;

procedure TdxMapItemViewInfo.CapturePaintCanvas;
var
  DC: HDC;
begin
  DC := dxGPPaintCanvas.GetHDC;
  cxPaintCanvas.BeginPaint(DC);
end;

procedure TdxMapItemViewInfo.Invalidate;
begin
  Item.Layer.InvalidateRect(Bounds);
end;

function TdxMapItemViewInfo.IsBoundsScalable: Boolean;
begin
  Result := False;
end;

function TdxMapItemViewInfo.NeedCalculateScreenBoundsToDetermineVisibility: Boolean;
begin
  Result := not IsBoundsScalable;
end;

procedure TdxMapItemViewInfo.ReleasePaintCanvas;
var
  DC: HDC;
begin
  DC := cxPaintCanvas.Handle;
  cxPaintCanvas.EndPaint;
  dxGPPaintCanvas.ReleaseHDC(DC);
end;

procedure TdxMapItemViewInfo.CalculateCachedBounds;
begin
  DoCalculateCachedBounds;
  FIsCachedBoundsValid := True;
end;

function TdxMapItemViewInfo.GetBorderColor: TdxAlphaColor;
begin
  case State of
    mcesNormal:
      Result := LookAndFeelPainter.MapControlShapeBorderColor;
    mcesSelected:
      Result := LookAndFeelPainter.MapControlShapeBorderSelectedColor;
  else
    Result := LookAndFeelPainter.MapControlShapeBorderHighlightedColor
  end;
end;

function TdxMapItemViewInfo.GetBorderWidth: Integer;
begin
  case State of
    mcesNormal:
      Result := LookAndFeelPainter.MapControlShapeBorderWidth;
    mcesSelected:
      Result := LookAndFeelPainter.MapControlShapeBorderSelectedWidth;
  else
    Result := LookAndFeelPainter.MapControlShapeBorderHighlightedWidth
  end;
end;

function TdxMapItemViewInfo.GetColor: TdxAlphaColor;
begin
  case State of
    mcesNormal:
      Result := LookAndFeelPainter.MapControlShapeColor;
    mcesSelected:
      Result := LookAndFeelPainter.MapControlShapeSelectedColor;
  else
    Result := LookAndFeelPainter.MapControlShapeHighlightedColor
  end;
end;

function TdxMapItemViewInfo.GetLookAndFeelPainter: TcxCustomLookAndFeelPainter;
begin
  Result := GetMapControl(Item.Layer.GetParentComponent).LookAndFeelPainter;
end;

function TdxMapItemViewInfo.GetStyle: TdxMapItemStyle;

  procedure MergeStyles(AStyle, AOwnerStyle: TdxMapItemStyle);
  begin
    if mcsvColor in AStyle.AssignedValues then
      FStyle.Color := AStyle.Color
    else
      FStyle.Color := AOwnerStyle.Color;
    if FStyle.Color = dxacDefault then
      FStyle.Color := GetColor;

    if mcsvBorderWidth in AStyle.AssignedValues then
      FStyle.BorderWidth := AStyle.BorderWidth
    else
      if mcsvBorderWidth in AOwnerStyle.AssignedValues then
        FStyle.BorderWidth := AOwnerStyle.BorderWidth
      else
        FStyle.BorderWidth := GetBorderWidth;

    if mcsvBorderColor in AStyle.AssignedValues then
      FStyle.BorderColor := AStyle.BorderColor
    else
      FStyle.BorderColor := AOwnerStyle.BorderColor;
    if FStyle.BorderColor = dxacDefault then
      FStyle.BorderColor := GetBorderColor;

    if mcsvFont in AStyle.AssignedValues then
      FStyle.Font := AStyle.Font
    else
      if mcsvFont in AOwnerStyle.AssignedValues then
        FStyle.Font := AOwnerStyle.Font;

    if mcsvTextColor in AStyle.AssignedValues then
      FStyle.TextColor := AStyle.TextColor
    else
      FStyle.TextColor := AOwnerStyle.TextColor;
    if FStyle.TextColor = dxacDefault then
      FStyle.TextColor := GetTextColor;

    if mcsvTextGlowColor in AStyle.AssignedValues then
      FStyle.TextGlowColor := AStyle.TextGlowColor
    else
      FStyle.TextGlowColor := AOwnerStyle.TextGlowColor;
    if FStyle.TextGlowColor = dxacDefault then
      FStyle.TextGlowColor := GetTextGlowColor;
  end;

begin
  MergeStyles(Item.GetStyle(State), (Item.Layer as TdxCustomMapItemLayer).GetStyle(State));
  Result := FStyle;
end;

function TdxMapItemViewInfo.GetTextColor: TdxAlphaColor;
begin
  Result := LookAndFeelPainter.MapControlMapCustomElementTextColor;
end;

function TdxMapItemViewInfo.GetTextGlowColor: TdxAlphaColor;
begin
  Result := LookAndFeelPainter.MapControlMapCustomElementTextGlowColor;
end;

procedure TdxMapItemViewInfo.ResetRenderCache;
begin
  FCachedRenderScale := dxNullPointDouble;
  FCachedRenderOffset := dxNullPointDouble;
end;

{ TdxMapItem }

constructor TdxMapItem.Create(AOwner: TComponent);
begin
  inherited;
  FStyle := TdxMapItemStyle.Create(Self);
  FStyle.OnChanged := StyleChanged;
  FStyleHot := TdxMapItemStyle.Create(Self);
  FStyleHot.OnChanged := StyleChanged;
  FStyleSelected := TdxMapItemStyle.Create(Self);
  FStyleSelected.OnChanged := StyleChanged;
  FViewInfo := CreateViewInfo;
  FVisible := True;
end;

destructor TdxMapItem.Destroy;
begin
  GetMapControl(Layer.GetParentComponent).Deselect(Self);
  FreeAndNil(FViewInfo);
  FreeAndNil(FStyleSelected);
  FreeAndNil(FStyleHot);
  FreeAndNil(FStyle);
  inherited;
end;

procedure TdxMapItem.SetParentComponent(Value: TComponent);
begin
  FLayer := Value as TdxMapLayer;
  inherited SetParentComponent(Value);
end;

procedure TdxMapItem.Assign(Source: TPersistent);
begin
  if Source is TdxMapItem then
  begin
    Collection.BeginUpdate;
    try
      DoAssign(Source);
    finally
      Collection.EndUpdate;
    end;
  end
  else
    inherited Assign(Source);
end;

function TdxMapItem.GetSelected: Boolean;
begin
  Result := ViewInfo.State = mcesSelected;
end;

function TdxMapItem.GetStyle(AState: TdxMapControlElementState): TdxMapItemStyle;
begin
  case AState of
    mcesNormal:
      Result := Style;
    mcesSelected:
      Result := StyleSelected
  else  // mcesHot
    Result := StyleHot;
  end;
end;

function TdxMapItem.CreateViewInfo: TdxMapItemViewInfo;
begin
  Result := TdxMapItemViewInfo.Create(Self);
end;

procedure TdxMapItem.DoAssign(Source: TPersistent);
begin
  if Source is TdxMapItem then
  begin
  //  AllowHotTrack := TdxMapItem(Source).AllowHotTrack;
    Style := TdxMapItem(Source).Style;
    StyleHot := TdxMapItem(Source).StyleHot;
    StyleSelected := TdxMapItem(Source).StyleSelected;
    Visible := TdxMapItem(Source).Visible;
  end;
end;

function TdxMapItem.GetCollectionFromParent(
  AParent: TComponent): TcxComponentCollection;
begin
  Result := (AParent as TdxCustomMapItemLayer).MapItems;
end;

procedure TdxMapItem.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if not (csDestroying in ComponentState) and (Operation = opRemove) then
  begin
    if AComponent = FScreenTip then
      FScreenTip := nil;
  end;
end;

procedure TdxMapItem.SelectionChanged(AShift: TShiftState);
begin
  GetMapControl(Layer.GetParentComponent).Select(Self, AShift);
end;

procedure TdxMapItem.SetSelected(const Value: Boolean);
begin
  if Value then
    GetMapControl(Layer.GetParentComponent).Select(Self)
  else
    GetMapControl(Layer.GetParentComponent).Deselect(Self);
end;

procedure TdxMapItem.SetStyle(Value: TdxMapItemStyle);
begin
  FStyle.Assign(Value);
end;

procedure TdxMapItem.SetStyleHot(
  const Value: TdxMapItemStyle);
begin
  FStyleHot.Assign(Value);
end;

procedure TdxMapItem.SetStyleSelected(
  const Value: TdxMapItemStyle);
begin
  FStyleSelected.Assign(Value);
end;

procedure TdxMapItem.SetVisible(const Value: Boolean);
begin
  if FVisible <> Value then
  begin
    FVisible := Value;
    Changed(False);
  end;
end;

procedure TdxMapItem.StyleChanged(ASender: TObject);
begin
  Changed(False);
end;

{ TdxMapItems }

function TdxMapItems.Add(
  AMapItemClass: TdxMapItemClass): TdxMapItem;
begin
  Result := AMapItemClass.Create(ParentComponent.Owner);
  Result.SetParentComponent(ParentComponent);
  SetItemName(Result);
end;

procedure TdxMapItems.Assign(Source: TPersistent);
var
  I: Integer;
  AItem: TdxMapItem;
begin
  if Source is TdxMapItems then
  begin
    BeginUpdate;
    try
      Clear;
      for I := 0 to TdxMapItems(Source).Count - 1 do
      begin
        AItem := TdxMapItems(Source).Items[I];
        Add(TdxMapItemClass(AItem.ClassType)).Assign(AItem);
      end;
    finally
      EndUpdate;
    end;
  end
  else
    inherited Assign(Source);
end;

function TdxMapItems.GetItemPrefixName: string;
begin
  Result := 'TdxMap';
end;

procedure TdxMapItems.SetItemName(AItem: TcxComponentCollectionItem);
begin
  if csDesigning in ParentComponent.ComponentState then
    inherited SetItemName(AItem);
end;

function TdxMapItems.GetItem(Index: Integer): TdxMapItem;
begin
  Result := inherited GetItem(Index) as TdxMapItem;
end;

procedure TdxMapItems.SetItem(Index: Integer;
  const Value: TdxMapItem);
begin
  inherited SetItem(Index, Value);
end;

{ TdxMapControlMapSinglePointItem }

constructor TdxMapSinglePointItem.Create(AOwner: TComponent);
begin
  inherited;
  FLocation := TdxMapControlGeoLocation.Create(Self);
  FLocation.OnChanged := LocationChanged;
end;

destructor TdxMapSinglePointItem.Destroy;
begin
  FreeAndNil(FLocation);
  inherited;
end;

procedure TdxMapSinglePointItem.DoAssign(Source: TPersistent);
begin
  inherited DoAssign(Source);
  if Source is TdxMapSinglePointItem then
    Location := TdxMapSinglePointItem(Source).Location;
end;

procedure TdxMapSinglePointItem.LocationChanged(ASender: TObject);
begin
  ViewInfo.FIsCachedBoundsValid := False;
  Changed(False);
end;

procedure TdxMapSinglePointItem.SetLocation(
  Value: TdxMapControlGeoLocation);
begin
  FLocation.Assign(Value);
end;

{ TdxMapControlMapDotViewInfo }

procedure TdxMapDotViewInfo.CalculateScreenBounds;
var
  ALocation: TdxPointDouble;
begin
  ALocation := Item.Layer.MapUnitToScreenPoint(FMapLocation);
  Bounds := dxRectDoubleToRect(dxRectDoubleOffset(FRelativeBounds, ALocation));
end;

procedure TdxMapDotViewInfo.DoCalculateCachedBounds;
begin
  FMapLocation := Item.Layer.GeoPointToMapUnit(MapDot.Location.GeoPoint);
  FRadius := MapDot.Size / 2.0;
  FRelativeBounds := dxRectDouble(-FRadius, -FRadius, MapDot.Size, MapDot.Size);
end;

function TdxMapDotViewInfo.DoCanBeVisible: Boolean;
begin
  Result := (MapDot.Size >= 1) and
    cxRectIntersect(cxRect(Item.Layer.ViewportInPixels), Bounds);
end;

procedure TdxMapDotViewInfo.DoPaint;
var
  AStyle: TdxMapItemStyle;
begin
  AStyle := Style;
  case MapDot.ShapeKind  of
    mcskCircle:
      dxGPPaintCanvas.Ellipse(Bounds, AStyle.BorderColor, AStyle.Color, AStyle.BorderWidth)
  else // mcskRectangle
     dxGPPaintCanvas.Rectangle(Bounds, AStyle.BorderColor, AStyle.Color, AStyle.BorderWidth);
  end;
end;

function TdxMapDotViewInfo.GetMapDot: TdxMapDot;
begin
  Result := Item as TdxMapDot;
end;

{ TdxMapControlMapDot }

constructor TdxMapDot.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FSize := 5;
end;

function TdxMapDot.CreateViewInfo: TdxMapItemViewInfo;
begin
  Result := TdxMapDotViewInfo.Create(Self);
end;

procedure TdxMapDot.DoAssign(Source: TPersistent);
begin
  inherited DoAssign(Source);
  if Source is TdxMapDot then
  begin
    ShapeKind := TdxMapDot(Source).ShapeKind;
    Size := TdxMapDot(Source).Size;
  end;
end;

procedure TdxMapDot.SetShapeKind(
  Value: TdxMapDotShapeKind);
begin
  if FShapeKind <> Value then
  begin
    FShapeKind := Value;
    Changed(False);
  end;
end;

procedure TdxMapDot.SetSize(Value: Integer);
begin
  if FSize <> Value then
  begin
    FSize := Value;
    Changed(False);
  end;
end;

{ TdxMapControlMapRectangleViewInfo }

procedure TdxMapRectangleViewInfo.CalculateScreenBounds;
var
  APoint1, APoint2: TPoint;
begin
  APoint1 := dxPointDoubleToPoint(Item.Layer.MapUnitToScreenPoint(FMapPoint1));
  APoint2 := dxPointDoubleToPoint(Item.Layer.MapUnitToScreenPoint(FMapPoint2));
  Bounds := cxRect(APoint1, APoint2);
end;

procedure TdxMapRectangleViewInfo.DoCalculateCachedBounds;
var
  AGeoPoint2: TdxMapControlGeoPoint;
  AItem: TdxMapRectangle;
  AItemGeoSize: TdxSizeDouble;
begin
  AItem := Item as TdxMapRectangle;
  FMapPoint1 := Item.Layer.GeoPointToMapUnit(AItem.Location.GeoPoint);
  AItemGeoSize := Item.Layer.KilometersToGeoSize(AItem.Location.GeoPoint, dxSizeDouble(AItem.Width, AItem.Height));
  AGeoPoint2 := dxMapControlGeoPoint(AItem.Location.Latitude - AItemGeoSize.Height, AItem.Location.Longitude + AItemGeoSize.Width);
  FMapPoint2 := Item.Layer.GeoPointToMapUnit(AGeoPoint2);
  FMapBounds := dxRectDouble(FMapPoint1.X, FMapPoint1.Y, FMapPoint2.X - FMapPoint1.X, FMapPoint2.Y - FMapPoint1.Y);
end;

function TdxMapRectangleViewInfo.DoCanBeVisible: Boolean;
begin
  Result := (FMapBounds.Width * Item.Layer.RenderScale.X > 1) and
    (FMapBounds.Height * Item.Layer.RenderScale.Y > 1) and
    dxRectDoubleIntersect(Item.Layer.ViewPort, FMapBounds);
end;

procedure TdxMapRectangleViewInfo.DoPaint;
var
  AStyle: TdxMapItemStyle;
begin
  AStyle := Style;
  dxGPPaintCanvas.Rectangle(Bounds, AStyle.BorderColor, AStyle.Color, AStyle.BorderWidth);
end;

function TdxMapRectangleViewInfo.IsBoundsScalable: Boolean;
begin
  Result := True;
end;

{ TdxMapControlMapRectangle }

function TdxMapRectangle.CreateViewInfo: TdxMapItemViewInfo;
begin
  Result := TdxMapRectangleViewInfo.Create(Self);
end;

procedure TdxMapRectangle.DoAssign(Source: TPersistent);
begin
  inherited DoAssign(Source);
  if Source is TdxMapRectangle then
  begin
    Height := TdxMapRectangle(Source).Height;
    Width := TdxMapRectangle(Source).Width;
  end;
end;

procedure TdxMapRectangle.SetHeight(Value: Double);
begin
  if FHeight <> Value then
  begin
    FHeight := Value;
    Changed(False);
  end;
end;

procedure TdxMapRectangle.SetWidth(Value: Double);
begin
  if FWidth <> Value then
  begin
    FWidth := Value;
    Changed(False);
  end;
end;

{ TdxMapControlMapEllipseViewInfo }

procedure TdxMapEllipseViewInfo.DoPaint;
var
  AStyle: TdxMapItemStyle;
begin
  AStyle := Style;
  dxGPPaintCanvas.Ellipse(Bounds, AStyle.BorderColor, AStyle.Color, AStyle.BorderWidth);
end;

function TdxMapEllipseViewInfo.PtInElement(
  const APoint: TPoint): Boolean;
var
  ACenter: TPoint;
begin
  ACenter := cxRectCenter(Bounds);
  Result := Sqr(APoint.X - ACenter.X) / Sqr(cxRectWidth(Bounds) / 2) +
    Sqr(APoint.Y - ACenter.Y) / Sqr(cxRectHeight(Bounds) / 2) <= 1;
end;

{ TdxMapControlMapEllipse }

function TdxMapEllipse.CreateViewInfo: TdxMapItemViewInfo;
begin
  Result := TdxMapEllipseViewInfo.Create(Self);
end;

{ TdxMapControlGeoPointItem }

procedure TdxMapControlGeoPointItem.Assign(Source: TPersistent);
begin
  if Source is TdxMapControlGeoPointItem then
  begin
    Collection.BeginUpdate;
    try
      GeoPoint := TdxMapControlGeoPointItem(Source).GeoPoint;
    finally
      Collection.EndUpdate;
    end;
  end
  else
    inherited Assign(Source);
end;

function TdxMapControlGeoPointItem.GetLatitude: Double;
begin
  Result := FGeoPoint.Latitude;
end;

function TdxMapControlGeoPointItem.GetLongitude: Double;
begin
  Result := FGeoPoint.Longitude;
end;

procedure TdxMapControlGeoPointItem.SetGeoPoint(
  const Value: TdxMapControlGeoPoint);
begin
  if not dxMapControlGeoPointIsEqual(FGeoPoint, Value) then
  begin
    FGeoPoint := Value;
    Changed(False);
  end;
end;

procedure TdxMapControlGeoPointItem.SetLatitude(const Value: Double);
begin
  if FGeoPoint.Latitude <> Value then
  begin
    FGeoPoint.Latitude := Value;
    Changed(False);
  end;
end;

procedure TdxMapControlGeoPointItem.SetLongitude(const Value: Double);
begin
  if FGeoPoint.Longitude <> Value then
  begin
    FGeoPoint.Longitude := Value;
    Changed(False);
  end;
end;

{ TdxMapControlGeoPointCollection }

function TdxMapControlGeoPointCollection.Add: TdxMapControlGeoPointItem;
begin
  Result := inherited Add as TdxMapControlGeoPointItem;
end;

procedure TdxMapControlGeoPointCollection.Update(Item: TCollectionItem);
begin
  if Assigned(FOnChanged) then
    FOnChanged(Self);
end;

function TdxMapControlGeoPointCollection.GetItem(
  Index: Integer): TdxMapControlGeoPointItem;
begin
  Result := TdxMapControlGeoPointItem(inherited Items[Index]);
end;

procedure TdxMapControlGeoPointCollection.SetItem(Index: Integer;
  const Value: TdxMapControlGeoPointItem);
begin
  inherited Items[Index] := Value;
end;

{ TdxMapControlMapPolylineViewInfo }

function TdxMapPolylineViewInfo.IsIntersect(const ARect: TRect): Boolean;
var
  AGpCanvas: TdxGPGraphics;
  APath: TdxGPPath;
begin
  Result := inherited IsIntersect(ARect);
  if Result then
  begin
    APath := CreatePath;
    try
      if APath <> nil then
      begin
        AGpCanvas := TdxGPGraphics.Create(cxScreenCanvas.Handle);
        try
          AGpCanvas.SetClipPath(APath, gmReplace);
          Result := dxGpIsRectVisible(AGpCanvas.Handle, ARect);
        finally
          cxScreenCanvas.Dormant;
          AGpCanvas.Free;
        end;
      end;
    finally
      APath.Free;
    end;
  end;
end;

procedure TdxMapPolylineViewInfo.CalculateScreenBounds;
var
  I: Integer;
  ABounds: TRect;
  AIsOffsetOnly: Boolean;
begin
  ABounds := cxRect(MaxInt, MaxInt, -MaxInt, -MaxInt);
  AIsOffsetOnly := dxPointDoubleIsEqual(FCachedRenderScale, Item.Layer.RenderScale);
  for I := 0 to High(FScreenPoints) do
  begin
    if AIsOffsetOnly then
      FScreenPoints[I] := dxPointDoubleToPoint(dxPointDoubleOffset(dxPointDouble(FScreenPoints[I]),
        dxPointDoubleOffset(FCachedRenderOffset, Item.Layer.RenderOffset, False)))
    else
      FScreenPoints[I] := dxPointDoubleToPoint(Item.Layer.MapUnitToScreenPoint(FMapPoints[I]));
    ABounds.Left := Min(ABounds.Left, FScreenPoints[I].X);
    ABounds.Top := Min(ABounds.Top, FScreenPoints[I].Y);
    ABounds.Right := Max(ABounds.Right, FScreenPoints[I].X);
    ABounds.Bottom := Max(ABounds.Bottom, FScreenPoints[I].Y);
  end;
  Bounds := ABounds;
end;

function TdxMapPolylineViewInfo.CanAddPoint: Boolean;
begin
  Result := False;
end;

function TdxMapPolylineViewInfo.CreatePath: TdxGpPath;
begin
  Result := TdxGPPath.Create;
  Result.AddPolyline(FScreenPoints);
end;

procedure TdxMapPolylineViewInfo.DoCalculateCachedBounds;
var
  I: Integer;
  ALeft, ATop, ARigth, ABottom : Double;
begin
  ALeft := 1;
  ATop := 1;
  ARigth := 0;
  ABottom := 0;
  SetLength(FMapPoints, (Item as TdxMapPolyline).GeoPoints.Count);
  if Length(FMapPoints) < 2 then
    SetLength(FMapPoints, 0);
  for I := 0 to High(FMapPoints) do
  begin
    FMapPoints[I] := Item.Layer.GeoPointToMapUnit((Item as TdxMapPolyline).GeoPoints[I].GeoPoint);
    ALeft := Min(ALeft, FMapPoints[I].X);
    ATop := Min(ATop, FMapPoints[I].Y);
    ARigth := Max(ARigth, FMapPoints[I].X);
    ABottom := Max(ABottom, FMapPoints[I].Y);
  end;
  if (Length(FMapPoints) = 2) and CanAddPoint then
  begin
    SetLength(FMapPoints, 3);
    FMapPoints[2] := FMapPoints[0];
  end;
  FMapBounds := dxRectDouble(ALeft, ATop, ARigth - ALeft, ABottom - ATop);
  SetLength(FScreenPoints, Length(FMapPoints));
  inherited;
end;

function TdxMapPolylineViewInfo.DoCanBeVisible: Boolean;
begin
  Result := (Length(FScreenPoints) > 0) and
    cxRectIntersect(cxRect(Item.Layer.ViewportInPixels), Bounds);
end;

procedure TdxMapPolylineViewInfo.DoPaint;
var
  AStyle: TdxMapItemStyle;
begin
  if Length(FScreenPoints) > 0 then
  begin
    AStyle := Style;
    dxGPPaintCanvas.Polyline(FScreenPoints, AStyle.BorderColor, AStyle.BorderWidth);
  end;
end;

function TdxMapPolylineViewInfo.IsBoundsScalable: Boolean;
begin
  Result := True;
end;

function TdxMapPolylineViewInfo.NeedCalculateScreenBoundsToDetermineVisibility: Boolean;
begin
  Result := True;
end;

function TdxMapPolylineViewInfo.PtInElement(
  const APoint: TPoint): Boolean;
var
  APath: TdxGPPath;
begin
  Result := PtInRect(Bounds, APoint);
  if Result then
  begin
    APath := CreatePath;
    try
      if APath <> nil then
        Result := APath.IsPointInPathOutline(APoint, Style.BorderWidth);
    finally
      FreeAndNil(APath);
    end;
  end;
end;

{ TdxMapControlPolyline }

constructor TdxMapPolyline.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FGeoPoints := TdxMapControlGeoPointCollection.Create(TdxMapControlGeoPointItem);
  FGeoPoints.OnChanged := GeoPointsChanged;
end;

destructor TdxMapPolyline.Destroy;
begin
  FreeAndNil(FGeoPoints);
  inherited;
end;

function TdxMapPolyline.CreateViewInfo: TdxMapItemViewInfo;
begin
  Result := TdxMapPolylineViewInfo.Create(Self);
end;

procedure TdxMapPolyline.DoAssign(Source: TPersistent);
begin
  inherited DoAssign(Source);
  if Source is TdxMapPolyline then
    GeoPoints := TdxMapPolyline(Source).GeoPoints;
end;

procedure TdxMapPolyline.GeoPointsChanged(Sender: TObject);
begin
  ViewInfo.FIsCachedBoundsValid := False;
  Changed(False);
end;

procedure TdxMapPolyline.SetGeoPoints(
  const Value: TdxMapControlGeoPointCollection);
begin
  FGeoPoints.Assign(Value);
end;

{ TdxMapControlMapPolygonViewInfo }

function TdxMapPolygonViewInfo.CanAddPoint: Boolean;
begin
  Result := True;
end;

function TdxMapPolygonViewInfo.CreatePath: TdxGpPath;
begin
  Result := TdxGPPath.Create;
  Result.AddPolygon(FScreenPoints);
end;

function TdxMapPolygonViewInfo.DoCanBeVisible: Boolean;
begin
  Result := (Length(FScreenPoints) > 0) and (FMapBounds.Width * Item.Layer.RenderScale.X > 1) and
    (FMapBounds.Height * Item.Layer.RenderScale.Y > 1) and
    dxRectDoubleIntersect(Item.Layer.ViewPort, FMapBounds);
end;

procedure TdxMapPolygonViewInfo.DoPaint;
var
  AStyle: TdxMapItemStyle;
begin
  if Length(FScreenPoints) > 0 then
  begin
    AStyle := Style;
    dxGPPaintCanvas.Polygon(FScreenPoints, AStyle.BorderColor, AStyle.Color, AStyle.BorderWidth);
  end;
end;

function TdxMapPolygonViewInfo.NeedCalculateScreenBoundsToDetermineVisibility: Boolean;
begin
  Result := False;
end;

function TdxMapPolygonViewInfo.PtInElement(
  const APoint: TPoint): Boolean;
var
  APath: TdxGPPath;
begin
  Result := PtInRect(Bounds, APoint);
  if Result then
  begin
    APath := CreatePath;
    try
      if APath <> nil then
        Result := APath.IsPointInPath(APoint);
    finally
      FreeAndNil(APath);
    end;
  end;
end;

{ TdxMapControlMapPolygon }

function TdxMapPolygon.CreateViewInfo: TdxMapItemViewInfo;
begin
  Result := TdxMapPolygonViewInfo.Create(Self);
end;

{ TdxMapControlMapPathSegmentViewInfo }

constructor TdxMapPathSegmentViewInfo.Create(
  AOwner: TdxMapPathSegment);
begin
  inherited Create(AOwner.Collection.Owner as TdxMapItem);
  FSegment := AOwner;
end;

procedure TdxMapPathSegmentViewInfo.AddToPath(APath: TdxGPPath);
begin
  APath.AddPolygon(FScreenPoints);
end;

procedure TdxMapPathSegmentViewInfo.CalculateScreenBounds;
var
  I: Integer;
  ABounds: TRect;
begin
  ABounds := cxRect(MaxInt, MaxInt, -MaxInt, -MaxInt);
  for I := 0 to High(FScreenPoints) do
  begin
    FScreenPoints[I] := dxPointDoubleToPoint(Item.Layer.MapUnitToScreenPoint(FMapPoints[I]));
    ABounds.Left := Min(ABounds.Left, FScreenPoints[I].X);
    ABounds.Top := Min(ABounds.Top, FScreenPoints[I].Y);
    ABounds.Right := Max(ABounds.Right, FScreenPoints[I].X);
    ABounds.Bottom := Max(ABounds.Bottom, FScreenPoints[I].Y);
  end;
  Bounds := ABounds;
end;

procedure TdxMapPathSegmentViewInfo.DoCalculateCachedBounds;
var
  I: Integer;
  ALeft, ATop, ARight, ABottom : Double;
begin
  ALeft := 1;
  ATop := 1;
  ARight := 0;
  ABottom := 0;
  SetLength(FMapPoints, Segment.GeoPoints.Count);
  if Length(FMapPoints) < 2 then
    SetLength(FMapPoints, 0);
  for I := 0 to High(FMapPoints) do
  begin
    FMapPoints[I] := Item.Layer.GeoPointToMapUnit(Segment.GeoPoints[I].GeoPoint);
    ALeft := Min(ALeft, FMapPoints[I].X);
    ATop := Min(ATop, FMapPoints[I].Y);
    ARight := Max(ARight, FMapPoints[I].X);
    ABottom := Max(ABottom, FMapPoints[I].Y);
  end;
  if Length(FMapPoints) = 2 then
  begin
    SetLength(FMapPoints, 3);
    FMapPoints[2] := FMapPoints[0];
  end;
  FMapBounds := dxRectDouble(ALeft, ATop, ARight - ALeft, ABottom - ATop);
  SetLength(FScreenPoints, Length(FMapPoints));
  inherited;
end;

function TdxMapPathSegmentViewInfo.DoCanBeVisible: Boolean;
begin
  Result := (Length(FScreenPoints) > 0) and (FMapBounds.Width * Item.Layer.RenderScale.X > 1) and
    (FMapBounds.Height * Item.Layer.RenderScale.Y > 1) and
    dxRectDoubleIntersect(Item.Layer.ViewPort, FMapBounds);
end;

function TdxMapPathSegmentViewInfo.GetHitTestIndex: Integer;
begin
  Result := mchtNone;
end;

function TdxMapPathSegmentViewInfo.IsBoundsScalable: Boolean;
begin
  Result := True;
end;

{ TdxMapControlMapPathSegment }

constructor TdxMapPathSegment.Create(Collection: TCollection);
begin
  inherited Create(Collection);
  FGeoPoints := TdxMapControlGeoPointCollection.Create(TdxMapControlGeoPointItem);
  FGeoPoints.OnChanged := GeoPointsChanged;
  FViewInfo := TdxMapPathSegmentViewInfo.Create(Self);
  FVisible := True;
end;

destructor TdxMapPathSegment.Destroy;
begin
  FreeAndNil(FViewInfo);
  FreeAndNil(FGeoPoints);
  inherited;
end;

procedure TdxMapPathSegment.Assign(Source: TPersistent);
begin
  if Source is TdxMapPathSegment then
  begin
    Collection.BeginUpdate;
    try
      GeoPoints := TdxMapPathSegment(Source).GeoPoints;
      Visible := TdxMapPathSegment(Source).Visible;
    finally
      Collection.EndUpdate;
    end;
  end
  else
    inherited Assign(Source);
end;

procedure TdxMapPathSegment.GeoPointsChanged(Sender: TObject);
begin
  ViewInfo.FIsCachedBoundsValid := False;
  Changed(False);
end;

procedure TdxMapPathSegment.SetGeoPoints(
  const Value: TdxMapControlGeoPointCollection);
begin
  FGeoPoints.Assign(Value);
end;

procedure TdxMapPathSegment.SetVisible(const Value: Boolean);
begin
  if FVisible <> Value then
  begin
    FVisible := Value;
    Changed(False);
  end;
end;

{ TdxMapControlMapPathSegments }

function TdxMapPathSegments.Add: TdxMapPathSegment;
begin
  Result := inherited Add as TdxMapPathSegment;
end;

procedure TdxMapPathSegments.Update(Item: TCollectionItem);
begin
  if Assigned(FOnChanged) then
    FOnChanged(Self);
end;

function TdxMapPathSegments.GetItem(
  Index: Integer): TdxMapPathSegment;
begin
  Result := inherited Items[Index] as TdxMapPathSegment;
end;

procedure TdxMapPathSegments.SetItem(Index: Integer;
  const Value: TdxMapPathSegment);
begin
  inherited Items[Index] := Value;
end;

{ TdxMapControlMapPathViewInfo }

procedure TdxMapPathViewInfo.CalculateBounds;
var
  I: Integer;
  ABorderWidth: Integer;
begin
  Bounds := cxInvalidRect;
  if Count > 0 then
  begin
    SegmentInfo[0].CalculateBounds;
    Bounds := SegmentInfo[0].Bounds;
    for I := 1 to Count - 1 do
    begin
      SegmentInfo[I].CalculateBounds;
      Bounds := cxRectUnion(Bounds, SegmentInfo[I].Bounds);
    end;
    ABorderWidth := Style.BorderWidth;
    Bounds := cxRectInflate(Bounds, ABorderWidth, ABorderWidth);
  end;
end;

function TdxMapPathViewInfo.IsIntersect(const ARect: TRect): Boolean;
var
  AGpCanvas: TdxGPGraphics;
  APath: TdxGPPath;
begin
  Result := inherited IsIntersect(ARect);
  if Result then
  begin
    APath := CreatePath;
    try
      if APath <> nil then
      begin
        AGpCanvas := TdxGPGraphics.Create(cxScreenCanvas.Handle);
        try
          AGpCanvas.SetClipPath(APath, gmReplace);
          Result := dxGpIsRectVisible(AGpCanvas.Handle, ARect);
        finally
          cxScreenCanvas.Dormant;
          AGpCanvas.Free;
        end;
      end;
    finally
      APath.Free;
    end;
  end;
end;

procedure TdxMapPathViewInfo.AddVisibleElements;
var
  I: Integer;
begin
  for I := 0 to MapPath.Segments.Count - 1 do
    if MapPath.Segments[I].ViewInfo.CanBeVisible then
      Add(MapPath.Segments[I].ViewInfo);
end;

function TdxMapPathViewInfo.CreatePath: TdxGpPath;
var
  I: Integer;
begin
  Result := nil;
  if Count > 0 then
  begin
    Result := TdxGPPath.Create;
    for I := 0 to Count - 1 do
      SegmentInfo[I].AddToPath(Result);
  end;
end;

procedure TdxMapPathViewInfo.DoPaint;
var
  APath: TdxGPPath;
  AStyle: TdxMapItemStyle;
begin
  APath := CreatePath;
  try
    if APath <> nil then
    begin
      AStyle := Style;
      dxGPPaintCanvas.Path(APath, AStyle.BorderColor, AStyle.Color, AStyle.BorderWidth);
    end;
  finally
    APath.Free;
  end;
end;

function TdxMapPathViewInfo.PtInElement(
  const APoint: TPoint): Boolean;
var
  APath: TdxGPPath;
begin
  Result := PtInRect(Bounds, APoint);
  if Result then
  begin
    APath := CreatePath;
    try
      if APath <> nil then
        Result := APath.IsPointInPath(APoint);
    finally
      APath.Free;
    end;
  end;
end;

function TdxMapPathViewInfo.GetMapPath: TdxMapPath;
begin
  Result := Item as TdxMapPath;
end;

function TdxMapPathViewInfo.GetSegmentInfo(Index: Integer): TdxMapPathSegmentViewInfo;
begin
  Result := Items[Index] as TdxMapPathSegmentViewInfo;
end;

{ TdxMapControlMapPath }

constructor TdxMapPath.Create(AOwner: TComponent);
begin
  inherited;
  FSegments := TdxMapPathSegments.Create(Self, TdxMapPathSegment);
end;

destructor TdxMapPath.Destroy;
begin
  FreeAndNil(FSegments);
  inherited;
end;

function TdxMapPath.CreateViewInfo: TdxMapItemViewInfo;
begin
  Result := TdxMapPathViewInfo.Create(Self);
end;

procedure TdxMapPath.DoAssign(Source: TPersistent);
begin
  inherited DoAssign(Source);
  if Source is TdxMapPath then
    Segments := TdxMapPath(Source).Segments;
end;

procedure TdxMapPath.SetSegments(
  const Value: TdxMapPathSegments);
begin
  FSegments.Assign(Value);
end;

{ TdxMapControlMapPointerViewInfo }

destructor TdxMapPointerViewInfo.Destroy;
begin
  if FCachedImage <> nil then
    GdipDeleteCachedBitmap(FCachedImage);
  inherited;
end;

procedure TdxMapPointerViewInfo.CalculateScreenBounds;
var
  ALocation: TPoint;
begin
  ALocation := dxPointDoubleToPoint(MapPointer.Layer.MapUnitToScreenPoint(FMapLocation));
  FImageRect := cxRectOffset(FRelativeImageRect, ALocation);
  FTextBounds := cxRectOffset(FRelativeTextRect, ALocation);
  Bounds := cxRectOffset(FRelativeBounds, ALocation);
end;

procedure TdxMapPointerViewInfo.DoCalculateCachedBounds;
var
  AGpCanvas: TdxGPGraphics;
begin
  FMapLocation := MapPointer.Layer.GeoPointToMapUnit(MapPointer.Location.GeoPoint);
  FRelativeImageRect := cxNullRect;
  if IsImageVisible then
  begin
    if IsImageAssigned(MapPointer.Image) then
      FRelativeImageRect := MapPointer.Image.ClientRect
    else
      FRelativeImageRect := cxRect(LookAndFeelPainter.MapControlGetMapPushpinSize);
    FRelativeImageRect := cxRectOffset(FRelativeImageRect, dxPointDoubleToPoint(
      dxPointDoubleScale(GetImageOrigin, dxPointDouble(cxRectWidth(FRelativeImageRect), cxRectHeight(FRelativeImageRect)))), False);
  end;
  FRelativeTextRect := cxNullRect;
  if MapPointer.Text <> '' then
  begin
    AGpCanvas := TdxGPGraphics.Create(cxScreenCanvas.Handle);
    try
      dxGPGetTextRect(AGpCanvas, MapPointer.Text, Style.Font, False, cxNullRect, FRelativeTextRect);
    finally
      cxScreenCanvas.Dormant;
      AGpCanvas.Free;
    end;
  end;
  FRelativeTextRect := cxRectSetOrigin(FRelativeTextRect, cxPoint(FRelativeImageRect.Right, FRelativeImageRect.Top));
  FRelativeBounds := cxRectUnion(FRelativeImageRect, FRelativeTextRect);
end;

function TdxMapPointerViewInfo.DoCanBeVisible: Boolean;
begin
  Result := (cxRectWidth(Bounds) > 0) and (cxRectHeight(Bounds) > 0) and
    cxRectIntersect(cxRect(Item.Layer.ViewportInPixels), Bounds);
end;

procedure TdxMapPointerViewInfo.DoPaint;
var
  AStyle: TdxMapItemStyle;
begin
  if IsImageVisible then
    if IsImageAssigned(MapPointer.Image) then
    begin
      if CheckCachedImageCreated(dxGPPaintCanvas) then
        GdipDrawCachedBitmap(dxGPPaintCanvas.Handle, FCachedImage, FImageRect.Left, FImageRect.Top)
      else
        dxGPPaintCanvas.Draw(MapPointer.Image, FImageRect, $FF);
    end
    else
    begin
      CapturePaintCanvas;
      try
        LookAndFeelPainter.DrawMapPushpin(cxPaintCanvas, FImageRect, mcesNormal);
      finally
        ReleasePaintCanvas;
      end;
    end;

  if MapPointer.Text <> '' then
  begin
    AStyle := Style;
    dxGPDrawGlowText(dxGPPaintCanvas, MapPointer.Text, FTextBounds, AStyle.Font,
      AStyle.TextColor, AStyle.TextGlowColor);
  end;
end;

function TdxMapPointerViewInfo.GetImageOrigin: TdxPointDouble;
begin
  if IsImageAssigned(MapPointer.Image) then
    Result := MapPointer.ImageOrigin.Point
  else
    Result := dxPointDouble(0.5, 1); 
end;

function TdxMapPointerViewInfo.PtInElement(
  const APoint: TPoint): Boolean;
begin
  Result := cxRectPtIn(Bounds, APoint);
end;

function TdxMapPointerViewInfo.UsePushpinImageAsDefault: Boolean;
begin
  Result := MapPointer.ImageVisible;
end;

function TdxMapPointerViewInfo.CheckCachedImageCreated(AGpCanvas: TdxGPCanvas): Boolean;

  function CheckRecreated: Boolean;
  begin
    if FCachedImage <> nil then
      GdipDeleteCachedBitmap(FCachedImage);
    Result := GdipCreateCachedBitmap(MapPointer.Image.Handle, AGpCanvas.Handle, FCachedImage) = Ok;
  end;

begin
  Result := IsWinSevenOrLater and
    (FIsCachedImageValid or CheckRecreated);
end;

function TdxMapPointerViewInfo.GetMapPointer: TdxMapPointer;
begin
  Result := Item as TdxMapPointer;
end;

function TdxMapPointerViewInfo.IsImageVisible: Boolean;
begin
  Result := MapPointer.ImageVisible;
end;

{ TdxMapControlMapPointer }

constructor TdxMapPointer.Create(AOwner: TComponent);
begin
  inherited;
  FImage := TdxSmartImage.Create;
  FImage.OnChange := ImageChanged;
  FImageVisible := True;
  FImageOrigin := TdxPointDoublePersistent.Create(Self, 0.5, 0.5);
  FImageOrigin.OnChange := ImageOriginChanged;
end;

destructor TdxMapPointer.Destroy;
begin
  FreeAndNil(FImageOrigin);
  FreeAndNil(FImage);
  inherited;
end;

function TdxMapPointer.CreateViewInfo: TdxMapItemViewInfo;
begin
  Result := TdxMapPointerViewInfo.Create(Self);
end;

procedure TdxMapPointer.DoAssign(Source: TPersistent);
begin
  inherited DoAssign(Source);
  if Source is TdxMapPointer then
  begin
    Image := TdxMapPointer(Source).Image;
    Text := TdxMapPointer(Source).Text;
    ImageOrigin := TdxMapPointer(Source).ImageOrigin;
    ImageVisible := TdxMapPointer(Source).ImageVisible;
  end;
end;

procedure TdxMapPointer.ImageChanged(ASender: TObject);
begin
  ViewInfo.FIsCachedBoundsValid := False;
  (ViewInfo as TdxMapPointerViewInfo).FIsCachedImageValid := False;
  Changed(False);
end;

procedure TdxMapPointer.ImageOriginChanged(ASender: TObject);
begin
  ViewInfo.FIsCachedBoundsValid := False;
  Changed(False);
end;

function TdxMapPointer.IsImageOriginStored: Boolean;
begin
  Result := not FImageOrigin.IsEqual(dxMapPointerDefaultImageOrigin);
end;

procedure TdxMapPointer.SetImage(AValue: TdxSmartImage);
begin
  FImage.Assign(AValue);
end;

procedure TdxMapPointer.SetImageOrigin(Value: TdxPointDoublePersistent);
begin
  FImageOrigin.Assign(Value);
end;

procedure TdxMapPointer.SetImageVisible(const Value: Boolean);
begin
  if FImageVisible <> Value then
  begin
    FImageVisible := Value;
    ViewInfo.FIsCachedBoundsValid := False;
    Changed(False);
  end;
end;

procedure TdxMapPointer.SetText(const Value: string);
begin
  if FText <> Value then
  begin
    FText := Value;
    ViewInfo.FIsCachedBoundsValid := False;
    Changed(False);
  end;
end;

{ TdxMapControlMapCustomItemViewInfo }

procedure TdxMapCustomElementViewInfo.DoCalculateCachedBounds;
begin
  inherited DoCalculateCachedBounds;
  FRelativeBounds := cxRectInflate(FRelativeBounds, LookAndFeelPainter.MapControlMapCustomElementSelectionOffset);
end;

procedure TdxMapCustomElementViewInfo.DoPaint;
begin
  CapturePaintCanvas;
  try
    LookAndFeelPainter.DrawMapCustomElementBackground(cxPaintCanvas, Bounds, State);
  finally
    ReleasePaintCanvas;
  end;
  inherited DoPaint;
end;

{ TdxMapControlMapCustomItem }

function TdxMapCustomElement.CreateViewInfo: TdxMapItemViewInfo;
begin
  Result := TdxMapCustomElementViewInfo.Create(Self);
end;

end.
