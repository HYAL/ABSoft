{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressMapControl                                        }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSMAPCONTROL AND ALL             }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxCustomMapItemLayer;

interface

{$I cxVer.inc}

uses
  Types, Classes, Math, SysUtils, RtlConsts,
  cxGraphics, cxGeometry, cxClasses, dxCoreClasses, dxGDIPlusClasses, cxLookAndFeelPainters,
  dxMapControlTypes, dxMapLayer, dxMapControlElementViewInfo,
  dxMapControlProjections, dxMapItem, dxMapItemStyle;

type
  TdxCustomMapItemLayer = class;

  TdxMapItemLayerViewInfo = class(TdxMapLayerViewInfo)
  private
    FLastNonPointerItemIndex: Integer;
    FSelectedElements: TdxFastObjectList;
  protected
    procedure AddVisibleElements; override;
  public
    constructor Create(AOwner: TdxMapLayer); override;
    destructor Destroy; override;
    procedure CalculateSize; override;
    procedure Paint(ACanvas: TcxCanvas); override;
  end;

  TdxCustomMapItemLayer = class(TdxMapLayer)
  private
    FAllowHotTrack: Boolean;
    FMapItems: TdxMapItems;
    FProjection: TdxMapControlCustomProjection;
    FProjectionClass: TdxMapControlCustomProjectionClass;
    FInitialMapSize: TcxSize;
    FItemStyleSelected: TdxMapItemStyle;
    FItemStyle: TdxMapItemStyle;
    FItemStyleHot: TdxMapItemStyle;
    function GetProjectionClassName: string;
    function GetProjectionClass: TdxMapControlCustomProjectionClass;
    procedure InitialMapSizeChanged(ASender: TObject);
    function IsInitialMapSizeStored: Boolean;
    procedure ItemStyleChanged(ASender: TObject);
    procedure SetInitialMapSize(AValue: TcxSize);
    procedure SetItemStyle(const Value: TdxMapItemStyle);
    procedure SetItemStyleHot(const Value: TdxMapItemStyle);
    procedure SetItemStyleSelected(const Value: TdxMapItemStyle);
    procedure SetMapItems(const Value: TdxMapItems);
    procedure SetProjection(const Value: TdxMapControlCustomProjection);
    procedure SetProjectionClassName(const Value: string);
    procedure SetProjectionClass(const Value: TdxMapControlCustomProjectionClass);
  protected
    function CreateViewInfo: TdxMapLayerViewInfo; override;
    procedure DoAssign(Source: TPersistent); override;
    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;
    function GetMapProjection: TdxMapControlCustomProjection; override;
    function GetMapSizeInPixels(AZoomLevel: Double): TdxSizeDouble; override;
    procedure MapItemsChanged(Sender: TObject; AItem: TcxComponentCollectionItem;
      AAction: TcxComponentCollectionNotification); virtual;
    procedure ProjectionChanged(Sender: TObject); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function AddItem(AMapItemClass: TdxMapItemClass): TdxMapItem;
    procedure GetItemsInRegion(const ARect: TRect; AItems: TList);
    function GetStyle(AState: TdxMapControlElementState): TdxMapItemStyle;

    property AllowHotTrack: Boolean read FAllowHotTrack write FAllowHotTrack default True;
    property InitialMapSize: TcxSize read FInitialMapSize write SetInitialMapSize stored IsInitialMapSizeStored;
    property ItemStyle: TdxMapItemStyle read FItemStyle write SetItemStyle;
    property ItemStyleHot: TdxMapItemStyle read FItemStyleHot write SetItemStyleHot;
    property ItemStyleSelected: TdxMapItemStyle read FItemStyleSelected write SetItemStyleSelected;
    property MapItems: TdxMapItems read FMapItems write SetMapItems;
    property ProjectionClass: TdxMapControlCustomProjectionClass read GetProjectionClass write SetProjectionClass;
  published
    property ProjectionClassName: string read GetProjectionClassName write SetProjectionClassName;
    property Projection: TdxMapControlCustomProjection read FProjection write SetProjection;
  end;

implementation

uses
  dxMapControl;

{ TdxMapControlItemsLayerViewInfo }

constructor TdxMapItemLayerViewInfo.Create(AOwner: TdxMapLayer);
begin
  inherited Create(AOwner);
  FSelectedElements := TdxFastObjectList.Create(False);
end;

destructor TdxMapItemLayerViewInfo.Destroy;
begin
  FreeAndNil(FSelectedElements);
  inherited;
end;

procedure TdxMapItemLayerViewInfo.CalculateSize;
begin
// do nothing
end;

procedure TdxMapItemLayerViewInfo.Paint(ACanvas: TcxCanvas);
var
  I: Integer;
  AItemHot: TdxMapControlElementViewInfo;
begin
  FSelectedElements.Clear;
  AItemHot := nil;
  for I := 0 to Count - 1 do
  begin
    if Items[I].State = mcesSelected then
      FSelectedElements.Add(Items[I])
    else
      if Items[I].State = mcesHot then
        AItemHot := Items[I]
      else
        Items[I].Paint(ACanvas);
  end;
  for I := 0 to FSelectedElements.Count - 1 do
    (FSelectedElements[I] as TdxMapControlElementViewInfo).Paint(ACanvas);
  if AItemHot <> nil then
    AItemHot.Paint(ACanvas);
end;

procedure TdxMapItemLayerViewInfo.AddVisibleElements;
var
  I: Integer;
  ALayer: TdxCustomMapItemLayer;
  AMapItemViewInfo: TdxMapItemViewInfo;
begin
  ALayer := Layer as TdxCustomMapItemLayer;
  FLastNonPointerItemIndex := -1;
  for I := 0 to ALayer.MapItems.Count - 1 do
  begin
    AMapItemViewInfo := ALayer.MapItems[I].ViewInfo;
    if AMapItemViewInfo.CanBeVisible then
    begin
      if ALayer.MapItems[I] is TdxMapPointer then
        Add(AMapItemViewInfo)
      else
      begin
        Inc(FLastNonPointerItemIndex);
        Insert(FLastNonPointerItemIndex , AMapItemViewInfo)
      end;
    end;
  end;
end;

{ TdxCustomMapItemLayer }

constructor TdxCustomMapItemLayer.Create;
begin
  inherited;
  FAllowHotTrack := True;
  FMapItems := TdxMapItems.Create(Self, TdxMapItem);
  FMapItems.OnChange := MapItemsChanged;
  FInitialMapSize := TcxSize.Create(Self, dxMapControlDefaultMapSize, dxMapControlDefaultMapSize);
  FInitialMapSize.OnChange := InitialMapSizeChanged;
  FItemStyle := TdxMapItemStyle.Create(Self);
  FItemStyle.OnChanged := ItemStyleChanged;
  FItemStyleHot := TdxMapItemStyle.Create(Self);
  FItemStyleHot.OnChanged := ItemStyleChanged;
  FItemStyleSelected := TdxMapItemStyle.Create(Self);
  FItemStyleSelected.OnChanged := ItemStyleChanged;
  ProjectionClass := TdxMapControlSphericalMercatorProjection;
end;

destructor TdxCustomMapItemLayer.Destroy;
begin
  FreeAndNil(FProjection);
  FreeAndNil(FItemStyleSelected);
  FreeAndNil(FItemStyleHot);
  FreeAndNil(FItemStyle);
  FreeAndNil(FInitialMapSize);
  FreeAndNil(FMapItems);
  inherited;
end;

procedure TdxCustomMapItemLayer.DoAssign(Source: TPersistent);
begin
  inherited DoAssign(Source);
  if Source is TdxCustomMapItemLayer then
  begin
    MapItems := TdxCustomMapItemLayer(Source).MapItems;
    AllowHotTrack := TdxCustomMapItemLayer(Source).AllowHotTrack;
    InitialMapSize := TdxCustomMapItemLayer(Source).InitialMapSize;
    ItemStyle := TdxCustomMapItemLayer(Source).ItemStyle;
    ItemStyleHot := TdxCustomMapItemLayer(Source).ItemStyleHot;
    ItemStyleSelected := TdxCustomMapItemLayer(Source).ItemStyleSelected;
    ProjectionClassName := TdxCustomMapItemLayer(Source).ProjectionClassName;
    Projection := TdxCustomMapItemLayer(Source).Projection;
  end;
end;

function TdxCustomMapItemLayer.AddItem(AMapItemClass: TdxMapItemClass): TdxMapItem;
begin
  Result := FMapItems.Add(AMapItemClass);
end;

procedure TdxCustomMapItemLayer.GetItemsInRegion(const ARect: TRect; AItems: TList);
var
  I: Integer;
begin
  for I := 0 to ViewInfo.Count - 1 do
  begin
    if (ViewInfo[I] as TdxMapItemViewInfo).IsIntersect(ARect) then
      AItems.Add(TdxMapItemViewInfo(ViewInfo[I]).Item);
  end;
end;

function TdxCustomMapItemLayer.GetStyle(AState: TdxMapControlElementState): TdxMapItemStyle;
begin
  case AState of
    mcesNormal:
      Result := ItemStyle;
    mcesSelected:
      Result := ItemStyleSelected
  else  // mcesHot
    Result := ItemStyleHot;
  end;
end;

function TdxCustomMapItemLayer.CreateViewInfo: TdxMapLayerViewInfo;
begin
  Result := TdxMapItemLayerViewInfo.Create(Self);
end;

procedure TdxCustomMapItemLayer.GetChildren(Proc: TGetChildProc;
  Root: TComponent);
var
  I: Integer;
begin
  for I := 0 to MapItems.Count - 1 do
    if MapItems[I].Owner = Root then Proc(MapItems[I]);
end;

function TdxCustomMapItemLayer.GetMapProjection: TdxMapControlCustomProjection;
begin
  Result := FProjection;
end;

function TdxCustomMapItemLayer.GetMapSizeInPixels(
  AZoomLevel: Double): TdxSizeDouble;
var
  ACoeff, ALevel: Double;
begin
  if AZoomLevel < 1.0 then
    Result := dxSizeDouble(AZoomLevel * InitialMapSize.Width, AZoomLevel * InitialMapSize.Height)
  else
  begin
    ALevel := Max(0.0, AZoomLevel - 1.0);
    ACoeff := Power(2.0, ALevel);
    Result := dxSizeDouble(ACoeff * InitialMapSize.Width, ACoeff * InitialMapSize.Height);
  end;
end;

procedure TdxCustomMapItemLayer.MapItemsChanged(Sender: TObject;
  AItem: TcxComponentCollectionItem; AAction: TcxComponentCollectionNotification);
begin
  Changed(False);
end;

procedure TdxCustomMapItemLayer.ProjectionChanged(Sender: TObject);
begin
  ViewInfo.ClearCache;
  InvalidateViewPort;
  Changed(False);
end;

function TdxCustomMapItemLayer.GetProjectionClass: TdxMapControlCustomProjectionClass;
begin
  Result := FProjectionClass;
end;

function TdxCustomMapItemLayer.GetProjectionClassName: string;
begin
  if FProjection <> nil then
    Result := FProjection.ClassName
  else
    Result := '';
end;

procedure TdxCustomMapItemLayer.InitialMapSizeChanged(ASender: TObject);
begin
  InvalidateViewPort;
  Changed(False);
end;

function TdxCustomMapItemLayer.IsInitialMapSizeStored: Boolean;
begin
  Result := not FInitialMapSize.IsEqual(cxSize(dxMapControlDefaultMapSize, dxMapControlDefaultMapSize));
end;

procedure TdxCustomMapItemLayer.ItemStyleChanged(ASender: TObject);
begin
  Changed(False);
end;

procedure TdxCustomMapItemLayer.SetInitialMapSize(AValue: TcxSize);
begin
  FInitialMapSize.Assign(AValue);
end;

procedure TdxCustomMapItemLayer.SetItemStyle(
  const Value: TdxMapItemStyle);
begin
  FItemStyle.Assign(Value);
end;

procedure TdxCustomMapItemLayer.SetItemStyleHot(
  const Value: TdxMapItemStyle);
begin
  FItemStyleHot.Assign(Value);
end;

procedure TdxCustomMapItemLayer.SetItemStyleSelected(
  const Value: TdxMapItemStyle);
begin
  FItemStyleSelected.Assign(Value);
end;

procedure TdxCustomMapItemLayer.SetMapItems(
  const Value: TdxMapItems);
begin
  FMapItems.Assign(Value);
end;

procedure TdxCustomMapItemLayer.SetProjection(
  const Value: TdxMapControlCustomProjection);
begin
  FProjection.Assign(Value);
end;

procedure TdxCustomMapItemLayer.SetProjectionClass(
  const Value: TdxMapControlCustomProjectionClass);
begin
  if FProjectionClass <> Value then
  begin
    FreeAndNil(FProjection);
    FProjectionClass := Value;
    if FProjectionClass <> nil then
    begin
      FProjection := FProjectionClass.Create(nil);
      FProjection.OnChanged := ProjectionChanged;
    end;
    ProjectionChanged(FProjection);
  end;
end;

procedure TdxCustomMapItemLayer.SetProjectionClassName(const Value: string);
begin
  ProjectionClass := TdxMapControlCustomProjectionClass(dxRegisteredMapProjections.FindByClassName(Value));
end;

end.
