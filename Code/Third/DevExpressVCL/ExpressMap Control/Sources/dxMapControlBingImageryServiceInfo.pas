{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressMapControl                                        }
{                                                                    }
{           Copyright (c) 2013-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSMAPCONTROL AND ALL             }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM       }
{   ONLY.                                                            }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxMapControlBingImageryServiceInfo;

interface

{$I cxVer.inc}

uses
  Math, SysUtils, Classes,
  dxCore, dxCustomTree, dxXmlDoc, dxMapControlHttpRequest;

type
  TdxMapControlBingImageryServiceInfo = class
  private
    FImageUrlTemplate: string;
    FIsValid: Boolean;
    FMaxZoomLevel: Integer;
    FSubdomains: array of string;
    FTileHeight: Integer;
    FTileWidth: Integer;
    function GetSubdomain(I: Integer): string;
    function GetImageHeight: Integer;
    function GetImageWidth: Integer;
    function GetSubdomainCount: Integer;
    function ParseResponse(AResponseContent: TMemoryStream): Boolean;
    function Read(ADocument: TdxXMLDocument): Boolean;
  public
    constructor Create;
    procedure RequestImageryData(const AUri: string);

    property ImageHeight: Integer read GetImageHeight;
    property ImageUrlTemplate: string read FImageUrlTemplate;
    property ImageWidth: Integer read GetImageWidth;
    property IsValid: Boolean read FIsValid;
    property Subdomain [I: Integer]: string read GetSubdomain;
    property SubdomainCount: Integer read GetSubdomainCount;
    property TileHeight: Integer read FTileHeight;
    property TileWidth: Integer read FTileWidth;
  end;

implementation

const
  DefaultMaxZoomLevel = 21;
  DefaultTileSize = 256;

{ BingImageryServiceInfo }

constructor TdxMapControlBingImageryServiceInfo.Create;
begin
  inherited;
  FTileHeight := DefaultTileSize;
  FTileWidth := DefaultTileSize;
  FMaxZoomLevel := DefaultMaxZoomLevel;
end;

procedure TdxMapControlBingImageryServiceInfo.RequestImageryData(const AUri: string);
var
  AResponseContent: TMemoryStream;
begin
  FIsValid := False;
  AResponseContent := TMemoryStream.Create;
  try
    FIsValid := GetContent(AUri, AResponseContent) and
      ParseResponse(AResponseContent);
  finally
    AResponseContent.Free;
  end;
end;

function TdxMapControlBingImageryServiceInfo.GetImageHeight: Integer;
begin
  Result := Round(Power(2.0, FmaxZoomLevel) * TileHeight);
end;

function TdxMapControlBingImageryServiceInfo.GetImageWidth: Integer;
begin
  Result := Round(Power(2.0, FmaxZoomLevel) * TileWidth);
end;

function TdxMapControlBingImageryServiceInfo.GetSubdomain(I: Integer): string;
begin
  Result := FSubdomains[I];
end;

function TdxMapControlBingImageryServiceInfo.GetSubdomainCount: Integer;
begin
  Result := Length(FSubdomains);
end;

function TdxMapControlBingImageryServiceInfo.ParseResponse(AResponseContent: TMemoryStream): Boolean;
var
  ADocument: TdxXMLDocument;
begin
  AResponseContent.Position := 0;
  ADocument := TdxXMLDocument.Create(nil);
  try
    ADocument.LoadFromStream(AResponseContent);
    Result := Read(ADocument);
  finally
    ADocument.Free;
  end;
end;

function TdxMapControlBingImageryServiceInfo.Read(ADocument: TdxXMLDocument): Boolean;
var
  ANode: TdxXMLNode;
  AUrlNode, ASubdomainNode: TdxXMLNode;
  AChildNode: TdxXMLNode;
  I: Integer;
begin
  Result := False;
  if ADocument.FindChild('Response', ANode) and
    ANode.FindChild('ResourceSets', ANode) and
      ANode.FindChild('ResourceSet', ANode) and
        ANode.FindChild('Resources', ANode) and
          ANode.FindChild('ImageryMetadata', ANode) then
          begin
            if ANode.FindChild('ImageUrl', AUrlNode) then
              FImageUrlTemplate := AUrlNode.TextAsUnicodeString;
            if ANode.FindChild('ImageUrlSubdomains', ASubdomainNode) then
            begin
              SetLength(FSubdomains, ASubdomainNode.Count);
              for I := 0 to ASubdomainNode.Count - 1 do
                FSubdomains[I] := ASubdomainNode[I].TextAsUnicodeString;
            end;
            if ANode.FindChild('ImageWidth', AChildNode) then
              FTileWidth := StrToInt(AChildNode.TextAsUnicodeString);
            if ANode.FindChild('ImageHeight', AChildNode) then
              FTileHeight := StrToInt(AChildNode.TextAsUnicodeString);
            if ANode.FindChild('ZoomMax', AChildNode) then
              FMaxZoomLevel := StrToInt(AChildNode.TextAsUnicodeString);
            Result := (AUrlNode <> nil) and (ASubdomainNode <> nil);
          end;
end;

end.
