{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           GDI+ Library                                             }
{                                                                    }
{           Copyright (c) 2002-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE GDIPLUS LIBRARY AND ALL ACCOMPANYING  }
{   VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY.              }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxGDIPlusClasses;

{$I cxVer.inc}
{$DEFINE DXREGISTERPNGIMAGE}

interface

uses
  Windows, Classes, SysUtils, Graphics, ActiveX, Types,
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  dxCore, dxCoreGraphics, cxGeometry, dxGDIPlusAPI, dxCoreClasses, Contnrs;

type
  TdxGPBrush = class;
  TdxGPCanvas = class;
  TdxGPCustomBrush = class;
  TdxGPImage = class;
  TdxGPPath = class;
  TdxPNGImage = class;

  TdxImageDataFormat = (dxImageUnknown, dxImageBitmap, dxImageJpeg, dxImagePng, dxImageTiff, dxImageGif);

  TdxGPInterpolationMode = (
    imDefault = InterpolationModeDefault,
    imLowQuality = InterpolationModeLowQuality,
    imHighQuality = InterpolationModeHighQuality,
    imBilinear = InterpolationModeBilinear,
    imBicubic = InterpolationModeBicubic,
    imNearestNeighbor = InterpolationModeNearestNeighbor,
    imHighQualityBilinear = InterpolationModeHighQualityBilinear,
    imHighQualityBicubic = InterpolationModeHighQualityBicubic);

  TdxGPSmoothingMode = (smDefault, smHighSpeed, smHighQuality, smNone, smAntiAlias);

  { TdxGPCustomGraphicObject }

  TdxGPCustomGraphicObject = class(TdxGpBase)
  private
    FHandle: GpHandle;
    FOnChange: TNotifyEvent;
    function GetHandle: GpHandle;
    function GetHandleAllocated: Boolean;
  protected
    procedure Changed; virtual;
    procedure DoCreateHandle(out AHandle: GpHandle); virtual; abstract;
    procedure DoFreeHandle(AHandle: GpHandle); virtual; abstract;
  public
    constructor Create; virtual;

    procedure Assign(ASource: TdxGPCustomGraphicObject); virtual;
    procedure BeforeDestruction; override;
    procedure FreeHandle;
    procedure HandleNeeded; virtual;
    //
    property Handle: GpHandle read GetHandle;
    property HandleAllocated: Boolean read GetHandleAllocated;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
  end;

  { TdxGPMatrix }

  TdxGPMatrix = class(TdxGPCustomGraphicObject)
  private
    function GetIsIdentity: Boolean;
    function GetIsInvertible: Boolean;
  protected
    procedure DoCreateHandle(out AHandle: GpHandle); override;
    procedure DoFreeHandle(AHandle: GpHandle); override;
  public
    constructor CreateEx(M11, M12, M21, M22, DX, DY: Single);
    constructor CreateFlip(AFlipHorizontally, AFlipVertically: Boolean; const APivotPointX, APivotPointY: Single); overload;
    constructor CreateFlip(AFlipHorizontally, AFlipVertically: Boolean; const APivotPoint: TdxPointF); overload;
    procedure Assign(ASource: TdxGPCustomGraphicObject); override;
    // Elements
    procedure GetElements(out M11, M12, M21, M22, DX, DY: Single);
    procedure SetElements(M11, M12, M21, M22, DX, DY: Single);

    procedure Flip(AFlipHorizontally, AFlipVertically: Boolean; const APivotPoint: TdxPointF);
    procedure Invert;
    function GetBoundingRectangle(const R: TdxRectF): TdxRectF;
    procedure Multiply(AMatrix: TdxGPMatrix; AMatrixOrder: TdxGpMatrixOrder = MatrixOrderPrepend); overload;
    procedure Reset;
    procedure Rotate(AAngle: Single; AMatrixOrder: TdxGpMatrixOrder = MatrixOrderPrepend); overload;
    procedure Rotate(AAngle: Single; const APivotPoint: TdxPointF; AMatrixOrder: TdxGpMatrixOrder = MatrixOrderPrepend); overload;
    procedure Scale(const AScale: TdxPointF; AMatrixOrder: TdxGpMatrixOrder = MatrixOrderPrepend); overload; {$IFDEF DELPHI9}inline;{$ENDIF}
    procedure Scale(const AScaleX, AScaleY: Single; AMatrixOrder: TdxGpMatrixOrder = MatrixOrderPrepend); overload;
    procedure Shear(const AShear: TdxPointF; AMatrixOrder: TdxGpMatrixOrder = MatrixOrderPrepend); overload; {$IFDEF DELPHI9}inline;{$ENDIF}
    procedure Shear(const AShearX, AShearY: Single; AMatrixOrder: TdxGpMatrixOrder = MatrixOrderPrepend); overload;
    procedure Translate(const AOffset: TdxPointF; AMatrixOrder: TdxGpMatrixOrder = MatrixOrderPrepend); overload; {$IFDEF DELPHI9}inline;{$ENDIF}
    procedure Translate(const AOffsetX, AOffsetY: Single; AMatrixOrder: TdxGpMatrixOrder = MatrixOrderPrepend); overload;
    function TransformPoint(const P: TdxPointF): TdxPointF; overload;
    function TransformPoint(const P: TPoint): TPoint; overload;
    function TransformRect(const R: TdxRectF): TdxRectF; overload;
    function TransformRect(const R: TRect): TRect; overload;
    //
    property IsIdentity: Boolean read GetIsIdentity;
    property IsInvertible: Boolean read GetIsInvertible;
  end;

  { TdxGPBrushGradientPoints }

  TdxGPBrushGradientPoints = class(TdxGpBase)
  private
    FCapacity: Integer;
    FColors: array of TdxAlphaColor;
    FCount: Integer;
    FOffsets: array of Single;
    FOnChange: TNotifyEvent;

    procedure CheckIndex(Index: Integer); {$IFDEF DELPHI9} inline;{$ENDIF}
    procedure Grow;
    function GetColor(Index: Integer): TdxAlphaColor;
    function GetOffset(Index: Integer): Single;
    function InternalAdd(AOffset: Single; AColor: TdxAlphaColor): Integer; {$IFDEF DELPHI9} inline;{$ENDIF}
    procedure SetCapacity(AValue: Integer);
    procedure SetColor(Index: Integer; const AValue: TdxAlphaColor);
    procedure SetOffset(Index: Integer; const AValue: Single);
  protected
    procedure CalculateParams(out AColors: PdxAlphaColor; out AOffsets: PSingle; out ACount: Integer);
    procedure Changed;
    procedure ValidateOrder;

    property OnChange: TNotifyEvent read FOnChange write FOnChange;
  public
    constructor Create; virtual;
    function Add(AOffset: Single; AColor: TdxAlphaColor): Integer;
    procedure Assign(Source: TdxGPBrushGradientPoints);
    procedure Clear;
    procedure Delete(Index: Integer);
    procedure InvertOrder;
    //
    property Capacity: Integer read FCapacity write SetCapacity;
    property Colors[Index: Integer]: TdxAlphaColor read GetColor write SetColor;
    property Count: Integer read FCount;
    property Offsets[Index: Integer]: Single read GetOffset write SetOffset;
  end;

  { TdxGPCustomBrush }

  TdxGPCustomBrush = class(TdxGPCustomGraphicObject)
  private
    FTargetRect: TdxGpRectF;
  protected
    procedure DoFreeHandle(AHandle: GpHandle); override;
    procedure DoTargetRectChanged; virtual;
    function NeedRecreateHandleOnTargetRectChange: Boolean; virtual;
  public
    procedure Assign(ASource: TdxGPCustomGraphicObject); override;
    procedure SetTargetRect(const R: TRect); overload; {$IFDEF DELPHI9} inline; {$ENDIF}
    procedure SetTargetRect(const R: TdxRectF); overload; {$IFDEF DELPHI9} inline; {$ENDIF}
    procedure SetTargetRect(const R: TdxGpRect); overload; {$IFDEF DELPHI9} inline; {$ENDIF}
    procedure SetTargetRect(const R: TdxGpRectF); overload; {$IFDEF DELPHI9} inline; {$ENDIF}

    property TargetRect: TdxGpRectF read FTargetRect;
  end;

  { TdxGPBrush }

  TdxGPBrushGradientMode = (gpbgmHorizontal, gpbgmVertical, gpbgmForwardDiagonal, gpbgmBackwardDiagonal);
  TdxGPBrushStyle = (gpbsSolid, gpbsGradient, gpbsTexture, gpbsClear);

  TdxGPBrush = class(TdxGPCustomBrush)
  private
    FColor: TdxAlphaColor;
    FGradientMode: TdxGPBrushGradientMode;
    FGradientPoints: TdxGPBrushGradientPoints;
    FStyle: TdxGPBrushStyle;
    FTexture: TdxGpImage;

    procedure SetColor(const AValue: TdxAlphaColor);
    procedure SetGradientMode(const AValue: TdxGPBrushGradientMode);
    procedure SetGradientPoints(const AValue: TdxGPBrushGradientPoints);
    procedure SetStyle(const AValue: TdxGPBrushStyle);
    procedure SetTexture(const AValue: TdxGpImage);
    //
    procedure PointsChangeHandler(Sender: TObject);
    procedure TextureChangeHandler(Sender: TObject);
  protected
    procedure CreateEmptyBrushHandle(out AHandle: GpBrush);
    procedure CreateGradientBrushHandle(out AHandle: GpBrush);
    procedure CreateTextureBrushHandle(out AHandle: GpBrush);
    function GetIsEmpty: Boolean; virtual;
    //
    procedure DoCreateHandle(out AHandle: GpHandle); override;
    procedure DoTargetRectChanged; override;
    function NeedRecreateHandleOnTargetRectChange: Boolean; override;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Assign(ASource: TdxGPCustomGraphicObject); override;
    //
    property Color: TdxAlphaColor read FColor write SetColor;
    property GradientMode: TdxGPBrushGradientMode read FGradientMode write SetGradientMode;
    property GradientPoints: TdxGPBrushGradientPoints read FGradientPoints write SetGradientPoints;
    property Style: TdxGPBrushStyle read FStyle write SetStyle;
    property Texture: TdxGpImage read FTexture write SetTexture;
    //
    property IsEmpty: Boolean read GetIsEmpty;
  end;

  { TdxGPPen }

  TdxGPPenStyle = (gppsSolid, gppsDash, gppsDot, gppsDashDot, gppsDashDotDot);

  TdxGPPen = class(TdxGPCustomGraphicObject)
  private
    FBrush: TdxGPBrush;
    FStyle: TdxGPPenStyle;
    FWidth: Single;

    procedure SetBrush(const AValue: TdxGPBrush);
    procedure SetStyle(const AValue: TdxGPPenStyle);
    procedure SetWidth(AValue: Single);
  protected
    procedure DoCreateHandle(out AHandle: GpHandle); override;
    procedure DoFreeHandle(AHandle: GpHandle); override;
    procedure DoSetDashStyle(AHandle: GpHandle);
    function GetIsEmpty: Boolean; virtual;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Assign(ASource: TdxGPCustomGraphicObject); override;
    procedure HandleNeeded; override;
    procedure SetTargetRect(const R: TdxGpRect); overload; {$IFDEF DELPHI9} inline; {$ENDIF}
    procedure SetTargetRect(const R: TRect); overload; {$IFDEF DELPHI9} inline; {$ENDIF}
    procedure SetTargetRect(const R: TdxRectF); overload; {$IFDEF DELPHI9} inline; {$ENDIF}
    //
    property Brush: TdxGPBrush read FBrush write SetBrush;
    property Style: TdxGPPenStyle read FStyle write SetStyle;
    property Width: Single read FWidth write SetWidth;
    //
    property IsEmpty: Boolean read GetIsEmpty;
  end;

  { TdxGPPenBrush }

  TdxGPPenBrush = class(TdxGPBrush)
  private
    FPen: TdxGPPen;
    procedure UpdatePenFillHandle(AHandle: GpHandle);
  protected
    procedure Changed; override;
    procedure DoCreateHandle(out AHandle: GpHandle); override;
  public
    constructor Create(APen: TdxGPPen); reintroduce;
  end;

  { TdxGPPath }

  TdxGPFillMode = (gpfmAlternate, gpfmWinding);

  TdxGPPath = class(TdxGpBase)
  private
    FHandle: GpPath;
  public
    constructor Create; overload; virtual;
    constructor Create(AFillMode: TdxGPFillMode); overload; virtual;
    constructor Create(APoints: TdxGpPointFDynArray; APointTypes: TBytes); overload; virtual;
    destructor Destroy; override;
    //
    procedure Flatten(AMatrix: TdxGPMatrix = nil; AFlatness: Single = 0.25);
    procedure FigureFinish;
    procedure FigureStart;
    procedure Reset;
    //
    procedure AddArc(const X, Y, Width, Height, StartAngle, SweepAngle: Single);
    procedure AddEllipse(const R: TRect); overload;
    procedure AddEllipse(const R: TdxRectF); overload;
    procedure AddLine(const X1, Y1, X2, Y2: Single);
    procedure AddPolygon(const APoints: array of TPoint);
    procedure AddPolyline(const APoints: array of TPoint);
    procedure AddRect(const R: TRect); overload;
    procedure AddRect(const R: TdxRectF); overload;
    procedure AddRoundRect(const R: TRect; ARadiusX, ARadiusY: Integer); overload;
    procedure AddRoundRect(const R: TdxRectF; ARadiusX, ARadiusY: Single); overload;
    procedure AddString(AText: PWideChar; AFont: TFont; AEmSize: Single; const ARect: TRect);
    //
    function GetBounds(APen: TdxGPPen = nil): TRect;
    function GetBoundsF(APen: TdxGPPen = nil): TdxRectF;
    function IsPointInPath(const P: TdxPointF; ACanvas: TdxGPCanvas = nil): Boolean; overload;
    function IsPointInPath(const P: TPoint; ACanvas: TdxGPCanvas = nil): Boolean; overload;
    function IsPointInPathOutline(const P: TPoint; APenWidth: Integer; ACanvas: TdxGPCanvas = nil): Boolean; overload;
    function IsPointInPathOutline(const P: TdxPointF; APen: TdxGPPen; ACanvas: TdxGPCanvas = nil): Boolean; overload;
    function IsPointInPathOutline(const P: TPoint; APen: TdxGPPen; ACanvas: TdxGPCanvas = nil): Boolean; overload;
    //
    property Handle: GpPath read FHandle;
  end;

  TdxGPCombineMode = (gmReplace, gmIntersect, gmUnion, gmXor, gmExclude, gmComplement); 

  { TdxGPRegion }

  TdxGPRegion = class(TdxGPBase)
  private
    FHandle: GpRegion;
  public
    constructor Create; overload; virtual;
    constructor Create(APath: GpPath); overload; virtual;
    constructor Create(const ARect: TRect); overload; virtual;
    destructor Destroy; override;

    procedure CombineRegionRect(const ARect: TRect; AMode: TdxGPCombineMode);
    property Handle: GpRegion read FHandle;
  end;

  { TdxGPCanvas }

  TdxGPCanvas = class(TdxGPBase)
  private
    FHandle: GpGraphics;
    FSavedClipRegions: TStack;

    function GetInterpolationMode: TdxGPInterpolationMode;
    function GetSmoothingMode: TdxGPSmoothingMode;
    procedure SetInterpolationMode(AValue: TdxGPInterpolationMode);
    procedure SetSmoothingMode(AValue: TdxGPSmoothingMode);
  protected
    procedure CheckDestRect(var R: TRect);
    procedure CreateHandle(DC: HDC);
    procedure FreeHandle;
  public
    constructor Create; overload; virtual;
    constructor Create(AHandle: GpGraphics); overload;
    constructor Create(DC: THandle); overload;
    destructor Destroy; override;
    //
    function GetHDC: HDC;
    procedure ReleaseHDC(DC: HDC);
    procedure Clear(AColor: TColor);
    //
    procedure Draw(AGraphic: TdxGPImage; const ADestRect, ASourceRect: TdxRectF; AAlpha: Byte = 255); overload;
    procedure Draw(AGraphic: TdxGPImage; const ADestRect, ASourceRect: TRect; AAlpha: Byte = 255); overload;
    procedure Draw(AGraphic: TdxGPImage; const R: TdxRectF; AAlpha: Byte = 255); overload;
    procedure Draw(AGraphic: TdxGPImage; const R: TRect; AAlpha: Byte = 255); overload;
    procedure DrawBitmap(ABitmap: TBitmap; const R: TRect; AAlpha: Byte = 255); // deprecated
    procedure DrawTile(AGraphic: TdxGPImage; const R: TRect; AAlpha: Byte = 255); overload;

    // Clipping
    function IsClipEmpty: Boolean;
    procedure RestoreClipRegion;
    procedure SaveClipRegion;
    procedure SetClipPath(APath: TdxGPPath; AMode: TdxGPCombineMode);
    procedure SetClipRect(R: TRect; AMode: TdxGPCombineMode);
    procedure SetClipRegion(ARgn: HRGN; AMode: TdxGPCombineMode);

    // Arc
    procedure Arc(R: TRect; AStartAngle, ASweepAngle: Single; APenColor: TColor;
      APenWidth: Single; APenStyle: TPenStyle; APenColorAlpha: Byte); overload;
    procedure Arc(R: TRect; AStartAngle, ASweepAngle: Single; APenColor: TdxAlphaColor;
      APenWidth: Single = 1; APenStyle: TPenStyle = psSolid); overload;

    // Ellipse
    procedure Ellipse(R: TRect; APen: TdxGPPen; ABrush: TdxGPCustomBrush); overload;
    procedure Ellipse(R: TdxRectF; APenColor, ABrushColor: TColor; APenWidth: Single;
      APenStyle: TPenStyle; APenColorAlpha, ABrushColorAlpha: Byte); overload;
    procedure Ellipse(R: TRect; APenColor, ABrushColor: TdxAlphaColor; APenWidth: Single = 1;
      APenStyle: TPenStyle = psSolid); overload;
    procedure Ellipse(R: TRect; APenColor, ABrushColor: TColor; APenWidth: Single;
      APenStyle: TPenStyle; APenColorAlpha, ABrushColorAlpha: Byte); overload;

    // Line
    procedure Line(X1, Y1, X2, Y2: Integer; APenColor: TColor; APenWidth: Single;
      APenStyle: TPenStyle; APenColorAlpha: Byte); overload;
    procedure Line(X1, Y1, X2, Y2: Integer; APenColor: TdxAlphaColor; APenWidth: Single = 1;
      APenStyle: TPenStyle = psSolid); overload;
    procedure Line(X1, Y1, X2, Y2: Integer; APen: TdxGPPen); overload;

    // Path
    procedure Path(APath: TdxGPPath; APen: TdxGPPen; ABrush: TdxGPCustomBrush); overload;
    procedure Path(APath: TdxGPPath; APenColor, ABrushColor: TdxAlphaColor; APenWidth: Single = 1;
      APenStyle: TPenStyle = psSolid); overload;
    procedure Path(APath: TdxGPPath; APenColor, ABrushColor: TColor; APenWidth: Single;
      APenStyle: TPenStyle; APenColorAlpha, ABrushColorAlpha: Byte); overload;

    // Pie
    procedure Pie(R: TRect; AStartAngle, ASweepAngle: Single; APenColor: TColor;
      ABrushColor: TColor; APenWidth: Single; APenStyle: TPenStyle;
      APenColorAlpha, ABrushColorAlpha: Byte); overload;
    procedure Pie(R: TRect; AStartAngle, ASweepAngle: Single; APenColor, ABrushColor: TdxAlphaColor;
      APenWidth: Single = 1; APenStyle: TPenStyle = psSolid); overload;

    // Polygon
    procedure Polygon(const APoints: array of TPoint; APenColor, ABrushColor: TColor; APenWidth: Single;
      APenStyle: TPenStyle; APenColorAlpha, ABrushColorAlpha: Byte); overload;
    procedure Polygon(const APoints: array of TPoint; APenColor, ABrushColor: TdxAlphaColor; APenWidth: Single = 1;
      APenStyle: TPenStyle = psSolid); overload;
    procedure Polygon(const APoints: array of TdxPointF; APenColor, ABrushColor: TColor; APenWidth: Single;
      APenStyle: TPenStyle; APenColorAlpha, ABrushColorAlpha: Byte); overload;

    // Polyline
    procedure Polyline(const APoints: array of TPoint; APenColor: TColor;
      APenWidth: Single; APenStyle: TPenStyle; APenColorAlpha: Byte); overload;
    procedure Polyline(const APoints: array of TPoint; APenColor: TdxAlphaColor;
      APenWidth: Single = 1; APenStyle: TPenStyle = psSolid); overload;
    procedure Polyline(const APoints: array of TdxPointF; APenColor: TColor;
      APenWidth: Single; APenStyle: TPenStyle; APenColorAlpha: Byte); overload;

    // Rectangle
    procedure Rectangle(R: TRect; APen: TdxGPPen; ABrush: TdxGPCustomBrush); overload;
    procedure Rectangle(R: TRect; APenColor, ABrushColor: TdxAlphaColor; APenWidth: Single = 1;
      APenStyle: TPenStyle = psSolid); overload;
    procedure Rectangle(R: TRect; APenColor, ABrushColor: TColor; APenWidth: Single;
      APenStyle: TPenStyle; APenColorAlpha, ABrushColorAlpha: Byte); overload;

    // RoundRect
    procedure RoundRect(R: TRect; APen: TdxGPPen; ABrush: TdxGPCustomBrush; ARadiusX, ARadiusY: Integer); overload;
    procedure RoundRect(R: TRect; APenColor, ABrushColor: TColor; ARadiusX, ARadiusY: Integer;
      APenWidth: Integer; APenColorAlpha, ABrushColorAlpha: Byte); overload;
    procedure RoundRect(R: TRect; APenColor, ABrushColor: TdxAlphaColor; ARadiusX, ARadiusY: Integer;
      APenWidth: Single = 1); overload;

    // World Transform
    procedure FlipWorldTransform(AFlipHorizontally, AFlipVertically: Boolean; const APivotPoint: TdxPointF); overload; {$IFDEF DELPHI9} inline; {$ENDIF}
    procedure FlipWorldTransform(AFlipHorizontally, AFlipVertically: Boolean; const APivotPoint: TPoint); overload; {$IFDEF DELPHI9} inline; {$ENDIF}
    procedure FlipWorldTransform(AFlipHorizontally, AFlipVertically: Boolean; const APivotPointX, APivotPointY: Single); overload;
    function GetWorldTransform: TdxGPMatrix;
    procedure ModifyWorldTransform(AMatrix: TdxGPMatrix; AOrder: TdxGpMatrixOrder = MatrixOrderPrepend);
    procedure ResetWorldTransform;
    procedure RotateWorldTransform(AAngle: Single; AOrder: TdxGpMatrixOrder = MatrixOrderPrepend); overload;
    procedure RotateWorldTransform(AAngle: Single; const APivotPoint: TPoint; AOrder: TdxGpMatrixOrder = MatrixOrderPrepend); overload;
    procedure RotateWorldTransform(AAngle: Single; const APivotPoint: TdxPointF; AOrder: TdxGpMatrixOrder = MatrixOrderPrepend); overload;
    procedure ScaleWorldTransform(AScaleX, AScaleY: Single; AOrder: TdxGpMatrixOrder = MatrixOrderPrepend);
    procedure SetWorldTransform(AMatrix: TdxGPMatrix);
    procedure TranslateWorldTransform(AOffestX, AOffsetY: Single; AOrder: TdxGpMatrixOrder = MatrixOrderPrepend);

    property Handle: GpGraphics read FHandle;
    property InterpolationMode: TdxGPInterpolationMode read GetInterpolationMode write SetInterpolationMode;
    property SmoothingMode: TdxGPSmoothingMode read GetSmoothingMode write SetSmoothingMode;
  end;

  TdxGPGraphics = class(TdxGPCanvas);

  { TdxGPCustomPaintCanvas }

  TdxGPPaintCanvasState = record
    Handle: GpGraphics;
    Buffer: TBitmap;
    DrawRect: TRect;
    DC: HDC;
  end;
  TdxGPPaintCanvasStates = array of TdxGPPaintCanvasState;

  TdxGPCustomPaintCanvas = class(TdxGPGraphics)
  private
    FBuffer: TBitmap;
    FDrawRect: TRect;
    FDrawDC: HDC;

    procedure CreateBuffer(DC: HDC; const R: TRect);
    procedure FreeBuffer;
    procedure OutputBuffer;
  protected
    procedure SaveState; virtual;
    procedure RestoreState; virtual;
  public
    procedure BeginPaint(DC: HDC; const R: TRect); overload;
    procedure BeginPaint(AHandle: GpGraphics); overload;
    procedure EndPaint;
  end;

  { TdxGPPaintCanvas }

  TdxGPPaintCanvas = class(TdxGPCustomPaintCanvas)
  private
    FCounter: Integer;
    FSavedStates: TdxGPPaintCanvasStates;

    procedure SetCapacity(AValue: Integer);
  protected
    procedure SaveState; override;
    procedure RestoreState; override;
  public
    destructor Destroy; override;
  end;

  { TdxGPMemoryStream }

  TdxGPMemoryStream = class(TMemoryStream)
  protected
    function Realloc(var ANewCapacity: Integer): Pointer; override;
  end;

  { TdxGPStreamAdapter }

  TdxGPStreamAdapter = class(TStreamAdapter)
  public
    function Stat(out StatStg: TStatStg; StatFlag: Integer): HRESULT; override; stdcall;
  end;

  { TdxGPImageData }

  TdxGPImageData = class(TObject)
  private
    function GetIsEmpty: Boolean;
  protected
    FBits: TRGBColors;
    FBitsBounds: TSize;
    FData: TdxGPMemoryStream;
    FNewSize: TSize;
    procedure Assign(AData: TdxGPImageData);
    procedure FreeImageData;
    procedure LoadFromBits(const ABits: TRGBColors; const ASize: TSize; AHasAlphaChannel: Boolean);
    procedure LoadFromStream(AStream: TStream);
  public
    destructor Destroy; override;
    //
    property Empty: Boolean read GetIsEmpty;
  end;

  { TdxGPImage }

  TdxGPImageClass = class of TdxGPImage;
  TdxGPImage = class(TGraphic)
  private
    FHandle: GpImage;
    FImageData: TdxGPImageData;
    FImageDataFormat: TdxImageDataFormat;
    FIsAlphaUsed: Boolean;
    FIsAlphaUsedAssigned: Boolean;
    function CheckAlphaUsed: Boolean;
    function GetClientRect: TRect;
    function GetHandle: GpImage;
    function GetHandleAllocated: Boolean;
    function GetImageDataFormat: TdxImageDataFormat;
    function GetIsAlphaUsed: Boolean;
    procedure RecognizeImageRawDataFormat;
    procedure SetImageDataFormat(AValue: TdxImageDataFormat);
  protected
    function CheckHasAlphaChannel(const ABits: TRGBColors): Boolean;
    procedure CreateHandleFromBitmap(ABitmap: TBitmap);
    procedure CreateHandleFromBits(AWidth, AHeight: Integer; const ABits: TRGBColors; AHasAlphaChannel: Boolean);
    procedure CreateHandleFromHBITMAP(ABitmap: HBITMAP; APalette: HPALETTE);
    procedure CreateHandleFromImageData;
    procedure CreateHandleFromStream(AStream: TStream);

    procedure AssignFromGpImage(AGpImage: TdxGPImage); virtual;
    procedure AssignFromGraphic(AGraphic: TGraphic); virtual;
    procedure AssignTo(ADest: TPersistent); override;
    procedure Changed(Sender: TObject); override;
    procedure Draw(ACanvas: TCanvas; const ARect: TRect); override;
    procedure FreeHandle;
    procedure InternalSaveToStream(AStream: TStream; AImageFormat: TdxImageDataFormat; AParameters: PEncoderParameters);

  {$IFDEF DELPHI14}
    function Equals(Graphic: TGraphic): Boolean; override;
  {$ENDIF}
    function GetEmpty: Boolean; override;
    function GetHeight: Integer; override;
    function GetLoaded: Boolean; virtual;
    function GetSize: TSize;
    function GetTransparent: Boolean; override;
    function GetWidth: Integer; override;
    procedure SetHandle(AValue: GPImage);
    procedure SetHeight(Value: Integer); override;
    procedure SetWidth(Value: Integer); override;
    //
    property Loaded: Boolean read GetLoaded;  
    property ImageData: TdxGPImageData read FImageData;
  public
    constructor Create; override;
    constructor CreateSize(const ASize: TSize; AColor: TdxAlphaColor = 0); overload; virtual;
    constructor CreateSize(const R: TRect; AColor: TdxAlphaColor = 0); overload;
    constructor CreateSize(AWidth, AHeight: Integer; AColor: TdxAlphaColor = 0); overload;
    constructor CreateFromBitmap(ABitmap: TBitmap); virtual;
    constructor CreateFromHBitmap(ABitmap: HBitmap); virtual;
    constructor CreateFromBits(AWidth, AHeight: Integer; const ABits: TRGBColors; AHasAlphaChannel: Boolean); virtual;
    constructor CreateFromStream(AStream: TStream); virtual;
    destructor Destroy; override;

    procedure Assign(ASource: TPersistent); override;
    procedure LoadFromBits(AWidth, AHeight: Integer; const ABits: TRGBColors; AHasAlphaChannel: Boolean);
    procedure LoadFromClipboardFormat(AFormat: Word; AData: THandle; APalette: HPALETTE); override;
    procedure LoadFromFieldValue(const AValue: Variant);
    procedure LoadFromResource(AInstance: THandle; const AResName: string; AResType: PChar);
    procedure LoadFromStream(AStream: TStream); override;
    procedure SaveToClipboardFormat(var AFormat: Word; var AData: THandle; var APalette: HPALETTE); override;
    procedure SaveToStream(AStream: TStream); override;
    procedure SaveToStreamByCodec(AStream: TStream; AImageFormat: TdxImageDataFormat);

    procedure Clear; virtual;
    function Clone: TdxGPImage; virtual;
    procedure ChangeColor(AColor: TColor); virtual;
    function Compare(AImage: TdxGPImage): Boolean; virtual;
    procedure ConvertToBitmap;
    function CreateCanvas: TdxGPCanvas;
    function GetAsBitmap: TBitmap; virtual;
    function GetBitmapBits: TRGBColors;
    function GetHashCode: Integer; {$IFDEF DELPHI14} override; {$ENDIF}
    function MakeComposition(AOverlayImage: TdxGPImage; AOverlayAlpha: Byte): TdxGPImage; overload;
    function MakeComposition(AOverlayImage: TdxGPImage; AOverlayAlpha, ASourceAlpha: Byte): TdxGPImage; overload;
    procedure HandleNeeded;
    procedure Resize(const AWidth, AHeight: Integer); overload; virtual;
    procedure Resize(const ASize: TSize); overload;
    procedure SetBitmap(ABitmap: TBitmap); virtual;
    procedure Scale(const M, D: Integer);
    procedure StretchDraw(DC: HDC; const ADest, ASource: TRect; AAlpha: Byte = 255); overload; // deprecated
    procedure StretchDraw(DC: HDC; const ADest: TRect; AAlpha: Byte = 255); overload; // deprecated

    property ClientRect: TRect read GetClientRect;
    property Handle: GpImage read GetHandle write SetHandle;
    property HandleAllocated: Boolean read GetHandleAllocated;
    property ImageDataFormat: TdxImageDataFormat read GetImageDataFormat write SetImageDataFormat;
    property IsAlphaUsed: Boolean read GetIsAlphaUsed;
    property Size: TSize read GetSize;
  end;

  { TdxSmartImage }

  TdxSmartImage = class(TdxGPImage)
  protected
    procedure ReadData(Stream: TStream); override;
    procedure WriteData(Stream: TStream); override;
  end;

  { TdxPNGImage }

  TdxPNGImage = class(TdxSmartImage)
  public
    procedure SaveToStream(AStream: TStream); override;
  end;

  { TdxJPEGImage }

  TdxJPEGImage = class(TdxSmartImage)
  private
    FQuality: Cardinal;
    procedure SetQuality(AValue: Cardinal);
  public
    constructor Create; override;
    procedure SaveToStream(AStream: TStream); override;
    //
    property Quality: Cardinal read FQuality write SetQuality;
  end;

  { TdxGIFImage }

  TdxGIFImage = class(TdxSmartImage)
  public
    procedure SaveToStream(AStream: TStream); override;
  end;

  { TdxTIFFImage }

  TdxTIFFImage = class(TdxSmartImage)
  public
    procedure SaveToStream(AStream: TStream); override;
  end;

  { TdxBMPImage }

  TdxBMPImage = class(TdxSmartImage)
  public
    procedure SaveToStream(AStream: TStream); override;
  end;

function dxGpIsDoubleBufferedNeeded(DC: HDC): Boolean;
function dxGpIsRectVisible(AGraphics: GpGraphics; const R: TdxRectF): LongBool; overload;
function dxGpIsRectVisible(AGraphics: GpGraphics; const R: TRect): LongBool; overload;

procedure dxGpDrawImage(AGraphics: GpGraphics; const ADestRect: TRect;
  const ASourceRect: TRect; AImage: GpImage; AAlpha: Byte = 255); overload;
procedure dxGpDrawImage(AGraphics: GpGraphics; const ADestRect: TdxRectF;
  const ASourceRect: TdxRectF; AImage: GpImage; AAlpha: Byte = 255); overload;

procedure dxGPDrawText(AGraphics: TdxGPGraphics; const AText: string;
  const ARect: TRect; AFont: TFont; ATextColor: TdxAlphaColor;
  AHorzAlignment: TAlignment = taLeftJustify; AVertAlignment: TVerticalAlignment = taVerticalCenter;
  AWordWrap: Boolean = False; ARendering: TdxGpTextRenderingHint = TextRenderingHintSystemDefault); overload;
procedure dxGPDrawText(AGraphics: TdxGPGraphics; const AText: string;
  const ARect: TdxRectF; AFont: TFont; ATextColor: TdxAlphaColor;
  AHorzAlignment: TAlignment = taLeftJustify; AVertAlignment: TVerticalAlignment = taVerticalCenter;
  AWordWrap: Boolean = False; ARendering: TdxGpTextRenderingHint = TextRenderingHintSystemDefault); overload;

procedure dxGPDrawGlowText(AGraphics: TdxGPGraphics; const AText: string;
  const ARect: TRect; AFont: TFont; ATextColor, ATextGlowColor: TdxAlphaColor);

procedure dxGPGetTextRect(AGraphics: TdxGPGraphics; const AText: string; AFont: TFont;
  AWordWrap: Boolean; const ALayoutRect: TRect; out ATextRect: TRect); overload;
procedure dxGPGetTextRect(AGraphics: TdxGPGraphics; const AText: string; AFont: TFont;
  AWordWrap: Boolean; const ALayoutRect: TdxRectF; out ATextRect: TdxRectF); overload;

procedure dxGpFillRect(DC: HDC; const R: TRect; AColor: TColor; AColorAlpha: Byte = 255); overload;
procedure dxGpFillRect(AGraphics: GpGraphics; const R: TRect; AColor: TdxAlphaColor); overload;
procedure dxGpFillRectByGradient(AGraphics: GpGraphics; const R: TRect;
  AColor1, AColor2: TdxAlphaColor; AMode: TdxGPLinearGradientMode); overload;
procedure dxGpFillRectByGradient(DC: HDC; const R: TRect;
  AColor1, AColor2: TColor; AMode: TdxGPLinearGradientMode;
  AColor1Alpha: Byte = 255; AColor2Alpha: Byte = 255); overload;
procedure dxGpTilePart(DC: HDC; const ADestRect, ASourceRect: TRect; AImage: GpBitmap);
procedure dxGpTilePartEx(AGraphics: GpGraphics;
  const ADestRect, ASourceRect: TRect; AImage: GpBitmap; AAlpha: Byte = 255);
procedure dxGpResetRect(AGraphics: GpGraphics; const R: TRect);
procedure dxGpRoundRect(DC: HDC; const R: TRect;
  APenColor: TColor; ABrushColor: TColor; ARadius: Integer; APenWidth: Integer = 1;
  APenColorAlpha: Byte = 255; ABrushColorAlpha: Byte = 255);

function dxGpBeginPaint(AHandle: GpGraphics): TdxGPGraphics; overload;
function dxGpBeginPaint(DC: HDC; const R: TRect): TdxGPGraphics; overload;
procedure dxGpEndPaint(var AGraphics: TdxGPGraphics);

function dxGetImageDataFormat(const AFormatId: TGUID): TdxImageDataFormat;
function dxGetImageDataFormatExtension(AImageDataFormat: TdxImageDataFormat): string;
function dxGetImageEncoder(AImageDataFormat: TdxImageDataFormat): TGUID; overload;

function dxGPPaintCanvas: TdxGPPaintCanvas;

implementation

uses
  Math, RTLConsts, dxHashUtils;

const
  PenStylesMap: array[psSolid..psDashDotDot] of GpDashStyle = (
    DashStyleSolid, DashStyleDash, DashStyleDot, DashStyleDashDot, DashStyleDashDotDot
  );

type
  TdxWindowFromDCFunc = function (hDC: HDC): HWND; stdcall;

  TMemoryStreamAccess = class(TMemoryStream);

  { TdxGPAlphaBlendAttributes }

  TdxGPAlphaBlendAttributes = class(TdxGPBase)
  private
    FAlpha: Byte;
    FColorMatrix: TdxGPColorMatrix;
    FHandle: GpImageAttributes;
    procedure SetAlpha(AValue: Byte);
  public
    constructor Create; virtual;
    destructor Destroy; override;
    //
    property Alpha: Byte read FAlpha write SetAlpha;
    property Handle: GpImageAttributes read FHandle;
  end;

  { TdxGPImageDataHelper }

  TdxGPImageDataHelper = class
  public
    class function IsBitmapStream(AStream: TStream): Boolean;
  end;

var
  FWindowFromDC: TdxWindowFromDCFunc;
  GPAlphaBlendAttributes: TdxGPAlphaBlendAttributes;
  GPPaintCanvas: TdxGPPaintCanvas;

function dxWindowFromDC(DC: HDC): HWND;
begin
  if Assigned(FWindowFromDC) then
    Result := FWindowFromDC(DC)
  else
    Result := 0;
end;

function dxGpCreateBrush(out ABrushHandle: GpBrush; ABrushColor: TColor; ABrushColorAlpha: Byte = 255): Boolean;
begin
  Result := ABrushColor <> clNone;
  if Result then
    GdipCheck(GdipCreateSolidFill(dxColorToAlphaColor(ABrushColor, ABrushColorAlpha), ABrushHandle));
end;

function dxGpCreatePen(out APenHandle: GpPen; APenColor: TColor; APenWidth: Single;
  APenStyle: TPenStyle = psSolid; APenColorAlpha: Byte = 255): Boolean;
begin
  Result := (APenColor <> clNone) and (APenWidth > 0) and (APenStyle >= psSolid) and (APenStyle <= psDashDotDot);
  if Result then
  begin
    GdipCheck(GdipCreatePen1(dxColorToAlphaColor(APenColor, APenColorAlpha), APenWidth, guPixel, APenHandle));
    GdipCheck(GdipSetPenDashStyle(APenHandle, PenStylesMap[APenStyle]));
  end;
end;

function dxGpGetAlphaBlendAttributes(const AAlpha: Byte): TdxGPAlphaBlendAttributes;
begin
  if GPAlphaBlendAttributes = nil then
    GPAlphaBlendAttributes := TdxGPAlphaBlendAttributes.Create;
  GPAlphaBlendAttributes.Alpha := AAlpha;
  Result := GPAlphaBlendAttributes;
end;

function dxCloneStream(AStream: TdxGPMemoryStream): TdxGPMemoryStream;
var
  APrevStreamPosition: Int64;
begin
  if AStream <> nil then
  begin
    APrevStreamPosition := AStream.Position;
    try
      AStream.Position := 0;
      Result := TdxGPMemoryStream.Create;
      Result.Size := AStream.Size;
      Result.CopyFrom(AStream, AStream.Size);
      Result.Position := 0;
    finally
      AStream.Position := APrevStreamPosition;
    end;
  end
  else
    Result := nil;
end;

function dxGpBeginPaint(AHandle: GpGraphics): TdxGPGraphics;
begin
  Result := TdxGPCustomPaintCanvas.Create(nil);
  TdxGPCustomPaintCanvas(Result).BeginPaint(AHandle);
end;

function dxGpBeginPaint(DC: HDC; const R: TRect): TdxGPGraphics;
begin
  Result := TdxGPCustomPaintCanvas.Create(nil);
  TdxGPCustomPaintCanvas(Result).BeginPaint(DC, R);
end;

procedure dxGpEndPaint(var AGraphics: TdxGPGraphics);
begin
  (AGraphics as TdxGPCustomPaintCanvas).EndPaint;
  FreeAndNil(AGraphics);
end;

function dxGpGetCanvasHandle(ACanvas: TdxGPCanvas): GpGraphics;
begin
  if ACanvas <> nil then
    Result := ACanvas.Handle
  else
    Result := nil;
end;

function dxGetImageDataFormat(const AFormatId: TGUID): TdxImageDataFormat;
begin
  if IsEqualGUID(AFormatId, ImageFormatBMP) or IsEqualGUID(AFormatId, ImageFormatMemoryBMP) then
    Result := dxImageBitmap
  else

  if IsEqualGUID(AFormatId, ImageFormatJPEG) then
    Result := dxImageJpeg
  else

  if IsEqualGUID(AFormatId, ImageFormatPNG) then
    Result := dxImagePng
  else

  if IsEqualGUID(AFormatId, ImageFormatTIFF) then
    Result := dxImageTiff
  else

  if IsEqualGUID(AFormatId, ImageFormatGIF) then
    Result := dxImageGif
  else

  if IsEqualGUID(AFormatId, ImageFormatIcon) then
    Result := dxImageBitmap
  else
    Result := dxImageUnknown;
end;

function dxGetImageDataFormatExtension(AImageDataFormat: TdxImageDataFormat): string;
const
  Map: array[TdxImageDataFormat] of string = ('', '.bmp', '.jpg', '.png', '.tif', '.gif');
begin
  Result := Map[AImageDataFormat];
end;

function dxGetImageEncoder(AImageDataFormat: TdxImageDataFormat): TGUID;
begin
  case AImageDataFormat of
    dxImageBitmap:
      Result := BMPCodec;
    dxImageJpeg:
      Result := JPEGCodec;
    dxImagePng:
      Result := PNGCodec;
    dxImageTiff:
      Result := TIFFCodec;
    dxImageGif:
      Result := GIFCodec;
  else
    raise EdxException.Create('dxGetImageEncoder fails');
  end;
end;

procedure dxGpDrawImage(AGraphics: GpGraphics; const ADestRect: TRect;
  const ASourceRect: TRect; AImage: GpImage; AAlpha: Byte = 255);
var
  AAttributes: GpImageAttributes;
  DstH, DstW, SrcH, SrcW: Integer;
begin
  SrcW := ASourceRect.Right - ASourceRect.Left;
  SrcH := ASourceRect.Bottom - ASourceRect.Top;
  DstW := ADestRect.Right - ADestRect.Left;
  DstH := ADestRect.Bottom - ADestRect.Top;
  if (SrcW < 1) or (SrcH < 1) or (DstW < 1) or (DstH < 1) then Exit;
  if (DstW > SrcW) and (SrcW > 1) then Dec(SrcW);
  if (DstH > SrcH) and (SrcH > 1) then Dec(SrcH);
  if dxGpIsRectVisible(AGraphics, ADestRect) then
  begin
    if AAlpha < 255 then
      AAttributes := dxGpGetAlphaBlendAttributes(AAlpha).Handle
    else
      AAttributes := nil;

    GdipCheck(GdipDrawImageRectRectI(AGraphics, AImage, ADestRect.Left,
      ADestRect.Top, DstW, DstH, ASourceRect.Left, ASourceRect.Top, SrcW, SrcH,
      guPixel, AAttributes, nil, nil));
  end;
end;

procedure dxGpDrawImage(AGraphics: GpGraphics; const ADestRect: TdxRectF;
  const ASourceRect: TdxRectF; AImage: GpImage; AAlpha: Byte = 255);
var
  AAttributes: GpImageAttributes;
  DstH, DstW, SrcH, SrcW: Single;
begin
  SrcW := ASourceRect.Right - ASourceRect.Left;
  SrcH := ASourceRect.Bottom - ASourceRect.Top;
  DstW := ADestRect.Right - ADestRect.Left;
  DstH := ADestRect.Bottom - ADestRect.Top;

  if (SrcW < 1) or (SrcH < 1) or (DstW < 1) or (DstH < 1) then
    Exit;
  if (DstW > SrcW) and (SrcW > 1) then
    SrcW := SrcW - 1;
  if (DstH > SrcH) and (SrcH > 1) then
    SrcH := SrcH - 1;

  if dxGpIsRectVisible(AGraphics, ADestRect) then
  begin
    if AAlpha < 255 then
      AAttributes := dxGpGetAlphaBlendAttributes(AAlpha).Handle
    else
      AAttributes := nil;

    GdipCheck(GdipDrawImageRectRect(AGraphics, AImage, ADestRect.Left,
      ADestRect.Top, DstW, DstH, ASourceRect.Left, ASourceRect.Top, SrcW, SrcH,
      guPixel, AAttributes, nil, nil));
  end;
end;

procedure dxGPDrawText(AGraphics: TdxGPGraphics; const AText: string;
  const ARect: TRect; AFont: TFont; ATextColor: TdxAlphaColor;
  AHorzAlignment: TAlignment; AVertAlignment: TVerticalAlignment; AWordWrap: Boolean;
  ARendering: TdxGpTextRenderingHint);
begin
  dxGPDrawText(AGraphics, AText, cxRectF(ARect), AFont, ATextColor, AHorzAlignment, AVertAlignment, AWordWrap, ARendering);
end;

procedure dxGPDrawText(AGraphics: TdxGPGraphics; const AText: string;
  const ARect: TdxRectF; AFont: TFont; ATextColor: TdxAlphaColor;
  AHorzAlignment: TAlignment = taLeftJustify; AVertAlignment: TVerticalAlignment = taVerticalCenter;
  AWordWrap: Boolean = False; ARendering: TdxGpTextRenderingHint = TextRenderingHintSystemDefault);
const
  WordWrapMap: array[Boolean] of TdxGpStringFormatFlags = (StringFormatFlagsNoWrap, StringFormatFlagsNone);
  HorzAlignmentMap: array[TAlignment] of TdxGpStringAlignment = (StringAlignmentNear, StringAlignmentFar, StringAlignmentCenter);
  VertAlignmentMap: array[TVerticalAlignment] of TdxGpStringAlignment = (StringAlignmentNear, StringAlignmentFar, StringAlignmentCenter);
var
  AGpBrush: GpBrush;
  AGpFont: GpFont;
  AGpRectF: TdxGpRectF;
  AStringFormat: GpStringFormat;
begin
  if cxRectIsEmpty(ARect) or (AText = '') then Exit;

  GdipCheck(GdipCreateStringFormat(Integer(WordWrapMap[AWordWrap]), LANG_NEUTRAL, AStringFormat));
  GdipCheck(GdipCreateSolidFill(ATextColor, AGpBrush));
  AGpFont := dxGpCreateFont(AFont);
  try
    GdipCheck(GdipSetStringFormatAlign(AStringFormat, HorzAlignmentMap[AHorzAlignment]));
    GdipCheck(GdipSetStringFormatLineAlign(AStringFormat, VertAlignmentMap[AVertAlignment]));
    GdipCheck(GdipSetTextRenderingHint(AGraphics.Handle, ARendering));

    AGpRectF := MakeRect(ARect.Left, ARect.Top, cxRectWidth(ARect), cxRectHeight(ARect));
    GdipCheck(GdipDrawString(AGraphics.Handle, PWideChar(WideString(AText)), -1, AGpFont, @AGpRectF, AStringFormat, AGpBrush));
  finally
    GdipDeleteFont(AGpFont);
    GdipDeleteBrush(AGpBrush);
    GdipDeleteStringFormat(AStringFormat);
  end;
end;

procedure dxGPDrawGlowText(AGraphics: TdxGPGraphics; const AText: string;
  const ARect: TRect; AFont: TFont; ATextColor, ATextGlowColor: TdxAlphaColor);
const
  StrokeWidth = 4;
var
  APath: TdxGPPath;
  ADpiY: Single;
  ABrush: GpBrush;
  APen: GpPen;
begin
  if cxRectIsEmpty(ARect) or (AText = '') then Exit;

  GdipCheck(GdipGetDpiY(AGraphics.Handle, ADpiY));
  APath := TdxGPPath.Create;
  try
    APath.AddString(PWideChar({$IFNDEF DELPHI14}WideString{$ENDIF}(AText)), AFont, ADpiY * AFont.Size / 72,
      cxRectSetNullOrigin(ARect));
    AGraphics.SmoothingMode := smAntiAlias;
    AGraphics.TranslateWorldTransform(ARect.Left, ARect.Top);
    try
      GdipCheck(GdipCreatePen1(ATextGlowColor, StrokeWidth, guPixel, APen));
      GdipCheck(GdipSetPenDashStyle(APen, DashStyleSolid));
      GdipCheck(GdipSetPenLineJoin(APen, LineJoinRound));
      GdipCheck(GdipDrawPath(AGraphics.Handle, APen, APath.Handle));
      GdipCheck(GdipDeletePen(APen));

      GdipCheck(GdipCreateSolidFill(ATextColor, ABrush));
      GdipCheck(GdipFillPath(AGraphics.Handle, ABrush, APath.Handle));
      GdipCheck(GdipDeleteBrush(ABrush));
    finally
      AGraphics.TranslateWorldTransform(-ARect.Left, -ARect.Top);
    end;
  finally
    APath.Free;
  end;
end;

procedure dxGPGetTextRect(AGraphics: TdxGPGraphics; const AText: string; AFont: TFont;
  AWordWrap: Boolean; const ALayoutRect: TRect; out ATextRect: TRect);
var
  R: TdxRectF;
begin
  dxGPGetTextRect(AGraphics, AText, AFont, AWordWrap, cxRectF(ALayoutRect), R);
  ATextRect := cxRect(Trunc(R.Left), Trunc(R.Top), Ceil(R.Left + cxRectWidth(R)), Ceil(R.Top + cxRectHeight(R)));
end;

procedure dxGPGetTextRect(AGraphics: TdxGPGraphics; const AText: string; AFont: TFont;
  AWordWrap: Boolean; const ALayoutRect: TdxRectF; out ATextRect: TdxRectF);
const
  WordWrapMap: array[Boolean] of TdxGpStringFormatFlags = (StringFormatFlagsNoWrap, StringFormatFlagsNone);
var
  ALayoutRectF, ABoundingBoxF: TdxGpRectF;
  AGpFont: GpFont;
  AStringFormat: GpStringFormat;
  ACodePointsFitted, ALinesFilled: Integer;
begin
  ALayoutRectF := MakeRect(ALayoutRect.Left, ALayoutRect.Top, cxRectWidth(ALayoutRect), cxRectHeight(ALayoutRect));
  GdipCheck(GdipCreateStringFormat(Integer(WordWrapMap[AWordWrap]), LANG_NEUTRAL, AStringFormat));
  AGpFont := dxGpCreateFont(AFont);
  try
    GdipCheck(GdipMeasureString(AGraphics.Handle, PWideChar({$IFNDEF DELPHI14}WideString{$ENDIF}(AText)),
      Length(AText), AGpFont, @ALayoutRectF, AStringFormat, @ABoundingBoxF, ACodePointsFitted, ALinesFilled));
    ATextRect := cxRectF(ABoundingBoxF.X, ABoundingBoxF.Y, ABoundingBoxF.X + ABoundingBoxF.Width,
      ABoundingBoxF.Y + ABoundingBoxF.Height);
  finally
    GdipDeleteFont(AGpFont);
    GdipDeleteStringFormat(AStringFormat);
  end;
end;

procedure dxGpFillRect(DC: HDC; const R: TRect; AColor: TColor; AColorAlpha: Byte = 255);
begin
  dxGPPaintCanvas.BeginPaint(DC, R);
  dxGpFillRect(dxGPPaintCanvas.Handle, R, dxColorToAlphaColor(AColor, AColorAlpha));
  dxGPPaintCanvas.EndPaint;
end;

procedure dxGpFillRect(AGraphics: GpGraphics; const R: TRect; AColor: TdxAlphaColor);
var
  ABrush: GpSolidFill;
  R1: TdxGPRect;
begin
  R1 := MakeRect(R.Left, R.Top, cxRectWidth(R), cxRectHeight(R));
  GdipCheck(GdipCreateSolidFill(AColor, ABrush));
  GdipCheck(GdipFillRectangleI(AGraphics, ABrush, R1.X, R1.Y, R1.Width, R1.Height));
  GdipCheck(GdipDeleteBrush(ABrush));
end;

procedure dxGpFillRectByGradient(DC: HDC; const R: TRect;
  AColor1, AColor2: TColor; AMode: TdxGPLinearGradientMode;
  AColor1Alpha: Byte = 255; AColor2Alpha: Byte = 255);
begin
  if not IsRectEmpty(R) then
  begin
    dxGPPaintCanvas.BeginPaint(DC, R);
    dxGpFillRectByGradient(dxGPPaintCanvas.Handle, R,
      dxColorToAlphaColor(AColor1, AColor1Alpha),
      dxColorToAlphaColor(AColor2, AColor2Alpha), AMode);
    dxGPPaintCanvas.EndPaint;
  end;
end;

procedure dxGpFillRectByGradient(AGraphics: GpGraphics;
  const R: TRect; AColor1, AColor2: TdxAlphaColor; AMode: TdxGPLinearGradientMode);
var
  ABrush: GpLineGradient;
  ABrushRect: TdxGPRect;
begin
  // Inflate: Avoid GDIPlus gradient fill bug
  ABrushRect := MakeRect(R.Left - 1, R.Top - 1, R.Right - R.Left + 2, R.Bottom - R.Top + 2);
  GdipCheck(GdipCreateLineBrushFromRectI(@ABrushRect, AColor1, AColor2, AMode, WrapModeTile, ABrush));
  GdipCheck(GdipFillRectangleI(AGraphics, ABrush, R.Left, R.Top, R.Right - R.Left, R.Bottom - R.Top));
  GdipCheck(GdipDeleteBrush(ABrush));
end;

function dxGpIsDoubleBufferedNeeded(DC: HDC): Boolean;
var
  AWindowHandle: HWND;
begin
  Result := (GetDeviceCaps(DC, BITSPIXEL) <= 16) or
    (GetDeviceCaps(DC, TECHNOLOGY) = DT_RASPRINTER) or
    (GetDeviceCaps(DC, NUMCOLORS) > 1);

  if not (Result or IsWinVistaOrLater) then 
  begin
    AWindowHandle := dxWindowFromDC(DC);
    if AWindowHandle <> 0 then
      Result := GetWindowLong(AWindowHandle, GWL_EXSTYLE) and WS_EX_LAYERED <> 0;
  end;
end;

function dxGpIsRectVisible(AGraphics: GpGraphics; const R: TdxRectF): LongBool;
begin
  GdipCheck(GdipIsVisibleRect(AGraphics, R.Left, R.Top, R.Right - R.Left, R.Bottom - R.Top, Result));
end;

function dxGpIsRectVisible(AGraphics: GpGraphics; const R: TRect): LongBool;
begin
  GdipCheck(GdipIsVisibleRectI(AGraphics, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R), Result));
end;

procedure dxGpTilePart(DC: HDC; const ADestRect, ASourceRect: TRect; AImage: GpBitmap);
begin
  dxGPPaintCanvas.BeginPaint(DC, ADestRect);
  dxGpTilePartEx(dxGPPaintCanvas.Handle, ADestRect, ASourceRect, AImage);
  dxGPPaintCanvas.EndPaint;
end;

procedure dxGpTilePartEx(AGraphics: GpGraphics;
  const ADestRect, ASourceRect: TRect; AImage: GpBitmap; AAlpha: Byte = 255);

  function CreateTextureBrush(const R: TRect; out ATexture: GpTexture): Boolean;
  begin
    Result := GdipCreateTexture2I(AImage, WrapModeTile,
      R.Left, R.Top, cxRectWidth(R), cxRectHeight(R), ATexture) = Ok;
  end;

  procedure ManualTilePart(const ADest, ASource: TRect;
    ADestWidth, ADestHeight, ASourceWidth, ASourceHeight: Integer);
  var
    ALastCol, ALastRow, ACol, ARow: Integer;
    RDest, RSrc: TRect;
  begin
    ALastCol := ADestWidth div ASourceWidth - Ord(ADestWidth mod ASourceWidth = 0);
    ALastRow := ADestHeight div ASourceHeight - Ord(ADestHeight mod ASourceHeight = 0);
    for ARow := 0 to ALastRow do
    begin
      RSrc.Top := ASource.Top;
      RSrc.Bottom := ASource.Bottom;
      RDest.Top := ADest.Top + ASourceHeight * ARow;
      RDest.Bottom := RDest.Top + ASourceHeight;
      if RDest.Bottom > ADest.Bottom then
      begin
        Dec(RSrc.Bottom, RDest.Bottom - ADest.Bottom);
        RDest.Bottom := ADest.Bottom;
      end;
      for ACol := 0 to ALastCol do
      begin
        RSrc.Left := ASource.Left;
        RSrc.Right := ASource.Right;
        RDest.Left := ADest.Left + ASourceWidth * ACol;
        RDest.Right := RDest.Left + ASourceWidth;
        if RDest.Right > ADest.Right then
        begin
          Dec(RSrc.Right, RDest.Right - ADest.Right);
          RDest.Right := ADest.Right;
        end;
        dxGpDrawImage(AGraphics, RDest, RSrc, AImage, AAlpha);
      end;
    end;
  end;

  function TilePartByBrush(const R, ASource: TRect): Boolean;
  var
    ABitmap: GpBitmap;
    ABitmapGraphics: GpGraphics;
    ATexture: GpTexture;
    AWidth, AHeight: Integer;
  begin
    Result := CreateTextureBrush(ASource, ATexture);
    if Result then
    try
      AWidth := cxRectWidth(R);
      AHeight := cxRectHeight(R);
      GdipCheck(GdipCreateBitmapFromScan0(AWidth, AHeight, 0, PixelFormat32bppPARGB, nil, ABitmap));
      try
        GdipCheck(GdipGetImageGraphicsContext(ABitmap, ABitmapGraphics));
        GdipCheck(GdipFillRectangleI(ABitmapGraphics, ATexture, 0, 0, AWidth, AHeight));
        GdipCheck(GdipDrawImageRectI(AGraphics, ABitmap, R.Left, R.Top, AWidth, AHeight));
        GdipCheck(GdipDeleteGraphics(ABitmapGraphics));
      finally
        GdipCheck(GdipDisposeImage(ABitmap));
      end;
    finally
      GdipCheck(GdipDeleteBrush(ATexture));
    end;
  end;

var
  ADestWidth, ADestHeight: Integer;
  ASourceWidth, ASourceHeight: Integer;
begin
  if not IsRectEmpty(ASourceRect) and dxGpIsRectVisible(AGraphics, ADestRect) then
  begin
    ADestWidth := cxRectWidth(ADestRect);
    ADestHeight := cxRectHeight(ADestRect);
    ASourceWidth := cxRectWidth(ASourceRect);
    ASourceHeight := cxRectHeight(ASourceRect);
    if (AAlpha <> 255) or (ADestWidth <= ASourceWidth) and (ADestHeight <= ASourceHeight) or
      not TilePartByBrush(ADestRect, ASourceRect)
    then
      ManualTilePart(ADestRect, ASourceRect, ADestWidth, ADestHeight, ASourceWidth, ASourceHeight);
  end;
end;

procedure dxGpResetRect(AGraphics: GpGraphics; const R: TRect);
var
  ACompositionMode: Integer;
begin
  GdipCheck(GdipGetCompositingMode(AGraphics, ACompositionMode));
  try
    GdipCheck(GdipSetCompositingMode(AGraphics, CompositingModeSourceCopy));
    dxGpFillRect(AGraphics, R, 0);
  finally
    GdipCheck(GdipSetCompositingMode(AGraphics, ACompositionMode))
  end;
end;

procedure dxGpRoundRect(DC: HDC; const R: TRect; APenColor: TColor;
  ABrushColor: TColor; ARadius: Integer; APenWidth: Integer = 1;
  APenColorAlpha: Byte = 255; ABrushColorAlpha: Byte = 255);
begin
  dxGPPaintCanvas.BeginPaint(DC, R);
  try
    dxGPPaintCanvas.RoundRect(R, APenColor, ABrushColor,
      ARadius, ARadius, APenWidth, APenColorAlpha, ABrushColorAlpha);
  finally
    dxGPPaintCanvas.EndPaint;
  end;
end;

function dxGPPaintCanvas: TdxGPPaintCanvas;
begin
  if GPPaintCanvas = nil then
    GPPaintCanvas := TdxGPPaintCanvas.Create(nil);
  Result := GPPaintCanvas;
end;

{ TdxGPCustomGraphicObject }

constructor TdxGPCustomGraphicObject.Create;
begin
  // do nothing
end;

procedure TdxGPCustomGraphicObject.Assign(ASource: TdxGPCustomGraphicObject);
begin
  // do nothing
end;

procedure TdxGPCustomGraphicObject.BeforeDestruction;
begin
  inherited BeforeDestruction;
  FreeHandle;
end;

procedure TdxGPCustomGraphicObject.FreeHandle;
begin
  if HandleAllocated then
  begin
    DoFreeHandle(FHandle);
    FHandle := nil;
  end;
end;

procedure TdxGPCustomGraphicObject.HandleNeeded;
begin
  if FHandle = nil then
    DoCreateHandle(FHandle);
end;

procedure TdxGPCustomGraphicObject.Changed;
begin
  dxCallNotify(OnChange, Self);
end;

function TdxGPCustomGraphicObject.GetHandle: GpHandle;
begin
  HandleNeeded;
  Result := FHandle;
end;

function TdxGPCustomGraphicObject.GetHandleAllocated: Boolean;
begin
  Result := FHandle <> nil;
end;

{ TdxGPMatrix }

constructor TdxGPMatrix.CreateEx(M11, M12, M21, M22, DX, DY: Single);
begin
  Create;
  SetElements(M11, M12, M21, M22, DX, DY);
end;

constructor TdxGPMatrix.CreateFlip(AFlipHorizontally, AFlipVertically: Boolean; const APivotPointX, APivotPointY: Single);
const
  FlipValueMap: array[Boolean] of Integer = (1, -1);
begin
  CreateEx(FlipValueMap[AFlipHorizontally], 0, 0, FlipValueMap[AFlipVertically],
    IfThen(AFlipHorizontally, 2 * APivotPointX), IfThen(AFlipVertically, 2 * APivotPointY));
end;

constructor TdxGPMatrix.CreateFlip(AFlipHorizontally, AFlipVertically: Boolean; const APivotPoint: TdxPointF);
begin
  CreateFlip(AFlipHorizontally, AFlipVertically, APivotPoint.X, APivotPoint.Y);
end;

procedure TdxGPMatrix.Assign(ASource: TdxGPCustomGraphicObject);
var
  M11, M12, M21, M22, DX, DY: Single;
begin
  inherited Assign(ASource);
  if ASource is TdxGPMatrix then
  begin
    TdxGPMatrix(ASource).GetElements(M11, M12, M21, M22, DX, DY);
    SetElements(M11, M12, M21, M22, DX, DY);
  end
end;

procedure TdxGPMatrix.GetElements(out M11, M12, M21, M22, DX, DY: Single);
var
  AElements: array[0..5] of Single;
begin
  GdipCheck(GdipGetMatrixElements(Handle, @AElements[0]));
  M11 := AElements[0];
  M12 := AElements[1];
  M21 := AElements[2];
  M22 := AElements[3];
  DX := AElements[4];
  DY := AElements[5];
end;

procedure TdxGPMatrix.SetElements(M11, M12, M21, M22, DX, DY: Single);
begin
  GdipCheck(GdipSetMatrixElements(Handle, M11, M12, M21, M22, DX, DY));
  Changed;
end;

procedure TdxGPMatrix.Flip(AFlipHorizontally, AFlipVertically: Boolean; const APivotPoint: TdxPointF);
var
  AMatrix: TdxGPMatrix;
begin
  if AFlipHorizontally or AFlipVertically then
  begin
    AMatrix := TdxGPMatrix.CreateFlip(AFlipHorizontally, AFlipVertically, APivotPoint.X, APivotPoint.Y);
    try
      Multiply(AMatrix, MatrixOrderAppend);
    finally
      AMatrix.Free;
    end;
  end;
end;

procedure TdxGPMatrix.Invert;
begin
  GdipCheck(GdipInvertMatrix(Handle));
  Changed;
end;

function TdxGPMatrix.GetBoundingRectangle(const R: TdxRectF): TdxRectF;
var
  ATopLeft, ATopRight, ABottomLeft, ABottomRight: TdxPointF;
begin
  ATopLeft := TransformPoint(dxPointF(R.Left, R.Top));
  ATopRight := TransformPoint(dxPointF(R.Right, R.Top));
  ABottomLeft := TransformPoint(dxPointF(R.Left, R.Bottom));
  ABottomRight := TransformPoint(dxPointF(R.Right, R.Bottom));

  Result := dxRectF(
    Min(Min(ATopLeft.X, ATopRight.X), Min(ABottomLeft.X, ABottomRight.X)),
    Min(Min(ATopLeft.Y, ATopRight.Y), Min(ABottomLeft.Y, ABottomRight.Y)),
    Max(Max(ATopLeft.X, ATopRight.X), Max(ABottomLeft.X, ABottomRight.X)),
    Max(Max(ATopLeft.Y, ATopRight.Y), Max(ABottomLeft.Y, ABottomRight.Y)));
end;

procedure TdxGPMatrix.Multiply(AMatrix: TdxGPMatrix; AMatrixOrder: TdxGpMatrixOrder);
begin
  GdipCheck(GdipMultiplyMatrix(Handle, AMatrix.Handle, AMatrixOrder));
  Changed;
end;

procedure TdxGPMatrix.Reset;
begin
  SetElements(1, 0, 0, 1, 0, 0);
end;

procedure TdxGPMatrix.Rotate(AAngle: Single; AMatrixOrder: TdxGpMatrixOrder = MatrixOrderPrepend);
begin
  GdipCheck(GdipRotateMatrix(Handle, AAngle, AMatrixOrder));
  Changed;
end;

procedure TdxGPMatrix.Rotate(AAngle: Single; const APivotPoint: TdxPointF; AMatrixOrder: TdxGpMatrixOrder = MatrixOrderPrepend);
const
  TranslateDirectionMap: array[TdxGpMatrixOrder] of Integer = (1, -1);
begin
  Translate(TranslateDirectionMap[AMatrixOrder] * APivotPoint.X,
    TranslateDirectionMap[AMatrixOrder] * APivotPoint.Y, AMatrixOrder);
  Rotate(AAngle, AMatrixOrder);
  Translate(-TranslateDirectionMap[AMatrixOrder] * APivotPoint.X,
    -TranslateDirectionMap[AMatrixOrder] * APivotPoint.Y, AMatrixOrder);
end;

procedure TdxGPMatrix.Scale(const AScale: TdxPointF; AMatrixOrder: TdxGpMatrixOrder = MatrixOrderPrepend);
begin
  Scale(AScale.X, AScale.Y, AMatrixOrder);
end;

procedure TdxGPMatrix.Scale(const AScaleX, AScaleY: Single; AMatrixOrder: TdxGpMatrixOrder);
begin
  GdipCheck(GdipScaleMatrix(Handle, AScaleX, AScaleY, AMatrixOrder));
  Changed;
end;

procedure TdxGPMatrix.Shear(const AShear: TdxPointF; AMatrixOrder: TdxGpMatrixOrder = MatrixOrderPrepend);
begin
  Shear(AShear.X, AShear.Y, AMatrixOrder);
end;

procedure TdxGPMatrix.Shear(const AShearX, AShearY: Single; AMatrixOrder: TdxGpMatrixOrder);
begin
  GdipCheck(GdipShearMatrix(Handle, AShearX, AShearY, AMatrixOrder));
  Changed;
end;

procedure TdxGPMatrix.Translate(const AOffset: TdxPointF; AMatrixOrder: TdxGpMatrixOrder);
begin
  Translate(AOffset.X, AOffset.Y, AMatrixOrder);
end;

function TdxGPMatrix.TransformPoint(const P: TdxPointF): TdxPointF;
var
  M11, M12, M21, M22, DX, DY: Single;
begin
  GetElements(M11, M12, M21, M22, DX, DY);
  Result.X := P.X * M11 + P.Y * M21 + DX;
  Result.Y := P.X * M12 + P.Y * M22 + DY;
end;

function TdxGPMatrix.TransformPoint(const P: TPoint): TPoint;
begin
  Result := cxPoint(TransformPoint(dxPointF(P)), False);
end;

function TdxGPMatrix.TransformRect(const R: TdxRectF): TdxRectF;
{$IFNDEF DELPHI14}
var
  P: TdxPointF;
{$ENDIF}
begin
{$IFDEF DELPHI14}
  Result.TopLeft := TransformPoint(R.TopLeft);
  Result.BottomRight := TransformPoint(R.BottomRight);
{$ELSE}
  P := TransformPoint(cxPointF(R.Left, R.Top));
  Result.Left := P.X;
  Result.Top := P.Y;

  P := TransformPoint(cxPointF(R.Right, R.Bottom));
  Result.Right := P.X;
  Result.Bottom := P.Y;
{$ENDIF}
end;

function TdxGPMatrix.TransformRect(const R: TRect): TRect;
begin
  Result.TopLeft := TransformPoint(R.TopLeft);
  Result.BottomRight := TransformPoint(R.BottomRight);
end;

procedure TdxGPMatrix.Translate(const AOffsetX, AOffsetY: Single; AMatrixOrder: TdxGpMatrixOrder);
begin
  GdipCheck(GdipTranslateMatrix(Handle, AOffsetX, AOffsetY, AMatrixOrder));
  Changed;
end;

procedure TdxGPMatrix.DoCreateHandle(out AHandle: GpHandle);
begin
  GdipCheck(GdipCreateMatrix(AHandle));
end;

procedure TdxGPMatrix.DoFreeHandle(AHandle: GpHandle);
begin
  GdipCheck(GdipDeleteMatrix(AHandle));
end;

function TdxGPMatrix.GetIsIdentity: Boolean;
var
  AValue: LongBool;
begin
  GdipCheck(GdipIsMatrixIdentity(Handle, AValue));
  Result := AValue;
end;

function TdxGPMatrix.GetIsInvertible: Boolean;
var
  AValue: LongBool;
begin
  GdipCheck(GdipIsMatrixInvertible(Handle, AValue));
  Result := AValue;
end;

{ TdxGPBrushGradientPoints }

constructor TdxGPBrushGradientPoints.Create;
begin
  inherited Create;
  SetLength(FColors, 2);
  SetLength(FOffsets, 2);
end;

function TdxGPBrushGradientPoints.Add(AOffset: Single; AColor: TdxAlphaColor): Integer;
begin
  if Count = Capacity then
    Grow;
  Result := InternalAdd(AOffset, AColor);
  Changed;
end;

procedure TdxGPBrushGradientPoints.Assign(Source: TdxGPBrushGradientPoints);
var
  I: Integer;
begin
  if (Source <> nil) and (Source <> Self) then
  begin
    Clear;
    Capacity := Source.Count;
    for I := 0 to Source.Count - 1 do
      InternalAdd(Source.Offsets[I], Source.Colors[I]);
    Changed;
  end;
end;

procedure TdxGPBrushGradientPoints.Clear;
begin
  Capacity := 0;
end;

procedure TdxGPBrushGradientPoints.Delete(Index: Integer);
var
  I: Integer;
begin
  CheckIndex(Index);
  for I := Index to Count - 1 do
  begin
    FColors[I + 1] := FColors[I + 2];
    FOffsets[I + 1] := FOffsets[I + 2];
  end;
  Dec(FCount);
  Changed;
end;

procedure TdxGPBrushGradientPoints.InvertOrder;
var
  I: Integer;
begin
  if Count > 1 then
  begin
    for I := 0 to Count div 2 - 1 do
      ExchangeLongWords(FColors[I + 1], FColors[Count - I]);
    for I := 0 to Count div 2 - 1 do
      ExchangeLongWords(FOffsets[I + 1], FOffsets[Count - I]);
    for I := 0 to Count - 1 do
      FOffsets[I + 1] := 1 - FOffsets[I + 1];
    Changed;
  end;
end;

procedure TdxGPBrushGradientPoints.CalculateParams(out AColors: PdxAlphaColor; out AOffsets: PSingle; out ACount: Integer);
begin
  ACount := Count;
  if Offsets[0] > 0 then
  begin
    AOffsets := @FOffsets[0];
    AColors := @FColors[0];
    Inc(ACount);
  end
  else
  begin
    AOffsets := @FOffsets[1];
    AColors := @FColors[1];
  end;

  if Offsets[Count - 1] < 1 then
  begin
    FColors[Count + 1] := 0;
    FOffsets[Count + 1] := 1;
    Inc(ACount);
  end;
end;

procedure TdxGPBrushGradientPoints.Changed;
begin
  ValidateOrder;
  dxCallNotify(OnChange, Self);
end;

procedure TdxGPBrushGradientPoints.ValidateOrder;

  procedure Sort(L, R: Integer);
  var
    I, J, P: Integer;
  begin
    repeat
      I := L;
      J := R;
      P := (L + R) shr 1;
      repeat
        while FOffsets[I] < FOffsets[P] do
          Inc(I);
        while FOffsets[J] > FOffsets[P] do
          Dec(J);

        if I <= J then
        begin
          if I <> J then
          begin
            ExchangeLongWords(FColors[I], FColors[J]);
            ExchangeLongWords(FOffsets[I], FOffsets[J]);
          end;

          if P = I then
            P := J
          else
            if P = J then
              P := I;

          Inc(I);
          Dec(J);
        end;
      until I > J;
      if L < J then
        Sort(L, J);
      L := I;
    until I >= R;
  end;

begin
  Sort(1, Count);
end;

procedure TdxGPBrushGradientPoints.CheckIndex(Index: Integer);
begin
  if (Index < 0) or (Index >= Count) then
    raise Exception.CreateFmt(SListIndexError, [Index]);
end;

procedure TdxGPBrushGradientPoints.Grow;
var
  ADelta: Integer;
begin
  if Capacity > 128 then
    ADelta := FCapacity shr 1
  else
    if Capacity > 16 then
      ADelta := 32
    else
      ADelta := 16;

  Capacity := Capacity + ADelta;
end;

function TdxGPBrushGradientPoints.GetColor(Index: Integer): TdxAlphaColor;
begin
  CheckIndex(Index);
  Result := FColors[Index + 1];
end;

function TdxGPBrushGradientPoints.GetOffset(Index: Integer): Single;
begin
  CheckIndex(Index);
  Result := FOffsets[Index + 1];
end;

function TdxGPBrushGradientPoints.InternalAdd(AOffset: Single; AColor: TdxAlphaColor): Integer;
begin
  Result := Count;
  FOffsets[Result + 1] := AOffset;
  FColors[Result + 1] := AColor;
  Inc(FCount);
end;

procedure TdxGPBrushGradientPoints.SetCapacity(AValue: Integer);
begin
  AValue := Max(AValue, 0);
  if AValue <> FCapacity then
  begin
    FCapacity := AValue;
    SetLength(FOffsets, Capacity + 2);
    SetLength(FColors, Capacity + 2);
    if Count > Capacity then
    begin
      FCount := Capacity;
      Changed;
    end;
  end;
end;

procedure TdxGPBrushGradientPoints.SetColor(Index: Integer; const AValue: TdxAlphaColor);
begin
  CheckIndex(Index);
  if FColors[Index + 1] <> AValue then
  begin
    FColors[Index + 1] := AValue;
    Changed;
  end;
end;

procedure TdxGPBrushGradientPoints.SetOffset(Index: Integer; const AValue: Single);
begin
  CheckIndex(Index);
  if not SameValue(FOffsets[Index + 1], AValue) then
  begin
    FOffsets[Index + 1] := AValue;
    Changed;
  end;
end;

{ TdxGPCustomBrush }

procedure TdxGPCustomBrush.Assign(ASource: TdxGPCustomGraphicObject);
begin
  inherited Assign(ASource);
  if ASource is TdxGPCustomBrush then
    SetTargetRect(TdxGPCustomBrush(ASource).TargetRect);
end;

procedure TdxGPCustomBrush.SetTargetRect(const R: TRect);
begin
  SetTargetRect(dxGpMakeRectF(R));
end;

procedure TdxGPCustomBrush.SetTargetRect(const R: TdxRectF);
begin
  SetTargetRect(MakeRect(R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
end;

procedure TdxGPCustomBrush.SetTargetRect(const R: TdxGpRect);
begin
  SetTargetRect(dxGpMakeRectF(R));
end;

procedure TdxGPCustomBrush.SetTargetRect(const R: TdxGpRectF);
var
  ARect: TdxGpRectF;
begin
  // Inflate: Avoid GDIPlus gradient fill bug
  ARect := MakeRect(R.X - 1, R.Y - 1, R.Width + 2, R.Height + 2);
  if not dxGpRectIsEqual(ARect, FTargetRect) then
  begin
    FTargetRect := ARect;
    DoTargetRectChanged;
  end;
end;

procedure TdxGPCustomBrush.DoFreeHandle(AHandle: GpHandle);
begin
  GdipCheck(GdipDeleteBrush(AHandle));
end;

procedure TdxGPCustomBrush.DoTargetRectChanged;
begin
  if NeedRecreateHandleOnTargetRectChange then
    FreeHandle;
end;

function TdxGPCustomBrush.NeedRecreateHandleOnTargetRectChange: Boolean;
begin
  Result := False;
end;

{ TdxGPBrush }

constructor TdxGPBrush.Create;
begin
  inherited Create;
  FGradientPoints := TdxGPBrushGradientPoints.Create;
  FGradientPoints.OnChange := PointsChangeHandler;
  FTexture := TdxGpImage.Create;
  FTexture.OnChange := TextureChangeHandler;
end;

destructor TdxGPBrush.Destroy;
begin
  FreeAndNil(FGradientPoints);
  FreeAndNil(FTexture);
  inherited Destroy;
end;

procedure TdxGPBrush.Assign(ASource: TdxGPCustomGraphicObject);
begin
  inherited Assign(ASource);
  if ASource is TdxGPBrush then
  begin
    Color := TdxGPBrush(ASource).Color;
    GradientMode := TdxGPBrush(ASource).GradientMode;
    GradientPoints := TdxGPBrush(ASource).GradientPoints;
    Texture := TdxGPBrush(ASource).Texture;
    Style := TdxGPBrush(ASource).Style;
  end;
end;

procedure TdxGPBrush.CreateEmptyBrushHandle(out AHandle: GpBrush);
begin
  GdipCheck(GdipCreateSolidFill(0, AHandle));
end;

procedure TdxGPBrush.CreateGradientBrushHandle(out AHandle: GpBrush);
const
  Map: array[TdxGPBrushGradientMode] of LinearGradientMode = (
    LinearGradientModeHorizontal, LinearGradientModeVertical,
    LinearGradientModeForwardDiagonal, LinearGradientModeBackwardDiagonal
  );
var
  AColors: PdxAlphaColor;
  ACount: Integer;
  AOffsets: PSingle;
begin
  if (GradientPoints.Count = 0) or (FTargetRect.Width = 0) or (FTargetRect.Height = 0) then
  begin
    CreateEmptyBrushHandle(AHandle);
    Exit;
  end;

  GradientPoints.CalculateParams(AColors, AOffsets, ACount);
  GdipCheck(GdipCreateLineBrushFromRect(@FTargetRect, 0, 0, Map[GradientMode], WrapModeTileFlipX, AHandle));
  GdipCheck(GdipSetLinePresetBlend(AHandle, AColors, AOffsets, ACount));
end;

procedure TdxGPBrush.CreateTextureBrushHandle(out AHandle: GpBrush);
begin
  if Texture.Empty then
    CreateEmptyBrushHandle(AHandle)
  else
    GdipCheck(GdipCreateTexture2I(Texture.Handle, WrapModeTile, 0, 0, Texture.Width, Texture.Height, AHandle));
end;

function TdxGPBrush.GetIsEmpty: Boolean;
begin
  case Style of
    gpbsSolid:
      Result := Color = 0;
    gpbsClear:
      Result := True;
    gpbsGradient:
      Result := GradientPoints.Count = 0;
    gpbsTexture:
      Result := Texture.Empty;
  else
    Result := False;
  end;
end;

procedure TdxGPBrush.DoCreateHandle(out AHandle: GpHandle);
begin
  case Style of
    gpbsSolid:
      GdipCheck(GdipCreateSolidFill(Color, AHandle));
    gpbsClear:
      CreateEmptyBrushHandle(AHandle);
    gpbsGradient:
      CreateGradientBrushHandle(AHandle);
    gpbsTexture:
      CreateTextureBrushHandle(AHandle);
  end;
end;

procedure TdxGPBrush.DoTargetRectChanged;
begin
  inherited DoTargetRectChanged;
  if Style = gpbsTexture then
  begin
    GdipCheck(GdipResetTextureTransform(Handle));
    GdipCheck(GdipTranslateTextureTransform(Handle, FTargetRect.X, FTargetRect.Y, MatrixOrderAppend));
  end;
end;

function TdxGPBrush.NeedRecreateHandleOnTargetRectChange: Boolean;
begin
  Result := Style = gpbsGradient;
end;

procedure TdxGPBrush.SetColor(const AValue: TdxAlphaColor);
begin
  if AValue <> FColor then
  begin
    FColor := AValue;
    if HandleAllocated and (Style = gpbsSolid) then
      GdipCheck(GdipSetSolidFillColor(Handle, Color));
    Changed;
  end;
end;

procedure TdxGPBrush.SetGradientMode(const AValue: TdxGPBrushGradientMode);
begin
  if FGradientMode <> AValue then
  begin
    FGradientMode := AValue;
    if Style = gpbsGradient then
      FreeHandle;
    Changed;
  end;
end;

procedure TdxGPBrush.SetGradientPoints(const AValue: TdxGPBrushGradientPoints);
begin
  FGradientPoints.Assign(AValue);
end;

procedure TdxGPBrush.SetStyle(const AValue: TdxGPBrushStyle);
begin
  if FStyle <> AValue then
  begin
    FStyle := AValue;
    FreeHandle;
    Changed;
  end;
end;

procedure TdxGPBrush.SetTexture(const AValue: TdxGpImage);
begin
  FTexture.Assign(AValue);
end;

procedure TdxGPBrush.PointsChangeHandler(Sender: TObject);
begin
  if Style = gpbsGradient then
    FreeHandle;
  Changed;
end;

procedure TdxGPBrush.TextureChangeHandler(Sender: TObject);
begin
  if Style = gpbsTexture then
    FreeHandle;
  Changed;
end;

{ TdxGPPen }

constructor TdxGPPen.Create;
begin
  inherited Create;
  FBrush := TdxGPPenBrush.Create(Self);
  FWidth := 1;
end;

destructor TdxGPPen.Destroy;
begin
  FreeAndNil(FBrush);
  inherited Destroy;
end;

procedure TdxGPPen.Assign(ASource: TdxGPCustomGraphicObject);
begin
  inherited Assign(ASource);
  if ASource is TdxGPPen then
  begin
    Brush := TdxGPPen(ASource).Brush;
    Style := TdxGPPen(ASource).Style;
    Width := TdxGPPen(ASource).Width;
  end;
end;

procedure TdxGPPen.DoCreateHandle(out AHandle: GpHandle);
begin
  GdipCheck(GdipCreatePen2(Brush.Handle, Width, guPixel, AHandle));
  DoSetDashStyle(AHandle);
end;

procedure TdxGPPen.DoFreeHandle(AHandle: GpHandle);
begin
  GdipCheck(GdipDeletePen(AHandle));
end;

procedure TdxGPPen.DoSetDashStyle(AHandle: GpHandle);
const
  Map: array[TdxGPPenStyle] of TdxGpDashStyle = (
    DashStyleSolid, DashStyleDash, DashStyleDot, DashStyleDashDot, DashStyleDashDotDot
  );
begin
  GdipCheck(GdipSetPenDashStyle(AHandle, Map[Style]));
end;

function TdxGPPen.GetIsEmpty: Boolean;
begin
  Result := IsZero(Width) or Brush.IsEmpty;
end;

procedure TdxGPPen.HandleNeeded;
begin
  Brush.HandleNeeded;
  inherited HandleNeeded;
end;

procedure TdxGPPen.SetBrush(const AValue: TdxGPBrush);
begin
  FBrush.Assign(AValue);
end;

procedure TdxGPPen.SetStyle(const AValue: TdxGPPenStyle);
begin
  if FStyle <> AValue then
  begin
    FStyle := AValue;
    if HandleAllocated then
      DoSetDashStyle(Handle);
    Changed;
  end;
end;

procedure TdxGPPen.SetTargetRect(const R: TdxGpRect);
begin
  Brush.SetTargetRect(R);
end;

procedure TdxGPPen.SetTargetRect(const R: TRect);
begin
  Brush.SetTargetRect(R);
end;

procedure TdxGPPen.SetTargetRect(const R: TdxRectF);
begin
  Brush.SetTargetRect(R);
end;

procedure TdxGPPen.SetWidth(AValue: Single);
begin
  AValue := Max(AValue, 0);
  if FWidth <> AValue then
  begin
    FWidth := AValue;
    if HandleAllocated then
      GdipCheck(GdipSetPenWidth(Handle, Width));
    Changed;
  end;
end;

{ TdxGPPenBrush }

constructor TdxGPPenBrush.Create(APen: TdxGPPen);
begin
  inherited Create;
  FPen := APen;
end;

procedure TdxGPPenBrush.Changed;
begin
  inherited Changed;
  if HandleAllocated then
    UpdatePenFillHandle(FHandle);
  FPen.Changed;
end;

procedure TdxGPPenBrush.DoCreateHandle(out AHandle: Pointer);
begin
  inherited DoCreateHandle(AHandle);
  UpdatePenFillHandle(AHandle);
end;

procedure TdxGPPenBrush.UpdatePenFillHandle(AHandle: GpHandle);
begin
  if FPen.HandleAllocated then
    GdipCheck(GdipSetPenBrushFill(FPen.Handle, AHandle));
end;

{ TdxGPPath }

constructor TdxGPPath.Create;
begin
  Create(gpfmAlternate);
end;

constructor TdxGPPath.Create(AFillMode: TdxGPFillMode);
begin
  inherited Create;
  GdipCheck(GdipCreatePath(FillMode(AFillMode), FHandle));
end;

constructor TdxGPPath.Create(APoints: TdxGpPointFDynArray; APointTypes: TBytes);
begin
  inherited Create;
  GdipCheck(GdipCreatePath2(@APoints[0], @APointTypes[0], Length(APoints), FillModeAlternate, FHandle));
end;

destructor TdxGPPath.Destroy;
begin
  GdipCheck(GdipDeletePath(FHandle));
  inherited Destroy;
end;

procedure TdxGPPath.Flatten(AMatrix: TdxGPMatrix = nil; AFlatness: Single = 0.25);
begin
  GdipCheck(GdipFlattenPath(Handle, AMatrix, AFlatness));
end;

procedure TdxGPPath.FigureFinish;
begin
  GdipCheck(GdipClosePathFigure(Handle));
end;

procedure TdxGPPath.FigureStart;
begin
  GdipCheck(GdipStartPathFigure(Handle));
end;

procedure TdxGPPath.Reset;
begin
  GdipCheck(GdipResetPath(Handle));
end;

procedure TdxGPPath.AddArc(const X, Y, Width, Height, StartAngle, SweepAngle: Single);
begin
  GdipCheck(GdipAddPathArc(Handle, X, Y, Width, Height, StartAngle, SweepAngle));
end;

procedure TdxGPPath.AddEllipse(const R: TRect);
begin
  GdipCheck(GdipAddPathEllipseI(Handle, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
end;

procedure TdxGPPath.AddEllipse(const R: TdxRectF);
begin
  GdipCheck(GdipAddPathEllipse(Handle, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
end;

procedure TdxGPPath.AddLine(const X1, Y1, X2, Y2: Single);
begin
  GdipCheck(GdipAddPathLine(Handle, X1, Y1, X2, Y2));
end;

procedure TdxGPPath.AddPolygon(const APoints: array of TPoint);
begin
  GdipCheck(GdipAddPathPolygonI(Handle, @APoints[0], Length(APoints)));
end;

procedure TdxGPPath.AddPolyline(const APoints: array of TPoint);
begin
  GdipCheck(GdipAddPathLine2I(Handle, @APoints[0], Length(APoints)));
end;

procedure TdxGPPath.AddRect(const R: TRect);
begin
  GdipCheck(GdipAddPathRectangleI(Handle, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
end;

procedure TdxGPPath.AddRect(const R: TdxRectF);
begin
  GdipCheck(GdipAddPathRectangle(Handle, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
end;

procedure TdxGPPath.AddRoundRect(const R: TRect; ARadiusX, ARadiusY: Integer);
begin
  AddRoundRect(dxRectF(R), ARadiusX, ARadiusY);
end;

procedure TdxGPPath.AddRoundRect(const R: TdxRectF; ARadiusX, ARadiusY: Single);
begin
  ARadiusX := Min(ARadiusX, cxRectWidth(R) / 3);
  ARadiusY := Min(ARadiusY, cxRectHeight(R) / 3);

  FigureStart;
  try
    if (ARadiusX > 0) and (ARadiusY > 0) then
    begin
      AddLine(R.Left + ARadiusX, R.Top, R.Right - 2 * ARadiusX, R.Top);
      AddArc(R.Right - 2 * ARadiusX, R.Top, 2 * ARadiusX, 2 * ARadiusY, 270, 90);
      AddLine(R.Right, R.Top + ARadiusY, R.Right, R.Bottom - 2 * ARadiusY);
      AddArc(R.Right - 2 * ARadiusX, R.Bottom - 2 * ARadiusY, 2 * ARadiusX, 2 * ARadiusY, 0, 90);
      AddLine(R.Right - 2 * ARadiusX, R.Bottom, R.Left + ARadiusX, R.Bottom);
      AddArc(R.Left, R.Bottom - 2 * ARadiusY, 2 * ARadiusX, 2 * ARadiusY, 90, 90);
      AddLine(R.Left, R.Bottom - 2 * ARadiusY, R.Left, R.Top + ARadiusY);
      AddArc(R.Left, R.Top, 2 * ARadiusX, 2 * ARadiusY, 180, 90);
    end
    else
      AddRect(R);
  finally
    FigureFinish;
  end;
end;

procedure TdxGPPath.AddString(AText: PWideChar; AFont: TFont; AEmSize: Single; const ARect: TRect);

  function GetStyle: Integer;
  var
    AFontStyle: TFontStyle;
  begin
    Result := 0;
    for AFontStyle := Low(TFontStyle) to High(TFontStyle) do
      if AFontStyle in AFont.Style then
        Result := Result + 1 shl Ord(AFontStyle);
  end;

var
  AStringFormat: GpStringFormat;
  AGpRectF: TdxGpRectF;
  AFontFamily: GpFontFamily;
begin
  GdipCheck(GdipCreateStringFormat(0, LANG_NEUTRAL, AStringFormat));
  GdipCheck(GdipCreateFontFamilyFromName(PWideChar({$IFNDEF DELPHI14}WideString{$ENDIF}(AFont.Name)), nil, AFontFamily));
  try
    AGpRectF := dxGpMakeRectF(ARect);
    GdipCheck(GdipAddPathStringI(Handle, AText, -1, AFontFamily, GetStyle, AEmSize, @AGpRectF, AStringFormat));
  finally
    GdipDeleteFontFamily(AFontFamily);
    GdipDeleteStringFormat(AStringFormat);
  end;
end;

function TdxGPPath.GetBounds(APen: TdxGPPen = nil): TRect;
var
  R: TdxGpRect;
  APenHandle: GpPen;
begin
  if APen <> nil then
    APenHandle := APen.Handle
  else
    APenHandle := nil;

  GdipCheck(GdipGetPathWorldBoundsI(Handle, @R, nil, APenHandle));
  Result := cxRectBounds(R.X, R.Y, R.Width, R.Height);
end;

function TdxGPPath.GetBoundsF(APen: TdxGPPen = nil): TdxRectF;
var
  R: TdxGpRectF;
  APenHandle: GpPen;
begin
  if APen <> nil then
    APenHandle := APen.Handle
  else
    APenHandle := nil;

  GdipCheck(GdipGetPathWorldBounds(Handle, @R, nil, APenHandle));
  Result := cxRectFBounds(R.X, R.Y, R.Width, R.Height);
end;

function TdxGPPath.IsPointInPath(const P: TPoint; ACanvas: TdxGPCanvas = nil): Boolean;
var
  AValue: LongBool;
begin
  GdipCheck(GdipIsVisiblePathPointI(Handle, P.X, P.Y, dxGpGetCanvasHandle(ACanvas), AValue));
  Result := AValue;
end;

function TdxGPPath.IsPointInPath(const P: TdxPointF; ACanvas: TdxGPCanvas = nil): Boolean;
var
  AValue: LongBool;
begin
  GdipCheck(GdipIsVisiblePathPoint(Handle, P.X, P.Y, dxGpGetCanvasHandle(ACanvas), AValue));
  Result := AValue;
end;

function TdxGPPath.IsPointInPathOutline(const P: TPoint; APen: TdxGPPen; ACanvas: TdxGPCanvas = nil): Boolean;
var
  AValue: LongBool;
begin
  GdipCheck(GdipIsOutlineVisiblePathPointI(Handle, P.X, P.Y, APen.Handle, dxGpGetCanvasHandle(ACanvas), AValue));
  Result := AValue;
end;

function TdxGPPath.IsPointInPathOutline(const P: TPoint; APenWidth: Integer; ACanvas: TdxGPCanvas = nil): Boolean;
var
  APenHandle: GpPen;
  AValue: LongBool;
begin
  Result := False;
  if dxGpCreatePen(APenHandle, clRed, APenWidth, psSolid, 255) then
  begin
    GdipCheck(GdipIsOutlineVisiblePathPointI(Handle, P.X, P.Y, APenHandle, dxGpGetCanvasHandle(ACanvas), AValue));
    Result := AValue;
    GdipCheck(GdipDeletePen(APenHandle));
  end;
end;

function TdxGPPath.IsPointInPathOutline(const P: TdxPointF; APen: TdxGPPen; ACanvas: TdxGPCanvas = nil): Boolean;
var
  AValue: LongBool;
begin
  GdipCheck(GdipIsOutlineVisiblePathPoint(Handle, P.X, P.Y, APen.Handle, dxGpGetCanvasHandle(ACanvas), AValue));
  Result := AValue;
end;

{ TdxGPRegion }

constructor TdxGPRegion.Create;
begin
  inherited Create;
  GdipCheck(GdipCreateRegion(FHandle));
end;

constructor TdxGPRegion.Create(APath: GpPath);
begin
  inherited Create;
  GdipCheck(GdipCreateRegionPath(APath, FHandle));
end;

constructor TdxGPRegion.Create(const ARect: TRect);
var
  R: TdxGPRect;
begin
  inherited Create;
  R := MakeRect(ARect);
  GdipCheck(GdipCreateRegionRectI(@R, FHandle));
end;

destructor TdxGPRegion.Destroy;
begin
  GdipCheck(GdipDeleteRegion(FHandle));
  inherited Destroy;
end;

procedure TdxGPRegion.CombineRegionRect(const ARect: TRect; AMode: TdxGPCombineMode);
var
  R: TdxGPRect;
begin
  R := MakeRect(ARect);
  GdipCheck(GdipCombineRegionRectI(FHandle, @R, TGpCombineMode(AMode)));
end;

{ TdxGPCanvas }

constructor TdxGPCanvas.Create;
begin
  inherited Create;
  FSavedClipRegions := TStack.Create;
end;

constructor TdxGPCanvas.Create(AHandle: GpGraphics);
begin
  Create;
  FHandle := AHandle;
end;

constructor TdxGPCanvas.Create(DC: THandle);
begin
  Create;
  CreateHandle(DC);
end;

destructor TdxGPCanvas.Destroy;
begin
  dxTestCheck(FSavedClipRegions.Count = 0, ClassName + '.FSavedClipRegions <> 0');
  FreeHandle;
  FreeAndNil(FSavedClipRegions);
  inherited Destroy;
end;

procedure TdxGPCanvas.CheckDestRect(var R: TRect);
begin
  Dec(R.Bottom);
  Dec(R.Right);
end;

procedure TdxGPCanvas.Clear(AColor: TColor);
begin
  GdipCheck(GdipGraphicsClear(Handle, dxColorToAlphaColor(AColor)));
end;

procedure TdxGPCanvas.Draw(AGraphic: TdxGPImage; const R: TdxRectF; AAlpha: Byte = 255);
begin
  Draw(AGraphic, R, dxRectF(AGraphic.ClientRect), AAlpha);
end;

procedure TdxGPCanvas.Draw(AGraphic: TdxGPImage; const R: TRect; AAlpha: Byte = 255);
begin
  Draw(AGraphic, R, AGraphic.ClientRect, AAlpha);
end;

procedure TdxGPCanvas.Draw(AGraphic: TdxGPImage; const ADestRect, ASourceRect: TdxRectF; AAlpha: Byte = 255);
begin
  dxGpDrawImage(Handle, ADestRect, ASourceRect, AGraphic.Handle, AAlpha);
end;

procedure TdxGPCanvas.Draw(AGraphic: TdxGPImage; const ADestRect, ASourceRect: TRect; AAlpha: Byte = 255);
begin
  dxGpDrawImage(Handle, ADestRect, ASourceRect, AGraphic.Handle, AAlpha);
end;

procedure TdxGPCanvas.DrawBitmap(ABitmap: TBitmap; const R: TRect; AAlpha: Byte = 255);
var
  AImage: TdxGPImage;
begin
  if not ABitmap.Empty then
  begin
    AImage := TdxGPImage.CreateFromBitmap(ABitmap);
    try
      Draw(AImage, R, AAlpha);
    finally
      AImage.Free;
    end;
  end;
end;

procedure TdxGPCanvas.DrawTile(AGraphic: TdxGPImage; const R: TRect; AAlpha: Byte);
begin
  dxGpTilePartEx(Handle, R, AGraphic.ClientRect, AGraphic.Handle, AAlpha);
end;

function TdxGPCanvas.IsClipEmpty: Boolean;
var
  AValue: LongBool;
begin
  GdipCheck(GdipIsClipEmpty(FHandle, @AValue));
  Result := AValue;
end;

procedure TdxGPCanvas.RestoreClipRegion;
var
  ARegion: GpRegion;
begin
  ARegion := FSavedClipRegions.Pop;
  GdipCheck(GdipSetClipRegion(Handle, ARegion, CombineModeReplace));
  GdipCheck(GdipDeleteRegion(ARegion));
end;

procedure TdxGPCanvas.SaveClipRegion;
var
  ARegion: GpRegion;
begin
  GdipCheck(GdipCreateRegion(ARegion));
  GdipCheck(GdipGetClip(Handle, ARegion));
  FSavedClipRegions.Push(ARegion);
end;

procedure TdxGPCanvas.SetClipPath(APath: TdxGPPath; AMode: TdxGPCombineMode);
begin
  GdipCheck(GdipSetClipPath(Handle, APath.Handle, TGpCombineMode(AMode)));
end;

procedure TdxGPCanvas.SetClipRect(R: TRect; AMode: TdxGPCombineMode);
begin
  CheckDestRect(R);
  GdipCheck(GdipSetClipRectI(Handle, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R), TGpCombineMode(AMode)));
end;

procedure TdxGPCanvas.SetClipRegion(ARgn: HRGN; AMode: TdxGPCombineMode);
begin
  GdipCheck(GdipSetClipHrgn(Handle, ARgn, TGpCombineMode(AMode)));
end;

procedure TdxGPCanvas.Arc(R: TRect; AStartAngle, ASweepAngle: Single; APenColor: TColor;
  APenWidth: Single; APenStyle: TPenStyle; APenColorAlpha: Byte);
var
  APen: GpPen;
begin
  CheckDestRect(R);
  if dxGpCreatePen(APen, APenColor, APenWidth, APenStyle, APenColorAlpha) then
  begin
    GdipCheck(GdipDrawArcI(Handle, APen, R.Left, R.Top,
      cxRectWidth(R), cxRectHeight(R), -AStartAngle, -ASweepAngle));
    GdipCheck(GdipDeletePen(APen));
  end;
end;

procedure TdxGPCanvas.Arc(R: TRect; AStartAngle, ASweepAngle: Single; APenColor: TdxAlphaColor;
  APenWidth: Single = 1; APenStyle: TPenStyle = psSolid);
var
  APen: GpPen;
begin
  CheckDestRect(R);
  GdipCheck(GdipCreatePen1(APenColor, APenWidth, guPixel, APen));
  GdipCheck(GdipSetPenDashStyle(APen, PenStylesMap[APenStyle]));
  GdipCheck(GdipDrawArcI(Handle, APen, R.Left, R.Top,
    cxRectWidth(R), cxRectHeight(R), -AStartAngle, -ASweepAngle));
  GdipCheck(GdipDeletePen(APen));
end;

procedure TdxGPCanvas.Ellipse(R: TRect; APen: TdxGPPen; ABrush: TdxGPCustomBrush);
begin
  CheckDestRect(R);

  if ABrush <> nil then
  begin
    ABrush.SetTargetRect(R);
    GdipCheck(GdipFillEllipseI(Handle, ABrush.Handle, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
  end;

  if APen <> nil then
  begin
    APen.SetTargetRect(R);
    GdipCheck(GdipDrawEllipseI(Handle, APen.Handle, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
  end;
end;

procedure TdxGPCanvas.Ellipse(R: TRect; APenColor, ABrushColor: TColor; APenWidth: Single;
  APenStyle: TPenStyle; APenColorAlpha, ABrushColorAlpha: Byte);
var
  ABrush: GpBrush;
  APen: GpPen;
begin
  CheckDestRect(R);

  if dxGpCreateBrush(ABrush, ABrushColor, ABrushColorAlpha) then
  begin
    GdipCheck(GdipFillEllipseI(Handle, ABrush, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
    GdipCheck(GdipDeleteBrush(ABrush));
  end;

  if dxGpCreatePen(APen, APenColor, APenWidth, APenStyle, APenColorAlpha) then
  begin
    GdipCheck(GdipDrawEllipseI(Handle, APen, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
    GdipCheck(GdipDeletePen(APen));
  end;
end;

procedure TdxGPCanvas.Ellipse(R: TRect; APenColor, ABrushColor: TdxAlphaColor; APenWidth: Single = 1;
  APenStyle: TPenStyle = psSolid);
var
  ABrush: GpBrush;
  APen: GpPen;
begin
  CheckDestRect(R);

  GdipCheck(GdipCreateSolidFill(ABrushColor, ABrush));
  GdipCheck(GdipFillEllipseI(Handle, ABrush, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
  GdipCheck(GdipDeleteBrush(ABrush));

  GdipCheck(GdipCreatePen1(APenColor, APenWidth, guPixel, APen));
  GdipCheck(GdipSetPenDashStyle(APen, PenStylesMap[APenStyle]));
  GdipCheck(GdipDrawEllipseI(Handle, APen, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
  GdipCheck(GdipDeletePen(APen));
end;

procedure TdxGPCanvas.Ellipse(R: TdxRectF; APenColor, ABrushColor: TColor; APenWidth: Single;
  APenStyle: TPenStyle; APenColorAlpha, ABrushColorAlpha: Byte);
var
  ABrush: GpBrush;
  APen: GpPen;
begin
  if dxGpCreateBrush(ABrush, ABrushColor, ABrushColorAlpha) then
  begin
    GdipCheck(GdipFillEllipse(Handle, ABrush, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
    GdipCheck(GdipDeleteBrush(ABrush));
  end;

  if dxGpCreatePen(APen, APenColor, APenWidth, APenStyle, APenColorAlpha) then
  begin
    GdipCheck(GdipDrawEllipse(Handle, APen, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
    GdipCheck(GdipDeletePen(APen));
  end;
end;

procedure TdxGPCanvas.Line(X1, Y1, X2, Y2: Integer; APenColor: TColor;
  APenWidth: Single; APenStyle: TPenStyle; APenColorAlpha: Byte);
var
  APen: GpPen;
begin
  if dxGpCreatePen(APen, APenColor, APenWidth, APenStyle, APenColorAlpha) then
  begin
    GdipCheck(GdipDrawLineI(Handle, APen, X1, Y1, X2, Y2));
    GdipCheck(GdipDeletePen(APen));
  end;
end;

procedure TdxGPCanvas.Line(X1, Y1, X2, Y2: Integer; APenColor: TdxAlphaColor; APenWidth: Single = 1;
  APenStyle: TPenStyle = psSolid);
var
  APen: GpPen;
begin
  GdipCheck(GdipCreatePen1(APenColor, APenWidth, guPixel, APen));
  GdipCheck(GdipSetPenDashStyle(APen, PenStylesMap[APenStyle]));
  GdipCheck(GdipDrawLineI(Handle, APen, X1, Y1, X2, Y2));
  GdipCheck(GdipDeletePen(APen));
end;

procedure TdxGPCanvas.Line(X1, Y1, X2, Y2: Integer; APen: TdxGPPen);
begin
  GdipCheck(GdipDrawLineI(Handle, APen.Handle, X1, Y1, X2, Y2));
end;

procedure TdxGPCanvas.Path(APath: TdxGPPath; APen: TdxGPPen; ABrush: TdxGPCustomBrush);
var
  R: TdxRectF;
begin
  R := APath.GetBoundsF;
  if ABrush <> nil then
  begin
    ABrush.SetTargetRect(R);
    GdipCheck(GdipFillPath(Handle, ABrush.Handle, APath.Handle));
  end;

  if APen <> nil then
  begin
    APen.SetTargetRect(R);
    GdipCheck(GdipDrawPath(Handle, APen.Handle, APath.Handle));
  end;
end;

procedure TdxGPCanvas.Path(APath: TdxGPPath; APenColor, ABrushColor: TdxAlphaColor; APenWidth: Single = 1;
  APenStyle: TPenStyle = psSolid);
var
  ABrush: GpBrush;
  APen: GpPen;
begin
  GdipCheck(GdipCreateSolidFill(ABrushColor, ABrush));
  GdipCheck(GdipFillPath(Handle, ABrush, APath.Handle));
  GdipCheck(GdipDeleteBrush(ABrush));

  GdipCheck(GdipCreatePen1(APenColor, APenWidth, guPixel, APen));
  GdipCheck(GdipSetPenDashStyle(APen, PenStylesMap[APenStyle]));
  GdipCheck(GdipDrawPath(Handle, APen, APath.Handle));
  GdipCheck(GdipDeletePen(APen));
end;

procedure TdxGPCanvas.Path(APath: TdxGPPath; APenColor, ABrushColor: TColor; APenWidth: Single;
  APenStyle: TPenStyle; APenColorAlpha, ABrushColorAlpha: Byte);
var
  ABrush: GpBrush;
  APen: GpPen;
begin
  if dxGpCreateBrush(ABrush, ABrushColor, ABrushColorAlpha) then
  begin
    GdipCheck(GdipFillPath(Handle, ABrush, APath.Handle));
    GdipCheck(GdipDeleteBrush(ABrush));
  end;

  if dxGpCreatePen(APen, APenColor, APenWidth, psSolid, APenColorAlpha) then
  begin
    GdipCheck(GdipDrawPath(Handle, APen, APath.Handle));
    GdipCheck(GdipDeletePen(APen));
  end;
end;

procedure TdxGPCanvas.Pie(R: TRect; AStartAngle, ASweepAngle: Single; APenColor: TColor; ABrushColor: TColor;
  APenWidth: Single; APenStyle: TPenStyle; APenColorAlpha, ABrushColorAlpha: Byte);
var
  ABrush: GpBrush;
  APen: GpPen;
begin
  CheckDestRect(R);

  if dxGpCreateBrush(ABrush, ABrushColor, ABrushColorAlpha) then
  begin
    GdipCheck(GdipFillPieI(Handle, ABrush, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R), -AStartAngle, -ASweepAngle));
    GdipCheck(GdipDeleteBrush(ABrush));
  end;

  if dxGpCreatePen(APen, APenColor, APenWidth, APenStyle, APenColorAlpha) then
  begin
    GdipCheck(GdipDrawPieI(Handle, APen, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R), -AStartAngle, -ASweepAngle));
    GdipCheck(GdipDeletePen(APen));
  end;
end;

procedure TdxGPCanvas.Pie(R: TRect; AStartAngle, ASweepAngle: Single; APenColor, ABrushColor: TdxAlphaColor;
  APenWidth: Single = 1; APenStyle: TPenStyle = psSolid);
var
  ABrush: GpBrush;
  APen: GpPen;
begin
  GdipCheck(GdipCreateSolidFill(ABrushColor, ABrush));
  GdipCheck(GdipFillPieI(Handle, ABrush, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R), -AStartAngle, -ASweepAngle));
  GdipCheck(GdipDeleteBrush(ABrush));

  GdipCheck(GdipCreatePen1(APenColor, APenWidth, guPixel, APen));
  GdipCheck(GdipSetPenDashStyle(APen, PenStylesMap[APenStyle]));
  GdipCheck(GdipDrawPieI(Handle, APen, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R), -AStartAngle, -ASweepAngle));
  GdipCheck(GdipDeletePen(APen));
end;

procedure TdxGPCanvas.Polygon(const APoints: array of TPoint; APenColor, ABrushColor: TColor;
  APenWidth: Single; APenStyle: TPenStyle; APenColorAlpha, ABrushColorAlpha: Byte);
var
  ABrush: GpBrush;
  APen: GpPen;
begin
  if dxGpCreateBrush(ABrush, ABrushColor, ABrushColorAlpha) then
  begin
    GdipCheck(GdipFillPolygonI(Handle, ABrush, @APoints[0], Length(APoints), FillModeWinding));
    GdipCheck(GdipDeleteBrush(ABrush));
  end;

  if dxGpCreatePen(APen, APenColor, APenWidth, APenStyle, APenColorAlpha) then
  begin
    GdipCheck(GdipDrawPolygonI(Handle, APen, @APoints[0], Length(APoints)));
    GdipCheck(GdipDeletePen(APen));
  end;
end;

procedure TdxGPCanvas.Polygon(const APoints: array of TPoint; APenColor, ABrushColor: TdxAlphaColor; APenWidth: Single = 1;
  APenStyle: TPenStyle = psSolid);
var
  ABrush: GpBrush;
  APen: GpPen;
begin
  GdipCheck(GdipCreateSolidFill(ABrushColor, ABrush));
  GdipCheck(GdipFillPolygonI(Handle, ABrush, @APoints[0], Length(APoints), FillModeWinding));
  GdipCheck(GdipDeleteBrush(ABrush));

  GdipCheck(GdipCreatePen1(APenColor, APenWidth, guPixel, APen));
  GdipCheck(GdipSetPenDashStyle(APen, PenStylesMap[APenStyle]));
  GdipCheck(GdipDrawPolygonI(Handle, APen, @APoints[0], Length(APoints)));
  GdipCheck(GdipDeletePen(APen));
end;

procedure TdxGPCanvas.Polygon(const APoints: array of TdxPointF; APenColor, ABrushColor: TColor;
  APenWidth: Single; APenStyle: TPenStyle; APenColorAlpha, ABrushColorAlpha: Byte);
var
  ABrush: GpBrush;
  APen: GpPen;
begin
  if dxGpCreateBrush(ABrush, ABrushColor, ABrushColorAlpha) then
  begin
    GdipCheck(GdipFillPolygon(Handle, ABrush, @APoints[0], Length(APoints), FillModeWinding));
    GdipCheck(GdipDeleteBrush(ABrush));
  end;

  if dxGpCreatePen(APen, APenColor, APenWidth, APenStyle, APenColorAlpha) then
  begin
    GdipCheck(GdipDrawPolygon(Handle, APen, @APoints[0], Length(APoints)));
    GdipCheck(GdipDeletePen(APen));
  end;
end;

procedure TdxGPCanvas.Polyline(const APoints: array of TPoint; APenColor: TColor;
  APenWidth: Single; APenStyle: TPenStyle; APenColorAlpha: Byte);
var
  APen: GpPen;
begin
  if dxGpCreatePen(APen, APenColor, APenWidth, APenStyle, APenColorAlpha) then
  begin
    GdipCheck(GdipDrawLinesI(Handle, APen, @APoints[0], Length(APoints)));
    GdipCheck(GdipDeletePen(APen));
  end;
end;

procedure TdxGPCanvas.Polyline(const APoints: array of TPoint; APenColor: TdxAlphaColor;
  APenWidth: Single = 1; APenStyle: TPenStyle = psSolid);
var
  APen: GpPen;
begin
  GdipCheck(GdipCreatePen1(APenColor, APenWidth, guPixel, APen));
  GdipCheck(GdipSetPenDashStyle(APen, PenStylesMap[APenStyle]));
  GdipCheck(GdipDrawLinesI(Handle, APen, @APoints[0], Length(APoints)));
  GdipCheck(GdipDeletePen(APen));
end;

procedure TdxGPCanvas.Polyline(const APoints: array of TdxPointF;
  APenColor: TColor; APenWidth: Single; APenStyle: TPenStyle; APenColorAlpha: Byte);
var
  APen: GpPen;
begin
  if dxGpCreatePen(APen, APenColor, APenWidth, APenStyle, APenColorAlpha) then
  begin
    GdipCheck(GdipDrawLines(Handle, APen, @APoints[0], Length(APoints)));
    GdipCheck(GdipDeletePen(APen));
  end;
end;

procedure TdxGPCanvas.Rectangle(R: TRect; APen: TdxGPPen; ABrush: TdxGPCustomBrush);
begin
  CheckDestRect(R);

  if ABrush <> nil then
  begin
    ABrush.SetTargetRect(R);
    GdipCheck(GdipFillRectangleI(Handle, ABrush.Handle, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
  end;

  if APen <> nil then
  begin
    APen.SetTargetRect(R);
    GdipCheck(GdipDrawRectangleI(Handle, APen.Handle, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
  end;
end;

procedure TdxGPCanvas.Rectangle(R: TRect; APenColor, ABrushColor: TdxAlphaColor; APenWidth: Single = 1;
  APenStyle: TPenStyle = psSolid);
var
  ABrush: GpBrush;
  APen: GpPen;
begin
  CheckDestRect(R);

  GdipCheck(GdipCreateSolidFill(ABrushColor, ABrush));
  GdipCheck(GdipFillRectangleI(Handle, ABrush, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
  GdipCheck(GdipDeleteBrush(ABrush));

  GdipCheck(GdipCreatePen1(APenColor, APenWidth, guPixel, APen));
  GdipCheck(GdipSetPenDashStyle(APen, PenStylesMap[APenStyle]));
  GdipCheck(GdipDrawRectangleI(Handle, APen, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
  GdipCheck(GdipDeletePen(APen));
end;

procedure TdxGPCanvas.Rectangle(R: TRect; APenColor, ABrushColor: TColor; APenWidth: Single;
  APenStyle: TPenStyle; APenColorAlpha, ABrushColorAlpha: Byte);
var
  ABrush: GpBrush;
  APen: GpPen;
begin
  CheckDestRect(R);

  if dxGpCreateBrush(ABrush, ABrushColor, ABrushColorAlpha) then
  begin
    GdipCheck(GdipFillRectangleI(Handle, ABrush, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
    GdipCheck(GdipDeleteBrush(ABrush));
  end;

  if dxGpCreatePen(APen, APenColor, APenWidth, APenStyle, APenColorAlpha) then
  begin
    GdipCheck(GdipDrawRectangleI(Handle, APen, R.Left, R.Top, cxRectWidth(R), cxRectHeight(R)));
    GdipCheck(GdipDeletePen(APen));
  end;
end;

procedure TdxGPCanvas.RoundRect(R: TRect; APen: TdxGPPen; ABrush: TdxGPCustomBrush; ARadiusX, ARadiusY: Integer);
var
  APath: TdxGPPath;
begin
  APath := TdxGPPath.Create;
  try
    CheckDestRect(R);
    APath.AddRoundRect(R, ARadiusX, ARadiusY);
    Path(APath, APen, ABrush);
  finally
    APath.Free;
  end;
end;

procedure TdxGPCanvas.RoundRect(R: TRect; APenColor, ABrushColor: TColor; ARadiusX, ARadiusY: Integer;
  APenWidth: Integer; APenColorAlpha, ABrushColorAlpha: Byte);
begin
  RoundRect(R, dxColorToAlphaColor(APenColor, APenColorAlpha),
    dxColorToAlphaColor(ABrushColor, ABrushColorAlpha), ARadiusX, ARadiusY, APenWidth);
end;

procedure TdxGPCanvas.RoundRect(R: TRect; APenColor, ABrushColor: TdxAlphaColor;
  ARadiusX, ARadiusY: Integer; APenWidth: Single = 1);
var
  APath: TdxGPPath;
begin
  APath := TdxGPPath.Create;
  try
    CheckDestRect(R);
    APath.AddRoundRect(R, ARadiusX, ARadiusY);
    Path(APath, APenColor, ABrushColor, APenWidth, psSolid);
  finally
    APath.Free;
  end;
end;

procedure TdxGPCanvas.FlipWorldTransform(AFlipHorizontally, AFlipVertically: Boolean; const APivotPoint: TdxPointF);
begin
  FlipWorldTransform(AFlipHorizontally, AFlipVertically, APivotPoint.X, APivotPoint.Y);
end;

procedure TdxGPCanvas.FlipWorldTransform(AFlipHorizontally, AFlipVertically: Boolean; const APivotPoint: TPoint);
begin
  FlipWorldTransform(AFlipHorizontally, AFlipVertically, APivotPoint.X, APivotPoint.Y);
end;

procedure TdxGPCanvas.FlipWorldTransform(AFlipHorizontally, AFlipVertically: Boolean; const APivotPointX, APivotPointY: Single);
var
  AMatrix: TdxGPMatrix;
begin
  if AFlipHorizontally or AFlipVertically then
  begin
    AMatrix := TdxGPMatrix.CreateFlip(AFlipHorizontally, AFlipVertically, APivotPointX, APivotPointY);
    try
      ModifyWorldTransform(AMatrix);
    finally
      AMatrix.Free;
    end;
  end;
end;

function TdxGPCanvas.GetWorldTransform: TdxGPMatrix;
begin
  Result := TdxGPMatrix.Create;
  GdipCheck(GdipGetWorldTransform(Handle, Result.Handle));
end;

procedure TdxGPCanvas.ModifyWorldTransform(AMatrix: TdxGPMatrix; AOrder: TdxGpMatrixOrder = MatrixOrderPrepend);
begin
  GdipCheck(GdipMultiplyWorldTransform(Handle, AMatrix.Handle, AOrder));
end;

procedure TdxGPCanvas.ResetWorldTransform;
begin
  GdipCheck(GdipResetWorldTransform(Handle));
end;

procedure TdxGPCanvas.RotateWorldTransform(AAngle: Single; AOrder: TdxGpMatrixOrder = MatrixOrderPrepend);
begin
  GdipCheck(GdipRotateWorldTransform(Handle, AAngle, AOrder));
end;

procedure TdxGPCanvas.RotateWorldTransform(AAngle: Single;
  const APivotPoint: TPoint; AOrder: TdxGpMatrixOrder = MatrixOrderPrepend);
begin
  RotateWorldTransform(AAngle, dxPointF(APivotPoint), AOrder);
end;

procedure TdxGPCanvas.RotateWorldTransform(AAngle: Single; const APivotPoint: TdxPointF; AOrder: TdxGpMatrixOrder = MatrixOrderPrepend);
const
  DirectionMap: array[TdxGpMatrixOrder] of Integer = (1, -1);
begin
  TranslateWorldTransform(DirectionMap[AOrder] * APivotPoint.X, DirectionMap[AOrder] * APivotPoint.Y, AOrder);
  RotateWorldTransform(AAngle, AOrder);
  TranslateWorldTransform(-DirectionMap[AOrder] * APivotPoint.X, -DirectionMap[AOrder] * APivotPoint.Y, AOrder);
end;

procedure TdxGPCanvas.ScaleWorldTransform(AScaleX, AScaleY: Single; AOrder: TdxGpMatrixOrder = MatrixOrderPrepend);
begin
  GdipCheck(GdipScaleWorldTransform(Handle, AScaleX, AScaleY, AOrder));
end;

procedure TdxGPCanvas.SetWorldTransform(AMatrix: TdxGPMatrix);
begin
  GdipCheck(GdipSetWorldTransform(Handle, AMatrix.Handle));
end;

procedure TdxGPCanvas.TranslateWorldTransform(AOffestX, AOffsetY: Single; AOrder: TdxGpMatrixOrder = MatrixOrderPrepend);
begin
  GdipCheck(GdipTranslateWorldTransform(Handle, AOffestX, AOffsetY, AOrder));
end;

procedure TdxGPCanvas.CreateHandle(DC: HDC);
begin
  GdipCheck(GdipCreateFromHDC(DC, FHandle));
end;

procedure TdxGPCanvas.FreeHandle;
begin
  if FHandle <> nil then
  begin
    GdipCheck(GdipDeleteGraphics(FHandle));
    FHandle := nil;
  end;
end;

function TdxGPCanvas.GetHDC: HDC;
begin
  GdipCheck(GdipGetDC(Handle, Result));
end;

procedure TdxGPCanvas.ReleaseHDC(DC: HDC);
begin
  GdipCheck(GdipReleaseDC(Handle, DC));
end;

function TdxGPCanvas.GetInterpolationMode: TdxGPInterpolationMode;
var
  AResult: Integer;
begin
  GdipCheck(GdipGetInterpolationMode(Handle, AResult));
  if (AResult >= Ord(Low(TdxGPInterpolationMode))) and (AResult <= Ord(High(TdxGPInterpolationMode))) then
    Result := TdxGPInterpolationMode(AResult)
  else
    Result := imDefault;
end;

function TdxGPCanvas.GetSmoothingMode: TdxGPSmoothingMode;
var
  AResult: Integer;
begin
  GdipCheck(GdipGetSmoothingMode(Handle, AResult));
  if (AResult >= Ord(Low(TdxGPSmoothingMode))) and (AResult <= Ord(High(TdxGPSmoothingMode))) then
    Result := TdxGPSmoothingMode(AResult)
  else
    Result := smDefault;
end;

procedure TdxGPCanvas.SetInterpolationMode(AValue: TdxGPInterpolationMode);
begin
  GdipCheck(GdipSetInterpolationMode(Handle, Integer(AValue)));
end;

procedure TdxGPCanvas.SetSmoothingMode(AValue: TdxGPSmoothingMode);
begin
  GdipCheck(GdipSetSmoothingMode(Handle, Integer(AValue)));
end;

{ TdxGPCustomPaintCanvas }

procedure TdxGPCustomPaintCanvas.BeginPaint(DC: HDC; const R: TRect);
begin
  SaveState;
  FDrawRect := R;
  FDrawDC := DC;
  if dxGpIsDoubleBufferedNeeded(DC) then
  begin
    CreateBuffer(DC, R);
    DC := FBuffer.Canvas.Handle;
  end
  else
    FBuffer := nil;

  CreateHandle(DC);
end;

procedure TdxGPCustomPaintCanvas.BeginPaint(AHandle: GpGraphics);
begin
  SaveState;
  FDrawRect := Rect(0, 0, 0, 0);
  FDrawDC := 0;
  FHandle := AHandle;
  FBuffer := nil;
end;

procedure TdxGPCustomPaintCanvas.EndPaint;
begin
  if FDrawDC <> 0 then
    FreeHandle;
  if FBuffer <> nil then
    FreeBuffer;
  RestoreState;
end;

procedure TdxGPCustomPaintCanvas.CreateBuffer(DC: HDC; const R: TRect);
begin
  FBuffer := TBitmap.Create;
  FBuffer.PixelFormat := pf32bit;
  FBuffer.Height := cxRectHeight(R) + 1;
  FBuffer.Width := cxRectWidth(R) + 1;
  FBuffer.Canvas.Lock;

  BitBlt(FBuffer.Canvas.Handle, 0, 0, FBuffer.Width, FBuffer.Height, DC, R.Left, R.Top, SRCCOPY);
  SetWindowOrgEx(FBuffer.Canvas.Handle, R.Left, R.Top, nil);
end;

procedure TdxGPCustomPaintCanvas.FreeBuffer;
begin
  OutputBuffer;
  FBuffer.Canvas.Unlock;
  FreeAndNil(FBuffer);
end;

procedure TdxGPCustomPaintCanvas.OutputBuffer;
var
  ACanvas: TCanvas;
  ASaveIndex: Integer;
begin
  ACanvas := TCanvas.Create;
  try
    ACanvas.Lock;
    try
      ASaveIndex := SaveDC(FDrawDC);
      ACanvas.Handle := FDrawDC;
      SetWindowOrgEx(FBuffer.Canvas.Handle, 0, 0, nil);
      ACanvas.Draw(FDrawRect.Left, FDrawRect.Top, FBuffer);
      ACanvas.Handle := 0;
      RestoreDC(FDrawDC, ASaveIndex);
    finally
      ACanvas.Unlock;
    end;
  finally
    ACanvas.Free;
  end;
end;

procedure TdxGPCustomPaintCanvas.SaveState;
begin
  //do nothing
end;

procedure TdxGPCustomPaintCanvas.RestoreState;
begin
  //do nothing
end;

{ TdxGPPaintCanvas }

destructor TdxGPPaintCanvas.Destroy;
begin
  Finalize(FSavedStates);
  inherited Destroy;
end;

procedure TdxGPPaintCanvas.SaveState;
begin
  SetCapacity(FCounter + 1);
  FSavedStates[FCounter].Handle := Handle;
  FSavedStates[FCounter].Buffer := FBuffer;
  FSavedStates[FCounter].DrawRect := FDrawRect;
  FSavedStates[FCounter].DC := FDrawDC;
  Inc(FCounter);
end;

procedure TdxGPPaintCanvas.RestoreState;
begin
  Dec(FCounter);
  FHandle := FSavedStates[FCounter].Handle;
  FBuffer := FSavedStates[FCounter].Buffer;
  FDrawRect := FSavedStates[FCounter].DrawRect;
  FDrawDC := FSavedStates[FCounter].DC;
end;

procedure TdxGPPaintCanvas.SetCapacity(AValue: Integer);
begin
  if AValue > Length(FSavedStates) then
    SetLength(FSavedStates, Max(AValue, Length(FSavedStates) + 4));
end;

{ TdxGPMemoryStream }

function TdxGPMemoryStream.Realloc(var ANewCapacity: Integer): Pointer;
begin
  Result := Memory;
  if ANewCapacity <> Capacity then
  begin
    if ANewCapacity > 0 then
    begin
      Result := GdipAlloc(ANewCapacity);
      if Result = nil then
        raise EStreamError.CreateRes(@SMemoryStreamError);
      if Capacity > 0 then
        Move(Memory^, Result^, Min(Capacity, ANewCapacity));
    end
    else
      Result := nil;

    if Capacity > 0 then
      GdipFree(Memory);
  end;
end;

{ TdxGPStreamAdapter }

function TdxGPStreamAdapter.Stat(out StatStg: TStatStg; StatFlag: Integer): HRESULT;
begin
  Result := S_OK;
  try
    if @StatStg <> nil then
    begin
      ZeroMemory(@StatStg, SizeOf(StatStg));
      Result := inherited Stat(StatStg, StatFlag);
    end;
  except
    Result := E_UNEXPECTED;
  end;
end;

{ TdxGPImageData}

destructor TdxGPImageData.Destroy;
begin
  FreeImageData;
  inherited Destroy;
end;

procedure TdxGPImageData.Assign(AData: TdxGPImageData);
begin
  FreeImageData;
  if AData <> nil then
  begin
    FBits := AData.FBits;
    FBitsBounds := AData.FBitsBounds;
    FNewSize := AData.FNewSize;
    FData := dxCloneStream(AData.FData);
  end;
end;

procedure TdxGPImageData.FreeImageData;
begin
  FBits := nil;
  FBitsBounds := cxNullSize;
  FNewSize := cxNullSize;
  FreeAndNil(FData);
end;

procedure TdxGPImageData.LoadFromBits(const ABits: TRGBColors; const ASize: Size; AHasAlphaChannel: Boolean);
var
  I: Integer;
begin
  FreeImageData;
  FBits := ABits;
  FBitsBounds := ASize;
  if not AHasAlphaChannel then
  begin
    for I := 0 to Length(FBits) - 1 do
      FBits[I].rgbReserved := 255;
  end;
end;

procedure TdxGPImageData.LoadFromStream(AStream: TStream);
var
  ASize: Integer;
begin
  FreeImageData;
  ASize := AStream.Size - AStream.Position;
  if ASize > 0 then
  begin
    FData := TdxGPMemoryStream.Create;
    FData.Size := ASize;
    FData.CopyFrom(AStream, ASize);
    FData.Position := 0;
  end;
end;

function TdxGPImageData.GetIsEmpty: Boolean;
begin
  Result := (FData = nil) and (Length(FBits) = 0);
end;

{ TdxGPImage }

constructor TdxGPImage.Create;
begin
  inherited Create;
  FImageData := TdxGPImageData.Create;
end;

constructor TdxGPImage.CreateSize(const ASize: TSize; AColor: TdxAlphaColor = 0);
var
  I: Integer;
  ABits: TRGBColors;
begin
  Create;
  SetLength(ABits, ASize.cx * ASize.cy);
  for I := 0 to Length(ABits) - 1 do
    Integer(ABits[I]) := AColor;
  CreateHandleFromBits(ASize.cx, ASize.cy, ABits, True);
end;

constructor TdxGPImage.CreateSize(const R: TRect; AColor: TdxAlphaColor);
begin
  CreateSize(cxSize(R), AColor);
end;

constructor TdxGPImage.CreateSize(AWidth, AHeight: Integer; AColor: TdxAlphaColor = 0);
begin
  CreateSize(cxSize(AWidth, AHeight), AColor);
end;

constructor TdxGPImage.CreateFromBitmap(ABitmap: TBitmap);
begin
  Create;
  CreateHandleFromBitmap(ABitmap);
end;

constructor TdxGPImage.CreateFromHBitmap(ABitmap: HBitmap);
begin
  Create;
  CreateHandleFromHBITMAP(ABitmap, 0);
end;

constructor TdxGPImage.CreateFromBits(AWidth, AHeight: Integer; const ABits: TRGBColors; AHasAlphaChannel: Boolean);
begin
  Create;
  CreateHandleFromBits(AWidth, AHeight, ABits, AHasAlphaChannel);
end;

constructor TdxGPImage.CreateFromStream(AStream: TStream);
begin
  Create;
  CreateHandleFromStream(AStream);
end;

destructor TdxGPImage.Destroy;
begin
  FreeHandle;
  FreeAndNil(FImageData);
  inherited Destroy;
end;

procedure TdxGPImage.Assign(ASource: TPersistent);
begin
  if ASource = nil then
    Clear
  else
    if ASource is TdxGPImage then
      AssignFromGpImage(TdxGPImage(ASource))
    else
      if ASource is TGraphic then
        AssignFromGraphic(TGraphic(ASource))
      else
        inherited Assign(ASource);
end;

procedure TdxGPImage.LoadFromClipboardFormat(AFormat: Word; AData: THandle; APalette: HPALETTE);
var
  ABitmap: TBitmap;
begin
  ABitmap := TBitmap.Create;
  try
    ABitmap.PixelFormat := pf32Bit;
    ABitmap.HandleType := bmDIB;
    ABitmap.LoadFromClipboardFormat(AFormat, AData, APalette);
    SetBitmap(ABitmap);
  finally
    ABitmap.Free;
  end;
end;

procedure TdxGPImage.LoadFromFieldValue(const AValue: Variant);
type
  TGraphicHeader = record
    Count: Word;
    HType: Word;
    Size: Longint;
  end;
var
  AHeader: TGraphicHeader;
  ASize: Longint;
  AStream: TMemoryStream;
  AValueAsString: AnsiString;
begin
  if not dxVarIsBlob(AValue) then Exit;
  AStream := TMemoryStream.Create;
  try
    AValueAsString := dxVariantToAnsiString(AValue);
    ASize := Length(AValueAsString);
    if ASize >= SizeOf(AHeader) then
    begin
      TMemoryStreamAccess(AStream).SetPointer(@AValueAsString[1], ASize);
      AStream.Position := 0;
      AStream.Read(AHeader, SizeOf(AHeader));
      if (AHeader.Count <> 1) or (AHeader.HType <> $0100) or
        (AHeader.Size <> ASize - SizeOf(AHeader)) then
        AStream.Position := 0;
    end;
    if AStream.Size > 0 then
      LoadFromStream(AStream);
  finally
    AStream.Free;
  end;
end;

procedure TdxGPImage.LoadFromResource(AInstance: THandle; const AResName: string; AResType: PChar);
var
  AStream: TStream;
begin
  AStream := TResourceStream.Create(AInstance, AResName, AResType);
  try
    LoadFromStream(AStream);
  finally
    AStream.Free;
  end;
end;

procedure TdxGPImage.LoadFromStream(AStream: TStream);
begin
  CreateHandleFromStream(AStream);
  Changed(Self);
end;

procedure TdxGPImage.LoadFromBits(AWidth, AHeight: Integer; const ABits: TRGBColors; AHasAlphaChannel: Boolean);
begin
  CreateHandleFromBits(AWidth, AHeight, ABits, AHasAlphaChannel);
  Changed(Self);
end;

procedure TdxGPImage.SaveToClipboardFormat(var AFormat: Word; var AData: THandle; var APalette: HPALETTE);
var
  ABitmap: TBitmap;
begin
  ABitmap := TBitmap.Create;
  try
    ABitmap.Width := Width;
    ABitmap.Height := Height;
    ABitmap.Canvas.Brush.Color := clWhite;
    ABitmap.Canvas.FillRect(ClientRect);
    Draw(ABitmap.Canvas, ClientRect);
    ABitmap.SaveToClipboardFormat(AFormat, AData, APalette);
  finally
    ABitmap.Free;
  end;
end;

procedure TdxGPImage.SaveToStream(AStream: TStream);
begin
  SaveToStreamByCodec(AStream, ImageDataFormat);
end;

procedure TdxGPImage.SaveToStreamByCodec(AStream: TStream; AImageFormat: TdxImageDataFormat);
begin
  InternalSaveToStream(AStream, AImageFormat, nil);
end;

procedure TdxGPImage.Clear;
begin
  if HandleAllocated or not ImageData.Empty then
  begin
    FreeHandle;
    Changed(Self);
  end;
end;

function TdxGPImage.Clone: TdxGPImage;
begin
  Result := TdxGPImageClass(ClassType).Create;
  Result.AssignFromGpImage(Self);
end;

procedure TdxGPImage.ChangeColor(AColor: TColor);
var
  AColors: TRGBColors;
  R, G, B, A: Byte;
  I: Integer;
begin
  if (AColor <> clNone) and (AColor <> clDefault) and not Empty then
  begin
    AColor := ColorToRGB(AColor);
    R := GetRValue(AColor);
    G := GetGValue(AColor);
    B := GetBValue(AColor);
    AColors := GetBitmapBits;
    for I := 0 to Length(AColors) - 1 do
    begin
      A := AColors[I].rgbReserved;
      AColors[I].rgbBlue := MulDiv(B, A, MaxByte);
      AColors[I].rgbGreen := MulDiv(G, A, MaxByte);
      AColors[I].rgbRed := MulDiv(R, A, MaxByte);
    end;
    LoadFromBits(Width, Height, AColors, True);
  end;
end;

function TdxGPImage.Compare(AImage: TdxGPImage): Boolean;
var
  AColors: TRGBColors;
  AColors2: TRGBColors;
begin
  AColors := nil;
  AColors2 := nil;
  Result := (AImage.Height = Height) and (AImage.Width = Width);
  if Result and not (AImage.Empty or Empty) then
  begin
    AColors := GetBitmapBits;
    AColors2 := AImage.GetBitmapBits;
    Result := CompareMem(@AColors[0], @AColors2[0], SizeOf(AColors[0]) * Length(AColors));
  end;
end;

procedure TdxGPImage.ConvertToBitmap;
var
  AGraphics: GpGraphics;
  ANewHandle: GpImage;
begin
  if Handle <> nil then
  begin
    ANewHandle := dxGpCreateBitmap(Width, Height);
    GdipCheck(GdipGetImageGraphicsContext(ANewHandle, AGraphics));
    GdipCheck(GdipDrawImageRectI(AGraphics, Handle, 0, 0, Width, Height));
    Handle := ANewHandle;
  end;
end;

function TdxGPImage.CreateCanvas: TdxGPCanvas;
var
  AGraphics: GpGraphics;
begin
  GdipCheck(GdipGetImageGraphicsContext(Handle, AGraphics));
  Result := TdxGPCanvas.Create(AGraphics);
  Changed(Self);
end;

function TdxGPImage.GetAsBitmap: TBitmap;
var
  ABitmapHandle: HBITMAP;
begin
  GdipCheck(GdipCreateHBITMAPFromBitmap(Handle, ABitmapHandle, 0));
  Result := TBitmap.Create;
  Result.PixelFormat := pf32Bit;
  Result.Handle := ABitmapHandle;
end;

function TdxGPImage.GetBitmapBits: TRGBColors;
var
  ABitmap: TBitmap;
begin
  ABitmap := GetAsBitmap;
  try
    dxCoreGraphics.GetBitmapBits(ABitmap, Result, True);
  finally
    ABitmap.Free;
  end;
end;

function TdxGPImage.GetHashCode: Integer;
var
  AColors: TRGBColors;
begin
  Result := 0;
  if not Empty then
  begin
    AColors := GetBitmapBits;
    Result := dxCRC32(@AColors[0], Length(AColors) * SizeOf(AColors[0]));
  end;
end;

function TdxGPImage.MakeComposition(AOverlayImage: TdxGPImage; AOverlayAlpha: Byte): TdxGPImage;
begin
  Result := MakeComposition(AOverlayImage, AOverlayAlpha, 255);
end;

function TdxGPImage.MakeComposition(AOverlayImage: TdxGPImage; AOverlayAlpha, ASourceAlpha: Byte): TdxGPImage;
var
  ACanvas: TdxGPCanvas;
begin
  Result := TdxGPImage.CreateSize(cxSize(Width, Height));
  if not Result.Empty then
  begin
    ACanvas := Result.CreateCanvas;
    try
      ACanvas.Draw(Self, Result.ClientRect, ASourceAlpha);
      ACanvas.Draw(AOverlayImage, Result.ClientRect, AOverlayImage.ClientRect, AOverlayAlpha);
    finally
      ACanvas.Free;
    end;
  end;
end;

procedure TdxGPImage.HandleNeeded;
begin
  if not Loaded then
    CreateHandleFromImageData;
end;

procedure TdxGPImage.Resize(const AWidth, AHeight: Integer);
var
  AGraphics: GpGraphics;
  AScaledImageHandle: GpBitmap;
  R: TRect;
begin
  FImageData.FNewSize := cxSize(AWidth, AHeight);
  if HandleAllocated then
  begin
    R := cxRect(FImageData.FNewSize);
    AScaledImageHandle := dxGpCreateBitmap(R);
    GdipCheck(GdipGetImageGraphicsContext(AScaledImageHandle, AGraphics));
    GdipCheck(GdipDrawImageRectI(AGraphics, Handle, R.Left, R.Top, R.Right, R.Bottom));
    GdipCheck(GdipDeleteGraphics(AGraphics));
    Handle := AScaledImageHandle;
  end;
end;

procedure TdxGPImage.Resize(const ASize: TSize);
begin
  Resize(ASize.cx, ASize.cy);
end;

procedure TdxGPImage.SetBitmap(ABitmap: TBitmap);
begin
  CreateHandleFromBitmap(ABitmap);
  Changed(Self);
end;

procedure TdxGPImage.Scale(const M, D: Integer);
begin
  Resize(MulDiv(Width, M, D),  MulDiv(Height, M, D));
end;

procedure TdxGPImage.StretchDraw(DC: HDC; const ADest: TRect; AAlpha: Byte = 255);
begin
  StretchDraw(DC, ADest, ClientRect, AAlpha);
end;

procedure TdxGPImage.StretchDraw(DC: HDC; const ADest, ASource: TRect; AAlpha: Byte = 255);
begin
  if Handle <> nil then
  begin
    dxGPPaintCanvas.BeginPaint(DC, ADest);
    dxGPPaintCanvas.Draw(Self, ADest, ASource, AAlpha);
    dxGPPaintCanvas.EndPaint;
  end;
end;

function TdxGPImage.CheckHasAlphaChannel(const ABits: TRGBColors): Boolean;

  function HasNonTransparentPixels: Boolean;
  var
    I: Integer;
  begin
    Result := False;
    for I := 0 to Length(ABits) - 1 do
    begin
      Result := ABits[I].rgbReserved > 0;
      if Result then
        Break;
    end;
  end;

  function CheckTransparentPixels: Boolean;
  var
    I: Integer;
  begin
    Result := False;
    for I := 0 to Length(ABits) - 1 do
    begin
      Result := DWORD(ABits[I]) = 0;
      if not Result then
        Break;
    end;
  end;

begin
  Result := HasNonTransparentPixels or CheckTransparentPixels;
end;

procedure TdxGPImage.CreateHandleFromBitmap(ABitmap: TBitmap);
var
  AColors: TRGBColors;
begin
  if cxGetBitmapPixelFormat(ABitmap) < 32 then
    CreateHandleFromHBITMAP(ABitmap.Handle, ABitmap.Palette)
  else
    if dxCoreGraphics.GetBitmapBits(ABitmap, AColors, True) then
      CreateHandleFromBits(ABitmap.Width, ABitmap.Height, AColors, CheckHasAlphaChannel(AColors));
end;

procedure TdxGPImage.CreateHandleFromBits(AWidth, AHeight: Integer; const ABits: TRGBColors; AHasAlphaChannel: Boolean);
begin
  FreeHandle;
  ImageData.LoadFromBits(ABits, cxSize(AWidth, AHeight), AHasAlphaChannel);
end;

procedure TdxGPImage.CreateHandleFromHBITMAP(ABitmap: HBITMAP; APalette: HPALETTE);
begin
  FreeHandle;
  GdipCheck(GdipCreateBitmapFromHBITMAP(ABitmap, APalette, FHandle));
  RecognizeImageRawDataFormat;
end;

procedure TdxGPImage.CreateHandleFromImageData;
var
  ABitmap: TBitmap;
  AStreamAdapter: IStream;
begin
  if not ImageData.Empty then
  begin
    if Length(ImageData.FBits) > 0 then
      GdipCheck(GdipCreateBitmapFromScan0(ImageData.FBitsBounds.cx,
        ImageData.FBitsBounds.cy, ImageData.FBitsBounds.cx * 4,
        PixelFormat32bppPARGB, @ImageData.FBits[0], FHandle))
    else
    begin
      ImageData.FData.Position := 0;
      if TdxGPImageDataHelper.IsBitmapStream(ImageData.FData) then
      begin
        ABitmap := TBitmap.Create;
        try
          ABitmap.LoadFromStream(ImageData.FData);
          ImageData.FreeImageData;
          CreateHandleFromBitmap(ABitmap);
        finally
          ABitmap.Free;
        end;
      end
      else
      begin
        AStreamAdapter := TdxGPStreamAdapter.Create(ImageData.FData, soReference);
        GdipCheck(GdipLoadImageFromStream(AStreamAdapter, FHandle));
        AStreamAdapter := nil;
      end;
    end;
    if (ImageData.FNewSize.cx > 0) and (ImageData.FNewSize.cy > 0) then
      Resize(ImageData.FNewSize);
  end;
  RecognizeImageRawDataFormat;
  if ImageDataFormat = dxImageUnknown then
    ConvertToBitmap;
end;

procedure TdxGPImage.CreateHandleFromStream(AStream: TStream);
begin
  FreeHandle;
  ImageData.LoadFromStream(AStream);
end;

procedure TdxGPImage.AssignFromGpImage(AGpImage: TdxGPImage);
var
  ABitmap: TBitmap;
  AStream: TMemoryStream;
begin
  if AGpImage.Empty then
    Clear
  else
    if not AGpImage.ImageData.Empty then
    begin
      FreeHandle;
      ImageData.Assign(AGpImage.ImageData);
      Changed(Self);
    end
    else
      if AGpImage.ImageDataFormat = dxImageBitmap then
      begin
        ABitmap := AGpImage.GetAsBitmap;
        try
          SetBitmap(ABitmap);
        finally
          ABitmap.Free;
        end;
      end
      else
      begin
        AStream := TMemoryStream.Create;
        try
          AGpImage.SaveToStream(AStream);
          AStream.Position := 0;
          LoadFromStream(AStream);
        finally
          AStream.Free;
        end;
      end;
end;

procedure TdxGPImage.AssignFromGraphic(AGraphic: TGraphic);
var
  ABitmap: TBitmap;
begin
  if AGraphic is TBitmap then
    SetBitmap(TBitmap(AGraphic))
  else
  begin
    ABitmap := TBitmap.Create;
    try
      ABitmap.PixelFormat := pf24bit;
    {$IFDEF DELPHI10}
      ABitmap.SetSize(AGraphic.Width, AGraphic.Height);
    {$ELSE}
      ABitmap.Width := AGraphic.Width;
      ABitmap.Height := AGraphic.Height;
    {$ENDIF}
      ABitmap.Canvas.Draw(0, 0, AGraphic);
      SetBitmap(ABitmap);
    finally
      ABitmap.Free;
    end;
  end;
end;

procedure TdxGPImage.AssignTo(ADest: TPersistent);
var
  ABitmap: TBitmap;
begin
  if ADest is TdxGPImage then
    ADest.Assign(Self)
  else
    if ADest is TBitmap then
    begin
      ABitmap := GetAsBitmap;
      try
        ADest.Assign(ABitmap);
      finally
        ABitmap.Free;
      end;
    end
    else
      inherited AssignTo(ADest);
end;

procedure TdxGPImage.Changed(Sender: TObject);
begin
  FIsAlphaUsedAssigned := False;
  inherited Changed(Sender);
end;

procedure TdxGPImage.Draw(ACanvas: TCanvas; const ARect: TRect);
begin
  StretchDraw(ACanvas.Handle, ARect);
end;

procedure TdxGPImage.FreeHandle;
begin
  if HandleAllocated then
  begin
    GdipDisposeImage(FHandle);
    FImageDataFormat := dxImageUnknown;
    FHandle := nil;
  end;
  FImageData.FreeImageData;
end;

procedure TdxGPImage.InternalSaveToStream(AStream: TStream;
  AImageFormat: TdxImageDataFormat; AParameters: PEncoderParameters);

  function CreateStream(AImageFormat: TdxImageDataFormat): TMemoryStream;
  var
    ACodec: TGUID;
  begin
    Result := TdxGPMemoryStream.Create;
    ACodec := dxGetImageEncoder(AImageFormat);
    GdipCheck(GdipSaveImageToStream(Handle, TdxGPStreamAdapter.Create(Result, soReference), @ACodec, AParameters));
    Result.Position := 0;
  end;

  procedure RecreateHandle(AStream: TStream; AFreeStream: Boolean = False);
  begin
    AStream.Position := 0;
    CreateHandleFromStream(AStream);
    if AFreeStream then
      AStream.Free;
  end;

var
  ATempStream: TMemoryStream;
begin
  if Handle <> nil then
  begin
    ATempStream := CreateStream(AImageFormat);
    try
      AStream.CopyFrom(ATempStream, ATempStream.Size);

      if AImageFormat <> ImageDataFormat then
        RecreateHandle(CreateStream(ImageDataFormat), True)
      else
        RecreateHandle(ATempStream);
    finally
      ATempStream.Free;
    end;
  end;
end;

{$IFDEF DELPHI14}
function TdxGPImage.Equals(Graphic: TGraphic): Boolean;
begin
  if Graphic is TdxGPImage then
    Result := Compare(TdxGPImage(Graphic))
  else
    Result := inherited Equals(Graphic);
end;
{$ENDIF}

function TdxGPImage.GetEmpty: Boolean;
begin
  Result := cxRectIsEmpty(ClientRect);
end;

function TdxGPImage.GetHeight: Integer;
begin
  Result := GetSize.cy;
end;

function TdxGPImage.GetLoaded: Boolean;
begin
  Result := HandleAllocated or ImageData.Empty;
end;

function TdxGPImage.GetSize: TSize;
var
  W, H: Single;
begin
  if Handle <> nil then
    GdipCheck(GdipGetImageDimension(Handle, W, H))
  else
  begin
    W := 0;
    H := 0;
  end;
  Result.cx := Trunc(W);
  Result.cy := Trunc(H);
end;

function TdxGPImage.GetWidth: Integer;
begin
  Result := GetSize.cx;
end;

function TdxGPImage.GetTransparent: Boolean;
begin
  Result := IsAlphaUsed;
end;

procedure TdxGPImage.SetWidth(Value: Integer);
begin
end;

procedure TdxGPImage.SetHandle(AValue: GPImage);
begin
  if AValue <> FHandle then
  begin
    FreeHandle;
    FHandle := AValue;
    RecognizeImageRawDataFormat;
    Changed(Self);
  end;
end;

procedure TdxGPImage.SetHeight(Value: Integer);
begin
end;

function TdxGPImage.CheckAlphaUsed: Boolean;
var
  AColors: TRGBColors;
  I: Integer;
begin
  AColors := nil;
  Result := False;
  if not Empty then
  begin
    AColors := GetBitmapBits;
    for I := Low(AColors) to High(AColors) do
    begin
      Result := AColors[I].rgbReserved <> 255;
      if Result then Break;
    end;
  end;
end;

function TdxGPImage.GetClientRect: TRect;
begin
  Result := cxRect(GetSize);
end;

function TdxGPImage.GetHandle: GpImage;
begin
  HandleNeeded;
  Result := FHandle;
end;

function TdxGPImage.GetHandleAllocated: Boolean;
begin
  Result := FHandle <> nil;
end;

function TdxGPImage.GetImageDataFormat: TdxImageDataFormat;
begin
  HandleNeeded;
  Result := FImageDataFormat;
end;

function TdxGPImage.GetIsAlphaUsed: Boolean;
begin
  if not FIsAlphaUsedAssigned then
  begin
    FIsAlphaUsed := CheckAlphaUsed;
    FIsAlphaUsedAssigned := True;
  end;
  Result := FIsAlphaUsed
end;

procedure TdxGPImage.RecognizeImageRawDataFormat;
var
  AFormatID: TGUID;
begin
  if Handle <> nil then
  begin
    GdipCheck(GdipGetImageRawFormat(Handle, AFormatID));
    FImageDataFormat := dxGetImageDataFormat(AFormatID);
  end
  else
    FImageDataFormat := dxImageUnknown;
end;

procedure TdxGPImage.SetImageDataFormat(AValue: TdxImageDataFormat);
var
  AStream: TMemoryStream;
begin
  if AValue <> ImageDataFormat then
  begin
    AStream := TMemoryStream.Create;
    try
      SaveToStreamByCodec(AStream, AValue);
      AStream.Position := 0;
      LoadFromStream(AStream);
    finally
      AStream.Free;
    end;
  end;
end;

{ TdxSmartImage }

procedure TdxSmartImage.ReadData(Stream: TStream);
begin
  LoadFromStream(Stream);
end;

procedure TdxSmartImage.WriteData(Stream: TStream);
begin
  SaveToStream(Stream);
end;

{ TdxPNGImage }

procedure TdxPNGImage.SaveToStream(AStream: TStream);
begin
  SaveToStreamByCodec(AStream, dxImagePng);
end;

{ TdxJPEGImage }

constructor TdxJPEGImage.Create;
begin
  inherited Create;
  FQuality := 75;
end;

procedure TdxJPEGImage.SaveToStream(AStream: TStream);
var
  AParameters: TEncoderParameters;
begin
  AParameters.Count := 1;
  AParameters.Parameter[0].Guid := EncoderQuality;
  AParameters.Parameter[0].Type_ := EncoderParameterValueTypeLong;
  AParameters.Parameter[0].NumberOfValues := 1;
  AParameters.Parameter[0].Value := @FQuality;
  InternalSaveToStream(AStream, dxImageJpeg, @AParameters);
end;

procedure TdxJPEGImage.SetQuality(AValue: Cardinal);
begin
  FQuality := Min(AValue, 100);
end;

{ TdxGIFImage }

procedure TdxGIFImage.SaveToStream(AStream: TStream);
begin
  SaveToStreamByCodec(AStream, dxImageGif);
end;

{ TdxTIFFImage }

procedure TdxTIFFImage.SaveToStream(AStream: TStream);
begin
  SaveToStreamByCodec(AStream, dxImageTiff);
end;

{ TdxBMPImage }

procedure TdxBMPImage.SaveToStream(AStream: TStream);
begin
  SaveToStreamByCodec(AStream, dxImageBitmap);
end;

{ TdxGPAlphaBlendAttributes }

constructor TdxGPAlphaBlendAttributes.Create;
begin
  inherited Create;
  GdipCheck(GdipCreateImageAttributes(FHandle));
  FColorMatrix := dxGpDefaultColorMatrix;
  Alpha := 255;
end;

destructor TdxGPAlphaBlendAttributes.Destroy;
begin
  GdipCheck(GdipDisposeImageAttributes(FHandle));
  inherited Destroy;
end;

procedure TdxGPAlphaBlendAttributes.SetAlpha(AValue: Byte);
begin
  if AValue <> FAlpha then
  begin
    FAlpha := AValue;
    FColorMatrix[3, 3] := Alpha / 255;
    GdipCheck(GdipSetImageAttributesColorMatrix(FHandle,
      ColorAdjustTypeBitmap, True, @FColorMatrix, nil, ColorMatrixFlagsDefault));
  end;
end;

{ TdxGPImageDataHelper }

class function TdxGPImageDataHelper.IsBitmapStream(AStream: TStream): Boolean;
var
  AHeader: TBitmapFileHeader;
begin
  AStream.ReadBuffer(AHeader, SizeOf(AHeader));
  Result := AHeader.bfType = $4D42;
  AStream.Seek(-SizeOf(AHeader), soCurrent);
end;

//

procedure RegisterAssistants;
begin
  @FWindowFromDC := GetProcAddress(GetModuleHandle(user32), 'WindowFromDC');
  if CheckGdiPlus then
  begin
    CheckImageCodecs;
    RegisterClasses([TdxPNGImage, TdxSmartImage]);
  {$IFDEF DXREGISTERPNGIMAGE}
    if IsCodecIDValid(TIFFCodec) then
    begin
      TPicture.RegisterFileFormat('TIFF', 'TIFF graphics from DevExpress', TdxSmartImage);
      TPicture.RegisterFileFormat('TIF', 'TIFF graphics from DevExpress', TdxSmartImage);
    end;

    if IsCodecIDValid(GIFCodec) then
      TPicture.RegisterFileFormat('GIF', 'GIF graphics from DevExpress', TdxSmartImage);

    if IsCodecIDValid(JPEGCodec) then
    begin
      TPicture.RegisterFileFormat('JPEG', 'JPEG graphics from DevExpress', TdxSmartImage);
      TPicture.RegisterFileFormat('JPG', 'JPEG graphics from DevExpress', TdxSmartImage);
    end;

    if IsCodecIDValid(PNGCodec) then
      TPicture.RegisterFileFormat('PNG', 'PNG graphics from DevExpress', TdxSmartImage);
  {$ELSE}
    TPicture.RegisterFileFormat('', '', TdxGPImage);
    TPicture.RegisterFileFormat('', '', TdxSmartImage);
  {$ENDIF}
    TPicture.RegisterFileFormat('', '', TdxPNGImage);
  end;
end;

procedure UnregisterAssistants;
begin
  FreeAndNil(GPAlphaBlendAttributes);
  FreeAndNil(GPPaintCanvas);
  TPicture.UnregisterGraphicClass(TdxSmartImage);
  TPicture.UnregisterGraphicClass(TdxPNGImage);
  UnregisterClasses([TdxPNGImage, TdxSmartImage]);
end;

initialization
  dxUnitsLoader.AddUnit(@RegisterAssistants, @UnregisterAssistants);

finalization
  dxUnitsLoader.RemoveUnit(@UnregisterAssistants);
end.
