{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetClasses;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
{$IFDEF DELPHI12}
  AnsiStrings,
{$ENDIF}
  Windows, SysUtils, Classes, Generics.Collections, Generics.Defaults, Graphics, dxCore, dxCoreClasses, cxGraphics,
  dxSpreadSheetTypes, cxClasses, cxFormats, dxSpreadSheetStrs, dxGDIPlusClasses, dxHashUtils, dxSpreadSheetGraphics,
  cxVariants;

type
  TdxSpreadSheetCellStyles = class;

  { TdxSpreadSheetBordersHandle }

  TdxSpreadSheetBordersHandle = class(TdxHashTableItem)
  protected
    procedure CalculateHash; override;
    function CompareItem(const AItem: TdxHashTableItem): Integer; override;
  public
    BorderColor: array[TcxBorder] of TColor;
    BorderStyle: array[TcxBorder] of TdxSpreadSheetCellBorderStyle;
    constructor Create(AOwner: TdxDynamicItemList; AIndex: Integer); override;
    procedure Assign(ASource: TdxSpreadSheetBordersHandle); reintroduce; overload;
    function Clone: TdxSpreadSheetBordersHandle;
    procedure LoadFromStream(AReader: TcxReader);
    procedure SaveToStream(AWriter: TcxWriter);
  end;

  { TdxSpreadSheetBorders }

  TdxSpreadSheetBorders = class(TdxHashTable)
  strict private
    FDefaultBorders: TdxSpreadSheetBordersHandle;
    FStyles: TdxSpreadSheetCellStyles;
  protected
    procedure DeleteItem(AItem: TdxHashTableItem); override;
  public
    constructor Create(AStyles: TdxSpreadSheetCellStyles); reintroduce; overload;
    function AddBorders(const ABorders: TdxSpreadSheetBordersHandle): TdxSpreadSheetBordersHandle;
    function CreateBorders: TdxSpreadSheetBordersHandle;
    function CreateFromStream(AReader: TcxReader): TdxSpreadSheetBordersHandle;

    property DefaultBorders: TdxSpreadSheetBordersHandle read FDefaultBorders;
    property Styles: TdxSpreadSheetCellStyles read FStyles;
  end;

  { TdxSpreadSheetBrushHandle }

  TdxSpreadSheetBrushHandle = class(TdxHashTableItem)
  protected
    procedure CalculateHash; override;
    function CompareItem(const AItem: TdxHashTableItem): Integer; override;
  public
    BackgroundColor: TColor;
    ForegroundColor: TColor;
    Style: TdxSpreadSheetCellFillStyle;
    constructor Create(AOwner: TdxDynamicItemList; AIndex: Integer); override;
    procedure Assign(ASource: TdxSpreadSheetBrushHandle); reintroduce; overload;
    function Clone: TdxSpreadSheetBrushHandle;
    procedure LoadFromStream(AReader: TcxReader);
    procedure SaveToStream(AWriter: TcxWriter);
  end;

  { TdxSpreadSheetBrushes }

  TdxSpreadSheetBrushes = class(TdxHashTable)
  strict private
    FDefaultBrush: TdxSpreadSheetBrushHandle;
    FStyles: TdxSpreadSheetCellStyles;
  protected
    procedure DeleteItem(AItem: TdxHashTableItem); override;
  public
    constructor Create(AStyles: TdxSpreadSheetCellStyles); reintroduce; overload;
    function AddBrush(const ABrush: TdxSpreadSheetBrushHandle): TdxSpreadSheetBrushHandle;
    function CreateBrush: TdxSpreadSheetBrushHandle;
    function CreateFromStream(AReader: TcxReader): TdxSpreadSheetBrushHandle;

    property DefaultBrush: TdxSpreadSheetBrushHandle read FDefaultBrush;
    property Styles: TdxSpreadSheetCellStyles read FStyles;
  end;

  { TdxSpreadSheetFontHandle }

  TdxSpreadSheetFontHandle = class(TdxHashTableItem)
  strict private
    FGraphicObject: TFont;
    function GetGraphicObject: TFont; inline;
  protected
    procedure CalculateHash; override;
    function CompareItem(const AItem: TdxHashTableItem): Integer; override;
  public
    Charset: TFontCharset;
    Color: TColor;
    Name: TFontName;
    Pitch: TFontPitch;
    Size: Integer;
    Style: TFontStyles;
    destructor Destroy; override;
    procedure Assign(ASource: TdxSpreadSheetFontHandle); reintroduce; overload;
    procedure AssignToFont(ATargetFont: TFont); virtual;
    function Clone: TdxSpreadSheetFontHandle;
    procedure LoadFromStream(AReader: TcxReader);
    procedure SaveToStream(AWriter: TcxWriter);

    property GraphicObject: TFont read GetGraphicObject;
  end;

  { TdxSpreadSheetFonts }

  TdxSpreadSheetFonts = class(TdxHashTable)
  strict private
    FDefaultFont: TdxSpreadSheetFontHandle;
    FStyles: TdxSpreadSheetCellStyles;
    function InternalAddFont(const AName: TFontName; ASize: Integer): TdxSpreadSheetFontHandle;
  protected
    procedure DeleteItem(AItem: TdxHashTableItem); override;
  public
    constructor Create(AStyles: TdxSpreadSheetCellStyles); reintroduce; overload;
    function AddFont(const AFont: TdxSpreadSheetFontHandle): TdxSpreadSheetFontHandle;
    function CreateFont: TdxSpreadSheetFontHandle;
    function CreateFromStream(AReader: TcxReader): TdxSpreadSheetFontHandle;

    property DefaultFont: TdxSpreadSheetFontHandle read FDefaultFont;
    property Styles: TdxSpreadSheetCellStyles read FStyles;
  end;

  { TdxSpreadSheetFormatHandle }

  TdxSpreadSheetFormatHandle = class(TdxHashTableItem)
  protected
    procedure CalculateHash; override;
    function CompareItem(const AItem: TdxHashTableItem): Integer; override;
  public
    FormatCode: TdxUnicodeString;
    FormatCodeID: Integer;
    Formatter: TObject;

    constructor Create(const AFormatCode: TdxUnicodeString; const AFormatCodeID: Integer = -1); reintroduce; overload;
    destructor Destroy; override;
    procedure LoadFromStream(AReader: TcxReader);
    procedure SaveToStream(AWriter: TcxWriter);
  end;

  { TdxSpreadSheetPredefinedFormats }

  TdxSpreadSheetPredefinedFormats = class(TList<TdxSpreadSheetFormatHandle>)
  protected
    procedure Notify(const Item: TdxSpreadSheetFormatHandle; Action: TCollectionNotification); override;
  public
    function GetFormatHandleByID(ID: Integer): TdxSpreadSheetFormatHandle;
    function GetIDByFormatCode(const AFormatCode: TdxUnicodeString): Integer;
  end;

  { TdxSpreadSheetFormats }

  TdxSpreadSheetFormats = class(TdxHashTable)
  strict private
    FDefaultFormat: TdxSpreadSheetFormatHandle;
    FPredefinedFormats: TdxSpreadSheetPredefinedFormats;
    FStyles: TdxSpreadSheetCellStyles;
  protected
    procedure DeleteItem(AItem: TdxHashTableItem); override;
    procedure PopulatePredefinedFormats; virtual;
  public
    constructor Create(AStyles: TdxSpreadSheetCellStyles); reintroduce; overload;
    destructor Destroy; override;
    function AddFormat(const AFormatCode: TdxUnicodeString; AFormatCodeID: Integer = -1): TdxSpreadSheetFormatHandle;
    function CreateFromStream(AReader: TcxReader): TdxSpreadSheetFormatHandle;

    property DefaultFormat: TdxSpreadSheetFormatHandle read FDefaultFormat;
    property PredefinedFormats: TdxSpreadSheetPredefinedFormats read FPredefinedFormats;
    property Styles: TdxSpreadSheetCellStyles read FStyles;
  end;

  { TdxSpreadSheetSharedImageHandle }

  TdxSpreadSheetSharedImageHandle = class(TdxHashTableItem)
  strict private
    FImage: TdxSmartImage;
  protected
    procedure CalculateHash; override;
    function CompareItem(const AItem: TdxHashTableItem): Integer; override;
  public
    constructor Create; reintroduce; overload; virtual;
    constructor Create(AImage: TdxSmartImage); reintroduce; overload;
    constructor Create(AStream: TStream); reintroduce; overload;
    destructor Destroy; override;
    //
    property Image: TdxSmartImage read FImage;
  end;

  { TdxSpreadSheetSharedImages }

  TdxSpreadSheetSharedImages = class(TdxHashTable)
  public
    function Add(AImage: TdxSmartImage): TdxSpreadSheetSharedImageHandle; overload;
    function Add(AStream: TStream): TdxSpreadSheetSharedImageHandle; overload;
    function Add(const AFileName: string): TdxSpreadSheetSharedImageHandle; overload;
  end;

  { TdxSpreadSheetSharedString }
  
  TdxSpreadSheetSharedString = class(TdxHashTableItem)
  strict private
    function GetValue: TdxUnicodeString;
  protected
    FData: Pointer;
    FLength: Integer;
    procedure CalculateHash; override;
    function CompareItem(const AItem: TdxHashTableItem): Integer; override;
    class function ObjectID: Integer; virtual;
  public
    constructor CreateObject(const AValue: TdxUnicodeString); virtual;
    destructor Destroy; override;
    procedure LoadFromStream(AReader: TcxReader); virtual;
    procedure SaveToStream(AWriter: TcxWriter); virtual;

    property Value: TdxUnicodeString read GetValue;
  end;

  { TdxSpreadSheetSharedStringTable }

  TdxSpreadSheetSharedStringTable = class(TdxHashTable)
  private
    FFontTable: TdxSpreadSheetFonts;
  protected
    function CreateStringClassByID(const ID: Integer): TdxSpreadSheetSharedString; virtual;
  public
    constructor Create(AFontTable: TdxSpreadSheetFonts); reintroduce; virtual;
    function Add(AString: TdxSpreadSheetSharedString): TdxSpreadSheetSharedString; overload;
    function Add(const AValue: TdxUnicodeString): TdxSpreadSheetSharedString; overload;
    function LoadItemFromStream(AReader: TcxReader): TdxSpreadSheetSharedString;

    property FontTable: TdxSpreadSheetFonts read FFontTable;
  end;

  { TdxSpreadSheetFormattedSharedStringRun }

  TdxSpreadSheetFormattedSharedStringRun = class
  strict private
    FStartIndex: Integer;
    FFontHandle: TdxSpreadSheetFontHandle;
    procedure SetFontHandle(AValue: TdxSpreadSheetFontHandle);
  public
    destructor Destroy; override;
    //
    property FontHandle: TdxSpreadSheetFontHandle read FFontHandle write SetFontHandle;
    property StartIndex: Integer read FStartIndex write FStartIndex;
  end;

  { TdxSpreadSheetFormattedSharedStringRuns }

  TdxSpreadSheetFormattedSharedStringRuns = class(TcxObjectList)
  strict private
    function GetItem(Index: Integer): TdxSpreadSheetFormattedSharedStringRun;
  public
    function Add: TdxSpreadSheetFormattedSharedStringRun; overload;
    function Add(AStartIndex: Integer; AFontHandle: TdxSpreadSheetFontHandle): TdxSpreadSheetFormattedSharedStringRun; overload;
    procedure Assign(ASource: TdxSpreadSheetFormattedSharedStringRuns);
    //
    property Items[Index: Integer]: TdxSpreadSheetFormattedSharedStringRun read GetItem; default;
  end;

  { TdxSpreadSheetFormattedSharedString }

  TdxSpreadSheetFormattedSharedString = class(TdxSpreadSheetSharedString)
  strict private
    FRuns: TdxSpreadSheetFormattedSharedStringRuns;
  protected
    procedure CalculateHash; override;
    function CompareItem(const AItem: TdxHashTableItem): Integer; override;
    class function ObjectID: Integer; override;
  public
    constructor CreateObject(const AValue: TdxUnicodeString); override;
    destructor Destroy; override;
    procedure LoadFromStream(AReader: TcxReader); override;
    procedure SaveToStream(AWriter: TcxWriter); override;

    property Runs: TdxSpreadSheetFormattedSharedStringRuns read FRuns;
  end;

  { TdxSpreadSheetCellStyleHandle }

  TdxSpreadSheetCellStyleHandle = class(TdxHashTableItem)
  strict private
    FAlignHorz: TdxSpreadSheetDataAlignHorz;
    FAlignHorzIndent: Integer;
    FAlignVert: TdxSpreadSheetDataAlignVert;
    FBorders: TdxSpreadSheetBordersHandle;
    FBrush: TdxSpreadSheetBrushHandle;
    FDataFormat: TdxSpreadSheetFormatHandle;
    FFont: TdxSpreadSheetFontHandle;
    FRotation: Integer;
    FStates: TdxSpreadSheetCellStates;

    procedure SetAlignHorzIndent(AValue: Integer);
    procedure SetBorders(AValue: TdxSpreadSheetBordersHandle);
    procedure SetBrush(AValue: TdxSpreadSheetBrushHandle);
    procedure SetDataFormat(AValue: TdxSpreadSheetFormatHandle);
    procedure SetFont(AValue: TdxSpreadSheetFontHandle);
  protected
    procedure CalculateHash; override;
    function CompareItem(const AItem: TdxHashTableItem): Integer; override;
  public
    constructor Create(AOwner: TdxDynamicItemList; AIndex: Integer); override;
    destructor Destroy; override;
    procedure Assign(ASource: TdxSpreadSheetCellStyleHandle); reintroduce; overload;
    function Clone: TdxSpreadSheetCellStyleHandle;
    procedure LoadFromStream(AReader: TcxReader);
    procedure SaveToStream(AWriter: TcxWriter);

    property AlignHorz: TdxSpreadSheetDataAlignHorz read FAlignHorz write FAlignHorz;
    property AlignHorzIndent: Integer read FAlignHorzIndent write SetAlignHorzIndent;
    property AlignVert: TdxSpreadSheetDataAlignVert read FAlignVert write FAlignVert;
    property Borders: TdxSpreadSheetBordersHandle read FBorders write SetBorders;
    property Brush: TdxSpreadSheetBrushHandle read FBrush write SetBrush;
    property DataFormat: TdxSpreadSheetFormatHandle read FDataFormat write SetDataFormat;
    property Font: TdxSpreadSheetFontHandle read FFont write SetFont;
    property Rotation: Integer read FRotation write FRotation;
    property States: TdxSpreadSheetCellStates read FStates write FStates;
  end;

  { TdxSpreadSheetCellStyles }

  TdxSpreadSheetCellStyles = class(TdxHashTable)
  strict private
    FBorders: TdxSpreadSheetBorders;
    FBrushes: TdxSpreadSheetBrushes;
    FDefaultStyle: TdxSpreadSheetCellStyleHandle;
    FFonts: TdxSpreadSheetFonts;
    FFormats: TdxSpreadSheetFormats;
    FOnChange: TNotifyEvent;
  protected
    function CreateBorders: TdxSpreadSheetBorders; virtual;
    function CreateBrushes: TdxSpreadSheetBrushes; virtual;
    function CreateDefaultStyle: TdxSpreadSheetCellStyleHandle; virtual;
    function CreateFonts: TdxSpreadSheetFonts; virtual;
    function CreateFormats: TdxSpreadSheetFormats; virtual;
    procedure DeleteItem(AItem: TdxHashTableItem); override;
  public
    constructor Create; override;
    destructor Destroy; override;
    function AddStyle(AStyle: TdxSpreadSheetCellStyleHandle): TdxSpreadSheetCellStyleHandle;
    function CreateStyle(AFont: TdxSpreadSheetFontHandle = nil; AFormat: TdxSpreadSheetFormatHandle = nil;
      ABrush: TdxSpreadSheetBrushHandle = nil; ABorders: TdxSpreadSheetBordersHandle = nil): TdxSpreadSheetCellStyleHandle;
    function CreateStyleFromStream(AReader: TcxReader): TdxSpreadSheetCellStyleHandle;

    property Borders: TdxSpreadSheetBorders read FBorders;
    property Brushes: TdxSpreadSheetBrushes read FBrushes;
    property Fonts: TdxSpreadSheetFonts read FFonts;
    property Formats: TdxSpreadSheetFormats read FFormats;

    property DefaultStyle: TdxSpreadSheetCellStyleHandle read FDefaultStyle;

    property OnChange: TNotifyEvent read FOnChange write FOnChange;
  end;

  { TdxSpreadSheetFormatSettings }
  
  TdxSpreadSheetFormatSettings = class
  strict private
    function GetDecimalSeparator: TdxUnicodeChar; inline;
    function GetListSeparator: TdxUnicodeChar; inline;
    procedure SetDecimalSeparator(const AValue: TdxUnicodeChar); inline;
    procedure SetListSeparator(const AValue: TdxUnicodeChar); inline;
  protected
    function GetLocaleID: Integer; virtual;
    procedure UpdateOperations;
  public
    ArraySeparator: TdxUnicodeChar;
    BreakChars: TdxUnicodeString;
    CurrencyFormat: TdxUnicodeString;
    Data: TFormatSettings;
    DateTimeSystem: TdxSpreadSheetDataTimeSystem;
    Operations: TdxSpreadSheetOperationStrings;
    R1C1Reference: Boolean;

    constructor Create;
    function ExpandExternalLinks: Boolean; virtual;
    function GetFunctionName(const AName: Pointer): TdxUnicodeString; virtual;
    procedure UpdateSettings; virtual;

    property DecimalSeparator: TdxUnicodeChar read GetDecimalSeparator write SetDecimalSeparator;
    property ListSeparator: TdxUnicodeChar read GetListSeparator write SetListSeparator;
  end;

  { TdxSpreadSheetInvalidObject }

  TdxSpreadSheetInvalidObject = class
  public
    class procedure AssignTo(var AObject); inline;
    class function Instance: TObject;
    class function IsInvalid(const AObject: TObject): Boolean; inline;
    class function IsLive(const AObject: TObject): Boolean; inline;
  end;

function dxChangeHandle(var ACurrentValue: TdxHashTableItem; ANewValue: TdxHashTableItem): Boolean;

function dxFloatToStr(const AValue: Double; const AFormatSettings: TFormatSettings): TdxUnicodeString;
function dxTryStrToFloat(const AValue: TdxUnicodeString; var AFloatValue: Double; const AFormatSettings: TFormatSettings): Boolean;
function dxTryStrToNumeric(const AStr: TdxUnicodeString; out ANumeric: Double): Boolean;

implementation

uses
  Math, StrUtils, dxSpreadSheetUtils;

var
  dxSpreadSheetInvalidObject: TObject;

function dxChangeHandle(var ACurrentValue: TdxHashTableItem; ANewValue: TdxHashTableItem): Boolean;
var
  APrevValue: TdxHashTableItem;
begin
  Result := ACurrentValue <> ANewValue;
  if Result then
  begin
    APrevValue := ACurrentValue;
    ACurrentValue := ANewValue;
    if ACurrentValue <> nil then
      ACurrentValue.AddRef;
    if APrevValue <> nil then
      APrevValue.Release; 
  end;
end;

function dxFloatToStr(const AValue: Double; const AFormatSettings: TFormatSettings): TdxUnicodeString;

  function IsExponentialFormatStringShouldBeUsed(const AAbsValue: Extended): Boolean;
  begin
    if AAbsValue < 1 then
      Result := (AAbsValue > 0) and (AAbsValue < 1E-10) or (AAbsValue < 1E-08) and
        (Length(FormatFloat('0.############', AAbsValue, AFormatSettings)) > 9 + 2)
    else
      Result := AAbsValue > 1E11;
  end;

  function GetFormatString(const AAbsValue: Extended): string;
  begin
    if IsExponentialFormatStringShouldBeUsed(AAbsValue) then
      Result := '0.#####E+00'
    else
      Result := '0.' + DupeString('#', Max(0, 10 - Length(FormatFloat('0', AAbsValue, AFormatSettings))));
  end;

begin
  Result := FormatFloat(GetFormatString(Abs(AValue)), AValue, AFormatSettings);
end;

function dxTryStrToFloat(const AValue: TdxUnicodeString;
  var AFloatValue: Double; const AFormatSettings: TFormatSettings): Boolean;
begin
  Result := TryStrToFloat(AValue, AFloatValue, AFormatSettings);
end;

function dxTryStrToNumeric(const AStr: TdxUnicodeString; out ANumeric: Double): Boolean;
var
  ADate: TDateTime;
begin
  Result := TryStrToFloat(AStr, ANumeric);
  if not Result then
  begin
    Result := TryStrToDateTime(AStr, ADate);
    if Result then
      ANumeric := ADate;
  end;
end;

{ TdxSpreadSheetBordersHandle }

constructor TdxSpreadSheetBordersHandle.Create(AOwner: TdxDynamicItemList; AIndex: Integer);
var
  ABorder: TcxBorder;
begin
  inherited Create(AOwner, AIndex);
  for ABorder := Low(TcxBorder) to High(TcxBorder) do
    BorderColor[ABorder] := clDefault;
end;

procedure TdxSpreadSheetBordersHandle.Assign(ASource: TdxSpreadSheetBordersHandle);
begin
  Move(ASource.BorderColor, BorderColor, SizeOf(BorderColor));
  Move(ASource.BorderStyle, BorderStyle, SizeOf(BorderStyle));
end;

function TdxSpreadSheetBordersHandle.Clone: TdxSpreadSheetBordersHandle;
begin
  Result := TdxSpreadSheetBordersHandle.Create(nil, 0);
  Result.Assign(Self);
end;

procedure TdxSpreadSheetBordersHandle.LoadFromStream(AReader: TcxReader);
var
  ASide: TcxBorder;
begin
  for ASide := bLeft to bBottom do
  begin
    BorderColor[ASide] := AReader.ReadInteger;
    BorderStyle[ASide] := TdxSpreadSheetCellBorderStyle(AReader.ReadByte);
  end;
end;

procedure TdxSpreadSheetBordersHandle.SaveToStream(AWriter: TcxWriter);
var
  ASide: TcxBorder;
begin
  for ASide := bLeft to bBottom do
  begin
    AWriter.WriteInteger(BorderColor[ASide]);
    AWriter.WriteByte(Byte(BorderStyle[ASide]));
  end;
end;

procedure TdxSpreadSheetBordersHandle.CalculateHash;
var
  ABorder: TcxBorder;
  S: AnsiString;
begin
  S := '';
  for ABorder := Low(TcxBorder) to High(TcxBorder) do
  begin
    AddValueToString(S, BorderColor[ABorder], SizeOf(TColor));
    AddValueToString(S, BorderStyle[ABorder], SizeOf(TdxSpreadSheetCellBorderStyle));
  end;
  Key := dxElfHash(S);
end;

function TdxSpreadSheetBordersHandle.CompareItem(const AItem: TdxHashTableItem): Integer;
var
  ABorder: TcxBorder;
begin
  for ABorder := Low(TcxBorder) to High(TcxBorder) do
  begin
    Result := dxCompareValues(BorderColor[ABorder], TdxSpreadSheetBordersHandle(AItem).BorderColor[ABorder]);
    if Result = 0 then
      Result := dxCompareValues(Integer(BorderStyle[ABorder]), Integer(TdxSpreadSheetBordersHandle(AItem).BorderStyle[ABorder]));
    if Result <> 0 then
      Break;
  end;
end;

{ TdxSpreadSheetBorders }

constructor TdxSpreadSheetBorders.Create(AStyles: TdxSpreadSheetCellStyles);
begin
  inherited Create;
  FStyles := AStyles;
  FDefaultBorders := AddBorders(TdxSpreadSheetBordersHandle.Create(nil, 0));
end;

function TdxSpreadSheetBorders.AddBorders(const ABorders: TdxSpreadSheetBordersHandle): TdxSpreadSheetBordersHandle;
begin
  Result := ABorders;
  Result.CalculateHash;
  CheckAndAddItem(Result);
end;

function TdxSpreadSheetBorders.CreateBorders: TdxSpreadSheetBordersHandle;
begin
  Result := DefaultBorders.Clone;
end;

function TdxSpreadSheetBorders.CreateFromStream(AReader: TcxReader): TdxSpreadSheetBordersHandle;
begin
  Result := CreateBorders;
  Result.LoadFromStream(AReader);
  Result := AddBorders(Result);
end;

procedure TdxSpreadSheetBorders.DeleteItem(AItem: TdxHashTableItem);
begin
  if AItem = FDefaultBorders then
  begin
    if Styles.DefaultStyle <> nil then
      FDefaultBorders := Styles.DefaultStyle.Borders
    else
      FDefaultBorders := nil;
  end;
end;

{ TdxSpreadSheetBrushHandle }

constructor TdxSpreadSheetBrushHandle.Create(AOwner: TdxDynamicItemList; AIndex: Integer);
begin
  inherited Create(AOwner, AIndex);
  ForegroundColor := clDefault;
  BackgroundColor := clDefault;
end;

procedure TdxSpreadSheetBrushHandle.Assign(ASource: TdxSpreadSheetBrushHandle);
begin
  Style := ASource.Style;
  ForegroundColor := ASource.ForegroundColor;
  BackgroundColor := ASource.BackgroundColor;
end;

procedure TdxSpreadSheetBrushHandle.CalculateHash;
var
  S: AnsiString;
begin
  S := '';
  AddValueToString(S, ForegroundColor, SizeOf(ForegroundColor));
  AddValueToString(S, BackgroundColor, SizeOf(BackgroundColor));
  AddValueToString(S, Style, SizeOf(Style));
  Key := dxElfHash(S);
end;

function TdxSpreadSheetBrushHandle.Clone: TdxSpreadSheetBrushHandle;
begin
  Result := TdxSpreadSheetBrushHandle.Create(nil, 0);
  Result.Assign(Self);
end;

procedure TdxSpreadSheetBrushHandle.LoadFromStream(AReader: TcxReader);
begin
  BackgroundColor := AReader.ReadInteger;
  ForegroundColor := AReader.ReadInteger;
  Style := TdxSpreadSheetCellFillStyle(AReader.ReadByte);
end;

procedure TdxSpreadSheetBrushHandle.SaveToStream(AWriter: TcxWriter);
begin
  AWriter.WriteInteger(BackgroundColor);
  AWriter.WriteInteger(ForegroundColor);
  AWriter.WriteByte(Byte(Style));
end;

function TdxSpreadSheetBrushHandle.CompareItem(const AItem: TdxHashTableItem): Integer;
begin
  Result := dxCompareValues(ForegroundColor, TdxSpreadSheetBrushHandle(AItem).ForegroundColor);
  if Result = 0 then
    Result := dxCompareValues(BackgroundColor, TdxSpreadSheetBrushHandle(AItem).BackgroundColor);
  if Result = 0 then
    Result := dxCompareValues(Integer(Style), Integer(TdxSpreadSheetBrushHandle(AItem).Style));
end;

{ TdxSpreadSheetBrushes }

constructor TdxSpreadSheetBrushes.Create(AStyles: TdxSpreadSheetCellStyles);
begin
  inherited Create;
  FStyles := AStyles;
  FDefaultBrush := AddBrush(TdxSpreadSheetBrushHandle.Create(nil, 0));
end;

function TdxSpreadSheetBrushes.AddBrush(const ABrush: TdxSpreadSheetBrushHandle): TdxSpreadSheetBrushHandle;
begin
  Result := ABrush;
  Result.CalculateHash;
  CheckAndAddItem(Result);
end;

function TdxSpreadSheetBrushes.CreateBrush: TdxSpreadSheetBrushHandle;
begin
  Result := DefaultBrush.Clone;
end;

function TdxSpreadSheetBrushes.CreateFromStream(AReader: TcxReader): TdxSpreadSheetBrushHandle;
begin
  Result := CreateBrush;
  Result.LoadFromStream(AReader);
  Result := AddBrush(Result);
end;

procedure TdxSpreadSheetBrushes.DeleteItem(AItem: TdxHashTableItem);
begin
  if AItem = FDefaultBrush then
  begin
    if Styles.DefaultStyle <> nil then
      FDefaultBrush := Styles.DefaultStyle.Brush
    else
      FDefaultBrush := nil;
  end;
end;

{ TdxSpreadSheetFontHandle }

destructor TdxSpreadSheetFontHandle.Destroy;
begin
  FreeAndNil(FGraphicObject);
  inherited Destroy;
end;

procedure TdxSpreadSheetFontHandle.Assign(ASource: TdxSpreadSheetFontHandle);
begin
  Charset := ASource.Charset;
  Color := ASource.Color;
  Size := ASource.Size;
  Name := ASource.Name;
  Pitch := ASource.Pitch;
  Style := ASource.Style;
end;

procedure TdxSpreadSheetFontHandle.AssignToFont(ATargetFont: TFont);
begin
  ATargetFont.Charset := Charset;
  ATargetFont.Color := Color;
  ATargetFont.Size := Size;
  ATargetFont.Name := Name;
  ATargetFont.Pitch := Pitch;
  ATargetFont.Style := Style;
end;

procedure TdxSpreadSheetFontHandle.CalculateHash;
var
  S: AnsiString;
begin
  S := dxStringToAnsiString(Name);
  AddValueToString(S, Charset, SizeOf(TFontCharset));
  AddValueToString(S, Color, SizeOf(TColor));
  AddValueToString(S, Size, SizeOf(Integer));
  AddValueToString(S, Pitch, SizeOf(TFontPitch));
  AddValueToString(S, Style, SizeOf(TFontStyles));
  Key := dxElfHash(S);
end;

function TdxSpreadSheetFontHandle.Clone: TdxSpreadSheetFontHandle;
begin
  Result := TdxSpreadSheetFontHandle.Create(nil, 0);
  Result.Assign(Self);
end;

procedure TdxSpreadSheetFontHandle.LoadFromStream(AReader: TcxReader);
begin
  Charset := AReader.ReadByte;
  Color := AReader.ReadInteger;
  Size := AReader.ReadInteger;
  Name := AReader.ReadWideString;
  Pitch := TFontPitch(AReader.ReadByte);
  AReader.Stream.ReadBuffer(Style, SizeOf(Style));
end;

procedure TdxSpreadSheetFontHandle.SaveToStream(AWriter: TcxWriter);
begin
  AWriter.WriteByte(Charset);
  AWriter.WriteInteger(Color);
  AWriter.WriteInteger(Size);
  AWriter.WriteWideString(Name);
  AWriter.WriteByte(Byte(Pitch));
  AWriter.Stream.WriteBuffer(Style, SizeOf(Style));
end;

function TdxSpreadSheetFontHandle.CompareItem(const AItem: TdxHashTableItem): Integer;
begin
  Result := AnsiCompareStr(Name, TdxSpreadSheetFontHandle(AItem).Name);
  if Result = 0 then
    Result := dxCompareValues(Integer(Charset), Integer(TdxSpreadSheetFontHandle(AItem).Charset));
  if Result = 0 then
    Result := dxCompareValues(Integer(Color), Integer(TdxSpreadSheetFontHandle(AItem).Color));
  if Result = 0 then
    Result := dxCompareValues(Integer(Size), Integer(TdxSpreadSheetFontHandle(AItem).Size));
  if Result = 0 then
    Result := dxCompareValues(Integer(Pitch), Integer(TdxSpreadSheetFontHandle(AItem).Pitch));
  if Result = 0 then
    Result := dxCompareValues(Byte(Style), Byte(TdxSpreadSheetFontHandle(AItem).Style));
end;

function TdxSpreadSheetFontHandle.GetGraphicObject: TFont;
begin
  if FGraphicObject = nil then
  begin
    FGraphicObject := TFont.Create;
    AssignToFont(FGraphicObject);
  end;
  Result := FGraphicObject;
end;

{ TdxSpreadSheetFonts }

constructor TdxSpreadSheetFonts.Create(AStyles: TdxSpreadSheetCellStyles);
begin
  inherited Create;
  FStyles := AStyles;
  FDefaultFont := InternalAddFont('Calibri', 11);
end;

function TdxSpreadSheetFonts.AddFont(const AFont: TdxSpreadSheetFontHandle): TdxSpreadSheetFontHandle;
begin
  Result := AFont;
  Result.CalculateHash;
  CheckAndAddItem(Result);
end;

function TdxSpreadSheetFonts.CreateFont: TdxSpreadSheetFontHandle;
begin
  Result := DefaultFont.Clone;
end;

function TdxSpreadSheetFonts.CreateFromStream(AReader: TcxReader): TdxSpreadSheetFontHandle;
begin
  Result := CreateFont;
  Result.LoadFromStream(AReader);
  Result := AddFont(Result);
end;

procedure TdxSpreadSheetFonts.DeleteItem(AItem: TdxHashTableItem);
begin
  if AItem = FDefaultFont then
  begin
    if Styles.DefaultStyle <> nil then
      FDefaultFont := Styles.DefaultStyle.Font
    else
      FDefaultFont := nil;
  end;
end;

function TdxSpreadSheetFonts.InternalAddFont(const AName: TFontName; ASize: Integer): TdxSpreadSheetFontHandle;
begin
  Result := TdxSpreadSheetFontHandle.Create(nil, 0);
  Result.Name := AName;
  Result.Charset := DEFAULT_CHARSET;
  Result.Color := clDefault;
  Result.Size := ASize;
  CheckAndAddItem(Result);
end;

{ TdxSpreadSheetFormatHandle }

constructor TdxSpreadSheetFormatHandle.Create(const AFormatCode: TdxUnicodeString; const AFormatCodeID: Integer = -1);
begin
  FormatCode := AFormatCode;
  FormatCodeID := AFormatCodeID;
  inherited Create(nil, 0);
end;

destructor TdxSpreadSheetFormatHandle.Destroy;
begin
  FreeAndNil(Formatter);
  inherited Destroy;
end;

procedure TdxSpreadSheetFormatHandle.LoadFromStream(AReader: TcxReader);
begin
  FormatCode := AReader.ReadWideString;
  FormatCodeID := AReader.ReadInteger;
  FreeAndNil(Formatter);
end;

procedure TdxSpreadSheetFormatHandle.SaveToStream(AWriter: TcxWriter);
begin
  AWriter.WriteWideString(FormatCode);
  AWriter.WriteInteger(FormatCodeID);
end;

procedure TdxSpreadSheetFormatHandle.CalculateHash;
begin
  Key := dxElfHash(FormatCode);
end;

function TdxSpreadSheetFormatHandle.CompareItem(const AItem: TdxHashTableItem): Integer;
begin
  Result := AnsiCompareStr(FormatCode, TdxSpreadSheetFormatHandle(AItem).FormatCode);
  if Result = 0 then
    Result := FormatCodeID - TdxSpreadSheetFormatHandle(AItem).FormatCodeID;
end;

{ TdxSpreadSheetPredefinedFormats }

function TdxSpreadSheetPredefinedFormats.GetFormatHandleByID(ID: Integer): TdxSpreadSheetFormatHandle;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to Count - 1 do
    if Items[I].FormatCodeID = ID then
    begin
      Result := Items[I];
      Break;
    end;
end;

function TdxSpreadSheetPredefinedFormats.GetIDByFormatCode(const AFormatCode: TdxUnicodeString): Integer;
var
  I: Integer;
begin
  Result := -1;
  for I := 0 to Count - 1 do
    if Items[I].FormatCode = AFormatCode then
    begin
      Result := Items[I].FormatCodeID;
      Break;
    end;
end;

procedure TdxSpreadSheetPredefinedFormats.Notify(
  const Item: TdxSpreadSheetFormatHandle; Action: TCollectionNotification);
begin
  inherited Notify(Item, Action);
  case Action of
    cnAdded:
      Item.AddRef;
    cnRemoved:
      Item.Release;
  end;
end;

{ TdxSpreadSheetFormats }

constructor TdxSpreadSheetFormats.Create(AStyles: TdxSpreadSheetCellStyles);
begin
  inherited Create;
  FStyles := AStyles;
  FPredefinedFormats := TdxSpreadSheetPredefinedFormats.Create;
  PopulatePredefinedFormats;
  FDefaultFormat := PredefinedFormats.First;
end;

destructor TdxSpreadSheetFormats.Destroy;
begin
  FreeAndNil(FPredefinedFormats);
  inherited Destroy;
end;

function TdxSpreadSheetFormats.AddFormat(const AFormatCode: TdxUnicodeString; AFormatCodeID: Integer = -1): TdxSpreadSheetFormatHandle;
begin
  Result := TdxSpreadSheetFormatHandle.Create(AFormatCode, AFormatCodeID);
  Result.CalculateHash;
  CheckAndAddItem(Result);
end;

function TdxSpreadSheetFormats.CreateFromStream(AReader: TcxReader): TdxSpreadSheetFormatHandle;
var
  AValue: TdxUnicodeString;
  ACode: Integer;
begin
  AValue := AReader.ReadWideString;
  ACode := AReader.ReadInteger;
  Result := AddFormat(AValue, ACode);
end;

procedure TdxSpreadSheetFormats.DeleteItem(AItem: TdxHashTableItem);
begin
  if AItem = FDefaultFormat then
  begin
    if Styles.DefaultStyle <> nil then
      FDefaultFormat := Styles.DefaultStyle.DataFormat
    else
      FDefaultFormat := nil;
  end;
end;

procedure TdxSpreadSheetFormats.PopulatePredefinedFormats;

  procedure InternalAdd(AFormatCodeID: Integer; const AFormatCode: TdxUnicodeString);
  begin
    PredefinedFormats.Add(AddFormat(AFormatCode, AFormatCodeID));
  end;

begin
  InternalAdd($00, dxSpreadSheetGeneralNumberFormat);
  InternalAdd($01, '0');
  InternalAdd($02, '0.00');
  InternalAdd($03, '#,##0');
  InternalAdd($04, '#,##0.00');
  InternalAdd($05, '$#,##0_);($#,##0)');
  InternalAdd($06, '$#,##0_);[Red]($#,##0)');
  InternalAdd($07, '$#,##0.00_);($#,##0.00)');
  InternalAdd($08, '$#,##0.00_);[Red]($#,##0.00)');
  InternalAdd($09, '0%');
  InternalAdd($0a, '0.00%');
  InternalAdd($0b, '0.00E+00');
  InternalAdd($0c, '# ?/?');
  InternalAdd($0d, '# ??/??');
  InternalAdd($0e, 'm/d/yy');
  InternalAdd($0f, 'd-mmm-yy');
  InternalAdd($10, 'd-mmm');
  InternalAdd($11, 'mmm-yy');
  InternalAdd($12, 'h:mm AM/PM');
  InternalAdd($13, 'h:mm:ss AM/PM');
  InternalAdd($14, 'h:mm');
  InternalAdd($15, 'h:mm:ss');
  InternalAdd($16, 'm/d/yy h:mm');
  InternalAdd($25, '#,##0_);(#,##0)');
  InternalAdd($26, '#,##0_);[Red](#,##0)');
  InternalAdd($27, '#,##0.00_);(#,##0.00)');
  InternalAdd($28, '#,##0.00_);[Red](#,##0.00)');
  InternalAdd($29, '_(* #,##0_);_(* (#,##0);_(* "-"_);_(@_)');
  InternalAdd($2a, '_($* #,##0_);_($* (#,##0);_($* "-"_);_(@_)');
  InternalAdd($2b, '_(* #,##0.00_);_(* (#,##0.00);_(* "-"??_);_(@_)');
  InternalAdd($2c, '_($* #,##0.00_);_($* (#,##0.00);_($* "-"??_);_(@_)');
  InternalAdd($2d, 'mm:ss');
  InternalAdd($2e, '[h]:mm:ss');
  InternalAdd($2f, 'mm:ss.0');
  InternalAdd($30, '##0.0E+0');
  InternalAdd($31, '@');
end;

{ TdxSpreadSheetSharedImageHandle }

constructor TdxSpreadSheetSharedImageHandle.Create;
begin
  inherited Create(nil, 0);
  FImage := TdxSmartImage.Create;
end;

constructor TdxSpreadSheetSharedImageHandle.Create(AImage: TdxSmartImage);
begin
  Create;
  try
    Image.Assign(AImage);
    Image.HandleNeeded;
  except
    Image.Clear;
  end;
end;

constructor TdxSpreadSheetSharedImageHandle.Create(AStream: TStream);
begin
  Create;
  try
    Image.LoadFromStream(AStream);
    Image.HandleNeeded;
  except
    Image.Clear;
  end;
end;

destructor TdxSpreadSheetSharedImageHandle.Destroy;
begin
  FreeAndNil(FImage);
  inherited Destroy;
end;

procedure TdxSpreadSheetSharedImageHandle.CalculateHash;
begin
  Key := Image.GetHashCode;
end;

function TdxSpreadSheetSharedImageHandle.CompareItem(const AItem: TdxHashTableItem): Integer;
begin
  if Image.Compare(TdxSpreadSheetSharedImageHandle(AItem).Image) then
    Result := 0
  else
    Result := TdxNativeInt(Self) - TdxNativeInt(AItem);
end;

{ TdxSpreadSheetSharedImages }

function TdxSpreadSheetSharedImages.Add(AStream: TStream): TdxSpreadSheetSharedImageHandle;
begin
  Result := TdxSpreadSheetSharedImageHandle.Create(AStream);
  Result.CalculateHash;
  CheckAndAddItem(Result);
end;

function TdxSpreadSheetSharedImages.Add(const AFileName: string): TdxSpreadSheetSharedImageHandle;
var
  AStream: TStream;
begin
  AStream := TFileStream.Create(AFileName, fmOpenRead or fmShareDenyNone);
  try
    Result := Add(AStream);
  finally
    AStream.Free;
  end;
end;

function TdxSpreadSheetSharedImages.Add(AImage: TdxSmartImage): TdxSpreadSheetSharedImageHandle;
begin
  Result := TdxSpreadSheetSharedImageHandle.Create(AImage);
  Result.CalculateHash;
  CheckAndAddItem(Result);
end;

{ TdxSpreadSheetSharedString }

constructor TdxSpreadSheetSharedString.CreateObject(const AValue: TdxUnicodeString);
begin
  FLength := Length(AValue);
  FData := AllocMem((FLength + 1) * SizeOf(WideChar));
  Move(AValue[1], FData^, FLength * SizeOf(WideChar));
  inherited Create(nil, 0);
end;

destructor TdxSpreadSheetSharedString.Destroy;
begin
  FreeMem(FData);
  inherited Destroy;
end;


procedure TdxSpreadSheetSharedString.LoadFromStream(AReader: TcxReader);
begin
  FLength := AReader.ReadInteger();
  FreeMem(FData);
  if FLength > 0 then
  begin
    FData := AllocMem((FLength + 1) * SizeOf(WideChar));
    AReader.Stream.ReadBuffer(FData^, FLength * SizeOf(WideChar));
  end;
end;

procedure TdxSpreadSheetSharedString.SaveToStream(AWriter: TcxWriter);
begin
  AWriter.WriteInteger(ObjectID);
  AWriter.WriteInteger(FLength);
  if FLength > 0 then
    AWriter.Stream.WriteBuffer(FData^, FLength * SizeOf(WideChar));
end;

procedure TdxSpreadSheetSharedString.CalculateHash;
begin
  Key := dxElfHash(FData, FLength, nil, 0);
end;

function TdxSpreadSheetSharedString.CompareItem(const AItem: TdxHashTableItem): Integer;
begin
  Result := Integer(ClassType) - Integer(AItem.ClassType);
  if Result = 0 then
    Result := CompareStringW(LOCALE_USER_DEFAULT, 0, FData, FLength,
      TdxSpreadSheetSharedString(AItem).FData, TdxSpreadSheetSharedString(AItem).FLength) - 2;
end;

class function TdxSpreadSheetSharedString.ObjectID: Integer;
begin
  Result := 0;
end;

function TdxSpreadSheetSharedString.GetValue: TdxUnicodeString;
begin
  SetString(Result, PWideChar(FData), FLength);
end;

{ TdxSpreadSheetSharedStringTable }

constructor TdxSpreadSheetSharedStringTable.Create(AFontTable: TdxSpreadSheetFonts);
begin
  inherited Create;
  FFontTable := AFontTable;
end;

function TdxSpreadSheetSharedStringTable.Add(const AValue: TdxUnicodeString): TdxSpreadSheetSharedString;
begin
  Result := Add(TdxSpreadSheetSharedString.CreateObject(AValue));
end;

function TdxSpreadSheetSharedStringTable.Add(AString: TdxSpreadSheetSharedString): TdxSpreadSheetSharedString;
begin
  Result := AString;
  Result.CalculateHash;
  CheckAndAddItem(Result);
end;

function TdxSpreadSheetSharedStringTable.LoadItemFromStream(AReader: TcxReader): TdxSpreadSheetSharedString;
begin
  Result := CreateStringClassByID(AReader.ReadInteger);
  Result.FHashTable := Self;
  Result.LoadFromStream(AReader);
  Result := Add(Result);
end;

function TdxSpreadSheetSharedStringTable.CreateStringClassByID(const ID: Integer): TdxSpreadSheetSharedString;
begin
  if ID = 0 then
    Result := TdxSpreadSheetSharedString.CreateObject('')
  else
    Result := TdxSpreadSheetFormattedSharedString.CreateObject('')
end;

{ TdxSpreadSheetFormattedSharedStringRun }

destructor TdxSpreadSheetFormattedSharedStringRun.Destroy;
begin
  FontHandle := nil;
  inherited Destroy;
end;

procedure TdxSpreadSheetFormattedSharedStringRun.SetFontHandle(AValue: TdxSpreadSheetFontHandle);
begin
  dxChangeHandle(TdxHashTableItem(FFontHandle), AValue);
end;

{ TdxSpreadSheetFormattedSharedStringRuns }

function TdxSpreadSheetFormattedSharedStringRuns.Add: TdxSpreadSheetFormattedSharedStringRun;
begin
  Result := TdxSpreadSheetFormattedSharedStringRun.Create;
  inherited Add(Result);
end;

function TdxSpreadSheetFormattedSharedStringRuns.Add(
  AStartIndex: Integer; AFontHandle: TdxSpreadSheetFontHandle): TdxSpreadSheetFormattedSharedStringRun;
begin
  Result := Add;
  Result.StartIndex := AStartIndex;
  Result.FontHandle := AFontHandle;
end;

procedure TdxSpreadSheetFormattedSharedStringRuns.Assign(ASource: TdxSpreadSheetFormattedSharedStringRuns);
var
  I: Integer;
begin
  Clear;
  for I := 0 to ASource.Count - 1 do
    Add(ASource[I].StartIndex, ASource[I].FontHandle);
end;

function TdxSpreadSheetFormattedSharedStringRuns.GetItem(Index: Integer): TdxSpreadSheetFormattedSharedStringRun;
begin
  Result := TdxSpreadSheetFormattedSharedStringRun(inherited Items[Index]);
end;

{ TdxSpreadSheetFormattedSharedString }

constructor TdxSpreadSheetFormattedSharedString.CreateObject(const AValue: TdxUnicodeString);
begin
  inherited CreateObject(AValue);
  FRuns := TdxSpreadSheetFormattedSharedStringRuns.Create;
end;

destructor TdxSpreadSheetFormattedSharedString.Destroy;
begin
  FreeAndNil(FRuns);
  inherited Destroy;
end;

procedure TdxSpreadSheetFormattedSharedString.LoadFromStream(AReader: TcxReader);
var
  AIndex, I: Integer;
  AFont: TdxSpreadSheetFontHandle;
begin
  inherited LoadFromStream(AReader);
  for I := 0 to AReader.ReadInteger - 1 do
  begin
    AIndex := AReader.ReadInteger;
    AFont := TdxSpreadSheetSharedStringTable(HashTable).FontTable.CreateFromStream(AReader);
    Runs.Add(AIndex, AFont);
  end;
end;

procedure TdxSpreadSheetFormattedSharedString.SaveToStream(AWriter: TcxWriter);
var
  I: Integer;
begin
  inherited SaveToStream(AWriter);
  AWriter.WriteInteger(Runs.Count);
  for I := 0 to Runs.Count - 1 do
  begin
    AWriter.WriteInteger(Runs[I].StartIndex);
    Runs[I].FontHandle.SaveToStream(AWriter);
  end;
end;

procedure TdxSpreadSheetFormattedSharedString.CalculateHash;
var
  ARun: TdxSpreadSheetFormattedSharedStringRun;
  AValue: Cardinal;
  I: Integer;
  S: AnsiString;
begin
  S := '';
  AValue := dxElfHash(FData, FLength, nil, 0);
  AddValueToString(S, AValue, SizeOf(AValue));
  for I := 0 to Runs.Count - 1 do
  begin
    ARun := Runs[I];

    AValue := ARun.StartIndex;
    AddValueToString(S, AValue, SizeOf(AValue));

    AValue := ARun.FontHandle.Key;
    AddValueToString(S, AValue, SizeOf(AValue));
  end;
  Key := dxElfHash(S);
end;

function TdxSpreadSheetFormattedSharedString.CompareItem(const AItem: TdxHashTableItem): Integer;
var
  I: Integer;
begin
  Result := inherited CompareItem(AItem);
  if Result = 0 then
    Result := dxCompareValues(Runs.Count, TdxSpreadSheetFormattedSharedString(AItem).Runs.Count);
  if Result = 0 then
    for I := 0 to Runs.Count - 1 do
    begin
      Result := dxCompareValues(Runs[I].StartIndex, TdxSpreadSheetFormattedSharedString(AItem).Runs[I].StartIndex);
      if Result = 0 then
        Result := dxCompareValues(Runs[I].FontHandle, TdxSpreadSheetFormattedSharedString(AItem).Runs[I].FontHandle);
      if Result <> 0 then
        Break;
    end;
end;

class function TdxSpreadSheetFormattedSharedString.ObjectID: Integer;
begin
  Result := 1;
end;

{ TdxSpreadSheetCellStyleHandle }

constructor TdxSpreadSheetCellStyleHandle.Create(AOwner: TdxDynamicItemList; AIndex: Integer);
begin
  inherited Create(AOwner, AIndex);
  AlignVert := ssavBottom;
  States := States + [csLocked];
end;

destructor TdxSpreadSheetCellStyleHandle.Destroy;
begin
  Borders := nil;
  Brush := nil;
  DataFormat := nil;
  Font := nil;
  inherited Destroy;
end;

procedure TdxSpreadSheetCellStyleHandle.Assign(ASource: TdxSpreadSheetCellStyleHandle);
begin
  AlignHorz := ASource.AlignHorz;
  AlignHorzIndent := ASource.AlignHorzIndent;
  AlignVert := ASource.AlignVert;
  States := ASource.States;
  Font := ASource.Font;
  DataFormat := ASource.DataFormat;
  Brush := ASource.Brush;
  Borders := ASource.Borders;
  Rotation := ASource.Rotation;
end;

function TdxSpreadSheetCellStyleHandle.Clone: TdxSpreadSheetCellStyleHandle;
begin
  Result := TdxSpreadSheetCellStyleHandle.Create(nil, 0);
  Result.Assign(Self);
end;

procedure TdxSpreadSheetCellStyleHandle.LoadFromStream(AReader: TcxReader);
begin
  Byte(FAlignHorz) := AReader.ReadByte;
  Byte(FAlignVert) := AReader.ReadByte;
  FAlignHorzIndent := AReader.ReadInteger;
  FRotation := AReader.ReadInteger;
  AReader.Stream.ReadBuffer(FStates, SizeOf(FStates));
end;

procedure TdxSpreadSheetCellStyleHandle.SaveToStream(AWriter: TcxWriter);
begin
  Font.SaveToStream(AWriter);
  Brush.SaveToStream(AWriter);
  Borders.SaveToStream(AWriter);
  DataFormat.SaveToStream(AWriter);
  AWriter.WriteByte(Byte(AlignHorz));
  AWriter.WriteByte(Byte(AlignVert));
  AWriter.WriteInteger(AlignHorzIndent);
  AWriter.WriteInteger(Rotation);
  AWriter.Stream.WriteBuffer(FStates, SizeOf(FStates));
end;

procedure TdxSpreadSheetCellStyleHandle.CalculateHash;
var
  AValue: Boolean;
  I: TdxSpreadSheetCellState;
  S: AnsiString;
begin
  S := '';
  AddValueToString(S, AlignHorz, SizeOf(AlignHorz));
  AddValueToString(S, AlignHorzIndent, SizeOf(AlignHorzIndent));
  AddValueToString(S, AlignVert, SizeOf(AlignVert));
  AddValueToString(S, Font, SizeOf(Font));
  AddValueToString(S, DataFormat, SizeOf(DataFormat));
  AddValueToString(S, Brush, SizeOf(Brush));
  AddValueToString(S, Borders, SizeOf(Borders));
  AddValueToString(S, Rotation, SizeOf(Rotation));

  for I := Low(I) to High(I) do
  begin
    AValue := I in States;
    AddValueToString(S, AValue, SizeOf(AValue));
  end;

  Key := dxElfHash(S);
end;

function TdxSpreadSheetCellStyleHandle.CompareItem(const AItem: TdxHashTableItem): Integer;
var
  I: TdxSpreadSheetCellState;
begin
  Result := dxCompareValues(Integer(AlignHorz), Integer(TdxSpreadSheetCellStyleHandle(AItem).AlignHorz));
  if Result = 0 then
    Result := dxCompareValues(AlignHorzIndent, TdxSpreadSheetCellStyleHandle(AItem).AlignHorzIndent);
  if Result = 0 then
    Result := dxCompareValues(Integer(AlignVert), Integer(TdxSpreadSheetCellStyleHandle(AItem).AlignVert));
  if Result = 0 then
    Result := dxCompareValues(Rotation, TdxSpreadSheetCellStyleHandle(AItem).Rotation);
  if Result = 0 then
    Result := Font.CompareItem(TdxSpreadSheetCellStyleHandle(AItem).Font);
  if Result = 0 then
    Result := DataFormat.CompareItem(TdxSpreadSheetCellStyleHandle(AItem).DataFormat);
  if Result = 0 then
    Result := Brush.CompareItem(TdxSpreadSheetCellStyleHandle(AItem).Brush);
  if Result = 0 then
    Result := Borders.CompareItem(TdxSpreadSheetCellStyleHandle(AItem).Borders);
  if Result = 0 then
    for I := Low(I) to High(I) do
    begin
      Result := dxCompareValues(Integer(I in States), Integer(I in TdxSpreadSheetCellStyleHandle(AItem).States));
      if Result <> 0 then
        Break;
    end;
end;

procedure TdxSpreadSheetCellStyleHandle.SetAlignHorzIndent(AValue: Integer);
begin
  FAlignHorzIndent := Max(0, Min(AValue, 250));
end;

procedure TdxSpreadSheetCellStyleHandle.SetBorders(AValue: TdxSpreadSheetBordersHandle);
begin
  dxChangeHandle(TdxHashTableItem(FBorders), AValue);
end;

procedure TdxSpreadSheetCellStyleHandle.SetBrush(AValue: TdxSpreadSheetBrushHandle);
begin
  dxChangeHandle(TdxHashTableItem(FBrush), AValue);
end;

procedure TdxSpreadSheetCellStyleHandle.SetDataFormat(AValue: TdxSpreadSheetFormatHandle);
begin
  dxChangeHandle(TdxHashTableItem(FDataFormat), AValue);
end;

procedure TdxSpreadSheetCellStyleHandle.SetFont(AValue: TdxSpreadSheetFontHandle);
begin
  dxChangeHandle(TdxHashTableItem(FFont), AValue);
end;

{ TdxSpreadSheetCellStyles }

constructor TdxSpreadSheetCellStyles.Create;
begin
  inherited Create;
  FFonts := CreateFonts;
  FBrushes := CreateBrushes;
  FFormats := CreateFormats;
  FBorders := CreateBorders;

  FDefaultStyle := CreateDefaultStyle;
  FDefaultStyle.AddRef;
  FDefaultStyle.CalculateHash;
end;

destructor TdxSpreadSheetCellStyles.Destroy;
begin
  FDefaultStyle.Release;
  Clear;
  FreeAndNil(FBorders);
  FreeAndNil(FBrushes);
  FreeAndNil(FFormats);
  FreeAndNil(FFonts);
  inherited Destroy;
end;

function TdxSpreadSheetCellStyles.AddStyle(AStyle: TdxSpreadSheetCellStyleHandle): TdxSpreadSheetCellStyleHandle;
begin
  Result := AStyle;
  Result.CalculateHash;
  if (DefaultStyle.Index = Result.Index) and DefaultStyle.IsEqual(AStyle) then
  begin
    Result.Free;
    Result := DefaultStyle;
  end
  else
    CheckAndAddItem(Result);
end;

function TdxSpreadSheetCellStyles.CreateBorders: TdxSpreadSheetBorders;
begin
  Result := TdxSpreadSheetBorders.Create(Self);
end;

function TdxSpreadSheetCellStyles.CreateStyle(
  AFont: TdxSpreadSheetFontHandle = nil; AFormat: TdxSpreadSheetFormatHandle = nil;
  ABrush: TdxSpreadSheetBrushHandle = nil; ABorders: TdxSpreadSheetBordersHandle = nil): TdxSpreadSheetCellStyleHandle;
begin
  if AFont = nil then
    AFont := DefaultStyle.Font;
  if AFormat = nil then
    AFormat := DefaultStyle.DataFormat;
  if ABrush = nil then
    ABrush := DefaultStyle.Brush;
  if ABorders = nil then
    ABorders := DefaultStyle.Borders;

  Result := TdxSpreadSheetCellStyleHandle.Create(nil, 0);
  Result.Borders := ABorders;
  Result.DataFormat := AFormat;
  Result.Brush := ABrush;
  Result.Font := AFont;
end;

function TdxSpreadSheetCellStyles.CreateStyleFromStream(AReader: TcxReader): TdxSpreadSheetCellStyleHandle;
var
  AFont: TdxSpreadSheetFontHandle;
  ABrush: TdxSpreadSheetBrushHandle;
  ABorders: TdxSpreadSheetBordersHandle;
  ADataFormat: TdxSpreadSheetFormatHandle;
begin
  AFont := Fonts.CreateFromStream(AReader);
  ABrush := Brushes.CreateFromStream(AReader);
  ABorders := Borders.CreateFromStream(AReader);
  ADataFormat := Formats.CreateFromStream(AReader);

  Result := CreateStyle(AFont, ADataFormat, ABrush, ABorders);
  Result.LoadFromStream(AReader);
  Result := AddStyle(Result);
end;

function TdxSpreadSheetCellStyles.CreateDefaultStyle: TdxSpreadSheetCellStyleHandle;
begin
  Result := TdxSpreadSheetCellStyleHandle.Create(nil, 0);
  Result.FHashTable := Self;
  Result.Font := Fonts.DefaultFont;
  Result.DataFormat := Formats.DefaultFormat;
  Result.Brush := Brushes.DefaultBrush;
  Result.Borders := Borders.DefaultBorders;
  Inc(FCount);
  Inc(FUniqueCount);
end;

function TdxSpreadSheetCellStyles.CreateBrushes: TdxSpreadSheetBrushes;
begin
  Result := TdxSpreadSheetBrushes.Create(Self);
end;

function TdxSpreadSheetCellStyles.CreateFonts: TdxSpreadSheetFonts;
begin
  Result := TdxSpreadSheetFonts.Create(Self);
end;

function TdxSpreadSheetCellStyles.CreateFormats: TdxSpreadSheetFormats;
begin
  Result := TdxSpreadSheetFormats.Create(Self);
end;

procedure TdxSpreadSheetCellStyles.DeleteItem(AItem: TdxHashTableItem);
begin
  if AItem = FDefaultStyle then
  begin
    Dec(FCount);
    FDefaultStyle := nil;
  end;
end;

{ TdxSpreadSheetFormatSettings }

constructor TdxSpreadSheetFormatSettings.Create;
begin
  inherited Create;
  UpdateSettings;
end;

function TdxSpreadSheetFormatSettings.ExpandExternalLinks: Boolean;
begin
  Result := False;
end;

function TdxSpreadSheetFormatSettings.GetFunctionName(const AName: Pointer): TdxUnicodeString;
begin
  Result := dxSpreadSheetUpperCase(LoadResString(AName));
end;

procedure TdxSpreadSheetFormatSettings.UpdateSettings;
begin
  dxGetLocaleFormatSettings(GetLocaleID, Data);
  CurrencyFormat := Data.CurrencyString;
  ArraySeparator := ';';
  R1C1Reference := False;
  DateTimeSystem := dts1900;
  UpdateOperations;
end;

function TdxSpreadSheetFormatSettings.GetLocaleID: Integer;
begin
  Result := dxGetInvariantLocaleID;
end;

procedure TdxSpreadSheetFormatSettings.UpdateOperations;
var
  I: TdxSpreadSheetFormulaOperation;
begin
  Operations := dxDefaultOperations;
  Operations[opRange] := dxAreaSeparator;
  BreakChars := dxStringMarkChar + dxRefSeparator + dxAreaSeparator + dxStringMarkChar2 +
    dxReferenceLeftParenthesis + dxReferenceRightParenthesis + dxLeftParenthesis + dxRightParenthesis +
    dxLeftArrayParenthesis + dxRightArrayParenthesis + ArraySeparator + ListSeparator;

  for I := Low(Operations) to High(Operations) do
    if Length(Operations[I]) = 1 then
      BreakChars := BreakChars + Operations[I];
end;

function TdxSpreadSheetFormatSettings.GetDecimalSeparator: TdxUnicodeChar;
begin
  Result := dxCharToUnicodeChar(Data.DecimalSeparator);
end;

function TdxSpreadSheetFormatSettings.GetListSeparator: TdxUnicodeChar;
begin
  Result := dxCharToUnicodeChar(Data.ListSeparator);
end;

procedure TdxSpreadSheetFormatSettings.SetDecimalSeparator(const AValue: TdxUnicodeChar);
begin
  Data.DecimalSeparator := dxUnicodeCharToChar(AValue);
end;

procedure TdxSpreadSheetFormatSettings.SetListSeparator(const AValue: TdxUnicodeChar);
begin
  Data.ListSeparator := dxUnicodeCharToChar(AValue);
end;

{ TdxSpreadSheetInvalidObject }

class procedure TdxSpreadSheetInvalidObject.AssignTo(var AObject);
begin
  TObject(AObject) := Instance;
end;

class function TdxSpreadSheetInvalidObject.Instance: TObject;
begin
  Result := dxSpreadSheetInvalidObject;
end;

class function TdxSpreadSheetInvalidObject.IsInvalid(const AObject: TObject): Boolean;
begin
  Result := AObject = Instance;
end;

class function TdxSpreadSheetInvalidObject.IsLive(const AObject: TObject): Boolean;
begin
  Result := (AObject <> nil) and (AObject <> Instance);
end;

initialization
  dxSpreadSheetInvalidObject := TdxSpreadSheetInvalidObject.Create;

finalization
  FreeAndNil(dxSpreadSheetInvalidObject);
end.


