{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFunctionsLookup;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Windows, SysUtils, Classes, StrUtils, DateUtils, Variants, Math, cxVariants, dxCore, dxSpreadSheetTypes,
  dxSpreadSheetCore, dxSpreadSheetUtils, dxSpreadSheetStrs, dxSpreadSheetFormulas;

procedure fnAddress(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnColumn(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnColumns(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnFormulaText(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnHLookup(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnLookup(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnMatch(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnRow(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnRows(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnVLookup(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);

{
procedure fnChoose(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
}
implementation

uses
  dxSpreadSheetFunctionsText;

type
  TdxSpreadSheetFormulaExtractVectorFromRange = function(var AVector: Variant; const ARange: TdxSpreadSheetFormulaToken;
    AVectorIndex: Integer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): Boolean of object;

  TdxSpreadSheetXLookupComparer = function(const AValue1, AValue2: Variant): Integer;

  TTokenAccess = class(TdxSpreadSheetFormulaToken);
  TReferenceAccess = class(TdxSpreadSheetFormulaReference);

procedure fnAddress(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
const
  anAbsolute        = 1;
  anAbsRowRelColumn = 2;
  anRelRowAbsColumn = 3;
  anRelative        = 4;

  function CheckParams(var ARow, AColumn, AAbsNum: Variant): Boolean;
  begin
    ARow := Trunc(ARow);
    AColumn := Trunc(AColumn);
    AAbsNum := Trunc(AAbsNum);
    Result := InRange(ARow, 1, 65536) and InRange(AColumn, 1, 256) and
      (Trunc(AAbsNum) in [anAbsolute, anAbsRowRelColumn, anRelRowAbsColumn, anRelative]);
    if not Result then
      Sender.SetError(ecValue);
  end;

  function GetSheetAddressName(const ASheet: TdxUnicodeString): TdxUnicodeString;
  begin
    Result := ASheet;
    if Result = '' then
      Exit;
    if Pos(' ', Result) > 0 then
      Result := '''' + Result + '''';
    Result := Result + '!';
  end;

var
  ARow, AColumn, AAbsNum, AA1RefStyle, ASheet: Variant;
  ARowReference, AColumnReference: TdxSpreadSheetReference;
begin
  if not (Sender.ExtractNumericParameter(ARow, AParams) and Sender.ExtractNumericParameter(AColumn, AParams, 1) and
          Sender.ExtractNumericParameterDef(AAbsNum, 1, 1, AParams, 2) and
          Sender.ExtractNumericParameterDef(AA1RefStyle, 1, 1, AParams, 3) and
          CheckParams(ARow, AColumn, AAbsNum) and Sender.ExtractParameterDef(ASheet, '', '', AParams, 4)) then Exit;
  ARowReference.Offset := ARow - 1;
  AColumnReference.Offset := AColumn - 1;
  ARowReference.IsAbsolute := (AAbsNum = anAbsolute) or (AAbsNum = anAbsRowRelColumn);
  AColumnReference.IsAbsolute := (AAbsNum = anAbsolute) or (AAbsNum = anRelRowAbsColumn);
  if AA1RefStyle = 0 then
  begin
    if not AColumnReference.IsAbsolute then
      AColumnReference.Offset := AColumnReference.Offset + 1;
    if not ARowReference.IsAbsolute then
      ARowReference.Offset := ARowReference.Offset + 1;
  end;
  if AA1RefStyle <> 0 then
    Sender.AddValue(GetSheetAddressName(VarToStr(ASheet)) + dxReferenceToString(0, 0, ARowReference, AColumnReference))
  else
    Sender.AddValue(GetSheetAddressName(VarToStr(ASheet)) + dxR1C1ReferenceToString(0, 0, ARowReference, AColumnReference));
end;

procedure fnColumn(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParam: TdxSpreadSheetFormulaToken;
begin
  if not Sender.ParameterExists(AParams, 0) then
    Sender.AddValue(Sender.Owner.Cell.ColumnIndex + 1)
  else
  begin
    AParam := AParams.FirstChild;
    if not (AParam is TdxSpreadSheetFormulaReference) then
      Sender.SetError(ecValue)
    else
      Sender.AddValue(TdxSpreadSheetFormulaReference(AParam).ActualColumn + 1);
  end;
end;

procedure fnColumns(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParam: TdxSpreadSheetFormulaToken;
  ADimension: TdxSpreadSheetAreaDimension;
begin
  if not Sender.ParameterExists(AParams, 0) then
    Sender.SetError(ecValue)
  else
  begin
    AParam := AParams.FirstChild;
    if (AParam is TdxSpreadSheetFormulaReference) or (AParam is TdxSpreadSheetFormulaAreaReference) or
      (AParam is TdxSpreadSheetFormulaArrayToken) then
      begin
        ADimension := Sender.CalculateParamsDimension(AParam);
        if Sender.Validate then
          Sender.AddValue(ADimension.ColumnCount);
      end
    else
      if (AParam is TdxSpreadSheetFormulaIntegerValueToken) or (AParam is TdxSpreadSheetFormulaFloatValueToken) then
        Sender.AddValue(1)
      else
        Sender.SetError(ecValue);
  end;
end;

procedure fnFormulaText(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParam: TdxSpreadSheetFormulaToken;
  ACell: TdxSpreadSheetCell;
  AReference: TReferenceAccess;
begin
  if not Sender.ParameterExists(AParams, 0) then
    Sender.SetError(ecNA)
  else
  begin
    AParam := AParams.FirstChild;
    if not(AParam is TdxSpreadSheetFormulaReference) then
      Sender.SetError(ecNA)
    else
    begin
      AReference := TReferenceAccess(TTokenAccess(AParam));
      ACell := AReference.GetSheet.Cells[TdxSpreadSheetFormulaReference(AParam).ActualRow, TdxSpreadSheetFormulaReference(AParam).ActualColumn];
      if ACell.IsFormula then
        Sender.AddValue(ACell.AsFormula.AsText)
      else
        Sender.SetError(ecNA);
    end;
  end;
end;

function GetApproximateXLookupIndex(AValue, AVector: Variant; const AAscending: Boolean = True): Integer;

  function BooleanLookupComparer(const ABoolValue, AValue2: Variant): Integer;
  begin
    if not dxIsLogical(AValue2) then
      Result := -1
    else
      if (ABoolValue = False) and (AValue2 = True) then
        Result := -1
      else
        if (ABoolValue = True) and (AValue2 = False) then
          Result := 1
        else
          Result := 0;
  end;

  function NumericLookupComparer(const ANumericValue, AValue2: Variant): Integer;
  begin
    if not dxIsNumberOrDateTime(AValue2) then
      Result := -1
    else
      Result := Sign(Double(ANumericValue - AValue2));
  end;

  function StringLookupComparer(const AStrValue, AValue2: Variant): Integer;
  const
    AResult: array[Boolean] of Integer = (-1,1);
  begin
    if not VarIsStr(AValue2) then
      Result := -1
    else
      if VarEquals(AStrValue, AValue2) then
        Result := 0
      else
        Result := AResult[AStrValue > AValue2];
  end;

  function GetBinaryLookupIndex(AComparer: TdxSpreadSheetXLookupComparer; AStartIndex, AEndIndex: Integer): Integer;

    function AreCompatibleTypesOf(AValue1, AValue2: Variant): Boolean;
    begin
      Result := (dxIsLogical(AValue1) and dxIsLogical(AValue2)) or
                (dxIsNumberOrDateTime(AValue1) and dxIsNumberOrDateTime(AValue2)) or
                (dxIsText(AValue1) and dxIsText(AValue2));
    end;

    function ExecuteCompare(AValue1, AValue2: Variant): Integer;
    const
      ASign: array[Boolean] of Integer = (-1, 1);
    begin
      Result := AComparer(AValue1, AValue2) * ASign[AAscending];
    end;

  var
    AStartValue, AEndValue, AMedianValue: Variant;
    AMedianIndex: Integer;
  begin
    AStartValue := AVector[AStartIndex];
    if AComparer(AValue, AStartValue) = 0 then
      Result := AStartIndex
    else
    begin
      Result := -1;
      AEndValue := AVector[AEndIndex];
      AMedianIndex := Round((AStartIndex + AEndIndex) / 2 + 0.1);
      AMedianValue := AVector[AMedianIndex];
      if AMedianIndex = AEndIndex then
      begin
        if (ExecuteCompare(AValue, AStartValue) > 0) and AreCompatibleTypesOf(AValue, AStartValue) then
          Result := AStartIndex;
        if (ExecuteCompare(AValue, AMedianValue) >= 0) and AreCompatibleTypesOf(AValue, AMedianValue) then
          Result := AMedianIndex;
      end
      else
        if ExecuteCompare(AValue, AMedianValue) < 0 then
        begin
          Result := GetBinaryLookupIndex(AComparer, AStartIndex, AMedianIndex);
          if Result = -1 then
            Result := GetBinaryLookupIndex(AComparer, AMedianIndex, AEndIndex);
        end
        else
        begin
          Result := GetBinaryLookupIndex(AComparer, AMedianIndex, AEndIndex);
          if Result = -1 then
            Result := GetBinaryLookupIndex(AComparer, AStartIndex, AMedianIndex);
        end;
    end;
  end;

var
  AHighIndex: Integer;
  AComparer: TdxSpreadSheetXLookupComparer;
begin
  Result := -1;
  AHighIndex := VarArrayHighBound(AVector, 1);
  if dxIsLogical(AValue) then
    AComparer := @BooleanLookupComparer
  else
    if dxIsNumberOrDateTime(AValue) then
      AComparer := @NumericLookupComparer
    else
      if VarIsStr(AValue) then
        AComparer := @StringLookupComparer
      else
        Exit;
  Result := GetBinaryLookupIndex(AComparer, 0, AHighIndex);

  while (Result > -1) and (Result < AHighIndex) and (AComparer(AVector[Result], AVector[Result + 1]) = 0) do
    Inc(Result);
end;

function GetExactXLookupIndex(AValue, AVector: Variant): Integer;

  function HasMaskTokens(S: TdxUnicodeString): Boolean;

    function HasMaskToken(AToken: TdxUnicodeString; var AStartPos: Integer): Boolean;
    begin
      AStartPos := PosEx(AToken, S, AStartPos);
      Result := (AStartPos = 1) or ((AStartPos > 1) and (S[AStartPos - 1] <> '~'));
      if not Result and (AStartPos > 1) then
        Inc(AStartPos);
    end;

  var
    APosQ, APosA: Integer;
  begin
    APosQ := 1;
    APosA := 1;
    Result := False;
    while (APosQ > 0) or (APosA > 0) do
    begin
      Result := HasMaskToken('?', APosQ) or HasMaskToken('*', APosA);
      if Result then
        Break;
    end;
  end;

var
  I, AHighIndex: Integer;
  ANeedMaskSearch, ASuccess: Boolean;
begin
  Result := -1;
  ANeedMaskSearch := VarIsStr(AValue) and HasMaskTokens(VarToStr(AValue));
  AHighIndex := VarArrayHighBound(AVector, 1);
  for I := 0 to AHighIndex do
  begin
    if ANeedMaskSearch then
      ASuccess := dxMaskSearch(AValue, AVector[I], 1) = 1
    else
      ASuccess := VarEquals(AValue, AVector[I]);
    if ASuccess then
    begin
      Result := I;
      Break;
    end;
  end;
end;

procedure DoXLookup(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken;
  AExtractVectorMethod: TdxSpreadSheetFormulaExtractVectorFromRange);

  function CheckDimension(ADimension: TdxSpreadSheetAreaDimension; ACheckedIndex: Integer): Boolean;
  var
    ASenderExtractColumnMethod: TdxSpreadSheetFormulaExtractVectorFromRange;
  begin
    ASenderExtractColumnMethod := Sender.ExtractColumnFromRange;
    if dxSameMethods(ASenderExtractColumnMethod, AExtractVectorMethod) then
      Result := ACheckedIndex < ADimension.ColumnCount
    else
      Result := ACheckedIndex < ADimension.RowCount;
  end;

var
  ALookupValue, AResultVectorIndex, AApproximateMatch: Variant;
  ARangeParam: TdxSpreadSheetFormulaToken;
  ARangeDimension: TdxSpreadSheetAreaDimension;
  AFirstVector, AResultVector, AResult: Variant;
  AErrorCode: TdxSpreadSheetFormulaErrorCode;
  AFoundIndex: Integer;
begin
  if not (Sender.ExtractParameterDef(ALookupValue, 0, AParams) and
          Sender.ExtractNumericParameterDef(AResultVectorIndex, 0, AParams, 2) and
          Sender.ExtractNumericParameterDef(AApproximateMatch, 1, 0, AParams, 3)) then
    Exit;
  AResultVectorIndex := Trunc(AResultVectorIndex) - 1;

  ARangeParam := AParams.Next.FirstChild;
  ARangeDimension := Sender.CalculateParamsDimension(ARangeParam);
  if AExtractVectorMethod(AFirstVector, ARangeParam, 0, AErrorCode) then
  begin
    if AApproximateMatch <> 0 then
      AFoundIndex := GetApproximateXLookupIndex(ALookupValue, AFirstVector)
    else
      AFoundIndex := GetExactXLookupIndex(ALookupValue, AFirstVector);
    if AFoundIndex = -1 then
      Sender.SetError(ecNA)
    else
      if AResultVectorIndex < 0 then
        Sender.SetError(ecValue)
      else
        if not AExtractVectorMethod(AResultVector, ARangeParam, AResultVectorIndex, AErrorCode) then
          Sender.SetError(AErrorCode)
        else
          if not CheckDimension(ARangeDimension, AResultVectorIndex) then
            Sender.SetError(ecRefErr)
          else
          begin
            AResult := AResultVector[AFoundIndex];
            if VarIsNull(AResult) or VarIsEmpty(AResult) then
              AResult := 0;
            Sender.AddValue(AResult);
          end;
  end
  else
    Sender.SetError(AErrorCode);
end;

procedure fnHLookup(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  DoXLookup(Sender, AParams, Sender.ExtractRowFromRange);
end;

procedure fnVLookup(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  DoXLookup(Sender, AParams, Sender.ExtractColumnFromRange);
end;

procedure fnLookup(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ALookupValue: Variant;
  ALookupRange, AResultRange: TdxSpreadSheetFormulaToken;
  ALookupRangeDimension, AResultRangeDimension: TdxSpreadSheetAreaDimension;
  AExtractLookupVectorMethod: TdxSpreadSheetFormulaExtractVectorFromRange;
  ALookupVector, AResultVector, AResult: Variant;
  AErrorCode: TdxSpreadSheetFormulaErrorCode;
  ALookupVectorLength, AFoundIndex: Integer;
begin
  if not Sender.ExtractParameterDef(ALookupValue, 0, AParams) then
    Exit;

  ALookupRange := AParams.Next.FirstChild;
  ALookupRangeDimension := Sender.CalculateParamsDimension(ALookupRange);
  if ALookupRangeDimension.ColumnCount > ALookupRangeDimension.RowCount then
    AExtractLookupVectorMethod := Sender.ExtractRowFromRange
  else
    AExtractLookupVectorMethod := Sender.ExtractColumnFromRange;
  if not AExtractLookupVectorMethod(ALookupVector, ALookupRange, 0, AErrorCode) then
    Exit;

  if Sender.ParameterExists(AParams, 2) then
  begin
    AResultRange := AParams.Next.Next.FirstChild;
    AResultRangeDimension := Sender.CalculateParamsDimension(AResultRange);
    if (AResultRangeDimension.ColumnCount > 1) and (AResultRangeDimension.RowCount > 1) then
    begin
      Sender.SetError(ecNA);
      Exit;
    end;
    ALookupVectorLength := VarArrayHighBound(ALookupVector, 1) + 1;
    if AResultRangeDimension.RowCount > 1 then
      Sender.ExtractVerticalVectorAsVariantArray(AResultVector, ALookupVectorLength, AResultRange, AErrorCode)
    else
      Sender.ExtractHorizontalVectorAsVariantArray(AResultVector, ALookupVectorLength, AResultRange, AErrorCode)
  end
  else
    AExtractLookupVectorMethod(AResultVector, ALookupRange, Min(ALookupRangeDimension.ColumnCount - 1,
      ALookupRangeDimension.RowCount - 1), AErrorCode);

  if Sender.Validate then
  begin
    AFoundIndex := GetApproximateXLookupIndex(ALookupValue, ALookupVector);
    if (AFoundIndex = -1) or (AFoundIndex > VarArrayHighBound(AResultVector, 1)) then
      Sender.SetError(ecNA)
    else
    begin
      AResult := AResultVector[AFoundIndex];
      if VarIsNull(AResult) or VarIsEmpty(AResult) then
        AResult := 0;
      Sender.AddValue(AResult);
    end;
  end;
end;

procedure fnMatch(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ALookupValue, AMatchType: Variant;
  ALookupArray: Variant;
  AFoundIndex: Integer;
begin
  if not (Sender.ExtractParameterDef(ALookupValue, 0, AParams) and
          Sender.ExtractNumericParameterDef(AMatchType, 1, 0, AParams, 2) and
          Sender.ExtractVariantArray(ALookupArray, AParams.Next.FirstChild)) then
    Exit;

  if AMatchType = 0 then
    AFoundIndex := GetExactXLookupIndex(ALookupValue, ALookupArray)
  else
    AFoundIndex := GetApproximateXLookupIndex(ALookupValue, ALookupArray, AMatchType > 0);
  if AFoundIndex = -1 then
    Sender.SetError(ecNA)
  else
    Sender.AddValue(AFoundIndex + 1);
end;

procedure fnRow(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParam: TdxSpreadSheetFormulaToken;
begin
  if not Sender.ParameterExists(AParams, 0) then
    Sender.AddValue(Sender.Owner.Cell.RowIndex + 1)
  else
  begin
    AParam := AParams.FirstChild;
    if not (AParam is TdxSpreadSheetFormulaReference) then
      Sender.SetError(ecValue)
    else
      Sender.AddValue(TdxSpreadSheetFormulaReference(AParam).ActualRow + 1);
  end;
end;

procedure fnRows(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParam: TdxSpreadSheetFormulaToken;
  ADimension: TdxSpreadSheetAreaDimension;
begin
  if not Sender.ParameterExists(AParams, 0) then
    Sender.SetError(ecValue)
  else
  begin
    AParam := AParams.FirstChild;
    if (AParam is TdxSpreadSheetFormulaReference) or (AParam is TdxSpreadSheetFormulaAreaReference) or
      (AParam is TdxSpreadSheetFormulaArrayToken) then
      begin
        ADimension := Sender.CalculateParamsDimension(AParam);
        if Sender.Validate then
          Sender.AddValue(ADimension.RowCount);
      end
    else
      if (AParam is TdxSpreadSheetFormulaIntegerValueToken) or (AParam is TdxSpreadSheetFormulaFloatValueToken) then
        Sender.AddValue(1)
      else
        Sender.SetError(ecValue);
  end;
end;

(*
procedure fnChoose(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AList: TList;
  AIndex: Variant;
  AIntIndex: Integer;
begin
  if not Sender.ExtractNumericParameter(AIndex, AParams) then
    Exit;
  AIntIndex := Trunc(AIndex);
  if not (AIntIndex in [1..254]) then
  begin
    Sender.SetError(ecValue);
    Exit;
  end;
  AList := TList.Create;
  try
    Sender.ForEach(AParams.Next, @cbPopulateList, AList);
    if AIntIndex <= AList.Count then
      Sender.AddValue(Variant(AList[AIntIndex - 1]^))
    else
      Sender.SetError(ecValue);
  finally
    AList.Free;
  end;
end;
*)
end.
