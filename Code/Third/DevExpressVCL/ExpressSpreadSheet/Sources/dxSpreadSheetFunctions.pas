{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFunctions;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Windows, dxCore, dxCoreClasses, cxClasses, dxSpreadSheetCore, dxSpreadSheetTypes, dxSpreadSheetUtils, Types,
  dxSpreadSheetFormulas, dxSpreadSheetStrs, dxSpreadSheetClasses;

type
  { TdxSpreadSheetFunctionsRepository }

  TdxSpreadSheetFunctionsRepository = class(TcxObjectList)
  private
    FSorted: Boolean;
    function GetItem(AIndex: Integer): TdxSpreadSheetFunctionInfo;
    procedure SetSorted(AValue: Boolean);
  protected
    property Sorted: Boolean read FSorted write SetSorted;
  public
    procedure Add(const AName: TcxResourceStringID; AProc: TdxSpreadSheetFunction; AParamCount: Integer; AToken: Word; AType: TdxSpreadSheetFunctionType = ftCommon);
    function AddUnknown(const AName: TdxUnicodeString; AType: TdxSpreadSheetFunctionType = ftCommon): TdxSpreadSheetFunctionInfo;
    function GetInfoByID(const ID: Integer): TdxSpreadSheetFunctionInfo; inline;
    function GetInfoByName(const AName: TdxUnicodeString): TdxSpreadSheetFunctionInfo; overload; inline;
    function GetInfoByName(const AName: PdxUnicodeChar; ALength: Integer): TdxSpreadSheetFunctionInfo; overload; inline;
    procedure TranslationChanged(AFormatSettings: TdxSpreadSheetFormatSettings);

    property Items[Index: Integer]: TdxSpreadSheetFunctionInfo read GetItem;
  end;

  function dxSpreadSheetFunctionsRepository: TdxSpreadSheetFunctionsRepository;

implementation

uses
  Classes, SysUtils,
  dxSpreadSheetFunctionsCompatibility, dxSpreadSheetFunctionsDateTime, dxSpreadSheetFunctionsInformation,
  dxSpreadSheetFunctionsFinancial, dxSpreadSheetFunctionsLogical, dxSpreadSheetFunctionsLookup,
  dxSpreadSheetFunctionsMath, dxSpreadSheetFunctionsStatistical, dxSpreadSheetFunctionsText;

type

  { TdxSpreadSheedFunctionRec }

  TdxSpreadSheedFunctionRec = record
    NamePtr: Pointer;
    ParamCount: Integer;
    Proc: TdxSpreadSheetFunction;
    Token: Word;
    TypeID: TdxSpreadSheetFunctionType;
  end;

var
  Repository: TdxSpreadSheetFunctionsRepository;

  PredefinedFunctions: array[0..239] of TdxSpreadSheedFunctionRec = (
    // Compatibility
   (NamePtr: @sfnBetaDist; ParamCount: 5; Proc: fnBetaDist; Token: 270; TypeID: ftCompatibility),
   (NamePtr: @sfnBetaInv; ParamCount: 5; Proc: fnBetaInv; Token: 272; TypeID: ftCompatibility),
   (NamePtr: @sfnBinomDist; ParamCount: 4; Proc: fnBinomDist; Token: 273; TypeID: ftCompatibility),
   (NamePtr: @sfnChiDist; ParamCount: 2; Proc: fnChiDist; Token: 274; TypeID: ftCompatibility),
   (NamePtr: @sfnChiInv; ParamCount: 2; Proc: fnChiInv; Token: 275; TypeID: ftCompatibility),
   (NamePtr: @sfnCovar; ParamCount: 2; Proc: fnCovariance_P; Token: 308; TypeID: ftCompatibility),
   (NamePtr: @sfnExponDist; ParamCount: 3; Proc: fnExponDist; Token: 280; TypeID: ftCompatibility),
   (NamePtr: @sfnGammaDist; ParamCount: 4; Proc: fnGamma_Dist; Token: 286; TypeID: ftCompatibility),
   (NamePtr: @sfnGammaInv; ParamCount: 3; Proc: fnGamma_Inv; Token: 287; TypeID: ftCompatibility),
   (NamePtr: @sfnHypgeomDist; ParamCount: 4; Proc: fnHypgeomDist; Token: 289; TypeID: ftCompatibility),
   (NamePtr: @sfnNormDist; ParamCount: 4; Proc: fnNormDist; Token: 293; TypeID: ftCompatibility),
   (NamePtr: @sfnNormInv; ParamCount: 3; Proc: fnNormInv; Token: 295; TypeID: ftCompatibility),
   (NamePtr: @sfnNormSDist; ParamCount: 1; Proc: fnNormSDist; Token: 294; TypeID: ftCompatibility),
   (NamePtr: @sfnNormSInv; ParamCount: 1; Proc: fnNormSInv; Token: 296; TypeID: ftCompatibility),
   (NamePtr: @sfnPercentile; ParamCount: 2; Proc: fnPercentile_Inc; Token: 328; TypeID: ftCompatibility),
   (NamePtr: @sfnQuartile; ParamCount: 2; Proc: fnQuartile_Inc; Token: 327; TypeID: ftCompatibility),
   (NamePtr: @sfnPoisson; ParamCount: 3; Proc: fnPoisson; Token: 300; TypeID: ftCompatibility),
   (NamePtr: @sfnRank; ParamCount: 3; Proc: fnRank; Token: 216; TypeID: ftCompatibility),
   (NamePtr: @sfnStDev; ParamCount: 1; Proc: fnStDev; Token: 12; TypeID: ftCompatibility),
   (NamePtr: @sfnStDevP; ParamCount: 1; Proc: fnStDevP; Token: 193; TypeID: ftCompatibility),
   (NamePtr: @sfnTDist; ParamCount: 3; Proc: fnTDist; Token: 301; TypeID: ftCompatibility),
   (NamePtr: @sfnTInv; ParamCount: 2; Proc: fnTInv; Token: 332; TypeID: ftCompatibility),
   (NamePtr: @sfnVar; ParamCount: 1; Proc: fnVar; Token: 46; TypeID: ftCompatibility),
   (NamePtr: @sfnVarP; ParamCount: 1; Proc: fnVarP; Token: 194; TypeID: ftCompatibility),
   (NamePtr: @sfnWeibull; ParamCount: 4; Proc: fnWeibull; Token: 302; TypeID: ftCompatibility),
    // DateTime
   (NamePtr: @sfnDate; ParamCount: 3; Proc: fnDate; Token: 65; TypeID: ftDateTime),
   (NamePtr: @sfnDateValue; ParamCount: 1; Proc: fnDateValue; Token: 140; TypeID: ftDateTime),
   (NamePtr: @sfnDay; ParamCount: 1; Proc: fnDay; Token: 67; TypeID: ftDateTime),
   (NamePtr: @sfnDays; ParamCount: 2; Proc: fnDays; Token: 255; TypeID: ftDateTime),
   (NamePtr: @sfnDays360; ParamCount: 3; Proc: fnDays360; Token: 220; TypeID: ftDateTime),
   (NamePtr: @sfnEDate; ParamCount: 2; Proc: fnEDate; Token: 255; TypeID: ftDateTime),
   (NamePtr: @sfnEOMonth; ParamCount: 2; Proc: fnEOMonth; Token: 255; TypeID: ftDateTime),
   (NamePtr: @sfnHour; ParamCount: 1; Proc: fnHour; Token: 71; TypeID: ftDateTime),
   (NamePtr: @sfnIsoWeekNum; ParamCount: 1; Proc: fnIsoWeekNum; Token: 255; TypeID: ftDateTime),
   (NamePtr: @sfnMinute; ParamCount: 1; Proc: fnMinute; Token: 72; TypeID: ftDateTime),
   (NamePtr: @sfnMonth; ParamCount: 1; Proc: fnMonth; Token: 68; TypeID: ftDateTime),
   (NamePtr: @sfnNow; ParamCount: 0; Proc: fnNow; Token: 74; TypeID: ftDateTime),
   (NamePtr: @sfnSecond; ParamCount: 1; Proc: fnSecond; Token: 73; TypeID: ftDateTime),
   (NamePtr: @sfnTime; ParamCount: 3; Proc: fnTime; Token: 66; TypeID: ftDateTime),
   (NamePtr: @sfnTimeValue; ParamCount: 1; Proc: fnTimeValue; Token: 141; TypeID: ftDateTime),
   (NamePtr: @sfnToday; ParamCount: 0; Proc: fnToday; Token: 221; TypeID: ftDateTime),
   (NamePtr: @sfnWeekDay; ParamCount: 2; Proc: fnWeekDay; Token: 70; TypeID: ftDateTime),
   (NamePtr: @sfnWeekNum; ParamCount: 2; Proc: fnWeekNum; Token: 255; TypeID: ftDateTime),
   (NamePtr: @sfnYear; ParamCount: 1; Proc: fnYear; Token: 69; TypeID: ftDateTime),
   (NamePtr: @sfnYearFrac; ParamCount: 3; Proc: fnYearFrac; Token: 255; TypeID: ftDateTime),
  // financial function names
   (NamePtr: @sfnFV; ParamCount: 5; Proc: fnFV; Token: 57; TypeID: ftFinancial),
   (NamePtr: @sfnIPMT; ParamCount: 6; Proc: fnIPMT; Token: 167; TypeID: ftFinancial),
   (NamePtr: @sfnNPV; ParamCount: 2; Proc: fnNPV; Token: 11; TypeID: ftFinancial),
   (NamePtr: @sfnNPer; ParamCount: 5; Proc: fnNPer; Token: 58; TypeID: ftFinancial),
   (NamePtr: @sfnPMT; ParamCount: 5; Proc: fnPMT; Token: 59; TypeID: ftFinancial),
   (NamePtr: @sfnPPMT; ParamCount: 6; Proc: fnPPMT; Token: 168; TypeID: ftFinancial),
   (NamePtr: @sfnPV; ParamCount: 5; Proc: fnPV; Token: 56; TypeID: ftFinancial),
    // Information
   (NamePtr: @sfnIsBlank; ParamCount: 1; Proc: fnIsBlank; Token: 129; TypeID: ftInformation),
   (NamePtr: @sfnIsErr; ParamCount: 1; Proc: fnIsErr; Token: 126; TypeID: ftInformation),
   (NamePtr: @sfnIsError; ParamCount: 1; Proc: fnIsError; Token: 3; TypeID: ftInformation),
   (NamePtr: @sfnIsEven; ParamCount: 1; Proc: fnIsEven; Token: 255; TypeID: ftInformation),
   (NamePtr: @sfnIsLogical; ParamCount: 1; Proc: fnIsLogical; Token: 198; TypeID: ftInformation),
   (NamePtr: @sfnIsNA; ParamCount: 1; Proc: fnIsNA; Token: 2; TypeID: ftInformation),
   (NamePtr: @sfnIsNonText; ParamCount: 1; Proc: fnIsNonText; Token: 190; TypeID: ftInformation),
   (NamePtr: @sfnIsNumber; ParamCount: 1; Proc: fnIsNumber; Token: 128; TypeID: ftInformation),
   (NamePtr: @sfnIsOdd; ParamCount: 1; Proc: fnIsOdd; Token: 255; TypeID: ftInformation),
   (NamePtr: @sfnIsText; ParamCount: 1; Proc: fnIsText; Token: 127; TypeID: ftInformation),
   (NamePtr: @sfnN; ParamCount: 1; Proc: fnN; Token: 131; TypeID: ftInformation),
   (NamePtr: @sfnNA; ParamCount: 1; Proc: fnNA; Token: 10; TypeID: ftInformation),
    // Lookup and reference
   (NamePtr: @sfnAddress; ParamCount: 5; Proc: fnAddress; Token: 219; TypeID: ftLookupAndReference),
   (NamePtr: @sfnColumn; ParamCount: 1; Proc: fnColumn; Token: 9; TypeID: ftLookupAndReference),
   (NamePtr: @sfnColumns; ParamCount: 1; Proc: fnColumns; Token: 77; TypeID: ftLookupAndReference),
   (NamePtr: @sfnFormulaText; ParamCount: 1; Proc: fnFormulaText; Token: 255; TypeID: ftLookupAndReference),
   (NamePtr: @sfnHLookup; ParamCount: 4; Proc: fnHLookup; Token: 101; TypeID: ftLookupAndReference),
   (NamePtr: @sfnLookup; ParamCount: 3; Proc: fnLookup; Token: 28; TypeID: ftLookupAndReference),
   (NamePtr: @sfnMatch; ParamCount: 3; Proc: fnMatch; Token: 64; TypeID: ftLookupAndReference),
   (NamePtr: @sfnRow; ParamCount: 1; Proc: fnRow; Token: 8; TypeID: ftLookupAndReference),
   (NamePtr: @sfnRows; ParamCount: 1; Proc: fnRows; Token: 76; TypeID: ftLookupAndReference),
   (NamePtr: @sfnVLookup; ParamCount: 4; Proc: fnVLookup; Token: 102; TypeID: ftLookupAndReference),
    // Logical
   (NamePtr: @sfnAnd; ParamCount: 1; Proc: fnAND; Token: 36; TypeID: ftLogical),
   (NamePtr: @sfnFalse; ParamCount: 0; Proc: fnFalse; Token: 35; TypeID: ftLogical),
   (NamePtr: @sfnIF; ParamCount: 3; Proc: fnIF; Token: 1; TypeID: ftLogical),
   (NamePtr: @sfnIfError; ParamCount: 2; Proc: fnIfError; Token: 255; TypeID: ftLogical),
   (NamePtr: @sfnIfNA; ParamCount: 2; Proc: fnIfNA; Token: 255; TypeID: ftLogical),
   (NamePtr: @sfnNot; ParamCount: 1; Proc: fnNOT; Token: 38; TypeID: ftLogical),
   (NamePtr: @sfnOr; ParamCount: 1; Proc: fnOR; Token: 37; TypeID: ftLogical),
   (NamePtr: @sfnTrue; ParamCount: 0; Proc: fnTrue; Token: 34; TypeID: ftLogical),
   (NamePtr: @sfnXor; ParamCount: 1; Proc: fnXOR; Token: 255; TypeID: ftLogical),
    // Math and trigonometry
   (NamePtr: @sfnAbs; ParamCount: 1; Proc: fnABS; Token: 24; TypeID: ftMath),
   (NamePtr: @sfnAcos; ParamCount: 1; Proc: fnACOS; Token: 99; TypeID: ftMath),
   (NamePtr: @sfnAcosh; ParamCount: 1; Proc: fnACOSH; Token: 233; TypeID: ftMath),
   (NamePtr: @sfnAcot; ParamCount: 1; Proc: fnACOT; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnAcoth; ParamCount: 1; Proc: fnACOTH; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnAsin; ParamCount: 1; Proc: fnASIN; Token: 98; TypeID: ftMath),
   (NamePtr: @sfnAsinh; ParamCount: 1; Proc: fnASINH; Token: 232; TypeID: ftMath),
   (NamePtr: @sfnAtan; ParamCount: 1; Proc: fnATAN; Token: 18; TypeID: ftMath),
   (NamePtr: @sfnAtan2; ParamCount: 2; Proc: fnATAN2; Token: 97; TypeID: ftMath),
   (NamePtr: @sfnAtanh; ParamCount: 1; Proc: fnATANH; Token: 234; TypeID: ftMath),
   (NamePtr: @sfnBase; ParamCount: 3; Proc: fnBase; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnCeiling; ParamCount: 2; Proc: fnCeiling; Token: 288; TypeID: ftMath),
   (NamePtr: @sfnCeiling_Math; ParamCount: 3; Proc: fnCeiling_Math; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnCeiling_Precise; ParamCount: 2; Proc: fnCeiling_Precise; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnCos; ParamCount: 1; Proc: fnCOS; Token: 16; TypeID: ftMath),
   (NamePtr: @sfnCosh; ParamCount: 1; Proc: fnCOSH; Token: 230; TypeID: ftMath),
   (NamePtr: @sfnCot; ParamCount: 1; Proc: fnCOT; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnCoth; ParamCount: 1; Proc: fnCOTH; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnCombin; ParamCount: 2; Proc: fnCombin; Token: 276; TypeID: ftMath),
   (NamePtr: @sfnCombinA; ParamCount: 2; Proc: fnCombinA; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnCsc; ParamCount: 1; Proc: fnCSC; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnCsch; ParamCount: 1; Proc: fnCSCH; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnDecimal; ParamCount: 2; Proc: fnDecimal; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnDegrees; ParamCount: 1; Proc: fnDegrees; Token: 343; TypeID: ftMath),
   (NamePtr: @sfnExp; ParamCount: 1; Proc: fnExp; Token: 21; TypeID: ftMath),
   (NamePtr: @sfnEven; ParamCount: 1; Proc: fnEven; Token: 279; TypeID: ftMath),
   (NamePtr: @sfnFact; ParamCount: 1; Proc: fnFact; Token: 184; TypeID: ftMath),
   (NamePtr: @sfnFactDouble; ParamCount: 1; Proc: fnFactDouble; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnFloor; ParamCount: 2; Proc: fnFloor; Token: 285; TypeID: ftMath),
   (NamePtr: @sfnFloor_Math; ParamCount: 3; Proc: fnFloor_Math; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnFloor_Precise; ParamCount: 2; Proc: fnFloor_Precise; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnInt; ParamCount: 1; Proc: fnINT; Token: 25; TypeID: ftMath),
   (NamePtr: @sfnIso_Ceiling; ParamCount: 2; Proc: fnIso_Ceiling; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnLn; ParamCount: 1; Proc: fnLN; Token: 22; TypeID: ftMath),
   (NamePtr: @sfnLog; ParamCount: 1; Proc: fnLOG; Token: 109; TypeID: ftMath),
   (NamePtr: @sfnLog10; ParamCount: 1; Proc: fnLOG10; Token: 23; TypeID: ftMath),
   (NamePtr: @sfnMod; ParamCount: 2; Proc: fnMOD; Token: 39; TypeID: ftMath),
   (NamePtr: @sfnMRound; ParamCount: 2; Proc: fnMRound; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnOdd; ParamCount: 1; Proc: fnOdd; Token: 298; TypeID: ftMath),
   (NamePtr: @sfnPi; ParamCount: 0; Proc: fnPI; Token: 19; TypeID: ftMath),
   (NamePtr: @sfnPower; ParamCount: 2; Proc: fnPower; Token: 337; TypeID: ftMath),
   (NamePtr: @sfnProduct; ParamCount: 1; Proc: fnProduct; Token: 183; TypeID: ftMath),
   (NamePtr: @sfnQuotient; ParamCount: 2; Proc: fnQuotient; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnRadians; ParamCount: 1; Proc: fnRadians; Token: 342; TypeID: ftMath),
   (NamePtr: @sfnRand; ParamCount: 0; Proc: fnRand; Token: 63; TypeID: ftMath),
   (NamePtr: @sfnRandBetween; ParamCount: 2; Proc: fnRandBetween; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnRound; ParamCount: 2; Proc: fnRound; Token: 27; TypeID: ftMath),
   (NamePtr: @sfnRoundDown; ParamCount: 2; Proc: fnRoundDown; Token: 213; TypeID: ftMath),
   (NamePtr: @sfnRoundUp; ParamCount: 2; Proc: fnRoundUp; Token: 212; TypeID: ftMath),
   (NamePtr: @sfnSec; ParamCount: 1; Proc: fnSec; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnSech; ParamCount: 1; Proc: fnSech; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnSign; ParamCount: 1; Proc: fnSign; Token: 26; TypeID: ftMath),
   (NamePtr: @sfnSin; ParamCount: 1; Proc: fnSin; Token: 15; TypeID: ftMath),
   (NamePtr: @sfnSinh; ParamCount: 1; Proc: fnSinH; Token: 229; TypeID: ftMath),
   (NamePtr: @sfnSqrt; ParamCount: 1; Proc: fnSQRT; Token: 20; TypeID: ftMath),
   (NamePtr: @sfnSqrtPi; ParamCount: 1; Proc: fnSQRTPI; Token: 255; TypeID: ftMath),
   (NamePtr: @sfnSum; ParamCount: 1; Proc: fnSum; Token: 4; TypeID: ftMath),
   (NamePtr: @sfnSumIF; ParamCount: 3; Proc: fnSumIF; Token: 345; TypeID: ftMath),
   (NamePtr: @sfnSumSQ; ParamCount: 1; Proc: fnSumSQ; Token: 321; TypeID: ftMath),
   (NamePtr: @sfnTan; ParamCount: 1; Proc: fnTan;  Token: 17; TypeID: ftMath),
   (NamePtr: @sfnTanh; ParamCount: 1; Proc: fnTanH; Token: 231; TypeID: ftMath),
   (NamePtr: @sfnTrunc; ParamCount: 2; proc: fnTrunc; Token: 197; TypeID: ftMath),
   // Statistical
   (NamePtr: @sfnAveDev; ParamCount: 1; Proc: fnAveDev; Token: 269; TypeID: ftStatistical),
   (NamePtr: @sfnAverage; ParamCount: 1; Proc: fnAverage; Token: 5; TypeID: ftStatistical),
   (NamePtr: @sfnAverageA; ParamCount: 1; Proc: fnAverageA; Token: 361; TypeID: ftStatistical),
   (NamePtr: @sfnAverageIF; ParamCount: 3; Proc: fnAverageIF; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnBeta_Dist; ParamCount: 6; Proc: fnBeta_Dist; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnBeta_Inv; ParamCount: 5; Proc: fnBeta_Inv; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnBinom_Dist; ParamCount: 4; Proc: fnBinom_Dist; Token: 255; TypeID: ftCompatibility),
   (NamePtr: @sfnBinom_Dist_Range; ParamCount: 4; Proc: fnBinom_Dist_Range; Token: 255; TypeID: ftCompatibility),
   (NamePtr: @sfnChiSQ_Dist; ParamCount: 3; Proc: fnChiSQ_Dist; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnChiSQ_Dist_RT; ParamCount: 2; Proc: fnChiSQ_Dist_RT; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnChiSQ_Inv; ParamCount: 2; Proc: fnChiSQ_Inv; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnChiSQ_Inv_RT; ParamCount: 2; Proc: fnChiSQ_Inv_RT; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnCorrel; ParamCount: 2; Proc: fnCorrel; Token: 307; TypeID: ftStatistical),
   (NamePtr: @sfnCovariance_P; ParamCount: 2; Proc: fnCovariance_P; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnCovariance_S; ParamCount: 2; Proc: fnCovariance_S; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnCount; ParamCount: 1; Proc: fnCount; Token: 0; TypeID: ftStatistical),
   (NamePtr: @sfnCountA; ParamCount: 1; Proc: fnCountA; Token: 169; TypeID: ftStatistical),
   (NamePtr: @sfnCountBlank; ParamCount: 1; Proc: fnCountBlank; Token: 347; TypeID: ftStatistical),
   (NamePtr: @sfnCountIF; ParamCount: 2; Proc: fnCountIF; Token: 346; TypeID: ftStatistical),
   (NamePtr: @sfnDevSQ; ParamCount: 1; Proc: fnDevSQ; Token: 318; TypeID: ftStatistical),
   (NamePtr: @sfnExpon_Dist; ParamCount: 3; Proc: fnExpon_Dist; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnForecast; ParamCount: 3; Proc: fnForecast; Token: 309; TypeID: ftStatistical),
   (NamePtr: @sfnGamma; ParamCount: 1; Proc: fnGamma; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnGamma_Dist; ParamCount: 4; Proc: fnGamma_Dist; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnGamma_Inv; ParamCount: 3; Proc: fnGamma_Inv; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnGammaLn; ParamCount: 1; Proc: fnGammaLn; Token: 271; TypeID: ftStatistical),
   (NamePtr: @sfnGammaLn_Precise; ParamCount: 1; Proc: fnGammaLn_Precise; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnGauss; ParamCount: 1; Proc: fnGauss; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnGeomean; ParamCount: 1; Proc: fnGeomean; Token: 319; TypeID: ftStatistical),
   (NamePtr: @sfnHypgeom_Dist; ParamCount: 5; Proc: fnHypgeom_Dist; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnIntercept; ParamCount: 2; Proc: fnIntercept; Token: 311; TypeID: ftStatistical),
   (NamePtr: @sfnLarge; ParamCount: 2; Proc: fnLarge; Token: 325; TypeID: ftStatistical),
   (NamePtr: @sfnMax; ParamCount: 1; Proc: fnMax; Token: 7; TypeID: ftStatistical),
   (NamePtr: @sfnMaxA; ParamCount: 1; Proc: fnMaxA; Token: 362; TypeID: ftStatistical),
   (NamePtr: @sfnMedian; ParamCount: 1; Proc: fnMedian; Token: 227; TypeID: ftStatistical),
   (NamePtr: @sfnMin; ParamCount: 1; Proc: fnMin; Token: 6; TypeID: ftStatistical),
   (NamePtr: @sfnMinA; ParamCount: 1; Proc: fnMinA; Token: 363; TypeID: ftStatistical),
   (NamePtr: @sfnNorm_Dist; ParamCount: 4; Proc: fnNorm_Dist; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnNorm_Inv; ParamCount: 3; Proc: fnNorm_Inv; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnNorm_S_Dist; ParamCount: 2; Proc: fnNorm_S_Dist; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnNorm_S_Inv; ParamCount: 1; Proc: fnNorm_S_Inv; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnPearson; ParamCount: 2; Proc: fnPearson; Token: 312; TypeID: ftStatistical),
   (NamePtr: @sfnPercentile_Exc; ParamCount: 2; Proc: fnPercentile_Exc; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnPercentile_Inc; ParamCount: 2; Proc: fnPercentile_Inc; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnPermut; ParamCount: 2; Proc: fnPermut; Token: 299; TypeID: ftStatistical),
   (NamePtr: @sfnPoisson_Dist; ParamCount: 3; Proc: fnPoisson_Dist; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnQuartile_Exc; ParamCount: 2; Proc: fnQuartile_Exc; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnQuartile_Inc; ParamCount: 2; Proc: fnQuartile_Inc; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnRank_Avg; ParamCount: 3; Proc: fnRank_Avg; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnRank_Eq; ParamCount: 3; Proc: fnRank_Eq; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnRSQ; ParamCount: 2; Proc: fnRSQ; Token: 313; TypeID: ftStatistical),
   (NamePtr: @sfnSkew; ParamCount: 1; Proc: fnSkew; Token: 323; TypeID: ftStatistical),
   (NamePtr: @sfnSkew_P; ParamCount: 1; Proc: fnSkew_P; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnSlope; ParamCount: 2; Proc: fnSlope; Token: 315; TypeID: ftStatistical),
   (NamePtr: @sfnSmall; ParamCount: 2; Proc: fnSmall; Token: 326; TypeID: ftStatistical),
   (NamePtr: @sfnStandardize; ParamCount: 3; Proc: fnStandardize; Token: 297; TypeID: ftStatistical),
   (NamePtr: @sfnStDev_S; ParamCount: 1; Proc: fnStDev_S; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnStDev_P; ParamCount: 1; Proc: fnStDev_P; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnStDevA; ParamCount: 1; Proc: fnStDevA; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnStDevPA; ParamCount: 1; Proc: fnStDevPA; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnSTEYX; ParamCount: 2; Proc: fnSTEYX; Token: 314; TypeID: ftStatistical),
   (NamePtr: @sfnSubTotal; ParamCount: 2; Proc: fnSubTotal; Token: 344; TypeID: ftStatistical),
   (NamePtr: @sfnT_Dist; ParamCount: 3; Proc: fnT_Dist; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnT_Dist_2T; ParamCount: 2; Proc: fnT_Dist_2T; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnT_Dist_RT; ParamCount: 2; Proc: fnT_Dist_RT; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnT_Inv; ParamCount: 2; Proc: fnT_Inv; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnT_Inv_2T; ParamCount: 2; Proc: fnT_Inv_2T; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnVar_P; ParamCount: 1; Proc: fnVar_P; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnVar_S; ParamCount: 1; Proc: fnVar_S; Token: 255; TypeID: ftStatistical),
   (NamePtr: @sfnVarA; ParamCount: 1; Proc: fnVarA; Token: 367; TypeID: ftStatistical),
   (NamePtr: @sfnVarPA; ParamCount: 1; Proc: fnVarPA; Token: 365; TypeID: ftStatistical),
   (NamePtr: @sfnWeibull_Dist; ParamCount: 4; Proc: fnWeibull_Dist; Token: 255; TypeID: ftStatistical),
   // Text
   (NamePtr: @sfnChar; ParamCount: 1; Proc: fnChar; Token: 111; TypeID: ftText),
   (NamePtr: @sfnClean; ParamCount: 1; Proc: fnClean; Token: 162; TypeID: ftText),
   (NamePtr: @sfnCode; ParamCount: 1; Proc: fnCode; Token: 121; TypeID: ftText),
   (NamePtr: @sfnConcatenate; ParamCount: 1; Proc: fnConcatenate; Token: 336; TypeID: ftText),
   (NamePtr: @sfnDollar; ParamCount: 2; Proc: fnDollar; Token: 13; TypeID: ftText),
   (NamePtr: @sfnExact; ParamCount: 2; Proc: fnExact; Token: 117; TypeID: ftText),
   (NamePtr: @sfnFind; ParamCount: 3; Proc: fnFind; Token: 124; TypeID: ftText),
   (NamePtr: @sfnFixed; ParamCount: 3; Proc: fnFixed; Token: 14; TypeID: ftText),
   (NamePtr: @sfnLeft; ParamCount: 2; Proc: fnLeft; Token: 115; TypeID: ftText),
   (NamePtr: @sfnLen; ParamCount: 1; Proc: fnLen; Token: 32; TypeID: ftText),
   (NamePtr: @sfnLower; ParamCount: 1; Proc: fnLower; Token: 112; TypeID: ftText),
   (NamePtr: @sfnMid; ParamCount: 3; Proc: fnMid; Token: 31; TypeID: ftText),
   (NamePtr: @sfnProper; ParamCount: 1; Proc: fnProper; Token: 114; TypeID: ftText),
   (NamePtr: @sfnReplace; ParamCount: 4; Proc: fnReplace; Token: 119; TypeID: ftText),
   (NamePtr: @sfnRept; ParamCount: 2; Proc: fnRept; Token: 30; TypeID: ftText),
   (NamePtr: @sfnRight; ParamCount: 2; Proc: fnRight; Token: 116; TypeID: ftText),
   (NamePtr: @sfnSearch; ParamCount: 3; Proc: fnSearch; Token: 82; TypeID: ftText),
   (NamePtr: @sfnSubstitute; ParamCount: 4; Proc: fnSubstitute; Token: 120; TypeID: ftText),
   (NamePtr: @sfnT; ParamCount: 1; Proc: fnT; Token: 130; TypeID: ftText),
   (NamePtr: @sfnText; ParamCount: 2; Proc: fnText; Token: 48; TypeID: ftText),
   (NamePtr: @sfnTrim; ParamCount: 1; Proc: fnTrim; Token: 118; TypeID: ftText),
   (NamePtr: @sfnUpper; ParamCount: 1; Proc: fnUpper; Token: 113; TypeID: ftText),
   (NamePtr: @sfnValue; ParamCount: 1; Proc: fnValue; Token: 33; TypeID: ftText)
   );

function dxSpreadSheetFunctionsRepository: TdxSpreadSheetFunctionsRepository;
begin
  if Repository = nil then
    Repository := TdxSpreadSheetFunctionsRepository.Create();
  Result := Repository;
end;

function dxCompareFunctions(AInfo1, AInfo2: TdxSpreadSheetFunctionInfo): Integer;
begin
  if AInfo1 = AInfo2 then
    Result := 0
  else
    Result := dxSpreadSheetCompareText(AInfo1.Name, AInfo2.Name)
end;

{ TdxSpreadSheetFunctionsRepository }

procedure TdxSpreadSheetFunctionsRepository.Add(const AName: TcxResourceStringID;
  AProc: TdxSpreadSheetFunction; AParamCount: Integer; AToken: Word; AType: TdxSpreadSheetFunctionType = ftCommon);
var
  AInfo: TdxSpreadSheetFunctionInfo;
begin
  AInfo := TdxSpreadSheetFunctionInfo.Create;
  AInfo.NamePtr := AName;
  AInfo.Proc := AProc;
  AInfo.ParamCount := AParamCount;
  AInfo.Token := AToken;
  AInfo.TypeID := AType;
  AInfo.UpdateInfo(nil);
  inherited Add(AInfo);
  Sorted := False;
end;

function TdxSpreadSheetFunctionsRepository.AddUnknown(const AName: TdxUnicodeString;
  AType: TdxSpreadSheetFunctionType = ftCommon): TdxSpreadSheetFunctionInfo;
begin
  Result := TdxSpreadSheetFunctionInfo.Create;
  Result.NamePtr := nil;
  Result.Name := AName;
  Result.Proc := nil;
  inherited Add(Result);
  Sorted := False;
end;

function TdxSpreadSheetFunctionsRepository.GetInfoByID(const ID: Integer): TdxSpreadSheetFunctionInfo;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to Count - 1 do
    if Items[I].Token = ID then
    begin
      Result := Items[I];
      Break;
    end;
end;

function TdxSpreadSheetFunctionsRepository.GetInfoByName(const AName: TdxUnicodeString): TdxSpreadSheetFunctionInfo;
begin
  Result := GetInfoByName(@AName[1], Length(AName));
end;

function TdxSpreadSheetFunctionsRepository.GetInfoByName(
  const AName: PdxUnicodeChar; ALength: Integer): TdxSpreadSheetFunctionInfo;
var
  L, H, I, C: TdxNativeInt;
begin
  Sorted := True;
  Result := nil;
  L := 0;
  H := Count - 1;
  while L <= H do
  begin
    I := (L + H) shr 1;
    C := dxSpreadSheetCompareText(TdxSpreadSheetFunctionInfo(List[I]).Name, AName, ALength);
    if C < 0 then
      L := I + 1
    else
    begin
      H := I - 1;
      if C = 0 then
      begin
        Result := TdxSpreadSheetFunctionInfo(List[I]);
        Break;
      end;
    end;
  end;
end;

procedure TdxSpreadSheetFunctionsRepository.TranslationChanged;
var
  I: Integer; 
begin
  Sorted := False;
  for I := 0 to Count - 1 do
    Items[I].UpdateInfo(AFormatSettings);
  Sorted := True;
end;

function TdxSpreadSheetFunctionsRepository.GetItem(
  AIndex: Integer): TdxSpreadSheetFunctionInfo;
begin
  Result := TdxSpreadSheetFunctionInfo(inherited Items[AIndex]);
end;

procedure TdxSpreadSheetFunctionsRepository.SetSorted(AValue: Boolean);
begin
  if FSorted <> AValue then
  begin
    FSorted := AValue;
    if Sorted then
      Sort(@dxCompareFunctions);
  end;
end;

procedure RegisterFunctions;
var
  I: Integer;
begin
  for I := Low(PredefinedFunctions) to High(PredefinedFunctions) do
    dxSpreadSheetFunctionsRepository.Add(PredefinedFunctions[I].NamePtr,
      PredefinedFunctions[I].Proc, PredefinedFunctions[I].ParamCount, PredefinedFunctions[I].Token, PredefinedFunctions[I].TypeID);
end;

procedure UnregisterFunctions;
begin
  FreeAndNil(Repository);
end;

initialization
  RegisterFunctions;

finalization
  UnregisterFunctions;

end.
