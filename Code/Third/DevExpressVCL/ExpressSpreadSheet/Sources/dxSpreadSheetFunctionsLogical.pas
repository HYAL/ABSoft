{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFunctionsLogical;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  SysUtils, Variants, Math, cxDateUtils, dxSpreadSheetCore, dxSpreadSheetTypes, dxSpreadSheetUtils,
  dxSpreadSheetCoreHelpers;

procedure fnAND(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnFalse(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnIF(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnIfError(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnIfNA(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnNOT(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnOR(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnTrue(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnXOR(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);

implementation

type
  TdxAccumulationLogicalType = (altAND, altOR, altXOR);

function dxAccumulateLogicalResult(const AParameter: Variant; ACanConvertStrToNumber: Boolean;
  var AErrorCode: TdxSpreadSheetFormulaErrorCode; AData: TdxSpreadSheetEnumValues; AType: TdxAccumulationLogicalType): Boolean;
var
  ACheckedValue: Variant;
begin
  Result := AErrorCode = ecNone;
  if Result then
  begin
    if not AData.CheckValueOnLogical(AParameter, ACanConvertStrToNumber, ACheckedValue) then
      AData.SetErrorCode(ecValue)
    else
      if not VarIsNull(ACheckedValue) then
      case AType of
        altAND:
          AData.ResultValue := AData.ResultValue and ACheckedValue;
        altOR, altXOR:
          AData.ResultValue := AData.ResultValue or ACheckedValue;
      end;
  end
  else
    AData.SetErrorCode(AErrorCode);
end;

function cbAND(AValue: Variant; ACanConvertStrToNumber: Boolean; var AErrorCode: TdxSpreadSheetFormulaErrorCode;
  AData: TdxSpreadSheetEnumValues; AInfo: Pointer = nil): Boolean;
begin
  Result := dxAccumulateLogicalResult(AValue, ACanConvertStrToNumber, AErrorCode, AData, altAND);
end;

function cbOR(AValue: Variant; ACanConvertStrToNumber: Boolean; var AErrorCode: TdxSpreadSheetFormulaErrorCode;
  AData: TdxSpreadSheetEnumValues; AInfo: Pointer = nil): Boolean;
begin
  Result := dxAccumulateLogicalResult(AValue, ACanConvertStrToNumber, AErrorCode, AData, altOR);
end;

function cbXOR(AValue: Variant; ACanConvertStrToNumber: Boolean; var AErrorCode: TdxSpreadSheetFormulaErrorCode;
  AData: TdxSpreadSheetEnumValues; AInfo: Pointer = nil): Boolean;
begin
  Result := dxAccumulateLogicalResult(AValue, ACanConvertStrToNumber, AErrorCode, AData, altXOR);
end;

procedure fnANDORXOR(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken;
  AProc: TdxSpreadSheetForEachCallBack);
var
  AData: TdxSpreadSheetEnumValues;
  ADataInfo: TdxSpreadSheetEnumValuesProcessingInfo;
begin
  ADataInfo.Init(True, False, False, False);
  AData := TdxSpreadSheetEnumValues.Create(Sender.FormatSettings, @AProc = @cbAND, ADataInfo);
  try
    dxSpreadSheetCalculateForEachParam(Sender, AParams, AData, AProc);
    if not AData.Validate or (AData.EmptyCount + AData.StringCount = AData.Count) then
      Sender.SetError(AData.ErrorCode)
    else
    begin
      if @AProc = @cbXOR then
        AData.ResultValue := AData.ResultValue and Odd(AData.TrueCount);
      Sender.AddValue(AData.ResultValue);
    end;
  finally
    FreeAndNil(AData);
  end;
end;

procedure fnAND(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  fnANDORXOR(Sender, AParams, @cbAND);
end;

procedure fnFalse(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  Sender.AddValue(False);
end;

procedure fnIF(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ACondition, AValueIfTrue, AValueIfFalse: Variant;
begin
  if not Sender.ExtractNumericParameter(ACondition, AParams) then
    Sender.SetError(ecValue)
  else
    if ACondition <> 0 then
    begin
      if Sender.ExtractParameterDef(AValueIfTrue, True, 0, AParams, 1) then
        Sender.AddValue(AValueIfTrue)
    end
    else
    begin
      if Sender.ExtractParameterDef(AValueIfFalse, False, 0, AParams, 2) then
        Sender.AddValue(AValueIfFalse);
    end;
end;

procedure fnIfError(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractParameterDef(AParameter, 0, AParams) then
    Sender.AddValue(AParameter)
  else
    begin
      Sender.SetError(ecNone);
      if Sender.ExtractParameterDef(AParameter, 0, AParams, 1) then
        Sender.AddValue(AParameter);
    end;
end;

procedure fnIfNA(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  Sender.ExtractParameterDef(AParameter, 0, AParams);
  if Sender.ErrorCode <> ecNA then
    Sender.AddValue(AParameter)
  else
    begin
      Sender.SetError(ecNone);
      if Sender.ExtractParameterDef(AParameter, 0, AParams, 1) then
        Sender.AddValue(AParameter);
    end;
end;

procedure fnNOT(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractParameter(AParameter, AParams) then
    Sender.AddValue(not AParameter);
end;

procedure fnOR(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  fnANDORXOR(Sender, AParams, @cbOR);
end;

procedure fnTrue(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  Sender.AddValue(True);
end;

procedure fnXOR(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  fnANDORXOR(Sender, AParams, @cbXOR);
end;

end.
