{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFormatXLSTypes;

{$I cxVer.Inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Graphics, dxCore, dxSpreadSheetClasses;

type
  { TdxXLSFont }
  TdxXLSFont = class
  public
    Style: TFontStyles;
    Size: Integer;
    Charset: TFontCharset;
    Color: TColor;
    Name: TdxUnicodeString;
    Handle: TdxSpreadSheetFontHandle;
    procedure AssignTo(AFont: TdxSpreadSheetFontHandle);
  end;

  { TdxXLSCellStyle }

  TdxXLSCellStyle = class
  public
    Font: Word;
    Format: Word;
    Flags: Word;
    Alignments: Word;
    Indents: Word;
    BordersStyle: Word;
    LeftRightBorders: Word;
    TopBottomBordersAndFill: Integer;
    FillColors: Word;
    //
    Handle: TdxSpreadSheetCellStyleHandle;
    destructor Destroy; override;
  end;


const

  { Biff reecords constants }
  brc1904                    =   $0022;              // 1904 Date System
  brcADDIN                   =   $0087;              // Workbook Is an Add-in Macro
  brcADDMENU                 =   $00C2;              // Menu Addition
  brcARRAY                   =   $0221;              // Array-Entered Formula
  brcAUTOFILTER              =   $009E;              // AutoFilter Data
  brcAUTOFILTERINFO          =   $009D;              // Drop-Down Arrow Count
  brcBACKUP                  =   $0040;              // Save Backup Version of the File
  brcBLANK                   =   $0201;              // Cell Value, Blank Cell
  brcBOF                     =   $0809;              // Beginning of File
  brcBOOKBOOL                =   $00DA;              // Workbook Option Flag
  brcBOOLERR                 =   $0205;              // Cell Value, Boolean or Error
  brcBOTTOMMARGIN            =   $0029;              // Bottom Margin Measurement
  brcBOUNDSHEET              =   $0085;              // Sheet Information
  brcCALCCOUNT               =   $000C;              // Iteration Count
  brcCALCMODE                =   $000D;              // Calculation Mode
  brcCF                      =   $01B1;              // Conditional Formatting Conditions
  brcCODENAME                =   $0042;              // VBE Object Name
  brcCODEPAGE                =   $0142;              // Default Code Page
  brcCOLINFO                 =   $007D;              // Column Formatting Information
  brcCONDFMT                 =   $01B0;              // Conditional Formatting Range Information
  brcCONTINUE                =   $003C;              // Continues Long Records
  brcCONTINUEFRT             =   $0812;              // Continued FRT
  brcCOORDLIST               =   $00A9;              // Polygon Object Vertex Coordinates
  brcCOUNTRY                 =   $008C;              // Default Country and WIN.INI Country
  brcCRN                     =   $005A;              // Nonresident Operands
  brcDBCELL                  =   $00D7;              // Stream Offsets
  brcDBQUERYEXT              =   $0803;              // Database Query Extensions
  brcDCON                    =   $0050;              // Data Consolidation Information
  brcDCONBIN                 =   $01B5;              // Data Consolidation Information
  brcDCONNAME                =   $0052;              // Data Consolidation Named References
  brcDCONREF                 =   $0051;              // Data Consolidation References
  brcDEFAULTROWHEIGHT        =   $0225;              // Default Row Height
  brcDEFCOLWIDTH             =   $0055;              // Default Width for Columns
  brcDELMENU                 =   $00C3;              // Menu Deletion
  brcDELTA                   =   $0010;              // Iteration Increment
  brcDIMENSIONS              =   $0200;              // Cell Table Size
  brcDOCROUTE                =   $00B8;              // Routing Slip Information
  brcDSF                     =   $0161;              // Double Stream File
  brcDV                      =   $01BE;              // Data Validation Criteria
  brcDVAL                    =   $01B2;              // Data Validation Information
  brcEDG                     =   $0088;              // Edition Globals
  brcEOF                     =   $000A;              // End of File
  brcEXCEL9FILE              =   $01C0;              // Excel 9 File
  brcEXTERNCOUNT             =   $0016;              // Number of External References
  brcEXTERNNAME              =   $0023;              // Externally Referenced Name
  brcEXTERNSHEET             =   $0017;              // External Reference
  brcEXTSST                  =   $00FF;              // Extended Shared String Table
  brcEXTSTRING               =   $0804;              // FRT String
  brcFILEPASS                =   $002F;              // File Is Password-Protected
  brcFILESHARING             =   $005B;              // File-Sharing Information
  brcFILESHARING2            =   $01A5;              // File-Sharing Information for Shared Lists
  brcFILTERMODE              =   $009B;              // Sheet Contains Filtered List
  brcFNGROUPCOUNT            =   $009C;              // Built-in Function Group Count
  brcFNGROUPNAME             =   $009A;              // Function Group Name
  brcFONT                    =   $0031;              // Font Description
  brcFOOTER                  =   $0015;              // Print Footer on Each Page
  brcFORMAT                  =   $041E;              // Number Format
  brcFORMULA                 =   $0006;              // Cell Formula
  brcGCW                     =   $00Ab;              // Global Column-Width Flags
  brcGRIDSET                 =   $0082;              // State Change of Gridlines Option
  brcGUTS                    =   $0080;              // Size of Row and Column Gutters
  brcHCENTER                 =   $0083;              // Center Between Horizontal Margins
  brcHEADER                  =   $0014;              // Print Header on Each Page
  brcHIDEOBJ                 =   $008D;              // Object Display Options
  brcHLINK                   =   $01B8;              // Hyperlink
  brcHLINKTOOLTIP            =   $0800;              // Hyperlink Tooltip
  brcHORIZONTALPAGEBREAKS    =   $001B;              // Explicit Row Page Breaks
  brcIMDATA                  =   $007F;              // Image Data
  brcINDEX                   =   $020B;              // Index Record
  brcINTERFACEEND            =   $00E2;              // End of User Interface Records
  brcINTERFACEHDR            =   $00E1;              // Beginning of User Interface Records
  brcITERATION               =   $0011;              // Iteration Mode
  brcLABEL                   =   $0204;              // Cell Value, String Constant
  brcLABELSST                =   $00FD;              // Cell Value, String Constant/SST
  brcLEFTMARGIN              =   $0026;              // Left Margin Measurement
  brcLHNGRAPH                =   $0095;              // Named Graph Information
  brcLHRECORD                =   $0094;              // .WK? File Conversion Information
  brcLPR                     =   $0098;              // Sheet Was Printed Using LINE.PRINT(
  brcMERGECELLS              =   $00E5;              // Merged Cells
  brcMMS                     =   $00C1;              // ADDMENU/DELMENU Record Group Count
  brcMSODRAWINGGROUP         =   $00EB;              // Microsoft Office Drawing Group
  brcMSODRAWING              =   $00EC;              // Microsoft Office Drawing
  brcMSODRAWINGSELECTION     =   $00ED;              // Microsoft Office Drawing Selection
  brcMULBLANK                =   $00BE;              // Multiple Blank Cells
  brcMULRK                   =   $00BD;              // Multiple RK Cells
  brcNAME                    =   $0018;              // Defined Name
  brcNOTE                    =   $001C;              // Comment Associated with a Cell
  brcNUMBER                  =   $0203;              // Cell Value, Floating-Point Number
  brcOBJ                     =   $005D;              // Describes a Graphic Object
  brcOBJPROTECT              =   $0063;              // Objects Are Protected
  brcOBPROJ                  =   $00D3;              // Visual Basic Project
  brcOLEDBCONN               =   $080A;              // OLE Database Connection
  brcOLESIZE                 =   $00DE;              // Size of OLE Object
  brcPALETTE                 =   $0092;              // Color Palette Definition
  brcPANE                    =   $0041;              // Number of Panes and Their Position
  brcPARAMQRY                =   $00DC;              // Query Parameters
  brcPASSWORD                =   $0013;              // Protection Password
  brcPLS                     =   $004D;              // Environment-Specific Print Record
  brcPRECISION               =   $000E;              // Precision
  brcPRINTGRIDLINES          =   $002B;              // Print Gridlines Flag
  brcPRINTHEADERS            =   $002A;              // Print Row/Column Labels
  brcPROTECT                 =   $0012;              // Protection Flag
  brcPROT4REV                =   $01AF;              // Shared Workbook Protection Flag
  brcQSI                     =   $01AD;              // External Data Range
  brcQSIF                    =   $0807;              // Query Table Field Formatting
  brcQSIR                    =   $0806;              // Query Table Formatting
  brcQSISXTAG                =   $0802;              // PivotTable and Query Table Extensions
  brcRECALCID                =   $01C1;              // Recalc Information
  brcRECIPNAME               =   $00B9;              // Recipient Name
  brcREFMODE                 =   $000F;              // Reference Mode
  brcREFRESHALL              =   $01B7;              // Refresh Flag
  brcRIGHTMARGIN             =   $0027;              // Right Margin Measurement
  brcRK                      =   $027E;              // Cell Value, RK Number
  brcROW                     =   $0208;              // Describes a Row
  brcRSTRING                 =   $00D6;              // Cell with Character Formatting
  brcSAVERECALC              =   $005F;              // Recalculate Before Save
  brcSCENARIO                =   $00AF;              // Scenario Data
  brcSCENMAN                 =   $00AE;              // Scenario Output Data
  brcSCENPROTECT             =   $00DD;              // Scenario Protection
  brcSCL                     =   $00A0;              // Window Zoom Magnification
  brcSELECTION               =   $001D;              // Current Selection
  brcSETUP                   =   $00A1;              // Page Setup
  brcSHRFMLA                 =   $04BC;              // Shared Formula
  brcSORT                    =   $0090;              // Sorting Options
  brcSOUND                   =   $0096;              // Sound Note
  brcSST                     =   $00FC;              // Shared String Table
  brcSTANDARDWIDTH           =   $0099;              // Standard Column Width
  brcSTRING                  =   $0207;              // String Value of a Formula
  brcSTYLE                   =   $0293;              // Style Information
  brcSUB                     =   $0091;              // Subscriber
  brcSUPBOOK                 =   $01AE;              // Supporting Workbook
  brcSXDB                    =   $00C6;              // PivotTable Cache Data
  brcSXDBEX                  =   $0122;              // PivotTable Cache Data
  brcSXDI                    =   $00C5;              // Data Item
  brcSXEX                    =   $00F1;              // PivotTable View Extended Information
//  brcSXEXT                   =   $00DC;              // External Source Information
  brcSXFDBTYPE               =   $01BB;              // SQL Datatype Identifier
  brcSXFILT                  =   $00F2;              // PivotTable Rule Filter
  brcSXFORMAT                =   $00FB;              // PivotTable Format Record
  brcSXFORMULA               =   $0103;              // PivotTable Formula Record
  brcSXFMLA                  =   $00F9;              // PivotTable Parsed Expression
  brcSXIDSTM                 =   $00D5;              // Stream ID
  brcSXIVD                   =   $00B4;              // Row/Column Field Ids
  brcSXLI                    =   $00B5;              // Line Item Array
  brcSXNAME                  =   $00F6;              // PivotTable Name
  brcSXPAIR                  =   $00F8;              // PivotTable Name Pair
  brcSXPI                    =   $00B6;              // Page Item
  brcSXPIEX                  =   $080E;              // OLAP Page Item Extensions
  brcSXRULE                  =   $00F0;              // PivotTable Rule Data
  brcSXSTRING                =   $00CD;              // String
  brcSXSELECT                =   $00F7;              // PivotTable Selection Information
  brcSXTBL                   =   $00D0;              // Multiple Consolidation Source Info
  brcSXTBPG                  =   $00D2;              // Page Item Indexes
  brcSXTBRGIITM              =   $00D1;              // Page Item Name Count
  brcSXTH                    =   $080D;              // PivotTable OLAP Hierarchy
  brcSXVD                    =   $00B1;              // View Fields
  brcSXVDEX                  =   $0100;              // Extended PivotTable View Fields
  brcSXVDTEX                 =   $080F;              // View Dimension OLAP Extensions
  brcSXVI                    =   $00B2;              // View Item
  brcSXVIEW                  =   $00B0;              // View Definition
  brcSXVIEWEX                =   $080C;              // Pivot Table OLAP Extensions
  brcSXVIEWEX9               =   $0810;              // Pivot Table Extensions
  brcSXVS                    =   $00E3;              // View Source
  brcTABID                   =   $013D;              // Sheet Tab Index Array
  brcTABIDCONF               =   $00EA;              // Sheet Tab ID of Conflict History
  brcTABLE                   =   $0236;              // Data Table
  brcTEMPLATE                =   $0060;              // Workbook Is a Template
  brcTOPMARGIN               =   $0028;              // Top Margin Measurement
  brcTXO                     =   $01B6;              // Text Object
  brcUDDESC                  =   $00DF;              // Description String for Chart Autoformat
  brcUNCALCED                =   $005E;              // Recalculation Status
  brcUSERBVIEW               =   $01A9;              // Workbook Custom View Settings
  brcUSERSVIEWBEGIN          =   $01AA;              // Custom View Settings
  brcUSERSVIEWEND            =   $01Ab;              // End of Custom View Records
  brcUSESELFS                =   $0160;              // Natural Language Formulas Flag
  brcVCENTER                 =   $0084;              // Center Between Vertical Margins
  brcVERTICALPAGEBREAKS      =   $001A;              // Explicit Column Page Breaks
  brcWINDOW1                 =   $003D;              // Window Information
  brcWINDOW2                 =   $023E;              // Sheet Window Information
  brcWINDOWPROTECT           =   $0019;              // Windows Are Protected
  brcWOPT                    =   $080B;              // Web Options
  brcWRITEACCESS             =   $005C;              // Write Access User Name
  brcWRITEPROT               =   $0086;              // Workbook Is Write-Protected
  brcWSBOOL                  =   $0081;              // Additional Workspace Information
  brcXCT                     =   $0059;              // CRN Record Count
  brcXF                      =   $00E0;              // Extended Format
  brcXL5MODIFY               =   $0162;              // Flag for DSF

  // microsoft excel formula tokens for parsed expression
  ptgExp       = $01;
  ptgTbl       = $02;
  ptgAdd       = $03;
  ptgSub       = $04;
  ptgMul       = $05;
  ptgDiv       = $06;
  ptgPower     = $07;
  ptgConcat    = $08;
  ptgLT        = $09;
  ptgLE        = $0A;
  ptgEQ        = $0B;
  ptgGE        = $0C;
  ptgGT        = $0D;
  ptgNE        = $0E;
  ptgIsect     = $0F;
  ptgUnion     = $10;
  ptgRange     = $11;
  ptgUplus     = $12;
  ptgUminus    = $13;
  ptgPercent   = $14;
  ptgParen     = $15;
  ptgMissArg   = $16;
  ptgStr       = $17;
  ptgExtend    = $18;
  ptgAttr      = $19;
  ptgSheet     = $1A;
  ptgEndSheet  = $1B;
  ptgErr       = $1C;
  ptgBool      = $1D;
  ptgInt       = $1E;
  ptgNum       = $1F;
  ptgArray     = $20;
  ptgFunc      = $21;
  ptgFuncVar   = $22;
  ptgName      = $23;
  ptgRef       = $24;
  ptgArea      = $25;
  ptgMemArea   = $26;
  ptgMemErr    = $27;
  ptgMemNoMem  = $28;
  ptgMemFunc   = $29;
  ptgRefErr    = $2A;
  ptgAreaErr   = $2B;
  ptgRefN      = $2C;
  ptgAreaN     = $2D;
  ptgMemAreaN  = $2E;
  ptgMemNoMemN = $2F;

  ptgNameX     = $39;
  ptgRef3d     = $3A;
  ptgArea3d    = $3B;
  ptgRefErr3d  = $3C;
  ptgAreaErr3d = $3D;

  ptgArrayV    = $40;
  ptgFuncV     = $41;
  ptgFuncVarV  = $42;
  ptgNameV     = $43;
  ptgRefV      = $44;
  ptgAreaV     = $45;
  ptgMemAreaV  = $46;
  ptgMemErrV   = $47;
  ptgMemNoMemV = $48;
  ptgMemFuncV  = $49;
  ptgRefErrV   = $4A;
  ptgAreaErrV  = $4B;
  ptgRefNV     = $4C;
  ptgAreaNV    = $4D;
  ptgMemAreaNV = $4E;
  ptgMemNoMemNV= $4F;

  ptgFuncCEV   = $58;
  ptgNameXV    = $59;
  ptgRef3dV    = $5A;
  ptgArea3dV   = $5B;
  ptgRefErr3dV = $5C;
  ptgAreaErr3dV= $5D;

  ptgArrayA    = $60;
  ptgFuncA     = $61;
  ptgFuncVarA  = $62;
  ptgNameA     = $63;
  ptgRefA      = $64;
  ptgAreaA     = $65;
  ptgMemAreaA  = $66;
  ptgMemErrA   = $67;
  ptgMemNoMemA = $68;
  ptgMemFuncA  = $69;
  ptgRefErrA   = $6A;
  ptgAreaErrA  = $6B;
  ptgRefNA     = $6C;
  ptgAreaNA    = $6D;
  ptgMemAreaNA = $6E;
  ptgMemNoMemNA= $6F;

  ptgFuncCEA   = $78;
  ptgNameXA    = $79;
  ptgRef3dA    = $7A;
  ptgArea3dA   = $7B;
  ptgRefErr3dA = $7C;
  ptgAreaErr3dA= $7D;


implementation


{ TdxXLSCellStyle }

destructor TdxXLSCellStyle.Destroy;
begin
  Handle.Release;
  inherited Destroy;
end;

{ TdxXLSFont }

procedure TdxXLSFont.AssignTo(AFont: TdxSpreadSheetFontHandle);
begin
  AFont.Charset := Charset;
  AFont.Color := Color;
  AFont.Name := Name;
  AFont.Size := Size;
  AFont.Style := Style;
end;

end.
