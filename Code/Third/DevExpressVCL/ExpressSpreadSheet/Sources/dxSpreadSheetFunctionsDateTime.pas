{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFunctionsDateTime;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  SysUtils, Variants, Math, DateUtils, cxDateUtils, dxCore, dxSpreadSheetTypes, dxSpreadSheetUtils, dxSpreadSheetCore,
  dxSpreadSheetCoreHelpers;

procedure fnDate(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnDateValue(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnDay(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnDays(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnDays360(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnEDate(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnEOMonth(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnHour(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnIsoWeekNum(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnMinute(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnMonth(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnNow(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnSecond(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnTime(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnTimeValue(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnToday(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnWeekDay(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnWeekNum(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnYear(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
procedure fnYearFrac(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);

implementation

const
  dxMondaysWeek: array [1..7] of Integer =    (7, 1, 2, 3, 4, 5, 6);
  dxMondays0Week: array [1..7] of Integer =   (6, 0, 1, 2, 3, 4, 5);
  dxTuesdaysWeek: array [1..7] of Integer =   (6, 7, 1, 2, 3, 4, 5);
  dxWednesdaysWeek: array [1..7] of Integer = (5, 6, 7, 1, 2, 3, 4);
  dxThursdaysWeek: array [1..7] of Integer =  (4, 5, 6, 7, 1, 2, 3);
  dxFridaysWeek: array [1..7] of Integer =    (3, 4, 5, 6, 7, 1, 2);
  dxSaturDaysWeek: array [1..7] of Integer =  (2, 3, 4, 5, 6, 7, 1);

function dxIsSpreadSheetLeapYear(AYear: Word): Boolean;
begin
  Result := IsLeapYear(AYear) or (AYear = 1900);
end;

function dxGetDays360(const AStart, AEnd: TDate; AMethod: Boolean): Integer;

  function GetGeneralMonthIndex(const AYear, AMonth: Word): Word;
  begin
    Result := (AYear - 1900) * 12 + AMonth;
  end;

var
  AStartYear, AStartMonth, AStartDay: Word;
  AEndYear, AEndMonth, AEndDay: Word;
  ADate: TDate;
begin
  DecodeDate(AStart, AStartYear, AStartMonth, AStartDay);
  DecodeDate(AEnd, AEndYear, AEndMonth, AEndDay);
  if (AStart < 2) and (AEnd < 2) then
  begin
    Result := Trunc(AEnd) - Trunc(AStart);
    Exit;
  end;
  if AMethod then
  begin    
    AStartDay := Min(AStartDay, 30);
    AEndDay := Min(AEndDay, 30);
  end
  else
  begin   
    if (AStartDay = 31) or ((AStartMonth = 2) and (AStartDay = DaysPerMonth(AStartYear, AStartMonth))) then
      AStartDay := 30;
    if AEndDay = 31 then
    begin
      ADate := AEnd + ValueIncr[AStartDay < 30];
      DecodeDate(ADate, AEndYear, AEndMonth, AEndDay);
    end;
  end;
  Result := (GetGeneralMonthIndex(AEndYear, AEndMonth) - GetGeneralMonthIndex(AStartYear, AStartMonth)) * 30 +
    AEndDay - AStartDay;
  Inc(Result, Integer(AStart = 0));
end;

function dxGetIsoWeekNum(ADate: TDate): Word;

  function GetFirstISOYearMonday(AYear: Word): TDate;
  var
    ANewYear: TDate;
    ANewYearWeekDay: Word;
  begin
    ANewYear := EncodeDate(AYear, 1, 1);
    ANewYearWeekDay := DayOfTheWeek(ANewYear);
    if ANewYearWeekDay <= 4 then
      Result := ANewYear - ANewYearWeekDay + 1
    else
      Result := ANewYear + 8 - ANewYearWeekDay;
  end;

var
  AFirstISOYearMonday: TDate;
  AYear, AMonth, ADay: Word;
begin
  DecodeDate(ADate, AYear, AMonth, ADay);
  AFirstISOYearMonday := GetFirstISOYearMonday(AYear + 1);
  if ADate < AFirstISOYearMonday then
    AFirstISOYearMonday := GetFirstISOYearMonday(AYear);
  if ADate < AFirstISOYearMonday then
    AFirstISOYearMonday := GetFirstISOYearMonday(AYear - 1);
  Result := Trunc((ADate - AFirstISOYearMonday) / 7 + 1);
end;

function dxGetWeekDay(ADate: TDate; AType: Integer): Integer;  inline;
var
  AWeekDay: Word;
begin
  Result := -1;
  AWeekDay := DayOfWeek(ADate);
  case AType of
    1, 17:     
      Result := AWeekDay;

    2, 11:    
      Result := dxMondaysWeek[AWeekDay];

    3:        
      Result := dxMondays0Week[AWeekDay];

    12:       
      Result := dxTuesdaysWeek[AWeekDay];

    13:       
      Result := dxWednesdaysWeek[AWeekDay];

    14:       
      Result := dxThursdaysWeek[AWeekDay];

    15:       
      Result := dxFridaysWeek[AWeekDay];

    16:       
      Result := dxSaturDaysWeek[AWeekDay];
  end;
end;

function dxGetWeekNum(ADate: TDate; AType: Integer): Integer;

  function GetFirstDayOfFirstWeekOfYear: TDate;
  var
    ANewYear: TDate;
    ANewYearWeekDay: Word;
  begin
    ANewYear := EncodeDate(YearOf(ADate), 1, 1);
    ANewYearWeekDay := DayOfWeek(ANewYear);
    case AType of
      2, 11:    
        Result := ANewYear - dxMondaysWeek[ANewYearWeekDay] + 1;

      12:       
        Result := ANewYear - dxTuesdaysWeek[ANewYearWeekDay] + 1;

      13:       
        Result := ANewYear - dxWednesdaysWeek[ANewYearWeekDay] + 1;

      14:       
        Result := ANewYear - dxThursdaysWeek[ANewYearWeekDay] + 1;

      15:       
        Result := ANewYear - dxFridaysWeek[ANewYearWeekDay] + 1;

      16:       
        Result := ANewYear - dxSaturDaysWeek[ANewYearWeekDay] + 1;
    else   
        Result := ANewYear - ANewYearWeekDay + 1;
    end;
  end;

begin
  if AType = 21 then
     Result := dxGetIsoWeekNum(ADate)
  else
  begin
    Result := -1;
    if AType in [1, 2, 11, 12, 13, 14, 15, 16, 17] then
      Result := Trunc((ADate - GetFirstDayOfFirstWeekOfYear) / 7 + 1);
  end;
end;

function dxGetXLSDate(AYear, AMonth, ADay: Variant): TDateTime; inline;
begin
  Result := 2;   
  if AYear < 1900 then
    AYear := AYear + 1900;
  Result := IncDay(IncMonth(IncYear(Result, AYear - 1900), AMonth - 1), ADay - 1);
end;

function dxGetYearFrac(AStart, AEnd: TDate; ABasis: Integer): Real;

  procedure CheckExchange(var AStartDate, AEndDate: TDate);
  var
    ADate: TDate;
  begin
    if AStartDate < AEndDate then
      Exit;
    ADate := AStartDate;
    AStartDate := AEndDate;
    AEndDate := ADate;
  end;

  function GetAverageDaysPerYears(AYearStart, AYearEnd: Word): Real;
  var
    I: Word;
  begin
    Result := 0;
    for I := AYearStart to AYearEnd do
      Result := Result + DaysPerYear[dxIsSpreadSheetLeapYear(I)];
    Result := Result / (AYearEnd - AYearStart + 1);
  end;

  function GetDaysPerYears(AStartDate, AEndDate: TDate): Real;

    function IsPeriodHasLeapDay: Boolean;
    var
      AYearStart, AYearEnd: Word;
    begin
      AYearStart := YearOf(AStartDate);
      AYearEnd := YearOf(AEndDate);
      Result := dxIsSpreadSheetLeapYear(AYearStart) and dxIsSpreadSheetLeapYear(AYearEnd);
      if not Result then
        if dxIsSpreadSheetLeapYear(AYearStart) then
          Result := (EncodeDate(AYearStart, 3, 1) > AStartDate) and (AEndDate > (EncodeDate(AYearStart, 2, 28)))
        else
          if dxIsSpreadSheetLeapYear(AYearEnd) then
            Result := (EncodeDate(AYearEnd, 3, 1) > AStartDate) and (AEndDate > (EncodeDate(AYearEnd, 2, 28)))
    end;

  begin
    if IncYear(AStartDate, 1) >= AEndDate then
      Result := DaysPerYear[IsPeriodHasLeapDay]
    else
      Result := GetAverageDaysPerYears(YearOf(AStartDate), YearOf(AEndDate));
  end;

const
  ADaysPerYear: array [Boolean] of Word = (360, 365);
begin
  CheckExchange(AStart, AEnd);
  Result := -1;
  case ABasis of
    0, 4:
      Result := dxGetDays360(AStart, AEnd, ABasis = 4) / 360;
    1:
      Result := (AEnd - AStart) / GetDaysPerYears(AStart, AEnd);
    2, 3:
      Result := (AEnd - AStart) / ADaysPerYear[ABasis = 3];
  end;
end;

procedure fnDate(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AYear, AMonth, ADay: Variant;
  ADate: Variant;
begin
  if Sender.ExtractNumericParameter(AYear, AParams) and Sender.ExtractNumericParameter(AMonth, AParams, 1) and
     Sender.ExtractNumericParameter(ADay, AParams, 2) then
  begin
    ADate := dxGetXLSDate(Trunc(AYear), Trunc(AMonth), Trunc(ADay));
    if ADate < 1 then
      Sender.SetError(ecNum)
    else
      Sender.AddValue(ADate);
  end;
end;

procedure fnDateValue(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractDateTimeOnlyParameter(AParameter, AParams) then
    Sender.AddValue(Trunc(AParameter));
end;

procedure fnDay(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractDateTimeParameter(AParameter, AParams) then
    if InRange(AParameter, 0, 60) then
    begin
      if AParameter < 32 then
        Sender.AddValue(AParameter)
      else
        Sender.AddValue(AParameter - 31);
    end
    else
      Sender.AddValue(DayOf(AParameter));
end;

procedure fnDays(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AEnd, AStart: Variant;
begin
  if Sender.ExtractDateTimeParameter(AEnd, AParams) and Sender.ExtractDateTimeParameter(AStart, AParams.Next) then
    Sender.AddValue(Trunc(AEnd) - Trunc(AStart));
end;

procedure fnDays360(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AStart, AEnd, AMethod: Variant;
begin
  if Sender.ExtractDateTimeParameter(AStart, AParams) and Sender.ExtractDateTimeParameter(AEnd, AParams.Next) and
     Sender.ExtractNumericParameterDef(AMethod, False, False, AParams, 2) then
    Sender.AddValue(dxGetDays360(Trunc(Integer(AStart)), Trunc(Integer(AEnd)), AMethod <> 0));
end;

function dxExtractStartDateAndMonthCount(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken;
  out ADate, AMonthCount: Variant): Boolean;
begin
  Result := False;
  if dxSpreadSheetExtractedArgumentsAreNotNull(Sender, AParams, ADate, AMonthCount) then
    Result := Sender.ExtractDateTimeParameterWithoutBoolean(ADate, AParams) and
      Sender.ExtractNumericParameterWithoutBoolean(AMonthCount, AParams.Next);
end;

procedure fnEDate(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ADate, AMonthCount: Variant;
begin
  if dxExtractStartDateAndMonthCount(Sender, AParams, ADate, AMonthCount) then
    Sender.AddValue(IncMonth(Trunc(ADate), Trunc(AMonthCount)));
end;

procedure fnEOMonth(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ADate, AMonthCount: Variant;
  AYear, AMonth, ADay: Word;
begin
  if dxExtractStartDateAndMonthCount(Sender, AParams, ADate, AMonthCount) then
  begin
    ADate := Trunc(ADate);
    AMonthCount := Trunc(AMonthCount);
    ADate := IncMonth(ADate, AMonthCount);
    DecodeDate(ADate, AYear, AMonth, ADay);
    Sender.AddValue(EncodeDate(AYear, AMonth, DaysPerMonth(AYear, AMonth)));
  end;
end;

procedure fnHour(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractDateTimeParameter(AParameter, AParams) then
    if AParameter < 0 then
      Sender.SetError(ecNUM)
    else
      Sender.AddValue(HourOf(AParameter));
end;

procedure fnIsoWeekNum(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractDateTimeParameter(AParameter, AParams) then
    Sender.AddValue(dxGetIsoWeekNum(Trunc(AParameter)));
end;

procedure fnMinute(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractDateTimeParameter(AParameter, AParams) then
    if AParameter < 0 then
      Sender.SetError(ecNUM)
    else
      Sender.AddValue(MinuteOf(AParameter));
end;

procedure fnMonth(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractDateTimeParameter(AParameter, AParams) then
    if AParameter < 0 then
      Sender.SetError(ecNUM)
    else
      if AParameter <= 2 then
        Sender.AddValue(1)
      else
        Sender.AddValue(MonthOf(AParameter));
end;

procedure fnNow(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  Sender.AddValue(Now);
end;

procedure fnSecond(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractDateTimeParameter(AParameter, AParams) then
    if AParameter < 0 then
      Sender.SetError(ecNUM)
    else
      Sender.AddValue(SecondOf(AParameter));
end;

procedure fnTime(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AHour, AMinute, ASecond: Variant;
  AResult: Variant;
begin
  if Sender.ExtractNumericParameter(AHour, AParams) and Sender.ExtractNumericParameter(AMinute, AParams.Next) and
     Sender.ExtractNumericParameter(ASecond, AParams, 2) then
  begin
    AResult := AHour * 60 * 60 + AMinute * 60 + ASecond;
    if AResult < 0  then
      Sender.SetError(ecNUM);
    Sender.AddValue(Frac(AResult / 24 / 60 / 60));
  end;
end;

procedure fnTimeValue(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractDateTimeOnlyParameter(AParameter, AParams) then
    Sender.AddValue(Frac(AParameter));
end;

procedure fnToday(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
begin
  Sender.AddValue(Date);
end;

procedure fnWeekDay(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ADate, AType: Variant;
  AIntType: Integer;
begin
  if Sender.ExtractDateTimeParameter(ADate, AParams) and Sender.ExtractNumericParameterDef(AType, 1, 0, AParams.Next) then
  begin
    ADate := Trunc(ADate);
    AIntType := Trunc(AType);
    if AIntType in [1, 2, 3, 11, 12, 13, 14, 15, 16, 17] then
      Sender.AddValue(dxGetWeekDay(ADate, AIntType))
    else
      Sender.SetError(ecNUM);
  end;
end;

procedure fnWeekNum(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  ADate, AType: Variant;
  AIntType: Integer;
begin
  if Sender.ExtractDateTimeParameterWithoutBoolean(ADate, AParams) and
     Sender.ExtractNumericParameterDefWithoutBoolean(AType, 1, AParams.Next) then
  begin
    ADate := Trunc(ADate);
    AIntType := Trunc(AType);
    if AIntType in [1, 2, 11, 12, 13, 14, 15, 16, 17, 21] then
      Sender.AddValue(dxGetWeekNum(ADate, AIntType))
    else
      Sender.SetError(ecNUM);
  end;
end;

procedure fnYear(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AParameter: Variant;
begin
  if Sender.ExtractDateTimeParameter(AParameter, AParams) then
    if AParameter < 0 then
      Sender.SetError(ecNUM)
    else
      if AParameter <= 2 then
        Sender.AddValue(1900)
      else
       Sender.AddValue(YearOf(AParameter));
end;

procedure fnYearFrac(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);
var
  AStart, AEnd, ABasis: Variant;
begin
  if Sender.ExtractDateTimeParameterWithoutBoolean(AStart, AParams) and Sender.ExtractDateTimeParameterWithoutBoolean(AEnd, AParams.Next) and
     Sender.ExtractNumericParameterDefWithoutBoolean(ABasis, 0, AParams, 2)  then
  begin
    AStart := Trunc(AStart);
    AEnd := Trunc(AEnd);
    ABasis := Trunc(ABasis);
    if (ABasis < 0) or (ABasis > 4) then
      Sender.SetError(ecNUM)
    else
      Sender.AddValue(dxGetYearFrac(AStart, AEnd, ABasis));
  end;
end;

end.
