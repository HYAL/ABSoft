{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFormatXLSX;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Types, Windows, SysUtils, Classes, dxSpreadSheetCore, dxSpreadSheetTypes, dxSpreadSheetClasses, dxCore;

const
  dxXLSXMaxColumnIndex = dxSpreadSheetMaxColumnCount - 1;
  dxXLSXMaxRowIndex = dxSpreadSheetMaxRowCount - 1;

type

  { TdxSpreadSheetXLSXFormat }

  TdxSpreadSheetXLSXFormat = class(TdxSpreadSheetCustomFormat)
  public
    class function CanReadFromStream(AStream: TStream): Boolean; override;
    class function CreateFormatSettings: TdxSpreadSheetFormatSettings; override;
    class function GetExt: string; override;
    class function GetReader: TdxSpreadSheetCustomReaderClass; override;
    class function GetWriter: TdxSpreadSheetCustomWriterClass; override;
  end;

  { TdxSpreadSheetXLSXCellRef }

  TdxSpreadSheetXLSXCellRef = class
  public
    class procedure Decode(const S: TdxUnicodeString; out AColumnIndex, ARowIndex: Integer);
    class function DecodeRange(const S: TdxUnicodeString): TRect;
    //
    class function Encode(ARowIndex, AColumnIndex: Integer): TdxUnicodeString; overload;
    class function Encode(const P: TPoint): TdxUnicodeString; overload; inline;
    class function EncodeRange(const R: TRect): TdxUnicodeString; overload;
  end;

  { TdxSpreadSheetXLSXUtils }

  TdxSpreadSheetXLSXUtils = class
  private
    const PercentsResolution = 1000;
    const PositiveFixedAngleResolution = 60000;
  private
    class function DecodeValue(AValue: Integer): Integer; inline;
    class function EncodeValue(AValue: Integer): Integer; inline;
  public
    // Color Alpha
    class function DecodeColorAlpha(const AValue: Integer): Byte; inline;
    class function EncodeColorAlpha(const AValue: Byte): Integer; inline;
    // Percents
    class function DecodePercents(const AValue: Integer): Double;
    class function EncodePercents(const AValue: Double): Integer;
    // PositiveFixedAngle
    class function DecodePositiveFixedAngle(const AValue: Integer): Integer;
    class function EncodePositiveFixedAngle(const AValue: Integer): Integer;
    // Source Rect
    class function DecodeSourceRect(const R: TRect): TRect;
    class function EncodeSourceRect(const R: TRect): TRect;

    class function GetRelsFileNameForFile(const AFileName: AnsiString): AnsiString;
  end;

implementation

uses
  AnsiStrings, dxZIPUtils, dxSpreadSheetFormatXLSXReader, dxSpreadSheetFormatXLSXWriter, cxGraphics, cxGeometry,
  dxSpreadSheetUtils;

{ TdxSpreadSheetXLSXFormat }

class function TdxSpreadSheetXLSXFormat.CanReadFromStream(AStream: TStream): Boolean;
var
  AFileName: AnsiString;
  AReader: TdxSpreadSheetXLSXReader;
  ASavedPosition: Int64;
begin
  ASavedPosition := AStream.Position;
  try
    try
      AReader := TdxSpreadSheetXLSXReader.Create(nil, AStream);
      try
        AReader.IgnoreMessages := [Low(TdxSpreadSheetMessageType)..High(TdxSpreadSheetMessageType)];
        Result := AReader.GetWorkbookFileName(AFileName);
      finally
        AReader.Free;
      end;
    except
      Result := False;
    end;
  finally
    AStream.Position := ASavedPosition;
  end;
end;

class function TdxSpreadSheetXLSXFormat.CreateFormatSettings: TdxSpreadSheetFormatSettings;
begin
  Result := TdxSpreadSheetFormatSettings.Create;
end;

class function TdxSpreadSheetXLSXFormat.GetExt: string;
begin
  Result := '.xlsx';
end;

class function TdxSpreadSheetXLSXFormat.GetReader: TdxSpreadSheetCustomReaderClass;
begin
  Result := TdxSpreadSheetXLSXReader;
end;

class function TdxSpreadSheetXLSXFormat.GetWriter: TdxSpreadSheetCustomWriterClass;
begin
  Result := TdxSpreadSheetXLSXWriter;
end;

{ TdxSpreadSheetXLSXCellRef }

class procedure TdxSpreadSheetXLSXCellRef.Decode(const S: TdxUnicodeString; out AColumnIndex, ARowIndex: Integer);

  function FindFirstNumber(const S: TdxUnicodeString): Integer;
  var
    I: Integer;
  begin
    Result := 0;
    for I := 1 to Length(S) do
      if dxCharInSet(Char(S[I]), ['0'..'9']) then
      begin;
        Result := I;
        Break;
      end;
  end;

var
  APos: Integer;
begin
  APos := FindFirstNumber(S);
  if APos > 0 then
    AColumnIndex := TdxSpreadSheetColumnHelper.IndexByName(Copy(S, 1, APos - 1))
  else
    AColumnIndex := -1;

  ARowIndex := StrToIntDef(dxUnicodeStringToString(Copy(S, APos, MaxInt)), 0) - 1;
end;

class function TdxSpreadSheetXLSXCellRef.DecodeRange(const S: TdxUnicodeString): TRect;
var
  APos: Integer;
begin
  APos := Pos(TdxUnicodeString(':'), S);
  if APos > 0 then
  begin
    Decode(Copy(S, 1, APos - 1), Result.Left, Result.Top);
    Decode(Copy(S, APos + 1, MaxInt), Result.Right, Result.Bottom);
  end
  else
  begin
    Decode(S, Result.Left, Result.Top);
    Result.BottomRight := Result.TopLeft;
  end;
end;

class function TdxSpreadSheetXLSXCellRef.Encode(ARowIndex, AColumnIndex: Integer): TdxUnicodeString;
begin
  Result := TdxSpreadSheetColumnHelper.NameByIndex(AColumnIndex) + IntToStr(ARowIndex + 1);
end;

class function TdxSpreadSheetXLSXCellRef.Encode(const P: TPoint): TdxUnicodeString;
begin
  Result := Encode(P.Y, P.X);
end;

class function TdxSpreadSheetXLSXCellRef.EncodeRange(const R: TRect): TdxUnicodeString;
begin
  Result := Encode(R.TopLeft) + ':' + Encode(R.BottomRight);
end;

{ TdxSpreadSheetXLSXUtils }

class function TdxSpreadSheetXLSXUtils.DecodeColorAlpha(const AValue: Integer): Byte;
begin
  Result := MulDiv(AValue, MaxByte, 100 * PercentsResolution)
end;

class function TdxSpreadSheetXLSXUtils.EncodeColorAlpha(const AValue: Byte): Integer;
begin
  Result := MulDiv(AValue, 100 * PercentsResolution, MaxByte);
end;

class function TdxSpreadSheetXLSXUtils.DecodePercents(const AValue: Integer): Double;
begin
  Result := AValue / PercentsResolution;
end;

class function TdxSpreadSheetXLSXUtils.EncodePercents(const AValue: Double): Integer;
begin
  Result := Trunc(AValue * PercentsResolution);
end;

class function TdxSpreadSheetXLSXUtils.DecodePositiveFixedAngle(const AValue: Integer): Integer;
begin
  Result := (AValue div PositiveFixedAngleResolution) mod 360;
end;

class function TdxSpreadSheetXLSXUtils.EncodePositiveFixedAngle(const AValue: Integer): Integer;
begin
  Result := AValue mod 360;
  if Result < 0 then
    Inc(Result, 360);
  Result := Result * PositiveFixedAngleResolution;
end;

class function TdxSpreadSheetXLSXUtils.DecodeSourceRect(const R: TRect): TRect;
begin
  Result := cxRect(DecodeValue(R.Left), DecodeValue(R.Top), DecodeValue(R.Right), DecodeValue(R.Bottom));
end;

class function TdxSpreadSheetXLSXUtils.DecodeValue(AValue: Integer): Integer;
begin
  Result := MulDiv(AValue, cxGetCurrentDPI, 21333);
end;

class function TdxSpreadSheetXLSXUtils.EncodeSourceRect(const R: TRect): TRect;
begin
  Result := cxRect(EncodeValue(R.Left), EncodeValue(R.Top), EncodeValue(R.Right), EncodeValue(R.Bottom));
end;

class function TdxSpreadSheetXLSXUtils.EncodeValue(AValue: Integer): Integer;
begin
  Result := MulDiv(AValue, 21333, cxGetCurrentDPI);
end;

class function TdxSpreadSheetXLSXUtils.GetRelsFileNameForFile(const AFileName: AnsiString): AnsiString;
begin
  Result := TdxZIPPathHelper.DecodePath(AFileName);
  Result := ExtractFilePath(Result) + '_rels' + PathDelim + ExtractFileName(Result) + '.rels';
  Result := TdxZIPPathHelper.EncodePath(Result);
end;

initialization
  TdxSpreadSheetXLSXFormat.Register;

finalization
  TdxSpreadSheetXLSXFormat.Unregister;
end.
