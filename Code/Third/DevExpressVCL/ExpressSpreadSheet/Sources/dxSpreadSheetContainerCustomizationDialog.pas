{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetContainerCustomizationDialog;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, Menus, StdCtrls, ActnList,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxLayoutContainer, dxLayoutControl, cxClasses,
  dxLayoutLookAndFeels, dxLayoutControlAdapters, cxButtons, dxSpreadSheetCore, cxRadioGroup, dxColorDialog,
  dxCoreGraphics, dxGDIPlusClasses, dxLayoutcxEditAdapters, cxContainer, cxEdit, cxImage, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxImageComboBox, ExtCtrls, dxSpreadSheetContainerCustomizationDialogHelpers, cxGroupBox, cxCalc,
  cxLabel, cxSpinEdit, cxCheckBox;

type

  { TdxSpreadSheetContainerCustomizationDialogForm }

  TdxSpreadSheetContainerCustomizationDialogForm = class(TForm)
    acTextureFillLoad: TAction;
    acTextureFillSave: TAction;
    ActionList1: TActionList;
    btnCancel: TcxButton;
    btnGradientFillAddStop: TcxButton;
    btnGradientFillColor: TcxButton;
    btnGradientFillRemoveStop: TcxButton;
    btnGradientLineAddStop: TcxButton;
    btnGradientLineColor: TcxButton;
    btnGradientLineRemoveStop: TcxButton;
    btnOK: TcxButton;
    btnReset: TcxButton;
    btnSolidFillColor: TcxButton;
    btnSolidLineColor: TcxButton;
    btnTextureFillLoad: TcxButton;
    btnTextureFillSave: TcxButton;
    cbLockAspectRatio: TcxCheckBox;
    cbRelativeToPictureSize: TcxCheckBox;
    ccbGradientFillDirection: TcxComboBox;
    ccbGradientLineDirection: TcxComboBox;
    ccbLineStyle: TcxComboBox;
    ceLineWidth: TcxSpinEdit;
    ColorDialog: TdxColorDialog;
    dxLayoutCxLookAndFeel1: TdxLayoutCxLookAndFeel;
    dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList;
    imTextureFill: TcxImage;
    lbCrop: TcxLabel;
    lbOriginalSize: TcxLabel;
    lbPosition: TcxLabel;
    lbScale: TcxLabel;
    lbSizeAndRotate: TcxLabel;
    lcgCrop: TdxLayoutGroup;
    lcgFill: TdxLayoutGroup;
    lcgGradientFill: TdxLayoutGroup;
    lcgGradientLine: TdxLayoutGroup;
    lcgLine: TdxLayoutGroup;
    lcgOriginalSize: TdxLayoutGroup;
    lcgProperties: TdxLayoutGroup;
    lcgSize: TdxLayoutGroup;
    lcgSolidFill: TdxLayoutItem;
    lcgSolidLine: TdxLayoutItem;
    lcgTextureFill: TdxLayoutAutoCreatedGroup;
    lciCrop: TdxLayoutItem;
    lciCropLeft: TdxLayoutItem;
    lciGradientFillDirection: TdxLayoutItem;
    lciGradientFillStops: TdxLayoutItem;
    lciGradientLineDirection: TdxLayoutItem;
    lciGradientLineStops: TdxLayoutItem;
    lciHeight: TdxLayoutItem;
    lciLineStyle: TdxLayoutItem;
    lciLineWidth: TdxLayoutItem;
    lciOriginalSize: TdxLayoutItem;
    lciRelativeToPictureSize: TdxLayoutItem;
    lciRotation: TdxLayoutItem;
    lciScaleHeight: TdxLayoutItem;
    lciScaleWidth: TdxLayoutItem;
    lciWidth: TdxLayoutItem;
    lclOriginalSize: TdxLayoutLabeledItem;
    lcMain: TdxLayoutControl;
    lcMainGroup_Root: TdxLayoutGroup;
    lcMainGroup1: TdxLayoutGroup;
    lcMainGroup10: TdxLayoutAutoCreatedGroup;
    lcMainGroup11: TdxLayoutGroup;
    lcMainGroup12: TdxLayoutAutoCreatedGroup;
    lcMainGroup13: TdxLayoutGroup;
    lcMainGroup14: TdxLayoutAutoCreatedGroup;
    lcMainGroup15: TdxLayoutAutoCreatedGroup;
    lcMainGroup2: TdxLayoutAutoCreatedGroup;
    lcMainGroup3: TdxLayoutGroup;
    lcMainGroup4: TdxLayoutAutoCreatedGroup;
    lcMainGroup5: TdxLayoutAutoCreatedGroup;
    lcMainGroup6: TdxLayoutAutoCreatedGroup;
    lcMainGroup7: TdxLayoutGroup;
    lcMainGroup8: TdxLayoutGroup;
    lcMainGroup9: TdxLayoutGroup;
    lcMainItem1: TdxLayoutItem;
    lcMainItem10: TdxLayoutItem;
    lcMainItem11: TdxLayoutItem;
    lcMainItem12: TdxLayoutItem;
    lcMainItem13: TdxLayoutItem;
    lcMainItem14: TdxLayoutItem;
    lcMainItem15: TdxLayoutItem;
    lcMainItem16: TdxLayoutItem;
    lcMainItem17: TdxLayoutItem;
    lcMainItem18: TdxLayoutItem;
    lcMainItem19: TdxLayoutItem;
    lcMainItem2: TdxLayoutItem;
    lcMainItem20: TdxLayoutItem;
    lcMainItem21: TdxLayoutItem;
    lcMainItem22: TdxLayoutItem;
    lcMainItem23: TdxLayoutItem;
    lcMainItem24: TdxLayoutItem;
    lcMainItem25: TdxLayoutItem;
    lciCropTop: TdxLayoutItem;
    lcMainItem27: TdxLayoutItem;
    lciCropRight: TdxLayoutItem;
    lciCropBottom: TdxLayoutItem;
    lcMainItem3: TdxLayoutItem;
    lcMainItem4: TdxLayoutItem;
    lcMainItem5: TdxLayoutItem;
    lcMainItem6: TdxLayoutItem;
    lcMainItem7: TdxLayoutItem;
    lcMainItem8: TdxLayoutItem;
    lcMainItem9: TdxLayoutItem;
    lcMainSeparatorItem1: TdxLayoutSeparatorItem;
    lcMainSeparatorItem2: TdxLayoutSeparatorItem;
    rbAbsolute: TcxRadioButton;
    rbGradientFill: TcxRadioButton;
    rbGradientLine: TcxRadioButton;
    rbNoFill: TcxRadioButton;
    rbNoLine: TcxRadioButton;
    rbOneCell: TcxRadioButton;
    rbSolidFill: TcxRadioButton;
    rbSolidLine: TcxRadioButton;
    rbTextureFill: TcxRadioButton;
    rbTwoCells: TcxRadioButton;
    seCropBottom: TcxSpinEdit;
    seCropLeft: TcxSpinEdit;
    seCropRight: TcxSpinEdit;
    seCropTop: TcxSpinEdit;
    seHeight: TcxSpinEdit;
    seRotation: TcxSpinEdit;
    seScaleHeight: TcxSpinEdit;
    seScaleWidth: TcxSpinEdit;
    seWidth: TcxSpinEdit;
    TextureOpenDialog: TOpenDialog;
    TextureSaveDialog: TSaveDialog;

    procedure acTextureFillLoadExecute(Sender: TObject);
    procedure acTextureFillSaveExecute(Sender: TObject);
    procedure acTextureFillSaveUpdate(Sender: TObject);
    procedure btnGradientFillAddStopClick(Sender: TObject);
    procedure btnGradientFillColorClick(Sender: TObject);
    procedure btnGradientFillRemoveStopClick(Sender: TObject);
    procedure btnGradientLineAddStopClick(Sender: TObject);
    procedure btnGradientLineColorClick(Sender: TObject);
    procedure btnGradientLineRemoveStopClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnSolidFillColorClick(Sender: TObject);
    procedure btnSolidLineColorClick(Sender: TObject);
    procedure cbLockAspectRatioClick(Sender: TObject);
    procedure cbRelativeToPictureSizeClick(Sender: TObject);
    procedure rbGradientLineClick(Sender: TObject);
    procedure rbTextureFillClick(Sender: TObject);
    procedure seCropHorzPropertiesChange(Sender: TObject);
    procedure seCropVertPropertiesChange(Sender: TObject);
    procedure seHeightPropertiesChange(Sender: TObject);
    procedure seScaleHeightPropertiesChange(Sender: TObject);
    procedure seScaleWidthPropertiesChange(Sender: TObject);
    procedure seWidthPropertiesChange(Sender: TObject);
  strict private
    FContainer: TdxSpreadSheetContainer;
    FGradientFillStops: TdxSpreadSheetGradientStops;
    FGradientLineStops: TdxSpreadSheetGradientStops;
    FPrevCropMargins: TRect;
    FScaleDenominator: Integer;
    FScaleNumerator: Integer;

    procedure ApplyLocalization;

    procedure GradientFillStopsDblClickHandler(Sender: TObject);
    procedure GradientFillStopsSelectionChangedHandler(Sender: TObject);
    procedure GradientLineStopsDblClickHandler(Sender: TObject);
    procedure GradientLineStopsSelectionChangedHandler(Sender: TObject);

    function GetContainerAsPicture: TdxSpreadSheetPictureContainer;
    function GetContainerAsShape: TdxSpreadSheetShapeContainer;
    function GetCropMargins: TRect;
    procedure SetCropMargins(const Value: TRect);

    procedure LoadShape(AShape: TdxSpreadSheetShape);
    procedure LoadShapeBrush(ABrush: TdxGPBrush);
    procedure LoadShapePen(APen: TdxGPPen);
    procedure LoadSizeParams;
    procedure SaveShape(AShape: TdxSpreadSheetShape);
    procedure SaveShapeBrush(ABrush: TdxGPBrush);
    procedure SaveShapePen(APen: TdxGPPen);
    procedure SaveSizeParams;

    procedure PopulateGradientDirection(ACombobox: TcxComboBox);
    procedure PopulatePenStyle(ACombobox: TcxComboBox);
    procedure SelectColor(AButton: TcxButton);
    procedure SelectGradientStopColor(AStops: TdxSpreadSheetGradientStops; AButton: TcxButton);

    procedure UpdateColorButtonGlyph(AButton: TcxButton); overload;
    procedure UpdateColorButtonGlyph(AButton: TcxButton; AColor: TdxAlphaColor); overload;
    procedure UpdateOriginalSizeInfo;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Initialize(AContainer: TdxSpreadSheetContainer);
    procedure Load;
    procedure Save;
    //
    property Container: TdxSpreadSheetContainer read FContainer;
    property ContainerAsPicture: TdxSpreadSheetPictureContainer read GetContainerAsPicture;
    property ContainerAsShape: TdxSpreadSheetShapeContainer read GetContainerAsShape;
    property CropMargins: TRect read GetCropMargins write SetCropMargins;
    property GradientFillStops: TdxSpreadSheetGradientStops read FGradientFillStops;
    property GradientLineStops: TdxSpreadSheetGradientStops read FGradientLineStops;
  end;

procedure ShowContainerCustomizationDialog(AContainer: TdxSpreadSheetContainer);
implementation

uses
  Types, dxSpreadSheetDialogStrs, Math, dxTypeHelpers, cxGeometry;

{$R *.dfm}

type
  TdxSpreadSheetContainerAccess = class(TdxSpreadSheetContainer);

procedure ShowContainerCustomizationDialog(AContainer: TdxSpreadSheetContainer);
var
  ADialog: TdxSpreadSheetContainerCustomizationDialogForm;
begin
  ADialog := TdxSpreadSheetContainerCustomizationDialogForm.Create(GetParentForm(AContainer.SpreadSheet));
  try
    ADialog.Initialize(AContainer);
    ADialog.Load;
    if ADialog.ShowModal = mrOk then
      ADialog.Save;
  finally
    ADialog.Free;
  end;
end;

{ TdxSpreadSheetContainerCustomizationDialogForm }

constructor TdxSpreadSheetContainerCustomizationDialogForm.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FGradientFillStops := TdxSpreadSheetGradientStops.Create(lcMain);
  FGradientFillStops.OnSelectionChanged := GradientFillStopsSelectionChangedHandler;
  FGradientFillStops.OnDblClick := GradientFillStopsDblClickHandler;
  lciGradientFillStops.Control := GradientFillStops;
  FGradientFillStops.Height := 20;

  FGradientLineStops := TdxSpreadSheetGradientStops.Create(lcMain);
  FGradientLineStops.OnSelectionChanged := GradientLineStopsSelectionChangedHandler;
  FGradientLineStops.OnDblClick := GradientLineStopsDblClickHandler;
  lciGradientLineStops.Control := GradientLineStops;
  FGradientLineStops.Height := 20;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.Initialize(AContainer: TdxSpreadSheetContainer);
begin
  FContainer := AContainer;
  lciRelativeToPictureSize.Visible := ContainerAsPicture <> nil;
  lciCrop.Visible := ContainerAsPicture <> nil;
  lcgCrop.Visible := ContainerAsPicture <> nil;
  lcgFill.Visible := ContainerAsShape <> nil;
  lcgLine.Visible := ContainerAsShape <> nil;
  lcgOriginalSize.Visible := ContainerAsPicture <> nil;
  lciOriginalSize.Visible := ContainerAsPicture <> nil;

  UpdateColorButtonGlyph(btnGradientLineColor);
  UpdateColorButtonGlyph(btnGradientFillColor);
  UpdateColorButtonGlyph(btnSolidFillColor);
  UpdateColorButtonGlyph(btnSolidLineColor);
  ApplyLocalization;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.Load;
begin
  LoadSizeParams;
  if ContainerAsShape <> nil then
    LoadShape(ContainerAsShape.Shape);
  UpdateOriginalSizeInfo;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.Save;
begin
  SaveSizeParams;
  if ContainerAsShape <> nil then
    SaveShape(ContainerAsShape.Shape);
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.ApplyLocalization;
begin
  Caption := cxGetResourceString(@sdxContainerCustomizationDialogCaption);
  btnOK.Caption := cxGetResourceString(@sdxContainerCustomizationDialogButtonOK);
  btnCancel.Caption := cxGetResourceString(@sdxContainerCustomizationDialogButtonCancel);

  // Fill
  acTextureFillLoad.Caption := cxGetResourceString(@sdxContainerCustomizationDialogButtonLoad);
  acTextureFillSave.Caption := cxGetResourceString(@sdxContainerCustomizationDialogButtonSave);
  btnGradientFillAddStop.Caption := cxGetResourceString(@sdxContainerCustomizationDialogButtonAdd);
  btnGradientFillColor.Caption := cxGetResourceString(@sdxContainerCustomizationDialogButtonColor);
  btnGradientFillRemoveStop.Caption := cxGetResourceString(@sdxContainerCustomizationDialogButtonRemove);
  btnSolidFillColor.Caption := cxGetResourceString(@sdxContainerCustomizationDialogButtonColor);
  lcgFill.Caption := cxGetResourceString(@sdxContainerCustomizationDialogGroupFill);
  lciGradientFillDirection.Caption := cxGetResourceString(@sdxContainerCustomizationDialogDirection);
  lciGradientFillStops.Caption := cxGetResourceString(@sdxContainerCustomizationDialogStops);
  rbGradientFill.Caption := cxGetResourceString(@sdxContainerCustomizationDialogGradientFill);
  rbNoFill.Caption := cxGetResourceString(@sdxContainerCustomizationDialogNoFill);
  rbSolidFill.Caption := cxGetResourceString(@sdxContainerCustomizationDialogSolidFill);
  rbTextureFill.Caption := cxGetResourceString(@sdxContainerCustomizationDialogTextureFill);
  PopulateGradientDirection(ccbGradientFillDirection);

  // Line
  lcgLine.Caption := cxGetResourceString(@sdxContainerCustomizationDialogLine);
  lciLineStyle.Caption := cxGetResourceString(@sdxContainerCustomizationDialogLineStyle);
  lciLineWidth.Caption := cxGetResourceString(@sdxContainerCustomizationDialogLineWidth);
  rbGradientLine.Caption := cxGetResourceString(@sdxContainerCustomizationDialogGradientLine);
  rbNoLine.Caption := cxGetResourceString(@sdxContainerCustomizationDialogNoLine);
  rbSolidLine.Caption := cxGetResourceString(@sdxContainerCustomizationDialogSolidLine);
  btnGradientLineAddStop.Caption := cxGetResourceString(@sdxContainerCustomizationDialogButtonAdd);
  btnGradientLineColor.Caption := cxGetResourceString(@sdxContainerCustomizationDialogButtonColor);
  btnGradientLineRemoveStop.Caption := cxGetResourceString(@sdxContainerCustomizationDialogButtonRemove);
  btnSolidLineColor.Caption := cxGetResourceString(@sdxContainerCustomizationDialogButtonColor);
  lciGradientLineDirection.Caption := cxGetResourceString(@sdxContainerCustomizationDialogDirection);
  lciGradientLineStops.Caption := cxGetResourceString(@sdxContainerCustomizationDialogStops);
  PopulateGradientDirection(ccbGradientLineDirection);
  PopulatePenStyle(ccbLineStyle);

  // Properties
  lcgProperties.Caption := cxGetResourceString(@sdxContainerCustomizationDialogGroupProperties);
  lbPosition.Caption := cxGetResourceString(@sdxContainerCustomizationDialogPositioning);
  rbAbsolute.Caption := cxGetResourceString(@sdxContainerCustomizationDialogAbsolute);
  rbOneCell.Caption := cxGetResourceString(@sdxContainerCustomizationDialogOneCells);
  rbTwoCells.Caption := cxGetResourceString(@sdxContainerCustomizationDialogTwoCells);

  // Size
  cbLockAspectRatio.Caption := cxGetResourceString(@sdxContainerCustomizationDialogLockAspectRatio);
  cbRelativeToPictureSize.Caption := cxGetResourceString(@sdxContainerCustomizationDialogRelativeToPictureSize);
  lbCrop.Caption := cxGetResourceString(@sdxContainerCustomizationDialogCropFrom);
  lbScale.Caption := cxGetResourceString(@sdxContainerCustomizationDialogScale);
  lbSizeAndRotate.Caption := cxGetResourceString(@sdxContainerCustomizationDialogSizeAndRotate);
  lcgSize.Caption := cxGetResourceString(@sdxContainerCustomizationDialogGroupSize);
  lciCropBottom.Caption := cxGetResourceString(@sdxContainerCustomizationDialogCropBottom);
  lciCropTop.Caption := cxGetResourceString(@sdxContainerCustomizationDialogCropTop);
  lciCropLeft.Caption := cxGetResourceString(@sdxContainerCustomizationDialogCropLeft);
  lciCropRight.Caption := cxGetResourceString(@sdxContainerCustomizationDialogCropRight);
  lciHeight.Caption := cxGetResourceString(@sdxContainerCustomizationDialogHeight);
  lciRotation.Caption := cxGetResourceString(@sdxContainerCustomizationDialogRotation);
  lciScaleHeight.Caption := cxGetResourceString(@sdxContainerCustomizationDialogScaleHeight);
  lciScaleWidth.Caption := cxGetResourceString(@sdxContainerCustomizationDialogScaleWidth);
  lciWidth.Caption := cxGetResourceString(@sdxContainerCustomizationDialogWidth);
  lbOriginalSize.Caption := cxGetResourceString(@sdxContainerCustomizationDialogOriginalSize);
  btnReset.Caption := cxGetResourceString(@sdxContainerCustomizationDialogReset);
  UpdateOriginalSizeInfo;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.GradientFillStopsDblClickHandler(Sender: TObject);
begin
  if btnGradientFillColor.Enabled then
    btnGradientFillColor.Click;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.GradientFillStopsSelectionChangedHandler(Sender: TObject);
begin
  btnGradientFillColor.Tag := GradientFillStops.SelectedStopColor;
  btnGradientFillColor.Enabled := GradientFillStops.SelectedStopIndex >= 0;
  btnGradientFillRemoveStop.Enabled := GradientFillStops.SelectedStopIndex >= 0;
  UpdateColorButtonGlyph(btnGradientFillColor);
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.GradientLineStopsDblClickHandler(Sender: TObject);
begin
  if btnGradientLineColor.Enabled then
    btnGradientLineColor.Click;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.GradientLineStopsSelectionChangedHandler(Sender: TObject);
begin
  btnGradientLineColor.Tag := GradientLineStops.SelectedStopColor;
  btnGradientLineColor.Enabled := GradientLineStops.SelectedStopIndex >= 0;
  btnGradientLineRemoveStop.Enabled := GradientLineStops.SelectedStopIndex >= 0;
  UpdateColorButtonGlyph(btnGradientLineColor);
end;

function TdxSpreadSheetContainerCustomizationDialogForm.GetContainerAsPicture: TdxSpreadSheetPictureContainer;
begin
  if Container is TdxSpreadSheetPictureContainer then
    Result := TdxSpreadSheetPictureContainer(Container)
  else
    Result := nil;
end;

function TdxSpreadSheetContainerCustomizationDialogForm.GetContainerAsShape: TdxSpreadSheetShapeContainer;
begin
  if Container is TdxSpreadSheetShapeContainer then
    Result := TdxSpreadSheetShapeContainer(Container)
  else
    Result := nil;
end;

function TdxSpreadSheetContainerCustomizationDialogForm.GetCropMargins: TRect;
begin
  Result := cxRect(seCropLeft.Value, seCropTop.Value, seCropRight.Value, seCropBottom.Value);
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.LoadShape(AShape: TdxSpreadSheetShape);
begin
  LoadShapeBrush(AShape.Brush);
  LoadShapePen(AShape.Pen);
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.LoadShapeBrush(ABrush: TdxGPBrush);
begin
  rbGradientFill.Checked := ABrush.Style = gpbsGradient;
  rbNoFill.Checked := ABrush.Style = gpbsClear;
  rbSolidFill.Checked := ABrush.Style = gpbsSolid;
  rbTextureFill.Checked := ABrush.Style = gpbsTexture;

  if rbSolidFill.Checked then
  begin
    btnSolidFillColor.Tag := ABrush.Color;
    UpdateColorButtonGlyph(btnSolidFillColor);
  end
  else

  if rbTextureFill.Checked then
    imTextureFill.Picture.Graphic := ABrush.Texture
  else

  if rbGradientFill.Checked then
  begin
    ccbGradientFillDirection.ItemObject := TObject(ABrush.GradientMode);
    GradientFillStops.Brush.GradientPoints.Assign(ABrush.GradientPoints);
  end;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.LoadShapePen(APen: TdxGPPen);
begin
  ceLineWidth.Value := APen.Width;
  ccbLineStyle.ItemObject := TObject(APen.Style);

  rbSolidLine.Checked := True;
  rbNoLine.Checked := APen.Brush.Style = gpbsClear;
  rbGradientLine.Checked := APen.Brush.Style = gpbsGradient;

  if rbSolidLine.Checked then
  begin
    btnSolidLineColor.Tag := APen.Brush.Color;
    UpdateColorButtonGlyph(btnSolidLineColor);
  end
  else

  if rbGradientLine.Checked then
  begin
    ccbGradientLineDirection.ItemObject := TObject(APen.Brush.GradientMode);
    GradientLineStops.Brush.GradientPoints.Assign(APen.Brush.GradientPoints);
  end;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.LoadSizeParams;
var
  ARect: TRect;
begin
  rbAbsolute.Checked := Container.AnchorType = catAbsolute;
  rbOneCell.Checked := Container.AnchorType = catOneCell;
  rbTwoCells.Checked := Container.AnchorType = catTwoCell;
  seRotation.Value := Container.Transform.RotationAngle;

  ARect := TdxSpreadSheetContainerAccess(Container).Calculator.CalculateBounds;
  seHeight.Value := ARect.Height;
  seWidth.Value := ARect.Width;
  seHeight.Tag := ARect.Height;
  seWidth.Tag := ARect.Width;

  if ContainerAsPicture <> nil then
  begin
    CropMargins := ContainerAsPicture.Picture.CropMargins;
    seHeight.Tag := seHeight.Tag + cxMarginsHeight(CropMargins);
    seWidth.Tag := seWidth.Tag + cxMarginsWidth(CropMargins);
  end;

  seScaleHeight.Value := 100;
  seScaleWidth.Value := 100;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.SaveShape(AShape: TdxSpreadSheetShape);
begin
  SaveShapeBrush(AShape.Brush);
  SaveShapePen(AShape.Pen);
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.SaveShapeBrush(ABrush: TdxGPBrush);
begin
  if rbSolidFill.Checked then
  begin
    ABrush.Color := btnSolidFillColor.Tag;
    ABrush.Style := gpbsSolid;
  end
  else

  if rbTextureFill.Checked and (imTextureFill.Picture.Graphic <> nil) then
  begin
    ABrush.Texture.Assign(imTextureFill.Picture.Graphic);
    ABrush.Style := gpbsTexture;
  end
  else

  if rbGradientFill.Checked and (GradientFillStops.Brush.GradientPoints.Count > 0) then
  begin
    ABrush.Style := gpbsGradient;
    ABrush.GradientPoints.Assign(GradientFillStops.Brush.GradientPoints);
    ABrush.GradientMode := TdxGPBrushGradientMode(ccbGradientFillDirection.ItemObject);
  end
  else
    ABrush.Style := gpbsClear
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.SaveShapePen(APen: TdxGPPen);
begin
  APen.Width := ceLineWidth.Value;
  APen.Style := TdxGPPenStyle(ccbLineStyle.ItemObject);

  if rbSolidLine.Checked then
  begin
    APen.Brush.Color := btnSolidLineColor.Tag;
    APen.Brush.Style := gpbsSolid;
  end
  else

  if rbGradientLine.Checked and (GradientLineStops.Brush.GradientPoints.Count > 0) then
  begin
    APen.Brush.Style := gpbsGradient;
    APen.Brush.GradientPoints.Assign(GradientLineStops.Brush.GradientPoints);
    APen.Brush.GradientMode := TdxGPBrushGradientMode(ccbGradientLineDirection.ItemObject);
  end
  else
    APen.Brush.Style := gpbsClear;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.SaveSizeParams;
var
  ARect: TRect;
begin
  if rbAbsolute.Checked then
    Container.AnchorType := catAbsolute;
  if rbOneCell.Checked then
    Container.AnchorType := catOneCell;
  if rbTwoCells.Checked then
    Container.AnchorType := catTwoCell;
  if ContainerAsPicture <> nil then
    ContainerAsPicture.Picture.CropMargins := CropMargins;

  ARect := TdxSpreadSheetContainerAccess(Container).Calculator.CalculateBounds;
  if (ARect.Width <> seWidth.Value) or (ARect.Height <> seHeight.Value) then
  begin
    ARect.Height := seHeight.Value;
    ARect.Width := seWidth.Value;
    TdxSpreadSheetContainerAccess(Container).Calculator.UpdateAnchors(ARect);
  end;
  Container.Transform.RotationAngle := seRotation.Value;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.PopulateGradientDirection(ACombobox: TcxComboBox);
var
  AMode: TdxGPBrushGradientMode;
  ASavedIndex: Integer;
begin
  ACombobox.Properties.Items.BeginUpdate;
  try
    ASavedIndex := ACombobox.ItemIndex;
    ACombobox.Properties.Items.Clear;
    for AMode := Low(AMode) to High(AMode) do
      ACombobox.Properties.Items.AddObject(cxGetResourceString(dxGradientModeNames[AMode]), TObject(AMode));
    ACombobox.ItemIndex := Max(ASavedIndex, 0);
  finally
    ACombobox.Properties.Items.EndUpdate;
  end;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.PopulatePenStyle(ACombobox: TcxComboBox);
var
  AStyle: TdxGPPenStyle;
  ASavedIndex: Integer;
begin
  ACombobox.Properties.Items.BeginUpdate;
  try
    ASavedIndex := ACombobox.ItemIndex;
    ACombobox.Properties.Items.Clear;
    for AStyle := Low(AStyle) to High(AStyle) do
      ACombobox.Properties.Items.AddObject(cxGetResourceString(dxPenStyleNames[AStyle]), TObject(AStyle));
    ACombobox.ItemIndex := Max(ASavedIndex, 0);
  finally
    ACombobox.Properties.Items.EndUpdate;
  end;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.SelectColor(AButton: TcxButton);
begin
  ColorDialog.Color := AButton.Tag;
  if ColorDialog.Execute(Handle) then
  begin
    AButton.Tag := ColorDialog.Color;
    UpdateColorButtonGlyph(AButton);
  end;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.SelectGradientStopColor(
  AStops: TdxSpreadSheetGradientStops; AButton: TcxButton);
begin
  ColorDialog.Color := AButton.Tag;
  if ColorDialog.Execute(Handle) then
  begin
    AStops.SelectedStopColor := ColorDialog.Color;
    AButton.Tag := ColorDialog.Color;
    UpdateColorButtonGlyph(AButton);
  end;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.SetCropMargins(const Value: TRect);
begin
  seCropBottom.Value := Value.Bottom;
  seCropLeft.Value := Value.Left;
  seCropRight.Value := Value.Right;
  seCropTop.Value := Value.Top;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.seCropHorzPropertiesChange(Sender: TObject);
begin
  if (Sender as TcxSpinEdit).Focused then
    seWidth.Value := seWidth.Value + cxMarginsWidth(FPrevCropMargins) - cxMarginsWidth(CropMargins);
  FPrevCropMargins := CropMargins;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.seCropVertPropertiesChange(Sender: TObject);
begin
  if (Sender as TcxSpinEdit).Focused then
    seHeight.Value := seHeight.Value + cxMarginsHeight(FPrevCropMargins) - cxMarginsHeight(CropMargins);
  FPrevCropMargins := CropMargins;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.seHeightPropertiesChange(Sender: TObject);
var
  AMarginHeight: Integer;
begin
  AMarginHeight := cxMarginsHeight(CropMargins);
  if cbLockAspectRatio.Checked then
    seWidth.Value := MulDiv(seHeight.Value + AMarginHeight, FScaleNumerator, FScaleDenominator) - cxMarginsWidth(CropMargins);
  if not seScaleHeight.Focused then
    seScaleHeight.Value := MulDiv(100, seHeight.Value + AMarginHeight, seHeight.Tag);
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.seWidthPropertiesChange(Sender: TObject);
var
  AMarginWidth: Integer;
begin
  AMarginWidth := cxMarginsWidth(CropMargins);
  if cbLockAspectRatio.Checked then
    seHeight.Value := MulDiv(seWidth.Value + AMarginWidth, FScaleDenominator, FScaleNumerator) - cxMarginsHeight(CropMargins);
  if not seScaleWidth.Focused then
    seScaleWidth.Value := MulDiv(100, seWidth.Value + AMarginWidth, seWidth.Tag);
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.seScaleHeightPropertiesChange(Sender: TObject);
begin
  if seScaleHeight.Focused then
    seHeight.Value := MulDiv(seHeight.Tag, seScaleHeight.Value, 100) - cxMarginsHeight(CropMargins);
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.seScaleWidthPropertiesChange(Sender: TObject);
begin
  if seScaleWidth.Focused then
    seWidth.Value := MulDiv(seWidth.Tag, seScaleWidth.Value, 100) - cxMarginsWidth(CropMargins);
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.UpdateColorButtonGlyph(AButton: TcxButton);
begin
  UpdateColorButtonGlyph(AButton, AButton.Tag);
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.UpdateColorButtonGlyph(AButton: TcxButton; AColor: TdxAlphaColor);
var
  ABitmap32: TcxBitmap32;
begin
  ABitmap32 := TcxBitmap32.CreateSize(18, 18, False);
  try
    cxDrawTransparencyCheckerboard(ABitmap32.cxCanvas, ABitmap32.ClientRect, 4);
    dxGPPaintCanvas.BeginPaint(ABitmap32.Canvas.Handle, ABitmap32.ClientRect);
    try
      dxGPPaintCanvas.Rectangle(ABitmap32.ClientRect, dxColorToAlphaColor(clBlack), AColor);
    finally
      dxGPPaintCanvas.EndPaint;
    end;
    ABitmap32.MakeOpaque;
    AButton.Glyph := ABitmap32;
  finally
    ABitmap32.Free;
  end;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.UpdateOriginalSizeInfo;
begin
  if ContainerAsPicture <> nil then
    lclOriginalSize.Caption := Format(cxGetResourceString(@sdxContainerCustomizationDialogOriginalSizeFormatString),
      [ContainerAsPicture.Picture.Image.Width, ContainerAsPicture.Picture.Image.Height]);
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.acTextureFillLoadExecute(Sender: TObject);
var
  ASmartImage: TdxGPImage;
begin
  TextureOpenDialog.Filter := GraphicFilter(TdxGPImage);
  if TextureOpenDialog.Execute(Handle) then
  begin
    ASmartImage := TdxGPImage.Create;
    try
      ASmartImage.LoadFromFile(TextureOpenDialog.FileName);
      imTextureFill.Picture.Graphic := ASmartImage;
    finally
      ASmartImage.Free;
    end;
  end;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.acTextureFillSaveExecute(Sender: TObject);
var
  AImageExt: string;
begin
  AImageExt := dxGetImageDataFormatExtension((imTextureFill.Picture.Graphic as TdxGPImage).ImageDataFormat);
  TextureSaveDialog.DefaultExt := AImageExt;
  TextureSaveDialog.Filter := UpperCase(Copy(AImageExt, 2, MaxInt)) + ' Image|*' + AImageExt + ';';
  if TextureSaveDialog.Execute(Handle) then
    imTextureFill.Picture.Graphic.SaveToFile(ChangeFileExt(TextureSaveDialog.FileName, AImageExt));
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.acTextureFillSaveUpdate(Sender: TObject);
begin
  acTextureFillSave.Enabled := rbTextureFill.Checked and (imTextureFill.Picture.Graphic <> nil);
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.btnGradientFillAddStopClick(Sender: TObject);
begin
  GradientFillStops.Add;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.btnGradientFillColorClick(Sender: TObject);
begin
  SelectGradientStopColor(GradientFillStops, btnGradientFillColor);
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.btnGradientFillRemoveStopClick(Sender: TObject);
begin
  GradientFillStops.DeleteSelected;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.btnGradientLineAddStopClick(Sender: TObject);
begin
  GradientLineStops.Add;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.btnGradientLineColorClick(Sender: TObject);
begin
  SelectGradientStopColor(GradientLineStops, btnGradientLineColor);
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.btnGradientLineRemoveStopClick(Sender: TObject);
begin
  GradientLineStops.DeleteSelected;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.btnResetClick(Sender: TObject);
begin
  CropMargins := cxNullRect;
  seHeight.Tag := ContainerAsPicture.Picture.Image.Height;
  seWidth.Tag := ContainerAsPicture.Picture.Image.Width;
  seHeight.Value := seHeight.Tag;
  seWidth.Value := seWidth.Tag;
  seRotation.Value := 0;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.btnSolidLineColorClick(Sender: TObject);
begin
  SelectColor(btnSolidLineColor);
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.btnSolidFillColorClick(Sender: TObject);
begin
  SelectColor(btnSolidFillColor);
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.cbLockAspectRatioClick(Sender: TObject);
begin
  if cbRelativeToPictureSize.Checked then
  begin
    FScaleNumerator := ContainerAsPicture.Picture.Image.Width;
    FScaleDenominator := ContainerAsPicture.Picture.Image.Height;
  end
  else
  begin
    FScaleNumerator := seWidth.Value;
    FScaleDenominator := seHeight.Value;
  end;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.cbRelativeToPictureSizeClick(Sender: TObject);
var
  ARect: TRect;
begin
  if cbRelativeToPictureSize.Checked then
    ARect := ContainerAsPicture.Picture.Image.ClientRect
  else
  begin
    ARect := TdxSpreadSheetContainerAccess(Container).Calculator.CalculateBounds;
    if ContainerAsPicture <> nil then
      ARect := cxRectInflate(ARect, ContainerAsPicture.Picture.CropMargins);
  end;

  seHeight.Tag := ARect.Height;
  seWidth.Tag := ARect.Width;
  cbLockAspectRatioClick(nil);
  seHeightPropertiesChange(nil);
  seWidthPropertiesChange(nil);
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.rbGradientLineClick(Sender: TObject);
begin
  lciLineWidth.Visible := not rbNoLine.Checked;
  lciLineStyle.Visible := not rbNoLine.Checked;
  lcgSolidLine.Visible := rbSolidLine.Checked;
  lcgGradientLine.Visible := rbGradientLine.Checked;
end;

procedure TdxSpreadSheetContainerCustomizationDialogForm.rbTextureFillClick(Sender: TObject);
begin
  lcgGradientFill.Visible := rbGradientFill.Checked;
  lcgSolidFill.Visible := rbSolidFill.Checked;
  lcgTextureFill.Visible := rbTextureFill.Checked;
end;

end.
