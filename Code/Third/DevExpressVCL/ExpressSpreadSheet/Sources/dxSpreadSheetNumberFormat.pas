{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetNumberFormat;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Graphics, dxCoreClasses, dxCore, Classes, SysUtils, cxClasses, dxSpreadSheetClasses, dxSpreadSheetTypes, Contnrs,
  cxRegExpr, dxSpreadSheetStrs, dxSpreadSheetUtils;

type
  EdxSpreadSheetNumberFormatError = class(EdxException);

  { TdxSpreadSheetNumberFormatResult }

  TdxSpreadSheetNumberFormatResult = record
    Color: TColor;
    IsError: Boolean;
    IsText: TdxDefaultBoolean;
    Text: TdxUnicodeString;
    procedure Reset;
  end;

  { TdxSpreadSheetNumberFormatValueInfo }

  TdxSpreadSheetNumberFormatValueInfo = record
    DateTimeSystem: TdxSpreadSheetDataTimeSystem;
    FormatSettings: TFormatSettings;
    ValueType: TdxSpreadSheetCellDataType;
    constructor Create(const AValue: Variant; AValueType: TdxSpreadSheetCellDataType;
      const ADateTimeSystem: TdxSpreadSheetDataTimeSystem; const AFormatSettings: TFormatSettings);
  end;

  { TdxSpreadSheetNumberFormatPatternMatch }

  TdxSpreadSheetNumberFormatPatternMatch = class(TObject)
  private
    FPosition: Integer;
    FLength: Integer;
  public
    property Position: Integer read FPosition write FPosition;
    property Length: Integer read FLength write FLength;
  end;

  { TdxSpreadSheetNumberFormatPattern }

  TdxSpreadSheetNumberFormatPattern = class(TObject)
  private
    FRegExpr: TcxRegExpr;
    function GetMatch(Index: Integer): TdxSpreadSheetNumberFormatPatternMatch;
    function GetMatchCount: Integer;
  protected
    FMatches: TcxObjectList;
    function AddMatch: TdxSpreadSheetNumberFormatPatternMatch;
    procedure JumpToNextPart(const S: TdxUnicodeString; var AIndex: Integer);
    procedure ProcessPart(const S: TdxUnicodeString; var AIndex: Integer);
    //
    property RegExpr: TcxRegExpr read FRegExpr;
  public
    constructor Create(const APatternString: TdxUnicodeString);
    destructor Destroy; override;
    procedure Process(const S: TdxUnicodeString);
    //
    property Matches[Index: Integer]: TdxSpreadSheetNumberFormatPatternMatch read GetMatch;
    property MatchCount: Integer read GetMatchCount;
  end;

  { TdxSpreadSheetCustomNumberFormatProvider }

  TdxSpreadSheetCustomNumberFormatProvider = class(TcxIUnknownObject)
  protected
    FFormatCode: TdxUnicodeString;
    function FormatNumeric(const AValue: Extended; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo): TdxUnicodeString; virtual;
    function FormatText(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo): TdxUnicodeString; virtual;

    function GetHasMilliseconds: Boolean; virtual;
    function GetIs12HourTime: Boolean; virtual;
    function GetIsDateTime: Boolean; virtual;
    function GetIsFraction: Boolean; virtual;
    function GetIsNumeric: Boolean; virtual;
    function GetIsNumericNoFraction: Boolean; virtual;
    function GetIsText: Boolean; virtual;
    procedure SetHasMilliseconds(const AValue: Boolean); virtual;
    procedure SetIs12HourTime(const AValue: Boolean); virtual;
    procedure SetIsDateTime(const AValue: Boolean); virtual;
    procedure SetIsFraction(const AValue: Boolean); virtual;
    procedure SetIsNumeric(const AValue: Boolean); virtual;
    procedure SetIsNumericNoFraction(const AValue: Boolean); virtual;
    procedure SetIsText(const AValue: Boolean); virtual;
  public
    procedure Format(const AValue: Variant; AValueType: TdxSpreadSheetCellDataType;
      AFormatSettings: TdxSpreadSheetFormatSettings; var AResult: TdxSpreadSheetNumberFormatResult); overload;
    procedure Format(const AValue: Variant; AValueType: TdxSpreadSheetCellDataType;
      ADateTimeSystem: TdxSpreadSheetDataTimeSystem; var AResult: TdxSpreadSheetNumberFormatResult); overload;
    procedure Format(const AValue: Variant; AValueType: TdxSpreadSheetCellDataType;
      ADateTimeSystem: TdxSpreadSheetDataTimeSystem; const AFormatSettings: TFormatSettings;
      var AResult: TdxSpreadSheetNumberFormatResult); overload;
    procedure Format(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); overload; virtual;
    //
    property HasMilliseconds: Boolean read GetHasMilliseconds write SetHasMilliseconds;
    property Is12HourTime: Boolean read GetIs12HourTime write SetIs12HourTime;
    property IsDateTime: Boolean read GetIsDateTime write SetIsDateTime;
    property IsFraction: Boolean read GetIsFraction write SetIsFraction;
    property IsNumeric: Boolean read GetIsNumeric write SetIsNumeric;
    property IsNumericNoFraction: Boolean read GetIsNumericNoFraction write SetIsNumericNoFraction;
    property IsText: Boolean read GetIsText write SetIsText;
    //
    property FormatCode: TdxUnicodeString read FFormatCode;
  end;

  { TdxSpreadSheetCustomNumberFormatProviders }

  TdxSpreadSheetCustomNumberFormatProviders = class(TObjectList)
  private
    function GetItem(Index: Integer): TdxSpreadSheetCustomNumberFormatProvider;
    procedure SetItem(Index: Integer; AValue: TdxSpreadSheetCustomNumberFormatProvider);
  public
    property Items[Index: Integer]: TdxSpreadSheetCustomNumberFormatProvider read GetItem write SetItem; default;
  end;

  { TdxSpreadSheetCustomNumberFormat }

  TdxSpreadSheetCustomNumberFormatClass = class of TdxSpreadSheetCustomNumberFormat;
  TdxSpreadSheetCustomNumberFormat = class(TdxSpreadSheetCustomNumberFormatProvider)
  private
    FContainsNumericSpecifiers: Boolean;
    FFormatCodeFixedForDecimal: TdxUnicodeString;
    FHasMilliseconds: Boolean;
    FId: Integer;
    FIs12HourFormat: Boolean;
    FIsDateTime: Boolean;
    FIsInsignificantZerosPlaceholder: Boolean;
    FIsText: Boolean;
  protected
    function DetectPossibleDecimalPointProblem(const AFormatCode: TdxUnicodeString): TdxUnicodeString; virtual;
    function EscapeTrailingDecimalPoints(const AFormatCode: TdxUnicodeString; AFrom: Integer): TdxUnicodeString; virtual;
    function ReplaceInsignificantZeroesWithSpaces(AText, AFormatString: TdxUnicodeString;
      const AFormatSettings: TFormatSettings): TdxUnicodeString; overload;
    procedure ReplaceInsignificantZeroesWithSpaces(var AResult: TdxUnicodeString;
      const AText, AFormatString: TdxUnicodeString; AFrom, ATo, AStep: Integer); overload;

    function FormatCodeToFormatStringDateTime(const AFormatCode: TdxUnicodeString): TdxUnicodeString;
    function FormatCodeToFormatStringNumeric(const AFormatCode: TdxUnicodeString): TdxUnicodeString;
    function FormatFloatValue(AValue: Double; AFormatString: TdxUnicodeString; const AFormatSettings: TFormatSettings): TdxUnicodeString;
    function FormatNumeric(const AValue: Extended; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo): TdxUnicodeString; override;
    function FormatNumericValue(const AValue: Double;
      const AFormatString: TdxUnicodeString; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo): TdxUnicodeString;
    function FormatText(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo): TdxUnicodeString; override;

    function GetHasMilliseconds: Boolean; override;
    function GetInnerFormatCode: TdxUnicodeString; virtual;
    function GetIs12HourTime: Boolean; override;
    function GetIsCustom: Boolean; virtual;
    function GetIsDateTime: Boolean; override;
    function GetIsNumeric: Boolean; override;
    function GetIsNumericNoFraction: Boolean; override;
    function GetIsText: Boolean; override;

    procedure SetHasMilliseconds(const AValue: Boolean); override;
    procedure SetInnerFormatCode(const AValue: TdxUnicodeString); virtual;
    procedure SetIs12HourTime(const AValue: Boolean); override;
    procedure SetIsDateTime(const AValue: Boolean); override;

    procedure Initialize; virtual;
  public
    constructor Create(const AFormatCode: TdxUnicodeString; AId: Integer = -1); virtual;
    //
    property ContainsNumericSpecifiers: Boolean read FContainsNumericSpecifiers;
    property Id: Integer read FId;
    property IsCustom: Boolean read GetIsCustom;
    property InnerFormatCode: TdxUnicodeString read GetInnerFormatCode write SetInnerFormatCode;
  end;

  { TdxSpreadSheetNumberFormat }

  TdxSpreadSheetNumberFormat = class(TdxSpreadSheetCustomNumberFormat)
  private
    FRootFormatProvider: TdxSpreadSheetCustomNumberFormatProvider;
    function GetRootFormatProvider: TdxSpreadSheetCustomNumberFormatProvider;
  protected
    function CreateRootFormatProvider: TdxSpreadSheetCustomNumberFormatProvider; virtual;
    function CreateSimpleNumberFormatProvider(AFormatCode: TdxUnicodeString;
      AIsNegativeNumberFormat: Boolean = False): TdxSpreadSheetCustomNumberFormatProvider; virtual;
    function DetectPossibleDecimalPointProblem(const AFormatCode: TdxUnicodeString): TdxUnicodeString; override;
    function EscapeTrailingDecimalPoints(const AFormatCode: TdxUnicodeString; AFrom: Integer): TdxUnicodeString; override;

    procedure SetInnerFormatCode(const AValue: TdxUnicodeString); override;
  public
    destructor Destroy; override;
    procedure Format(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
    //
    property RootFormatProvider: TdxSpreadSheetCustomNumberFormatProvider read GetRootFormatProvider;
  end;

  { TdxSpreadSheetNumberFormatDateTimeHelper }

  TdxSpreadSheetNumberFormatDateTimeHelper = class
  protected
    class function DetectDateTimeFormatCore(const AFormatCode: TdxUnicodeString): Boolean;
    class function DoMergeBaseDateTimeResults(var APrimaryResult, ASecondaryResult: TdxUnicodeString;
      AFrom, ATo, AStep: Integer; AFirstResultChar, ASecondResultChar, AReplaceWithChar: TdxUnicodeChar): Integer;
    class function RemoveLocaleIdPrefix(const AFormatCode: TdxUnicodeString): TdxUnicodeString;
    class function RemoveMillisecondsSuffixes(const AFormatCode: TdxUnicodeString): TdxUnicodeString;
  public
    class function Format(const AValue: TDateTime; const AFormatString: TdxUnicodeString;
      const AFormatSettings: TFormatSettings; AIs12HourTime, AHasMilliseconds: Boolean): TdxUnicodeString;
    class function DetectDateTimeFormat(AFormatCode: TdxUnicodeString): Boolean;
    class function MergeBaseDateTimeResults(AFirstResult, ASecondResult: TdxUnicodeString;
      AFirstResultChar, ASecondResultChar, AReplaceWithChar: TdxUnicodeChar): TdxUnicodeString;
  end;

  { TdxSpreadSheetNumberFormatSplitter }

  TdxSpreadSheetNumberFormatSplitter = class
  public
    class procedure Split(const AFormatCode: TdxUnicodeString; AList: TStringList);
  end;

  { TdxSpreadSheetPredefinedNumberFormat }

  TdxSpreadSheetPredefinedNumberFormat = class(TdxSpreadSheetNumberFormat)
  protected
    function GetIsCustom: Boolean; override;
  end;

  { TdxSpreadSheetFullDateTimeNumberFormat }

  TdxSpreadSheetFullDateTimeNumberFormat = class(TdxSpreadSheetPredefinedNumberFormat)
  private
    FShortDateFormat: TdxSpreadSheetNumberFormat;
  public
    constructor Create; reintroduce; virtual;
    destructor Destroy; override;
    procedure Format(const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo;
      var AResult: TdxSpreadSheetNumberFormatResult); override;
  end;

function dxGetDataTypeByVariantValue(const AValue: Variant): TdxSpreadSheetCellDataType;
function dxIsNumericType(AValueType: TdxSpreadSheetCellDataType): Boolean;
implementation

uses
  Variants, cxVariants, cxFormats, StrUtils, cxDateUtils, Math, Windows, dxSpreadSheetNumberFormatProviders,
  dxSpreadSheetFunctionsMath;

const
  sdxDecimalPoint = '.';
  sdxTextHolder = '@';

  dxNumericFormatChars: array[0..2] of Char = ('0', '#', '?');
  dxDateTimeFormatChars: array[0..10] of Char = ('m', 'M', 's', 'S', 'y', 'Y', 'd', 'D', 'h', 'H', 'N');

function dxFormatFloatTemplateGetFractionalDigitsCount(const S: TdxUnicodeString; out ACount: Integer): Boolean;
var
  AChar: TdxUnicodeChar;
  ADecimalIndex: Integer;
  ADigitCount: Integer;
  ALength: Integer;
  AScientific: Boolean;
  APosition: Integer;
begin
  ADecimalIndex := -1;
  ADigitCount := 0;
  ALength := Length(S);
  APosition := 1;
  AScientific := False;

  while APosition <= ALength do
  begin
    case S[APosition] of
      ',':
         Inc(APosition);

      '.':
        begin
          if ADecimalIndex = -1 then
            ADecimalIndex := ADigitCount;
          Inc(APosition);
        end;

      '"':
        begin
          Inc(APosition);
          while (APosition <= ALength) and (S[APosition] <> '"') do
            Inc(APosition);
          if APosition <= ALength then
            Inc(APosition);
        end;

      '''':
        begin
          Inc(APosition);
          while (APosition <= ALength) and (S[APosition] <> '''') do
            Inc(APosition);
          if APosition <= ALength then
            Inc(APosition);
        end;

      'e', 'E':
        begin
          Inc(APosition);
          if APosition <= ALength then
          begin
            AChar := S[APosition];
            if (AChar = '-') or (AChar = '+') then
            begin
              AScientific := true;
              Inc(APosition);
              while (APosition <= ALength) and (S[APosition] = '0') do
                Inc(APosition);
            end;
          end;
        end;

      '#', '0':
        begin
          Inc(ADigitCount);
          Inc(APosition);
        end;

      else
        Inc(APosition);
    end;
  end;

  if ADecimalIndex = -1 then
    ADecimalIndex := ADigitCount;

  ACount := ADigitCount - ADecimalIndex;
  Result := not AScientific and (ACount > 0);
end;

function dxGetDataTypeByVariantValue(const AValue: Variant): TdxSpreadSheetCellDataType;
begin
  case VarType(AValue) of
    varBoolean:
      Result := cdtBoolean;
    varCurrency:
      Result := cdtCurrency;
    varSingle, varDouble:
      Result := cdtFloat;
    varDate:
      Result := cdtDateTime;
    varSmallint, varInteger, varShortInt, varByte, varWord, varLongWord, varInt64, varUInt64:
      Result := cdtInteger;
  else
    Result := cdtString;
  end;
end;

function dxIsNumericType(AValueType: TdxSpreadSheetCellDataType): Boolean;
begin
  Result := AValueType in [cdtCurrency, cdtFloat, cdtDateTime, cdtInteger];
end;

function dxStringHasOneOfSymbols(const S: TdxUnicodeString; ASymbols: array of Char): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to Length(ASymbols) - 1 do
  begin
    Result := Pos(ASymbols[I], S) > 0;
    if Result then
      Break;
  end;
end;

function dxStringStartsWith(const S, AStartsWith: TdxUnicodeString): Boolean;
begin
  Result := (AStartsWith <> '') and
    (Length(S) >= Length(AStartsWith)) and
    (Copy(S, 1, Length(AStartsWith)) = AStartsWith);
end;

{ TdxSpreadSheetNumberFormatResult }

procedure TdxSpreadSheetNumberFormatResult.Reset;
begin
  Color := clDefault;
  IsError := False;
  IsText := bDefault;
  Text := EmptyStr;
end;

{ TdxSpreadSheetNumberFormatValueInfo }

constructor TdxSpreadSheetNumberFormatValueInfo.Create(const AValue: Variant; AValueType: TdxSpreadSheetCellDataType;
  const ADateTimeSystem: TdxSpreadSheetDataTimeSystem; const AFormatSettings: TFormatSettings);
begin
  if AValueType = cdtFormula then
    AValueType := dxGetDataTypeByVariantValue(AValue);
  DateTimeSystem := ADateTimeSystem;
  FormatSettings := AFormatSettings;
  ValueType := AValueType;
end;

{ TdxSpreadSheetNumberFormatPattern }

constructor TdxSpreadSheetNumberFormatPattern.Create(const APatternString: TdxUnicodeString);
begin
  inherited Create;
  FMatches := TcxObjectList.Create;
  FRegExpr := TcxRegExpr.Create;
  FRegExpr.Compile(string(APatternString));
end;

destructor TdxSpreadSheetNumberFormatPattern.Destroy;
begin
  FreeAndNil(FRegExpr);
  FreeAndNil(FMatches);
  inherited Destroy;
end;

procedure TdxSpreadSheetNumberFormatPattern.Process(const S: TdxUnicodeString);
var
  AIndex: Integer;
begin
  AIndex := 1;
  while AIndex <= Length(S) do
  begin
    ProcessPart(S, AIndex);
    JumpToNextPart(S, AIndex);
  end;
end;

function TdxSpreadSheetNumberFormatPattern.AddMatch: TdxSpreadSheetNumberFormatPatternMatch;
begin
  Result := TdxSpreadSheetNumberFormatPatternMatch.Create;
  FMatches.Add(Result);
end;

procedure TdxSpreadSheetNumberFormatPattern.JumpToNextPart(const S: TdxUnicodeString; var AIndex: Integer);
var
  AChar: Char;
begin
  RegExpr.Reset;
  while AIndex <= Length(S) do
  begin
    AChar := Char(S[AIndex]);
    if RegExpr.Next(AChar) then
      Break;
    Inc(AIndex);
  end;
end;

procedure TdxSpreadSheetNumberFormatPattern.ProcessPart(const S: TdxUnicodeString; var AIndex: Integer);
var
  AChar: Char;
  AMatch: TdxSpreadSheetNumberFormatPatternMatch;
  I: Integer;
begin
  RegExpr.Reset;

  AMatch := nil;
  for I := AIndex to Length(S) do
  begin
    AChar := Char(S[I]);
    if RegExpr.Next(AChar) then
    begin
      if AMatch = nil then
      begin
        AMatch := AddMatch;
        AMatch.Position := I;
      end;
    end
    else
      Break;
  end;
  if AMatch <> nil then
  begin
    if RegExpr.IsFinal then
      AMatch.Length := I - AMatch.Position
    else
      FMatches.FreeAndRemove(AMatch);
  end;
  AIndex := I;
end;

function TdxSpreadSheetNumberFormatPattern.GetMatch(Index: Integer): TdxSpreadSheetNumberFormatPatternMatch;
begin
  Result := TdxSpreadSheetNumberFormatPatternMatch(FMatches[Index]);
end;

function TdxSpreadSheetNumberFormatPattern.GetMatchCount: Integer;
begin
  Result := FMatches.Count;
end;

{ TdxSpreadSheetCustomNumberFormatProvider }

procedure TdxSpreadSheetCustomNumberFormatProvider.Format(
  const AValue: Variant; AValueType: TdxSpreadSheetCellDataType;
  AFormatSettings: TdxSpreadSheetFormatSettings; var AResult: TdxSpreadSheetNumberFormatResult);
begin
  Format(AValue, AValueType, AFormatSettings.DateTimeSystem, AFormatSettings.Data, AResult);
end;

procedure TdxSpreadSheetCustomNumberFormatProvider.Format(const AValue: Variant;
  AValueType: TdxSpreadSheetCellDataType; ADateTimeSystem: TdxSpreadSheetDataTimeSystem;
  var AResult: TdxSpreadSheetNumberFormatResult);
var
  AFormatSettings: TFormatSettings;
begin
  dxGetLocaleFormatSettings(GetThreadLocale, AFormatSettings);
  Format(AValue, AValueType, ADateTimeSystem, AFormatSettings, AResult);
end;

procedure TdxSpreadSheetCustomNumberFormatProvider.Format(const AValue: Variant; AValueType: TdxSpreadSheetCellDataType;
  ADateTimeSystem: TdxSpreadSheetDataTimeSystem; const AFormatSettings: TFormatSettings; var AResult: TdxSpreadSheetNumberFormatResult);
begin
  Format(AValue, TdxSpreadSheetNumberFormatValueInfo.Create(AValue, AValueType, ADateTimeSystem, AFormatSettings), AResult);
end;

procedure TdxSpreadSheetCustomNumberFormatProvider.Format(const AValue: Variant;
  const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
begin
  AResult.Reset;
  case AValueInfo.ValueType of
    cdtBlank:
      {do nothing};
    cdtBoolean:
      AResult.Text := dxBoolToString[Boolean(AValue)];
    cdtError:
      AResult.Text := dxSpreadSheetErrorCodeToString(TdxSpreadSheetFormulaErrorCode(AValue));
    cdtCurrency, cdtFloat, cdtDateTime, cdtInteger:
      AResult.Text := FormatNumeric(AValue, AValueInfo);
    cdtString:
      AResult.Text := FormatText(AValue, AValueInfo);
  else
    raise EdxSpreadSheetNumberFormatError.CreateFmt(cxGetResourceString(@sdxErrorInternal), ['cannot format variant']);
  end;
end;


function TdxSpreadSheetCustomNumberFormatProvider.FormatNumeric(
  const AValue: Extended; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo): TdxUnicodeString;
begin
  if AValueInfo.ValueType = cdtDateTime then
    Result := DateTimeToStr(dxDateTimeToRealDateTime(AValue, AValueInfo.DateTimeSystem), AValueInfo.FormatSettings)
  else
    Result := dxFloatToStr(AValue, AValueInfo.FormatSettings);
end;

function TdxSpreadSheetCustomNumberFormatProvider.FormatText(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo): TdxUnicodeString;
begin
  if VarIsNull(AValue) then
    Result := ''
  else
    Result := AValue;

  if (Result <> '') and (Result[1] = '''') then
    Delete(Result, 1, 1);
end;

function TdxSpreadSheetCustomNumberFormatProvider.GetHasMilliseconds: Boolean;
begin
  Result := False;
end;

function TdxSpreadSheetCustomNumberFormatProvider.GetIs12HourTime: Boolean;
begin
  Result := False;
end;

function TdxSpreadSheetCustomNumberFormatProvider.GetIsDateTime: Boolean;
begin
  Result := False;
end;

function TdxSpreadSheetCustomNumberFormatProvider.GetIsFraction: Boolean;
begin
  Result := False;
end;

function TdxSpreadSheetCustomNumberFormatProvider.GetIsNumeric: Boolean;
begin
  Result := False;
end;

function TdxSpreadSheetCustomNumberFormatProvider.GetIsNumericNoFraction: Boolean;
begin
  Result := False;
end;

function TdxSpreadSheetCustomNumberFormatProvider.GetIsText: Boolean;
begin
  Result := False;
end;

procedure TdxSpreadSheetCustomNumberFormatProvider.SetHasMilliseconds(const AValue: Boolean);
begin
  // do nothing
end;

procedure TdxSpreadSheetCustomNumberFormatProvider.SetIs12HourTime(const AValue: Boolean);
begin
  // do nothing
end;

procedure TdxSpreadSheetCustomNumberFormatProvider.SetIsDateTime(const AValue: Boolean);
begin
  // do nothing
end;

procedure TdxSpreadSheetCustomNumberFormatProvider.SetIsFraction(const AValue: Boolean);
begin
  // do nothing
end;

procedure TdxSpreadSheetCustomNumberFormatProvider.SetIsNumeric(const AValue: Boolean);
begin
  // do nothing
end;

procedure TdxSpreadSheetCustomNumberFormatProvider.SetIsNumericNoFraction(const AValue: Boolean);
begin
  // do nothing
end;

procedure TdxSpreadSheetCustomNumberFormatProvider.SetIsText(const AValue: Boolean);
begin
  // do nothing
end;

{ TdxSpreadSheetCustomNumberFormatProviders }

function TdxSpreadSheetCustomNumberFormatProviders.GetItem(Index: Integer): TdxSpreadSheetCustomNumberFormatProvider;
begin
  Result := TdxSpreadSheetCustomNumberFormatProvider(inherited Items[Index]);
end;

procedure TdxSpreadSheetCustomNumberFormatProviders.SetItem(
  Index: Integer; AValue: TdxSpreadSheetCustomNumberFormatProvider);
begin
  inherited Items[Index] := AValue;
end;

{ TdxSpreadSheetCustomNumberFormat }

constructor TdxSpreadSheetCustomNumberFormat.Create(const AFormatCode: TdxUnicodeString; AId: Integer = -1);
begin
  inherited Create;
  FId := AId;
  if SameText(AFormatCode, dxSpreadSheetGeneralNumberFormat) then
    FFormatCode := ''
  else
    FFormatCode := AFormatCode;

  Initialize;
end;

function TdxSpreadSheetCustomNumberFormat.DetectPossibleDecimalPointProblem(const AFormatCode: TdxUnicodeString): TdxUnicodeString;
var
  AEscapedFormatCode: TdxUnicodeString;
  AFormatSettings: TFormatSettings;
begin
  Result := AFormatCode;
  dxGetLocaleFormatSettings(GetThreadLocale, AFormatSettings);
  AFormatSettings.DecimalSeparator := sdxDecimalPoint;

  // there are decimal point in formatCode
  if Pos(sdxDecimalPoint, FormatFloat(AFormatCode, 5.0, AFormatSettings)) = 0 then
  begin
    //no decimal point
    AEscapedFormatCode := StringReplace(AFormatCode, '.', '\..', [rfReplaceAll]);
    if Pos(sdxDecimalPoint, FormatFloat(AEscapedFormatCode, 5.0, AFormatSettings)) > 0 then
    begin
      if Pos('..', FormatFloat(AEscapedFormatCode, 5.1, AFormatSettings)) = 0 then
        Result := AEscapedFormatCode
    end
  end;
end;

function TdxSpreadSheetCustomNumberFormat.EscapeTrailingDecimalPoints(
  const AFormatCode: TdxUnicodeString; AFrom: Integer): TdxUnicodeString;
var
  AIndex: Integer;
begin
  if AFrom > Length(AFormatCode) then
  begin
    Result := AFormatCode;
    Exit;
  end;

  AIndex := PosEx(sdxDecimalPoint, AFormatCode, AFrom);
  if AIndex = 0 then
  begin
    Result := AFormatCode;
    Exit;
  end;

  AFrom := 1;
  Result := EmptyStr;
  while True do
  begin
    Result := Result + Copy(AFormatCode, AFrom, AIndex - AFrom) + '\.';
    AFrom := AIndex + 1;
    if AFrom > Length(AFormatCode) then
      Break;

    AIndex := PosEx(sdxDecimalPoint, AFormatCode, AFrom);
    if AIndex = 0 then
    begin
      Result := Result + Copy(AFormatCode, AFrom, MaxInt);
      Break;
    end;
  end;
end;

function TdxSpreadSheetCustomNumberFormat.ReplaceInsignificantZeroesWithSpaces(
  AText, AFormatString: TdxUnicodeString; const AFormatSettings: TFormatSettings): TdxUnicodeString;
var
  AFormatDigitalSeparatorIndex: Integer;
  AFormatRightLength: Integer;
  AOriginalTextLength: Integer;
  APadding: Integer;
  AResultDigitalSeparatorIndex: Integer;
  AResultRightLength: Integer;
  ASeparator: TdxUnicodeString;
  ASeparatorIndex: Integer;
  ATextLeftPadding: Integer;
begin
  AOriginalTextLength := Length(AText);
  ASeparator := AFormatSettings.DecimalSeparator;

  AFormatDigitalSeparatorIndex := Pos(sdxDecimalPoint, AFormatString);
  if AFormatDigitalSeparatorIndex = 0 then
    AFormatDigitalSeparatorIndex := Length(AFormatString) + 1;

  AResultDigitalSeparatorIndex := Pos(ASeparator, AText);
  if AResultDigitalSeparatorIndex = 0 then
    AResultDigitalSeparatorIndex := Length(AText) + 1;

  AFormatRightLength := Length(AFormatString) - AFormatDigitalSeparatorIndex - 1;
  AResultRightLength := Length(AText) - AResultDigitalSeparatorIndex - 1;

  ATextLeftPadding := 1;
  APadding := AFormatDigitalSeparatorIndex - AResultDigitalSeparatorIndex;
  if APadding > 0 then
  begin
    AText := DupeString(' ', APadding) + AText;
    ATextLeftPadding := APadding;
  end
  else
    if APadding < 0 then
      AFormatString := DupeString(' ', -APadding) + AFormatString;

  APadding := AFormatRightLength - AResultRightLength;
  if APadding > 0 then
    AText := AText + DupeString(' ', APadding)
  else
    if APadding < 0 then
      AFormatString := AFormatString + DupeString(' ', -APadding);

  dxTestCheck(Length(AText) = Length(AFormatString), 'ReplaceInsignificantZeroesWithSpaces');

  Result := AText;
  ASeparatorIndex := Max(AFormatDigitalSeparatorIndex, AResultDigitalSeparatorIndex);
  ReplaceInsignificantZeroesWithSpaces(Result, AText, AFormatString, 1, ASeparatorIndex, 1);
  if ASeparatorIndex < Length(AText) then
    ReplaceInsignificantZeroesWithSpaces(Result, AText, AFormatString, Length(AText), ASeparatorIndex, -1);
  Result := Copy(Result, ATextLeftPadding, AOriginalTextLength);
end;

procedure TdxSpreadSheetCustomNumberFormat.ReplaceInsignificantZeroesWithSpaces(
  var AResult: TdxUnicodeString; const AText, AFormatString: TdxUnicodeString; AFrom, ATo, AStep: Integer);
var
  I: Integer;
begin
  I := AFrom;
  while True do
  begin
    if dxCharIsNumeric(Char(AText[I])) then
    begin
      if AText[I] <> '0' then
        Break;
      if AFormatString[I] = '?' then
        AResult[I] := ' ';
    end;
    if I = ATo then
      Break;
    Inc(I, AStep);
  end;
end;

function TdxSpreadSheetCustomNumberFormat.FormatCodeToFormatStringDateTime(const AFormatCode: TdxUnicodeString): TdxUnicodeString;
begin
  Result := FormatCodeToFormatStringNumeric(AFormatCode);
  Result := StringReplace(Result, 'D', 'd', [rfReplaceAll]);
  Result := StringReplace(Result, 'Y', 'y', [rfReplaceAll]);
end;

function TdxSpreadSheetCustomNumberFormat.FormatCodeToFormatStringNumeric(const AFormatCode: TdxUnicodeString): TdxUnicodeString;
begin
  Result := StringReplace(AFormatCode, sdxTextHolder, EmptyStr, [rfReplaceAll]);
end;

function TdxSpreadSheetCustomNumberFormat.FormatFloatValue(AValue: Double;
  AFormatString: TdxUnicodeString; const AFormatSettings: TFormatSettings): TdxUnicodeString;
var
  ADigitsCount: Integer;
  ATempValue: Extended;
begin
  if Pos('%', AFormatString) > 0 then
    AValue := AValue * 100;
  AFormatString := StringReplace(AFormatString, '\.', '"."', [rfReplaceAll]);
  if dxFormatFloatTemplateGetFractionalDigitsCount(AFormatString, ADigitsCount) then
  try
    ATempValue := AValue;
    dxSpreadSheetRound(ATempValue, IntPower(10, ADigitsCount));
    AValue := ATempValue;
  except
    // do nothing
  end;
  Result := FormatFloat(AFormatString, AValue, AFormatSettings);

  if (Pos(sdxDecimalPoint, AFormatString) > 0) and (Pos(AFormatSettings.DecimalSeparator, Result) = 0) then
    Result := Result + AFormatSettings.DecimalSeparator;
end;

function TdxSpreadSheetCustomNumberFormat.FormatNumeric(
  const AValue: Extended; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo): TdxUnicodeString;
begin
  if IsDateTime then
    Result := TdxSpreadSheetNumberFormatDateTimeHelper.Format(dxDateTimeToRealDateTime(AValue, AValueInfo.DateTimeSystem),
      FormatCodeToFormatStringDateTime(FormatCode), AValueInfo.FormatSettings, Is12HourTime, HasMilliseconds)
  else
    if Frac(AValue) = 0 then
      Result := FormatNumericValue(AValue, FormatCodeToFormatStringNumeric(FFormatCodeFixedForDecimal), AValueInfo)
    else
      Result := FormatNumericValue(AValue, FormatCodeToFormatStringNumeric(FormatCode), AValueInfo);
end;

function TdxSpreadSheetCustomNumberFormat.FormatNumericValue(const AValue: Double;
  const AFormatString: TdxUnicodeString; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo): TdxUnicodeString;
var
  AModifiedFormatString: TdxUnicodeString;
begin
  if not ContainsNumericSpecifiers and (AFormatString <> '') then
    Result := AFormatString
  else
    if FIsInsignificantZerosPlaceholder then
    begin
      AModifiedFormatString := StringReplace(AFormatString, '?', '0', [rfReplaceAll]);
      Result := FormatFloatValue(AValue, AModifiedFormatString, AValueInfo.FormatSettings);
      Result := ReplaceInsignificantZeroesWithSpaces(Result, AFormatString, AValueInfo.FormatSettings);
    end
    else
    begin
      Result := FormatFloatValue(AValue, AFormatString, AValueInfo.FormatSettings);
      if Result <> '' then
        Result := IfThen((AFormatString <> '') and (AFormatString[1] = ' '), ' ') + TrimLeft(Result);
    end;
end;

function TdxSpreadSheetCustomNumberFormat.FormatText(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo): TdxUnicodeString;
begin
  if IsText then
    Result := StringReplace(FormatCode, sdxTextHolder, AValue, [rfReplaceAll])
  else
    Result := AValue;
end;

function TdxSpreadSheetCustomNumberFormat.GetHasMilliseconds: Boolean;
begin
  Result := FHasMilliseconds;
end;

function TdxSpreadSheetCustomNumberFormat.GetInnerFormatCode: TdxUnicodeString;
begin
  Result := FormatCode;
end;

function TdxSpreadSheetCustomNumberFormat.GetIs12HourTime: Boolean;
begin
  Result := FIs12HourFormat;
end;

function TdxSpreadSheetCustomNumberFormat.GetIsCustom: Boolean;
begin
  Result := True;
end;

function TdxSpreadSheetCustomNumberFormat.GetIsDateTime: Boolean;
begin
  Result := FIsDateTime;
end;

function TdxSpreadSheetCustomNumberFormat.GetIsNumeric: Boolean;
begin
  Result := not IsDateTime;
end;

function TdxSpreadSheetCustomNumberFormat.GetIsNumericNoFraction: Boolean;
begin
  Result := IsNumeric;
end;

function TdxSpreadSheetCustomNumberFormat.GetIsText: Boolean;
begin
  Result := FIsText;
end;

procedure TdxSpreadSheetCustomNumberFormat.SetHasMilliseconds(const AValue: Boolean);
begin
  FHasMilliseconds := AValue;
end;

procedure TdxSpreadSheetCustomNumberFormat.SetInnerFormatCode(const AValue: TdxUnicodeString);
begin
  if FormatCode <> AValue then
  begin
    FFormatCode := AValue;
    Initialize;
  end;
end;

procedure TdxSpreadSheetCustomNumberFormat.SetIs12HourTime(const AValue: Boolean);
begin
  FIs12HourFormat := AValue;
end;

procedure TdxSpreadSheetCustomNumberFormat.SetIsDateTime(const AValue: Boolean);
begin
  if AValue then
    FIsDateTime := AValue;
end;

procedure TdxSpreadSheetCustomNumberFormat.Initialize;
var
  ADecimalPointIndex: Integer;
  AParts: TStringList;
begin
  FIsInsignificantZerosPlaceholder := Pos('?', FormatCode) > 0;

  AParts := TStringList.Create;
  try
    TdxSpreadSheetNumberFormatSplitter.Split(FormatCode, AParts);
    if AParts.Count > 0 then
      FIsText := Pos(sdxTextHolder, AParts[0]) > 0
    else
      FIsText := Pos(sdxTextHolder, FormatCode) > 0;
  finally
    AParts.Free;
  end;

  FIsDateTime := not IsText and TdxSpreadSheetNumberFormatDateTimeHelper.DetectDateTimeFormat(FormatCode);
  if IsDateTime then
    FFormatCodeFixedForDecimal := FormatCode
  else
  begin
    ADecimalPointIndex := Pos(sdxDecimalPoint, FormatCode);
    if ADecimalPointIndex > 0 then
    begin
      FFormatCode := EscapeTrailingDecimalPoints(FormatCode, ADecimalPointIndex + 1);
      FFormatCodeFixedForDecimal := DetectPossibleDecimalPointProblem(FormatCode);
    end
    else
      FFormatCodeFixedForDecimal := FormatCode;
  end;

  if IsNumeric then
    FContainsNumericSpecifiers := dxStringHasOneOfSymbols(FormatCode, dxNumericFormatChars);
end;

{ TdxSpreadSheetNumberFormat }

destructor TdxSpreadSheetNumberFormat.Destroy;
begin
  FreeAndNil(FRootFormatProvider);
  inherited Destroy;
end;

procedure TdxSpreadSheetNumberFormat.Format(const AValue: Variant;
  const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
begin
  AResult.Reset;
  if AValueInfo.ValueType <> cdtBlank then
  try
    RootFormatProvider.Format(AValue, AValueInfo, AResult);
    if IsText then
      AResult.IsText := bTrue;
  except
    AResult.IsError := True;
  end;
end;

function TdxSpreadSheetNumberFormat.CreateRootFormatProvider: TdxSpreadSheetCustomNumberFormatProvider;
var
  AConditionalProvider: TdxSpreadSheetDefaultConditionalNumberFormatProvider;
  AParts: TStringList;
  AProvider: TdxSpreadSheetCustomNumberFormatProvider;
  I: Integer;
begin
  if FormatCode <> '' then
  begin
    AConditionalProvider := TdxSpreadSheetDefaultConditionalNumberFormatProvider.Create;

    AParts := TStringList.Create;
    try
      TdxSpreadSheetNumberFormatSplitter.Split(FormatCode, AParts);
      for I := 0 to AParts.Count - 1 do
      begin
        if AParts[I] = '' then
          AProvider := TdxSpreadSheetExplicitTextFormatProvider.Create(EmptyStr)
        else
          AProvider := CreateSimpleNumberFormatProvider(AParts[I], I = 1);

        AConditionalProvider.AddProvider(AProvider);
      end;
    finally
      AParts.Free;
    end;
    Result := AConditionalProvider;
  end
  else
    Result := TdxSpreadSheetCustomNumberFormatProvider.Create;
end;

function TdxSpreadSheetNumberFormat.CreateSimpleNumberFormatProvider(
  AFormatCode: TdxUnicodeString; AIsNegativeNumberFormat: Boolean = False): TdxSpreadSheetCustomNumberFormatProvider;
var
  AColor: TColor;
  AFactory: TdxSpreadSheetSimpleNumberFormatProviderFactory;
begin
  AFactory := TdxSpreadSheetSimpleNumberFormatProviderFactory.Create;
  try
    if TdxSpreadSheetColorNumberFormatProvider.Parse(AFormatCode, AColor) then
      Result := TdxSpreadSheetColorNumberFormatProvider.Create(AFactory.CreateProvider(AFormatCode, Id), AColor)
    else
      Result := AFactory.CreateProvider(AFormatCode, Id);

    if AIsNegativeNumberFormat then
      Result := TdxSpreadSheetNegativeNumberFormatProvider.Create(Result);
  finally
    AFactory.Free;
  end;
end;

function TdxSpreadSheetNumberFormat.DetectPossibleDecimalPointProblem(const AFormatCode: TdxUnicodeString): TdxUnicodeString;
begin
  Result := AFormatCode;
end;

function TdxSpreadSheetNumberFormat.EscapeTrailingDecimalPoints(const AFormatCode: TdxUnicodeString; AFrom: Integer): TdxUnicodeString;
begin
  Result := AFormatCode;
end;

procedure TdxSpreadSheetNumberFormat.SetInnerFormatCode(const AValue: TdxUnicodeString);
begin
  if InnerFormatCode <> AValue then
  begin
    inherited SetInnerFormatCode(AValue);
    FreeAndNil(FRootFormatProvider);
  end;
end;

function TdxSpreadSheetNumberFormat.GetRootFormatProvider: TdxSpreadSheetCustomNumberFormatProvider;
begin
  if FRootFormatProvider = nil then
    FRootFormatProvider := CreateRootFormatProvider;
  Result := FRootFormatProvider;
end;

{ TdxSpreadSheetNumberFormatDateTimeHelper }

class function TdxSpreadSheetNumberFormatDateTimeHelper.Format(const AValue: TDateTime;
  const AFormatString: TdxUnicodeString; const AFormatSettings: TFormatSettings; AIs12HourTime, AHasMilliseconds: Boolean): TdxUnicodeString;

  function DoFormatDateTime(AValue: TDateTime; const AFormatString: TdxUnicodeString): TdxUnicodeString;
  begin
    try
      if not AHasMilliseconds then
        AValue := AValue + EncodeTime(0, 0, 0, 500);

      if AIs12HourTime then
      begin
        Result := SysUtils.FormatDateTime('A/P' + AFormatString, AValue, AFormatSettings);
        if (Length(Result) > 1) and ((Result[1] = 'A') or (Result[1] = 'P')) then
          Delete(Result, 1, 1);
      end
      else
        Result := SysUtils.FormatDateTime(AFormatString, AValue, AFormatSettings);
    except
      Result := AFormatString;
    end;
  end;

  function DoMergeBaseDateTimeResults(const AFirstResult, ASecondResult: TDateTime;
    AFirstResultChar, ASecondResultChar, AReplaceWithChar: TdxUnicodeChar): TdxUnicodeString;
  begin
    Result := MergeBaseDateTimeResults(DoFormatDateTime(AFirstResult, AFormatString),
      DoFormatDateTime(ASecondResult, AFormatString), AFirstResultChar, ASecondResultChar, AReplaceWithChar);
  end;

var
  ADateTime: TDateTime;
begin

  if (AValue < 0) or (AValue >= MaxDateTime) then
  begin
    Result := '#';
    Exit;
  end;

  ADateTime := AValue;
  if ADateTime < 61 then
    ADateTime := ADateTime + 1;

  if AValue < 1 then
    Result := DoMergeBaseDateTimeResults(ADateTime + 1, ADateTime + 2, '1', '2', '0')
  else
    if AValue = 60 then 
      Result := DoMergeBaseDateTimeResults(AValue, EncodeDate(2000, 2, 29), '8', '9', '9')
    else
      Result := DoFormatDateTime(ADateTime, AFormatString);
end;

class function TdxSpreadSheetNumberFormatDateTimeHelper.DetectDateTimeFormat(AFormatCode: TdxUnicodeString): Boolean;
begin
  Result := False;
  if AFormatCode <> '' then
  begin
    Result := DetectDateTimeFormatCore(AFormatCode);
    if not Result then
    begin
      AFormatCode := RemoveMillisecondsSuffixes(AFormatCode);
      Result := DetectDateTimeFormatCore(AFormatCode);
      if not Result then
      begin
        AFormatCode := RemoveLocaleIdPrefix(AFormatCode);
        Result := DetectDateTimeFormatCore(AFormatCode);
      end;
    end;
  end;
end;

class function TdxSpreadSheetNumberFormatDateTimeHelper.MergeBaseDateTimeResults(
  AFirstResult, ASecondResult: TdxUnicodeString; AFirstResultChar, ASecondResultChar, AReplaceWithChar: TdxUnicodeChar): TdxUnicodeString;
var
  ADesyncIndex: Integer;
  ALength: Integer;
begin
  if Length(AFirstResult) = Length(ASecondResult) then
    DoMergeBaseDateTimeResults(AFirstResult, ASecondResult, 1, Length(AFirstResult), 1,
      AFirstResultChar, ASecondResultChar, AReplaceWithChar)
  else
  begin
    ALength := Min(Length(AFirstResult), Length(ASecondResult));
    ADesyncIndex := DoMergeBaseDateTimeResults(AFirstResult, ASecondResult, 1,
      Length(AFirstResult), 1,AFirstResultChar, ASecondResultChar, AReplaceWithChar);
    if ADesyncIndex <> ALength + 1 then
    begin
      DoMergeBaseDateTimeResults(AFirstResult, ASecondResult, Length(AFirstResult),
        ADesyncIndex, -1, AFirstResultChar, ASecondResultChar, AReplaceWithChar);
    end;
  end;
  Result := AFirstResult;
end;

class function TdxSpreadSheetNumberFormatDateTimeHelper.DetectDateTimeFormatCore(const AFormatCode: TdxUnicodeString): Boolean;
begin
  Result := (AFormatCode <> '') and (not dxStringHasOneOfSymbols(AFormatCode, dxNumericFormatChars) and
    dxStringHasOneOfSymbols(AFormatCode, dxDateTimeFormatChars));
end;

class function TdxSpreadSheetNumberFormatDateTimeHelper.DoMergeBaseDateTimeResults(
  var APrimaryResult, ASecondaryResult: TdxUnicodeString; AFrom, ATo, AStep: Integer;
  AFirstResultChar, ASecondResultChar, AReplaceWithChar: TdxUnicodeChar): Integer;
var
  I: Integer;
begin
  I := AFrom;
  while True do
  begin
    if APrimaryResult[I] <> ASecondaryResult[I] then
    begin
      if (APrimaryResult[I] = AFirstResultChar) and (ASecondaryResult[I] = ASecondResultChar) then
        APrimaryResult[I] := AReplaceWithChar
      else
      begin
        Result := I;
        Exit;
      end;
    end;
    if I = ATo then
      Break;
    Inc(I, AStep);
  end;
  Result := ATo + 1;
end;

class function TdxSpreadSheetNumberFormatDateTimeHelper.RemoveLocaleIdPrefix(const AFormatCode: TdxUnicodeString): TdxUnicodeString;
var
  APos: Integer;
begin
  Result := AFormatCode;
  if dxStringStartsWith(Result, '[$') then
  begin
    APos := Pos(']', Result);
    if APos > 0 then
      Delete(Result, 1, APos);
  end;
end;

class function TdxSpreadSheetNumberFormatDateTimeHelper.RemoveMillisecondsSuffixes(const AFormatCode: TdxUnicodeString): TdxUnicodeString;
var
  APattern: TdxSpreadSheetNumberFormatPattern;
  I: Integer;
begin
  APattern := TdxSpreadSheetNumberFormatPattern.Create('\.0{1,3}');
  try
    APattern.Process(AFormatCode);

    Result := AFormatCode;
    for I := APattern.MatchCount - 1 downto 0 do
      Delete(Result, APattern.Matches[I].Position, APattern.Matches[I].Length);
  finally
    APattern.Free;
  end;
end;

{ TdxSpreadSheetNumberFormatSplitter }

class procedure TdxSpreadSheetNumberFormatSplitter.Split(const AFormatCode: TdxUnicodeString; AList: TStringList);
var
  AChar: TdxUnicodeChar;
  AHasBackSlash: Boolean;
  AHasQuotationMark: Boolean;
  ATemp: TdxUnicodeString;
  I: Integer;
begin
  if AFormatCode <> '' then
  begin
    AHasBackSlash := False;
    AHasQuotationMark := False;
    for I := 1 to Length(AFormatCode) do
    begin
      AChar := AFormatCode[I];
      case AChar of
        '"':
          if AHasBackSlash then
            AHasBackSlash := False
          else
            AHasQuotationMark := not AHasQuotationMark;

        '\':
          if not AHasQuotationMark then
            AHasBackSlash := not AHasBackSlash;

        ';':
          if AHasBackSlash then
            AHasBackSlash := False
          else
            if not AHasQuotationMark then
            begin
              AList.Add(ATemp);
              ATemp := EmptyStr;
              Continue;
            end;
      else
        AHasBackSlash := False;
      end;
      ATemp := ATemp + AChar;
    end;
    AList.Add(ATemp);
  end;
end;

{ TdxSpreadSheetPredefinedNumberFormat }

function TdxSpreadSheetPredefinedNumberFormat.GetIsCustom: Boolean;
begin
  Result := False;
end;

{ TdxSpreadSheetFullDateTimeNumberFormat }

constructor TdxSpreadSheetFullDateTimeNumberFormat.Create;
begin
  inherited Create('m/d/yy h:mm', 22); 
  FShortDateFormat := TdxSpreadSheetNumberFormat.Create('mm-dd-yy', 14);
end;

destructor TdxSpreadSheetFullDateTimeNumberFormat.Destroy;
begin
  FreeAndNil(FShortDateFormat); 
  inherited Destroy;
end;

procedure TdxSpreadSheetFullDateTimeNumberFormat.Format(
  const AValue: Variant; const AValueInfo: TdxSpreadSheetNumberFormatValueInfo; var AResult: TdxSpreadSheetNumberFormatResult);
var
  AProvider: TdxSpreadSheetSystemShortTimeNoTimeDesignatorFormatProvider;
  AProviderResult:  TdxSpreadSheetNumberFormatResult;
begin
  FShortDateFormat.Format(AValue, AValueInfo, AResult);
  if not AResult.IsError then
  begin
    AProvider := TdxSpreadSheetSystemShortTimeNoTimeDesignatorFormatProvider.Create;
    try
      AProvider.Format(AValue, AValueInfo, AProviderResult);
      AResult.Text := AResult.Text + ' ' + AProviderResult.Text;
    finally
      AProvider.Free;
    end;
  end;
end;

end.
