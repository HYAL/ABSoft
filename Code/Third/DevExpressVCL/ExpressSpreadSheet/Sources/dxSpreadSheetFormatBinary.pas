{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFormatBinary;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Types, Windows, SysUtils, Classes, Generics.Collections, dxCore, cxVariants,
  dxSpreadSheetCore, dxSpreadSheetTypes, dxSpreadSheetClasses;

const
  dxSpreadSheetBinaryFormatVersion = 4;

type

  { TdxSpreadSheetBinaryFormatHeader }

  TdxSpreadSheetBinaryFormatHeader = packed record
    ID: Int64;
    Version: Cardinal;
    procedure Initialize;
    function IsValid: Boolean;
  end;

  { TdxSpreadSheetBinaryFormat }

  TdxSpreadSheetBinaryFormat = class(TdxSpreadSheetCustomFormat)
  public
    class function CanReadFromStream(AStream: TStream): Boolean; override;
    class function CreateFormatSettings: TdxSpreadSheetFormatSettings; override;
    class function GetExt: string; override;
    class function GetReader: TdxSpreadSheetCustomReaderClass; override;
    class function GetWriter: TdxSpreadSheetCustomWriterClass; override;
  end;

  { TdxSpreadSheetBinaryReader }

  TdxSpreadSheetBinaryReader = class(TdxSpreadSheetCustomReader)
  strict private
    FCellStyles: TList<TdxSpreadSheetCellStyleHandle>;
    FFormulaRefs: TdxSpreadSheetFormulaAsTextInfoList;
    FReader: TcxReader;

    procedure CheckSectionHeader;
  protected
    function CreateProgressHelper: TdxSpreadSheetCustomFilerProgressHelper; override;
    procedure ReadCellStyles; virtual;
    procedure ReadDefinedNames; virtual;
    procedure ReadExternalLinks; virtual;
    procedure ReadProperties; virtual;
    //
    procedure ReadTableView(AView: TdxSpreadSheetTableView); virtual;
    procedure ReadTableViewCells(AView: TdxSpreadSheetTableView); virtual;
    procedure ReadTableViewItems(AItems: TdxSpreadSheetTableItems); virtual;
    procedure ReadTableViewMergedCells(AView: TdxSpreadSheetTableView); virtual;
    procedure ReadTableViewProperties(AView: TdxSpreadSheetTableView); virtual;
    procedure ReadTableViewSelection(AView: TdxSpreadSheetTableView); virtual;
    //
    procedure ReadView(AView: TdxSpreadSheetCustomView); virtual;
    procedure ReadViewContainers(AView: TdxSpreadSheetCustomView); virtual;
    procedure ReadViews; virtual;
    //
    property CellStyles: TList<TdxSpreadSheetCellStyleHandle> read FCellStyles;
    property Reader: TcxReader read FReader;
  public
    constructor Create(AOwner: TdxCustomSpreadSheet; AStream: TStream); override;
    destructor Destroy; override;
    procedure ReadData; override;
  end;

  { TdxSpreadSheetBinaryWriter }

  TdxSpreadSheetBinaryWriter = class(TdxSpreadSheetCustomWriter)
  strict private
    FCellStyles: TList<TdxSpreadSheetCellStyleHandle>;
    FWriter: TcxWriter;

    procedure WriteCount(const ACount: Integer; const ACountFieldPosition: Int64);
    procedure WriteNullCount(out ACount: Integer; out ACountFieldPosition: Int64);
  protected
    function CreateProgressHelper: TdxSpreadSheetCustomFilerProgressHelper; override;
    procedure WriteCellStyles; virtual;
    procedure WriteDefinedNames; virtual;
    procedure WriteExternalLinks; virtual;
    procedure WriteProperties; virtual;
    procedure WriteSectionHeader; virtual;

    procedure WriteTableView(AView: TdxSpreadSheetTableView); virtual;
    procedure WriteTableViewCells(AView: TdxSpreadSheetTableView); virtual;
    procedure WriteTableViewItems(AItems: TdxSpreadSheetTableItems); virtual;
    procedure WriteTableViewMergedCells(AView: TdxSpreadSheetTableView); virtual;
    procedure WriteTableViewProperties(AView: TdxSpreadSheetTableView); virtual;
    procedure WriteTableViewSelection(AView: TdxSpreadSheetTableView); virtual;

    procedure WriteView(AView: TdxSpreadSheetCustomView); virtual;
    procedure WriteViewContainer(AContainer: TdxSpreadSheetContainer); virtual;
    procedure WriteViewContainers(AView: TdxSpreadSheetCustomView); virtual;
    procedure WriteViews; virtual;
    //
    property CellStyles: TList<TdxSpreadSheetCellStyleHandle> read FCellStyles;
    property Writer: TcxWriter read FWriter;
  public
    constructor Create(AOwner: TdxCustomSpreadSheet; AStream: TStream); override;
    destructor Destroy; override;
    procedure WriteData; override;
  end;

implementation

uses
  dxHashUtils, dxSpreadSheetStrs, cxClasses, Math;

const
  dxSpreadSheetBinaryFormatHeaderID: Int64 = $4642327653535844; 
  dxSpreadSheetBinaryFormatSectionID = $20534642; 

type
  TdxDynamicItemListAccess = class(TdxDynamicItemList);
  TdxHashTableAccess = class(TdxHashTable);
  TdxSpreadSheetCellAccess = class(TdxSpreadSheetCell);
  TdxSpreadSheetContainerAccess = class(TdxSpreadSheetContainer);
  TdxSpreadSheetTableItemAccess = class(TdxSpreadSheetTableItem);
  TdxSpreadSheetTableRowAccess = class(TdxSpreadSheetTableRow);
  TdxSpreadSheetDefinedNameAccess = class(TdxSpreadSheetDefinedName);

{ TdxSpreadSheetBinaryFormatHeader }

procedure TdxSpreadSheetBinaryFormatHeader.Initialize;
begin
  ID := dxSpreadSheetBinaryFormatHeaderID;
  Version := dxSpreadSheetBinaryFormatVersion;
end;

function TdxSpreadSheetBinaryFormatHeader.IsValid: Boolean;
begin
  Result := (ID = dxSpreadSheetBinaryFormatHeaderID) and InRange(Version, 1, dxSpreadSheetBinaryFormatVersion);
end;

{ TdxSpreadSheetBinaryFormat }

class function TdxSpreadSheetBinaryFormat.CanReadFromStream(AStream: TStream): Boolean;
var
  AHeader: TdxSpreadSheetBinaryFormatHeader;
  ASavedPosition: Int64;
begin
  ASavedPosition := AStream.Position;
  try
    try
      AStream.ReadBuffer(AHeader, SizeOf(AHeader));
      Result := AHeader.IsValid;
    except
      Result := False;
    end;
  finally
    AStream.Position := ASavedPosition;
  end;
end;

class function TdxSpreadSheetBinaryFormat.CreateFormatSettings: TdxSpreadSheetFormatSettings;
begin
  Result := TdxSpreadSheetFormatSettings.Create;
end;

class function TdxSpreadSheetBinaryFormat.GetExt: string;
begin
  Result := '.bin';
end;

class function TdxSpreadSheetBinaryFormat.GetReader: TdxSpreadSheetCustomReaderClass;
begin
  Result := TdxSpreadSheetBinaryReader;
end;

class function TdxSpreadSheetBinaryFormat.GetWriter: TdxSpreadSheetCustomWriterClass;
begin
  Result := TdxSpreadSheetBinaryWriter;
end;

{ TdxSpreadSheetBinaryReader }

constructor TdxSpreadSheetBinaryReader.Create(AOwner: TdxCustomSpreadSheet; AStream: TStream);
begin
  inherited Create(AOwner, AStream);
  FFormulaRefs := TdxSpreadSheetFormulaAsTextInfoList.Create(SpreadSheet);
  FCellStyles := TList<TdxSpreadSheetCellStyleHandle>.Create;
  FReader := TcxReader.Create(AStream);
end;

destructor TdxSpreadSheetBinaryReader.Destroy;
begin
  FreeAndNil(FFormulaRefs);
  FreeAndNil(FCellStyles);
  FreeAndNil(FReader);
  inherited Destroy;
end;

procedure TdxSpreadSheetBinaryReader.ReadData;
var
  AHeader: TdxSpreadSheetBinaryFormatHeader;
begin
  Stream.ReadBuffer(AHeader, SizeOf(AHeader));
  if not AHeader.IsValid then
  begin
    DoError(cxGetResourceString(@sdxErrorUnsupportedDocumentFormat), ssmtError);
    Exit;
  end;

  Reader.Version := AHeader.Version;
  ReadProperties;
  ReadExternalLinks;
  ReadCellStyles;
  ReadViews;
  ReadDefinedNames;
end;

function TdxSpreadSheetBinaryReader.CreateProgressHelper: TdxSpreadSheetCustomFilerProgressHelper;
begin
  Result := TdxSpreadSheetCustomFilerProgressHelper.Create(Self, 3);
end;

procedure TdxSpreadSheetBinaryReader.ReadCellStyles;
var
  ACount: Integer;
  I: Integer;
begin
  CheckSectionHeader;
  ACount := Reader.ReadInteger;
  ProgressHelper.BeginStage(ACount);
  try
    for I := 0 to ACount - 1 do
    begin
      CellStyles.Add(SpreadSheet.CellStyles.CreateStyleFromStream(Reader));
      ProgressHelper.NextTask;
    end;
  finally
    ProgressHelper.EndStage;
  end;
end;

procedure TdxSpreadSheetBinaryReader.ReadProperties;
begin
  CheckSectionHeader;
  SpreadSheet.OptionsView.DateTimeSystem := TdxSpreadSheetDataTimeSystem(Reader.ReadInteger);
  SpreadSheet.OptionsView.R1C1Reference := Reader.ReadInteger <> 0;

  if Reader.Version > 2 then
  begin
    SpreadSheet.OptionsView.GridLines := Reader.ReadBoolean;
    SpreadSheet.OptionsView.ShowFormulas := Reader.ReadBoolean;
    SpreadSheet.OptionsView.ZeroValues := Reader.ReadBoolean;
    SpreadSheet.OptionsView.Headers := Reader.ReadBoolean;
    SpreadSheet.OptionsView.HorizontalScrollBar := Reader.ReadBoolean;
    SpreadSheet.OptionsView.VerticalScrollBar := Reader.ReadBoolean;

    SpreadSheet.OptionsBehavior.Protected := Reader.ReadBoolean;
    SpreadSheet.OptionsBehavior.IterativeCalculation := Reader.ReadBoolean;
    SpreadSheet.OptionsBehavior.IterativeCalculationMaxCount := Reader.ReadInteger;
  end;
end;

procedure TdxSpreadSheetBinaryReader.ReadTableView(AView: TdxSpreadSheetTableView);
begin
  ReadTableViewProperties(AView);
  ReadTableViewItems(AView.Columns);
  ReadTableViewItems(AView.Rows);
  ReadTableViewCells(AView);
  ReadTableViewMergedCells(AView);
  ReadTableViewSelection(AView);
end;

procedure TdxSpreadSheetBinaryReader.ReadTableViewCells(AView: TdxSpreadSheetTableView);
var
  ACell: TdxSpreadSheetCellAccess;
  ACellCount: Integer;
  ARow: TdxSpreadSheetTableRow;
  ARowCount: Integer;
begin
  ARowCount := Reader.ReadInteger;
  ProgressHelper.BeginStage(ARowCount);
  try
    while ARowCount > 0 do
    begin
      ARow := AView.Rows.CreateItem(Reader.ReadInteger);

      ACellCount := Reader.ReadInteger;
      while ACellCount > 0 do
      begin
        ACell := TdxSpreadSheetCellAccess(ARow.CreateCell(Reader.ReadInteger));
        ACell.LoadFromStream(Reader, CellStyles[Reader.ReadInteger], FFormulaRefs);
        Dec(ACellCount);
      end;

      ProgressHelper.NextTask;
      Dec(ARowCount);
    end;
  finally
    ProgressHelper.EndStage;
  end;
end;

procedure TdxSpreadSheetBinaryReader.ReadTableViewItems(AItems: TdxSpreadSheetTableItems);
var
  ACount: Integer;
  AItem: TdxSpreadSheetTableItemAccess;
begin
  CheckSectionHeader;
  AItems.DefaultSize := Reader.ReadInteger;
  ACount := Reader.ReadInteger;
  while ACount > 0 do
  begin
    AItem := TdxSpreadSheetTableItemAccess(AItems.CreateItem(Reader.ReadInteger));
    AItem.Style.Handle := CellStyles[Reader.ReadInteger];
    AItem.Visible := Reader.ReadBoolean;
    if not Reader.ReadBoolean then
      AItem.Size := Reader.ReadInteger;
    if Reader.Version > 1 then
      AItem.IsCustomSize := Reader.ReadBoolean;
    Dec(ACount);
  end;
end;

procedure TdxSpreadSheetBinaryReader.ReadTableViewMergedCells(AView: TdxSpreadSheetTableView);
var
  ACount: Integer;
begin
  ACount := Reader.ReadInteger;
  while ACount > 0 do
  begin
    AView.MergedCells.Add(Reader.ReadRect);
    Dec(ACount);
  end;
end;

procedure TdxSpreadSheetBinaryReader.ReadTableViewProperties(AView: TdxSpreadSheetTableView);
begin
  AView.FrozenColumn := Reader.ReadInteger;
  AView.FrozenRow := Reader.ReadInteger;
  AView.Options.ZoomFactor := Reader.ReadInteger;
  AView.Options.GridLines := TdxDefaultBoolean(Reader.ReadInteger);
  AView.Options.ShowFormulas := TdxDefaultBoolean(Reader.ReadInteger);
  AView.Options.ZeroValues := TdxDefaultBoolean(Reader.ReadInteger);

  if Reader.Version > 2 then
  begin
    AView.Options.DefaultColumnWidth := Reader.ReadInteger;
    AView.Options.DefaultRowHeight := Reader.ReadInteger;
    AView.Options.Headers := TdxDefaultBoolean(Reader.ReadInteger);
    AView.Options.HorizontalScrollBar := TdxDefaultBoolean(Reader.ReadInteger);
    AView.Options.Protected := Reader.ReadBoolean;
    AView.Options.VerticalScrollBar := TdxDefaultBoolean(Reader.ReadInteger);
  end;
end;

procedure TdxSpreadSheetBinaryReader.ReadTableViewSelection(AView: TdxSpreadSheetTableView);
var
  ACount: Integer;
begin
  ACount := Reader.ReadInteger;
  while ACount > 0 do
  begin
    AView.Selection.Add(Reader.ReadRect, [ssCtrl]);
    Dec(ACount);
  end;
end;

procedure TdxSpreadSheetBinaryReader.ReadView(AView: TdxSpreadSheetCustomView);
begin
  AView.Visible := Reader.ReadBoolean;
  if AView is TdxSpreadSheetTableView then
    ReadTableView(TdxSpreadSheetTableView(AView));
  ReadViewContainers(AView);
end;

procedure TdxSpreadSheetBinaryReader.ReadViewContainers(AView: TdxSpreadSheetCustomView);
var
  AClass: TClass;
  ACount: Integer;
begin
  ACount := Reader.ReadInteger;
  while ACount > 0 do
  begin
    AClass := GetClass(Reader.ReadWideString);
    if (AClass = nil) or not AClass.InheritsFrom(TdxSpreadSheetContainer) then
    begin
      DoError(cxGetResourceString(@sdxErrorUnsupportedSheetType), ssmtError);
      Abort;
    end;
    TdxSpreadSheetContainerAccess(AView.Containers.Add(TdxSpreadSheetContainerClass(AClass))).LoadFromStream(Reader);
    Dec(ACount);
  end;
end;

procedure TdxSpreadSheetBinaryReader.ReadDefinedNames;
var
  ACount: Integer;
begin
  CheckSectionHeader;
  ACount := Reader.ReadInteger;
  while ACount > 0 do
  begin
    SpreadSheet.DefinedNames.AddFromStream(Reader);
    Dec(ACount);
  end;
end;

procedure TdxSpreadSheetBinaryReader.ReadExternalLinks;
var
  ACount: Integer;
begin
  CheckSectionHeader;
  ACount := Reader.ReadInteger;
  while ACount > 0 do
  begin
    SpreadSheet.ExternalLinks.Add(Reader.ReadWideString);
    Dec(ACount);
  end;
end;

procedure TdxSpreadSheetBinaryReader.ReadViews;
var
  AClass: TClass;
  ACount: Integer;
begin
  CheckSectionHeader;
  ACount := Reader.ReadInteger;
  ProgressHelper.BeginStage(ACount);
  try
    while ACount > 0 do
    begin
      CheckSectionHeader;

      AClass := GetClass(Reader.ReadWideString);
      if (AClass = nil) or not AClass.InheritsFrom(TdxSpreadSheetCustomView) then
      begin
        DoError(cxGetResourceString(@sdxErrorUnsupportedSheetType), ssmtError);
        Abort;
      end;

      ReadView(SpreadSheet.AddSheet(Reader.ReadWideString, TdxSpreadSheetCustomViewClass(AClass)));
      ProgressHelper.NextTask;
      Dec(ACount);
    end;
    FFormulaRefs.ResolveReferences;
    FFormulaRefs.Clear;
  finally
    ProgressHelper.EndStage;
  end;
  SpreadSheet.ActiveSheetIndex := Reader.ReadInteger;
end;

procedure TdxSpreadSheetBinaryReader.CheckSectionHeader;
begin
  if Reader.ReadCardinal <> dxSpreadSheetBinaryFormatSectionID then
  begin
    DoError(cxGetResourceString(@sdxErrorDocumentIsCorrupted), ssmtError);
    Abort;
  end;
end;

{ TdxSpreadSheetBinaryWriter }

constructor TdxSpreadSheetBinaryWriter.Create(AOwner: TdxCustomSpreadSheet; AStream: TStream);
begin
  inherited Create(AOwner, AStream);
  FCellStyles := TList<TdxSpreadSheetCellStyleHandle>.Create;
  FWriter := TcxWriter.Create(AStream);
end;

destructor TdxSpreadSheetBinaryWriter.Destroy;
begin
  FreeAndNil(FCellStyles);
  FreeAndNil(FWriter);
  inherited Destroy;
end;

procedure TdxSpreadSheetBinaryWriter.WriteData;
var
  AHeader: TdxSpreadSheetBinaryFormatHeader;
begin
  AHeader.Initialize;
  Writer.Version := AHeader.Version;
  Stream.WriteBuffer(AHeader, SizeOf(AHeader));

  WriteProperties;
  WriteExternalLinks;
  WriteCellStyles;
  WriteViews;
  WriteDefinedNames;
end;

function TdxSpreadSheetBinaryWriter.CreateProgressHelper: TdxSpreadSheetCustomFilerProgressHelper;
begin
  Result := TdxSpreadSheetCustomFilerProgressHelper.Create(Self, 1);
end;

procedure TdxSpreadSheetBinaryWriter.WriteCellStyles;
var
  I: Integer;
begin
  WriteSectionHeader;

  CellStyles.Capacity := 256;
  CellStyles.Add(SpreadSheet.CellStyles.DefaultStyle);
  TdxHashTableAccess(SpreadSheet.CellStyles).ForEach(
    procedure (AItem: TdxDynamicListItem; AData: Pointer)
    begin
      CellStyles.Add(TdxSpreadSheetCellStyleHandle(AItem));
    end);

  Writer.WriteInteger(CellStyles.Count);
  for I := 0 to CellStyles.Count - 1 do
    CellStyles.Items[I].SaveToStream(Writer);
end;

procedure TdxSpreadSheetBinaryWriter.WriteDefinedNames;
var
  I: Integer;
begin
  WriteSectionHeader;
  Writer.WriteInteger(SpreadSheet.DefinedNames.Count);
  for I := 0 to SpreadSheet.DefinedNames.Count - 1 do
    TdxSpreadSheetDefinedNameAccess(SpreadSheet.DefinedNames[I]).SaveToStream(Writer);
end;

procedure TdxSpreadSheetBinaryWriter.WriteExternalLinks;
var
  I: Integer;
begin
  WriteSectionHeader;
  Writer.WriteInteger(SpreadSheet.ExternalLinks.Count);
  for I := 0 to SpreadSheet.ExternalLinks.Count - 1 do
    Writer.WriteWideString(SpreadSheet.ExternalLinks[I].Target);
end;

procedure TdxSpreadSheetBinaryWriter.WriteProperties;
begin
  WriteSectionHeader;
  Writer.WriteInteger(Ord(SpreadSheet.OptionsView.DateTimeSystem));
  Writer.WriteInteger(Ord(SpreadSheet.OptionsView.R1C1Reference));

  Writer.WriteBoolean(SpreadSheet.OptionsView.GridLines);
  Writer.WriteBoolean(SpreadSheet.OptionsView.ShowFormulas);
  Writer.WriteBoolean(SpreadSheet.OptionsView.ZeroValues);
  Writer.WriteBoolean(SpreadSheet.OptionsView.Headers);
  Writer.WriteBoolean(SpreadSheet.OptionsView.HorizontalScrollBar);
  Writer.WriteBoolean(SpreadSheet.OptionsView.VerticalScrollBar);

  Writer.WriteBoolean(SpreadSheet.OptionsBehavior.Protected);
  Writer.WriteBoolean(SpreadSheet.OptionsBehavior.IterativeCalculation);
  Writer.WriteInteger(SpreadSheet.OptionsBehavior.IterativeCalculationMaxCount);
end;

procedure TdxSpreadSheetBinaryWriter.WriteSectionHeader;
begin
  Writer.WriteCardinal(dxSpreadSheetBinaryFormatSectionID);
end;

procedure TdxSpreadSheetBinaryWriter.WriteTableView(AView: TdxSpreadSheetTableView);
begin
  WriteTableViewProperties(AView);
  WriteTableViewItems(AView.Columns);
  WriteTableViewItems(AView.Rows);
  WriteTableViewCells(AView);
  WriteTableViewMergedCells(AView);
  WriteTableViewSelection(AView);
end;

procedure TdxSpreadSheetBinaryWriter.WriteTableViewCells(AView: TdxSpreadSheetTableView);
var
  ARowCount: Integer;
  ARowCountPosition: Int64;
begin
  WriteNullCount(ARowCount, ARowCountPosition);

  TdxDynamicItemListAccess(AView.Rows).ForEach(
    procedure (Item: TdxDynamicListItem; Data: Pointer)
    var
      ACellCount: Integer;
      ACellCountPosition: Int64;
    begin
      Writer.WriteInteger(Item.Index);
      WriteNullCount(ACellCount, ACellCountPosition);

      TdxDynamicItemListAccess(TdxSpreadSheetTableRowAccess(Item).RowCells).ForEach(
        procedure (Item: TdxDynamicListItem; Data: Pointer)
        var
          ACell: TdxSpreadSheetCellAccess;
        begin
          ACell := TdxSpreadSheetCellAccess(Item);
          Writer.WriteInteger(ACell.ColumnIndex);
          ACell.SaveToStream(Writer, CellStyles.IndexOf(ACell.Style.Handle));
          Inc(ACellCount);
        end);

      WriteCount(ACellCount, ACellCountPosition);
      Inc(ARowCount);
    end);

  WriteCount(ARowCount, ARowCountPosition);
end;

procedure TdxSpreadSheetBinaryWriter.WriteTableViewItems(AItems: TdxSpreadSheetTableItems);
var
  ACount: Integer;
  ACountPosition: Int64;
begin
  WriteSectionHeader;
  Writer.WriteInteger(AItems.DefaultSize);
  WriteNullCount(ACount, ACountPosition);

  TdxDynamicItemListAccess(AItems).ForEach(
    procedure (Item: TdxDynamicListItem; Data: Pointer)
    var
      AItem: TdxSpreadSheetTableItemAccess;
    begin
      AItem := TdxSpreadSheetTableItemAccess(Item);
      Writer.WriteInteger(AItem.Index);
      Writer.WriteInteger(CellStyles.IndexOf(AItem.Style.Handle));
      Writer.WriteBoolean(AItem.Visible);
      Writer.WriteBoolean(AItem.DefaultSize);
      if not AItem.DefaultSize then
        Writer.WriteInteger(AItem.Size);
      Writer.WriteBoolean(AItem.IsCustomSize);
      Inc(ACount);
    end);

  WriteCount(ACount, ACountPosition);
end;

procedure TdxSpreadSheetBinaryWriter.WriteTableViewMergedCells(AView: TdxSpreadSheetTableView);
var
  I: Integer;
begin
  Writer.WriteInteger(AView.MergedCells.Count);
  for I := 0 to AView.MergedCells.Count - 1 do
    Writer.WriteRect(AView.MergedCells.Items[I].Area);
end;

procedure TdxSpreadSheetBinaryWriter.WriteTableViewProperties(AView: TdxSpreadSheetTableView);
begin
  Writer.WriteInteger(AView.FrozenColumn);
  Writer.WriteInteger(AView.FrozenRow);
  Writer.WriteInteger(AView.Options.ZoomFactor);
  Writer.WriteInteger(Ord(AView.Options.GridLines));
  Writer.WriteInteger(Ord(AView.Options.ShowFormulas));
  Writer.WriteInteger(Ord(AView.Options.ZeroValues));

  Writer.WriteInteger(AView.Options.DefaultColumnWidth);
  Writer.WriteInteger(AView.Options.DefaultRowHeight);
  Writer.WriteInteger(Ord(AView.Options.Headers));
  Writer.WriteInteger(Ord(AView.Options.HorizontalScrollBar));
  Writer.WriteBoolean(AView.Options.Protected);
  Writer.WriteInteger(Ord(AView.Options.VerticalScrollBar));
end;

procedure TdxSpreadSheetBinaryWriter.WriteTableViewSelection(AView: TdxSpreadSheetTableView);
var
  I: Integer;
begin
  Writer.WriteInteger(AView.Selection.Count);
  for I := 0 to AView.Selection.Count - 1 do
    Writer.WriteRect(AView.Selection[I].Rect);
end;

procedure TdxSpreadSheetBinaryWriter.WriteView(AView: TdxSpreadSheetCustomView);
begin
  WriteSectionHeader;
  Writer.WriteWideString(AView.ClassName);
  Writer.WriteWideString(AView.Caption);
  Writer.WriteBoolean(AView.Visible);
  if AView is TdxSpreadSheetTableView then
    WriteTableView(TdxSpreadSheetTableView(AView));
  WriteViewContainers(AView);
end;

procedure TdxSpreadSheetBinaryWriter.WriteViewContainer(AContainer: TdxSpreadSheetContainer);
begin
  Writer.WriteWideString(AContainer.ClassName);
  TdxSpreadSheetContainerAccess(AContainer).SaveToStream(Writer);
end;

procedure TdxSpreadSheetBinaryWriter.WriteViewContainers(AView: TdxSpreadSheetCustomView);
var
  I: Integer;
begin
  Writer.WriteInteger(AView.Containers.Count);
  for I := 0 to AView.Containers.Count - 1 do
    WriteViewContainer(AView.Containers[I]);
end;

procedure TdxSpreadSheetBinaryWriter.WriteViews;
var
  I: Integer;
begin
  WriteSectionHeader;
  Writer.WriteInteger(SpreadSheet.SheetCount);

  ProgressHelper.BeginStage(SpreadSheet.SheetCount);
  try
    for I := 0 to SpreadSheet.SheetCount - 1 do
    begin
      WriteView(SpreadSheet.Sheets[I]);
      ProgressHelper.NextTask;
    end;
  finally
    ProgressHelper.EndStage;
  end;

  Writer.WriteInteger(SpreadSheet.ActiveSheetIndex);
end;

procedure TdxSpreadSheetBinaryWriter.WriteCount(const ACount: Integer; const ACountFieldPosition: Int64);
var
  APosition: Int64;
begin
  APosition := Writer.Stream.Position;
  Writer.Stream.Position := ACountFieldPosition;
  Writer.WriteInteger(ACount);
  Writer.Stream.Position := APosition;
end;

procedure TdxSpreadSheetBinaryWriter.WriteNullCount(out ACount: Integer; out ACountFieldPosition: Int64);
begin
  ACount := 0;
  ACountFieldPosition := Writer.Stream.Position;
  Writer.WriteInteger(ACount);
end;

initialization
  TdxSpreadSheetBinaryFormat.Register;

finalization
  TdxSpreadSheetBinaryFormat.Unregister;
end.
