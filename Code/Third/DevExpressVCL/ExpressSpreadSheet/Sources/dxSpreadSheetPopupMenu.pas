{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetPopupMenu;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Types, Classes, Graphics, ImgList, Menus, cxControls, cxGraphics,
  dxSpreadSheetCore, dxSpreadSheetUtils, dxSpreadSheetTypes, dxBuiltInPopupMenu;

{$R dxSpreadSheetPopupMenu.res}

type
  { TdxSpreadSheetCustomPopupMenu }

  TdxSpreadSheetCustomPopupMenu = class(TComponent)
  strict private
    FAdapter: TdxCustomBuiltInPopupMenuAdapter;
    FImageList: TcxImageList;

    function GetSpreadSheet: TdxCustomSpreadSheet; inline;
  protected
    function AddImage(const AResourceName: string): Integer;
    procedure MenuItemClick(Sender: TObject);
    procedure PopulateMenuItems; virtual; abstract;
    procedure ProcessClick(ACommand: Integer); virtual; abstract;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function Popup(const P: TPoint): Boolean;

    property Adapter: TdxCustomBuiltInPopupMenuAdapter read FAdapter;
    property ImageList: TcxImageList read FImageList;
    property SpreadSheet: TdxCustomSpreadSheet read GetSpreadSheet;
  end;

  { TdxSpreadSheetBuiltInPageControlTabPopupMenu }

  TdxSpreadSheetBuiltInPageControlTabPopupMenu = class(TdxSpreadSheetCustomPopupMenu)
  strict private const
    ccDelete = 0;
    ccHide = 1;
    ccInsert = 2;
    ccRename = 3;
    ccUnhide = 4;
  protected
    function HasHiddenTabs: Boolean; virtual;
    function IsLastVisibleTab: Boolean; virtual;
    procedure Insert; virtual;
    procedure Rename; virtual;

    procedure PopulateMenuItems; override;
    procedure ProcessClick(ACommand: Integer); override;
  end;

  { TdxSpreadSheetBuiltInTableViewPopupMenu }

  TdxSpreadSheetBuiltInTableViewPopupMenu = class(TdxSpreadSheetCustomPopupMenu)
  strict private const
    ccClearContent = 0;
    ccCopy = 1;
    ccCut = 2;
    ccDelete = 3;
    ccFormatCells = 4;
    ccHide = 5;
    ccInsert = 6;
    ccMerge = 8;
    ccPaste = 9;
    ccSplit = 10;
    ccUnhide = 11;
    ccBringToFront = 12;
    ccSendToBack = 13;
    ccCustomizeObject = 14;
  strict private
    function GetFocusedContainer: TdxSpreadSheetContainer;
    function GetView: TdxSpreadSheetTableView; inline;
    function IsSelectionContainsEntireRowOrColumn: Boolean;
  protected
    procedure ChangeVisibility(AVisibility: Boolean); virtual;
    procedure Delete; virtual;
    procedure Insert; virtual;

    procedure PopulateMenuItems; override;
    procedure PopulateMenuItemsForCells; virtual;
    procedure PopulateMenuItemsForContainer; virtual;
    procedure ProcessClick(ACommand: Integer); override;
  public
    property FocusedContainer: TdxSpreadSheetContainer read GetFocusedContainer;
    property View: TdxSpreadSheetTableView read GetView;
  end;

implementation

uses
  SysUtils, dxGDIPlusClasses, cxClasses, dxSpreadSheetStrs, dxSpreadSheetFormatCellsDialog, dxHashUtils, Math,
  dxSpreadSheetContainerCustomizationDialog, Dialogs, dxSpreadSheetUnhideSheetDialog;

type
  TdxSpreadSheetTableItemsAccess = class(TdxSpreadSheetTableItems);
  TdxSpreadSheetTableViewAccess = class(TdxSpreadSheetTableView);

{ TdxSpreadSheetCustomPopupMenu }

constructor TdxSpreadSheetCustomPopupMenu.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FImageList := TcxImageList.Create(Self);
  FAdapter := TdxBuiltInPopupMenuAdapterManager.GetActualAdapterClass.Create(Self);
  FAdapter.SetImages(ImageList);
end;

destructor TdxSpreadSheetCustomPopupMenu.Destroy;
begin
  FreeAndNil(FAdapter);
  FreeAndNil(FImageList);
  inherited Destroy;
end;

function TdxSpreadSheetCustomPopupMenu.Popup(const P: TPoint): Boolean;
begin
  Adapter.Clear;
  Adapter.SetLookAndFeel(SpreadSheet.LookAndFeel);
  PopulateMenuItems;
  Result := Adapter.Popup(SpreadSheet.ClientToScreen(P));
end;

function TdxSpreadSheetCustomPopupMenu.AddImage(const AResourceName: string): Integer;
var
  ABitmap: TBitmap;
  APNGImage: TdxPNGImage;
begin
  APNGImage := TdxPNGImage.Create;
  try
    APNGImage.LoadFromResource(HInstance, AResourceName, 'PNG');
    ABitmap := APNGImage.GetAsBitmap;
    try
      Result := ImageList.Add(ABitmap, nil);
    finally
      ABitmap.Free;
    end;
  finally
    APNGImage.Free;
  end;
end;

procedure TdxSpreadSheetCustomPopupMenu.MenuItemClick(Sender: TObject);
begin
  ProcessClick(TComponent(Sender).Tag);
end;

function TdxSpreadSheetCustomPopupMenu.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := Owner as TdxCustomSpreadSheet;
end;

{ TdxSpreadSheetBuiltInPageControlTabPopupMenu }

function TdxSpreadSheetBuiltInPageControlTabPopupMenu.HasHiddenTabs: Boolean;
begin
  Result := SpreadSheet.VisibleSheetCount <> SpreadSheet.SheetCount;
end;

function TdxSpreadSheetBuiltInPageControlTabPopupMenu.IsLastVisibleTab: Boolean;
begin
  Result := SpreadSheet.VisibleSheetCount = 1;
end;

procedure TdxSpreadSheetBuiltInPageControlTabPopupMenu.Insert;
var
  ASheet: TdxSpreadSheetCustomView;
begin
  ASheet := SpreadSheet.AddSheet;
  ASheet.Index := SpreadSheet.ActiveSheetIndex;
  ASheet.Active := True;
end;

procedure TdxSpreadSheetBuiltInPageControlTabPopupMenu.Rename;
var
  AValue: string;
begin
  AValue := SpreadSheet.ActiveSheet.Caption;
  while True do
  try
    if InputQuery(cxGetResourceString(@sdxRenameDialogCaption), cxGetResourceString(@sdxRenameDialogSheetName), AValue) then
      SpreadSheet.ActiveSheet.Caption := AValue;
    Break;
  except
    MessageDlg(cxGetResourceString(@sdxErrorCannotRenameSheet), mtWarning, [mbOK], 0);
  end;
end;

procedure TdxSpreadSheetBuiltInPageControlTabPopupMenu.PopulateMenuItems;
begin
  Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuInsert),
    MenuItemClick, ccInsert, AddImage('DXSPREADSHEET_POPUPMENU_GLYPH_INSERTSHEET'));
  Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuDelete),
    MenuItemClick, ccDelete, AddImage('DXSPREADSHEET_POPUPMENU_GLYPH_REMOVESHEET'), not IsLastVisibleTab);
  Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuRename), MenuItemClick, ccRename);
  Adapter.AddSeparator;
  Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuHide), MenuItemClick, ccHide, -1, not IsLastVisibleTab);
  Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuUnhideDialog), MenuItemClick, ccUnhide, -1, HasHiddenTabs);
end;

procedure TdxSpreadSheetBuiltInPageControlTabPopupMenu.ProcessClick(ACommand: Integer);
begin
  case ACommand of
    ccInsert:
      Insert;
    ccRename:
      Rename;
    ccDelete:
      SpreadSheet.ActiveSheet.Free;
    ccHide:
      SpreadSheet.ActiveSheet.Visible := False;
    ccUnhide:
      ShowUnhideSheetDialog(SpreadSheet);
  end;
end;

{ TdxSpreadSheetBuiltInTableViewPopupMenu }

procedure TdxSpreadSheetBuiltInTableViewPopupMenu.PopulateMenuItems;
begin
  Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuCut), MenuItemClick,
    ccCut, AddImage('DXSPREADSHEET_POPUPMENU_GLYPH_CUT'), View.CanCutToClipboard);
  Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuCopy), MenuItemClick,
    ccCopy, AddImage('DXSPREADSHEET_POPUPMENU_GLYPH_COPY'), View.CanCopyToClipboard);
  Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuPaste), MenuItemClick,
    ccPaste, AddImage('DXSPREADSHEET_POPUPMENU_GLYPH_PASTE'), View.CanPasteFromClipboard);
  Adapter.AddSeparator;

  if FocusedContainer = nil then
    PopulateMenuItemsForCells
  else
    PopulateMenuItemsForContainer
end;

procedure TdxSpreadSheetBuiltInTableViewPopupMenu.PopulateMenuItemsForCells;
const
  DeleteCommandCaption: array[Boolean] of Pointer = (
    @sdxBuiltInPopupMenuDelete, @sdxBuiltInPopupMenuDeleteDialog
  );
  InsertCommandCaption: array[Boolean] of Pointer = (
    @sdxBuiltInPopupMenuInsert, @sdxBuiltInPopupMenuInsertDialog
  );
var
  AIsSelectionContainsEntireRowOrColumn: Boolean;
begin
  if SpreadSheet.OptionsBehavior.Editing then
  begin
    AIsSelectionContainsEntireRowOrColumn := IsSelectionContainsEntireRowOrColumn;

    Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuMergeCells), MenuItemClick,
      ccMerge, AddImage('DXSPREADSHEET_POPUPMENU_GLYPH_MERGE'), View.CanMergeSelected);
    Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuSplitCells), MenuItemClick,
      ccSplit, AddImage('DXSPREADSHEET_POPUPMENU_GLYPH_UNMERGE'), View.CanSplitSelected);
    Adapter.AddSeparator;

    if SpreadSheet.OptionsBehavior.Inserting then
      Adapter.Add(cxGetResourceString(InsertCommandCaption[not AIsSelectionContainsEntireRowOrColumn]),
        MenuItemClick, ccInsert, -1, View.CanInsert);

    if SpreadSheet.OptionsBehavior.Deleting then
      Adapter.Add(cxGetResourceString(DeleteCommandCaption[not AIsSelectionContainsEntireRowOrColumn]),
        MenuItemClick, ccDelete, -1, View.CanDelete);

    Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuClearContents), MenuItemClick, ccClearContent, -1, View.CanClearCells);

    if SpreadSheet.OptionsBehavior.Formatting then
    begin
      Adapter.AddSeparator;
      Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuFormatCells), MenuItemClick,
        ccFormatCells, AddImage('DXSPREADSHEET_POPUPMENU_GLYPH_FORMATCELLS'));
    end;

    if AIsSelectionContainsEntireRowOrColumn then
    begin
      Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuHide), MenuItemClick, ccHide);
      Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuUnhide), MenuItemClick, ccUnhide);
    end;
  end;
end;

procedure TdxSpreadSheetBuiltInTableViewPopupMenu.PopulateMenuItemsForContainer;
begin
  Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuBringToFront), MenuItemClick,
    ccBringToFront, AddImage('DXSPREADSHEET_POPUPMENU_GLYPH_BRINGTOFRONT'), View.Containers.Count > 1);
  Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuSendToBack), MenuItemClick,
    ccSendToBack, AddImage('DXSPREADSHEET_POPUPMENU_GLYPH_SENDTOBACK'), View.Containers.Count > 1);
  Adapter.AddSeparator;

  Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuDelete), MenuItemClick, ccDelete, -1, View.CanDelete);
  Adapter.AddSeparator;

  Adapter.Add(cxGetResourceString(@sdxBuiltInPopupMenuCustomizeObject), MenuItemClick, ccCustomizeObject);
end;

procedure TdxSpreadSheetBuiltInTableViewPopupMenu.ProcessClick(ACommand: Integer);
begin
  case ACommand of
    ccCut:
      View.CutToClipboard;
    ccCopy:
      View.CopyToClipboard;
    ccPaste:
      View.PasteFromClipboard;
    ccSplit:
      View.SplitSelected;
    ccMerge:
      View.MergeSelected;
    ccClearContent:
      View.ClearCellsValues;
    ccDelete:
      Delete;
    ccInsert:
      Insert;
    ccFormatCells:
      ShowFormatCellsDialog(View);
    ccHide:
      ChangeVisibility(False);
    ccUnhide:
      ChangeVisibility(True);
    ccBringToFront:
      FocusedContainer.BringToFront;
    ccSendToBack:
      FocusedContainer.SendToBack;
    ccCustomizeObject:
      ShowContainerCustomizationDialog(FocusedContainer);
  end;
end;

procedure TdxSpreadSheetBuiltInTableViewPopupMenu.ChangeVisibility(AVisibility: Boolean);

  procedure DoChangeItemsVisibility(AItems: TdxSpreadSheetTableItems; AStartIndex, AFinishIndex: Integer);
  var
    I: Integer;
  begin
    if AVisibility then
    begin
      TdxSpreadSheetTableItemsAccess(AItems).ForEach(
        procedure (AItem: TdxDynamicListItem; AData: Pointer)
        begin
          TdxSpreadSheetTableItem(AItem).Visible := True;
        end,
        AStartIndex, AFinishIndex);
    end
    else
      for I := AStartIndex to AFinishIndex do
        AItems.CreateItem(I).Visible := AVisibility;
  end;

  procedure DoChangeVisibilityInArea(const AArea: TRect);
  begin
    if dxSpreadSheetIsEntireRow(AArea) then
      DoChangeItemsVisibility(View.Rows, AArea.Top, AArea.Bottom)
    else
      if dxSpreadSheetIsEntireColumn(AArea) then
        DoChangeItemsVisibility(View.Columns, AArea.Left, AArea.Right);
  end;

var
  AAreaIndex: Integer;
  ASelection: TdxSpreadSheetTableViewSelection;
begin
  View.BeginUpdate;
  try
    ASelection := View.Selection;
    for AAreaIndex := 0 to ASelection.Count - 1 do
      DoChangeVisibilityInArea(dxSpreadSheetGetRealArea(ASelection[AAreaIndex].Rect));
  finally
    View.EndUpdate;
  end;
end;

procedure TdxSpreadSheetBuiltInTableViewPopupMenu.Delete;
begin
  if FocusedContainer <> nil then
    FocusedContainer.Free
  else
    View.DeleteCells;
end;

procedure TdxSpreadSheetBuiltInTableViewPopupMenu.Insert;
begin
  View.InsertCells;
end;

function TdxSpreadSheetBuiltInTableViewPopupMenu.GetFocusedContainer: TdxSpreadSheetContainer;
begin
  Result := TdxSpreadSheetTableViewAccess(View).Controller.FocusedContainer;
end;

function TdxSpreadSheetBuiltInTableViewPopupMenu.GetView: TdxSpreadSheetTableView;
begin
  Result := SpreadSheet.ActiveSheetAsTable;
end;

function TdxSpreadSheetBuiltInTableViewPopupMenu.IsSelectionContainsEntireRowOrColumn: Boolean;
begin
  Result := (FocusedContainer = nil) and (dxSpreadSheetIsEntireColumn(View.Selection.Area) or
    dxSpreadSheetIsEntireRow(View.Selection.Area));
end;

end.
