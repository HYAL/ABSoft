{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFormatXLSFormulas;

{$I cxVer.inc}

interface

uses
  Types, Classes, SysUtils, Dialogs, Math, dxCore, cxClasses, Generics.Collections, dxSpreadSheetFormatXLS,
  dxSpreadSheetFormatXLSTypes, dxSpreadSheetCore, dxSpreadSheetFormulas, dxSpreadSheetTypes, dxSpreadSheetFunctions;

type
  TdxXLSTokenReaderProc = procedure of object;

  TdxSpreadSheetFormulaAccess = class(TdxSpreadSheetFormula);
  TdxSpreadSheetDefinedNameAccess = class(TdxSpreadSheetDefinedName);

  {TdxXLSFormulaReader}

  TdxXLSFormulaReader = class
  private
    FArrays: TList<TdxSpreadSheetFormulaArrayToken>;
    FCell: TdxSpreadSheetCell;
    FCurrentToken: Byte;
    FFormula: TdxSpreadSheetFormulaAccess;
    FIsBreak: Boolean;
    FIsError: Boolean;
    FOffset: Word;
    FOwner: TdxSpreadSheetXLSReader;
    FSize: Integer;
    FSource: PByteArray;
    FSourceOffset: Integer;
    FTokenReaders: TList<TdxXLSTokenReaderProc>;
    FTokens: TList<TdxSpreadSheetFormulaToken>;
    function GetColumn: Integer; {$IFNDEF VER220} inline; {$ENDIF}
    function GetReader: TdxXLSReader; inline;
    function GetRecordReader: TdxXLSRecordReader; inline;
    function GetRow: Integer; {$IFNDEF VER220} inline; {$ENDIF}
    function GetWords(AIndex: Integer): Word; inline;
  protected
    procedure AddFormula(AInfo: TdxSpreadSheetFunctionInfo; AParams: TdxSpreadSheetFormulaToken);
    procedure AddReader(AToken: Byte; AReader: TdxXLSTokenReaderProc);
    procedure AddToken(AToken: TdxSpreadSheetFormulaToken; AIncOffset: Integer = 0);
    procedure DecodeAreaReference(ASrcRow, ASrcColumn, ASrcRow2, ASrcColumn2: Word;
      var ARow, AColumn, ARow2, AColumn2: Integer; var AAbsRow, AAbsColumn, AAbsRow2, AAbsColumn2: Boolean); inline;
    procedure DecodeAreaReference2(ASrcRow, ASrcColumn, ASrcRow2, ASrcColumn2: Word;
      var ARow, AColumn, ARow2, AColumn2: Integer; var AAbsRow, AAbsColumn, AAbsRow2, AAbsColumn2: Boolean); inline;
    procedure DecodeReference(ASrcRow, ASrcColumn: Word;
      var ARow, AColumn: Integer; var AAbsRow, AAbsColumn: Boolean); inline;
    procedure DecodeReference2(ASrcRow, ASrcColumn: Word;
      var ARow, AColumn: Integer; var AAbsRow, AAbsColumn: Boolean); inline;
    procedure DoRead; virtual;
    function ExtractParameters(ACount: Integer): TdxSpreadSheetFormulaToken;
    function ExtractToken(AIndex: Integer = MaxInt): TdxSpreadSheetFormulaToken;
    procedure ClearTokens;
    procedure Read_Array;
    function Read_ArrayValue: TdxSpreadSheetFormulaToken;
    procedure Read_Area;
    procedure Read_AreaN;
    procedure Read_Area3d;
    procedure Read_Area3dErr;
    procedure Read_Attr;
    procedure Read_Bool;
    procedure Read_Exp;
    procedure Read_Err;
    procedure Read_Func;
    procedure Read_FuncVar;
    procedure Read_FuncVarV;
    procedure Read_Int;
    procedure Read_MemErr;
    procedure Read_MemFunc;
    procedure Read_MissArg;
    procedure Read_Name;
    procedure Read_NameX;
    procedure Read_Num;
    procedure Read_Operation;
    procedure Read_Paren;
    procedure Read_Ref;
    procedure Read_RefN;
    procedure Read_RefError;
    procedure Read_Ref3d;
    procedure Read_Ref3dErr;
    procedure Read_Str;
    procedure Read_Table;
    procedure Initialize;
    function SheetNameByRef(APage: Integer): string;
  public
    constructor Create(AOwner: TdxSpreadSheetXLSReader); virtual;
    destructor Destroy; override;
    procedure ReadFormula(ACell: TdxSpreadSheetCell); virtual;
    procedure ReadName(AName: TdxSpreadSheetDefinedName; ASize: Integer; ASource: PByteArray); virtual;

    property Arrays: TList<TdxSpreadSheetFormulaArrayToken> read FArrays;
    property Cell: TdxSpreadSheetCell read FCell;
    property Column: Integer read GetColumn;
    property CurrentToken: Byte read FCurrentToken;
    property Formula: TdxSpreadSheetFormulaAccess read FFormula;
    property IsBreak: Boolean read FIsBreak write FIsBreak;
    property IsError: Boolean read FIsError write FIsError;
    property Offset: Word read FOffset write FOffset;
    property Owner: TdxSpreadSheetXLSReader read FOwner;
    property Reader: TdxXLSReader read GetReader;
    property RecordReader: TdxXLSRecordReader read GetRecordReader;
    property Row: Integer read GetRow;
    property Size: Integer read FSize;
    property Source: PByteArray read FSource;
    property SourceOffset: Integer read FSourceOffset;
    property TokenReaders: TList<TdxXLSTokenReaderProc> read FTokenReaders;
    property Tokens: TList<TdxSpreadSheetFormulaToken> read FTokens;
    property Words[AIndex: Integer]: Word read GetWords;
  end;

implementation

{TdxXLSFormulaReader}

constructor TdxXLSFormulaReader.Create(AOwner: TdxSpreadSheetXLSReader);
begin
  FOwner := AOwner;
  FArrays := TList<TdxSpreadSheetFormulaArrayToken>.Create;
  FTokens := TList<TdxSpreadSheetFormulaToken>.Create;
  FTokenReaders := TList<TdxXLSTokenReaderProc>.Create;
  TokenReaders.Count := $FF;
  Initialize;
end;

destructor TdxXLSFormulaReader.Destroy;
begin
  ClearTokens;
  FreeAndNil(FArrays);
  FreeAndNil(FTokenReaders);
  FreeAndNil(FTokens);
  inherited Destroy;
end;

procedure TdxXLSFormulaReader.ReadFormula(ACell: TdxSpreadSheetCell);
begin
  FCell := ACell;
  FFormula := TdxSpreadSheetFormulaAccess(TdxSpreadSheetFormula.Create(Cell));
  FSize := Reader.ReadWord;
  FSourceOffset := RecordReader.Position;
  FSource := @PByteArray(RecordReader.Memory)^[RecordReader.Position];
  DoRead;
  if FFormula <> nil then
    Cell.AsFormula := FFormula;
end;

procedure TdxXLSFormulaReader.ReadName(AName: TdxSpreadSheetDefinedName; ASize: Integer; ASource: PByteArray);
begin
  FFormula := TdxSpreadSheetFormulaAccess(TdxSpreadSheetDefinedNameFormula.Create(AName));
  try
    FSize := ASize;
    FSource := ASource;
    FSourceOffset := TdxNativeInt(ASource) - TdxNativeInt(RecordReader.Memory);
    DoRead;
  finally
    TdxSpreadSheetDefinedNameAccess(AName).Formula := TdxSpreadSheetDefinedNameFormula(Formula);
  end;
end;

procedure TdxXLSFormulaReader.AddFormula(AInfo: TdxSpreadSheetFunctionInfo; AParams: TdxSpreadSheetFormulaToken);
var
  AToken: TdxSpreadSheetFormulaToken;
begin
  if AInfo <> nil then
    AToken := TdxSpreadSheetFormulaFunctionToken.Create(AInfo)
  else
    AToken := TdxSpreadSheetFormulaUnknownFunctionToken.Create('');
  Formula.AddChild(AToken, AParams);
  AddToken(AToken);
end;

procedure TdxXLSFormulaReader.AddReader(AToken: Byte; AReader: TdxXLSTokenReaderProc);
begin
  TokenReaders[AToken] := AReader;
end;

procedure TdxXLSFormulaReader.AddToken(AToken: TdxSpreadSheetFormulaToken; AIncOffset: Integer = 0);
begin
  Tokens.Add(AToken);
  Offset := Offset + AIncOffset;
end;

procedure TdxXLSFormulaReader.DecodeAreaReference(ASrcRow, ASrcColumn, ASrcRow2, ASrcColumn2: Word;
  var ARow, AColumn, ARow2, AColumn2: Integer; var AAbsRow, AAbsColumn, AAbsRow2, AAbsColumn2: Boolean);
begin
  DecodeReference(ASrcRow, ASrcColumn, ARow, AColumn, AAbsRow, AAbsColumn);
  DecodeReference(ASrcRow2, ASrcColumn2, ARow2, AColumn2, AAbsRow2, AAbsColumn2);
end;

procedure TdxXLSFormulaReader.DecodeAreaReference2(ASrcRow, ASrcColumn, ASrcRow2, ASrcColumn2: Word;
  var ARow, AColumn, ARow2, AColumn2: Integer; var AAbsRow, AAbsColumn, AAbsRow2, AAbsColumn2: Boolean);
begin
  DecodeReference2(ASrcRow, ASrcColumn, ARow, AColumn, AAbsRow, AAbsColumn);
  DecodeReference2(ASrcRow2, ASrcColumn2, ARow2, AColumn2, AAbsRow2, AAbsColumn2);
end;

procedure TdxXLSFormulaReader.DecodeReference(ASrcRow, ASrcColumn: Word; var ARow, AColumn: Integer; var AAbsRow, AAbsColumn: Boolean);
begin
  ARow := SmallInt(ASrcRow);
  AColumn := Shortint(Byte(ASrcColumn and $3FFF));
  AAbsRow := (ASrcColumn and $8000) = 0;
  AAbsColumn := (ASrcColumn and $4000) = 0;
  if not AAbsRow then
    ARow := ARow - Row;
  if not AAbsColumn then
     AColumn := AColumn - Column;
end;

procedure TdxXLSFormulaReader.DecodeReference2(ASrcRow, ASrcColumn: Word; var ARow, AColumn: Integer; var AAbsRow, AAbsColumn: Boolean);
begin
  ARow := SmallInt(ASrcRow);
  AColumn := Shortint(Byte(ASrcColumn and $3FFF));
  AAbsRow := (ASrcColumn and $8000) = 0;
  AAbsColumn := (ASrcColumn and $4000) = 0;
end;

procedure TdxXLSFormulaReader.DoRead;
var
  ATokenReader: TdxXLSTokenReaderProc;
begin
  if (Source^[0] = ptgExp) and (Size <= 5) then
  begin
    FreeAndNil(FFormula);
    Exit;
  end;
  Offset := 0;
  while (Offset < Size) and not IsBreak and not IsError do
  begin
    FCurrentToken := Source^[FOffset];
    Inc(FOffset);
    ATokenReader := TokenReaders[CurrentToken];
    IsError := not Assigned(ATokenReader);
    if IsError then Break;
    ATokenReader;
  end;
  while not IsBreak and not IsError and (Arrays.Count > 0) do
  begin
    Inc(FOffset);
    Read_Table;
  end;
  if not IsError and (Tokens.Count = 1) then
    Formula.FTokens := ExtractToken(0);
end;

function TdxXLSFormulaReader.ExtractParameters(ACount: Integer): TdxSpreadSheetFormulaToken;
var
  I: Integer;
  AParameter: TdxSpreadSheetFormulaToken;
begin
  Result := nil;
  ACount := Min(ACount, Tokens.Count);
  if ACount = 0 then Exit;
  for I := 0 to ACount - 1 do
  begin
    AParameter := TdxSpreadSheetFormulaToken.Create();
    Formula.AddChild(AParameter, ExtractToken);
    if Result <> nil then
      Formula.Add(AParameter, Result);
    Result := AParameter;
  end;
end;

function TdxXLSFormulaReader.ExtractToken(AIndex: Integer = MaxInt): TdxSpreadSheetFormulaToken;
begin
  if AIndex > Tokens.Count then
    AIndex := Tokens.Count - 1;
  Result := Tokens[AIndex];
  Tokens.Delete(AIndex);
  Formula.SetAsOwner(Result);
end;

procedure TdxXLSFormulaReader.ClearTokens;
var
  I: Integer;
begin
  for I := 0 to Tokens.Count - 1 do
    Tokens[I].Free;
  Tokens.Clear;
end;

procedure TdxXLSFormulaReader.Read_Array;
var
  ASize: TSize;
  AArray: TdxSpreadSheetFormulaArrayToken;
begin
  ASize.cx := Source^[Offset];
  if ASize.cx = 0 then
    ASize.cx := 256;
  ASize.cy:= PWord(@Source^[Offset + 1])^;
  AArray := TdxSpreadSheetFormulaArrayToken.Create;
  AArray.Size := ASize;
  Arrays.Add(AArray);
  AddToken(AArray, 7);
end;

function TdxXLSFormulaReader.Read_ArrayValue: TdxSpreadSheetFormulaToken;
var
  AKey, ALen: Integer;
  AFloatValue: Double;
  AStringValue: TdxUnicodeString;
begin
  AKey := Source^[Offset];
  Inc(FOffset);
  if AKey = 1 then
  begin
    AFloatValue := PDouble(@Source^[Offset])^;
    Result := TdxSpreadSheetFormulaFloatValueToken.Create(AFloatValue);
    Inc(FOffset, 8);
  end
  else
    if AKey = 2 then
    begin
      ALen := Source^[Offset];
      SetLength(AStringValue, ALen);
      if ALen > 0 then
        Move(Source^[Offset + 1], AStringValue[1], ALen);
      Result := TdxSpreadSheetFormulaStringValueToken.Create(AStringValue);
      Inc(FOffset, ALen + 1);
    end
    else
      Result := TdxSpreadSheetFormulaNullToken.Create;
end;

procedure TdxXLSFormulaReader.Read_Area;
var
  ATokens: PWordArray;
  ARow, AColumn, ARow2, AColumn2: Integer;
  AbsRow, AbsColumn, AbsRow2, AbsColumn2: Boolean;
begin
  ATokens := @Source^[Offset];
  DecodeAreaReference(ATokens^[0], ATokens^[2], ATokens^[1], ATokens^[3],
    ARow, AColumn, ARow2, AColumn2, AbsRow, AbsColumn, AbsRow2, AbsColumn2);
  AddToken(TdxSpreadSheetFormulaAreaReference.Create(ARow, AColumn, ARow2, AColumn2,
    AbsRow, AbsColumn, AbsRow2, AbsColumn2));
  Inc(FOffset, 8);
end;

procedure TdxXLSFormulaReader.Read_AreaN;
var
  ATokens: PWordArray;
  ARow, AColumn, ARow2, AColumn2: Integer;
  AbsRow, AbsColumn, AbsRow2, AbsColumn2: Boolean;
begin
  ATokens := @Source^[Offset];
  DecodeAreaReference2(ATokens^[0], ATokens^[2], ATokens^[1], ATokens^[3],
    ARow, AColumn, ARow2, AColumn2, AbsRow, AbsColumn, AbsRow2, AbsColumn2);
  AddToken(TdxSpreadSheetFormulaAreaReference.Create(ARow, AColumn, ARow2, AColumn2,
    AbsRow, AbsColumn, AbsRow2, AbsColumn2));
  Inc(FOffset, 8);
end;

procedure TdxXLSFormulaReader.Read_Area3d;
var
  ATokens: PWordArray;
  ALink: TdxSpreadSheet3DReferenceLink;
  ARow, AColumn, ARow2, AColumn2: Integer;
  AbsRow, AbsColumn, AbsRow2, AbsColumn2: Boolean;
begin
  ATokens := @Source^[Offset];
  ALink := TdxSpreadSheet3DReferenceLink.Create(Owner.GetSheet(ATokens^[0]));
  DecodeAreaReference(ATokens^[1], ATokens^[3], ATokens^[2], ATokens^[4],
    ARow, AColumn, ARow2, AColumn2, AbsRow, AbsColumn, AbsRow2, AbsColumn2);
  AddToken(TdxSpreadSheetFormula3DAreaReference.Create(ALink, nil,
    ARow, AColumn, ARow2, AColumn2, AbsRow, AbsColumn, AbsRow2, AbsColumn2));
  Inc(FOffset, 10);
end;

procedure TdxXLSFormulaReader.Read_Area3dErr;
begin
  Read_Area3d;
  TdxSpreadSheetFormula3DAreaReference(Tokens.Last).IsError := True;
end;

procedure TdxXLSFormulaReader.Read_Attr;
//var
//  ASpaceAttribute, ASpaceCount: Byte;
begin
  if (Source^[FOffset] and $04) <> 0 then
    FOffset := FOffset + (PWord(@Source^[FOffset + 1])^ + 2) * 2 - 3
  else
    if (Source^[FOffset] and $40) <> 0 then
    begin
//      ASpaceAttribute := Source^[1];
//      Add(TdxSpreadSheetFormulaAttributeToken.Create(Source^[FOffset + 2])); todo:
    end
    else
      if (Source^[FOffset] and $10) <> 0 then
        AddFormula(dxSpreadSheetFunctionsRepository.GetInfoByID(4), ExtractParameters(1));
  Inc(FOffset, 3);
end;

procedure TdxXLSFormulaReader.Read_Bool;
begin
  AddToken(TdxSpreadSheetFormulaBooleanValueToken.Create(Boolean(Source^[FOffset])));
  Inc(FOffset);
end;

procedure TdxXLSFormulaReader.Read_Exp;
begin
  Inc(FOffset, 4);
end;

procedure TdxXLSFormulaReader.Read_Err;
begin
  AddToken(TdxSpreadSheetFormulaErrorValueToken.Create(XLSErrorToErrorCode(Source^[FOffset])));
  Inc(FOffset);
end;

procedure TdxXLSFormulaReader.Read_Func;
var
  AInfo: TdxSpreadSheetFunctionInfo;
  AParams: TdxSpreadSheetFormulaToken;
begin
  AInfo := dxSpreadSheetFunctionsRepository.GetInfoByID(PWord(@Source[Offset])^);
  Inc(FOffset, 2);
  AParams := nil;
  if AInfo <> nil then
    AParams := ExtractParameters(AInfo.ParamCount);
  AddFormula(AInfo, AParams);
end;

procedure TdxXLSFormulaReader.Read_FuncVar;
var
  ACount: Byte;
begin
  ACount := Source[Offset] and $7F;
  Inc(FOffset);
  AddFormula(dxSpreadSheetFunctionsRepository.GetInfoByID(PWord(@Source^[FOffset])^ and $7FFF),
    ExtractParameters(ACount));
  Inc(FOffset, 2);
end;

procedure TdxXLSFormulaReader.Read_FuncVarV;
var
  ID: Word;
  ACount: Byte;
  AParams: TdxSpreadSheetFormulaToken;
begin
  ACount := Source[Offset] and $7F;
  Inc(FOffset);
  ID := PWord(@Source^[FOffset])^ and $7FFF;
  if ID = 255 then
  begin
    AParams := ExtractParameters(ACount - 1);
    Formula.AddChild(Tokens.Last, AParams);
  end
  else
    AddFormula(dxSpreadSheetFunctionsRepository.GetInfoByID(ID), ExtractParameters(ACount));
  Inc(FOffset, 2);
end;

procedure TdxXLSFormulaReader.Read_Int;
begin
  AddToken(TdxSpreadSheetFormulaIntegerValueToken.Create(PWord(@FSource[FOffset])^), 2);
end;

procedure TdxXLSFormulaReader.Read_MemErr;
begin
  Inc(FOffset, 6);
end;

procedure TdxXLSFormulaReader.Read_MemFunc;
begin
  Inc(FOffset, 2);
end;

procedure TdxXLSFormulaReader.Read_MissArg;
begin
  AddToken(TdxSpreadSheetFormulaNullToken.Create());
end;

procedure TdxXLSFormulaReader.Read_Name;
var
  AInfo: TdxSpreadSheetFunctionInfo;
  AName: TdxUnicodeString;
begin
  AName := Owner.GetName(PWord(@Source^[FOffset])^ - 1, AInfo);
  if AInfo = nil then
    AddToken(TdxSpreadSheetDefinedNameToken.Create(AName))
  else
    AddToken(TdxSpreadSheetFormulaFunctionToken.Create(AInfo));
  Inc(FOffset, 4);
end;

procedure TdxXLSFormulaReader.Read_NameX;
var
  AInfo: TdxSpreadSheetFunctionInfo;
  AName: TdxUnicodeString;
begin
  Inc(FOffset, 2); // skip, SheetIndex
  AName := Owner.GetExternalName(PWord(@Source^[FOffset])^ - 1, AInfo);
  if AInfo = nil then
    AddToken(TdxSpreadSheetDefinedNameToken.Create(AName))
  else
    AddToken(TdxSpreadSheetFormulaFunctionToken.Create(AInfo));
  Inc(FOffset, 4);
end;

procedure TdxXLSFormulaReader.Read_Num;
begin
  AddToken(TdxSpreadSheetFormulaFloatValueToken.Create(PDouble(@Source^[Offset])^), 8);
end;

procedure TdxXLSFormulaReader.Read_Operation;
var
  ALastToken, AOperation: TdxSpreadSheetFormulaToken;
begin
  AOperation := TdxSpreadSheetFormulaOperationToken.Create(TdxSpreadSheetFormulaOperation(CurrentToken - ptgAdd));
  case CurrentToken of
    ptgAdd..ptgRange:
      begin
        ALastToken := ExtractToken();
        Formula.AddLast(Tokens.Last, ALastToken);
        Formula.AddLast(ALastToken, AOperation);
      end;
    ptgUPlus..ptgPercent:
      Formula.Add(Tokens.Last, AOperation);
  end;
end;

procedure TdxXLSFormulaReader.Read_Paren;
var
  AToken: TdxSpreadSheetFormulaToken;
begin
  AToken := TdxSpreadSheetFormulaParenthesesToken.Create;
  Formula.AddChild(AToken, ExtractToken);
  AddToken(AToken);
end;

procedure TdxXLSFormulaReader.Read_Ref;
var
  ARow, AColumn: Integer;
  AAbsRow, AAbsColumn: Boolean;
begin
  DecodeReference(PWordArray(@FSource^[Offset])^[0], PWordArray(@FSource^[Offset])^[1],
    ARow, AColumn, AAbsRow, AAbsColumn);
  AddToken(TdxSpreadSheetFormulaReference.Create(ARow, AColumn, AAbsRow, AAbsColumn));
  Inc(FOffset, 4);
end;

procedure TdxXLSFormulaReader.Read_RefN;
var
  ARow, AColumn: Integer;
  AAbsRow, AAbsColumn: Boolean;
begin
  DecodeReference2(PWordArray(@FSource^[Offset])^[0], PWordArray(@FSource^[Offset])^[1],
    ARow, AColumn, AAbsRow, AAbsColumn);
  AddToken(TdxSpreadSheetFormulaReference.Create(ARow, AColumn, AAbsRow, AAbsColumn));
  Inc(FOffset, 4);
end;

procedure TdxXLSFormulaReader.Read_RefError;
begin
  AddToken(TdxSpreadSheetFormulaErrorValueToken.Create(ecRefErr));
  Inc(FOffset, 4);
end;

procedure TdxXLSFormulaReader.Read_Ref3d;
var
  ATokens: PWordArray;
  ARow, AColumn: Integer;
  AAbsRow, AAbsColumn: Boolean;
  ALink: TdxSpreadSheet3DReferenceLink;
begin
  ATokens := @Source^[FOffset];
  DecodeReference(ATokens^[1], ATokens^[2], ARow, AColumn, AAbsRow, AAbsColumn);
  ALink := TdxSpreadSheet3DReferenceLink.Create(Owner.GetSheet(ATokens^[0]));
  AddToken(TdxSpreadSheetFormula3DReference.Create(ALink, ARow, AColumn, AAbsRow, AAbsColumn));
  Inc(FOffset, 6);
end;

procedure TdxXLSFormulaReader.Read_Ref3dErr;
begin
  Read_Ref3d;
  TdxSpreadSheetFormula3DReference(Tokens.Last).IsError := True;
end;

procedure TdxXLSFormulaReader.Read_Str;
var
  AValue: TdxUnicodeString;
begin
  AValue := Reader.XLS_ReadSimpleString(FOffset + SourceOffset);
  AddToken(TdxSpreadSheetFormulaStringValueToken.Create(AValue));
  FOffset := RecordReader.Position - SourceOffset;
end;

procedure TdxXLSFormulaReader.Read_Table;

  procedure ReadArray(AArray: TdxSpreadSheetFormulaArrayToken);
  var
    ARow, AColumn: Integer;
    AParameter: TdxSpreadSheetFormulaToken;
  begin
    for ARow := 0 to AArray.Size.cy do
    begin
      for AColumn := 0 to AArray.Size.cx do
      begin
        AParameter := TdxSpreadSheetFormulaToken.Create;
        Formula.AddChild(AParameter, Read_ArrayValue);
        Formula.AddChild(AArray, AParameter);
      end;
      if ARow < AArray.Size.cy then
        Formula.AddChild(AArray, TdxSpreadSheetFormulaArrayRowSeparator.Create);
     end;
     Arrays.Delete(0);
  end;

begin
  Inc(FOffset, 2);
  if Arrays.Count > 0 then
    ReadArray(Arrays[0])
  else
    IsBreak := True;
end;

procedure TdxXLSFormulaReader.Initialize;
var
  I: Integer;
begin
  for I := ptgAdd to ptgPercent do
    AddReader(I, Read_Operation);

  AddReader(ptgArray, Read_Array);
  AddReader(ptgArrayA, Read_Array);
  AddReader(ptgAttr, Read_Attr);
  AddReader(ptgBool, Read_Bool);
  AddReader(ptgExp, Read_Exp);
  //
  AddReader(ptgFunc, Read_Func);
  AddReader(ptgFuncV, Read_Func);
  AddReader(ptgFuncA, Read_Func);
  AddReader(ptgFuncVar, Read_FuncVar);
  AddReader(ptgFuncVarV, Read_FuncVar);
  AddReader(ptgFuncVarV, Read_FuncVarV);
  AddReader(ptgFuncVarA, Read_FuncVar);
  //
  AddReader(ptgInt, Read_Int);
  //
  AddReader(ptgMemFunc, Read_MemFunc);
  AddReader(ptgMemNoMemN, Read_MemFunc);
  AddReader(ptgMemErr, Read_MemErr);
  AddReader(ptgMemArea, Read_MemErr);
  AddReader(ptgMemAreaN, Read_MemErr);
  AddReader(ptgMemAreaA, Read_MemErr);
  //
  AddReader(ptgName, Read_Name);
  AddReader(ptgNameV, Read_Name);
  AddReader(ptgNameA, Read_Name);
  AddReader(ptgNameX, Read_NameX);
  //
  AddReader(ptgNum, Read_Num);
  AddReader(ptgParen, Read_Paren);
  //
  AddReader(ptgArea, Read_Area);
  AddReader(ptgAreaA, Read_Area);
  AddReader(ptgAreaV, Read_Area);

  AddReader(ptgAreaN, Read_AreaN);
  AddReader(ptgAreaNA, Read_AreaN);
  AddReader(ptgAreaNV, Read_AreaN);

  AddReader(ptgArea3d, Read_Area3d);
  AddReader(ptgAreaErr3d, Read_Area3dErr);
   //
  AddReader(ptgRef, Read_Ref);
  AddReader(ptgRefA, Read_Ref);
  AddReader(ptgRefV, Read_Ref);

  AddReader(ptgRefN, Read_RefN);
  AddReader(ptgRefNA, Read_RefN);
  AddReader(ptgRefNV, Read_RefN);
  //
  AddReader(ptgRefErr, Read_RefError);
  AddReader(ptgRefErrA, Read_RefError);
  AddReader(ptgRefErrV, Read_RefError);
  //
  AddReader(ptgRef3d, Read_Ref3d);
  AddReader(ptgRef3dA, Read_Ref3d);
  AddReader(ptgRef3dV, Read_Ref3d);
  //
  AddReader(ptgRefErr3d, Read_Ref3dErr);
  AddReader(ptgRefErr3dA, Read_Ref3dErr);
  AddReader(ptgRefErr3dV, Read_Ref3dErr);
  //
  AddReader(ptgTbl, Read_Table);
  AddReader(ptgStr, Read_Str);
  AddReader(ptgErr, Read_Err);
  AddReader(ptgMissArg, Read_MissArg);
end;

function TdxXLSFormulaReader.SheetNameByRef(APage: Integer): string;
begin
{  if Assigned(Owner) then
    with TcxFileReaderAccess(Owner) do
    begin
      APage := XltPage(APage);
      if APage < BoundSheets.Count then
        Result := BoundSheets[APage]
      else
        Result := scxRefError + '3D!';
    end;
    if Pos(' ', Result) <> 0 then
      Result := '''' + Result + '''';}
end;

function TdxXLSFormulaReader.GetColumn: Integer;
begin
  if Cell <> nil then
    Result := Cell.ColumnIndex
  else
    Result := 0;
end;

function TdxXLSFormulaReader.GetReader: TdxXLSReader;
begin
  Result := Owner.Reader;
end;

function TdxXLSFormulaReader.GetRecordReader: TdxXLSRecordReader;
begin
  Result := Owner.RecordReader;
end;

function TdxXLSFormulaReader.GetRow: Integer;
begin
  if Cell <> nil then
    Result := Cell.RowIndex
  else
    Result := 0;
end;

function TdxXLSFormulaReader.GetWords(AIndex: Integer): Word;
begin
  Result := PWordArray(FSource[Offset])^[AIndex];
end;

{procedure TdxXLSFormulaReader.SetLastString(const Value: string);
begin
  FStringList[FStringList.Count - 1] := Value;
end;}


end.
