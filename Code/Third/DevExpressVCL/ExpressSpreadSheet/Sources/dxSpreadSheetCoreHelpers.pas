{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetCoreHelpers;

{$I cxVer.Inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Types, Windows, Classes, Graphics, Generics.Collections, Generics.Defaults, SysUtils, Clipbrd, cxClasses, cxGraphics,
  dxCore, cxVariants, dxHashUtils, dxSpreadSheetCore, dxSpreadSheetClasses, dxSpreadSheetTypes, dxSpreadSheetStrs,
  dxSpreadSheetGraphics;

type

  { TdxSpreadSheetCustomTableViewHelper }

  TdxSpreadSheetCustomTableViewHelper = class(TObject)
  strict private
    FView: TdxSpreadSheetTableView;

    function GetColumns: TdxSpreadSheetTableColumns; inline;
    function GetHistory: TdxSpreadSheetHistory;
    function GetRows: TdxSpreadSheetTableRows; inline;
    function GetSpreadSheet: TdxCustomSpreadSheet; inline;
  protected
    procedure Changed;
  public
    constructor Create(AView: TdxSpreadSheetTableView); virtual;
    destructor Destroy; override;
    //
    property Columns: TdxSpreadSheetTableColumns read GetColumns;
    property History: TdxSpreadSheetHistory read GetHistory;
    property Rows: TdxSpreadSheetTableRows read GetRows;
    property SpreadSheet: TdxCustomSpreadSheet read GetSpreadSheet;
    property View: TdxSpreadSheetTableView read FView;
  end;

  { TdxSpreadSheetTableViewCellsModificationHelper }

  TdxSpreadSheetTableViewCellsModificationHelper = class(TdxSpreadSheetCustomTableViewHelper)
  strict private
    FFixedContainerAnchorTypes: TDictionary<TdxSpreadSheetContainer, TdxSpreadSheetContainerAnchorType>;
    FFixedContainers: TDictionary<TdxSpreadSheetContainer, TRect>;
    FIsHorizontalShifting: Boolean;
    FMergedCellsConflictResolution: TdxSpreadSheetTableViewMergedCellsConflictResolution;
    FModification: TdxSpreadSheetCellsModification;
    FMovableArea: TRect;
  protected
    procedure DoCommand(const AArea: TRect; AModification: TdxSpreadSheetCellsModification; AIsHorizontal: Boolean);
    procedure DoProcess(const AArea: TRect); virtual; abstract;
    procedure Initialize(var AArea: TRect);

    procedure CheckIsAreaEmpty(AItems: TdxSpreadSheetTableItems; const AArea: TRect);
    procedure CreateBlankCellsForStyledItems(AItems, AOpositeItems: TdxSpreadSheetTableItems; const AArea: TRect; AShiftDelta: Integer);
    function IsAnchorPointInArea(const AArea: TRect; AAnchorPoint: TdxSpreadSheetContainerAnchorPoint): Boolean;
    function IsAreaEmpty(AItems: TdxSpreadSheetTableItems; const AArea: TRect): Boolean;

    procedure CheckMergedCells;
    function HasAffectedMergedCells: Boolean;
    procedure MoveMergedCells(ADeltaX, ADeltaY: Integer);
    procedure UnmergeAllAffectedCells;

    procedure StoreFixedContainersBounds;
    procedure RestoreFixedContainersBounds;

    procedure ShiftCellsHorizontally(AStartFromIndex, AStartRowIndex, AFinishRowIndex, ADelta: Integer);
    procedure ShiftCellsVertically(AStartFromIndex, AStartColumnIndex, AFinishColumnIndex, ADelta: Integer);
    procedure ShiftColumns(AStartFromIndex, ADelta: Integer);
    procedure ShiftIndexex(AList: TdxDynamicItemList; AStartFromIndex, ADelta: Integer);
    procedure ShiftRows(AStartFromIndex, ADelta: Integer);
  public
    constructor Create(AView: TdxSpreadSheetTableView); override;
    destructor Destroy; override;
    procedure Process(AArea: TRect);
    //
    property FixedContainerAnchorTypes: TDictionary<TdxSpreadSheetContainer, TdxSpreadSheetContainerAnchorType> read FFixedContainerAnchorTypes;
    property FixedContainers: TDictionary<TdxSpreadSheetContainer, TRect> read FFixedContainers;
    property IsHorizontalShifting: Boolean read FIsHorizontalShifting;
    property MergedCellsConflictResolution: TdxSpreadSheetTableViewMergedCellsConflictResolution read FMergedCellsConflictResolution write FMergedCellsConflictResolution;
    property Modification: TdxSpreadSheetCellsModification read FModification write FModification;
    property MovableArea: TRect read FMovableArea;
  end;

  { TdxSpreadSheetTableViewDeleteCellsHelper }

  TdxSpreadSheetTableViewDeleteCellsHelper = class(TdxSpreadSheetTableViewCellsModificationHelper)
  strict private
    procedure MoveAnchor(AAnchor: TdxSpreadSheetContainerAnchorPoint; const AArea: TRect; AMoveForward: Boolean);
  protected
    procedure CheckContainer(const AArea: TRect; AContainer: TdxSpreadSheetContainer);
    procedure CheckContainers(const AArea: TRect);
    procedure DeleteCellsInArea(const AArea: TRect);
    procedure DeleteItems(AItems: TdxSpreadSheetTableItems; AStartIndex, AFinishIndex: Integer);

    procedure DoProcess(const AArea: TRect); override;
  end;

  { TdxSpreadSheetTableViewInsertCellsHelper }

  TdxSpreadSheetTableViewInsertCellsHelper = class(TdxSpreadSheetTableViewCellsModificationHelper)
  protected
    procedure CalculateStyle(ANewStyle, ALeftSourceStyle, ARightSourceStyle: TdxSpreadSheetCellStyle); virtual;
    procedure CalculateStyleForNewCells(ATarget, ALeftSource, ARightSource: TdxSpreadSheetTableItem;
      AOpositeItems: TdxSpreadSheetTableItems; AStartIndex, AFinishIndex: Integer);
    procedure CalculateStyleForNewItems(const AArea: TRect; AItems, AOpositeItems: TdxSpreadSheetTableItems);
    procedure DoProcess(const AArea: TRect); override;

    function GetCellStyle(ASource: TdxSpreadSheetTableItem; ACellIndex: Integer;
      AOpositeItems: TdxSpreadSheetTableItems): TdxSpreadSheetCellStyle; inline;
    function GetStyle(AItem: TdxSpreadSheetTableItem): TdxSpreadSheetCellStyle; inline;
  end;

  { TdxSpreadSheetTableViewMergeCellStyleHelper }

  TdxSpreadSheetTableViewMergeCellStyleHelper = class(TdxSpreadSheetCustomTableViewHelper)
  strict private
    FBorderColors: array[TcxBorder] of TColor;
    FBorderStyles: array[TcxBorder] of TdxSpreadSheetCellBorderStyle;
    FMergeCell: TdxSpreadSheetMergedCell;
    FNonBlankCell: TdxSpreadSheetCell;

    function GetActiveCell: TdxSpreadSheetCell; inline;
    function GetDefaultStyle: TdxSpreadSheetCellStyle; inline;
    function GetStylePattern: TdxSpreadSheetCellStyle;
  protected
    procedure CalculateBorder(ABorder: TcxBorder; AItem: TdxSpreadSheetTableItem;
      AStartIndex, AFinishIndex: Integer; out ABorderColor: TColor; out ABorderStyle: TdxSpreadSheetCellBorderStyle);
    procedure CalculateCellStyles;
    procedure CalculateCellStylesInCustomArea(const AArea: TRect; AStylePattern: TdxSpreadSheetCellStyle);
    procedure CalculateCellStylesInEntireColumnOrRow(AItems: TdxSpreadSheetTableItems;
      AStartIndex, AFinishIndex: Integer; AStylePattern: TdxSpreadSheetCellStyle);
    procedure CalculateCellValues;
    procedure DoCalculate;
    procedure SetupActiveCellValue;
    //
    property ActiveCell: TdxSpreadSheetCell read GetActiveCell;
    property DefaultStyle: TdxSpreadSheetCellStyle read GetDefaultStyle;
    property MergeCell: TdxSpreadSheetMergedCell read FMergeCell;
    property NonBlankCell: TdxSpreadSheetCell read FNonBlankCell;
  public
    constructor Create(AMergeCell: TdxSpreadSheetMergedCell); reintroduce;
    class procedure Calculate(AMergeCell: TdxSpreadSheetMergedCell);
  end;

  { TdxSpreadSheetTableViewSortingHelper }

  TdxSpreadSheetTableViewSortingHelper = class(TdxSpreadSheetCustomTableViewHelper)
  protected
    Area: TRect;
    DefaultSorting: Boolean;
    SortOrder: TdxSortOrder;
    SortOrders: TList<TdxSortOrder>;
    SortIndexes: TList<Integer>;
    function Compare(AItem1, AItem2: Pointer): Integer;
    function CompareCells(AData1, AData2: TdxSpreadSheetCellData): Integer;
    function CompareValues(const AValue1, AValue2: Variant): Integer;
    procedure DoSort(AFromIndex, AToIndex, ASortIndex: Integer);
    procedure ExchangeColumnValues(AColumn1, AColumn2: Integer);
    procedure ExchangeRowValues(ARow1, ARow2: Integer);
    function GetCellData(ARow, AColumn: Integer): TdxSpreadSheetCellData;
    function Initialize(AIndex1, AIndex2: Integer; const ASortOrders: array of TdxSortOrder; const AIndexes: array of Integer): Boolean;
    function IsColumnEmpty(AIndex: Integer): Boolean;
    function IsRowEmpty(AIndex: Integer): Boolean;
    procedure SortValues(const AValues: TcxObjectList; L, H: Integer);
    procedure SwapValues(AValue1, AValue2: TdxSpreadSheetCellData);
    function ValidateArea(const AArea: TRect): Boolean;
  public
    constructor Create(AView: TdxSpreadSheetTableView); override;
    destructor Destroy; override;
    procedure SortByColumnValues(const AArea: TRect; const ASortOrders: array of TdxSortOrder; const AColumnIndexes: array of Integer);
    procedure SortByRowValues(const AArea: TRect; const ASortOrders: array of TdxSortOrder; const ARowIndexes: array of Integer);
  end;

  { TdxSpreadSheetTableViewClipboardHelper }

  TdxSpreadSheetTableViewClipboardHelper = class(TdxSpreadSheetCustomTableViewHelper)
  strict private
    FArea: TRect;
    FCellsData: TMemoryStream;
    FCellsReader: TcxReader;
    FCellsWriter: TcxWriter;
    FContainersData: TMemoryStream;
    FContainersReader: TcxReader;
    FContainersWriter: TcxWriter;
    FInvariantFormatSettings: TFormatSettings;
    FMergedCells: TList<TdxSpreadSheetMergedCell>;
    FStyles: TList;
    FStylesData: TMemoryStream;
    FStylesReader: TcxReader;
    FStylesWriter: TcxWriter;
    FText: TStringList;

    procedure CheckSelection;
    procedure DecodeText(const S: string);
    function DecodeValue(const S: string): Variant;
    function GetSelectedContainer: TdxSpreadSheetContainer;
    function GetSelection: TdxSpreadSheetTableViewSelection; inline;
    procedure SetSelectedContainer(const Value: TdxSpreadSheetContainer);
  protected
    procedure AddMergedCells(const Area: TRect);
    procedure DoCopyCells;
    procedure DoCopyContainers;
    procedure DoCopyDataToClipboard;
    procedure DoMergeBordersForInsertedCells;
    procedure DoPasteCells;
    procedure DoPasteContainers;
    procedure GetDataFromClipboard;
    procedure PasteAsFormattedValues;
    procedure PasteAsText;
    procedure ReadData(var ABuffer: PByte; var AData; ASize: Integer);
    function ReadInteger(var ABuffer: PByte): Integer;
    function ReadRect(var ABuffer: PByte): TRect;
    procedure SetDataToClipboard;
    procedure WriteData(var ABuffer: PByte; const AData; ASize: Integer);
    procedure WriteInteger(var ABuffer: PByte; const AValue: Integer);
    procedure WriteRect(var ABuffer: PByte; const R: TRect);

    property Area: TRect read FArea;
    property CellsData: TMemoryStream read FCellsData;
    property CellsReader: TcxReader read FCellsReader;
    property CellsWriter: TcxWriter read FCellsWriter;
    property ContainersData: TMemoryStream read FContainersData;
    property ContainersReader: TcxReader read FContainersReader;
    property ContainersWriter: TcxWriter read FContainersWriter;
    property MergedCells: TList<TdxSpreadSheetMergedCell> read FMergedCells;
    property Styles: TList read FStyles;
    property StylesData: TMemoryStream read FStylesData;
    property StylesReader: TcxReader read FStylesReader;
    property StylesWriter: TcxWriter read FStylesWriter;
    property Text: TStringList read FText;
  public
    constructor Create(AView: TdxSpreadSheetTableView); override;
    destructor Destroy; override;
    class function CanPaste: Boolean;
    procedure Copy;
    procedure Cut;
    procedure Paste;

    property SelectedContainer: TdxSpreadSheetContainer read GetSelectedContainer write SetSelectedContainer;
    property Selection: TdxSpreadSheetTableViewSelection read GetSelection;
  end;

  { TdxSpreadSheetTableViewEnumCellStylesHelper }

  TdxSpreadSheetTableViewEnumCellStylesProcRef = reference to procedure (
    ACellStyle: TdxSpreadSheetCellStyle; ARow, AColumn: Integer; const AArea: TRect);

  TdxSpreadSheetTableViewEnumCellStylesHelper = class(TObject)
  strict private
    FEnumDefaultStyles: Boolean;
    FHasEntireRowWithDefaultStyle: Boolean;
    FSheet: TdxSpreadSheetTableView;
    procedure EnumColumnStyles(const AArea: TRect; AProcRef: TdxSpreadSheetTableViewEnumCellStylesProcRef);
    procedure EnumDefaultCellsStyles(const R: TRect; AProcRef: TdxSpreadSheetTableViewEnumCellStylesProcRef);
    procedure EnumRowStyles(const AArea: TRect; AProcRef: TdxSpreadSheetTableViewEnumCellStylesProcRef);
  public
    constructor Create(ASheet: TdxSpreadSheetTableView); virtual;
    procedure PrepareToSave(AArea: TRect);
    procedure ProcessArea(const AArea: TRect; AProcRef: TdxSpreadSheetTableViewEnumCellStylesProcRef);
    //
    property EnumDefaultStyles: Boolean read FEnumDefaultStyles write FEnumDefaultStyles;
    property Sheet: TdxSpreadSheetTableView read FSheet;
  end;

  { TdxSpreadSheetTableViewPackHelper }

  TdxSpreadSheetTableViewPackHelper = class(TdxSpreadSheetCustomTableViewHelper)
  protected
    procedure RemoveUnusedCells;
    procedure RemoveUnusedItems(AItems: TdxSpreadSheetTableItems);
  public
    procedure Pack;
  end;

  { TdxSpreadSheetEnumValuesProcessingInfo }

  TdxSpreadSheetEnumValuesProcessingInfo = record
  public
    ErrorIfNonReferenceToken: Boolean;
    IgnoreHiddenRows: Boolean;
    NumericListSortedInAscendingOrder: Boolean;
    PopulateNumericListWhenAccumulateResult: Boolean;

    procedure Init(const ANumericListSortedInAscendingOrder, APopulateNumericListWhenAccumulateResult,
      AErrorIfNonReferenceToken, AIgnoreHiddenRows: Boolean);
  end;

  { TdxSpreadSheetEnumValues }

  TdxSpreadSheetEnumValues = class
  strict private
    FIndex: Integer;
    FCount: Integer;
    FEmptyCount: Integer;
    FErrorCode: TdxSpreadSheetFormulaErrorCode;
    FFormatSettings: TdxSpreadSheetFormatSettings;
    FNullCount: Integer;
    FNumericCount: Integer;
    FNumericList: TList<Double>;
    FProcessingInfo: TdxSpreadSheetEnumValuesProcessingInfo;
    FStringCount: Integer;
    FTrueCount: Integer;
    FResultValue: Variant;
    function ConvertNullToDefValue(const ADefValue: Variant; var AOutputValue: Variant; ACanConvertStrToNumeric: Boolean): Boolean;
    procedure Initialize(const AInitialResultValue: Variant);
  public
    constructor Create(const AFormatSettings: TdxSpreadSheetFormatSettings; const AProcessingInfo: TdxSpreadSheetEnumValuesProcessingInfo); overload;
    constructor Create(const AFormatSettings: TdxSpreadSheetFormatSettings; const AInitialResultValue: Variant; const AProcessingInfo: TdxSpreadSheetEnumValuesProcessingInfo); overload;
    destructor Destroy; override;
    function AddToNumericList(const AValue: Variant; const AValueIsChecked: Boolean): Boolean;
    function CheckValueOnLogical(const AValue: Variant; ACanConvertStrToNumber: Boolean; var AOutputValue: Variant): Boolean;
    function CheckValueOnNumeric(const AValue: Variant; ACanConvertStrToNumber: Boolean; AWithoutSimularNumericByReference: Boolean;
      var AOutputValue: Variant): Boolean;
    function DeleteFromNumericList(const AIndex: Integer): Boolean;
    function InsertIntoNumericList(const AIndex: Integer; const ANumber: Double): Boolean;
    function PopulateNumericList(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken;
      const AProcessAllParams: Boolean = False): TdxSpreadSheetFormulaErrorCode;
    procedure SetErrorCode(AErrorCode: TdxSpreadSheetFormulaErrorCode);
    function Validate: Boolean;

    property Index: Integer read FIndex;
    property Count: Integer read FCount;
    property EmptyCount: Integer read FEmptyCount;
    property ErrorCode: TdxSpreadSheetFormulaErrorCode read FErrorCode;
    property FormatSettings: TdxSpreadSheetFormatSettings read FFormatSettings;
    property NullCount: Integer read FNullCount;
    property NumericCount: Integer read FNumericCount;
    property NumericList: TList<Double> read FNumericList;
    property ProcessingInfo: TdxSpreadSheetEnumValuesProcessingInfo read FProcessingInfo;
    property ResultValue: Variant read FResultValue write FResultValue;
    property StringCount: Integer read FStringCount;
    property TrueCount: Integer read FTrueCount;
  end;

  { TdxSpreadSheetTableItemStyleMergeHelper }

  TdxSpreadSheetTableItemStyleMergeHelper = class
  strict private
    FIsUpdateBestFitNeeded: Boolean;
    FItem: TdxSpreadSheetTableItem;
    FNewStyle: TdxSpreadSheetCellStyleHandle;
    FPrevStyle: TdxSpreadSheetCellStyleHandle;

    procedure MergeAlignment(ACellStyle: TdxSpreadSheetCellStyle);
    procedure MergeBorders(ACell: TdxSpreadSheetCell);
    procedure MergeBrush(ACellStyle: TdxSpreadSheetCellStyle);
    procedure MergeFont(ACellStyle: TdxSpreadSheetCellStyle);
  protected
    procedure CheckIsUpdateBestFitNeeded;

    property Item: TdxSpreadSheetTableItem read FItem;
    property NewStyle: TdxSpreadSheetCellStyleHandle read FNewStyle;
    property PrevStyle: TdxSpreadSheetCellStyleHandle read FPrevStyle;
  public
    constructor Create(AItem: TdxSpreadSheetTableItem; APrevStyle, ANewStyle: TdxSpreadSheetCellStyleHandle);
    procedure ProcessCell(ACell: TdxSpreadSheetCell);
    //
    property IsUpdateBestFitNeeded: Boolean read FIsUpdateBestFitNeeded;
  end;

var
  CFSpreadSheet: Cardinal = 0;

procedure dxSpreadSheetCalculateForEachParam(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken;
  AData: TdxSpreadSheetEnumValues; ACalculateProc: TdxSpreadSheetForEachCallBack);

function dxSpreadSheetExtractedArgumentsAreNotNull(Sender: TdxSpreadSheetFormulaResult;
  const AParams: TdxSpreadSheetFormulaToken; var AArgument1, AArgument2: Variant): Boolean;

implementation

uses
  Math, Dialogs, Controls, Variants, StrUtils,
  cxGeometry, dxCoreClasses, cxControls, dxSpreadSheetUtils, dxTypeHelpers, dxSpreadSheetFormulas, cxDateUtils,
  dxSpreadSheetFormatBinary;

type
  TdxDynamicItemListAccess = class(TdxDynamicItemList);
  TdxSpreadSheetCellAccess = class(TdxSpreadSheetCell);
  TdxSpreadSheetContainerAccess = class(TdxSpreadSheetContainer);
  TdxSpreadSheetMergedCellAccess = class(TdxSpreadSheetMergedCell);
  TdxSpreadSheetTableColumnAccess = class(TdxSpreadSheetTableColumn);
  TdxSpreadSheetTableColumnsAccess = class(TdxSpreadSheetTableColumns);
  TdxSpreadSheetTableItemsAccess = class(TdxSpreadSheetTableItems);
  TdxSpreadSheetTableRowAccess = class(TdxSpreadSheetTableRow);
  TdxSpreadSheetTableRowsAccess = class(TdxSpreadSheetTableRows);
  TdxSpreadSheetTableViewAccess = class(TdxSpreadSheetTableView);
  TdxSpreadSheetTableViewInfoAccess = class(TdxSpreadSheetTableViewInfo);

  { TdxSpreadSheetFakeCellStyle }

  TdxSpreadSheetFakeCellStyle = class(TdxSpreadSheetCellStyle)
  strict private
    function GetView: TdxSpreadSheetTableView;
  protected
    procedure Changed; override;
    procedure CloneHandle; override;
    function GetSpreadSheet: TdxCustomSpreadSheet; override;
    procedure ReplaceHandle; override;
  public
    constructor Create(AOwner: TObject); override;
    procedure Calculate(ARow: TdxSpreadSheetTableRow; AColumn: TdxSpreadSheetTableColumn);
    //
    property View: TdxSpreadSheetTableView read GetView;
  end;

  { TdxDescendingComparerDoubleF }

  TdxDescendingComparerDoubleF = class(TComparer<Double>)
  public
    function Compare(const Left, Right: Double): Integer; override;
  end;

  { TdxSpreadSheetHistoryDeleteCommand }

  TdxSpreadSheetHistoryDeleteCommand = class(TdxSpreadSheetHistoryCustomCommand)
  strict private
    FArea: TRect;
    FIsHorizontal: Boolean;
    FModification: TdxSpreadSheetCellsModification;
    FView: TdxSpreadSheetTableView;
  public
    constructor Create(AView: TdxSpreadSheetTableView; const AArea: TRect;
      AModification: TdxSpreadSheetCellsModification; AIsHorizontal: Boolean);
    procedure Redo; override;
    procedure Undo; override;
  end;

  { TdxSpreadSheetHistoryInsertCommand }

  TdxSpreadSheetHistoryInsertCommand = class(TdxSpreadSheetHistoryDeleteCommand)
  public
    procedure Redo; override;
    procedure Undo; override;
  end;

function dxConvertToNumberOrDateTime(const AValue: Variant): Variant;
begin
  Result := AValue;
  if dxIsDateTime(AValue) then
    Result := VarToDateTime(AValue)
  else
    if dxIsLogical(AValue) then
      Result := Integer(AValue = True);
end;

procedure dxSpreadSheetCalculateForEachParam(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken;
  AData: TdxSpreadSheetEnumValues; ACalculateProc: TdxSpreadSheetForEachCallBack);
var
  ACurrentParams: TdxSpreadSheetFormulaToken;
begin
  if not InRange(Sender.GetParamsCount(AParams), 1, 255) then
    AData.SetErrorCode(ecNA)
  else
  begin
    ACurrentParams := AParams;
    while AData.Validate and (ACurrentParams <> nil) do
    begin
      Sender.ForEach(ACurrentParams, ACalculateProc, AData);
      ACurrentParams := ACurrentParams.Next;
    end
  end;
end;

function dxSpreadSheetExtractedArgumentsAreNotNull(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken;
  var AArgument1, AArgument2: Variant): Boolean;
begin
  Result := not(Sender.ParameterIsNull(AArgument1, AParams) or (Sender.ParameterIsNull(AArgument2, AParams.Next)));
  if not Result then
    Sender.SetError(ecNA);
end;

{ TdxSpreadSheetFakeCellStyle }

constructor TdxSpreadSheetFakeCellStyle.Create(AOwner: TObject);
begin
  inherited Create(AOwner);
  Handle := Handle.Clone;
end;

procedure TdxSpreadSheetFakeCellStyle.Calculate(ARow: TdxSpreadSheetTableRow; AColumn: TdxSpreadSheetTableColumn);
const
  NeighborBorderSide: array[TcxBorder] of TcxBorder = (bRight, bBottom, bLeft, bTop);
var
  ABorderColor: TColor;
  ABorderStyle: TdxSpreadSheetCellBorderStyle;
  ANeighborCellStyle: TdxSpreadSheetCellStyle;
  ASide: TcxBorder;
  AViewInfo: TdxSpreadSheetTableViewInfoAccess;
begin
  AViewInfo := TdxSpreadSheetTableViewInfoAccess(TdxSpreadSheetTableViewAccess(View).ViewInfo);
  TdxSpreadSheetTableRowAccess(ARow).InitializeCellStyle(Self, AColumn);
  for ASide := Low(TcxBorder) to High(TcxBorder) do
  begin
    ANeighborCellStyle := AViewInfo.GetNeighborCellStyle(ARow.Index, AColumn.Index, ASide);
    AViewInfo.MergeBorderStyle(Borders[ASide], ANeighborCellStyle.Borders[NeighborBorderSide[ASide]], ABorderColor, ABorderStyle);
    Borders[ASide].Color := ABorderColor;
    Borders[ASide].Style := ABorderStyle;
  end;
end;

procedure TdxSpreadSheetFakeCellStyle.Changed;
begin
  // do nothing
end;

procedure TdxSpreadSheetFakeCellStyle.CloneHandle;
begin
  // do nothing
end;

function TdxSpreadSheetFakeCellStyle.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := View.SpreadSheet;
end;

procedure TdxSpreadSheetFakeCellStyle.ReplaceHandle;
begin
  // do nothing
end;

function TdxSpreadSheetFakeCellStyle.GetView: TdxSpreadSheetTableView;
begin
  Result := TdxSpreadSheetTableView(Owner);
end;

{ TdxDescendingComparerDoubleF }

function TdxDescendingComparerDoubleF.Compare(const Left, Right: Double): Integer;
begin
  Result := Sign(Right - Left);
end;

{ TdxSpreadSheetHistoryDeleteCommand }

constructor TdxSpreadSheetHistoryDeleteCommand.Create(AView: TdxSpreadSheetTableView; const AArea: TRect;
  AModification: TdxSpreadSheetCellsModification; AIsHorizontal: Boolean);
begin
  inherited Create;
  FView := AView;
  FArea := AArea;
  FModification := AModification;
  FIsHorizontal := AIsHorizontal;
end;

procedure TdxSpreadSheetHistoryDeleteCommand.Redo;
begin
  with TdxSpreadSheetTableViewDeleteCellsHelper.Create(FView) do
  try
    DoCommand(FArea, FModification, FIsHorizontal);
  finally
    Free;
  end;
end;

procedure TdxSpreadSheetHistoryDeleteCommand.Undo;
begin
  with TdxSpreadSheetTableViewInsertCellsHelper.Create(FView) do
  try
    DoCommand(FArea, FModification, FIsHorizontal);
  finally
    Free;
  end;
end;

{ TdxSpreadSheetHistoryInsertCommand }

procedure TdxSpreadSheetHistoryInsertCommand.Redo;
begin
  inherited Undo;
end;
procedure TdxSpreadSheetHistoryInsertCommand.Undo;
begin
  inherited Redo;
end;

{ TdxSpreadSheetCustomTableViewHelper }

constructor TdxSpreadSheetCustomTableViewHelper.Create(AView: TdxSpreadSheetTableView);
begin
  inherited Create;
  FView := AView;
  FView.BeginUpdate;
  ShowHourglassCursor;
end;

destructor TdxSpreadSheetCustomTableViewHelper.Destroy;
begin
  FView.EndUpdate;
  HideHourglassCursor;
  inherited Destroy;
end;

procedure TdxSpreadSheetCustomTableViewHelper.Changed;
begin
  TdxSpreadSheetTableViewAccess(View).Changed;
end;

function TdxSpreadSheetCustomTableViewHelper.GetColumns: TdxSpreadSheetTableColumns;
begin
  Result := View.Columns;
end;

function TdxSpreadSheetCustomTableViewHelper.GetHistory: TdxSpreadSheetHistory;
begin
  Result := TdxSpreadSheetTableViewAccess(View).History;
end;

function TdxSpreadSheetCustomTableViewHelper.GetRows: TdxSpreadSheetTableRows;
begin
  Result := View.Rows;
end;

function TdxSpreadSheetCustomTableViewHelper.GetSpreadSheet: TdxCustomSpreadSheet;
begin
  Result := View.SpreadSheet;
end;

{ TdxSpreadSheetTableViewCellsModificationHelper }

constructor TdxSpreadSheetTableViewCellsModificationHelper.Create(AView: TdxSpreadSheetTableView);
begin
  inherited Create(AView);
  FFixedContainers := TDictionary<TdxSpreadSheetContainer, TRect>.Create;
  FFixedContainerAnchorTypes := TDictionary<TdxSpreadSheetContainer, TdxSpreadSheetContainerAnchorType>.Create;
end;

destructor TdxSpreadSheetTableViewCellsModificationHelper.Destroy;
begin
  FreeAndNil(FFixedContainerAnchorTypes);
  FreeAndNil(FFixedContainers);
  inherited Destroy;
end;

procedure TdxSpreadSheetTableViewCellsModificationHelper.Process(AArea: TRect);
begin
  Initialize(AArea);
  try
    DoProcess(AArea);
    Changed;
    TdxSpreadSheetTableViewAccess(View).AddChanges([sscData]);
  except
    on E: EAbort do
      // do nothing
    else
      raise;
  end;
end;

procedure TdxSpreadSheetTableViewCellsModificationHelper.DoCommand(const AArea: TRect;
  AModification: TdxSpreadSheetCellsModification; AIsHorizontal: Boolean);
begin
  FMovableArea := AArea;
  FModification := AModification;
  FIsHorizontalShifting := IsHorizontalShifting;
  DoProcess(AArea);
end;

procedure TdxSpreadSheetTableViewCellsModificationHelper.Initialize(var AArea: TRect);
begin
  AArea := cxRectAdjust(AArea);
  AArea.Top := Max(AArea.Top, 0);
  AArea.Left := Max(AArea.Left, 0);
  AArea.Right := Min(AArea.Right, dxSpreadSheetMaxColumnIndex);
  AArea.Bottom := Min(AArea.Bottom, dxSpreadSheetMaxRowIndex);

  if dxSpreadSheetIsEntireRow(AArea) and (Modification = cmShiftCellsVertically) then
    Modification := cmShiftRows;
  if dxSpreadSheetIsEntireColumn(AArea) and (Modification = cmShiftCellsHorizontally) then
    Modification := cmShiftColumns;

  case Modification of
    cmShiftColumns:
      AArea := cxRect(AArea.Left, 0, AArea.Right, dxSpreadSheetMaxRowIndex);
    cmShiftRows:
      AArea := cxRect(0, AArea.Top, dxSpreadSheetMaxColumnIndex, AArea.Bottom);
  end;

  case Modification of
    cmShiftCellsHorizontally, cmShiftColumns:
      FMovableArea := cxRect(AArea.Left, AArea.Top, MaxInt, AArea.Bottom);
    cmShiftCellsVertically, cmShiftRows:
      FMovableArea := cxRect(AArea.Left, AArea.Top, AArea.Right, MaxInt);
  end;
  FIsHorizontalShifting := Modification in [cmShiftCellsHorizontally, cmShiftColumns];
end;

procedure TdxSpreadSheetTableViewCellsModificationHelper.CheckIsAreaEmpty(AItems: TdxSpreadSheetTableItems; const AArea: TRect);
begin
  if (AArea.Left <= AArea.Right) and (AArea.Top <= AArea.Bottom) then
  begin
    if not IsAreaEmpty(AItems, AArea) then
      raise EdxSpreadSheetError.Create(cxGetResourceString(@sdxErrorPossibleDataLoss));
  end;
end;

procedure TdxSpreadSheetTableViewCellsModificationHelper.CreateBlankCellsForStyledItems(
  AItems, AOpositeItems: TdxSpreadSheetTableItems; const AArea: TRect; AShiftDelta: Integer);

  procedure ApplyStyleToBlankCellsInItem(AItem: TdxSpreadSheetTableItem; AStyleHandle: TdxSpreadSheetCellStyleHandle);
  var
    ACell: TdxSpreadSheetCell;
    AOpositeItem: TdxSpreadSheetTableItem;
    I: Integer;
  begin
    for I := AArea.Top to AArea.Bottom do
      if AItem.Cells[I] = nil then
      begin
        AOpositeItem := AOpositeItems.Items[I];
        if IsHorizontalShifting and (AOpositeItem <> nil) and not AOpositeItem.Style.IsDefault then
          Continue;

        ACell := AItem.CreateCell(I);
        if AStyleHandle <> nil then
          ACell.Style.Handle := AStyleHandle
        else
          if AOpositeItem <> nil then
            ACell.Style.Handle := AOpositeItem.Style.Handle;
      end;
  end;

  procedure ApplyStyleToBlankCells(AIsSecondPass: Boolean);
  var
    AItem: TdxSpreadSheetTableItem;
    AMaxItemIndex: Integer;
  begin
    AItem := AItems.Last;
    while (AItem <> nil) and (AItem.Index > AArea.Right) do
      AItem := AItem.Prev;

    AMaxItemIndex := TdxSpreadSheetTableItemsAccess(AItems).GetMaxItemIndex;
    while (AItem <> nil) and (AItem.Index >= AArea.Left) do
    begin
      if not AItem.Style.IsDefault then
      begin
        if AIsSecondPass then
          ApplyStyleToBlankCellsInItem(AItem, nil)
        else
          if InRange(AItem.Index + AShiftDelta, AArea.Left, AMaxItemIndex) then
            ApplyStyleToBlankCellsInItem(AItems.CreateItem(AItem.Index + AShiftDelta), AItem.Style.Handle);
      end;
      AItem := AItem.Prev;
    end;
  end;

begin
  ApplyStyleToBlankCells(False);
  ApplyStyleToBlankCells(True);
end;

function TdxSpreadSheetTableViewCellsModificationHelper.IsAnchorPointInArea(
  const AArea: TRect; AAnchorPoint: TdxSpreadSheetContainerAnchorPoint): Boolean;
begin
  Result := dxSpreadSheetContains(AArea, AAnchorPoint.Cell.RowIndex, AAnchorPoint.Cell.ColumnIndex);
end;

function TdxSpreadSheetTableViewCellsModificationHelper.IsAreaEmpty(AItems: TdxSpreadSheetTableItems; const AArea: TRect): Boolean;
var
  AItem: TdxSpreadSheetTableItem;
  I: Integer;
begin
  Result := True;
  AItem := AItems.Last;
  while (AItem <> nil) and (AItem.Index > AArea.Right) do
    AItem := AItem.Prev;
  while (AItem <> nil) and (AItem.Index >= AArea.Left) do
  begin
    if not AItem.Style.IsDefault then
      Exit(False);
    for I := AArea.Top to Min(AItem.CellCount - 1, AArea.Bottom) do
    begin
      if AItem.Cells[I] <> nil then
        Exit(False);
    end;
    AItem := AItem.Prev;
  end;
end;

procedure TdxSpreadSheetTableViewCellsModificationHelper.CheckMergedCells;
begin
  if HasAffectedMergedCells then
    case MergedCellsConflictResolution of
      mccrShowConfirmation:
        if MessageDlg(cxGetResourceString(@sdxUnmergeCellsConfirmation), mtConfirmation, [mbOK, mbCancel], 0) = mrOk then
          UnmergeAllAffectedCells
        else
          Abort;

      mccrUnmergeAllAffectedCells:
        UnmergeAllAffectedCells;

    else
      raise EdxSpreadSheetError.Create(cxGetResourceString(@sdxErrorCannotMoveBecauseOfMergedCells));
    end;
end;

function TdxSpreadSheetTableViewCellsModificationHelper.HasAffectedMergedCells: Boolean;
var
  ACell: TdxSpreadSheetMergedCell;
  ARect: TRect;
begin
  Result := False;
  ACell := View.MergedCells.First;
  while ACell <> nil do
  begin
    if dxSpreadSheetIntersects(ACell.Area, MovableArea, ARect) then
    begin
      if not cxRectIsEqual(ACell.Area, ARect) then
        Exit(True);
    end;
    ACell := ACell.Next;
  end;
end;

procedure TdxSpreadSheetTableViewCellsModificationHelper.MoveMergedCells(ADeltaX, ADeltaY: Integer);
var
  AArea: TRect;
  ACell: TdxSpreadSheetMergedCell;
  ARect: TRect;
begin
  ACell := View.MergedCells.First;
  while ACell <> nil do
  begin
    if dxSpreadSheetIntersects(ACell.Area, MovableArea, ARect) then
    begin
      if cxRectIsEqual(ACell.Area, ARect) then
        AArea := cxRectOffset(ACell.Area, ADeltaX, ADeltaY)
      else
        AArea := cxRectInflate(ACell.Area, 0, 0, ADeltaX, ADeltaY);

      TdxSpreadSheetMergedCellAccess(ACell).Initialize(ACell.Owner, AArea);
    end;
    ACell := ACell.Next;
  end;
end;

procedure TdxSpreadSheetTableViewCellsModificationHelper.UnmergeAllAffectedCells;
var
  ACell: TdxSpreadSheetMergedCell;
  ACellPrev: TdxSpreadSheetMergedCell;
  ARect: TRect;
begin
  ACell := View.MergedCells.Last;
  while ACell <> nil do
  begin
    ACellPrev := ACell.Prev;
    if dxSpreadSheetIntersects(ACell.Area, MovableArea, ARect) then
    begin
      if not cxRectIsEqual(ACell.Area, ARect) then
        ACell.Free;
    end;
    ACell := ACellPrev;
  end;
end;

procedure TdxSpreadSheetTableViewCellsModificationHelper.StoreFixedContainersBounds;

  function NeedSaveBounds(AContainer: TdxSpreadSheetContainerAccess): Boolean;
  var
    R: TRect;
  begin
    Result := False;
    case AContainer.AnchorType of
      catOneCell:
        Result := not (AContainer.AnchorPoint1.FixedToCell and IsAnchorPointInArea(MovableArea, AContainer.AnchorPoint1));
      catTwoCell:
        begin
          R := MovableArea;
          if IsHorizontalShifting then
            R.Left := 0
          else
            R.Top := 0;

          Result := not (
            AContainer.AnchorPoint1.FixedToCell and IsAnchorPointInArea(R, AContainer.AnchorPoint1) and
            AContainer.AnchorPoint2.FixedToCell and IsAnchorPointInArea(R, AContainer.AnchorPoint2));
        end;
    end;
  end;

var
  AContainer: TdxSpreadSheetContainerAccess;
  I: Integer;
begin
  for I := 0 to View.Containers.Count - 1 do
  begin
    AContainer := TdxSpreadSheetContainerAccess(View.Containers[I]);
    if NeedSaveBounds(AContainer) then
    begin
      FixedContainerAnchorTypes.Add(AContainer, AContainer.AnchorType);
      FixedContainers.Add(AContainer, AContainer.Calculator.CalculateBounds);
    end;
  end;
end;

procedure TdxSpreadSheetTableViewCellsModificationHelper.RestoreFixedContainersBounds;
var
  AAnchorType: TdxSpreadSheetContainerAnchorType;
  AContainer: TdxSpreadSheetContainerAccess;
  AContainerBounds: TRect;
  ATempContainer: TdxSpreadSheetContainer;
begin
  for ATempContainer in FixedContainers.Keys do
  begin
    AContainer := TdxSpreadSheetContainerAccess(ATempContainer);
    AContainer.BeginUpdate;
    try
      AContainerBounds := FixedContainers.Items[AContainer];

      AAnchorType := FixedContainerAnchorTypes.Items[AContainer];
      if AContainer.AnchorType <> AAnchorType then
      begin
        AContainer.AnchorType := AAnchorType;
        AContainer.Calculator.UpdateAnchors(AContainerBounds);
      end;
      if not AContainer.Calculator.UpdateAnchorsAfterResize(AContainerBounds) then
        AContainer.Calculator.UpdateAnchors(AContainerBounds);
    finally
      AContainer.EndUpdate;
    end;
  end;
end;

procedure TdxSpreadSheetTableViewCellsModificationHelper.ShiftCellsHorizontally(
  AStartFromIndex, AStartRowIndex, AFinishRowIndex, ADelta: Integer);
begin
  if ADelta > 0 then
    CheckIsAreaEmpty(Columns, cxRect(dxSpreadSheetMaxColumnIndex - ADelta + 1,
      AStartRowIndex, dxSpreadSheetMaxColumnIndex, AFinishRowIndex));

  CheckMergedCells;
  TdxSpreadSheetTableRowsAccess(Rows).ForEach(
    procedure (AItem: TdxDynamicListItem; AData: Pointer)
    begin
      ShiftIndexex(TdxSpreadSheetTableRowAccess(AItem).RowCells, AStartFromIndex, ADelta);
    end,
    AStartRowIndex, AFinishRowIndex);

  TdxSpreadSheetTableRowsAccess(Rows).ForEachCell(
    cxRect(AStartFromIndex, AStartRowIndex, MaxInt, AFinishRowIndex),
    procedure (AItem: TdxDynamicListItem; AData: Pointer)
    begin
      Columns.CreateItem(TdxSpreadSheetCell(AItem).ColumnIndex);
    end);

  MoveMergedCells(ADelta, 0);
end;

procedure TdxSpreadSheetTableViewCellsModificationHelper.ShiftCellsVertically(
  AStartFromIndex, AStartColumnIndex, AFinishColumnIndex, ADelta: Integer);
begin
  if ADelta > 0 then
    CheckIsAreaEmpty(Rows, cxRect(dxSpreadSheetMaxRowIndex - ADelta + 1,
      AStartColumnIndex, dxSpreadSheetMaxRowIndex, AFinishColumnIndex));

  CheckMergedCells;
  TdxSpreadSheetTableRowsAccess(Rows).ForEach(
    procedure (AItem: TdxDynamicListItem; AData: Pointer)
    var
      ACell: TdxSpreadSheetCellAccess;
      ANextCell: TdxSpreadSheetCellAccess;
      ARow: TdxSpreadSheetTableRowAccess;
      ATargetRowIndex: Integer;
    begin
      ARow := TdxSpreadSheetTableRowAccess(AItem);
      ATargetRowIndex := ARow.Index + ADelta;
      ACell := TdxSpreadSheetCellAccess(ARow.RowCells.First);
      while ACell <> nil do
      begin
        ANextCell := TdxSpreadSheetCellAccess(ACell.FNext);
        if InRange(ACell.ColumnIndex, AStartColumnIndex, AFinishColumnIndex) then
          ACell.RowIndex := ATargetRowIndex;
        ACell := ANextCell;
      end;
    end,
    AStartFromIndex, MaxInt, ADelta <= 0);
  MoveMergedCells(0, ADelta);
end;

procedure TdxSpreadSheetTableViewCellsModificationHelper.ShiftColumns(AStartFromIndex, ADelta: Integer);
begin
  if ADelta > 0 then
    CheckIsAreaEmpty(Columns, cxRect(dxSpreadSheetMaxColumnIndex - ADelta + 1, 0, dxSpreadSheetMaxColumnIndex, MaxInt));
  ShiftIndexex(Columns, AStartFromIndex, ADelta);
  if View.FrozenColumn >= AStartFromIndex then
    View.FrozenColumn := View.FrozenColumn + ADelta;
  MoveMergedCells(ADelta, 0);
end;

procedure TdxSpreadSheetTableViewCellsModificationHelper.ShiftIndexex(
  AList: TdxDynamicItemList; AStartFromIndex, ADelta: Integer);
begin
  TdxDynamicItemListAccess(AList).ShiftIndexex(AStartFromIndex, ADelta);
end;

procedure TdxSpreadSheetTableViewCellsModificationHelper.ShiftRows(AStartFromIndex, ADelta: Integer);
begin
  if ADelta > 0 then
    CheckIsAreaEmpty(Rows, cxRect(dxSpreadSheetMaxRowIndex - ADelta + 1, 0, dxSpreadSheetMaxRowIndex, MaxInt));
  ShiftIndexex(Rows, AStartFromIndex, ADelta);
  if View.FrozenRow >= AStartFromIndex then
    View.FrozenRow := View.FrozenRow + ADelta;
  MoveMergedCells(0, ADelta);
end;

{ TdxSpreadSheetTableViewDeleteCellsHelper }

procedure TdxSpreadSheetTableViewDeleteCellsHelper.CheckContainer(const AArea: TRect; AContainer: TdxSpreadSheetContainer);
var
  ANeedDelete, ARemoveFromFixedContainers: Boolean;
begin
  ANeedDelete := False;
  ARemoveFromFixedContainers := True;
  case AContainer.AnchorType of
    catTwoCell:
      if AContainer.AnchorPoint1.FixedToCell and IsAnchorPointInArea(AArea, AContainer.AnchorPoint1) and
         AContainer.AnchorPoint2.FixedToCell and IsAnchorPointInArea(AArea, AContainer.AnchorPoint2)
      then
        ANeedDelete := True
      else
        if AContainer.AnchorPoint1.FixedToCell and IsAnchorPointInArea(AArea, AContainer.AnchorPoint1) then
        begin
          MoveAnchor(AContainer.AnchorPoint1, AArea, True);
          ARemoveFromFixedContainers := AContainer.AnchorPoint2.FixedToCell;
        end
        else
          if AContainer.AnchorPoint1.FixedToCell and AContainer.AnchorPoint2.FixedToCell and
            IsAnchorPointInArea(AArea, AContainer.AnchorPoint2)
          then
            MoveAnchor(AContainer.AnchorPoint2, AArea, False)
          else
            ARemoveFromFixedContainers := False;

    catOneCell:
      if AContainer.AnchorPoint1.FixedToCell and IsAnchorPointInArea(AArea, AContainer.AnchorPoint1) then
        MoveAnchor(AContainer.AnchorPoint1, AArea, True)
      else
        ARemoveFromFixedContainers := False;

  else
    ARemoveFromFixedContainers := False;
  end;

  if ARemoveFromFixedContainers then
    FixedContainers.Remove(AContainer);
  if ANeedDelete then
    AContainer.Free;
end;

procedure TdxSpreadSheetTableViewDeleteCellsHelper.CheckContainers(const AArea: TRect);
var
  I: Integer;
begin
  for I := View.Containers.Count - 1 downto 0 do
    CheckContainer(AArea, View.Containers[I]);
end;

procedure TdxSpreadSheetTableViewDeleteCellsHelper.DeleteCellsInArea(const AArea: TRect);
begin
  TdxSpreadSheetTableRowsAccess(Rows).ForEachCell(AArea,
    procedure (AItem: TdxDynamicListItem; AData: Pointer)
    begin
      AItem.Free;
    end,
    False);
end;

procedure TdxSpreadSheetTableViewDeleteCellsHelper.DeleteItems(
  AItems: TdxSpreadSheetTableItems; AStartIndex, AFinishIndex: Integer);
begin
  TdxDynamicItemListAccess(AItems).ForEach(
    procedure (AItem: TdxDynamicListItem; AData: Pointer)
    begin
      AItem.Free;
    end,
    AStartIndex, AFinishIndex);
end;

procedure TdxSpreadSheetTableViewDeleteCellsHelper.DoProcess(const AArea: TRect);
begin
  History.BeginAction(TdxSpreadSheetHistoryDeleteCellsAction);
  try
    if Modification in [cmShiftCellsHorizontally, cmShiftCellsVertically] then
      CheckMergedCells;
    SpreadSheet.FormulaController.UpdateReferences(View, AArea, Modification, True);
    StoreFixedContainersBounds;
    try
      View.MergedCells.DeleteItemsInArea(AArea);
      CheckContainers(AArea);
      DeleteCellsInArea(AArea);

      case Modification of
        cmShiftColumns:
          begin
            DeleteItems(Columns, AArea.Left, AArea.Right);
            ShiftColumns(AArea.Left, -(cxRectWidth(AArea) + 1));
          end;

        cmShiftRows:
          begin
            DeleteItems(Rows, AArea.Top, AArea.Bottom);
            ShiftRows(AArea.Top, -(cxRectHeight(AArea) + 1));
          end;

        cmShiftCellsHorizontally:
          begin
            ShiftCellsHorizontally(AArea.Left, AArea.Top, AArea.Bottom, -(cxRectWidth(AArea) + 1));
            CreateBlankCellsForStyledItems(Columns, Rows, MovableArea, -(cxRectWidth(AArea) + 1));
          end;

        cmShiftCellsVertically:
          begin
            ShiftCellsVertically(AArea.Top, AArea.Left, AArea.Right, -(cxRectHeight(AArea) + 1));
            CreateBlankCellsForStyledItems(Rows, Columns, cxRect(MovableArea.Top,
              MovableArea.Left, MovableArea.Bottom, MovableArea.Right), -(cxRectHeight(AArea) + 1));
          end;
      end;
    finally
      RestoreFixedContainersBounds;
    end;
    History.AddCommand(TdxSpreadSheetHistoryDeleteCommand.Create(View, AArea, Modification, IsHorizontalShifting));
  finally
    History.EndAction;
  end;
end;

procedure TdxSpreadSheetTableViewDeleteCellsHelper.MoveAnchor(
  AAnchor: TdxSpreadSheetContainerAnchorPoint; const AArea: TRect; AMoveForward: Boolean);
var
  AIndex: Integer;
  AOffset: TPoint;
begin
  if IsHorizontalShifting then
  begin
    AIndex := IfThen(AMoveForward, AArea.Right + 1, AArea.Left - 1);
    AIndex := Min(Max(AIndex, 0), dxSpreadSheetMaxColumnIndex);
    AAnchor.Cell := View.CreateCell(AAnchor.Cell.RowIndex, AIndex);
  end
  else
  begin
    AIndex := IfThen(AMoveForward, AArea.Bottom + 1, AArea.Top - 1);
    AIndex := Min(Max(AIndex, 0), dxSpreadSheetMaxRowIndex);
    AAnchor.Cell := View.CreateCell(AIndex, AAnchor.Cell.ColumnIndex);
  end;

  if AMoveForward then
    AOffset := cxNullPoint
  else
    AOffset := cxPoint(cxSize(AAnchor.Cell.GetAbsoluteBounds));

  if IsHorizontalShifting then
    AAnchor.Offset := cxPoint(AOffset.X, AAnchor.Offset.Y)
  else
    AAnchor.Offset := cxPoint(AAnchor.Offset.X, AOffset.Y);
end;

{ TdxSpreadSheetTableViewInsertCellsHelper }

procedure TdxSpreadSheetTableViewInsertCellsHelper.CalculateStyle(
  ANewStyle, ALeftSourceStyle, ARightSourceStyle: TdxSpreadSheetCellStyle);

  procedure GetSides(out ALeftSide, ARightSide: TcxBorder);
  begin
    if IsHorizontalShifting then
    begin
      ARightSide := bRight;
      ALeftSide := bLeft;
    end
    else
    begin
      ARightSide := bBottom;
      ALeftSide := bTop;
    end;
  end;

  function MergeBorderStyles: Boolean;
  var
    ALeftSide, ARightSide: TcxBorder;
  begin
    Result := False;
    if (ALeftSourceStyle = nil) or (ARightSourceStyle = nil) then
      Exit;

    GetSides(ALeftSide, ARightSide);
    Result := (ALeftSourceStyle.Borders[ARightSide].Style <> sscbsDefault) and
      (ARightSourceStyle.Borders[ALeftSide].Style <> sscbsDefault);

    if Result then
    begin
      ANewStyle.BeginUpdate;
      try
        ANewStyle.Borders[ARightSide].Assign(ARightSourceStyle.Borders[ALeftSide]);
        ANewStyle.Borders[ALeftSide].Assign(ALeftSourceStyle.Borders[ARightSide]);
      finally
        ANewStyle.EndUpdate;
      end;
    end;
  end;

begin
  if ALeftSourceStyle <> nil then
    ANewStyle.Handle := ALeftSourceStyle.Handle;

  if not MergeBorderStyles then
  begin
    ANewStyle.BeginUpdate;
    try
      ANewStyle.Handle.Borders := View.CellStyles.DefaultStyle.Borders;
    finally
      ANewStyle.EndUpdate;
    end;
  end;
end;

procedure TdxSpreadSheetTableViewInsertCellsHelper.CalculateStyleForNewCells(
  ATarget, ALeftSource, ARightSource: TdxSpreadSheetTableItem;
  AOpositeItems: TdxSpreadSheetTableItems; AStartIndex, AFinishIndex: Integer);
var
  ALeftSourceStyle: TdxSpreadSheetCellStyle;
  ARightSourceStyle: TdxSpreadSheetCellStyle;
  I: Integer;
begin
  AFinishIndex := Min(AFinishIndex, View.Dimensions.Bottom);
  for I := AStartIndex to AFinishIndex do
  begin
    ALeftSourceStyle := GetCellStyle(ALeftSource, I, AOpositeItems);
    ARightSourceStyle := GetCellStyle(ARightSource, I, AOpositeItems);
    if (ALeftSourceStyle <> nil) or (ARightSourceStyle <> nil) then
      CalculateStyle(ATarget.CreateCell(I).Style, ALeftSourceStyle, ARightSourceStyle);
  end;
end;

procedure TdxSpreadSheetTableViewInsertCellsHelper.CalculateStyleForNewItems(
  const AArea: TRect; AItems, AOpositeItems: TdxSpreadSheetTableItems);
var
  AItem: TdxSpreadSheetTableItem;
  ALeftSourceItem: TdxSpreadSheetTableItem;
  ARightSourceItem: TdxSpreadSheetTableItem;
  I: Integer;
begin
  if Modification in [cmShiftCellsHorizontally, cmShiftCellsVertically] then
  begin
    CreateBlankCellsForStyledItems(AItems, AOpositeItems,
      cxRect(AArea.Left, AArea.Top, MaxInt, AArea.Bottom), cxRectWidth(AArea) + 1);
  end;

  if AArea.Left > 0 then
  begin
    ALeftSourceItem := AItems[AArea.Left - 1];
    ARightSourceItem := AItems[AArea.Right + 1];
    for I := AArea.Left to AArea.Right do
    begin
      AItem := AItems.CreateItem(I);
      if Modification in [cmShiftColumns, cmShiftRows] then
      begin
        CalculateStyle(AItem.Style, GetStyle(ALeftSourceItem), GetStyle(ARightSourceItem));
        if (ALeftSourceItem <> nil) and not ALeftSourceItem.DefaultSize then
          AItem.Size := ALeftSourceItem.Size;
      end;
      CalculateStyleForNewCells(AItem, ALeftSourceItem, ARightSourceItem, AOpositeItems, AArea.Top, AArea.Bottom);
    end;
  end;
end;

procedure TdxSpreadSheetTableViewInsertCellsHelper.DoProcess(const AArea: TRect);
begin
  History.BeginAction(TdxSpreadSheetHistoryInsertCellsAction);
  try
    History.AddCommand(TdxSpreadSheetHistoryInsertCommand.Create(View, AArea, Modification, IsHorizontalShifting));
    SpreadSheet.FormulaController.UpdateReferences(View, AArea, Modification, False);
    case Modification of
      cmShiftColumns:
        begin
          ShiftColumns(AArea.Left, cxRectWidth(AArea) + 1);
          CalculateStyleForNewItems(cxRect(AArea.Left, 0, AArea.Right, MaxInt), Columns, Rows);
        end;

      cmShiftRows:
        begin
          ShiftRows(AArea.Top, cxRectHeight(AArea) + 1);
          CalculateStyleForNewItems(cxRect(AArea.Top, 0, AArea.Bottom, MaxInt), Rows, Columns);
        end;

      cmShiftCellsHorizontally:
        begin
          StoreFixedContainersBounds;
          ShiftCellsHorizontally(AArea.Left, AArea.Top, AArea.Bottom, cxRectWidth(AArea) + 1);
          CalculateStyleForNewItems(AArea, Columns, Rows);
          RestoreFixedContainersBounds;
        end;

      cmShiftCellsVertically:
        begin
          StoreFixedContainersBounds;
          ShiftCellsVertically(AArea.Top, AArea.Left, AArea.Right, cxRectHeight(AArea) + 1);
          CalculateStyleForNewItems(cxRect(AArea.Top, AArea.Left, AArea.Bottom, AArea.Right), Rows, Columns);
          RestoreFixedContainersBounds;
        end;
    end;
  finally
    History.EndAction;
  end;
end;

function TdxSpreadSheetTableViewInsertCellsHelper.GetCellStyle(ASource: TdxSpreadSheetTableItem;
  ACellIndex: Integer; AOpositeItems: TdxSpreadSheetTableItems): TdxSpreadSheetCellStyle;
var
  ACell: TdxSpreadSheetCell;
begin
  if ASource <> nil then
    ACell := ASource.Cells[ACellIndex]
  else
    ACell := nil;

  if ACell <> nil then
    Result := ACell.Style
  else
    if IsHorizontalShifting then
    begin
      Result := GetStyle(AOpositeItems.Items[ACellIndex]);
      if (Result = nil) or Result.IsDefault then
        Result := GetStyle(ASource);
    end
    else
    begin
      Result := GetStyle(ASource);
      if (Result = nil) or Result.IsDefault then
        Result := GetStyle(AOpositeItems.Items[ACellIndex]);
    end;
end;

function TdxSpreadSheetTableViewInsertCellsHelper.GetStyle(AItem: TdxSpreadSheetTableItem): TdxSpreadSheetCellStyle;
begin
  if AItem <> nil then
    Result := AItem.Style
  else
    Result := nil;
end;

{ TdxSpreadSheetTableViewMergeCellStyleHelper }

constructor TdxSpreadSheetTableViewMergeCellStyleHelper.Create(AMergeCell: TdxSpreadSheetMergedCell);
begin
  inherited Create(AMergeCell.Owner.View);
  FMergeCell := AMergeCell;
end;

class procedure TdxSpreadSheetTableViewMergeCellStyleHelper.Calculate(AMergeCell: TdxSpreadSheetMergedCell);
begin
  with Create(AMergeCell) do
  try
    DoCalculate;
  finally
    Free;
  end;
end;

procedure TdxSpreadSheetTableViewMergeCellStyleHelper.CalculateCellValues;
begin
  FNonBlankCell := nil;
  TdxSpreadSheetTableRowsAccess(View.Rows).ForEachCell(MergeCell.Area,
    procedure (AItem: TdxDynamicListItem; AData: Pointer)
    begin
      if not TdxSpreadSheetCell(AItem).IsEmpty then
      begin
        if FNonBlankCell = nil then
          FNonBlankCell := TdxSpreadSheetCell(AItem)
        else
          TdxSpreadSheetCell(AItem).IsEmpty := True;
      end;
    end);
end;

procedure TdxSpreadSheetTableViewMergeCellStyleHelper.CalculateBorder(ABorder: TcxBorder; AItem: TdxSpreadSheetTableItem;
  AStartIndex, AFinishIndex: Integer; out ABorderColor: TColor; out ABorderStyle: TdxSpreadSheetCellBorderStyle);

  procedure GetBorderParams(ACell: TdxSpreadSheetCell; out ABorderColor: TColor; out ABorderStyle: TdxSpreadSheetCellBorderStyle);
  begin
    if ACell <> nil then
    begin
      ABorderColor := ACell.Style.Borders[ABorder].Color;
      ABorderStyle := ACell.Style.Borders[ABorder].Style;
    end
    else
    begin
      ABorderColor := clDefault;
      ABorderStyle := sscbsDefault;
    end;
  end;

  procedure ResetBorderParams(out ABorderColor: TColor; out ABorderStyle: TdxSpreadSheetCellBorderStyle);
  begin
    GetBorderParams(nil, ABorderColor, ABorderStyle);
  end;

var
  ATempBorderColor: TColor;
  ATempBorderStyle: TdxSpreadSheetCellBorderStyle;
begin
  if AItem = nil then
  begin
    ResetBorderParams(ABorderColor, ABorderStyle);
    Exit;
  end;

  GetBorderParams(AItem.Cells[AStartIndex], ABorderColor, ABorderStyle);
  while AStartIndex <= AFinishIndex do
  begin
    GetBorderParams(AItem.Cells[AStartIndex], ATempBorderColor, ATempBorderStyle);
    if (ABorderColor <> ATempBorderColor) or (ABorderStyle <> ATempBorderStyle) then
    begin
      ResetBorderParams(ABorderColor, ABorderStyle);
      Break;
    end;
    Inc(AStartIndex);
  end;
end;

procedure TdxSpreadSheetTableViewMergeCellStyleHelper.CalculateCellStyles;
var
  AArea: TRect;
  ABorder: TcxBorder;
  AHasStyle: Boolean;
  AStylePattern: TdxSpreadSheetCellStyle;
begin
  AArea := MergeCell.Area;
  AArea.Right := Min(AArea.Right, dxSpreadSheetMaxColumnIndex);
  AArea.Bottom := Min(AArea.Bottom, dxSpreadSheetMaxRowIndex);

  CalculateBorder(bBottom, Rows[AArea.Bottom], AArea.Left, AArea.Right, FBorderColors[bBottom], FBorderStyles[bBottom]);
  CalculateBorder(bLeft, Columns[AArea.Left], AArea.Top, AArea.Bottom, FBorderColors[bLeft], FBorderStyles[bLeft]);
  CalculateBorder(bRight, Columns[AArea.Right], AArea.Top, AArea.Bottom, FBorderColors[bRight], FBorderStyles[bRight]);
  CalculateBorder(bTop, Rows[AArea.Top], AArea.Left, AArea.Right, FBorderColors[bTop], FBorderStyles[bTop]);

  AStylePattern := GetStylePattern;
  AHasStyle := not AStylePattern.IsDefault;
  for ABorder := Low(ABorder) to High(ABorder) do
    AHasStyle := AHasStyle or (FBorderStyles[ABorder] <> sscbsDefault);

  if not AHasStyle then
  begin
    TdxSpreadSheetTableRowsAccess(View.Rows).ForEachCell(AArea,
      procedure (AItem: TdxDynamicListItem; AData: Pointer)
      begin
        TdxSpreadSheetCell(AItem).Style.Handle := AStylePattern.Handle;
      end);
    Exit;
  end;

  if dxSpreadSheetIsEntireColumn(AArea) <> dxSpreadSheetIsEntireRow(AArea) then
  begin
    if dxSpreadSheetIsEntireColumn(AArea) then
      CalculateCellStylesInEntireColumnOrRow(View.Columns, AArea.Left, AArea.Right, AStylePattern)
    else
      if dxSpreadSheetIsEntireRow(AArea) then
        CalculateCellStylesInEntireColumnOrRow(View.Rows, AArea.Top, AArea.Bottom, AStylePattern)
      else
        CalculateCellStylesInCustomArea(AArea, AStylePattern);
  end;
end;

procedure TdxSpreadSheetTableViewMergeCellStyleHelper.CalculateCellStylesInCustomArea(
  const AArea: TRect; AStylePattern: TdxSpreadSheetCellStyle);
var
  ACell: TdxSpreadSheetCell;
  AColumnIndex: Integer;
  AIsBottomRow: Boolean;
  AIsTopRow: Boolean;
  ARow: TdxSpreadSheetTableRow;
  ARowIndex: Integer;
begin
  for ARowIndex := AArea.Top to AArea.Bottom do
  begin
    ARow := View.Rows.CreateItem(ARowIndex);
    AIsBottomRow := ARowIndex = AArea.Bottom;
    AIsTopRow := ARowIndex = AArea.Top;
    for AColumnIndex := AArea.Left to AArea.Right do
    begin
      ACell := ARow.CreateCell(AColumnIndex);
      ACell.Style.Assign(AStylePattern);

      if AIsTopRow then
      begin
        ACell.Style.Borders[bTop].Color := FBorderColors[bTop];
        ACell.Style.Borders[bTop].Style := FBorderStyles[bTop];
      end
      else
        ACell.Style.Borders[bTop].Reset;

      if AIsBottomRow then
      begin
        ACell.Style.Borders[bBottom].Color := FBorderColors[bBottom];
        ACell.Style.Borders[bBottom].Style := FBorderStyles[bBottom];
      end
      else
        ACell.Style.Borders[bBottom].Reset;

      if AColumnIndex = AArea.Left then
      begin
        ACell.Style.Borders[bLeft].Color := FBorderColors[bLeft];
        ACell.Style.Borders[bLeft].Style := FBorderStyles[bLeft];
      end
      else
        ACell.Style.Borders[bLeft].Reset;

      if AColumnIndex = AArea.Right then
      begin
        ACell.Style.Borders[bRight].Color := FBorderColors[bRight];
        ACell.Style.Borders[bRight].Style := FBorderStyles[bRight];
      end
      else
        ACell.Style.Borders[bRight].Reset;
    end;
  end;
end;

procedure TdxSpreadSheetTableViewMergeCellStyleHelper.CalculateCellStylesInEntireColumnOrRow(
  AItems: TdxSpreadSheetTableItems; AStartIndex, AFinishIndex: Integer; AStylePattern: TdxSpreadSheetCellStyle);
var
  AItem: TdxSpreadSheetTableItem;
  AItemIndex: Integer;
  ALeftSide: TcxBorder;
  ARightSide: TcxBorder;
begin
  if AItems = View.Rows then
  begin
    ALeftSide := bLeft;
    ARightSide := bRight;
  end
  else
  begin
    ALeftSide := bTop;
    ARightSide := bRight;
  end;

  for AItemIndex := AStartIndex to AFinishIndex do
  begin
    AItem := AItems.CreateItem(AItemIndex);
    AItem.Style.Assign(AStylePattern);

    if AItemIndex = AStartIndex then
    begin
      AItem.Style.Borders[ALeftSide].Color := FBorderColors[ALeftSide];
      AItem.Style.Borders[ALeftSide].Style := FBorderStyles[ALeftSide];
    end
    else
      AItem.Style.Borders[ALeftSide].Reset;

    if AItemIndex = AFinishIndex then
    begin
      AItem.Style.Borders[ARightSide].Color := FBorderColors[ARightSide];
      AItem.Style.Borders[ARightSide].Style := FBorderStyles[ARightSide];
    end
    else
      AItem.Style.Borders[ARightSide].Reset;
  end;
end;

procedure TdxSpreadSheetTableViewMergeCellStyleHelper.DoCalculate;
begin
  CalculateCellValues;
  CalculateCellStyles;
  SetupActiveCellValue;
end;

procedure TdxSpreadSheetTableViewMergeCellStyleHelper.SetupActiveCellValue;
begin
  if (NonBlankCell <> nil) and (NonBlankCell <> ActiveCell) then
  begin
    if ActiveCell = nil then
      View.CreateCell(MergeCell.Area.Top, MergeCell.Area.Left);
    TdxSpreadSheetCellAccess(ActiveCell).AssignData(NonBlankCell);
    NonBlankCell.IsEmpty := True;
  end;
end;

function TdxSpreadSheetTableViewMergeCellStyleHelper.GetActiveCell: TdxSpreadSheetCell;
begin
  Result := MergeCell.ActiveCell;
end;

function TdxSpreadSheetTableViewMergeCellStyleHelper.GetDefaultStyle: TdxSpreadSheetCellStyle;
begin
  Result := SpreadSheet.DefaultCellStyle;
end;

function TdxSpreadSheetTableViewMergeCellStyleHelper.GetStylePattern: TdxSpreadSheetCellStyle;
begin
  if NonBlankCell <> nil then
    Result := NonBlankCell.Style
  else
    if ActiveCell <> nil then
      Result := ActiveCell.Style
    else
      Result := DefaultStyle;
end;

{ TdxSpreadSheetTableViewSortingHelper }

constructor TdxSpreadSheetTableViewSortingHelper.Create(AView: TdxSpreadSheetTableView);
begin
  inherited Create(AView);
  SortOrders := TList<TdxSortOrder>.Create;
  SortIndexes := TList<Integer>.Create;
  History.BeginAction(TdxSpreadSheetHistorySortingAction);
end;

destructor TdxSpreadSheetTableViewSortingHelper.Destroy;
begin
  History.EndAction;
  FreeAndNil(SortOrders);
  FreeAndNil(SortIndexes);
  inherited Destroy;
end;

procedure TdxSpreadSheetTableViewSortingHelper.SortByColumnValues(const AArea: TRect;
  const ASortOrders: array of TdxSortOrder; const AColumnIndexes: array of Integer);
var
  AIndex, ARow: Integer;
begin
  DefaultSorting := True;
  ValidateArea(AArea);
  AIndex := 0;
  for ARow := Area.Top to Area.Bottom do
    if IsRowEmpty(ARow) then
      SortIndexes.Add(ARow)
    else
    begin
      SortIndexes.Insert(AIndex, ARow);
      Inc(AIndex);
    end;
  if AIndex = 0 then Exit;
  Area.Bottom := Area.Top + AIndex - 1;
  for ARow := Area.Top to Area.Bottom do
    if SortIndexes[ARow - Area.Top] <> ARow then
      ExchangeRowValues(ARow, SortIndexes[ARow - Area.Top]);
  if Initialize(Area.Left, Area.Right, ASortOrders, AColumnIndexes) then
    DoSort(Area.Top, Area.Bottom, 0);
end;

procedure TdxSpreadSheetTableViewSortingHelper.SortByRowValues(const AArea: TRect;
  const ASortOrders: array of TdxSortOrder; const ARowIndexes: array of Integer);
var
  AIndex, AColumn: Integer;
begin
  ValidateArea(AArea);
  AIndex := 0;
  for AColumn := Area.Left to Area.Right do
    if IsColumnEmpty(AColumn) then
      SortIndexes.Add(AColumn)
    else
    begin
      SortIndexes.Insert(AIndex, AColumn);
      Inc(AIndex);
    end;
  if AIndex = 0 then Exit;
  Area.Right := Area.Left + AIndex - 1;
  for AColumn := Area.Left to Area.Right do
    if SortIndexes[AColumn - Area.Left] <> AColumn then
      ExchangeColumnValues(AColumn, SortIndexes[AColumn - Area.Left]);
  if Initialize(Area.Top, Area.Bottom, ASortOrders, ARowIndexes) then
    DoSort(Area.Left, Area.Right, 0);
end;

function TdxSpreadSheetTableViewSortingHelper.Compare(AItem1, AItem2: Pointer): Integer;
var
  AData1, AData2: TdxSpreadSheetCellData;
begin
  Result := 0;
  AData1 := TdxSpreadSheetCellData(AItem1);
  AData2 := TdxSpreadSheetCellData(AItem2);
  if AData1 = AData2 then
    Exit
  else
    Result := CompareCells(AData1, AData2);
  if Result <> 0 then
  begin
    if SortOrder = soDescending then
      Result := -Result;
  end
  else
  begin
    Result := dxCompareValues(AData1.Row, AData2.Row);
    if Result = 0 then
      Result := dxCompareValues(AData1.Column, AData2.Column);
  end;
end;

function TdxSpreadSheetTableViewSortingHelper.CompareCells(AData1, AData2: TdxSpreadSheetCellData): Integer;
begin
  if AData1 = AData2 then
    Result := 0
  else
    Result := CompareValues(AData1.Value, AData2.Value);
end;

function TdxSpreadSheetTableViewSortingHelper.CompareValues(const AValue1, AValue2: Variant): Integer;
begin
  Result := VarCompare(AValue1, AValue2);
end;

procedure TdxSpreadSheetTableViewSortingHelper.SwapValues(AValue1, AValue2: TdxSpreadSheetCellData);
begin
  if DefaultSorting then
  begin
    ExchangeRowValues(AValue1.Row, AValue2.Row);
    ExchangeLongWords(AValue1.Row, AValue2.Row);
  end
  else
  begin
    ExchangeColumnValues(AValue1.Column, AValue2.Column);
    ExchangeLongWords(AValue1.Column, AValue2.Column);
  end;
end;

procedure TdxSpreadSheetTableViewSortingHelper.DoSort(AFromIndex, AToIndex, ASortIndex: Integer);
var
  AValues: TcxObjectList;
  AIndex, AIndex2: Integer;
begin
  if (AFromIndex = AToIndex) or (ASortIndex >= SortOrders.Count) then Exit;
  AValues := TcxObjectList.Create;
  try
    AValues.Capacity := AToIndex - AFromIndex + 1;
    for AIndex := AFromIndex to AToIndex do
      if DefaultSorting then
        AValues.Add(GetCellData(AIndex, SortIndexes[ASortIndex]))
      else
        AValues.Add(GetCellData(SortIndexes[ASortIndex], AIndex));
    SortOrder := SortOrders[ASortIndex];
    SortValues(AValues, 0, AValues.Count - 1);
    AIndex := AFromIndex;
    while AIndex < AToIndex do
    begin
      AIndex2 := AIndex + 1;
      while (AIndex2 < AToIndex) do
        if CompareCells(TdxSpreadSheetCellData(AValues[AIndex - AFromIndex]), TdxSpreadSheetCellData(AValues[AIndex2 - AFromIndex])) = 0 then
          Inc(AIndex2)
        else
        begin
          Dec(AIndex2);
          Break;
        end;
      if AIndex <> AIndex2 then
        DoSort(AIndex, AIndex2, ASortIndex + 1);
      AIndex := AIndex2 + 1;
    end;
  finally
    AValues.Free;
  end;
end;

procedure TdxSpreadSheetTableViewSortingHelper.ExchangeColumnValues(AColumn1, AColumn2: Integer);
var
  ARow: Integer;
begin
  for ARow := Area.Top to Area.Bottom do
    TdxSpreadSheetTableViewAccess(View).ExchangeValues(ARow, AColumn1, ARow, AColumn2);
end;

procedure TdxSpreadSheetTableViewSortingHelper.ExchangeRowValues(ARow1, ARow2: Integer);
var
  AColumn: Integer;
begin
  for AColumn := Area.Left to Area.Right do
    TdxSpreadSheetTableViewAccess(View).ExchangeValues(ARow1, AColumn, ARow2, AColumn);
end;

function TdxSpreadSheetTableViewSortingHelper.GetCellData(ARow, AColumn: Integer): TdxSpreadSheetCellData;
var
  AValue: Variant;
  ACell: TdxSpreadSheetCell;
begin
  ACell := View.Cells[ARow, AColumn];
  if ACell = nil then
    AValue := Null
  else
    AValue := ACell.AsVariant;
  Result := TdxSpreadSheetCellData.Create(ARow, AColumn, AValue);
end;

function TdxSpreadSheetTableViewSortingHelper.Initialize(AIndex1, AIndex2: Integer;
  const ASortOrders: array of TdxSortOrder; const AIndexes: array of Integer): Boolean;
var
  AIndex: Integer;
begin
  SortOrders.Clear;
  SortIndexes.Clear;
  if Length(ASortOrders) = 0 then
  begin
    SortOrders.Count := AIndex2 - AIndex1 + 1;
    SortIndexes.Count := AIndex2 - AIndex1 + 1;
    for AIndex := 0 to AIndex2 - AIndex1 do
    begin
      SortIndexes[AIndex] := AIndex + AIndex1;
      SortOrders[AIndex] := soAscending;
    end;
  end
  else
  begin
    for AIndex := 0 to Length(ASortOrders) - 1 do
    begin
      if AIndex > AIndex2 - AIndex1 then Break;
      if AIndex < Length(AIndexes) then
      begin
        if InRange(AIndexes[AIndex], AIndex1, AIndex2) then
          SortIndexes.Add(AIndexes[AIndex])
        else
          Continue;
      end
      else
        if Length(AIndexes) = 0 then
          SortIndexes.Add(AIndex + AIndex1)
        else
          Break;
      SortOrders.Add(ASortOrders[AIndex]);
    end;
    if Length(AIndexes) = 0 then
      for AIndex := AIndex1 + Length(ASortOrders) to AIndex2 do
      begin
        SortOrders.Add(soAscending);
        SortIndexes.Add(AIndex);
      end;
  end;
  Result := SortOrders.Count > 0;
end;

function TdxSpreadSheetTableViewSortingHelper.IsColumnEmpty(AIndex: Integer): Boolean;
var
  ARow: Integer;
  ACell: TdxSpreadSheetCell;
begin
  Result := True;
  if AIndex > View.Dimensions.Right then
    Exit;
  ARow := Area.Top;
  while Result and (ARow <= Area.Bottom) do
  begin
    ACell := View.Cells[ARow, AIndex];
    Result := (ACell = nil) or ACell.IsEmpty or VarIsNull(ACell.AsVariant);
    Inc(ARow);
  end;
end;

function TdxSpreadSheetTableViewSortingHelper.IsRowEmpty(AIndex: Integer): Boolean;
var
  AColumn: Integer;
  ACell: TdxSpreadSheetCell;
begin
  Result := True;
  if (View.Rows[AIndex] = nil) or (View.Rows[AIndex].CellCount = 0) then
    Exit;
  AColumn := Area.Left;
  while Result and (AColumn <= Area.Right) do
  begin
    ACell := View.Cells[AIndex, AColumn];
    Result := (ACell = nil) or ACell.IsEmpty or VarIsNull(ACell.AsVariant);
    Inc(AColumn);
  end;
end;

procedure TdxSpreadSheetTableViewSortingHelper.SortValues(const AValues: TcxObjectList; L, H: Integer);
var
  I, J: Integer;
  Middle: TObject;
begin
  repeat
    I := L;
    J := H;
    Middle := AValues[(L + H) shr 1];
    repeat
      while Compare(AValues[I], Middle) < 0 do Inc(I);
      while Compare(AValues[J], Middle) > 0 do Dec(J);
      if I <= J then
      begin
        if I <> J then
        begin
          AValues.Exchange(I, J);
          SwapValues(TdxSpreadSheetCellData(AValues[I]), TdxSpreadSheetCellData(AValues[J]));
        end;
        Inc(I);
        Dec(J);
      end;
    until I > J;
    if L < J then
      SortValues(AValues, L, J);
    L := I;
  until I >= H;
end;

function TdxSpreadSheetTableViewSortingHelper.ValidateArea(const AArea: TRect): Boolean;
begin
  Area := cxRectAdjust(AArea);
  Area.Bottom := Min(Area.Bottom, View.Dimensions.Bottom);
  Area.Right := Min(Area.Right, View.Dimensions.Right);
  Result := (Area.Top <= Area.Bottom) and (Area.Left <= Area.Right)
end;

{ TdxSpreadSheetTableViewClipboardHelper }

constructor TdxSpreadSheetTableViewClipboardHelper.Create(AView: TdxSpreadSheetTableView);
begin
  inherited Create(AView);
  FArea := Selection.Area;
  FArea.Right := Min(FArea.Right, View.Dimensions.Right);
  FArea.Bottom := Min(FArea.Bottom, View.Dimensions.Bottom);
  FMergedCells := TList<TdxSpreadSheetMergedCell>.Create;
  FText := TStringList.Create;
  FStyles := TList.Create;

  FContainersData := TMemoryStream.Create;
  FContainersReader := TcxReader.Create(ContainersData, dxSpreadSheetBinaryFormatVersion);
  FContainersWriter := TcxWriter.Create(ContainersData, dxSpreadSheetBinaryFormatVersion);

  FCellsData := TMemoryStream.Create;
  FCellsReader := TcxReader.Create(FCellsData, dxSpreadSheetBinaryFormatVersion);
  FCellsWriter := TcxWriter.Create(FCellsData, dxSpreadSheetBinaryFormatVersion);

  FStylesData := TMemoryStream.Create;
  FStylesReader := TcxReader.Create(FStylesData, dxSpreadSheetBinaryFormatVersion);
  FStylesWriter := TcxWriter.Create(FStylesData, dxSpreadSheetBinaryFormatVersion);

  dxGetLocaleFormatSettings(dxGetInvariantLocaleID, FInvariantFormatSettings);
end;

destructor TdxSpreadSheetTableViewClipboardHelper.Destroy;
begin
  FreeAndNil(FContainersData);
  FreeAndNil(FContainersReader);
  FreeAndNil(FContainersWriter);
  FreeAndNil(FCellsData);
  FreeAndNil(FCellsReader);
  FreeAndNil(FCellsWriter);
  FreeAndNil(FStyles);
  FreeAndNil(FStylesData);
  FreeAndNil(FStylesReader);
  FreeAndNil(FStylesWriter);
  FreeAndNil(FText);
  FreeAndNil(FMergedCells);
  inherited Destroy;
end;

class function TdxSpreadSheetTableViewClipboardHelper.CanPaste: Boolean;
begin
  Result := Clipboard.HasFormat(CFSpreadsheet) or Clipboard.HasFormat(CF_UNICODETEXT) or Clipboard.HasFormat(CF_TEXT);
end;

procedure TdxSpreadSheetTableViewClipboardHelper.Copy;
begin
  CheckSelection;
  if SelectedContainer <> nil then
    DoCopyContainers
  else
    DoCopyCells;

  DoCopyDataToClipboard;
end;

procedure TdxSpreadSheetTableViewClipboardHelper.Cut;
begin
  Copy;
  if SelectedContainer <> nil then
    SelectedContainer.Free
  else
    View.ClearCells(Selection.Area);
end;

procedure TdxSpreadSheetTableViewClipboardHelper.Paste;
begin
  CheckSelection;
  Clipboard.Open;
  try
    GetDataFromClipboard;
    if (CellsData.Size > 0) or (ContainersData.Size > 0) then
      PasteAsFormattedValues
    else
      PasteAsText;
  finally
    Clipboard.Close;
  end;
end;

procedure TdxSpreadSheetTableViewClipboardHelper.CheckSelection;
begin
  if (SelectedContainer = nil) and (Selection.Count <> 1) then
    raise EdxSpreadSheetError.Create(cxGetResourceString(@sdxErrorInvalidSelection));
end;

procedure TdxSpreadSheetTableViewClipboardHelper.AddMergedCells(const Area: TRect);
var
  ACell: TdxSpreadSheetMergedCellAccess;
begin
  ACell := TdxSpreadSheetMergedCellAccess.Create;
  ACell.Initialize(nil, Area);
  MergedCells.Add(ACell);
end;

procedure TdxSpreadSheetTableViewClipboardHelper.DoCopyCells;
var
  ACell: TdxSpreadSheetCell;
  ADisplayText: string;
  AStyleIndex, ARow, AColumn: Integer;
  S: string;
begin
  View.MergedCells.Extract(Area, MergedCells);
  for ARow := Area.Top to Area.Bottom do
  begin
    S := '';
    for AColumn := Area.Left to Area.Right do
    begin
      ACell := View.Cells[ARow, AColumn];
      CellsWriter.WriteInteger(ARow - Area.Top);
      CellsWriter.WriteInteger(AColumn - Area.Left);
      CellsWriter.WriteBoolean(ACell <> nil);
      if ACell <> nil then
      begin
        ADisplayText := ACell.DisplayText;
        if PosEx(dxCRLF, ADisplayText) > 0 then
          S := S + '"' + ADisplayText + '"'
        else
          S := S + ADisplayText;

        AStyleIndex := Styles.IndexOf(ACell.Style.Handle);
        if AStyleIndex < 0 then
        begin
          AStyleIndex := Styles.Add(ACell.Style.Handle);
          ACell.Style.Handle.SaveToStream(StylesWriter);
        end;
        TdxSpreadSheetCellAccess(ACell).SaveToStream(CellsWriter, AStyleIndex);
      end;
      if AColumn < Area.Right then
        S := S + DXTAB
    end;
    Text.Add(S);
  end;
end;

procedure TdxSpreadSheetTableViewClipboardHelper.DoCopyContainers;
begin
  ContainersWriter.WriteAnsiString(dxStringToAnsiString(SelectedContainer.ClassName));
  TdxSpreadSheetContainerAccess(SelectedContainer).SaveToStream(ContainersWriter);
  ContainersWriter.WriteRect(TdxSpreadSheetContainerAccess(SelectedContainer).Calculator.CalculateBounds);
end;

procedure TdxSpreadSheetTableViewClipboardHelper.DoCopyDataToClipboard;
begin
  Clipboard.Clear;
  Clipboard.Open;
  try
    if Text.Count > 0 then
      Clipboard.AsText := Text.Text;
    SetDataToClipboard;
  finally
    Clipboard.Close;
  end;
end;

procedure TdxSpreadSheetTableViewClipboardHelper.DoMergeBordersForInsertedCells;
const
  ColumnOffset: array[TcxBorder] of Integer = (-1, 0, 1, 0);
  RowOffset: array[TcxBorder] of Integer = (0, -1, 0, 1);
  SideMap: array[TcxBorder] of TcxBorder = (bRight, bBottom, bLeft, bTop);
begin
  TdxSpreadSheetTableRowsAccess(Rows).ForEachCell(Area,
    procedure (AItem: TdxDynamicListItem; AData: Pointer)
    var
      ACell: TdxSpreadSheetCell;
      ANeighborCell: TdxSpreadSheetCell;
      ASide: TcxBorder;
    begin
      ACell := TdxSpreadSheetCell(AItem);
      for ASide := Low(ASide) to High(ASide) do
        if ACell.Style.Borders[ASide].Style = sscbsDefault then
        begin
          ANeighborCell := View.Cells[ACell.RowIndex + RowOffset[ASide], ACell.ColumnIndex + ColumnOffset[ASide]];
          if ANeighborCell <> nil then
            ACell.Style.Borders[ASide].Assign(ANeighborCell.Style.Borders[SideMap[ASide]]);
        end;
    end);
end;

procedure TdxSpreadSheetTableViewClipboardHelper.DoPasteCells;
var
  ACell: TdxSpreadSheetCell;
  AColumn: Integer;
  AFormulas: TdxSpreadSheetFormulaAsTextInfoList;
  ARow: Integer;
begin
  AFormulas := TdxSpreadSheetFormulaAsTextInfoList.Create(SpreadSheet);
  try
    OffsetRect(FArea, Selection.FocusedColumn, Selection.FocusedRow);
    for ARow := Area.Top to Area.Bottom do
      for AColumn := Area.Left to Area.Right do
      begin
        CellsReader.ReadInteger;
        CellsReader.ReadInteger;
        ACell := View.Cells[ARow, AColumn];
        if CellsReader.ReadBoolean then
        begin
          if ACell = nil then
            ACell := View.CreateCell(ARow, AColumn);
          TdxSpreadSheetCellAccess(ACell).LoadFromStream(CellsReader, FStyles[CellsReader.ReadInteger], AFormulas);
        end
        else
          if ACell <> nil then
          begin
            ACell.Clear;
            ACell.Style.Handle := SpreadSheet.DefaultCellStyle.Handle;
          end;
      end;

    AFormulas.ResolveReferences;
    DoMergeBordersForInsertedCells;
  finally
    AFormulas.Free;
  end;
end;

procedure TdxSpreadSheetTableViewClipboardHelper.DoPasteContainers;

  function CalculateTargetOrigin: TPoint;
  const
    ContainerOffset = 10;
  begin
    if SelectedContainer <> nil then
    begin
      Result := TdxSpreadSheetContainerAccess(SelectedContainer).Calculator.CalculateBounds.TopLeft;
      Result := cxPointOffset(Result, ContainerOffset, ContainerOffset);
    end
    else
      if (View.Selection.FocusedColumn > -1) and (View.Selection.FocusedRow > -1) then
        Result := View.CreateCell(View.Selection.FocusedRow, View.Selection.FocusedColumn).GetAbsoluteBounds.TopLeft
      else
        Result := cxNullPoint;
  end;

var
  AClass: TClass;
  AContainer: TdxSpreadSheetContainerAccess;
begin
  AClass := GetClass(dxAnsiStringToString(ContainersReader.ReadAnsiString));
  if (AClass <> nil) and AClass.InheritsFrom(TdxSpreadSheetContainer) then
  begin
    AContainer := TdxSpreadSheetContainerAccess(View.Containers.Add(TdxSpreadSheetContainerClass(AClass)));
    AContainer.Parent := View;
    AContainer.LoadFromStream(ContainersReader);
    AContainer.Calculator.UpdateAnchors(cxRectSetOrigin(ContainersReader.ReadRect, CalculateTargetOrigin));
    SelectedContainer := AContainer;
  end;
end;

procedure TdxSpreadSheetTableViewClipboardHelper.GetDataFromClipboard;
var
  AData: THandle;
  ADataPtr: PByte;
  I: Integer;
begin
  if Clipboard.HasFormat(CFSpreadsheet) then
  begin
    AData := GetClipboardData(CFSpreadsheet);
    if AData <> 0 then
    begin
      ADataPtr := GlobalLock(AData);
      try
        if (ADataPtr <> nil) and (Cardinal(ReadInteger(ADataPtr)) = GlobalSize(AData)) then
        begin
          FArea := ReadRect(ADataPtr);
          for I := 0 to ReadInteger(ADataPtr) - 1 do
            AddMergedCells(ReadRect(ADataPtr));

          StylesData.Size := ReadInteger(ADataPtr);
          ReadData(ADataPtr, StylesData.Memory^, StylesData.Size);

          CellsData.Size := ReadInteger(ADataPtr);
          ReadData(ADataPtr, CellsData.Memory^, CellsData.Size);

          ContainersData.Size := ReadInteger(ADataPtr);
          ReadData(ADataPtr, ContainersData.Memory^, ContainersData.Size);
        end;
      finally
        GlobalUnlock(AData);
      end;
    end;
  end;

  if Clipboard.HasFormat(CF_UNICODETEXT) then
    DecodeText(Clipboard.AsText)
  else
    if Clipboard.HasFormat(CF_TEXT) then
    begin
      AData := GetClipboardData(CF_TEXT);
      if AData <> 0 then
      try
        DecodeText(dxAnsiStringToString(PAnsiChar(GlobalLock(AData))));
      finally
        GlobalUnlock(AData);
      end;
    end
    else
      Text.Clear;
end;

procedure TdxSpreadSheetTableViewClipboardHelper.PasteAsFormattedValues;
var
  I: Integer;
begin
  try
    while StylesData.Position <> StylesData.Size do
      FStyles.Add(SpreadSheet.CellStyles.CreateStyleFromStream(FStylesReader));

    if CellsData.Size > 0 then
      DoPasteCells;
    if ContainersData.Size > 0 then
      DoPasteContainers;

    for I := 0 to MergedCells.Count - 1 do
      View.MergedCells.Add(cxRectOffset(MergedCells[I].Area, Area.TopLeft));
    View.Selection.Add(Area, [], Area.Top, Area.Left);
  finally
    for I := 0 to MergedCells.Count - 1 do
      MergedCells[I].Free;
  end;
end;

procedure TdxSpreadSheetTableViewClipboardHelper.PasteAsText;
var
  AColumnIndex: Integer;
  ADelimPos: Integer;
  ALine: string;
  APrevDelimPos: Integer;
  ARow: TdxSpreadSheetTableRow;
  AValue: string;
  I: Integer;
begin
  APrevDelimPos := 0;
  FArea := cxRectBounds(Max(0, Selection.FocusedColumn), Max(0, Selection.FocusedRow), 0, Text.Count - 1);
  for I := 0 to Text.Count - 1 do
  begin
    ARow := View.Rows.CreateItem(Area.Top + I);

    AColumnIndex := Area.Left;
    ALine := Text[I];
    repeat
      ADelimPos := PosEx(dxTAB, ALine, APrevDelimPos + 1);
      if ADelimPos = 0 then
        AValue := System.Copy(ALine, APrevDelimPos + 1, MaxInt)
      else
        AValue := System.Copy(ALine, APrevDelimPos + 1, ADelimPos - APrevDelimPos - 1);

      ARow.CreateCell(AColumnIndex).AsVariant := DecodeValue(AValue);
      APrevDelimPos := ADelimPos;
      Inc(AColumnIndex);
    until ADelimPos = 0;
    FArea.Right := Max(FArea.Right, AColumnIndex - 1);
  end;
  View.Selection.Add(Area, [], Area.Top, Area.Left);
end;

procedure TdxSpreadSheetTableViewClipboardHelper.ReadData(var ABuffer: PByte; var AData; ASize: Integer);
begin
  Move(ABuffer^, AData, ASize);
  Inc(ABuffer, ASize);
end;

function TdxSpreadSheetTableViewClipboardHelper.ReadInteger(var ABuffer: PByte): Integer;
begin
  ReadData(ABuffer, Result, SizeOf(Result));
end;

function TdxSpreadSheetTableViewClipboardHelper.ReadRect(var ABuffer: PByte): TRect;
begin
  ReadData(ABuffer, Result, SizeOf(Result));
end;

procedure TdxSpreadSheetTableViewClipboardHelper.SetDataToClipboard;
var
  AData: THandle;
  ADataPtr: PByte;
  I, ASize: Integer;
  P: Pointer;
begin
  ASize := StylesData.Size + CellsData.Size + ContainersData.Size + MergedCells.Count * SizeOf(TRect);
  if ASize = 0 then
    Exit;

  ASize := ASize + SizeOf(TRect) + SizeOf(Integer) * 4 + SizeOf(Integer);
  AData := GlobalAlloc(GMEM_MOVEABLE or GMEM_DDESHARE, ASize);
  try
    ADataPtr := GlobalLock(AData);
    P := ADataPtr;
    try
      FillChar(P^, ASize, 0);
      WriteInteger(ADataPtr, ASize);

      WriteRect(ADataPtr, cxRectOffset(Area, Area.TopLeft, False));

      WriteInteger(ADataPtr, MergedCells.Count);
      for I := 0 to MergedCells.Count - 1 do
        WriteRect(ADataPtr, cxRectOffset(MergedCells[I].Area, Area.TopLeft, False));

      WriteInteger(ADataPtr, StylesData.Size);
      WriteData(ADataPtr, StylesData.Memory^, StylesData.Size);

      WriteInteger(ADataPtr, CellsData.Size);
      WriteData(ADataPtr, CellsData.Memory^, CellsData.Size);

      WriteInteger(ADataPtr, ContainersData.Size);
      WriteData(ADataPtr, ContainersData.Memory^, ContainersData.Size);

      SetClipboardData(CFSpreadsheet, AData);
    finally
      GlobalUnlock(AData);
    end;
  except
    GlobalFree(AData);
    raise;
  end;
end;

procedure TdxSpreadSheetTableViewClipboardHelper.WriteData(var ABuffer: PByte; const AData; ASize: Integer);
begin
  Move(AData, ABuffer^, ASize);
  Inc(ABuffer, ASize);
end;

procedure TdxSpreadSheetTableViewClipboardHelper.WriteInteger(var ABuffer: PByte; const AValue: Integer);
begin
  WriteData(ABuffer, AValue, SizeOf(AValue));
end;

procedure TdxSpreadSheetTableViewClipboardHelper.WriteRect(var ABuffer: PByte; const R: TRect);
begin
  WriteData(ABuffer, R, SizeOf(R));
end;

procedure TdxSpreadSheetTableViewClipboardHelper.DecodeText(const S: string);
var
  ALine: string;
  AScan: PChar;
  AStart: PChar;
begin
  AScan := PChar(S);
  while AScan^ <> #0 do
  begin
    AStart := AScan;
    while not CharInSet(AScan^, [#0, '"', #10, #13])  do
      Inc(AScan);

    if AScan^ = '"' then
    begin
      Inc(AScan);
      while not CharInSet(AScan^, [#0, '"']) do
        Inc(AScan);
      if AScan^ = '"' then
        Inc(AScan);
    end;

    SetString(ALine, AStart, AScan - AStart);
    Text.Add(StringReplace(ALine, '"', '', [rfReplaceAll]));

    if CharInSet(AScan^, [#13, #10]) then
      Inc(AScan);
  end;
end;

function TdxSpreadSheetTableViewClipboardHelper.DecodeValue(const S: string): Variant;
var
  AValueDouble: Double;
begin
  if TryStrToFloat(S, AValueDouble) or TryStrToFloat(S, AValueDouble, FInvariantFormatSettings) then
    Result := AValueDouble
  else
    Result := S;
end;

function TdxSpreadSheetTableViewClipboardHelper.GetSelectedContainer: TdxSpreadSheetContainer;
begin
  Result := TdxSpreadSheetTableViewAccess(View).Controller.FocusedContainer;
end;

function TdxSpreadSheetTableViewClipboardHelper.GetSelection: TdxSpreadSheetTableViewSelection;
begin
  Result := View.Selection;
end;

procedure TdxSpreadSheetTableViewClipboardHelper.SetSelectedContainer(const Value: TdxSpreadSheetContainer);
begin
  TdxSpreadSheetTableViewAccess(View).Controller.FocusedContainer := Value;
end;

{ TdxSpreadSheetTableViewEnumCellStylesHelper }

constructor TdxSpreadSheetTableViewEnumCellStylesHelper.Create(ASheet: TdxSpreadSheetTableView);
begin
  inherited Create;
  FSheet := ASheet;
end;

procedure TdxSpreadSheetTableViewEnumCellStylesHelper.PrepareToSave(AArea: TRect);
var
  AColumnIndex: Integer;
  AIsEntireColumn: Boolean;
  AIsEntireRow: Boolean;
  ARow: TdxSpreadSheetTableRow;
  ARowIndex: Integer;
begin
  AArea := dxSpreadSheetGetRealArea(AArea);
  AIsEntireColumn := dxSpreadSheetIsEntireColumn(AArea);
  AIsEntireRow := dxSpreadSheetIsEntireRow(AArea);

  if AIsEntireRow and not AIsEntireColumn then
  begin
    for ARowIndex := AArea.Top to AArea.Bottom do
      Sheet.Rows.CreateItem(ARowIndex)
  end;

  if AIsEntireColumn then
  begin
    for AColumnIndex := AArea.Left to AArea.Right do
      Sheet.Columns.CreateItem(AColumnIndex);
  end;

  if not (AIsEntireColumn or AIsEntireRow) then
    for ARowIndex := AArea.Top to AArea.Bottom do
    begin
      ARow := Sheet.Rows.CreateItem(ARowIndex);
      for AColumnIndex := AArea.Left to AArea.Right do
        ARow.CreateCell(AColumnIndex);
    end;
end;

procedure TdxSpreadSheetTableViewEnumCellStylesHelper.ProcessArea(
  const AArea: TRect; AProcRef: TdxSpreadSheetTableViewEnumCellStylesProcRef);
var
  ARect: TRect;
begin
  ARect := dxSpreadSheetGetRealArea(AArea);

  if dxSpreadSheetIsEntireColumn(ARect) and dxSpreadSheetIsEntireRow(ARect) then
  begin
    Inc(TdxSpreadSheetTableViewAccess(Sheet).FMergeCellStylesLockCount);
    try
      EnumColumnStyles(ARect, AProcRef);
      EnumRowStyles(ARect, AProcRef);
    finally
      Dec(TdxSpreadSheetTableViewAccess(Sheet).FMergeCellStylesLockCount);
    end;
  end
  else
  begin
    if dxSpreadSheetIsEntireColumn(ARect) then
      EnumColumnStyles(ARect, AProcRef);
    if dxSpreadSheetIsEntireRow(ARect) then
      EnumRowStyles(ARect, AProcRef);
  end;

  TdxSpreadSheetTableRowsAccess(Sheet.Rows).ForEachCell(ARect,
    procedure (AItem: TdxDynamicListItem; AData: Pointer)
    var
      ACell: TdxSpreadSheetCell;
    begin
      ACell := TdxSpreadSheetCell(AItem);
      AProcRef(ACell.Style, ACell.RowIndex, ACell.ColumnIndex, ARect);
    end);

  if EnumDefaultStyles then
    EnumDefaultCellsStyles(ARect, AProcRef);
end;

procedure TdxSpreadSheetTableViewEnumCellStylesHelper.EnumColumnStyles(
  const AArea: TRect; AProcRef: TdxSpreadSheetTableViewEnumCellStylesProcRef);
begin
  TdxSpreadSheetTableColumnsAccess(Sheet.Columns).ForEach(
    procedure (AItem: TdxDynamicListItem; AData: Pointer)
    var
      AColumn: TdxSpreadSheetTableColumn;
    begin
      AColumn := TdxSpreadSheetTableColumn(AItem);
      AProcRef(AColumn.Style, -1, AColumn.Index, PRect(AData)^);
    end,
    AArea.Left, AArea.Right, True, @AArea);
end;

procedure TdxSpreadSheetTableViewEnumCellStylesHelper.EnumDefaultCellsStyles(
  const R: TRect; AProcRef: TdxSpreadSheetTableViewEnumCellStylesProcRef);
var
  ACellsCount: Integer;
  AColumn: TdxSpreadSheetTableColumnAccess;
  ARow: TdxSpreadSheetTableRowAccess;
  ARowCount: Integer;
  AStyle: TdxSpreadSheetFakeCellStyle;
begin
  AStyle := TdxSpreadSheetFakeCellStyle.Create(Sheet);
  try
    ARowCount := 0;
    ARow := TdxSpreadSheetTableRowAccess(Sheet.Rows.First);
    while (ARow <> nil) and (ARow.Index < R.Top) do
      ARow := TdxSpreadSheetTableRowAccess(ARow.Next);
    while (ARow <> nil) and (ARow.Index <= R.Bottom) do
    begin
      ACellsCount := 0;
      AColumn := TdxSpreadSheetTableColumnAccess(Sheet.Columns.First);
      while (AColumn <> nil) and (AColumn.Index < R.Left) do
        AColumn := TdxSpreadSheetTableColumnAccess(AColumn.Next);
      while (AColumn <> nil) and (AColumn.Index <= R.Right) do
      begin
        if not (ARow.Style.IsDefault and AColumn.Style.IsDefault) and (ARow.Cells[AColumn.Index] = nil) then
        begin
          AStyle.Calculate(ARow, AColumn);
          AProcRef(AStyle, ARow.Index, AColumn.Index, R);
        end;
        AColumn := TdxSpreadSheetTableColumnAccess(AColumn.Next);
        Inc(ACellsCount);
      end;
      if ACellsCount <> R.Width + 1 then
        AProcRef(ARow.Style, ARow.Index, -1, R);
      ARow := TdxSpreadSheetTableRowAccess(ARow.Next);
      Inc(ARowCount);
    end;

    FHasEntireRowWithDefaultStyle := FHasEntireRowWithDefaultStyle or (ARowCount <> R.Height + 1);
    if FHasEntireRowWithDefaultStyle then
    begin
      ACellsCount := 0;
      AColumn := TdxSpreadSheetTableColumnAccess(Sheet.Columns.First);
      while (AColumn <> nil) and (AColumn.Index < R.Left) do
        AColumn := TdxSpreadSheetTableColumnAccess(AColumn.Next);
      while (AColumn <> nil) and (AColumn.Index <= R.Right) do
      begin
        if not AColumn.Style.IsDefault then
        begin
          AProcRef(AColumn.Style, -1, AColumn.Index, R);
          Inc(ACellsCount);
        end;
        AColumn := TdxSpreadSheetTableColumnAccess(AColumn.Next);
      end;
      if ACellsCount <> R.Width + 1 then
        AProcRef(Sheet.SpreadSheet.DefaultCellStyle, -1, -1, R);
    end;
  finally
    AStyle.Free;
  end;
end;

procedure TdxSpreadSheetTableViewEnumCellStylesHelper.EnumRowStyles(
  const AArea: TRect; AProcRef: TdxSpreadSheetTableViewEnumCellStylesProcRef);
var
  ACount: Integer;
begin
  ACount := 0;
  TdxSpreadSheetTableRowsAccess(Sheet.Rows).ForEach(
    procedure (AItem: TdxDynamicListItem; AData: Pointer)
    var
      ARow: TdxSpreadSheetTableRow;
    begin
      ARow := TdxSpreadSheetTableRow(AItem);
      AProcRef(ARow.Style, ARow.Index, -1, PRect(AData)^);
      FHasEntireRowWithDefaultStyle := FHasEntireRowWithDefaultStyle or ARow.Style.IsDefault;
      Inc(ACount);
    end,
    AArea.Top, AArea.Bottom, True, @AArea);

  FHasEntireRowWithDefaultStyle := FHasEntireRowWithDefaultStyle or (ACount <> AArea.Height + 1);
end;

{ TdxSpreadSheetTableViewPackHelper }

procedure TdxSpreadSheetTableViewPackHelper.Pack;
begin
  RemoveUnusedCells;
  RemoveUnusedItems(Rows);
  RemoveUnusedItems(Columns);
  TdxSpreadSheetTableViewAccess(View).AddChanges([sscLayout, sscData]);
end;

procedure TdxSpreadSheetTableViewPackHelper.RemoveUnusedCells;
begin
  TdxSpreadSheetTableRowsAccess(Rows).ForEachCell(cxRect(0, 0, MaxInt, MaxInt),
    procedure (AItem: TdxDynamicListItem; AData: Pointer)
    var
      ACell: TdxSpreadSheetCell;
    begin
      ACell := TdxSpreadSheetCell(AItem);
      if ACell.IsEmpty and not ACell.IsMerged then
      begin
        if (ACell.Style.Handle = ACell.Row.Style.Handle) and
           (not ACell.Row.Style.IsDefault or ACell.Column.Style.IsDefault) or
           (ACell.Style.Handle = ACell.Column.Style.Handle) and ACell.Row.Style.IsDefault
        then
          ACell.Free;
      end;
    end);
end;

procedure TdxSpreadSheetTableViewPackHelper.RemoveUnusedItems(AItems: TdxSpreadSheetTableItems);
begin
  TdxSpreadSheetTableItemsAccess(AItems).ForEach(
    procedure (AItem: TdxDynamicListItem; AData: Pointer)
    var
      ATableItem: TdxSpreadSheetTableItem;
      I: Integer;
    begin
      ATableItem := TdxSpreadSheetTableItem(AItem);
      if ATableItem.DefaultSize and ATableItem.Visible and ATableItem.Style.IsDefault then
      begin
        for I := 0 to ATableItem.CellCount - 1 do
        begin
          if ATableItem.Cells[I] <> nil then
            Exit;
        end;
        ATableItem.Free;
      end;
    end);
end;

{ TdxSpreadSheetEnumValuesProcessingInfo }

procedure TdxSpreadSheetEnumValuesProcessingInfo.Init(const ANumericListSortedInAscendingOrder,
  APopulateNumericListWhenAccumulateResult, AErrorIfNonReferenceToken, AIgnoreHiddenRows: Boolean);
begin
  NumericListSortedInAscendingOrder := ANumericListSortedInAscendingOrder;
  PopulateNumericListWhenAccumulateResult := APopulateNumericListWhenAccumulateResult;
  ErrorIfNonReferenceToken := AErrorIfNonReferenceToken;
  IgnoreHiddenRows := AIgnoreHiddenRows;
end;

{ TdxSpreadSheetEnumValues }

constructor TdxSpreadSheetEnumValues.Create(const AFormatSettings: TdxSpreadSheetFormatSettings;
  const AProcessingInfo: TdxSpreadSheetEnumValuesProcessingInfo);
begin
  Create(AFormatSettings, 0, AProcessingInfo);
end;

constructor TdxSpreadSheetEnumValues.Create(const AFormatSettings: TdxSpreadSheetFormatSettings;
  const AInitialResultValue: Variant; const AProcessingInfo: TdxSpreadSheetEnumValuesProcessingInfo);
begin
  inherited Create;
  FFormatSettings := AFormatSettings;
  FProcessingInfo := AProcessingInfo;
  if FProcessingInfo.NumericListSortedInAscendingOrder then
    FNumericList := TList<Double>.Create
  else
    FNumericList := TList<Double>.Create(TdxDescendingComparerDoubleF.Create);
  Initialize(AInitialResultValue);
end;

destructor TdxSpreadSheetEnumValues.Destroy;
begin
  FreeAndNil(FNumericList);
  inherited Destroy;
end;

function TdxSpreadSheetEnumValues.AddToNumericList(const AValue: Variant; const AValueIsChecked: Boolean): Boolean;
begin
  Result := AValueIsChecked or dxIsNumberOrDateTime(AValue);
  if Result then
  begin
    NumericList.Add(Double(AValue));
    FNumericCount := NumericList.Count;
  end;
end;

function TdxSpreadSheetEnumValues.ConvertNullToDefValue(const ADefValue: Variant; var AOutputValue: Variant;
  ACanConvertStrToNumeric: Boolean): Boolean;
begin
  Result := ACanConvertStrToNumeric;
  if Result then
  begin
    Inc(FNumericCount);
    AOutputValue := ADefValue;
  end
  else
    Inc(FNullCount);
end;

function TdxSpreadSheetEnumValues.CheckValueOnLogical(const AValue: Variant; ACanConvertStrToNumber: Boolean;
  var AOutputValue: Variant): Boolean;
var
  ABoolean: Boolean;
  ANumeric: Variant;
begin
  Result := False;
  AOutputValue := Null;
  Inc(FCount);
  if VarIsEmpty(AValue) then
  begin
    Inc(FEmptyCount);
    Result := True;
  end
  else
    if VarIsNull(AValue) then
      Result := ConvertNullToDefValue(False, AOutputValue, ACanConvertStrToNumber)
    else
      if dxIsText(AValue) and not ACanConvertStrToNumber then
      begin
        Inc(FStringCount);
        Result := True;
      end
      else
        if dxIsNumericOrDateTime(AValue) then
        begin
          Inc(FNumericCount);
          Result := True;
          ANumeric := dxConvertToNumberOrDateTime(AValue);
          AOutputValue := ANumeric <> 0;
        end
        else
          if dxIsText(AValue) then
          begin
            Result := (AValue <> '') and ACanConvertStrToNumber and dxTryStrToBool(AValue, ABoolean);
            if Result then
            begin
              Inc(FNumericCount);
              AOutputValue := ABoolean;
            end
            else
              Inc(FStringCount);
          end;
  if Result and not VarIsNull(AOutputValue) and AOutputValue then
    Inc(FTrueCount);
end;

function TdxSpreadSheetEnumValues.CheckValueOnNumeric(const AValue: Variant; ACanConvertStrToNumber: Boolean;
  AWithoutSimularNumericByReference: Boolean; var AOutputValue: Variant): Boolean;

  procedure CheckIsString(const S: Variant);
  begin
    if VarIsStr(S) then
      Inc(FStringCount);
  end;

var
  ANumeric: Variant;
  ADate: TDateTime;
begin
  Result := False;
  AOutputValue := AValue;
  Inc(FCount);
  if VarIsEmpty(AValue) then
    Inc(FEmptyCount)
  else
    if VarIsNull(AValue) then
      Result := ConvertNullToDefValue(0, AOutputValue, ACanConvertStrToNumber)
    else
      if not ACanConvertStrToNumber and AWithoutSimularNumericByReference then
      begin
        if dxIsNumberOrDateTime(AValue) then
        begin
          Inc(FNumericCount);
          if dxIsDateTime(AValue) then
            AOutputValue := VarToDateTime(AValue);
          Result := True;
        end
        else
          CheckIsString(AValue);
      end
      else
        if dxIsNumericOrDateTime(AValue) then
        begin
          Inc(FNumericCount);
          AOutputValue := dxConvertToNumberOrDateTime(AValue);
          Result := True;
        end
        else
        begin
          ANumeric := Null;
          if dxIsText(AValue) and (AValue <> '') and ACanConvertStrToNumber and
            (dxTryStrToOrdinal(AValue, ANumeric, FormatSettings) or dxConvertToXLSDate(AValue, ADate)) then
          begin
            Inc(FNumericCount);
            if ANumeric <> Null then
              AOutputValue := ANumeric
            else
              AOutputValue := ADate;
            Result := True;
          end
          else
            CheckIsString(AValue);
        end;
end;

function TdxSpreadSheetEnumValues.DeleteFromNumericList(const AIndex: Integer): Boolean;
begin
  Result := (AIndex >= 0) and (AIndex <= NumericList.Count - 1);
  if Result then
  begin
    NumericList.Delete(AIndex);
    FNumericCount := NumericList.Count;
  end;
end;

procedure TdxSpreadSheetEnumValues.Initialize(const AInitialResultValue: Variant);
begin
  FIndex := 0;
  FCount := 0;
  FNullCount := 0;
  FNumericCount := 0;
  FStringCount := 0;
  FEmptyCount := 0;
  FErrorCode := ecNone;
  FTrueCount := 0;
  FResultValue := AInitialResultValue;
end;

function TdxSpreadSheetEnumValues.InsertIntoNumericList(const AIndex: Integer; const ANumber: Double): Boolean;
begin
  Result := (AIndex >= 0) and (AIndex < NumericList.Count - 1);
  if Result then
  begin
    NumericList.Insert(AIndex, ANumber);
    FNumericCount := NumericList.Count;
  end;
end;

function TdxSpreadSheetEnumValues.PopulateNumericList(Sender: TdxSpreadSheetFormulaResult;
  const AParams: TdxSpreadSheetFormulaToken; const AProcessAllParams: Boolean = False): TdxSpreadSheetFormulaErrorCode;

  function InternalAddToNumericList(const AValue: Variant; ACanConvertStrToNumber: Boolean;
    var AErrorCode: TdxSpreadSheetFormulaErrorCode; AData: TdxSpreadSheetEnumValues; AInfo: Pointer = nil): Boolean;
  var
    ANumeric: Variant;
  begin
    Result := AErrorCode = ecNone;
    if Result then
      if AData.CheckValueOnNumeric(AValue, ACanConvertStrToNumber, True, ANumeric) then
        AData.AddToNumericList(ANumeric, True)
      else
        if ACanConvertStrToNumber then
          AErrorCode := ecValue;
    AData.SetErrorCode(AErrorCode);
  end;

var
  ACurrentParams: TdxSpreadSheetFormulaToken;
begin
  begin
    ACurrentParams := AParams;
    while (ErrorCode = ecNone) and (ACurrentParams <> nil) do
    begin
      Sender.ForEach(ACurrentParams, @InternalAddToNumericList, Self);
      if not AProcessAllParams then
        Break;
      ACurrentParams := ACurrentParams.Next;
    end
  end;
  Result := ErrorCode;
end;

procedure TdxSpreadSheetEnumValues.SetErrorCode(AErrorCode: TdxSpreadSheetFormulaErrorCode);
begin
  FErrorCode := AErrorCode;
end;

function TdxSpreadSheetEnumValues.Validate: Boolean;
begin
  Result := ErrorCode = ecNone;
end;

{ TdxSpreadSheetTableItemStyleMergeHelper }

constructor TdxSpreadSheetTableItemStyleMergeHelper.Create(
  AItem: TdxSpreadSheetTableItem; APrevStyle, ANewStyle: TdxSpreadSheetCellStyleHandle);
begin
  inherited Create;
  FItem := AItem;
  FNewStyle := ANewStyle;
  FPrevStyle := APrevStyle;
  CheckIsUpdateBestFitNeeded;
end;

procedure TdxSpreadSheetTableItemStyleMergeHelper.ProcessCell(ACell: TdxSpreadSheetCell);
begin
  ACell.Style.BeginUpdate;
  try
    MergeAlignment(ACell.Style);
    MergeBrush(ACell.Style);
    MergeFont(ACell.Style);
    MergeBorders(ACell);

    if PrevStyle.DataFormat <> NewStyle.DataFormat then
      ACell.Style.Handle.DataFormat := NewStyle.DataFormat;
    if PrevStyle.States <> NewStyle.States then
      ACell.Style.Handle.States := NewStyle.States;
  finally
    ACell.Style.EndUpdate;
  end;
end;

procedure TdxSpreadSheetTableItemStyleMergeHelper.CheckIsUpdateBestFitNeeded;
begin
  FIsUpdateBestFitNeeded := (PrevStyle.DataFormat <> NewStyle.DataFormat) or
    (PrevStyle.Font <> NewStyle.Font) or (PrevStyle.States <> NewStyle.States) or
    (PrevStyle.Borders <> NewStyle.Borders);
end;

procedure TdxSpreadSheetTableItemStyleMergeHelper.MergeAlignment(ACellStyle: TdxSpreadSheetCellStyle);
begin
  if PrevStyle.AlignHorz <> NewStyle.AlignHorz then
    ACellStyle.AlignHorz := NewStyle.AlignHorz;
  if PrevStyle.AlignHorzIndent <> NewStyle.AlignHorzIndent then
    ACellStyle.AlignHorzIndent := NewStyle.AlignHorzIndent;
  if PrevStyle.AlignVert <> NewStyle.AlignVert then
    ACellStyle.AlignVert := NewStyle.AlignVert;
end;

procedure TdxSpreadSheetTableItemStyleMergeHelper.MergeBorders(ACell: TdxSpreadSheetCell);
var
  ABorder: TcxBorder;
begin
  for ABorder := Low(ABorder) to High(ABorder) do
  begin
    if PrevStyle.Borders.BorderColor[ABorder] <> NewStyle.Borders.BorderColor[ABorder] then
      ACell.Style.Borders[ABorder].Color := NewStyle.Borders.BorderColor[ABorder];
    if PrevStyle.Borders.BorderStyle[ABorder] <> NewStyle.Borders.BorderStyle[ABorder] then
      ACell.Style.Borders[ABorder].Style := NewStyle.Borders.BorderStyle[ABorder];
  end;
end;

procedure TdxSpreadSheetTableItemStyleMergeHelper.MergeBrush(ACellStyle: TdxSpreadSheetCellStyle);
begin
  if PrevStyle.Brush.BackgroundColor <> NewStyle.Brush.BackgroundColor then
    ACellStyle.Brush.BackgroundColor := NewStyle.Brush.BackgroundColor;
  if PrevStyle.Brush.ForegroundColor <> NewStyle.Brush.ForegroundColor then
    ACellStyle.Brush.ForegroundColor := NewStyle.Brush.ForegroundColor;
  if PrevStyle.Brush.Style <> NewStyle.Brush.Style then
    ACellStyle.Brush.Style := NewStyle.Brush.Style;
end;

procedure TdxSpreadSheetTableItemStyleMergeHelper.MergeFont(ACellStyle: TdxSpreadSheetCellStyle);

  procedure CheckFontStyle(AStyle: TFontStyle);
  begin
    if (AStyle in PrevStyle.Font.Style) <> (AStyle in NewStyle.Font.Style) then
    begin
      if AStyle in NewStyle.Font.Style then
        ACellStyle.Font.Style := ACellStyle.Font.Style + [AStyle]
      else
        ACellStyle.Font.Style := ACellStyle.Font.Style - [AStyle];
    end;
  end;

var
  AStyle: TFontStyle;
begin
  if PrevStyle.Font.Charset <> NewStyle.Font.Charset then
    ACellStyle.Font.Charset := NewStyle.Font.Charset;
  if PrevStyle.Font.Color <> NewStyle.Font.Color then
    ACellStyle.Font.Color := NewStyle.Font.Color;
  if PrevStyle.Font.Size <> NewStyle.Font.Size then
    ACellStyle.Font.Size := NewStyle.Font.Size;
  if PrevStyle.Font.Name <> NewStyle.Font.Name then
    ACellStyle.Font.Name := NewStyle.Font.Name;
  if PrevStyle.Font.Pitch <> NewStyle.Font.Pitch then
    ACellStyle.Font.Pitch := NewStyle.Font.Pitch;
  if PrevStyle.Font.Style <> NewStyle.Font.Style then
  begin
    for AStyle := Low(AStyle) to High(AStyle) do
      CheckFontStyle(AStyle);
  end;
end;

initialization
  CFSpreadsheet := RegisterClipboardFormat('DX SpreadSheet');
end.
