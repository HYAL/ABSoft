{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFormatXLSXWriter;

{$I cxVer.Inc}

interface

uses
  Types, Windows, SysUtils, Classes, Graphics, dxCore, dxCoreClasses, cxClasses, dxCustomTree, dxXMLDoc, dxZIPUtils,
  dxSpreadSheetCore, dxSpreadSheetTypes, dxSpreadSheetClasses, dxSpreadSheetStrs, dxSpreadSheetPackedFileFormatCore,
  dxSpreadSheetUtils, dxSpreadSheetGraphics, Generics.Collections, dxGDIPlusClasses, dxCoreGraphics, cxGeometry;

type

  { TdxSpreadSheetXLSXWriterRels }

  TdxSpreadSheetXLSXWriterRels = class(TdxSpreadSheetXMLDocument)
  strict private
    function GetEmpty: Boolean;
  protected
    function DoAddRelationship(const AType, ATarget: TdxXMLString): TdxXMLNode;
  public
    constructor Create(AOwner: TPersistent); override;
    procedure AddExternalRelationship(const AType, ATarget: TdxXMLString);
    procedure AddRelationship(const AType, ATarget: TdxXMLString);
    function GetRelationshipId(const ATarget: TdxXMLString): TdxXMLString;
    //
    property Empty: Boolean read GetEmpty;
  end;

  { TdxSpreadSheetXLSXWriter }

  TdxSpreadSheetXLSXWriter = class(TdxSpreadSheetCustomPackedWriter)
  strict private
    FBorders: TList;
    FCellStyles: TList;
    FCustomFormats: TList;
    FFills: TList;
    FFonts: TList;
    FFormats: TList;
    FImages: TDictionary<TdxGPImage, AnsiString>;
    FSharedStrings: TList;

    FColumnWidthHelper: TdxSpreadSheetExcelColumnWidthHelper;
    FContentType: TdxSpreadSheetXMLDocument;
  protected
    function CreateProgressHelper: TdxSpreadSheetCustomFilerProgressHelper; override;

    procedure AddColorNode(AParentNode: TdxXMLNode; const AChildNodeName: TdxXMLString; AColor: TColor);
    procedure AddDefaultResources;
    procedure AddFontNode(ANode: TdxXMLNode; AFont: TdxSpreadSheetFontHandle); virtual;
    procedure InitializeContentTypes(ANode: TdxXMLNode);

    procedure RegisterFile(const AFileName, AContentType: AnsiString;
      const ARelationship: AnsiString = ''; ARels: TdxSpreadSheetXLSXWriterRels = nil); virtual;

    property Borders: TList read FBorders;
    property CellStyles: TList read FCellStyles;
    property CustomFormats: TList read FCustomFormats;
    property Fills: TList read FFills;
    property Fonts: TList read FFonts;
    property Formats: TList read FFormats;
    property Images: TDictionary<TdxGPImage, AnsiString> read FImages;
    property SharedStrings: TList read FSharedStrings;
    //
    property ColumnWidthHelper: TdxSpreadSheetExcelColumnWidthHelper read FColumnWidthHelper;
  public
    constructor Create(AOwner: TdxCustomSpreadSheet; AStream: TStream); override;
    destructor Destroy; override;
    procedure WriteData; override;
  end;

  { TdxSpreadSheetXLSXWriterCustomBuilder }

  TdxSpreadSheetXLSXWriterCustomBuilder = class(TdxSpreadSheetCustomPackedWriterBuilder)
  strict private
    FOwnerRels: TdxSpreadSheetXLSXWriterRels;
    function GetOwner: TdxSpreadSheetXLSXWriter;
  protected
    function AddResource(AResource: TObject; AResourceList: TList): Integer;
    procedure RegisterFile(const AFileName, AContentType: AnsiString;
      const ARelationship: AnsiString = ''; ARels: TdxSpreadSheetXLSXWriterRels = nil); inline;
  public
    constructor Create(AOwner: TdxSpreadSheetXLSXWriter; AOwnerRels: TdxSpreadSheetXLSXWriterRels);
    //
    property Owner: TdxSpreadSheetXLSXWriter read GetOwner;
    property OwnerRels: TdxSpreadSheetXLSXWriterRels read FOwnerRels;
  end;

  { TdxSpreadSheetXLSXWriterCustomFileBuilder }

  TdxSpreadSheetXLSXWriterCustomFileBuilder = class(TdxSpreadSheetXLSXWriterCustomBuilder)
  strict private
    FTargetFileName: AnsiString;
    function GetTargetFileNameRels: AnsiString; inline;
  public
    constructor Create(AOwner: TdxSpreadSheetXLSXWriter;
      AOwnerRels: TdxSpreadSheetXLSXWriterRels; const ATargetFileName: AnsiString);
    //
    property TargetFileName: AnsiString read FTargetFileName;
    property TargetFileNameRels: AnsiString read GetTargetFileNameRels;
  end;

  { TdxSpreadSheetXLSXWriterCustomContainerBuilder }

  TdxSpreadSheetXLSXWriterCustomContainerBuilderClass = class of TdxSpreadSheetXLSXWriterCustomContainerBuilder;
  TdxSpreadSheetXLSXWriterCustomContainerBuilder = class(TdxSpreadSheetXLSXWriterCustomBuilder)
  strict private
    FContainer: TdxSpreadSheetContainer;
    FParentNode: TdxXMLNode;

    procedure WriteAnchorMarker(ANode: TdxXMLNode; AAnchor: TdxSpreadSheetContainerAnchorPoint);
    procedure WriteEditAsMode(ANode: TdxXMLNode);
    procedure WritePoint(ANode: TdxXMLNode; const AValue: TPoint; const XName, YName: TdxXMLString);
  protected
    // Anchors
    function WriteAbsoluteAnchors(ANode: TdxXMLNode): TdxXMLNode; virtual;
    function WriteAnchors(ANode: TdxXMLNode): TdxXMLNode; virtual;
    function WriteOneCellAnchors(ANode: TdxXMLNode): TdxXMLNode; virtual;
    function WriteTwoCellAnchors(ANode: TdxXMLNode): TdxXMLNode; virtual;

    procedure WriteContent(ARootNode: TdxXMLNode); virtual;
  public
    constructor Create(AContainer: TdxSpreadSheetContainer;
      AOwner: TdxSpreadSheetXLSXWriter; AOwnerRels: TdxSpreadSheetXLSXWriterRels; AParentNode: TdxXMLNode);
    procedure Execute; override;
    //
    property Container: TdxSpreadSheetContainer read FContainer;
    property ParentNode: TdxXMLNode read FParentNode;
  end;

  { TdxSpreadSheetXLSXWriterCustomShapeContainerBuilder }

  TdxSpreadSheetXLSXWriterCustomShapeContainerBuilder = class(TdxSpreadSheetXLSXWriterCustomContainerBuilder)
  strict private
    function GetContainer: TdxSpreadSheetShapeContainer; inline;
  protected
    function WriteImage(AImage: TdxGPImage): AnsiString;

    // Brushes
    procedure WriteColor(ANode: TdxXMLNode; AAlphaColor: TdxAlphaColor); virtual;
    procedure WriteGradientBrushParameters(ANode: TdxXMLNode; ABrush: TdxGPBrush); virtual;
    procedure WriteSolidBrushParameters(ANode: TdxXMLNode; ABrush: TdxGPBrush); virtual;
    procedure WriteTextureBrushParameters(ANode: TdxXMLNode; ABrush: TdxGPBrush); virtual;

    procedure WriteDescription(ANode: TdxXMLNode); virtual;
    procedure WriteRestrictions(ANode: TdxXMLNode); virtual;
    procedure WriteShapeFillParameters(AParentNode: TdxXMLNode; ABrush: TdxGPBrush); virtual;
    procedure WriteShapeGeometry(ANode: TdxXMLNode); virtual;
    procedure WriteShapeLineParameters(ANode: TdxXMLNode; APen: TdxGPPen); virtual;
    procedure WriteShapeProperties(ANode: TdxXMLNode); virtual;
    procedure WriteTransform(ANode: TdxXMLNode);
  public
    property Container: TdxSpreadSheetShapeContainer read GetContainer;
  end;

  { TdxSpreadSheetXLSXWriterShapeContainerBuilder }

  TdxSpreadSheetXLSXWriterShapeContainerBuilder = class(TdxSpreadSheetXLSXWriterCustomShapeContainerBuilder)
  protected
    procedure WriteContent(ARootNode: TdxXMLNode); override;
    procedure WriteShapeDescription(ANode: TdxXMLNode); virtual;
  end;

  { TdxSpreadSheetXLSXWriterPictureContainerBuilder }

  TdxSpreadSheetXLSXWriterPictureContainerBuilder = class(TdxSpreadSheetXLSXWriterShapeContainerBuilder)
  strict private
    function GetContainer: TdxSpreadSheetPictureContainer; inline;
  protected
    procedure WriteContent(ARootNode: TdxXMLNode); override;
    procedure WritePictureDescription(ANode: TdxXMLNode); virtual;
    procedure WritePictureResource(ANode: TdxXMLNode); virtual;
  public
    property Container: TdxSpreadSheetPictureContainer read GetContainer;
  end;

  { TdxSpreadSheetXLSXWriterExternalLinkBuilder }

  TdxSpreadSheetXLSXWriterExternalLinkBuilder = class(TdxSpreadSheetXLSXWriterCustomFileBuilder)
  strict private
    FLink: TdxSpreadSheetExternalLink;
  protected
    function EncodePath(const APath: TdxUnicodeString): TdxXMLString; virtual;
  public
    constructor Create(AOwner: TdxSpreadSheetXLSXWriter; AOwnerRels: TdxSpreadSheetXLSXWriterRels;
      ALink: TdxSpreadSheetExternalLink; const ATargetFileName: AnsiString);
    procedure Execute; override;
    //
    property Link: TdxSpreadSheetExternalLink read FLink;
  end;

  { TdxSpreadSheetXLSXWriterSharedStringsBuilder }

  TdxSpreadSheetXLSXWriterSharedStringsBuilder = class(TdxSpreadSheetXLSXWriterCustomFileBuilder)
  strict private
    function GetSharedStrings: TList; inline;
  public
    procedure Execute; override;
    procedure WriteFormattedSharedString(ANode: TdxXMLNode; AFormattedString: TdxSpreadSheetFormattedSharedString); virtual;
    //
    property SharedStrings: TList read GetSharedStrings;
  end;

  { TdxSpreadSheetXLSXWriterStylesBuilder }

  TdxSpreadSheetXLSXWriterStylesBuilder = class(TdxSpreadSheetXLSXWriterCustomFileBuilder)
  strict private
    function GetBorders: TList; inline;
    function GetCellStyles: TList; inline;
    function GetCustomFormats: TList; inline;
    function GetFills: TList; inline;
    function GetFonts: TList; inline;
    function GetFormats: TList; inline;
  public
    procedure AddColorNode(AParentNode: TdxXMLNode; const AChildNodeName: TdxXMLString; AColor: TColor);
    procedure Execute; override;
    function GetFormatID(AFormat: TdxSpreadSheetFormatHandle): Integer;
    procedure ProcessStylesGroup(AParentNode: TdxXMLNode;
      const AChildNodeName: AnsiString; AStyles: TList; AProc: TdxXMLNodeForEachProc);
    procedure ProcessStylesGroupBorders(ANode: TdxXMLNode; AData: Pointer); virtual;
    procedure ProcessStylesGroupCellStyle(ANode: TdxXMLNode; AData: Pointer); virtual;
    procedure ProcessStylesGroupFill(ANode: TdxXMLNode; AData: Pointer); virtual;
    procedure ProcessStylesGroupFont(ANode: TdxXMLNode; AData: Pointer); virtual;
    procedure ProcessStylesGroupNumberFormat(ANode: TdxXMLNode; AData: Pointer); virtual;
    //
    property Borders: TList read GetBorders;
    property CellStyles: TList read GetCellStyles;
    property CustomFormats: TList read GetCustomFormats;
    property Fills: TList read GetFills;
    property Fonts: TList read GetFonts;
    property Formats: TList read GetFormats;
  end;

  { TdxSpreadSheetXLSXWriterWorkbookBuilder }

  TdxSpreadSheetXLSXWriterWorkbookBuilder = class(TdxSpreadSheetXLSXWriterCustomFileBuilder)
  public
    procedure Execute; override;
    procedure WriteCalcProperties(AWorkbook: TdxXMLNode); virtual;
    procedure WriteDefinedName(ANode: TdxXMLNode; ADefinedName: TdxSpreadSheetDefinedName); virtual;
    procedure WriteDefinedNames(ANode: TdxXMLNode); virtual;
    procedure WriteExternalLink(const AFileName: AnsiString;
      ALink: TdxSpreadSheetExternalLink; ARels: TdxSpreadSheetXLSXWriterRels); virtual;
    procedure WriteExternalLinks(ANode: TdxXMLNode; ARels: TdxSpreadSheetXLSXWriterRels); virtual;
    procedure WriteProperties(AWorkbook: TdxXMLNode); virtual;
    procedure WriteSharedStrings(const AFileName: AnsiString; ARels: TdxSpreadSheetXLSXWriterRels); virtual;
    procedure WriteSheet(ANode: TdxXMLNode; AView: TdxSpreadSheetCustomView; ARels: TdxSpreadSheetXLSXWriterRels); virtual;
    procedure WriteSheets(ANode: TdxXMLNode; ARels: TdxSpreadSheetXLSXWriterRels); virtual;
    procedure WriteSheetView(const AFileName: AnsiString; AView: TdxSpreadSheetCustomView; ARels: TdxSpreadSheetXLSXWriterRels); virtual;
    procedure WriteStyles(const AFileName: AnsiString; ARels: TdxSpreadSheetXLSXWriterRels); virtual;
  end;

  { TdxSpreadSheetXLSXWriterWorksheetContainersBuilder }

  TdxSpreadSheetXLSXWriterWorksheetContainersBuilder = class(TdxSpreadSheetXLSXWriterCustomFileBuilder)
  strict private
    FContainerBuildersMap: TDictionary<TClass, TdxSpreadSheetXLSXWriterCustomContainerBuilderClass>;
    FView: TdxSpreadSheetTableView;
  protected
    function GetContainerBuilderClass(AContainer: TdxSpreadSheetContainer): TdxSpreadSheetXLSXWriterCustomContainerBuilderClass;
    procedure WriteContainer(ANode: TdxXMLNode; AContainer: TdxSpreadSheetContainer; ARels: TdxSpreadSheetXLSXWriterRels); virtual;
    procedure WriteContainers(ANode: TdxXMLNode; ARels: TdxSpreadSheetXLSXWriterRels); virtual;
  public
    constructor Create(AOwner: TdxSpreadSheetXLSXWriter; AOwnerRels: TdxSpreadSheetXLSXWriterRels;
      const ATargetFileName: AnsiString; AView: TdxSpreadSheetTableView);
    destructor Destroy; override;
    procedure Execute; override;
    //
    property ContainerBuildersMap: TDictionary<TClass, TdxSpreadSheetXLSXWriterCustomContainerBuilderClass> read FContainerBuildersMap;
    property View: TdxSpreadSheetTableView read FView;
  end;

  { TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder }

  TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder = class(TdxSpreadSheetXLSXWriterCustomFileBuilder)
  strict private
    FView: TdxSpreadSheetTableView;
  protected
    function CheckAreaBounds(const R: TRect): TRect;
    function ConvertRowHeight(const AValue: Integer): Double; virtual;
    function EncodeFloat(const AValue: Double): TdxUnicodeString;

    procedure WriteCell(ANode: TdxXMLNode; ACell: TdxSpreadSheetCell); virtual;
    procedure WriteColumns(ANode: TdxXMLNode; const ADimension: TRect); virtual;
    procedure WriteContainers(ANode: TdxXMLNode; ARels: TdxSpreadSheetXLSXWriterRels); virtual;
    procedure WriteContent(ANode: TdxXMLNode; ARels: TdxSpreadSheetXLSXWriterRels); virtual;
    procedure WriteFixedPaneProperties(ANode: TdxXMLNode); virtual;
    procedure WriteMergedCells(ANode: TdxXMLNode; AMergedCells: TdxSpreadSheetMergedCellList); virtual;
    procedure WriteProperties(ANode: TdxXMLNode); virtual;
    procedure WriteRows(ANode: TdxXMLNode; const ADimension: TRect); virtual;
    procedure WriteSelection(ANode: TdxXMLNode; ASelection: TdxSpreadSheetTableViewSelection); virtual;
    procedure WriteViewProperties(ANode: TdxXMLNode); virtual;
    procedure WriteViewProtection(ANode: TdxXMLNode); virtual;
  public
    constructor Create(AOwner: TdxSpreadSheetXLSXWriter; AOwnerRels: TdxSpreadSheetXLSXWriterRels;
      const ATargetFileName: AnsiString; AView: TdxSpreadSheetTableView);
    procedure Execute; override;
    //
    property View: TdxSpreadSheetTableView read FView;
  end;

implementation

uses
  AnsiStrings, Math, dxColorPicker, cxGraphics, dxSpreadSheetFormulas, dxHashUtils, dxSpreadSheetFormatXLSXTags,
  dxSpreadSheetFormatXLSX, StrUtils, dxTypeHelpers;

const
  CustomNumberFormatBase = 164;
  MaxZoomFactor = 400;
  MinZoomFactor = 10;

const
  sMsgWrongDataType = 'wrong data type';

type
  TdxSpreadSheetContainerAccess = class(TdxSpreadSheetContainer);
  TdxSpreadSheetTableColumnAccess = class(TdxSpreadSheetTableColumn);
  TdxSpreadSheetTableRowAccess = class(TdxSpreadSheetTableRow);

{ TdxSpreadSheetXLSXWriterRels }

constructor TdxSpreadSheetXLSXWriterRels.Create(AOwner: TPersistent);
begin
  inherited Create(AOwner);
  Root.AddChild(sdxXLSXNodeRelationships).Attributes.Add(sdxXLSXAttrXMLNS, sdxXLSXRelsNameSpace);
end;

procedure TdxSpreadSheetXLSXWriterRels.AddExternalRelationship(const AType, ATarget: TdxXMLString);
begin
  DoAddRelationship(AType, ATarget).Attributes.Add(sdxXLSXAttrTargetMode, sdxXLSXValueTargetModeExternal);
end;

procedure TdxSpreadSheetXLSXWriterRels.AddRelationship(const AType, ATarget: TdxXMLString);
begin
  DoAddRelationship(AType, ATarget);
end;

function TdxSpreadSheetXLSXWriterRels.GetRelationshipId(const ATarget: TdxXMLString): TdxXMLString;
var
  AHash: Pointer;
  ANode: TdxXMLNode;
begin
  Result := '';
  AHash := Pointer(dxElfHash(ATarget));
  ANode := Root.First.First;
  while ANode <> nil do
  begin
    if (AHash = ANode.Data) and (ANode.Attributes.GetValue(sdxXLSXAttrTarget) = ATarget) then
    begin
      Result := ANode.Attributes.GetValue(sdxXLSXAttrId);
      Break;
    end;
    ANode := ANode.Next;
  end;
end;

function TdxSpreadSheetXLSXWriterRels.DoAddRelationship(const AType, ATarget: TdxXMLString): TdxXMLNode;
begin
  Result := Root.First.AddChild(sdxXLSXNodeRelationship);
  Result.Attributes.Add(sdxXLSXAttrId, 'rId' + dxStringToAnsiString(IntToStr(Root.First.Count)));
  Result.Attributes.Add(sdxXLSXAttrType, AType);
  Result.Attributes.Add(sdxXLSXAttrTarget, ATarget);
  Result.Data := Pointer(dxElfHash(ATarget));
end;

function TdxSpreadSheetXLSXWriterRels.GetEmpty: Boolean;
begin
  Result := Root[0].Count = 0;
end;

{ TdxSpreadSheetXLSXWriter }

constructor TdxSpreadSheetXLSXWriter.Create(AOwner: TdxCustomSpreadSheet; AStream: TStream);
begin
  inherited Create(AOwner, AStream);
  FImages := TDictionary<TdxGPImage, AnsiString>.Create;
  FContentType := TdxSpreadSheetXMLDocument.Create(nil);
  InitializeContentTypes(FContentType.AddChild('Types'));

  FCustomFormats := TList.Create;
  FCellStyles := TList.Create;
  FFonts := TList.Create;
  FSharedStrings := TList.Create;
  FFills := TList.Create;
  FFormats := TList.Create;
  FBorders := TList.Create;

  AddDefaultResources;

  FColumnWidthHelper := TdxSpreadSheetExcelColumnWidthHelper.Create;
  SpreadSheet.DefaultCellStyle.Font.AssignToFont(FColumnWidthHelper.Font);
end;

destructor TdxSpreadSheetXLSXWriter.Destroy;
begin
  FreeAndNil(FImages);
  FreeAndNil(FFonts);
  FreeAndNil(FBorders);
  FreeAndNil(FFills);
  FreeAndNil(FFormats);
  FreeAndNil(FCellStyles);
  FreeAndNil(FContentType);
  FreeAndNil(FSharedStrings);
  FreeAndNil(FColumnWidthHelper);
  FreeAndNil(FCustomFormats);
  inherited Destroy;
end;

procedure TdxSpreadSheetXLSXWriter.WriteData;
var
  ARels: TdxSpreadSheetXLSXWriterRels;
begin
  ARels := TdxSpreadSheetXLSXWriterRels.Create(nil);
  try
    ExecuteSubTask(TdxSpreadSheetXLSXWriterWorkbookBuilder.Create(Self, ARels, sdxXLSXWorkbookFileName));
    RegisterFile(TdxSpreadSheetXLSXUtils.GetRelsFileNameForFile(''), sdxXLSXRelsContentType);
    WriteXML(TdxSpreadSheetXLSXUtils.GetRelsFileNameForFile(''), ARels);
    WriteXML(sdxXLSXContentTypeFileName, FContentType);
  finally
    ARels.Free;
  end;
end;

function TdxSpreadSheetXLSXWriter.CreateProgressHelper: TdxSpreadSheetCustomFilerProgressHelper;
begin
  Result := TdxSpreadSheetCustomFilerProgressHelper.Create(Self, SpreadSheet.SheetCount + 2);
end;

procedure TdxSpreadSheetXLSXWriter.AddColorNode(AParentNode: TdxXMLNode; const AChildNodeName: TdxXMLString; AColor: TColor);
begin
  if AColor <> clDefault then
  begin
    AParentNode := AParentNode.AddChild(AChildNodeName);
    if AColor = clDefault then
      AParentNode.Attributes.Add(sdxXLSXAttrIndexed, 64)
    else
      AParentNode.Attributes.Add(sdxXLSXAttrRGB, TdxColorPickerHexCodeHelper.AlphaColorToHexCode(dxColorToAlphaColor(AColor), cphnHTML));
  end;
end;

procedure TdxSpreadSheetXLSXWriter.AddDefaultResources;
var
  ABrush: TdxSpreadSheetBrushHandle;
begin
  Fills.Add(SpreadSheet.CellStyles.Brushes.AddBrush(SpreadSheet.CellStyles.Brushes.CreateBrush));

  ABrush := SpreadSheet.CellStyles.Brushes.CreateBrush;
  ABrush.Style := sscfsGray12;
  Fills.Add(SpreadSheet.CellStyles.Brushes.AddBrush(ABrush));

  CellStyles.Add(SpreadSheet.DefaultCellStyle.Handle);
end;

procedure TdxSpreadSheetXLSXWriter.AddFontNode(ANode: TdxXMLNode; AFont: TdxSpreadSheetFontHandle);
var
  AStyle: TFontStyle;
begin
  AddColorNode(ANode, sdxXLSXNodeColor, AFont.Color);
  ANode.AddChild(sdxXLSXNodeSZ).Attributes.Add(sdxXLSXAttrVal, AFont.Size);
  ANode.AddChild(sdxXLSXNodeName).Attributes.Add(sdxXLSXAttrVal, AFont.Name);
  ANode.AddChild(sdxXLSXNodeCharset).Attributes.Add(sdxXLSXAttrVal, AFont.Charset);

  for AStyle := Low(AStyle) to High(AStyle) do
  begin
    if AStyle in AFont.Style then
      ANode.AddChild(dxXLSXFontStyles[AStyle]);
  end;
end;

procedure TdxSpreadSheetXLSXWriter.InitializeContentTypes(ANode: TdxXMLNode);

  procedure AddDefaults(ANode: TdxXMLNode; const AExtension, AContentType: AnsiString);
  begin
    ANode := ANode.AddChild(sdxXLSXNodeDefault);
    ANode.Attributes.Add(sdxXLSXAttrExtension, AExtension);
    ANode.Attributes.Add(sdxXLSXAttrContentType, AContentType);
  end;

begin
  ANode.Attributes.Add(sdxXLSXAttrXMLNS, sdxXLSXContentTypeNameSpace);
  AddDefaults(ANode, sdxXLSXMimeTypePNGExt, sdxXLSXMimeTypePNG);
  AddDefaults(ANode, sdxXLSXMimeTypeJPGExt, sdxXLSXMimeTypeJPG);
  AddDefaults(ANode, sdxXLSXMimeTypeRELSExt, sdxXLSXMimeTypeRELS);
  AddDefaults(ANode, sdxXLSXMimeTypeXMLExt, sdxXLSXMimeTypeXML);
end;

procedure TdxSpreadSheetXLSXWriter.RegisterFile(const AFileName, AContentType: AnsiString;
  const ARelationship: AnsiString = ''; ARels: TdxSpreadSheetXLSXWriterRels = nil);
var
  ANode: TdxXMLNode;
begin
  if ARels <> nil then
    ARels.AddRelationship(ARelationship, dxUnixPathDelim + AFileName);

  ANode := FContentType.Root.First.AddChild(sdxXLSXNodeOverride);
  ANode.Attributes.Add(sdxXLSXAttrPartName, dxUnixPathDelim + AFileName);
  ANode.Attributes.Add(sdxXLSXAttrContentType, AContentType);
end;

{ TdxSpreadSheetXLSXWriterCustomBuilder }

constructor TdxSpreadSheetXLSXWriterCustomBuilder.Create(
  AOwner: TdxSpreadSheetXLSXWriter; AOwnerRels: TdxSpreadSheetXLSXWriterRels);
begin
  inherited Create(AOwner);
  FOwnerRels := AOwnerRels;
end;

function TdxSpreadSheetXLSXWriterCustomBuilder.AddResource(AResource: TObject; AResourceList: TList): Integer;
begin
  Result := AResourceList.IndexOf(AResource);
  if Result < 0 then
    Result := AResourceList.Add(AResource);
end;

function TdxSpreadSheetXLSXWriterCustomBuilder.GetOwner: TdxSpreadSheetXLSXWriter;
begin
  Result := TdxSpreadSheetXLSXWriter(inherited Owner);
end;

procedure TdxSpreadSheetXLSXWriterCustomBuilder.RegisterFile(const AFileName, AContentType: AnsiString;
  const ARelationship: AnsiString = ''; ARels: TdxSpreadSheetXLSXWriterRels = nil);
begin
  Owner.RegisterFile(AFileName, AContentType, ARelationship, ARels);
end;

{ TdxSpreadSheetXLSXWriterCustomFileBuilder }

constructor TdxSpreadSheetXLSXWriterCustomFileBuilder.Create(AOwner: TdxSpreadSheetXLSXWriter;
  AOwnerRels: TdxSpreadSheetXLSXWriterRels; const ATargetFileName: AnsiString);
begin
  inherited Create(AOwner, AOwnerRels);
  FTargetFileName := ATargetFileName;
end;

function TdxSpreadSheetXLSXWriterCustomFileBuilder.GetTargetFileNameRels: AnsiString;
begin
  Result := TdxSpreadSheetXLSXUtils.GetRelsFileNameForFile(TargetFileName);
end;

{ TdxSpreadSheetXLSXWriterCustomContainerBuilder }

constructor TdxSpreadSheetXLSXWriterCustomContainerBuilder.Create(AContainer: TdxSpreadSheetContainer;
  AOwner: TdxSpreadSheetXLSXWriter; AOwnerRels: TdxSpreadSheetXLSXWriterRels; AParentNode: TdxXMLNode);
begin
  inherited Create(AOwner, AOwnerRels);
  FParentNode := AParentNode;
  FContainer := AContainer;
end;

procedure TdxSpreadSheetXLSXWriterCustomContainerBuilder.Execute;
begin
  WriteContent(WriteAnchors(ParentNode));
end;

function TdxSpreadSheetXLSXWriterCustomContainerBuilder.WriteAnchors(ANode: TdxXMLNode): TdxXMLNode;
begin
  Result := ANode;
  case Container.AnchorType of
    catAbsolute:
      Result := WriteAbsoluteAnchors(ANode);
    catOneCell:
      Result := WriteOneCellAnchors(ANode);
    catTwoCell:
      Result := WriteTwoCellAnchors(ANode);
  else
    DoError(sdxErrorInternal, [sMsgWrongDataType], ssmtError);
  end;
end;

function TdxSpreadSheetXLSXWriterCustomContainerBuilder.WriteAbsoluteAnchors(ANode: TdxXMLNode): TdxXMLNode;
begin
  Result := ANode.AddChild(sdxXLSXNodeAnchorAbsolute);
  WritePoint(Result.AddChild(sdxXLSXNodePos), Container.AnchorPoint1.Offset, sdxXLSXAttrCoordX, sdxXLSXAttrCoordY);
  WritePoint(Result.AddChild(sdxXLSXNodeExt), Container.AnchorPoint2.Offset, sdxXLSXAttrCoordExtX, sdxXLSXAttrCoordExtY);
end;

function TdxSpreadSheetXLSXWriterCustomContainerBuilder.WriteOneCellAnchors(ANode: TdxXMLNode): TdxXMLNode;
begin
  Result := ANode.AddChild(sdxXLSXNodeAnchorOneCell);
  WriteAnchorMarker(Result.AddChild(sdxXLSXNodeAnshorFrom), Container.AnchorPoint1);
  WritePoint(Result.AddChild(sdxXLSXNodeExt), Container.AnchorPoint2.Offset, sdxXLSXAttrCoordExtX, sdxXLSXAttrCoordExtY);
  WriteEditAsMode(Result);
end;

function TdxSpreadSheetXLSXWriterCustomContainerBuilder.WriteTwoCellAnchors(ANode: TdxXMLNode): TdxXMLNode;
begin
  Result := ANode.AddChild(sdxXLSXNodeAnchorTwoCell);
  WriteAnchorMarker(Result.AddChild(sdxXLSXNodeAnshorFrom), Container.AnchorPoint1);
  WriteAnchorMarker(Result.AddChild(sdxXLSXNodeAnshorTo), Container.AnchorPoint2);
  WriteEditAsMode(Result);
end;

procedure TdxSpreadSheetXLSXWriterCustomContainerBuilder.WriteContent(ARootNode: TdxXMLNode);
begin
  // do nothing
end;

procedure TdxSpreadSheetXLSXWriterCustomContainerBuilder.WriteAnchorMarker(ANode: TdxXMLNode; AAnchor: TdxSpreadSheetContainerAnchorPoint);

  function GetCellColumnIndex(AValue: TdxSpreadSheetCell): Integer;
  begin
    if AValue <> nil then
      Result := AValue.ColumnIndex
    else
      Result := 0;
  end;

  function GetCellRowIndex(AValue: TdxSpreadSheetCell): Integer;
  begin
    if AValue <> nil then
      Result := AValue.RowIndex
    else
      Result := 0;
  end;

begin
  ANode.AddChild(sdxXLSXNodeDrawingPosMarkerColumn).TextAsUnicodeString := IntToStr(GetCellColumnIndex(AAnchor.Cell));
  ANode.AddChild(sdxXLSXNodeDrawingPosMarkerColumnOffset).TextAsUnicodeString := IntToStr(dxPixelsToEMU(AAnchor.Offset.X));

  ANode.AddChild(sdxXLSXNodeDrawingPosMarkerRow).TextAsUnicodeString := IntToStr(GetCellRowIndex(AAnchor.Cell));
  ANode.AddChild(sdxXLSXNodeDrawingPosMarkerRowOffset).TextAsUnicodeString := IntToStr(dxPixelsToEMU(AAnchor.Offset.Y));
end;

procedure TdxSpreadSheetXLSXWriterCustomContainerBuilder.WriteEditAsMode(ANode: TdxXMLNode);
begin
  if not Container.AnchorPoint1.FixedToCell then
    ANode.Attributes.SetValue(sdxXLSXAttrEditAs, sdxXLSXValueEditAsAbsolute)
  else
    if Container.AnchorPoint2.FixedToCell then
      ANode.Attributes.SetValue(sdxXLSXAttrEditAs, sdxXLSXValueEditAsTwoCell)
    else
      ANode.Attributes.SetValue(sdxXLSXAttrEditAs, sdxXLSXValueEditAsOneCell);
end;

procedure TdxSpreadSheetXLSXWriterCustomContainerBuilder.WritePoint(
  ANode: TdxXMLNode; const AValue: TPoint; const XName, YName: TdxXMLString);
begin
  ANode.Attributes.SetValueAsInt64(XName, dxPixelsToEMU(AValue.X));
  ANode.Attributes.SetValueAsInt64(YName, dxPixelsToEMU(AValue.Y));
end;

{ TdxSpreadSheetXLSXWriterCustomShapeContainerBuilder }

function TdxSpreadSheetXLSXWriterCustomShapeContainerBuilder.WriteImage(AImage: TdxGPImage): AnsiString;
const
  CodecMap: array[Boolean] of TdxImageDataFormat = (dxImagePng, dxImageJpeg);
  ExtMap: array[Boolean] of string = (sdxXLSXMimeTypePNGExt, sdxXLSXMimeTypeJPGExt);
var
  AFileName: AnsiString;
  AMemStream: TMemoryStream;
  ASaveAsJPEG: Boolean;
begin
  if not Owner.Images.TryGetValue(AImage, AFileName) then
  begin
    AMemStream := TMemoryStream.Create;
    try
      ASaveAsJPEG := AImage.ImageDataFormat = dxImageJpeg;
      AImage.SaveToStreamByCodec(AMemStream, CodecMap[ASaveAsJPEG]);

      AFileName := Format(sdxXLSXFileTemplateImage, [Owner.Images.Count + 1, ExtMap[ASaveAsJPEG]]);
      WriteFile(AFileName, AMemStream);
      Owner.Images.Add(AImage, AFileName);
    finally
      AMemStream.Free;
    end;
  end;
  OwnerRels.AddRelationship(sdxXLSXImageRelationship, dxUnixPathDelim + AFileName);
  Result := OwnerRels.GetRelationshipId(dxUnixPathDelim + AFileName);
end;

procedure TdxSpreadSheetXLSXWriterCustomShapeContainerBuilder.WriteColor(ANode: TdxXMLNode; AAlphaColor: TdxAlphaColor);
var
  AAlpha: Byte;
begin
  ANode.Attributes.Add(sdxXLSXAttrVal, TdxColorPickerHexCodeHelper.AlphaColorToHexCode(AAlphaColor, cphnHTML, False));

  AAlpha := dxGetAlpha(AAlphaColor);
  if AAlpha <> MaxByte then
    ANode.AddChild(sdxXLSXNodeColorAlpha).Attributes.Add(sdxXLSXAttrVal, TdxSpreadSheetXLSXUtils.EncodeColorAlpha(AAlpha));
end;

procedure TdxSpreadSheetXLSXWriterCustomShapeContainerBuilder.WriteGradientBrushParameters(ANode: TdxXMLNode; ABrush: TdxGPBrush);
const
  AnglesMap: array[TdxGPBrushGradientMode] of Integer = (0, 90, 45, 135);
var
  AChildNode: TdxXMLNode;
  AGradientPointNode: TdxXMLNode;
  I: Integer;
begin
  AChildNode := ANode.AddChild(sdxXLSXNodeGradientPoints);
  for I := 0 to ABrush.GradientPoints.Count - 1 do
  begin
    AGradientPointNode := AChildNode.AddChild(sdxXLSXNodeGradientPoint);
    AGradientPointNode.Attributes.Add(sdxXLSXAttrGradientPointPos,
      TdxSpreadSheetXLSXUtils.EncodePercents(ABrush.GradientPoints.Offsets[I] * 100));
    WriteColor(AGradientPointNode.AddChild(sdxXLSXNodeThemesCustomColor), ABrush.GradientPoints.Colors[I]);
  end;

  AChildNode := ANode.AddChild(sdxXLSXNodeLinearGradientFill);
  AChildNode.Attributes.Add(sdxXLSXAttrAngle, TdxSpreadSheetXLSXUtils.EncodePositiveFixedAngle(AnglesMap[ABrush.GradientMode]));
  AChildNode.Attributes.Add(sdxXLSXAttrScaled, True);
end;

procedure TdxSpreadSheetXLSXWriterCustomShapeContainerBuilder.WriteSolidBrushParameters(ANode: TdxXMLNode; ABrush: TdxGPBrush);
begin
  WriteColor(ANode.AddChild(sdxXLSXNodeThemesCustomColor), ABrush.Color);
end;

procedure TdxSpreadSheetXLSXWriterCustomShapeContainerBuilder.WriteTextureBrushParameters(ANode: TdxXMLNode; ABrush: TdxGPBrush);
var
  AChildNode: TdxXMLNode;
begin
  ANode.Attributes.SetValueAsBoolean(sdxXLSXAttrRotateWithShape, True);

  AChildNode := ANode.AddChild(sdxXLSXNodeDrawingBlip);
  if not ABrush.Texture.Empty then
  begin
    AChildNode.Attributes.SetValue(sdxXLSXAttrXMLNSR, sdxXLSXCommonRelationshipPath);
    AChildNode.Attributes.SetValue(sdxXLSXAttrDrawingResourceEmbed, WriteImage(ABrush.Texture));
  end;

  ANode.AddChild(sdxXLSXNodeDrawingAttributeSourceRect);

  AChildNode := ANode.AddChild(sdxXLSXNodeTile);
  AChildNode.Attributes.SetValueAsInteger(sdxXLSXAttrTileTX, 0);
  AChildNode.Attributes.SetValueAsInteger(sdxXLSXAttrTileTY, 0);
  AChildNode.Attributes.SetValueAsInteger(sdxXLSXAttrTileSX, TdxSpreadSheetXLSXUtils.EncodePercents(100));
  AChildNode.Attributes.SetValueAsInteger(sdxXLSXAttrTileSY, TdxSpreadSheetXLSXUtils.EncodePercents(100));
  AChildNode.Attributes.SetValueAsString(sdxXLSXAttrFlip, 'none');
  AChildNode.Attributes.SetValueAsString(sdxXLSXAttrTileAlign, 'tl');
end;

procedure TdxSpreadSheetXLSXWriterCustomShapeContainerBuilder.WriteDescription(ANode: TdxXMLNode);
begin
  ANode.Attributes.SetValueAsInteger('id', Container.Index + 1);
  ANode.Attributes.SetValueAsUnicodeString(sdxXLSXAttrName, Container.Name);
  if Container.Description <> '' then
    ANode.Attributes.SetValueAsUnicodeString(sdxXLSXAttrAlternateText, Container.Description);
  if Container.Title <> '' then
    ANode.Attributes.SetValueAsUnicodeString(sdxXLSXAttrTitle, Container.Title);
end;

procedure TdxSpreadSheetXLSXWriterCustomShapeContainerBuilder.WriteRestrictions(ANode: TdxXMLNode);
var
  AIndex: TdxSpreadSheetContainerRestriction;
begin
  for AIndex := Low(TdxSpreadSheetContainerRestriction) to High(TdxSpreadSheetContainerRestriction) do
  begin
    if AIndex in Container.Restrictions then
      ANode.Attributes.SetValueAsBoolean(dxXLSXRestrictionNames[AIndex], True);
  end;
end;

procedure TdxSpreadSheetXLSXWriterCustomShapeContainerBuilder.WriteShapeFillParameters(AParentNode: TdxXMLNode; ABrush: TdxGPBrush);
begin
  if ABrush.IsEmpty then
    AParentNode.AddChild(sdxXLSXNodeNoFill)
  else
    case ABrush.Style of
      gpbsSolid:
        WriteSolidBrushParameters(AParentNode.AddChild(sdxXLSXNodeSolidFill), ABrush);
      gpbsGradient:
        WriteGradientBrushParameters(AParentNode.AddChild(sdxXLSXNodeGradientFill), ABrush);
      gpbsTexture:
        WriteTextureBrushParameters(AParentNode.AddChild(sdxXLSXNodeTexturedFill), ABrush);
    end;
end;

procedure TdxSpreadSheetXLSXWriterCustomShapeContainerBuilder.WriteShapeGeometry(ANode: TdxXMLNode);
begin
  ANode.Attributes.Add(sdxXLSXAttrPreset, dxXLSXShapeTypeMap[Container.Shape.ShapeType]);
  ANode.AddChild(sdxXLSXNodeAVList);
end;

procedure TdxSpreadSheetXLSXWriterCustomShapeContainerBuilder.WriteShapeLineParameters(ANode: TdxXMLNode; APen: TdxGPPen);
begin
  ANode.Attributes.SetValueAsInt64(sdxXLSXAttrLineWidth, dxPixelsToEMUF(APen.Width));
  WriteShapeFillParameters(ANode, APen.Brush);
  if APen.Style <> gppsSolid then
    ANode.AddChild(sdxXLSXNodeLineDash).Attributes.Add(sdxXLSXAttrVal, dxXLSXPenStyleMap[APen.Style]);
end;

procedure TdxSpreadSheetXLSXWriterCustomShapeContainerBuilder.WriteShapeProperties(ANode: TdxXMLNode);
begin
  WriteTransform(ANode.AddChild(sdxXLSXNodeDrawingXForm));
  WriteShapeGeometry(ANode.AddChild(sdxXLSXNodeDrawingShapeGeometry));
  WriteShapeFillParameters(ANode, Container.Shape.Brush);
  WriteShapeLineParameters(ANode.AddChild(sdxXLSXNodeLine), Container.Shape.Pen);
end;

procedure TdxSpreadSheetXLSXWriterCustomShapeContainerBuilder.WriteTransform(ANode: TdxXMLNode);
var
  AAngle: Integer;
  ABounds: TRect;
  AChildNode: TdxXMLNode;
begin
  AAngle := Round(Container.Transform.RotationAngle);
  if AAngle <> 0 then
    ANode.Attributes.SetValueAsInteger(sdxXLSXAttrRot, TdxSpreadSheetXLSXUtils.EncodePositiveFixedAngle(AAngle));
  if Container.Transform.FlipHorizontally then
    ANode.Attributes.SetValueAsBoolean(sdxXLSXAttrFlipH, Container.Transform.FlipHorizontally);
  if Container.Transform.FlipVertically then
    ANode.Attributes.SetValueAsBoolean(sdxXLSXAttrFlipV, Container.Transform.FlipVertically);

  ABounds := TdxSpreadSheetContainerAccess(Container).Calculator.CalculateBounds;

  AChildNode := ANode.AddChild(sdxXLSXNodeCoordOff);
  AChildNode.Attributes.Add(sdxXLSXAttrCoordX, dxPixelsToEMU(ABounds.Left));
  AChildNode.Attributes.Add(sdxXLSXAttrCoordY, dxPixelsToEMU(ABounds.Top));

  AChildNode := ANode.AddChild(sdxXLSXNodeCoordExt);
  AChildNode.Attributes.Add(sdxXLSXAttrCoordExtX, dxPixelsToEMU(ABounds.Width));
  AChildNode.Attributes.Add(sdxXLSXAttrCoordExtY, dxPixelsToEMU(ABounds.Height));
end;

function TdxSpreadSheetXLSXWriterCustomShapeContainerBuilder.GetContainer: TdxSpreadSheetShapeContainer;
begin
  Result := inherited Container as TdxSpreadSheetShapeContainer;
end;

{ TdxSpreadSheetXLSXWriterShapeContainerBuilder }

procedure TdxSpreadSheetXLSXWriterShapeContainerBuilder.WriteContent(ARootNode: TdxXMLNode);
var
  ANode: TdxXMLNode;
begin
  ANode := ARootNode.AddChild(sdxXLSXNodeDrawingShapeContainer);
  ANode.Attributes.SetValue(sdxXLSXAttrMacro, '');
  ANode.Attributes.SetValue(sdxXLSXAttrTextLink, '');
  WriteShapeDescription(ANode.AddChild(sdxXLSXNodeDrawingShapeDesription));
  WriteShapeProperties(ANode.AddChild(sdxXLSXNodeDrawingShapeProperties));

  ARootNode.AddChild(sdxXLSXNodeClientData);
end;

procedure TdxSpreadSheetXLSXWriterShapeContainerBuilder.WriteShapeDescription(ANode: TdxXMLNode);
begin
  WriteDescription(ANode.AddChild(sdxXLSXNodeDrawingDescription));
  WriteRestrictions(ANode.AddChild(sdxXLSXNodeDrawingShapeAttributesEx).AddChild(sdxXLSXNodeDrawingShapeLocks));
end;

{ TdxSpreadSheetXLSXWriterPictureContainerBuilder }

procedure TdxSpreadSheetXLSXWriterPictureContainerBuilder.WriteContent(ARootNode: TdxXMLNode);
var
  ANode: TdxXMLNode;
begin
  ANode := ARootNode.AddChild(sdxXLSXNodeDrawingPictureContainer);
  WritePictureDescription(ANode.AddChild(sdxXLSXNodeDrawingPictureDescription));
  WritePictureResource(ANode.AddChild(sdxXLSXNodeDrawingBlipFill));
  WriteShapeProperties(ANode.AddChild(sdxXLSXNodeDrawingShapeProperties));
  ARootNode.AddChild(sdxXLSXNodeClientData);
end;

procedure TdxSpreadSheetXLSXWriterPictureContainerBuilder.WritePictureDescription(ANode: TdxXMLNode);
begin
  WriteDescription(ANode.AddChild(sdxXLSXNodeDrawingDescription));
  WriteRestrictions(ANode.AddChild(sdxXLSXNodeDrawingPictureAttributes).AddChild(sdxXLSXNodeDrawingPictureLocks));
end;

procedure TdxSpreadSheetXLSXWriterPictureContainerBuilder.WritePictureResource(ANode: TdxXMLNode);
var
  AChildNode: TdxXMLNode;
begin
  AChildNode := ANode.AddChild(sdxXLSXNodeDrawingBlip);
  if not Container.Picture.Empty then
  begin
    AChildNode.Attributes.SetValue(sdxXLSXAttrXMLNSR, sdxXLSXCommonRelationshipPath);
    AChildNode.Attributes.SetValue(sdxXLSXAttrDrawingResourceEmbed, WriteImage(Container.Picture.Image));
  end;
  ANode.AddChild(sdxXLSXNodeStretch).AddChild(sdxXLSXNodeFillRect);
end;

function TdxSpreadSheetXLSXWriterPictureContainerBuilder.GetContainer: TdxSpreadSheetPictureContainer;
begin
  Result := inherited Container as TdxSpreadSheetPictureContainer;
end;

{ TdxSpreadSheetXLSXWriterExternalLinkBuilder }

constructor TdxSpreadSheetXLSXWriterExternalLinkBuilder.Create(AOwner: TdxSpreadSheetXLSXWriter;
  AOwnerRels: TdxSpreadSheetXLSXWriterRels; ALink: TdxSpreadSheetExternalLink; const ATargetFileName: AnsiString);
begin
  inherited Create(AOwner, AOwnerRels, ATargetFileName);
  FLink := ALink;
end;

procedure TdxSpreadSheetXLSXWriterExternalLinkBuilder.Execute;
var
  ADoc: TdxXMLDocument;
  ADocRels: TdxSpreadSheetXLSXWriterRels;
  ANode: TdxXMLNode;
  ATarget: TdxXMLString;
begin
  ADoc := TdxXMLDocument.Create(nil);
  try
    ATarget := EncodePath(Link.Target);

    ANode := ADoc.Root.AddChild(sdxXLSXNodeExternalLink);
    ANode.Attributes.SetValue(sdxXLSXAttrXMLNS, sdxXLSXWorkbookNameSpace);

    ADocRels := TdxSpreadSheetXLSXWriterRels.Create(nil);
    try
      ADocRels.AddExternalRelationship(sdxXLSXExternalLinkPathRelationship, ATarget);
      WriteXML(TargetFileNameRels, ADocRels);

      ANode := ANode.AddChild(sdxXLSXNodeExternalBook);
      ANode.Attributes.SetValue(sdxXLSXAttrXMLNSR, sdxXLSXCommonRelationshipPath);
      ANode.Attributes.SetValue(sdxXLSXAttrRId, ADocRels.GetRelationshipId(ATarget));
    finally
      ADocRels.Free;
    end;

    RegisterFile(TargetFileName, sdxXLSXExternalLinkContentType, sdxXLSXExternalLinkRelationship, OwnerRels);
    WriteXML(TargetFileName, ADoc);
  finally
    ADoc.Free;
  end;
end;

function TdxSpreadSheetXLSXWriterExternalLinkBuilder.EncodePath(const APath: TdxUnicodeString): TdxXMLString;
begin
  Result := dxUnicodeStringToXMLString(APath);
end;

{ TdxSpreadSheetXLSXWriterSharedStringsBuilder }

procedure TdxSpreadSheetXLSXWriterSharedStringsBuilder.Execute;
var
  ADoc: TdxXMLDocument;
  ANode: TdxXMLNode;
  AString: TdxSpreadSheetSharedString;
  AStringNode: TdxXMLNode;
  I: Integer;
begin
  if SharedStrings.Count = 0 then
  begin
    Owner.ProgressHelper.SkipStage;
    Exit;
  end;

  ADoc := TdxSpreadSheetXMLDocument.Create(nil);
  try
    ANode := ADoc.Root.AddChild(sdxXLSXNodeSST);
    ANode.Attributes.Add(sdxXLSXAttrXMLNS, sdxXLSXWorkbookNameSpace);
    ANode.Attributes.Add(sdxXLSXAttrCount, SharedStrings.Count);
    ANode.Attributes.Add(sdxXLSXAttrUniqueCount, SharedStrings.Count);

    Owner.ProgressHelper.BeginStage(SharedStrings.Count);
    try
      for I := 0 to SharedStrings.Count - 1 do
      begin
        AString := TdxSpreadSheetSharedString(SharedStrings.List[I]);
        AStringNode := ANode.AddChild(sdxXLSXNodeSI);
        if AString is TdxSpreadSheetFormattedSharedString then
          WriteFormattedSharedString(AStringNode, TdxSpreadSheetFormattedSharedString(AString))
        else
          AStringNode.AddChild(sdxXLSXNodeText).TextAsUnicodeString := AString.Value;

        Owner.ProgressHelper.NextTask;
      end;
    finally
      Owner.ProgressHelper.EndStage;
    end;

    RegisterFile(TargetFileName, sdxXLSXSharedStringsContentType, sdxXLSXSharedStringRelationship, OwnerRels);
    WriteXML(TargetFileName, ADoc);
  finally
    ADoc.Free;
  end;
end;

procedure TdxSpreadSheetXLSXWriterSharedStringsBuilder.WriteFormattedSharedString(
  ANode: TdxXMLNode; AFormattedString: TdxSpreadSheetFormattedSharedString);
var
  ALength: Integer;
  ARun: TdxSpreadSheetFormattedSharedStringRun;
  ARunNode: TdxXMLNode;
  I: Integer;
  S: TdxUnicodeString;
begin
  S := AFormattedString.Value;
  for I := 0 to AFormattedString.Runs.Count - 1 do
  begin
    ARun := AFormattedString.Runs[I];
    if I + 1 < AFormattedString.Runs.Count then
      ALength := AFormattedString.Runs[I + 1].StartIndex - ARun.StartIndex
    else
      ALength := Length(S) - ARun.StartIndex + 1;

    ARunNode := ANode.AddChild(sdxXLSXNodeRichTextRun);
    Owner.AddFontNode(ARunNode.AddChild(sdxXLSXNodeRichTextRunParagraph), ARun.FontHandle);
    ARunNode.AddChild(sdxXLSXNodeText).TextAsUnicodeString := Copy(S, ARun.StartIndex, ALength);
  end;
end;

function TdxSpreadSheetXLSXWriterSharedStringsBuilder.GetSharedStrings: TList;
begin
  Result := Owner.SharedStrings;
end;

{ TdxSpreadSheetXLSXWriterStylesBuilder }

procedure TdxSpreadSheetXLSXWriterStylesBuilder.Execute;
var
  ADoc: TdxXMLDocument;
  ANode: TdxXMLNode;
  ANodeCellStyleXfs: TdxXMLNode;
  AStyle: TdxSpreadSheetCellStyleHandle;
  I: Integer;
begin
  ADoc := TdxSpreadSheetXMLDocument.Create(nil);
  try
    ANode := ADoc.Root.AddChild(sdxXLSXNodeStyleSheet);
    ANode.Attributes.Add(sdxXLSXAttrXMLNS, sdxXLSXWorkbookNameSpace);

    for I := 0 to CellStyles.Count - 1 do
    begin
      AStyle := TdxSpreadSheetCellStyleHandle(CellStyles[I]);
      AddResource(AStyle.Borders, Borders);
      AddResource(AStyle.DataFormat, Formats);
      AddResource(AStyle.Brush, Fills);
      AddResource(AStyle.Font, Fonts);
    end;

    Owner.ProgressHelper.BeginStage(CellStyles.Count + Borders.Count + Formats.Count + Fonts.Count);
    try
      ProcessStylesGroup(ANode.AddChild(sdxXLSXNodeStyleNumberFormats), sdxXLSXNodeStyleNumberFormat, Formats, ProcessStylesGroupNumberFormat);
      ProcessStylesGroup(ANode.AddChild(sdxXLSXNodeStyleFonts), sdxXLSXNodeStyleFont, Fonts, ProcessStylesGroupFont);
      ProcessStylesGroup(ANode.AddChild(sdxXLSXNodeStyleFills), sdxXLSXNodeStyleFill, Fills, ProcessStylesGroupFill);
      ProcessStylesGroup(ANode.AddChild(sdxXLSXNodeStyleBorders), sdxXLSXNodeStyleBorder, Borders, ProcessStylesGroupBorders);

      ANodeCellStyleXfs := ANode.AddChild(sdxXLSXNodeStyleCellStyleXfs);
      ANodeCellStyleXfs.Attributes.Add(sdxXLSXAttrCount, 1);
      ANodeCellStyleXfs := ANodeCellStyleXfs.AddChild(sdxXLSXNodeStyleCellXf);
      ANodeCellStyleXfs.Attributes.Add(sdxXLSXAttrBorderId, 0);
      ANodeCellStyleXfs.Attributes.Add(sdxXLSXAttrFillId, 0);
      ANodeCellStyleXfs.Attributes.Add(sdxXLSXAttrFontId, 0);
      ANodeCellStyleXfs.Attributes.Add(sdxXLSXAttrNumFmtId, 0);

      ProcessStylesGroup(ANode.AddChild(sdxXLSXNodeStyleCellXfs), sdxXLSXNodeStyleCellXf, CellStyles, ProcessStylesGroupCellStyle);
    finally
      Owner.ProgressHelper.EndStage;
    end;

    RegisterFile(TargetFileName, sdxXLSXStylesContentType, sdxXLSXStyleRelationship, OwnerRels);
    WriteXML(TargetFileName, ADoc);
  finally
    ADoc.Free;
  end;
end;

procedure TdxSpreadSheetXLSXWriterStylesBuilder.AddColorNode(
  AParentNode: TdxXMLNode; const AChildNodeName: TdxXMLString; AColor: TColor);
begin
  Owner.AddColorNode(AParentNode, AChildNodeName, AColor);
end;

function TdxSpreadSheetXLSXWriterStylesBuilder.GetFormatID(AFormat: TdxSpreadSheetFormatHandle): Integer;
begin
  Result := AFormat.FormatCodeID;
  if (Result < 0) or (SpreadSheet.CellStyles.Formats.PredefinedFormats.GetFormatHandleByID(Result) = nil) then
  begin
    Result := CustomFormats.IndexOf(AFormat);
    if Result < 0 then
      Result := CustomFormats.Add(AFormat);
    Inc(Result, CustomNumberFormatBase);
  end;
end;

procedure TdxSpreadSheetXLSXWriterStylesBuilder.ProcessStylesGroup(
  AParentNode: TdxXMLNode; const AChildNodeName: AnsiString; AStyles: TList; AProc: TdxXMLNodeForEachProc);
var
  I: Integer;
begin
  AParentNode.Attributes.Add(sdxXLSXAttrCount, AStyles.Count);
  for I := 0 to AStyles.Count - 1 do
  begin
    AProc(AParentNode.AddChild(AChildNodeName), AStyles.List[I]);
    Owner.ProgressHelper.NextTask;
  end;
end;

procedure TdxSpreadSheetXLSXWriterStylesBuilder.ProcessStylesGroupBorders(ANode: TdxXMLNode; AData: Pointer);
const
  BordersOrder: array [0..3] of TcxBorder = (bLeft, bRight, bTop, bBottom);
var
  ABorder: TcxBorder;
  ABorderNode: TdxXMLNode;
  ABordersHandle: TdxSpreadSheetBordersHandle;
  ABorderStyle: AnsiString;
  I: Integer;
begin
  ABordersHandle := TdxSpreadSheetBordersHandle(AData);
  for I := 0 to Length(BordersOrder) - 1 do
  begin
    ABorder := BordersOrder[I];
    ABorderNode := ANode.AddChild(dxXLSXBorderNames[ABorder]);
    ABorderStyle := TdxSpreadSheetXLSXHelper.BorderStyleToString(ABordersHandle.BorderStyle[ABorder]);
    if ABorderStyle <> '' then
      ABorderNode.Attributes.Add(sdxXLSXAttrStyle, ABorderStyle);
    AddColorNode(ABorderNode, sdxXLSXNodeColor, ABordersHandle.BorderColor[ABorder]);
  end;
  ANode.AddChild(sdxXLSXNodeDiagonal);
end;

procedure TdxSpreadSheetXLSXWriterStylesBuilder.ProcessStylesGroupCellStyle(ANode: TdxXMLNode; AData: Pointer);
var
  AApplyProtection: Boolean;
  AChildNode: TdxXMLNode;
  AStyleHandle: TdxSpreadSheetCellStyleHandle;
begin
  AStyleHandle := TdxSpreadSheetCellStyleHandle(AData);
  AApplyProtection := [csHidden, csLocked] * AStyleHandle.States <> [];

  ANode.Attributes.Add(sdxXLSXAttrApplyAlignment, True);
  ANode.Attributes.Add(sdxXLSXAttrApplyBorder, True);
  ANode.Attributes.Add(sdxXLSXAttrApplyFill, True);
  ANode.Attributes.Add(sdxXLSXAttrApplyNumberFormat, True);
  ANode.Attributes.Add(sdxXLSXAttrApplyFont, True);
  ANode.Attributes.Add(sdxXLSXAttrApplyProtection, AApplyProtection);

  ANode.Attributes.Add(sdxXLSXAttrBorderId, Borders.IndexOf(AStyleHandle.Borders));
  ANode.Attributes.Add(sdxXLSXAttrFillId, Fills.IndexOf(AStyleHandle.Brush));
  ANode.Attributes.Add(sdxXLSXAttrFontId, Fonts.IndexOf(AStyleHandle.Font));
  ANode.Attributes.Add(sdxXLSXAttrNumFmtId, GetFormatID(AStyleHandle.DataFormat));
  ANode.Attributes.Add(sdxXLSXAttrXFId, 0);

  AChildNode := ANode.AddChild(sdxXLSXNodeAlignment);
  AChildNode.Attributes.Add(sdxXLSXAttrHorizontal, TdxSpreadSheetXLSXHelper.AlignHorzToString(AStyleHandle.AlignHorz));
  AChildNode.Attributes.Add(sdxXLSXAttrVertical, TdxSpreadSheetXLSXHelper.AlignVertToString(AStyleHandle.AlignVert));
  if AStyleHandle.AlignHorzIndent <> 0 then
    AChildNode.Attributes.Add(sdxXLSXAttrIndent, Owner.ColumnWidthHelper.PixelsToSpacesNumber(AStyleHandle.AlignHorzIndent));
  AChildNode.Attributes.Add(sdxXLSXAttrTextRotation, AStyleHandle.Rotation);
  AChildNode.Attributes.Add(sdxXLSXAttrShrinkToFit, csShrinkToFit in AStyleHandle.States);
  AChildNode.Attributes.Add(sdxXLSXAttrWrapText, csWordWrap in AStyleHandle.States);

  if AApplyProtection then
  begin
    AChildNode := ANode.AddChild(sdxXLSXNodeProtection);
    AChildNode.Attributes.Add(sdxXLSXAttrHidden, csHidden in AStyleHandle.States);
    AChildNode.Attributes.Add(sdxXLSXAttrLocked, csLocked in AStyleHandle.States);
  end;
end;

procedure TdxSpreadSheetXLSXWriterStylesBuilder.ProcessStylesGroupFill(ANode: TdxXMLNode; AData: Pointer);
var
  ABrushHandle: TdxSpreadSheetBrushHandle;
begin
  ABrushHandle := TdxSpreadSheetBrushHandle(AData);

  ANode := ANode.AddChild(sdxXLSXNodeCellStylePatternFill);
  ANode.Attributes.Add(sdxXLSXAttrPatternType, TdxSpreadSheetXLSXHelper.FillStyleToString(ABrushHandle));

  if ABrushHandle.Style = sscfsSolid then
  begin
    AddColorNode(ANode, sdxXLSXNodeForegroundColor, ABrushHandle.BackgroundColor);
    AddColorNode(ANode, sdxXLSXNodeBackgroundColor, clDefault);
  end
  else
  begin
    AddColorNode(ANode, sdxXLSXNodeForegroundColor, ABrushHandle.ForegroundColor);
    AddColorNode(ANode, sdxXLSXNodeBackgroundColor, ABrushHandle.BackgroundColor);
  end;
end;

procedure TdxSpreadSheetXLSXWriterStylesBuilder.ProcessStylesGroupFont(ANode: TdxXMLNode; AData: Pointer);
begin
  Owner.AddFontNode(ANode, TdxSpreadSheetFontHandle(AData));
end;

procedure TdxSpreadSheetXLSXWriterStylesBuilder.ProcessStylesGroupNumberFormat(ANode: TdxXMLNode; AData: Pointer);
var
  AFormatHandle: TdxSpreadSheetFormatHandle;
begin
  AFormatHandle := TdxSpreadSheetFormatHandle(AData);
  ANode.Attributes.Add(sdxXLSXAttrFormatCode, AFormatHandle.FormatCode);
  ANode.Attributes.Add(sdxXLSXAttrNumFmtId, GetFormatID(AFormatHandle));
end;

function TdxSpreadSheetXLSXWriterStylesBuilder.GetBorders: TList;
begin
  Result := Owner.Borders;
end;

function TdxSpreadSheetXLSXWriterStylesBuilder.GetCellStyles: TList;
begin
  Result := Owner.CellStyles;
end;

function TdxSpreadSheetXLSXWriterStylesBuilder.GetCustomFormats: TList;
begin
  Result := Owner.CustomFormats;
end;

function TdxSpreadSheetXLSXWriterStylesBuilder.GetFills: TList;
begin
  Result := Owner.Fills;
end;

function TdxSpreadSheetXLSXWriterStylesBuilder.GetFonts: TList;
begin
  Result := Owner.Fonts;
end;

function TdxSpreadSheetXLSXWriterStylesBuilder.GetFormats: TList;
begin
  Result := Owner.Formats;
end;

{ TdxSpreadSheetXLSXWriterWorkbookBuilder }

procedure TdxSpreadSheetXLSXWriterWorkbookBuilder.Execute;
var
  ANode: TdxXMLNode;
  AWorkbook: TdxXMLDocument;
  AWorkbookRels: TdxSpreadSheetXLSXWriterRels;
begin
  AWorkbook := TdxSpreadSheetXMLDocument.Create(nil);
  try
    AWorkbookRels := TdxSpreadSheetXLSXWriterRels.Create(nil);
    try
      ANode := AWorkbook.Root.AddChild(sdxXLSXNodeWorkbook);
      ANode.Attributes.Add(sdxXLSXAttrXMLNS, sdxXLSXWorkbookNameSpace);
      ANode.Attributes.Add(sdxXLSXAttrXMLNSR, sdxXLSXCommonRelationshipPath);

      WriteProperties(ANode);
      WriteSheets(ANode.AddChild(sdxXLSXNodeSheets), AWorkbookRels);
      if SpreadSheet.ExternalLinks.Count > 0 then
        WriteExternalLinks(ANode.AddChild(sdxXLSXNodeExternalReferences), AWorkbookRels);
      if SpreadSheet.DefinedNames.Count > 0 then
        WriteDefinedNames(ANode.AddChild(sdxXLSXNodeDefinedNames));
      WriteCalcProperties(ANode);

      WriteSharedStrings(sdxXLSXSharedStringsFileName, AWorkbookRels); 
      WriteStyles(sdxXLSXStylesFileName, AWorkbookRels);

      WriteXML(TargetFileNameRels, AWorkbookRels);
    finally
      AWorkbookRels.Free;
    end;

    RegisterFile(TargetFileName, sdxXLSXWorkbookContentType, sdxXLSXWorkbookRelationship, OwnerRels);
    WriteXML(TargetFileName, AWorkbook);
  finally
    AWorkbook.Free;
  end;
end;

procedure TdxSpreadSheetXLSXWriterWorkbookBuilder.WriteCalcProperties(AWorkbook: TdxXMLNode);
const
  Map: array[Boolean] of AnsiString = (sdxXLSXValueA1, sdxXLSXValueR1C1);
var
  ANode: TdxXMLNode;
begin
  ANode := AWorkbook.AddChild(sdxXLSXNodeCalcPr);
  ANode.Attributes.Add(sdxXLSXAttrRefMode, Map[SpreadSheet.OptionsView.R1C1Reference]);
  if SpreadSheet.OptionsBehavior.IterativeCalculation then
  begin
    ANode.Attributes.Add(sdxXLSXAttrIterate, True);
    if SpreadSheet.OptionsBehavior.IterativeCalculationMaxCount <> 100 then
      ANode.Attributes.Add(sdxXLSXAttrIterateCount, SpreadSheet.OptionsBehavior.IterativeCalculationMaxCount);
  end;
end;

procedure TdxSpreadSheetXLSXWriterWorkbookBuilder.WriteDefinedName(ANode: TdxXMLNode; ADefinedName: TdxSpreadSheetDefinedName);
var
  AReference: TdxUnicodeString;
begin
  ANode.Attributes.SetValueAsUnicodeString(sdxXLSXAttrName, ADefinedName.Caption);
  if ADefinedName.Scope <> nil then
    ANode.Attributes.SetValueAsInteger(sdxXLSXAttrLocalSheetId, ADefinedName.Scope.Index);
  AReference := ADefinedName.Reference;
  if (Length(AReference) > 0) and (AReference[1] = dxDefaultOperations[opEQ]) then
    Delete(AReference, 1, 1);
  ANode.TextAsUnicodeString := AReference;
end;

procedure TdxSpreadSheetXLSXWriterWorkbookBuilder.WriteDefinedNames(ANode: TdxXMLNode);
var
  I: Integer;
begin
  for I := 0 to SpreadSheet.DefinedNames.Count - 1 do
    WriteDefinedName(ANode.AddChild(sdxXLSXNodeDefinedName), SpreadSheet.DefinedNames[I]);
end;

procedure TdxSpreadSheetXLSXWriterWorkbookBuilder.WriteExternalLink(
  const AFileName: AnsiString; ALink: TdxSpreadSheetExternalLink; ARels: TdxSpreadSheetXLSXWriterRels);
begin
  ExecuteSubTask(TdxSpreadSheetXLSXWriterExternalLinkBuilder.Create(Owner, ARels, ALink, AFileName));
end;

procedure TdxSpreadSheetXLSXWriterWorkbookBuilder.WriteExternalLinks(
  ANode: TdxXMLNode; ARels: TdxSpreadSheetXLSXWriterRels);
var
  I: Integer;
  AFileName: AnsiString;
begin
  for I := 0 to SpreadSheet.ExternalLinks.Count - 1 do
  begin
    AFileName := Format(sdxXLSXFileTemplateExternalLink, [I + 1]);
    WriteExternalLink(AFileName, SpreadSheet.ExternalLinks[I], ARels);
    ANode.AddChild(sdxXLSXNodeExternalReference).Attributes.Add(sdxXLSXAttrRId, ARels.GetRelationshipId(dxUnixPathDelim + AFileName));
  end;
end;

procedure TdxSpreadSheetXLSXWriterWorkbookBuilder.WriteProperties(AWorkbook: TdxXMLNode);
var
  ANode: TdxXMLNode;
begin
  ANode := AWorkbook.AddChild(sdxXLSXNodeWorkbookPr);
  ANode.Attributes.Add(sdxXLSXAttrDate1904, SpreadSheet.OptionsView.ActualDateTimeSystem = dts1904);

  if SpreadSheet.OptionsBehavior.Protected then
  begin
    ANode := AWorkbook.AddChild(sdxXLSXNodeWorkbookProtection);
    ANode.Attributes.Add(sdxXLSXAttrLockStructure, True);
  end;
end;

procedure TdxSpreadSheetXLSXWriterWorkbookBuilder.WriteSheets(ANode: TdxXMLNode; ARels: TdxSpreadSheetXLSXWriterRels);
var
  I: Integer;
begin
  for I := 0 to SpreadSheet.SheetCount - 1 do
    WriteSheet(ANode.AddChild(sdxXLSXNodeSheet), SpreadSheet.Sheets[I], ARels);
end;

procedure TdxSpreadSheetXLSXWriterWorkbookBuilder.WriteSheetView(
  const AFileName: AnsiString; AView: TdxSpreadSheetCustomView; ARels: TdxSpreadSheetXLSXWriterRels);
begin
  if AView is TdxSpreadSheetTableView then
    ExecuteSubTask(TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder.Create(Owner, ARels, AFileName, TdxSpreadSheetTableView(AView)))
  else
    DoError(sdxErrorInternal, ['export of ' + AView.ClassName + ' is not supported'], ssmtError);
end;

procedure TdxSpreadSheetXLSXWriterWorkbookBuilder.WriteStyles(
  const AFileName: AnsiString; ARels: TdxSpreadSheetXLSXWriterRels);
begin
  ExecuteSubTask(TdxSpreadSheetXLSXWriterStylesBuilder.Create(Owner, ARels, AFileName));
end;

procedure TdxSpreadSheetXLSXWriterWorkbookBuilder.WriteSharedStrings(
  const AFileName: AnsiString; ARels: TdxSpreadSheetXLSXWriterRels);
begin
  ExecuteSubTask(TdxSpreadSheetXLSXWriterSharedStringsBuilder.Create(Owner, ARels, AFileName));
end;

procedure TdxSpreadSheetXLSXWriterWorkbookBuilder.WriteSheet(
  ANode: TdxXMLNode; AView: TdxSpreadSheetCustomView; ARels: TdxSpreadSheetXLSXWriterRels);
var
  AFileName: AnsiString;
begin
  AFileName := Format(sdxXLSXFileTemplateWorksheet, [AView.Index + 1]);
  ANode.Attributes.Add(sdxXLSXAttrName, AView.Caption);
  ANode.Attributes.Add(sdxXLSXAttrSheetId, AView.Index + 1);
  WriteSheetView(AFileName, AView, ARels);
  ANode.Attributes.Add(sdxXLSXAttrRId, ARels.GetRelationshipId(dxUnixPathDelim + AFileName));
end;

{ TdxSpreadSheetXLSXWriterWorksheetContainersBuilder }

constructor TdxSpreadSheetXLSXWriterWorksheetContainersBuilder.Create(AOwner: TdxSpreadSheetXLSXWriter;
  AOwnerRels: TdxSpreadSheetXLSXWriterRels; const ATargetFileName: AnsiString; AView: TdxSpreadSheetTableView);
begin
  inherited Create(AOwner, AOwnerRels, ATargetFileName);
  FView := AView;
  FContainerBuildersMap := TDictionary<TClass, TdxSpreadSheetXLSXWriterCustomContainerBuilderClass>.Create;
  FContainerBuildersMap.Add(TdxSpreadSheetShapeContainer, TdxSpreadSheetXLSXWriterShapeContainerBuilder);
  FContainerBuildersMap.Add(TdxSpreadSheetPictureContainer, TdxSpreadSheetXLSXWriterPictureContainerBuilder);
end;

destructor TdxSpreadSheetXLSXWriterWorksheetContainersBuilder.Destroy;
begin
  FreeAndNil(FContainerBuildersMap);
  inherited Destroy;
end;

procedure TdxSpreadSheetXLSXWriterWorksheetContainersBuilder.Execute;
var
  ADoc: TdxXMLDocument;
  ANode: TdxXMLNode;
  ARels: TdxSpreadSheetXLSXWriterRels;
begin
  ADoc := TdxSpreadSheetXMLDocument.Create(nil);
  ARels := TdxSpreadSheetXLSXWriterRels.Create(nil);
  try
    ANode := ADoc.Root.AddChild(sdxXLSXNodeDrawingHeader);
    ANode.Attributes.SetValue(sdxXLSXAttrXMLNSXDR, sdxXLSXDrawingNamespaceXDR);
    ANode.Attributes.SetValue(sdxXLSXAttrXMLNSA, sdxXLSXDrawingNamespace);
    WriteContainers(ANode, ARels);

    RegisterFile(TargetFileName, sdxXLSXDrawingContentType, sdxXLSXDrawingRelationship, OwnerRels);
    WriteXML(TargetFileNameRels, ARels);
    WriteXML(TargetFileName, ADoc);
  finally
    ARels.Free;
    ADoc.Free;
  end;
end;

function TdxSpreadSheetXLSXWriterWorksheetContainersBuilder.GetContainerBuilderClass(
  AContainer: TdxSpreadSheetContainer): TdxSpreadSheetXLSXWriterCustomContainerBuilderClass;
begin
  Result := ContainerBuildersMap[AContainer.ClassType];
  if Result = nil then
    Result := TdxSpreadSheetXLSXWriterCustomContainerBuilder;
end;

procedure TdxSpreadSheetXLSXWriterWorksheetContainersBuilder.WriteContainer(
  ANode: TdxXMLNode; AContainer: TdxSpreadSheetContainer; ARels: TdxSpreadSheetXLSXWriterRels);
begin
  ExecuteSubTask(GetContainerBuilderClass(AContainer).Create(AContainer, Owner, ARels, ANode));
end;

procedure TdxSpreadSheetXLSXWriterWorksheetContainersBuilder.WriteContainers(
  ANode: TdxXMLNode; ARels: TdxSpreadSheetXLSXWriterRels);
var
  I: Integer;
begin
  for I := 0 to View.Containers.Count - 1 do
    WriteContainer(ANode, View.Containers[I], ARels);
end;

{ TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder }

constructor TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder.Create(AOwner: TdxSpreadSheetXLSXWriter;
  AOwnerRels: TdxSpreadSheetXLSXWriterRels; const ATargetFileName: AnsiString; AView: TdxSpreadSheetTableView);
begin
  inherited Create(AOwner, AOwnerRels, ATargetFileName);
  FView := AView;
end;

procedure TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder.Execute;
var
  ADoc: TdxXMLDocument;
  ANode: TdxXMLNode;
  ARels: TdxSpreadSheetXLSXWriterRels;
begin
  ADoc := TdxSpreadSheetXMLDocument.Create(nil);
  ARels := TdxSpreadSheetXLSXWriterRels.Create(nil);
  try
    ANode := ADoc.Root.AddChild(sdxXLSXNodeWorksheet);
    ANode.Attributes.Add(sdxXLSXAttrXMLNS, sdxXLSXWorkbookNameSpace);
    ANode.Attributes.Add(sdxXLSXAttrXMLNSR, sdxXLSXCommonRelationshipPath);
    WriteContent(ANode, ARels);

    RegisterFile(TargetFileName, sdxXLSXWorksheetContentType, sdxXLSXWorksheetRelationship, OwnerRels);
    WriteXML(TargetFileNameRels, ARels);
    WriteXML(TargetFileName, ADoc);
  finally
    ARels.Free;
    ADoc.Free;
  end;
end;

function TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder.CheckAreaBounds(const R: TRect): TRect;
begin
  Result := R;
  Result.Right := Min(Result.Right, dxXLSXMaxColumnIndex);
  Result.Bottom := Min(Result.Bottom, dxXLSXMaxRowIndex);
end;

function TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder.ConvertRowHeight(const AValue: Integer): Double;
begin
  Result := AValue * 75 / 100;
end;

function TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder.EncodeFloat(const AValue: Double): TdxUnicodeString;
begin
  Result := FloatToStr(AValue, Owner.InvariantFormatSettings);
end;

procedure TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder.WriteCell(ANode: TdxXMLNode; ACell: TdxSpreadSheetCell);
const
  Map: array[TdxSpreadSheetCellDataType] of TdxSpreadSheetXLSXCellType = (
    sxctUnknown, sxctBoolean, sxctError, sxctFloat, sxctFloat, sxctFloat, sxctFloat, sxctSharedString, sxctFormula
  );
var
  ADataType: TdxSpreadSheetXLSXCellType;
  ASubNode: TdxXMLNode;
begin
  ANode.Attributes.Add(sdxXLSXAttrStyleIndex, AddResource(ACell.Style.Handle, Owner.CellStyles));

  ADataType := Map[ACell.DataType];
  if ADataType <> sxctUnknown then
  begin
    ANode.Attributes.Add(sdxXLSXAttrCellType, dxXLSXCellDataTypeNames[ADataType]);
    case ADataType of
      sxctBoolean:
        ANode.AddChild(sdxXLSXNodeCellValue).Text := TdxXMLHelper.EncodeBoolean(ACell.AsBoolean);

      sxctError:
        ANode.AddChild(sdxXLSXNodeCellValue).TextAsUnicodeString := dxSpreadSheetErrorCodeToString(ACell.AsError);

      sxctFloat:
        ANode.AddChild(sdxXLSXNodeCellValue).TextAsUnicodeString := EncodeFloat(ACell.AsFloat);

      sxctSharedString:
        ANode.AddChild(sdxXLSXNodeCellValue).TextAsUnicodeString := IntToStr(AddResource(ACell.AsSharedString, Owner.SharedStrings));

      sxctFormula:
        begin
          ASubNode := ANode.AddChild(sdxXLSXNodeCellFunction);
          ASubNode.TextAsUnicodeString := Copy(ACell.AsFormula.AsText, 2, MaxInt);
          if ACell.AsFormula.IsArrayFormula then
            ASubNode.Attributes.Add(sdxXLSXAttrCellType, sdxXLSXValueArray);
        end;

    else
      DoError(sdxErrorInternal, [sMsgWrongDataType], ssmtError);
    end;
  end;
end;

procedure TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder.WriteColumns(ANode: TdxXMLNode; const ADimension: TRect);

  procedure DoAddColumnInfo(AStartIndex: Integer; AColumn: TdxSpreadSheetTableColumnAccess);
  var
    AColumnNode: TdxXMLNode;
  begin
    AColumnNode := ANode.AddChild(sdxXLSXNodeColumn);
    AColumnNode.Attributes.Add(sdxXLSXAttrMax, AColumn.Index + 1);
    AColumnNode.Attributes.Add(sdxXLSXAttrMin, AStartIndex + 1);
    AColumnNode.Attributes.Add(sdxXLSXAttrStyle, AddResource(AColumn.Style.Handle, Owner.CellStyles));
    AColumnNode.Attributes.Add(sdxXLSXAttrWidth, EncodeFloat(Owner.ColumnWidthHelper.PixelsToWidth(AColumn.Size)));
    if AColumn.IsCustomSize then
      AColumnNode.Attributes.Add(sdxXLSXAttrCustomWidth, True);
    if not AColumn.Visible then
      AColumnNode.Attributes.Add(sdxXLSXAttrHidden, True);
  end;

var
  AColumn: TdxSpreadSheetTableColumnAccess;
  APrevColumn: TdxSpreadSheetTableColumnAccess;
  AStartIndex: Integer;
  I: Integer;
begin
  AStartIndex := 0;
  APrevColumn := nil;
  for I := ADimension.Left to ADimension.Right do
  begin
    AColumn := TdxSpreadSheetTableColumnAccess(View.Columns.Items[I]);
    if AColumn <> nil then
    begin
      if APrevColumn = nil then
        AStartIndex := AColumn.Index
      else
        if (AColumn.Style.Handle <> APrevColumn.Style.Handle) or (AColumn.Size <> APrevColumn.Size) or
          (AColumn.Visible <> APrevColumn.Visible) then
        begin
          DoAddColumnInfo(AStartIndex, APrevColumn);
          AStartIndex := AColumn.Index;
        end;

      APrevColumn := AColumn;
    end;
  end;
  if APrevColumn <> nil then
    DoAddColumnInfo(AStartIndex, APrevColumn);
end;

procedure TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder.WriteContainers(
  ANode: TdxXMLNode; ARels: TdxSpreadSheetXLSXWriterRels);
var
  AFileName: AnsiString;
begin
  AFileName := Format(sdxXLSXFileTemplateDrawing, [View.Index + 1]);
  ExecuteSubTask(TdxSpreadSheetXLSXWriterWorksheetContainersBuilder.Create(Owner, ARels, AFileName, View));
  ANode.Attributes.SetValue(sdxXLSXAttrRId, ARels.GetRelationshipId(dxUnixPathDelim + AFileName));
end;

procedure TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder.WriteContent(
  ANode: TdxXMLNode; ARels: TdxSpreadSheetXLSXWriterRels);
var
  AChildNode: TdxXMLNode;
  ADimension: TRect;
begin
  ADimension := CheckAreaBounds(View.Dimensions);

  AChildNode := ANode.AddChild(sdxXLSXNodeDimension);
  AChildNode.Attributes.Add(sdxXLSXAttrRef, TdxSpreadSheetXLSXCellRef.EncodeRange(ADimension));

  WriteViewProperties(ANode.AddChild(sdxXLSXNodeSheetsView).AddChild(sdxXLSXNodeSheetView));
  WriteProperties(ANode.AddChild(sdxXLSXNodeSheetFormatPr));

  AChildNode := ANode.AddChild(sdxXLSXNodeColumns);
  WriteColumns(AChildNode, ADimension);
  if (AChildNode.Attributes.Count = 0) and (AChildNode.Count = 0) then
    AChildNode.Free;

  WriteRows(ANode.AddChild(sdxXLSXNodeSheetData), ADimension);
  if View.Options.Protected then
    WriteViewProtection(ANode.AddChild(sdxXLSXNodeSheetProtection));

  if View.MergedCells.Count > 0 then
    WriteMergedCells(ANode.AddChild(sdxXLSXNodeMergeCells), View.MergedCells);
  if View.Containers.Count > 0 then
    WriteContainers(ANode.AddChild(sdxXLSXNodeDrawing), ARels);
end;

procedure TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder.WriteFixedPaneProperties(ANode: TdxXMLNode);
begin
  ANode.Attributes.Add(sdxXLSXAttrState, sdxXLSXValueFrozen);
  ANode.Attributes.Add(sdxXLSXAttrActivePane, sdxXLSXValueBottomRight);
  ANode.Attributes.Add(sdxXLSXAttrTopLeftCell, TdxSpreadSheetXLSXCellRef.Encode(Max(0, View.FrozenRow) + 1, Max(0, View.FrozenColumn) + 1));
  if View.FrozenColumn >= 0 then
    ANode.Attributes.Add(sdxXLSXAttrSplitX, View.FrozenColumn + 1);
  if View.FrozenRow >= 0 then
    ANode.Attributes.Add(sdxXLSXAttrSplitY, View.FrozenRow + 1);
end;

procedure TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder.WriteMergedCells(
  ANode: TdxXMLNode; AMergedCells: TdxSpreadSheetMergedCellList);
var
  AItem: TdxSpreadSheetMergedCell;
begin
  AItem := AMergedCells.First;
  while AItem <> nil do
  begin
    ANode.AddChild(sdxXLSXNodeMergeCell).Attributes.Add(sdxXLSXAttrRef, TdxSpreadSheetXLSXCellRef.EncodeRange(AItem.Area));
    AItem := TdxSpreadSheetMergedCell(AItem.Next);
  end;
end;

procedure TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder.WriteProperties(ANode: TdxXMLNode);
begin
  ANode.Attributes.SetValueAsFloat(sdxXLSXAttrDefaultColumnWidth, Owner.ColumnWidthHelper.PixelsToWidth(View.Columns.DefaultSize));
  ANode.Attributes.SetValueAsBoolean(sdxXLSXAttrCustomHeight, True);
  ANode.Attributes.SetValueAsFloat(sdxXLSXAttrDefaultRowHeight, ConvertRowHeight(View.Rows.DefaultSize));
end;

procedure TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder.WriteRows(ANode: TdxXMLNode; const ADimension: TRect);
var
  ACell: TdxSpreadSheetCell;
  ACellNode: TdxXMLNode;
  ARow: TdxSpreadSheetTableRowAccess;
  ARowNode: TdxXMLNode;
  R, C: Integer;
begin
  Owner.ProgressHelper.BeginStage(cxRectHeight(ADimension) + 1);
  try
    for R := ADimension.Top to ADimension.Bottom do
    begin
      ARow := TdxSpreadSheetTableRowAccess(View.Rows.Items[R]);
      if ARow <> nil then
      begin
        ARowNode := ANode.AddChild(sdxXLSXNodeRow);
        ARowNode.Attributes.Add(sdxXLSXAttrRowIndex, R + 1);
        if not ARow.Visible then
          ARowNode.Attributes.Add(sdxXLSXAttrHidden, True);

        if not ARow.DefaultSize then
        begin
          if ARow.IsCustomSize then
            ARowNode.Attributes.Add(sdxXLSXAttrCustomHeight, True);
          ARowNode.Attributes.SetValueAsFloat(sdxXLSXAttrRowHeight, ConvertRowHeight(ARow.Size));
        end;

        if not ARow.Style.IsDefault then
        begin
          ARowNode.Attributes.Add(sdxXLSXAttrCustomFormat, True);
          ARowNode.Attributes.Add(sdxXLSXAttrStyleIndex, AddResource(ARow.Style.Handle, Owner.CellStyles));
        end;

        for C := ADimension.Left to ADimension.Right do
        begin
          ACell := ARow.Cells[C];
          if ACell <> nil then
          begin
            ACellNode := ARowNode.AddChild('c');
            ACellNode.Attributes.Add(sdxXLSXAttrCellColumn, TdxSpreadSheetXLSXCellRef.Encode(R, C));
            WriteCell(ACellNode, ACell);
          end;
        end;
      end;
      Owner.ProgressHelper.NextTask;
    end;
  finally
    Owner.ProgressHelper.EndStage;
  end;
end;

procedure TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder.WriteSelection(
  ANode: TdxXMLNode; ASelection: TdxSpreadSheetTableViewSelection);

  function EncodeSelection: TdxUnicodeString;
  var
    I: Integer;
    R: TRect;
  begin
    Result := '';
    for I := 0 to ASelection.Count - 1 do
    begin
      R := CheckAreaBounds(ASelection.Items[I].Rect);
      if I > 0 then
        Result := Result + ' ';
      if (R.Left = R.Right) and (R.Top = R.Bottom) then
        Result := Result + TdxSpreadSheetXLSXCellRef.Encode(R.TopLeft)
      else
        Result := Result + TdxSpreadSheetXLSXCellRef.EncodeRange(R);
    end;
  end;

begin
  ANode.Attributes.Add(sdxXLSXAttrSqRef, EncodeSelection);
  if InRange(ASelection.FocusedRow, 0, dxXLSXMaxRowIndex) and InRange(ASelection.FocusedColumn, 0, dxXLSXMaxColumnIndex) then
    ANode.Attributes.Add(sdxXLSXAttrActiveCell, TdxSpreadSheetXLSXCellRef.Encode(ASelection.FocusedRow, ASelection.FocusedColumn));
end;

procedure TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder.WriteViewProperties(ANode: TdxXMLNode);
var
  AZoomFactor: Integer;
begin
  ANode.Attributes.Add(sdxXLSXAttrWorkbookViewId, 0);

  AZoomFactor := Max(Min(View.Options.ZoomFactor, MaxZoomFactor), MinZoomFactor);
  if AZoomFactor <> 100 then
  begin
    ANode.Attributes.Add(sdxXLSXAttrZoomScale, AZoomFactor);
    ANode.Attributes.Add(sdxXLSXAttrZoomScaleNormal, AZoomFactor);
  end;

  if View.Active then
    ANode.Attributes.Add(sdxXLSXAttrTabSelected, True);

  ANode.Attributes.Add(sdxXLSXAttrZeroValues, View.Options.ActualZeroValues);
  ANode.Attributes.Add(sdxXLSXAttrShowFormulas, View.Options.ActualShowFormulas);
  ANode.Attributes.Add(sdxXLSXAttrGridLines, View.Options.ActualGridLines);

  if (View.FrozenColumn >= 0) or (View.FrozenRow >= 0) then
    WriteFixedPaneProperties(ANode.AddChild(sdxXLSXNodePane));

  if View.Selection.Count > 0 then
    WriteSelection(ANode.AddChild(sdxXLSXNodeSelection), View.Selection);
end;

procedure TdxSpreadSheetXLSXWriterWorksheetTableViewBuilder.WriteViewProtection(ANode: TdxXMLNode);
begin
  ANode.Attributes.Add(sdxXLSXAttrSheet, True);
  ANode.Attributes.Add(sdxXLSXAttrformatCells, False);
  ANode.Attributes.Add(sdxXLSXAttrFormatColumns, False);
  ANode.Attributes.Add(sdxXLSXAttrFormatRows, False);
end;

end.
