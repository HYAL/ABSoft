{.$DEFINE XLSDRAWING}

{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}

unit dxSpreadSheetFormatXLS;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  RTLConsts, Windows, Generics.Collections, Classes, Types, SysUtils, Math, Graphics, dxCore, dxOLEDocument, cxClasses,
  cxVariants, dxSpreadSheetCore, dxCoreClasses, dxSpreadSheetTypes, dxSpreadSheetClasses, dxSpreadSheetFormatXLSTypes,
  dxSpreadSheetUtils, dxSpreadSheetGraphics, cxGraphics, dxSpreadSheetFormulas, dxSpreadSheetFunctions, dxGDIPlusClasses;

type
  TdxSpreadSheetXLSReader = class;
  TdxSpreadSheetXLSWriter = class;

  { TdxSpreadSheetXLSFormat }

  TdxSpreadSheetXLSFormat = class(TdxSpreadSheetCustomFormat)
  public
    class function CanReadFromStream(AStream: TStream): Boolean; override;
    class function GetExt: string; override;
    class function GetReader: TdxSpreadSheetCustomReaderClass; override;
    class function GetWriter: TdxSpreadSheetCustomWriterClass; override;
  end;

  { TdxXLSRecordReader }

  TdxXLSRecordReader = class(TMemoryStream)
  private
    FCurrentPart: Integer;
    FOwner: TdxSpreadSheetXLSReader;
    FParts: TList;
    FRecordID: Word;
    FSize: Integer;
    FStartPosition: Integer;
    function GetSource: TStream; inline;
  protected
    procedure AddPart(ASize: Word);

    property CurrentPart: Integer read FCurrentPart write FCurrentPart;
    property Source: TStream read GetSource;
  public
    constructor Create(AOwner: TdxSpreadSheetXLSReader);
    destructor Destroy; override;
    procedure Initialize;
    procedure PrepareRecord;
    procedure SkipRecord;

    property Owner: TdxSpreadSheetXLSReader read FOwner;
    property Parts: TList read FParts;
    property RecordID: Word read FRecordID;
    property StartPosition: Integer read FStartPosition;
    property Size: Integer read FSize;
  end;

  { TdxXLSReader }

  TdxXLSReader = class(TcxReader)
  private
    FXLSRecord: TdxXLSRecordReader;
    function GetByteByIndex(AIndex: Integer): Byte; inline;
    function GetIntegerByIndex(AIndex: Integer): Integer; inline;
    function GetWordByIndex(AIndex: Integer): Word; inline;
  protected
    procedure CheckRange(const ASize: Integer; var AFirstSize, ASecondSize, APosition: Integer); inline;
  public
    constructor Create(ARecord: TdxXLSRecordReader);
    function XLS_ReadREF: TRect; inline;
    function XLS_ReadRKValue(AOffset: Integer): Double;
    function XLS_ReadSimpleREF: TRect;
    function XLS_ReadSimpleString(AOffset: Integer): TdxUnicodeString; inline;
    function XLS_ReadString: TdxSpreadSheetSharedString; overload; inline;
    function XLS_ReadString(const AOptions, ALength: Integer): TdxUnicodeString; overload; inline;

    property Bytes[Index: Integer]: Byte read GetByteByIndex;
    property Integers[Index: Integer]: Integer read GetIntegerByIndex;
    property Words[Index: Integer]: Word read GetWordByIndex;
    property XLSRecord: TdxXLSRecordReader read FXLSRecord;
  end;

  TdxXLSMethod = procedure of object;

  { TdxSpreadSheetXLSReader }

  TdxSpreadSheetXLSReader = class(TdxSpreadSheetCustomReader)
  private
    FActiveTabIndex: Integer;
    FColumnWidthHelper: TdxSpreadSheetExcelColumnWidthHelper;
    FCommonDataReady: Boolean;
    FContainers: TDictionary<Word, TdxSpreadSheetContainer>;
    FCurrentSheet: TdxSpreadSheetTableView;
    FData: TStream;
    FDocument: TdxOLEDocument;
    FExternalNames: TStringList;
    FFirstTabIndex: Integer;
    FFonts: TcxObjectList;
    FFormats: TList<TdxSpreadSheetFormatHandle>;
    FHasFrozenPanes: Boolean;
    FImages: TList<TdxSmartImage>;
    FNames: TStringList;
    FProgressValue: Integer;
    FReader: TdxXLSReader;
    FReaders: TList<TdxXLSMethod>;
    FRecordReader: TdxXLSRecordReader;
    FSheetOffset: TList<Integer>;
    FSheetIndexes: array of Integer;
    FStrings: TcxObjectList;
    FStyles: TcxObjectList;
    //
    procedure ReadBlankCell;
    procedure ReadBlankCells;
    procedure ReadBOF;
    procedure ReadBoolErrorCell;
    procedure ReadBoundSheet;
    procedure ReadCalcCount;
    procedure ReadCalcMode;
    procedure ReadColumnFormatInformation;
    procedure ReadDataFormat;
    procedure ReadDateSystem;
    procedure ReadDefaultColumnWidth;
    procedure ReadDefaultRowHeight;
    procedure ReadEOF;
    procedure ReadExternName;
    procedure ReadExternSheet;
    procedure ReadFont;
    procedure ReadFormulaArray;
    procedure ReadFormulas(ATop, ALeft, ABottom, ARight, AOffset: Integer);
    procedure ReadFormulaCell;
    procedure ReadGridlines;
    procedure ReadIteration;
    procedure ReadLabelCell;
    procedure ReadLabelSSTCell;
    procedure ReadMergedCells;
    procedure ReadMulRKCells;
    procedure ReadMSODrawing;
    procedure ReadMSODrawingGroup;
    procedure ReadName;
    procedure ReadNumberCell;
    procedure ReadPanes;
    procedure ReadPalette;
    procedure ReadPrintHeaders;
    procedure ReadProtection;
    procedure ReadRowFormatInformation;
    procedure ReadRKCell;
    procedure ReadSelection;
    procedure ReadSharedFormula;
    procedure ReadSharedStringTable;
    procedure ReadWindow1Information;
    procedure ReadWindow2Information;
    procedure ReadXFRecord;
    procedure ReadZoomFactor;
    //
    procedure SetProgressValue(AValue: Integer);
  protected
    Palette: array of TColor;
    function CreateCell: TdxSpreadSheetCell; overload;
    function CreateCell(ARow, AColumn, AStyleIndex: Integer): TdxSpreadSheetCell; overload;
    function CreateProgressHelper: TdxSpreadSheetCustomFilerProgressHelper; override;
    function GetColor(AIndex: Integer): TColor; inline;
    function GetFillStyle(AXLSFillStyle: Byte): TdxSpreadSheetCellFillStyle; inline;
    function GetFont(AIndex: Integer): TdxSpreadSheetFontHandle; inline;
    function GetFormat(AIndex: Integer): TdxSpreadSheetFormatHandle; inline;
    function GetStyle(AIndex: Integer): TdxXLSCellStyle; inline;
    function IsEOF: Boolean; inline;
    //
    procedure PrepareCommonData;
    procedure PrepareFonts;
    procedure PrepareFormattedSharedString(AString: TdxSpreadSheetFormattedSharedString);
    procedure PrepareSharedStrings;
    procedure PrepareStyle(AXLSStyle: TdxXLSCellStyle);
    procedure PrepareStyles;
    //
    procedure RegisterReader(ARecordID: Word; AReader: TdxXLSMethod);
    procedure RegisterReaders; virtual;
    procedure UpdateProgrees;
  public
    constructor Create(AOwner: TdxCustomSpreadSheet; AStream: TStream); override;
    destructor Destroy; override;
    procedure AddImage(AImage: TdxSmartImage);
    function GetExternalName(AIndex: Integer; var AInfo: TdxSpreadSheetFunctionInfo): TdxUnicodeString; inline;
    function GetName(AIndex: Integer; var AInfo: TdxSpreadSheetFunctionInfo): TdxUnicodeString; inline;
    function GetSheet(AIndex: Integer): TdxSpreadSheetTableView; inline;
    procedure ReadData; override;

    property ActiveTabIndex: Integer read FActiveTabIndex write FActiveTabIndex;
    property ColumnWidthHelper: TdxSpreadSheetExcelColumnWidthHelper read FColumnWidthHelper;
    property CommonDataReady: Boolean read FCommonDataReady write FCommonDataReady;
    property Containers: TDictionary<Word, TdxSpreadSheetContainer> read FContainers;
    property CurrentSheet: TdxSpreadSheetTableView read FCurrentSheet;
    property Data: TStream read FData;
    property Document: TdxOLEDocument read FDocument;
    property ExternalNames: TStringList read FExternalNames;
    property FirstTabIndex: Integer read FFirstTabIndex write FFirstTabIndex;
    property Fonts: TcxObjectList read FFonts;
    property Formats: TList<TdxSpreadSheetFormatHandle> read FFormats;
    property HasFrozenPanes: Boolean read FHasFrozenPanes write FHasFrozenPanes;
    property Images: TList<TdxSmartImage> read FImages;
    property Names: TStringList read FNames;
    property ProgressValue: Integer read FProgressValue write SetProgressValue;
    property Reader: TdxXLSReader read FReader;
    property Readers: TList<TdxXLSMethod> read FReaders;
    property RecordReader: TdxXLSRecordReader read FRecordReader;
    property SheetOffset: TList<Integer> read FSheetOffset;
    property Strings: TcxObjectList read FStrings;
    property Styles: TcxObjectList read FStyles;
  end;

  TdxSpreadSheetXLSWriter = class
  end;

function XLSErrorToErrorCode(ACode: Byte): TdxSpreadSheetFormulaErrorCode;

resourcestring
  sdxInvalidStreamVersion = 'Invalid stream version';
  sdxInvalidStreamFormat = 'Invalid data format';

const
  FillStyles: array[0..17] of Byte = ($01, $03, $02, $04, $11, $12,
    $05, $06, $07, $08, $09, $0A, $0B, $0C, $0D, $0E, $10, $0F);

implementation

uses
  dxSpreadSheetFormatXLSFormulas, {$IFDEF XLSDRAWING} dxSpreadSheetFormatXLSDrawing, {$ENDIF}dxSpreadSheetCoreHelpers;

function XLSErrorToErrorCode(ACode: Byte): TdxSpreadSheetFormulaErrorCode;
begin
  Result := ecNone;
  case ACode of
    0:
      Result := ecNull;
    7:
      Result := ecDivByZero;
    15:
      Result := ecValue;
    23:
      Result := ecRefErr;
    29:
      Result := ecName;
    36:
      Result := ecNum;
    42:
      Result := ecNA;
  end;
end;

{ TdxSpreadSheetXLSXFormat }

class function TdxSpreadSheetXLSFormat.CanReadFromStream(AStream: TStream): Boolean;
begin
  Result := dxIsOLEStream(AStream);
end;

class function TdxSpreadSheetXLSFormat.GetExt: string;
begin
  Result := '.xls';
end;

class function TdxSpreadSheetXLSFormat.GetReader: TdxSpreadSheetCustomReaderClass;
begin
  Result := TdxSpreadSheetXLSReader;
end;

class function TdxSpreadSheetXLSFormat.GetWriter: TdxSpreadSheetCustomWriterClass;
begin
  Result := nil; // todo:
end;

{ TdxSpreadSheetXLSReader }

constructor TdxSpreadSheetXLSReader.Create(AOwner: TdxCustomSpreadSheet; AStream: TStream);
begin
  inherited Create(AOwner, AStream);
  FColumnWidthHelper := TdxSpreadSheetExcelColumnWidthHelper.Create;
  FFonts := TcxObjectList.Create;
  FFormats := TList<TdxSpreadSheetFormatHandle>.Create;
  FStyles := TcxObjectList.Create;
  FSheetOffset := TList<Integer>.Create;
  FStrings := TcxObjectList.Create;
  FNames := TStringList.Create;
  FExternalNames := TStringList.Create;
  FProgressValue := -1;
  FDocument := TdxOLEDocument.Create(AStream, dmReading);
  FData := FDocument.StreamByName('Workbook');
  FRecordReader := TdxXLSRecordReader.Create(Self);
  FReaders := TList<TdxXLSMethod>.Create;
  FReader := TdxXLSReader.Create(FRecordReader);
  FImages := TList<TdxSmartImage>.Create;
  FContainers := TDictionary<Word, TdxSpreadSheetContainer>.Create;
  Check(FData <> nil, sdxInvalidStreamFormat);
  SetLength(Palette, Length(dxExcelStandardColors));
  Move(dxExcelStandardColors[0], Palette[0], Length(dxExcelStandardColors) * SizeOf(TColor));
  RegisterReaders;
  ProgressValue := 0;
end;

destructor TdxSpreadSheetXLSReader.Destroy;
begin
  if CommonDataReady then
    Strings.Count := 0;
  FreeAndNil(FImages);
  FreeAndNil(FExternalNames);
  FreeAndNil(FNames);
  FreeAndNil(FColumnWidthHelper);
  FreeAndNil(FStrings);
  FreeAndNil(FStyles);
  FreeAndNil(FFonts);
  FreeAndNil(FSheetOffset);
  FreeAndNil(FDocument);
  FreeAndNil(FRecordReader);
  FreeAndNil(FReader);
  FreeAndNil(FReaders);
  FreeAndNil(FFormats);
  inherited Destroy;
end;

procedure TdxSpreadSheetXLSReader.AddImage(AImage: TdxSmartImage);
var
  AContainer: TdxSpreadSheetPictureContainer;
begin
  if AImage <> nil then
  begin
    AContainer := CurrentSheet.Containers.Add(TdxSpreadSheetPictureContainer) as TdxSpreadSheetPictureContainer;
    AContainer.Picture.Image := AImage;
    Containers.Add(Containers.Count, AContainer);
  end
  else
    Containers.Add(Containers.Count, nil);
end;

function TdxSpreadSheetXLSReader.GetExternalName(AIndex: Integer; var AInfo: TdxSpreadSheetFunctionInfo): TdxUnicodeString;
begin
  Result := '';
  AInfo := nil;
  if (AIndex >= 0) and (AIndex < ExternalNames.Count) then
  begin
    Result := FExternalNames[AIndex];
    AInfo := FExternalNames.Objects[AIndex] as TdxSpreadSheetFunctionInfo;
  end;
end;

function TdxSpreadSheetXLSReader.GetName(AIndex: Integer; var AInfo: TdxSpreadSheetFunctionInfo): TdxUnicodeString;
begin
  Result := '';
  AInfo := nil;
  if (AIndex >= 0) and (AIndex < Names.Count) then
  begin
    Result := FNames[AIndex];
    AInfo := FNames.Objects[AIndex] as TdxSpreadSheetFunctionInfo;
  end;
end;

function TdxSpreadSheetXLSReader.GetSheet(AIndex: Integer): TdxSpreadSheetTableView;
var
  APageIndex: Integer;
begin
  TdxSpreadSheetInvalidObject.AssignTo(Result);
  if AIndex < Length(FSheetIndexes) then
    APageIndex := FSheetIndexes[AIndex]
  else
    APageIndex := AIndex;
  if APageIndex < SpreadSheet.SheetCount then
    Result := TdxSpreadSheetTableView(SpreadSheet.Sheets[APageIndex])
end;

procedure TdxSpreadSheetXLSReader.ReadData;
var
  AReader: TdxXLSMethod;
begin
  while not IsEOF do
  begin
    AReader := nil;
    RecordReader.PrepareRecord;
    if RecordReader.RecordID < Readers.Count then
      AReader := Readers[RecordReader.RecordID];
    if Assigned(AReader) then
    begin
      RecordReader.Initialize;
      AReader;
    end
    else
      RecordReader.SkipRecord;
    UpdateProgrees;
  end;
end;

function TdxSpreadSheetXLSReader.CreateCell: TdxSpreadSheetCell;
begin
  Result := CreateCell(Reader.Words[0], Reader.Words[1], Reader.Words[2]);
end;

function TdxSpreadSheetXLSReader.CreateCell(ARow, AColumn, AStyleIndex: Integer): TdxSpreadSheetCell;
begin
  Result := CurrentSheet.CreateCell(ARow, AColumn);
  Result.Style.Handle := GetStyle(AStyleIndex).Handle;
end;

function TdxSpreadSheetXLSReader.CreateProgressHelper: TdxSpreadSheetCustomFilerProgressHelper;
begin
  Result := TdxSpreadSheetCustomFilerProgressHelper.Create(Self, 0);
end;

function TdxSpreadSheetXLSReader.GetColor(AIndex: Integer): TColor;
begin
  Dec(AIndex, 8);
  if (AIndex < 0) or (AIndex > High(Palette)) then
    Result := clDefault
  else
    Result := Palette[AIndex];
end;

function TdxSpreadSheetXLSReader.GetFillStyle(AXLSFillStyle: Byte): TdxSpreadSheetCellFillStyle;
var
  I: Byte;
begin
  Result := sscfsSolid;
  for I := 0 to High(FillStyles) do
    if FillStyles[I] = AXLSFillStyle then
    begin
      Result := TdxSpreadSheetCellFillStyle(I);
      Break;
    end;
end;

function TdxSpreadSheetXLSReader.GetFont(AIndex: Integer): TdxSpreadSheetFontHandle;
begin
  if AIndex >= Fonts.Count then
    AIndex := 0;
  Result := TdxXLSFont(Fonts.List[AIndex]).Handle;
end;

function TdxSpreadSheetXLSReader.GetFormat(AIndex: Integer): TdxSpreadSheetFormatHandle;
var
  I: Integer;
begin
  Result := CellStyles.Formats.PredefinedFormats.GetFormatHandleByID(AIndex);
  if Result <> nil then Exit;
  Result := CellStyles.Formats.PredefinedFormats.GetFormatHandleByID(0);
  for I := 0 to Formats.Count - 1 do
    if Formats[I].FormatCodeID = AIndex then
    begin
      Result := Formats[I];
      Break;
    end
end;

function TdxSpreadSheetXLSReader.GetStyle(AIndex: Integer): TdxXLSCellStyle;
begin
  if AIndex > Styles.Count then
    AIndex := 0;
  Result := TdxXLSCellStyle(Styles.List[AIndex]);
end;

function TdxSpreadSheetXLSReader.IsEOF: Boolean;
begin
  Result := Data.Position >= (Data.Size - 4);
end;

procedure TdxSpreadSheetXLSReader.PrepareCommonData;
begin
  PrepareFonts;
  PrepareStyles;
  PrepareSharedStrings;
  CommonDataReady := True;
end;

procedure TdxSpreadSheetXLSReader.PrepareFonts;
var
  I: Integer;
  AFont: TdxSpreadSheetFontHandle;
begin
  for I := 0 to Fonts.Count - 1 do
  begin
    TdxXLSFont(Fonts[I]).Color := GetColor(TdxXLSFont(Fonts[I]).Color);
    AFont := CellStyles.Fonts.CreateFont();
    TdxXLSFont(Fonts[I]).AssignTo(AFont);
    TdxXLSFont(Fonts[I]).Handle := AddFont(AFont);
  end;
  ColumnWidthHelper.Font := TdxXLSFont(Fonts[0]).Handle.GraphicObject;
end;

procedure TdxSpreadSheetXLSReader.PrepareFormattedSharedString(
  AString: TdxSpreadSheetFormattedSharedString);
var
  I: Integer;
  ARun: TdxSpreadSheetFormattedSharedStringRun;
begin
  for I := 0 to AString.Runs.Count - 1 do
  begin
    ARun := AString.Runs[I];
    ARun.StartIndex := ARun.StartIndex shr 16 + 1;
    ARun.FontHandle := GetFont(ARun.StartIndex and $FFFF);
  end;
end;

procedure TdxSpreadSheetXLSReader.PrepareSharedStrings;
var
  I: Integer;
  ASharedString: TdxSpreadSheetSharedString;
begin
  for I := 0 to Strings.Count - 1 do
  begin
    ASharedString := TdxSpreadSheetSharedString(Strings[I]);
    if ASharedString.ClassType = TdxSpreadSheetFormattedSharedString then
      PrepareFormattedSharedString(TdxSpreadSheetFormattedSharedString(ASharedString));
    TList(Strings).List[I] := StringTable.Add(ASharedString);
  end;
end;

 {

function TcxExcelFileReader.XLSFillStyleToSpreadSheetStyles(AFillStyle: Byte): TdxSpreadSheetCellFillStyle;
var
  I: Byte;
begin
  Result := fsSolid;
  for I := 0 to High(FillStyles) do
    if FillStyles[I] = ABrushStyle then
    begin
      Result := TcxSSFillStyle(I);
      Break;
    end;
end;


function FillStyleToXlsFillStyle(AStyle: TcxSSFillStyle; ABkColor, AFgColor: Word): Integer;
begin
  Result := 0;
  Inc(ABkColor, 8);
  Inc(AFgColor, 8);
  if AFgColor = $41 then
    AFgColor := $40;
  if not ((AStyle = fsSolid) and
   ((AFgColor = $40) or ((ABkColor in [$40, $41]) and (AFgColor = 9)))) then
  begin
    PWordArray(@Result)^[0] := FillStyles[Byte(AStyle)] shl 10;
    PWordArray(@Result)^[1] := (ABkColor and $7F shl 7) or (AFgColor and $7F);
  end;
end;

}

procedure TdxSpreadSheetXLSReader.PrepareStyle(AXLSStyle: TdxXLSCellStyle);
const
  BorderStyleMask: array[TcxBorder] of Integer = ($000F, $0F00, $00F0, $F000);
  BorderStyleOffset: array[TcxBorder] of Integer = (0, 8, 4, 12);
  XLSBorderStyleToSpreadSheetStyle: array[0..15] of TdxSpreadSheetCellBorderStyle = (
    sscbsDefault,	sscbsThin, sscbsMedium, sscbsDashed, sscbsDotted, sscbsThick,	sscbsDouble, sscbsHair,
    sscbsMediumDashed, sscbsDashDot, sscbsMediumDashDot, sscbsDashDotDot, sscbsMediumDashDotDot,
    sscbsSlantedDashDot, sscbsDefault, sscbsDefault
  );
var
  ABorder: TcxBorder;
  AMask: Integer;
  AStyle: TdxSpreadSheetCellStyleHandle;
  ABorders: TdxSpreadSheetBordersHandle;
  ABrush: TdxSpreadSheetBrushHandle;
begin
  if AXLSStyle.Font >= 4 then
    Dec(AXLSStyle.Font);
  AStyle := CellStyles.CreateStyle(GetFont(AXLSStyle.Font), GetFormat(AXLSStyle.Format));
  ABorders := CellStyles.Borders.CreateBorders;
  ABrush := CellStyles.Brushes.CreateBrush;
  //
  AStyle.States := [];
  if AXLSStyle.Flags and $01 = $01 then
    AStyle.States := AStyle.States + [csLocked];
  if AXLSStyle.Flags and $02 = $02 then
    AStyle.States := AStyle.States + [csHidden];
  if AXLSStyle.Indents and $0010 = $0010 then
    AStyle.States := AStyle.States + [csShrinkToFit];
  if AXLSStyle.Alignments and $0008 = $0008 then
    AStyle.States := AStyle.States + [csWordWrap];
  //
  AStyle.AlignHorzIndent := ColumnWidthHelper.SpacesNumberToPixels(AXLSStyle.Indents and $F);
  if AXLSStyle.Alignments and $7 >= 6 then
    AStyle.AlignHorz := ssahCenter
  else
    AStyle.AlignHorz := TdxSpreadSheetDataAlignHorz(AXLSStyle.Alignments and $7);
  //
  AStyle.AlignVert := TdxSpreadSheetDataAlignVert(AXLSStyle.Alignments shr 4 and $7);
  //
  for ABorder := bLeft to bBottom do
    ABorders.BorderStyle[ABorder] := XLSBorderStyleToSpreadSheetStyle[
      (AXLSStyle.BordersStyle and BorderStyleMask[ABorder]) shr BorderStyleOffset[ABorder]];
  ABorders.BorderColor[bLeft] := GetColor(AXLSStyle.LeftRightBorders and $007F);
  ABorders.BorderColor[bRight] := GetColor((AXLSStyle.LeftRightBorders and $3F80) shr 7);
  ABorders.BorderColor[bTop] := GetColor(AXLSStyle.TopBottomBordersAndFill and $007F);
  ABorders.BorderColor[bBottom] := GetColor((AXLSStyle.TopBottomBordersAndFill and $3F80) shr 7);
  //
  AMask := AXLSStyle.TopBottomBordersAndFill shr 26 and $FF;
  ABrush.Style := GetFillStyle(AMask);
  ABrush.BackgroundColor := clDefault;
  ABrush.ForegroundColor := clDefault;
  if AMask <> 0 then
  begin
    if ABrush.Style <> sscfsSolid then
    begin
      ABrush.BackgroundColor := GetColor(AXLSStyle.FillColors shr 7 and $7F);
      ABrush.ForegroundColor := GetColor(AXLSStyle.FillColors and $7F);
    end
    else
    begin
      ABrush.BackgroundColor := GetColor(AXLSStyle.FillColors and $7F);
      ABrush.ForegroundColor := GetColor(AXLSStyle.FillColors shr 7 and $7F);
    end;
  end;
  //
  AStyle.Brush := ABrush;
  AStyle.Borders := ABorders;
  AXLSStyle.Handle := CellStyles.AddStyle(AStyle);
  AXLSStyle.Handle.AddRef;
end;

procedure TdxSpreadSheetXLSReader.PrepareStyles;
var
  I: Integer;
begin
  for I := 0 to Styles.Count - 1 do
    PrepareStyle(GetStyle(I));
end;

procedure TdxSpreadSheetXLSReader.RegisterReader(ARecordID: Word; AReader: TdxXLSMethod);
begin
  FReaders.Count := Max(FReaders.Count, ARecordID + 1);
  FReaders[ARecordID] := AReader;
end;

procedure TdxSpreadSheetXLSReader.RegisterReaders;
begin
   RegisterReader(brcBOF, ReadBOF);
   RegisterReader(brcFORMAT, ReadDataFormat);
   RegisterReader(brc1904, ReadDateSystem);

   RegisterReader(brcBoundSheet, ReadBoundSheet);
   RegisterReader(brcPRINTGRIDLINES, ReadGridlines);
   RegisterReader(brcPrintHeaders, ReadPrintHeaders);
   RegisterReader(brcProtect, ReadProtection);
   RegisterReader(brcFont, ReadFont);
   RegisterReader(brcColInfo, ReadColumnFormatInformation);
   RegisterReader(brcROW, ReadRowFormatInformation);
   RegisterReader(brcDefColWidth, ReadDefaultColumnWidth);
   RegisterReader(brcDefaultRowHeight, ReadDefaultRowHeight);
   RegisterReader(brcSST, ReadSharedStringTable);
   RegisterReader(brcBlank, ReadBlankCell);
   RegisterReader(brcBoolErr, ReadBoolErrorCell);
   RegisterReader(brcMergeCells, ReadMergedCells);
   RegisterReader(brcLabelSST, ReadLabelSSTCell);
   RegisterReader(brcLabel, ReadLabelCell);
   RegisterReader(brcNumber, ReadNumberCell);
   RegisterReader(brcMulBlank, ReadBlankCells);
   RegisterReader(brcMulRk, ReadMulRKCells);
   RegisterReader(brcRK, ReadRKCell);
   RegisterReader(brcRString, ReadLabelCell);
   RegisterReader(brcArray, ReadFormulaArray);
   RegisterReader(brcSHRFMLA, ReadSharedFormula);
   RegisterReader(brcFORMULA, ReadFormulaCell);
   RegisterReader(brcName, ReadName);
   RegisterReader(brcEXTERNNAME, ReadExternName);
   RegisterReader(brcExternSheet, ReadExternSheet);
   RegisterReader(brcXF, ReadXFRecord);
   RegisterReader(brcPALETTE, ReadPalette);
   RegisterReader(brcEOF, ReadEOF);
   RegisterReader(brcScl, ReadZoomFactor);
   RegisterReader(brcSelection, ReadSelection);
//     RegisterReader(brcIndex, ReadForFile(@ABiffRecHeader^.RecData);
   RegisterReader(brcPANE, ReadPanes);
   RegisterReader(brcWindow1, ReadWindow1Information);
   RegisterReader(brcWindow2, ReadWindow2Information);

   RegisterReader(brcCALCCOUNT, ReadCalcCount);
   RegisterReader(brcCALCMODE, ReadCalcMode);
   RegisterReader(brcITERATION, ReadIteration);

   //
   RegisterReader(brcMSODRAWING, ReadMSODrawing);
   RegisterReader(brcMSODRAWINGGROUP, ReadMSODrawingGroup);
end;

procedure TdxSpreadSheetXLSReader.UpdateProgrees;
begin
  ProgressValue := MulDiv(Data.Position, 100, Data.Size);
end;

procedure TdxSpreadSheetXLSReader.ReadBlankCell;
begin
  CreateCell.Clear;
end;

procedure TdxSpreadSheetXLSReader.ReadBlankCells;
var
  AIndex, ARow, AStartColumn: Integer;
begin
  ARow := Reader.Words[0];
  AStartColumn := Reader.Words[1];
  for AIndex := 0 to RecordReader.Size div 2 - 4 do
    CreateCell(ARow, AIndex + AStartColumn, Reader.Words[2 + AIndex]).Clear;
end;

procedure TdxSpreadSheetXLSReader.ReadBOF;
var
  V: Word;
  AIndex: Integer;
begin
  V := Reader.ReadWord;
  Check(V = $600, sdxInvalidStreamVersion);
  //todo: need skip chart
  FCurrentSheet := nil;
  if Reader.Words[1] = $10 then
  begin
    AIndex := SheetOffset.IndexOf(RecordReader.StartPosition);
    if AIndex >= 0 then
      FCurrentSheet := SpreadSheet.Sheets[AIndex] as TdxSpreadSheetTableView;
    if not CommonDataReady then
      PrepareCommonData;
   end;
end;

procedure TdxSpreadSheetXLSReader.ReadBoolErrorCell;
var
  ACell: TdxSpreadSheetCell;
begin
  ACell := CreateCell;
  if Reader.Bytes[7] = 0 then
    ACell.AsBoolean := Boolean(Reader.Bytes[6])
  else
    ACell.AsError :=  XLSErrorToErrorCode(Reader.Bytes[6]);
end;

procedure TdxSpreadSheetXLSReader.ReadBoundSheet;
var
  AFlag: Byte;
  AView: TdxSpreadSheetTableView;
begin
  AFlag := (Reader.Words[2] shr 8) and 3;
  if not (AFlag in [0, 1]) then Exit;
  SheetOffset.Add(Reader.Integers[0]);
  if AFlag in [0, 1] then
  begin
    AView := AddTableView(Reader.XLS_ReadSimpleString(6));
    AView.Visible := not (Reader.Words[2] in [1, 2]);
  end;
end;

procedure TdxSpreadSheetXLSReader.ReadCalcCount;
begin
  SpreadSheet.OptionsBehavior.IterativeCalculationMaxCount := Reader.ReadWord;
end;

procedure TdxSpreadSheetXLSReader.ReadCalcMode;
begin
  SpreadSheet.OptionsBehavior.AutomaticCalculation := Reader.ReadWord = 1;
end;

procedure TdxSpreadSheetXLSReader.ReadColumnFormatInformation;
var
  AIndex, AOptions: Word;
  AColumn: TdxSpreadSheetTableColumn;
begin
  AOptions := Reader.Words[4];
  for AIndex := Reader.Words[0] to Reader.Words[1] do
  begin
    AColumn := CurrentSheet.Columns.CreateItem(AIndex);
    AColumn.Size := ColumnWidthHelper.CharsNumberToPixels(Reader.Words[2] / 256) - 5;
    AColumn.Style.Handle := GetStyle(Reader.Words[3]).Handle;
    AColumn.Visible := AOptions and $01 = 0;
  end;
end;

procedure TdxSpreadSheetXLSReader.ReadDataFormat;
var
  AValue: AnsiString;
begin
  if Reader.Words[0] > $31 then
  begin
    SetLength(AValue, Reader.Bytes[2]);
    Move(PByteArray(RecordReader.Memory)^[3 + 2], AValue[1], Length(AValue));
    Formats.Add(CellStyles.Formats.AddFormat(dxAnsiStringToString(AValue), Reader.Words[0]));
  end;
end;

procedure TdxSpreadSheetXLSReader.ReadDateSystem;
begin
  SpreadSheet.OptionsView.DateTimeSystem := TdxSpreadSheetDataTimeSystem(Boolean(Reader.ReadWord));
end;

procedure TdxSpreadSheetXLSReader.ReadDefaultColumnWidth;
begin
  CurrentSheet.Options.DefaultColumnWidth := 3 + ColumnWidthHelper.CharsNumberToPixels(Reader.Words[0]);
end;

procedure TdxSpreadSheetXLSReader.ReadDefaultRowHeight;
begin
  CurrentSheet.Options.DefaultRowHeight := Round(Reader.Words[1] / 12.7) - 3; //todo: need use special code
end;

procedure TdxSpreadSheetXLSReader.ReadEOF;
begin
  FCurrentSheet := nil;
end;

procedure TdxSpreadSheetXLSReader.ReadExternSheet;
var
  I: Integer;
begin
  SetLength(FSheetIndexes, Reader.ReadWord);
  for I := 0 to Length(FSheetIndexes) - 1 do
    FSheetIndexes[I] := Reader.Words[I * 3 + 2];
end;

procedure TdxSpreadSheetXLSReader.ReadExternName;
var
  AOptions, ASize: Word;
  AName, ANameNoPrefix: TdxUnicodeString;
  AInfo: TdxSpreadSheetFunctionInfo;
  ADefinedName: TdxSpreadSheetDefinedName;
const
  XLSFuncPrefix = '_xlfn.';
begin
  AOptions := Reader.ReadWord;
  RecordReader.Position := 7;
  AName := Reader.XLS_ReadString(Reader.ReadByte, Reader.Bytes[6]);
  ASize := Reader.ReadWord;
  if ASize < 5 then
  begin
    AInfo := dxSpreadSheetFunctionsRepository.GetInfoByName(AName);
    if (AInfo = nil) and (Pos(XLSFuncPrefix, AName) = 1) then
    begin
      ANameNoPrefix := Copy(AName, Length(XLSFuncPrefix) + 1, MaxInt);
      AInfo := dxSpreadSheetFunctionsRepository.GetInfoByName(ANameNoPrefix);
      if AInfo <> nil then
        AName := ANameNoPrefix;
    end;
    if AInfo = nil then
      AInfo := dxSpreadSheetFunctionsRepository.AddUnknown(AName);
    ExternalNames.AddObject(AName, AInfo);
  end
  else
    if (AOptions = 0) or (AOptions = 2) then
    begin
      ExternalNames.Add(AName);
      ADefinedName := SpreadSheet.DefinedNames.Add(AName, '', nil);
      with TdxXLSFormulaReader.Create(Self) do
      try
        ReadName(ADefinedName, ASize, @PByteArray(RecordReader.Memory)^[RecordReader.Position]);
      finally
        Free;
      end;
    end
    else
      ExternalNames.Add(AName);
end;

procedure TdxSpreadSheetXLSReader.ReadFont;
var
  AFont: TdxXLSFont;
  AFlag: Integer;
begin
  AFont := TdxXLSFont.Create;
  AFont.Size := Round(Reader.Words[0] / 20);
  AFlag := Reader.Words[1];
  if (AFlag and $2) <> 0 then
    Include(AFont.Style, fsItalic);
  if (AFlag and $8) <> 0 then
    Include(AFont.Style, fsStrikeOut);
  AFlag := Reader.Words[3];
  if AFlag <> $190 then
    Include(AFont.Style, fsBold);
  if Reader.Bytes[10] <> 0 then
    Include(AFont.Style, fsUnderline);
  AFont.Charset := TFontCharset(Reader.Bytes[12]);
  if Integer(AFont.Charset) = 0 then
    AFont.Charset := DEFAULT_CHARSET;
  AFont.Name := Reader.XLS_ReadSimpleString(14);
  AFont.Color := Reader.Words[2];
  Fonts.Add(AFont);
end;

procedure TdxSpreadSheetXLSReader.ReadFormulaArray;
begin
  ReadFormulas(Reader.Words[0], Reader.Bytes[4], Reader.Words[1], Reader.Bytes[5], 12);
end;

procedure TdxSpreadSheetXLSReader.ReadFormulas(ATop, ALeft, ABottom, ARight, AOffset: Integer);
var
  ARow, AColumn: Integer;
  ACell: TdxSpreadSheetCell;
begin
  for ARow := ATop to ABottom do
  begin
    for AColumn := ALeft to ARight do
    begin
       ACell := CurrentSheet.CreateCell(ARow, AColumn);
       RecordReader.Position := AOffset;
       with TdxXLSFormulaReader.Create(Self) do
       try
         ReadFormula(ACell);
       finally
         Free;
       end;
    end;
  end;
end;

procedure TdxSpreadSheetXLSReader.ReadFormulaCell;
begin
  with TdxXLSFormulaReader.Create(Self) do
  try
    RecordReader.Position := 20;
    ReadFormula(CreateCell)
  finally
    Free;
  end;
end;

procedure TdxSpreadSheetXLSReader.ReadLabelCell;
begin
  RecordReader.Position := 6;
  CreateCell.AsSharedString := Reader.XLS_ReadString;
end;

procedure TdxSpreadSheetXLSReader.ReadGridlines;
begin
{var
  V: Word;
begin
  V := Reader.ReadWord;
  if Reader.ReadWord = 1 then
    CurrentSheet.Options.GridLines :=}
end;

procedure TdxSpreadSheetXLSReader.ReadIteration;
begin
  SpreadSheet.OptionsBehavior.IterativeCalculation := Reader.ReadWord = 1;
end;

procedure TdxSpreadSheetXLSReader.ReadLabelSSTCell;
begin
  Reader.Stream.Position := 6;
  CreateCell.AsSharedString := TdxSpreadSheetSharedString(Strings[Reader.ReadInteger]);
end;

procedure TdxSpreadSheetXLSReader.ReadMergedCells;
var
  AIndex: Integer;
  AMergedIndex: Integer;
begin
  for AIndex := 0 to Reader.ReadWord - 1 do
  begin
    AMergedIndex := CurrentSheet.MergedCells.Count;
    CurrentSheet.MergedCells.Add(Reader.XLS_ReadREF);
    if AMergedIndex < CurrentSheet.MergedCells.Count then
      TdxSpreadSheetTableViewMergeCellStyleHelper.Calculate(CurrentSheet.MergedCells.Last);
  end;
end;

procedure TdxSpreadSheetXLSReader.ReadMulRKCells;
var
  AIndex, ARow, AStartColumn: Integer;
begin
  ARow := Reader.Words[0];
  AStartColumn := Reader.Words[1];
  for AIndex := 0 to (RecordReader.Size - 6) div 6 - 1 do
    CreateCell(ARow, AIndex + AStartColumn, Reader.Words[2 + AIndex * 3]).AsFloat :=
      Reader.XLS_ReadRKValue((AIndex + 1) * 6);
end;

procedure TdxSpreadSheetXLSReader.ReadMSODrawing;
begin
{$IFDEF XLSDRAWING}
  with TdxSpreadSheetMSODrawingReader.Create(Self) do
  try
    Read;
  finally
    Free;
  end;
{$ENDIF}
end;

procedure TdxSpreadSheetXLSReader.ReadMSODrawingGroup;
begin
{$IFDEF XLSDRAWING}
  with TdxSpreadSheetMSODrawingGroupReader.Create(Self) do
  try
    Read;
  finally
    Free;
  end;
{$ENDIF}
end;

procedure TdxSpreadSheetXLSReader.ReadName;
var
  AOptions, ASize: Word;
  AName, ANameNoPrefix: TdxUnicodeString;
  AInfo: TdxSpreadSheetFunctionInfo;
  ADefinedName: TdxSpreadSheetDefinedName;
const
  XLSFuncPrefix = '_xlfn.';
begin
  AOptions := Reader.ReadWord;
  ASize := Reader.Bytes[4];
  RecordReader.Position := 14;
  AName := Reader.XLS_ReadString(Reader.ReadByte, Reader.Bytes[3]);
  if ASize < 5 then
  begin
    AInfo := dxSpreadSheetFunctionsRepository.GetInfoByName(AName);
    if (AInfo = nil) and (Pos(XLSFuncPrefix, AName) = 1) then
    begin
      ANameNoPrefix := Copy(AName, Length(XLSFuncPrefix) + 1, MaxInt);
      AInfo := dxSpreadSheetFunctionsRepository.GetInfoByName(ANameNoPrefix);
      if AInfo <> nil then
        AName := ANameNoPrefix;
    end;
    if AInfo = nil then
      AInfo := dxSpreadSheetFunctionsRepository.AddUnknown(AName);
    Names.AddObject(AName, AInfo);
  end
  else
    if (AOptions = 0) or (AOptions = 2) then
    begin
      Names.Add(AName);
      ADefinedName := SpreadSheet.DefinedNames.Add(AName, '', nil);
      with TdxXLSFormulaReader.Create(Self) do
      try
        ReadName(ADefinedName, ASize, @PByteArray(RecordReader.Memory)^[RecordReader.Position]);
      finally
        Free;
      end;
    end
    else
      Names.Add(AName);
end;

procedure TdxSpreadSheetXLSReader.ReadNumberCell;
begin
  RecordReader.Position := 6;
  CreateCell().AsFloat := Reader.ReadDateTime;
end;

procedure TdxSpreadSheetXLSReader.ReadPanes;
begin
  if not HasFrozenPanes then Exit;
  CurrentSheet.FrozenColumn := Reader.ReadWord - 1;
  CurrentSheet.FrozenRow := Reader.ReadWord - 1;
end;

procedure TdxSpreadSheetXLSReader.ReadPalette;
begin
  SetLength(Palette, Reader.ReadWord);
  Reader.Stream.ReadBuffer(Palette[0], Length(Palette) * SizeOf(TColor));
end;

procedure TdxSpreadSheetXLSReader.ReadPrintHeaders;
begin
end;

procedure TdxSpreadSheetXLSReader.ReadProtection;
begin
  if CurrentSheet <> nil then
    CurrentSheet.Options.Protected := Reader.ReadWord = 1
  else
    SpreadSheet.OptionsBehavior.Protected := Reader.ReadWord = 1;
end;

procedure TdxSpreadSheetXLSReader.ReadRowFormatInformation;
var
  ASize: Integer;
  AOptions: Word;
  ARow: TdxSpreadSheetTableRow;
begin
  ARow := CurrentSheet.Rows.CreateItem(Reader.Words[0]);
  AOptions := Reader.Words[6];
  ARow.Visible := (AOptions and $20 = 0);
  ASize := Round(Reader.Words[3] / 20 * 1.325);
  if AOptions and $80 = $80 then
  begin
    ARow.Style.Handle := GetStyle((Reader.Words[7] and $FFF)).Handle;
    if ASize > 32768 then Exit;
  end;
  ARow.Size := ASize;
end;

procedure TdxSpreadSheetXLSReader.ReadRKCell;
begin
  CreateCell.AsFloat := Reader.XLS_ReadRKValue(6);
end;

procedure TdxSpreadSheetXLSReader.ReadSelection;
var
  AIndex, ACount: Integer;
  AFocusedRow, AFocusedColumn: Word;
begin
  CurrentSheet.Selection.Clear;
  Reader.ReadByte;
  AFocusedRow := Reader.ReadWord;
  AFocusedColumn := Reader.ReadWord;
  Reader.ReadWord;
  ACount := Reader.ReadWord;
  for AIndex := 0 to ACount - 1 do
    CurrentSheet.Selection.Add(Reader.XLS_ReadSimpleREF, [ssShift]);
  CurrentSheet.Selection.SetFocused(AFocusedRow, AFocusedColumn, [ssShift]);
end;

procedure TdxSpreadSheetXLSReader.ReadSharedFormula;
begin
  ReadFormulas(Reader.Words[0], Reader.Bytes[4], Reader.Words[1], Reader.Bytes[5], 8);
end;

procedure TdxSpreadSheetXLSReader.ReadSharedStringTable;
var
  AIndex: Integer;
begin
  Strings.Count := Reader.Integers[1];
  RecordReader.Position := 8;
  for AIndex := 0 to Strings.Count - 1 do
    TList(Strings).Items[AIndex] := Reader.XLS_ReadString;
end;

procedure TdxSpreadSheetXLSReader.ReadWindow1Information;
var
  AOptions: Word;
begin
  AOptions := Reader.Words[4];
  ActiveTabIndex := Reader.Words[5];
  FirstTabIndex := Reader.Words[6];
  SpreadSheet.OptionsView.HorizontalScrollBar := AOptions and $8 = $8;
  SpreadSheet.OptionsView.VerticalScrollBar := AOptions and $10 = $10;
end;

procedure TdxSpreadSheetXLSReader.ReadWindow2Information;
var
  AOptions: Word;
begin
  AOptions := Reader.ReadWord;
  CurrentSheet.TopRow := Reader.ReadWord;
  CurrentSheet.LeftColumn := Reader.ReadWord;
//  CurrentSheet.Options.GridLineColor := GetColor(Reader.ReadInteger);
  CurrentSheet.Options.ShowFormulas := TdxDefaultBoolean(AOptions and $1 = $1);
  CurrentSheet.Options.GridLines := TdxDefaultBoolean(AOptions and $2 = $2);
  CurrentSheet.Options.Headers := TdxDefaultBoolean(AOptions and $4 = $4);
  HasFrozenPanes := AOptions and $8 = $8;
end;

procedure TdxSpreadSheetXLSReader.ReadXFRecord;
var
  AStyle: TdxXLSCellStyle;
begin
  AStyle := TdxXLSCellStyle.Create;
  AStyle.Font := Reader.ReadWord;
  AStyle.Format := Reader.ReadWord;
  AStyle.Flags := Reader.ReadWord;
  AStyle.Alignments := Reader.ReadWord;
  AStyle.Indents := Reader.ReadWord;
  AStyle.BordersStyle := Reader.ReadWord;
  AStyle.LeftRightBorders := Reader.ReadWord;
  AStyle.TopBottomBordersAndFill := Reader.ReadInteger;
  AStyle.FillColors := Reader.ReadWord;
  FStyles.Add(AStyle);
end;

procedure TdxSpreadSheetXLSReader.ReadZoomFactor;
var
  ANum, ADenom: Word;
begin
  ANum := Reader.ReadWord;
  ADenom := Reader.ReadWord;
  CurrentSheet.Options.ZoomFactor := MulDiv(100, ANum, ADenom);
end;

procedure TdxSpreadSheetXLSReader.SetProgressValue(AValue: Integer);
begin
  if FProgressValue <> AValue then
  begin
    FProgressValue := AValue;
    DoProgress(AValue);
  end;
end;

{ TdxXLSRecordReader }

constructor TdxXLSRecordReader.Create(AOwner: TdxSpreadSheetXLSReader);
begin
  inherited Create;
  FOwner := AOwner;
  FParts := TList.Create;
end;

destructor TdxXLSRecordReader.Destroy;
begin
  FreeAndNil(FParts);
  inherited Destroy;
end;

procedure TdxXLSRecordReader.AddPart(ASize: Word);
begin
  if Parts.Count = 0 then
    FSize := 0;
  Inc(FSize, ASize);
  Parts.Add(Pointer(FSize));
  inherited Size := FSize;
  Source.ReadBuffer(PByteArray(Memory)^[FSize - ASize], ASize);
end;

procedure TdxXLSRecordReader.Initialize;
var
  AType: Word;
begin
  Clear;
  Parts.Clear;
  CurrentPart := 0;
  AddPart(FSize);
  while not Owner.IsEOF do
  begin
    AType := ReadWordFunc(Source);
    if (AType = brcContinue) or ((RecordID = brcMSODRAWINGGROUP) and (AType = RecordID)) then
      AddPart(ReadWordFunc(Source))
    else
    begin
      Source.Position := Source.Position - 2;
      Break;
    end;
  end;
end;

procedure TdxXLSRecordReader.PrepareRecord;
begin
  FStartPosition := Source.Position;
  FRecordID := ReadWordFunc(Source);
  FSize := ReadWordFunc(Source);
end;

procedure TdxXLSRecordReader.SkipRecord;
begin
  Source.Position := Source.Position + Size;
  while not Owner.IsEOF do
  begin
    if ReadWordFunc(Source) = brcCONTINUE then
    begin
      FSize := ReadWordFunc(Source);
      Source.Position := Source.Position + FSize;
    end
    else
    begin
      Source.Position := Source.Position - 2;
      Break;
    end;
  end;
end;

function TdxXLSRecordReader.GetSource: TStream;
begin
  Result := Owner.Data; 
end;

{ TdxXLSReader }

constructor TdxXLSReader.Create(ARecord: TdxXLSRecordReader);
begin
  inherited Create(ARecord);
  FXLSRecord := ARecord;
end;

function TdxXLSReader.XLS_ReadREF: TRect;
begin
  Result.Top := ReadWord;
  Result.Bottom := ReadWord;
  Result.Left := ReadWord;
  Result.Right := ReadWord;
end;

function TdxXLSReader.XLS_ReadRKValue(AOffset: Integer): Double;
var
  AValue: Integer;
begin
  Result := 0;
  FXLSRecord.Position := AOffset;
  AValue := ReadInteger;
  PIntegerArray(@Result)^[1] := Integer(AValue and $FFFFFFFC);
  case AValue and 3 of
    1:
      Result := Result / 100;
    2:
      Result := Integer(AValue and $FFFFFFFC) / 4;
    3:
      Result := Integer(AValue and $FFFFFFFC) / 400;
  end;
end;

function TdxXLSReader.XLS_ReadSimpleREF: TRect;
begin
  Result.Top := ReadWord;
  Result.Bottom := ReadWord;
  Result.Left := ReadByte;
  Result.Right := ReadByte;
end;

function TdxXLSReader.XLS_ReadSimpleString(AOffset: Integer): TdxUnicodeString;
begin
  Stream.Position := AOffset;
  Result := XLS_ReadString(ReadByte, ReadByte);
end;

function TdxXLSReader.XLS_ReadString: TdxSpreadSheetSharedString;
var
  AOptions: Byte;
  ALength, AFormatRunsCount: Word;
  AExtRstLen: Integer;
begin
  ALength := ReadWord;
  AOptions := ReadByte;
  AExtRstLen := 0;
  AFormatRunsCount := 0;
  if ALength = 0 then
  begin
    Result := TdxSpreadSheetSharedString.CreateObject('');
    Exit;
  end;
  // read rich runs count
  if (AOptions and $08) <> 0 then
    AFormatRunsCount := ReadWord;
  // read Far East Versions ExtRst data
  if AOptions and $04 <> 0 then
    AExtRstLen := ReadInteger;
  // read string data
  if AFormatRunsCount = 0 then
    Result := TdxSpreadSheetSharedString.CreateObject(XLS_ReadString(AOptions, ALength))
  else
  begin
    Result := TdxSpreadSheetFormattedSharedString.CreateObject(XLS_ReadString(AOptions, ALength));
    while AFormatRunsCount > 0 do
    begin
      TdxSpreadSheetFormattedSharedString(Result).Runs.Add(ReadInteger, nil);
      Dec(AFormatRunsCount);
    end;
  end;
  Stream.Position := Stream.Position + AExtRstLen;
end;

function TdxXLSReader.XLS_ReadString(const AOptions, ALength: Integer): TdxUnicodeString;
var
  ACharSize: Byte;
  AAnsiString: AnsiString;
  AFirstSize, ASecondSize, AOffset: Integer;
  L: Integer;
begin
  ACharSize := Byte(AOptions and $01 <> 0) + 1;
  CheckRange(ALength * ACharSize, AFirstSize, ASecondSize, AOffset);
  L := AFirstSize div ACharSize;
  if ACharSize = 2 then
  begin
    SetLength(Result, L);
    Stream.ReadBuffer(Result[1], L * SizeOf(WideChar));
  end
  else
  begin
    SetLength(AAnsiString, L);
    Stream.ReadBuffer(AAnsiString[1], L);
    Result := dxAnsiStringToString(AAnsiString);
  end;
  if ASecondSize > 0 then
  begin
    Stream.Position := AOffset;
    Result := Result + XLS_ReadString(ReadByte, ASecondSize div ACharSize);
  end;
end;

procedure TdxXLSReader.CheckRange(const ASize: Integer; var AFirstSize, ASecondSize, APosition: Integer);
var
  I, AStartPos, AFinishPos: Integer;
begin
  AFirstSize := ASize;
  ASecondSize := 0;
  APosition := 0;
  if XLSRecord.Parts.Count = 1 then Exit;
  AStartPos := XLSRecord.Position;
  AFinishPos := AStartPos + ASize;
  for I := XLSRecord.CurrentPart to XLSRecord.Parts.Count - 1 do
  begin
    APosition := Integer(XLSRecord.Parts.List[I]);
    if AFinishPos < APosition then
      Break
    else
      if AStartPos <= APosition then
      begin
        XLSRecord.CurrentPart := I;
        if AStartPos = APosition then
          AFirstSize := APosition - AStartPos
        else
          AFirstSize := APosition - AStartPos;
        ASecondSize := ASize - AFirstSize;
        Break;
      end
  end;
end;

function TdxXLSReader.GetByteByIndex(AIndex: Integer): Byte;
begin
  Result := PByteArray(FXLSRecord.Memory)^[AIndex];
end;

function TdxXLSReader.GetIntegerByIndex(AIndex: Integer): Integer;
begin
  Result := PIntegerArray(FXLSRecord.Memory)^[AIndex];
end;

function TdxXLSReader.GetWordByIndex(AIndex: Integer): Word;
begin
  Result := PWordArray(FXLSRecord.Memory)^[AIndex];
end;

initialization
  TdxSpreadSheetXLSFormat.Register;

finalization
  TdxSpreadSheetXLSFormat.UnRegister;

end.

