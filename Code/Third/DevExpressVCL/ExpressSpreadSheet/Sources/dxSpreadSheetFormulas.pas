{********************************************************************}
{                                                                    }
{           Developer Express Visual Component Library               }
{           ExpressSpreadSheet                                       }
{                                                                    }
{           Copyright (c) 2001-2014 Developer Express Inc.           }
{           ALL RIGHTS RESERVED                                      }
{                                                                    }
{   The entire contents of this file is protected by U.S. and        }
{   International Copyright Laws. Unauthorized reproduction,         }
{   reverse-engineering, and distribution of all or any portion of   }
{   the code contained in this file is strictly prohibited and may   }
{   result in severe civil and criminal penalties and will be        }
{   prosecuted to the maximum extent possible under the law.         }
{                                                                    }
{   RESTRICTIONS                                                     }
{                                                                    }
{   THIS SOURCE CODE AND ALL RESULTING INTERMEDIATE FILES            }
{   (DCU, OBJ, DLL, ETC.) ARE CONFIDENTIAL AND PROPRIETARY TRADE     }
{   SECRETS OF DEVELOPER EXPRESS INC. THE REGISTERED DEVELOPER IS    }
{   LICENSED TO DISTRIBUTE THE EXPRESSSPREADSHEET CONTROL AND ALL    }
{   ACCOMPANYING VCL CONTROLS AS PART OF AN EXECUTABLE PROGRAM ONLY. }
{                                                                    }
{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED       }
{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE         }
{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE        }
{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT   }
{   AND PERMISSION FROM DEVELOPER EXPRESS INC.                       }
{                                                                    }
{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON        }
{   ADDITIONAL RESTRICTIONS.                                         }
{                                                                    }
{********************************************************************}


{.$DEFINE TESTPARSER}
unit dxSpreadSheetFormulas;

{$I cxVer.inc}

interface

uses
{$IFDEF DELPHI16}
  System.UITypes,
{$ENDIF}
  Windows, SysUtils, Variants, Classes, dxCore, dxCoreClasses, cxClasses, cxFormats, dxSpreadSheetCore, dxSpreadSheetTypes,
  dxSpreadSheetUtils, dxSpreadSheetStrs, dxSpreadSheetClasses, cxVariants, Generics.Collections, cxGeometry, Types;


type
  TdxSpreadSheetFormulaReference = class;
  TdxSpreadSheetFunction = procedure(Sender: TdxSpreadSheetFormulaResult; const AParams: TdxSpreadSheetFormulaToken);

  TdxSpreadSheetFunctionType = (ftCommon, ftCompatibility, ftDateTime, ftMath, ftFinancial,
    ftInformation, ftLookupAndReference, ftLogical, ftStatistical, ftText);

  { TdxSpreadSheetFunctionInfo }

  TdxSpreadSheetFunctionInfo = class
  public
    NamePtr: Pointer;
    Name: TdxUnicodeString;
    Proc: TdxSpreadSheetFunction;
    ParamCount: Integer;
    Token: Word;
    TypeID: TdxSpreadSheetFunctionType;
    Validator: TdxSpreadSheetFunction;
    procedure UpdateInfo(AFormatSettings: TdxSpreadSheetFormatSettings);
  end;

  { TdxSpreadSheetFormulaNullToken }

  TdxSpreadSheetFormulaNullToken = class(TdxSpreadSheetFormulaToken)
  protected
    procedure Calculate(AResult: TdxSpreadSheetFormulaResult); override;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  end;

  { TdxSpreadSheetFormulaStringValueToken }

  TdxSpreadSheetFormulaStringValueToken = class(TdxSpreadSheetFormulaToken)
  protected
    FValue: TdxUnicodeString;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  public
    constructor Create(const AValue: TdxUnicodeString); virtual;
    procedure GetValue(var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode); override;
    procedure LoadFromStream(AReader: TcxReader); override;

    property Value: TdxUnicodeString read FValue;
  end;

  { TdxSpreadSheetFormulaOperationToken }

  TdxSpreadSheetFormulaOperationToken = class(TdxSpreadSheetFormulaToken)
  protected
    FOperation: TdxSpreadSheetFormulaOperation;
    procedure Calculate(AResult: TdxSpreadSheetFormulaResult); override;
    procedure CalculateReferences(AResult: TdxSpreadSheetFormulaResult);
    function ExtractReference(AResult: TdxSpreadSheetFormulaResult; var ASheet: TdxSpreadSheetTableView; var AArea: TRect): Boolean;
    function GetTokenPriority: Integer; override;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  public
    constructor Create(AOperation: TdxSpreadSheetFormulaOperation); virtual;
    procedure CheckNeighbors; override;
    procedure LoadFromStream(AReader: TcxReader); override;

    property Operation: TdxSpreadSheetFormulaOperation read FOperation;
  end;

  { TdxSpreadSheetFormulaParenthesesToken }

  TdxSpreadSheetFormulaParenthesesToken = class(TdxSpreadSheetFormulaToken)
  protected
    procedure Calculate(AResult: TdxSpreadSheetFormulaResult); override;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  end;

  { TdxSpreadSheetFormulaBooleanValueToken }

  TdxSpreadSheetFormulaBooleanValueToken = class(TdxSpreadSheetFormulaToken)
  protected
    FValue: Boolean;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  public
    constructor Create(AValue: Boolean); virtual;
    procedure GetValue(var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode); override;
    procedure LoadFromStream(AReader: TcxReader); override;

    property Value: Boolean read FValue;
  end;

  { TdxSpreadSheetFormulaIntegerValueToken }

  TdxSpreadSheetFormulaIntegerValueToken = class(TdxSpreadSheetFormulaToken)
  protected
    FValue: Integer;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  public
    constructor Create(AValue: Integer); virtual;
    procedure GetValue(var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode); override;
    procedure LoadFromStream(AReader: TcxReader); override;

    property Value: Integer read FValue;
  end;

  { TdxSpreadSheetFormulaFloatValueToken }

  TdxSpreadSheetFormulaFloatValueToken = class(TdxSpreadSheetFormulaToken)
  protected
    FValue: Double;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  public
    constructor Create(const AValue: Double); virtual;
    procedure GetValue(var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode); override;
    procedure LoadFromStream(AReader: TcxReader); override;

    property Value: Double read FValue;
  end;

  { TdxSpreadSheetFormulaCurrencyValueToken }

  TdxSpreadSheetFormulaCurrencyValueToken = class(TdxSpreadSheetFormulaToken)
  protected
    FValue: Currency;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  public
    constructor Create(AValue: Currency); virtual;
    procedure GetValue(var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode); override;
    procedure LoadFromStream(AReader: TcxReader); override;

    property Value: Currency read FValue;
  end;

  { TdxSpreadSheetFormulaDateTimeValueToken }

  TdxSpreadSheetFormulaDateTimeValueToken = class(TdxSpreadSheetFormulaToken)
  protected
    FValue: TDateTime;
    procedure Calculate(AResult: TdxSpreadSheetFormulaResult); override;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  public
    constructor Create(AValue: TDateTime); virtual;
    procedure LoadFromStream(AReader: TcxReader); override;

    property Value: TDateTime read FValue;
  end;

  { TdxSpreadSheetFormulaAttributeToken }

  TdxSpreadSheetFormulaAttributeToken = class(TdxSpreadSheetFormulaToken)
  protected
    procedure Calculate(AResult: TdxSpreadSheetFormulaResult); override;
    function GetTokenPriority: Integer; override;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  end;

  { TdxSpreadSheetFormulaErrorValueToken }

  TdxSpreadSheetFormulaErrorValueToken = class(TdxSpreadSheetFormulaToken)
  strict private
    FErrorCode: TdxSpreadSheetFormulaErrorCode;
  protected
    procedure Calculate(AResult: TdxSpreadSheetFormulaResult); override;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  public
    constructor Create(AErrorCode: TdxSpreadSheetFormulaErrorCode); virtual;
    procedure GetValue(var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode); override;
    procedure LoadFromStream(AReader: TcxReader); override;

    property ErrorCode: TdxSpreadSheetFormulaErrorCode read FErrorCode;
  end;

  { TdxSpreadSheetListToken }

  TdxSpreadSheetListToken = class(TdxSpreadSheetFormulaToken)
  protected
    function ParametersToString: TdxUnicodeString; virtual;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  end;

  { TdxSpreadSheetFormulaFunctionToken }

  TdxSpreadSheetFormulaFunctionToken = class(TdxSpreadSheetListToken)
  strict private
    FInformation: TdxSpreadSheetFunctionInfo;
  protected
    procedure Calculate(AResult: TdxSpreadSheetFormulaResult); override;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  public
    constructor Create(AInformation: TdxSpreadSheetFunctionInfo); virtual;
    procedure LoadFromStream(AReader: TcxReader); override;

    property Information: TdxSpreadSheetFunctionInfo read FInformation;
  end;

  { TdxSpreadSheetFormulaUnknownNameToken }

  TdxSpreadSheetFormulaUnknownNameToken = class(TdxSpreadSheetListToken)
  strict private
    FName: TdxUnicodeString;
  protected
    procedure Calculate(AResult: TdxSpreadSheetFormulaResult); override;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  public
    constructor Create(const AName: TdxUnicodeString); virtual;
    procedure LoadFromStream(AReader: TcxReader); override;

    property Name: TdxUnicodeString read FName;
  end;

  { TdxSpreadSheetFormulaUnknownFunctionToken }

  TdxSpreadSheetFormulaUnknownFunctionToken = class(TdxSpreadSheetFormulaUnknownNameToken)
  protected
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  end;

  { TdxSpreadSheet3DReferenceCustomLink }

  TdxSpreadSheet3DReferenceCustomLink = class
  protected
    FData: TObject;
    FOwner: TdxSpreadSheetFormulaToken;
  public
    constructor Create(AData: TObject); virtual;
    function ToString: TdxUnicodeString; reintroduce; virtual;

    property Data: TObject read FData write FData;
    property Owner: TdxSpreadSheetFormulaToken read FOwner;
  end;

  { TdxSpreadSheet3DReferenceLink }

  TdxSpreadSheet3DReferenceLink = class(TdxSpreadSheet3DReferenceCustomLink)
  strict private
    function GetSheet: TdxSpreadSheetTableView; inline;
  public
    function ToString: TdxUnicodeString; override;

    property Sheet: TdxSpreadSheetTableView read GetSheet;
  end;

  { TdxSpreadSheet3DExternalReferenceLink }

  TdxSpreadSheet3DExternalReferenceLink = class(TdxSpreadSheet3DReferenceCustomLink)
  strict private
    function GetExternalLink: TdxSpreadSheetExternalLink; inline;
  protected
    FName: TdxUnicodeString;
  public
    constructor Create(AData: TObject; const AName: TdxUnicodeString); reintroduce; overload;
    function ToString: TdxUnicodeString; override;

    property ExternalLink: TdxSpreadSheetExternalLink read GetExternalLink;
    property Name: TdxUnicodeString read FName;
  end;

  { TdxSpreadSheetDefinedNameToken }

  TdxSpreadSheetDefinedNameToken = class(TdxSpreadSheetFormulaStringValueToken)
  strict private
    function GetLink: TdxSpreadSheetDefinedName; inline;
  protected
    procedure Calculate(AResult: TdxSpreadSheetFormulaResult); override;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  public
    function CanConvertStrToNumber: Boolean; override;
    procedure GetValue(var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode); override;

    property Link: TdxSpreadSheetDefinedName read GetLink;
  end;

  { TdxSpreadSheetFormulaReference }

  TdxSpreadSheetFormulaReference = class(TdxSpreadSheetFormulaToken)
  strict private
    function GetAbsoluteColumn: Boolean;
    function GetAbsoluteRow: Boolean;
    function GetActualColumn: Integer;
    function GetActualRow: Integer;
    function GetAnchorColumn: Integer;
    function GetAnchorRow: Integer;
    function GetIsError: Boolean;
    function GetR1C1Reference: Boolean;
    procedure SetIsError(AValue: Boolean);
  protected
    FColumn: TdxSpreadSheetReference;
    FRow: TdxSpreadSheetReference;
    function ExtractReference(var ASheet: TdxSpreadSheetTableView; var AArea: TRect): Boolean; virtual;
    function LinkToString(ALink: TdxSpreadSheet3DReferenceCustomLink): TdxUnicodeString; inline;
    function ReferenceToString(const ARow, AColumn: TdxSpreadSheetReference): TdxUnicodeString;
    procedure SetLink(var Field, ALink: TdxSpreadSheet3DReferenceCustomLink);
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
    procedure UpdateReferences(AView: TdxSpreadSheetTableView; const AArea: TRect;
      AModification: TdxSpreadSheetCellsModification; AIsDeletion: Boolean); override;

    property R1C1Reference: Boolean read GetR1C1Reference;
  public
    constructor Create(ARow, AColumn: Integer; AAbsoluteRow, AAbsoluteColumn: Boolean);
    function CanConvertStrToNumber: Boolean; override;
    procedure GetValue(var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode); override;
    procedure LoadFromStream(AReader: TcxReader); override;

    property AbsoluteColumn: Boolean read GetAbsoluteColumn;
    property AbsoluteRow: Boolean read GetAbsoluteRow;
    property ActualColumn: Integer read GetActualColumn;
    property ActualRow: Integer read GetActualRow;
    property AnchorColumn: Integer read GetAnchorColumn;
    property AnchorRow: Integer read GetAnchorRow;
    property IsError: Boolean read GetIsError write SetIsError;
  end;

  { TdxSpreadSheetFormula3DReference }

  TdxSpreadSheetFormula3DReference = class(TdxSpreadSheetFormulaReference)
  strict private
    FLink: TdxSpreadSheet3DReferenceCustomLink;
  protected
    function ExtractReference(var ASheet: TdxSpreadSheetTableView; var AArea: TRect): Boolean; override;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  public
    constructor Create(ALink: TdxSpreadSheet3DReferenceCustomLink;
       ARow, AColumn: Integer; AAbsoluteRow, AAbsoluteColumn: Boolean);
    destructor Destroy; override;
    procedure GetValue(var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode); override;
    procedure LoadFromStream(AReader: TcxReader); override;

    property Link: TdxSpreadSheet3DReferenceCustomLink read FLink;
  end;

  { TdxSpreadSheetFormulaAreaReference }

  TdxSpreadSheetFormulaAreaReference = class(TdxSpreadSheetFormulaReference)
  strict private
    function GetAbsoluteColumn2: Boolean;
    function GetAbsoluteRow2: Boolean;
    function GetActualColumn2: Integer;
    function GetActualRow2: Integer;
  protected
    FColumn2: TdxSpreadSheetReference;
    FRow2: TdxSpreadSheetReference;
    function ExtractReference(var ASheet: TdxSpreadSheetTableView; var AArea: TRect): Boolean; override;
    function ForEach(AProc: TdxSpreadSheetForEachCallBack; const AData: Pointer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): Boolean; override;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
    procedure UpdateReferences(AView: TdxSpreadSheetTableView; const AArea: TRect;
      AModification: TdxSpreadSheetCellsModification; AIsDeletion: Boolean); override;
  public
    constructor Create(ARow, AColumn, ARow2, AColumn2: Integer;
      AAbsoluteRow, AAbsoluteColumn, AAbsoluteRow2, AAbsoluteColumn2: Boolean);
    function ExtractColumn(AIndex: Integer; var AColumn: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode): Boolean;
    function ExtractRow(AIndex: Integer; var ARow: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode): Boolean;
    function GetDimension: TdxSpreadSheetAreaDimension;
    procedure GetValueRelatedWithCell(ACell: TdxSpreadSheetCell; var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode); override;
    function IsEnumeration: Boolean; override;
    procedure LoadFromStream(AReader: TcxReader); override;

    property AbsoluteColumn2: Boolean read GetAbsoluteColumn2;
    property AbsoluteRow2: Boolean read GetAbsoluteRow2;
    property ActualColumn2: Integer read GetActualColumn2;
    property ActualRow2: Integer read GetActualRow2;
  end;

  { TdxSpreadSheetFormula3DAreaReference }

  TdxSpreadSheetFormula3DAreaReference = class(TdxSpreadSheetFormulaAreaReference)
  protected
    FLink: TdxSpreadSheet3DReferenceCustomLink;
    FLink2: TdxSpreadSheet3DReferenceCustomLink;
    function ExtractReference(var ASheet: TdxSpreadSheetTableView; var AArea: TRect): Boolean; override;
    function GetSheet: TdxSpreadSheetTableView; override;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  public
    constructor Create(ALink, ALink2: TdxSpreadSheet3DReferenceCustomLink; ARow, AColumn, ARow2, AColumn2: Integer;
      AAbsoluteRow, AAbsoluteColumn, AAbsoluteRow2, AAbsoluteColumn2: Boolean);
    destructor Destroy; override;

    property Link: TdxSpreadSheet3DReferenceCustomLink read FLink;
    property Link2: TdxSpreadSheet3DReferenceCustomLink read FLink2;
    property AbsoluteColumn;
    property AbsoluteColumn2;
    property AbsoluteRow;
    property AbsoluteRow2;
    property ActualColumn;
    property ActualColumn2;
    property ActualRow;
    property ActualRow2;
//    property Column;
//    property Column2;
//    property Row;
//    property Row2;
  end;

  { TdxSpreadSheetFormulaArrayToken }

  TdxSpreadSheetFormulaArrayToken = class(TdxSpreadSheetListToken)
  private
    FSize: TSize;
  protected
    function ForEach(AProc: TdxSpreadSheetForEachCallBack; const AData: Pointer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): Boolean; override;
    procedure ToString(var AAsText: TdxSpreadSheetFormulaToken); override;
  public
    function CanConvertStrToNumber: Boolean; override;
    function ExtractColumn(AIndex: Integer; out AColumn: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode): Boolean;
    function ExtractRow(AIndex: Integer; out ARow: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode): Boolean;
    function GetArray(var AErrorCode: TdxSpreadSheetFormulaErrorCode): Variant;
    function GetDimension(var AErrorCode: TdxSpreadSheetFormulaErrorCode): TdxSpreadSheetAreaDimension;
    procedure GetValue(var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode); override;
    function IsEnumeration: Boolean; override;

    property Size: TSize read FSize write FSize;
  end;

  { TdxSpreadSheetFormulaArrayRowSeparator }

  TdxSpreadSheetFormulaArrayRowSeparator = class(TdxSpreadSheetFormulaToken)
  protected
  end;

  { TdxSpreadSheetFormulaVariantToken }

  TdxSpreadSheetFormulaVariantToken = class(TdxSpreadSheetFormulaToken)
  private
    FValue: Variant;
  public
    constructor Create(const AValue: Variant); virtual;
    procedure GetValue(var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode); override;

    property Value: Variant read FValue;
  end;

  TdxSpreadSheetParserTokenController = function(var APosition: Integer; ALength: Integer; out AName: TdxSpreadSheetFormulaToken): Boolean of object;

  { TdxSpreadSheetFormulaParser }

  TdxSpreadSheetFormulaParser = class
  private
    FFormula: TdxSpreadSheetFormula;
    FSpreadSheet: TdxCustomSpreadSheet;
    function GetCell: TdxSpreadSheetCell;
    function GetColumn: Integer; {$IFNDEF VER220} inline; {$ENDIF}
    function GetFormatSettings: TdxSpreadSheetFormatSettings;
    function GetR1C1Reference: Boolean;
    function GetRow: Integer; {$IFNDEF VER220} inline; {$ENDIF}
    function GetSheet: TdxSpreadSheetTableView; {$IFNDEF VER220} inline; {$ENDIF}
  {$IFDEF TESTPARSER}
    function GetTreeAsText(ATree: TdxSpreadSheetFormulaToken): string;
  {$ENDIF}
  protected
    FFormulaText: TdxUnicodeString;
    FFormulaSourceText: TdxUnicodeString;
    FOffset: Integer;
    TokenControllers: array of TdxSpreadSheetParserTokenController;
    //
    procedure AddToken(var AList: TdxSpreadSheetFormulaToken; AToken: TdxSpreadSheetFormulaToken);
    procedure AddTokenFromStack(var AList, AStack: TdxSpreadSheetFormulaToken);
    //
    function IsABCChar(APosition: Integer): Boolean;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function IsArray(var APosition: Integer; ALength: Integer; out AArray: TdxSpreadSheetFormulaToken): Boolean; {$IFDEF DELPHI9} inline; {$ENDIF}
    function IsArraySeparator(const APosition: Integer): Boolean;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function IsBreakChar(APosition: Integer): Boolean;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function IsBoolean(var APosition: Integer; ALength: Integer; out ANumber: TdxSpreadSheetFormulaToken): Boolean; {$IFDEF DELPHI9} inline; {$ENDIF}
    function IsError(var APosition: Integer; ALength: Integer; out AError: TdxSpreadSheetFormulaToken): Boolean; {$IFDEF DELPHI9} inline; {$ENDIF}
    function IsErrorReference(var APosition: Integer; AFinishPos: Integer): Boolean; {$IFDEF DELPHI9} inline; {$ENDIF}
    function IsFunction(var APosition: Integer; ALength: Integer; out AFunction: TdxSpreadSheetFormulaToken): Boolean;
    function IsListSeparator(const APosition: Integer): Boolean;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function IsName(var APosition: Integer; ALength: Integer; out AName: TdxSpreadSheetFormulaToken): Boolean;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function IsNumber(var APosition: Integer; ALength: Integer; out ANumber: TdxSpreadSheetFormulaToken): Boolean;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function IsOperation(var APosition: Integer; ALength: Integer; out AOperation: TdxSpreadSheetFormulaToken): Boolean;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function IsReference(var APosition: Integer; ALength: Integer; out AReference: TdxSpreadSheetFormulaToken): Boolean;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function IsSeparator(var APosition: Integer): Boolean; {$IFDEF DELPHI9} inline; {$ENDIF}
    function IsString(var APosition: Integer; ALength: Integer; out AString: TdxSpreadSheetFormulaToken): Boolean; {$IFDEF DELPHI9} inline; {$ENDIF}
    function IsStringMark(APosition: Integer; ACheckAdditionalMark: Boolean = False): Boolean;
    function IsSubExpression(var APosition: Integer; ALength: Integer; out AExpression: TdxSpreadSheetFormulaToken): Boolean;
    function IsUnknown(var APosition: Integer; ALength: Integer; out AToken: TdxSpreadSheetFormulaToken): Boolean;
    function CheckExtraChars: Boolean;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function CheckError: Boolean; {$IFDEF DELPHI9} inline; {$ENDIF}
    function CheckReference(var APosition: Integer; AFinishPos: Integer; var ARow, AColumn: Integer; var AbsRow, AbsColumn: Boolean): Boolean; {$IFDEF DELPHI9} inline; {$ENDIF}
    function CheckRCReference(var APosition: Integer; AFinishPos: Integer; var ARow, AColumn: Integer; var AbsRow, AbsColumn: Boolean): Boolean; {$IFDEF DELPHI9} inline; {$ENDIF}
    function CheckRCReferencePart(var APosition: Integer; AFinishPos: Integer; const APrefix: TdxUnicodeString; var AIndex: Integer; var AAbsIndex: Boolean): Boolean; {$IFDEF DELPHI9} inline; {$ENDIF}
    function CheckAbsoluteReference(var AStartPos: Integer; AFinishPos: Integer; var AbsoluteReference: Boolean): Boolean; {$IFDEF DELPHI9} inline; {$ENDIF}
    function CheckAreaSeparator(var AStartPos: Integer; AFinishPos: Integer): Boolean; {$IFDEF DELPHI9} inline; {$ENDIF}
    function CheckColumnReference(var AStartPos: Integer; ALength: Integer; var AColumn: Integer): Boolean; {$IFDEF DELPHI9} inline; {$ENDIF}
    function CheckFullColumnRowReference(var AStartPos: Integer; AFinishPos: Integer; var ALink: TdxSpreadSheet3DReferenceCustomLink;
      var ARow, ACol, ARow2, ACol2: Integer; var AAbsRow, AAbsCol, AAbsRow2, AAbsCol2: Boolean): Boolean;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function CheckRowReference(var AStartPos, AFinishPos: Integer; var ARow: Integer): Boolean;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function CheckSignChar(const APosition: Integer): Boolean; {$IFDEF DELPHI9} inline; {$ENDIF}
    function CheckStandardReference(var APosition: Integer; AFinishPos: Integer; var ARow, AColumn: Integer; var AbsRow, AbsColumn: Boolean): Boolean;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function CheckSheetName(var APosition: Integer; AFinishPos: Integer; var ALink: TdxSpreadSheet3DReferenceCustomLink): Boolean;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function CheckText(const APosition: Integer; const ACandidate: TdxUnicodeString): Boolean;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function CheckUnaryOperation(AValue: TdxSpreadSheetFormulaToken; AOperation: TdxSpreadSheetFormulaOperationToken): Boolean; inline;
    function DoFullParse(AStartPos, AFinishPos: Integer): TdxSpreadSheetFormulaToken; virtual;
    function DoParse(var AStartPos, AFinishPos: Integer): TdxSpreadSheetFormulaToken; virtual;
    function GetError(var AStartPos, AFinishPos: Integer; var ACode: TdxSpreadSheetFormulaErrorCode): Boolean;
    function GetIntReference(var AStartPos, AFinishPos: Integer; var AValue: Integer): Boolean;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function GetIntValue(var AStartPos, AFinishPos: Integer; var AValue: Integer): Boolean;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function GetParameters(AParent: TdxSpreadSheetFormulaToken; AStartPos, AFinishPos: Integer): Boolean;
    function GetSheetByName(APosition, ALength: Integer; var ASheet: TdxSpreadSheetTableView): Boolean; {$IFDEF DELPHI9} inline; {$ENDIF}
    function GetStringLength(APosition, ALength: Integer): Integer;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function GetStringPart(var APosition: Integer; ALength: Integer): TdxUnicodeString; {$IFDEF DELPHI9} inline; {$ENDIF}
    function GetSubString(const APosition, ALength: Integer): TdxUnicodeString;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function GetSubExpressionLength(APosition, ALength: Integer): Integer;  {$IFDEF DELPHI9} inline; {$ENDIF}
    function GetNextToken(var APosition: Integer; const ALength: Integer): TdxSpreadSheetFormulaToken;
    function MakeReference(ALink, ALink2: TdxSpreadSheet3DReferenceCustomLink; ARow, ACol, ARow2, ACol2: Integer;
      AAbsRow, AAbsCol, AAbsRow2, AAbsCol2: Boolean; AIsArea: Boolean): TdxSpreadSheetFormulaToken; {$IFDEF DELPHI9} inline; {$ENDIF}
    function PrepareTokensFromString(var AStartPos, AFinishPos: Integer): TdxSpreadSheetFormulaToken;
    procedure PostProcessFormulaTokens(AParent: TdxSpreadSheetFormulaToken);
    procedure RegisterTokenControllers; virtual;
    procedure SetErrorIndex(AErrorIndex: Integer; const ACode: TdxSpreadSheetFormulaErrorCode = ecNone);
    procedure ValidateReference(AIndex: Integer; var AReference: Integer; AIsAbsolute: Boolean);
    procedure ValidateR1C1Reference(AIndex: Integer; var AReference: Integer; AIsAbsolute: Boolean);
  public
    constructor Create(ASpreadSheet: TdxCustomSpreadSheet); virtual;
    function ParseFormula(const AFormulaText: TdxUnicodeString; ACell: TdxSpreadSheetCell): Boolean; overload; virtual;
    function ParseFormula(const AFormulaText: TdxUnicodeString; var AFormula: TdxSpreadSheetFormula): Boolean; overload; virtual;

    property Cell: TdxSpreadSheetCell read GetCell;
    property Column: Integer read GetColumn;
    property Formula: TdxSpreadSheetFormula read FFormula;
    property FormatSettings: TdxSpreadSheetFormatSettings read GetFormatSettings;
    property R1C1Reference: Boolean read GetR1C1Reference;
    property Row: Integer read GetRow;
    property Sheet: TdxSpreadSheetTableView read GetSheet;
    property SpreadSheet: TdxCustomSpreadSheet read FSpreadSheet;
  end;

  function dxSpreadSheetFormulaTokensRepository: TList<TClass>;

implementation

uses
  dxSpreadSheetFunctions, Math;

type
  TNameAccess = class(TdxSpreadSheetDefinedName);
  TFormulaAccess = class(TdxSpreadSheetFormula);
  TTokenAccess = class(TdxSpreadSheetFormulaToken);

var
  TokensRepository: TList<TClass>;

function dxSpreadSheetFormulaTokensRepository: TList<TClass>;
begin
  if TokensRepository = nil then
    TokensRepository := TList<TClass>.Create;
  Result := TokensRepository;
end;

{ TdxSpreadSheetFunctionInfo }

procedure TdxSpreadSheetFunctionInfo.UpdateInfo(AFormatSettings: TdxSpreadSheetFormatSettings);
begin
  if NamePtr = nil then Exit;
  if AFormatSettings <> nil then
    Name := AFormatSettings.GetFunctionName(NamePtr)
  else
    Name := dxSpreadSheetUpperCase(dxStringToWideString(cxGetResourceString(NamePtr)));
end;

{ TdxSpreadSheetFormulaEmptyToken }

procedure TdxSpreadSheetFormulaNullToken.Calculate(AResult: TdxSpreadSheetFormulaResult);
begin
  AResult.AddValue(Null)
end;

procedure TdxSpreadSheetFormulaNullToken.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  AttachString(AAsText, '');
end;

{ TdxSpreadSheetFormulaStringValueToken }

constructor TdxSpreadSheetFormulaStringValueToken.Create(const AValue: TdxUnicodeString);
begin
  FValue := AValue;
end;

procedure TdxSpreadSheetFormulaStringValueToken.GetValue(
  var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode);
begin
  AValue := FValue;
end;

procedure TdxSpreadSheetFormulaStringValueToken.LoadFromStream(AReader: TcxReader);
begin
  FValue := AReader.ReadWideString;
  inherited LoadFromStream(AReader);
end;

procedure TdxSpreadSheetFormulaStringValueToken.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  AttachString(AAsText, dxStringMarkChar + Value + dxStringMarkChar);
end;

{ TdxSpreadSheetFormulaOperationToken }

constructor TdxSpreadSheetFormulaOperationToken.Create(AOperation: TdxSpreadSheetFormulaOperation);
begin
  FOperation := AOperation;
end;

procedure TdxSpreadSheetFormulaOperationToken.CheckNeighbors;
begin
  if not (Operation in [opAdd, opSub]) then Exit;
  if (Prev = nil) or (Prev.Priority >= 1) then
    Inc(FOperation, $F);
end;

procedure TdxSpreadSheetFormulaOperationToken.LoadFromStream(AReader: TcxReader);
begin
  FOperation := TdxSpreadSheetFormulaOperation(AReader.ReadByte);
  inherited LoadFromStream(AReader);
end;

function dxSpreadSheetVarCompare(const V1, V2: Variant): Integer;
begin
  if VarIsStr(V1) then
  begin
    if VarIsNull(V2) or VarIsNumeric(V2) then
      Result := 1
    else
      if dxIsLogical(V2) then
        Result := -1
      else
        Result := VarCompare(V1, V2)
  end
  else
    if VarIsStr(V2) then
      Result := -dxSpreadSheetVarCompare(V2, V1)
    else
      Result := VarCompare(V1, V2);
end;

procedure TdxSpreadSheetFormulaOperationToken.Calculate(AResult: TdxSpreadSheetFormulaResult);
var
  AError: TdxSpreadSheetFormulaErrorCode;
  ALeftVariantValue, ARightVariantValue: Variant;
  ALeft, ARight: Variant;
  ACanConvertStrToNumber: Boolean;
begin
  if FOperation in [opIsect, opUnion, opRange]  then
  begin
    CalculateReferences(AResult);
    Exit;
  end;
  ALeft := Null;
  ARightVariantValue := Null;
  ALeftVariantValue := Null;
  if FOperation = opConcat then
  begin
    ARight := AResult.ExtractStringValue;
    ALeft := AResult.ExtractStringValue;
  end
  else
  begin
    if (AResult.Count >= 2) and (Operation in [opAdd, opSub, opMul]) then
    begin
      AResult.Items[AResult.Count - 1].GetValueRelatedWithCell(Owner.Cell, ARightVariantValue, AError);
      AResult.Items[AResult.Count - 2].GetValueRelatedWithCell(Owner.Cell, ALeftVariantValue, AError);
    end;
    ARight := AResult.ExtractValue(ACanConvertStrToNumber);
    if FOperation in [opAdd, opConcat, opSub, opMul, opDiv, opPower, opLT, opLE, opEQ, opGE, opGT, opNE] then
      ALeft := AResult.ExtractValue(ACanConvertStrToNumber);
  end;

  if AResult.ErrorCode <> ecValue then
  begin
    if (VarIsNull(ALeftVariantValue) or VarIsEmpty(ALeftVariantValue)) and (VarIsNull(ARightVariantValue) or
      VarIsEmpty(ARightVariantValue)) and (Operation in [opAdd, opSub, opMul]) then
      AResult.AddValue(Null)
    else
    begin
      if Operation in [opAdd, opSub, opMul, opDiv, opPower, opUplus, opUminus] then
      begin
        if not AResult.ConvertToNumeric(ALeft, True, False) or not  AResult.ConvertToNumeric(ARight, True, False) then
          Exit;
      end;
      case FOperation of
        opAdd, opConcat:
          AResult.AddValue(ALeft + ARight);
        opSub:
          AResult.AddValue(ALeft - ARight);
        opMul:
          AResult.AddValue(ALeft * ARight);
        opDiv:
          try
            if Abs(ARight) <= MinDouble then
              AResult.SetError(ecDivByZero)
            else
              AResult.AddValue(Extended(ALeft) / Extended(ARight));
          except
            AResult.SetError(ecDivByZero);
          end;
        opPower:
          AResult.AddValue(Power(ALeft, ARight));
       opLT:
          AResult.AddValue(dxSpreadSheetVarCompare(ALeft, ARight) < 0);
        opLE:
          AResult.AddValue(dxSpreadSheetVarCompare(ALeft, ARight) <= 0);
        opEQ:
          AResult.AddValue(dxSpreadSheetVarCompare(ALeft, ARight) = 0);
        opGE:
          AResult.AddValue(dxSpreadSheetVarCompare(ALeft, ARight) >= 0);
        opGT:
          AResult.AddValue(dxSpreadSheetVarCompare(ALeft, ARight) > 0);
        opNE:
          AResult.AddValue(dxSpreadSheetVarCompare(ALeft, ARight) <> 0);
        opUplus:
          AResult.AddValue(ARight);
        opUminus:
          AResult.AddValue(-ARight);
        opPercent:
          AResult.AddValue(ARight / 100)
      end;
    end;
  end;
end;

procedure TdxSpreadSheetFormulaOperationToken.CalculateReferences(AResult: TdxSpreadSheetFormulaResult);
var
  R1, R2: TRect;
  ASheet1, ASheet2: TdxSpreadSheetTableView;
begin
  if not (ExtractReference(AResult, ASheet2, R2) and ExtractReference(AResult, ASheet1, R1)) then
    Exit;
  if ASheet1 = nil then
    ASheet1 := ASheet2;
  if ASheet1 = nil then
    ASheet1 := AResult.Owner.Sheet;
  if Operation = opIsect then
  begin
    if not dxSpreadSheetIntersects(R1, R2, R1) then
      AResult.SetError(ecNull);
  end
  else
    if Operation = opUnion then
      R1 := dxSpreadSheetCellsUnion(R1, R2)
    else
      R1.BottomRight := R2.BottomRight;
   AResult.Add(TdxSpreadSheetFormula3DAreaReference.Create(TdxSpreadSheet3DReferenceLink.Create(ASheet1),
     TdxSpreadSheet3DReferenceLink.Create(ASheet1), R1.Top, R1.Left, R1.Bottom, R1.Right, True, True, True, True));
end;

function TdxSpreadSheetFormulaOperationToken.ExtractReference(
  AResult: TdxSpreadSheetFormulaResult; var ASheet: TdxSpreadSheetTableView; var AArea: TRect): Boolean;
var
  AToken: TdxSpreadSheetFormulaToken;
begin
  Result := AResult.ErrorCode = ecNone;
  if not Result then Exit;
  AToken := AResult.ExtractValueToken;
  try
    Result := AToken is TdxSpreadSheetFormulaReference;
    if not Result or not TdxSpreadSheetFormulaReference(AToken).ExtractReference(ASheet, AArea) then
      AResult.SetError(ecRefErr);
    AArea := cxRectAdjust(AArea);
  finally
    if AToken.Owner = nil then
      AToken.Free;
  end;
end;

function TdxSpreadSheetFormulaOperationToken.GetTokenPriority: Integer;
const
  Operation2Priority: array[TdxSpreadSheetFormulaOperation] of Integer =
   (2, 2, 3, 3, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 6, 6, -1, -1);
begin
  Result := Operation2Priority[FOperation];
end;

procedure TdxSpreadSheetFormulaOperationToken.ToString(var AAsText: TdxSpreadSheetFormulaToken);
var
  ALeft, ARight: TdxUnicodeString;
begin
  case FOperation of
    opUplus, opUminus:
      AttachString(AAsText, FormatSettings.Operations[FOperation] + ExtractLastTokenAsString(AAsText));
    opPercent:
      AttachString(AAsText, ExtractLastTokenAsString(AAsText) + FormatSettings.Operations[FOperation]);
    opParen:
      AttachString(AAsText, dxLeftParenthesis + ExtractLastTokenAsString(AAsText) + dxRightParenthesis);
  else
    begin
      ARight := ExtractLastTokenAsString(AAsText);
      ALeft := ExtractLastTokenAsString(AAsText);
      AttachString(AAsText, ALeft + FormatSettings.Operations[FOperation] + ARight);
    end;
  end;
end;

{ TdxSpreadSheetFormulaParenthesesToken }

procedure TdxSpreadSheetFormulaParenthesesToken.Calculate(AResult: TdxSpreadSheetFormulaResult);
begin
  if HasChildren then
    AResult.AddResultValue(TFormulaAccess(Owner).Calculate(FirstChild))
end;

procedure TdxSpreadSheetFormulaParenthesesToken.ToString(
  var AAsText: TdxSpreadSheetFormulaToken);
begin
  AttachString(AAsText, dxLeftParenthesis + GetExpressionAsText(FirstChild) + dxRightParenthesis);
end;

{ TdxSpreadSheetFormulaBooleanValueToken }

constructor TdxSpreadSheetFormulaBooleanValueToken.Create(AValue: Boolean);
begin
  FValue := AValue;
end;

procedure TdxSpreadSheetFormulaBooleanValueToken.LoadFromStream(AReader: TcxReader);
begin
  FValue := AReader.ReadBoolean;
  inherited LoadFromStream(AReader);
end;

procedure TdxSpreadSheetFormulaBooleanValueToken.GetValue(
  var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode);
begin
  AValue := FValue;
end;

procedure TdxSpreadSheetFormulaBooleanValueToken.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  AttachString(AAsText, dxBoolToString[FValue]);
end;

{ TdxSpreadSheetFormulaIntegerValueToken }

constructor TdxSpreadSheetFormulaIntegerValueToken.Create(AValue: Integer);
begin
  FValue := AValue;
end;

procedure TdxSpreadSheetFormulaIntegerValueToken.GetValue(
  var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode);
begin
  AValue := FValue;
end;

procedure TdxSpreadSheetFormulaIntegerValueToken.LoadFromStream(AReader: TcxReader);
begin
  FValue := AReader.ReadInteger;
  inherited LoadFromStream(AReader);
end;

procedure TdxSpreadSheetFormulaIntegerValueToken.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  AttachString(AAsText, IntToStr(FValue));
end;

{ TdxSpreadSheetFormulaFloatValueToken }

constructor TdxSpreadSheetFormulaFloatValueToken.Create(const AValue: Double);
begin
  FValue := AValue;
end;

procedure TdxSpreadSheetFormulaFloatValueToken.GetValue(
  var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode);
begin
  AValue := FValue;
end;

procedure TdxSpreadSheetFormulaFloatValueToken.LoadFromStream(AReader: TcxReader);
begin
  FValue := AReader.ReadFloat;
  inherited LoadFromStream(AReader);
end;

procedure TdxSpreadSheetFormulaFloatValueToken.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  AttachString(AAsText, dxFloatToStr(FValue, FormatSettings.Data));
end;

{ TdxSpreadSheetFormulaCurrencyValueToken }

constructor TdxSpreadSheetFormulaCurrencyValueToken.Create(AValue: Currency);
begin
  FValue := AValue;
end;

procedure TdxSpreadSheetFormulaCurrencyValueToken.GetValue(
  var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode);
begin
  AValue := FValue;
end;

procedure TdxSpreadSheetFormulaCurrencyValueToken.LoadFromStream(AReader: TcxReader);
begin
  FValue := AReader.ReadCurrency;
  inherited LoadFromStream(AReader);
end;

procedure TdxSpreadSheetFormulaCurrencyValueToken.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  AttachString(AAsText, dxFloatToStr(FValue, FormatSettings.Data));
end;

{ TdxSpreadSheetFormulaDateTimeValueToken }

constructor TdxSpreadSheetFormulaDateTimeValueToken.Create(AValue: TDateTime);
begin
  FValue := AValue;
end;

procedure TdxSpreadSheetFormulaDateTimeValueToken.LoadFromStream(AReader: TcxReader);
begin
  FValue := AReader.ReadDateTime;
  inherited LoadFromStream(AReader);
end;

procedure TdxSpreadSheetFormulaDateTimeValueToken.Calculate(AResult: TdxSpreadSheetFormulaResult);
begin
  AResult.AddValue(FValue);
end;

procedure TdxSpreadSheetFormulaDateTimeValueToken.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  AttachString(AAsText, DateTimeToStr(FValue));
end;

{ TdxSpreadSheetFormulaAttributeToken }

procedure TdxSpreadSheetFormulaAttributeToken.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
// todo: need qualification how to processing this token
//  AAsText.AddString(dxOperations[opIsect] + AAsText.ExtractLastTokenAsString);
//
end;

procedure TdxSpreadSheetFormulaAttributeToken.Calculate(AResult: TdxSpreadSheetFormulaResult);
begin
end;

function TdxSpreadSheetFormulaAttributeToken.GetTokenPriority: Integer;
begin
  Result := -1;
end;

{ TdxSpreadSheetFormulaErrorValueToken }

constructor TdxSpreadSheetFormulaErrorValueToken.Create(
  AErrorCode: TdxSpreadSheetFormulaErrorCode);
begin
  FErrorCode := AErrorCode;
end;

procedure TdxSpreadSheetFormulaErrorValueToken.GetValue(var AValue: Variant;
  var AErrorCode: TdxSpreadSheetFormulaErrorCode);
begin
  AValue := Null;
  FErrorCode := ErrorCode;
end;

procedure TdxSpreadSheetFormulaErrorValueToken.LoadFromStream(AReader: TcxReader);
begin
  FErrorCode := TdxSpreadSheetFormulaErrorCode(AReader.ReadByte);
  inherited LoadFromStream(AReader);
end;

procedure TdxSpreadSheetFormulaErrorValueToken.Calculate(AResult: TdxSpreadSheetFormulaResult);
begin
  AResult.SetError(FErrorCode);
end;

procedure TdxSpreadSheetFormulaErrorValueToken.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  AttachString(AAsText, dxSpreadSheetErrorCodeToString(FErrorCode));
end;

{ TdxSpreadSheetFormulaReference }

constructor TdxSpreadSheetFormulaReference.Create(
  ARow, AColumn: Integer; AAbsoluteRow, AAbsoluteColumn: Boolean);
begin
  FRow.Offset := ARow;
  FRow.IsAbsolute := AAbsoluteRow;
  FColumn.Offset := AColumn;
  FColumn.IsAbsolute := AAbsoluteColumn;
end;

function TdxSpreadSheetFormulaReference.GetAbsoluteColumn: Boolean;
begin
  Result := FColumn.IsAbsolute;
end;

function TdxSpreadSheetFormulaReference.GetAbsoluteRow: Boolean;
begin
  Result := FRow.IsAbsolute;
end;

function TdxSpreadSheetFormulaReference.GetActualColumn: Integer;
begin
  Result := FColumn.ActualValue(AnchorColumn);
end;

function TdxSpreadSheetFormulaReference.GetActualRow: Integer;
begin
  Result := FRow.ActualValue(AnchorRow);
end;

function TdxSpreadSheetFormulaReference.GetAnchorColumn: Integer;
begin
  if Owner = nil then
    Result := 0
  else
    Result := Owner.Column;
end;

function TdxSpreadSheetFormulaReference.GetAnchorRow: Integer;
begin
  if Owner = nil then
    Result := 0
  else
    Result := Owner.Row;
end;

function TdxSpreadSheetFormulaReference.GetIsError: Boolean;
begin
  Result := FRow.IsError or FColumn.IsError;
end;

function TdxSpreadSheetFormulaReference.GetR1C1Reference: Boolean;
begin
  Result := Owner.FormatSettings.R1C1Reference;
end;

procedure TdxSpreadSheetFormulaReference.SetIsError(AValue: Boolean);
begin
  FRow.IsError := True;
  FColumn.IsError := True;
end;

procedure TdxSpreadSheetFormulaReference.GetValue(
  var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode);
begin
  GetCellValue(Owner.Sheet, ActualRow, ActualColumn, AValue, AErrorCode);
end;

procedure TdxSpreadSheetFormulaReference.LoadFromStream(AReader: TcxReader);
begin
  FRow.Value := AReader.ReadInt64;
  FColumn.Value := AReader.ReadInt64;
  inherited LoadFromStream(AReader);
end;

function TdxSpreadSheetFormulaReference.CanConvertStrToNumber: Boolean;
begin
  Result := False;
end;

function TdxSpreadSheetFormulaReference.ExtractReference(var ASheet: TdxSpreadSheetTableView; var AArea: TRect): Boolean;
begin
  ASheet := nil;
  AArea.Top := ActualRow;
  AArea.Left := ActualColumn;
  AArea.BottomRight := AArea.TopLeft;
  Result := (AArea.Top >= 0) and (AArea.Left >= 0);
end;

function TdxSpreadSheetFormulaReference.LinkToString(ALink: TdxSpreadSheet3DReferenceCustomLink): TdxUnicodeString;
begin
  if ALink <> nil then
    Result := ALink.ToString
  else
    Result := '';
end;

function TdxSpreadSheetFormulaReference.ReferenceToString(const ARow, AColumn: TdxSpreadSheetReference): TdxUnicodeString;
begin
  if R1C1Reference then
    Result := dxR1C1ReferenceToString(AnchorRow, AnchorColumn, ARow, AColumn)
  else
    Result := dxReferenceToString(AnchorRow, AnchorColumn, ARow, AColumn);
end;

procedure TdxSpreadSheetFormulaReference.SetLink(var Field, ALink: TdxSpreadSheet3DReferenceCustomLink);
begin
  Field := ALink;
  if ALink <> nil then
    ALink.FOwner := Self;
end;

procedure TdxSpreadSheetFormulaReference.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  AttachString(AAsText, ReferenceToString(FRow, FColumn));
end;

procedure TdxSpreadSheetFormulaReference.UpdateReferences(AView: TdxSpreadSheetTableView; const AArea: TRect;
   AModification: TdxSpreadSheetCellsModification; AIsDeletion: Boolean);
begin
  if AView <> Sheet then Exit;
  if AModification in [cmShiftCellsVertically, cmShiftRows] then
  begin
    if (AModification <> cmShiftCellsVertically) or InRange(AnchorColumn, AArea.Left, AArea.Right) then
      FRow.UpdateReference(AnchorRow, AArea.Top, AArea.Bottom, AIsDeletion)
  end
  else
    if (AModification <> cmShiftCellsHorizontally) or InRange(AnchorRow, AArea.Top, AArea.Bottom) then
      FColumn.UpdateReference(AnchorColumn, AArea.Left, AArea.Right, AIsDeletion);
end;

{ TdxSpreadSheetFormula3DReference }

constructor TdxSpreadSheetFormula3DReference.Create(ALink: TdxSpreadSheet3DReferenceCustomLink;
  ARow, AColumn: Integer; AAbsoluteRow, AAbsoluteColumn: Boolean);
begin
  inherited Create(ARow, AColumn, AAbsoluteRow, AAbsoluteColumn);
  FLink := ALink;
  if ALink <> nil then
    ALink.FOwner := Self;
end;

destructor TdxSpreadSheetFormula3DReference.Destroy;
begin
  FreeAndNil(FLink);
  inherited Destroy;
end;

procedure TdxSpreadSheetFormula3DReference.GetValue(var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode);
begin
  if Link is TdxSpreadSheet3DReferenceLink then
    GetCellValue(TdxSpreadSheet3DReferenceLink(Link).Sheet, ActualRow, ActualColumn, AValue, AErrorCode)
  else
    AErrorCode := ecRefErr;
end;

procedure TdxSpreadSheetFormula3DReference.LoadFromStream(AReader: TcxReader);
begin
  // do nothing
end;

function TdxSpreadSheetFormula3DReference.ExtractReference(var ASheet: TdxSpreadSheetTableView; var AArea: TRect): Boolean;
begin
  Result := inherited ExtractReference(ASheet, AArea) and (Link <> nil) and TdxSpreadSheetInvalidObject.IsLive(Link.Data);
  if Result then
    ASheet := Link.Data as TdxSpreadSheetTableView;
end;

procedure TdxSpreadSheetFormula3DReference.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  AttachString(AAsText, LinkToString(Link) + ReferenceToString(FRow, FColumn));
end;

{ TdxSpreadSheetFormulaAreaReference }

constructor TdxSpreadSheetFormulaAreaReference.Create(ARow, AColumn, ARow2, AColumn2: Integer;
  AAbsoluteRow, AAbsoluteColumn, AAbsoluteRow2, AAbsoluteColumn2: Boolean);
begin
  inherited Create(ARow, AColumn, AAbsoluteRow, AAbsoluteColumn);
  FRow2.Offset := ARow2;
  FRow2.IsAbsolute := AAbsoluteRow2;
  FColumn2.Offset := AColumn2;
  FColumn2.IsAbsolute := AAbsoluteColumn2;
  FRow.IsAllItems := ARow2 = MaxInt;
  FColumn.IsAllItems := AColumn2 = MaxInt;
end;

function TdxSpreadSheetFormulaAreaReference.GetDimension: TdxSpreadSheetAreaDimension;
begin
  Result.ColumnCount := ActualColumn2 - ActualColumn + 1;
  Result.RowCount := ActualRow2 - ActualRow + 1;
end;

procedure TdxSpreadSheetFormulaAreaReference.GetValueRelatedWithCell(
  ACell: TdxSpreadSheetCell; var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode);

  function CheckValue(var AValue: Integer; AIndex1, AIndex2: Integer): Boolean;
  begin
    Result := (AValue >= Min(AIndex1, AIndex2)) and (AValue <= Max(AIndex1, AIndex2));
    if not Result then
      AValue := Min(AIndex1, AIndex2);
    if Min(AIndex1, AIndex2) < 0 then
      AErrorCode := ecValue;
  end;

var
  ARow, AColumn: Integer;
  AIsRowValid, AIsColumnValid: Boolean;
begin
  if ACell <> nil then
  begin
    ARow := ACell.RowIndex;
    AColumn := ACell.ColumnIndex;
  end
  else
  begin
    ARow := Owner.Row;
    AColumn := Owner.Column;
  end;
  if (ActualRow <> ActualRow2) and (ActualColumn <> ActualColumn2) then
    AErrorCode := ecValue
  else
  begin
    AIsRowValid := CheckValue(ARow, ActualRow, ActualRow2) and (ActualColumn = ActualColumn2);
    AIsColumnValid := CheckValue(AColumn, ActualColumn, ActualColumn2) and  (ActualRow = ActualRow2);
    if not AIsRowValid and not AIsColumnValid then
      AErrorCode := ecValue;
    if AErrorCode <> ecValue then
      GetCellValue(GetSheet, ARow, AColumn, AValue, AErrorCode);
    if (Owner = nil) or (Owner.Cell = nil) and (ACell = nil) then // skip errors for defined names
      AErrorCode := ecNone;
  end;
end;

function TdxSpreadSheetFormulaAreaReference.IsEnumeration: Boolean;
begin
  Result := True;
end;

procedure TdxSpreadSheetFormulaAreaReference.LoadFromStream(AReader: TcxReader);
begin
  FRow2.Value := AReader.ReadInt64;
  FColumn2.Value := AReader.ReadInt64;
  inherited LoadFromStream(AReader);
end;

function TdxSpreadSheetFormulaAreaReference.ExtractColumn(AIndex: Integer; var AColumn: Variant;
  var AErrorCode: TdxSpreadSheetFormulaErrorCode): Boolean;
begin
  Result := ExtractVerticalVectorAsVariantArray(ActualRow, ActualColumn + AIndex,
    ActualRow2 - ActualRow + 1, AColumn, AErrorCode);
end;

function TdxSpreadSheetFormulaAreaReference.ExtractRow(AIndex: Integer; var ARow: Variant;
  var AErrorCode: TdxSpreadSheetFormulaErrorCode): Boolean;
begin
  Result := ExtractHorizontalVectorAsVariantArray(ActualRow + AIndex, ActualColumn,
    ActualColumn2 - ActualColumn + 1, ARow, AErrorCode);
end;

function TdxSpreadSheetFormulaAreaReference.ForEach(AProc: TdxSpreadSheetForEachCallBack;
  const AData: Pointer; var AErrorCode: TdxSpreadSheetFormulaErrorCode): Boolean;
begin
  Result := ForEachCell(GetSheet, ActualColumn, ActualRow, ActualColumn2, ActualRow2, AProc, AData, AErrorCode);
end;

function TdxSpreadSheetFormulaAreaReference.ExtractReference(var ASheet: TdxSpreadSheetTableView;
  var AArea: TRect): Boolean;
begin
  Result := inherited ExtractReference(ASheet, AArea);
  if Result then
  begin
    AArea.Bottom := ActualRow2;
    AArea.Right := ActualColumn2;
    Result := (AArea.Bottom >= 0) and (AArea.Right >= 0);
  end;
end;

procedure TdxSpreadSheetFormulaAreaReference.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  if R1C1Reference then
    AttachString(AAsText, dxR1C1ReferenceAreaToString(AnchorRow, AnchorColumn, '', FRow, FColumn, '', FRow2, FColumn2))
  else
    AttachString(AAsText, ReferenceToString(FRow, FColumn) + dxAreaSeparator + ReferenceToString(FRow2, FColumn2));
end;

procedure TdxSpreadSheetFormulaAreaReference.UpdateReferences(AView: TdxSpreadSheetTableView; const AArea: TRect;
  AModification: TdxSpreadSheetCellsModification; AIsDeletion: Boolean);
begin
  if AView <> Sheet then Exit;
  inherited UpdateReferences(AView, AArea, AModification, AIsDeletion);
  if AModification in [cmShiftCellsVertically, cmShiftRows] then
  begin
    if (AModification <> cmShiftCellsVertically) or InRange(AnchorColumn, AArea.Left, AArea.Right) then
      FRow2.UpdateReference(AnchorRow, AArea.Top, AArea.Bottom, AIsDeletion)
  end
  else
    if (AModification <> cmShiftCellsHorizontally) or InRange(AnchorRow, AArea.Top, AArea.Bottom) then
      FColumn2.UpdateReference(AnchorColumn, AArea.Left, AArea.Right, AIsDeletion);
end;

function TdxSpreadSheetFormulaAreaReference.GetAbsoluteColumn2: Boolean;
begin
  Result := FColumn2.IsAbsolute;
end;

function TdxSpreadSheetFormulaAreaReference.GetAbsoluteRow2: Boolean;
begin
  Result := FRow2.IsAbsolute;
end;

function TdxSpreadSheetFormulaAreaReference.GetActualColumn2: Integer;
begin
  Result := FColumn2.ActualValue(AnchorColumn);
end;

function TdxSpreadSheetFormulaAreaReference.GetActualRow2: Integer;
begin
  Result := FRow2.ActualValue(AnchorRow);
end;

{ TdxSpreadSheetFormula3DAreaReference }

constructor TdxSpreadSheetFormula3DAreaReference.Create(ALink, ALink2: TdxSpreadSheet3DReferenceCustomLink;
  ARow, AColumn, ARow2, AColumn2: Integer; AAbsoluteRow, AAbsoluteColumn, AAbsoluteRow2, AAbsoluteColumn2: Boolean);
begin
  inherited Create(ARow, AColumn, ARow2, AColumn2, AAbsoluteRow, AAbsoluteColumn, AAbsoluteRow2, AAbsoluteColumn2);
  SetLink(FLink, ALink);
  SetLink(FLink2, ALink2);
end;

destructor TdxSpreadSheetFormula3DAreaReference.Destroy;
begin
  FreeAndNil(FLink);
  FreeAndNil(FLink2);
  inherited Destroy;
end;

function TdxSpreadSheetFormula3DAreaReference.ExtractReference(var ASheet: TdxSpreadSheetTableView; var AArea: TRect): Boolean;
begin
  Result := inherited ExtractReference(ASheet, AArea);
  ASheet := GetSheet;
  Result := Result and (ASheet <> nil)
end;

function TdxSpreadSheetFormula3DAreaReference.GetSheet: TdxSpreadSheetTableView;
begin
  Result := nil;
  if (Link <> nil) and TdxSpreadSheetInvalidObject.IsLive(Link.Data) then
    Result := TdxSpreadSheetTableView(Link.Data);
  if (Result = nil) and (Link2 <> nil) and TdxSpreadSheetInvalidObject.IsLive(Link2.Data) then
    Result := TdxSpreadSheetTableView(Link2.Data);
end;

procedure TdxSpreadSheetFormula3DAreaReference.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  if R1C1Reference then
    AttachString(AAsText, dxR1C1ReferenceAreaToString(
      AnchorRow, AnchorColumn, LinkToString(Link), FRow, FColumn, LinkToString(Link2), FRow2, FColumn2))
  else
    AttachString(AAsText, LinkToString(Link) + ReferenceToString(FRow, FColumn) +
      dxAreaSeparator + LinkToString(Link2) + ReferenceToString(FRow2, FColumn2));
end;

{ TdxSpreadSheetFormulaArrayToken }

function TdxSpreadSheetFormulaArrayToken.CanConvertStrToNumber: Boolean;
begin
  Result := False;
end;

function TdxSpreadSheetFormulaArrayToken.ExtractColumn(AIndex: Integer; out AColumn: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode): Boolean;
var
  ADimension: TdxSpreadSheetAreaDimension;
  AToken: TdxSpreadSheetFormulaToken;
  AValue: Variant;
  ARowIndex, AColumnIndex: Integer;
begin
  AColumn := Null;
  Result := False;
  ADimension := GetDimension(AErrorCode);
  if (AErrorCode = ecNone) and (AIndex >= ADimension.ColumnCount) then
    AErrorCode := ecRefErr;
  if AErrorCode <> ecNone then
    Exit;
  AToken := FirstChild;
  AColumn := VarArrayCreate([0, ADimension.RowCount - 1], varVariant);
  ARowIndex := 0;
  AColumnIndex := 0;
  while (AToken <> nil) and (AErrorCode = ecNone) do
  begin
    if AToken is TdxSpreadSheetFormulaArrayRowSeparator then
    begin
      Inc(ARowIndex);
      AColumnIndex := 0;
    end
    else
      if AToken.HasChildren then
      begin
        if not AToken.FirstChild.CanConvertStrToNumber then
          AErrorCode := ecValue
        else
        begin
          if AColumnIndex = AIndex then
          begin
            TTokenAccess(AToken.FirstChild).GetValue(AValue, AErrorCode);
            if AErrorCode = ecNone then
              AColumn[ARowIndex] := AValue;
          end;
          Inc(AColumnIndex);
        end;
      end
      else
        AErrorCode := ecValue;
    AToken := AToken.Next;
  end;
  Result := AErrorCode = ecNone;
  if not Result then
    AColumn := Null;
end;

function TdxSpreadSheetFormulaArrayToken.ExtractRow(AIndex: Integer; out  ARow: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode): Boolean;
var
  ADimension: TdxSpreadSheetAreaDimension;
  AToken: TdxSpreadSheetFormulaToken;
  AValue: Variant;
  ARowIndex, AColumnIndex: Integer;
begin
  ARow := Null;
  Result := False;
  ADimension := GetDimension(AErrorCode);
  if (AErrorCode = ecNone) and (AIndex >= ADimension.RowCount) then
    AErrorCode := ecRefErr;
  if AErrorCode <> ecNone then
    Exit;
  AToken := FirstChild;
  ARow := VarArrayCreate([0, ADimension.ColumnCount - 1], varVariant);
  ARowIndex := 0;
  AColumnIndex := 0;
  while (AToken <> nil) and (AErrorCode = ecNone) do
  begin
    if AToken is TdxSpreadSheetFormulaArrayRowSeparator then
      Inc(ARowIndex)
    else
    if ARowIndex = AIndex then
    begin
      if AToken.HasChildren then
      begin
        if not AToken.FirstChild.CanConvertStrToNumber then
          AErrorCode := ecValue
        else
        begin
          TTokenAccess(AToken.FirstChild).GetValue(AValue, AErrorCode);
          if AErrorCode = ecNone then
            ARow[AColumnIndex] := AValue;
          Inc(AColumnIndex);
        end;
      end
      else
        AErrorCode := ecValue;
    end;
    AToken := AToken.Next;
  end;
  Result := AErrorCode = ecNone;
  if not Result then
    ARow := Null;
end;

function TdxSpreadSheetFormulaArrayToken.ForEach(AProc: TdxSpreadSheetForEachCallBack; const AData: Pointer;
  var AErrorCode: TdxSpreadSheetFormulaErrorCode): Boolean;
var
  AValue: Variant;
  AToken: TdxSpreadSheetFormulaToken;
begin
  AToken := FirstChild;
  Result := False;
  while (AToken <> nil) and (AErrorCode = ecNone) do
  begin
    if not (AToken is TdxSpreadSheetFormulaArrayRowSeparator) then
    begin
      if AToken.HasChildren then
        TTokenAccess(AToken.FirstChild).GetValue(AValue, AErrorCode)
      else
        AErrorCode := ecValue;
      Result := (AErrorCode = ecNone) and AProc(AValue, CanConvertStrToNumber, AErrorCode, AData, nil) and (AErrorCode = ecNone);
    end;
    AToken := AToken.Next;
  end;
end;

function TdxSpreadSheetFormulaArrayToken.GetArray(var AErrorCode: TdxSpreadSheetFormulaErrorCode): Variant;
var
  ADimension: TdxSpreadSheetAreaDimension;
  AToken: TdxSpreadSheetFormulaToken;
  AValue: Variant;
  ARow, AColumn: Integer;
begin
  Result := Null;
  ADimension := GetDimension(AErrorCode);
  if AErrorCode <> ecNone then
    Exit;
  AToken := FirstChild;
  Result := VarArrayCreate([0, ADimension.RowCount - 1, 0, ADimension.ColumnCount - 1], varVariant);
  ARow := 0;
  AColumn := 0;
  while (AToken <> nil) and (AErrorCode = ecNone) do
  begin
    if AToken is TdxSpreadSheetFormulaArrayRowSeparator then
      Inc(ARow)
    else
    begin
      if AToken.HasChildren then
      begin
        if not AToken.FirstChild.CanConvertStrToNumber then
          AErrorCode := ecValue
        else
        begin
          TTokenAccess(AToken.FirstChild).GetValue(AValue, AErrorCode);
          if AErrorCode = ecNone then
            Result[ARow, AColumn] := AValue;
          Inc(AColumn);
        end;
      end
      else
        AErrorCode := ecValue;
    end;
    AToken := AToken.Next;
  end;
  if AErrorCode <> ecNone then
    Result := Null;
end;

function TdxSpreadSheetFormulaArrayToken.GetDimension(
  var AErrorCode: TdxSpreadSheetFormulaErrorCode): TdxSpreadSheetAreaDimension;
var
  AToken: TdxSpreadSheetFormulaToken;
  ACurrentRowTokenCount: Integer;
begin
  Result.RowCount := 1;
  Result.ColumnCount := 0;
  AToken := FirstChild;
  ACurrentRowTokenCount := 0;
  while (AToken <> nil) and (AErrorCode = ecNone) do
  begin
    if AToken is TdxSpreadSheetFormulaArrayRowSeparator then
    begin
      if Result.RowCount = 1 then
        Result.ColumnCount := ACurrentRowTokenCount
      else
        if Result.ColumnCount <> ACurrentRowTokenCount then
          AErrorCode := ecValue;
      Inc(Result.RowCount);
      ACurrentRowTokenCount := 0;
    end
    else
    begin
      if AToken.HasChildren then
        Inc(ACurrentRowTokenCount)
      else
        AErrorCode := ecValue;
    end;
    AToken := AToken.Next;
  end;

  if Result.RowCount = 1 then
    Result.ColumnCount := ACurrentRowTokenCount;
  if (AErrorCode <> ecNone) or (Result.ColumnCount = 0) then
  begin
    Result.RowCount := 0;
    Result.ColumnCount := 0;
  end;
end;

procedure TdxSpreadSheetFormulaArrayToken.GetValue(
  var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode);
var
  AToken: TdxSpreadSheetFormulaToken;
begin
  AToken := FirstChild;
  if AToken.HasChildren then
    TTokenAccess(AToken.FirstChild).GetValue(AValue, AErrorCode)
  else
    AErrorCode := ecValue;
end;

function TdxSpreadSheetFormulaArrayToken.IsEnumeration: Boolean;
begin
  Result := True;
end;

procedure TdxSpreadSheetFormulaArrayToken.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  AttachString(AAsText, dxLeftArrayParenthesis + ParametersToString + dxRightArrayParenthesis);
end;

{ TdxSpreadSheetFormulaVariantToken }

constructor TdxSpreadSheetFormulaVariantToken.Create(const AValue: Variant);
begin
  FValue := AValue;
end;

procedure TdxSpreadSheetFormulaVariantToken.GetValue(
  var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode);
begin
  AValue := FValue;
end;

{ TdxSpreadSheetListToken }

function TdxSpreadSheetListToken.ParametersToString: TdxUnicodeString;
var
  AToken: TdxSpreadSheetFormulaToken;
begin
  Result := '';
  AToken := FirstChild;
  if AToken <> nil then
  begin
    repeat
      if AToken.HasChildren then
      begin
        Result := Result + GetExpressionAsText(AToken.FirstChild);
        if (AToken.Next <> nil) and AToken.Next.HasChildren then
          Result :=  Result + FormatSettings.ListSeparator;
      end
      else
        Result := Result + FormatSettings.ArraySeparator;
      AToken := AToken.Next;
    until AToken = nil;
  end;
end;

procedure TdxSpreadSheetListToken.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  AttachString(AAsText, ParametersToString);
end;

{ TdxSpreadSheetFormulaFunctionToken }

constructor TdxSpreadSheetFormulaFunctionToken.Create(AInformation: TdxSpreadSheetFunctionInfo);
begin
  FInformation := AInformation;
end;

procedure TdxSpreadSheetFormulaFunctionToken.LoadFromStream(AReader: TcxReader);
begin
  FInformation := dxSpreadSheetFunctionsRepository.GetInfoByID(AReader.ReadInteger);
  inherited LoadFromStream(AReader);
end;

procedure TdxSpreadSheetFormulaFunctionToken.Calculate(AResult: TdxSpreadSheetFormulaResult);
begin
  if (FInformation <> nil) and Assigned(FInformation.Proc) then
    FInformation.Proc(AResult, FirstChild)
  else
    AResult.SetError(ecName);
end;

procedure TdxSpreadSheetFormulaFunctionToken.ToString(var AAsText: TdxSpreadSheetFormulaToken);
var
  AName: TdxUnicodeString;
begin
  AName := '';
  if Information <> nil then
  begin
    if Information.NamePtr <> nil then
      AName := FormatSettings.GetFunctionName(Information.NamePtr)
    else
      AName := Information.Name;
  end;
  AttachString(AAsText, AName + dxLeftParenthesis + ParametersToString + dxRightParenthesis);
end;

{ TdxSpreadSheetFormulaUnknownNameToken }

constructor TdxSpreadSheetFormulaUnknownNameToken.Create(const AName: TdxUnicodeString);
begin
  FName := AName;
end;

procedure TdxSpreadSheetFormulaUnknownNameToken.LoadFromStream(AReader: TcxReader);
begin
  FName := AReader.ReadWideString;
  inherited LoadFromStream(AReader);
end;

procedure TdxSpreadSheetFormulaUnknownNameToken.Calculate(AResult: TdxSpreadSheetFormulaResult);
begin
  AResult.SetError(ecName);
end;

procedure TdxSpreadSheetFormulaUnknownNameToken.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  AttachString(AAsText, Name);
end;

{ TdxSpreadSheetFormulaUnknownFunctionToken }

procedure TdxSpreadSheetFormulaUnknownFunctionToken.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  AttachString(AAsText, Name + dxLeftParenthesis + ParametersToString + dxRightParenthesis);
end;

{ TdxSpreadSheet3DReferenceCustomLink }

constructor TdxSpreadSheet3DReferenceCustomLink.Create(AData: TObject);
begin
  inherited Create;
  FData := AData;
end;

function TdxSpreadSheet3DReferenceCustomLink.ToString: TdxUnicodeString;
begin
  Result := '';
end;

{ TdxSpreadSheet3DReferenceLink }

function TdxSpreadSheet3DReferenceLink.ToString: TdxUnicodeString;
begin
  if Sheet = nil then
    Result := ''
  else
    if TdxSpreadSheetInvalidObject.IsInvalid(Sheet) then
      Result := serRefError
    else
    begin
      Result := Sheet.Caption;
      if TFormulaAccess(Owner).IsCaptionTextDelimited(Sheet) then
        Result :=  dxStringMarkChar2 + Result + dxStringMarkChar2;
      Result := Result + dxRefSeparator
    end;
end;

function TdxSpreadSheet3DReferenceLink.GetSheet: TdxSpreadSheetTableView;
begin
  Result := TdxSpreadSheetTableView(Data);
  Owner.ValidateSheet(Result);
  FData := Result;
end;

{ TdxSpreadSheet3DExternalReferenceLink }

constructor TdxSpreadSheet3DExternalReferenceLink.Create(AData: TObject; const AName: TdxUnicodeString);
begin
  inherited Create(AData);
  FName := AName;
end;

function TdxSpreadSheet3DExternalReferenceLink.ToString: TdxUnicodeString;
begin
  if ExternalLink = nil then
    Result := ''
  else
    if TdxSpreadSheetInvalidObject.IsInvalid(ExternalLink) then
      Result := serRefError
    else
      if Owner.FormatSettings.ExpandExternalLinks then
        Result := ExternalLink.ActualTarget
      else
        Result := dxReferenceLeftParenthesis + IntToStr(ExternalLink.Index + 1) + dxReferenceRightParenthesis;

  if Length(FName) > 0 then
  begin
    if (ExternalLink <> nil) and not TdxSpreadSheetInvalidObject.IsInvalid(ExternalLink) then
      Result := Result + dxRefSeparator + FName
    else
      Result := FName
  end;
end;

function TdxSpreadSheet3DExternalReferenceLink.GetExternalLink: TdxSpreadSheetExternalLink;
begin
  Result := TdxSpreadSheetExternalLink(Data);
  Owner.ValidateExternalLink(Result);
  FData := Result;
end;

{ TdxSpreadSheetDefinedNameToken }

procedure TdxSpreadSheetDefinedNameToken.Calculate(AResult: TdxSpreadSheetFormulaResult);
var
  ALink: TNameAccess;
begin
  ALink := TNameAccess(Link);
 if ALink <> nil then
   if (ALink.Formula <> nil) and (ALink.Formula.ErrorCode = ecNone) then
     AResult.AddResultValue(ALink.Formula.ResultValue)
 else
   AResult.SetError(ecName);
end;

function TdxSpreadSheetDefinedNameToken.CanConvertStrToNumber: Boolean;
begin
  Result := False;
end;

procedure TdxSpreadSheetDefinedNameToken.GetValue(
  var AValue: Variant; var AErrorCode: TdxSpreadSheetFormulaErrorCode);
begin
  inherited;
end;

procedure TdxSpreadSheetDefinedNameToken.ToString(var AAsText: TdxSpreadSheetFormulaToken);
begin
  AttachString(AAsText, Value);
end;

function TdxSpreadSheetDefinedNameToken.GetLink: TdxSpreadSheetDefinedName;
begin
  Result := SpreadSheet.DefinedNames.GetItemByName(Value, Owner.Sheet);
  if Result = nil then
    Result := SpreadSheet.DefinedNames.GetItemByName(Value, nil);
end;

{ TdxSpreadSheetFormulaParser }

constructor TdxSpreadSheetFormulaParser.Create(ASpreadSheet: TdxCustomSpreadSheet);
begin
  FSpreadSheet := ASpreadSheet;
  RegisterTokenControllers;
end;

function TdxSpreadSheetFormulaParser.ParseFormula(
  const AFormulaText: TdxUnicodeString; ACell: TdxSpreadSheetCell): Boolean;
begin
  FFormula := TdxSpreadSheetFormula.Create(ACell);
  try
    Result := ParseFormula(AFormulaText, FFormula);
    if Result then
      ACell.AsFormula := FFormula
    else
      FFormula.Free;
  except
    SetErrorIndex(Length(FFormula.SourceText));
    FFormula.Free;
    Result := False;
  end;
end;

function TdxSpreadSheetFormulaParser.ParseFormula(const AFormulaText: TdxUnicodeString; var AFormula: TdxSpreadSheetFormula): Boolean;

  function HasSpecialSymbolAtLeft(ASpacePos: Integer; const S: TdxUnicodeString): Boolean;
  begin
    Result := (S[ASpacePos-1] = dxAreaSeparator) or (S[ASpacePos-1] = dxLeftParenthesis) or
     (S[ASpacePos-1] = dxLeftArrayParenthesis) or (S[ASpacePos-1] = FormatSettings.ListSeparator) or
     (S[ASpacePos-1] = dxRefSeparator);
  end;

  function HasSpecialSymbolAtRight(AFirstSpacePos: Integer; const S: TdxUnicodeString): Boolean;
  var
    I: Integer;
  begin
    Result := False;
    for I := AFirstSpacePos + 1 to Length(S) do
      if S[I] <> ' ' then
      begin
        Result := (S[I] = dxAreaSeparator) or (S[I] = dxRightParenthesis) or (S[I] = dxRightArrayParenthesis) ;
        Break;
      end;
  end;

  function IsSymbolBeetweenMarkChars(ASymbolPos: Integer; const S: TdxUnicodeString): Boolean;
  var
    ACount, I: Integer;
  begin
    ACount := 0;
    for I := 1 to ASymbolPos - 1 do
      if SameStr(S[I], dxStringMarkChar) then
        Inc(ACount);
    Result := Odd(ACount);
  end;

  function CleanSpaces(const S: TdxUnicodeString): TdxUnicodeString;
  var
    I: Integer;
  begin
    I := 2;
    Result := S;
    while I <= Length(Result) do
      if (Result[I] = ' ') and (HasSpecialSymbolAtLeft(I, Result) or HasSpecialSymbolAtRight(I, Result)) and
         not IsSymbolBeetweenMarkChars(I, Result) then
        Delete(Result, I, 1)
      else
        Inc(I);
  end;

begin
  FFormula := AFormula;
  FFormulaSourceText := AFormulaText;
  Result := (Length(FFormulaSourceText) >= 1) and (FFormulaSourceText[1] = FormatSettings.Operations[opEQ]);
  if not Result then
  begin
    FreeAndNil(AFormula);
    Exit;
  end;
  FFormulaSourceText := CleanSpaces(Trim(Copy(FFormulaSourceText, 2, Length(AFormulaText) - 1)));
  FFormulaText := WideUpperCase(FFormulaSourceText);
  FFormula.SourceText := FFormulaSourceText;
  if CheckExtraChars then
  begin
    TFormulaAccess(FFormula).FTokens := DoFullParse(1, Length(FFormulaText));
    if CheckError then
      PostProcessFormulaTokens(TFormulaAccess(FFormula).FTokens)
    else
      Result := True;
  end;
{$IFDEF TESTPARSER}
  MessageBox(0, PChar(GetTreeAsText(FFormula.FTokens)), PChar(dxWideStringToString(FFormulaText)), 0);
{$ENDIF}
end;

function TdxSpreadSheetFormulaParser.IsBoolean(var APosition: Integer;
  ALength: Integer; out ANumber: TdxSpreadSheetFormulaToken): Boolean;
begin
  if CheckText(APosition, dxBoolToString[False]) then
    ANumber := TdxSpreadSheetFormulaBooleanValueToken.Create(False)
  else
  begin
    if CheckText(APosition, dxBoolToString[True]) then
      ANumber := TdxSpreadSheetFormulaBooleanValueToken.Create(True)
  end;
  Result := ANumber <> nil;
  if Result then
    Inc(APosition, Length(dxBoolToString[TdxSpreadSheetFormulaBooleanValueToken(ANumber).Value]))
end;

function TdxSpreadSheetFormulaParser.IsError(
  var APosition: Integer; ALength: Integer; out AError: TdxSpreadSheetFormulaToken): Boolean;
var
  ASavePos: Integer;
  ALastPos: Integer;
  ACode: TdxSpreadSheetFormulaErrorCode;
begin
  ASavePos := APosition;
  ALastPos := APosition + ALength;
  Result := GetError(APosition, ALastPos, ACode) and
    ((APosition >= ALastPos) or (not CheckText(APosition, dxAreaSeparator) and IsBreakChar(APosition)));
  if Result then
    AError := TdxSpreadSheetFormulaErrorValueToken.Create(ACode)
  else
    APosition := ASavePos;
end;

function TdxSpreadSheetFormulaParser.IsErrorReference(var APosition: Integer; AFinishPos: Integer): Boolean;
var
  ASavePos: Integer;
  ACode: TdxSpreadSheetFormulaErrorCode;
begin
  ASavePos := APosition;
  Result := GetError(APosition, AFinishPos, ACode) and (ACode = ecRefErr);
  if not Result then
    APosition := ASavePos;
end;

procedure TdxSpreadSheetFormulaParser.AddToken(
  var AList: TdxSpreadSheetFormulaToken; AToken: TdxSpreadSheetFormulaToken);
begin
  if AList = nil then
    AList := AToken
  else
    TFormulaAccess(Formula).Add(AList.LastSibling, AToken);
end;

procedure TdxSpreadSheetFormulaParser.AddTokenFromStack(var AList, AStack: TdxSpreadSheetFormulaToken);
var
  AItem: TdxSpreadSheetFormulaToken;
begin
  AItem := AStack.LastSibling;
  AStack := AItem.Prev;
  AItem.ExtractFromList;
  AddToken(AList, AItem);
end;

function TdxSpreadSheetFormulaParser.IsArraySeparator(const APosition: Integer): Boolean;
begin
  Result := CheckText(APosition, FormatSettings.ArraySeparator);
end;

function TdxSpreadSheetFormulaParser.IsBreakChar(APosition: Integer): Boolean;
var
  I: Integer;
begin
  Result := APosition <= Length(FFormulaText);
  if not Result then Exit;
  for I := 1 to Length(FormatSettings.BreakChars) do
  begin
    Result := FFormulaText[APosition] = FormatSettings.BreakChars[I];
    if Result then Break;
  end;
end;

function TdxSpreadSheetFormulaParser.IsABCChar(APosition: Integer): Boolean;
begin
  Result := (Integer(FFormulaText[APosition]) >= Integer(WideChar('A'))) and
    (Integer(FFormulaText[APosition]) <= Integer(WideChar('Z')));
end;

function TdxSpreadSheetFormulaParser.IsArray(var APosition: Integer;
  ALength: Integer; out AArray: TdxSpreadSheetFormulaToken): Boolean;
var
  L: Integer;
begin
  Result := CheckText(APosition, dxLeftArrayParenthesis);
  if not Result then Exit;
  L := GetSubExpressionLength(APosition, ALength) + APosition;
  AArray := TdxSpreadSheetFormulaArrayToken.Create();
  GetParameters(AArray, APosition + 1, L);
  APosition := L + 2;
end;

function TdxSpreadSheetFormulaParser.IsFunction(var APosition: Integer;
  ALength: Integer; out AFunction: TdxSpreadSheetFormulaToken): Boolean;
var
  I, J: Integer;
  AInfo: TdxSpreadSheetFunctionInfo;
begin
  Result := False;
  I := APosition + 1;
  while not Result and (I < APosition + ALength) do
  begin
    Result := CheckText(I, dxLeftParenthesis);
    Inc(I);
  end;
  if not Result then Exit;
  AInfo := dxSpreadSheetFunctionsRepository.GetInfoByName(@FFormulaText[APosition], I - APosition - 1);
  Result := AInfo <> nil;
  if Result then
  begin
    AFunction := TdxSpreadSheetFormulaFunctionToken.Create(AInfo);
    J := GetSubExpressionLength(I - 1, ALength - (I - 1 - APosition)) + I - 1;
    GetParameters(AFunction, I,  + J);
    APosition := J + 2;
  end;
end;

function TdxSpreadSheetFormulaParser.IsListSeparator(const APosition: Integer): Boolean;
begin
  Result := CheckText(APosition, FormatSettings.ListSeparator);
end;

function TdxSpreadSheetFormulaParser.IsName(var APosition: Integer;
  ALength: Integer; out AName: TdxSpreadSheetFormulaToken): Boolean;
var
  ASavePos: Integer;
  ACandidate: TdxUnicodeString;
begin
  ASavePos := APosition;
  ACandidate := GetStringPart(APosition, ALength);
  Result := SpreadSheet.DefinedNames.IndexOf(ACandidate) >= 0;
  if Result then
    AName := TdxSpreadSheetDefinedNameToken.Create(Copy(FFormulaSourceText, ASavePos, Length(ACandidate)))
  else
    APosition := ASavePos;
end;

function TdxSpreadSheetFormulaParser.IsNumber(var APosition: Integer;
  ALength: Integer; out ANumber: TdxSpreadSheetFormulaToken): Boolean;
var
  I: Integer; 
  AIntegerValue: Integer;
  AFloatValue: Double;
  //
  APartAsString: TdxUnicodeString;
  AHasSeparator, AHasExponent, AHasExponentSign: Boolean;
  ANumberCharCount: Integer;
begin
  ANumber := nil;
  I := APosition;
  if not IsBoolean(APosition, ALength, ANumber) then
  begin
    ANumberCharCount := 0;
    AHasExponent := False;
    AHasSeparator := False;
    AHasExponentSign := False;
    while I <= (APosition + ALength) do
    begin
      if dxWideIsNumeric(FFormulaText[I]) then
        Inc(ANumberCharCount)
      else
        if not AHasSeparator and CheckText(I, FormatSettings.DecimalSeparator) then
          AHasSeparator := True
        else
          if not AHasExponent and CheckText(I, dxExponentChar) and (ANumberCharCount > 0) then
            AHasExponent := True
          else
            if (AHasExponent and not AHasExponentSign) and CheckSignChar(I) then
              AHasExponentSign := True
            else
            Break;
      Inc(I);
    end;
    AHasSeparator := AHasSeparator or AHasExponent;
    if (ANumberCharCount > 0) and not CheckText(I, dxAreaSeparator) then
    begin
      APartAsString := Copy(FFormulaText, APosition, I - APosition);
      if not AHasSeparator and TryStrToInt(APartAsString, AIntegerValue) then
        ANumber := TdxSpreadSheetFormulaIntegerValueToken.Create(AIntegerValue)
      else
        if dxTryStrToFloat(APartAsString, AFloatValue, FormatSettings.Data) then
          ANumber := TdxSpreadSheetFormulaFloatValueToken.Create(AFloatValue);
    end;
  end
  else
    I := APosition;
  Result := ANumber <> nil;
  if Result then
    APosition := I;
end;

function TdxSpreadSheetFormulaParser.IsOperation(var APosition: Integer;
  ALength: Integer; out AOperation: TdxSpreadSheetFormulaToken): Boolean;
var
  I, J: TdxSpreadSheetFormulaOperation;
begin
  Result := False;
  AOperation := nil;
  J := Low(FormatSettings.Operations);
  for I := Low(FormatSettings.Operations) to High(FormatSettings.Operations) do
    if CheckText(APosition, FormatSettings.Operations[I]) and
      (not Result or (Length(FormatSettings.Operations[J]) < Length(FormatSettings.Operations[I]))) then
    begin
      J := I;
      Result := True;
    end;
  if Result then
  begin
    if J = opIsect then 
      AOperation := TdxSpreadSheetFormulaAttributeToken.Create
    else
      AOperation := TdxSpreadSheetFormulaOperationToken.Create(J);
    Inc(APosition, Length(FormatSettings.Operations[J]));
  end;
end;

function TdxSpreadSheetFormulaParser.IsSeparator(var APosition: Integer): Boolean;
begin
  Result := FFormulaText[APosition] = FormatSettings.ListSeparator;
end;

function TdxSpreadSheetFormulaParser.IsString(var APosition: Integer;
  ALength: Integer; out AString: TdxSpreadSheetFormulaToken): Boolean;
var
  L: Integer;
begin
  Result := IsStringMark(APosition);
  if Result then
  begin
    L := GetStringLength(APosition, ALength);
    if ((L + 2) < ALength) and CheckText(APosition + L + 2, dxRefSeparator) then
    begin
      Result := False;
      Exit;
    end;
    Result := not CheckText(APosition + L + 2, dxRefSeparator);
    if Result then
    begin
      AString := TdxSpreadSheetFormulaStringValueToken.Create(Copy(FFormulaSourceText, APosition + 1, L));
      Inc(APosition, L + 2);
    end; 
  end;
end;

function TdxSpreadSheetFormulaParser.IsStringMark(APosition: Integer; ACheckAdditionalMark: Boolean): Boolean;
begin
 Result := CheckText(APosition, dxStringMarkChar) or (ACheckAdditionalMark and CheckText(APosition, dxStringMarkChar2));
end;

function TdxSpreadSheetFormulaParser.IsSubExpression(var APosition: Integer;
  ALength: Integer; out AExpression: TdxSpreadSheetFormulaToken): Boolean;
var
  L: Integer;
begin
  Result := CheckText(APosition, dxLeftParenthesis);
  if not Result then Exit;
  L := GetSubExpressionLength(APosition, ALength);
  AExpression := TdxSpreadSheetFormulaParenthesesToken.Create();
  if L > 0 then
    TFormulaAccess(Formula).AddChild(AExpression, DoFullParse(APosition + 1, APosition + L));
  Inc(APosition, L + 2);
end;

function TdxSpreadSheetFormulaParser.IsUnknown(var APosition: Integer;
  ALength: Integer; out AToken: TdxSpreadSheetFormulaToken): Boolean;
var
  I, J: Integer;
  AName: TdxUnicodeString;
begin
  I := APosition;
  while ((FFormulaText[I] = ' ') or not IsBreakChar(I)) and (I <= APosition + ALength) do
    Inc(I);
  Result := I > APosition;
  if not Result then Exit; 
  AName := Copy(FFormulaSourceText, APosition, I - APosition);
  if CheckText(I, dxLeftParenthesis) then
  begin
    AToken := TdxSpreadSheetFormulaUnknownFunctionToken.Create(AName);
    J := GetSubExpressionLength(I, ALength - (I - APosition)) + I;
    GetParameters(AToken, I + 1, J);
    APosition := J + 2;
  end
  else
  begin
    AToken := TdxSpreadSheetFormulaUnknownNameToken.Create(AName);
    APosition := I;
  end;
end;

function TdxSpreadSheetFormulaParser.IsReference(var APosition: Integer;
  ALength: Integer; out AReference: TdxSpreadSheetFormulaToken): Boolean;
var
  AIsArea: Boolean;
  ASavePos, AFinishPos, ARow, ARow2, ACol, ACol2: Integer;
  AAbsRow, AAbsRow2, AAbsCol, AAbsCol2: Boolean;
  ALink, ALink2: TdxSpreadSheet3DReferenceCustomLink;
begin
  // todo: need check maximu XFD column number
  AIsArea := True;
  ASavePos := APosition;
  AFinishPos := APosition + ALength;
  ALink := nil;
  ALink2 := nil;
  CheckSheetName(APosition, AFinishPos, ALink);
  Result := CheckReference(APosition, AFinishPos, ARow, ACol, AAbsRow, AAbsCol);
  if not Result then
    Result := CheckFullColumnRowReference(APosition, AFinishPos, ALink2, ARow, ACol, ARow2, ACol2,
      AAbsRow, AAbsCol, AAbsRow2, AAbsCol2)
  else
    if CheckAreaSeparator(APosition, AFinishPos) then
    begin
      CheckSheetName(APosition, AFinishPos, ALink2);
      Result := CheckReference(APosition, AFinishPos, ARow2, ACol2, AAbsRow2, AAbsCol2);
    end
    else
      AIsArea := False;
  if Result then
    AReference := MakeReference(ALink, ALink2, ARow, ACol, ARow2, ACol2, AAbsRow, AAbsCol, AAbsRow2, AAbsCol2, AIsArea)
  else
    if (ALink <> nil) or (ALink2 <> nil) then
    begin
      FreeAndNil(ALink);
      FreeAndNil(ALink2);
      SetErrorIndex(APosition);
    end;
  if not Result then
    APosition := ASavePos;
end;

function TdxSpreadSheetFormulaParser.CheckExtraChars: Boolean;
var
  AIndex: Integer;
begin
  AIndex := 1;
  while AIndex <= Length(FFormulaText) do
  begin
    if IsStringMark(AIndex) then
      Inc(AIndex, GetStringLength(AIndex, Length(FFormulaText)) + 1)
    else
      if CheckText(AIndex, dxStringMarkChar2) then
      begin
        Inc(AIndex);
        while (AIndex <= Length(FFormulaText)) and not CheckText(AIndex, dxStringMarkChar2) do
          Inc(AIndex);
      end
      else
        if (FFormulaText[AIndex] = dxLeftParenthesis) or (FFormulaText[AIndex] = dxLeftArrayParenthesis)  then
          Inc(AIndex, GetSubExpressionLength(AIndex, Length(FFormulaText)) + 1);
    Inc(AIndex);
  end;
  Result := (AIndex - Length(FFormulaText) = 1) and CheckError;
  if not Result then
     SetErrorIndex(Length(FFormulaText));
end;

function TdxSpreadSheetFormulaParser.CheckError: Boolean;
begin
  Result := Formula.ErrorIndex = 0;
end;

function TdxSpreadSheetFormulaParser.CheckReference(var APosition: Integer; AFinishPos: Integer;
  var ARow, AColumn: Integer; var AbsRow, AbsColumn: Boolean): Boolean;
var
  ASavePos: Integer;
begin
  ASavePos := APosition;
  ARow := -MaxInt;
  ARow := -MaxInt;
  Result := IsErrorReference(APosition, AFinishPos);
  if Result then Exit;
  if R1C1Reference then
    Result := CheckRCReference(APosition, AFinishPos, ARow, AColumn, AbsRow, AbsColumn)
  else
    Result := CheckStandardReference(APosition, AFinishPos, ARow, AColumn, AbsRow, AbsColumn);
  if not Result then
    APosition := ASavePos;
end;

function TdxSpreadSheetFormulaParser.CheckRCReference(var APosition: Integer; AFinishPos: Integer;
  var ARow, AColumn: Integer; var AbsRow, AbsColumn: Boolean): Boolean;
begin
  Result := CheckRCReferencePart(APosition, AFinishPos, dxRCRowReferenceChar, ARow, AbsRow) and
    CheckRCReferencePart(APosition, AFinishPos, dxRCColumnReferenceChar, AColumn, AbsColumn)
end;

function TdxSpreadSheetFormulaParser.CheckRCReferencePart(var APosition: Integer; AFinishPos: Integer;
  const APrefix: TdxUnicodeString; var AIndex: Integer; var AAbsIndex: Boolean): Boolean;
var
  ASavePos: Integer;
begin
  ASavePos := APosition;
  Result := CheckText(APosition, APrefix);
  Inc(APosition);
  AAbsIndex := Result and not CheckText(APosition, dxReferenceLeftParenthesis);
  if not AAbsIndex then
    Inc(APosition);
  if Result then
  begin
    if not AAbsIndex then
      Result := GetIntReference(APosition, AFinishPos, AIndex) and CheckText(APosition, dxReferenceRightParenthesis)
    else
    begin
      AAbsIndex := GetIntReference(APosition, AFinishPos, AIndex);
      if not AAbsIndex then
        AIndex := 0;
    end;
  end;
  if Result and not AAbsIndex and CheckText(APosition, dxReferenceRightParenthesis) then
    Inc(APosition);
  if not Result then
    APosition := ASavePos; 
end;

function TdxSpreadSheetFormulaParser.CheckAbsoluteReference(
  var AStartPos: Integer; AFinishPos: Integer; var AbsoluteReference: Boolean): Boolean;
begin
  Result := AStartPos <= AFinishPos;
  if not Result then Exit;
  AbsoluteReference := CheckText(AStartPos, dxAbsoluteReferenceChar);
  if AbsoluteReference then
    Inc(AStartPos);
end;

function TdxSpreadSheetFormulaParser.CheckAreaSeparator(
  var AStartPos: Integer; AFinishPos: Integer): Boolean;
begin
  Result := (AStartPos < AFinishPos) and CheckText(AStartPos, dxAreaSeparator);
  if Result then
    Inc(AStartPos);
end;

function TdxSpreadSheetFormulaParser.CheckColumnReference(
  var AStartPos: Integer; ALength: Integer; var AColumn: Integer): Boolean;
var
  I: Integer;
  AName: TdxUnicodeString;
begin
  Result := ALength > 0;
  if not Result then Exit;
  I := AStartPos;
  repeat
    if IsABCChar(I) then
      Inc(I)
    else
    begin
      Dec(I);
      Break;
    end;
  until I > (AStartPos + ALength);
  if I > (AStartPos + ALength) then
    Dec(I);
  I := I - AStartPos + 1;
  Result := (I > 0) and (I <= 3);
  if not Result then Exit;
  AName := Copy(FFormulaText, AStartPos, I);
  AColumn := TdxSpreadSheetColumnHelper.IndexByName(AName, False);
  Inc(AStartPos, I);
end;

function TdxSpreadSheetFormulaParser.CheckFullColumnRowReference(
  var AStartPos: Integer; AFinishPos: Integer; var ALink: TdxSpreadSheet3DReferenceCustomLink;
  var ARow, ACol, ARow2, ACol2: Integer; var AAbsRow, AAbsCol, AAbsRow2, AAbsCol2: Boolean): Boolean;
var
  ASavePos: Integer;
begin
  ASavePos := AStartPos;
  ARow := 0;
  ACol := 0;
  ARow2 := MaxInt;
  ACol2 := MaxInt;
  if R1C1Reference then
  begin
    Result := CheckRCReferencePart(AStartPos, AFinishPos, dxRCRowReferenceChar, ARow, AAbsRow);
    if Result then
    begin
      if CheckAreaSeparator(AStartPos, AFinishPos) then
        Result := CheckRCReferencePart(AStartPos, AFinishPos, dxRCRowReferenceChar, ARow2, AAbsRow2)
      else
      begin
        ARow2 := ARow;
        AAbsRow2 :=  AAbsRow;
      end;
    end
    else
    begin
      Result := CheckRCReferencePart(AStartPos, AFinishPos, dxRCColumnReferenceChar, ACol, AAbsCol);
      if CheckAreaSeparator(AStartPos, AFinishPos) then
        Result := CheckRCReferencePart(AStartPos, AFinishPos, dxRCColumnReferenceChar, ACol2, AAbsCol2)
      else
      begin
        ACol2 := ACol;
        AAbsCol2 :=  AAbsCol;
      end;
    end;
  end
  else
  begin
    Result := CheckAbsoluteReference(AStartPos, AFinishPos, AAbsCol) and
      CheckColumnReference(AStartPos, AFinishPos, ACol);
    if Result then
    begin
      Result := CheckAreaSeparator(AStartPos, AFinishPos);
      if Result then
        CheckSheetName(AStartPos, AFinishPos, ALink);
      Result := Result and CheckAbsoluteReference(AStartPos, AFinishPos, AAbsCol2) and
        CheckColumnReference(AStartPos, AFinishPos, ACol2);
    end
    else
    begin
      AStartPos := ASavePos;
      Result := CheckAbsoluteReference(AStartPos, AFinishPos, AAbsRow) and
        CheckRowReference(AStartPos, AFinishPos, ARow);
      if Result then
      begin
        Result := CheckAreaSeparator(AStartPos, AFinishPos);
        if Result then
          CheckSheetName(AStartPos, AFinishPos, ALink);
        Result := Result and CheckAbsoluteReference(AStartPos, AFinishPos, AAbsRow2) and
          CheckRowReference(AStartPos, AFinishPos, ARow2);
      end;
    end;
  end;
  if not Result then
    AStartPos := ASavePos;
end;

function TdxSpreadSheetFormulaParser.CheckRowReference(
  var AStartPos, AFinishPos: Integer; var ARow: Integer): Boolean;
begin
  Result := GetIntReference(AStartPos, AFinishPos, ARow);
end;

function TdxSpreadSheetFormulaParser.CheckSignChar(const APosition: Integer): Boolean;
begin
  Result := CheckText(APosition, FormatSettings.Operations[opAdd]) or CheckText(APosition, FormatSettings.Operations[opSub]);  
end;

function TdxSpreadSheetFormulaParser.CheckStandardReference(var APosition: Integer; AFinishPos: Integer;
  var ARow, AColumn: Integer; var AbsRow, AbsColumn: Boolean): Boolean;
var
  ASavePos: Integer;
begin
  ASavePos := APosition;
  Result := CheckAbsoluteReference(APosition, AFinishPos, AbsColumn) and CheckColumnReference(APosition, AFinishPos, AColumn) and
    CheckAbsoluteReference(APosition, AFinishPos, AbsRow) and CheckRowReference(APosition, AFinishPos, ARow);
  if not Result then
    APosition := ASavePos;
end;

function TdxSpreadSheetFormulaParser.CheckSheetName(var APosition: Integer; AFinishPos: Integer;
  var ALink: TdxSpreadSheet3DReferenceCustomLink): Boolean;
var
  HasStringMark: Boolean;
  ASavePos, L, ALinkID: Integer;
  ASheet: TdxSpreadSheetTableView;
begin
  ALink := nil;
  ASavePos := APosition;
  HasStringMark := IsStringMark(APosition, True);
  if HasStringMark then
    Inc(APosition);
  L := APosition;
  Result := IsErrorReference(APosition, AFinishPos) and not IsBreakChar(APosition);
  if Result then
  begin
    ALink := TdxSpreadSheet3DReferenceLink.Create(TdxSpreadSheetInvalidObject.Instance);
    Exit;
  end;
  APosition := L;
  if CheckText(APosition, dxReferenceLeftParenthesis) then
  begin
    Inc(APosition);
    if GetIntValue(APosition, AFinishPos, ALinkID) and CheckText(APosition, dxReferenceRightParenthesis) then
    begin
      ALink := TdxSpreadSheet3DExternalReferenceLink.Create(SpreadSheet.ExternalLinks[ALinkID - 1]);
      Inc(APosition);
    end
    else
      APosition := L;
  end;
  L := 0;
  while not CheckText(APosition + L, dxRefSeparator) and (APosition + L < AFinishPos) do
    Inc(L);
  if ALink <> nil then
  begin
    Result := L > 0;
    if Result then
      TdxSpreadSheet3DExternalReferenceLink(ALink).FName := Copy(FFormulaSourceText, APosition, L - 1)
    else
      FreeAndNil(ALink); 
  end
  else
  begin
    Result := GetSheetByName(APosition, L - Byte(HasStringMark), ASheet);
    if Result then
      ALink := TdxSpreadSheet3DReferenceLink.Create(ASheet);
  end;
  if Result then
  begin
    Inc(APosition, L);
    if CheckText(APosition, dxRefSeparator) then
      Inc(APosition);
  end
  else
    APosition := ASavePos;
end;

function TdxSpreadSheetFormulaParser.CheckText(const APosition: Integer; const ACandidate: TdxUnicodeString): Boolean;
begin
  Result := (Length(ACandidate) > 0) and (APosition + Length(ACandidate) - 1 <= Length(FFormulaText)) and
    CompareMem(@FFormulaText[APosition], @ACandidate[1], Length(ACandidate) * SizeOf(TdxUnicodeChar));
end;

function TdxSpreadSheetFormulaParser.CheckUnaryOperation(AValue: TdxSpreadSheetFormulaToken;
  AOperation: TdxSpreadSheetFormulaOperationToken): Boolean;
var
  ASign: TValueSign;
begin
  ASign := 0;
  if AOperation.Operation = opUplus then
    ASign := 1
  else
    if AOperation.Operation = opUminus then
      ASign := -1;
  Result := ASign <> 0;
  if not Result then Exit;
  if (AValue is TdxSpreadSheetFormulaIntegerValueToken) and (Sign(TdxSpreadSheetFormulaIntegerValueToken(AValue).Value) <> ASign) then
    TdxSpreadSheetFormulaIntegerValueToken(AValue).FValue := TdxSpreadSheetFormulaIntegerValueToken(AValue).FValue * ASign
  else
    if (AValue is TdxSpreadSheetFormulaFloatValueToken) and (Sign(TdxSpreadSheetFormulaFloatValueToken(AValue).Value) <> ASign) then
      TdxSpreadSheetFormulaFloatValueToken(AValue).FValue := TdxSpreadSheetFormulaFloatValueToken(AValue).FValue * ASign
    else
      if (AValue is TdxSpreadSheetFormulaCurrencyValueToken) and (Sign(TdxSpreadSheetFormulaCurrencyValueToken(AValue).Value) <> ASign) then
        TdxSpreadSheetFormulaCurrencyValueToken(AValue).FValue := TdxSpreadSheetFormulaCurrencyValueToken(AValue).FValue * ASign
      else
        if (AValue is TdxSpreadSheetFormulaDateTimeValueToken) and (Sign(TdxSpreadSheetFormulaDateTimeValueToken(AValue).Value) <> ASign) then
          TdxSpreadSheetFormulaDateTimeValueToken(AValue).FValue := TdxSpreadSheetFormulaDateTimeValueToken(AValue).FValue * ASign
        else
          Result := False;
end;

function TdxSpreadSheetFormulaParser.DoFullParse(AStartPos, AFinishPos: Integer): TdxSpreadSheetFormulaToken;
var
  ASavePos: Integer;
begin
  ASavePos := AStartPos;
  Result := DoParse(AStartPos, AFinishPos);
  if not CheckError or ((AStartPos < AFinishPos) and not IsListSeparator(AStartPos)) then
    SetErrorIndex(AStartPos)
  else
    if AStartPos < AFinishPos then
    begin
      AStartPos := ASavePos;
      TFormulaAccess(Formula).DestroyTokens(Result);
      Result := TdxSpreadSheetListToken.Create;
      TFormulaAccess(Formula).SetAsOwner(Result);
      GetParameters(Result, AStartPos, AFinishPos);
    end;
end;

function TdxSpreadSheetFormulaParser.DoParse(var AStartPos, AFinishPos: Integer): TdxSpreadSheetFormulaToken;
var
  AToken, ANextToken, AStack: TdxSpreadSheetFormulaToken;
begin
  Result := nil;
  AStack := nil;
  AToken := PrepareTokensFromString(AStartPos, AFinishPos);
  if AToken = nil then Exit;  
  while AToken <> nil do
  begin
    ANextToken := AToken.Next;
    AToken.ExtractFromList;
    if AToken.Priority = -1 then
      AddToken(Result, AToken)
    else
    begin
      AStack := AStack.LastSibling;
      while (AStack <> nil) and (AStack.Priority >= AToken.Priority) do
        AddTokenFromStack(Result, AStack);
      AddToken(AStack,  AToken);
    end;
    AToken := ANextToken;
  end;
  while AStack <> nil do
    AddTokenFromStack(Result, AStack); 
{$IFDEF TESTPARSER}
   MessageBox(0, PChar(GetTreeAsText(Result)), PChar(dxWideStringToString(Copy(FFormulaText, AStartPos, AFinishPos - AStartPos))), 0);
 {$ENDIF}
end;

function TdxSpreadSheetFormulaParser.GetStringLength(APosition, ALength: Integer): Integer;
begin
  Result := 0;
  if CheckText(APosition + Result, dxStringMarkChar) then
    Inc(APosition)
  else
    Exit; 
  while Result <= ALength do
  begin
    if CheckText(APosition + Result, dxStringMarkChar) then
    begin
      if (Result < ALength) and CheckText(APosition + Result + 1, dxStringMarkChar) then
        Inc(Result)
      else
        Break;
    end
    else
      Inc(Result)
  end;
  if Result > ALength then
    SetErrorIndex(APosition); 
end;

function TdxSpreadSheetFormulaParser.GetStringPart(var APosition: Integer; ALength: Integer): TdxUnicodeString;
var
  ALen: Integer;
begin
  ALen := 0;
  while (ALen < ALength) and not IsBreakChar(APosition + ALen) do
    Inc(ALen);
  Result := GetSubString(APosition, ALen);
  Inc(APosition, ALen);
end;

function TdxSpreadSheetFormulaParser.GetSubString(const APosition, ALength: Integer): TdxUnicodeString;
begin
  SetLength(Result, ALength);
  if ALength > 0 then
    Move(FFormulaText[APosition], Result[1], ALength * SizeOf(TdxUnicodeChar));
end;

function TdxSpreadSheetFormulaParser.GetError(
  var AStartPos, AFinishPos: Integer; var ACode: TdxSpreadSheetFormulaErrorCode): Boolean;

  function CheckCode(const AText: TdxUnicodeString; AValue: TdxSpreadSheetFormulaErrorCode): Boolean;
  begin
    Result := dxSpreadSheetCompareText(AText, @FFormulaText[AStartPos], Length(AText)) = 0;
    if Result then
    begin
      Inc(AStartPos, Length(AText));
      ACode := AValue;
    end;
  end;

begin
  Result := CheckText(AStartPos, dxErrorPrefix) and (
    CheckCode(serNullError, ecNull) or
    CheckCode(serDivZeroError, ecDivByZero) or
    CheckCode(serValueError, ecValue) or
    CheckCode(serRefError, ecRefErr) or
    CheckCode(serNameError, ecName) or
    CheckCode(serNumError, ecNUM) or
    CheckCode(serNAError, ecNA));
end;

function TdxSpreadSheetFormulaParser.GetIntReference(var AStartPos, AFinishPos: Integer; var AValue: Integer): Boolean;
begin
  Result := GetIntValue(AStartPos, AFinishPos, AValue);
  if Result and not R1C1Reference then
    if AValue > 0 then
      Dec(AValue)
    else
      Result := False;
end;

function TdxSpreadSheetFormulaParser.GetIntValue(var AStartPos, AFinishPos: Integer; var AValue: Integer): Boolean;
var
  APos: Integer;
  AHasSign: Boolean;
begin
  Result := AStartPos <= AFinishPos;
  if not Result then Exit;
  AValue := 0;
  AHasSign := CheckText(AStartPos, FormatSettings.Operations[opAdd]) or CheckText(AStartPos, FormatSettings.Operations[opSub]);
  APos := AStartPos + Byte(AHasSign);
  repeat
    if (Integer(FFormulaText[APos]) >= Integer(WideChar('0'))) and (Integer(FFormulaText[APos]) <= Integer(WideChar('9'))) then
      Inc(APos)
    else
    begin
      Dec(APos);
      Break;
    end;
  until APos > AFinishPos;
  if APos > AFinishPos then
    Dec(APos);
  Result := (APos - Byte(AHasSign) >= AStartPos ) and
    TryStrToInt(Copy(FFormulaText, AStartPos, APos - AStartPos + 1), AValue);
  if not Result then Exit;
  AStartPos := APos + 1;
end;

function TdxSpreadSheetFormulaParser.GetParameters(AParent: TdxSpreadSheetFormulaToken; AStartPos, AFinishPos: Integer): Boolean;

  procedure AddParameter(ATokens: TdxSpreadSheetFormulaToken);
  var
    AParameter: TdxSpreadSheetFormulaToken;
  begin
    if CheckError then
    begin
      AParameter := TdxSpreadSheetFormulaToken.Create;
      TFormulaAccess(Formula).AddChild(AParameter, ATokens);
      TFormulaAccess(Formula).AddChild(AParent, AParameter);
    end
    else
      TFormulaAccess(Formula).DestroyTokens(ATokens);
  end;

begin
  while CheckError and (AStartPos <= AFinishPos) do
  begin
    AddParameter(DoParse(AStartPos, AFinishPos));
    if IsListSeparator(AStartPos) then
    begin
      while AStartPos <= AFinishPos do
      begin
        Inc(AStartPos);
        if (AStartPos > AFinishPos) or IsListSeparator(AStartPos) then
          AddParameter(TdxSpreadSheetFormulaNullToken.Create)
        else
          Break;
      end;
    end
    else
      if IsArraySeparator(AStartPos) then
      begin
        TFormulaAccess(Formula).AddChild(AParent, TdxSpreadSheetFormulaArrayRowSeparator.Create());
        Inc(AStartPos)
      end
      else
        if AStartPos < AFinishPos then
          SetErrorIndex(AStartPos)
        else
          Break;
  end;
  Result := CheckError;
end;

function TdxSpreadSheetFormulaParser.GetSheetByName(APosition, ALength: Integer;
  var ASheet: TdxSpreadSheetTableView): Boolean;
var
  ASheetIndex: Integer;
begin
  ASheet := nil;
  Result := False;
  for ASheetIndex := 0 to SpreadSheet.SheetCount - 1 do
    if dxSpreadSheetCompareText(SpreadSheet.Sheets[ASheetIndex].Caption, @FFormulaText[APosition], ALength) = 0 then
    begin
      ASheet := SpreadSheet.Sheets[ASheetIndex] as TdxSpreadSheetTableView;
      Result := True;
      Break;
    end;
end;

function TdxSpreadSheetFormulaParser.GetSubExpressionLength(APosition, ALength: Integer): Integer;
var
  I, C: Integer;
begin
  C := 1;
  Result := 0;
  I := APosition + 1;
  while I <= (APosition + ALength) do
  begin
    if CheckText(I, dxStringMarkChar) then
      Inc(I, GetStringLength(I, ALength - (I - APosition)) + 1)
    else
      if CheckText(I, dxStringMarkChar2) then
      begin
        Inc(I);
        while (I <= Length(FFormulaText)) and not CheckText(I, dxStringMarkChar2) do
          Inc(I);
      end
      else
        if CheckText(I, dxLeftParenthesis) or CheckText(I, dxLeftArrayParenthesis) then
          Inc(I, GetSubExpressionLength(I, ALength - (I - APosition)) + 1)
        else
          if CheckText(I, dxRightParenthesis) or CheckText(I, dxRightArrayParenthesis) then
          begin
            Dec(C);
            if C = 0 then
            begin
              Result := I - APosition - 1;
              Break;
            end;
          end;
      Inc(I);
  end;
end;

function TdxSpreadSheetFormulaParser.GetNextToken(
  var APosition: Integer; const ALength: Integer): TdxSpreadSheetFormulaToken;
var
  I: Integer; 
begin
  Result := nil;
  if FFormulaText[APosition] = '' then
  begin
    Inc(APosition);
    Exit;
  end;
  for I := 0 to High(TokenControllers) do
    if TokenControllers[I](APosition, ALength, Result) then
      Break;
  if Result = nil then
    SetErrorIndex(APosition, ecName);
  TFormulaAccess(Formula).SetAsOwner(Result);
end;

function TdxSpreadSheetFormulaParser.MakeReference(
  ALink, ALink2: TdxSpreadSheet3DReferenceCustomLink; ARow, ACol, ARow2, ACol2: Integer;
  AAbsRow, AAbsCol, AAbsRow2, AAbsCol2: Boolean; AIsArea: Boolean): TdxSpreadSheetFormulaToken;
begin
  if R1C1Reference then
  begin
    ValidateR1C1Reference(Row, ARow, AAbsRow);
    ValidateR1C1Reference(Column, ACol, AAbsCol);
    if AIsArea then
    begin 
      ValidateR1C1Reference(Row, ARow2, AAbsRow2);
      ValidateR1C1Reference(Column, ACol2, AAbsCol2);
    end;
  end
  else
  begin
    ValidateReference(Row, ARow, AAbsRow);
    ValidateReference(Column, ACol, AAbsCol);
    if AIsArea then
    begin
      ValidateReference(Row, ARow2, AAbsRow2);
      ValidateReference(Column, ACol2, AAbsCol2);
    end;
  end;
  if AIsArea then
  begin
    if (ALink <> nil) or (ALink2 <> nil) then
      Result := TdxSpreadSheetFormula3DAreaReference.Create(ALink, ALink2,
        ARow, ACol, ARow2, ACol2, AAbsRow, AAbsCol, AAbsRow2, AAbsCol2)
    else
      Result := TdxSpreadSheetFormulaAreaReference.Create(ARow, ACol, ARow2, ACol2, AAbsRow, AAbsCol, AAbsRow2, AAbsCol2)
  end
  else
  begin
    if ALink <> nil then
      Result := TdxSpreadSheetFormula3DReference.Create(ALink, ARow, ACol, AAbsRow, AAbsCol)
    else
      Result := TdxSpreadSheetFormulaReference.Create(ARow, ACol, AAbsRow, AAbsCol);
  end
end;

function TdxSpreadSheetFormulaParser.PrepareTokensFromString(
  var AStartPos, AFinishPos: Integer): TdxSpreadSheetFormulaToken;
var
  AToken, APrevToken, ANextToken: TdxSpreadSheetFormulaToken;

  procedure RemoveToken;
  begin
    ANextToken := AToken.Next;
    if AToken = Result then
      Result := ANextToken;
    AToken.ExtractFromList;
    AToken.Free;
  end;

begin
  Result := nil;
  APrevToken := nil;
  // convert to tokens
  if IsListSeparator(AStartPos) or IsArraySeparator(AStartPos) then
    Result := TdxSpreadSheetFormulaNullToken.Create
  else
    while (AStartPos <= AFinishPos) and not (IsListSeparator(AStartPos) or IsArraySeparator(AStartPos)) do
    begin
      AToken := GetNextToken(AStartPos, AFinishPos - AStartPos + 1);
      if AToken = nil then Break;
      if Result = nil then
        Result := AToken
      else
        AddToken(APrevToken, AToken);
      APrevToken := AToken;
    end;
  AToken := Result;
  while AToken <> nil do
  begin
    AToken.CheckNeighbors;
    if AToken is TdxSpreadSheetFormulaAttributeToken then
    begin
      APrevToken := AToken.Prev;
      RemoveToken;
      if (APrevToken is TdxSpreadSheetFormulaReference) and (ANextToken is TdxSpreadSheetFormulaReference) then
      begin
         AToken := TdxSpreadSheetFormulaOperationToken.Create(opIsect);
         TFormulaAccess(Formula).Add(APrevToken, AToken);
         TFormulaAccess(Formula).SetAsOwner(AToken);
      end;
      AToken := ANextToken;
    end
    else
      if (AToken is TdxSpreadSheetFormulaOperationToken) and CheckUnaryOperation(AToken.Next, TdxSpreadSheetFormulaOperationToken(AToken)) then
      begin
        RemoveToken;
        AToken := ANextToken;
      end
      else
        AToken := AToken.Next;
  end;
  //
  if Formula.ErrorIndex <> 0 then
    TFormulaAccess(Formula).DestroyTokens(Result);
end;

procedure TdxSpreadSheetFormulaParser.PostProcessFormulaTokens(AParent: TdxSpreadSheetFormulaToken);
begin
{  while AParent <> nil do
  begin
    AParent.PostProcessData;
    PostProcessFormulaTokens(AParent.FirstChild);
    AParent := AParent.Next;
  end;}
end;

procedure TdxSpreadSheetFormulaParser.RegisterTokenControllers;

  procedure AddController(AController: TdxSpreadSheetParserTokenController);
  begin
    SetLength(TokenControllers, Length(TokenControllers) + 1);
    TokenControllers[Length(TokenControllers) - 1] := AController;
  end;

begin
  AddController(IsString);
  AddController(IsSubExpression);
  AddController(IsArray);
  AddController(IsError);
  AddController(IsOperation);
  AddController(IsFunction);
  AddController(IsNumber);
  AddController(IsName);
  AddController(IsReference);
  AddController(IsUnknown);
end;

procedure TdxSpreadSheetFormulaParser.SetErrorIndex(AErrorIndex: Integer;
  const ACode: TdxSpreadSheetFormulaErrorCode = ecNone);
begin
  TFormulaAccess(Formula).SetError(ACode, AErrorIndex);
  TFormulaAccess(Formula).DestroyTokens(TFormulaAccess(Formula).FTokens);
end;

procedure TdxSpreadSheetFormulaParser.ValidateReference(
  AIndex: Integer; var AReference: Integer; AIsAbsolute: Boolean);
begin
  if (AReference = MaxInt) or (AReference = -MaxInt) then Exit;
  if not AIsAbsolute then
    Integer(AReference) := AReference - AIndex
end;

procedure TdxSpreadSheetFormulaParser.ValidateR1C1Reference(
  AIndex: Integer; var AReference: Integer; AIsAbsolute: Boolean);
begin
  if (AReference = MaxInt) or (AReference = -MaxInt) then Exit; 
{  if not AIsAbsolute then
    AReference := AReference + AIndex
  else
    Dec(AReference);}
end;

function TdxSpreadSheetFormulaParser.GetCell: TdxSpreadSheetCell;
begin
  Result := Formula.Cell;
end;

function TdxSpreadSheetFormulaParser.GetColumn: Integer;
begin
  if Cell <> nil then
    Result := Cell.ColumnIndex
  else
    Result := 0;
end;

function TdxSpreadSheetFormulaParser.GetFormatSettings: TdxSpreadSheetFormatSettings;
begin
  Result := SpreadSheet.FormulaController.FormatSettings;
end;

function TdxSpreadSheetFormulaParser.GetR1C1Reference: Boolean;
begin
  Result := FormatSettings.R1C1Reference;
end;

function TdxSpreadSheetFormulaParser.GetRow: Integer;
begin
  if Cell <> nil then
    Result := Cell.RowIndex
  else
    Result := 0;
end;

function TdxSpreadSheetFormulaParser.GetSheet: TdxSpreadSheetTableView;
begin
  if Cell <> nil then
    Result := Cell.View
  else
    Result := nil;
end;

{$IFDEF TESTPARSER}
function TdxSpreadSheetFormulaParser.GetTreeAsText(ATree: TdxSpreadSheetFormulaToken): string;
var
  S: string;
begin
  Result := '';
  while ATree <> nil do
  begin
    if ATree is TdxSpreadSheetFormulaIntegerValueToken then
      S := IntToStr(TdxSpreadSheetFormulaIntegerValueToken(ATree).Value)
    else
      if ATree is TdxSpreadSheetFormulaFloatValueToken then
        S := FloatToStr(TdxSpreadSheetFormulaFloatValueToken(ATree).Value)
      else
        if ATree is TdxSpreadSheetFormulaOperationToken then
          case TdxSpreadSheetFormulaOperationToken(ATree).Operation of
            opUPlus, opUMinus:
              S := dxOperations[TdxSpreadSheetFormulaOperationToken(ATree).Operation] + ' (unary)';
            opParen:
              S := '(' + GetTreeAsText(ATree.FirstChild) +   ')';
           else
              S := dxOperations[TdxSpreadSheetFormulaOperationToken(ATree).Operation]
           end
        else
          if ATree is TdxSpreadSheetFormulaBooleanValueToken then
            S := dxBoolToString[TdxSpreadSheetFormulaBooleanValueToken(ATree).Value]
          else
            if ATree is TdxSpreadSheetFormulaFunctionToken then
              S := TdxSpreadSheetFormulaFunctionToken(ATree).Information.Name;

    Result := Result + S;
    ATree := ATree.FNext;
    if ATree <> nil then
      Result := Result + '  ';
  end;
end;
{$ENDIF}

procedure InitializeTokens;
begin
  TdxSpreadSheetFormulaToken.Register;
  TdxSpreadSheetFormulaNullToken.Register;
  TdxSpreadSheetFormulaStringValueToken.Register;
  TdxSpreadSheetFormulaOperationToken.Register;
  TdxSpreadSheetFormulaParenthesesToken.Register;
  TdxSpreadSheetFormulaBooleanValueToken.Register;
  TdxSpreadSheetFormulaIntegerValueToken.Register;
  TdxSpreadSheetFormulaFloatValueToken.Register;
  TdxSpreadSheetFormulaCurrencyValueToken.Register;
  TdxSpreadSheetFormulaDateTimeValueToken.Register;
  TdxSpreadSheetFormulaAttributeToken.Register;
  TdxSpreadSheetFormulaErrorValueToken.Register;
  TdxSpreadSheetListToken.Register;
  TdxSpreadSheetFormulaFunctionToken.Register;
  TdxSpreadSheetFormulaUnknownNameToken.Register;
  TdxSpreadSheetFormulaUnknownFunctionToken.Register;
  TdxSpreadSheetDefinedNameToken.Register;
  TdxSpreadSheetFormulaReference.Register;
  TdxSpreadSheetFormula3DReference.Register;
  TdxSpreadSheetFormulaAreaReference.Register;
  TdxSpreadSheetFormula3DAreaReference.Register;
  TdxSpreadSheetFormulaArrayToken.Register;
  TdxSpreadSheetFormulaArrayRowSeparator.Register;
  TdxSpreadSheetFormulaVariantToken.Register;
end;

initialization
  InitializeTokens;

finalization
  FreeAndNil(TokensRepository);
end.
