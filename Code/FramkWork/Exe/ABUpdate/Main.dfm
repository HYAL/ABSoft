object ABUpdateForm: TABUpdateForm
  Left = 250
  Top = 144
  Caption = #21319#32423#19982#21516#27493#31243#24207
  ClientHeight = 485
  ClientWidth = 927
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 217
    Top = 0
    Width = 4
    Height = 485
    ExplicitHeight = 429
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 217
    Height = 485
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 217
      Height = 245
      Align = alTop
      Caption = #26087#25968#25454#24211
      TabOrder = 0
      DesignSize = (
        217
        245)
      object Label1: TLabel
        Left = 11
        Top = 62
        Width = 44
        Height = 13
        AutoSize = False
        Caption = #26381#21153#22120'   '
        Transparent = True
      end
      object Label2: TLabel
        Left = 11
        Top = 89
        Width = 44
        Height = 13
        AutoSize = False
        Caption = #25968#25454#24211'   '
        Transparent = True
      end
      object Label3: TLabel
        Left = 11
        Top = 116
        Width = 44
        Height = 13
        AutoSize = False
        Caption = #29992#25143#21517'   '
        Transparent = True
      end
      object Label4: TLabel
        Left = 11
        Top = 143
        Width = 44
        Height = 13
        AutoSize = False
        Caption = #23494#30721'   '
        Transparent = True
      end
      object Label19: TLabel
        Left = 11
        Top = 35
        Width = 44
        Height = 13
        AutoSize = False
        Caption = #24211#31867#22411
        Transparent = True
      end
      object SpeedButton2: TButton
        Left = 186
        Top = 63
        Width = 20
        Height = 21
        Anchors = [akTop, akRight]
        Caption = '...'
        TabOrder = 6
        OnClick = SpeedButton2Click
      end
      object SpeedButton3: TButton
        Left = 186
        Top = 86
        Width = 20
        Height = 21
        Anchors = [akTop, akRight]
        Caption = '...'
        TabOrder = 7
        OnClick = SpeedButton3Click
      end
      object BitBtn1: TBitBtn
        Left = 11
        Top = 205
        Width = 175
        Height = 25
        Caption = #27979#35797
        Default = True
        TabOrder = 0
        OnClick = BitBtn1Click
      end
      object Edit1: TEdit
        Tag = 91
        Left = 58
        Top = 113
        Width = 128
        Height = 21
        Hint = #19981#25903#25345#23494#30721#20026#31354#30340#29992#25143#21517#30331#24405
        Anchors = [akLeft, akTop, akRight]
        ImeName = #20013#25991' ('#31616#20307') - '#24494#36719#25340#38899
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object Edit2: TEdit
        Tag = 91
        Left = 58
        Top = 140
        Width = 128
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        ImeName = #20013#25991' ('#31616#20307') - '#24494#36719#25340#38899
        PasswordChar = '*'
        TabOrder = 2
      end
      object ComboBox1: TComboBox
        Tag = 911
        Left = 58
        Top = 59
        Width = 128
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        ImeName = #20013#25991' ('#31616#20307') - '#24494#36719#25340#38899
        TabOrder = 3
      end
      object ComboBox2: TComboBox
        Tag = 911
        Left = 58
        Top = 86
        Width = 128
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        ImeName = #20013#25991' ('#31616#20307') - '#24494#36719#25340#38899
        TabOrder = 4
      end
      object ComboBox5: TComboBox
        Tag = 91
        Left = 58
        Top = 32
        Width = 128
        Height = 21
        Style = csDropDownList
        Anchors = [akLeft, akTop, akRight]
        ImeName = #20013#25991' ('#31616#20307') - '#24494#36719#25340#38899
        ItemIndex = 0
        TabOrder = 5
        Text = 'SQLServer'
        OnChange = ComboBox5Change
        Items.Strings = (
          'SQLServer'
          'Oracle')
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 245
      Width = 217
      Height = 240
      Align = alClient
      Caption = #26032#25968#25454#24211
      TabOrder = 1
      DesignSize = (
        217
        240)
      object Label5: TLabel
        Left = 11
        Top = 31
        Width = 44
        Height = 13
        AutoSize = False
        Caption = #26381#21153#22120'   '
        Transparent = True
      end
      object Label6: TLabel
        Left = 11
        Top = 61
        Width = 44
        Height = 13
        AutoSize = False
        Caption = #25968#25454#24211'   '
        Transparent = True
      end
      object Label7: TLabel
        Left = 11
        Top = 88
        Width = 44
        Height = 13
        AutoSize = False
        Caption = #29992#25143#21517'   '
        Transparent = True
      end
      object Label8: TLabel
        Left = 11
        Top = 115
        Width = 44
        Height = 13
        AutoSize = False
        Caption = #23494#30721'   '
        Transparent = True
      end
      object SpeedButton1: TButton
        Left = 186
        Top = 31
        Width = 20
        Height = 21
        Anchors = [akTop, akRight]
        Caption = '...'
        TabOrder = 5
        OnClick = SpeedButton1Click
      end
      object SpeedButton4: TButton
        Left = 186
        Top = 58
        Width = 20
        Height = 21
        Anchors = [akTop, akRight]
        Caption = '...'
        TabOrder = 6
        OnClick = SpeedButton4Click
      end
      object BitBtn3: TBitBtn
        Left = 11
        Top = 200
        Width = 175
        Height = 25
        Caption = #27979#35797
        Default = True
        TabOrder = 0
        OnClick = BitBtn3Click
      end
      object Edit3: TEdit
        Tag = 91
        Left = 58
        Top = 85
        Width = 128
        Height = 21
        Hint = #19981#25903#25345#23494#30721#20026#31354#30340#29992#25143#21517#30331#24405
        Anchors = [akLeft, akTop, akRight]
        ImeName = #20013#25991' ('#31616#20307') - '#24494#36719#25340#38899
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object Edit4: TEdit
        Tag = 91
        Left = 58
        Top = 112
        Width = 128
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        ImeName = #20013#25991' ('#31616#20307') - '#24494#36719#25340#38899
        PasswordChar = '*'
        TabOrder = 2
      end
      object ComboBox3: TComboBox
        Tag = 911
        Left = 58
        Top = 28
        Width = 128
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        ImeName = #20013#25991' ('#31616#20307') - '#24494#36719#25340#38899
        TabOrder = 3
      end
      object ComboBox4: TComboBox
        Tag = 911
        Left = 58
        Top = 58
        Width = 128
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        ImeName = #20013#25991' ('#31616#20307') - '#24494#36719#25340#38899
        TabOrder = 4
      end
    end
  end
  object Panel8: TPanel
    Left = 221
    Top = 0
    Width = 706
    Height = 485
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object PageControl2: TPageControl
      Tag = 91
      Left = 0
      Top = 0
      Width = 706
      Height = 442
      ActivePage = TabSheet11
      Align = alClient
      TabOrder = 0
      TabStop = False
      OnChanging = PageControl2Changing
      object TabSheet11: TTabSheet
        Caption = #21319#32423#25805#20316
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 698
          Height = 414
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBox3: TGroupBox
            Left = 0
            Top = 209
            Width = 698
            Height = 205
            Align = alClient
            Caption = #21319#32423#26085#24535
            TabOrder = 0
            object Memo_log: TMemo
              Tag = 910
              Left = 2
              Top = 15
              Width = 694
              Height = 188
              Align = alClient
              ImeName = #24555#20048#20116#31508
              ScrollBars = ssBoth
              TabOrder = 0
              OnKeyDown = Memo_updateSQL1KeyDown
            end
          end
          object GroupBox4: TGroupBox
            Left = 0
            Top = 0
            Width = 698
            Height = 209
            Align = alTop
            Caption = #21319#32423#33050#26412
            TabOrder = 1
            object PageControl1: TPageControl
              Tag = 91
              Left = 2
              Top = 15
              Width = 694
              Height = 136
              ActivePage = TabSheet10
              Align = alClient
              TabOrder = 0
              object TabSheet1: TTabSheet
                Caption = #33050#26412'1'
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Memo_updateSQL1: TMemo
                  Tag = 910
                  Left = 0
                  Top = 0
                  Width = 556
                  Height = 108
                  Align = alClient
                  ImeName = #24555#20048#20116#31508
                  ScrollBars = ssBoth
                  TabOrder = 0
                  OnKeyDown = Memo_updateSQL1KeyDown
                end
                object GroupBox5: TGroupBox
                  Left = 556
                  Top = 0
                  Width = 130
                  Height = 108
                  Align = alRight
                  TabOrder = 1
                  object Label9: TLabel
                    Left = 6
                    Top = 4
                    Width = 67
                    Height = 13
                    AutoSize = False
                    Caption = #33050#26412#21517#31216
                  end
                  object Edit_Caption1: TEdit
                    Tag = 91
                    Left = 6
                    Top = 21
                    Width = 115
                    Height = 21
                    ImeName = #24555#20048#20116#31508
                    TabOrder = 0
                  end
                end
              end
              object TabSheet2: TTabSheet
                Caption = #33050#26412'2'
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Memo_updateSQL2: TMemo
                  Tag = 910
                  Left = 0
                  Top = 0
                  Width = 556
                  Height = 108
                  Align = alClient
                  ImeName = #24555#20048#20116#31508
                  ScrollBars = ssBoth
                  TabOrder = 0
                  OnKeyDown = Memo_updateSQL1KeyDown
                end
                object GroupBox6: TGroupBox
                  Left = 556
                  Top = 0
                  Width = 130
                  Height = 108
                  Align = alRight
                  TabOrder = 1
                  object Label10: TLabel
                    Left = 6
                    Top = 3
                    Width = 59
                    Height = 13
                    AutoSize = False
                    Caption = #33050#26412#21517#31216
                  end
                  object Edit_Caption2: TEdit
                    Tag = 91
                    Left = 6
                    Top = 22
                    Width = 119
                    Height = 21
                    ImeName = #24555#20048#20116#31508
                    TabOrder = 0
                  end
                end
              end
              object TabSheet3: TTabSheet
                Caption = #33050#26412'3'
                ImageIndex = 2
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Memo_updateSQL3: TMemo
                  Tag = 910
                  Left = 0
                  Top = 0
                  Width = 556
                  Height = 108
                  Align = alClient
                  ImeName = #24555#20048#20116#31508
                  ScrollBars = ssBoth
                  TabOrder = 0
                  OnKeyDown = Memo_updateSQL1KeyDown
                end
                object GroupBox7: TGroupBox
                  Left = 556
                  Top = 0
                  Width = 130
                  Height = 108
                  Align = alRight
                  TabOrder = 1
                  object Label11: TLabel
                    Left = 6
                    Top = 3
                    Width = 59
                    Height = 13
                    AutoSize = False
                    Caption = #33050#26412#21517#31216
                  end
                  object Edit_Caption3: TEdit
                    Tag = 91
                    Left = 6
                    Top = 22
                    Width = 119
                    Height = 21
                    ImeName = #24555#20048#20116#31508
                    TabOrder = 0
                  end
                end
              end
              object TabSheet4: TTabSheet
                Caption = #33050#26412'4'
                ImageIndex = 3
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Memo_updateSQL4: TMemo
                  Tag = 910
                  Left = 0
                  Top = 0
                  Width = 556
                  Height = 108
                  Align = alClient
                  ImeName = #24555#20048#20116#31508
                  ScrollBars = ssBoth
                  TabOrder = 0
                  OnKeyDown = Memo_updateSQL1KeyDown
                end
                object GroupBox8: TGroupBox
                  Left = 556
                  Top = 0
                  Width = 130
                  Height = 108
                  Align = alRight
                  TabOrder = 1
                  object Label12: TLabel
                    Left = 6
                    Top = 3
                    Width = 59
                    Height = 13
                    AutoSize = False
                    Caption = #33050#26412#21517#31216
                  end
                  object Edit_Caption4: TEdit
                    Tag = 91
                    Left = 6
                    Top = 22
                    Width = 119
                    Height = 21
                    ImeName = #24555#20048#20116#31508
                    TabOrder = 0
                  end
                end
              end
              object TabSheet5: TTabSheet
                Caption = #33050#26412'5'
                ImageIndex = 4
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Memo_updateSQL5: TMemo
                  Tag = 910
                  Left = 0
                  Top = 0
                  Width = 556
                  Height = 108
                  Align = alClient
                  ImeName = #24555#20048#20116#31508
                  ScrollBars = ssBoth
                  TabOrder = 0
                  OnKeyDown = Memo_updateSQL1KeyDown
                end
                object GroupBox9: TGroupBox
                  Left = 556
                  Top = 0
                  Width = 130
                  Height = 108
                  Align = alRight
                  TabOrder = 1
                  object Label13: TLabel
                    Left = 6
                    Top = 3
                    Width = 59
                    Height = 13
                    AutoSize = False
                    Caption = #33050#26412#21517#31216
                  end
                  object Edit_Caption5: TEdit
                    Tag = 91
                    Left = 6
                    Top = 22
                    Width = 119
                    Height = 21
                    ImeName = #24555#20048#20116#31508
                    TabOrder = 0
                  end
                end
              end
              object TabSheet6: TTabSheet
                Caption = #33050#26412'6'
                ImageIndex = 5
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Memo_updateSQL6: TMemo
                  Tag = 910
                  Left = 0
                  Top = 0
                  Width = 556
                  Height = 108
                  Align = alClient
                  ImeName = #24555#20048#20116#31508
                  ScrollBars = ssBoth
                  TabOrder = 0
                  OnKeyDown = Memo_updateSQL1KeyDown
                end
                object GroupBox10: TGroupBox
                  Left = 556
                  Top = 0
                  Width = 130
                  Height = 108
                  Align = alRight
                  TabOrder = 1
                  object Label14: TLabel
                    Left = 6
                    Top = 3
                    Width = 59
                    Height = 13
                    AutoSize = False
                    Caption = #33050#26412#21517#31216
                  end
                  object Edit_Caption6: TEdit
                    Tag = 91
                    Left = 6
                    Top = 22
                    Width = 119
                    Height = 21
                    ImeName = #24555#20048#20116#31508
                    TabOrder = 0
                  end
                end
              end
              object TabSheet7: TTabSheet
                Caption = #33050#26412'7'
                ImageIndex = 6
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Memo_updateSQL7: TMemo
                  Tag = 910
                  Left = 0
                  Top = 0
                  Width = 556
                  Height = 108
                  Align = alClient
                  ImeName = #24555#20048#20116#31508
                  ScrollBars = ssBoth
                  TabOrder = 0
                  OnKeyDown = Memo_updateSQL1KeyDown
                end
                object GroupBox11: TGroupBox
                  Left = 556
                  Top = 0
                  Width = 130
                  Height = 108
                  Align = alRight
                  TabOrder = 1
                  object Label15: TLabel
                    Left = 6
                    Top = 3
                    Width = 59
                    Height = 13
                    AutoSize = False
                    Caption = #33050#26412#21517#31216
                  end
                  object Edit_Caption7: TEdit
                    Tag = 91
                    Left = 6
                    Top = 22
                    Width = 119
                    Height = 21
                    ImeName = #24555#20048#20116#31508
                    TabOrder = 0
                  end
                end
              end
              object TabSheet8: TTabSheet
                Caption = #33050#26412'8'
                ImageIndex = 7
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Memo_updateSQL8: TMemo
                  Tag = 910
                  Left = 0
                  Top = 0
                  Width = 556
                  Height = 108
                  Align = alClient
                  ImeName = #24555#20048#20116#31508
                  ScrollBars = ssBoth
                  TabOrder = 0
                  OnKeyDown = Memo_updateSQL1KeyDown
                end
                object GroupBox12: TGroupBox
                  Left = 556
                  Top = 0
                  Width = 130
                  Height = 108
                  Align = alRight
                  TabOrder = 1
                  object Label16: TLabel
                    Left = 6
                    Top = 3
                    Width = 59
                    Height = 13
                    AutoSize = False
                    Caption = #33050#26412#21517#31216
                  end
                  object Edit_Caption8: TEdit
                    Tag = 91
                    Left = 6
                    Top = 22
                    Width = 119
                    Height = 21
                    ImeName = #24555#20048#20116#31508
                    TabOrder = 0
                  end
                end
              end
              object TabSheet9: TTabSheet
                Caption = #33050#26412'9'
                ImageIndex = 8
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Memo_updateSQL9: TMemo
                  Tag = 910
                  Left = 0
                  Top = 0
                  Width = 556
                  Height = 108
                  Align = alClient
                  ImeName = #24555#20048#20116#31508
                  ScrollBars = ssBoth
                  TabOrder = 0
                  OnKeyDown = Memo_updateSQL1KeyDown
                end
                object GroupBox13: TGroupBox
                  Left = 556
                  Top = 0
                  Width = 130
                  Height = 108
                  Align = alRight
                  TabOrder = 1
                  object Label17: TLabel
                    Left = 6
                    Top = 3
                    Width = 59
                    Height = 13
                    AutoSize = False
                    Caption = #33050#26412#21517#31216
                  end
                  object Edit_Caption9: TEdit
                    Tag = 91
                    Left = 6
                    Top = 22
                    Width = 119
                    Height = 21
                    ImeName = #24555#20048#20116#31508
                    TabOrder = 0
                  end
                end
              end
              object TabSheet10: TTabSheet
                Caption = #33050#26412'10'
                ImageIndex = 9
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Memo_updateSQL10: TMemo
                  Tag = 910
                  Left = 0
                  Top = 0
                  Width = 556
                  Height = 108
                  Align = alClient
                  ImeName = #24555#20048#20116#31508
                  ScrollBars = ssBoth
                  TabOrder = 0
                  OnKeyDown = Memo_updateSQL1KeyDown
                end
                object GroupBox14: TGroupBox
                  Left = 556
                  Top = 0
                  Width = 130
                  Height = 108
                  Align = alRight
                  TabOrder = 1
                  object Label18: TLabel
                    Left = 6
                    Top = 3
                    Width = 59
                    Height = 13
                    AutoSize = False
                    Caption = #33050#26412#21517#31216
                  end
                  object Edit_Caption10: TEdit
                    Tag = 91
                    Left = 6
                    Top = 22
                    Width = 119
                    Height = 21
                    ImeName = #24555#20048#20116#31508
                    TabOrder = 0
                  end
                end
              end
            end
            object GroupBox15: TGroupBox
              Left = 2
              Top = 151
              Width = 694
              Height = 56
              Align = alBottom
              Caption = #36816#34892#30340#33050#26412
              TabOrder = 1
              object CheckBox_Run1: TCheckBox
                Tag = 91
                Left = 14
                Top = 16
                Width = 115
                Height = 17
                Caption = #25191#34892#33050#26412'1'
                Checked = True
                State = cbChecked
                TabOrder = 0
              end
              object CheckBox_Run2: TCheckBox
                Tag = 91
                Left = 135
                Top = 16
                Width = 115
                Height = 17
                Caption = #25191#34892#33050#26412'2'
                Checked = True
                State = cbChecked
                TabOrder = 1
              end
              object CheckBox_Run3: TCheckBox
                Tag = 91
                Left = 256
                Top = 16
                Width = 115
                Height = 17
                Caption = #25191#34892#33050#26412'3'
                Checked = True
                State = cbChecked
                TabOrder = 2
              end
              object CheckBox_Run4: TCheckBox
                Tag = 91
                Left = 377
                Top = 16
                Width = 115
                Height = 17
                Caption = #25191#34892#33050#26412'4'
                Checked = True
                State = cbChecked
                TabOrder = 3
              end
              object CheckBox_Run5: TCheckBox
                Tag = 91
                Left = 500
                Top = 16
                Width = 115
                Height = 17
                Caption = #25191#34892#33050#26412'5'
                Checked = True
                State = cbChecked
                TabOrder = 4
              end
              object CheckBox_Run6: TCheckBox
                Tag = 91
                Left = 14
                Top = 34
                Width = 115
                Height = 17
                Caption = #25191#34892#33050#26412'6'
                Checked = True
                State = cbChecked
                TabOrder = 5
              end
              object CheckBox_Run7: TCheckBox
                Tag = 91
                Left = 135
                Top = 34
                Width = 115
                Height = 17
                Caption = #25191#34892#33050#26412'7'
                Checked = True
                State = cbChecked
                TabOrder = 6
              end
              object CheckBox_Run8: TCheckBox
                Tag = 91
                Left = 256
                Top = 34
                Width = 115
                Height = 17
                Caption = #25191#34892#33050#26412'8'
                Checked = True
                State = cbChecked
                TabOrder = 7
              end
              object CheckBox_Run9: TCheckBox
                Tag = 91
                Left = 377
                Top = 34
                Width = 115
                Height = 17
                Caption = #25191#34892#33050#26412'9'
                Checked = True
                State = cbChecked
                TabOrder = 8
              end
              object CheckBox_Run10: TCheckBox
                Tag = 91
                Left = 500
                Top = 34
                Width = 115
                Height = 17
                Caption = #25191#34892#33050#26412'10'
                Checked = True
                State = cbChecked
                TabOrder = 9
              end
            end
          end
          object CheckBox2: TCheckBox
            Tag = 91
            Left = 0
            Top = 209
            Width = 761
            Height = 17
            Caption = '('#26087#24211#20026'SQL SERVER'#26102')'#21319#32423#21518#23558#26087#24211#21152#26102#38388#21518#32512#22791#20221','#21516#26102#23558#26032#24211#20462#25913#20026#26087#24211#21517#31216
            TabOrder = 2
          end
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 442
      Width = 706
      Height = 43
      Align = alBottom
      TabOrder = 1
      object Label25: TLabel
        Left = 608
        Top = 16
        Width = 37
        Height = 13
        AutoSize = False
      end
      object Button_Run: TButton
        Left = 6
        Top = 4
        Width = 100
        Height = 35
        Caption = #24320#22987#36816#34892
        TabOrder = 0
        OnClick = Button_RunClick
      end
      object Button_OpenSetup: TButton
        Left = 250
        Top = 4
        Width = 100
        Height = 35
        Caption = #24320#21551#35774#32622
        TabOrder = 1
        OnClick = Button_OpenSetupClick
      end
      object Button_CloseSetup: TButton
        Left = 355
        Top = 4
        Width = 100
        Height = 35
        Caption = #20851#38381#35774#32622
        TabOrder = 2
        OnClick = Button_CloseSetupClick
      end
      object Button_Stop: TButton
        Left = 112
        Top = 4
        Width = 100
        Height = 35
        Caption = #20572#27490#36816#34892
        Enabled = False
        TabOrder = 3
        OnClick = Button_StopClick
      end
      object Animate1: TAnimate
        Left = 599
        Top = 1
        Width = 106
        Height = 41
        Align = alRight
        AutoSize = False
        Color = clBtnFace
        ParentColor = False
        StopFrame = 8
        Timers = True
      end
    end
  end
  object FireDACConnection_old: TFireDACConnection
    ResourceOptions.AssignedValues = [rvAutoReconnect]
    ResourceOptions.AutoReconnect = True
    LoginPrompt = False
    Left = 264
    Top = 88
  end
  object FireDACConnection_New: TFireDACConnection
    ResourceOptions.AssignedValues = [rvAutoReconnect]
    ResourceOptions.AutoReconnect = True
    LoginPrompt = False
    Left = 264
    Top = 264
  end
  object FireDACQuery1_New: TABThirdReadDataQuery
    ActiveStoredUsage = []
    ReadOnly = False
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    Left = 392
    Top = 264
  end
  object FireDACQuery1_Old: TABThirdReadDataQuery
    ActiveStoredUsage = []
    ReadOnly = False
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    Left = 384
    Top = 88
  end
  object FireDACQuery2_New: TABThirdReadDataQuery
    ActiveStoredUsage = []
    ReadOnly = False
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    Left = 528
    Top = 256
  end
  object FireDACQuery3_New: TABThirdReadDataQuery
    ActiveStoredUsage = []
    ReadOnly = False
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    Left = 632
    Top = 256
  end
  object FireDACQuery2_Old: TABThirdReadDataQuery
    ActiveStoredUsage = []
    ReadOnly = False
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    Left = 496
    Top = 88
  end
  object FireDACQuery3_Old: TABThirdReadDataQuery
    ActiveStoredUsage = []
    ReadOnly = False
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    Left = 616
    Top = 88
  end
end
