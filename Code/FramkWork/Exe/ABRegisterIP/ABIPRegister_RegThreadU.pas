unit ABIPRegister_RegThreadU;

interface

uses
  ABPubVarU,
  ABPubFuncU,
  ABPubIndyU,
  Classes,ActiveX,SysUtils,IdFTP,IdBaseComponent,idHTTP;

type
  TABIPRegister_RegThread = class(TThread)
  private
    { Private declarations }
  protected
    procedure Execute; override;
  end;

implementation

{ Important: Methods and properties of objects in visual components can only be
  used in a method called using Synchronize, for example,

      Synchronize(UpdateCaption);

  and UpdateCaption could look like,

    procedure YBRegWeb.UpdateCaption;
    begin
      Form1.Caption := 'Updated in a thread';
    end; }

{ TABIPRegister_RegThread }

procedure TABIPRegister_RegThread.Execute;
var
  FIdHTTP: TIdHTTP;
  FIdFTP: TIdFTP;
  tempStr1:string;

  tempUpWANIPFTPPort:longint;
  tempGetWANIPAspAddress,
  tempUpWANIPFTPAddress,
  tempUpWANIPPath,
  tempRegName:string;
begin
  { Place thread code here }
  CoInitialize(nil);
  FIdHTTP:=TIdHTTP.Create(nil);
  FIdFTP:=TIdFTP.Create(nil);
  FIdFTP.Passive:=true;
  try
    while true do
    begin
      try
        //取得网络中显示外网IP的地址
        tempGetWANIPAspAddress:=ABReadInI('RegIPSetup','GetWWWIPAspAddress', ABSoftSetPath+'Setup.ini');
        if tempGetWANIPAspAddress=emptystr then
        begin
          tempGetWANIPAspAddress:='http://www.aibosoft.cn/getip.asp';
          ABWriteInI('RegIPSetup','GetWWWIPAspAddress',tempGetWANIPAspAddress, ABSoftSetPath+'Setup.ini' );
        end;

        //将取得的外网IP存到哪个FTP地址中
        tempUpWANIPFTPAddress:=ABReadInI('RegIPSetup','UpWWWIPFTPAddress', ABSoftSetPath+'Setup.ini' );
        if tempUpWANIPFTPAddress=emptystr then
        begin
          tempUpWANIPFTPAddress:='ftp://aibosoft:asd123@sh25.gvk.cn/';
          ABWriteInI('RegIPSetup','UpWWWIPFTPAddress','', ABSoftSetPath+'Setup.ini' );
        end;
        //FTP地址中存放外网信息的路径
        tempUpWANIPPath:=ABReadInI('RegIPSetup','UpWWWIPPath', ABSoftSetPath+'Setup.ini' );
        if tempUpWANIPPath=emptystr then
        begin
          tempUpWANIPPath:='/aibosoft/web/ClientIP/';
          ABWriteInI('RegIPSetup','UpWWWIPPath',tempUpWANIPPath, ABSoftSetPath+'Setup.ini' );
        end;

        //FTP通讯的端口
        tempStr1:=ABReadInI('RegIPSetup','FTPPort',ABSoftSetPath+'Setup.ini' );
        if tempStr1=emptystr then
        begin
          tempStr1:='8099';
          ABWriteInI('RegIPSetup','FTPPort',tempStr1, ABSoftSetPath+'Setup.ini' );
        end
        else
        begin

        end;
        tempUpWANIPFTPPort:=StrToInt(tempStr1);

        //客户的注册名称
        tempRegName:=ABReadInI('RegIPSetup','RegName', ABSoftSetPath+'Setup.ini');
        if tempRegName=emptystr then
        begin
          tempRegName:='CustomRegName';
          ABWriteInI('RegIPSetup','RegName',tempRegName, ABSoftSetPath+'Setup.ini');
        end;

        //得到外网通讯的IP与端口并写入到临时文件中
        ABWriteTxt(ABSoftSetPath+'tempWANIPAndPort.txt',FIdHTTP.Get(tempGetWANIPAspAddress)+':'+inttostr(tempUpWANIPFTPPort));
        try
          ABSetIDFtpConn(tempUpWANIPFTPAddress,FIdFTP);
          ABUpToFtp(ABSoftSetPath+'tempWANIPAndPort.txt',tempUpWANIPPath+tempRegName+'.txt',FIdFTP);
        finally
          FIdFTP.Quit;
        end;
      except

      end;
      Sleep(30000);
    end;
  finally
    FIdHTTP.free;
    FIdFTP.free;
    CoUnInitialize;
  end;
end;

Initialization

Finalization

end.
