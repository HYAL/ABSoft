program ABServer;

uses
  ABPubMessageU,
  ABPubVarU,
  ABPubServiceU,
  ABPubFuncU,
  ABPubConstU,

  ABFramkWorkUpAndDownU,

  Forms,
  Windows,
  ShellAPI,
  SysUtils;

{$R *.res}
begin
  Application.Initialize;

  if (ABCheckFileExists(ABAppPath+'ABServiceP.exe')) or
     (ABCheckFileExists(ABAppPath+'ABServiceP')) then
  begin
    //服务方式的服务器程序
    ABInitAndRun('ABServiceP');
  end
  else if ABCheckFileExists(ABAppPath+'ABServerP.exe') then
  begin
    //EXE方式的服务器程序
    ABInitAndRun('ABServerP');
  end;

  Application.Run;
end.

