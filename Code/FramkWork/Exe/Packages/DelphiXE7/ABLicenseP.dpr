program ABLicenseP;

uses
  Forms,
  Main in '..\..\ABLicense\Main.pas' {ABRegisterForm},
  ShowRightU in '..\..\ABLicense\ShowRightU.pas' {ABShowRightForm};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TABRegisterForm, ABRegisterForm);
  Application.Run;
end.
