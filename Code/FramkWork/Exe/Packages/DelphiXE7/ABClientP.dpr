program ABClientP;

uses
  Forms,
  Windows,
  SysUtils,
  ABPubMessageU,
  ABPubStartFormU,
  ABPubManualProgressBarU,
  ABFramkWorkUserU,
  ABClientU in '..\..\ABClient\ABClientU.pas' {ABClientForm},
  ABClientBackU in '..\..\ABClient\ABClientBackU.pas' {ABClientBackForm};

{$R *.res}

begin
  Application.Initialize;
  if ABUser.Login(ltLongin) then
  begin
    try
      ABGetPubManualProgressBarForm.RefreshAll;
      ABGetPubManualProgressBarForm.NextMainIndex;

      Application.CreateForm(TABClientForm, ABClientForm);
    finally
      ABGetPubManualProgressBarForm.Stop;
      ABCloseStartForm;
    end;
  end;
  Application.Run;
end.

