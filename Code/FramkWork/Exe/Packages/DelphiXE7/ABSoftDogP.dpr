program ABSoftDogP;

uses
  Forms,
  Main in '..\..\ABSoftDog\Main.pas' {ABRegisterForm},
  ShowRightU in '..\..\ABLicense\ShowRightU.pas' {ABShowRightForm};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TABRegisterForm, ABRegisterForm);
  Application.Run;
end.
