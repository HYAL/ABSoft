unit Main;

interface

uses
  ShowRightU,

  ABpubInPutPassWordU,
  abpubmessageU,
  ABPubFormU,
  ABPubFuncU,
  ABPubPassU,
  ABPubVarU,
  ABPubUserU,
  abpubconstU,
  ABPubCheckTreeViewU,
  ABPubInPutStrU,
  ABPubSelectComboBoxU,
  ABPubMemoU,
  ABPubDogKeyU,
  ABpubDBU,
  ABThirdDBU,

  cxGridExportLink,
  cxTextEdit,
  cxLookAndFeelPainters,
  cxCalendar,
  cxDropDownEdit,
  cxMaskEdit,
  cxCalc,
  cxEdit,
  cxContainer,
  cxLookAndFeels,
  cxControls,
  cxGraphics,
  cxMemo,
  cxDBEdit,

  DateUtils, Math,
  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,Menus,
  StdCtrls,ExtCtrls,DB,ADODB,Grids,DBGrids,DBCtrls,Mask,ADOInt,
  Buttons, ComCtrls, ABPubMultilingualDBNavigatorU, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxDBData, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView,
  ABThird_DevExtPopupMenuU, cxClasses, cxGridLevel, cxGrid, cxSpinEdit,
  cxGridDBTableView, cxNavigator;

type
  TABRegisterForm = class(TABPubForm)
    DataSource1_1: TDataSource;
    ADOQuery1_1: TADOQuery;
    ADOConnection1: TADOConnection;
    DataSource1_2: TDataSource;
    ADOQuery1_2: TADOQuery;
    ADOConnection2: TADOConnection;
    ADOQuery3: TADOQuery;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    DataSource1_21: TDataSource;
    ADOQuery1_21: TADOQuery;
    ADOQuery1_1ID: TAutoIncField;
    ADOQuery1_1Name: TWideStringField;
    ADOQuery1_1BegDatetime: TDateTimeField;
    ADOQuery1_1EndDatetime: TDateTimeField;
    ADOQuery1_1ClientInfo: TWideMemoField;
    ADOQuery1_1CardCount: TIntegerField;
    ADOQuery1_1ControlCount: TIntegerField;
    ADOQuery1_1V100Count: TIntegerField;
    ADOQuery1_1V200Count: TIntegerField;
    ADOQuery1_1V300Count: TIntegerField;
    ADOQuery1_1ReaderCount: TIntegerField;
    ADOQuery1_1TerminalCount: TIntegerField;
    ADOQuery1_1FuncRight: TWideMemoField;
    ADOQuery1_2ID: TAutoIncField;
    ADOQuery1_2BegDatetime: TDateTimeField;
    ADOQuery1_2EndDatetime: TDateTimeField;
    ADOQuery1_2ClientInfo: TWideMemoField;
    ADOQuery1_2CardCount: TIntegerField;
    ADOQuery1_2ControlCount: TIntegerField;
    ADOQuery1_2V100Count: TIntegerField;
    ADOQuery1_2V200Count: TIntegerField;
    ADOQuery1_2V300Count: TIntegerField;
    ADOQuery1_2ReaderCount: TIntegerField;
    ADOQuery1_2TerminalCount: TIntegerField;
    ADOQuery1_2FuncRight: TWideMemoField;
    ADOQuery1_2State: TWideStringField;
    ADOQuery1_2RegName: TWideStringField;
    ADOQuery1_2RegKey: TWideStringField;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    Panel3: TPanel;
    Label10: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label2: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    SpeedButton7: TSpeedButton;
    SpeedButton5: TSpeedButton;
    cxDBCalcEdit2: TcxDBCalcEdit;
    cxDBCalcEdit5: TcxDBCalcEdit;
    cxDBCalcEdit6: TcxDBCalcEdit;
    cxDBCalcEdit7: TcxDBCalcEdit;
    cxDBDateEdit3: TcxDBDateEdit;
    cxDBDateEdit4: TcxDBDateEdit;
    cxDBCalcEdit3: TcxDBCalcEdit;
    cxDBTextEdit3: TcxDBTextEdit;
    Panel5: TPanel;
    Panel6: TPanel;
    Button1: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button11: TButton;
    ABCheckTreeView1: TABCheckTreeView;
    cxDBCalcEdit1: TcxDBCalcEdit;
    cxDBCalcEdit4: TcxDBCalcEdit;
    cxDBCalcEdit8: TcxDBCalcEdit;
    TabSheet4: TTabSheet;
    Label1: TLabel;
    Splitter3: TSplitter;
    Panel1: TPanel;
    Splitter1: TSplitter;
    Panel8: TPanel;
    ABCheckTreeView2: TABCheckTreeView;
    Panel9: TPanel;
    Label20: TLabel;
    Button7: TButton;
    Button8: TButton;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1Name: TcxGridDBColumn;
    cxGrid1DBTableView1BegDatetime: TcxGridDBColumn;
    cxGrid1DBTableView1EndDatetime: TcxGridDBColumn;
    cxGrid1DBTableView1SubsequentClientCount: TcxGridDBColumn;
    cxGrid1DBTableView1CardCount: TcxGridDBColumn;
    cxGrid1DBTableView1ControlCount: TcxGridDBColumn;
    cxGrid1DBTableView1V100Count: TcxGridDBColumn;
    cxGrid1DBTableView1V200Count: TcxGridDBColumn;
    cxGrid1DBTableView1V300Count: TcxGridDBColumn;
    cxGrid1DBTableView1ReaderCount: TcxGridDBColumn;
    cxGrid1DBTableView1TerminalCount: TcxGridDBColumn;
    cxGrid1DBTableView1FuncRight: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    ADOQuery1_21ListID: TIntegerField;
    ADOQuery1_21Datetime: TDateTimeField;
    ADOQuery1_21Remark: TWideMemoField;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1Datetime: TcxGridDBColumn;
    cxGridDBTableView1Remark: TcxGridDBColumn;
    ADOQuery1_1SubsequentClientCount: TIntegerField;
    ADOQuery1_2SubsequentClientCount: TIntegerField;
    cxGrid3: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxGridDBTableView2ID: TcxGridDBColumn;
    cxGridDBTableView2RegName: TcxGridDBColumn;
    cxGridDBTableView2BegDatetime: TcxGridDBColumn;
    cxGridDBTableView2EndDatetime: TcxGridDBColumn;
    cxGridDBTableView2SubsequentClientCount: TcxGridDBColumn;
    cxGridDBTableView2CardCount: TcxGridDBColumn;
    cxGridDBTableView2ControlCount: TcxGridDBColumn;
    cxGridDBTableView2V100Count: TcxGridDBColumn;
    cxGridDBTableView2V200Count: TcxGridDBColumn;
    cxGridDBTableView2V300Count: TcxGridDBColumn;
    cxGridDBTableView2ReaderCount: TcxGridDBColumn;
    cxGridDBTableView2TerminalCount: TcxGridDBColumn;
    cxGridDBTableView2FuncRight: TcxGridDBColumn;
    ADOQuery1_2Count: TIntegerField;
    cxGridDBTableView2Count: TcxGridDBColumn;
    pm1: TPopupMenu;
    MenuItem1: TMenuItem;
    ADOQuery1_1Remark: TWideMemoField;
    ADOQuery1_2Remark: TWideMemoField;
    Panel2: TPanel;
    SpeedButton2: TSpeedButton;
    SpeedButton1: TSpeedButton;
    ADOQuery1_1Price: TFloatField;
    ADOQuery1_2Price: TFloatField;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    Panel4: TPanel;
    cxDBMemo2: TcxDBMemo;
    Label3: TLabel;
    cxDBCalcEdit9: TcxDBCalcEdit;
    lbl3: TLabel;
    cxDBMemo1: TcxDBMemo;
    cxGridDBTableView2Column1: TcxGridDBColumn;
    cxGridDBTableView2Column2: TcxGridDBColumn;
    ADOQuery1_1CustomFunc: TWideMemoField;
    Label6: TLabel;
    Label4: TLabel;
    ADOQuery1_2CustomFunc: TWideMemoField;
    Panel10: TPanel;
    Panel7: TPanel;
    Button2: TButton;
    procedure ADOQuery1_1AfterInsert(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ABCheckTreeView1EndDrag(Sender, Target: TObject; X, Y: Integer);
    procedure Button11Click(Sender: TObject);
    procedure ABCheckTreeView2Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure ADOQuery1_2BeforePost(DataSet: TDataSet);
    procedure ABCheckTreeView1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cxGridDBTableView2FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure Button2Click(Sender: TObject);
  private
    FStartGetFuncStr:boolean;
    FStartSetFuncStr:boolean;
    function GetFuncStr(aTreeView:TABCheckTreeView): string;
    procedure SetFuncStr(aTreeView:TABCheckTreeView;aFuncRight: string);
    procedure SaveFuncTreeView;
    procedure SaveFuncRight(aDataset: Tadoquery; aTreeView: TABCheckTreeView);
    procedure WriteToLog;
    function WriteToDog: boolean;
    function ReadSoftDog(var aResult: string): LongInt;
    procedure AppendOrEditSoftDog(aAppend: Boolean; aBegDatetime,
      aEndDatetime: TDateTime; aSubsequentClientCount, aCardCount,
      aControlCount, aV100Count, aV200Count, aV300Count, aReaderCount,
      aTerminalCount,aPrice: Integer; aClientInfo, aFuncRight,aCustomFunc,aRemark: string);
    procedure UniteTemplate(var aBegDatetime, aEndDatetime: TDateTime;
      var aSubsequentClientCount, aCardCount, aControlCount, aV100Count,
      aV200Count, aV300Count, aReaderCount, aTerminalCount,aPrice: Integer;
      var aClientInfo, aFuncRight,aCustomFunc,aRemark: string);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABRegisterForm: TABRegisterForm;

implementation
var
  KeyPath: array[0..Max_Path] of ansichar = ''; //储存加密锁所在路径字符串；

{$R *.dfm}

procedure TABRegisterForm.FormCreate(Sender: TObject);
begin
  if ABCheckFileExists(ABSoftSetPath+'FuncTreeView.Data') then
  begin
    ABCheckTreeView1.LoadFromFile(ABSoftSetPath+'FuncTreeView.Data');
    ABCheckTreeView2.LoadFromFile(ABSoftSetPath+'FuncTreeView.Data');
  end;
  ADOConnection1.Close;
  ADOConnection1.ConnectionString:= 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+ABSoftSetPath+'Data.mdb;Persist Security Info=False';
  ADOConnection1.open;

  ADOQuery1_1.close;
  ADOQuery1_1.Open;

  ADOQuery1_2.close;
  ADOQuery1_2.Open;

  ADOQuery1_21.close;
  ADOQuery1_21.Open;

  ADOQuery1_1.Recordset.Properties['Update Criteria'].Value:=adCriteriaKey;
  ADOQuery1_2.Recordset.Properties['Update Criteria'].Value:=adCriteriaKey;
  ADOQuery1_21.Recordset.Properties['Update Criteria'].Value:=adCriteriaKey;

  FStartGetFuncStr:=true;
  FStartSetFuncStr:=true;
  SetFuncStr(ABCheckTreeView2,cxGrid1DBTableView1.DataController.DataSet.FieldByName('FuncRight').AsString);
  SetFuncStr(ABCheckTreeView1,cxGridDBTableView2.DataController.DataSet.FieldByName('FuncRight').AsString);

end;

procedure TABRegisterForm.FormShow(Sender: TObject);
var
  tempSubsequentClientCount: longint;
begin
  if (not ABCheckLicense(ABPublicSetPath+'License.key','', now(), tempSubsequentClientCount,true))
      and (not ABInputPassWord)
      then
  begin
    Panel10.Visible:=false;
    Panel7.Visible:=true;
    Panel7.Align:=alClient;
  end;

  if cxGrid1.CanFocus then
    cxGrid1.SetFocus;
  if not cxGrid1DBTableView1.Focused then
    cxGrid1DBTableView1.Focused:=true;

  if cxGrid1DBTableView1.DataController.RecordCount>0 then
    cxGrid1DBTableView1.DataController.SelectRows(0,0);
end;

procedure TABRegisterForm.ADOQuery1_2BeforePost(DataSet: TDataSet);
begin
  ABSetFieldValue(ABDoRegisterKey(DataSet.FindField('RegName').AsString),DataSet.FindField('RegKey'));
end;

procedure TABRegisterForm.ADOQuery1_1AfterInsert(DataSet: TDataSet);
begin
  DataSet.FindField('BegDatetime').AsDateTime:= now;
  DataSet.FindField('EndDatetime').AsDateTime:= now+365*5;
  DataSet.FindField('SubsequentClientCount').AsInteger:= 0;
  DataSet.FindField('ClientInfo').AsString:= '';
  DataSet.FindField('CardCount').AsInteger:= 0;
  DataSet.FindField('ControlCount').AsInteger:= 0;
  DataSet.FindField('V100Count').AsInteger:= 0;
  DataSet.FindField('V200Count').AsInteger:= 0;
  DataSet.FindField('V300Count').AsInteger:= 0;
  DataSet.FindField('ReaderCount').AsInteger:= 0;
  DataSet.FindField('TerminalCount').AsInteger:= 0;

  DataSet.FindField('FuncRight').AsString:=ABFillStr('',200);
  if DataSet=ADOQuery1_1 then
  begin
    SetFuncStr(ABCheckTreeView2,DataSet.FindField('FuncRight').AsString);
  end
  else if DataSet=ADOQuery1_2 then
  begin
    SetFuncStr(ABCheckTreeView1,DataSet.FindField('FuncRight').AsString);
  end;
end;

function TABRegisterForm.GetFuncStr(aTreeView:TABCheckTreeView):string;
var
  i,j:LongInt;
  tempText:string;
begin
  Result:=ABFillStr('',200);
  for I := 0 to aTreeView.Items.Count-1 do
  begin
    if aTreeView.Checked[aTreeView.Items[i]] then
    begin
      tempText:=aTreeView.Items[i].Text;
      if (Pos('[',tempText)>0) and
         (Pos(']',tempText)>0)
          then
      begin
        j:=StrToIntDef(ABGetItemValue(tempText,'[','',']'),0);
        if (j>0) and (j<=200) then
        begin
          Result[j]:='1';
        end;
      end;
    end;
  end;
end;

procedure TABRegisterForm.MenuItem1Click(Sender: TObject);
var
  tempFileName:string;
begin
  tempFileName:=ABSaveFile('','', 'xls|*.xls');
  if tempFileName<>EmptyStr then
  begin
    ExportGridToExcel(tempFileName,cxGrid3,true,true)
  end;
end;

procedure TABRegisterForm.SetFuncStr(aTreeView:TABCheckTreeView;aFuncRight:string);
var
  i,j:LongInt;
  tempText:string;
begin
  FStartSetFuncStr:=false;
  if Length(aFuncRight)<200 then
    aFuncRight:=ABFillStr('',200);

  aTreeView.Items.BeginUpdate;
  try
    for I := 0 to aTreeView.Items.Count-1 do
    begin
      tempText:=aTreeView.Items[i].Text;
      if (Pos('[',tempText)>0) and
         (Pos(']',tempText)>0)
          then
      begin
        j:=StrToIntDef(ABGetItemValue(tempText,'[','',']'),0);
        if (j>0) and (j<=200) then
        begin
          aTreeView.Checked[aTreeView.Items[i]]:=(aFuncRight[j]='1');
        end;
      end
      else
        aTreeView.Checked[aTreeView.Items[i]]:=false;
    end;
  finally
    aTreeView.Items.EndUpdate;
    FStartSetFuncStr:=true;
  end;
end;

procedure TABRegisterForm.ABCheckTreeView1Click(Sender: TObject);
begin
  SaveFuncRight(ADOQuery1_2,ABCheckTreeView1);
end;

procedure TABRegisterForm.ABCheckTreeView2Click(Sender: TObject);
begin
  SaveFuncRight(ADOQuery1_1,ABCheckTreeView2);
end;

procedure TABRegisterForm.SaveFuncRight(aDataset:Tadoquery;aTreeView:TABCheckTreeView);
begin
  if not FStartGetFuncStr then
    exit;

  if aDataset.State in [dsBrowse] then
    aDataset.Edit;

  if aDataset.State in [dsEdit,dsInsert] then
    aDataset.FieldByName('FuncRight').AsString:=GetFuncStr(aTreeView);
end;

procedure TABRegisterForm.ABCheckTreeView1EndDrag(Sender, Target: TObject; X,
  Y: Integer);
begin
  SaveFuncTreeView;
end;

procedure TABRegisterForm.N1Click(Sender: TObject);
begin
  Panel6.Visible:=ABInputPassWord;

  if Panel6.Visible then
  begin
    ABCheckTreeView1.DragMode:= dmAutomatic;
  end
  else
  begin
    ABCheckTreeView1.DragMode:= dmManual;
  end;
end;

procedure TABRegisterForm.N2Click(Sender: TObject);
begin
  ABSetCheckTreeViewState(ABCheckTreeView1,coCheck);
end;

procedure TABRegisterForm.N3Click(Sender: TObject);
begin
  ABSetCheckTreeViewState(ABCheckTreeView1,coUnCheck);
end;

procedure TABRegisterForm.Button1Click(Sender: TObject);
var
  tempSQL,
  tempKey,
  tempParentKey,
  tempName:string;
begin
  if (ABCheckFileExists(ABSoftSetPath+'ABConn.udl')) and
     (ABShow('是否使用上次数据库连接?',[],['是','否'],2)=2) then
  begin
    ABDeleteFile(ABSoftSetPath+'ABConn.udl');
  end;

  ADOConnection2.Connected:=false;
  if ABOpenADOConnectionByUDL(ADOConnection2,'',True,false)  then
  begin
    tempSQL:= ADOQuery3.SQL.Text;
    if (ABShowEditMemo(tempSQL,false,'请输入功能菜单的查询SQL')) and
       (tempSQL<>EmptyStr) then
    begin
      ADOQuery3.Close;
      ADOQuery3.SQL.Text:=tempSQL;
      ADOQuery3.Open;
      tempKey :='tempGuid';
      tempParentKey :='tempParentGuid';
      tempName :='tempname';
      if (ABSelectComboBox(tempKey,ADOQuery3,'请选择主键字段')) and
         (tempKey<>EmptyStr) then
      begin
        if (ABSelectComboBox(tempParentKey,ADOQuery3,'请选择父级主键字段')) and
           (tempParentKey<>EmptyStr) then
        begin
          if (ABSelectComboBox(tempName,ADOQuery3,'请选择显示字段')) and
             (tempName<>EmptyStr) then
          begin
            ABDataSetsToTree
            (
             ABCheckTreeView1.Items,
             ADOQuery3,
             tempParentKey,
             tempKey,
             tempName
             );
            SaveFuncTreeView;
          end;
        end;
      end;
    end;
  end;
end;

procedure TABRegisterForm.Button2Click(Sender: TObject);
begin
  SpeedButton5Click(SpeedButton5);
end;

procedure TABRegisterForm.Button3Click(Sender: TObject);
var
  tempName:string;
begin
  if (ABInPutStr('名称',tempName)) and
     (tempName<>EmptyStr) then
  begin
    if (Assigned(ABCheckTreeView1.Selected)) then
    begin
      ABCheckTreeView1.Items.AddChild(ABCheckTreeView1.Selected,tempName);
    end
    else
    begin
      ABCheckTreeView1.Items.AddChild(nil,tempName);
    end;
    SaveFuncTreeView;
  end;
end;

procedure TABRegisterForm.Button11Click(Sender: TObject);
var
  tempName:string;
begin
  if (ABInPutStr('名称',tempName)) and
     (tempName<>EmptyStr) then
  begin
    if (Assigned(ABCheckTreeView1.Selected)) then
    begin
      ABCheckTreeView1.Selected.Text:=tempName;
    end;
    SaveFuncTreeView;
  end;
end;

procedure TABRegisterForm.Button4Click(Sender: TObject);
begin
  if (Assigned(ABCheckTreeView1.Selected)) then
  begin
    ABCheckTreeView1.Items.Delete(ABCheckTreeView1.Selected);
    SaveFuncTreeView;
  end;
end;

procedure TABRegisterForm.Button5Click(Sender: TObject);
begin
  if (Assigned(ABCheckTreeView1.Selected)) then
  begin
    ABCheckTreeView1.Selected.MoveTo(nil,naAdd);
    SaveFuncTreeView;
  end;
end;

procedure TABRegisterForm.SaveFuncTreeView;
begin
  ABCheckTreeView1.SaveToFile(ABSoftSetPath+'FuncTreeView.Data');
  ABCheckTreeView2.LoadFromFile(ABSoftSetPath+'FuncTreeView.Data');
end;

procedure TABRegisterForm.Button7Click(Sender: TObject);
begin
  ADOQuery1_1.Append;
end;

procedure TABRegisterForm.Button8Click(Sender: TObject);
begin
  ADOQuery1_1.Delete;
end;

procedure TABRegisterForm.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  if not FStartSetFuncStr then
    exit;

  SetFuncStr(ABCheckTreeView2,cxGrid1DBTableView1.DataController.DataSet.FieldByName('FuncRight').AsString);
end;

procedure TABRegisterForm.cxGridDBTableView2FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  if not FStartSetFuncStr then
    exit;

  SetFuncStr(ABCheckTreeView1,cxGridDBTableView2.DataController.DataSet.FieldByName('FuncRight').AsString);
end;

procedure TABRegisterForm.SpeedButton1Click(Sender: TObject);
var
  tempBegDatetime,
  tempEndDatetime:TDateTime;
  tempSubsequentClientCount,
  tempCardCount,
  tempControlCount,
  tempV100Count,
  tempV200Count,
  tempV300Count,
  tempReaderCount,
  tempTerminalCount,
  tempPrice:LongInt;

  tempClientInfo,
  tempFuncRight,
  tempCustomFunc,
  tempRemark:string;
begin
  UniteTemplate(
                tempBegDatetime,
                tempEndDatetime,
                tempSubsequentClientCount,
                tempCardCount,
                tempControlCount,
                tempV100Count,
                tempV200Count,
                tempV300Count,
                tempReaderCount,
                tempTerminalCount,
                tempPrice,

                tempClientInfo,
                tempFuncRight,
                tempCustomFunc,
                tempRemark
                );

  AppendOrEditSoftDog(false,
                      tempBegDatetime,
                      tempEndDatetime,
                      tempSubsequentClientCount,
                      tempCardCount,
                      tempControlCount,
                      tempV100Count,
                      tempV200Count,
                      tempV300Count,
                      tempReaderCount,
                      tempTerminalCount,
                      tempPrice,

                      tempClientInfo,
                      tempFuncRight,
                      tempCustomFunc,
                      tempRemark
                      );
end;

procedure TABRegisterForm.SpeedButton2Click(Sender: TObject);
var
  tempBegDatetime,
  tempEndDatetime:TDateTime;
  tempSubsequentClientCount,
  tempCardCount,
  tempControlCount,
  tempV100Count,
  tempV200Count,
  tempV300Count,
  tempReaderCount,
  tempTerminalCount:LongInt;
  tempPrice:LongInt;

  tempClientInfo,
  tempFuncRight,
  tempCustomFunc,
  tempRemark:string;
begin
  if ADOQuery1_2.State in [dsEdit,dsInsert] then
    ADOQuery1_2.post;

  UniteTemplate(
                tempBegDatetime,
                tempEndDatetime,
                tempSubsequentClientCount,
                tempCardCount,
                tempControlCount,
                tempV100Count,
                tempV200Count,
                tempV300Count,
                tempReaderCount,
                tempTerminalCount,
                tempPrice,

                tempClientInfo,
                tempFuncRight,
                tempCustomFunc,
                tempRemark
                );

  AppendOrEditSoftDog(true,
                      tempBegDatetime,
                      tempEndDatetime,
                      tempSubsequentClientCount,
                      tempCardCount,
                      tempControlCount,
                      tempV100Count,
                      tempV200Count,
                      tempV300Count,
                      tempReaderCount,
                      tempTerminalCount,
                      tempPrice,

                      tempClientInfo,
                      tempFuncRight,
                      tempCustomFunc,
                      tempRemark
                      );
end;

procedure TABRegisterForm.UniteTemplate(
                          var aBegDatetime,
                          aEndDatetime:TDateTime;
                          var aSubsequentClientCount,
                          aCardCount,
                          aControlCount,
                          aV100Count,
                          aV200Count,
                          aV300Count,
                          aReaderCount,
                          aTerminalCount,
                          aPrice:LongInt;

                          var aClientInfo,
                          aFuncRight,aCustomFunc,aRemark:string);
var
  i,j:LongInt;
  tempStrings1,
  tempStrings2,
  tempStrings5,
  tempStrings6:TStrings;
begin
  aBegDatetime:=0;
  aEndDatetime:=0;
  aSubsequentClientCount:=0;
  aCardCount:=0;
  aControlCount:=0;
  aV100Count:=0;
  aV200Count:=0;
  aV300Count:=0;
  aReaderCount:=0;
  aTerminalCount:=0;
  aPrice:=0;

  aClientInfo:=emptystr;
  aFuncRight:=emptystr;
  aCustomFunc:=emptystr;

  tempStrings1:=TStringList.Create;
  tempStrings2:=TStringList.Create;
  tempStrings5:=TStringList.Create;
  tempStrings6:=TStringList.Create;
  FStartSetFuncStr:=False;
  try
    for I := 0 to cxGrid1DBTableView1.DataController.GetSelectedCount- 1 do
    begin
      j:= cxGrid1DBTableView1.DataController.GetSelectedRowIndex(i);
      cxGrid1DBTableView1.DataController.DataSet.RecNo:=cxGrid1DBTableView1.DataController.GetRowInfo(j).RecordIndex+1;

      if (ADOQuery1_1.FindField('BegDatetime').AsDateTime>0)  then
      begin
        if (aBegDatetime=0) then
          aBegDatetime:=ADOQuery1_1.FindField('BegDatetime').AsDateTime
        else
          aBegDatetime:=min(aBegDatetime,ADOQuery1_1.FindField('BegDatetime').AsDateTime);
      end;

      if (ADOQuery1_1.FindField('EndDatetime').AsDateTime>0)  then
      begin
        if (aEndDatetime=0) then
          aEndDatetime:=ADOQuery1_1.FindField('EndDatetime').AsDateTime
        else
          aEndDatetime:=max(aEndDatetime,ADOQuery1_1.FindField('EndDatetime').AsDateTime);
      end;

      aSubsequentClientCount        :=aSubsequentClientCount  +  ADOQuery1_1.FindField('SubsequentClientCount').AsInteger ;
      aCardCount                    :=aCardCount              +  ADOQuery1_1.FindField('CardCount').AsInteger             ;
      aControlCount                 :=aControlCount           +  ADOQuery1_1.FindField('ControlCount').AsInteger          ;
      aV100Count                    :=aV100Count              +  ADOQuery1_1.FindField('V100Count').AsInteger             ;
      aV200Count                    :=aV200Count              +  ADOQuery1_1.FindField('V200Count').AsInteger             ;
      aV300Count                    :=aV300Count              +  ADOQuery1_1.FindField('V300Count').AsInteger             ;
      aReaderCount                  :=aReaderCount            +  ADOQuery1_1.FindField('ReaderCount').AsInteger           ;
      aTerminalCount                :=aTerminalCount          +  ADOQuery1_1.FindField('TerminalCount').AsInteger         ;
      aPrice                        :=aPrice                  +  ADOQuery1_1.FindField('Price').AsInteger;
      aRemark                       :=aRemark+ ADOQuery1_1.FindField('Remark').AsString;

      if Length(ADOQuery1_1.FindField('FuncRight').AsString)=200 then
      begin
        if aFuncRight=EmptyStr then
          aFuncRight:=ADOQuery1_1.FindField('FuncRight').AsString
        else
        begin
          for j := 1 to 200 do
          begin
            if (aFuncRight[j]='0') and (ADOQuery1_1.FindField('FuncRight').AsString[j]='0') then
              aFuncRight[j]:='0'
            else
              aFuncRight[j]:='1';
          end;
        end;
      end;

      if ADOQuery1_1.FindField('ClientInfo').AsString<>EmptyStr then
      begin
        tempStrings1.Text:=ADOQuery1_1.FindField('ClientInfo').AsString;
        for j := 0 to tempStrings1.Count-1 do
        begin
          if tempStrings2.IndexOf(tempStrings1.Strings[j])<0 then
          begin
            tempStrings2.Add(tempStrings1.Strings[j]);
          end;
        end;
      end;
      if ADOQuery1_1.FindField('CustomFunc').AsString<>EmptyStr then
      begin
        tempStrings5.Text:=ADOQuery1_1.FindField('CustomFunc').AsString;
        for j := 0 to tempStrings5.Count-1 do
        begin
          if tempStrings6.IndexOf(tempStrings5.Strings[j])<0 then
          begin
            tempStrings6.Add(tempStrings5.Strings[j]);
          end;
        end;
      end;
    end;
    aClientInfo:= tempStrings2.Text;
    aCustomFunc:= tempStrings6.Text;
  finally
    tempStrings1.Free;
    tempStrings2.Free;
    tempStrings5.Free;
    tempStrings6.Free;
    FStartSetFuncStr:=True;
    SetFuncStr(ABCheckTreeView2,cxGrid1DBTableView1.DataController.DataSet.FieldByName('FuncRight').AsString);
  end;
end;

procedure TABRegisterForm.AppendOrEditSoftDog(aAppend:Boolean;
                          aBegDatetime,
                          aEndDatetime:TDateTime;
                          aSubsequentClientCount,
                          aCardCount,
                          aControlCount,
                          aV100Count,
                          aV200Count,
                          aV300Count,
                          aReaderCount,
                          aTerminalCount,
                          aPrice:LongInt;

                          aClientInfo,
                          aFuncRight,
                          aCustomFunc,aRemark:string);
var
  i,j:LongInt;
  tempStrings1,
  tempStrings2:TStrings;
begin
  if ADOQuery1_2.State in [dsBrowse] then
  begin
    if aAppend then
      ADOQuery1_2.Append
    else
      ADOQuery1_2.Edit
  end;

  if ADOQuery1_2.State in [dsEdit,dsInsert] then
  begin
    tempStrings1:=TStringList.Create;
    tempStrings2:=TStringList.Create;
    try
      if aBegDatetime>0 then
        ADOQuery1_2.FindField('BegDatetime').AsDateTime := aBegDatetime
      else
        ADOQuery1_2.FindField('BegDatetime').Clear;

      if aEndDatetime>0 then
        ADOQuery1_2.FindField('EndDatetime').AsDateTime := aEndDatetime
      else
        ADOQuery1_2.FindField('EndDatetime').Clear;

      if ADOQuery1_2.State in [dsEdit] then
      begin
        ADOQuery1_2.FindField('SubsequentClientCount').AsInteger    := aSubsequentClientCount+ADOQuery1_2.FindField('SubsequentClientCount').AsInteger ;
        ADOQuery1_2.FindField('Remark').AsString                    := aRemark               +ADOQuery1_2.FindField('Remark').AsString             ;

        ADOQuery1_2.FindField('CardCount').AsInteger                := aCardCount            +ADOQuery1_2.FindField('CardCount').AsInteger             ;
        ADOQuery1_2.FindField('ControlCount').AsInteger             := aControlCount         +ADOQuery1_2.FindField('ControlCount').AsInteger          ;
        ADOQuery1_2.FindField('V100Count').AsInteger                := aV100Count            +ADOQuery1_2.FindField('V100Count').AsInteger             ;
        ADOQuery1_2.FindField('V200Count').AsInteger                := aV200Count            +ADOQuery1_2.FindField('V200Count').AsInteger             ;
        ADOQuery1_2.FindField('V300Count').AsInteger                := aV300Count            +ADOQuery1_2.FindField('V300Count').AsInteger             ;
        ADOQuery1_2.FindField('ReaderCount').AsInteger              := aReaderCount          +ADOQuery1_2.FindField('ReaderCount').AsInteger           ;
        ADOQuery1_2.FindField('TerminalCount').AsInteger            := aTerminalCount        +ADOQuery1_2.FindField('TerminalCount').AsInteger         ;
        ADOQuery1_2.FindField('Price').AsInteger                    := aPrice                +ADOQuery1_2.FindField('Price').AsInteger         ;

        if Length(aFuncRight)=200 then
        begin
          for j := 1 to 200 do
          begin
            if (aFuncRight[j]='0') and (ADOQuery1_2.FindField('FuncRight').AsString[j]='0') then
              aFuncRight[j]:='0'
            else
              aFuncRight[j]:='1';
          end;

          ADOQuery1_2.FindField('FuncRight').AsString:=aFuncRight;
        end;
        if aClientInfo<>EmptyStr then
        begin
          tempStrings1.Text:=aClientInfo;
          tempStrings2.Text:=ADOQuery1_2.FindField('ClientInfo').AsString;
          for I := 0 to tempStrings1.Count-1 do
          begin
            if tempStrings2.IndexOf(tempStrings1.Strings[i])<0 then
            begin
              tempStrings2.Add(tempStrings1.Strings[i]);
            end;
          end;
          ADOQuery1_2.FindField('ClientInfo').AsString := tempStrings2.Text;
        end;

        if aCustomFunc<>EmptyStr then
        begin
          tempStrings1.Text:=aCustomFunc;
          tempStrings2.Text:=ADOQuery1_2.FindField('CustomFunc').AsString;
          for I := 0 to tempStrings1.Count-1 do
          begin
            if tempStrings2.IndexOf(tempStrings1.Strings[i])<0 then
            begin
              tempStrings2.Add(tempStrings1.Strings[i]);
            end;
          end;
          ADOQuery1_2.FindField('CustomFunc').AsString := tempStrings2.Text;
        end;
      end
      else
      begin
        ADOQuery1_2.FindField('SubsequentClientCount').AsInteger    := aSubsequentClientCount;
        ADOQuery1_2.FindField('ClientInfo').AsString    := aClientInfo  ;
        ADOQuery1_2.FindField('CustomFunc').AsString    := aCustomFunc  ;
        ADOQuery1_2.FindField('Remark').AsString    := aRemark  ;

        ADOQuery1_2.FindField('CardCount').AsInteger    := aCardCount   ;
        ADOQuery1_2.FindField('ControlCount').AsInteger := aControlCount;
        ADOQuery1_2.FindField('V100Count').AsInteger    := aV100Count   ;
        ADOQuery1_2.FindField('V200Count').AsInteger    := aV200Count   ;
        ADOQuery1_2.FindField('V300Count').AsInteger    := aV300Count   ;
        ADOQuery1_2.FindField('ReaderCount').AsInteger  := aReaderCount  ;
        ADOQuery1_2.FindField('TerminalCount').AsInteger:= aTerminalCount;
        ADOQuery1_2.FindField('Price').AsInteger        := aPrice ;

        ADOQuery1_2.FindField('FuncRight').AsString     := aFuncRight;
      end;
    finally
      tempStrings1.Free;
      tempStrings2.Free;
    end;
    SetFuncStr(ABCheckTreeView1,cxGridDBTableView2.DataController.DataSet.FieldByName('FuncRight').AsString);
  end;
end;

procedure TABRegisterForm.SpeedButton7Click(Sender: TObject);
begin
  ABPostDataset(ADOQuery1_2);
  if WriteToDog then
  begin
    WriteToLog;
  end;
end;

function TABRegisterForm.WriteToDog:boolean;
var
  tempWrite:ansiString;
  tempOutChar: pansichar;
  tempStr:string;
  tempID,KeyResult,i:LongInt;
begin
  Result:=false;
  FStartSetFuncStr:=False;
  try
    KeyResult:=ReadSoftDog(tempStr);
    if (KeyResult= 0)  then
    begin
      tempID:=StrToIntDef(ABGetItemValue(tempStr,'SoftDogID=','',';'),0);
      //if (tempID=0) or (ADOQuery1_2.FieldByName('ID').AsInteger=tempID) then
      begin
        //if (tempID=0)  then
        begin
          if ADOQuery1_2.State in [dsBrowse] then
            ADOQuery1_2.Edit;

          if ADOQuery1_2.State in [dsEdit,dsInsert] then
          begin
            ADOQuery1_2.Edit;
            ADOQuery1_2.FindField('Count').AsInteger := ADOQuery1_2.FindField('Count').AsInteger+1;
            ADOQuery1_2.post;
          end;
        end;

        tempWrite:='FuncRight='+ABFillStr(ADOQuery1_2.FindField('FuncRight').AsString,200,'0',axdright)+';'+
                   'SoftDogID='+inttostr(ADOQuery1_2.FindField('ID').AsInteger)+';'+
                   'BegDatetime='+ABDateTimeToStr(ADOQuery1_2.FindField('BegDatetime').AsDateTime)+';'+
                   'EndDatetime='+ABDateTimeToStr(ADOQuery1_2.FindField('EndDatetime').AsDateTime)+';'+
                   'SubsequentClientCount='+inttostr(ADOQuery1_2.FindField('SubsequentClientCount').AsInteger)+';'+
                   'CardCount='+inttostr(ADOQuery1_2.FindField('CardCount').AsInteger)+';'+
                   'ControlCount='+inttostr(ADOQuery1_2.FindField('ControlCount').AsInteger)+';'+
                   'V100Count='+inttostr(ADOQuery1_2.FindField('V100Count').AsInteger)+';'+
                   'V200Count='+inttostr(ADOQuery1_2.FindField('V200Count').AsInteger)+';'+
                   'V300Count='+inttostr(ADOQuery1_2.FindField('V300Count').AsInteger)+';'+
                   'ReaderCount='+inttostr(ADOQuery1_2.FindField('ReaderCount').AsInteger)+';'+
                   'TerminalCount='+inttostr(ADOQuery1_2.FindField('TerminalCount').AsInteger)+';';
        i:=YWriteString(pansichar(tempWrite),0,pansichar('11111111'),pansichar('11111111'),KeyPath);
        if i=0 then
        begin
          Result:=true;
          ABShow('加密狗成功写入[%s]字节.',[inttostr(length(tempWrite))]);
        end;
       {
      end
      else
      begin
        ABShow('当前狗ID[%s]与客户[%s]狗ID[%s]不符,请检查.',[inttostr(tempID),
                                                             ADOQuery1_2.FieldByName('RegName').AsString,
                                                             ADOQuery1_2.FieldByName('ID').AsString]);
         }
      end;
    end
    else
    begin
      ABShow('读狗失败，返回值为%s',[inttostr(KeyResult)]);
    end;
  finally
    FStartSetFuncStr:=true;
  end;
end;

function TABRegisterForm.ReadSoftDog(var aResult:string):LongInt;
var
  KeyResult : Integer;
  OutChar: pansichar;
  tempID,mylen:integer;
  tempRemark:string;
begin
  aResult:=EmptyStr;
  mylen := 480;
  OutChar:= AllocMem(mylen+1); //这里增加1，用于储存结束字符串
  ZeroMemory(OutChar,mylen+1);
  try
    Result:= YReadString(OutChar,0, mylen,pansichar('11111111'),pansichar('11111111'), KeyPath);
    if Result=0 then
      aResult:=AnsiString(OutChar);
  finally
    FreeMem(OutChar);
  end;
end;

procedure TABRegisterForm.SpeedButton5Click(Sender: TObject);
var
  tempID,
  KeyResult : Integer;
  tempStr,
  tempRemark:string;
  tempForm:TABShowRightForm;
begin
  FStartSetFuncStr:=False;
  try
    KeyResult:=ReadSoftDog(tempStr);
    if (KeyResult= 0)  then
    begin
      tempID:=StrToIntDef(ABGetItemValue(tempStr,'SoftDogID=','',';'),0);
      if tempID>0 then
      begin
        tempRemark:=
            '狗ID('                 +trim(ABGetItemValue(tempStr,'SoftDogID=','',';'))+') '+ ABEnterWrapStr+
            '开始时间('             +trim(ABGetItemValue(tempStr,'BegDatetime=','',';'))+') '+ ABEnterWrapStr+
            '结束时间('             +trim(ABGetItemValue(tempStr,'EndDatetime=','',';'))+') '+ ABEnterWrapStr+
            '并发客户端数('         +trim(ABGetItemValue(tempStr,'SubsequentClientCount=','',';'))+') '+ ABEnterWrapStr+
            '卡数量('               +trim(ABGetItemValue(tempStr,'CardCount=','',';'))+') '+ ABEnterWrapStr+
            '控制器数量('           +trim(ABGetItemValue(tempStr,'ControlCount=','',';'))+') '+ ABEnterWrapStr+
            'V100数量('             +trim(ABGetItemValue(tempStr,'V100Count=','',';'))+') '+ ABEnterWrapStr+
            'V200数量('             +trim(ABGetItemValue(tempStr,'V200Count=','',';'))+') '+ ABEnterWrapStr+
            'V300数量('             +trim(ABGetItemValue(tempStr,'V300Count=','',';'))+') '+ ABEnterWrapStr+
            '读卡器数量('           +trim(ABGetItemValue(tempStr,'ReaderCount=','',';'))+') '+ ABEnterWrapStr+
            '消费机数量('           +trim(ABGetItemValue(tempStr,'TerminalCount=','',';'))+')';

        tempForm:=TABShowRightForm.Create(nil);
        try
          if ADOQuery1_2.Locate('ID',tempID,[]) then
          begin
            tempForm.Caption:='加密狗客户名称为:'+ADOQuery1_2.FindField('RegName').AsString;
          end
          else
          begin
            tempForm.Caption:='加密狗未注册';
          end;
          tempForm.mmo1.Text:='详细权限如下:'+ABEnterWrapStr+tempRemark;

          ABCheckTreeView1.SaveToFile(ABSoftSetPath+'FuncTreeView.Data');
          tempForm.RzCheckTree3.LoadFromFile(ABSoftSetPath+'FuncTreeView.Data');
          SetFuncStr(tempForm.RzCheckTree3,ABGetItemValue(tempStr,'FuncRight=','',';'));

          tempForm.ShowModal;
        finally
          tempForm.Free;
        end;
      end
      else
      begin
        ABShow('狗ID[%s]未找到.',[inttostr(tempID)]);
      end;
    end
    else
    begin
      ABShow('读狗失败，返回值为%s',[inttostr(KeyResult)]);
    end;
  finally
    FStartSetFuncStr:=true;
  end;
end;

procedure TABRegisterForm.WriteToLog;
begin
  ADOQuery1_21.Insert;
  ADOQuery1_21.FieldByName('ListID').AsInteger:=ADOQuery1_2.FieldByName('ID').AsInteger;
  ADOQuery1_21.FieldByName('Datetime').AsDateTime:=Now;
  ADOQuery1_21.FieldByName('Remark').AsString:=
    '将客户信息写入狗[开始时间('+ABDateTimeToStr(ADOQuery1_2.FindField('BegDatetime').AsDateTime)+') '+
                     '结束时间('+ABDateTimeToStr(ADOQuery1_2.FindField('EndDatetime').AsDateTime)+') '+
                     '并发客户端数('+inttostr(ADOQuery1_2.FindField('SubsequentClientCount').AsInteger)+') '+
                     '卡数量('+inttostr(ADOQuery1_2.FindField('CardCount').AsInteger)+') '+
                     '控制器数量('+inttostr(ADOQuery1_2.FindField('ControlCount').AsInteger)+') '+
                     'V100数量('+inttostr(ADOQuery1_2.FindField('V100Count').AsInteger)+') '+
                     'V200数量('+inttostr(ADOQuery1_2.FindField('V200Count').AsInteger)+') '+
                     'V300数量('+inttostr(ADOQuery1_2.FindField('V300Count').AsInteger)+') '+
                     '读卡器数量('+inttostr(ADOQuery1_2.FindField('ReaderCount').AsInteger)+') '+
                     '消费机数量('+inttostr(ADOQuery1_2.FindField('TerminalCount').AsInteger)+') '+
                     '菜单权限('+ADOQuery1_2.FindField('FuncRight').AsString+') '+
                    ']';
  ADOQuery1_21.Post;
end;

end.



