object ABServiceToolsForm: TABServiceToolsForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #26381#21153#27880#20876#24037#20855
  ClientHeight = 167
  ClientWidth = 506
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 31
    Top = 30
    Width = 60
    Height = 13
    AutoSize = False
    Caption = #26381#21153#21517#31216
  end
  object Label2: TLabel
    Left = 31
    Top = 54
    Width = 60
    Height = 13
    AutoSize = False
    Caption = 'EXE'#36335#24452
  end
  object SpeedButton1: TSpeedButton
    Left = 473
    Top = 54
    Width = 23
    Height = 22
    Caption = '...'
    OnClick = SpeedButton1Click
  end
  object Label3: TLabel
    Left = 206
    Top = 30
    Width = 60
    Height = 13
    AutoSize = False
    Caption = #26174#31034#21517#31216
  end
  object Button1: TButton
    Left = 45
    Top = 105
    Width = 70
    Height = 25
    Caption = #27880#20876
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 121
    Top = 105
    Width = 70
    Height = 25
    Caption = #21368#36733
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 209
    Top = 105
    Width = 70
    Height = 25
    Caption = #21551#21160
    TabOrder = 2
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 285
    Top = 105
    Width = 70
    Height = 25
    Caption = #20572#27490
    TabOrder = 3
    OnClick = Button4Click
  end
  object Edit1: TEdit
    Tag = 91
    Left = 93
    Top = 27
    Width = 110
    Height = 21
    TabOrder = 4
    OnExit = Edit1Exit
  end
  object Edit2: TEdit
    Tag = 91
    Left = 93
    Top = 54
    Width = 377
    Height = 21
    TabOrder = 5
  end
  object Edit3: TEdit
    Tag = 91
    Left = 268
    Top = 27
    Width = 202
    Height = 21
    TabOrder = 6
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 148
    Width = 506
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object Button5: TButton
    Left = 382
    Top = 105
    Width = 88
    Height = 25
    Caption = #21047#26032#29366#24577
    TabOrder = 8
    OnClick = Button5Click
  end
end
