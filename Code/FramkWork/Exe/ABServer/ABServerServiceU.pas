unit ABServerServiceU;

interface

uses
  Windows,Messages,SysUtils,Classes,Graphics,Controls,SvcMgr,Dialogs;

type
  TABSoftIPRegister = class(TService)
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServicePause(Sender: TService; var Paused: Boolean);
    procedure ServiceContinue(Sender: TService; var Continued: Boolean);
  private
    { Private declarations }
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  ABSoftIPRegister: TABSoftIPRegister;

implementation

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  ABSoftIPRegister.Controller(CtrlCode);
end;

function TABSoftIPRegister.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TABSoftIPRegister.ServiceStart(Sender: TService;
  var Started: Boolean);
begin
  Started := true;
end;

procedure TABSoftIPRegister.ServiceStop(Sender: TService;
  var Stopped: Boolean);
begin
  Stopped := true;
end;

procedure TABSoftIPRegister.ServicePause(Sender: TService;
  var Paused: Boolean);
begin
  Paused := true;
end;

procedure TABSoftIPRegister.ServiceContinue(Sender: TService;
  var Continued: Boolean);
begin
  Continued := true;
end;

end.
