unit Main;

interface

uses
  ABPubFormU,
  ABPubConstU,
  ABPubFuncU,
  ABPubVarU,
  ABPubUserU,
  ABPubLogU,
  ABPubLocalParamsU,
  ABPubThreadU,

  ABThirdConnDatabaseU,

  ServerContainerUnit1,

  SysUtils,Classes,Controls,Forms,Dialogs,ExtCtrls,ComCtrls,StdCtrls,Grids,
  ExtDlgs,DateUtils, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TMainForm = class(TABPubForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    StringGrid1: TStringGrid;
    Timer1: TTimer;
    Panel1: TPanel;
    Button_ConnSetup: TButton;
    Button_Start: TButton;
    Button_Stop: TButton;
    Panel2: TPanel;
    Button2: TButton;
    Button3: TButton;
    SaveTextFileDialog1: TSaveTextFileDialog;
    TabSheet3: TTabSheet;
    Memo1: TMemo;
    StatusBar1: TStatusBar;
    Label5: TLabel;
    Label6: TLabel;
    Edit5: TEdit;
    Edit6: TEdit;
    CheckBox2: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure Button_ConnSetupClick(Sender: TObject);
    procedure Button_StartClick(Sender: TObject);
    procedure Button_StopClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Memo1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDestroy(Sender: TObject);
  private
    FBeginDatetime:TDateTime;
    FTimerDoStart:Boolean;
    procedure StartEnabled;
    procedure StopEnabled;
    { Private declarations }
  public
    procedure addLog(aStr: string);
    procedure BackLog(aUseCriticalSection:boolean=true);
    procedure AddClient(aIpAddress, aClientPort, aProtocol, aCreateDatetime,aUpdateDatetime,aRemark: string);
    procedure DeleteClient(aIP,aPort: string);
    procedure EditUpdateDatetime(aIpAddress, aClientPort,aUpdateDatetime: string);
    { Public declarations }
  end;


var
  MainForm: TMainForm;

implementation

{$R *.dfm}

{ TMainForm }
procedure TMainForm.BackLog(aUseCriticalSection:boolean);
var
  tempBackFileName:string;
begin
  if aUseCriticalSection then
    ABEnterCriticalSection;
  try
    if Memo1.Lines.Count>0 then
    begin
      tempBackFileName:=FormatDateTime('YYYY-MM-DD HH-NN-SS-ZZZ',now);
      Memo1.Lines.SaveToFile(ABBackPath+tempBackFileName+'.txt');
      Memo1.Lines.Clear;
    end;
  finally
    if aUseCriticalSection then
      ABLeaveCriticalSection;
  end;
end;

procedure TMainForm.addLog(aStr: string);
begin
  ABEnterCriticalSection;
  try
    if Memo1.Lines.Count>5000 then
    begin
      BackLog(False);
    end;

    if (Assigned(ServerContainer1)) and (ServerContainer1.ClientIP<>EmptyStr) then
    begin
      aStr:= 'ClientIP:'+ServerContainer1.ClientIP+' ClientPort:'+ServerContainer1.ClientPort+' '+aStr;
    end;
    aStr:= FormatDateTime('YYYY-MM-DD HH:NN:SS:ZZZ',now)+' '+aStr;

    Memo1.Lines.Add(aStr);
  finally
    ABLeaveCriticalSection;
  end;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  BackLog;
end;

procedure TMainForm.Button2Click(Sender: TObject);
begin
  BackLog;
  Memo1.Lines.Clear;
end;

procedure TMainForm.AddClient(aIpAddress, aClientPort, aProtocol,aCreateDatetime,aUpdateDatetime,aRemark: string);
var
  i:LongInt;
begin
  ABEnterCriticalSection;
  try
    i:=StringGrid1.RowCount;
    StringGrid1.Cells[0,i-1]:=IntToStr(i-1);
    StringGrid1.Cells[1,i-1]:=aIpAddress;
    StringGrid1.Cells[2,i-1]:=aClientPort;
    StringGrid1.Cells[3,i-1]:=aProtocol;
    StringGrid1.Cells[4,i-1]:=aCreateDatetime;
    StringGrid1.Cells[5,i-1]:=aUpdateDatetime;
    StringGrid1.Cells[6,i-1]:=aRemark;

    StringGrid1.RowCount:=i+1;
  finally
    ABLeaveCriticalSection;
  end;
end;

procedure TMainForm.EditUpdateDatetime(aIpAddress, aClientPort,aUpdateDatetime: string);
var
  i:LongInt;
begin
  ABEnterCriticalSection;
  try
    for I := 1 to StringGrid1.RowCount-1 do
    begin
      if (AnsiCompareText(StringGrid1.Cells[1,i],aIpAddress)=0) and
         (AnsiCompareText(StringGrid1.Cells[2,i],aClientPort)=0) then
      begin
        StringGrid1.Cells[5,i]:=aUpdateDatetime;
        break;
      end;
    end;
  finally
    ABLeaveCriticalSection;
  end;
end;

procedure TMainForm.DeleteClient(aIP,aPort: string);
var
  I,k,tempFindIndex: integer;
begin
  ABEnterCriticalSection;
  try
    tempFindIndex:=0;
    for I := 1 to StringGrid1.RowCount-1 do
    begin
      if (AnsiCompareText(StringGrid1.Cells[1,i],aIP)=0) and
         (AnsiCompareText(StringGrid1.Cells[2,i],aPort)=0) then
      begin
        tempFindIndex:=i;
        break;
      end;
    end;

    if tempFindIndex > 0 then
    begin
      for k := tempFindIndex to StringGrid1.RowCount - 2 do
      begin
        StringGrid1.Rows[k]:=StringGrid1.Rows[k+1] ;
        StringGrid1.Cells[0,K]:=INTTOSTR(K);
      end;
      StringGrid1.RowCount := StringGrid1.RowCount - 1;
    end;
  finally
    ABLeaveCriticalSection;
  end;
end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  Button_StopClick(Button_Stop);
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  FBeginDatetime:=Now;
  ABLocalParams.LoginType:=ltCS_Two;

  StatusBar1.Panels.Items[1].Text:=FormatDateTime('YYYY-MM-DD HH:NN:SS',FBeginDatetime);
  StatusBar1.Panels.Items[7].Text:=Format('%.2f', [ABGetTotalMemory / 1024]);

  StringGrid1.Cells[0,0]:='序号';
  StringGrid1.Cells[1,0]:='IP地址';
  StringGrid1.Cells[2,0]:='端口号';
  StringGrid1.Cells[3,0]:='协议';
  StringGrid1.Cells[4,0]:='连接时间';
  StringGrid1.Cells[5,0]:='更新时间';
  StringGrid1.Cells[6,0]:='程序文件';

  PageControl1.ActivePageIndex:=0;
  Memo1.Lines.Clear;
  FTimerDoStart:=true;
  addLog('运行程序');

  ABThirdConnDatabaseU.ABSetConn(False);
  Timer1.Enabled:=true;
end;

procedure TMainForm.Memo1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  ABEnterCriticalSection;
  try
    if (Shift=[ssCtrl]) then
    begin
      if  (Key=65) then
      begin
        Memo1.SelectAll;
      end;
    end;
  finally
    ABLeaveCriticalSection;
  end;
end;

procedure TMainForm.Button3Click(Sender: TObject);
var
  tempFileName:string;
begin
  ABEnterCriticalSection;
  try
    tempFileName:=ABSaveFile('','', 'txt|*.txt');
    if tempFileName<>EmptyStr then
    begin
      Memo1.Lines.SaveToFile(tempFileName)
    end;
  finally
    ABLeaveCriticalSection;
  end;
end;

procedure TMainForm.Button_ConnSetupClick(Sender: TObject);
begin
  if ABThirdConnDatabaseU.ABSetConn(True) then
  begin
    Button_StopClick(Button_Stop);
    Button_StartClick(Button_Start);
  end;
end;

procedure TMainForm.StartEnabled;
begin
  Button_Start.Enabled:=false;
  Button_Stop.Enabled:=true;
end;

procedure TMainForm.StopEnabled;
begin
  Button_Start.Enabled:=true;
  Button_Stop.Enabled:=false;
end;

procedure TMainForm.Button_StartClick(Sender: TObject);
var
  i:LongInt;
begin
  for i:=1 to StringGrid1.rowcount-1 do
    StringGrid1.rows[i].clear;

  StringGrid1.RowCount:=2;
  StringGrid1.FixedRows:=1;
  StringGrid1.FixedCols:=1;
  StartEnabled;
  try
    ServerContainer1.DSServer1.Stop;
    ServerContainer1.DSServer1.Start;

    ServerContainer1.DSTCPServerTransport1.Stop;
    ServerContainer1.DSTCPServerTransport1.Port:=StrToInt(Edit5.Text);
    ServerContainer1.DSTCPServerTransport1.Start;

    Edit5.Enabled:=false;
    Edit6.Enabled:=false;
    Button_ConnSetup.Enabled:=false;
    addLog('启动');
    FTimerDoStart:=false;
  except
    StopEnabled;
    raise;
  end;
end;

procedure TMainForm.Button_StopClick(Sender: TObject);
begin
  StopEnabled;
  try
    ServerContainer1.DSServer1.Stop;
    ServerContainer1.DSTCPServerTransport1.Stop;

    Edit5.Enabled:=True;
    Edit6.Enabled:=True;
    Button_ConnSetup.Enabled:=True;
    addLog('停止');
  except
    StartEnabled;
    raise;
  end;
end;

procedure TMainForm.Timer1Timer(Sender: TObject);
var
  tempDatetime:TDateTime;
  AYear, AMonth, ADay, AHour, AMinute, ASecond,AMilliSecond: Word;
begin                                          
  Timer1.Enabled:=false;
  try
    try
      //开始运行时如果3秒没手工启动则自动启动
      if (FTimerDoStart) and
         (Button_Start.Enabled) then
      begin
        Button_StartClick(Button_Start);
      end;

      //刷新已运行时间
      AYear:=0;
      AMonth:=0;
      ADay:=0;
      AHour:=0;
      AMinute:=0;
      ASecond:=0;
      AMilliSecond:=0;
      tempDatetime:=now-FBeginDatetime;
      if tempDatetime>=1 then
        DecodeDate(tempDatetime, AYear, AMonth, ADay);
      DecodeTime(tempDatetime, AHour, AMinute, ASecond, AMilliSecond);
      StatusBar1.Panels.Items[3].Text:=IntToStr(AYear)+'年'+
                                       IntToStr(AMonth)+'月'+
                                       IntToStr(ADay)+'天'+
                                       IntToStr(AHour)+'时'+
                                       IntToStr(AMinute)+'分'+
                                       IntToStr(ASecond)+'秒';
      //刷新内存使用情况
      StatusBar1.Panels.Items[5].Text:=Format('%.3f', [ABGetProcessMemorySize(ABAppFullName) / 1000]);
    except
      on E: Exception do
      begin
        addLog('Timer1Timer发生异常：'+e.Message);
      end;
    end;
  finally
    Timer1.Enabled:=true;
  end;
end;


end.


