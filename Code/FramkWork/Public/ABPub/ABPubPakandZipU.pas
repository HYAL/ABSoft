{
三方合并文件单元
}
unit ABPubPakandZipU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFuncU,
  ABPubManualProgressBar_ThreadU,

  Classes, SysUtils;


//将多个文件合成一个包
function ABDoPak(aFileNames: Tstrings; aPakFileName: string): Boolean;
//增加文件到包中
procedure ABAddPakItem(aPakFileName: string; aFileName: string);
//从包中删除文件
procedure ABDeletePakItem(aPakFileName: string; aFileName: string);

//将包解到多个文件中
procedure ABUnDoPak(aPakFileName: string; aFilePath: string; aString: TStrings);
//从包中释放单个文件
procedure ABPakItemToFile(aPakFileName, aFileName: string);
//取包的文件数目
function ABGetPakItemCount(aPakFileName: string): LongInt;

implementation

const
  VER_MAJOR_CURRENT = 0;
  VER_MINOR_CURRENT = 1;

type
  TcymPakHeader = class(TPersistent)
  private
    fAuthorID: array[0..49] of Char;
    fReserved: array[0..23] of Char;
    FDirSize: LongWord;
    FDirOffset: LongWord;
    FVersionMinor: Word;
    FVersionMajor: Word;
    procedure SetDirOffset(const Value: LongWord);
    procedure SetDirSize(const Value: LongWord);
    procedure SetVersionMajor(const Value: Word);
    procedure SetVersionMinor(const Value: Word);
    function GetID: string;
    procedure SetID(const Value: string);
  protected

  public
    constructor Create(AOwner: TComponent);
    destructor Destroy; override;

    procedure LoadFromStream(Strm: TStream);
    procedure WriteToStream(Strm: TStream);

    property VersionMajor: Word read FVersionMajor write SetVersionMajor;
    property VersionMinor: Word read FVersionMinor write SetVersionMinor;
    property DirOffset: LongWord read FDirOffset write SetDirOffset;
    property DirSize: LongWord read FDirSize write SetDirSize;
    property AuthorID: string read GetID write SetID;
  published

  end;

  TcymPakDirectoryItem = class(TPersistent)
  private
    FSize: LongWord;
    FOffSet: LongWord;
    FItemType: Word;
    fItemName: array[0..255] of Char;
    procedure SetItemType(const Value: Word);
    procedure SetOffSet(const Value: LongWord);
    procedure SetSize(const Value: LongWord);
    function GetItemName: string;
    procedure SetItemname(const Value: string);

  protected

  public

    constructor Create;
    destructor Destroy; override;

    procedure LoadFromStream(Strm: TStream);
    procedure WriteToStream(Strm: TStream);

    property ItemType: Word read FItemType write SetItemType;
    property OffSet: LongWord read FOffSet write SetOffSet;
    property Size: LongWord read FSize write SetSize;
    property Itemname: string read GetItemName write SetItemname;

  published

  end;

  TcymPakDirectory = class(TPersistent)
  private
    fItems: TList;
    function GetItems(Index: Integer): TcymPakDirectoryItem;
    function GetCount: Integer;

  protected

  public
    constructor Create(AOwner: TComponent);
    destructor Destroy; override;

    procedure ClearItems;
    procedure WriteToStream(Strm: TStream);
    procedure LoadFromStream(Strm: TStream; NumItems: Word);
    procedure AddItem(Item: TcymPakDirectoryItem);
    procedure DeleteItem(Index: Integer);

    property Items[Index: Integer]: TcymPakDirectoryItem read GetItems;
    property Count: Integer read GetCount;

  published

  end;

  TcymPak = class(TComponent)
  private
    fFileName: string;
    fStream: TFileStream;
    fHeader: TcymPakHeader;
    fDirectory: TcymPakDirectory;

    procedure SetDirectory(const Value: TcymPakDirectory);
    procedure SetHeader(const Value: TcymPakHeader);

  protected

  public
    constructor Create(AOwner: TComponent; fName: string); reintroduce;
    destructor Destroy; override;

    procedure AddItem(fName, Desc: string; fType: Word);
    procedure DeleteItem(Index: Integer);
    procedure ItemToFile(Index: Integer; fName: string);
    procedure ItemToStream(Index: Integer; Strm: TStream);
    procedure UpdatePak;
    procedure PakPak;

    property Directory: TcymPakDirectory read FDirectory write SetDirectory;
    property Header: TcymPakHeader read FHeader write SetHeader;
  published

  end;

function ABGetPakItemCount(aPakFileName: string): LongInt;
var
  tempPAK: TcymPak;
begin
  result := -1;
  if FileExists(aPakFileName) then
  begin
    ABUnCompressFile(aPakFileName,aPakFileName);
    tempPAK := TcymPak.Create(nil, aPakFileName);
    try
      result := tempPAK.fDirectory.Count;
    finally
      tempPAK.Free;
    end;
  end;
end;

//将多个文件压成一个包
function ABDoPak(aFileNames: Tstrings; aPakFileName: string): Boolean;
var
  tempCurProgress: Integer;
  tempPAK: TcymPak;
  i: LongInt;
begin
  result := false;
  if aPakFileName <> EmptyStr then
  begin
    if Copy(extractfileext(aPakFileName), 2, maxint) = '' then
    begin
      aPakFileName := aPakFileName + '.pak';
    end;

    if (FileExists(aPakFileName)) then
      DeleteFile(aPakFileName);

    ABPubManualProgressBar_ThreadU.ABRunProgressBar(tempCurProgress,aFileNames.Count );
    try
      tempPAK := TcymPak.Create(nil, aPakFileName);
      try
        for I := 0 to aFileNames.Count - 1 do
        begin
          tempCurProgress:=tempCurProgress+1;

          tempPAK.AddItem(aFileNames[i], extractfilename(aFileNames[i]), 0);
        end;
        result := true;
      finally
        tempPAK.Free;
      end;
      ABCompressFile(aPakFileName,aPakFileName);
    finally
      ABPubManualProgressBar_ThreadU.ABStopProgressBar;
    end;
  end;
end;

procedure ABUnDoPak(aPakFileName: string; aFilePath: string; aString: TStrings);
var
  tempPAK: TcymPak;
  i: LongInt;
  tempCurProgress: Integer;
begin
  if FileExists(aPakFileName) then
  begin
    if Assigned(aString) then
      aString.Clear;

    ABUnCompressFile(aPakFileName,aPakFileName);
    tempPAK := TcymPak.Create(nil, aPakFileName);
    try
      ABPubManualProgressBar_ThreadU.ABRunProgressBar(tempCurProgress,tempPAK.fDirectory.Count);
      try
        for I := 0 to tempPAK.fDirectory.Count - 1 do
        begin
          tempCurProgress:=tempCurProgress+1;

          if Assigned(aString) then
            aString.Add(aFilePath + tempPAK.fDirectory.Items[i].Itemname);
          tempPAK.ItemToFile(i, aFilePath + tempPAK.fDirectory.Items[i].Itemname);
        end;
      finally
        ABPubManualProgressBar_ThreadU.ABStopProgressBar;
      end;
    finally
      tempPAK.Free;
    end;
  end;
end;

//增加文件到包中

procedure ABAddPakItem(aPakFileName: string; aFileName: string);
var
  tempPAK: TcymPak;
begin
  if FileExists(aPakFileName) then
  begin
    ABUnCompressFile(aPakFileName,aPakFileName);
    tempPAK := TcymPak.Create(nil, aPakFileName);
    try
      tempPAK.AddItem(aFileName,  extractfilename(aFileName), 0);
      tempPAK.PakPak;
    finally
      tempPAK.Free;
    end;
    ABCompressFile(aPakFileName,aPakFileName);
  end;
end;
//从包中删除文件

procedure ABDeletePakItem(aPakFileName: string; aFileName: string);
var
  tempPAK: TcymPak;
  i: LongInt;
begin
  if FileExists(aPakFileName) then
  begin
    ABUnCompressFile(aPakFileName,aPakFileName);
    tempPAK := TcymPak.Create(nil, aPakFileName);
    try
      for I := 0 to tempPAK.fDirectory.Count - 1 do
      begin
        if AnsiCompareText(tempPAK.fDirectory.Items[i].Itemname, extractfilename(aFileName)) = 0 then
        begin
          tempPAK.DeleteItem(i);
          tempPAK.PakPak;
          break;
        end;
      end;
    finally
      tempPAK.Free;
    end;
    ABCompressFile(aPakFileName,aPakFileName);
  end;
end;

//从包中释放文件

procedure ABPakItemToFile(aPakFileName, aFileName: string);
var
  tempPAK: TcymPak;
  i: LongInt;
begin
  if FileExists(aPakFileName) then
  begin
    ABUnCompressFile(aPakFileName,aPakFileName);
    tempPAK := TcymPak.Create(nil, aPakFileName);
    try
      for I := 0 to tempPAK.fDirectory.Count - 1 do
      begin
        if AnsiCompareText(tempPAK.fDirectory.Items[i].Itemname, extractfilename(aFileName)) = 0 then
        begin
          tempPAK.ItemToFile(i, aFileName);
          break;
        end;
      end;
    finally
      tempPAK.Free;
    end;
  end;
end;

procedure StrToArr(var Arr: array of Char; const Str: string);
var
  loop, last: Integer;
begin
  //fillchar(Arr, High(Arr), #0);
  //edit by grj
  fillchar(Arr, sizeof(Arr), #0);

  if length(Str) > High(Arr) then last := High(Arr)
  else Last := Length(Str);
  for loop := 0 to last do
  begin
    Arr[loop] := Str[loop + 1];
  end;
end;

{ TcymPakDirectory }

procedure TcymPakDirectory.AddItem(Item: TcymPakDirectoryItem);
begin
  fItems.Add(Item);
end;

procedure TcymPakDirectory.ClearItems;
var
  loop: Integer;
begin
  if fItems.Count > 0 then
    for loop := fItems.Count - 1 downto 0 do
    begin
      TcymPakDirectoryItem(fItems[loop]).Free;
      fItems.Delete(loop);
    end;
end;

constructor TcymPakDirectory.Create(AOwner: TComponent);
begin
  inherited Create;
  fItems := TList.Create;
end;

procedure TcymPakDirectory.DeleteItem(Index: Integer);
begin
  TcymPakDirectoryItem(fItems[Index]).Free;
  fItems.Delete(Index);
end;

destructor TcymPakDirectory.Destroy;
begin
//Clear and free item list
  ClearItems;
  fItems.Free;
  fItems := nil;

  inherited Destroy;
end;

function TcymPakDirectory.GetCount: Integer;
begin
  RESULT := fItems.Count;
end;

function TcymPakDirectory.GetItems(Index: Integer): TcymPakDirectoryItem;
begin
  RESULT := TcymPakDirectoryItem(fItems[Index]);
end;

procedure TcymPakDirectory.LoadFromStream(Strm: TStream;
  NumItems: Word);
var
  loop: word;
  DirItem: TcymPakDirectoryItem;
begin
  for loop := 0 to NumItems - 1 do
  begin
    DirItem := TcymPakDirectoryItem.Create;
    DirItem.LoadFromStream(Strm);
    fItems.Add(DirItem);
  end;
end;

procedure TcymPakDirectory.WriteToStream(Strm: TStream);
var
  loop: word;
begin
  if fItems.Count > 0 then
  begin
    for loop := 0 to fItems.Count - 1 do
    begin
      TcymPakDirectoryItem(fItems[loop]).WriteToStream(Strm);
    end;
  end;
end;

{ TcymPakDirectoryItem }

constructor TcymPakDirectoryItem.Create;
begin
  inherited Create;
  //FillChar(fItemName, High(fItemName) + 1, #0);
  //edit by grj
  FillChar(fItemName, sizeof(fItemName), #0);

  FItemType := 0;
  FOffSet := 0;
  FSize := 0;
end;

destructor TcymPakDirectoryItem.Destroy;
begin

  inherited Destroy;
end;

function TcymPakDirectoryItem.GetItemName: string;
begin
  RESULT := fItemName;
end;

procedure TcymPakDirectoryItem.LoadFromStream(Strm: TStream);
begin
  //Strm.Read(fItemName, High(fItemName) + 1);
  //edit by grj
  Strm.Read(fItemName, sizeof(fItemName));

  Strm.Read(fItemType, SizeOf(fItemType));
  Strm.Read(fOffset, SizeOf(fOffset));
  Strm.Read(fSize, SizeOf(fSize));
end;

procedure TcymPakDirectoryItem.SetItemname(const Value: string);
begin
  StrToArr(fItemName, Value);
end;

procedure TcymPakDirectoryItem.SetItemType(const Value: Word);
begin
  FItemType := Value;
end;

procedure TcymPakDirectoryItem.SetOffSet(const Value: LongWord);
begin
  FOffSet := Value;
end;

procedure TcymPakDirectoryItem.SetSize(const Value: LongWord);
begin
  FSize := Value;
end;

procedure TcymPakDirectoryItem.WriteToStream(Strm: TStream);
begin
  //Strm.Write(fItemName, High(fItemName) + 1);
  //edit by grj
  Strm.Write(fItemName, sizeof(fItemName));

  Strm.Write(fItemType, SizeOf(fItemType));
  Strm.Write(fOffset, SizeOf(fOffset));
  Strm.Write(fSize, SizeOf(fSize));
end;


{ TcymPakHeader }

constructor TcymPakHeader.Create(AOwner: TComponent);
begin
  inherited Create;
  //FillChar(fAuthorID, High(fAuthorID) + 1, #0);
  //FillChar(fReserved, High(fReserved) + 1, 'X');
  //edit by grj
  FillChar(fAuthorID, sizeOf(fAuthorID), #0);
  FillChar(fReserved, sizeOf(fReserved), 'X');


  fVersionMajor := VER_MAJOR_CURRENT;
  fVersionMinor := VER_MINOR_CURRENT;
  fDirOffset := 0;
  fDirSize := 0;
end;

destructor TcymPakHeader.Destroy;
begin

  inherited Destroy;
end;

function TcymPakHeader.GetID: string;
begin
  RESULT := fAuthorID;
end;

procedure TcymPakHeader.LoadFromStream(Strm: TStream);
begin
  Strm.Read(fVersionMajor, SizeOf(fVersionMajor));
  Strm.Read(fVersionMinor, SizeOf(fVersionMinor));
  Strm.Read(fDirOffset, SizeOf(fDirOffset));
  Strm.Read(fDirSize, SizeOf(fDirSize));
  //Strm.Read(fAuthorID, High(fAuthorID) + 1);
  //Strm.Read(fReserved, High(fReserved) + 1);
  //edit by grj
  Strm.Read(fAuthorID, SizeOf(fAuthorID));
  Strm.Read(fReserved, SizeOf(fReserved));
end;

procedure TcymPakHeader.SetDirOffset(const Value: LongWord);
begin
  FDirOffset := Value;
end;

procedure TcymPakHeader.SetDirSize(const Value: LongWord);
begin
  FDirSize := Value;
end;

procedure TcymPakHeader.SetID(const Value: string);
begin
  StrToArr(fAuthorID, Value);
end;

procedure TcymPakHeader.SetVersionMajor(const Value: Word);
begin
  FVersionMajor := Value;
end;

procedure TcymPakHeader.SetVersionMinor(const Value: Word);
begin
  FVersionMinor := Value;
end;

procedure TcymPakHeader.WriteToStream(Strm: TStream);
begin
  Strm.Write(fVersionMajor, SizeOf(fVersionMajor));
  Strm.Write(fVersionMinor, SizeOf(fVersionMinor));
  Strm.Write(fDirOffset, SizeOf(fDirOffset));
  Strm.Write(fDirSize, SizeOf(fDirSize));
  //Strm.Write(fAuthorID, High(fAuthorID) + 1);
  //Strm.Write(fReserved, High(fReserved) + 1);
  //edit by grj
  Strm.Write(fAuthorID, sizeof(fAuthorID));
  Strm.Write(fReserved, sizeof(fReserved));
end;

{ TcymPak }

procedure TcymPak.AddItem(fName, Desc: string; fType: Word);
var
  FS: TFileStream;
  DirItem: TcymPakDirectoryItem;
begin
  if FileExists(fName) then //Make sure file exists
  begin
    //Open file for reading then write it to the end of the file.
    //(Overwrite the directory, which is updated from memory later)
    FS := TFileStream.Create(fName, fmOpenRead);
    DirItem := TcymPakDirectoryItem.Create;
    fStream.Position := fHeader.DirOffSet;
    DirItem.OffSet := fStream.Position;
    DirItem.Size := FS.Size;
    DirItem.ItemType := fType;
    DirItem.Itemname := Desc;
    FS.Position := 0;
    fStream.CopyFrom(FS, FS.Size);

    //Add the item to the pak directory
    fDirectory.AddItem(DirItem);
    fHeader.DirOffSet := fStream.Position;

    //Close the file to be added
    FS.Free;
  end;
end;

constructor TcymPak.Create(AOwner: TComponent; fName: string);
var
  fMode: Word;
begin
  inherited Create(AOwner);

  //If the file exists we are going to read it, else we will assume we are
  //going to create a new one
  if FileExists(fName) = false then
    //fMode := (fmCreate or fmOpenReadWrite)
    //edit by grj
    fMode := fmCreate
  else
    fMode := fmOpenReadWrite;
  fFileName := fName;
  fStream := TFileStream.Create(fName, fMode);
  fHeader := TcymPakHeader.Create(Self);
  fDirectory := TcymPakDirectory.Create(Self);
  if (fmCreate xor fMode = 0) then
  begin
    //fmCreate IS in fMode, so create new
    fHeader.WriteToStream(fStream);
    fHeader.DirOffset := fStream.Position;
  end
  else
  begin
    //fmCreate is NOT in fMode, so Loadfrom file
    fStream.Position := 0;
    fHeader.LoadFromStream(fStream);
    //Make sure we compensate for a 0 size directory
    if fHeader.DirSize > 0 then
    begin
      fStream.Position := fHeader.DirOffset;
      fDirectory.LoadFromStream(fStream, fHeader.DirSize);
    end
    else
    begin
      fHeader.DirOffset := fStream.Position;
    end;
  end;
end;

procedure TcymPak.DeleteItem(Index: Integer);
begin
  //simply delete the item from the directory, we can free the space in the
  //file by calling Pak.PakPak (Silly procname = yes).  I did this so it
  //only has to make disk writes occasionally when removing lots of items.
  fDirectory.DeleteItem(Index);
end;

destructor TcymPak.Destroy;
begin
  //Make sure the directory and header are up to date and written to the pak
  //then frre all the sub-objects.
  UpdatePak;
  fHeader.Free;
  fDirectory.Free;
  fStream.Free;
  inherited Destroy;
end;

procedure TcymPak.ItemToFile(Index: Integer; fName: string);
var
  FS: TFileStream;
begin
  FS := TFileStream.Create(fName, fmCreate);
  try
    fStream.Position := fDirectory.Items[Index].OffSet;
    FS.CopyFrom(fStream, fDirectory.Items[Index].Size);
  finally
    FS.Free;
  end;
end;

procedure TcymPak.ItemToStream(Index: Integer; Strm: TStream);
begin
  fStream.Position := fDirectory.Items[Index].OffSet;
  Strm.CopyFrom(fStream, fDirectory.Items[Index].Size);
end;

procedure TcymPak.PakPak;
(***************************************************************************
This may be a very inneficient way of updating the file and recovering space,
I havent tested it too much.
***************************************************************************)
var
  loop: Integer;
  WriteS, ReadS: TFileStream;
begin
//Write all directory and header changes
  UpdatePak;
//Close the pak so we can open it once for writing and once for reading
  fStream.Free;
//Create Read and write stream
  WriteS := TFileStream.Create(fFileName, fmOpenWrite or fmShareDenyNone);
  ReadS := TFileStream.Create(fFileName, fmOpenRead or fmShareDenyNone);
  WriteS.Position := 0;
  ReadS.Position := 0;
  fHeader.WriteToStream(WriteS);
  for loop := 0 to fDirectory.Count - 1 do
  begin
    ReadS.Position := fDirectory.Items[loop].OffSet;
    fDirectory.Items[loop].Offset := WriteS.Position;
    WriteS.CopyFrom(ReadS, fDirectory.Items[loop].Size);
  end;
  WriteS.Size := WriteS.Position;
  fHeader.DirOffset := WriteS.Position;
  ReadS.Free;
  WriteS.Free;
  fStream := TFileStream.Create(fFileName, fmOpenReadWrite);
  UpdatePak;
end;

procedure TcymPak.SetDirectory(const Value: TcymPakDirectory);
begin
  fDirectory.Assign(Value);
end;

procedure TcymPak.SetHeader(const Value: TcymPakHeader);
begin
  fHeader.Assign(Value);
end;

procedure TcymPak.UpdatePak;
begin
  //Make the changes to the pak file Header and Directory current.
  fStream.Position := 0;
  fHeader.DirSize := fDirectory.Count;
  fHeader.WriteToStream(fStream);
  fStream.Position := fHeader.DirOffset;
  fDirectory.WriteToStream(fStream);
end;


end.

