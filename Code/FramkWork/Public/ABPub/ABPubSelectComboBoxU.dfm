object ABSelectComboBoxForm: TABSelectComboBoxForm
  Left = 446
  Top = 227
  Caption = #36873#25321#23383#27573
  ClientHeight = 100
  ClientWidth = 300
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  DesignSize = (
    300
    100)
  PixelsPerInch = 96
  TextHeight = 13
  object ComboBox1: TComboBox
    Left = 16
    Top = 24
    Width = 276
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    ImeName = #24555#20048#20116#31508
    TabOrder = 0
    Text = 'ComboBox1'
  end
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 300
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      300
      41)
    object Button1: TButton
      Left = 219
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #21462#28040
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 139
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #30830#23450
      TabOrder = 1
      OnClick = Button2Click
    end
  end
end
