{
TreeView选择窗体单元
}
unit ABPubSelectTreeViewU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,
  ABPubDBU,
  ABPubFuncU,

  SysUtils,Classes,Controls,Forms,ExtCtrls,StdCtrls,ComCtrls,DB;

type
  TABSelectTreeViewForm = class(TABPubForm)
    pnl1: TPanel;
    btn1: TButton;
    btn2: TButton;
    TreeView1: TTreeView;
    procedure btn2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public                                              
    { Public declarations }
  end;


//选择数据集中的树形结点
function ABSelectTreeView(
                          aDataSet: TDataSet;
                          aParentField: string;
                          aKeyField: string;
                          aNameField: string;
                          aMainLevelOrder:Boolean;

                          aDetailDataSet: TDataSet;
                          aDetailParentField: string;
                          aDetailNameField: string;

                          var aValues:string;

                          aCaption:String=''
                          ):Boolean;

implementation

{$R *.dfm}
function ABSelectTreeView(
  aDataSet: TDataSet;
  aParentField: string;
  aKeyField: string;
  aNameField: string;
  aMainLevelOrder:Boolean;

  aDetailDataSet: TDataSet;
  aDetailParentField: string;
  aDetailNameField: string;

  var aValues:string;

  aCaption:String
  ):Boolean;
var
  tempForm:TABSelectTreeViewForm;
  tempFind:TTreeNode;
begin
  Result:=false;
  if (Assigned(aDataSet)) and
     (aDataSet.Active) and
     (aDataSet.FieldCount>0) then
  begin
    tempForm:=TABSelectTreeViewForm.Create(nil);
    if aCaption<>emptystr then
      tempForm.Caption:=aCaption;
    try
      ABDataSetsToTree
      (
        tempForm.TreeView1.Items,
        aDataSet,
        aParentField,
        aKeyField,
        aNameField,
        aMainLevelOrder,

        aDetailDataSet,
        aDetailParentField,
        aDetailNameField
        );


      tempFind:= ABFindTree(tempForm.TreeView1.Items,aValues,[]);
      if Assigned(tempFind) then
      begin
        tempFind.Selected:=true;
      end;
      if tempForm.ShowModal=mrOK then
      begin
        if Assigned(tempForm.TreeView1.Selected) then
          aValues:=tempForm.TreeView1.Selected.Text;
        Result:=True;
      end;
    finally
      tempForm.Free;
    end;
  end;
end;

procedure TABSelectTreeViewForm.btn1Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TABSelectTreeViewForm.btn2Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;



end.
