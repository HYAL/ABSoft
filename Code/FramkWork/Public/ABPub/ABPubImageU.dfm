object ABImageForm: TABImageForm
  Left = 288
  Top = 194
  Caption = 'Image'#32534#36753#31383#20307
  ClientHeight = 400
  ClientWidth = 500
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 0
    Top = 0
    Width = 500
    Height = 359
    Align = alClient
    Stretch = True
    ExplicitWidth = 649
    ExplicitHeight = 413
  end
  object Panel1: TPanel
    Left = 0
    Top = 359
    Width = 500
    Height = 41
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      500
      41)
    object Button1: TButton
      Left = 8
      Top = 6
      Width = 75
      Height = 25
      Caption = #23548#20837
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 96
      Top = 6
      Width = 75
      Height = 25
      Caption = #23548#20986
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 403
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #30830#35748
      TabOrder = 2
      OnClick = Button3Click
    end
  end
  object SaveDialog1: TSaveDialog
    Filter = 'bmp|*.bmp'
    Left = 455
    Top = 319
  end
  object OpenDialog1: TOpenDialog
    Filter = 'bmp|*.bmp'
    Left = 423
    Top = 263
  end
end
