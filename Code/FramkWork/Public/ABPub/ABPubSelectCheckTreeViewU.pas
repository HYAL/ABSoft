{
CheckTreeView选择窗体单元
}
unit ABPubSelectCheckTreeViewU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,
  ABPubFuncU,
  ABPubConstU,
  ABPubCheckTreeViewU,
  ABPubDBU,
  ABPubLogU,

  SysUtils,Classes,Controls,Forms,ExtCtrls,StdCtrls,ComCtrls,DB;

type
  TABSelectCheckTreeViewForm = class(TABPubForm)
    pnl1: TPanel;
    btn1: TButton;
    btn2: TButton;
    ABcxButton1: TButton;
    ABcxButton2: TButton;
    ABcxButton3: TButton;
    RzCheckTree3: TABCheckTreeView;
    procedure btn2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure ABcxButton1Click(Sender: TObject);
    procedure ABcxButton2Click(Sender: TObject);
    procedure ABcxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//选择数据集中的树形结点
function ABSelectCheckTreeView(
                                aDataSet: TDataSet;
                                aParentField: string;
                                aKeyField: string;
                                aNameField: string;
                                aMainLevelOrder:Boolean;

                                aDetailDataSet: TDataSet;
                                aDetailParentField: string;
                                aDetailNameField: string;

                                astrings:Tstrings;
                                aSelectField:string;

                                aProgressBar:TProgressBar=nil;
                                aCaption:String=''
                                ):Boolean;

implementation

{$R *.dfm}
function ABSelectCheckTreeView(
  aDataSet: TDataSet;
  aParentField: string;
  aKeyField: string;
  aNameField: string;
  aMainLevelOrder:Boolean;

  aDetailDataSet: TDataSet;
  aDetailParentField: string;
  aDetailNameField: string;

  aStrings:Tstrings;
  aSelectField:string;

  aProgressBar:TProgressBar;
  aCaption:String
  ):Boolean;
var
  tempForm:TABSelectCheckTreeViewForm;
  I: Integer;
  tempStrings:TStringList;
begin
  Result:=false;
  if (Assigned(aDataSet)) and
     (aDataSet.Active) and
     (aDataSet.FieldCount>0) then
  begin
    tempForm:=TABSelectCheckTreeViewForm.Create(nil);
    if aCaption<>emptystr then
      tempForm.Caption:=aCaption;
    astrings.BeginUpdate;
    try
      ABDataSetsToTree
      (
        tempForm.RzCheckTree3.Items,
        aDataSet,
        aParentField,
        aKeyField,
        aNameField,
        aMainLevelOrder,

        aDetailDataSet,
        aDetailParentField,
        aDetailNameField
        );

      if (astrings.Count>0) and
         (astrings.Text<>emptystr) then
      begin
        ABSetProgressBarBegin(aProgressBar,tempForm.RzCheckTree3.Items.Count);
        tempStrings:=TStringList.Create;
        try
          //TStringList().Sorted:=true;将自动使用二分法查找，IndexOf速度成倍提高
          tempStrings.Sorted:=true;
          tempStrings.Assign(astrings);

          for I := 0 to tempForm.RzCheckTree3.Items.Count-1 do
          begin
            ABSetProgressBarNext(aProgressBar);

            if (Assigned(tempForm.RzCheckTree3.Items[i].Data)) and
               (not ABPointerIsNull(tempForm.RzCheckTree3.Items[i].Data)) then
            begin
              if (Assigned(aDetailDataSet)) and
                 (ABSetDatasetRecno(aDetailDataSet,LongInt(tempForm.RzCheckTree3.Items[i].Data),'2')>0) and
                 (tempStrings.IndexOf(aDetailDataSet.FieldByName(aSelectField).AsString)>=0) or
                 (not Assigned(aDetailDataSet)) and
                 (ABSetDatasetRecno(aDataSet,LongInt(tempForm.RzCheckTree3.Items[i].Data))>0) and
                 (tempStrings.IndexOf(aDataSet.FieldByName(aSelectField).AsString)>=0) then
              begin
                if (not tempForm.RzCheckTree3.Items[i].HasChildren) then
                begin
                  TABCheckTreeNode(tempForm.RzCheckTree3.Items[i]).InternalSetState(cbchecked,False,false);
                end
                else
                begin
                  TABCheckTreeNode(tempForm.RzCheckTree3.Items[i]).CheckState:=cbchecked;
                end;
              end
              else
              begin
                //TABCheckTreeNode(tempForm.RzCheckTree3.Items[i]).InternalSetState(cbUnchecked,False,false);
              end;
            end
            else
            begin
              //TABCheckTreeNode(tempForm.RzCheckTree3.Items[i]).InternalSetState(cbUnchecked,False,false);
            end;
          end;
        finally
          tempStrings.Free;
          ABSetProgressBarEnd(aProgressBar);
        end;
        ABSetCheckTreeViewParentNodeState(tempForm.RzCheckTree3,aProgressBar);
      end;

      if tempForm.ShowModal=mrOK then
      begin
        astrings.Clear;
        for I := 0 to tempForm.RzCheckTree3.Items.Count-1 do
        begin
          if (not tempForm.RzCheckTree3.Items[i].HasChildren) and
             (tempForm.RzCheckTree3.Checked[tempForm.RzCheckTree3.Items[i]]) and
             (Assigned(tempForm.RzCheckTree3.Items[i].Data)) and
             (not ABPointerIsNull(tempForm.RzCheckTree3.Items[i].Data)) then
          begin
            if (Assigned(aDetailDataSet)) then
            begin
              if (ABSetDatasetRecno(aDetailDataSet,LongInt(tempForm.RzCheckTree3.Items[i].Data),'2')>0) then
                astrings.Add(aDetailDataSet.FieldByName(aSelectField).AsString);
            end
            else
            begin
              if ABSetDatasetRecno(aDataSet,LongInt(tempForm.RzCheckTree3.Items[i].Data))>0 then
                astrings.Add(aDataSet.FieldByName(aSelectField).AsString);
            end;
          end;
        end;
        Result:=True;
      end;
    finally
      astrings.EndUpdate;
      tempForm.Free;
    end;
  end;
end;

procedure TABSelectCheckTreeViewForm.ABcxButton1Click(Sender: TObject);
begin
  inherited;
  ABSetCheckTreeViewState(RzCheckTree3,coCheck);
end;

procedure TABSelectCheckTreeViewForm.ABcxButton2Click(Sender: TObject);
begin
  inherited;
  ABSetCheckTreeViewState(RzCheckTree3,coUnCheck);
end;

procedure TABSelectCheckTreeViewForm.ABcxButton3Click(Sender: TObject);
begin
  inherited;
  ABSetCheckTreeViewState(RzCheckTree3,coReverse);
end;

procedure TABSelectCheckTreeViewForm.btn1Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TABSelectCheckTreeViewForm.btn2Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;




end.
