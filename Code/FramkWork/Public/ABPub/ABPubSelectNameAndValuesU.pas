{
选择来源与目标对应字串的窗体单元（可多选择）
}
unit ABPubSelectNameAndValuesU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFuncU,


  SysUtils,Classes,Controls,Forms,ExtCtrls,StdCtrls;

type
  TABSelectNameAndValuesForm = class(TForm)
    Panel2: TPanel;
    Panel1: TPanel;
    ComboBox1: TComboBox;
    Label1: TLabel;
    ComboBox2: TComboBox;
    Label2: TLabel;
    SpeedButton1: TButton;
    Button1: TButton;
    Button2: TButton;
    ListBox1: TListBox;
    procedure SpeedButton1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


//在来源与目标列表中选择对应项
function ABSelectNameAndValues(aLeftStrings,aRightStrings:Tstrings;
                             var aValue:string;
                             aSpaceSign:string=',';
                             aCaption:string='';
                             aFromCaption:string='';
                             aToCaption:string=''
                             ):Boolean;

implementation

{$R *.dfm}
function ABSelectNameAndValues(aLeftStrings,aRightStrings:Tstrings;
                             var aValue:string;
                             aSpaceSign:string;
                             aCaption:string;
                             aFromCaption:string;
                             aToCaption:string):Boolean;
var
  tempForm:TABSelectNameAndValuesForm;
begin
  Result:=false;
  tempForm:=TABSelectNameAndValuesForm.Create(nil);
  try
    if aCaption<>emptystr then
      tempForm.caption:=aCaption;
    if aFromCaption<>emptystr then
      tempForm.Label1.caption:=aFromCaption;
    if aToCaption<>emptystr then
      tempForm.Label2.caption:=aToCaption;

    tempForm.ComboBox1.Items.Assign(aLeftStrings);
    tempForm.ComboBox2.Items.Assign(aRightStrings);
    ABStrsToStrings(aValue,aSpaceSign,tempForm.ListBox1.Items);

    if tempForm.ShowModal=mrOK then
    begin
      aValue:=ABStringsToStrs(tempForm.ListBox1.Items,aSpaceSign);
      Result:=true;
    end;
  finally
    tempForm.Free;
  end;
end;

procedure TABSelectNameAndValuesForm.Button1Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TABSelectNameAndValuesForm.Button2Click(Sender: TObject);
begin
  if ListBox1.ItemIndex>=0 then
  begin
    ListBox1.Items.Delete(ListBox1.ItemIndex);
  end;
end;

procedure TABSelectNameAndValuesForm.SpeedButton1Click(Sender: TObject);
begin
  if (ComboBox1.Text<>EmptyStr) and (ComboBox2.Text<>EmptyStr) then
  begin
    if ListBox1.Items.IndexOf(ComboBox1.Text+'='+ComboBox2.Text)<0 then
      ListBox1.Items.Add(ComboBox1.Text+'='+ComboBox2.Text);
  end;
end;

end.


