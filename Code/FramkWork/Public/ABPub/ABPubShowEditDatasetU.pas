{
数据集显示编辑窗体单元
}
unit ABPubShowEditDatasetU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,
  ABPubMultilingualDBNavigatorU,

  SysUtils,Classes,Controls,Forms,DB,ExtCtrls,DBCtrls,DBGrids,StdCtrls, Grids;

type
  TABShowEditDatasetForm = class(TABPubForm)
    DBGrid1: TDBGrid;
    ABMultilingualDBNavigator1: TABMultilingualDBNavigator;
    DataSource1: TDataSource;
    pnl1: TPanel;
    btn1: TButton;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//显示或编辑数据集数据
//aAppend=是否能增加
//aDelete=是否能删除
//aEdit=是否能修改
//aCaption=显示框标题
procedure ABShowEditDataset(aDataSet:TDataset;
                            aAppend:Boolean=true;aDelete:Boolean=true;aEdit:Boolean=true;
                            aCaption:string='';
                            aSetMaxWidth:Boolean=true;
                            aMaxWidth:LongInt=100);

implementation

{$R *.dfm}

procedure ABShowEditDataset(aDataSet:TDataset;aAppend,aDelete,aEdit:Boolean;aCaption:string;
                            aSetMaxWidth:Boolean;
                            aMaxWidth:LongInt);
var
  tempForm: TABShowEditDatasetForm;
  i:LongInt;
begin
  if not Assigned(aDataSet) then
    exit;

  tempForm := TABShowEditDatasetForm.Create(nil);
  try
    if aAppend then
      tempForm.ABMultilingualDBNavigator1.VisibleButtons:=tempForm.ABMultilingualDBNavigator1.VisibleButtons+[nbInsert]
    else
      tempForm.ABMultilingualDBNavigator1.VisibleButtons:=tempForm.ABMultilingualDBNavigator1.VisibleButtons-[nbInsert];
    if aDelete then
      tempForm.ABMultilingualDBNavigator1.VisibleButtons:=tempForm.ABMultilingualDBNavigator1.VisibleButtons+[nbDelete]
    else
      tempForm.ABMultilingualDBNavigator1.VisibleButtons:=tempForm.ABMultilingualDBNavigator1.VisibleButtons-[nbDelete];
    if aEdit then
      tempForm.ABMultilingualDBNavigator1.VisibleButtons:=tempForm.ABMultilingualDBNavigator1.VisibleButtons+[nbEdit]
    else
      tempForm.ABMultilingualDBNavigator1.VisibleButtons:=tempForm.ABMultilingualDBNavigator1.VisibleButtons-[nbEdit];

    if (not aAppend) and
       (not aEdit)  then
      tempForm.ABMultilingualDBNavigator1.VisibleButtons:=tempForm.ABMultilingualDBNavigator1.VisibleButtons-[nbPost,nbCancel];

    if (not aAppend) and (not aDelete) and (not aEdit) then
      tempForm.ABMultilingualDBNavigator1.Visible:=false;

    if aCaption<>EmptyStr then
      tempForm.Caption:= aCaption;
    tempForm.Caption:=(tempForm.Caption);

    tempForm.Datasource1.DataSet:=aDataSet;
    if aSetMaxWidth then
    begin
      for I := 0 to tempForm.DBGrid1.Columns.Count-1 do
      begin
        if tempForm.DBGrid1.Columns[i].Width>aMaxWidth then
          tempForm.DBGrid1.Columns[i].Width:=aMaxWidth;
      end;
    end;

    tempForm.ShowModal;
  finally
    tempForm.Free;
  end;
end;

procedure TABShowEditDatasetForm.btn1Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TABShowEditDatasetForm.Button1Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

end.

