{
使用多线程的可控进度条单元
调用时传入当前位置的变量地址，再在主线程内修改变量的值就能在线程中不停的显示进度条位置了
//使用方法：
var
  tempMax,tempCur,i: Integer;
begin
  tempCur:=0;
  tempMax:= 1000;
  ABPubManualProgressBar_ThreadU.ABRunProgressBar(tempCur,tempMax);
  try
    for I := 1 to tempMax do
    begin
      tempCur:=i;
      Sleep(2);
    end;
  finally
    ABPubManualProgressBar_ThreadU.ABStopProgressBar;
  end;
end;

在显示的过程中如果要暂停进度条做其它操作，操作完成后后再恢复进度条,则要
var
  tempMax,tempCur,i: Integer;
begin
  tempCur:=0;
  tempMax:= 1000;

  ABPubManualProgressBar_ThreadU.ABRunProgressBar(tempCur,tempMax);
  try
    for I := 1 to 500 do
    begin
      tempCur:=i;
      Sleep(2);
    end;
    ABPubManualProgressBar_ThreadU.ABStopProgressBar;
    Sleep(3000);
    tempCur:=501;
    ABPubManualProgressBar_ThreadU.ABRunProgressBar(tempCur,tempMax);
    for I := 502 to tempMax do
    begin
      tempCur:=i;
      Sleep(2);
    end;
  finally
    ABPubManualProgressBar_ThreadU.ABStopProgressBar;
  end;
end;
}
unit ABPubManualProgressBar_ThreadU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubVarU,
  ABPubConstU,
  ABPubLocalParamsU,

  Windows,Messages,CommCtrl,Forms,SysUtils;


//启动进度条
//aCur=进度条当前位置,
//aMax=进度条最大值
//Handle=呼叫窗口句柄
//Caption=进度条窗品标题
//aWidth=进度条宽度
procedure ABRunProgressBar(var aCur:Longint;
                           aMax:LongInt=100;
                           aCaption: string='';
                           aHandle: THandle=0;
                           aWidth:LongInt=320);
//停止进度条
procedure ABStopProgressBar;

var
  ABProgressState:TABProgressState;

implementation

const
	AppName  = 'ABManualProgressBar_Thread';
	ID_PROGBAR = $FF;

var
	Master  : THandle;     {呼叫窗口}
	hThread : Thandle;     {附加进程}

	hWindow  : hWnd;       {主窗口}
	hProgBar : hWnd;       {进度条}
  hEditWnd : hwnd;

  hWidth :Integer;       //主窗体宽度
	pTimer : integer;      {时钟}

	aMessage : TMsg;       {消息}

	Registered : Boolean = False;  {注册标识}
  MasterDone : Boolean = False;

	WinCaption : string = 'Running'; {'作业进行中...';}

  HCur:PLongInt;         {当前进度}
  HMax:LongInt;          {最大进度}

function WindowProc(hWindow: HWnd; aMessage, Wparam, LParam:Longint): Longint; stdcall;
begin
	Result := 0;
	case aMessage of
		wm_Destroy:
			begin
				KillTimer(hWindow,pTimer);
				PostQuitMessage(0);
			end;
		wm_Timer:
			begin
        SetWindowText(hEditWnd, PChar(IntToStr(HCur^)+'/'+IntToStr(HMax)));
				SendMessage(hProgBar,PBM_SETPOS,HCur^,0);

        if MasterDone and (HCur^ = HMax) then
          SendMessage(hWindow,WM_Destroy,0,0);
			end;
		else WindowProc := CallWindowProc(@DefWindowProc, hWindow,AMessage,WParam,LParam);
	end;

  if ABProgressState=PsStop then
  begin
    KillTimer(hWindow, pTimer);
    PostQuitMessage(0);
  end;
end;

function WinRegister: Boolean;
var
	WindowClass: TWndClass;
begin
	WindowClass.style := cs_vRedraw or cs_hRedraw;
	WindowClass.lpfnWndProc := @WindowProc;
	WindowClass.cbClsExtra := 0;
	WindowClass.cbWndExtra := 0;
	WindowClass.hInstance := hInstance;
	WindowClass.hIcon := LoadIcon(0,idi_Application);
	WindowClass.hCursor := LoadCursor(0,idc_Arrow);
	WindowClass.hbrBackground := Hbrush(Color_Window);
	WindowClass.lpszMenuName := nil;
	WindowClass.lpszClassName := AppName;

	Result :=Windows.RegisterClass(WindowClass) <> 0;
end;

function WinCreate: Boolean;
var
	{主窗口的位置及大小}
	L,T : integer;
	W : integer;
	H : integer;

	{进度条的位置及大小}
	pL : integer ;
	pT : integer ;
	pW : integer ;
	pH : integer ;

 dwStyle: DWORD;
begin
	W := hWidth;
	H := 80;

	pL := 18;
	pT := 18;
	pW := W-40;
	pH := H-60;

	{确定窗口的位置:居中}
	L := (Screen.Width  - W) div 2;
	T := (Screen.Height - H) div 2;
	{创建主窗口}
	hWindow  := CreateWindowEx(WS_EX_TOPMOST, AppName, PChar(WinCaption), WS_Caption or WS_VISIBLE or WS_DLGFRAME,
													 L, T, W, H, 0, 0, hInstance, nil);
	{创建进度条}
	hProgBar := CreateWindow(PROGRESS_CLASS,nil,WS_CHILD or WS_VISIBLE,
													 pL,pT,pW,pH,hWindow,HMENU(ID_PROGBAR),hInstance,nil);
	{创建进度提示}
   dwStyle := WS_VISIBLE or WS_CHILD or ES_MULTILINE or ES_AUTOVSCROLL or ES_LEFT;
   hEditWnd := CreateWindow('EDIT', nil, dwStyle, (W - 50) div 2,0,(W - 50) div 2,17, hWindow, 0, hInstance, nil);

	{设置进度条的基本属性}
	SendMessage(hProgBar,PBM_SETRANGE32,1,LPARAM(HMax));
	SendMessage(hProgBar,PBM_SETSTEP,WPARAM(4),0);

	result := ( hWindow <> 0 ) and ( hProgBar <> 0 ) and ( hEditWnd <> 0 );
end;

procedure RunProgBar; stdcall;
begin
	{注册类别}
	if not Registered then
	begin
		Registered := WinRegister;
		if not Registered then Exit;
	end;
	{创建窗口}
	if MasterDone or (not WinCreate) then Exit;
	{启动时钟}
	SetTimer(hWindow, pTimer, 28, nil);
	{消息循环}
	while GetMessage(AMessage, 0, 0, 0) do
  begin
		TranslateMessage(Amessage);
		DispatchMessage(AMessage);
	end;
end;

{启动进度条}
procedure ABRunProgressBar(var aCur:Longint;aMax:LongInt;
                            aCaption: string;aHandle: THandle;aWidth:LongInt);
var
	ThreadID : DWord;
begin
 if ABLocalParams.Debug then
   exit;

  aCur:=0;

  ABProgressState:=PsRun;

  HCur:=@aCur;
  HMax:=aMax;
  hWidth:= aWidth;

  Application.ProcessMessages;
	Master := aHandle;
	if aCaption <> '' then
    WinCaption := aCaption;

  MasterDone := False;
  //hWindow := 0;
  //创建且立即运行线程
	hThread := CreateThread(nil,0,@RunProgBar,nil,0,ThreadID);
  //设置最低优先级
  {
THREAD_PRIORITY_ABOVE_NORMAL：比默认的稍高 
THREAD_PRIORITY_BELOW_NORMAL：比默认的稍低                                    
THREAD_PRIORITY_HIGHEST：这个级别最高 
THREAD_PRIORITY_IDLE：这是是空闲，也就是在没有其它钱程时才运行它
THREAD_PRIORITY_LOWEST：这个是最低 
THREAD_PRIORITY_NORMAL：正常的 
  }
	SetThreadPriority(hThread,THREAD_PRIORITY_LOWEST);

	CloseHandle(hThread);
end;

{停止进度条}
procedure ABStopProgressBar;
begin
  ABProgressState:=PsStop;

  SendMessage(hWindow, WM_Destroy, 0, 0);
  MasterDone := True;
	SendMessage(Master,wm_Paint,0,0);
	SetForegroundWindow(Master);
  Application.ProcessMessages;
end;




end.

