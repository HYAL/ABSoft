{
SQL解析单元
}
unit ABPubSQLParseU;

interface
{$I ..\ABInclude.ini}
uses
  ABPubMessageU,
  abpubfuncU,
  ABPubConstU,

  ABPubDBU,

  SysUtils,Classes,Variants,Comctrls,strutils;

type
  //SQL语句解析类
  PABSQLParseDef = ^TABSQLParseDef;
  TABSQLParseDef = record
    SQL: string;                  //解释的SQL

    BeforeSelectStr: string;      //Select语句的定义部分
    SingleTableName: string;      //单表时的表名

    SelectPos,                    //位Select 开始位置
    FromPos,                      //位From   位开始置
    WherePos,                     //位Where  位开始置
    GroupPos,                     //位Group  位开始置
    HavingPos,                    //位Having 位开始置
    OrderPos,                     //位Order  位开始置
    ComputePos,                   //位Compute位开始置
    UnionPos: longint;            //位Union  位开始置

    SelectEndPos,                 //位Select 结束位置
    FromEndPos,                   //位From   位结束置
    WhereEndPos,                  //位Where  位结束置
    GroupEndPos,                  //位Group  位结束置
    HavingEndPos,                 //位Having 位结束置
    OrderEndPos,                  //位Order  位结束置
    ComputeEndPos,                //位Compute位结束置
    UnionEndPos: longint;         //位Union  位结束置

    SelectStr,                    //位Select 内容
    FromStr,                      //位From   内容
    WhereStr,                     //位Where  内容
    GroupStr,                     //位Group  内容
    HavingStr,                    //位Having 内容
    OrderStr,                     //位Order  内容
    ComputeStr,                   //位Compute内容
    UnionStr: string;             //位Union  内容

    ParentDef: PABSQLParseDef;    //上一级的解析结构,内嵌子查询时有效
  end;
  TABSQLParse = class
  private
    FSQLParseDef: PABSQLParseDef;
    FTreeView: TTreeView;
    FDefList: TStrings;
    function ParseToTree(aSql: string; aPABSQLParseDef: PABSQLParseDef; aTreeNode: TTreeNode = nil): string;
    procedure FreeDefList;
    procedure InitDef(var aDef: PABSQLParseDef; aParentDef: PABSQLParseDef = nil);
    function CheckSQL(aSql: string): string;
    function AddTreeChild(aParentTreeNode: TTreeNode; aStr: string): TTreeNode;
  public
    constructor Create(AOwner: TComponent);
    destructor Destroy; override;
    //复制aSQLParseDef1到aSQLParseDef2的解析结构
    procedure CopySQLParseDef(aSQLParseDef1, aSQLParseDef2: PABSQLParseDef);

    //增加查询条件
    //增加条件到SQL,返回结果SQL
    function AddWhere(aWhere: string; aSql: string): string; overload;
    //在内部SQL的基础上增加条件后返回
    function AddWhere(aWhere: string): string; overload;
    //在内部SQL的基础上增加条件后返回(不影响原有的SQL与内部结构)
    function AddWhereNoEditOldSQL(aWhere: string): string;


    //删除查询条件
    //在内部SQL的基础上删除所有条件后返回
    function DelWhere: string; overload;
    //从SQL删除所有条件,返回结果SQL
    function DelWhere(aWhere: string; aSql: string): string; overload;
    //在内部SQL的基础上删除条件后返回
    function DelWhere(aWhere: string): string; overload;
    //在内部SQL的基础上删除条件后返回(不影响原有的SQL与内部结构)
    function DelWhereNoEditOldSQL(aWhere: string): string;

    //增加排序
    //在SQL的基础上增加排序
    function AddOrder(aOrder: string; aSql: string): string; overload;
    //在内部SQL的基础上增加排序
    function AddOrder(aOrder: string): string; overload;
    //在内部SQL的基础上增加排序后返回(不影响原有的SQL与内部结构)
    function AddOrderNoEditOldSQL(aOrder: string): string;

    //删除排序
    //在内部SQL的基础上删除所有排序
    function DelOrder: string; overload;
    //在SQL的基础上删除排序
    function DelOrder(aOrder: string; aSql: string): string; overload;
    //在内部SQL的基础上删除排序
    function DelOrder(aOrder: string): string; overload;
    //在内部SQL的基础上删除排序后返回(不影响原有的SQL与内部结构)
    function DelOrderNoEditOldSQL(aOrder: string): string;

    //解释SQL到内部解析结构中
    function Parse(aSql: string): string;
    //组合内部解析结构到SQL
    function MakeSQL: string;

    //当前解析的SQL结构
    property SQLParseDef: PABSQLParseDef read FSQLParseDef write FSQLParseDef;
    //用于显示当前解释结构的树
    property TreeView: TTreeView read FTreeView write FTreeView;
  end;


var
  ABPubSQLParse: TABSQLParse;          //SQL解释对象


implementation

// TABSQLParse

constructor TABSQLParse.Create(AOwner: TComponent);
begin
  inherited Create;
  FDefList := TStringList.Create;

end;

destructor TABSQLParse.Destroy;
begin
  FreeDefList;
  FDefList.Free;
  inherited;
end;

procedure TABSQLParse.FreeDefList;
var
  i: LongInt;
begin
  for I := FDefList.Count - 1 downto 0 do
  begin
    Dispose(PABSQLParseDef(FDefList.Objects[i]));
    FDefList.Delete(i);
  end;
end;

function TABSQLParse.ParseToTree(aSql: string; aPABSQLParseDef: PABSQLParseDef;
  aTreeNode: TTreeNode): string;
  procedure SetSQLParseDef(aSQL, aCurWord: string;
    aIndex: LongInt;
    var aLeftBracketCount, aRightBracketCount: LongInt;
    var aWordEndFlag: Boolean
    );
  begin
    if (aPABSQLParseDef.SelectPos <= 0) and (AnsiCompareText(aCurWord,
      ' SELECT ') = 0) then
    begin //得出 SELECT 的位置
      aPABSQLParseDef.BeforeSelectStr := copy(aSql, 1, aIndex - length(' SELECT '));

      aPABSQLParseDef.SelectPos := aIndex;
      aWordEndFlag := true;
    end
    else if (aPABSQLParseDef.FromPos <= 0) and
      (aLeftBracketCount = aRightBracketCount) and (AnsiCompareText(aCurWord,
      ' FROM ') = 0) then
    begin //得出 FROM 的位置 与字段列表
      aPABSQLParseDef.FromPos := aIndex;
      if (aPABSQLParseDef.SelectPos > 0) and (aPABSQLParseDef.SelectEndPos <= 0) then
      begin
        aPABSQLParseDef.SelectEndPos := aPABSQLParseDef.FromPos -
          length(aCurWord);
        aPABSQLParseDef.SelectStr := copy(aSql, aPABSQLParseDef.SelectPos,
          aPABSQLParseDef.SelectEndPos - aPABSQLParseDef.SelectPos + 1);
      end;
      aWordEndFlag := true;
    end
    else if (aPABSQLParseDef.WherePos <= 0) and
      (aLeftBracketCount = aRightBracketCount) and (AnsiCompareText(aCurWord,
      ' WHERE ') = 0) then
    begin //得出 WHERE 的位置
      aPABSQLParseDef.WherePos := aIndex;
      if (aPABSQLParseDef.SelectPos > 0) and (aPABSQLParseDef.SelectEndPos <= 0) then
      begin
        aPABSQLParseDef.SelectEndPos := aPABSQLParseDef.WherePos -
          length(aCurWord);
        aPABSQLParseDef.SelectStr := copy(aSql, aPABSQLParseDef.SelectPos,
          aPABSQLParseDef.SelectEndPos - aPABSQLParseDef.SelectPos + 1);
      end
      else if (aPABSQLParseDef.FromPos > 0) and (aPABSQLParseDef.FromEndPos <= 0) then
      begin
        aPABSQLParseDef.FromEndPos := aPABSQLParseDef.WherePos -
          length(aCurWord);
        aPABSQLParseDef.FromStr := copy(aSql, aPABSQLParseDef.FromPos,
          aPABSQLParseDef.FromEndPos - aPABSQLParseDef.FromPos + 1);
      end;
      aWordEndFlag := true;
    end
    else if (aPABSQLParseDef.GroupPos <= 0) and
      (aLeftBracketCount = aRightBracketCount) and
      (AnsiCompareText(copy(aCurWord, 1, 7), ' GROUP ') = 0) and
      (AnsiCompareText(TrimLeft(copy(aCurWord, 7, Length(aCurWord))), 'BY ') = 0) then
    begin //得出 GROUP BY 的位置
      aPABSQLParseDef.GroupPos := aIndex;
      if (aPABSQLParseDef.SelectPos > 0) and (aPABSQLParseDef.SelectEndPos <= 0) then
      begin
        aPABSQLParseDef.SelectEndPos := aPABSQLParseDef.GroupPos -
          length(aCurWord);
        aPABSQLParseDef.SelectStr := copy(aSql, aPABSQLParseDef.SelectPos,
          aPABSQLParseDef.SelectEndPos - aPABSQLParseDef.SelectPos + 1);
      end
      else if (aPABSQLParseDef.FromPos > 0) and (aPABSQLParseDef.FromEndPos <= 0) then
      begin
        aPABSQLParseDef.FromEndPos := aPABSQLParseDef.GroupPos -
          length(aCurWord);
        aPABSQLParseDef.FromStr := copy(aSql, aPABSQLParseDef.FromPos,
          aPABSQLParseDef.FromEndPos - aPABSQLParseDef.FromPos + 1);
      end
      else if (aPABSQLParseDef.WherePos > 0) and (aPABSQLParseDef.WhereEndPos <=
        0) then
      begin
        aPABSQLParseDef.WhereEndPos := aPABSQLParseDef.GroupPos -
          length(aCurWord);
        aPABSQLParseDef.WhereStr := copy(aSql, aPABSQLParseDef.WherePos,
          aPABSQLParseDef.WhereEndPos - aPABSQLParseDef.WherePos + 1);
      end;
      aWordEndFlag := true;
    end
    else if (aPABSQLParseDef.HavingPos <= 0) and
      (aLeftBracketCount = aRightBracketCount) and (AnsiCompareText(aCurWord,
      ' HAVING ') = 0) then
    begin //得出 HAVING 的位置
      aPABSQLParseDef.HavingPos := aIndex;
      if (aPABSQLParseDef.SelectPos > 0) and (aPABSQLParseDef.SelectEndPos <= 0) then
      begin
        aPABSQLParseDef.SelectEndPos := aPABSQLParseDef.HavingPos -
          length(aCurWord);
        aPABSQLParseDef.SelectStr := copy(aSql, aPABSQLParseDef.SelectPos,
          aPABSQLParseDef.SelectEndPos - aPABSQLParseDef.SelectPos + 1);
      end
      else if (aPABSQLParseDef.FromPos > 0) and (aPABSQLParseDef.FromEndPos <= 0) then
      begin
        aPABSQLParseDef.FromEndPos := aPABSQLParseDef.HavingPos -
          length(aCurWord);
        aPABSQLParseDef.FromStr := copy(aSql, aPABSQLParseDef.FromPos,
          aPABSQLParseDef.FromEndPos - aPABSQLParseDef.FromPos + 1);
      end
      else if (aPABSQLParseDef.WherePos > 0) and (aPABSQLParseDef.WhereEndPos <=
        0) then
      begin
        aPABSQLParseDef.WhereEndPos := aPABSQLParseDef.HavingPos -
          length(aCurWord);
        aPABSQLParseDef.WhereStr := copy(aSql, aPABSQLParseDef.WherePos,
          aPABSQLParseDef.WhereEndPos - aPABSQLParseDef.WherePos + 1);
      end
      else if (aPABSQLParseDef.GroupPos > 0) and (aPABSQLParseDef.GroupEndPos <=
        0) then
      begin
        aPABSQLParseDef.GroupEndPos := aPABSQLParseDef.HavingPos -
          length(aCurWord);
        aPABSQLParseDef.GroupStr := copy(aSql, aPABSQLParseDef.GroupPos,
          aPABSQLParseDef.GroupEndPos - aPABSQLParseDef.GroupPos + 1);
      end;
      aWordEndFlag := true;
    end
    else if (aPABSQLParseDef.OrderPos <= 0) and
      (aLeftBracketCount = aRightBracketCount) and
      (AnsiCompareText(copy(aCurWord, 1, 7), ' ORDER ') = 0) and
      (AnsiCompareText(TrimLeft(copy(aCurWord, 7, Length(aCurWord))), 'BY ') = 0) then
    begin //得出 ORDER BY 的位置
      aPABSQLParseDef.OrderPos := aIndex;
      if (aPABSQLParseDef.SelectPos > 0) and (aPABSQLParseDef.SelectEndPos <= 0) then
      begin
        aPABSQLParseDef.SelectEndPos := aPABSQLParseDef.OrderPos -
          length(aCurWord);
        aPABSQLParseDef.SelectStr := copy(aSql, aPABSQLParseDef.SelectPos,
          aPABSQLParseDef.SelectEndPos - aPABSQLParseDef.SelectPos + 1);
      end
      else if (aPABSQLParseDef.FromPos > 0) and (aPABSQLParseDef.FromEndPos <= 0) then
      begin
        aPABSQLParseDef.FromEndPos := aPABSQLParseDef.OrderPos -
          length(aCurWord);
        aPABSQLParseDef.FromStr := copy(aSql, aPABSQLParseDef.FromPos,
          aPABSQLParseDef.FromEndPos - aPABSQLParseDef.FromPos + 1);
      end
      else if (aPABSQLParseDef.WherePos > 0) and (aPABSQLParseDef.WhereEndPos <=
        0) then
      begin
        aPABSQLParseDef.WhereEndPos := aPABSQLParseDef.OrderPos -
          length(aCurWord);
        aPABSQLParseDef.WhereStr := copy(aSql, aPABSQLParseDef.WherePos,
          aPABSQLParseDef.WhereEndPos - aPABSQLParseDef.WherePos + 1);
      end
      else if (aPABSQLParseDef.GroupPos > 0) and (aPABSQLParseDef.GroupEndPos <=
        0) then
      begin
        aPABSQLParseDef.GroupEndPos := aPABSQLParseDef.OrderPos -
          length(aCurWord);
        aPABSQLParseDef.GroupStr := copy(aSql, aPABSQLParseDef.GroupPos,
          aPABSQLParseDef.GroupEndPos - aPABSQLParseDef.GroupPos + 1);
      end
      else if (aPABSQLParseDef.HavingPos > 0) and (aPABSQLParseDef.HavingEndPos
        <= 0) then
      begin
        aPABSQLParseDef.HavingEndPos := aPABSQLParseDef.OrderPos -
          length(aCurWord);
        aPABSQLParseDef.HavingStr := copy(aSql, aPABSQLParseDef.HavingPos,
          aPABSQLParseDef.HavingEndPos - aPABSQLParseDef.HavingPos + 1);
      end;
      aWordEndFlag := true;
    end
    else if (aPABSQLParseDef.ComputePos <= 0) and
      (aLeftBracketCount = aRightBracketCount) and
      (AnsiCompareText(copy(aCurWord, 1, 9), ' COMPUTE ') = 0) and
      (AnsiCompareText(TrimLeft(copy(aCurWord, 9, Length(aCurWord))), 'BY ') = 0) then
    begin //得出 COMPUTE BY 的位置
      aPABSQLParseDef.ComputePos := aIndex;
      if (aPABSQLParseDef.SelectPos > 0) and (aPABSQLParseDef.SelectEndPos <= 0) then
      begin
        aPABSQLParseDef.SelectEndPos := aPABSQLParseDef.ComputePos -
          length(aCurWord);
        aPABSQLParseDef.SelectStr := copy(aSql, aPABSQLParseDef.SelectPos,
          aPABSQLParseDef.SelectEndPos - aPABSQLParseDef.SelectPos + 1);
      end
      else if (aPABSQLParseDef.FromPos > 0) and (aPABSQLParseDef.FromEndPos <= 0) then
      begin
        aPABSQLParseDef.FromEndPos := aPABSQLParseDef.ComputePos -
          length(aCurWord);
        aPABSQLParseDef.FromStr := copy(aSql, aPABSQLParseDef.FromPos,
          aPABSQLParseDef.FromEndPos - aPABSQLParseDef.FromPos + 1);
      end
      else if (aPABSQLParseDef.WherePos > 0) and (aPABSQLParseDef.WhereEndPos <=
        0) then
      begin
        aPABSQLParseDef.WhereEndPos := aPABSQLParseDef.ComputePos -
          length(aCurWord);
        aPABSQLParseDef.WhereStr := copy(aSql, aPABSQLParseDef.WherePos,
          aPABSQLParseDef.WhereEndPos - aPABSQLParseDef.WherePos + 1);
      end
      else if (aPABSQLParseDef.GroupPos > 0) and (aPABSQLParseDef.GroupEndPos <=
        0) then
      begin
        aPABSQLParseDef.GroupEndPos := aPABSQLParseDef.ComputePos -
          length(aCurWord);
        aPABSQLParseDef.GroupStr := copy(aSql, aPABSQLParseDef.GroupPos,
          aPABSQLParseDef.GroupEndPos - aPABSQLParseDef.GroupPos + 1);
      end
      else if (aPABSQLParseDef.HavingPos > 0) and (aPABSQLParseDef.HavingEndPos
        <= 0) then
      begin
        aPABSQLParseDef.HavingEndPos := aPABSQLParseDef.ComputePos -
          length(aCurWord);
        aPABSQLParseDef.HavingStr := copy(aSql, aPABSQLParseDef.HavingPos,
          aPABSQLParseDef.HavingEndPos - aPABSQLParseDef.HavingPos + 1);
      end
      else if (aPABSQLParseDef.OrderPos > 0) and (aPABSQLParseDef.OrderEndPos <=
        0) then
      begin
        aPABSQLParseDef.OrderEndPos := aPABSQLParseDef.ComputePos -
          length(aCurWord);
        aPABSQLParseDef.OrderStr := copy(aSql, aPABSQLParseDef.OrderPos,
          aPABSQLParseDef.OrderEndPos - aPABSQLParseDef.OrderPos + 1);
      end;
      aWordEndFlag := true;
    end
    else if (aPABSQLParseDef.UnionPos <= 0) and
      (aLeftBracketCount = aRightBracketCount) and (AnsiCompareText(aCurWord,
      ' UNION ') = 0) then
    begin //得出 UNION 的位置
      aPABSQLParseDef.UnionPos := aIndex;
      if (aPABSQLParseDef.SelectPos > 0) and (aPABSQLParseDef.SelectEndPos <= 0) then
      begin
        aPABSQLParseDef.SelectEndPos := aPABSQLParseDef.UnionPos -
          length(aCurWord);
        aPABSQLParseDef.SelectStr := copy(aSql, aPABSQLParseDef.SelectPos,
          aPABSQLParseDef.SelectEndPos - aPABSQLParseDef.SelectPos + 1);
      end
      else if (aPABSQLParseDef.FromPos > 0) and (aPABSQLParseDef.FromEndPos <= 0) then
      begin
        aPABSQLParseDef.FromEndPos := aPABSQLParseDef.UnionPos -
          length(aCurWord);
        aPABSQLParseDef.FromStr := copy(aSql, aPABSQLParseDef.FromPos,
          aPABSQLParseDef.FromEndPos - aPABSQLParseDef.FromPos + 1);
      end
      else if (aPABSQLParseDef.WherePos > 0) and (aPABSQLParseDef.WhereEndPos <=
        0) then
      begin
        aPABSQLParseDef.WhereEndPos := aPABSQLParseDef.UnionPos -
          length(aCurWord);
        aPABSQLParseDef.WhereStr := copy(aSql, aPABSQLParseDef.WherePos,
          aPABSQLParseDef.WhereEndPos - aPABSQLParseDef.WherePos + 1);
      end
      else if (aPABSQLParseDef.GroupPos > 0) and (aPABSQLParseDef.GroupEndPos <=
        0) then
      begin
        aPABSQLParseDef.GroupEndPos := aPABSQLParseDef.UnionPos -
          length(aCurWord);
        aPABSQLParseDef.GroupStr := copy(aSql, aPABSQLParseDef.GroupPos,
          aPABSQLParseDef.GroupEndPos - aPABSQLParseDef.GroupPos + 1);
      end
      else if (aPABSQLParseDef.HavingPos > 0) and (aPABSQLParseDef.HavingEndPos
        <= 0) then
      begin
        aPABSQLParseDef.HavingEndPos := aPABSQLParseDef.UnionPos -
          length(aCurWord);
        aPABSQLParseDef.HavingStr := copy(aSql, aPABSQLParseDef.HavingPos,
          aPABSQLParseDef.HavingEndPos - aPABSQLParseDef.HavingPos + 1);
      end
      else if (aPABSQLParseDef.OrderPos > 0) and (aPABSQLParseDef.OrderEndPos <=
        0) then
      begin
        aPABSQLParseDef.OrderEndPos := aPABSQLParseDef.UnionPos -
          length(aCurWord);
        aPABSQLParseDef.OrderStr := copy(aSql, aPABSQLParseDef.OrderPos,
          aPABSQLParseDef.OrderEndPos - aPABSQLParseDef.OrderPos + 1);
      end
      else if (aPABSQLParseDef.ComputePos > 0) and (aPABSQLParseDef.ComputeEndPos
        <= 0) then
      begin
        aPABSQLParseDef.ComputeEndPos := aPABSQLParseDef.UnionPos -
          length(aCurWord);
        aPABSQLParseDef.ComputeStr := copy(aSql, aPABSQLParseDef.ComputePos,
          aPABSQLParseDef.ComputeEndPos - aPABSQLParseDef.ComputePos + 1);
      end;
      aWordEndFlag := true;
    end
    else if (aSql[aIndex] = '(') then
    begin //记录左块开始的数量
      aLeftBracketCount := aLeftBracketCount + 1;
    end //记录右块开始的数量
    else if (aSql[aIndex] = ')') then
    begin
      aRightBracketCount := aRightBracketCount + 1;
      if (aLeftBracketCount = aRightBracketCount) then
        aWordEndFlag := true;
    end;
  end;

  procedure CheckSQLParseDef(aSQL: string);
  var
    i: LongInt;
  begin
    i := length(aSql);
    if (aPABSQLParseDef.SelectPos > 0) and (aPABSQLParseDef.SelectEndPos = 0) then
      aPABSQLParseDef.SelectEndPos := i
    else if (aPABSQLParseDef.FromPos > 0) and (aPABSQLParseDef.FromEndPos = 0) then
      aPABSQLParseDef.FromEndPos := i
    else if (aPABSQLParseDef.WherePos > 0) and (aPABSQLParseDef.WhereEndPos = 0) then
      aPABSQLParseDef.WhereEndPos := i
    else if (aPABSQLParseDef.GroupPos > 0) and (aPABSQLParseDef.GroupEndPos = 0) then
      aPABSQLParseDef.GroupEndPos := i
    else if (aPABSQLParseDef.HavingPos > 0) and (aPABSQLParseDef.HavingEndPos =
      0) then
      aPABSQLParseDef.HavingEndPos := i
    else if (aPABSQLParseDef.OrderPos > 0) and (aPABSQLParseDef.OrderEndPos = 0) then
      aPABSQLParseDef.OrderEndPos := i
    else if (aPABSQLParseDef.ComputePos > 0) and (aPABSQLParseDef.ComputeEndPos
      = 0) then
      aPABSQLParseDef.ComputeEndPos := i;

    if aPABSQLParseDef.ComputePos > 0 then
      aPABSQLParseDef.ComputeStr := copy(aSql, aPABSQLParseDef.ComputePos,
        length(aSql))
    else if aPABSQLParseDef.OrderPos > 0 then
      aPABSQLParseDef.OrderStr := copy(aSql, aPABSQLParseDef.OrderPos,
        length(aSql))
    else if aPABSQLParseDef.HavingPos > 0 then
      aPABSQLParseDef.HavingStr := copy(aSql, aPABSQLParseDef.HavingPos,
        length(aSql))
    else if aPABSQLParseDef.GroupPos > 0 then
      aPABSQLParseDef.GroupStr := copy(aSql, aPABSQLParseDef.GroupPos,
        length(aSql))
    else if aPABSQLParseDef.WherePos > 0 then
      aPABSQLParseDef.WhereStr := copy(aSql, aPABSQLParseDef.WherePos,
        length(aSql))
    else if aPABSQLParseDef.FromPos > 0 then
      aPABSQLParseDef.FromStr := copy(aSql, aPABSQLParseDef.FromPos,
        length(aSql))
    else if aPABSQLParseDef.SelectPos > 0 then
      aPABSQLParseDef.SelectStr := copy(aSql, aPABSQLParseDef.SelectPos,
        length(aSql));
  end;
  //如果词以空格开头与结尾或以换行/回车/,结尾且不是几个固定的关键字一部分则自动结束词
  procedure AutoEndWordEndFlag(aSQL, aCurWord: string;
    aIndex: LongInt;
    var aWordEndFlag: Boolean
    );
  begin
    if (not aWordEndFlag) and
      (
      (
      (aCurWord <> ' ') and
      (copy(aCurWord, 1, 1) = ' ') and
      (copy(aCurWord, length(aCurWord), 1) = ' ')
      ) or
      (aSql[aIndex] = ABEnterWrapStr) or
      (aSql[aIndex] = ',')
      ) and
      (
      (AnsiCompareText(TrimRight(aCurWord), ' GROUP') <> 0) and
      (AnsiCompareText(TrimRight(aCurWord), ' ORDER') <> 0) and
      (AnsiCompareText(TrimRight(aCurWord), ' COMPUTE') <> 0)
      ) then
    begin
      aWordEndFlag := true;
    end;
  end;
  //将SQL分析的结果填充到aTreeView
  procedure FillTree;
    procedure FillFields(aParentDef: PABSQLParseDef; aFields: string; aTreeNode:
      TTreeNode);
    var
      tempCurWord, tempStr: string;
      i, J, K: LongInt;
      tempWordEndFlag: Boolean;
      tempLeftBracketCount: LongInt;
      tempRightBracketCount: LongInt;
      tempTreeNode: TTreeNode;
      tempSQLParseDef: PABSQLParseDef;
    begin
      tempCurWord := EmptyStr;
      tempLeftBracketCount := 0;
      tempRightBracketCount := 0;
      i := 1;

      //一个一个地读出SQL的字符进行分析
      while i <= length(aFields) do
      begin
        tempWordEndFlag := false;
        tempCurWord := tempCurWord + aFields[i];
        if (aFields[i] = ',') or (i = length(aFields)) then
        begin
          if (tempLeftBracketCount = tempRightBracketCount) then
          begin
            tempStr := tempCurWord;
            if aFields[i] = ',' then
              tempStr := trim(Copy(tempCurWord, 1, Length(tempCurWord) - 1));

            tempTreeNode := AddTreeChild(aTreeNode, trim(tempStr));

            j := ABPos('(', tempStr);
            k := ABPos(')', ReverseString(tempStr));
            if (j > 0) and (k > 0) then
              tempStr := copy(tempStr, j + 1, length(tempStr) - k + 1 - (j +
                1));

            tempStr := trim(tempStr);

            if ABIsSelectSQL(tempStr) then
            begin
              new(tempSQLParseDef);
              InitDef(tempSQLParseDef, aParentDef);
              FDefList.AddObject(tempStr, TObject(tempSQLParseDef));
              ParseToTree(tempStr, tempSQLParseDef, tempTreeNode);
            end;

            tempWordEndFlag := true;
          end;
        end //记录右块开始的数量
        else if (aFields[i] = '(') then
        begin //记录左块开始的数量
          tempLeftBracketCount := tempLeftBracketCount + 1;
        end //记录右块开始的数量
        else if (aSql[i] = ')') then
        begin
          tempRightBracketCount := tempRightBracketCount + 1;
        end;

        if tempWordEndFlag then
        begin
          tempCurWord := ' ';
        end;

        i := i + 1;
      end;
    end;
  var
    tempTreeNode: TTreeNode;
    tempCurWord: string;
    i: LongInt;
  begin
    if aPABSQLParseDef.SelectStr <> EmptyStr then
    begin
      tempTreeNode := AddTreeChild(aTreeNode, 'SELECT');
      FillFields(aPABSQLParseDef, aPABSQLParseDef.SelectStr, tempTreeNode);
    end;
    if aPABSQLParseDef.FromStr <> EmptyStr then
    begin
      tempTreeNode := AddTreeChild(aTreeNode, 'FROM');
      for i := 1 to ABGetSpaceStrCount(aPABSQLParseDef.FromStr, ',') do
      begin
        tempCurWord := ABGetSpaceStr(aPABSQLParseDef.FromStr, i, ',');
        AddTreeChild(tempTreeNode, tempCurWord);
      end;
    end;
    if aPABSQLParseDef.WhereStr <> EmptyStr then
    begin
      tempTreeNode := AddTreeChild(aTreeNode, 'WHERE');
      for i := 1 to ABGetSpaceStrCount(aPABSQLParseDef.WhereStr, ',') do
      begin
        tempCurWord := (ABGetSpaceStr(aPABSQLParseDef.WhereStr, i, ','));
        AddTreeChild(tempTreeNode, tempCurWord);
      end;
    end;
    if aPABSQLParseDef.GroupStr <> EmptyStr then
    begin
      tempTreeNode := AddTreeChild(aTreeNode, 'GROUP BY');
      for i := 1 to ABGetSpaceStrCount(aPABSQLParseDef.GroupStr, ',') do
      begin
        tempCurWord := (ABGetSpaceStr(aPABSQLParseDef.GroupStr, i, ','));
        AddTreeChild(tempTreeNode, tempCurWord);
      end;
    end;
    if aPABSQLParseDef.HavingStr <> EmptyStr then
    begin
      tempTreeNode := AddTreeChild(aTreeNode, 'HAVING');
      for i := 1 to ABGetSpaceStrCount(aPABSQLParseDef.HavingStr, ',') do
      begin
        tempCurWord := (ABGetSpaceStr(aPABSQLParseDef.HavingStr, i,
          ','));
        AddTreeChild(tempTreeNode, tempCurWord);
      end;
    end;
    if aPABSQLParseDef.OrderStr <> EmptyStr then
    begin
      tempTreeNode := AddTreeChild(aTreeNode, 'ORDER BY');
      for i := 1 to ABGetSpaceStrCount(aPABSQLParseDef.OrderStr, ',') do
      begin
        tempCurWord := (ABGetSpaceStr(aPABSQLParseDef.OrderStr, i, ','));
        AddTreeChild(tempTreeNode, tempCurWord);
      end;
    end;
    if aPABSQLParseDef.ComputeStr <> EmptyStr then
    begin
      tempTreeNode := AddTreeChild(aTreeNode, 'COMPUTE BY');
      for i := 1 to ABGetSpaceStrCount(aPABSQLParseDef.ComputeStr, ',') do
      begin
        tempCurWord := (ABGetSpaceStr(aPABSQLParseDef.ComputeStr, i,
          ','));
        AddTreeChild(tempTreeNode, tempCurWord);
      end;
    end;
    if aPABSQLParseDef.UnionStr <> EmptyStr then
    begin
      AddTreeChild(aTreeNode, 'UNION');
    end;
  end;
var
  tempCurWord: string;
  i, tempLeftBracketCount,
    tempRightBracketCount: longint;
  tempWordEndFlag: boolean;
begin
  aSql := CheckSQL(aSql);

  tempCurWord := EmptyStr;
  tempLeftBracketCount := 0;
  tempRightBracketCount := 0;
  i := 1;
  //一个一个地读出SQL的字符进行分析
  while i <= length(aSql) do
  begin
    tempWordEndFlag := false;
    tempCurWord := tempCurWord + aSql[i];

    //设置SQL语句各部的位置及字串
    SetSQLParseDef(aSql, tempCurWord, i,
      tempLeftBracketCount, tempRightBracketCount,
      tempWordEndFlag);

    //在一些条件下自动结束词
    AutoEndWordEndFlag(aSql, tempCurWord, i, tempWordEndFlag);

    //如果块已结束,则重新分析后继的词
    if tempWordEndFlag then
    begin
      tempCurWord := ' ';
    end;

    i := i + 1;
  end;

  //分析到最后 ,如缺少一些SELECT语句的部分时则自动调整SQL语句各部的位置及字串
  CheckSQLParseDef(aSql);

  //将SQL分析的结果填充到aTreeView
  FillTree;

  FSQLParseDef.SingleTableName := trim(ABStringReplace(FSQLParseDef.FromStr,[ABEnterWrapStr],''));
  FSQLParseDef.SingleTableName := ABStringReplace(FSQLParseDef.SingleTableName, '(', '');
  FSQLParseDef.SingleTableName := ABStringReplace(FSQLParseDef.SingleTableName, ')', '');
  FSQLParseDef.SingleTableName := ABStringReplace(FSQLParseDef.SingleTableName, '[', '');
  FSQLParseDef.SingleTableName := ABStringReplace(FSQLParseDef.SingleTableName, ']', '');

  FSQLParseDef.SingleTableName := trim(ABGetLeftRightStr(FSQLParseDef.SingleTableName));
  if (ABPos(' ', FSQLParseDef.SingleTableName) > 0) then
    FSQLParseDef.SingleTableName := EmptyStr;

  result := aSql;
end;

function TABSQLParse.CheckSQL(aSql: string): string;
begin
  result := aSql;
  result := ABStringReplace(result, '(', ' ( ');
  result := ABStringReplace(result, ')', ' ) ');
  result := ABStringReplace(result, ABEnterWrapStr, ' '+ABEnterWrapStr+' ');
  result := ' ' + trim(result) + ' ';
end;

procedure TABSQLParse.CopySQLParseDef(aSQLParseDef1, aSQLParseDef2: PABSQLParseDef);
begin
  if Assigned(FSQLParseDef) then
  begin
    aSQLParseDef2.sql := aSQLParseDef1.sql;

    aSQLParseDef2.BeforeSelectStr := aSQLParseDef1.BeforeSelectStr;
    aSQLParseDef2.SingleTableName := aSQLParseDef1.SingleTableName;

    aSQLParseDef2.SelectPos := aSQLParseDef1.SelectPos;
    aSQLParseDef2.FromPos := aSQLParseDef1.FromPos;
    aSQLParseDef2.WherePos := aSQLParseDef1.WherePos;
    aSQLParseDef2.GroupPos := aSQLParseDef1.GroupPos;
    aSQLParseDef2.HavingPos := aSQLParseDef1.HavingPos;
    aSQLParseDef2.OrderPos := aSQLParseDef1.OrderPos;
    aSQLParseDef2.ComputePos := aSQLParseDef1.ComputePos;
    aSQLParseDef2.UnionPos := aSQLParseDef1.UnionPos;

    aSQLParseDef2.SelectEndPos := aSQLParseDef1.SelectEndPos;
    aSQLParseDef2.FromEndPos := aSQLParseDef1.FromEndPos;
    aSQLParseDef2.WhereEndPos := aSQLParseDef1.WhereEndPos;
    aSQLParseDef2.GroupEndPos := aSQLParseDef1.GroupEndPos;
    aSQLParseDef2.HavingEndPos := aSQLParseDef1.HavingEndPos;
    aSQLParseDef2.OrderEndPos := aSQLParseDef1.OrderEndPos;
    aSQLParseDef2.ComputeEndPos := aSQLParseDef1.ComputeEndPos;
    aSQLParseDef2.UnionEndPos := aSQLParseDef1.UnionEndPos;

    aSQLParseDef2.SelectStr := aSQLParseDef1.SelectStr;
    aSQLParseDef2.FromStr := aSQLParseDef1.FromStr;
    aSQLParseDef2.WhereStr := aSQLParseDef1.WhereStr;
    aSQLParseDef2.GroupStr := aSQLParseDef1.GroupStr;
    aSQLParseDef2.HavingStr := aSQLParseDef1.HavingStr;
    aSQLParseDef2.OrderStr := aSQLParseDef1.OrderStr;
    aSQLParseDef2.ComputeStr := aSQLParseDef1.ComputeStr;
    aSQLParseDef2.UnionStr := aSQLParseDef1.UnionStr;

    aSQLParseDef2.ParentDef := aSQLParseDef1.ParentDef;
  end;
end;

function TABSQLParse.Parse(aSql: string): string;
var
  tempSQLParseDef: PABSQLParseDef;
begin
  FreeDefList;

  if Assigned(FTreeView) then
    FTreeView.Items.Clear;

  new(tempSQLParseDef);
  InitDef(tempSQLParseDef);
  FDefList.AddObject('', TObject(tempSQLParseDef));
  FSQLParseDef := tempSQLParseDef;

  FSQLParseDef.SQL := ParseToTree(aSql, FSQLParseDef, nil);
  result := FSQLParseDef.SQL;

  if Assigned(FTreeView) then
    FTreeView.FullExpand;
end;

procedure TABSQLParse.InitDef(var aDef: PABSQLParseDef; aParentDef:
  PABSQLParseDef);
begin

  aDef.SQL := EmptyStr;
  aDef.BeforeSelectStr := EmptyStr;
  aDef.SingleTableName := EmptyStr;

  aDef.SelectPos := 0;
  aDef.FromPos := 0;
  aDef.WherePos := 0;
  aDef.GroupPos := 0;
  aDef.HavingPos := 0;
  aDef.OrderPos := 0;
  aDef.ComputePos := 0;
  aDef.UnionPos := 0;

  aDef.SelectEndPos := 0;
  aDef.FromEndPos := 0;
  aDef.WhereEndPos := 0;
  aDef.GroupEndPos := 0;
  aDef.HavingEndPos := 0;
  aDef.OrderEndPos := 0;
  aDef.ComputeEndPos := 0;
  aDef.UnionEndPos := 0;

  aDef.SelectStr := EmptyStr;
  aDef.FromStr := EmptyStr;
  aDef.WhereStr := EmptyStr;
  aDef.GroupStr := EmptyStr;
  aDef.HavingStr := EmptyStr;
  aDef.OrderStr := EmptyStr;
  aDef.ComputeStr := EmptyStr;
  aDef.UnionStr := EmptyStr;

  aDef.ParentDef := aParentDef;
end;

function TABSQLParse.AddTreeChild(aParentTreeNode: TTreeNode; aStr: string):
  TTreeNode;
begin
  Result := nil;
  if Assigned(FTreeView) then
    Result := FTreeView.Items.AddChild(aParentTreeNode, aStr);
end;

function TABSQLParse.AddOrder(aOrder: string; aSql: string): string;
begin
  result := aSql;

  if aSql <> EmptyStr then
    Parse(aSql);

  result := AddOrder(aOrder);
end;

function TABSQLParse.AddOrder(aOrder: string): string;
begin
  result := EmptyStr;
  aOrder:=Trim(aOrder);
  if (AnsiCompareText(Copy(aOrder, 1, 5), 'Order') = 0) then
    aOrder:=Copy(aOrder, 6, maxint);

  if Assigned(FSQLParseDef) then
  begin
    if FSQLParseDef.OrderStr = EmptyStr then
      FSQLParseDef.OrderStr := aOrder
    else
    begin
      FSQLParseDef.OrderStr := Trim(FSQLParseDef.OrderStr);
      if ABRightStr(FSQLParseDef.OrderStr, 1) = ',' then
        FSQLParseDef.OrderStr := FSQLParseDef.OrderStr + aOrder
      else
        FSQLParseDef.OrderStr := FSQLParseDef.OrderStr + ',' + aOrder;
    end;

    FSQLParseDef.SQL:=MakeSQL;
    result := FSQLParseDef.SQL;
  end
  else
    ABShow('未初始化SQL分析,请检查.');
end;

function TABSQLParse.AddOrderNoEditOldSQL(aOrder: string): string;
var
  tempOldSQL,
  tempOldWhere:string;
begin
  tempOldSQL:=FSQLParseDef.SQL;
  tempOldWhere:=FSQLParseDef.WhereStr;
  try
    result := AddOrder(aOrder);
  finally
    FSQLParseDef.SQL:=tempOldSQL;
    FSQLParseDef.WhereStr:=tempOldWhere;
  end;
end;

function TABSQLParse.DelOrder(aOrder: string; aSql: string): string;
begin
  result := aSql;

  if aSql <> EmptyStr then
    Parse(aSql);

  result := DelOrder(aOrder);
end;

function TABSQLParse.DelOrder(aOrder: string): string;
begin
  result := EmptyStr;

  if Assigned(FSQLParseDef) then
  begin
    if FSQLParseDef.OrderStr <> EmptyStr then
    begin
      FSQLParseDef.OrderStr :=
        StringReplace(FSQLParseDef.OrderStr, aOrder, '', [rfIgnoreCase]);
    end;
    if ABStrsInArray(FSQLParseDef.OrderStr,',', [' ', ',']) then
      FSQLParseDef.OrderStr := '';

    FSQLParseDef.SQL:=MakeSQL;
    result := FSQLParseDef.SQL;
  end
  else
    ABShow('未初始化SQL分析,请检查.');
end;

function TABSQLParse.DelOrderNoEditOldSQL(aOrder: string): string;
var
  tempOldSQL,
  tempOldWhere:string;
begin
  tempOldSQL:=FSQLParseDef.SQL;
  tempOldWhere:=FSQLParseDef.WhereStr;
  try
    result := DelOrder(aOrder);
  finally
    FSQLParseDef.SQL:=tempOldSQL;
    FSQLParseDef.WhereStr:=tempOldWhere;
  end;
end;

function TABSQLParse.DelOrder: string;
begin
  result := EmptyStr;

  if Assigned(FSQLParseDef) then
  begin
    if FSQLParseDef.OrderStr <> EmptyStr then
    begin
      FSQLParseDef.OrderStr := '';
    end;

    FSQLParseDef.SQL:=MakeSQL;
    result := FSQLParseDef.SQL;
  end
  else
    ABShow('未初始化SQL分析,请检查.');
end;

function TABSQLParse.AddWhere(aWhere: string; aSql: string): string;
begin
  result := aSql;

  if aSql <> EmptyStr then
    Parse(aSql);

  result := AddWhere(aWhere);
end;

function TABSQLParse.AddWhere(aWhere: string): string;
begin
  result := EmptyStr;
  aWhere := Trim(aWhere);
  if (AnsiCompareText(Copy(aWhere, 1, 5), 'Where') = 0) then
    aWhere:=Copy(aWhere, 6, maxint);

  if (Assigned(FSQLParseDef)) then
  begin
    if aWhere<>EmptyStr then
    begin
      if FSQLParseDef.SelectStr <> EmptyStr then
      begin
        if FSQLParseDef.WhereStr = EmptyStr then
        begin
          if (AnsiCompareText(Copy(aWhere, 1, 3), 'AND') = 0) then
            FSQLParseDef.WhereStr := copy(aWhere, 4, length(aWhere))
          else if (AnsiCompareText(Copy(aWhere, 1, 3), 'Or') = 0) then
            FSQLParseDef.WhereStr := copy(aWhere, 3, length(aWhere))
          else
            FSQLParseDef.WhereStr := aWhere;
        end
        else
        begin
          if (AnsiCompareText(Copy(aWhere, 1, 3), 'AND') = 0) or
             (AnsiCompareText(Copy(aWhere, 1, 2), 'Or ') = 0) then
            FSQLParseDef.WhereStr := '(' + FSQLParseDef.WhereStr + ') ' + aWhere
          else
            FSQLParseDef.WhereStr := '(' + FSQLParseDef.WhereStr + ') and (' +aWhere+')';
        end;
      end
      else
      begin
        FSQLParseDef.SQL:=FSQLParseDef.SQL+aWhere;
      end;
      FSQLParseDef.SQL:=MakeSQL;
      result := FSQLParseDef.SQL;
    end;
  end
  else
    ABShow('未初始化SQL分析,请检查.');
end;

function TABSQLParse.AddWhereNoEditOldSQL(aWhere: string): string;
var
  tempOldSQL,
  tempOldWhere:string;
begin
  tempOldSQL:=FSQLParseDef.SQL;
  tempOldWhere:=FSQLParseDef.WhereStr;
  try
    result := AddWhere(aWhere);
  finally
    FSQLParseDef.SQL:=tempOldSQL;
    FSQLParseDef.WhereStr:=tempOldWhere;
  end;
end;

function TABSQLParse.DelWhere(aWhere: string; aSql: string): string;
begin
  result := aSql;

  if aSql <> EmptyStr then
    Parse(aSql);

  result := DelWhere(aWhere);
end;

function TABSQLParse.DelWhere(aWhere: string): string;
begin
  result := EmptyStr;

  if Assigned(FSQLParseDef) then
  begin
    if FSQLParseDef.WhereStr <> EmptyStr then
    begin
      FSQLParseDef.WhereStr :=
        StringReplace(FSQLParseDef.WhereStr, aWhere, '', [rfIgnoreCase]);
    end;
    if ABStrsInArray(FSQLParseDef.WhereStr,',', [' ', '(', ')']) then
      FSQLParseDef.WhereStr := '';

    FSQLParseDef.SQL:=MakeSQL;
    result := FSQLParseDef.SQL;
  end
  else
    ABShow('未初始化SQL分析,请检查.');
end;

function TABSQLParse.DelWhereNoEditOldSQL(aWhere: string): string;
var
  tempOldSQL,
  tempOldWhere:string;
begin
  tempOldSQL:=FSQLParseDef.SQL;
  tempOldWhere:=FSQLParseDef.WhereStr;
  try
    result := DelWhere(aWhere);
  finally
    FSQLParseDef.SQL:=tempOldSQL;
    FSQLParseDef.WhereStr:=tempOldWhere;
  end;
end;

function TABSQLParse.DelWhere: string;
begin
  result := EmptyStr;

  if Assigned(FSQLParseDef) then
  begin
    if FSQLParseDef.WhereStr <> EmptyStr then
    begin
      FSQLParseDef.WhereStr := '';
    end;

    FSQLParseDef.SQL:=MakeSQL;
    result := FSQLParseDef.SQL;
  end
  else
    ABShow('未初始化SQL分析,请检查.');
end;

function TABSQLParse.MakeSQL: string;
begin
  result := EmptyStr;
  if FSQLParseDef.SelectStr<> EmptyStr then
  begin
    if FSQLParseDef.BeforeSelectStr <> EmptyStr then
      result := FSQLParseDef.BeforeSelectStr + ABEnterWrapStr;

    if FSQLParseDef.SelectStr <> EmptyStr then
      result := result + ' select ' + FSQLParseDef.SelectStr + ABEnterWrapStr;

    if FSQLParseDef.FromStr <> EmptyStr then
      result := result + ' from ' + FSQLParseDef.FromStr + ABEnterWrapStr;

    if FSQLParseDef.WhereStr <> EmptyStr then
      result := result + ' where ' + FSQLParseDef.WhereStr + ABEnterWrapStr;

    if FSQLParseDef.GroupStr <> EmptyStr then
      result := result + ' Group by ' + FSQLParseDef.GroupStr + ABEnterWrapStr;

    if FSQLParseDef.HavingStr <> EmptyStr then
      result := result + ' Having ' + FSQLParseDef.HavingStr + ABEnterWrapStr;

    if FSQLParseDef.OrderStr <> EmptyStr then
      result := result + ' Order by ' + FSQLParseDef.OrderStr + ABEnterWrapStr;

    if FSQLParseDef.ComputeStr <> EmptyStr then
      result := result + ' Compute by ' + FSQLParseDef.ComputeStr +
        ABEnterWrapStr;

    if FSQLParseDef.UnionStr <> EmptyStr then
      result := result + ' Union ' + ABEnterWrapStr + FSQLParseDef.UnionStr +
        ABEnterWrapStr;
  end
  else
  begin
    result:=FSQLParseDef.SQL;
  end;
end;



procedure ABInitialization;
begin
  ABPubSQLParse := TABSQLParse.Create(nil);
end;

procedure ABFinalization;
begin
  ABPubSQLParse.Free;
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;

end.
