{
信息框输入单元
}
unit ABPubInPutStrU;

interface
{$I ..\ABInclude.ini}

uses
  SysUtils,Classes,Controls,Forms,ExtCtrls,StdCtrls;

type
  TABInPutStrForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Label_Remark: TLabel;
    Label_Input: TLabel;
    Button1: TButton;
    Button2: TButton;
    Text_Input: TEdit;
    procedure Text_InputKeyPress(Sender: TObject; var Key: Char);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//显示输入字串框
//aLabelCaption=输入框前的标签
//aText=返回输入的字串
//aRemark=输入框上方的描述
//ACaption=输入框的标题
function ABInPutStr(aLabelCaption:string;
                    var aText: string;
                    aRemark:string='';
                    ACaption: string=''): boolean;

implementation                                                          

{$R *.dfm}
function ABInPutStr(aLabelCaption:string;var aText: string;aRemark:string;ACaption: string): boolean;
var
  tempForm:TABInPutStrForm;
begin
  result:=False;
  tempForm := TABInPutStrForm.Create(nil);
  try
    if ACaption<>emptystr then
      tempForm.Caption:=ACaption;
    tempForm.Label_Remark.Caption:=aRemark;

    tempForm.Text_Input.Text:=aText;
    tempForm.Label_Input.Caption:=aLabelCaption;
    if aRemark=EmptyStr then
    begin
      tempForm.Height:=tempForm.Height-tempForm.Panel1.Height;
      tempForm.Panel1.Height:=0;
    end;

    if tempForm.ShowModal=mrok then
    begin
      aText:= tempForm.Text_Input.Text;
      result:=true;
    end;
  finally
    tempForm.Free;
  end;
end;

procedure TABInPutStrForm.Button1Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TABInPutStrForm.Button2Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TABInPutStrForm.FormShow(Sender: TObject);
begin
  if Text_Input.CanFocus then
    Text_Input.SetFocus;
end;

procedure TABInPutStrForm.Text_InputKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (key= #13)  then
    Button1Click(nil);
end;


end.
