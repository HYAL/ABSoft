{
第三方加密狗单元
}
unit ABPubDogKeyU;

interface
{$I ..\ABInclude.ini}

uses
  SysUtils,Windows;
  const   
      Delta:   longword   =   $9e3779b9;
  type
    DWORD = Longword;

  function   StrEnc( InString ,  Key:ansistring):ansistring;
  function   StrDec( InString ,  Key:ansistring):ansistring;
  procedure   EnCode(indata:pByte;   outdata:pByte;InKey:ansistring);
  procedure   DeCode(indata:pByte; outdata:pByte;InKey:ansistring);
  function CheckKeyByFindort_2() :integer;
  function CheckKeyByReadEprom() :integer;
  function CheckKeyByEncstring() :integer;

  function IntToHex( indata:dword):ansistring ;
  function  HexToInt( s:ansistring):dword ;
  function lstrcpyA(lpString1,lpString2: PChar): PChar;
  stdcall; external 'Kernel32.dll' name 'lstrcpyA'

  function SetupDiGetClassDevsA( ClassGuid:dword; Enumerator:PANSIChar;hwndParent,Flag:DWORD):dword;
  stdcall;  external 'Setupapi.dll' name 'SetupDiGetClassDevsA'
  function  SetupDiEnumDeviceInterfaces( DeviceInfoSet, DeviceInfoData,InterfaceClassGuid,
  MemberIndex:dword; DeviceInterfaceData :dword):BOOLean; stdcall;  external 'Setupapi.dll' name 'SetupDiEnumDeviceInterfaces'
  function SetupDiGetDeviceInterfaceDetailA(DeviceInfoSet,DeviceInterfaceData:dword;
   DeviceInterfaceDetailData:dword;DeviceInterfaceDetailDataSize:dword;var RequiredSize:dword; DeviceInfoData  :dword):boolean;
  stdcall;   external 'Setupapi.dll' name 'SetupDiGetDeviceInterfaceDetailA'
  function SetupDiDestroyDeviceInfoList( DeviceInfoSet:dword):boolean;stdcall; external 'Setupapi.dll' name 'SetupDiDestroyDeviceInfoList'

  procedure HidD_GetHidGuid(  HidGuid:dword ) stdcall; external 'hid.dll' name 'HidD_GetHidGuid'
  function HidD_SetFeature(  HidDeviceObject,ReportBuffer,ReportBufferLength :dword):BOOLEAN;
   stdcall; external 'hid.dll' name 'HidD_SetFeature'
  function  HidD_GetFeature(HidDeviceObject:dword; ReportBuffer:dword;ReportBufferLength :dword):BOOLEAN;
    stdcall; external 'hid.dll' name 'HidD_GetFeature'
  function HidP_GetUsages( ReportType,UsagePage:dword; LinkCollection:integer; UsageList:dword;
    var UsageLength:dword;  PreparsedData:dword;  Report:PANSIChar; ReportLength:word):dword;
      stdcall; external 'hid.dll' name 'HidP_GetUsages'
  function  HidP_GetUsageValue( ReportType,UsagePage:dword; LinkCollection:integer;Usage:dword;
     UsageValue:dword; PreparsedData:dword; Report:PANSIChar;ReportLength :dword):dword;
   stdcall; external 'hid.dll' name 'HidP_GetUsageValue'
 function HidP_GetScaledUsageValue( ReportType,UsagePage:dword; LinkCollection:integer;Usage:dword;
     UsageValue:dword; PreparsedData:dword; Report:PANSIChar;ReportLength :dword ):dword;
  stdcall; external 'hid.dll' name 'HidP_GetScaledUsageValue'
  function HidP_SetUsages(ReportType,UsagePage:dword; LinkCollection:integer;UsageList:dword;
    var UsageLength :dword;  PreparsedData:dword;Report:PANSIChar;ReportLength:dword):dword;
     stdcall; external 'hid.dll' name 'HidP_SetUsages'
  function HidP_SetUsageValue( ReportType,UsagePage:dword; LinkCollection:integer;Usage:dword;
    UsageValue, PreparsedData:dword;Report:PANSIChar; ReportLength:dword):dword;
   stdcall; external 'hid.dll' name 'HidP_SetUsageValue'
   function HidP_GetCaps(PreparsedData:dword; Capabilities:dword):dword;
    stdcall; external 'hid.dll' name 'HidP_GetCaps'
   function HidP_GetSpecificValueCaps( ReportType,UsagePage:dword; LinkCollection:integer;Usage:dword;
   ValueCaps:dword; var ValueCapsLength:dword;PreparsedData:dword):dword;
     stdcall; external 'hid.dll' name 'HidP_GetSpecificValueCaps'
  function HidP_GetSpecificButtonCaps(ReportType,UsagePage:dword; LinkCollection:integer;Usage:dword;
   ButtonCaps:dword;var ButtonCapsLength:dword; PreparsedData :dword):dword;
  stdcall; external 'hid.dll' name 'HidP_GetSpecificButtonCaps'
  function   HidP_MaxUsageListLength( ReportType,UsagePage ,PreparsedData:dword):dword;
    stdcall; external 'hid.dll' name 'HidP_MaxUsageListLength'
  function HidD_FreePreparsedData(  PreparsedData :DWORD):BOOLEAN;
  stdcall; external 'hid.dll' name 'HidD_FreePreparsedData'
  function HidD_GetAttributes( HidDeviceObject:DWORD;    Attributes :dword):BOOLEAN;
    stdcall; external 'hid.dll' name 'HidD_GetAttributes'
  function HidD_GetPreparsedData(  HidDeviceObject:DWORD; PreparsedData :dword):BOOLEAN;
    stdcall; external 'hid.dll' name 'HidD_GetPreparsedData'
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//查找加密锁
function FindPort(Start: integer; OutPath: PANSIChar): integer; external;
 //查找指定的加密锁
function FindPort_2(Start: integer; In_Data: DWord; Verf_Data: DWord; OutPath: PANSIChar): integer; external;
//获到锁的版本
function NT_GetIDVersion(var AVersion: integer; InPath: PANSIChar): integer; external;
//获取锁的ID
function GetID(var ID_1, ID_2: DWord; InPath: PANSIChar): integer; external;
//算法函数
function sWriteEx(InData: DWord;var OutData:DWORD; InPath: PANSIChar): integer; external;
function sWrite_2Ex(InData: DWord;var OutData:DWORD; InPath: PANSIChar): integer; external;
function sWrite(InData: DWord; InPath: PANSIChar): integer; external;
function sWrite_2(InData: DWord; InPath: PANSIChar): integer; external;
function sRead(var OutData: DWord; InPath: PANSIChar): integer; external;
//从加密锁中读取一批字节
function YReadEx(OutData: PByte; Address: SmallInt; Len: SmallInt; HKey, LKey: PANSIChar; InPath: PANSIChar): Integer; external;
//从加密锁中读取一个字节，一般不使用
function YRead(var Out_EPROM: byte; Address: smallint; hkey,lkey,InPath: PANSIChar): integer; external;
//写一批字节到加密锁中
function YWriteEx(InData: PByte; Address: SmallInt; Len: SmallInt; HKey, LKey: PANSIChar; InPath: PANSIChar): Integer; external;
//写一个字节到加密锁中，一般不使用
function YWrite(InData: byte; Address: smallint;hkey,lkey, InPath: PANSIChar): integer; external;
//设置读密码
function SetReadPassword(HKey, LKey: PANSIChar; New_HKey, New_LKey: PANSIChar; InPath: PANSIChar): Integer; external;
//设置写密码
function SetWritePassword(HKey, LKey: PANSIChar; New_HKey, New_LKey: PANSIChar; InPath: PANSIChar): Integer; external;
//从加密锁中读字符串
function YReadString(OurStr: PANSIChar; Address: SmallInt; Len: Integer; HKey, LKey: PANSIChar; InPath: PANSIChar): Integer; external;
//写字符串到加密锁中
function YWriteString(InStr: PANSIChar; Address: SmallInt; HKey, LKey: PANSIChar; InPath: PANSIChar): Integer; external;
 //设置扩展算法密钥
function SetCal_2(Key,InPath:PANSIChar): Integer; external;
//使用增强算法对字符串进行加密
function EncString(InString,OutString,Path:PANSIChar): Integer; external;
//使用增强算法对二进制数据进行加密
function Cal(InBuf,OutBuf:PBYTE;Path:PANSIChar): Integer; external;

//使用增强算法对字符串进行解密(使用软件)
procedure DecString(InString,OutString,Key:PANSIChar);external;
//使用增强算法对二进制数据进行加密(使用软件)
procedure  DecBySoft(aData, aKey:PBYTE);external;
procedure  EncBySoft(aData,  aKey:PBYTE); external;
//字符串及二进制数据的转换
procedure  HexStringToByteArray(InString:PANSIChar;out_data:PBYTE);external;
procedure ByteArrayToHexString(in_data:PBYTE;OutString:PANSIChar;len:integer);external;
function ReadStringEx(addr:integer;outstring,DevicePath:PANSIChar) :integer;
function Sub_CheckKeyByEncstring(InString,DevicePath:PANSIChar)  :integer;

{
var
  KeyPath: array[0..Max_Path] of ansichar = ''; //储存加密锁所在路径字符串；
var
  KeyResult : Integer;
  MaxLen:short;
  OutChar: pansichar;
  mylen:integer;
begin
   //从加密锁中读取字符串
 	mylen := 5;//注意这里的6是长度，长度要与写入的字符串的长度相同
  OutChar:= AllocMem(mylen+1); //这里增加1，用于储存结束字符串
  ZeroMemory(OutChar,mylen+1);
  try
  KeyResult:= YReadString(OutChar,0, mylen,pansichar('04BBE109'),pansichar('E3AFEA47'), KeyPath);
  if KeyResult= 0 then
    ShowMessage('已成功读取加密锁EPROM中的数据！'+OutChar)
  else
     ShowMessage('读字符失败');
  finally
    FreeMem(OutChar);
  end;
end;
}
implementation
{$L ABPubDog.obj}

uses StrUtils;


function CheckKeyByFindort_2():integer;
var
        DevicePath: array[0..Max_Path] of ansichar; //用于储存加密锁的设备路径；
begin

        result:= FindPort_2(0, 1,DWORD(-350208594), DevicePath);
end;


function ReadStringEx(addr:integer;outstring,DevicePath:PANSIChar) :integer;
var
   ret:integer;
   nlen:short;
   buf:array[0..1]of byte;
begin
    //先从地址0读到以前写入的字符串的长度
    ret := YReadEx(@buf[0], addr, 1, '11111111', '11111111', DevicePath);
    nlen := buf[0];
    if ret <> 0 then begin
        result:=ret;
        exit;
    end;
    ZeroMemory(outstring,nlen+1);//将最后一个字符设置为0，即结束字符串
    //再读取相应长度的字符串
    result := YReadString(outstring, addr+1, nlen, '11111111', '11111111', DevicePath);

end;

function CheckKeyByReadEprom() :integer;
var
   ret,n:integer;
   DevicePath: array[0..Max_Path] of ansichar; //用于储存加密锁的设备路径；
   outstring:array[0..260] of ansichar;
begin
	//@NoUseCode_data result:=1;exit ;//如果没有使用这个功能，直接返回1
	for n:=0 to 255 do
        begin
		ret:=FindPort(n,DevicePath);
                if ret <> 0 then begin
                        result:=ret;
                        exit;
                end;
		ret:=ReadStringEx(0,outstring,DevicePath);
		if  (ret=0) and (StrComp('1234567890abcdeABCDE1234567890abcdeABCDE1234567890abcdeABCDE1234567890abcdeABCDE1234567890abcdeABCDE',outstring)=0)   then  begin
                        result:=0;
                        exit;
                end;
	end;
	result:= -92;
end;

function CheckKeyByEncstring() :integer;
var
   ret,n:integer;
   DevicePath: array[0..Max_Path] of ansichar; //用于储存加密锁的设备路径；
   InString:ansistring;
begin
	//推荐加密方案：生成随机数，让锁做加密运算，同时在程序中端使用代码做同样的加密运算，然后进行比较判断。

	//@NoUseKeyEx result:=1;exit ;//如果没有使用这个功能，直接返回1

	Randomize;
        InString:=inttohex(random(32767))+  inttohex(random(32767));

        for n:=0 to 255 do
        begin
		ret:=FindPort(n,DevicePath);
		if ret <> 0 then begin
                        result:=ret;
                        exit;
                end;
		if(Sub_CheckKeyByEncstring(PANSIChar(InString),DevicePath)=0) then  begin
                        result:=0;
                        exit;
                end;
	end;
	result:= -92;
end;

function Sub_CheckKeyByEncstring(InString,DevicePath:PANSIChar)  :integer;
var
   ret,nlen:integer;
   OutString:PANSIChar;
   OutString_2:ansistring;
begin
	//'使用增强算法对字符串进行加密
    nlen := lstrlenA(InString) + 1;
    if( nlen < 8 ) then  nlen := 8;
    outstring := AllocMem(nlen * 2+1);//注意，这里要加1一个长度，用于储存结束学符串
    ret := EncString(InString, outstring, DevicePath);
    if ret <> 0 then begin
        result:=ret;
        exit;
    end;
    OutString_2:= StrEnc(InString, '11111111111111111111111111111111');
    if(StrComp(outstring,PANSIChar(outstring_2))=0) then //比较结果是否相符
		ret:=0
	else
		ret:=-92;

     FreeMem (outstring);
     result:= ret;

end;


procedure _memset(P: Pointer; B: Byte; count: Integer); cdecl;
begin
  FillChar(P^, count, B);
end;

procedure _memcpy(dest, source: Pointer; count: Integer); cdecl;
begin
  Move(source^, dest^, count);
end;


function zlibAllocMem(AppData: Pointer; Items, Size: Integer): Pointer; register;
begin

  Result := AllocMem(Items * Size);
end;

procedure zlibFreeMem(AppData, Block: Pointer); register;
begin
  FreeMem(Block);
end;

function   StrEnc( InString ,  Key:ansistring):ansistring;//使用增强算法，加密字符串
var
  b,outb:pbyte;
  outstring:ansistring;
  temp_b,temp_outb:pbyte;
  n,nlen,outlen:integer;
  p_str:PANSIChar;
begin

  nlen := length(InString) + 1;
  If nlen < 8 Then
    outlen := 8
  else
    outlen := nlen;

  b:=AllocMem(outlen); temp_b:=b;
  outb:=AllocMem(outlen);temp_outb:=outb;
  p_str:=PANSIChar(InString);
  move(p_str^,b^,nlen);
  move(b^,outb^,outlen);

  n:=0;
  while n <= outlen-8  do
  begin
    Encode( b, outb, Key);
    inc(b,8);inc(outb,8);n:=n+8;
  end;
  outb:=temp_outb;b:=temp_b;
  outstring := '';
  for n := 0 to  outlen - 1 do
  begin
    outstring := outstring + IntToHex(outb^);
    inc(outb);
  end;
  outb:=temp_outb;
  FreeMem(b);FreeMem(outb);
  result:= outstring;
end;

function   StrDec( InString ,  Key:ansistring):ansistring;//使用增强算法，解密字符串
var
  b,outb:pbyte;
  temp_string,outstring:ansistring;
  temp_b,temp_outb:pbyte;
  n,nlen,outlen:integer;
  p_str:PANSIChar;
begin
  nlen := Length(InString);
  If nlen < 16 Then outlen := 16;
  outlen := nlen div 2  ;

   b:=AllocMem(outlen); temp_b:=b;
  outb:=AllocMem(outlen);temp_outb:=outb;

  n:=1;
  while  n<=nlen  do
  begin
    temp_string := MidStr(InString, n, 2);
    b^ := HexToInt(temp_string);
    inc(b);
    n:=n+2;
  end;
  b:=temp_b;

  move(b^,outb^,outlen);

  n:=0;
  while n <= outlen-8  do
  begin
    Decode( b, outb, Key);
    inc(b,8);inc(outb,8);n:=n+8;
  end;
  outb:=temp_outb;b:=temp_b;
  p_str:=AllocMem(outlen+1);
  move(outb^,p_str^,outlen);
  outstring:=p_str;
  FreeMem(b);FreeMem(outb);FreeMem(p_str);
  result:= outstring;
end;

function IntToHex( indata:dword):ansistring ;
var
 outstring:ansistring;
begin
    outstring:=Format('%X', [indata]);
    if length(outstring)<2 then outstring:='0'+outstring;
    result:=outstring;
end;

//以下用于将16进制字符串转化为无符号长整型
function  HexToInt( s:ansistring):dword ;
var
    i,j,r,n,k:integer;
    ch:ansistring;
    const   hexch:ansistring='0123456789ABCDEF';

begin
    s := ANSIUpperCase(s);
    k := 1; r := 0;
    for  i:=Length(s)   downto   1   do
    begin
      ch :=MidStr( s,i, 1);
      n:=0;
      for  j:=1   to   16   do
      begin
        if ch = MidStr(hexch,J,1) then
        begin
        n := j-1;
        end;
      end;
      r :=r+ (n * k);
      k := k*16;
    end;
    result:=r;
end;

procedure   EnCode(indata:pByte;   outdata:pByte;InKey:ansistring);
  var
      y,z,sum:   DWORD;
      Key: array[0..3]   of   DWORD;
      n,nlen,i:integer;
      temp_string:ansistring;
      keybuf: array[0..15]   of   byte;
  begin
    fillchar(keybuf,16,0);
    nlen := Length(InKey);
    i := 0;  n:=1;
    while n < nlen do
    begin
      temp_string :=MidStr(InKey, n,2);
      keybuf[i] := HexToInt(temp_string);
      i := i + 1; n:=n+2;
    end;

      for  n:=0   to   3   do
      begin
        Key[n]:=0;
      end;
      for  n:=0   to   3   do
      begin
        Key[0] := (keybuf[n] shl (n * 8)) or Key[0];
        Key[1] := (keybuf[n + 4] shl (n * 8)) or Key[1];
        Key[2] := (keybuf[n + 4 + 4] shl (n * 8)) or Key[2];
        Key[3] := (keybuf[n + 4 + 4 + 4] shl (n * 8)) or Key[3];
      end;

       y := 0;
       z := 0;
       for  n:=0   to   3   do
       begin
          y := (indata^ shl (n * 8)) or y;
          inc(indata);
       end;

        for  n:=0   to   3   do
       begin
          z := (indata^ shl (n * 8)) or z;
          inc(indata);
       end;

      sum:=0;
      for   n:=0   to   31   do
      begin
          inc(sum,Delta);
          inc(y,((z   shl   4)+key[0])   xor   (z+sum)   xor   ((z   shr   5)+key[1]));
          inc(z,((y   shl   4)+key[2])   xor   (y+sum)   xor   ((y   shr   5)+key[3]));
      end;

      for  n:=0   to   3   do
      begin
        outdata^ := BYTE(y shr (n * 8));
        inc(outdata)
     end;
     for  n:=0   to   3   do
      begin
        outdata^ := BYTE(z shr (n * 8));
        inc(outdata)
     end;
  end;

  procedure   DeCode(indata:pByte; outdata:pByte;InKey:ansistring);
  var
      y,z,sum:   DWORD;
      Key: array[0..3]   of   DWORD;
      n,nlen,i:integer;
      temp_string:ansistring;
      keybuf: array[0..15]   of   byte;
  begin
    fillchar(keybuf,16,0);
    nlen := Length(InKey);
    i := 0;  n:=1;
    while n < nlen do
    begin
      temp_string :=MidStr(InKey, n,2);
      keybuf[i] := HexToInt(temp_string);
      i := i + 1; n:=n+2;
    end;

      for  n:=0   to   3   do
      begin
        Key[n]:=0;
      end;
      for  n:=0   to   3   do
      begin
        Key[0] := (keybuf[n] shl (n * 8)) or Key[0];
        Key[1] := (keybuf[n + 4] shl (n * 8)) or Key[1];
        Key[2] := (keybuf[n + 4 + 4] shl (n * 8)) or Key[2];
        Key[3] := (keybuf[n + 4 + 4 + 4] shl (n * 8)) or Key[3];
      end;

       y := 0;
       z := 0;
       for  n:=0   to   3   do
       begin
          y := (indata^ shl (n * 8)) or y;
          inc(indata);
       end;

        for  n:=0   to   3   do
       begin
          z := (indata^ shl (n * 8)) or z;
          inc(indata);
       end;

      sum:=$C6EF3720;
      for   n:=0   to   31   do
      begin   
          dec(z,((y   shl   4)+key[2])   xor   (y+sum)   xor   ((y   shr   5)+key[3]));   
          dec(y,((z   shl   4)+key[0])   xor   (z+sum)   xor   ((z   shr   5)+key[1]));   
          dec(sum,Delta);   
      end;

      for  n:=0   to   3   do
      begin
        outdata^ := BYTE(y shr (n * 8));
        inc(outdata)
     end;
     for  n:=0   to   3   do
      begin
        outdata^ := BYTE(z shr (n * 8));
        inc(outdata)
     end;
  end;


end.


