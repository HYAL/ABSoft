{
SQL或数据集相关(此单元与特定的连接驱动无关)
}
unit ABPubDBU;
interface
{$I ..\ABInclude.ini}

uses
  ABPubConstU,
  ABPubVarU,
  ABPubMessageU,
  ABPubFuncU,
  ABPubUserU,
  ABPubLocalParamsU,

  Clipbrd, Classes, SysUtils, ADODB,DB,DBClient,Menus, Math, Variants,Types,
  Controls, Forms, ComCtrls,Graphics,

  System.Win.ComObj,ExtCtrls,
  TypInfo,jpeg;

//*******************SQL相关*********************************
//替换aSQL中的格式为"[ABParams_参数名] 、 [ABUser_属性名] 、 [ABPublicSQL_SQL名]"的字段名为数据集引用的字段值
function ABReplaceSqlExtParams(aSQL: string): string;
//将同分隔符分隔的字串转换成union连接的SQL语句(字串中不包含Select)
//aQuoted=aSpaceSign分隔的字串aStrs是否加Quoted
//aAddBegFieldValue=转换成union连接的SQL语句前缀
//aAddEndFieldValue=转换成union连接的SQL语句后缀
function ABStrsToSql(aStrs: string; aSpaceSign: string;aQuoted:boolean=false; aAddBegFieldValue: string='';aAddEndFieldValue: string=''): string;
//多字段名与多字段值转成SQL的Where
//aFieldNames=字段名数组
//aFieldValues=字段值数组
//aFieldValueQuoteds=字段值是否加Quoted
//aFieldNameAndFieldValueCompareStr=字段名与字段值的比较符
//aLinkAndOr=不同字段间的连接逻辑符
function ABFieldNamesAndFieldValuesToWhere(
                        aFieldNames: array of string;
                        aFieldValues: array of string;
                        aFieldValueQuoteds: array of Boolean;
                        aFieldNameAndFieldValueCompareStrs: array of string;
                        aLinkAndOr:string = ' and '): string;overload;
//数据集多字段名与值转成SQL的Where
function ABFieldNamesAndFieldValuesToWhere(
                        aDataset:TDataSet;
                        aFieldNames: array of string;
                        aFieldValueQuoteds: array of Boolean;
                        aFieldNameAndFieldValueCompareStrs: array of string;
                        aLinkAndOr:string = ' and '): string;overload;
//多字段名与单字段值转成SQL的Where
function ABFieldNamesAndFieldValueToWhere(
                        aFieldNames: array of string;
                        aFieldValue: string;
                        aFieldValueQuoted:Boolean;
                        aFieldNameAndFieldValueCompareStrs: array of string;
                        aLinkAndOr:string = ' or '): string;
//单字段名与多字段值转成SQL的Where
function ABFieldNameAndFieldValuesToWhere(
                        aFieldName: string;
                        aFieldValues:array of string;
                        aFieldValueQuoted:Boolean;
                        aFieldNameAndFieldValueCompareStrs: array of string;
                        aLinkAndOr:string = ' or '): string;

//取可应用到SQL语句中的字段值
function ABGetSQLFieldValue(aField:TField):string;
//替换aSQL中的格式为"[:字段名]"的字段名为数据集引用的字段值
function ABReplaceSqlFieldNameParams(aSQL: string; aDatasets:array of TDataset): string;

//是否是正确的SQL语句
function ABIsOkSQL(aSQL: string): Boolean;
//是否是正确的查询SQL语句
function ABIsSelectSQL(aSQL: string): Boolean;

//得到数据集记录的更新和插入的SQL语句
//aDataset=生成SQL的源数据集
//aTableName= 生成SQL的表名
//aKeyFieldnames=条件中的主键字段名
//aUpdateFieldnames=更新的字段名
//aInsertFieldnames=插入的字段名
//aFieldnameReplaceSQLs=对于aKeyFieldnames/aUpdateFieldnames/aInsertFieldnames的字段列表可设置替换的SQL脚本
//格式为字段名=aFieldnameReplaceDatasets数据集字段名=替换的SQL脚本,在替换的SQL脚本中如果有参数，要换成实际aFieldnameReplaceDatasets数据集字段值
//aFieldnameReplaceDatasets=提供替换字段对应值的数据集数组
//aAllRecord=是生成数据集所有记录还是公当前记录的数据

//调用示例如下(ABSys_Org_Tree为主表,ABSys_Org_TreeItem为从表,以Ti_Tr_Guid字段关联主表的Tr_Guid字段)
//tempStrings.Add('Ti_Tr_Guid=Tr_Guid=(select Tr_Guid from ABSys_Org_Tree where Tr_Code='':Tr_Code'')');
//ABGetSQLByDataset(
//                       tempDatasetTreeItem,
//                       'ABSys_Org_TreeItem',
//                       ['Ti_Tr_Guid','Ti_Code'],
//                       ['Ti_Group','Ti_Name','Ti_int1','Ti_int2','Ti_decimal1','Ti_decimal2','Ti_Varchar1','Ti_Varchar2','Ti_bit1','Ti_bit2','TI_Remark','Ti_Order'],
//                       ['Ti_Group','Ti_Name','Ti_int1','Ti_int2','Ti_decimal1','Ti_decimal2','Ti_Varchar1','Ti_Varchar2','Ti_bit1','Ti_bit2','TI_Remark','Ti_Order',
//                        'Ti_Tr_Guid','Ti_Code','Ti_ParentGuid','Ti_Guid','PubID'],
//                       tempStrings,
//                       [tempDatasetTree],
//                       true
//                       )
function ABGetSQLByDataset(aDataset:TDataSet;
                           aTableName:string;

                           aKeyFieldnames:array of string;
                           aUpdateFieldnames:array of string;
                           aInsertFieldnames:array of string;

                           aFieldnameReplaceSQLs:TStrings;
                           aFieldnameReplaceDatasets:array of TDataSet;

                           aAllRecord:Boolean=false):string;

//得到树关联数据集记录的更新和插入的SQL语句
//树中结点通过Data属性关联到数据集的Recno
function ABGetSQLByTreeView(aTreeView: TTreeView;
                           aDataset:TDataset;
                           aTableName:string;

                           aKeyFieldnames:array of string;
                           aUpdateFieldnames:array of string;
                           aInsertFieldnames:array of string;

                           aFieldnameReplaceSQLs:TStrings;
                           aFieldnameReplaceDatasets:array of TDataSet):string;
//得到树结点关联数据集记录的更新和插入的SQL语句
function ABGetSQLByTreeNode(aTreeNode: TTreeNode;
                           aDataset:TDataset;
                           aTableName:string;

                           aKeyFieldnames:array of string;
                           aUpdateFieldnames:array of string;
                           aInsertFieldnames:array of string;

                           aFieldnameReplaceSQLs:TStrings;
                           aFieldnameReplaceDatasets:array of TDataSet;
                           aGetClient:Boolean=true):string;



//**************读取与设置数据集、字段值相关*************
//得到数据集单个字段的值(由字段名决定返回的字段)
//aDefaultValueOnFieldNull=当字段值为Null时返回的默认值
//aNoFindIsShowMsg=当字段没有找到时是否要显示信息框
//aNoFindShowMsg=当字段没有找到时显示的信息文本
function ABGetFieldValue(aDataset: TDataset;
                         aFieldName: string;
                         aDefaultValueOnNull: Variant;

                         aNoFindIsShowMsg: Boolean = false;
                         aNoFindShowMsg: string = ''): Variant; overload;
//得到数据集单个字段的值(由Fields的字段序号决定返回的字段)
function ABGetFieldValue(aDataset: TDataset;
                         aIndex: LongInt;
                         aDefaultValueOnNull: Variant;

                         aNoFindIsShowMsg: Boolean = false;
                         aNoFindShowMsg: string = ''): Variant; overload;
//得到数据集指定Key字段值下的多个字段的值数组(由字段名串决定返回的多个字段)
function ABGetFieldValue(aDataset: TDataset;
                         aKeyFields: array of string;
                         aKeyValues: array of Variant;

                         aFieldNames: array of string;
                         aDefaultValueOnNull:Variant): Variant;overload;
//取数据集多个字段的值
function ABGetFieldValue( aDataset: TDataset;
                          aFieldNames:string;
                          aDefaultValueOnNull: array of Variant;

                          aSpaceSign: string=',';
                          aNoFindIsShowMsg: Boolean = false;
                          aNoFindShowMsg: string = ''
                          ): string;overload;
//得到数据集多个字段的值(由字段名数组决定返回的多个字段)
//aSpaceSign=多个字段值间的分隔符
function ABGetFieldValue( aDataset: TDataset;
                          aFieldNames: array of string;
                          aDefaultValueOnNull: array of Variant;

                          aSpaceSign: string=',';
                          aNoFindIsShowMsg: Boolean = false;
                          aNoFindShowMsg: string = ''
                          ): string;overload;
//得到数据集多个字段的值(由Fields的字段序号数组决定返回的多个字段)
function ABGetFieldValue( aDataset: TDataset;
                          aFieldIndexs: array of LongInt;
                          aDefaultValueOnNull: array of Variant;

                          aSpaceSign: string=',';
                          aNoFindIsShowMsg: Boolean = false;
                          aNoFindShowMsg: string = ''
                          ): string;overload;

//设置字段的值
//aPost=设置后是否进行保存
//aAppendValue=值是否追加在字段的原值上（数值类型字段按值相加，非数值类型按字符追加）
//aNoFindIsShowMsg=当字段不存在时是否要显示信息框
//aNoFindShowMsg=当字段不存在时显示的信息框文本
procedure ABSetFieldValue(
                          aValue: Variant;
                          aField: TField;

                          aPost: Boolean = false;
                          aAppendValue: Boolean = false;

                          aNoFindDFieldIsShowMsg: Boolean = false;
                          aNoFindDFieldShowMsg: string = ''
                          ); overload;
//将多个值设置给数据集多个字段(由字段名数组决定设置的多个字段)
//数组aValues与aFieldNames要一一对应
procedure ABSetFieldValue(
                          aValues:array of  Variant;
                          aDataset: TDataset;
                          aFieldNames:array of string;

                          aAppend: Boolean = false;
                          aPost: Boolean = false;
                          aAppendValue: Boolean = false;

                          aNoFindDFieldIsShowMsg: Boolean = false;
                          aNoFindDFieldShowMsg: string = ''
                          ); overload;
//设置数据集指定Key字段值下的多个字段的值(全为NULL时删除，不存在时新增，存在时修改)
procedure ABSetFieldValue(
                          aDataset: TDataset;
                          aKeyFieldNames: array of string;
                          aKeyFieldValues: array of Variant;
                          aFieldNames: array of string;
                          aFieldValues: array of Variant
                            );overload;
//将源数据集的多个字段值设置给目标数据集多个字段
//aSDataset=源数据集
//aSFieldNames=源数据集的多个字段
//aDDataset=目标数据集
//aDFieldNames=源数据集的多个字段
procedure ABSetFieldValue(
                          aSDataset: TDataset;
                          aSFieldNames:array of string;
                          aDDataset: TDataset;
                          aDFieldNames:array of string;

                          aAppend: Boolean = false;
                          aPost: Boolean = false;
                          aAppendValue: Boolean = false;

                          aNoFindDFieldIsShowMsg: Boolean = false;
                          aNoFindDFieldShowMsg: string = ''
                          ); overload;

//将源数据集与目标数据集中指定的多个共有字段复制给目标数据集字段中
//aFieldNames=指定要设置的字段名，仅当指定的字段名在源数据集与目标数据集都存在时才进行操作
procedure ABSetFieldValue(
                          aSDataset: TDataset;
                          aDDataset: TDataset;
                          aFieldNames:array of string;

                          aAppend: Boolean = false;
                          aPost: Boolean = false;
                          aAppendValue: Boolean = false;

                          aNoFindDFieldIsShowMsg: Boolean = false;
                          aNoFindDFieldShowMsg: string = ''
                          ); overload;
//将源数据集与目标数据集中所有共有的字段复制给目标数据集字段中
procedure ABSetFieldValue(
                          aSDataset: TDataset;
                          aDDataset: TDataset;

                          aAppend: Boolean = false;
                          aPost: Boolean = false;
                          aAppendValue: Boolean = false;

                          aNoFindDFieldIsShowMsg: Boolean = false;
                          aNoFindDFieldShowMsg: string = ''
                          ); overload;
//得到数据集多个字段所有记录的值(由字段名数组决定多个字段)
//aCheckFieldNames=在取数据集字段值时需附加判断的字段
//aCheckFieldValues=在取数据集字段值时需附加判断的字段值，仅当判断的字段等于判断的值时才进行值的累加
//aRecordSpaceSign=记录间隔符
//aFieldSpaceSign=字段间隔符
function ABGetDatasetValue(
                        aDataset: TDataset;
                        aFieldNames: array of string;

                        aCheckFieldNames: array of string;
                        aCheckFieldValues: array of string;

                        aRecordSpaceSign: string = ABEnterWrapStr;
                        aFieldSpaceSign: string = ',';
                        aFillSameFieldValues:Boolean=True
                        ): string; overload;
//得到数据集多个字段所有记录的值(由Fields的字段序号数组决定多个字段)
function ABGetDatasetValue(
                        aDataset: TDataset;
                        aFieldIndexs: array of LongInt;

                        aCheckFieldNames: array of string;
                        aCheckFieldValues: array of string;

                        aRecordSpaceSign: string = ABEnterWrapStr;
                        aFieldSpaceSign: string = ',';
                        aFillSameFieldValues:Boolean=True
                        ): string; overload;
//得到数据集多个字段所有记录的值(由字段名数组决定多个字段)
//在取每条记录值时先用检测数据集进行Locate
//aLocateDataset.Locate(aLocateDatasetFieldName,aDataset.FindField(aDatasetLocateValueFieldName).Value,[])
//如成功则返回数据集多个字段的值，
//如不成功则不返回记录值而返回aNoLocateDefaultValue,如果aNoLocateDefaultValue为空则不加入到返回中
function ABGetDatasetValue(
                        aDataset: TDataset;
                        aFieldNames: array of string;
                        aCheckFieldNames: array of string;
                        aCheckFieldValues: array of string;

                        aDatasetLocateValueFieldName: string;

                        aLocateDataset: TDataset;
                        aLocateDatasetFieldName: string;

                        aNoLocateDefaultValue: string = '';

                        aRecordSpaceSign: string = ABEnterWrapStr;
                        aFieldSpaceSign: string = ',';
                        aFillSameFieldValues:Boolean=True
                        ): string; overload;

//将源数据集所有记录多个字段值拷贝到目标数据集多个记录多个字段中
procedure ABSetDataSetValue(
                            aSDataSet: TDataSet;
                            aSFieldNames: array of string;
                            aDDataSet: TDataSet;
                            aDFieldNames: array of string
                            );overload;
//将源数据集所有记录多个共有字段拷贝到目标数据集中
procedure ABSetDataSetValue(
                            aSDataSet: TDataSet;
                            aDDataSet: TDataSet;
                            aFieldNames: array of string
                            );overload;
//将源数据集所有记录所有共有字段拷贝到目标数据集中
procedure ABSetDataSetValue(
                            aSDataSet: TDataSet;
                            aDDataSet: TDataSet
                            );overload;

//*******************数据集、字段定义、字段创建相关*********************************
//根据传入的字段信息创建并激活ClientDataSet且返回
//aFieldNames=创建的字段名数组
//aFieldDisplayLabels=创建的字段显示标签数组
//aFieldTypes=创建的字段类型数组
//aFieldsizes=创建的字段大小数组
//以上参数的数组大小需相同且一一对应
function ABCreateClientDataSet(
                                aFieldNames: array of string;
                                aFieldDisplayLabels: array of string;
                                aFieldTypes: array of TDataType;
                                aFieldsizes: array of LongInt
                                ): TClientDataSet;overload;
//根据传入的字段信息激活传入的数据集
procedure ABCreateClientDataSet(
                                aDataSet: TClientDataSet;
                                aFieldNames: array of string;
                                aFieldDisplayLabels: array of string;
                                aFieldTypes: array of TDataType;
                                aFieldsizes: array of LongInt
                                );overload;

//根据字段信息数据集创建字段定义及字段到目标字段列表中
procedure ABCreateFieldDefs(aFieldDefs:TFieldDefs;
                                 aFieldInfoDataset:TDataset;
                                 aFieldNameField:string='Name';
                                 aFieldTypeField:string='Type';
                                 aFieldByteField:string='Byte';
                                 aFieldLengthField:string='Length';
                                 aFieldEecimalDigitsField:string='EecimalDigits';

                                 aClearOldFieldDefs:Boolean=true;
                                 aCreateField:Boolean=false;

                                 aHaveField:string='';
                                 aNoHaveField:string='');
//创建单个字段定义
procedure ABCreateFieldDef(
                                 aFieldDefs:TFieldDefs;
                                 aFieldName,
                                 aFieldType:string;
                                 aFieldByte,
                                 aFieldLength,
                                 aEecimalDigits:Longint);
//根据字段定义创建字段
procedure ABCreateFields(aFieldDefs:TFieldDefs;
                              aHaveField:string='';
                              aNoHaveField:string='');

//*******************数据集其它操作相关*********************************
//备份字段的事件到参数并清空
procedure ABBackAndStopFieldEvent(aField:TField;
                           var aOnGetText: TFieldGetTextEvent;var aOnSetText: TFieldSetTextEvent;
                           var aOnValidate: TFieldNotifyEvent;var aOnChange: TFieldNotifyEvent
                           );
//将备份的字段事件恢复到字段中
procedure ABUnBackFieldEvent(aField:TField;
                             aOnGetText: TFieldGetTextEvent;aOnSetText: TFieldSetTextEvent;
                             aOnValidate: TFieldNotifyEvent;aOnChange: TFieldNotifyEvent
                           );
//备份数据集的事件到参数并清空
procedure ABBackAndStopDatasetEvent(aDataset:TDataset;
                           var aBeforeScroll: TDataSetNotifyEvent;var aAfterScroll: TDataSetNotifyEvent
                           );
//将备份的数据集事件恢复到数据集中
procedure ABUnBackDatasetEvent(aDataset:TDataSet;
                           aBeforeScroll: TDataSetNotifyEvent;aAfterScroll: TDataSetNotifyEvent
                           );

//数据集字段显示名到字段名的转换(字段显示名在当前数据集中不能重复)
//aDisplayLabels=需转换的字段显示名，如有多个则用分隔符aSpaceSign进行分隔
function ABGetFieldNamesByDisplayLabels(aDataSet: TDataSet; aDisplayLabels: string; aSpaceSign: string = ','): string;
//通过数据集字段显示名取得字段对象
function ABGetFieldByDisplayLabel(aDataSet: TDataSet; aDisplayLabel: string): TField;
//数据集字段字段名到显示名的转换(字段显示名在当前数据集中不能重复)
//aFieldNames=需转换的字段名，如有多个则用分隔符aSpaceSign进行分隔
function ABGetDisplayLabelsByFieldName(aDataSet: TDataSet; aFieldNames: string;aSpaceSign: string = ','): string;

//得到所有关联到数据集的所有层的子数据集列表
//aList=返回的子数据集列表
//aIncludeSelf=是否将主数据集aDataSet也加入到返回的列表中去
procedure ABGetDetailDatasetList(aDataSet: TDataSet; aList:TList;aIncludeSelf:Boolean=false);
//得到所有关联到数据集的DataSource列表
//aList=返回的子数据集列表
procedure ABGetDataSourceList(aOwner:TComponent;aDataset: TDataset; aList:TList);
//打开所有关联到数据集的相关数据集
//aIncludeSelf=是否将主数据集aDataSet也包含
procedure ABOpenDetailDataset(aDataSet: TDataSet; aIncludeSelf:Boolean=false);overload;
//打开列表中的数据集
procedure ABOpenDetailDataset(aList:TList);overload;
//得到所有关联到数据集的所有层的已打开子数据集列表
//aList=返回的子数据集列表
//aIncludeSelf=是否将主数据集aDataSet也加入到返回的列表中去
procedure ABGetActiveDetailDataSets(aDataSet: TDataSet; aList:TList;aIncludeSelf:Boolean=false);overload;


//断开所有关联到主数据集的所有层的子数据集与界面显示的关联
procedure ABSetDatasetDisableControls(aDataset: TDataset);
//恢复所有关联到主数据集的所有层的子数据集与界面显示的关联
procedure ABSetDatasetEnableControls(aDataset: TDataset);

//设置数据集指定字段名的字段显示
//aFieldNames=需设置的字段名
//aInitVisibleFalse=除了字段名数组aFieldNames指定的字段，其它的字段是否全部设置为隐藏
procedure ABOnlyShowFields(aDataSet: TDataSet;
                           aFieldNames: array of string;
                           aInitVisibleFalse: Boolean = true);
//设置数据集指定字段名的字段隐藏
procedure ABOnlyHintFields(aDataSet: TDataSet;
                           aFieldNames: array of string;
                           aInitVisibleTrue: Boolean = true);
//设置数据集指定字段名的字段只读
procedure ABOnlyReadFields(aDataSet: TDataSet;
                           aFieldNames: array of string;
                           aInitReadFalse: Boolean = true);
//设置数据集指定字段名的字段可编辑
procedure ABOnlyEditFields(aDataSet: TDataSet;
                           aFieldNames: array of string;
                           aInitReadTrue: Boolean = true);

//设置数据集的记录号
function ABSetDatasetRecno(aDataSet: TDataSet;aRecno:LongInt):LongInt;overload;
//将清除了开始与结束标记的记录号设置给数据集
function ABSetDatasetRecno(aDataSet: TDataSet;aRecno:LongInt;aBegFlag:string;aEndFlag:string=''):LongInt;overload;

//返回数据集记录号
function ABGetDatasetRecno(aDataSet: TDataSet):LongInt;overload;
//返回清除了开始与结束标记的数据集记录号
//因为统一记录记录集位置的需要，所以一般树形或菜单结点的.data属性中保存"开始标记[1=主表，2=从表]+数据集的记录号"
function ABGetDatasetRecno(aRecno:LongInt;aBegFlag:string;aEndFlag:string=''):LongInt;overload;

//取得设置了过滤条件后的数据集记录数量
function ABGetDataSetRecordCount(
                                aDataSet: TDataSet;
                                aFilterFieldNames: array of string;
                                aFilterFieldValues: array of string
                              ): LongInt;overload;
//取得数据集列表所有数据集的记录数量之和
function ABGetDataSetRecordCount(aDatasetList: TList): LongInt;overload;

//检测字段的新旧值是否相同
function ABComparisonFieldOldNewValueSame(aField: TField): Boolean;
//取字段的OldValue值,因为Delphi不支持ADO type Vt_Decimal (14)，所以不能直接访问ADO下Vt_Decimal字段的OldValue，此处兼容处理一下
function ABGetFieldOldValue(aField: TField):Variant;
//取字段的NewValue值
function ABGetFieldNewValue(aField: TField):Variant;

//得到数据集的过滤条件
function ABGetDatasetFilter(aDataset: TDataset):string;
//设置数据集的过滤条件
procedure ABSetDatasetFilter(aDataset: TDataset;aFilter:string);

//检测数据集是否可修改
function ABCheckDataSetCanEdit(aDataSet:TDataSet):boolean;overload;
//检测数据集是否在修改中
function ABCheckDataSetInEdit(aDataSet:TDataSet):boolean;overload;
//检测数据集是否打开中
function ABCheckDataSetInActive(aDataSet:TDataSet):boolean;overload;

//使数据集处于修改状态
procedure ABSetDatasetEdit(aDataSet:TDataSet);
//删除数据集中的数据
procedure ABDeleteDataset(aDataset:TDataset);
//保存数据集的修改
procedure ABPostDataset(aDataSet: TDataSet);
//判断数据集是否为空
function ABDatasetIsEmpty(aDataset: TDataset): boolean;

//返回数据集所有记录多个字段值的求和
//返回数组的长度=字段的数量,同一字段所有记录的值到一个返回数组项中
//aFieldNames=需求和的字段名数组
//aFieldValueSumTypes=字段求和方式(按数值求和还是按字串累加求和,如为[]时则数值类型字段按数值求和字符类型字段按字串累加)
//aCheckFieldNames=在求和时需附加判断的字段
//aCheckFieldValues=在求和时需附加判断的字段值，仅当判断的字段等于判断的值时才进行值的累加
//aSpaceSign=当按字串累加求和时不同记录间字段值间隔符
function ABSumDataset(aDataSet: TDataSet;
                      aFieldNames: array of string;
                      aFieldValueSumTypes: array of TABFieldValueSumType;

                      aCheckFieldNames: array of string;
                      aCheckFieldValues: array of string;
                      aSpaceSign: string = ','): TStringDynArray;

//拷贝源数据集所有共有字段的ProviderFlags属性到目标数据集中
procedure ABCopyFieldProviderFlags(aSDataset,aDDataset:Tdataset);
//拷贝源数据集所有共有字段的显示标题到目标数据集中
procedure ABCopyFieldDisplayLabel(aSDataset,aDDataset:Tdataset);

//检测对象是否是数据感知对象
function ABIsDataAware(aObject:TObject):boolean;
//设置与字段关联的感知控件得到焦点
function ABSetControlFocusByField(aField: TField): Boolean;

//检测数据集的多个字段是否是指定的值,如不是则由显示提示框
//aDataSet=检测字段值的数据集
//aCheckFieldNames=检测时判断的字段
//aCheckFieldValues=检测判断的字段值，仅当判断的字段等于判断的值时才返回True
//aNotEqualShowMsg=检测字段不等于检测值时是否显示信息框
//aaNotEqualMsg=检测字段不等于检测值时显示的信息框文本
//aCheckAllRecord=是否检测数据集的所有记录
function ABCheckDataSetValue(aDataSet: TDataSet;
                             aCheckFieldNames: array of string;
                             aCheckFieldValues: array of string;
                             aNotEqualShowMsg: boolean = False;
                             aNotEqualMsg: string='';
                             aCheckAllRecord: boolean = false
                             ): Boolean;

//通过窗体对象和数据集名称定位窗体中数据集符合条件的记录
function ABLocateFormDataset( aForm: TForm;
                              aDataSetName: string;
                              const aKeyFields: string;
                              const aKeyValues: Variant;
                              aOptions: TLocateOptions=[loCaseInsensitive]): Boolean;

//将主从数据集的数据合并到新的数据集中
//aNewDataSet=返回结果的新数据集
//aNewCopyFields=新数据集需从主从数据集拷贝的字段名数组
//aNewDetailDataFlagFieldName=新数据集中标识记录数据是来自主数据集还是从数据集的字段名(值=true时来自从数据集，值=False时来自主数据集)
//aMainDataSet=需合并到新数据集的主数据集
//aMainKeyField=主数据集与从数据集关联的字段
//aMainCopyFields=主数据集拷贝到新数据集的拷贝字段
//aDetailDataSet =需合并到新数据集的从数据集
//aDetailParentField =从数据集与主数据集关联的字段
//aDetailCopyFields  =从数据集拷贝到新数据集的拷贝字段
procedure ABDataSetsToNewDataSet(
                            aNewDataSet: TDataSet;
                            aNewCopyFields: array of string;
                            aNewDetailDataFlagFieldName: string;

                            aMainDataSet: TDataSet;
                            aMainKeyField: string;
                            aMainCopyFields: array of string;

                            aDetailDataSet: TDataSet;
                            aDetailParentField: string;
                            aDetailCopyFields: array of string
                            );
//设置主从结构的数据集中没有第一层记录的父字段的值
procedure ABSetNoParentDatasetValue(
                                    aDataSet: TDataSet;
                                    aParentFieldName: string;
                                    aKeyFieldName: string;
                                    aParentFieldValue: string='0'
                                    );
//删除主从结构数据集中没有子数据的记录
//aKeyField=主键字段
//aParentField=指向主记录主键的字段
//aCheckFieldNames=删除时需附加判断的字段
//aCheckFieldValues=删除时需附加判断的字段值，仅当判断的字段等于判断的值时才进行删除
procedure ABDeleteNoSubRecord(
                              aDataSet: TDataSet;
                              aParentFieldName: string;
                              aKeyFieldName: string;
                              aCheckFieldNames: array of string;
                              aCheckFieldValues: array of string
                              );

//将数据集的值填充到列表中
//aDataset=填充值的数据集
//aFieldNames=填充值的字段名
//aStrings=返回结果的字串列表
//SpaceSign=多个字段值时分隔符
//aNoFillSame=碰已存在于列表中的是否不重复填充
//aIsDelOld=在填充之前是否删除列表中已有的内容
procedure ABDatasetToStrings( aDataset: TDataset;
                              aFieldNames: array of string;
                              aStrings: TStrings;
                              SpaceSign: string = ',';
                              aNoFillSame: boolean = true;
                              aIsDelOld: boolean = true
                              );
//将数据集的字段名或标签名填充到列表中
//aDataset=填充值的数据集
//aStrings=返回结果的字串列表
//aIsDisplayLabel=是填充字段名还是填充字段的显示标签，false时填充字段名，True时填充字段标签
//aNoFillSame=碰已存在于列表中的是否不重复填充
//aIsDelOld=在填充之前是否删除列表中已有的内容
procedure ABFieldNameToStrings( aDataset: TDataset;
                                aStrings: TStrings;
                                aIsDisplayLabel: boolean = false;
                                aNoFillSame: boolean = true;
                                aIsDelOld: boolean = true
                                );

//删除字串列表中出现的数据集字段值的项
procedure ABDelStringsByDataset(  aStrings: TStrings;
                                  aDataset: TDataset;
                                  aFieldNames: array of string;
                                  SpaceSign: string = ','
                                  );
//替换字串中数据集编号字段值到数据集名称字段值并返回
//aStr=替换的字串
//aDataset=编号与名称字段的数据集
//aCodeFieldName=编号字段名
//aNameFieldName=名称字段名
function ABCodeAndValueFieldConversion( aStr: string;
                            aDataset: TDataSet;
                            aCodeFieldName,
                            aNameFieldName: string):string; overload;
function ABCodeAndValueFieldConversion(aStr: string;
                           aDataset: TDataSet;
                           aCodeFieldIndex,
                           aNameFieldIndex: Longint):string; overload;

//替换字串列表中数据集编号字段值到数据集名称字段值
//aStr=替换的字串
//aDataset=编号与名称字段的数据集
//aCodeFieldName=编号字段名
//aNameFieldName=名称字段名
procedure ABCodeAndValueFieldConversion(aStrings: TStrings;
                            aDataset: TDataSet;
                            aCodeFieldName,
                            aNameFieldName: string);overload;

procedure ABCodeAndValueFieldConversion(aStrings: TStrings;
                            aDataset: TDataSet;
                            aCodeFieldIndex,
                            aNameFieldIndex: Longint);overload;
//根据数据集字段的内容创建单选组RadioGroup的项
//aField=数据集字段
//aDefaultIndex=创建后默认选择的项序号
//aDefaultHeight=创建后单选组高度
//aClearOld=创建前是否清除单选组的项
procedure ABCreateRadioGroupItems(aField:TField;
                                  aRadioGroup: TRadioGroup;
                                  aDefaultIndex: LongInt = 0;
                                  aDefaultHeight: LongInt = 35;
                                  aClearOld:Boolean=true);

//拷贝BlobField字段中的JPEG到TBitmap
procedure ABBlobFieldJpegToBitmap(aField:TField;aBitmap:TBitmap);
//拷贝BlobField字段中的JPEG到TPicture
procedure ABBlobFieldJpegToPicture(aField:TField;aPicture:TPicture);

//得到数据集的字段名
function ABGetDatasetFieldNames(aDataset: TDataSet;aNotHaveFieldNames: string='';aSpaceSign: string=',' ): string;overload;
//关闭数据集
procedure ABCloseDataset(aDataset: TDataSet;aCloseDetail:boolean=false);
//取得主从数据集键值的层次
//aQueryStrings=备份已到到的键值层次，下次再找相同键值层次时就可以直接取了
function ABGetDataSetLevel(
                            aDataSet     :TDataSet;
                            aKeyField    :string;
                            aParentField :string;
                            aValue       :string;
                            aQueryStrings:TStrings;
                            aTopValue    :string=''
                            ): longint;
//设置主从数据集的层次
procedure ABSetDataSetLevel(
                            aDataSet     :TDataSet;
                            aKeyField    :string;
                            aParentField :string;
                            aLevelField    :string;
                            aTopValue    :string=''
                            );
//TreeView操作
//将主从结构的主数据集和明细数据集的数据导出到TreeView中
//aTreeNodes         =导出到TreeView控件的结点集合
//aMainDataSet       =主从结构主数据集
//aMainParentField   =主从结构主数据集的父键字段
//aMainKeyField      =主从结构主数据集的主键字段
//aMainNameField     =主从结构主数据集的名称键字段（此字段的值显示在TreeView结点中）
//aDoOrderData=        主数据是否按层次进行了排序,预先排好序可以大大提高加载的速度
//aDetailDataSet     =从数据集
//aDetailParentField =从数据集的父键字段
//aDetailNameField   =从数据集的名称键字段（此字段的值显示在TreeView结点中）
//aBeginNode         =导出到TreeView控件的开始结点，如不为空则出到此结点上，
//                    如为nil则先清除TreeView再导出到TreeView的顶层结点
//aSaveNodeData      =是否将数据集的记录号保存到结点的Data中
//aDoNotMainDetailData=主从结构主数据集中数据没有对应从数据集数据是是否还导出到TreeView中
procedure ABDataSetsToTree(
                          aTreeNodes        : TTreeNodes;

                          aMainDataSet          : TDataSet;
                          aMainParentField      : string;
                          aMainKeyField         : string;
                          aMainNameField        : string;
                          aMainLevelOrder       :Boolean=False;

                          aDetailDataSet    : TDataSet=nil;
                          aDetailParentField: string='';
                          aDetailNameField  : string='';

                          aParentItem        : TTreeNode=nil;
                          aSaveNodeData     :Boolean=true;
                          aDoNotMainDetailData:Boolean=true
                          );

//TMenu操作
//将主从结构的主数据集和明细数据集的数据导出到菜单中
//aMenu              =接收导出数据的菜单
//aMainDataSet       =主从结构主数据集
//aMainParentField   =主从结构主数据集的父键字段
//aMainKeyField      =主从结构主数据集的主键字段
//aMainNameField     =主从结构主数据集的名称键字段（此字段的值显示在菜单结点中）

//aMainHintField       =菜单结点Hint信息从主从结构主数据集取数据的字段
//aMainBeginGroupField =菜单结点是否以新组开始信息从主从结构主数据集取数据的字段
//aMainBitmapField     =菜单结点图标信息从主从结构主数据集取数据的字段
//aMainOnClick         =菜单结点的点击事件
//aMainLevelOrder=        数据是否按层次进行了排序，预先排好序可以大大提高加载的速度

//aDetailDataSet     =从数据集
//aDetailParentField =从数据集的父键字段
//aDetailNameField   =从数据集的名称键字段（此字段的值显示在菜单结点中）

//aDetailHintField       =菜单结点Hint信息从从数据集取数据的字段
//aDetailBeginGroupField =菜单结点是否以新组开始信息从从数据集取数据的字段
//aDetailBitmapField     =菜单结点图标信息从从数据集取数据的字段
//aDetailOnClick         =菜单结点的点击事件

//aParentItem         =导出到菜单控件的开始结点，如不为空则出到此结点上，
//aDoNotMainDetailData=主从结构主数据集中数据没有对应从数据集数据是是否还导出到TreeView中
//aLinkObject=        =菜单关联的对象，如主菜单关联的是窗体，右键菜单关联的是弹出右键的控件，传进来是为了快速创建菜单
procedure ABDataSetsToMenu(
                            aMenu: TMenu;

                            aMainDataSet: TDataSet;
                            aMainParentField: string;
                            aMainKeyField: string;
                            aMainNameField: string;

                            aMainHintField: string;
                            aMainBeginGroupField: string;
                            aMainBitmapField: string;
                            aMainOnClick: TNotifyEvent;
                            aMainLevelOrder:Boolean=False;

                            aDetailDataSet: TDataSet = nil;
                            aDetailParentField: string = '';
                            aDetailNameField: string = '';

                            aDetailHintField: string = '';
                            aDetailBeginGroupField: string = '';
                            aDetailBitmapField: string = '';
                            aDetailOnClick: TNotifyEvent = nil;

                            aParentItem:TMenuItem= nil;
                            aDoNotMainDetailData:Boolean=true;
                            aLinkObject:TObject=nil
                            );

//数据集输出到EXCEL(格式由aSFileNames决定)
//因为tempEXCELApp.ActiveSheet.Rows[aBeforeNRowInsert].Insert;速度太慢，
//所以使用Selection.Insert(Shift := xlDown)和剪切板来完美提高速度，非常快了
//aSFileNames=决定输出格式的源文件
//aDFileName=合并后的新文件名
//aDataset=输出数据的数据集
//aInsertDetailData=是否插入数据集每一行明细数据
//aBeforeNRowInsert=aInsertDetailData=TRUE时在哪行前进行插入明细数据
//aBeginCol=aInsertDetailData=TRUE时在哪列开始插入明细数据
//aFieldNameOrValueList=aInsertDetailData=TRUE时插入明细数据后开始填充数据的字段名或固定值
//aReplaceConstFieldName=格式中'字段名#记录号#'是否要自动替换成实际的字段值,如此值为True且数据集的数据又很多时则会很慢，应考虑用aInsertDetailData来代替
//aEndReplaceNames=在遍历数据集后需要全文件替换的标记（用","分隔）
//aEndReplaceValuesOrFieldNames=在遍历数据集后需要全文件替换的固定值或字段名（用","分隔）
//aOpenNew=结束后新文档是否要打开
function ABQueryOutExcel_OLE(
                         aSFileNames,
                         aDFileName:string;

                         aDataset:TDataSet;

                         aInsertDetailData:boolean;
                         aBeforeNRowInsert:LongInt;
                         aBeginCol:LongInt;
                         aFieldNameOrValueList:string;

                         aReplaceConstFieldName:boolean;

                         aEndReplaceNames,
                         aEndReplaceValuesOrFieldNames:string;

                         var aErrMsg:string;
                         aOpenNew:Boolean):boolean;
//数据集输出到Word(格式由aSFileNames决定)（OLE方式速度较慢）
//aSFileNames=决定输出格式的源文件
//aDFileName=合并后的新文件名
//aDataset=输出数据的数据集
//aInsertDetailData=是否插入数据集每一行明细数据
//aBeforeNRowInsert=aInsertDetailData=TRUE时在哪行前进行插入明细数据
//aBeginCol=aInsertDetailData=TRUE时在哪列开始插入明细数据
//aFieldNameOrValueList=aInsertDetailData=TRUE时插入明细数据后开始填充数据的字段名或固定值
//aReplaceConstFieldName=格式中'字段名#记录号#'是否要自动替换成实际的字段值
//aEndReplaceNames=在遍历数据集后需要全文件替换的标记（用","分隔）
//aEndReplaceValuesOrFieldNames=在遍历数据集后需要全文件替换的固定值或字段名（用","分隔）
//aOpenNew=结束后新文档是否要打开
function ABQueryOutWord_OLE(
                         aSFileNames,
                         aDFileName:string;

                         aDataset:TDataSet;

                         aInsertDetailData:boolean;
                         aBeforeNRowInsert:LongInt;
                         aBeginCol:LongInt;
                         aFieldNameOrValueList:string;

                         aReplaceConstFieldName:boolean;

                         aEndReplaceNames,
                         aEndReplaceValuesOrFieldNames:string;

                         var aErrMsg:string;
                         aOpenNew:Boolean):boolean;
//检测父集是否是数据集的父集
function ABCheckParentDataset(aParentDataset,aDataSet: TDataSet):Boolean;

implementation

function  ABCheckParentDataset(aParentDataset,aDataSet: TDataSet):Boolean;
begin
  result:=false;
  if (Assigned(aParentDataset)) and
     (Assigned(aDataSet)) and
     (Assigned(aDataSet.DataSource)) and
     (Assigned(aDataSet.DataSource.DataSet)) then
    Result:=(aDataSet.DataSource.DataSet=aParentDataset)   or
            (ABCheckParentDataset(aParentDataset,aDataSet.DataSource.DataSet));
end;

procedure ABCreateRadioGroupItems(aField:TField;
                                  aRadioGroup: TRadioGroup;
                                  aDefaultIndex: LongInt;
                                  aDefaultHeight: LongInt;
                                  aClearOld:Boolean);
var
  tempRows,tempMaxLen,tempColumns: LongInt;
  tempOldBeforeScroll:TDataSetNotifyEvent;
  tempOldAfterScroll:TDataSetNotifyEvent;
begin
  ABBackAndStopDatasetEvent(aField.Dataset,tempOldBeforeScroll,tempOldAfterScroll);
  aField.Dataset.DisableControls;
  aRadioGroup.Items.BeginUpdate;
  try
    if aClearOld then
      aRadioGroup.Items.Clear;

    //创建所有的通用查询项目
    tempMaxLen := 0;
    if ABDatasetIsEmpty(aField.Dataset) then
    begin
      aRadioGroup.Height:=0;
    end
    else
    begin
      aRadioGroup.Height:=aDefaultHeight;

      aField.Dataset.First;
      while not aField.Dataset.Eof do
      begin
        aRadioGroup.Items.Add(aField.AsString);
        tempMaxLen := max(tempMaxLen,Length(AnsiString(aField.AsString)));
        aField.Dataset.Next;
      end;

      //计算最佳的列数
      if tempMaxLen <> 0 then
      begin
        //选择框和前后空间大约占用5个字符
        tempMaxLen := (tempMaxLen * 7) +20;
        tempColumns:= ABCeil(aRadioGroup.Width / tempMaxLen);
        tempRows:=ABCeil(aField.Dataset.RecordCount /tempColumns);
        while (tempColumns-1<>0) and (ABCeil(aField.Dataset.RecordCount /(tempColumns-1))<=tempRows) do
        begin
          tempColumns:=tempColumns-1;
        end;
        aRadioGroup.Columns:=tempColumns;
        if tempColumns<>0 then
          aRadioGroup.Height:=max(35, ABCeil(aField.Dataset.RecordCount /tempColumns) * 15 + 20);
      end;
      if (aRadioGroup.Items.count-1>=aDefaultIndex) and (aDefaultIndex>=0) then
        aRadioGroup.ItemIndex:=aDefaultIndex;
    end;
  finally
    aRadioGroup.Items.EndUpdate;
    ABUnBackDatasetEvent(aField.Dataset,tempOldBeforeScroll,tempOldAfterScroll);
    aField.Dataset.EnableControls;
  end;
end;

procedure ABBlobFieldJpegToBitmap(aField:TField;aBitmap:TBitmap);
var
  JPG:Tjpegimage;
  tempMemoryStream:TMemoryStream;
begin
  if (Assigned(aField)) and (aField.IsBlob) and (not aField.IsNull) and
     (Assigned(aBitmap))  then
  begin
    tempMemoryStream:=TMemoryStream.Create;
    JPG:=TJpegImage.Create;
    try
      TBlobField(aField).SaveToStream(tempMemoryStream);
      JPG.LoadFromStream(tempMemoryStream);
      aBitmap.Assign(JPG);
    finally
      JPG.Free;
      tempMemoryStream.Free;
    end;
  end;
end;

procedure ABBlobFieldJpegToPicture(aField:TField;aPicture:TPicture);
var
  JPG:Tjpegimage;
  tempMemoryStream:TMemoryStream;
begin
  if (Assigned(aField)) and (aField.IsBlob) and (not aField.IsNull)  then
  begin
    JPG:=tjpegimage.Create;
    tempMemoryStream:=TMemoryStream.Create;
    try
      TBlobField(aField).SaveToStream(tempMemoryStream);
      JPG.LoadFromStream(tempMemoryStream);
      aPicture.Assign(JPG);
    finally
      JPG.Free;
      tempMemoryStream.Free;
    end;
  end;
end;

function ABQueryOutWord_OLE(
                         aSFileNames,
                         aDFileName:string;

                         aDataset:TDataSet;

                         aInsertDetailData:boolean;
                         aBeforeNRowInsert:LongInt;
                         aBeginCol:LongInt;
                         aFieldNameOrValueList:string;

                         aReplaceConstFieldName:boolean;

                         aEndReplaceNames,
                         aEndReplaceValuesOrFieldNames:string;

                         var aErrMsg:string;
                         aOpenNew:Boolean):boolean;
var
  tempApp,
  tempWordDocument: Variant;

  i,j:longint;

  tempReplaceFlag,
  tempReplaceValueOrFieldName,
  tempFieldNameOrValue:string;

  MatchCase, MatchWholeWord, MatchWildcards, MatchSoundsLike,
  MatchAllWordForms, aForward, Wrap, Format,  Replace:OleVariant;
  temTrue:OleVariant;
  tempOldBeforeScroll: TDataSetNotifyEvent;
  tempOldAfterScroll: TDataSetNotifyEvent;
begin
  Result := false;

  if (not ABCheckFileExists(aSFileNames)) or
     (trim(aDFileName)='')  then
    exit;

  MatchCase := True;       //是否区分大小写
  MatchWholeWord := false;  //是否全部一样
  MatchWildcards := False;  //使用通配符
  MatchSoundsLike := False; //同音
  MatchAllWordForms := false;//查找单词的各种形式
  aForward := false;         //向前
  Wrap := $00000001;   //找到是否继续
  Format := False;          //格式
  Replace := $00000002;          //

  ABDeleteFile(aDFileName);
  try
    tempApp := CreateOleObject('Word.Application');
    tempApp.DisplayAlerts := False;
    tempApp.Options.CheckGrammarAsYouType :=false;
    tempApp.Options.CheckSpellingAsYouType:=false;
    ABBackAndStopDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    aDataset.DisableControls;
    try
      tempWordDocument:=tempApp.Documents.Open(aSFileNames);
      if (aInsertDetailData) or
         (aReplaceConstFieldName) then
      begin
      {
        RangeStart := tempWordDocument.tables.item(1).cell(aBeforeNRowInsert, 1).Range.Start;
        RangeEnd := tempWordDocument.tables.item(1).cell(aBeforeNRowInsert, tempWordDocument.Tables.Item(1).Columns.Count).Range.end;
        tempWordDocument.Range(RangeStart, RangeEnd).Select; //选择行
        tempWordDocument.Range.Cells.Split(aDataset.RecordCount,tempWordDocument.Tables.Item(1).Columns.Count);
        }
        j:=0;
        {
        if (aInsertDetailData) or
           (aReplaceConstFieldName)  then
        begin
          tempStrings := TStringList.Create;
          try
            aDataset.First;
            while not aDataset.Eof do
            begin
              if aReplaceConstFieldName then
              begin
                //作数据集字段的替换
                j:=j+1;
                for i := 0 to aDataset.FieldCount-1 do
                begin
                  tempReplaceFlag := aDataset.Fields[i].FieldName+'#'+inttostr(j)+'#';
                  tempReplaceValueOrFieldName := aDataset.Fields[i].AsString;

                  tempApp.Cells.Replace(tempReplaceFlag, tempReplaceValueOrFieldName);
                end ;
              end;

              if aInsertDetailData then
              begin
                tempRowStr:=emptystr;
                for I := 1 to ABGetSpaceStrCount(aFieldNameOrValueList, ',') do
                begin
                  tempFieldNameOrValue := ABGetSpaceStr(aFieldNameOrValueList, i, ',');
                  if Assigned(aDataset.FindField(tempFieldNameOrValue)) then
                    tempFieldNameOrValue:=aDataset.FindField(tempFieldNameOrValue).AsString;

                  tempRowStr:=tempRowStr+tempFieldNameOrValue+#9;
                end;
                tempStrings.Add(tempRowStr);
              end;

              if aDataset.RecNo>= 65000 then
                Break;

              aDataset.Next;
            end;
            if aInsertDetailData then
            begin
              tempApp.ActiveSheet.Range['A'+inttostr(aBeforeNRowInsert),'IV'+inttostr(aBeforeNRowInsert+tempStrings.Count-1)].Select;
              tempApp.Selection.Insert(Shift := $FFFFEFE7);
              tempApp.ActiveSheet.Range[Chr(64+aBeginCol)+inttostr(aBeforeNRowInsert)].Select;
              Clipboard.AsText:=tempStrings.Text;
              tempApp.ActiveSheet.Paste;
            end;
          finally
            tempStrings.Free;
          end;
        end;
        }

        aDataset.First;
        while not aDataset.Eof do
        begin
          //作数据集表格的全部插入
          if aInsertDetailData then
          begin
            tempWordDocument.Tables.Item(1).rows.add(tempWordDocument.tables.item(1).rows.item(aBeforeNRowInsert));

            for I := 1 to ABGetSpaceStrCount(aFieldNameOrValueList, ',') do
            begin
              tempFieldNameOrValue := ABGetSpaceStr(aFieldNameOrValueList, i, ',');
              if Assigned(aDataset.FindField(tempFieldNameOrValue)) then
                tempFieldNameOrValue:=aDataset.FindField(tempFieldNameOrValue).AsString;

              tempWordDocument.Tables.Item(1).cell(aBeforeNRowInsert,aBeginCol+i-1).range.text:=tempFieldNameOrValue;
            end;
            aBeforeNRowInsert:=aBeforeNRowInsert+1;
          end;

          if aReplaceConstFieldName then
          begin
            //作数据集字段的替换
            j:=j+1;
            for i := 0 to aDataset.FieldCount-1 do
            begin
              if (not (aDataset.Fields[i].DataType in ABBlobDataType)) then
              begin
                tempReplaceFlag := aDataset.Fields[i].FieldName+'#'+inttostr(j)+'#';
                tempReplaceValueOrFieldName:=aDataset.Fields[i].AsString;

                tempWordDocument.Range.Find.Execute( tempReplaceFlag, MatchCase, MatchWholeWord,
                MatchWildcards, MatchSoundsLike, MatchAllWordForms, aForward,
                Wrap, Format, tempReplaceValueOrFieldName, Replace ,temTrue,temTrue,temTrue,temTrue);
              end;
            end ;
          end;

          aDataset.Next;
        end;
      end;

      //作最后的替换操作
      for I := 1 to ABGetSpaceStrCount(aEndReplaceNames, ',') do
      begin
        tempReplaceFlag := ABGetSpaceStr(aEndReplaceNames, i, ',');
        tempReplaceValueOrFieldName:=ABGetSpaceStr(aEndReplaceValuesOrFieldNames, i, ',');
        if Assigned(aDataset.FindField(tempReplaceValueOrFieldName)) then
          tempReplaceValueOrFieldName:=aDataset.FindField(tempReplaceValueOrFieldName).AsString;

        tempWordDocument.Range.Find.Execute( tempReplaceFlag, MatchCase, MatchWholeWord,
        MatchWildcards, MatchSoundsLike, MatchAllWordForms, aForward,
        Wrap, Format, tempReplaceValueOrFieldName, Replace ,temTrue,temTrue,temTrue,temTrue);
      end;

      tempWordDocument.Saveas(aDFileName);
      result:=true;
    finally
      aDataset.EnableControls;
      ABUnBackDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
      tempApp.DisplayAlerts := true;
      if (Result) and (aOpenNew) then
      begin
        tempApp.Visible := true;
      end
      else
      begin
        tempWordDocument.Close;
        tempApp.Quit;
      end;
    end;
  except
    on E: Exception do
    begin
      aErrMsg := E.Message;
    end;
  end;
end;

function ABQueryOutExcel_OLE(
                         aSFileNames,
                         aDFileName:string;

                         aDataset:TDataSet;

                         aInsertDetailData:boolean;
                         aBeforeNRowInsert:LongInt;
                         aBeginCol:LongInt;
                         aFieldNameOrValueList:string;

                         aReplaceConstFieldName:boolean;

                         aEndReplaceNames,
                         aEndReplaceValuesOrFieldNames:string;

                         var aErrMsg:string;
                         aOpenNew:Boolean):boolean;
var
  tempApp,
  tempExceBook: Variant;
  tempStrings: TStrings;

  tempRowStr: string;

  i,j:longint;

  tempReplaceFlag,
  tempReplaceValueOrFieldName,
  tempFieldNameOrValue:string;
  tempOldBeforeScroll: TDataSetNotifyEvent;
  tempOldAfterScroll: TDataSetNotifyEvent;
begin
  Result := false;

  if (not ABCheckFileExists(aSFileNames)) or
     (trim(aDFileName)='')  then
    exit;

  ABDeleteFile(aDFileName);
  try
    tempApp := CreateOleObject('Excel.Application');
    tempApp.DisplayAlerts := False;
    ABBackAndStopDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    aDataset.DisableControls;
    try
      tempExceBook:=tempApp.WorkBooks.Open(aSFileNames);
      if (aInsertDetailData) or
         (aReplaceConstFieldName) then
      begin
        j:=0;

        if (aInsertDetailData) or
           (aReplaceConstFieldName)  then
        begin
          tempStrings := TStringList.Create;
          try
            aDataset.First;
            while not aDataset.Eof do
            begin
              if aReplaceConstFieldName then
              begin
                //作数据集字段的替换
                j:=j+1;
                for i := 0 to aDataset.FieldCount-1 do
                begin
                  tempReplaceFlag := aDataset.Fields[i].FieldName+'#'+inttostr(j)+'#';
                  tempReplaceValueOrFieldName := aDataset.Fields[i].AsString;

                  tempApp.Cells.Replace(tempReplaceFlag, tempReplaceValueOrFieldName);
                end ;
              end;

              if aInsertDetailData then
              begin
                tempRowStr:=emptystr;
                for I := 1 to ABGetSpaceStrCount(aFieldNameOrValueList, ',') do
                begin
                  tempFieldNameOrValue := ABGetSpaceStr(aFieldNameOrValueList, i, ',');
                  if Assigned(aDataset.FindField(tempFieldNameOrValue)) then
                    tempFieldNameOrValue:=aDataset.FindField(tempFieldNameOrValue).AsString;

                  tempRowStr:=tempRowStr+tempFieldNameOrValue+#9;
                end;
                tempStrings.Add(tempRowStr);
              end;

              if aDataset.RecNo>= 65000 then
                Break;

              aDataset.Next;
            end;
            if aInsertDetailData then
            begin
              tempApp.ActiveSheet.Range['A'+inttostr(aBeforeNRowInsert),'IV'+inttostr(aBeforeNRowInsert+tempStrings.Count-1)].Select;
              tempApp.Selection.Insert(Shift := $FFFFEFE7);
              tempApp.ActiveSheet.Range[Chr(64+aBeginCol)+inttostr(aBeforeNRowInsert)].Select;
              Clipboard.AsText:=tempStrings.Text;
              tempApp.ActiveSheet.Paste;
            end;
          finally
            tempStrings.Free;
          end;
        end;
      end;
      //作最后的替换操作
      for I := 1 to ABGetSpaceStrCount(aEndReplaceNames, ',') do
      begin
        tempReplaceFlag := ABGetSpaceStr(aEndReplaceNames, i, ',');
        tempReplaceValueOrFieldName := ABGetSpaceStr(aEndReplaceValuesOrFieldNames, i, ',');
        if Assigned(aDataset.FindField(tempReplaceValueOrFieldName)) then
          tempReplaceValueOrFieldName:=aDataset.FindField(tempReplaceValueOrFieldName).AsString;

        tempApp.Cells.Replace(tempReplaceFlag, tempReplaceValueOrFieldName);
      end;

      tempExceBook.SaveAs(aDFileName);

      result:=true;
    finally
      aDataset.EnableControls;
      ABUnBackDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
      tempApp.DisplayAlerts := true;
      if (Result) and (aOpenNew) then
      begin
        tempApp.Visible := true;
      end
      else
      begin
        tempExceBook.Close;
        tempApp.Quit;
      end;
    end;
  except
    on E: Exception do
    begin
      aErrMsg := E.Message;
      raise;
    end;
  end;
end;

procedure ABCreateClientDataSet(
  aDataSet: TClientDataSet;
  aFieldNames: array of string;
  aFieldDisplayLabels: array of string;
  aFieldTypes: array of TDataType;
  aFieldsizes: array of LongInt
  );
var
  i: LongInt;
  tempFieldDef:TFieldDef;
begin
  for I := Low(aFieldNames) to High(aFieldNames) do
  begin
    tempFieldDef:= aDataSet.FieldDefs.AddFieldDef;
    tempFieldDef.Name := aFieldNames[i];
    tempFieldDef.DataType := aFieldTypes[i];
    if tempFieldDef.DataType = ftString then
      tempFieldDef.size := aFieldsizes[i];
  end;

  if aDataSet.FieldDefs.Count > 0 then
    aDataSet.CreateDataSet;

  for I := Low(aFieldDisplayLabels) to High(aFieldDisplayLabels) do
  begin
    aDataSet.FindField(aFieldNames[i]).DisplayLabel:=aFieldDisplayLabels[i];
  end;
end;

function ABCreateClientDataSet(
  aFieldNames: array of string;
  aFieldDisplayLabels: array of string;
  aFieldTypes: array of TDataType;
  aFieldsizes: array of LongInt
  ): TClientDataSet;
begin
  Result := TClientDataSet.Create(nil);
  ABCreateClientDataSet(Result,aFieldNames,aFieldDisplayLabels,aFieldTypes,aFieldsizes);
end;

function ABIsDataAware(aObject:TObject):boolean;
begin
  result :=(ABIsPublishProp(aObject, 'DataSource')) or
           (ABIsPublishProp(aObject, 'DataSet')) or
           (ABIsPublishProp(aObject, 'ABDataSource'))  or
           (ABIsPublishProp(aObject, 'ABDataSet'))  or
           (ABIsPublishProp(aObject, 'DataController.DataSource'))  or
           (ABIsPublishProp(aObject, 'DataController.DataSet'))  or
           (ABIsPublishProp(aObject, 'Databinding.DataSource')) or
           (ABIsPublishProp(aObject, 'Databinding.DataSet'));
end;

function ABSetControlFocusByField(aField: TField): Boolean;
begin
  result:=False;
  if Assigned(aField) then
  begin
    try
      aField.FocusControl;
      result:=true;
    except
      result:=False;
    end;
  end;
end;

procedure ABDeleteDataset(aDataset:TDataset);
begin
  aDataset.DisableControls;
  try
    aDataset.first;
    while not aDataset.Eof do
    begin
      aDataset.Delete;
    end;
  finally
    aDataset.EnableControls;
  end;
end;

procedure ABBackAndStopFieldEvent(aField:TField;
                           var aOnGetText: TFieldGetTextEvent;var aOnSetText: TFieldSetTextEvent;
                           var aOnValidate: TFieldNotifyEvent;var aOnChange: TFieldNotifyEvent
                           );
begin
  try
    aOnGetText :=aField.OnGetText  ;
    aOnSetText :=aField.OnSetText  ;
    aOnValidate:=aField.OnValidate ;
    aOnChange  :=aField.OnChange   ;
  finally
    aField.OnGetText  :=nil;
    aField.OnSetText  :=nil;
    aField.OnValidate :=nil;
    aField.OnChange   :=nil;
  end;
end;


function ABReplaceSqlFieldNameParams(aSQL: string; aDatasets:array of TDataset): string;
  function DoDataset(aBegFlag,aEndFlag:string;aDataset:TDataset): boolean;
  var
    i,j: longint;
    tempCurFieldName, tempCurFieldValue: string;
  begin
    result:=false;

    if Assigned(aDataset) then
    begin
      for i := 0 to aDataset.FieldCount - 1 do
      begin
        tempCurFieldName := aDataset.Fields[i].FieldName;
        j:=ABPos(aBegFlag + tempCurFieldName+aEndFlag, aSQL);
        if (j> 0) then
        begin
          if (aDataset.Fields[i].DataType in ABStrAndMemoDataType) then
          begin
            if aDataset.Fields[i].IsNull then
              tempCurFieldValue :=QuotedStr(EmptyStr)
            else
              tempCurFieldValue := QuotedStr(aDataset.Fields[i].AsString);
          end
          else if (aDataset.Fields[i].DataType in ABNumDataType) then
          begin
            if aDataset.Fields[i].IsNull then
              tempCurFieldValue :='0'
            else
              tempCurFieldValue := aDataset.Fields[i].AsString;
          end
          else
          begin
            if aDataset.Fields[i].IsNull then
              tempCurFieldValue :=EmptyStr
            else
              tempCurFieldValue := aDataset.Fields[i].AsString;
          end;

          aSQL := ABStringReplace(aSQL, aBegFlag + tempCurFieldName+aEndFlag, tempCurFieldValue);

          result:=true;
          if ABPos(aBegFlag, aSQL)<=0 then
            break;
        end;
      end;

      if (ABPos(aBegFlag, aSQL)>0) and
         (Assigned(aDataset.DataSource)) then
      begin
        result:=(result) or (DoDataset(aBegFlag,aEndFlag,aDataset.DataSource.DataSet));
      end;
    end;
  end;
  procedure DoDatasets(aBegFlag,aEndFlag:string);
  var
    i: longint;
  begin
    if ABPos(aBegFlag, aSQL)>0 then
    begin
      for i := Low(aDatasets) to High(aDatasets) do
      begin
        if DoDataset(aBegFlag,aEndFlag,aDatasets[i]) then
        begin
          if ABPos(aBegFlag, aSQL)<=0 then
            break;
        end;
      end;
    end;
  end;
begin
  aSQL:=Trim(aSQL);
  //为了防止引用的字段名中包含引用的字段名，优先用"[:字段名]"
  DoDatasets('[:',']');
  //兼容以前的老格式":字段名"
  DoDatasets(':','');

  Result := aSQL;
end;

function ABGetSQLByTreeNode(aTreeNode: TTreeNode;aDataset:TDataset;
                           aTableName:string;

                           aKeyFieldnames:array of string;
                           aUpdateFieldnames:array of string;
                           aInsertFieldnames:array of string;

                           aFieldnameReplaceSQLs:TStrings;
                           aFieldnameReplaceDatasets:array of TDataSet;
                           aGetClient:Boolean):string;
var
  j: LongInt;
begin
  Result:=emptystr;
  if (Assigned(aTreeNode.Data)) and
     (not ABPointerIsNull(aTreeNode.Data)) and
     (ABSetDatasetRecno(aDataSet,LongInt(aTreeNode.Data))>0) then
  begin
    ABAddstr(result,ABGetSQLByDataset(aDataset,
                                      aTableName,

                                      aKeyFieldnames,
                                      aUpdateFieldnames,
                                      aInsertFieldnames,

                                      aFieldnameReplaceSQLs,
                                      aFieldnameReplaceDatasets),ABEnterWrapStr+'go'+ABEnterWrapStr);
  end;

  if aGetClient then
  begin
    for j := 0 to aTreeNode.Count-1 do
    begin
      ABAddstr(result,ABGetSQLByTreeNode(aTreeNode.Item[j],
                                        aDataset,
                                        aTableName,

                                        aKeyFieldnames,
                                        aUpdateFieldnames,
                                        aInsertFieldnames,

                                        aFieldnameReplaceSQLs,
                                        aFieldnameReplaceDatasets),ABEnterWrapStr+'go'+ABEnterWrapStr);
    end;
  end;
end;

function ABGetSQLByTreeView(aTreeView: TTreeView;aDataset:TDataset;
                           aTableName:string;

                           aKeyFieldnames:array of string;
                           aUpdateFieldnames:array of string;
                           aInsertFieldnames:array of string;

                           aFieldnameReplaceSQLs:TStrings;
                           aFieldnameReplaceDatasets:array of TDataSet):string;
var
  i: LongInt;
begin
  result:=emptystr;
  for I := 0 to aTreeView.Items.Count-1 do
  begin
    if (aTreeView.Items[i].Level=0)  then
      ABAddstr(result,ABGetSQLByTreeNode(aTreeView.Items[i],
                                        aDataset,
                                        aTableName,

                                        aKeyFieldnames,
                                        aUpdateFieldnames,
                                        aInsertFieldnames,

                                        aFieldnameReplaceSQLs,
                                        aFieldnameReplaceDatasets,
                                        true),ABEnterWrapStr+'go'+ABEnterWrapStr);
  end;
end;

function ABGetSQLByDataset(aDataset:TDataSet;
                           aTableName:string;

                           aKeyFieldnames:array of string;
                           aUpdateFieldnames:array of string;
                           aInsertFieldnames:array of string;

                           aFieldnameReplaceSQLs:TStrings;
                           aFieldnameReplaceDatasets:array of TDataSet;

                           aAllRecord:Boolean=false):string;
  function ABGetSQLByDatasetCurRecord:string;
  var
    i:LongInt;
    tempCurFieldname,tempCurReplaceFieldStrinValue:string;

    tempStr1,tempSumWhere,tempSumUpdate,tempSumInsertFieldNames,tempSumInsertFieldValues,
    tempLocalFieldName:string;
    tempLocate:Boolean;
    tempDeclareStr:string;
    function LocalReplaceFieldStringsParentDatasets:string;
    var
      k:LongInt;
    begin
      tempLocate:=false;
      for k := Low(aFieldnameReplaceDatasets) to high(aFieldnameReplaceDatasets) do
      begin
        if (Assigned(aFieldnameReplaceDatasets[k].FindField(tempLocalFieldName))) and
           (
             (AnsiCompareText(aFieldnameReplaceDatasets[k].FindField(tempLocalFieldName).AsString,
                              aDataset.FieldByName(tempCurFieldname).AsString)=0) or
             (aFieldnameReplaceDatasets[k].Locate(tempLocalFieldName,
                                                     aDataset.FieldByName(tempCurFieldname).AsString,
                                                          []))
            ) then
        begin
          tempLocate:=true;
          break;
        end;
      end;
    end;
  begin
    result:= EmptyStr;
    tempDeclareStr:= EmptyStr;
    tempSumWhere:=EmptyStr;
    tempSumUpdate:=EmptyStr;
    tempSumInsertFieldNames:=EmptyStr;
    tempSumInsertFieldValues:=EmptyStr;

    //取记录Where
    for I := Low(aKeyFieldnames) to high(aKeyFieldnames) do
    begin
      tempCurFieldname:=aKeyFieldnames[i];
      if (Assigned(aFieldnameReplaceSQLs)) and (aFieldnameReplaceSQLs.IndexOfName(tempCurFieldname)>=0) then
      begin
        if abpos('declare @'+tempCurFieldname+inttostr(aDataset.RecNo),tempDeclareStr)<=0 then
        begin
          tempCurReplaceFieldStrinValue:=aFieldnameReplaceSQLs.Values[tempCurFieldname];
          tempLocalFieldName:=Trim(ABGetLeftRightStr(tempCurReplaceFieldStrinValue,axdLeft));
          LocalReplaceFieldStringsParentDatasets;
          if tempLocate then
          begin
            abaddstr(tempDeclareStr,'declare @'+tempCurFieldname+inttostr(aDataset.RecNo)+' Nvarchar(100)' ,ABEnterWrapStr);
            abaddstr(tempDeclareStr,'set @'+tempCurFieldname+inttostr(aDataset.RecNo)+'='+
                                                 ABReplaceSqlFieldNameParams(ABGetLeftRightStr(tempCurReplaceFieldStrinValue,axdright),
                                                 aFieldnameReplaceDatasets) ,ABEnterWrapStr);
            tempStr1:='@'+tempCurFieldname+inttostr(aDataset.RecNo);
          end
          else
          begin
            Continue;
          end;
        end
        else
        begin
          tempStr1:='@'+tempCurFieldname+inttostr(aDataset.RecNo);
        end;
      end
      else
      begin
        if Assigned(aDataset.FindField(aKeyFieldnames[i])) then
          tempStr1:=ABGetSQLFieldValue(aDataset.FieldByName(aKeyFieldnames[i]))
        else
          Continue;
      end;

      if AnsiCompareText(tempStr1,'null')=0 then
      begin
        abaddstr(tempSumWhere,aKeyFieldnames[i]+' is '+tempStr1 ,' and ');
      end
      else
      begin
        abaddstr(tempSumWhere,aKeyFieldnames[i]+' = '+tempStr1 ,' and ');
      end;
    end;

    //取记录Update
    for I := Low(aUpdateFieldnames) to high(aUpdateFieldnames) do
    begin
      tempCurFieldname:=aUpdateFieldnames[i];
      if ABStrInArray(tempCurFieldname,aKeyFieldnames)<0 then
      begin
        if (Assigned(aFieldnameReplaceSQLs)) and (aFieldnameReplaceSQLs.IndexOfName(aUpdateFieldnames[i])>=0) then
        begin
          if abpos('declare @'+tempCurFieldname+inttostr(aDataset.RecNo),tempDeclareStr)<=0 then
          begin
            tempCurReplaceFieldStrinValue:=aFieldnameReplaceSQLs.Values[tempCurFieldname];
            tempLocalFieldName:=Trim(ABGetLeftRightStr(tempCurReplaceFieldStrinValue,axdLeft));
            LocalReplaceFieldStringsParentDatasets;
            if tempLocate then
            begin
              abaddstr(tempDeclareStr,'declare @'+tempCurFieldname+inttostr(aDataset.RecNo)+' Nvarchar(100)' ,ABEnterWrapStr);
              abaddstr(tempDeclareStr,'set @'+tempCurFieldname+inttostr(aDataset.RecNo)+'='+
                                                   ABReplaceSqlFieldNameParams(ABGetLeftRightStr(tempCurReplaceFieldStrinValue,axdright),
                                                   aFieldnameReplaceDatasets) ,ABEnterWrapStr);
              tempStr1:='@'+tempCurFieldname+inttostr(aDataset.RecNo);
            end
            else
            begin
              Continue;
            end;
          end
          else
          begin
            tempStr1:='@'+tempCurFieldname+inttostr(aDataset.RecNo);
          end;
        end
        else
        begin
          if Assigned(aDataset.FindField(aUpdateFieldnames[i])) then
            tempStr1:=ABGetSQLFieldValue(aDataset.FieldByName(aUpdateFieldnames[i]))
          else
            Continue;
        end;

        abaddstr(tempSumUpdate,aUpdateFieldnames[i]+' = '+tempStr1,' , ');
      end;
    end;

    //取记录Insert
    for I := Low(aInsertFieldnames) to high(aInsertFieldnames) do
    begin
      tempCurFieldname:=aInsertFieldnames[i];
      if (Assigned(aFieldnameReplaceSQLs)) and (aFieldnameReplaceSQLs.IndexOfName(aInsertFieldnames[i])>=0) then
      begin
        tempCurReplaceFieldStrinValue:=aFieldnameReplaceSQLs.Values[tempCurFieldname];
        tempLocalFieldName:=Trim(ABGetLeftRightStr(tempCurReplaceFieldStrinValue,axdLeft));
        LocalReplaceFieldStringsParentDatasets;
        if abpos('declare @'+tempCurFieldname+inttostr(aDataset.RecNo),tempDeclareStr)<=0 then
        begin
          if tempLocate then
          begin
            abaddstr(tempDeclareStr,'declare @'+tempCurFieldname+inttostr(aDataset.RecNo)+' Nvarchar(100)' ,ABEnterWrapStr);
            abaddstr(tempDeclareStr,'set @'+tempCurFieldname+inttostr(aDataset.RecNo)+'='+
                                                 ABReplaceSqlFieldNameParams(ABGetLeftRightStr(tempCurReplaceFieldStrinValue,axdright),
                                                 aFieldnameReplaceDatasets) ,ABEnterWrapStr);
            tempStr1:='@'+tempCurFieldname+inttostr(aDataset.RecNo);
          end
          else
          begin
            Continue;
          end;
        end
        else
        begin
          tempStr1:='@'+tempCurFieldname+inttostr(aDataset.RecNo);
        end;
      end
      else
      begin
        if Assigned(aDataset.FindField(aInsertFieldnames[i])) then
          tempStr1:=ABGetSQLFieldValue(aDataset.FieldByName(aInsertFieldnames[i]))
        else
          Continue;
      end;

      abaddstr(tempSumInsertFieldNames,aInsertFieldnames[i],' , ');
      abaddstr(tempSumInsertFieldValues,tempStr1,' , ');
    end;

    //将当前记录组合成SQL
    if (tempSumUpdate<>EmptyStr) and (tempSumInsertFieldNames<>EmptyStr) then
    begin
      abaddstr(result,tempDeclareStr,ABEnterWrapStr);
      abaddstr(result,'if exists (select * from '+aTableName+' where '+tempSumWhere+')',ABEnterWrapStr);
      abaddstr(result,'  update '+aTableName+' set '+tempSumUpdate+'  where '+tempSumWhere ,ABEnterWrapStr);
      abaddstr(result,'else ' ,ABEnterWrapStr);
      abaddstr(result,'  insert into '+aTableName+'('+tempSumInsertFieldNames+') values('+tempSumInsertFieldValues+')',ABEnterWrapStr);
    end
    else
    begin
      abaddstr(result,tempDeclareStr,ABEnterWrapStr);
      if (tempSumUpdate<>EmptyStr) then
        abaddstr(result,'  update '+aTableName+' set '+tempSumUpdate+'  where '+tempSumWhere ,ABEnterWrapStr)
      else  if (tempSumInsertFieldNames<>EmptyStr) then
        abaddstr(result,'insert into '+aTableName+'('+tempSumInsertFieldNames+') values('+tempSumInsertFieldValues+')',ABEnterWrapStr);
    end;
  end;
var
  tempOldBeforeScroll: TDataSetNotifyEvent;
  tempOldAfterScroll: TDataSetNotifyEvent;
begin
  result:= EmptyStr;
  if aAllRecord then
  begin
    ABBackAndStopDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    aDataset.DisableControls;
    try
      aDataset.First;
      while not aDataset.Eof do
      begin
        abaddstr(result,ABGetSQLByDatasetCurRecord,ABEnterWrapStr+'go'+ABEnterWrapStr);
        aDataset.Next;
      end;
    finally
      aDataset.EnableControls;
      ABUnBackDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    end;
  end
  else
  begin
    abaddstr(result,ABGetSQLByDatasetCurRecord,ABEnterWrapStr+'go'+ABEnterWrapStr);
  end;
end;

function ABGetSQLFieldValue(aField:TField):string;
begin
  result:='';
  if aField.IsNull then
  begin
    result:='null';
    exit;
  end;

  if (aField.DataType=ftString) or
     (aField.DataType=ftMemo) or
     (aField.DataType=ftFmtMemo) or
     (aField.DataType=ftFixedChar) or
     (aField.DataType=ftWideString) or
     {$IF (ABDelphiVersion_int>=185)}(aField.DataType=ftFixedWideChar) or (aField.DataType=ftWideMemo) or {$IFEND}
     (aField.DataType=ftGuid)
      then
  begin
    Result:=QuotedStr(aField.AsString);
  end
  else if  (aField.DataType=ftSmallint) or
           (aField.DataType=ftInteger) or
           (aField.DataType=ftWord) or
           (aField.DataType=ftLargeint)
           {$IF (ABDelphiVersion_int>=200)} or (aField.DataType=ftLongWord) or (aField.DataType=ftShortint) {$IFEND}
             then
  begin
    Result:= IntToStr(aField.AsInteger) ;
  end
  else if  (aField.DataType=ftFloat) or
           (aField.DataType=ftCurrency) or
           (aField.DataType=ftBCD) or
           (aField.DataType=ftFMTBcd)
           {$IF (ABDelphiVersion_int>=200)} or (aField.DataType=DB.ftExtended) {$IFEND}
           then
  begin
    Result:= FloatToStr(aField.AsFloat) ;
  end
  else if  (aField.DataType=ftBoolean)   then
  begin
    Result:= IntToStr(ABBoolToInt(aField.AsBoolean));
  end
  else if  (aField.DataType=ftDate)   then
  begin
    Result:= QuotedStr(ABDateToStr(aField.AsDateTime)) ;
  end
  else if  (aField.DataType=ftTime)    then
  begin
    Result:= QuotedStr(ABTimeToStr(aField.AsDateTime)) ;
  end
  else if   (aField.DataType=ftDateTime) or
            (aField.DataType=ftTimeStamp)     then
  begin
    Result:= QuotedStr(ABDateTimeToStr(aField.AsDateTime,23)) ;
  end
  else
  begin
    Result:= QuotedStr(aField.AsString) ;
  end
end;

function ABReplaceSqlExtParams(aSQL: string): string;
var
  i:LongInt;
  tempPosBeginIndex,
  tempSubStrEndIndex, tempSubStrBegIndex: Integer;
  tempSubItemStr, tempSubStr, tempValue: string;
  tempStr1: string;
  tempDo : boolean;
begin
  Result := trim(aSQL);
  if (abpos('[', Result) > 0) and
     (abpos(']', Result) > 0) then
  begin
    //替换SQL中的操作员属性
    tempPosBeginIndex := 1;
    tempSubStrBegIndex := abpos('[', Result);
    while tempSubStrBegIndex > 0 do
    begin
      tempSubStrEndIndex := ABPos(']', Result, tempPosBeginIndex);
      if tempSubStrEndIndex <= 0 then
        break;

      tempSubItemStr := Copy(Result, tempSubStrBegIndex, tempSubStrEndIndex - tempSubStrBegIndex +length(']'));
      tempSubStr := tempSubItemStr;
      tempSubStr := ABStringReplace(tempSubStr, '[', EmptyStr);
      tempSubStr := Trim(ABStringReplace(tempSubStr, ']', EmptyStr));

      tempValue := 'Null';

      if abpos('ABUser_',tempSubStr)>0 then
      begin
        tempStr1:=emptystr;

             if AnsiCompareText(tempSubStr, 'ABUser_Guid'                  )=0 then tempStr1:=ABPubUser.Guid
        else if AnsiCompareText(tempSubStr, 'ABUser_Code'                  )=0 then tempStr1:=ABPubUser.Code
        else if AnsiCompareText(tempSubStr, 'ABUser_Name'                  )=0 then tempStr1:=ABPubUser.Name
        else if AnsiCompareText(tempSubStr, 'ABUser_PassWord'              )=0 then tempStr1:=ABPubUser.PassWord
        else if AnsiCompareText(tempSubStr, 'ABUser_EncryptPassWord'       )=0 then tempStr1:=ABPubUser.EncryptPassWord
        else if AnsiCompareText(tempSubStr, 'ABUser_FuncRight'             )=0 then tempStr1:=ABPubUser.FuncRight
        else if AnsiCompareText(tempSubStr, 'ABUser_CustomFunc'            )=0 then tempStr1:=ABPubUser.CustomFunc
        else if AnsiCompareText(tempSubStr, 'ABUser_UseBeginDateTime'      )=0 then tempStr1:=ABDateTimeToStr(ABPubUser.UseBeginDateTime)
        else if AnsiCompareText(tempSubStr, 'ABUser_UseEndDateTime'        )=0 then tempStr1:=ABDateTimeToStr(ABPubUser.UseEndDateTime)
        else if AnsiCompareText(tempSubStr, 'ABUser_LicenseBeginDateTime'  )=0 then tempStr1:=ABDateTimeToStr(ABPubUser.LicenseBeginDateTime)
        else if AnsiCompareText(tempSubStr, 'ABUser_LicenseEndDateTime'    )=0 then tempStr1:=ABDateTimeToStr(ABPubUser.LicenseEndDateTime)
        else if AnsiCompareText(tempSubStr, 'ABUser_PassWordEndDateTime'   )=0 then tempStr1:=ABDateTimeToStr(ABPubUser.PassWordEndDateTime)
        else if AnsiCompareText(tempSubStr, 'ABUser_LoginDateTime'         )=0 then tempStr1:=ABDateTimeToStr(ABPubUser.LoginDateTime)
        else if AnsiCompareText(tempSubStr, 'ABUser_LoginOutDateTime'      )=0 then tempStr1:=ABDateTimeToStr(ABPubUser.LoginOutDateTime)
        else if AnsiCompareText(tempSubStr, 'ABUser_SysLanguage'           )=0 then tempStr1:=ABGetSysLanguageStr(ABGetSysLanguage)
        else if AnsiCompareText(tempSubStr, 'ABUser_SysVersion'            )=0 then tempStr1:=ABGetSysVersion
        else if AnsiCompareText(tempSubStr, 'ABUser_HostName'              )=0 then tempStr1:=ABPubUser.HostName
        else if AnsiCompareText(tempSubStr, 'ABUser_HostIP'                )=0 then tempStr1:=ABPubUser.HostIP
        else if AnsiCompareText(tempSubStr, 'ABUser_MacAddress'            )=0 then tempStr1:=ABPubUser.MacAddress
        else if AnsiCompareText(tempSubStr, 'ABUser_BroadIP'               )=0 then tempStr1:=ABPubUser.BroadIP
        else if AnsiCompareText(tempSubStr, 'ABUser_LoginType'             )=0 then tempStr1:=GetEnumName(TypeInfo(TABLoginType), Ord(ABLocalParams.LoginType))
        else if AnsiCompareText(tempSubStr, 'ABUser_VarGuid'               )=0 then tempStr1:=ABPubUser.VarGuid
        else if AnsiCompareText(tempSubStr, 'ABUser_VarDatetime'           )=0 then tempStr1:=ABDateTimeToStr(ABPubUser.VarDatetime);

        Result := ABStringReplace(Result, tempSubItemStr, tempStr1);
      end
      else
      begin
        tempPosBeginIndex := tempSubStrEndIndex + length(']');
      end;

      tempSubStrBegIndex := abpos('[', Result, tempPosBeginIndex);
    end;

    if (abpos('[', Result) > 0) and
       (abpos(']', Result) > 0) then
    begin
      //替换SQL中的操作员扩展属性
      if (ABPubReplaceSQLStrings.Count>0) then
      begin
        for I := 0 to ABPubReplaceSQLStrings.Count-1 do
        begin
          tempPosBeginIndex:=ABPos(ABPubReplaceSQLStrings.Names[i], Result);
          Result:=ABStringReplace(Result,ABPubReplaceSQLStrings.Names[i],ABPubReplaceSQLStrings.ValueFromIndex[i],tempPosBeginIndex);
        end;
      end;

      if (abpos('[', Result) > 0) and
         (abpos(']', Result) > 0) then
      begin
        //替换SQL中的子SQL
        if (Assigned(ABPubReplaceSQLDataset)) and
           (ABPubReplaceSQLNameFieldName<>emptystr) and
           (ABPubReplaceSQLValueFieldName<>emptystr) then
        begin
          tempPosBeginIndex := 1;
          tempSubStrBegIndex := abpos('[',Result);
          while tempSubStrBegIndex > 0 do
          begin
            tempSubStrEndIndex := ABPos(']', Result, tempPosBeginIndex);
            if tempSubStrEndIndex <= 0 then
              break;

            tempSubItemStr := Copy(Result, tempSubStrBegIndex, tempSubStrEndIndex - tempSubStrBegIndex +length(']'));
            tempSubStr := tempSubItemStr;
            tempSubStr := ABStringReplace(tempSubStr, '[', EmptyStr);
            tempSubStr := Trim(ABStringReplace(tempSubStr, ']', EmptyStr));

            tempDo := false;
            tempValue := EmptyStr;
            if (abpos('ABPublicSQL_',tempSubStr)>0) then
            begin
              tempStr1 := ABStringReplace(tempSubStr, 'ABPublicSQL_', EmptyStr);

              if ABPubReplaceSQLDataset.Locate(ABPubReplaceSQLNameFieldName, tempStr1, []) then
              begin
                tempValue := ABPubReplaceSQLDataset.FindField(ABPubReplaceSQLValueFieldName).AsString;
              end;
              tempDo := true;
            end;

            if tempDo then
            begin
              Result := ABStringReplace(Result, tempSubItemStr, tempValue);
            end
            else
              tempPosBeginIndex := tempSubStrEndIndex + length(']');

            tempSubStrBegIndex := abpos('[', Result, tempPosBeginIndex);
          end;
        end;

        if (abpos('[', Result) > 0) and
           (abpos(']', Result) > 0) then
        begin
          //替换SQL中的参数数据集
          if (Assigned(ABPubParamDataset)) and
             (ABPubParamNameFieldName<>emptystr) and
             (ABPubParamValueFieldName<>emptystr) then
          begin
            tempPosBeginIndex := 1;
            tempSubStrBegIndex := abpos('[', Result);
            while tempSubStrBegIndex > 0 do
            begin
              tempSubStrEndIndex := ABPos(']', Result, tempPosBeginIndex);
              if tempSubStrEndIndex <= 0 then
                break;

              tempSubItemStr := Copy(Result, tempSubStrBegIndex, tempSubStrEndIndex - tempSubStrBegIndex +length(']'));
              tempSubStr := tempSubItemStr;
              tempSubStr := ABStringReplace(tempSubStr, '[', EmptyStr);
              tempSubStr := Trim(ABStringReplace(tempSubStr, ']', EmptyStr));

              tempValue := 'Null';

              //替换SQL中的参数
              if  (abpos('ABParams_',tempSubStr)>0) then
              begin
                tempStr1 := ABStringReplace(tempSubStr, 'ABParams_', EmptyStr);

                if ABPubParamDataset.Locate(ABPubParamNameFieldName, tempStr1, [loCaseInsensitive]) then
                begin
                  Result := ABStringReplace(Result, tempSubItemStr, ABPubParamDataset.FindField(ABPubParamValueFieldName).AsString);
                end;
              end
              else
              begin
                tempPosBeginIndex := tempSubStrEndIndex + length(']');
              end;

              tempSubStrBegIndex := abpos('[', Result, tempPosBeginIndex);
            end;
          end;
        end;
      end;
    end;
  end;
end;

function ABStrsToSql(aStrs: string; aSpaceSign: string;aQuoted:boolean; aAddBegFieldValue: string;aAddEndFieldValue: string): string;
var
  i: longint;
  tempstr2: string;
begin
  result := EmptyStr;

  for I := 1 to ABGetSpaceStrCount(aStrs, aSpaceSign) do
  begin
    tempstr2 :=  ABGetSpaceStr(aStrs, i, aSpaceSign) ;
    if aQuoted then
      tempstr2 :=QuotedStr(tempstr2);
    tempstr2 := ' select ' + aAddBegFieldValue + tempstr2 + aAddEndFieldValue;

    ABAddstr(Result,tempstr2,' union ');
  end;
end;

function ABFieldNamesAndFieldValuesToWhere(
                        aDataset:TDataSet;
                        aFieldNames: array of string;
                        aFieldValueQuoteds: array of Boolean;
                        aFieldNameAndFieldValueCompareStrs: array of string;
                        aLinkAndOr:string): string;
var
  tempFieldValues :array of string;
  i:LongInt;
begin
  SetLength(tempFieldValues,High(aFieldNames)-low(aFieldNames)+1);
  for I := low(aFieldNames) to High(aFieldNames) do
  begin
    tempFieldValues[i]:= aDataset.FieldByName(aFieldNames[i]).AsString;
  end;
    
  result:= ABFieldNamesAndFieldValuesToWhere( aFieldNames,
                                              tempFieldValues,
                                              aFieldValueQuoteds,
                                              aFieldNameAndFieldValueCompareStrs,
                                              aLinkAndOr);
end;

function ABFieldNamesAndFieldValuesToWhere(aFieldNames: array of string; aFieldValues: array of string;
                        aFieldValueQuoteds: array of Boolean;
                        aFieldNameAndFieldValueCompareStrs: array of string;
                        aLinkAndOr:string ): string;
var
  tempWhere,
  tempCompareStr: string;
  tempIsQuoted:Boolean;
  i: LongInt;
begin
  Result := EmptyStr;
  for I := Low(aFieldNames) to High(aFieldNames) do
  begin
    tempIsQuoted:=True;
    if i<=High(aFieldValueQuoteds) then
      tempIsQuoted:=aFieldValueQuoteds[i];
    tempCompareStr:='=';
    if i<=High(aFieldNameAndFieldValueCompareStrs) then
      tempCompareStr:=aFieldNameAndFieldValueCompareStrs[i];

    if tempIsQuoted then
      tempWhere := QuotedStr(aFieldValues[i])
    else
      tempWhere := aFieldValues[i];

    ABAddstr(Result,aFieldNames[i] + tempCompareStr + tempWhere,' ' + aLinkAndOr + ' ');
  end;

  if Result <> EmptyStr then
  begin
    Result := '(' + Result + ')';
  end;
end;

function ABFieldNamesAndFieldValueToWhere(
                        aFieldNames: array of string;
                        aFieldValue: string;aFieldValueQuoted:Boolean;
                        aFieldNameAndFieldValueCompareStrs: array of string;
                        aLinkAndOr:string): string;
var
  tempCompareStr: string;
  i: LongInt;
begin
  Result := EmptyStr;
  if aFieldValueQuoted then
    aFieldValue := QuotedStr(aFieldValue)
  else
    aFieldValue := aFieldValue;

  for I := Low(aFieldNames) to High(aFieldNames) do
  begin
    tempCompareStr:='=';
    if i<=High(aFieldNameAndFieldValueCompareStrs) then
      tempCompareStr:=aFieldNameAndFieldValueCompareStrs[i];

    ABAddstr(Result,aFieldNames[i] + tempCompareStr + aFieldValue,' ' + aLinkAndOr + ' ');
  end;

  if Result <> EmptyStr then
  begin
    Result := '(' + Result + ')';
  end;
end;

function ABFieldNameAndFieldValuesToWhere(
                        aFieldName: string;
                        aFieldValues:array of string;aFieldValueQuoted:Boolean;
                        aFieldNameAndFieldValueCompareStrs: array of string;
                        aLinkAndOr:string): string;
var
  tempCompareStr,
  tempWhere: string;
  i: LongInt;
begin
  Result := EmptyStr;
  for I := Low(aFieldValues) to High(aFieldValues) do
  begin
    tempCompareStr:='=';
    if i<=High(aFieldNameAndFieldValueCompareStrs) then
      tempCompareStr:=aFieldNameAndFieldValueCompareStrs[i];

    if aFieldValueQuoted then
      tempWhere := QuotedStr(aFieldValues[i])
    else
      tempWhere := aFieldValues[i];

    ABAddstr(Result,aFieldName + tempCompareStr + tempWhere,' ' + aLinkAndOr + ' ');
  end;

  if Result <> EmptyStr then
  begin
    Result := '(' + Result + ')';
  end;
end;

function ABIsOkSQL(aSQL: string): Boolean;
begin
  Result := (
    (ABPos('select ', aSQL) > 0) or
    (ABPos('update ', aSQL) > 0) or
    (ABPos('insert ', aSQL) > 0) or
    (ABPos('exec ', aSQL) > 0)
    ) and
    (Length(trim(ABStringReplace(aSQL,[ABEnterWrapStr],''))) > 0);
end;

function ABIsSelectSQL(aSQL: string): Boolean;
begin
  Result := false;
  aSQL:=Trim(aSQL);
  if (AnsiCompareText(Copy(aSQL, 1, 6), 'update') <> 0) and
     (AnsiCompareText(Copy(aSQL, 1, 6), 'insert') <> 0) and
     (ABPos('insert into', aSQL) <= 0) and
     (ABPos('select', aSQL) > 0) and
     (ABPos('into', aSQL) <= 0) then
    Result := true;
end;

procedure ABUnBackFieldEvent(aField:TField;
                             aOnGetText: TFieldGetTextEvent;aOnSetText: TFieldSetTextEvent;
                             aOnValidate: TFieldNotifyEvent;aOnChange: TFieldNotifyEvent
                           );
begin
  aField.OnGetText  :=aOnGetText;
  aField.OnSetText  :=aOnSetText;
  aField.OnValidate :=aOnValidate;
  aField.OnChange   :=aOnChange;
end;

procedure ABBackAndStopDatasetEvent(aDataset:TDataset;
                           var aBeforeScroll: TDataSetNotifyEvent;var aAfterScroll: TDataSetNotifyEvent
                           );
begin
  try
    aBeforeScroll :=aDataset.BeforeScroll  ;
    aAfterScroll :=aDataset.AfterScroll  ;
  finally
    aDataset.BeforeScroll  :=nil;
    aDataset.AfterScroll  :=nil;
  end;
end;
procedure ABUnBackDatasetEvent(aDataset:TDataSet;
                           aBeforeScroll: TDataSetNotifyEvent;aAfterScroll: TDataSetNotifyEvent
                           );
begin
  aDataset.BeforeScroll  :=aBeforeScroll;
  aDataset.AfterScroll  :=aAfterScroll;
end;

function ABCheckDataSetCanEdit(aDataSet:TDataSet):boolean;
begin
  result:=(Assigned(aDataSet)) and
          (aDataSet.Active) and
          (aDataSet.CanModify);
end;

function ABCheckDataSetInEdit(aDataSet:TDataSet):boolean;
begin
  result:=(ABCheckDataSetCanEdit(aDataSet)) and
          (aDataSet.State in [dsEdit,dsInsert]);
end;

function ABCheckDataSetInActive(aDataSet:TDataSet):boolean;
begin
  result:= (Assigned(aDataSet)) and
           (aDataSet.Active);
end;

procedure ABSetDatasetEdit(aDataSet:TDataSet);
begin
  if (aDataset.State in [dsBrowse]) then
    aDataset.Edit;
end;

function ABGetFieldNamesByDisplayLabels(aDataSet: TDataSet; aDisplayLabels: string;
  aSpaceSign: string): string;
var
  i, j: LongInt;
  tempstr: string;
begin
  Result := EmptyStr;
  for I := 1 to ABGetSpaceStrCount(aDisplayLabels, aSpaceSign) do
  begin
    tempstr := ABGetSpaceStr(aDisplayLabels, i, aSpaceSign);
    if tempstr <> EmptyStr then
    begin
      for j := 0 to aDataSet.FieldCount - 1 do
      begin
        if AnsiCompareText(tempstr, aDataSet.Fields[j].DisplayLabel) = 0 then
        begin
          ABAddstr(Result,aDataSet.Fields[j].FieldName,aSpaceSign);
          Break;
        end;
      end;
    end;
  end;
end;

function ABGetFieldByDisplayLabel(aDataSet: TDataSet; aDisplayLabel: string): TField;
var
  j: LongInt;
begin
  Result := nil;
  for j := 0 to aDataSet.FieldCount - 1 do
  begin
    if AnsiCompareText(aDisplayLabel, aDataSet.Fields[j].DisplayLabel) = 0 then
    begin
      Result := aDataSet.Fields[j];
      Break;
    end;
  end;
end;

function ABGetDisplayLabelsByFieldName(aDataSet: TDataSet; aFieldNames: string;
  aSpaceSign: string): string;
var
  i, j: LongInt;
  tempstr: string;
begin
  Result := EmptyStr;
  for I := 1 to ABGetSpaceStrCount(aFieldNames, aSpaceSign) do
  begin
    tempstr := ABGetSpaceStr(aFieldNames, i, aSpaceSign);
    if tempstr <> EmptyStr then
    begin
      for j := 0 to aDataSet.FieldCount - 1 do
      begin
        if AnsiCompareText(tempstr, aDataSet.Fields[j].FieldName) = 0 then
        begin
          ABAddstr(Result,aDataSet.Fields[j].DisplayLabel,aSpaceSign);
          Break;
        end;
      end;
    end;
  end;
end;

procedure ABCopyFieldProviderFlags(aSDataset,aDDataset:Tdataset);
var
  i:LongInt;
  tempSField,tempDField:TField;
begin
  for i := 0 to aSDataset.FieldCount - 1 do
  begin
    tempSField:= aSDataset.Fields[i];
    tempDField:= aDDataset.FindField(tempSField.FieldName);

    if (Assigned(tempSField)) and
       (Assigned(tempDField)) then
      tempDField.ProviderFlags := tempSField.ProviderFlags;
  end;
end;

procedure ABCopyFieldDisplayLabel(aSDataset,aDDataset:Tdataset);
var
  i:LongInt;
  tempSField,tempDField:TField;
begin
  for i := 0 to aSDataset.FieldCount - 1 do
  begin
    tempSField:= aSDataset.Fields[i];
    tempDField:= aDDataset.FindField(tempSField.FieldName);

    if (Assigned(tempSField)) and
       (Assigned(tempDField)) then
      tempDField.DisplayLabel := tempSField.DisplayLabel;
  end;
end;

function ABGetDataSetRecordCount(aDatasetList: TList): LongInt;
var
  i: LongInt;
begin
  Result := 0;
  for I := 0 to aDatasetList.Count - 1 do
  begin
    if TDataSet(aDatasetList[i]).Active then
    begin
      Result := Result + TDataSet(aDatasetList[i]).RecordCount;
    end;
  end;
end;

function ABGetDataSetRecordCount(
                                aDataSet: TDataSet;
                                aFilterFieldNames: array of string;
                                aFilterFieldValues: array of string
                              ): LongInt;
var
  tempSetFilter,
  tempFilter: string;
  I: Integer;
begin
  //保存数据集的原始情况
  aDataSet.DisableControls;
  tempFilter := ABGetDatasetFilter(aDataSet);
  try
    for I := Low(aFilterFieldNames) to High(aFilterFieldNames) do
    begin
      ABAddstr(tempSetFilter,Format('%s=%s', [aFilterFieldNames[i], QuotedStr(aFilterFieldValues[i])]),' and ') ;
    end;
    ABSetDatasetFilter(aDataSet,tempSetFilter);
    Result:= aDataSet.RecordCount;
  finally
    //恢复数据集的原始情况
    ABSetDatasetFilter(aDataSet, tempFilter);
    aDataSet.EnableControls;
  end;
end;

procedure ABDataSetsToNewDataSet(
                            aNewDataSet: TDataSet;
                            aNewCopyFields: array of string;
                            aNewDetailDataFlagFieldName: string;

                            aMainDataSet: TDataSet;
                            aMainKeyField: string;
                            aMainCopyFields: array of string;

                            aDetailDataSet: TDataSet;
                            aDetailParentField: string;
                            aDetailCopyFields: array of string
                            );
var
  tempDetailFilter: string;
  tempOldBeforeScroll_Main:TDataSetNotifyEvent;
  tempOldAfterScroll_Main:TDataSetNotifyEvent;
  tempOldBeforeScroll_Detail:TDataSetNotifyEvent;
  tempOldAfterScroll_Detail:TDataSetNotifyEvent;
  tempOldBeforeScroll_New:TDataSetNotifyEvent;
  tempOldAfterScroll_New:TDataSetNotifyEvent;
begin
  if (not Assigned(aMainDataSet)) or
     (not aMainDataSet.active) or
     (ABDatasetIsEmpty(aMainDataSet)) then
    exit;
  if (not Assigned(aDetailDataSet)) or
     (not aDetailDataSet.active)  then
    exit;
  if (not Assigned(aNewDataSet)) or
     (not aNewDataSet.active)  then
    exit;

  ABBackAndStopDatasetEvent(aMainDataSet,tempOldBeforeScroll_Main,tempOldAfterScroll_Main);
  ABBackAndStopDatasetEvent(aDetailDataSet,tempOldBeforeScroll_Detail,tempOldAfterScroll_Detail);
  ABBackAndStopDatasetEvent(aNewDataSet,tempOldBeforeScroll_New,tempOldAfterScroll_New);
  aNewDataSet.DisableControls;
  aDetailDataSet.DisableControls;
  tempDetailFilter := ABGetDatasetFilter(aDetailDataSet);
  try
    aMainDataSet.First;
    while not aMainDataSet.Eof do
    begin
      ABSetFieldValue(aMainDataSet, aMainCopyFields,aNewDataSet, aNewCopyFields,true);
      if aNewDetailDataFlagFieldName <> EmptyStr then
        aNewDataSet.FindField(aNewDetailDataFlagFieldName).AsBoolean := false;
      aNewDataSet.Post;

      ABSetDatasetFilter(aDetailDataSet,ABIIF(tempDetailFilter<>EmptyStr,tempDetailFilter+' and ',EmptyStr)+
                                        aDetailParentField+'='+QuotedStr(aMainDataSet.FieldByName(aMainKeyField).AsString));
      aDetailDataSet.First;
      while not aDetailDataSet.Eof do
      begin
        ABSetFieldValue(aDetailDataSet, aDetailCopyFields,aNewDataSet, aNewCopyFields,true);
        if aNewDetailDataFlagFieldName <> EmptyStr then
          aNewDataSet.FindField(aNewDetailDataFlagFieldName).AsBoolean := True;
        aNewDataSet.Post;

        aDetailDataSet.Next;
      end;
      aMainDataSet.Next;
    end;
  finally
    ABUnBackDatasetEvent(aMainDataSet,tempOldBeforeScroll_Main,tempOldAfterScroll_Main);
    ABUnBackDatasetEvent(aDetailDataSet,tempOldBeforeScroll_Detail,tempOldAfterScroll_Detail);
    ABUnBackDatasetEvent(aNewDataSet,tempOldBeforeScroll_New,tempOldAfterScroll_New);

    aNewDataSet.EnableControls;
    ABSetDatasetFilter(aDetailDataSet, tempDetailFilter);
    aDetailDataSet.EnableControls;
  end;
end;

function ABCheckDataSetValue(aDataSet: TDataSet;
                             aCheckFieldNames: array of string;
                             aCheckFieldValues: array of string;
                             aNotEqualShowMsg: boolean;
                             aNotEqualMsg: string;
                             aCheckAllRecord: boolean
                             ): Boolean;
var
  tempOldBeforeScroll: TDataSetNotifyEvent;
  tempOldAfterScroll: TDataSetNotifyEvent;
  function DoItem:Boolean;
  var
    i:LongInt;
    tempField:TField;
  begin
    result := true;
    for i := low(aCheckFieldNames) to high(aCheckFieldNames) do
    begin
      tempField:=aDataSet.FindField(aCheckFieldNames[i]);
      if (tempField.IsNull) and (not ABIsEmptyStr(aCheckFieldValues[i])) or
         (
           (not tempField.IsNull) and
           (
             ((tempField.DataType=ftBoolean) and (tempField.AsBoolean<>ABStrToBool(aCheckFieldValues[i]))) or
             ((tempField.DataType in ABNumDataType) and (ABCompareDouble(tempField.AsFloat,StrToFloat(aCheckFieldValues[i]))<>0)) or
             (AnsiCompareText(tempField.AsString,aCheckFieldValues[i])<>0)
            )
          ) then
      begin
        if aNotEqualShowMsg then
          ABShow(aNotEqualMsg);
        result := false;
        Break;
      end;
    end;
  end;
begin
  result := true;
  if aCheckAllRecord then
  begin
    ABBackAndStopDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    aDataset.DisableControls;
    try
      aDataSet.First;
      while not aDataSet.Eof do
      begin
        if not DoItem then
        begin
          result := false;
          break;
        end;
        aDataSet.Next;
      end;
    finally
      ABUnBackDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
      aDataset.EnableControls;
    end;
  end
  else
  begin
    if not DoItem then
    begin
      result := false;
    end;
  end;
end;

procedure ABGetActiveDetailDataSets(aDataSet: TDataSet;aList: TList;aIncludeSelf:Boolean);
var
  i:LongInt;
  tempList1: TList;
begin                                  
  tempList1 := TList.Create;
  try
    ABGetDetailDatasetList(aDataSet,tempList1,aIncludeSelf);
    for I := aList.Count - 1  downto 0 do
    begin
      if (not TDataSet(aList[i]).Active) then
      begin
        aList.Delete(i);
      end;
    end;
  finally
    tempList1.free;
  end;
end;

procedure ABGetDetailDatasetList(aDataSet: TDataSet; aList:TList;aIncludeSelf:Boolean);
var
  i: LongInt;
  tempList1: TList;
begin
  if aIncludeSelf then
    aList.Add(aDataSet);

  tempList1 := TList.Create;
  try
    aDataSet.GetDetailDataSets(tempList1);
    for I := 0 to tempList1.Count - 1 do
    begin
      aList.Add(tempList1[i]);
      ABGetDetailDatasetList(TDataSet(tempList1[i]),aList,false);
    end;
  finally
    tempList1.free;
  end;
end;

procedure ABOpenDetailDataset(aList:TList);
var
  i: LongInt;
begin
  for I := 0 to aList.Count - 1 do
  begin
    if not TDataSet(aList[i]).Active then
    begin
      TDataSet(aList[i]).Active:=true;
    end;
  end;
end;

procedure ABOpenDetailDataset(aDataSet: TDataSet; aIncludeSelf:Boolean=false);
var
  tempList1: TList;
begin                                  
  tempList1 := TList.Create;
  try
    ABGetDetailDatasetList(aDataSet,tempList1,aIncludeSelf);
    ABOpenDetailDataset(tempList1);
  finally
    tempList1.free;
  end;
end;

procedure ABSetDatasetDisableControls(aDataset: TDataset);
var
  i: LongInt;
  tempList1: TList;
begin
  if not Assigned(aDataset) then
    exit;

  tempList1 := TList.Create;
  try
    ABGetDetailDatasetList(aDataset,tempList1,true);
    for I := 0 to tempList1.Count - 1 do
    begin
      TDataset(tempList1[i]).DisableControls;
    end;
  finally
    tempList1.free;
  end;
end;

procedure ABSetDatasetEnableControls(aDataset: TDataset);
var
  i: LongInt;
  tempList1: TList;
begin
  if not Assigned(aDataset) then
    exit;

  tempList1 := TList.Create;
  try
    ABGetDetailDatasetList(aDataset,tempList1,true);
    for I := 0 to tempList1.Count - 1 do
    begin
      TDataset(tempList1[i]).EnableControls;
    end;
  finally
    tempList1.free;
  end;
end;

procedure ABGetDataSourceList(aOwner:TComponent;aDataset: TDataset;aList: TList);
var
  i: LongInt;
begin
  if Assigned(aOwner) then
  begin
    for I := 0 to aOwner.ComponentCount - 1 do
    begin
      if (aOwner.Components[i] is TDataSource) and
         (TDataSource(aOwner.Components[i]).DataSet=aDataset) then
      begin
        aList.Add(aOwner.Components[i]);
      end;
    end;
  end;
end;

function ABSumDataset(aDataSet: TDataSet;
                      aFieldNames: array of string;
                      aFieldValueSumTypes: array of TABFieldValueSumType;

                      aCheckFieldNames: array of string;
                      aCheckFieldValues: array of string;
                      aSpaceSign: string): TStringDynArray;
var
  i: longint;
  tempOldBeforeScroll:TDataSetNotifyEvent;
  tempOldAfterScroll:TDataSetNotifyEvent;
  tempHaveCheckField:boolean;
  tempFieldValueSumTypes: array of TABFieldValueSumType;


  procedure SetSumTypeByDataType;
  begin
    if aDataSet.FindField(aFieldNames[i]).DataType in ABStrDataType then
    begin
      tempFieldValueSumTypes[i]:=stString;
    end
    else if aDataSet.FindField(aFieldNames[i]).DataType in ABNumDataType then
    begin
      tempFieldValueSumTypes[i]:=stNumber;
    end;
  end;
begin
  SetLength(Result, high(aFieldNames) + 1);
  SetLength(tempFieldValueSumTypes, high(aFieldNames) + 1);
  tempHaveCheckField:=(high(aCheckFieldNames)>-1);

  //初始化返回值各项的类型
  for i := low(aFieldNames) to high(aFieldNames) do
  begin
    if i<=high(aFieldValueSumTypes) then
    begin
      tempFieldValueSumTypes[i]:=aFieldValueSumTypes[i];
      if tempFieldValueSumTypes[i]=stAuto then
      begin
        SetSumTypeByDataType;
      end;
    end
    else
    begin
      SetSumTypeByDataType;
    end;

    if tempFieldValueSumTypes[i]=stString then
    begin
      Result[i] := '';
    end
    else if tempFieldValueSumTypes[i]=stNumber then
    begin
      Result[i] := '0';
    end;
  end;

  ABBackAndStopDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
  aDataSet.DisableControls;
  try
    //循环计算
    aDataSet.First;
    while not aDataSet.Eof do
    begin
      if (not tempHaveCheckField) or
         (ABCheckDataSetValue(aDataSet, aCheckFieldNames, aCheckFieldValues)) then
      begin
        for i := low(aFieldNames) to high(aFieldNames) do
        begin
          if tempFieldValueSumTypes[i]=stString then
          begin
            ABAddstr(Result[i],aDataSet.FindField(aFieldNames[i]).AsString,aSpaceSign);
          end
          else if tempFieldValueSumTypes[i]=stNumber then
          begin
            Result[i] := FloatToStr(strtofloat(Result[i]) + aDataSet.FindField(aFieldNames[i]).AsFloat);
          end;
        end;
      end;
      aDataSet.next;
    end;
  finally
    ABUnBackDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    aDataset.EnableControls;
  end;
end;

procedure ABSetNoParentDatasetValue(
                                      aDataSet: TDataSet;
                                      aParentFieldName: string;
                                      aKeyFieldName: string;
                                      aParentFieldValue: string
                                      );
var
  tempDo:boolean;
  tempOldBeforeScroll:TDataSetNotifyEvent;
  tempOldAfterScroll:TDataSetNotifyEvent;
begin
  ABBackAndStopDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
  aDataSet.DisableControls;
  try
    tempDo:=true;
    while tempDo do
    begin
      tempDo:=False;
      aDataSet.First;
      while not aDataSet.Eof do
      begin
        if (VarToStrDef(aDataSet.Lookup(aKeyFieldName,aDataSet.FieldByName(aParentFieldName).AsString,aKeyFieldName),'')='') and
           (AnsiCompareText(aDataSet.FieldByName(aParentFieldName).AsString,aParentFieldValue)<>0) then
        begin
          aDataSet.Edit;
          aDataSet.FieldByName(aParentFieldName).AsString:=aParentFieldValue;
          aDataSet.Post;

          tempDo:=True;
        end;
        aDataSet.Next;
      end;
    end;
  finally
    ABUnBackDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    aDataSet.EnableControls;
  end;
end;

procedure ABDeleteNoSubRecord(
                              aDataSet: TDataSet;
                              aParentFieldName: string;
                              aKeyFieldName: string;
                              aCheckFieldNames: array of string;
                              aCheckFieldValues: array of string
                              );

var
  tempDo:boolean;
  tempOldBeforeScroll:TDataSetNotifyEvent;
  tempOldAfterScroll:TDataSetNotifyEvent;
  tempHaveCheckField:boolean;
begin
  tempHaveCheckField:=(high(aCheckFieldNames)>-1);
  ABBackAndStopDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
  aDataSet.DisableControls;
  try
    tempDo:=true;
    while tempDo do
    begin
      tempDo:=False;
      aDataSet.First;
      while not aDataSet.Eof do
      begin
        if (not tempHaveCheckField) or
           (ABCheckDataSetValue(aDataSet, aCheckFieldNames, aCheckFieldValues)) then
        begin
          if (VarToStrDef(aDataSet.Lookup(aParentFieldName,aDataSet.FieldByName(aKeyFieldName).AsString,aKeyFieldName),'')='') then
          begin
            aDataSet.Delete;
            tempDo:=True;
          end
        end;
        aDataSet.Next;
      end;
    end;
  finally
    ABUnBackDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    aDataSet.EnableControls;
  end;
end;

procedure ABPostDataset(aDataSet: TDataSet);
begin
  if aDataSet.State in [dsEdit,dsInsert] then
  begin
    aDataSet.Post;
  end;
end;

function ABGetDatasetRecno(aDataSet: TDataSet):LongInt;
begin
  result:=aDataSet.RecNo;
end;

function ABGetDatasetRecno(aRecno:LongInt;aBegFlag:string;aEndFlag:string):LongInt;
begin
  Result:=0;
  if ((aBegFlag=EmptyStr) or (aBegFlag= ABLeftStr(IntToStr(aRecno),length(aBegFlag)))) and
     ((aEndFlag=EmptyStr) or (aEndFlag= ABrightStr(IntToStr(aRecno),length(aEndFlag)))) then
  begin
    Result:=StrToInt(copy(IntToStr(aRecno),length(aBegFlag)+1,length(IntToStr(aRecno))-length(aBegFlag)-length(aEndFlag)))
  end;
end;

function ABSetDatasetRecno(aDataSet: TDataSet;aRecno:LongInt;aBegFlag,aEndFlag:string):LongInt;
var
  i:LongInt;
begin
  Result:=0;
  i:=ABGetDatasetRecno(aRecno,aBegFlag,aEndFlag);
  if i>0 then
  begin
    Result:=ABSetDatasetRecno(aDataSet,i);
  end;
end;

function ABSetDatasetRecno(aDataSet: TDataSet;aRecno:LongInt):LongInt;
begin
  Result:=0;
  if (aDataSet.RecordCount> 0) and
     (aRecno>0) and
     (aRecno<=aDataSet.RecordCount) then
  begin
    if (aRecno<>aDataSet.RecNo) then
    begin
      aDataSet.RecNo:=aRecno;
    end;
    result:=aRecno;
  end;
end;

procedure ABOnlyShowFields(aDataSet: TDataSet;
                           aFieldNames: array of string;
                           aInitVisibleFalse: boolean);
var
  i: longint;
begin
  for i := 0 to aDataSet.FieldCount - 1 do
  begin
    if ABStrInArray(aDataSet.Fields[i].FieldName,aFieldNames)>=0 then
    begin
      aDataSet.Fields[i].Visible := true;
    end
    else if (aInitVisibleFalse) then
        aDataSet.Fields[i].Visible := false;
  end;
end;

procedure ABOnlyHintFields(aDataSet: TDataSet; aFieldNames: array of string;aInitVisibleTrue: boolean);
var
  i: longint;
begin
  for i := 0 to aDataSet.FieldCount - 1 do
  begin
    if ABStrInArray(aDataSet.Fields[i].FieldName,aFieldNames)>=0 then
    begin
      aDataSet.Fields[i].Visible := false;
    end
    else if (aInitVisibleTrue) then
      aDataSet.Fields[i].Visible := true;
  end;
end;

procedure ABOnlyReadFields(aDataSet: TDataSet; aFieldNames: array of string;aInitReadFalse: boolean);
var
  i: longint;
begin
  for i := 0 to aDataSet.FieldCount - 1 do
  begin
    if ABStrInArray(aDataSet.Fields[i].FieldName,aFieldNames)>=0 then
    begin
      aDataSet.Fields[i].ReadOnly := true;
    end
    else if (aInitReadFalse) then
      aDataSet.Fields[i].ReadOnly := false;
  end;
end;

procedure ABOnlyEditFields(aDataSet: TDataSet; aFieldNames: array of string;aInitReadTrue: boolean);
var
  i: longint;
begin
  for i := 0 to aDataSet.FieldCount - 1 do
  begin
    if ABStrInArray(aDataSet.Fields[i].FieldName,aFieldNames)>=0 then
    begin
      aDataSet.Fields[i].ReadOnly := false;
    end
    else if (aInitReadTrue) then
      aDataSet.Fields[i].ReadOnly := true;
  end;
end;

function ABComparisonFieldOldNewValueSame(aField: TField):Boolean;
begin
  result :=ABComparisonvalueSame(ABGetFieldOldValue(aField),ABGetFieldNewValue(aField));
end;

function ABGetFieldOldValue(aField: TField):Variant;
begin
  if (aField.dataset is TADOQuery) and
     (FindVarData(aField.OldValue).VType=14) then
  begin
    Result := StrToFloat(VarToStr(aField.OldValue));
  end
  else
  begin
    Result:=aField.OldValue;
  end;
end;

function ABGetFieldNewValue(aField: TField):Variant;
begin
  Result:=aField.Value;
end;

procedure ABSetFieldValue(
                          aValue: Variant;
                          aField: TField;
                          aPost: Boolean;
                          aAppendValue: Boolean;
                          aNoFindDFieldIsShowMsg: Boolean;
                          aNoFindDFieldShowMsg: string
                          );
begin
  if Assigned(aField) then
  begin
    if (aField.FieldKind<>fkCalculated) or
       (aField.DataSet.State in [dsCalcFields]) then
    begin
      if (ABVarIsNull(aValue)) and
         (aField.IsNull) then
      begin
      end
      else
      begin
        if (aField.DataSet.State in [dsBrowse]) and
           (aField.FieldKind<>fkCalculated) then
        begin
          aField.DataSet.Edit;
        end;

        if ABVarIsNull(aValue) then
        begin
          aField.Clear;
        end
        else
        begin
          if (aAppendValue) and
             (not (aField.DataType in ABBlobDataType)) then
          begin
            if aField.IsNull then
            begin
              aField.Value:=0;
            end;

            if aField.Value<>aField.Value + aValue then
              aField.Value := aField.Value + aValue;
          end
          else
          begin
            if not ABComparisonValueSame(aField.Value,aValue) then
            begin
              if (aField.DataType=ftBoolean)  then
              begin
                aField.Value:=ABStrToBool(VarToStrdef(aValue,''));
              end
              else
              begin
                aField.Value := aValue;
              end;
            end;
          end
        end;

        if (aField.FieldKind<>fkCalculated) and
           (aField.DataSet.State in [dsEdit, dsInsert]) and
           (aPost) then
          aField.DataSet.Post;

      end;
    end;
  end
  else if aNoFindDFieldIsShowMsg then
  begin
    if aNoFindDFieldShowMsg = EmptyStr then
      aNoFindDFieldShowMsg:='目标字段不存在,不能赋值,请检查';
    ABShow(aNoFindDFieldShowMsg);
  end;
end;

procedure ABSetFieldValue(
                          aValues:array of  Variant;
                          aDataset: TDataset; aFieldNames:array of string;
                          aAppend: Boolean;
                          aPost: Boolean; aAppendValue: Boolean;
                          aNoFindDFieldIsShowMsg: Boolean; aNoFindDFieldShowMsg: string
                          );
var
  i:LongInt;
begin
  if aAppend then
  begin
    aDataset.Append;
  end;

  for I := Low(aValues) to High(aValues) do
  begin
    ABSetFieldValue(aValues[i], aDataset.FindField(aFieldNames[i]),
                    False,aAppendValue, aNoFindDFieldIsShowMsg,aNoFindDFieldShowMsg);
  end;

  if (aPost) then
    aDataset.Post;
end;

procedure ABSetFieldValue(
                          aSDataset: TDataset; aSFieldNames:array of string;
                          aDDataset: TDataset; aDFieldNames:array of string;
                          aAppend: Boolean;
                          aPost: Boolean;
                          aAppendValue: Boolean;
                          aNoFindDFieldIsShowMsg: Boolean; aNoFindDFieldShowMsg: string
                          );
var
  i:LongInt;
begin
  if aAppend then
  begin
    aDDataset.Append;
  end;

  for I := Low(aSFieldNames) to High(aSFieldNames) do
  begin
    ABSetFieldValue(aSDataset.FindField(aSFieldNames[i]).Value,
                    aDDataset.FindField(aDFieldNames[i]),
                    False,aAppendValue, aNoFindDFieldIsShowMsg,aNoFindDFieldShowMsg);
  end;
  if (aPost) then
    aDDataset.Post;
end;

procedure ABSetFieldValue(
                          aSDataset: TDataset;
                          aDDataset: TDataset;
                          aFieldNames:array of string;
                          aAppend: Boolean;
                          aPost: Boolean; aAppendValue: Boolean;
                          aNoFindDFieldIsShowMsg: Boolean; aNoFindDFieldShowMsg: string
                          );
var
  i:LongInt;
  tempSField,tempDField:TField;
begin
  if aAppend then
  begin
    aDDataset.Append;
  end;

  for I := Low(aFieldNames) to High(aFieldNames) do
  begin
    tempSField:= aSDataset.FindField(aFieldNames[i]);
    tempDField:= aDDataset.FindField(aFieldNames[i]);

    if (Assigned(tempSField)) and
       (Assigned(tempDField)) then
      ABSetFieldValue(tempSField.Value, tempDField,false,aAppendValue, aNoFindDFieldIsShowMsg,aNoFindDFieldShowMsg);
  end;

  if (aPost) then
    aDDataset.Post;
end;

procedure ABSetFieldValue(
                          aSDataset: TDataset;
                          aDDataset: TDataset;
                          aAppend: Boolean;
                          aPost: Boolean; aAppendValue: Boolean;
                          aNoFindDFieldIsShowMsg: Boolean; aNoFindDFieldShowMsg: string
                          );
var
  i:LongInt;
  tempSField,tempDField:TField;
begin
  if aAppend then
  begin
    aDDataset.Append;
  end;

  for I := 0 to aSDataset.FieldCount-1 do
  begin
    tempSField:= aSDataset.Fields[i];
    tempDField:= aDDataset.FindField(tempSField.FieldName);

    if (Assigned(tempSField)) and
       (Assigned(tempDField)) then
      ABSetFieldValue(tempSField.Value, tempDField,false,aAppendValue, aNoFindDFieldIsShowMsg,aNoFindDFieldShowMsg);
  end;

  if (aPost) then
    aDDataset.Post;
end;

function ABGetFieldValue( aDataset: TDataset;
                          aFieldIndexs: array of LongInt;
                          aDefaultValueOnNull: array of Variant;

                          aSpaceSign: string;
                          aNoFindIsShowMsg: Boolean;
                          aNoFindShowMsg: string
                          ): string;

var
  i,j: LongInt;
  tempStr1:string;
  tempDefaultValueOnNull:Variant;
begin
  Result := EmptyStr;

  j:=High(aDefaultValueOnNull);
  for I := low(aFieldIndexs) to High(aFieldIndexs) do
  begin
    if i<=j then
      tempDefaultValueOnNull:=aDefaultValueOnNull[i]
    else
      tempDefaultValueOnNull:=null;

    tempStr1:=ABGetFieldValue(aDataset, aFieldIndexs[i], tempDefaultValueOnNull,aNoFindIsShowMsg,aNoFindShowMsg);
    if tempStr1<>EmptyStr then
    begin
      ABAddstr(Result,tempStr1,aSpaceSign);
    end;
  end;
end;

function ABGetFieldValue( aDataset: TDataset;
                          aFieldNames:string;
                          aDefaultValueOnNull: array of Variant;

                          aSpaceSign: string;
                          aNoFindIsShowMsg: Boolean;
                          aNoFindShowMsg: string
                          ): string;
begin
  Result := ABGetFieldValue( aDataset,
                          ABStrToStringArray(aFieldNames),
                          aDefaultValueOnNull,

                          aSpaceSign,
                          aNoFindIsShowMsg,
                          aNoFindShowMsg
                          );
end;

function ABGetFieldValue( aDataset: TDataset;
                          aFieldNames: array of string;
                          aDefaultValueOnNull: array of Variant;

                          aSpaceSign: string;
                          aNoFindIsShowMsg: Boolean;
                          aNoFindShowMsg: string
                          ): string;
var
  i,j: LongInt;
  tempStr1:string;
  tempDefaultValueOnNull:Variant;
begin
  Result := EmptyStr;
  j:=High(aDefaultValueOnNull);
  for I := low(aFieldNames) to High(aFieldNames) do
  begin
    if i<=j then
      tempDefaultValueOnNull:=aDefaultValueOnNull[i]
    else
      tempDefaultValueOnNull:=null;

    tempStr1:=ABGetFieldValue(aDataset, aFieldNames[i], tempDefaultValueOnNull,aNoFindIsShowMsg,aNoFindShowMsg);
    if tempStr1<>EmptyStr then
    begin
      ABAddstr(Result,tempStr1,aSpaceSign);
    end;
  end;
end;

function ABGetDatasetValue( aDataSet: TDataSet;
                            aFieldNames: array of string;
                            aCheckFieldNames: array of string;
                            aCheckFieldValues: array of string;
                            aRecordSpaceSign: string;
                            aFieldSpaceSign: string;
                            aFillSameFieldValues:Boolean): string;
var
  tempOldBeforeScroll:TDataSetNotifyEvent;
  tempOldAfterScroll:TDataSetNotifyEvent;
  tempHaveCheckField:boolean;
  tempStrings:TStrings;
  tempStr1: string;
begin
  tempStrings:=nil;
  Result := emptystr;
  tempHaveCheckField:=(high(aCheckFieldNames)>-1);

  ABBackAndStopDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
  aDataSet.DisableControls;
  if not aFillSameFieldValues then
  begin
    tempStrings:=TStringList.Create;
    TStringList(tempStrings).Sorted:=True;
  end;
  try
    //循环计算
    aDataSet.First;
    while not aDataSet.Eof do
    begin
      if (not tempHaveCheckField) or
         (ABCheckDataSetValue(aDataSet, aCheckFieldNames, aCheckFieldValues)) then
      begin
        tempStr1 := ABGetFieldValue(aDataset, aFieldNames,[], aFieldSpaceSign);
        if (aFillSameFieldValues) or
           (tempStrings.IndexOf(tempStr1)<0) then
        begin
          if not aFillSameFieldValues then
            tempStrings.Add(tempStr1);
          ABAddstr(Result,tempStr1,aRecordSpaceSign);
        end;
      end;
      aDataSet.next;
    end;
  finally
    ABUnBackDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    aDataset.EnableControls;
  end;
end;

function ABGetDatasetValue( aDataset: TDataset;
                            aFieldIndexs: array of LongInt;
                            aCheckFieldNames: array of string;
                            aCheckFieldValues: array of string;
                            aRecordSpaceSign: string;
                            aFieldSpaceSign: string;
                            aFillSameFieldValues:Boolean
                            ): string;
var
  tempStr1: string;
  tempOldBeforeScroll:TDataSetNotifyEvent;
  tempOldAfterScroll:TDataSetNotifyEvent;
  tempHaveCheckField:boolean;
  tempStrings:TStrings;
begin
  tempStrings:=nil;
  Result := EmptyStr;
  tempHaveCheckField:=(high(aCheckFieldNames)>-1);
  ABBackAndStopDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
  aDataset.DisableControls;
  if not aFillSameFieldValues then
  begin
    tempStrings:=TStringList.Create;
    TStringList(tempStrings).Sorted:=True;
  end;
  try
    aDataset.First;
    while not aDataset.Eof do
    begin
      if (not tempHaveCheckField) or
         (ABCheckDataSetValue(aDataSet, aCheckFieldNames, aCheckFieldValues)) then
      begin
        tempStr1 := ABGetFieldValue(aDataset, aFieldIndexs,[], aFieldSpaceSign);
        if tempStr1<>EmptyStr then
        begin
          if (aFillSameFieldValues) or
             (tempStrings.IndexOf(tempStr1)<0) then
          begin
            if not aFillSameFieldValues then
              tempStrings.Add(tempStr1);
            ABAddstr(Result,tempStr1,aRecordSpaceSign);
          end;
        end;
      end;

      aDataset.Next;
    end;
  finally
    ABUnBackDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    aDataset.EnableControls;
  end;
end;

function ABGetDatasetValue(
                        aDataset: TDataset;
                        aFieldNames: array of string;
                        aCheckFieldNames: array of string;
                        aCheckFieldValues: array of string;
                        aDatasetLocateValueFieldName: string;

                        aLocateDataset: TDataset;
                        aLocateDatasetFieldName: string;

                        aNoLocateDefaultValue: string;

                        aRecordSpaceSign: string ;
                        aFieldSpaceSign: string ;
                        aFillSameFieldValues:Boolean
                        ): string;
var
  tempStr1: string;
  tempOldBeforeScroll:TDataSetNotifyEvent;
  tempOldAfterScroll:TDataSetNotifyEvent;
  tempOldBeforeScroll_Locate:TDataSetNotifyEvent;
  tempOldAfterScroll_Locate:TDataSetNotifyEvent;
  tempHaveCheckField:boolean;
  tempStrings:TStrings;
begin
  tempStrings:=nil;
  Result := EmptyStr;
  tempHaveCheckField:=(high(aCheckFieldNames)>-1);
  ABBackAndStopDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
  ABBackAndStopDatasetEvent(aLocateDataset,tempOldBeforeScroll_Locate,tempOldAfterScroll_Locate);
  aDataset.DisableControls;
  aLocateDataset.DisableControls;
  if not aFillSameFieldValues then
  begin
    tempStrings:=TStringList.Create;
    TStringList(tempStrings).Sorted:=True;
  end;
  try
    aDataset.First;
    while not aDataset.Eof do
    begin
      if (not tempHaveCheckField) or
         (ABCheckDataSetValue(aDataSet, aCheckFieldNames, aCheckFieldValues)) then
      begin
        if (aLocateDataset.Locate(aLocateDatasetFieldName, aDataset.FindField(aDatasetLocateValueFieldName).Value, [])) then
        begin
          tempStr1 := ABGetFieldValue(aDataset, aFieldNames,[], aFieldSpaceSign);
        end
        else
          tempStr1 := aNoLocateDefaultValue;

        if tempStr1<>EmptyStr then
        begin
          if (aFillSameFieldValues) or
             (tempStrings.IndexOf(tempStr1)<0) then
          begin
            if not aFillSameFieldValues then
              tempStrings.Add(tempStr1);

            ABAddstr(Result,tempStr1,aRecordSpaceSign);
          end;
        end;
      end;

      aDataset.Next;
    end;
  finally
    ABUnBackDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    ABUnBackDatasetEvent(aLocateDataset,tempOldBeforeScroll_Locate,tempOldAfterScroll_Locate);
    aDataset.EnableControls;
    aLocateDataset.EnableControls;
  end;
end;

function ABGetFieldValue(aDataset: TDataset;
                         aIndex: LongInt;
                         aDefaultValueOnNull: Variant;
                         aNoFindIsShowMsg: Boolean;
                         aNoFindShowMsg: string): Variant;
var
  tempField: TField;
begin
  result := Null;
  tempField:=nil;
  if (aIndex>=0) and (aIndex<=aDataset.FieldCount-1) then
    tempField := aDataset.Fields[aIndex];

  if Assigned(tempField) then
  begin
    if not tempField.IsNull then
      result := tempField.Value;
  end
  else if aNoFindIsShowMsg then
  begin
    if aNoFindShowMsg = EmptyStr then
    begin
      ABShow('序号[%d]字段不存在,请检查.', [aIndex]);
    end
    else
    begin
      ABShow(aNoFindShowMsg);
    end
  end;

  if ABVarIsNull(result) then
    result := aDefaultValueOnNull;

  if ABVarIsNull(result) then
    result := '';
end;

function ABGetFieldValue(aDataset: TDataset;
                         aFieldName: string;
                         aDefaultValueOnNull: Variant;
                         aNoFindIsShowMsg: Boolean;
                         aNoFindShowMsg: string): Variant;

var
  tempField: TField;
  tempIndex:LongInt;
begin
  tempIndex:=-1;
  tempField:=aDataset.FindField(aFieldName);
  if Assigned(tempField) then
    tempIndex:=tempField.Index;

  result := ABGetFieldValue(aDataset,tempIndex, aDefaultValueOnNull, aNoFindIsShowMsg, aNoFindShowMsg);
end;

function ABGetFieldValue(aDataset: TDataset;
                         aKeyFields: array of string;
                         aKeyValues: array of Variant;

                         aFieldNames: array of string;
                         aDefaultValueOnNull:Variant): Variant;
begin
  Result := null;
  if (Assigned(aDataset)) and
     (aDataset.Active) then
  begin
    Result :=aDataset.Lookup(ABStringArrayToStr(aKeyFields,False,';'),VarArrayOf(aKeyValues),ABStringArrayToStr(aFieldNames,False,';'));
  end;

  if (ABVarIsNull(Result)) then
    Result := aDefaultValueOnNull;
end;

procedure ABSetFieldValue(
                          aDataset: TDataset;
                          aKeyFieldNames: array of string;
                          aKeyFieldValues: array of Variant;
                          aFieldNames: array of string;
                          aFieldValues: array of Variant
                            );
var
  i:LongInt;
  tempAllNull:boolean;
begin
  if (Assigned(aDataset)) and
     (aDataset.Active)  then
  begin
    tempAllNull:=true;
    for I := low(aFieldNames) to High(aFieldNames) do
    begin
      if (ABVarIsNull(aFieldValues[i])) or
         (VarIsStr(aFieldValues[i])) and (VarToStr(aFieldValues[i])=emptystr) then
      begin
      end
      else
      begin
        tempAllNull:=false;
        break;
      end;
    end;

    if aDataset.Locate(ABStringArrayToStr(aKeyFieldNames,False,';'),VarArrayOf(aKeyFieldValues),[]) then
    begin
      if tempAllNull then
      begin
        aDataset.Delete;
      end
      else
      begin
        aDataset.Edit;
        for I := low(aFieldNames) to High(aFieldNames) do
        begin
          if Assigned(aDataset.FindField(aFieldNames[i])) then
            ABSetFieldValue(aFieldValues[i],aDataset.FindField(aFieldNames[i]));
        end;
        aDataset.Post;
      end;
    end
    else
    begin
      if not tempAllNull then
      begin
        aDataset.Append;
        for I := Low(aKeyFieldNames) to High(aKeyFieldNames) do
        begin
          if Assigned(aDataset.FindField(aKeyFieldNames[i])) then
            ABSetFieldValue(aKeyFieldValues[i],aDataset.FindField(aKeyFieldNames[i]));
        end;

        for I := low(aFieldNames) to High(aFieldNames) do
        begin
          if Assigned(aDataset.FindField(aFieldNames[i])) then
            ABSetFieldValue(aFieldValues[i],aDataset.FindField(aFieldNames[i]));
        end;
        aDataset.Post;
      end;
    end;
  end;
end;

procedure ABSetDataSetValue(
                            aSDataSet: TDataSet;
                            aSFieldNames: array of string;
                            aDDataSet: TDataSet;
                            aDFieldNames: array of string
                            );
var
  tempOldBeforeScroll_S: TDataSetNotifyEvent;
  tempOldAfterScroll_S: TDataSetNotifyEvent;
  tempOldBeforeScroll_D: TDataSetNotifyEvent;
  tempOldAfterScroll_D: TDataSetNotifyEvent;
begin
  ABBackAndStopDatasetEvent(aSDataSet,tempOldBeforeScroll_S,tempOldAfterScroll_S);
  ABBackAndStopDatasetEvent(aDDataSet,tempOldBeforeScroll_D,tempOldAfterScroll_D);
  aSDataSet.DisableControls;
  aDDataSet.DisableControls;
  try
    aSDataSet.First;
    while not aSDataSet.Eof do
    begin
      ABSetFieldValue(aSDataSet,aSFieldNames,aDDataSet,aDFieldNames,true,true);
      aSDataSet.Next;
    end;
  finally
    ABUnBackDatasetEvent(aDDataSet,tempOldBeforeScroll_D,tempOldBeforeScroll_D);
    ABUnBackDatasetEvent(aSDataSet,tempOldBeforeScroll_S,tempOldBeforeScroll_S);
    aDDataSet.EnableControls;
    aSDataSet.EnableControls;
  end;
end;

procedure ABSetDataSetValue(
                            aSDataSet: TDataSet;
                            aDDataSet: TDataSet;
                            aFieldNames: array of string
                            );
var
  tempOldBeforeScroll_S: TDataSetNotifyEvent;
  tempOldAfterScroll_S: TDataSetNotifyEvent;
  tempOldBeforeScroll_D: TDataSetNotifyEvent;
  tempOldAfterScroll_D: TDataSetNotifyEvent;
begin
  ABBackAndStopDatasetEvent(aSDataSet,tempOldBeforeScroll_S,tempOldAfterScroll_S);
  ABBackAndStopDatasetEvent(aDDataSet,tempOldBeforeScroll_D,tempOldAfterScroll_D);
  aSDataSet.DisableControls;
  aDDataSet.DisableControls;
  try
    aSDataSet.First;
    while not aSDataSet.Eof do
    begin
      ABSetFieldValue(aSDataSet,aDDataSet,aFieldNames,true,true);
      aSDataSet.Next;
    end;
  finally
    ABUnBackDatasetEvent(aDDataSet,tempOldBeforeScroll_D,tempOldBeforeScroll_D);
    ABUnBackDatasetEvent(aSDataSet,tempOldBeforeScroll_S,tempOldBeforeScroll_S);
    aDDataSet.EnableControls;
    aSDataSet.EnableControls;
  end;
end;

procedure ABSetDataSetValue(
                            aSDataSet: TDataSet;
                            aDDataSet: TDataSet
                            );
var
  tempOldBeforeScroll_S: TDataSetNotifyEvent;
  tempOldAfterScroll_S: TDataSetNotifyEvent;
  tempOldBeforeScroll_D: TDataSetNotifyEvent;
  tempOldAfterScroll_D: TDataSetNotifyEvent;
begin
  ABBackAndStopDatasetEvent(aSDataSet,tempOldBeforeScroll_S,tempOldAfterScroll_S);
  ABBackAndStopDatasetEvent(aDDataSet,tempOldBeforeScroll_D,tempOldAfterScroll_D);
  aSDataSet.DisableControls;
  aDDataSet.DisableControls;
  try
    aSDataSet.First;
    while not aSDataSet.Eof do
    begin
      ABSetFieldValue(aSDataSet,aDDataSet,true,true);
      aSDataSet.Next;
    end;
  finally
    ABUnBackDatasetEvent(aDDataSet,tempOldBeforeScroll_D,tempOldBeforeScroll_D);
    ABUnBackDatasetEvent(aSDataSet,tempOldBeforeScroll_S,tempOldBeforeScroll_S);
    aDDataSet.EnableControls;
    aSDataSet.EnableControls;
  end;
end;

function ABDatasetIsEmpty(aDataset: TDataset): boolean;
begin
  result :=
    (not aDataset.Active) or
    ((aDataset.State in [dsBrowse]) and (aDataset.IsEmpty));
end;

function ABLocateFormDataset(aForm: TForm; aDataSetName: string;
  const aKeyFields: string; const aKeyValues: Variant;
  aOptions: TLocateOptions): Boolean;
var
  tempDataset: TDataSet;
begin
  result := false;
  if Assigned(aForm) then
  begin
    tempDataset := TDataSet(aForm.FindComponent(aDataSetName));
    if Assigned(tempDataset) then
    begin
      result := tempDataset.Locate(aKeyFields, aKeyValues, aOptions);
    end;
  end;
end;

procedure ABDataSetsToTree(
  aTreeNodes: TTreeNodes;

  aMainDataSet: TDataSet;
  aMainParentField: string;
  aMainKeyField: string;
  aMainNameField: string;
  aMainLevelOrder:Boolean;

  aDetailDataSet: TDataSet;
  aDetailParentField: string;
  aDetailNameField: string;

  aParentItem: TTreeNode;
  aSaveNodeData:Boolean;
  aDoNotMainDetailData:Boolean
  );
var
  tempKeyList,
  tempParentKeyList:tstrings;
  tempHaveDetail:booleaN;
  tempMainNode: TTreeNode;
  tempDetailNode: TTreeNode;
  function GetTreeNode(aKeyValue:String): TTreeNode;
  var
    i:longint;
  begin
    i:=tempKeyList.IndexOf(aKeyValue);
    if i>=0 then
    begin
      Result:=TTreeNode(tempKeyList.Objects[i]);
    end
    else
    begin
      result:=nil;
    end;
  end;

  procedure MoveMainClient;
  var
    tempNode,tempParentNode: TTreeNode;
    i:longint;
  begin
    aMainDataSet.First;
    while not aMainDataSet.Eof do
    begin
      i:=tempKeyList.IndexOf(aMainDataSet.FieldByName(aMainKeyField).AsString);
      if i>=0 then
      begin
        tempNode:=TTreeNode(tempKeyList.Objects[i]);
        tempParentNode:=GetTreeNode(aMainDataSet.FieldByName(aMainParentField).AsString);
        if (Assigned(tempNode)) and
           (Assigned(tempParentNode)) and
           (tempNode.Parent<>tempParentNode) then
        begin
          tempNode.MoveTo(tempParentNode,naAddChild);
        end;
      end;

      aMainDataSet.Next;
    end;
  end;

  function RefreshMainRecord: TTreeNode;
  var
    tempParentNode: TTreeNode;
  begin
    Result:=nil;
    if (aDoNotMainDetailData) or
       (aDetailDataSet.Locate(aDetailParentField,aMainDataSet.FindField(aMainKeyField).AsString,[loCaseInsensitive])) then
    begin
      Result := GetTreeNode(aMainDataSet.FieldByName(aMainKeyField).AsString);
      if (Result = nil) then
      begin
        tempParentNode := GetTreeNode(aMainDataSet.FieldByName(aMainParentField).AsString);
        if not Assigned(tempParentNode) then
        begin
          tempParentNode := aParentItem;
        end;

        Result := TTreeNode(aTreeNodes.AddChild(tempParentNode, aMainDataSet.FieldByName(aMainNameField).AsString));
        tempKeyList.AddObject(aMainDataSet.FieldByName(aMainKeyField).AsString,Result);
        if aSaveNodeData then
        begin
          if tempHaveDetail then
            Result.Data := Pointer(StrToInt('1'+inttostr(aMainDataSet.RecNo)))
          else
            Result.Data := Pointer(StrToInt(inttostr(aMainDataSet.RecNo)));
        end;
      end;
    end;
  end;

  procedure RefreshDetailRecord(aParentNode:TTreeNode);
  begin
    aDetailDataSet.First;
    while not aDetailDataSet.Eof do
    begin
      if aDetailDataSet.FindField(aDetailParentField).AsString =aMainDataSet.FindField(aMainKeyField).AsString then
      begin
        tempDetailNode := TTreeNode(aTreeNodes.AddChild(aParentNode, aDetailDataSet.FindField(aDetailNameField).AsString));
        if aSaveNodeData then
        begin
          tempDetailNode.Data := Pointer(StrToInt('2'+inttostr(aDetailDataSet.RecNo)))
        end;
      end;
      aDetailDataSet.Next;
    end;
  end;
var
  tempOldBeforeScroll: TDataSetNotifyEvent;
  tempOldAfterScroll: TDataSetNotifyEvent;
  tempOldBeforeScroll_1: TDataSetNotifyEvent;
  tempOldAfterScroll_1: TDataSetNotifyEvent;
begin
  if not Assigned(aParentItem) then
    aTreeNodes.Clear;

  tempHaveDetail:=(Assigned(aDetailDataSet)) and
                   (aDetailParentField <> EmptyStr) and
                   (aDetailNameField <> EmptyStr);
  if tempHaveDetail then
  begin
    aDetailDataSet.DisableControls;
    ABBackAndStopDatasetEvent(aDetailDataSet,tempOldBeforeScroll_1,tempOldAfterScroll_1);
  end
  else
  begin
    aMainDataSet.DisableControls;
  end;
  tempParentKeyList:=TStringList.Create;
  tempKeyList:=TStringList.Create;
  ABBackAndStopDatasetEvent(aMainDataSet,tempOldBeforeScroll,tempOldAfterScroll);
  aTreeNodes.BeginUpdate;
  try
    aMainDataSet.First;
    while not aMainDataSet.Eof do
    begin
      tempMainNode:=RefreshMainRecord;
      if (Assigned(tempMainNode)) and (tempHaveDetail) then
      begin
        RefreshDetailRecord(tempMainNode);
      end;

      aMainDataSet.Next;
    end;

    if (AnsiCompareText(aMainParentField,aMainKeyField)<>0) and
       (not aMainLevelOrder) then
      MoveMainClient;
  finally
    aTreeNodes.EndUpdate;
    ABUnBackDatasetEvent(aMainDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    tempKeyList.free;
    tempParentKeyList.free;
    if tempHaveDetail then
    begin
      aDetailDataSet.EnableControls;
      ABUnBackDatasetEvent(aDetailDataSet,tempOldBeforeScroll_1,tempOldAfterScroll_1);
    end
    else
    begin
      aMainDataSet.EnableControls;
    end;
  end;
end;

procedure ABDataSetsToMenu(
  aMenu: TMenu;

  aMainDataSet: TDataSet;
  aMainParentField: string;
  aMainKeyField: string;
  aMainNameField: string;

  aMainHintField: string;
  aMainBeginGroupField: string;
  aMainBitmapField: string;
  aMainOnClick: TNotifyEvent;
  aMainLevelOrder:Boolean;

  aDetailDataSet: TDataSet;
  aDetailParentField: string ;
  aDetailNameField: string ;

  aDetailHintField: string;
  aDetailBeginGroupField: string;
  aDetailBitmapField: string;
  aDetailOnClick: TNotifyEvent;

  aParentItem:TMenuItem ;
  aDoNotMainDetailData:Boolean ;
  aLinkObject:TObject
  );
var
  tempKeyList:tstrings;
  tempHaveDetail:booleaN;
  tempMainNode: TMenuItem;
  tempStopMainMenu: boolean;
  tempStopPopupMenu: boolean;
  function DoCreateMenu(  aDataSet: TDataSet;
                          aNameField: string;
                          aHintField: string;
                          aBeginGroupField: string;
                          aBitmapField: string;
                          aOnClick: TNotifyEvent;

                          aParentItem: TMenuItem
                          ): TMenuItem;
  var
    tempSpaceMenuItem: TMenuItem;
  begin
    result := TMenuItem.Create(aMenu.Owner);
    result.OnClick := aOnClick;
    result.Caption := ABGetFieldValue(aDataSet, ABStrToStringArray(aNameField),[],'-');
    if aHintField <> EmptyStr then
      result.Hint := ABGetFieldValue(aDataSet, ABStrToStringArray(aHintField),[],'-');
    if (aBitmapField <> EmptyStr) and
       (not aDataSet.FindField(aBitmapField).IsNull) then
      ABBlobFieldJpegToBitmap(aDataSet.FindField(aBitmapField),result.Bitmap);

    if (aBeginGroupField <> EmptyStr) and
      (ABStrToBool(aDataSet.FindField(aBeginGroupField).AsString)) then
    begin
      tempSpaceMenuItem := TMenuItem.Create(aMenu.Owner);
      tempSpaceMenuItem.Caption := '-';
      if Assigned(aParentItem) then
        aParentItem.Add(tempSpaceMenuItem)
      else
        aMenu.Items.Add(tempSpaceMenuItem);
    end;
    if Assigned(aParentItem) then
      aParentItem.Add(result)
    else
      aMenu.Items.Add(result);
  end;

  function GeTMenuItem(aKeyValue:String): TMenuItem;
  var
    i:longint;
  begin
    i:=tempKeyList.IndexOf(aKeyValue);
    if i>=0 then
    begin
      Result:=TMenuItem(tempKeyList.Objects[i]);
    end
    else
    begin
      result:=nil;
    end;
  end;

  procedure MoveMainClient;
  var
    tempNode,tempParentNode: TMenuItem;
    i:longint;
  begin
    aMainDataSet.First;
    while not aMainDataSet.Eof do
    begin
      i:=tempKeyList.IndexOf(aMainDataSet.FieldByName(aMainKeyField).AsString);
      if i>=0 then
      begin
        tempNode:=TMenuItem(tempKeyList.Objects[i]);
        tempParentNode:=GeTMenuItem(aMainDataSet.FieldByName(aMainParentField).AsString);
        if (Assigned(tempNode)) and
           (Assigned(tempParentNode)) and
           (tempNode.Parent<>tempParentNode) then
        begin
          tempNode.Parent.Remove(tempNode);
          tempParentNode.Add(tempNode);
        end;
      end;

      aMainDataSet.Next;
    end;
  end;

  function RefreshMainRecord: TMenuItem;
  var
    tempParentNode: TMenuItem;
  begin
    Result:=nil;
    if (aDoNotMainDetailData) or
       (aDetailDataSet.Locate(aDetailParentField,aMainDataSet.FindField(aMainKeyField).AsString,[loCaseInsensitive])) then
    begin
      //当前菜单是否创建过
      Result := GeTMenuItem(aMainDataSet.FieldByName(aMainKeyField).AsString);
      if not (Assigned(Result))  then
      begin
        tempParentNode := GeTMenuItem(aMainDataSet.FieldByName(aMainParentField).AsString);
        if not Assigned(tempParentNode) then
        begin
          tempParentNode := aParentItem;
        end;
        Result := DoCreateMenu(
                    aMainDataSet,
                    aMainNameField,

                    aMainHintField,
                    aMainBeginGroupField,
                    aMainBitmapField,
                    aMainOnClick,

                    tempParentNode
                    );
        tempKeyList.AddObject(aMainDataSet.FieldByName(aMainKeyField).AsString,Result);
      end;
    end;
  end;

  procedure RefreshDetailRecord(aParentNode:TMenuItem);
  begin
    aDetailDataSet.First;
    while not aDetailDataSet.Eof do
    begin
      if aDetailDataSet.FindField(aDetailParentField).AsString =aMainDataSet.FindField(aMainKeyField).AsString then
      begin
        DoCreateMenu(
          aDetailDataSet,
          aDetailNameField,

          aDetailHintField,
          aDetailBeginGroupField,
          aDetailBitmapField,
          aDetailOnClick,

          aParentItem
          );
      end;
      aDetailDataSet.Next;
    end;
  end;
var
  tempOldBeforeScroll: TDataSetNotifyEvent;
  tempOldAfterScroll: TDataSetNotifyEvent;
  tempOldBeforeScroll_1: TDataSetNotifyEvent;
  tempOldAfterScroll_1: TDataSetNotifyEvent;
begin
  if not Assigned(aParentItem) then
    aMenu.Items.Clear;

  tempHaveDetail:=(Assigned(aDetailDataSet)) and
                   (aDetailParentField <> EmptyStr) and
                   (aDetailNameField <> EmptyStr);
  if tempHaveDetail then
  begin
    aDetailDataSet.DisableControls;
    ABBackAndStopDatasetEvent(aDetailDataSet,tempOldBeforeScroll_1,tempOldAfterScroll_1);
  end
  else
  begin
    aMainDataSet.DisableControls;
  end;

  tempStopMainMenu:=false;
  tempStopPopupMenu:=false;
  if (Assigned(aLinkObject)) then
  begin
    if (aMenu is TMainMenu) then
    begin
      if (aLinkObject is TCustomForm) then
      begin
        tempStopMainMenu:=true;
        TCustomForm(aLinkObject).Menu:=nil;
      end;
    end
    else if (aMenu is TPopupMenu) then
    begin
      ABSetObjectPropValue(aLinkObject,'PopupMenu',nil);
      tempStopPopupMenu:=true;
    end;
  end;

  tempKeyList:=TStringList.Create;
  ABBackAndStopDatasetEvent(aMainDataSet,tempOldBeforeScroll,tempOldAfterScroll);
  try
    aMainDataSet.First;
    while not aMainDataSet.Eof do
    begin
      tempMainNode:=RefreshMainRecord;
      if (Assigned(tempMainNode)) and (tempHaveDetail) then
      begin
        RefreshDetailRecord(tempMainNode);
      end;

      aMainDataSet.Next;
    end;

    if (AnsiCompareText(aMainParentField,aMainKeyField)<>0) and
       (not aMainLevelOrder) then
      MoveMainClient;
  finally
    if tempStopMainMenu then
      TCustomForm(aLinkObject).Menu:=TMainMenu(aMenu)
    else if tempStopPopupMenu then
      ABSetObjectPropValue(aLinkObject,'PopupMenu',aMenu);

    ABUnBackDatasetEvent(aMainDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    tempKeyList.free;
    if tempHaveDetail then
    begin
      aDetailDataSet.EnableControls;
      ABUnBackDatasetEvent(aDetailDataSet,tempOldBeforeScroll_1,tempOldAfterScroll_1);
    end
    else
    begin
      aMainDataSet.EnableControls;
    end;
  end;
end;

function ABGetDatasetFilter(aDataset: TDataset):string;
begin
  result:=EmptyStr;
  if not Assigned(aDataset) then
    exit;

  result:=aDataset.Filter;
end;

procedure ABSetDatasetFilter(aDataset: TDataset;aFilter:string);
begin
  if not Assigned(aDataset) then
    exit;

  if not aDataset.Filtered then
    aDataset.Filtered:=true;

  aDataset.Filter:=aFilter;
end;

function ABCodeAndValueFieldConversion(aStr: string; aDataset: TDataSet;
  aCodeFieldName, aNameFieldName: string): string;
var
  tempOldBeforeScroll: TDataSetNotifyEvent;
  tempOldAfterScroll: TDataSetNotifyEvent;
begin
  Result := aStr;
  if (not Assigned(aDataset)) or
    (not aDataset.active) then
    exit;

  ABBackAndStopDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
  aDataset.DisableControls;
  try
    aDataset.First;
    while not aDataset.Eof do
    begin
      if abpos(aDataset.FindField(aCodeFieldName).AsString, Result) > 0 then
        Result := ABStringReplace(Result,aDataset.FindField(aCodeFieldName).AsString,aDataset.FindField(aNameFieldName).AsString);

      aDataset.Next;
    end;
  finally
    aDataset.EnableControls;
    ABUnBackDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
  end;
end;

procedure ABCodeAndValueFieldConversion(aStrings: TStrings; aDataset: TDataSet;
  aCodeFieldName, aNameFieldName: string);
var
  i: LongInt;
begin
  for I := 0 to aStrings.Count - 1 do
  begin
    aStrings.Strings[i] := ABCodeAndValueFieldConversion(aStrings.Strings[i], aDataset,aCodeFieldName, aNameFieldName);
  end;
end;

function ABCodeAndValueFieldConversion(aStr: string; aDataset: TDataSet;
  aCodeFieldIndex, aNameFieldIndex: Longint): string;
begin
  Result := aStr;
  if (not Assigned(aDataset)) or
    (not aDataset.active) then
    exit;

  result := ABCodeAndValueFieldConversion(aStr, aDataset,
                              aDataset.Fields[aCodeFieldIndex].FieldName,
                              aDataset.Fields[aNameFieldIndex].FieldName);
end;

procedure ABCodeAndValueFieldConversion(aStrings: TStrings; aDataset: TDataSet;
  aCodeFieldIndex, aNameFieldIndex: Longint);
begin
  if (not Assigned(aDataset)) or
    (not aDataset.active) then
    exit;

  ABCodeAndValueFieldConversion(aStrings, aDataset,
    aDataset.Fields[aCodeFieldIndex].FieldName,
    aDataset.Fields[aNameFieldIndex].FieldName);
end;

procedure ABDelStringsByDataset(  aStrings: TStrings;
                                  aDataset: TDataset;
                                  aFieldNames: array of string;
                                  SpaceSign: string = ','
                                  );
var
  tempStr1: string;
  i: longint;
  tempOldBeforeScroll: TDataSetNotifyEvent;
  tempOldAfterScroll: TDataSetNotifyEvent;
begin
  if (not Assigned(aDataset)) or
    (not aDataset.active) then
    exit;

  ABBackAndStopDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
  aDataset.DisableControls;
  try
    aDataset.First;
    while not aDataset.Eof do
    begin
      tempStr1 := ABGetFieldValue(aDataset, aFieldNames,[], SpaceSign);
      if tempStr1 <> EmptyStr then
      begin
        i := aStrings.IndexOf(tempStr1);
        if i >= 0 then
        begin
          aStrings.Delete(i);
        end;
      end;
      aDataset.Next;
    end;
  finally
    aDataset.EnableControls;
    ABUnBackDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
  end;
end;

procedure ABFieldNameToStrings(aDataset: TDataset;
  aStrings: TStrings;
  aIsDisplayLabel: boolean;
  aNoFillSame: boolean;
  aIsDelOld: boolean
  );
var
  i: longint;
  tempStr1: string;
begin
  aStrings.BeginUpdate;
  try
    if aIsDelOld then
    begin
      aStrings.Clear;
    end;

    for I := 0 to aDataSet.FieldCount - 1 do
    begin
      if aIsDisplayLabel then
        tempStr1 := aDataSet.Fields[i].DisplayLabel
      else
        tempStr1 := aDataSet.Fields[i].FieldName;
      if aNoFillSame then
      begin
        if aStrings.IndexOf(tempStr1) < 0 then
        begin
          aStrings.Add(tempStr1);
        end;
      end
      else
      begin
        aStrings.Add(tempStr1);
      end;
    end;

  finally
    aStrings.EndUpdate;
  end;
end;

procedure ABDatasetToStrings(aDataset: TDataset;
  aFieldNames: array of string;
  aStrings: TStrings;
  SpaceSign: string;
  aNoFillSame: boolean;
  aIsDelOld: boolean
  );
var
  tempStr1: string;
  i, j: longint;
  tempOldBeforeScroll: TDataSetNotifyEvent;
  tempOldAfterScroll: TDataSetNotifyEvent;
begin
  if (not Assigned(aDataset)) or
     (not aDataset.active) then
    exit;

  aStrings.BeginUpdate;
  try
    if aIsDelOld then
    begin
      aStrings.Clear;
    end;

    ABBackAndStopDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    aDataset.DisableControls;
    try
      aDataset.First;
      while not aDataset.Eof do
      begin
        i := High(aFieldNames);
        if i = -1 then
          i := aDataset.FieldCount - 1;

        tempStr1 := EmptyStr;
        for j := 0 to i do
        begin
          if High(aFieldNames) > -1 then
          begin
            if tempStr1 = EmptyStr then
              tempStr1 := aDataset.FindField(aFieldNames[j]).AsString
            else
              tempStr1 := tempStr1 + SpaceSign +aDataset.FindField(aFieldNames[j]).AsString;
          end
          else
          begin
            if tempStr1 = EmptyStr then
              tempStr1 := aDataset.Fields[j].AsString
            else
              tempStr1 := tempStr1 + SpaceSign + aDataset.Fields[j].AsString;
          end;
        end;

        if tempStr1 <> EmptyStr then
        begin
          if aNoFillSame then
          begin
            if aStrings.IndexOf(tempStr1) < 0 then
            begin
              aStrings.Add(tempStr1);
            end;
          end
          else
          begin
            aStrings.Add(tempStr1);
          end;
        end;
        aDataset.Next;
      end;
    finally
      aDataset.EnableControls;
      ABUnBackDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    end;
  finally
    aStrings.EndUpdate;
  end;
end;

procedure ABCreateFieldDef(
           aFieldDefs:TFieldDefs;
           aFieldName,aFieldType:string;
           aFieldByte,aFieldLength,aEecimalDigits:Longint);
var
  tempFieldDef:TFieldDef;
begin
  aFieldType:=LowerCase(aFieldType);
  tempFieldDef:=aFieldDefs.AddFieldDef;
  if aFieldType='bigint' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftLargeint;
    tempFieldDef.Precision:=aFieldLength;
  end
  else if aFieldType='binary' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftVarBytes;
    tempFieldDef.Precision:=aFieldLength;
  end
  else if aFieldType='bit' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftBoolean;
  end
  else if aFieldType='char' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftString;
    tempFieldDef.Attributes := [faFixed];
    tempFieldDef.Size:=aFieldLength;
  end
  else if aFieldType='datetime' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftdatetime;
  end
  else if aFieldType='decimal' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftFloat;
    tempFieldDef.Precision:=aFieldLength;
    tempFieldDef.Size:=aEecimalDigits;
  end
  else if aFieldType='float' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftFloat;
  end
  else if aFieldType='image' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftBlob;
    tempFieldDef.Size:=1;
  end
  else if aFieldType='int' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftInteger;
  end
  else if aFieldType='money' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftFMTBcd;
    tempFieldDef.Precision:=aFieldLength;
    tempFieldDef.Size:=aEecimalDigits;
  end
  else if aFieldType='nchar' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftWideString;
    tempFieldDef.Size:=aFieldByte;
  end
  else if aFieldType='ntext' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:= {$IF (ABDelphiVersion_int>=185)} ftWideMemo {$ELSE} ftMemo {$IFEND} ;
    tempFieldDef.Size:=1;
  end
  else if aFieldType='numeric' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftFloat;
    tempFieldDef.Precision:=aFieldLength;
    tempFieldDef.Size:=aEecimalDigits;
  end
  else if aFieldType='nvarchar' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftWideString;
    tempFieldDef.Size:=aFieldByte;
  end
  else if aFieldType='real' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftFloat;
  end
  else if aFieldType='smalldatetime' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftdatetime;
  end
  else if aFieldType='smallint' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftSmallint;
  end
  else if aFieldType='smallmoney' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftFMTBcd;
    tempFieldDef.Precision:=aFieldLength;
    tempFieldDef.Size:=aEecimalDigits;
  end
  else if aFieldType='sql_variant' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftVarBytes;
    tempFieldDef.Size:=16;
  end
  else if aFieldType='text' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftMemo;
    tempFieldDef.Size:=1;
  end
  else if aFieldType='timestamp' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftDateTime;
    tempFieldDef.Size:=aFieldLength;
  end
  else if aFieldType='tinyint' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftSmallint;
  end
  else if aFieldType='uniqueidentifier' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftString;
    tempFieldDef.Size:=38;
  end
  else if aFieldType='varbinary' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftVarBytes;
    tempFieldDef.Size:=aFieldLength;
  end
  else if aFieldType='varchar' then
  begin
    tempFieldDef.Name:=aFieldName;
    tempFieldDef.DataType:=ftString;
    tempFieldDef.Size:=aFieldLength;
  end;
end;

procedure ABCreateFields(aFieldDefs:TFieldDefs;aHaveField:string;aNoHaveField:string);
var
  i:LongInt;
begin
  aFieldDefs.DataSet.Fields.Clear;
  for I := 0 to aFieldDefs.Count - 1 do
  begin
    if ((aNoHaveField=EmptyStr)  or (ABPos(','+aFieldDefs[i].Name+',',','+aNoHaveField  +',')<=0)) and
       ((aHaveField=EmptyStr)    or (ABPos(','+aFieldDefs[i].Name+',',','+aHaveField+',')> 0)) then
      aFieldDefs[i].CreateField(aFieldDefs.DataSet);
  end;
end;

procedure ABCreateFieldDefs(aFieldDefs:TFieldDefs;
                                 aFieldInfoDataset:TDataset;
                                 aFieldNameField:string='Name';
                                 aFieldTypeField:string='Type';
                                 aFieldByteField:string='Byte';
                                 aFieldLengthField:string='Length';
                                 aFieldEecimalDigitsField:string='EecimalDigits';

                                 aClearOldFieldDefs:Boolean=true;
                                 aCreateField:Boolean=false;

                                 aHaveField:string='';
                                 aNoHaveField:string='');
var
  tempFieldName,tempFieldType:string;
  tempFieldByte,tempFieldLength,tempFieldEecimalDigits:Longint;
begin
  if aClearOldFieldDefs then
  begin
    aFieldDefs.Clear;
    if aCreateField then
    begin
      aFieldDefs.DataSet.Fields.Clear;
    end;
  end;
  aFieldInfoDataset.First;
  while not aFieldInfoDataset.EOF do
  begin
    tempFieldName  :=aFieldInfoDataset.FindField(aFieldNameField).AsString;
    if ((aNoHaveField=EmptyStr)  or (ABPos(','+tempFieldName+',',','+aNoHaveField  +',')<=0)) and
       ((aHaveField=EmptyStr)    or (ABPos(','+tempFieldName+',',','+aHaveField+',')> 0)) then
    begin
      tempFieldType  :=aFieldInfoDataset.FindField(aFieldTypeField).AsString;
      tempFieldByte  :=StrToInt64Def(aFieldInfoDataset.FindField(aFieldByteField).AsString,0);
      tempFieldLength:=StrToInt64Def(aFieldInfoDataset.FindField(aFieldLengthField).AsString,0);
      tempFieldEecimalDigits:=StrToInt64Def(aFieldInfoDataset.FindField(aFieldEecimalDigitsField).AsString,0);

      ABCreateFieldDef(aFieldDefs,
                            tempFieldName,tempFieldType,tempFieldByte,tempFieldLength,tempFieldEecimalDigits);
      if aCreateField then
      begin
        aFieldDefs.Find(tempFieldName).CreateField(aFieldDefs.DataSet);
      end;
    end;
    aFieldInfoDataset.Next;
  end;
end;

function ABGetDatasetFieldNames(aDataset: TDataSet;aNotHaveFieldNames: string; aSpaceSign: string ): string;
var
  i: LongInt;
begin
  Result := EmptyStr;
  for i := 0 to aDataset.FieldCount - 1 do
  begin
    if (aNotHaveFieldNames=EmptyStr) or (abpos(','+aDataset.Fields[i].FieldName+',', ','+aNotHaveFieldNames+',') <= 0) then
    begin
      ABAddstr(Result,aDataset.Fields[i].FieldName,aSpaceSign);
    end;
  end;
end;

procedure ABCloseDataset(aDataset: TDataSet;aCloseDetail:boolean);
var
  tempList1:TList;
  i:LongInt;
begin
  if aCloseDetail then
  begin
    tempList1 := TList.Create;
    try
      ABGetDetailDatasetList(aDataset,tempList1,true);
      for I := 0 to tempList1.Count - 1 do
      begin
        if TDataset(tempList1[i]).Active then
        begin
          TDataset(tempList1[i]).close;
        end;
      end;
    finally
      tempList1.free;
    end;
  end;

  if aDataSet.Active then
  begin
    aDataSet.close;
  end;
end;

function ABGetDataSetLevel(
                            aDataSet     :TDataSet;
                            aKeyField    :string;
                            aParentField :string;
                            aValue       :string;
                            aQueryStrings:TStrings;
                            aTopValue    :string
                            ): longint;
  function GetLevel(aValue:string): longint;
  var
    i:longint;
    tempParentGuid:string;
  begin
    i:=aQueryStrings.IndexOf(aValue);
    if i>=0 then
    begin
      Result:=StrToInt(aQueryStrings.ValueFromIndex[i]);
    end
    else
    begin
      if (aValue=EmptyStr) or
         (aValue=aTopValue) then
      begin
        result:=0;
      end
      else
      begin
        tempParentGuid:=VarToStrDef(aDataSet.Lookup(aKeyField,aValue,aParentField),'');
        if tempParentGuid<>EmptyStr then
        begin
          result:=1+GetLevel(tempParentGuid);
        end
        else
        begin
          result:=0;
        end;
      end;
      aQueryStrings.Add(aValue+'='+inttostr(Result));
    end;
  end;
begin
  if not TStringList(aQueryStrings).Sorted then
    TStringList(aQueryStrings).Sorted:=true;

  Result:=GetLevel(aValue);
end;

procedure ABSetDataSetLevel(
                            aDataSet     :TDataSet;
                            aKeyField    :string;
                            aParentField :string;
                            aLevelField    :string;
                            aTopValue    :string
                            );
var
  tempStrings:TStrings;
  tempLevel:LongInt;
begin
  if Assigned(aDataSet.FieldByName(aLevelField)) then
  begin
    tempStrings:=TStringList.Create;
    aDataSet.DisableControls;
    try
      aDataSet.First;
      while not aDataSet.Eof do
      begin
        tempLevel:=ABGetDataSetLevel( aDataSet,
                                     aKeyField,
                                     aParentField,
                                     aDataSet.FieldByName(aParentField).AsString,
                                     tempStrings,
                                     aTopValue
                                     );
        aDataSet.Edit;
        aDataSet.FieldByName(aLevelField).AsInteger:=tempLevel;
        aDataSet.post;
        aDataSet.Next;
      end;
    finally
      aDataSet.EnableControls;
      tempStrings.Free;
    end;
  end;
end;


end.
