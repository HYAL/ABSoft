inherited ABSockFrame: TABSockFrame
  Width = 510
  Constraints.MinWidth = 510
  ParentFont = False
  ExplicitWidth = 510
  object Splitter1: TSplitter
    Left = 214
    Top = 0
    Height = 249
    ExplicitLeft = 217
    ExplicitTop = 15
    ExplicitHeight = 257
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 214
    Height = 249
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object GroupBox9: TGroupBox
      Left = 0
      Top = 136
      Width = 214
      Height = 113
      Align = alClient
      Caption = #21457#36865#25991#26412
      TabOrder = 0
      object Memo1: TMemo
        Tag = 911
        Left = 2
        Top = 15
        Width = 210
        Height = 96
        Align = alClient
        ImeName = #24555#20048#20116#31508
        Lines.Strings = (
          '123'#20013#21326#20154#27665#20849#21644#22269'456'#20013#22269)
        ScrollBars = ssVertical
        TabOrder = 0
        WantTabs = True
        WordWrap = False
        OnKeyDown = Memo1KeyDown
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 214
      Height = 136
      Align = alTop
      AutoSize = True
      BevelOuter = bvNone
      TabOrder = 1
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 214
        Height = 68
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label2: TLabel
          Left = 3
          Top = 27
          Width = 46
          Height = 13
          AutoSize = False
          Caption = #31867#22411
        end
        object Label8: TLabel
          Left = 3
          Top = 5
          Width = 46
          Height = 13
          AutoSize = False
          Caption = #27169#22411
        end
        object Label3: TLabel
          Left = 3
          Top = 50
          Width = 46
          Height = 13
          AutoSize = False
          Caption = #21253#22823#23567
        end
        object Label1: TLabel
          Left = 138
          Top = 49
          Width = 24
          Height = 17
          AutoSize = False
          Caption = #37325#21457
        end
        object ComboBox1: TComboBox
          Left = 48
          Top = 24
          Width = 164
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 0
          Text = 'UDPClient'
          OnChange = ComboBox1Change
          Items.Strings = (
            'UDPClient'
            'TCPServer'
            'TCPClient')
        end
        object ComboBox2: TComboBox
          Left = 48
          Top = 2
          Width = 164
          Height = 21
          Style = csDropDownList
          ItemIndex = 0
          TabOrder = 1
          Text = 'select('#36873#25321#27169#22411')'
          OnChange = ComboBox1Change
          Items.Strings = (
            'select('#36873#25321#27169#22411')'
            'WSAAsyncSelect('#24322#27493#36873#25321#27169#22411')'
            'WSAEventSelect('#20107#20214#36873#25321#27169#22411')'
            'Overlapped I/O ('#20107#20214#36890#30693#27169#22411')'
            'Overlapped I/O ('#23436#25104#20363#31243#27169#22411')'
            'IOCP('#23436#25104#31471#21475#27169#22411')')
        end
        object SpinEdit3: TSpinEdit
          Tag = 91
          Left = 48
          Top = 46
          Width = 89
          Height = 22
          MaxValue = 0
          MinValue = 25
          TabOrder = 2
          Value = 25
        end
        object SpinEdit4: TSpinEdit
          Tag = 91
          Left = 162
          Top = 46
          Width = 50
          Height = 22
          MaxValue = 1000
          MinValue = 0
          TabOrder = 3
          Value = 100
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 91
        Width = 214
        Height = 22
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label6: TLabel
          Left = 3
          Top = 4
          Width = 46
          Height = 17
          AutoSize = False
          Caption = #30446#30340'IP'
        end
        object Label7: TLabel
          Left = 138
          Top = 4
          Width = 24
          Height = 17
          AutoSize = False
          Caption = #31471#21475
        end
        object Edit1: TEdit
          Tag = 91
          Left = 48
          Top = 1
          Width = 89
          Height = 21
          ImeName = #24555#20048#20116#31508
          TabOrder = 0
          Text = '192.168.0.140'
        end
        object SpinEdit2: TSpinEdit
          Tag = 91
          Left = 162
          Top = 1
          Width = 50
          Height = 22
          Ctl3D = True
          MaxValue = 65535
          MinValue = 0
          ParentCtl3D = False
          TabOrder = 1
          Value = 6001
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 68
        Width = 214
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object Label5: TLabel
          Left = 3
          Top = 4
          Width = 46
          Height = 13
          AutoSize = False
          Caption = #26412#22320#31471#21475
        end
        object SpinEdit1: TSpinEdit
          Tag = 91
          Left = 48
          Top = 1
          Width = 164
          Height = 22
          MaxValue = 65535
          MinValue = 0
          TabOrder = 0
          Value = 6000
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 113
        Width = 214
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 3
        object Label4: TLabel
          Left = 3
          Top = 4
          Width = 46
          Height = 17
          AutoSize = False
          Caption = 'UDP'#32452#25773
        end
        object Edit3: TEdit
          Tag = 91
          Left = 48
          Top = 1
          Width = 164
          Height = 21
          ImeName = #24555#20048#20116#31508
          TabOrder = 0
          Text = '239.192.0.1'
        end
      end
    end
  end
  object Panel7: TPanel
    Left = 0
    Top = 249
    Width = 510
    Height = 36
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Button1: TButton
      Left = 4
      Top = 6
      Width = 100
      Height = 25
      Caption = #36830#25509
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 110
      Top = 6
      Width = 100
      Height = 25
      Caption = #26029#24320
      Enabled = False
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button5: TButton
      Left = 460
      Top = 6
      Width = 45
      Height = 25
      Caption = #28165#38500
      TabOrder = 2
      OnClick = Button5Click
    end
    object Button3: TButton
      Left = 258
      Top = 6
      Width = 45
      Height = 25
      Caption = #21457#25991#26412
      Enabled = False
      TabOrder = 3
      OnClick = Button3Click
    end
    object Edit2: TEdit
      Tag = 91
      Left = 216
      Top = 8
      Width = 42
      Height = 21
      TabOrder = 4
      Text = '1'
    end
    object Button4: TABDownButton
      Left = 302
      Top = 6
      Width = 45
      Height = 25
      Caption = #21457#25991#20214
      DropDownMenu = PopupMenu1
      Enabled = False
      TabOrder = 5
    end
    object Button6: TABDownButton
      Left = 346
      Top = 6
      Width = 56
      Height = 25
      BiDiMode = bdLeftToRight
      Caption = #21457#25968#25454#27969
      DropDownMenu = PopupMenu2
      Enabled = False
      ParentBiDiMode = False
      TabOrder = 6
    end
    object Button7: TButton
      Left = 401
      Top = 6
      Width = 56
      Height = 25
      Caption = #21457#23383#33410#27969
      Enabled = False
      TabOrder = 7
      OnClick = Button7Click
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 285
    Width = 510
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object GroupBox11: TGroupBox
    Left = 217
    Top = 0
    Width = 293
    Height = 249
    Align = alClient
    Caption = #25509#25910#25968#25454
    TabOrder = 3
    object Memo2: TMemo
      Left = 2
      Top = 15
      Width = 289
      Height = 215
      Align = alClient
      ImeName = #24555#20048#20116#31508
      ScrollBars = ssVertical
      TabOrder = 0
      OnKeyDown = Memo1KeyDown
      ExplicitLeft = 26
      ExplicitTop = 24
    end
    object ProgressBar1: TProgressBar
      Left = 2
      Top = 230
      Width = 289
      Height = 17
      Align = alBottom
      TabOrder = 1
    end
  end
  object ABSocket1: TABSocket
    LocalPort = 0
    ToPort = 0
    SocketType = stUDP
    SocketModel = smSelect
    OnError = ABSocket1Error
    OnData = ABSocket1Data
    OnClose = ABSocket1Close
    OnEvent = ABSocket1Event
    BuffSize = 1024
    MaxError = 10
    ACKOnData = False
    Left = 281
    Top = 88
  end
  object PopupMenu1: TPopupMenu
    Left = 272
    Top = 184
    object N1: TMenuItem
      Caption = #25991#20214#27969#26041#24335'('#36793#35835#36793#21457','#33410#30465#20869#23384')'
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #25968#25454#27969#26041#24335'('#21152#36733#20869#23384#21518#21457#36865','#38656#36739#22823#20869#23384')'
      OnClick = N2Click
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 352
    Top = 184
    object MenuItem1: TMenuItem
      Caption = #25991#20214#27969'('#36793#35835#36793#21457','#33410#30465#20869#23384')'
      OnClick = MenuItem1Click
    end
    object MenuItem2: TMenuItem
      Caption = #25968#25454#27969'('#21152#36733#20869#23384#21518#21457#36865','#38656#36739#22823#20869#23384')'
      OnClick = MenuItem2Click
    end
  end
end
