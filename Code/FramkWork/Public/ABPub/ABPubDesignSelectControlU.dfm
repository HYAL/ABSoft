object ABPubDesignSelectControlForm: TABPubDesignSelectControlForm
  Left = 235
  Top = 111
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #25511#20214#36873#25321
  ClientHeight = 500
  ClientWidth = 600
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object gbFilter: TGroupBox
    Left = 8
    Top = 9
    Width = 241
    Height = 118
    Caption = #32452#20214#23481#22120#36807#28388'(&F)'
    TabOrder = 0
    object rbCurrForm: TRadioButton
      Left = 8
      Top = 17
      Width = 160
      Height = 17
      Caption = #31383#20307#19978#25152#26377#25511#20214
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = DoUpdateListControls
    end
    object rbSpecControl: TRadioButton
      Left = 8
      Top = 40
      Width = 160
      Height = 17
      Caption = #25351#23450#25511#20214#30340#23376#25511#20214
      TabOrder = 1
      OnClick = DoUpdateListControls
    end
    object cbbFilterControl: TComboBox
      Left = 24
      Top = 62
      Width = 201
      Height = 21
      Style = csDropDownList
      Sorted = True
      TabOrder = 2
      OnChange = DoUpdateListControls
    end
    object cbIncludeChildren: TCheckBox
      Left = 8
      Top = 88
      Width = 161
      Height = 17
      Caption = #21253#21547#22810#32423#23376#25511#20214
      Checked = True
      State = cbChecked
      TabOrder = 3
      OnClick = DoUpdateListControls
    end
  end
  object gbByName: TGroupBox
    Left = 255
    Top = 9
    Width = 337
    Height = 118
    Caption = #32452#20214#21517#31216#31867#22411#36807#28388'(&N)'
    TabOrder = 1
    object edtByName: TEdit
      Left = 24
      Top = 40
      Width = 297
      Height = 21
      TabOrder = 1
      OnChange = DoUpdateListControls
    end
    object cbByName: TCheckBox
      Left = 8
      Top = 16
      Width = 276
      Height = 17
      Caption = #20801#35768#25511#20214#21517#31216#36807#28388
      TabOrder = 0
      OnClick = DoUpdateListControls
    end
    object cbByClass: TCheckBox
      Left = 8
      Top = 64
      Width = 121
      Height = 17
      Caption = #20801#35768#25511#20214#31867#22411#36807#28388
      TabOrder = 2
      OnClick = DoUpdateListControls
    end
    object cbbByClass: TComboBox
      Left = 24
      Top = 86
      Width = 297
      Height = 21
      Sorted = True
      TabOrder = 3
      OnChange = DoUpdateListControls
    end
    object cbSubClass: TCheckBox
      Left = 144
      Top = 64
      Width = 81
      Height = 17
      Caption = #21253#21547#23376#31867
      Checked = True
      State = cbChecked
      TabOrder = 4
      OnClick = DoUpdateListControls
    end
  end
  object gbComponentList: TGroupBox
    Left = 8
    Top = 130
    Width = 584
    Height = 313
    Caption = #32452#20214#21015#34920'(&M)'
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 88
      Height = 13
      Caption = #21487#20379#36873#25321#30340#25511#20214':'
    end
    object Label2: TLabel
      Left = 312
      Top = 16
      Width = 76
      Height = 13
      Caption = #24050#36873#25321#30340#25511#20214':'
    end
    object Label4: TLabel
      Left = 8
      Top = 273
      Width = 52
      Height = 13
      Caption = #25490#24207#26041#24335':'
    end
    object lbSource: TListBox
      Left = 8
      Top = 32
      Width = 233
      Height = 233
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ItemHeight = 14
      MultiSelect = True
      ParentFont = False
      TabOrder = 0
      OnDblClick = actAddExecute
    end
    object btnAdd: TButton
      Left = 248
      Top = 40
      Width = 57
      Height = 21
      Caption = '>'
      TabOrder = 2
      OnClick = actAddExecute
    end
    object btnAddAll: TButton
      Left = 248
      Top = 64
      Width = 57
      Height = 21
      Caption = '>>'
      TabOrder = 3
      OnClick = actAddAllExecute
    end
    object btnDelete: TButton
      Left = 248
      Top = 88
      Width = 57
      Height = 21
      Caption = '<'
      TabOrder = 4
      OnClick = actDeleteExecute
    end
    object btnDeleteAll: TButton
      Left = 248
      Top = 112
      Width = 57
      Height = 21
      Caption = '<<'
      TabOrder = 5
      OnClick = actDeleteAllExecute
    end
    object btnSelAll: TButton
      Left = 248
      Top = 192
      Width = 57
      Height = 21
      Caption = #20840#36873'(&A)'
      TabOrder = 6
      OnClick = actSelAllExecute
    end
    object btnSelNone: TButton
      Left = 248
      Top = 216
      Width = 57
      Height = 21
      Caption = #19981#36873'(&D)'
      TabOrder = 7
      OnClick = actSelNoneExecute
    end
    object btnSelInvert: TButton
      Left = 248
      Top = 240
      Width = 57
      Height = 21
      Caption = #21453#21521'(&I)'
      TabOrder = 8
      OnClick = actSelInvertExecute
    end
    object lbDest: TListBox
      Left = 311
      Top = 31
      Width = 260
      Height = 233
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ItemHeight = 14
      MultiSelect = True
      ParentFont = False
      TabOrder = 1
      OnDblClick = actDeleteExecute
    end
    object cbbSourceOrderStyle: TComboBox
      Left = 65
      Top = 271
      Width = 113
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 9
      Text = #40664#35748#25490#24207
      OnChange = DoUpdateSourceOrder
      Items.Strings = (
        #40664#35748#25490#24207
        #25353#32452#20214#21517#31216#25490#24207
        #25353#32452#20214#31867#22411#25490#24207)
    end
    object cbbSourceOrderDir: TComboBox
      Left = 184
      Top = 270
      Width = 57
      Height = 21
      Style = csDropDownList
      ItemIndex = 0
      TabOrder = 10
      Text = #21319#24207
      OnChange = DoUpdateSourceOrder
      Items.Strings = (
        #21319#24207
        #38477#24207)
    end
    object btnMoveToTop: TButton
      Left = 312
      Top = 270
      Width = 58
      Height = 22
      Caption = #31227#21040#39030#37096
      TabOrder = 11
      OnClick = actMoveToTopExecute
    end
    object btnMoveToBottom: TButton
      Left = 369
      Top = 270
      Width = 58
      Height = 22
      Caption = #31227#21040#24213#37096
      TabOrder = 12
      OnClick = actMoveToBottomExecute
    end
    object btnMoveUp: TButton
      Left = 426
      Top = 270
      Width = 58
      Height = 22
      Caption = #19978#31227#19968#26684
      TabOrder = 13
      OnClick = actMoveUpExecute
    end
    object btnMoveDown: TButton
      Left = 483
      Top = 270
      Width = 58
      Height = 22
      Caption = #19979#31227#19968#26684
      TabOrder = 14
      OnClick = actMoveDownExecute
    end
  end
  object btnOK: TButton
    Left = 424
    Top = 454
    Width = 75
    Height = 31
    Caption = #30830#23450'(&O)'
    Default = True
    TabOrder = 3
    OnClick = btnOKClick
  end
  object btnCancel: TButton
    Left = 504
    Top = 454
    Width = 75
    Height = 31
    Cancel = True
    Caption = #21462#28040'(&C)'
    ModalResult = 2
    TabOrder = 4
    OnClick = btnCancelClick
  end
end
