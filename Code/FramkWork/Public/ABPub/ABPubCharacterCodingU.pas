{
字符编码及转换单元
ASCII GB Big5 Unicode 都是字符的编码
}
unit ABPubCharacterCodingU;

interface           
{$I ..\ABInclude.ini}

uses
  abPubConstU,
  ABPubVarU,
  ABPubFuncU,

  Windows,Classes,SysUtils;


//Big5与GB内码转换(Big5为繁体自己的字符集)
//GB转Big5
function ABGBToBig5(aStr: string): string;
//Big5转GB
function ABBig5ToGB(aStr: string): string;

//GB字符集内内码转换(GB内包括简体与繁体)
//GB繁转简
function ABGBChtToGBChs(aStr: string): string;
//GB简转繁
function ABGBChsToGBCht(aStr: string): string;

//D2009及以后简到繁,2009后都使用了Unicode
//D2009及以后简转繁
function ABUnicodeJanToFan(Source : String) : String;
//D2009及以后繁转简
function ABUnicodeFanToJan(Source : String) : String;

//DFMW文件中的汉字编码转汉字
function ABDFMToChina(const aStr: string): string;
//汉字转DFMW文件中的汉字编码
function ABChinaToDFM(const aStr: string): string;


//判断语言是否是繁体
function ABLanguageIsBig5(aLanguage: string): Boolean;
//判断语言是否是简体
function ABLanguageIsGB(aLanguage: string): Boolean;

//语言转化为中文字符集类型
function ABLanguageToChineseType(aLanguage: string): TABChineseType;

//取得系统环境的字符集
function ABGetSysChineseType: TABChineseType;

//汉字的开始位置
function ABChinesePosition(aStr: string): integer;

//根据类型返回汉字的 1:拼音  2:五笔  3:拼音首字母  4:五笔首字母
function ABGetPYWB_List(const HZ: String;aType:LongInt): string;
//取汉字的拼音首字母
function ABGetFirstPy(const HZ: String): string;
//取汉字的全字母
function ABGetAllPy(const HZ: String): string;
//取汉字的五笔型首字母
function ABGetFirstWB(const HZ: String): string;
//取汉字的五笔型全字母
function ABGetAllWB(const HZ: String): string;


implementation

const
  GBCodePage   = 936;
  Big5CodePage = 950;

var
  FPYList: TStringList;
  FPYList_SM: TStringList;
  //系统环境的字符集
  FSysChineseType: TABChineseType;

function ABUnicodeJanToFan(Source : String) : String;
begin
  SetLength(Result,Length(Source));
  LCMapString(GetUserDefaultLCID,LCMAP_TRADITIONAL_CHINESE,
    PChar(Source),Length(Source),
    PChar(Result),Length(Result));
end;

function ABUnicodeFanToJan(Source : String) : String;
begin
  SetLength(Result,Length(Source));
  LCMapString(GetUserDefaultLCID,LCMAP_SIMPLIFIED_CHINESE,
    PChar(Source),Length(Source),
    PChar(Result),Length(Result));
end;

function ABGetSysChineseType: TABChineseType;
begin
  Result := ctnull;
  //******************繁體操作系统下*******************
  if ABGetSysLanguage=wlChinaTw then
  begin
    Result := ctBig5;
  end
    //******************簡體操作系统下*******************
  else if ABGetSysLanguage=wlChina then
  begin
    Result := ctGBChs;
  end;
end;

function ABLanguageToChineseType(aLanguage: string): TABChineseType;
begin
  Result := ctnull;
  //******************繁體操作系统下*******************
  if ABGetSysLanguage=wlChinaTw then
  begin
    if ABLanguageIsBig5(aLanguage) then
    begin
      Result := ctBig5;
    end
    else if ABLanguageIsGB(aLanguage) then
    begin
      Result := ctGBChs;
    end
  end
    //******************簡體操作系统下*******************
  else if ABGetSysLanguage=wlChina then
  begin
    if ABLanguageIsBig5(aLanguage) then
    begin
      Result := ctGBCht;
    end
    else if ABLanguageIsGB(aLanguage) then
    begin
      Result := ctGBChs;
    end
  end;
end;

function ABLanguageIsBig5(aLanguage: string): Boolean;
begin
  result :=
    (AnsiCompareText(aLanguage,'ChinaTw') = 0) or
    (AnsiCompareText(aLanguage,'ChinaHK') = 0) or
    (
     (ABGetSysLanguage=wlChinaTw) and
     (AnsiCompareText(aLanguage,'Auto') = 0)
    );
end;

function ABLanguageIsGB(aLanguage: string): Boolean;
begin
  result :=
    (AnsiCompareText(aLanguage,'China') = 0) or
    (
     (ABGetSysLanguage=wlChina) and
     (AnsiCompareText(aLanguage,'Auto') = 0)
    );
end;


procedure ABFreePYList;
begin
  FPYList.Free;
  FPYList_SM.Free;
end;

procedure ABInitPYList;
var
  i,tempBeg,tempEnd:LongInt;
  tempSM,tempSM_Pri:string;
begin
  //加载资源文件,将内容赋值给 s
  FPYList := TStringList.Create;
  FPYList_SM := TStringList.Create;
  if ABCheckFileExists(ABSoftSetPath+'FPYList.ini') then
  begin
    FPYList.LoadFromFile(ABSoftSetPath+'FPYList.ini');
  end
  else if ABCheckFileExists(ABPublicSetPath+'FPYList.ini') then
  begin
    FPYList.LoadFromFile(ABPublicSetPath+'FPYList.ini');
  end
  else if ABCheckFileExists(ABAPPPath+'FPYList.ini') then
  begin
    FPYList.LoadFromFile(ABAPPPath+'FPYList.ini');
  end;
  
  for i := 0 to FPYList.Count-1 do
  begin
    tempBeg:=pos('③',FPYList.Strings[i]);
    tempEnd:=pos('④',FPYList.Strings[i]);
    tempSM:=Copy(FPYList.Strings[i],tempBeg+1,tempEnd-tempBeg-1);
    FPYList.Strings[i]:=tempSM+'='+FPYList.Strings[i];
  end;
  FPYList.Sort;
  tempSM_Pri:='';
  for i := 0 to FPYList.Count-1 do
  begin
    tempSM:=FPYList.Names[i];
    if compareStr(tempSM,tempSM_Pri)<>0 then
    begin
      FPYList_SM.Add(tempSM+'='+inttostr(1));
    end
    else
    begin
      FPYList_SM.ValueFromIndex[FPYList_SM.Count-1]:=inttostr(StrToInt(FPYList_SM.ValueFromIndex[FPYList_SM.Count-1])+1);
    end;

    tempSM_Pri:=tempSM;
  end;
end;

function ABGetAllPy(const HZ: String): string;
begin
  Result:=ABGetPYWB_List(HZ,1);
end;

function ABGetFirstWB(const HZ: String): string;
begin
  Result:=ABGetPYWB_List(HZ,4);
end;

function ABGetAllWB(const HZ: String): string;
begin
  Result:=ABGetPYWB_List(HZ,2);
end;
//1: 拼音  2:五笔  3: 拼音简码  4:五笔简码
function ABGetPYWB_List(const HZ: String;aType:LongInt): string;
var
  I: Integer;
  tempCurChar: Char;
  //只单独汉字:
  function ABGetCharPy(HZ: AnsiString): Char;
  begin
    case LoWord(HZ[1]) shl 8 + LoWord(HZ[2]) of
      $B0A1..$B0C4: Result := 'A';
      $B0C5..$B2C0: Result := 'B';
      $B2C1..$B4ED: Result := 'C';
      $B4EE..$B6E9: Result := 'D';
      $B6EA..$B7A1: Result := 'E';
      $B7A2..$B8C0: Result := 'F';
      $B8C1..$B9FD: Result := 'G';
      $B9FE..$BBF6: Result := 'H';
      $BBF7..$BFA5: Result := 'J';
      $BFA6..$C0AB: Result := 'K';
      $C0AC..$C2E7: Result := 'L';
      $C2E8..$C4C2: Result := 'M';
      $C4C3..$C5B5: Result := 'N';
      $C5B6..$C5BD: Result := 'O';
      $C5BE..$C6D9: Result := 'P';
      $C6DA..$C8BA: Result := 'Q';
      $C8BB..$C8F5: Result := 'R';
      $C8F6..$CBF9: Result := 'S';
      $CBFA..$CDD9: Result := 'T';
      $CDDA..$CEF3: Result := 'W';
      $CEF4..$D188: Result := 'X';
      $D1B9..$D4D0: Result := 'Y';
          $D9F0        : result := 'Y'; //兖
      $D4D1..$D7F9: Result := 'Z';
          $F6CE        : Result := 'X';
  //    63182      : Result := 'X';

    else
      Result := Char(0);
    end;
  end;


  function ABGetPYWB_Char: string;
  var
    tempSM, tempCurcolStr: string;
    i,j: integer;
  begin
    result := EmptyStr;
    i := 0;
    tempSM:=ABGetCharPy(AnsiString(tempCurChar));
    if tempSM=EmptyStr then
    begin
      while i <= FPYList.Count - 1 do
      begin
        if compareStr(tempCurChar,FPYList.ValueFromIndex[i][1])=0 then
        begin
          tempCurcolStr := FPYList.ValueFromIndex[i];
          Break;
        end;
        i := i + 1;
      end;
    end
    else
    begin
      while i <= FPYList.Count - 1 do
      begin
        if (compareStr(tempSM,FPYList.Names[i])<>0) then
        begin
          i := i + StrToInt(FPYList_SM.Values[FPYList.Names[i]]);
        end
        else
        begin
          if compareStr(tempCurChar,FPYList.ValueFromIndex[i][1])=0 then
          begin
            tempCurcolStr := FPYList.ValueFromIndex[i];
            Break;
          end;
          i := i + 1;
        end;
      end;
    end;

    case aType of
      1:
      begin
        i:=pos('①', tempCurcolStr);
        j:=pos('②', tempCurcolStr);
        result:=Copy(tempCurcolStr,i+1,j-i-1);
      end;
      2:
      begin
        i:=pos('②', tempCurcolStr);
        j:=pos('③', tempCurcolStr);
        result:=Copy(tempCurcolStr,i+1,j-i-1);
      end;
      3:
      begin
        i:=pos('③', tempCurcolStr);
        j:=pos('④', tempCurcolStr);
        result:=Copy(tempCurcolStr,i+1,j-i-1);
      end;
      4:
      begin
        i:=pos('④', tempCurcolStr);
        j:=100;
        result:=Copy(tempCurcolStr,i+1,j-i-1);
      end;
    end;
  end;
begin
  Result := EmptyStr;
  if HZ=EmptyStr then
    exit;

  if not Assigned(FPYList) then
    ABInitPYList;

  i := 1;
  while i <= length(HZ) do   //提取汉字字符
  begin
    tempCurChar:=HZ[I];
    if tempCurChar<>' ' then
    begin
      if ABIsChinese(tempCurChar) then
      begin
        ABAddstr(Result,ABGetPYWB_Char,'');
      end
      else
      begin
        ABAddstr(Result,tempCurChar,'');
      end;
    end;
    i := i + 1;
  end;
  Result:=Trim(Result);
end;

//取汉字的拼音首字母
function ABGetFirstPy(const HZ: String): string;
begin
  Result:=ABGetPYWB_List(HZ,3);
end;

function ABChinesePosition(aStr: string): integer;
var
  i:LongInt;
begin
  result := 0;
  for I := 1 to Length(aStr) do
  begin
    if ABIsChinese(aStr[i]) then
    begin
      result := i;
      break;
    end;
  end;
end;

function ABDFMToChina(const aStr: string): string;
var
  tempstr, tempstr1: string;
  i: LongInt;
begin
  Result := EmptyStr;
  for I := 1 to ABGetSpaceStrCount(aStr, '#') do
  begin
    tempstr := ABGetSpaceStr(aStr, i, '#');
    if tempstr<>EmptyStr then
    begin
      tempstr1 := Copy(tempstr, 1, 5);
      if Length(tempstr) >= 5 then
      begin
        tempstr := Copy(tempstr, 6, Length(tempstr));
      end;
      Result := Result + widechar(strtoint(tempstr1)) + tempstr;
    end;
  end;
end;

function ABChinaToDFM(const aStr: string): string;
var
  tempWideChar: Char;
  i, j: LongInt;
  tempPriChinese: boolean;
begin
  Result := QuotedStr(aStr);
  if not ABHaveChinese(aStr) then
    exit;

  tempPriChinese := true;
  Result := EmptyStr;
  i := 1;
  j := 1;
  while i <= Length(aStr) do
  begin
    tempWideChar := aStr[j];
    if ABHaveChinese(tempWideChar) then
    begin
      if tempPriChinese then
        Result := Result + '#' + IntToStr(Ord(tempWideChar))
      else
        Result := Result + '''' + '#' + IntToStr(Ord(tempWideChar));

      i := i + 1;
      tempPriChinese := true;
    end
    else
    begin
      if tempPriChinese then
        Result := Result + '''' + (tempWideChar)
      else
        Result := Result + tempWideChar;

      i := i + 1;
      tempPriChinese := false;
    end;
    j := j + 1;
  end;

  if not tempPriChinese then
    Result := Result + '''';
end;

function ABBig5ToGB(aStr: string): string;
{进行big5转GB内码}
var
  Len: Integer;
  pBIG5Char: PChar;
  pGBCHSChar: PChar;
  pGBCHTChar: PChar;
  pUniCodeChar: PWideChar;
begin
  pGBCHTChar := nil;
  pGBCHSChar := nil;
  pUniCodeChar := nil;
  try
    //String -> PChar
    pBIG5Char := PChar(aStr);
    Len := MultiByteToWideChar(Big5CodePage, 0, PAnsiChar(pBIG5Char), -1, nil, 0);

    GetMem(pUniCodeChar, Len * 2);
    ZeroMemory(pUniCodeChar, Len * 2);
    //Big5 -> UniCode
    MultiByteToWideChar(Big5CodePage, 0, PAnsiChar(pBIG5Char), -1, pUniCodeChar, Len);
    Len := WideCharToMultiByte(GBCodePage, 0, pUniCodeChar, -1, nil, 0, nil, nil);
    GetMem(pGBCHTChar, Len * 2);
    GetMem(pGBCHSChar, Len * 2);
    ZeroMemory(pGBCHTChar, Len * 2);
    ZeroMemory(pGBCHSChar, Len * 2);
    //UniCode->GB CHT
    WideCharToMultiByte(GBCodePage, 0, pUniCodeChar, -1, PAnsiChar(pGBCHTChar), Len, nil, nil);
    //GB CHT -> GB CHS
    LCMapString($804, LCMAP_SIMPLIFIED_CHINESE, pGBCHTChar, -1, pGBCHSChar,
      Len);
    Result := string(pGBCHSChar);
  finally
    FreeMem(pGBCHTChar);
    FreeMem(pGBCHSChar);
    FreeMem(pUniCodeChar);
  end;
end;

function ABGBToBig5(aStr: string): string;
{进行GB转BIG5内码}
var
  Len: Integer;
  pGBCHTChar: PChar;
  pGBCHSChar: PChar;
  pUniCodeChar: PWideChar;
  pBIG5Char: PChar;
begin
  pBIG5Char := nil;
  pGBCHTChar := nil;
  pUniCodeChar := nil;
  try
    pGBCHSChar := PChar(aStr);
    Len := MultiByteToWideChar(GBCodePage, 0, PAnsiChar(pGBCHSChar) , -1, nil, 0);
    GetMem(pGBCHTChar, Len * 2 + 1);
    ZeroMemory(pGBCHTChar, Len * 2 + 1);
    //GB CHS -> GB CHT
    LCMapString($804, LCMAP_TRADITIONAL_CHINESE, pGBCHSChar, -1, pGBCHTChar, Len
      * 2);
    GetMem(pUniCodeChar, Len * 2);
    ZeroMemory(pUniCodeChar, Len * 2);
    //GB CHT -> UniCode
    MultiByteToWideChar(GBCodePage, 0,  PAnsiChar(pGBCHTChar), -1, pUniCodeChar, Len * 2);
    Len := WideCharToMultiByte(Big5CodePage, 0, pUniCodeChar, -1, nil, 0, nil, nil);
    GetMem(pBIG5Char, Len);
    ZeroMemory(pBIG5Char, Len);
    //UniCode -> Big5
    WideCharToMultiByte(Big5CodePage, 0, pUniCodeChar, -1,  PAnsiChar(pBIG5Char), Len, nil, nil);
    Result := string(pBIG5Char);
  finally
    FreeMem(pBIG5Char);
    FreeMem(pGBCHTChar);
    FreeMem(pUniCodeChar);
  end;
end;

//GBK内码 繁体->简体
function ABGBChtToGBChs(aStr: string): string;
var
  tempLen: Integer;
  tempResultStr: PChar;
begin
  tempLen:=Length(aStr);
  GetMem(tempResultStr,  tempLen* 2 + 1);
  try
    ZeroMemory(tempResultStr, tempLen * 2 + 1);
    //GB CHS -> GB CHT
    LCMapString($804, LCMAP_SIMPLIFIED_CHINESE, PChar(aStr), -1, tempResultStr, tempLen* 2);
    Result := string(tempResultStr);
  finally
    FreeMem(tempResultStr);
  end;
end;

//GBK内码 简体->繁体
function ABGBChsToGBCht(aStr: string): string;
var
  tempLen: Integer;
  tempResultStr: PChar;
begin
  tempLen:=Length(aStr);
  GetMem(tempResultStr,  tempLen* 2 + 1);
  try
    ZeroMemory(tempResultStr, tempLen * 2 + 1);
    //GB CHS -> GB CHT
    LCMapString($804, LCMAP_TRADITIONAL_CHINESE, PChar(aStr), -1, tempResultStr, tempLen* 2);
    Result := string(tempResultStr);
  finally
    FreeMem(tempResultStr);
  end;
end;

procedure ABFinalization;
begin
  ABFreePYList;
end;

procedure ABInitialization;
begin
  FSysChineseType:=ABGetSysChineseType;
end;

Initialization
  ABInitialization;
Finalization
  ABFinalization;

end.


{


//繁體操作系统下,繁到簡,要設置FONT的字体类型
//设置元件字体类型（Font.CharSet）->多个元件 134为'GB2312_CHARSET'
procedure ABAllAutoSetCharSet(aOwner: TComponent);
//设置元件字体类型（Font.CharSet）->单个元件
procedure ABOneAutoSetCharSet(aOwner: TComponent);


//利用WORD进行Big5与GB内码转换转换,使用前要先安装WORD软件,显示繁体字控件的字符集为简体就行 ,速度很慢
//简转繁
function ABGBToBIG5ByWord(aStr: string): string;
//繁转简
function ABBIG5ToGBByWord(aStr: string): string;

//TWordConvertMethod方式实现-----begin-----
function ABGBToUnicode(const GBStr: String): WideString;
function ABUnicodeToGB(const WStr: WideString): String;

function ABBig5ToUnicode(const BIG5Str: String): WideString;
function ABUnicodeToBig5(const WStr: WideString): String;

//功能应和ABGBToBig5与ABBig5ToGB一样
function ABGBToBig5_Ext(const GBStr: String): String;
function ABBig5ToGB_Ext(const Big5Str: String): String;

function ABGBToBig5Unicode(const GBStr: String): WideString;
function ABBig5UnicodeToGB(const WBig5Str: WideString): String;

function ABBig5ToGBUnicode(const Big5Str: String): WideString;
function ABGBUnicodeToBig5(const WGBStr: WideString): String;
//TWordConvertMethod方式实现-----end-----

//区位是GB编码的保存位置号
//查汉字区位码
function ABStrToGB(const s: AnsiString): string;
//通过区位码查汉字
function ABGBToStr(const n: Word): string;
//字串到Unicode数字
function ABStrToUnicode(aStr: string): String;
//Unicode数字到字串
function ABUnicodeToStr(aStr: string):string;



type
  TWordConvertMethod = function (const WStr: WideString): WideString;
  TMyDWordRec = packed record
    case Integer of
      0: (Value: DWORD);
      1: (Lo, Hi: Word);
  end;

var
  BIG5WordConvert_ToBig5,
  BIG5WordConvert_ToGB,
  GBWordConvert_ToBig5,
  GBWordConvert_ToGB:TWordConvertMethod;


////////////////////////////////////////////////////////////////////////////////
//这几个都是VC中的宏定义,Delphi中没有所以自己写一下
function ABMAKELCID(const wLanguageID, wSortID: Word): DWORD;
var
  Rec: TMyDWordRec;
begin
  Rec.Lo := wLanguageID;
  Rec.Hi := wSortID;
  Result := Rec.Value;
end;


function ABMAKELANGID(const usPrimaryLanguage, usSubLanguage: Word): Word;
begin
  Result := Word((usSubLanguage shl 10)) or usPrimaryLanguage;
end;

function ABChkErr(iVal: Integer): Integer;
begin
  Result := iVal;
  if Result = 0 then
    RaiseLastOSError;
end;


function ABMapChineseWString(const WStr: WideString;
  const dwMapFlags: Integer; const Locale: Integer = 0): WideString;
var
  LocaleID:  LCID;
  iLen: Integer;
begin
  Result := '';
  if WStr = '' then
    exit;
  if Locale = 0 then
  begin
    case dwMapFlags of
      LCMAP_SIMPLIFIED_CHINESE:
        LocaleID := ABMAKELCID(ABMAKELANGID(LANG_CHINESE,
          SUBLANG_CHINESE_SIMPLIFIED), SORT_CHINESE_PRC);
      LCMAP_TRADITIONAL_CHINESE:
        LocaleID := ABMAKELCID(ABMAKELANGID(LANG_CHINESE,
          SUBLANG_CHINESE_TRADITIONAL), SORT_CHINESE_BIG5);
    else
      LocaleID := 0;
    end;
  end
  else
    LocaleID := Locale;

  iLen := ABChkErr(LCMapStringW(LocaleID, dwMapFlags, PWideChar(WStr),
                             -1,  nil, 0));

  if iLen = 0 then
    exit;
  SetLength(Result, iLen - 1);
  ABChkErr(LCMapStringW(LocaleID, dwMapFlags, PWideChar(WStr), -1,
                     PWideChar(Result), iLen));
end;

function ABStrToUnicode(const S: String; const CodePage: DWord): WideString;
var
  iLen: Integer;
begin
  Result := '';
  if S = '' then
    exit;
  iLen := ABChkErr(MultiByteToWideChar(CodePage, 0, PansiChar(S), -1, nil, 0));
  SetLength(Result, iLen - 1);
  ABChkErr(MultiByteToWideChar(CodePage, 0, PansiChar(S), -1,
       PWideChar(Result), iLen));
end;

function ABUnicodeToStr(const WStr: WideString; const CodePage: DWord): String;
var
  iLen: Integer;
begin
  Result := '';
  if WStr = '' then
    exit;
  iLen := ABChkErr(WideCharToMultiByte(CodePage, 0,
     PWChar(WStr), -1, nil, 0, nil, nil));
  SetLength(Result, iLen - 1);
  ABChkErr(WideCharToMultiByte(CodePage, 0, PWChar(WStr), -1, PansiChar(Result), iLen,
       nil, nil));
end;


  ClearWordConvertMetods;
procedure ClearWordConvertMetods;
begin
  BIG5WordConvert_ToBig5 := nil;
  BIG5WordConvert_ToGB   := nil;
  GBWordConvert_ToBig5   := nil;
  GBWordConvert_ToGB     := nil;
end;


function ABWordConvert(const WStr: WideString;
  ConvertMethod: TWordConvertMethod): WideString;
begin
  if Assigned(ConvertMethod) and (WStr <> '') then
    Result := ConvertMethod(WStr)
  else
    Result := WStr;
end;

function ABDoBig5WordConvert_ToBig5(const WStr: WideString): WideString;
begin
  Result := ABWordConvert(WStr, BIG5WordConvert_ToBig5);
end;

function ABDoBig5WordConvert_ToGB(const WStr: WideString): WideString;
begin
  Result := ABWordConvert(WStr, BIG5WordConvert_ToGB);
end;

function ABDoGBWordConvert_ToBig5(const WStr: WideString): WideString;
begin
  Result := ABWordConvert(WStr, GBWordConvert_ToBig5);
end;

function ABDoGBWordConvert_ToGB(const WStr: WideString): WideString;
begin
  Result := ABWordConvert(WStr, GBWordConvert_ToGB);
end;

function ABGBToUnicode(const GBStr: String): WideString;
begin
  Result := ABStrToUnicode(GBStr, GBCodePage);
end;

function ABUnicodeToGB(const WStr: WideString): String;
begin
  Result :=  ABUnicodeToStr(
    ABMapChineseWString(WStr, LCMAP_SIMPLIFIED_CHINESE), GBCodePage);
end;

function ABBig5ToUnicode(const BIG5Str: String): WideString;
begin
  Result := ABStrToUnicode(BIG5Str, Big5CodePage);
end;

function ABUnicodeToBig5(const WStr: WideString): String;
begin
  Result := ABUnicodeToStr(
    ABMapChineseWString(WStr, LCMAP_TRADITIONAL_CHINESE), Big5CodePage);
end;

function ABGBToBig5Unicode(const GBStr: String): WideString;
begin
  Result := ABDoBig5WordConvert_ToBig5(ABMapChineseWString(
    ABDoGBWordConvert_ToBig5(ABStrToUnicode(GBStr, GBCodePage)),
      LCMAP_TRADITIONAL_CHINESE));
end;

function ABBig5UnicodeToGB(const WBig5Str: WideString): String;
begin
  Result := ABUnicodeToStr(ABDoGBWordConvert_ToGB(ABMapChineseWString(
    ABDoBig5WordConvert_ToGB(WBig5Str), LCMAP_SIMPLIFIED_CHINESE)), GBCodePage);
end;

function ABBig5ToGBUnicode(const Big5Str: String): WideString;
begin
  Result := ABDoGBWordConvert_ToGB(ABMapChineseWString(
    ABDoBig5WordConvert_ToGB(ABStrToUnicode(BIG5Str, Big5CodePage)),
      LCMAP_SIMPLIFIED_CHINESE));
end;

function ABGBUnicodeToBig5(const WGBStr: WideString): String;
begin
  Result := ABUnicodeToStr(ABDoBig5WordConvert_ToBig5(
    ABMapChineseWString(ABDoGBWordConvert_ToBig5(WGBStr),
      LCMAP_TRADITIONAL_CHINESE)), Big5CodePage);
end;

function ABBig5ToGB_Ext(const Big5Str: String): String;
begin
  if Big5Str = '' then
    Result := ''
  else
    Result := ABUnicodeToStr(ABBig5ToGBUnicode(Big5Str), GBCodePage);
end;

function ABGBToBig5_Ext(const GBStr: String): String;
begin
  if GBStr = '' then
    Result := ''
  else
    Result := ABUnicodeToStr(ABGBToBig5Unicode(GBStr), Big5CodePage);
end;

function ABGBToBIG5ByWord(aStr: string): string;
var
  app: olevariant;
  ABOleWord: olevariant;
begin
  ABOleWord := createoleobject('Word.Document');
  app := ABOleWord.application;
  app.Selection.typetext(aStr);
  app.Selection.wholestory;
  app.selection.select;
  app.WordBasic.ToolsSCTCTranslate(0, 0, 0); //简转繁
  Result := app.selection.text;
  delete(Result, length(Result), 1);
  ABOleWord.close(0);
end;

function ABBIG5ToGBByWord(aStr: string): string;
var
  app: olevariant;
  ABOleWord: olevariant;
begin
  ABOleWord := createoleobject('Word.Document');
  app := ABOleWord.application;
  app.Selection.typetext(aStr);
  app.Selection.wholestory;
  app.selection.select;
  app.WordBasic.ToolsTCSCTranslate(0, 0, 0); //繁转简
  Result := app.selection.text;
  delete(Result, length(Result), 1);
  ABOleWord.close(0);
end;

procedure ABAllAutoSetCharSet(aOwner: TComponent);
var
  i:LongInt;
begin
  ABOneAutoSetCharSet(aOwner);
  for I := 0 to aOwner.ComponentCount - 1 do
  begin
    ABOneAutoSetCharSet(aOwner.Components[i]);
  end;
end;

procedure ABOneAutoSetCharSet(aOwner: TComponent);
var
  tempCharSet: string;
begin
  //繁體操作系统下,繁到簡,要設置FONT的字符集 ,134-->'GB2312_CHARSET'
  tempCharSet := EmptyStr;
  if (ABGetSysLanguage=wlChinaTw) and (ABLanguageIsGB(ABLocalParams.Language)) then
  begin
    tempCharSet := '134';
  end
  else if (ABGetSysLanguage=wlChina) then
  begin
    tempCharSet := '1';
  end;

  if tempCharSet <> EmptyStr then
  begin
    ABSetPropValue(aOwner, 'Font.CharSet', tempCharSet);
    if aOwner.ClassType = TLabeledEdit then
    begin
      ABSetPropValue(aOwner, 'EditLabel.Font.CharSet', tempCharSet);
    end;
  end;
end;


//查汉字区位码
function ABStrToGB(const s: AnsiString): string;
const
  G = 160;
begin
  Result := Format('%d%d', [Ord(s[1])-G, Ord(s[2])-G]);
end;
//通过区位码查汉字
function ABGBToStr(const n: Word): string;
const
  G = 160;
begin
  Result := string(AnsiChar(n div 100 + G) + AnsiChar(n mod 100 + G));
end;

//转换
function ABStrToUnicode(aStr: string): String;
var
  i,len: Integer;
  cur: Integer;
  ws: WideString;
begin
  Result := '';
  ws := aStr;
  len := Length(ws);
  i := 1;
  while i <= len do
  begin
    cur := Ord(ws[i]);
    Result := Result + IntToStr(cur);
    Inc(i);
  end;
end;
//恢复
function ABUnicodeTostr(aStr: string):string;
var
  i,len: Integer;
  ws: WideString;
begin
  ws := '';
  i := 1;
  len := Length(aStr);
  while i < len do
  begin
    ws := ws + char(StrToInt('$' + Copy(aStr,i,4)));
    i := i+4;
  end;
  Result := ws;
end;

}
