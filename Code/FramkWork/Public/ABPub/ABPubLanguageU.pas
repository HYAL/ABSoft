{
多语言组件单元
Delphi 2009 新增单元 Character[2]: IsLetter、IsUpper、IsLower、IsDigit、IsNumber
IsLetter: 是否是个字母; 范围 A..Z 与 a..z
IsUpper:  是否是个大写字母; 范围 A..Z
IsLower:  是否是个小写字母; 范围 a..z
IsDigit:  是否是个十进制数字; 范围 0..9
IsNumber: 是否是个数字符号; 范围包括 0..9, 还有 ASCII 码中的 178、179、185、188、189、190 等
}
unit ABPubLanguageU;
{
  //10100~10100 为语言转换相关
  Tag=10101 //此控件不进行语言的转换
}

interface
{$I ..\ABInclude.ini}

uses
  abPubConstU,
  ABPubVarU,
  ABPubFuncU,
  ABPubDBU,

  ABPubLocalParamsU,
  ABPubCharacterCodingU,

  Classes,Menus,SysUtils,Forms,Variants;

type
  //ttSysCharType=系统环境字符集,用于控件属性
  //ttSourceCodeCharType=源码字符集 ，用于pas 中字串
  TABBTransitionStrType=(ttSysCharType,ttSourceCodeCharType);
  //语言类
  TABLanguage = class(TComponent)
  private
    //当前语言是否是中文
    FchinaLanguage:Boolean;
    //内部定义的翻译组件属性名,由编码者提供最基础的属性列表
    FInheritedTransitionPropertyNames: TStrings;
    //当前语言资源是否修改过
    FLanguageChang: Boolean;

    //根据定位标识从语言资源中取得字符串,由GetPropertyStr与GetCodeStr调用
    //aID=定位的标识,aStr=要转换的字串,aStrType=属性或pas字符串（属性是系统环境字符集，pas字符串是源码字符集）
    //aMulLine=字串是否多行,aSave=是否保存到语言内容的INI文件中
    function GetStr(aID: string; aStr: string;aStrType:TABBTransitionStrType;aMulLine:Boolean;aSave: Boolean): string;

    //字符串aStr由原字符集转化为目标字符集字符串
    function TransitionChineseType(aStr: string; aSrcType, aDesType: TABChineseType): string;

    //切换语言菜单时的动作
    procedure MenuItemOnClick(Sender: TObject);

    //加载语言资源（切换语言后调用）
    procedure LoadLanguage;
    //保存语言资源（切换语言前调用）
    procedure SaveLanguage;

    //在传入菜单项下创建语言子菜单
    procedure CreateMenuItems(aMenuItems: TMenuItem);
    //刷新所有的窗体使用新语言资源
    procedure ReFreshFormsLanguage;
    //检测是否可以改变语言
    function CheckCanChangeLanguage(const aLanguage: string): Boolean;

    //设置当前的语言菜单选中
    procedure SetCurLangeuageMenuItem(aMenuItems: TMenuItem);
  private
    FLanguage: string;
    FLanguageFileName: string;
    FNotTransitionFormComponentNames: TStrings;
    FNotTransitionFormNames: TStrings;
    FNotTransitionFormComponentPropertyNames: TStrings;
    FItems: TStrings;
    FExtTransitionPropertyNames: TStrings;
    FFormsConrtosPropertys: TStrings;

    procedure SetLanguage(const Value: string);
    procedure SetExtTransitionPropertyNames(const Value: TStrings);
    procedure SetNotTransitionFormComponentNames(const Value: TStrings);
    procedure SetNotTransitionFormComponentPropertyNames(const Value: TStrings);
    procedure SetNotTransitionFormNames(const Value: TStrings);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  public
    //在传入菜单项下创建语言菜单,并设置当前语言选中
    procedure ReFreshMenuItems(aMenuItems: TMenuItem);

    //根据当前语言设置窗体与其中控件的字符串
    procedure SetComponentsText(aForm: TComponent);
    procedure SetComponentText( aParentSign, aObjectSign: string;
                                aObject: TObject
                                );
    //设置集合的字项翻译
    procedure SetCollectionText(aParentSign: string; aCollection: TCollection);

    //根据当前语言取得字符串(用于DFM窗体中的组件),组件属性的原值存放在 FormsConrtosPropertys
    //aStr=组件属性的源字符串
    function GetPropertyStr(aFormName,aObjectName,aPropertyName: string; aStr: string): string;

    //根据当前语言取得字符串（用于PAS文件）
    //aStr= 源字符串 ,aSave= 是否保存到语言内容的INI文件中
    function GetCodeStr(aStr: string;
                        aSave: Boolean = true): string;overload;
    //aStr =源字符串 ,aFormatValues= aStr中参数值,aSave= 是否保存到语言内容的INI文件中
    function GetCodeStr(aStr: string;
                        aFormatValues:array of const;
                        aSave: Boolean = true): string;overload;
  published
    //用于保存DFM窗体中的组件属性值,这样当某一语言某属性为空时，再切换到其它语言时传到 GetStr中的aStr就不会为空了
    //name为窗体名称,关联的对象TStrings为此窗体下所有控件属性值，格式为：窗体名.控件名.属性名. = 属性值
    property FormsConrtosPropertys: TStrings  read FFormsConrtosPropertys;
    //语言内容的INI资源文件（其中包括FormsConrtosPropertys）,格式为 源串=当前语言串
    property Items: TStrings  read FItems;
    //当前语言名，如China,ChinaTw
    property Language: string read FLanguage write SetLanguage;
    //当前保存语所字串的INI文件名
    property LanguageFileName: string read FLanguageFileName;

    //以下属性要在窗体 DoCreate 的 inherited前面调用才能使得窗体的多语言有效果,因为继承的基础窗体中是在DoCreate;中实现多语言的
    //扩展翻译属性列表
    property ExtTransitionPropertyNames: TStrings read FExtTransitionPropertyNames write SetExtTransitionPropertyNames;
    //不需要翻译的窗体名(此窗体下所有都不翻译)
    property NotTransitionFormNames: TStrings read FNotTransitionFormNames write SetNotTransitionFormNames;
    //不需要翻译的控件名,格式为窗体名.控件名(此窗体下此控件的所有属性都不翻译)
    property NotTransitionFormComponentNames: TStrings read FNotTransitionFormComponentNames write SetNotTransitionFormComponentNames;
    //不需要翻译的控件属性名,格式为窗体名.控件名.属性名(此窗体下此控件的此属性不翻译)
    property NotTransitionFormComponentPropertyNames: TStrings read FNotTransitionFormComponentPropertyNames write SetNotTransitionFormComponentPropertyNames;
  end;

var
  //多语言对象
  ABLanguage: TABLanguage;

implementation


{TABLanguage}

procedure TABLanguage.SetCollectionText(aParentSign: string; aCollection:TCollection);
var
  I: Integer;
begin
  for I := 0 to aCollection.Count - 1 do
  begin
    SetComponentText(aParentSign, IntToStr(aCollection.Items[i].Index),aCollection.Items[i]);
  end;
end;

procedure TABLanguage.SetComponentsText(aForm: TComponent);
var
  i: LongInt;
begin
  if not ABLocalParams.LanguageEnabled then
    exit;
  if aForm.Name=EmptyStr then
    exit;

  if (FNotTransitionFormNames.Count=0) or
     (FNotTransitionFormNames.IndexOf(aForm.Name)<0) then
  begin
    if (FNotTransitionFormComponentNames.Count=0) or
       (FNotTransitionFormComponentNames.IndexOf(aForm.Name+'.'+aForm.Name)<0) then
    begin
      SetComponentText(aForm.Name, aForm.Name, aForm);
    end;

    for I := 0 to aForm.ComponentCount - 1 do
    begin
      if (FNotTransitionFormComponentNames.Count=0) or
         (FNotTransitionFormComponentNames.IndexOf(aForm.Name+'.'+aForm.Components[i].Name)<0) then
      begin
        SetComponentText(aForm.Name, aForm.Components[i].Name, aForm.Components[i]);
      end;
    end;
  end;
end;

procedure TABLanguage.SetComponentText(
  aParentSign, aObjectSign: string;
  aObject: TObject);
var
  j: LongInt;
  tempObject:TObject;
  tempPropertyName,
  tempPropertyValue,
  tempTransitionPropertyValue : string;
  procedure DoItem;
  begin
    if (FNotTransitionFormComponentPropertyNames.Count=0) or
       (FNotTransitionFormComponentPropertyNames.IndexOf(aParentSign +'.'+ aObjectSign +'.'+tempPropertyName)<0) then
    begin
      if ABIsPublishProp(aObject,tempPropertyName) then
      begin
        tempPropertyValue := ABGetPropValue(aObject, tempPropertyName);
        tempTransitionPropertyValue :=GetPropertyStr(aParentSign,aObjectSign,tempPropertyName,tempPropertyValue);

        if (tempTransitionPropertyValue <> tempPropertyValue) then
          ABSetPropValue(aObject, tempPropertyName, tempTransitionPropertyValue);

        //对集合属性进行处理
        if AnsiCompareText(tempPropertyName,'Items')=0 then
        begin
          tempObject:=ABGetObjectPropValue(aObject,'Items');
          if tempObject is TCollection then
          begin
            ABLanguage.SetCollectionText(aParentSign+'.'+aObjectSign,TCollection(tempObject));
          end;
        end;
        if AnsiCompareText(tempPropertyName,'lines')=0 then
        begin
          tempObject:=ABGetObjectPropValue(aObject,'lines');
          if tempObject is TCollection then
          begin
            ABLanguage.SetCollectionText(aParentSign+'.'+aObjectSign,TCollection(tempObject));
          end;
        end;
      end;
    end;
  end;
begin
  if VarToStrDef(ABGetPropValue(aObject,'Tag'),'')='10101' then
    exit;

  if ABIsDataAware(aObject) then
    exit;

  if aObjectSign=EmptyStr then
    exit;

  for j := 0 to FInheritedTransitionPropertyNames.Count-1 do
  begin
    tempPropertyName := FInheritedTransitionPropertyNames[j];
    DoItem;
  end;

  for j := 0 to FExtTransitionPropertyNames.Count-1 do
  begin
    tempPropertyName := FExtTransitionPropertyNames[j];
    DoItem;
  end;
end;

function  TABLanguage.TransitionChineseType(aStr: string; aSrcType, aDesType: TABChineseType): string;
begin
  Result := aStr;
  if Result=EmptyStr  then
    exit;

  if (not ABHaveChinese(aStr)) or (aSrcType = aDesType) or (aDesType = ctNull) then
    exit;

  {$IF (ABDelphiVersion_int>=200)}   //200=DELPHI 2009
    case aSrcType of
      ctGBChs:
      begin
        if (aDesType=ctGBCht) or (aDesType=ctBig5) then
        begin
          Result := ABUnicodeJanToFan(aStr)
        end;
      end;
      ctGBCht,ctBig5:
      begin
        if (aDesType=ctGBChs) then
        begin
          Result := ABUnicodeFanToJan(aStr)
        end;
      end;
    end;
  {$ELSE}
    case aSrcType of
      ctGBChs:
      case aDesType of
        ctGBCht:
          Result := ABGBChsToGBCht(aStr);
        ctBig5:
            Result := ABGBToBig5(aStr);
      end;
      ctGBCht:
      case aDesType of
        ctGBChs:
          Result := ABGBChtToGBChs(aStr);
        ctBig5:
          Result := ABGBToBig5(aStr);
      end;
      ctBig5:
      case aDesType of
        ctGBChs:
          Result := ABBig5ToGB(aStr);
        ctGBCht:
          Result := ABGBChsToGBCht(ABBig5ToGB(aStr));
      end;
    end;
  {$IFEND}
end;

function TABLanguage.GetPropertyStr(aFormName,aObjectName,aPropertyName: string; aStr: string): string;
var
  I,j: Integer;
  tempStr:string;
  tempFormStrings:TStrings;
  tempMoreLineProperty:Boolean;
begin
  result := aStr;

  i:=FFormsConrtosPropertys.IndexOf(aFormName);
  if i<0 then
  begin
    tempFormStrings:=TStringlist.Create;
    FFormsConrtosPropertys.AddObject(aFormName,tempFormStrings);
  end
  else
  begin
    tempFormStrings:=TStrings(FFormsConrtosPropertys.Objects[i]);
  end;

  //将aStr转换成窗体的源原串
  tempStr:=ABSubStrBegin+aFormName+'.'+aObjectName+'.'+aPropertyName;
  tempMoreLineProperty:=ABIsMoreLineProperty(aPropertyName);
  j:=tempFormStrings.IndexOfName(tempStr);
  if j<0 then
  begin
    if tempMoreLineProperty then
    begin
      result:=Trim(result);
    end;
    //在D2009之前繁体下不能显示简体,在TABPubForm.Create(AOwner: TComponent);中进行了简转繁的操作，此处要再转成代码中的类型进行保存
    {$IF (ABDelphiVersion_int<=185)}      
      if FchinaLanguage then
      begin
        if ABGetSysLanguage=wlChinaTw then
        begin
          result := TransitionChineseType(result,ABLocalParams.LanguageChineseType,ABLocalParams.SourceCodeChineseType);
        end;
      end;
    {$IFEND}
    tempFormStrings.Add(tempStr+'='+result);
  end
  else
  begin
    if tempMoreLineProperty  then
    begin
      Result :=trim(ABGetItemValue(tempFormStrings.Text,tempStr,'=',ABSubStrBegin+aFormName));
    end
    else
    begin
      Result := tempFormStrings.ValueFromIndex[j];
    end;
  end;

  //根据源码的原串转换到当前语言串
  if (result<>EmptyStr)  then
  begin
    result := GetStr( tempStr,
                      result,
                      ttSysCharType,ABIsMoreLineProperty(aPropertyName),true);
  end;
end;

function TABLanguage.GetCodeStr(aStr: string;
                    aFormatValues:array of const;                    //aShowValue中参数值
                    aSave: Boolean = true): string;
begin
  if High(aFormatValues)>-1 then
  begin
    aStr:=Format(aStr,aFormatValues);
  end;
  result:= GetCodeStr(aStr,aSave);
end;

function TABLanguage.GetCodeStr(aStr: string;aSave: Boolean): string;
begin
  result:=aStr;
  if not ABLocalParams.LanguageEnabled then
    exit;

  if (result<>EmptyStr)  then
  begin
    result := GetStr(ABSubStrBegin+aStr,
                     aStr,
                     ttSourceCodeCharType,false,aSave);
  end;
end;

//已保证此函数进来的aStr字串是ABLocalParams.SourceCodeChineseType
function TABLanguage.GetStr(aID: string; aStr: string;aStrType:TABBTransitionStrType;aMulLine:Boolean;aSave: Boolean ): string;
var
  i:LongInt;
  tempStr: string;
begin
  Result := aStr;
  aID:=ABStringReplace(aID,ABEnterWrapStr,'');
  if (aID=emptystr) or (aStr=emptystr) then
    exit;

  tempStr:=aStr;
  i:=FItems.IndexOfName(aID);
  if i<0 then
  begin
    if FchinaLanguage then
    begin
      //保证保存的要是未径过转化当前语言的ABLocalParams.SourceCodeChineseType格式源串
      if aSave then
      begin
        FItems.Add(aID+'='+tempStr);
        FLanguageChang:=true;
      end;
      tempStr := TransitionChineseType(aStr,ABLocalParams.SourceCodeChineseType,ABLocalParams.LanguageChineseType);
    end
    else
    begin
      tempStr:=EmptyStr;
      if aSave then
      begin
        FItems.Add(aID+'='+tempStr);
        FLanguageChang:=true;
      end;
    end;

    Result := tempStr;
  end
  else
  begin
    if (aMulLine) or
       (ABPos(ABEnterWrapStr,tempStr)>0) then
    begin
      Result :=trim(ABGetItemValue(FItems.Text,aID,'=',ABSubStrBegin));
    end
    else
    begin
      Result := FItems.ValueFromIndex[i];
    end;
  end;
end;

procedure TABLanguage.ReFreshFormsLanguage;
var
  i,j: LongInt;
begin
  for I := 0 to Screen.FormCount -1 do
  begin
    if Screen.Forms[i].FormStyle = fsMDIForm then
    begin
      for j := 0 to Screen.Forms[i].MDIChildCount - 1 do
      begin
        SetComponentsText(Screen.Forms[i].MDIChildren[j]);
      end;
    end;
    SetComponentsText(Screen.Forms[i]);
  end;
end;

constructor TABLanguage.Create(aOwner: TComponent);
begin
  inherited;
  FFormsConrtosPropertys:=TStringList.Create;
  FItems:=TStringList.Create;

  FInheritedTransitionPropertyNames:=TStringList.Create;
  FExtTransitionPropertyNames      :=TStringList.Create;

  FNotTransitionFormComponentNames :=TStringList.Create;
  FNotTransitionFormNames          :=TStringList.Create;
  FNotTransitionFormComponentPropertyNames:=TStringList.Create;

  //内部翻译的属性名称
  FInheritedTransitionPropertyNames.Add('Caption');
  FInheritedTransitionPropertyNames.Add('EditLabel.Caption');
  FInheritedTransitionPropertyNames.Add('Text');
  FInheritedTransitionPropertyNames.Add('Items.Text');
  FInheritedTransitionPropertyNames.Add('lines.Text');
  FInheritedTransitionPropertyNames.Add('properties.Items.Text');

  FLanguage := ABLocalParams.Language;
  LoadLanguage;
end;

procedure TABLanguage.SaveLanguage;
begin
  if FLanguageChang then
  begin
    //保存语言的ini文件(简体环境只保存简体INI,繁体环境只保存繁体INI)
    if (ABLocalParams.LanguageEnabled) then
    begin
      if FchinaLanguage then
      begin
        if (ABLocalParams.SourceCodeChineseType=ABLocalParams.LanguageChineseType) then
        begin
          {$IF (ABDelphiVersion_int>=200)}      //200 =DELPHI2009
            FItems.SaveToFile(ABLanguagesPath+FLanguageFileName+'.ini',TEncoding.Unicode);
          {$ELSE}
            FItems.SaveToFile(ABLanguagesPath+FLanguageFileName+'.ini');
          {$IFEND}
        end;
      end
      else
      begin
        {$IF (ABDelphiVersion_int>=200)}      //200 =DELPHI2009
          FItems.SaveToFile(ABLanguagesPath+FLanguageFileName+'.ini',TEncoding.Unicode);
        {$ELSE}
          FItems.SaveToFile(ABLanguagesPath+FLanguageFileName+'.ini');
        {$IFEND}
      end;
    end;
  end;
end;

procedure TABLanguage.LoadLanguage;
begin
  FItems.clear;

  if (AnsiCompareText(FLanguage,'Auto') =0) then
  begin
    FLanguageFileName := ABGetSysLanguageStr(ABGetSysLanguage);
  end
  else
  begin
    FLanguageFileName:=FLanguage;
  end;
  FchinaLanguage:=ABPos(','+FLanguageFileName+',',','+'China,ChinaTw,')>0;

  //加载语言的ini文件
  if (ABLocalParams.LanguageEnabled) then
  begin
    if (ABCheckFileExists(ABLanguagesPath+FLanguageFileName+'.ini')) then
    begin
      {$IF (ABDelphiVersion_int>=200)}      //200 =DELPHI2009
        FItems.LoadFromFile(ABLanguagesPath+FLanguageFileName+'.ini',TEncoding.Unicode);
      {$ELSE}
        FItems.LoadFromFile(ABLanguagesPath+FLanguageFileName+'.ini');
      {$IFEND}
    end;
  end;
  FLanguageChang:=false;
end;

destructor TABLanguage.Destroy;
var
  I: Integer;
begin
  for I := 0 to FFormsConrtosPropertys.Count-1 do
  begin
    TStrings(FFormsConrtosPropertys.Objects[i]).Free
  end;
  FFormsConrtosPropertys.Free;
  SaveLanguage;
  FItems.Free;

  FInheritedTransitionPropertyNames.Free;
  FExtTransitionPropertyNames      .Free;

  FNotTransitionFormComponentNames .Free;
  FNotTransitionFormNames          .Free;
  FNotTransitionFormComponentPropertyNames.Free;

  inherited;
end;

procedure TABLanguage.ReFreshMenuItems(aMenuItems: TMenuItem);
begin
  CreateMenuItems(aMenuItems);
  SetCurLangeuageMenuItem(aMenuItems);
end;

function TABLanguage.CheckCanChangeLanguage(const aLanguage: string): Boolean;
var
  tempNewLanguage:string;
begin
  if (AnsiCompareText(aLanguage,'Auto') =0) then
  begin
    tempNewLanguage := ABGetSysLanguageStr(ABGetSysLanguage);
  end
  else
  begin
    tempNewLanguage:=aLanguage;
  end;

  result := not ((AnsiCompareText(FLanguageFileName, tempNewLanguage) = 0));
end;

procedure TABLanguage.CreateMenuItems(aMenuItems: TMenuItem);
  procedure CreateMenuItem(aMenuItems: TMenuItem;aName: string);
  var
    tempMenuItem: TMenuItem;
  begin
    tempMenuItem := TMenuItem.Create(nil);
    tempMenuItem.Caption := aName;
    tempMenuItem.Hint := aName;
    tempMenuItem.GroupIndex := 100;
    tempMenuItem.RadioItem := true;
    tempMenuItem.AutoCheck := true;
    tempMenuItem.OnClick := MenuItemOnClick;
    aMenuItems.Add(tempMenuItem);

    {$IF (ABDelphiVersion_int<=185)}      
      //D2009之前繁体中文环境编译显示不能显示简体，所以关闭转换
      if (AnsiCompareText( aName,'China')=0) and
         (ABGetSysLanguage=wlChinaTw) then
      begin
        tempMenuItem.Enabled:=false;
      end;
    {$IFEND}
  end;
var
  i: longint;
  tempStrings:TStrings;
begin
  aMenuItems.Clear;
  if (ABLocalParams.LanguageEnabled) then
  begin
    tempStrings := TStringList.Create;
    try
      ABGetFileList(tempStrings,ABLanguagesPath,['ini'],1,True,nil,False);
      for i := 0 to tempStrings.Count - 1 do
      begin
        CreateMenuItem(aMenuItems,ChangeFileExt(tempStrings[i],''));
      end;
      //追加一个自动的菜单
      CreateMenuItem(aMenuItems,ABGetSysLanguageStr(wlAuto));
    finally
      tempStrings.Free;
    end;
  end;
end;

procedure TABLanguage.SetCurLangeuageMenuItem(aMenuItems: TMenuItem);
var
  i: longint;
begin
  for i := 0 to aMenuItems.Count - 1 do
  begin
    aMenuItems.Items[i].Checked := AnsiCompareText(Language,aMenuItems.Items[i].Hint) = 0;
  end;
end;

procedure TABLanguage.SetExtTransitionPropertyNames(const Value: TStrings);
begin
  FExtTransitionPropertyNames.Assign(Value);
end;

procedure TABLanguage.SetLanguage(const Value: string);
var
  tempCheckCanChangeLanguage:boolean;
begin
  tempCheckCanChangeLanguage:=CheckCanChangeLanguage(Value);
  if tempCheckCanChangeLanguage then
  begin
    ABLocalParams.PriLanguageChineseType := ABLanguageToChineseType(FLanguage);
    SaveLanguage;
  end;
  FLanguage := Value;
  ABSetLocalParamsValue('Language', FLanguage, 'Params');

  if tempCheckCanChangeLanguage then
  begin
    LoadLanguage;
    ReFreshFormsLanguage;
  end;
end;

procedure TABLanguage.SetNotTransitionFormComponentNames(const Value: TStrings);
begin
  FNotTransitionFormComponentNames.Assign(Value);
end;

procedure TABLanguage.SetNotTransitionFormComponentPropertyNames(
  const Value: TStrings);
begin
  FNotTransitionFormComponentPropertyNames.Assign(Value);
end;

procedure TABLanguage.SetNotTransitionFormNames(const Value: TStrings);
begin
  FNotTransitionFormNames.Assign(Value);
end;

procedure TABLanguage.MenuItemOnClick(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  Language := TMenuItem(Sender).Hint;
  SetCurLangeuageMenuItem(TMenuItem(Sender).Parent);
end;

procedure ABFinalization;
begin
  ABLanguage.Free;
end;

procedure ABInitialization;
begin
  ABLanguage := TABLanguage.create(nil);
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;

end.


