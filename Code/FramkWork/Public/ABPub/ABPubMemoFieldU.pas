{
��ʾ��༭��ע�ֶε�Ԫ
}
unit ABPubMemoFieldU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,

  SysUtils,Classes,Controls,Forms,DB,DBCtrls,ExtCtrls,StdCtrls;

type
  TABMemoFieldForm = class(TABPubForm)
    DBMemo1: TDBMemo;
    DataSource1: TDataSource;
    pnl1: TPanel;
    btn1: TButton;
    Button2: TButton;
    procedure Button2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//��ʾ��༭��ע�ֶ�
function ABShowEditMemoField(aDataSet:TDataSet;aFieldName:string;aReadOnly:Boolean=false;aCaption:string=''): boolean;

implementation

{$R *.dfm}


function ABShowEditMemoField(aDataSet:TDataSet;aFieldName:string;aReadOnly:Boolean;aCaption:string): boolean;
var
  tempForm: TABMemoFieldForm;
begin
  result:=False;
  if not Assigned(aDataSet.FindField(aFieldName)) then
    exit;

  tempForm := TABMemoFieldForm.Create(nil);
  tempForm.DataSource1.DataSet:=aDataSet;
  if aCaption=EmptyStr then
    aCaption:=aDataSet.FindField(aFieldName).DisplayLabel;
  tempForm.Caption:=aCaption;
  try
    tempForm.DBMemo1.DataField := aFieldName;
    tempForm.DBMemo1.ReadOnly := aReadOnly;
    if tempForm.ShowModal=mrok then
    begin
      result:=true;
    end;
  finally
    tempForm.Free;
  end;
end;

procedure TABMemoFieldForm.btn1Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TABMemoFieldForm.Button2Click(Sender: TObject);
begin
  if DBMemo1.Field.DataSet.State in [dsedit,dsinsert] then
    DBMemo1.Field.DataSet.Post;

  ModalResult:=mrOk;
end;


end.
