{
增强的TStringGrid控件单元(增加设置标题功能)
}
unit ABPubStringGridU;

interface
{$I ..\ABInclude.ini}
uses


  SysUtils,Classes,Grids;

type
  //可设置标题的StringGrid
  TABStringGrid = class(TStringGrid)
  private
    FColCaption: TStrings;
    FRowCaption: TStrings;
    procedure SetColCaption(const Value: TStrings);
    procedure SetRowCaption(const Value: TStrings);
  protected
    procedure Loaded; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure ReFreshColCaption;
    procedure ReFreshRowCaption;

    procedure DeleteRow(ARow: Longint);override;
    procedure DeleteColumn(ACol: Longint);override;
    procedure InsertRow(ARow: LongInt);
    procedure InsertColumn(ACol: LongInt);
  Published
    property RowCaption:TStrings read FRowCaption write SetRowCaption;
    property ColCaption:TStrings read FColCaption write SetColCaption;
  end;



implementation

{ TABStringGrid }

constructor TABStringGrid.Create(AOwner: TComponent);
begin
  inherited;
  Options:=Options+[goRowSizing];
  Options:=Options+[goColSizing];
  Options:=Options+[goRowMoving];
  Options:=Options+[goColMoving];
  Options:=Options+[goEditing];

  FColCaption:= TStringList.Create;
  FRowCaption:= TStringList.Create;

end;

destructor TABStringGrid.Destroy;
begin
  FColCaption.Free;
  FRowCaption.Free;
  inherited;
end;

procedure TABStringGrid.ReFreshColCaption;
var
  i:LongInt;
begin
  if RowCount<1 then exit;

  for I := 0 to ColCount - 1 do
  begin
    if i<=FColCaption.Count-1 then
      Cells[i,0]:=FColCaption.Strings[i]
    else
      Cells[i,0]:=EmptyStr;
  end;
end;

procedure TABStringGrid.ReFreshRowCaption;
var
  i:LongInt;
begin
  if ColCount<1 then exit;

  for I := 0 to RowCount - 1 do
  begin
    if i<=FRowCaption.Count-1 then
      Cells[0,i]:=FRowCaption.Strings[i]
    else
      Cells[0,i]:=EmptyStr;
  end;
end;

//调用控件时,设置此属性值时会报错,查看TStringList类型的公布属性是如何设置取值与设置值两个函数的
procedure TABStringGrid.SetColCaption(const Value: TStrings);
begin
  FColCaption.Assign(Value);
  ReFreshColCaption ;
end;

procedure TABStringGrid.SetRowCaption(const Value: TStrings);
begin
  FRowCaption.Assign(Value);
  ReFreshRowCaption ;
end;

procedure TABStringGrid.InsertColumn(ACol: Integer);
begin
  ColCount :=ColCount +1;
  MoveColumn(ColCount-1, ACol);
end;

procedure TABStringGrid.InsertRow(ARow: Integer);
begin
  RowCount :=RowCount +1;
  MoveRow(RowCount-1, ARow);
end;

procedure TABStringGrid.Loaded;
begin
  inherited;
  ReFreshColCaption ;
  ReFreshRowCaption ;
end;

procedure TABStringGrid.DeleteColumn(ACol: Longint);
begin
  inherited;
end;

procedure TABStringGrid.DeleteRow(ARow: Longint);
begin
  inherited;
end;



end.
