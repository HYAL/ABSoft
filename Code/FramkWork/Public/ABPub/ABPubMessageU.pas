{
提示信息框单元
}
unit ABPubMessageU;

interface                      
{$I ..\ABInclude.ini}

uses
  Windows, SysUtils, Variants, Classes, Controls, Forms,
  StdCtrls, ExtCtrls, clipbrd,math;

type 
  TABShowMessageForm = class(TForm)
    Panel1: TPanel;
    ShowSecond_Panel: TPanel;
    Timer1: TTimer;
    Label1: TLabel;
    CheckBox1: TCheckBox;
    Panel5: TPanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    ReMark_Panel: TPanel;
    ReMark_Memo: TMemo;
    Label2: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure ReMark_MemoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Label2DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FChickButtonIndex: longint;
    FIsModel: boolean;
 protected
    procedure DoCreate ; override;
    { Private declarations }
  public
    property IsModel:boolean read FIsModel write FIsModel;
    property ChickButtonIndex:longint read FChickButtonIndex write FChickButtonIndex;
    { Public declarations }
  end;


//*******************消息框操作*********************************
//显示多按钮消息框，可带有参数、备注、定时关闭, 最多5个按钮
function ABShow(aShowValue: Variant;                             //显示的内容
                aFormatValues:array of const;                    //显示内容中的参数值
                aButtonCaptions:array of string;                 //按钮标签数组最多五个
                aDefaultButtonIndex: LongInt = 1;                //默认按钮
                aRemark:string='';                               //显示的详细信息
                aCaption: string = '';                           //窗体标题
                aShowSecond:longint=-1;                          //显示的多少秒后自动关闭(-1表示不关闭)
                aAlignment:TAlignment = taCenter;                //显示内容的靠向
                aTransition:Boolean=true;                        //显示时是否翻译
                aIsShowModel:Boolean=true):LongInt; overload;    //是否显示模式窗体

//显示单按钮消息框，可带参数、备注、定时关闭
function ABShow(aShowValue: Variant;                             //显示的内容
                aFormatValues:array of const;                    //显示内容中的参数值
                aRemark:string='';                               //显示的详细信息
                aCaption: string = '';                           //窗体标题
                aShowSecond:longint=-1;                          //显示的多少秒后自动关闭(-1表示不关闭)
                aAlignment:TAlignment = taCenter;                //显示内容的靠向
                aTransition:Boolean=true;                        //显示时是否翻译
                aIsShowModel:Boolean=true):LongInt; overload;    //是否显示模式窗体
//显示单按钮消息框，可带备注、定时关闭
function ABShow(aShowValue: Variant;                             //显示的内容
                aRemark:string='';                               //显示的详细信息
                aCaption: string = '';                           //窗体标题
                aShowSecond:longint=-1;                          //显示的多少秒后自动关闭(-1表示不关闭)
                aAlignment:TAlignment = taCenter;                //显示内容的靠向
                aTransition:Boolean=true;                        //显示时是否翻译
                aIsShowModel:Boolean=true):LongInt; overload;    //是否显示模式窗体

//以MessageBoxExW函数显示单按钮消息框，可带参数
function ABShow_WindowsMessage(
                aShowValue: string;                              //显示的内容
                aFormatValues:array of const;                    //显示内容中的参数值
                aType: Cardinal = MB_OK;                         //窗体类型
                aCaption: string = '';                           //窗体标题
                aDefaultButton: Cardinal = MB_DEFBUTTON1;        //默认按钮
                aLANG: Word = LANG_CHINESE): Cardinal; overload; //默认语言



implementation


{$R *.dfm}



var
  PubABShowMessageForm: TABShowMessageForm;

function ABShow_WindowsMessage(
  aShowValue: string;
  aFormatValues:array of const;                    //aShowValue中参数值
  aType: Cardinal;
  aCaption: string;
  aDefaultButton:Cardinal;
  aLANG: Word): Cardinal;
begin
  if High(aFormatValues)>-1 then
  begin
    aShowValue:=Format(aShowValue,aFormatValues);
  end;

  if aCaption = emptystr then
  begin
    aCaption := '系统提示';
  end;

  result := MessageBoxExW(application.Handle, PWideChar(WideString(aShowValue)),PWideChar(WideString(aCaption)), aType + aDefaultButton, aLANG);
  //  （1）按钮组合常量
  //   MB_OK = $00000000;                  //一个确定按钮
  //   MB_OKCANCEL = $00000001;            //一个确定按钮,一个取消按钮
  //   MB_ABORTRETRYIGNORE = $00000002;    //一个异常终止按钮,一个重试按钮,一个忽略按钮
  //   MB_YESNOCANCEL = $00000003;         //一个是按钮,一个否按钮,一个取消按钮
  //   MB_YESNO = $00000004;               //一个是按钮,一个否按钮
  //   MB_RETRYCANCEL = $00000005;         //一个重试按钮,一个取消按钮
  //   （2）缺省按钮常量
  //   MB_DEFBUTTON1 = $00000000;          //第一个按钮为缺省按钮
  //   MB_DEFBUTTON2 = $00000100;          //第二个按钮为缺省按钮
  //   MB_DEFBUTTON3 = $00000200;          //第三个按钮为缺省按钮
  //   MB_DEFBUTTON4 = $00000300;          //第四个按钮为缺省按钮
  //   （3）图标常量
  //   MB_ICONHAND = $00000010;               //“×”号图标
  //   MB_ICONQUESTION = $00000020;           //“？”号图标
  //   MB_ICONEXCLAMATION = $00000030;        //“！”号图标
  //   MB_ICONASTERISK = $00000040;           //“i”图标
  //   MB_USERICON = $00000080;               //用户图标
  //   MB_ICONWARNING = MB_ICONEXCLAMATION;   //“！”号图标
  //   MB_ICONERROR = MB_ICONHAND;            //“×”号图标
  //   MB_ICONINFORMATION = MB_ICONASTERISK;  //“i”图标
  //   MB_ICONSTOP = MB_ICONHAND;             //“×”号图标
  //   （4）运行模式常量
  //   MB_APPLMODAL = $00000000;        //应用程序模式,在未结束对话框前也能切换到另一应用程序
  //   MB_SYSTEMMODAL = $00001000;      //系统模式,必须结束对话框后,才能做其他操作
  //   MB_TASKMODAL = $00002000;        //任务模式,在未结束对话框前也能切换到另一应用程序
  //   MB_HELP = $00004000;             //添加一个帮助按钮.
  //   MB_RIGHT = $00080000;            //文本显示时右对齐.
  //   MB_TOPMOST $00040000;            //信息对话框始终显示在桌面最前面.
  //   3、函数返回值
  //   0                       //对话框建立失败
  //   idOk = 1                //按确定按钮
  //   idCancel = 2            //按取消按钮
  //   idAbout = 3             //按异常终止按钮
  //   idRetry = 4             //按重试按钮
  //   idIgnore = 5            //按忽略按钮
  //   idYes = 6               //按是按钮
  //   idNo = 7                //按否按钮
  //  //未明白的
  //  MB_NOFOCUS = $00008000;
  //  MB_SETFOREGROUND = $00010000;
  //  MB_DEFAULT_DESKTOP_ONLY = $00020000;
  //  MB_RTLREADING = $00100000;
  //
  //  MB_SERVICE_NOTIFICATION = $00200000;
  //  MB_SERVICE_NOTIFICATION_NT3X = $00040000;
  //
  //  MB_TYPEMASK = $0000000F;
  //  MB_ICONMASK = $000000F0;
  //  MB_DEFMASK = $00000F00;
  //  MB_MODEMASK = $00003000;
  //  MB_MISCMASK = $0000C000;
end;

function ABShow(aShowValue: Variant;
                aFormatValues:array of const;                    //aShowValue中参数值
                aRemark:string;
                aCaption: string;
                aShowSecond:longint;
                aAlignment:TAlignment;
                aTransition:Boolean;
                aIsShowModel:Boolean):LongInt;
begin
  Result:=ABShow(aShowValue,aFormatValues,['確定'],1,aRemark,aCaption,aShowSecond,aAlignment,aTransition,aIsShowModel);
end;

function ABShow(aShowValue: Variant;
                aRemark:string;
                aCaption: string;
                aShowSecond:longint;
                aAlignment:TAlignment;
                aTransition:Boolean;
                aIsShowModel:Boolean):LongInt;
begin
  Result:=ABShow(aShowValue,[],['確定'],1,aRemark,aCaption,aShowSecond,aAlignment,aTransition,aIsShowModel);
end;

function ABShow(aShowValue: Variant;
                aFormatValues:array of const;                    //aShowValue中参数值
                aButtonCaptions:array of string;
                aDefaultButtonIndex: LongInt;
                aRemark:string;
                aCaption: string;
                aShowSecond:longint;
                aAlignment:TAlignment;
                aTransition:Boolean;
                aIsShowModel:Boolean):LongInt;
var
  tempShowStr:string;
  tempButtonCount:longint;
  function ABShowMessage:LongInt;
  var
    j,tempheight:LongInt;
    tempButton:TButton;
  begin
    tempheight:=175;
    if not Assigned(PubABShowMessageForm) then
      PubABShowMessageForm:= TABShowMessageForm.Create(nil);

    PubABShowMessageForm.Label2.Alignment:= aAlignment;
    PubABShowMessageForm.ChickButtonIndex:=aDefaultButtonIndex;
    PubABShowMessageForm.Label2.Caption:= VarToStrDef(aShowValue,'');

    if aShowSecond>0 then
    begin
      PubABShowMessageForm.ShowSecond_Panel.visible:=true;
      PubABShowMessageForm.CheckBox1.Checked:= true;
      PubABShowMessageForm.Label1.Caption:=inttostr(aShowSecond);
      PubABShowMessageForm.Label1.tag:= aShowSecond ;
      PubABShowMessageForm.Timer1.Enabled:=true;

      tempheight:=tempheight+PubABShowMessageForm.ShowSecond_Panel.height;
    end
    else
    begin
      PubABShowMessageForm.ShowSecond_Panel.visible:=false;
      PubABShowMessageForm.Timer1.Enabled:=false;
    end;

    if aRemark<>emptystr then
    begin
      PubABShowMessageForm.ReMark_Memo.Text:=aRemark;
      PubABShowMessageForm.ReMark_Panel.visible:=true;
      tempheight:=350;
    end
    else
    begin
      PubABShowMessageForm.ReMark_Panel.visible:=false;
    end;

    PubABShowMessageForm.height:=tempheight;
    case tempButtonCount of
      1:
      begin
        PubABShowMessageForm.Button1.left:=159;

        PubABShowMessageForm.Button1.visible:=true;
        PubABShowMessageForm.Button2.visible:=false;
        PubABShowMessageForm.Button3.visible:=false;
        PubABShowMessageForm.Button4.visible:=false;
        PubABShowMessageForm.Button5.visible:=false;
      end;
      2:
      begin
        PubABShowMessageForm.Button1.left:=128;
        PubABShowMessageForm.Button2.left:=217;

        PubABShowMessageForm.Button1.visible:=true;
        PubABShowMessageForm.Button2.visible:=true;
        PubABShowMessageForm.Button3.visible:=false;
        PubABShowMessageForm.Button4.visible:=false;
        PubABShowMessageForm.Button5.visible:=false;
      end;
      3:
      begin
        PubABShowMessageForm.Button1.left:=55;
        PubABShowMessageForm.Button2.left:=144;
        PubABShowMessageForm.Button3.left:=237;

        PubABShowMessageForm.Button1.visible:=true;
        PubABShowMessageForm.Button2.visible:=true;
        PubABShowMessageForm.Button3.visible:=true;
        PubABShowMessageForm.Button4.visible:=false;
        PubABShowMessageForm.Button5.visible:=false;
      end;
      4:
      begin
        PubABShowMessageForm.Button1.left:=31;
        PubABShowMessageForm.Button2.left:=112;
        PubABShowMessageForm.Button3.left:=193;
        PubABShowMessageForm.Button4.left:=274;

        PubABShowMessageForm.Button1.visible:=true;
        PubABShowMessageForm.Button2.visible:=true;
        PubABShowMessageForm.Button3.visible:=true;
        PubABShowMessageForm.Button4.visible:=true;
        PubABShowMessageForm.Button5.visible:=false;
      end;
      5:
      begin
        PubABShowMessageForm.Button1.left:=3;
        PubABShowMessageForm.Button2.left:=81;
        PubABShowMessageForm.Button3.left:=159;
        PubABShowMessageForm.Button4.left:=237;
        PubABShowMessageForm.Button5.left:=315;

        PubABShowMessageForm.Button1.visible:=true;
        PubABShowMessageForm.Button2.visible:=true;
        PubABShowMessageForm.Button3.visible:=true;
        PubABShowMessageForm.Button4.visible:=true;
        PubABShowMessageForm.Button5.visible:=true;
      end;
    end;

    for j := Low(aButtonCaptions) to tempButtonCount-1 do
    begin
      tempButton:=TButton(PubABShowMessageForm.Panel5.FindChildControl('Button'+inttostr(j+1)));
      tempButton.Caption:=aButtonCaptions[j];
      if Length(AnsiString(tempButton.Caption))>10 then
      begin
        tempButton.Width:=Length(AnsiString(tempButton.Caption))*7+7;
      end;

      if aDefaultButtonIndex=j+1 then
      begin
        if not tempButton.Default then
        begin
          tempButton.Default:=true;
        end;
      end
      else
      begin
        if tempButton.Default then
          tempButton.Default:=false;
      end;
    end;


    if PubABShowMessageForm.Showing then
      PubABShowMessageForm.Close;

    PubABShowMessageForm.IsModel:=aIsShowModel;
    if aIsShowModel then
    begin
      PubABShowMessageForm.FormStyle:=fsNormal;
      PubABShowMessageForm.ShowModal;
    end
    else
    begin
      PubABShowMessageForm.FormStyle:=fsStayOnTop;
      PubABShowMessageForm.Show;
    end;

    Result:=PubABShowMessageForm.ChickButtonIndex;
  end;
begin
  if High(aFormatValues)>-1 then
  begin
    aShowValue:=Format(VarToStrDef(aShowValue,''),aFormatValues);
  end;

  tempShowStr:=vartostrdef(aShowValue,'');
  tempButtonCount:=min(High(aButtonCaptions)+1,5);

  if aCaption = emptystr then
    aCaption := '系统提示';

  result := ABShowMessage;
end;

procedure TABShowMessageForm.Button1Click(Sender: TObject);
begin
  FChickButtonIndex:=1;
  if IsModel then
    ModalResult:=mrOk
  else
    close;
end;

procedure TABShowMessageForm.Button2Click(Sender: TObject);
begin
  FChickButtonIndex:=2;
  if IsModel then
    ModalResult:=mrOk
  else
    close;
end;

procedure TABShowMessageForm.Button3Click(Sender: TObject);
begin
  FChickButtonIndex:=3;
  if IsModel then
    ModalResult:=mrOk
  else
    close;
end;

procedure TABShowMessageForm.Button4Click(Sender: TObject);
begin
  FChickButtonIndex:=4;
  if IsModel then
    ModalResult:=mrOk
  else
    close;
end;

procedure TABShowMessageForm.Button5Click(Sender: TObject);
begin
  FChickButtonIndex:=5;
  if IsModel then
    ModalResult:=mrOk
  else
    close;
end;

procedure TABShowMessageForm.CheckBox1Click(Sender: TObject);
begin
  Timer1.Enabled:=CheckBox1.Checked;
end;

procedure TABShowMessageForm.DoCreate;
begin
  inherited;
end;

procedure TABShowMessageForm.FormShow(Sender: TObject);
begin
  if (Button1.default) and
     (Button1.canFocus)  then
    Button1.SetFocus
  else if (Button2.default) and
          (Button2.canFocus)  then
    Button2.SetFocus
  else if (Button3.default) and
          (Button3.canFocus)  then
    Button3.SetFocus
  else if (Button4.default) and
          (Button4.canFocus)  then
    Button4.SetFocus
  else if (Button5.default) and
          (Button5.canFocus)  then
    Button5.SetFocus;
end;

procedure TABShowMessageForm.Label2DblClick(Sender: TObject);
begin
  clipboard.AsText := Label2.Caption;
end;

procedure TABShowMessageForm.ReMark_MemoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Shift=[ssCtrl]) then
  begin
    if  (Key=65) then
    begin
      ReMark_Memo.SelectAll;
    end
    else if  (Key=67) then
    begin
      clipboard.AsText := ReMark_Memo.text;
      Key:=0;
    end;
  end;
end;

procedure TABShowMessageForm.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=false;
  try
    if Label1.tag<=1 then
    begin
      ModalResult:=mrOk;
      Timer1.Enabled:=false;
    end
    else
    begin
      Label1.tag:=Label1.tag-1;
      Label1.Caption:=inttostr(Label1.tag);
    end;
  finally
    Timer1.Enabled:=true;
  end;
end;


end.
