{
//多线程固定时间进度条单元
//使用方法：
begin
  ABPubAutoProgressBar_ThreadU.ABRunProgressBar;
  try
    ABSleep(3000);
  finally
    ABPubAutoProgressBar_ThreadU.ABStopProgressBar;
  end;
end;
显示MSG时如果要暂停进度条,显示后再恢复,则要
begin
  ABPubAutoProgressBar_ThreadU.ABRunProgressBar;
  try
    ABSleep(1000);
    ABPubAutoProgressBar_ThreadU.ABStopProgressBar;
    ABSleep(1000);
    ABPubAutoProgressBar_ThreadU.ABRunProgressBar;
    ABSleep(1000);
  finally
    ABPubAutoProgressBar_ThreadU.ABStopProgressBar;
  end;
end;
}
unit ABPubAutoProgressBar_ThreadU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubVarU,
  ABPubConstU,
  ABPubLocalParamsU,

  Windows,Messages,CommCtrl,Forms;


//启动进度条
//Handle=呼叫窗口句柄
//Caption=进度条窗品标题
//aSleep=进度条前进速度
procedure ABRunProgressBar(aCaption: string = '';Handle: THandle = 0; aSleep:Integer=1);
//停止进度条
procedure ABStopProgressBar;

var
  ABProgressState:TABProgressState;

implementation

const
  AppName = 'ABAutoProgressBar_Thread';
  ID_PROGBAR = $FF;

var
  Master: THandle; {呼叫窗口}
  hThread: Thandle; {附加进程}

  hWindow: hWnd; {主窗口}
  hProgBar: hWnd; {进度条}

  pTimer: integer; {时钟}
  pValue: integer = 0; {进度}
  PSleep: integer = 1; {速度}
  aMessage: TMsg; {消息}

  Registered: Boolean = False; {注册标识}
  MasterDone: Boolean = False;

  WinCaption: string = 'Running '; {'作业进行中';}

 {主窗口的位置及大小}
  L, T: integer;
  W: integer = 320;
  H: integer = 80;

 {进度条的位置及大小}
  pL: integer = 18;
  pT: integer = 18;
  pW: integer = 280;
  pH: integer = 20;

  PMax: integer = 100; //进度条的最大值

function WindowProc(hWindow: HWnd; aMessage, Wparam, LParam: Longint): Longint; stdcall;
begin
  Result := 0;
  case aMessage of
    wm_Destroy:
      begin
        KillTimer(hWindow, pTimer);
        PostQuitMessage(0);
      end;
    wm_Timer:
      begin
        pValue := (pValue + PSleep) mod PMax;

        SendMessage(hProgBar, PBM_SETPOS, pValue + PSleep, 0);
        //if MasterDone and (pValue = 0) then
        //  SendMessage(hWindow,WM_Destroy,0,0);
      end;
  else WindowProc := CallWindowProc(@DefWindowProc, hWindow, AMessage, WParam, LParam);
  end;

  if ABProgressState=PsStop then
  begin
    KillTimer(hWindow, pTimer);
    PostQuitMessage(0);
  end;
end;

function WinRegister: Boolean;
var
  WindowClass: TWndClass;
begin
  WindowClass.style := cs_vRedraw or cs_hRedraw;
  WindowClass.lpfnWndProc := @WindowProc;
  WindowClass.cbClsExtra := 0;
  WindowClass.cbWndExtra := 0;
  WindowClass.hInstance := hInstance;
  WindowClass.hIcon := LoadIcon(0, idi_Application);
  WindowClass.hCursor := LoadCursor(0, idc_Arrow);
  WindowClass.hbrBackground := Hbrush(Color_Window);
  WindowClass.lpszMenuName := nil;
  WindowClass.lpszClassName := AppName;

  Result := Windows.RegisterClass(WindowClass) <> 0;
end;


function WinCreate: Boolean;
begin
 {确定窗口的位置:居中}
  L := (Screen.Width - W) div 2;
  T := (Screen.Height - H) div 2;


 {创建主窗口}//         WS_BORDER        WS_POPUP or WS_DLGFRAME
  hWindow := CreateWindowEx(WS_EX_TOPMOST, AppName, PwideChar(WinCaption), WS_Caption or WS_VISIBLE or WS_DLGFRAME,
    L, T, W, H, 0, 0, hInstance, nil);
 {创建进度条}
  hProgBar := CreateWindow(PROGRESS_CLASS, nil, WS_CHILD or WS_VISIBLE,
    pL, pT, pW, pH, hWindow, HMENU(ID_PROGBAR), hInstance, nil);
 {设置进度条的基本属性}
  SendMessage(hProgBar, PBM_SETRANGE32, 1, LPARAM(PMax));
  SendMessage(hProgBar, PBM_SETSTEP, WPARAM(4), 0);

  result := (hWindow <> 0) and (hProgBar <> 0);
end;

procedure RunProgBar; stdcall;
begin
 {注册类别}
  if not Registered then
  begin
    Registered := WinRegister;
    if not Registered then Exit;
  end;
 {创建窗口}
  if MasterDone or (not WinCreate) then Exit;
 {启动时钟}
  SetTimer(hWindow, pTimer, 28, nil);
 {消息循环}
  while GetMessage(AMessage, 0, 0, 0) do
  begin
    TranslateMessage(Amessage);
    DispatchMessage(AMessage);
  end;
end;

{启动进度条}

procedure ABRunProgressBar(aCaption: string;Handle: THandle; aSleep:Integer);
var
  ThreadID: DWord;
begin
 if ABLocalParams.Debug then
   exit;

  ABProgressState:=PsRun;

  pValue := 0;
  PSleep:= aSleep;

  Application.ProcessMessages;
  Master := Handle;
  if aCaption <> '' then
    WinCaption := aCaption;

  MasterDone := False;
  //hWindow := 0;
  hThread := CreateThread(nil, 0, @RunProgBar, nil, 0, ThreadID);
  SetThreadPriority(hThread, THREAD_PRIORITY_LOWEST);
  CloseHandle(hThread);
end;

{停止进度条}

procedure ABStopProgressBar;
begin
  SendMessage(hWindow, WM_Destroy, 0, 0);
  MasterDone := True;
  SendMessage(Master, wm_Paint, 0, 0);
  SetForegroundWindow(Master);
  Application.ProcessMessages;

  ABProgressState:=PsStop;
end;



end.
