{
双列List列表控件单元
}
unit ABPubItemListsFrameU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubPanelU,
  abPubFuncU,
  ABPubItemListFrameU,
  ABPubFrameU,

  SysUtils,Classes,Controls,Forms,ExtCtrls,StdCtrls,Buttons;

type
  TABListBoxAddEvent = procedure(Str: string;var aDo:boolean) of object;
  TABListBoxDelEvent = procedure(Str: string;var aDo:boolean) of object;

  TABItemListsFrame = class(TABPubFrame)
    Panel1: TPanel;
    pnl1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    ABItemList1: TABItemList;
    ABItemList2: TABItemList;
    Panel3: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure FrameResize(Sender: TObject);
  private
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    { Public declarations }
  end;

  TABItemLists = class(TABCustomPanel)
  private
    FFrame: TABItemListsFrame;
    FOnListBox2Del: TABListBoxDelEvent;
    FOnListBox1Del: TABListBoxDelEvent;
    FOnListBox2Add: TABListBoxAddEvent;
    FOnListBox1Add: TABListBoxAddEvent;
    FLabelKey: string;
    FListBox2EndSign: string;
    FListBox1EndSign: string;
    FLableCount: longint;
    procedure ListBox2DblClick(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure SetLabelKey(const Value: string);

    procedure SetItems1Items2MoveVisible(const Value: Boolean);
    procedure SetItems1MoveVisible(const Value: Boolean);
    procedure SetItems2MoveVisible(const Value: Boolean);
    procedure SetLabelVisible(const Value: Boolean);
    function GetItems1Items2MoveVisible: Boolean;
    function GetItems1MoveVisible: Boolean;
    function GetItems2MoveVisible: Boolean;
    function GetLabelVisible: Boolean;
    procedure SetLableCount(const Value: longint);
  protected
    procedure Paint; override;
    procedure Resize;override;

    procedure DoListBox1Add(Str:string;var aDo:boolean);dynamic;
    procedure DoListBox1Del(Str:string;var aDo:boolean);dynamic;
    procedure DoListBox2Add(Str:string;var aDo:boolean);dynamic;
    procedure DoListBox2Del(Str:string;var aDo:boolean);dynamic;
  public
    procedure CheckPanel3;
    procedure RefreshLabel;     

 	  constructor Create(AOwner: TComponent); override;
	  destructor Destroy; override;
  Published
    property Frame: TABItemListsFrame     read FFrame     write FFrame ;

    property OnListBox1Add: TABListBoxAddEvent read FOnListBox1Add write FOnListBox1Add;
    property OnListBox1Del: TABListBoxDelEvent read FOnListBox1Del write FOnListBox1Del;
    property OnListBox2Add: TABListBoxAddEvent read FOnListBox2Add write FOnListBox2Add;
    property OnListBox2Del: TABListBoxDelEvent read FOnListBox2Del write FOnListBox2Del;

    property LabelKey :string read FLabelKey write SetLabelKey;
    property LableCount :longint read FLableCount write SetLableCount;


    property LabelVisible :Boolean read GetLabelVisible write SetLabelVisible;
    property Items1MoveVisible :Boolean read GetItems1MoveVisible write SetItems1MoveVisible;
    property Items2MoveVisible :Boolean read GetItems2MoveVisible write SetItems2MoveVisible;
    property Items1Items2MoveVisible :Boolean read GetItems1Items2MoveVisible write SetItems1Items2MoveVisible;

    property ListBox1EndSign :string read FListBox1EndSign write FListBox1EndSign;
    property ListBox2EndSign :string read FListBox2EndSign write FListBox2EndSign;
  end;
  


implementation

{$R *.dfm}

constructor TABItemListsFrame.Create(AOwner: TComponent);
begin
  inherited;
end;

destructor TABItemListsFrame.Destroy;
begin
  inherited;
end;

procedure TABItemListsFrame.FrameResize(Sender: TObject);
begin
  inherited;

end;

//>
procedure TABItemListsFrame.SpeedButton1Click(Sender: TObject);
var
  i:longint;
  tempBool:boolean;
  tempStr:string;
begin
  ABItemList1.Frame.ListBox1.Items.BeginUpdate;
  ABItemList2.Frame.ListBox1.Items.BeginUpdate;
  try
    if (ABItemList1.Frame.ListBox1.MultiSelect) and
       (ABItemList1.Frame.ListBox1.SelCount>1)  then
    begin
      for I :=ABItemList1.Frame.ListBox1.Count - 1 downto 0 do
      begin
        if ABItemList1.Frame.ListBox1.Selected[i] then
        begin
          tempBool:=True;
          tempStr:=ABItemList1.Frame.ListBox1.Items[i];
          if owner is TABItemLists then
          begin
            TABItemLists(owner).DoListBox1Del(tempStr,tempBool);
            TABItemLists(owner).DoListBox2Add(tempStr,tempBool);
          end;

          if tempBool then
            ABListBoxItemMoveToListBox(ABItemList1.Frame.ListBox1,i,TABItemLists(owner).ListBox1EndSign,ABItemList2.Frame.ListBox1,false);

          if ABItemList1.Frame.ListBox1.SelCount<=0 then
            Break;
        end;
      end;
    end
    else if ABItemList1.Frame.ListBox1.ItemIndex>=0 then
    begin
      tempBool:=True;
      tempStr:=ABItemList1.Frame.ListBox1.Items[ABItemList1.Frame.ListBox1.ItemIndex];
      if owner is TABItemLists then
      begin
        TABItemLists(owner).DoListBox1Del(tempStr,tempBool);
        TABItemLists(owner).DoListBox2Add(tempStr,tempBool);
      end;
      if tempBool then
        ABListBoxItemMoveToListBox(ABItemList1.Frame.ListBox1,ABItemList1.Frame.ListBox1.ItemIndex,TABItemLists(owner).ListBox1EndSign,ABItemList2.Frame.ListBox1);
    end;
    if owner is TABItemLists then
      TABItemLists(owner).RefreshLabel;
  finally
    ABItemList1.Frame.ListBox1.Items.EndUpdate;
    ABItemList2.Frame.ListBox1.Items.EndUpdate;
  end;
end;

//>>
procedure TABItemListsFrame.SpeedButton2Click(Sender: TObject);
var
  i:longint;
  tempStr:string;
  tempBool:boolean;
begin
  ABItemList1.Frame.ListBox1.Items.BeginUpdate;
  ABItemList2.Frame.ListBox1.Items.BeginUpdate;
  try
    for I :=0 to ABItemList1.Frame.ListBox1.Count - 1 do
    begin
      tempBool:=True;
      tempStr:=ABItemList1.Frame.ListBox1.Items[ABItemList1.Frame.ListBox1.ItemIndex];
      if owner is TABItemLists then
      begin
        TABItemLists(owner).DoListBox1Del(tempStr,tempBool);
        TABItemLists(owner).DoListBox2Add(tempStr,tempBool);
      end;
      if not tempBool then
        Continue;
      
      ABListBoxItemMoveToListBox(ABItemList1.Frame.ListBox1,0,TABItemLists(owner).ListBox1EndSign,ABItemList2.Frame.ListBox1,false);
    end;

    if owner is TABItemLists then
      TABItemLists(owner).RefreshLabel;
  finally
    ABItemList1.Frame.ListBox1.Items.EndUpdate;
    ABItemList2.Frame.ListBox1.Items.EndUpdate;
  end;
end;

//<
procedure TABItemListsFrame.SpeedButton3Click(Sender: TObject);
var
  i:longint;
  tempStr:string;
  tempBool:boolean;
begin
  ABItemList1.Frame.ListBox1.Items.BeginUpdate;
  ABItemList2.Frame.ListBox1.Items.BeginUpdate;
  try
    if (ABItemList2.Frame.ListBox1.MultiSelect) and
       (ABItemList2.Frame.ListBox1.SelCount>1) then
    begin
      for I :=ABItemList2.Frame.ListBox1.Count - 1 downto 0 do
      begin
        if ABItemList2.Frame.ListBox1.Selected[i] then
        begin
          tempBool:=True;
          tempStr:=ABItemList2.Frame.ListBox1.Items[i];
          if owner is TABItemLists then
          begin
            TABItemLists(owner).DoListBox2Del(tempStr,tempBool);
            TABItemLists(owner).DoListBox1Add(tempStr,tempBool);
          end;
          if tempBool then
            ABListBoxItemMoveToListBox(ABItemList2.Frame.ListBox1,i,TABItemLists(owner).ListBox2EndSign,ABItemList1.Frame.ListBox1,false);
          if ABItemList2.Frame.ListBox1.SelCount<=0 then
            Break;
        end;
      end;
    end
    else if ABItemList2.Frame.ListBox1.ItemIndex>=0 then
    begin
      tempBool:=True;
      tempStr:=ABItemList2.Frame.ListBox1.Items[ABItemList2.Frame.ListBox1.ItemIndex];
      if owner is TABItemLists then
      begin
        TABItemLists(owner).DoListBox2Del(tempStr,tempBool);
        TABItemLists(owner).DoListBox1Add(tempStr,tempBool);
      end;
      if tempBool then
        ABListBoxItemMoveToListBox(ABItemList2.Frame.ListBox1,ABItemList2.Frame.ListBox1.ItemIndex,TABItemLists(owner).ListBox2EndSign,ABItemList1.Frame.ListBox1);
    end;

    if owner is TABItemLists then
      TABItemLists(owner).RefreshLabel;
  finally
    ABItemList1.Frame.ListBox1.Items.EndUpdate;
    ABItemList2.Frame.ListBox1.Items.EndUpdate;
  end;
end;

//<<
procedure TABItemListsFrame.SpeedButton4Click(Sender: TObject);
var
  i:longint;
  tempStr:string;
  tempBool:boolean;
begin
  ABItemList1.Frame.ListBox1.Items.BeginUpdate;
  ABItemList2.Frame.ListBox1.Items.BeginUpdate;
  try
    for I :=0 to ABItemList2.Frame.ListBox1.Count - 1 do
    begin
      tempBool:=True;
      tempStr:=ABItemList2.Frame.ListBox1.Items[ABItemList2.Frame.ListBox1.ItemIndex];
      if owner is TABItemLists then
      begin
        TABItemLists(owner).DoListBox2Del(tempStr,tempBool);
        TABItemLists(owner).DoListBox1Add(tempStr,tempBool);
      end;
      if not tempBool then
        Continue;

      ABListBoxItemMoveToListBox(ABItemList2.Frame.ListBox1,0,TABItemLists(owner).ListBox2EndSign,ABItemList1.Frame.ListBox1,false);
    end;
    if owner is TABItemLists then
      TABItemLists(owner).RefreshLabel;

  finally
    ABItemList1.Frame.ListBox1.Items.EndUpdate;
    ABItemList2.Frame.ListBox1.Items.EndUpdate;
  end;
end;

{ TABItemLists }

procedure TABItemLists.ListBox1DblClick(Sender: TObject);
begin
  FFrame.SpeedButton1Click(nil);
end;

procedure TABItemLists.ListBox2DblClick(Sender: TObject);
begin
  FFrame.SpeedButton3Click(nil);
end;

constructor TABItemLists.Create(AOwner: TComponent);
begin
  inherited;
  FListBox2EndSign:=EmptyStr;
  FListBox1EndSign:=EmptyStr;
  FFrame:= TABItemListsFrame.Create(self);
  FFrame.ABItemList1.Frame.ListBox1.OnDblClick:=ListBox1DblClick;
  FFrame.ABItemList2.Frame.ListBox1.OnDblClick:=ListBox2DblClick;
  FFrame.Parent:=Self;
  FFrame.Show;
end;

destructor TABItemLists.Destroy;
begin
  FFrame.Free;
  inherited;
end;

procedure TABItemLists.DoListBox1Add(Str: string;var aDo:boolean);
begin
  if Assigned(FOnListBox1Add) then FOnListBox1Add(Str,aDo);
end;

procedure TABItemLists.DoListBox1Del(Str: string;var aDo:boolean);
begin
  if Assigned(FOnListBox1Del) then FOnListBox1Del(Str,aDo);
end;

procedure TABItemLists.DoListBox2Add(Str: string;var aDo:boolean);
begin
  if Assigned(FOnListBox2Add) then FOnListBox2Add(Str,aDo);
end;

procedure TABItemLists.DoListBox2Del(Str: string;var aDo:boolean);
begin
  if Assigned(FOnListBox2Del) then FOnListBox2Del(Str,aDo);
end;
                                                                
function TABItemLists.GetItems1Items2MoveVisible: Boolean;
begin
  result:= FFrame.Panel1.Visible ;
end;

function TABItemLists.GetItems1MoveVisible: Boolean;
begin
  result:= FFrame.ABItemList1.MoveVisible ;
end;

function TABItemLists.GetItems2MoveVisible: Boolean;
begin
  result:= FFrame.ABItemList2.MoveVisible ;
end;

function TABItemLists.GetLabelVisible: Boolean;
begin
  result:= FFrame.pnl1.Visible ;
end;

procedure TABItemLists.RefreshLabel;
begin
  FFrame.Label2.Caption:=('未选择')+inttostr(FFrame.ABItemList1.Frame.ListBox1.Count)+('个')+' '+
                         ('已选择')+inttostr(FFrame.ABItemList2.Frame.ListBox1.Count)+('个');
  if (FLabelKey=EmptyStr) then
  begin
    FFrame.Label1.Caption:=
      ('共有')+IntToStr(FLableCount)+('个')+' ';
  end
  else
  begin
    FFrame.Label1.Caption:=
      ('共有')+FLabelKey+IntToStr(FLableCount)+('个')+' ';
  end;
end;

procedure TABItemLists.Resize;
begin
  inherited;
  CheckPanel3;
end;

procedure TABItemLists.SetItems1Items2MoveVisible(const Value: Boolean);
begin
  FFrame.Panel1.Visible:=Value;
  if FFrame.Panel1.Visible then
    FFrame.Panel1.Width:=25
  else
    FFrame.Panel1.Width:=0;
end;

procedure TABItemLists.SetItems1MoveVisible(const Value: Boolean);
begin
  FFrame.ABItemList1.MoveVisible:=Value;
end;

procedure TABItemLists.SetItems2MoveVisible(const Value: Boolean);
begin
  FFrame.ABItemList2.MoveVisible:=Value;
end;

procedure TABItemLists.SetLabelVisible(const Value: Boolean);
begin
  FFrame.pnl1.Visible:=Value;

  if FFrame.pnl1.Visible then
    FFrame.pnl1.Height:=25
  else
    FFrame.pnl1.Height:=0;
end;

procedure TABItemLists.SetLableCount(const Value: longint);
begin
  FLableCount := Value;
  RefreshLabel;
end;

procedure TABItemLists.SetLabelKey(const Value: string);
begin
  FLabelKey := Value;
  RefreshLabel;
end;

procedure TABItemLists.CheckPanel3;
begin
  if FFrame.ABItemList1.Width<>ABTrunc((Width-FFrame.Panel1.Width)/2) then
  begin
    FFrame.ABItemList1.Width:=ABTrunc((Width-FFrame.Panel1.Width)/2);
    FFrame.ABItemList2.Width:=ABTrunc((Width-FFrame.Panel1.Width)/2);
  end;

  if FFrame.Panel3.Top<>ABTrunc((FFrame.Panel1.Height-FFrame.Panel3.Height)/2) then
  begin
    FFrame.Panel3.Top:=ABTrunc((FFrame.Panel1.Height-FFrame.Panel3.Height)/2);
  end;
end;

procedure TABItemLists.Paint;
begin
  inherited;
  CheckPanel3;
end;


end.
