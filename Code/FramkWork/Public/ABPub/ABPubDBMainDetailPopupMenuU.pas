{
操作主从结构数据的右键菜单单元（可挂在数据集关联控件中使用）
}
unit ABPubDBMainDetailPopupMenuU;

interface
{$I ..\ABInclude.ini}
uses

  ABPubDBU,

  SysUtils,Classes,Variants,DB,menus,Dialogs;

type
  TABDBMainDetailPopupMenu= class(TPopupMenu )
  private
    FParentField: string;
    FKeyField: string;
    FDefaultParentValue: string;
    FListField: string;
    FDataSource: TDataSource;
    procedure PopupMenu_SetBase(Sender: TObject);

    procedure PopupMenu_AddRoot(Sender: TObject);
    procedure PopupMenu_Add(Sender: TObject);
    procedure PopupMenu_Del(Sender: TObject);
    procedure PopupMenu_Edit(Sender: TObject);
    procedure PopupMenu_EditCaption(Sender: TObject);
    procedure PopupMenu_Save(Sender: TObject);
    procedure PopupMenu_Cancel(Sender: TObject);
    procedure DoAddItem(aParentFieldValue: string);
  protected
    procedure DoPopup(Sender: TObject); override;

    procedure CreatePopupMenu;virtual;
    procedure DestroyPopupMenu;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    { Public declarations }
  published
    //关联的数据源
    property DataSource: TDataSource read FDataSource write FDataSource;
    //主从结构数据集的父字段
    property ParentField: string read FParentField write  FParentField;
    //主从结构数据集的主键字段
    property KeyField: string read FKeyField write  FKeyField;
    //主从结构数据集的列表显示字段
    property ListField: string read FListField write  FListField;
    //新增第一层数据时默认的父字段值
    property DefaultParentValue: string read FDefaultParentValue write  FDefaultParentValue;
    { Published declarations }
  end;

implementation


{ TABDBMainDetailPopupMenu }
constructor TABDBMainDetailPopupMenu.Create(AOwner: TComponent);
begin
  inherited;
  FDefaultParentValue:='0';
  CreatePopupMenu;
end;

procedure TABDBMainDetailPopupMenu.CreatePopupMenu;
var
  tempMenuItem:TMenuItem;
begin
  if csDesigning in ComponentState   then exit;

  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Name:='PopupMenu_AddRoot';
  tempMenuItem.Caption:=('增加根分类');
  tempMenuItem.OnClick:=PopupMenu_AddRoot;
  Items.Add(tempMenuItem);

  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Name:='PopupMenu_Add';
  tempMenuItem.Caption:=('增加分类');
  tempMenuItem.OnClick:=PopupMenu_Add;
  Items.Add(tempMenuItem);

  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Name:='PopupMenu_Del';
  tempMenuItem.Caption:=('删除');
  tempMenuItem.OnClick:=PopupMenu_Del;
  Items.Add(tempMenuItem);

  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Name:='PopupMenu_Edit';
  tempMenuItem.Caption:=('修改');
  tempMenuItem.OnClick:=PopupMenu_Edit;
  Items.Add(tempMenuItem);

  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Name:='PopupMenu_EditCaption';
  tempMenuItem.Caption:=('修改名称');
  tempMenuItem.OnClick:=PopupMenu_EditCaption;
  Items.Add(tempMenuItem);

  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Caption:='-';
  Items.Add(tempMenuItem);

  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Name:='PopupMenu_Save';
  tempMenuItem.Caption:=('保存');
  tempMenuItem.OnClick:=PopupMenu_Save;
  Items.Add(tempMenuItem);

  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Name:='PopupMenu_Cancel';
  tempMenuItem.Caption:=('取消');
  tempMenuItem.OnClick:=PopupMenu_Cancel;
  Items.Add(tempMenuItem);

  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Name:='PopupMenu_SetBase';
  tempMenuItem.Caption:=('设为根分类');
  tempMenuItem.OnClick:=PopupMenu_SetBase;
  Items.Add(tempMenuItem);

end;

destructor TABDBMainDetailPopupMenu.Destroy;
begin
  DestroyPopupMenu;
  inherited;
end;

procedure TABDBMainDetailPopupMenu.DestroyPopupMenu;
var
  i:longint;
begin
  for i := Items.Count-1 downto 0  do
  begin
    Items[i].Free;
  end;
end;

procedure TABDBMainDetailPopupMenu.DoPopup(Sender: TObject);
begin
  inherited;
  Items.Find(('增加分类')).Enabled:=false;
  Items.Find(('删除')).Enabled:=false;
  Items.Find(('修改')).Enabled:=false;
  Items.Find(('修改名称')).Enabled:=false;
  Items.Find(('保存')).Enabled:=false;
  Items.Find(('取消')).Enabled:=false;
  Items.Find(('设为根分类')).Enabled:=false;

  if (Assigned(FDataSource)) and
     (Assigned(FDataSource.DataSet)) then
  begin
    if FDataSource.DataSet.CanModify then
    begin
      Items.Find(('增加分类')).Enabled:=True;
      Items.Find(('删除')).Enabled:=not ABDatasetIsEmpty(FDataSource.DataSet);
      Items.Find(('修改')).Enabled:= (not ABDatasetIsEmpty(FDataSource.DataSet)) and (FDataSource.DataSet.State in [dsbrowse]);
      Items.Find(('修改名称')).Enabled:= (not ABDatasetIsEmpty(FDataSource.DataSet)) and (FDataSource.DataSet.State in [dsbrowse]);
      Items.Find(('保存')).Enabled:=FDataSource.DataSet.State in [dsEdit,dsInsert];
      Items.Find(('取消')).Enabled:=FDataSource.DataSet.State in [dsEdit,dsInsert];
      Items.Find(('设为根分类')).Enabled:=not ABDatasetIsEmpty(FDataSource.DataSet);
    end;
  end;
end;

procedure TABDBMainDetailPopupMenu.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if (Operation = opRemove) then
  begin
    if (FDataSource =AComponent)   then
      FDataSource:=nil;
  end;
end;

procedure TABDBMainDetailPopupMenu.DoAddItem(aParentFieldValue: string);
var
  tempStr1:string;
begin
  if (Assigned(FDataSource)) and
     (Assigned(FDataSource.DataSet)) then
  begin
    if InputQuery('增加分类','新分类',tempStr1) then
    begin
      if tempStr1<>EmptyStr then
      begin
        FDataSource.DataSet.Append;
        if (Assigned(FDataSource.DataSet.FindField(FListField)))  then
          FDataSource.DataSet.FindField(FListField).AsString:=tempStr1;
        if (Assigned(FDataSource.DataSet.FindField(FParentField)))  then
          FDataSource.DataSet.FindField(FParentField).AsString:=aParentFieldValue;
        FDataSource.DataSet.Post;
      end;
    end;
  end;
end;

procedure TABDBMainDetailPopupMenu.PopupMenu_Add(Sender: TObject);
var
  tempKey:string;
begin
  tempKey:=FDefaultParentValue;
  if (Assigned(FDataSource)) and
     (Assigned(FDataSource.DataSet)) and
     (Assigned(FDataSource.DataSet.FindField(KeyField))) then
  begin
    tempKey:=FDataSource.DataSet.FindField(KeyField).AsString;
  end;
  DoAddItem(tempKey);
end;

procedure TABDBMainDetailPopupMenu.PopupMenu_AddRoot(Sender: TObject);
begin
  DoAddItem(FDefaultParentValue);
end;

procedure TABDBMainDetailPopupMenu.PopupMenu_Edit(Sender: TObject);
var
  tempStr1:string;
begin
  if (Assigned(FDataSource)) and
     (Assigned(FDataSource.DataSet)) and
     (Assigned(FDataSource.DataSet.FindField(FListField)))  then
  begin
    FDataSource.DataSet.Edit;
  end;
end;

procedure TABDBMainDetailPopupMenu.PopupMenu_EditCaption(Sender: TObject);
var
  tempStr1:string;
begin
  if (Assigned(FDataSource)) and
     (Assigned(FDataSource.DataSet)) and
     (Assigned(FDataSource.DataSet.FindField(FListField)))  then
  begin
    tempStr1:=FDataSource.DataSet.FindField(FListField).AsString;
    if InputQuery('修改分类名称','新名称',tempStr1) then
    begin
      if tempStr1<>EmptyStr then
      begin
        ABSetFieldValue(tempStr1,FDataSource.DataSet.FindField(FListField),True);
      end;
    end;
  end;
end;

procedure TABDBMainDetailPopupMenu.PopupMenu_Cancel(Sender: TObject);
begin
  if (Assigned(FDataSource)) and
     (Assigned(FDataSource.DataSet))  then
    FDataSource.DataSet.Cancel;
end;

procedure TABDBMainDetailPopupMenu.PopupMenu_Del(Sender: TObject);
begin
  if (Assigned(FDataSource)) and
     (Assigned(FDataSource.DataSet))  then
    FDataSource.DataSet.Delete;
end;

procedure TABDBMainDetailPopupMenu.PopupMenu_Save(Sender: TObject);
begin
  if (Assigned(FDataSource)) and
     (Assigned(FDataSource.DataSet))  then
    FDataSource.DataSet.Post;
end;

procedure TABDBMainDetailPopupMenu.PopupMenu_SetBase(Sender: TObject);
begin
  if (Assigned(FDataSource)) and
     (Assigned(FDataSource.DataSet))  then
    ABSetFieldValue(FDefaultParentValue,FDataSource.DataSet.FindField(FParentField),true);
end;


end.
