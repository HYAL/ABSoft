{
字符串及字段名多选择单元
}
unit ABPubSelectStrU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,
  ABPubFuncU,
  ABPubItemListsFrameU,
  ABPubDBU,

  SysUtils,Classes,Controls,Forms,ExtCtrls,DB,StdCtrls, ABPubPanelU;
                    
type
  TABSelectFieldNameOrStForm = class(TABPubForm)
    pnl1: TPanel;
    btn1: TButton;
    btn2: TButton;
    ABItemLists1: TABItemLists;
    procedure btn2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//选择数据集的字段名
//aValue为初始的以aSpaceSign间隔的已选择的字段名串(在列表的右边),返回最后选择后的字段名串(在列表的右边)
function ABSelectFieldNames(var aValue:string;
                     aDataSet:TDataSet;
                     aSpaceSign:string=',';
                     aCaption:string=''):Boolean;
//选择字段名到字段的值中(当前的按钮序号等于检测的按钮序号时才弹出选择框)
//aField=设置的字段
//aCurButtonIndex=当前的按钮序号
//aCheckButtonIndex=检测的按钮序号
function ABSelectFieldNamesToFieldValue(aField: TField;
                                        aCurButtonIndex: Integer=0;
                                        aCheckButtonIndex: Integer=0;
                                        aSpaceSign:string=',';
                                        aCaption:string=''):Boolean;

//选择数据集字段的字段值
function ABSelectFieldValue(var aValue:string;
                             aField: TField;
                             aSpaceSign:string=',';
                             aCaption:string=''):Boolean;overload;overload;
function ABSelectFieldValue(aStrings:Tstrings;
                             aField: TField;
                             aSpaceSign:string=',';
                             aCaption:string=''):Boolean;overload;overload;
//选择字符串
function ABSelectStr(var aLeftValue:string;
                     var aRightValue:string;
                     aSpaceSign:string=',';
                     aCaption:string=''):Boolean;

implementation

{$R *.dfm}

function ABSelectFieldNamesToFieldValue(aField: TField;
                      aCurButtonIndex: Integer;
                      aCheckButtonIndex: Integer;
                      aSpaceSign:string;
                      aCaption:string):Boolean;
var
  tempStr1: string;
begin
  Result:=false;
  if (aCurButtonIndex = aCheckButtonIndex)  then
  begin
    tempStr1 := aField.AsString;
    if (ABSelectFieldNames(tempStr1, aField.DataSet,aSpaceSign,aCaption)) and
      (tempStr1 <> aField.AsString) then
    begin
      ABSetFieldValue(tempStr1, aField);
      Result:=true;
    end;
  end;
end;

function ABSelectStr(var aLeftValue:string;var aRightValue:string;aSpaceSign:string;aCaption:string):Boolean;
var
  tempForm:TABSelectFieldNameOrStForm;
begin
  Result:=false;

  if (aLeftValue<>EmptyStr) or
     (aRightValue<>EmptyStr) then
  begin
    tempForm:=TABSelectFieldNameOrStForm.Create(nil);
    try
      if aCaption=emptystr then
        aCaption:= '多选';
      tempForm.caption:=(aCaption);

      tempForm.ABItemLists1.LabelKey:='';
      tempForm.ABItemLists1.LableCount:=
        ABGetSpaceStrCount(aLeftValue,aSpaceSign)+ABGetSpaceStrCount(aRightValue,aSpaceSign);

      ABStrsToStrings(aLeftValue,aSpaceSign ,tempForm.ABItemLists1.Frame.ABItemList1.Frame.ListBox1.Items);
      ABStrsToStrings(aRightValue,aSpaceSign,tempForm.ABItemLists1.Frame.ABItemList2.Frame.ListBox1.Items);
      tempForm.ABItemLists1.RefreshLabel;

      if tempForm.ShowModal=mrOK then
      begin
        aLeftValue:=ABStringsToStrs(tempForm.ABItemLists1.Frame.ABItemList1.Frame.ListBox1.Items,aSpaceSign);
        aRightValue:=ABStringsToStrs(tempForm.ABItemLists1.Frame.ABItemList2.Frame.ListBox1.Items,aSpaceSign);
        Result:=true;
      end;
    finally
      tempForm.Free;
    end;
  end;
end;

function ABSelectFieldValue(var aValue:string;
                     aField: TField;
                     aSpaceSign:string;
                     aCaption:string):Boolean;
var
  tempStrings:TStrings;
  i:LongInt;
begin
  tempStrings:=TStringList.Create;
  try
    for I := 1 to ABGetSpaceStrCount(aValue,aSpaceSign) do
    begin
      tempStrings.Add(ABGetSpaceStr(aValue, i,aSpaceSign));
    end;
    Result:=ABSelectFieldValue(tempStrings,aField,aSpaceSign,aCaption);
    if Result then
    begin
      aValue:=EmptyStr;
      for I := 0 to tempStrings.count-1 do
      begin
        ABAddstr(aValue,tempStrings[i],aSpaceSign);
      end;
    end;
  finally
    tempStrings.free;
  end;
end;

function ABSelectFieldValue(astrings:Tstrings;aField: TField;aSpaceSign:string;aCaption:string):Boolean;
var
  tempForm:TABSelectFieldNameOrStForm;
  tempStrings:TStringList;
begin
  Result:=false;

  if (Assigned(aField.DataSet)) then
  begin
    astrings.BeginUpdate;
    aField.DataSet.DisableControls;
    tempForm:=TABSelectFieldNameOrStForm.Create(nil);
    try
      if aCaption=emptystr then
        aCaption:= '多选';
      tempForm.caption:=(aCaption);

      tempForm.ABItemLists1.Frame.ABItemList1.Frame.ListBox1.Items.BeginUpdate;
      tempForm.ABItemLists1.Frame.ABItemList2.Frame.ListBox1.Items.BeginUpdate;
      try
        tempForm.ABItemLists1.LabelKey:='';
        tempForm.ABItemLists1.LableCount:=aField.DataSet.RecordCount;
        tempForm.ABItemLists1.Frame.ABItemList2.Frame.ListBox1.Items.Assign(astrings);

        tempStrings:=TStringList.Create;
        try
          //TStringList().Sorted:=true;将自动使用二分法查找，IndexOf速度成倍提高
          tempStrings.Sorted:=true;
          tempStrings.Assign(astrings);
          aField.DataSet.First;
          while not aField.DataSet.Eof do
          begin
            if tempStrings.IndexOf(aField.AsString)<0 then
            begin
              tempForm.ABItemLists1.Frame.ABItemList1.Frame.ListBox1.Items.Add(aField.AsString);
            end;
            aField.DataSet.Next;
          end;
        finally
          tempStrings.Free;
        end;
        tempForm.ABItemLists1.RefreshLabel;
      finally
        tempForm.ABItemLists1.Frame.ABItemList1.Frame.ListBox1.Items.EndUpdate;
        tempForm.ABItemLists1.Frame.ABItemList2.Frame.ListBox1.Items.EndUpdate;
      end;

      if tempForm.ShowModal=mrOK then
      begin
        astrings.Assign(tempForm.ABItemLists1.Frame.ABItemList2.Frame.ListBox1.Items);
        Result:=true;
      end;
    finally
      aField.DataSet.EnableControls;
      astrings.EndUpdate;
      tempForm.Free;
    end;
  end;
end;

function ABSelectFieldNames(var aValue:string;aDataSet:TDataSet;aSpaceSign:string;aCaption:string):Boolean;
var
  tempForm:TABSelectFieldNameOrStForm;
  I: Integer;
begin
  Result:=false;

  if (Assigned(aDataSet)) then
  begin
    tempForm:=TABSelectFieldNameOrStForm.Create(nil);
    try
      if aCaption=emptystr then
        aCaption:= '多选';
      tempForm.caption:=(aCaption);

      tempForm.ABItemLists1.LabelKey:=('字段');
      tempForm.ABItemLists1.Frame.ABItemList1.Frame.ListBox1.Items.Clear;
      tempForm.ABItemLists1.Frame.ABItemList2.Frame.ListBox1.Items.Clear;

      if aDataSet.FieldCount>0 then
      begin
        tempForm.ABItemLists1.LableCount:=aDataSet.FieldCount;
        for I := 0 to aDataSet.FieldCount - 1 do
        begin
          if ABPos(aSpaceSign+aDataSet.Fields[I].FieldName+aSpaceSign,aSpaceSign+aValue+aSpaceSign)>0 then
          begin
            tempForm.ABItemLists1.Frame.ABItemList2.Frame.ListBox1.Items.Add(aDataSet.Fields[I].DisplayLabel);
          end
          else
          begin
            tempForm.ABItemLists1.Frame.ABItemList1.Frame.ListBox1.Items.Add(aDataSet.Fields[I].DisplayLabel);
          end;
        end;
        tempForm.ABItemLists1.RefreshLabel;

        if tempForm.ShowModal=mrOK then
        begin
          aValue:=emptystr;
          for I := 0 to aDataSet.FieldCount - 1 do
          begin
            if tempForm.ABItemLists1.Frame.ABItemList2.Frame.ListBox1.Items.IndexOf(aDataSet.Fields[I].DisplayLabel)>=0 then
            begin
              ABAddstr(aValue,aDataSet.Fields[I].FieldName,aSpaceSign);
            end;
          end;
          Result:=true;
        end;
      end
      else if aDataSet.FieldDefs.Count>0 then
      begin
        tempForm.ABItemLists1.LableCount:=aDataSet.FieldDefs.Count;
        for I := 0 to aDataSet.FieldDefs.Count - 1 do
        begin
          if ABPos(aSpaceSign+aDataSet.FieldDefs.Items[I].Name+aSpaceSign,aSpaceSign+aValue+aSpaceSign)>0 then
          begin
            tempForm.ABItemLists1.Frame.ABItemList2.Frame.ListBox1.Items.Add(aDataSet.FieldDefs.Items[I].Name);
          end
          else
          begin
            tempForm.ABItemLists1.Frame.ABItemList1.Frame.ListBox1.Items.Add(aDataSet.FieldDefs.Items[I].Name);
          end;
        end;
        tempForm.ABItemLists1.RefreshLabel;

        if tempForm.ShowModal=mrOK then
        begin
          aValue:=emptystr;
          for I := 0 to aDataSet.FieldDefs.Count - 1 do
          begin
            if tempForm.ABItemLists1.Frame.ABItemList2.Frame.ListBox1.Items.IndexOf(aDataSet.FieldDefs.Items[I].Name)>=0 then
            begin
              ABAddstr(aValue,aDataSet.FieldDefs.Items[I].Name,aSpaceSign);
            end;
          end;
          Result:=true;
        end;
      end;
    finally
      tempForm.Free;
    end;
  end;
end;

procedure TABSelectFieldNameOrStForm.btn1Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TABSelectFieldNameOrStForm.btn2Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TABSelectFieldNameOrStForm.FormShow(Sender: TObject);
begin
  ABSetListBoxWidth(ABItemLists1.Frame.ABItemList1.Frame.ListBox1);
  ABSetListBoxWidth(ABItemLists1.Frame.ABItemList2.Frame.ListBox1);
end;


end.
