{
多线程单元
}
unit ABPubThreadU;

interface
{$I ..\ABInclude.ini}
uses
  ABPubFuncU,
  ABPubVarU,

  Windows, SysUtils, Classes;


//互斥操作
{互斥方式进行多线程同步
  tempHandle:THandle;
  tempHandle:=ABCreateMutex('TABConnDatabaseForm.LoadConnDataset');
  if tempHandle>0  then
  begin
    if WaitForSingleObject(tempHandle, INFINITE) = WAIT_OBJECT_0 then
    begin
      //同一时刻只可一个线程做的事情
      ReleaseMutex(tempHandle);
      ABCloseMutex('TABConnDatabaseForm.LoadConnDataset');
    end;
  end
  else
  begin
  end;
}
//根据名称创建互斥量
function ABCreateMutex(aName:string):THandle;
//根据名称关闭互斥量
procedure ABCloseMutex(aName:string);
//根据名称得到已创建的互斥量
function ABGetMutex(aName:string):THandle;

//临界区
{临界区方式进行多线程同步
  ABEnterCriticalSection;
  try
    //同一时刻只可一个线程做的事情
  finally
    ABLeaveCriticalSection;
  end;
}
//进入临界区
procedure ABEnterCriticalSection;
//离开临界区
procedure ABLeaveCriticalSection;

var
  //互斥变量列表
  FMutexList:Tstrings;
  //临界区
  ABPubCriticalSection : TRTLCriticalSection;
  //信号
  ABPubSemaphore: THandle;

implementation


procedure ABEnterCriticalSection;
begin
  EnterCriticalSection(ABPubCriticalSection);
end;

procedure ABLeaveCriticalSection;
begin
  LeaveCriticalSection(ABPubCriticalSection);
end;

procedure ABCloseMutex(aName:string);
var
  tempMutex:hwnd;
  i:LongInt;
begin
  i:=FMutexList.IndexOfName(ABStrToName(aName));
  if i>=0 then
  begin
    tempMutex:=StrToInt64def(FMutexList.ValueFromIndex[i],0);
    if tempMutex<>0 then
    begin
      //最后释放互斥对象
      CloseHandle(tempMutex);

      FMutexList.Delete(i);
    end;
  end;
end;

function ABGetMutex(aName:string):THandle;
var
  i:LongInt;
begin
  result:=0;
  i:=FMutexList.IndexOfName(ABStrToName(aName));
  if i>=0 then
  begin
    result:=StrToInt64def(FMutexList.ValueFromIndex[i],0);
  end;
end;

function ABCreateMutex(aName:string):THandle;
begin
  result:=0;
  if (AnsiCompareText(ABAppName,'BDS')<>0) and
     (AnsiCompareText(ABAppName,'Delphi32')<>0) then
  begin
    aName:=ABStrToName(aName);
    result:=ABGetMutex(aName);
    if result<=0 then
    begin
      //此处取得的有可能是其它程序已创建的互拆量的副本句柄
      //因为如果其它程序已创建互拆量时，再createMutex则是OpenMutex
      result:=createMutex(nil,false,PChar(aName));
      FMutexList.Add(aName+'='+IntToStr(result));
    end
    else
    begin
      MessageBox(0,PChar('互拆量['+aName+']已创建'),'提示',MB_ICONINFORMATION);
    end;
  end;
end;


procedure ABInitialization;
begin
  FMutexList:=TStringList.Create;
  //本程序进程内的互拆量，为本程序内部的多线程服务
  ABCreateMutex(ABAppFullName+'_insideCheck');
  InitializeCriticalSection(ABPubCriticalSection);
end;

procedure ABFinalization;
var
  i:LongInt;
begin
  CloseHandle(ABPubSemaphore);
  DeleteCriticalSection(ABPubCriticalSection);
  for I := FMutexList.Count-1 downto 0 do
  begin
    ABCloseMutex(FMutexList.Names[i]);
  end;
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;


end.
