{
主线程的可控主从进度条单元
//单组进度条
var
  i:LongInt;
  tempMainMax:LongInt;
begin
  tempMainMax:=100;
  ABGetPubManualProgressBarForm.Title:='执行中';
  ABGetPubManualProgressBarForm.MainMin:=0;
  ABGetPubManualProgressBarForm.MainPosition:=0;
  ABGetPubManualProgressBarForm.MainMax:=tempMainMax;
  ABGetPubManualProgressBarForm.Run;
  try
    for I := 0 to tempMainMax - 1 do
    begin
      ABGetPubManualProgressBarForm.NextMainIndex;

      ABSleep(10);
    end;
  finally
    ABGetPubManualProgressBarForm.Stop;
  end;
end;
//主从进度条
var
  i,j:LongInt;
  tempMainMax,tempDetailMax:LongInt;
begin
  tempMainMax:=10;
  tempDetailMax:=10;
  ABGetPubManualProgressBarForm.Title:='执行中';
  ABGetPubManualProgressBarForm.MainMin:=0;
  ABGetPubManualProgressBarForm.MainPosition:=0;
  ABGetPubManualProgressBarForm.MainMax:=tempMainMax;
  ABGetPubManualProgressBarForm.DetailVisible:=true;
  ABGetPubManualProgressBarForm.DetailMin:=0;
  ABGetPubManualProgressBarForm.DetailPosition:=0;
  //传入true表示执行过程中可以关闭退出
  ABGetPubManualProgressBarForm.Run(true);
  try
    for I := 0 to tempMainMax - 1 do
    begin
      ABGetPubManualProgressBarForm.NextMainIndex;
      ABGetPubManualProgressBarForm.DetailMax:=tempDetailMax;
      ABGetPubManualProgressBarForm.DetailPosition:=0;
      for j := 0 to tempDetailMax - 1 do
      begin
        if ABGetPubManualProgressBarForm.ExitRun  then
          exit;

        ABGetPubManualProgressBarForm.NextDetailIndex;
        ABSleep(1);
      end;
    end;
  finally
    ABGetPubManualProgressBarForm.DetailVisible:=false;
    ABGetPubManualProgressBarForm.Stop;
  end;
end;
}
unit ABPubManualProgressBarU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubVarU,
  ABPubConstU,
  ABPubLocalParamsU,
  ABPubFormU,

  Forms,Controls,ExtCtrls,Classes,StdCtrls,SysUtils,math, jpeg;

type
  TABManualProgressBarForm = class(TABPubForm)
    Image1: TImage;
    Label1: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    Panel11: TPanel;
    Panel12: TPanel;
    Panel13: TPanel;
    Panel14: TPanel;
    Panel15: TPanel;
    Panel16: TPanel;
    Panel17: TPanel;
    Panel18: TPanel;
    Panel19: TPanel;
    Panel20: TPanel;
    Panel21: TPanel;
    Panel22: TPanel;
    Panel23: TPanel;
    Panel24: TPanel;
    Panel25: TPanel;
    Panel26: TPanel;
    Panel27: TPanel;
    Panel28: TPanel;
    Panel29: TPanel;
    Panel30: TPanel;
    Panel31: TPanel;
    Panel32: TPanel;
    Panel33: TPanel;
    Panel34: TPanel;
    Panel35: TPanel;
    Panel36: TPanel;
    Panel37: TPanel;
    Panel38: TPanel;
    Panel39: TPanel;
    Panel40: TPanel;
    Label2: TLabel;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Label1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FMainPanels:array[1..20] of TPanel;
    FDetailPanels:array[1..20] of TPanel;

    FMainMin: LongInt;
    FDetailPosition: LongInt;
    FDetailMax: LongInt;
    FMainPosition: LongInt;
    FDetailMin: LongInt;
    FMainMax: LongInt;
    FDetailVisible: Boolean;
    FExitRun: Boolean;
    function GetTitle: string;
    procedure Settitle(const Value: string);
    procedure SetDetailPosition(const Value: LongInt);
    procedure SetMainPosition(const Value: LongInt);
    procedure SetDetailVisible(const Value: Boolean);
    function GetRemark: string;
    procedure SetRemark(const Value: string);
 protected
    procedure DoCreate ; override;
    procedure DoClose(var Action: TCloseAction); override;
    { Private declarations }
  public
    //刷新所有控件
    procedure RefreshAll;
    //运行显示进度条
    procedure Run(aHaveExit:Boolean=False);
    //停止关闭进度条
    procedure Stop;

    //进度条标题
    property Title:string read GetTitle write Settitle;
    //进度条描述
    property Remark:string read GetRemark write SetRemark;
    //明细进度条是否显示
    property DetailVisible:Boolean read FDetailVisible write SetDetailVisible;
    //进度条退出标志
    property ExitRun:Boolean read FExitRun write FExitRun;

    //主进度条位置
    property MainPosition:LongInt read FMainPosition write SetMainPosition;
    //明细进度条位置
    property DetailPosition:LongInt read FDetailPosition write SetDetailPosition;
    //主进度条最大值
    property MainMax:LongInt read FMainMax write FMainMax;
    //明细进度条最大值
    property DetailMax:LongInt read FDetailMax write FDetailMax;
    //主进度条最小值
    property MainMin:LongInt read FMainMin write FMainMin;
    //明细进度条最小值
    property DetailMin:LongInt read FDetailMin write FDetailMin;

    //主进度条下一位置
    procedure NextMainIndex;
    //明细进度条下一位置
    procedure NextDetailIndex;

    //进度条暂停
    procedure Pause;
    //进度条恢复
    procedure Continue;
    { Public declarations }
  end;

//取手工进度条
function ABGetPubManualProgressBarForm: TABManualProgressBarForm;

var
  ABProgressState:TABProgressState;

implementation

{$R *.dfm}

var
  ABPubManualProgressBarForm: TABManualProgressBarForm;

function ABGetPubManualProgressBarForm: TABManualProgressBarForm;
begin
  if not Assigned(ABPubManualProgressBarForm) then
    ABPubManualProgressBarForm := TABManualProgressBarForm.Create(nil);

  Result := ABPubManualProgressBarForm;
end;

procedure TABManualProgressBarForm.Continue;
begin
  if ABLocalParams.Debug then
    exit;
  if ABProgressState=PsStop then
    exit;

  if not showing then
  begin
    BringToFront;
    Show;
    Application.ProcessMessages;
  end;
end;

procedure TABManualProgressBarForm.DoClose(var Action: TCloseAction);
begin
  inherited;
  FExitRun:=True;

  ABProgressState:=PsStop;
end;

procedure TABManualProgressBarForm.DoCreate;
begin
  inherited;
end;

procedure TABManualProgressBarForm.FormCreate(Sender: TObject);
begin
  FMainPanels[1 ]:=Panel1   ;
  FMainPanels[2 ]:=Panel2   ;
  FMainPanels[3 ]:=Panel3   ;
  FMainPanels[4 ]:=Panel4   ;
  FMainPanels[5 ]:=Panel5   ;
  FMainPanels[6 ]:=Panel6   ;
  FMainPanels[7 ]:=Panel7   ;
  FMainPanels[8 ]:=Panel8   ;
  FMainPanels[9 ]:=Panel9   ;
  FMainPanels[10]:=Panel10  ;
  FMainPanels[11]:=Panel11  ;
  FMainPanels[12]:=Panel12  ;
  FMainPanels[13]:=Panel13  ;
  FMainPanels[14]:=Panel14  ;
  FMainPanels[15]:=Panel15  ;
  FMainPanels[16]:=Panel16  ;
  FMainPanels[17]:=Panel17  ;
  FMainPanels[18]:=Panel18  ;
  FMainPanels[19]:=Panel19  ;
  FMainPanels[20]:=Panel20  ;

  FDetailPanels[1 ]:=Panel21   ;
  FDetailPanels[2 ]:=Panel22   ;
  FDetailPanels[3 ]:=Panel23   ;
  FDetailPanels[4 ]:=Panel24   ;
  FDetailPanels[5 ]:=Panel25   ;
  FDetailPanels[6 ]:=Panel26   ;
  FDetailPanels[7 ]:=Panel27   ;
  FDetailPanels[8 ]:=Panel28   ;
  FDetailPanels[9 ]:=Panel29   ;
  FDetailPanels[10]:=Panel30  ;
  FDetailPanels[11]:=Panel31  ;
  FDetailPanels[12]:=Panel32  ;
  FDetailPanels[13]:=Panel33  ;
  FDetailPanels[14]:=Panel34  ;
  FDetailPanels[15]:=Panel35  ;
  FDetailPanels[16]:=Panel36  ;
  FDetailPanels[17]:=Panel37  ;
  FDetailPanels[18]:=Panel38  ;
  FDetailPanels[19]:=Panel39  ;
  FDetailPanels[20]:=Panel40  ;
end;

procedure TABManualProgressBarForm.FormShow(Sender: TObject);
begin
  FormStyle:=fsstayontop;
end;

function TABManualProgressBarForm.GetRemark: string;
begin
  Result:= Label2.Caption;
end;

function TABManualProgressBarForm.GetTitle: string;
begin
  Result:= Label1.Caption;
end;

procedure TABManualProgressBarForm.Label1DblClick(Sender: TObject);
begin
  Stop;
end;

procedure TABManualProgressBarForm.NextDetailIndex;
begin
  DetailPosition:=FDetailPosition+1;
end;

procedure TABManualProgressBarForm.NextMainIndex;
begin
  MainPosition:=FMainPosition+1;
end;

procedure TABManualProgressBarForm.Pause;
begin
  if ABLocalParams.Debug then
    exit;

  if ABProgressState=PsStop then
    exit;

  if showing then
  begin
    BringToFront;
    Hide;
    Application.ProcessMessages;
  end;
end;

procedure TABManualProgressBarForm.SetDetailPosition(const Value: LongInt);
var
  tempNewIndex:LongInt;
  i:LongInt;
begin
  if ABProgressState=PsStop then
    exit;

  FDetailPosition := Value;

  if FDetailPosition>FDetailMax then
    exit;

  //窗体失去显示时激活一下
  if (not Focused) and (CanFocus) and (Showing) then
  begin
    SetFocus;
    //BringToFront;
  end;

  tempNewIndex:=-1;
  if FDetailPosition=FDetailMax then
  begin
    tempNewIndex:=20;
  end
  else if FDetailPosition=FDetailMin then
  begin
    tempNewIndex:=1;
  end
  else
  begin
    if FDetailMax-FDetailMin<>0 then
      tempNewIndex:=Floor((FDetailPosition- FDetailMin)/((FDetailMax-FDetailMin)/20));
  end;

  if (tempNewIndex>=1) and (tempNewIndex<=20) then  //设置当前状态的颜色
  begin
    for I := 1 to 20 do
    begin
      if FDetailPanels[i].Color<>$00C49E3C then
        FDetailPanels[i].Color:=$00C49E3C;
    end;
    FDetailPanels[tempNewIndex].Color:=$00FBF404;
  end;

  Application.ProcessMessages;
end;

procedure TABManualProgressBarForm.SetMainPosition(const Value: LongInt);
var
  tempNewIndex:LongInt;
  i:LongInt;
begin
  if ABProgressState=PsStop then
    exit;
  //if (FMainMax<=1) and (not FDetailVisible) then
  //  exit;

  FMainPosition := Value;

  if FMainPosition>FMainMax then
    exit;


  //窗体失去显示时激活一下
  if (not Focused) and (CanFocus) and (Showing) then
  begin
    //ABShow(1);
    SetFocus;
    //BringToFront;
  end;

  Label4.Caption:=inttostr(FMainPosition)+'/'+inttostr(FMainMax);
  tempNewIndex:=-1;
  if FMainPosition=FMainMax then
  begin
    tempNewIndex:=20;
  end
  else if FMainPosition=FMainMin then
  begin
    tempNewIndex:=1;
  end
  else
  begin
    if FMainMax-FMainMin<>0 then
      tempNewIndex:=Floor((FMainPosition- FMainMin)/((FMainMax-FMainMin)/20));
  end;

  if (tempNewIndex>=1) and (tempNewIndex<=20) then  //设置当前状态的颜色
  begin
    for I := 1 to 20 do
    begin
      if FMainPanels[i].Color<>$00C49E3C then
        FMainPanels[i].Color:=$00C49E3C;
    end;
    FMainPanels[tempNewIndex].Color:=$00FBF404;
  end;

  Application.ProcessMessages;
end;

procedure TABManualProgressBarForm.SetRemark(const Value: string);
begin
  Label2.Caption:=Value;
  //RefreshAll;
end;

procedure TABManualProgressBarForm.Settitle(const Value: string);
begin
  Label1.Caption:=Value;
  RefreshAll;
end;

procedure TABManualProgressBarForm.RefreshAll;
begin
  Image1.Repaint;
  Image1.Refresh;
  Label1.Repaint;
  Label1.Refresh;
  Label2.Repaint;
  Label2.Refresh;
  Refresh;
  Application.ProcessMessages;
end;

procedure TABManualProgressBarForm.SetDetailVisible(const Value: Boolean);
var
  i:LongInt;
begin
  FDetailVisible := Value;
  for I := 1 to 20 do
  begin
    FDetailPanels[i].Visible:=FDetailVisible;
  end;
end;

procedure TABManualProgressBarForm.Run(aHaveExit:Boolean);
var
  i:LongInt;
begin
 if ABLocalParams.Debug then
   exit;

  caption:=emptystr;
  FExitRun:=false;
  ABProgressState:=PsRun;

  if (FMainMax<=1) and (not FDetailVisible) then
    exit;

  for I := 1 to 20 do
  begin
    FMainPanels[i].Color:=$00C49E3C;
  end;
  for I := 1 to 20 do
  begin
    FDetailPanels[i].Color:=$00C49E3C;
  end;

  FMainPosition:=0;
  FDetailPosition:=0;

  Label2.Caption:=EmptyStr;
  if aHaveExit then
  begin
    BorderStyle:=bsToolWindow;
    Height:=225;
  end
  else
  begin
    BorderStyle:=bsNone;
    Height:=202;
  end;

  Label4.Visible:=true;
  Show;
  RefreshAll;
end;

procedure TABManualProgressBarForm.Stop;
var
  tempBackExitRun:boolean;
begin
  tempBackExitRun := FExitRun;
  try
    ABProgressState:=PsStop;

    Label2.Caption:=emptystr;
    Label4.Caption:=emptystr;
    HIDE;

    Application.ProcessMessages;
  finally
    FExitRun:=tempBackExitRun;
  end;
end;


Initialization

Finalization
  if Assigned(ABPubManualProgressBarForm) then
    ABPubManualProgressBarForm.Free;

end.
