{
��ʾ��༭ͼƬ�ֶε�Ԫ
}
unit ABPubImageFieldU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,
  ABPubFuncU,

  SysUtils,Classes,Controls,Forms,Dialogs,ExtCtrls,DB,DBCtrls,StdCtrls;

type
  TABImageFieldForm = class(TABPubForm)
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    DBImage1: TDBImage;
    SaveDialog1: TSaveDialog;
    OpenDialog1: TOpenDialog;
    Button3: TButton;
    DataSource1: TDataSource;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//��ʾ��༭ͼƬ�ֶ�
function ABShowEditImageField(aDataSet:TDataSet;aFieldName:string;aReadOnly:Boolean=false;aCaption:string=''): boolean;

implementation

{$R *.dfm}


function ABShowEditImageField(aDataSet:TDataSet;aFieldName:string;aReadOnly:Boolean=false;aCaption:string=''): boolean;
var
  tempForm: TABImageFieldForm;
begin
  result:=False;
  if not Assigned(aDataSet.FindField(aFieldName)) then
    exit;

  tempForm := TABImageFieldForm.Create(nil);
  tempForm.DataSource1.DataSet:=aDataSet;
  if aCaption=EmptyStr then
    aCaption:=aDataSet.FindField(aFieldName).DisplayLabel;
  tempForm.Button1.Enabled:= not aReadOnly;
  tempForm.Caption:=aCaption;
  try
    tempForm.DBImage1.DataField := aFieldName;
    tempForm.DBImage1.ReadOnly := aReadOnly;
    if tempForm.ShowModal=mrok then
    begin
      result:=true;
    end;
  finally
    tempForm.Free;
  end;
end;

procedure TABImageFieldForm.Button1Click(Sender: TObject);
begin
  if (OpenDialog1.Execute)  then
  begin
    if (FileExists(OpenDialog1.FileName)) then
    begin
      if  DBImage1.DataSource.DataSet.State in [dsbrowse] then
        DBImage1.DataSource.DataSet.Edit;

      if DBImage1.DataSource.DataSet.State in  [dsedit,dsinsert] then
      begin
        TBlobField(DBImage1.DataSource.DataSet.FindField(DBImage1.DataField)).LoadFromFile(OpenDialog1.FileName);
      end;
    end;
  end;
end;

procedure TABImageFieldForm.Button2Click(Sender: TObject);
begin
  if (SaveDialog1.Execute)  then
  begin
    TBlobField(DBImage1.DataSource.DataSet.FindField(DBImage1.DataField)).SaveToFile(ABGetSaveDialogFileName(SaveDialog1));
  end;   
end;

procedure TABImageFieldForm.Button3Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;


end.
