{
多语言管理单元
}
unit ABPubLanguageEditU;
                             
interface
{$I ..\ABInclude.ini}

uses
  abpubConstU,
  ABPubInPutStrU,
  abpubVarU,
  abpubmessageU,

  ABPubFuncU,

  Windows,SysUtils,Variants,Classes,Graphics,Controls,Forms,StdCtrls,ExtCtrls,
  Grids,math;

type
  TABPubLanguageEditForm = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    StringGrid1: TStringGrid;
    Button2: TButton;
    Button3: TButton;
    Button5: TButton;
    Button4: TButton;
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure StringGrid1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure Button5Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure StringGrid1SetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure FormDestroy(Sender: TObject);
    procedure StringGrid1Exit(Sender: TObject);
  private
    FSelRow,FSelCol:Integer;
    FBegUpdate:boolean;
    procedure LoadIniLanguage(aCol: Integer; aIniFileName: string);
    procedure SetRowHeight;
    procedure SaveEdit(ACol, ARow: Integer;aClose:boolean);
    function CanAdd(aLanguageName: string): Boolean;
    procedure SetNameColWidth;
    { Private declarations }
  public                            
    { Public declarations }
  end;

//语言编辑
procedure ABLanguageEdit;

implementation

{$R *.dfm}

procedure ABLanguageEdit;
var
  tempLocalEditForm: TABPubLanguageEditForm;
begin
  tempLocalEditForm := TABPubLanguageEditForm.Create(nil);
  try
    tempLocalEditForm.ShowModal;
  finally
    tempLocalEditForm.Free;
  end;
end;

procedure TABPubLanguageEditForm.Button3Click(Sender: TObject);
begin
  close;
end;

//根据语言行设置行的高度
procedure TABPubLanguageEditForm.SetRowHeight;
var
  i,j,k: longint;
begin
  for I := 1 to StringGrid1.RowCount-1 do
  begin
    k:=1;
    for j := 2 to StringGrid1.ColCount-1 do
    begin
      k:=max(k,Ceil(Canvas.TextWidth(StringGrid1.Cells[j,i])/StringGrid1.DefaultColWidth));
    end;
    if k<=1 then
      StringGrid1.RowHeights[i]:=StringGrid1.DefaultRowHeight
    else
      StringGrid1.RowHeights[i]:=2*StringGrid1.DefaultRowHeight;
  end;
end;

//设置原串行宽度
procedure TABPubLanguageEditForm.SetNameColWidth;
var
  i,k: longint;
begin
  k:=0;
  for I := 1 to StringGrid1.RowCount-1 do
  begin
    k:=max(k,5+Canvas.TextWidth(StringGrid1.Cells[1,i]));
  end;
  StringGrid1.ColWidths[1]:=k;
end;

procedure TABPubLanguageEditForm.LoadIniLanguage(aCol:LongInt;aIniFileName:string);
var
  j,k: longint;

  tempValue:string;
  tempStringsCurField:TStrings;
begin
  //设置语言标题为文件名
  StringGrid1.Cells[aCol,0]:=ChangeFileExt(aIniFileName,'');
  tempStringsCurField := TStringList.Create;
  try
    //加载此语言的内容
    {$IF (ABDelphiVersion_int>=200)}      //200 =DELPHI2009
      tempStringsCurField.LoadFromFile(ABLanguagesPath+aIniFileName,TEncoding.Unicode);
    {$ELSE}
      tempStringsCurField.LoadFromFile(ABLanguagesPath+aIniFileName);
    {$IFEND}

    for j := 0 to tempStringsCurField.Count - 1 do
    begin
      if AnsiCompareText(copy(Trim(tempStringsCurField[j]),1,length(ABSubStrBegin)),ABSubStrBegin)=0 then
      begin
        //得到当前原文在表格原文中的索引号
        k:=StringGrid1.Cols[1].IndexOf(ABStringReplace(tempStringsCurField.Names[j],ABSubStrBegin,''));
        //如果在表格原文中没有找到则新加一条原文数据
        if k<0 then
        begin
          StringGrid1.RowCount:=StringGrid1.RowCount+1;
          k:=StringGrid1.RowCount-1;
          StringGrid1.Cells[0,k]:=inttostr(k);
          StringGrid1.Cells[1,k]:=ABStringReplace(tempStringsCurField.Names[j],ABSubStrBegin,'');
        end;

        if ((j+1)<=(tempStringsCurField.Count - 1)) and
           (AnsiCompareText(copy(Trim(tempStringsCurField[j+1]),1,length(ABSubStrBegin)),ABSubStrBegin)<>0) then
          tempValue:=trim(ABGetItemValue(tempStringsCurField.Text,tempStringsCurField.Names[j],'=',ABSubStrBegin))
        else
          tempValue:=tempStringsCurField.ValueFromIndex[j];

        StringGrid1.Cells[aCol,k]:=tempValue;
      end;
    end;
  finally
    tempStringsCurField.Free;
  end;
end;

procedure TABPubLanguageEditForm.FormCreate(Sender: TObject);
var
  i: longint;
  tempStringsFileNames:TStrings;
begin
  FSelRow:=0;
  FSelCol:=0;
  StringGrid1.Cells[0,0]:='序号';
  StringGrid1.Cells[1,0]:='原文';
  FBegUpdate:=False;
  tempStringsFileNames := TStringList.Create;
  try
    ABGetFileList(tempStringsFileNames,ABLanguagesPath,['ini'],1,True,nil,False);
    if tempStringsFileNames.Count>0 then
    begin
      StringGrid1.ColCount:=StringGrid1.ColCount+tempStringsFileNames.Count;
      for i := 0 to tempStringsFileNames.Count - 1 do
      begin
        LoadIniLanguage(i+2,tempStringsFileNames[i]);
      end;

      if StringGrid1.RowCount>1 then
      begin
        SetRowHeight;
        StringGrid1.FixedRows:=1;
      end;
      if StringGrid1.ColCount>1 then
      begin
        SetNameColWidth;
        StringGrid1.FixedCols:=1;
      end;
    end;
  finally
    tempStringsFileNames.Free;
    FBegUpdate:=true;
  end;
end;

procedure TABPubLanguageEditForm.FormDestroy(Sender: TObject);
begin
  inherited;
  SaveEdit(StringGrid1.Col,StringGrid1.Row,true);
end;

procedure TABPubLanguageEditForm.StringGrid1Exit(Sender: TObject);
var
 CanSelect: Boolean;
begin
  inherited;
  StringGrid1SelectCell(StringGrid1,0,0,CanSelect);
end;

procedure TABPubLanguageEditForm.StringGrid1SelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  inherited;
  SaveEdit(ACol,ARow,false);

  if (ARow = 0) or
     (ACol <= 1) then
    StringGrid1.Options := StringGrid1.Options - [goEditing]
  else
    StringGrid1.Options := StringGrid1.Options + [goEditing];
end;

procedure TABPubLanguageEditForm.SaveEdit(ACol,ARow:LongInt;aClose:boolean);
var
  i:LongInt;
  tempMulLine:boolean;
  tempFileName,
  tempName,
  tempBeforeValue,
  tempNewValue,
  tempStr:string;
  tempStringsCurField:TStrings;
begin
  //保存编辑的数据
  if (FBegUpdate) and
     (FSelRow>0) and
     (FSelCol>1) and
     ((aClose) or (StringGrid1.EditorMode) and ((FSelRow<>ARow) or (FSelCol<>ACol)) ) then
  begin
    tempFileName:=ABLanguagesPath+StringGrid1.Cells[FSelCol,0]+'.ini';
    tempName    :=ABSubStrBegin+StringGrid1.Cells[1,FSelRow];
    tempNewValue:=StringGrid1.Cells[FSelCol,FSelRow];

    tempStringsCurField := TStringList.Create;
    try
      //加载此语言的内容
      {$IF (ABDelphiVersion_int>=200)}      //200 =DELPHI2009
        tempStringsCurField.LoadFromFile(tempFileName,TEncoding.Unicode);
      {$ELSE}
        tempStringsCurField.LoadFromFile(tempFileName);
      {$IFEND}

      tempBeforeValue:=EmptyStr;
      tempMulLine:=False;
      i:=tempStringsCurField.IndexOfName(tempName);
      if i<0 then
      begin
        i:= tempStringsCurField.Add(tempName+'='+tempNewValue);
      end
      else
      begin
        if ((i+1)<=(tempStringsCurField.Count - 1)) and
           (AnsiCompareText(copy(Trim(tempStringsCurField[i+1]),1,length(ABSubStrBegin)),ABSubStrBegin)<>0) then
        begin
          tempBeforeValue:=trim(ABGetItemValue(tempStringsCurField.Text,tempStringsCurField.Names[i],'=',ABSubStrBegin));
          tempMulLine:=True;
        end
        else
        begin
          tempBeforeValue:=tempStringsCurField.ValueFromIndex[i];
          tempMulLine:=False;
        end;
      end;

      if tempBeforeValue<>tempNewValue then
      begin
        if tempMulLine then
        begin
          tempStr:=tempStringsCurField.Text;
          if ABSetItemValue(tempStr,
                            tempName+'=',
                            tempNewValue+ABEnterWrapStr,
                            ABSubStrBegin) then
          begin
            tempStringsCurField.Text:= tempStr;
          end;
        end
        else
        begin
          tempStringsCurField.ValueFromIndex[i]:=tempNewValue;
        end;

        {$IF (ABDelphiVersion_int>=200)}      //200 =DELPHI2009
          tempStringsCurField.SaveToFile(tempFileName,TEncoding.Unicode);
        {$ELSE}
          tempStringsCurField.SaveToFile(tempFileName);
        {$IFEND}
      end;
    finally
      tempStringsCurField.Free;
    end;
  end;
end;

procedure TABPubLanguageEditForm.StringGrid1SetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
begin
  inherited;
  FSelRow := ARow;
  FSelCol:=ACol;
end;

function TABPubLanguageEditForm.CanAdd(aLanguageName:string):Boolean;
begin
  result:=true;
  if (AnsiCompareText('China',aLanguageName)=0) or
     (AnsiCompareText('ChinaTw',aLanguageName)=0) or
     (AnsiCompareText('原文',aLanguageName)=0) then
  begin
    result:=false;
    ABShow('系统默认语言[%s]已存在,不能增加.',[aLanguageName]);
  end
  else if StringGrid1.Rows[0].IndexOf(aLanguageName)>=0 then
  begin
    result:=false;
    ABShow('语言[%s]已存在,不能增加.',[aLanguageName]);
  end
end;

procedure TABPubLanguageEditForm.Button5Click(Sender: TObject);
var
  tempStr:string;
begin
  inherited;
  if (ABInPutStr('语言名称',tempStr)) and
     (tempStr<>EmptyStr) then
  begin
    if CanAdd(tempStr) then
    begin
      StringGrid1.ColCount:=StringGrid1.ColCount+1;
      StringGrid1.Cells[StringGrid1.ColCount-1,0]:=tempStr;

      ABWriteTxt(ABLanguagesPath+tempStr+'.ini',emptystr);
    end;
  end;
end;

procedure TABPubLanguageEditForm.Button1Click(Sender: TObject);
var
  tempFileName,
  tempFileName_Long:string;
begin
  inherited;
  tempFileName_Long:=ABSelectFile('',emptystr,'Ini 语言文件|*.ini');
  tempFileName:=ABGetFilename(tempFileName_Long);
  if (tempFileName<>emptystr) then
  begin
    if CanAdd(tempFileName) then
    begin
      StringGrid1.ColCount:=StringGrid1.ColCount+1;
      StringGrid1.Cells[StringGrid1.ColCount-1,0]:=tempFileName;
      ABCopyFile(tempFileName_Long,ABLanguagesPath+tempFileName+'.ini');
      LoadIniLanguage(StringGrid1.ColCount-1,tempFileName+'.ini');
      SetRowHeight;
    end;
  end;
end;

procedure TABPubLanguageEditForm.Button2Click(Sender: TObject);
var
  i, cr : integer;
  tempStr:string;
begin
  tempStr:=StringGrid1.Cells[StringGrid1.Col,0];

  if (AnsiCompareText('China',tempStr)=0) or
     (AnsiCompareText('ChinaTw',tempStr)=0) or
     (AnsiCompareText('原文',tempStr)=0) then
  begin
    ABShow('系统默认语言[%s]不用能删除.',[tempStr]);
  end
  else
  begin
    if ABShow('确认删除语言[[%s]]吗?',[tempStr],['是','否'],1)=1 then
    begin
      ABCopyFile(ABLanguagesPath+tempStr+'.ini',
                 ABBackPath+tempStr+'['+ABStrToName(ABDateTimeToStr(Now))+'].ini');

      cr := StringGrid1.Selection.Left;
      for i := cr + 1 to StringGrid1.ColCount - 1 do
        StringGrid1.Cols[i-1].Assign(StringGrid1.Cols[i]);
      StringGrid1.ColCount := StringGrid1.ColCount - 1;

      ABDeleteFile(ABLanguagesPath+tempStr+'.ini');
    end;
  end;
end;

procedure TABPubLanguageEditForm.Button4Click(Sender: TObject);
var
  tempFileName:string;
begin
  inherited;
  tempFileName:=ABSaveFile('',StringGrid1.Cells[StringGrid1.Col,0]+'.ini','Ini 语言文件|*.ini');
  if (tempFileName<>emptystr) then
  begin
    ABCopyFile(ABLanguagesPath+StringGrid1.Cells[StringGrid1.Col,0]+'.ini',tempFileName);
  end;
end;

end.

