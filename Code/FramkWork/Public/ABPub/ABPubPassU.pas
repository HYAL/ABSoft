{
加解密单元
}
unit ABPubPassU;

interface
{$I ..\ABInclude.ini}
uses
  abPubUserU,
  ABPubMessageU,
  ABPubVarU,
  ABPubConstU,
  ABPubMd5U,
  ABPubManualProgressBarU,
  ABPubFuncU,
  ABPubDogKeyU,

  Windows,sysutils,classes;



//采用公钥与私钥相结合的方式,私钥为aPassWord,公钥为aKey
//加密
function ABDOPassWord(aPassWord: string;
                      aKey: string = ABSoftName;
                      aSameSeed:boolean=false): string;
//解密
function ABUnDOPassWord(aPassWord: string;
                        aKey: string = ABSoftName): string;

//检测密文是否由明文+公钥产生，  aWord=明文，aPassWord=密文，aKey=公钥
function ABCheckPassword(aWord,
                         aPassWord: string;
                         aKey: string = ABSoftName): boolean;
//取得注册码（采用ＭＤ５,不可逆转）
function ABDoRegisterKey(aPassword: string;
                         aKey: string = ABSoftName;
                         aLen: longint= 16): string;
//检测授权文件
function ABCheckLicense(aLicenseFileName,
                        aRegName:string;
                        aCurDatetime:TDatetime;
                        var aSubsequentClientCount:longint;
                        aShowMsg:boolean=true):Boolean;

//检测三方加密狗指定位置指定长度是否是指定字符
Function ABCheckLinkUsbSoftDog(aHKey,
                               aLKey:string;
                               aBeginAddress,
                               aLeng:LongInt;
                               aValue:string):Boolean;

implementation
const
  //每个密码字符转成的密文数字长度
  ABPassCharIntLength: LongInt= 6;
var
  KeyPath: array[0..Max_Path] of ansichar = ''; //储存加密锁所在路径字符串；


Function ABCheckLinkUsbSoftDog(aHKey, aLKey:string;aBeginAddress,aLeng:LongInt;aValue:string):Boolean;
var
  KeyResult : Integer;
  OutChar: pansichar;
begin
  result:=false;
  //从加密锁中读取字符串
  OutChar:= AllocMem(aLeng+1); //这里增加1，用于储存结束字符串
  ZeroMemory(OutChar,aLeng+1);
  try
    KeyResult:= YReadString(OutChar,aBeginAddress, aLeng,pansichar(AnsiString(aHKey)),pansichar(AnsiString(aLKey)), KeyPath);
    if (KeyResult= 0) and (AnsiCompareText(string(ansistring(OutChar)),aValue)=0) then
    begin
      //ShowMessage('已成功读取加密锁EPROM中的数据！'+OutChar)
      result:=True;
    end
    else
    begin
      ABGetPubManualProgressBarForm.Pause;
      try
        MessageBox(0,'系统未检查到正确的加密狗,程序将退出'+ABEnterWrapStr+'请检查是否插入了加密狗,然后再重启程序.','提示',MB_ICONINFORMATION);
      finally
        ABGetPubManualProgressBarForm.Continue;
      end;
    end;
  finally
    FreeMem(OutChar);
  end;
end;

function ABDoRegisterKey(aPassword: string; aKey: string; aLen: longint):
  string;
var
  md5_con: MD5Context;
  md5_str: md5digest;
  temInt, i: longint;
begin
  Result := EmptyStr;
  aPassword := aPassword + aKey;
  if aPassWord = emptystr then
    Exit;

  temInt := 0;
  for i := 1 to length(aPassword) do
  begin
    temInt := temInt + Ord(aPassword[i]) + i;
  end;
  temInt := temInt + temInt mod length(aPassword);
  aPassword := aPassword + IntToStr(temInt);

  MD5Init(md5_con);
  MD5Update(md5_con, PAnsiChar(AnsiString(aPassword)), length(aPassword));
  MD5Final(md5_con, md5_str);
  Result := ABSpaceStr(UpperCase(copy(MD5Print(md5_str), 1, aLen)), 4, '-');
end;

function ABDoPassword(aPassWord: string; aKey: string;aSameSeed:boolean): string;
var
  temInt, i, j: longint;
begin
  Result := aPassWord;
  if aPassWord = emptystr then
    Exit;

  Result := emptystr;
  if aSameSeed then
  begin
    temInt:=1;
  end
  else
  begin
    Randomize;
    temInt := ABTrunc(abs(Random(ABTicker) mod 128));
  end;
  for i := 1 to length(aPassWord) do
  begin
    if i > Length(aKey) then
      j := Length(aKey)
    else
      j := i;

    Result := Result + ABFillStr(IntToStr(Ord(aPassWord[i]) + temInt + i +Ord(aKey[j])), ABPassCharIntLength);
  end;
  result := Result + ABFillStr(IntToStr(temInt), ABPassCharIntLength);
end;

function ABUnDoPassword(aPassWord: string; aKey: string): string;
var
  temInt, i, j: longint;
begin
  aPassWord:=Trim(aPassWord);
  Result := aPassWord;
  if aPassWord = emptystr then
    Exit;

  Result := emptystr;
  temInt := StrToIntDef(Copy(aPassWord, Length(aPassWord) - ABPassCharIntLength +1, ABPassCharIntLength),0);
  aPassWord := Copy(aPassWord, 1, Length(aPassWord) - ABPassCharIntLength);
  for i := 1 to ABTrunc(length(aPassWord) / ABPassCharIntLength) do
  begin
    if i > Length(aKey) then
      j := Length(aKey)
    else
      j := i;

    Result := Result + Chr(StrToIntDef(Copy(aPassWord, (i - 1) * ABPassCharIntLength
      + 1, ABPassCharIntLength),0) - temInt - i - Ord(aKey[j]));
  end;
end;

function ABCheckPassword(aWord,aPassWord: string; aKey: string): boolean;
begin
  result:=(aWord=ABUnDoPassword(aPassWord,aKey));
end;

function ABCheckLicense(aLicenseFileName,aRegName:string;aCurDatetime:TDatetime;var aSubsequentClientCount:longint;aShowMsg:boolean):Boolean;
var
  tempRegKey,tempBegDatetime,tempEndDatetime,tempEditRegNameType,
  tempStr1,tempClientBegDatetime,tempClientEndDatetime,tempFuncRight:string;
  tempClientInfo,tempCustomFunc:Tstrings;

  i,tempCurClientIndex:longint;
  tempShowWantNotUse:boolean;
  tempStrings:Tstrings;
begin
  result:=false;
  if ABPubUser.IsNoCheckHost then
  begin
    result:=True;
  end
  else if ABCheckFileExists(aLicenseFileName)  then
  begin
    tempStrings:=TStringList.Create;
    tempCustomFunc     := TStringList.Create;
    tempClientInfo := TStringList.Create;
    try
      tempStrings.LoadFromFile(aLicenseFileName);
      tempStrings.Text:=ABUnDOPassWord(tempStrings.Text);

      tempRegKey         :=ABReadIniInStrings('MainSetup','RegKey',tempStrings);
      tempFuncRight      :=ABReadIniInStrings('MainSetup','FuncRight',tempStrings);
      tempEditRegNameType:=ABReadIniInStrings('MainSetup','EditRegNameType',tempStrings);
      if (AnsiCompareText(tempEditRegNameType,'CanEditRegName')=0) or
         (AnsiCompareText(tempEditRegNameType,'NotCanEditRegName')=0) then
      begin
        //检测注册码
        if (AnsiCompareText(tempEditRegNameType,'NotCanEditRegName')=0) and
           (ABDoRegisterKey(aRegName)<>tempRegKey)  then
        begin
          if aShowMsg then
            ABShow('注册名称[%s]与注册码[%s]不一致辞,请联系软件供应商.',[aRegName,tempRegKey]);
          exit;
        end
        else
        begin
          tempBegDatetime    :=ABReadIniInStrings('MainSetup','BegDatetime',tempStrings);
          tempEndDatetime    :=ABReadIniInStrings('MainSetup','EndDatetime',tempStrings);
          aSubsequentClientCount         :=strtointdef(ABReadIniInStrings('MainSetup','SubsequentClientCount',tempStrings),0);
          ABReadIniInStrings('ClientInfo',tempClientInfo,tempStrings);
          ABReadIniInStrings('CustomFunc',tempCustomFunc,tempStrings);
          tempShowWantNotUse:=false;
          try
            //检测客户端信息
            if (tempClientInfo.Count>0) and (tempClientInfo.text<>emptystr) then
            begin
              tempCurClientIndex:=-1;
              if tempCurClientIndex<0 then
                tempCurClientIndex:=tempClientInfo.IndexOfName(ABPubUser.HostIP);
              if tempCurClientIndex<0 then
                tempCurClientIndex:=tempClientInfo.IndexOfName(ABPubUser.HostName);
              if tempCurClientIndex<0 then
                tempCurClientIndex:=tempClientInfo.IndexOfName(ABPubUser.MacAddress);
              if tempCurClientIndex<0 then
                tempCurClientIndex:=tempClientInfo.IndexOfName(ABGetCpuID(1));
              if tempCurClientIndex<0 then
                tempCurClientIndex:=tempClientInfo.IndexOfName(ABGetDiskSerialNumber);

              if tempCurClientIndex<0 then
                tempCurClientIndex:=tempClientInfo.IndexOf(ABPubUser.HostIP);
              if tempCurClientIndex<0 then
                tempCurClientIndex:=tempClientInfo.IndexOf(ABPubUser.HostName);
              if tempCurClientIndex<0 then
                tempCurClientIndex:=tempClientInfo.IndexOf(ABPubUser.MacAddress);
              if tempCurClientIndex<0 then
                tempCurClientIndex:=tempClientInfo.IndexOf(ABGetCpuID(1));
              if tempCurClientIndex<0 then
                tempCurClientIndex:=tempClientInfo.IndexOf(ABGetDiskSerialNumber);

              if tempCurClientIndex<0 then
              begin
                if aShowMsg then
                  ABShow('此客户端未被授权访问,请联系软件供应商.');
                exit;
              end
              else
              begin
                tempStr1:=tempClientInfo.ValueFromIndex[tempCurClientIndex];
                tempClientBegDatetime:=ABGetLeftRightStr(tempStr1);
                tempClientEndDatetime:=ABGetLeftRightStr(tempStr1,axdRight);
                //检测开始使用时间
                if (tempClientBegDatetime<>'0') and (tempClientBegDatetime<>'') then
                begin
                  ABPubUser.LicenseBeginDateTime:=ABStrToDateTime(tempClientBegDatetime);
                  if aCurDatetime<ABPubUser.LicenseBeginDateTime then
                  begin
                    if aShowMsg then
                      ABShow('客户端未到开通时间[%s],不能使用,请联系软件供应商.',[abdatetimetostr(ABPubUser.LicenseBeginDateTime)]);
                    exit;
                  end;
                end;

                if (tempClientEndDatetime<>'0') and (tempClientEndDatetime<>'') then
                begin
                  //检测结束使用时间
                  ABPubUser.LicenseEndDateTime:=ABStrToDateTime(tempClientEndDatetime);
                  i:=ABTrunc(ABPubUser.LicenseEndDateTime-aCurDatetime);
                  if (i>0) and (i<=31) then
                  begin
                    tempShowWantNotUse:=true;
                    if aShowMsg then
                      ABShow('软件将在[%s]后过期,请及时联系软件供应商.',[abdatetimetostr(ABPubUser.LicenseEndDateTime)]);
                  end
                  else if i<=0 then
                  begin
                    if aShowMsg then
                      ABShow('客户端失效时间[%s]已到,不能使用,请联系软件供应商.',[abdatetimetostr(ABPubUser.LicenseEndDateTime)]);
                    exit;
                  end;
                end;
              end;
            end;

            //检测开始使用时间
            ABPubUser.LicenseBeginDateTime:=ABStrToDateTime(tempBegDatetime);
            if aCurDatetime<ABPubUser.LicenseBeginDateTime then
            begin
              if aShowMsg then
                ABShow('系统未到开通时间[%s],请联系软件供应商.',[ABDateTimeToStr(ABPubUser.LicenseBeginDateTime,23)]);
              exit;
            end;

            //检测结束使用时间
            ABPubUser.LicenseEndDateTime:=ABStrToDateTime(tempEndDatetime);
            i:=ABTrunc(ABPubUser.LicenseEndDateTime-aCurDatetime);
            if (i>0) and (i<=31) then
            begin
              if not tempShowWantNotUse then
                if aShowMsg then
                  ABShow('软件将在[%s]后过期,请及时联系软件供应商.',[abdatetimetostr(ABPubUser.LicenseEndDateTime)]);
            end
            else if i<=0 then
            begin
              if aShowMsg then
                ABShow('系统失效时间[%s]已到,不能使用,请联系软件供应商.',[abdatetimetostr(ABPubUser.LicenseEndDateTime)]);
              exit;
            end;

            ABPubUser.UseFuncRight:=abpos('1',tempFuncRight)>0;
            ABPubUser.FuncRight:=tempFuncRight;
            ABPubUser.CustomFunc:=tempCustomFunc.Text;
            result:=true;
          except
            if aShowMsg then
              ABShow('授权文件[%s]已损坏,请联系软件供应商.',['License.key']);
            exit;
          end;
        end;
      end
      else
      begin
        if aShowMsg then
          ABShow('授权文件[%s]已损坏,请联系软件供应商.',['License.key']);
        exit;
      end;
    finally
      tempCustomFunc.Free;
      tempClientInfo.Free;
    end;
  end
  else
  begin
    if aShowMsg then
      ABShow('未检测到授权文件[%s],请联系软件供应商.',['License.key']);
    exit;
  end;
end;


end.
