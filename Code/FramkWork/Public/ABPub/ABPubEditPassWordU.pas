{
修改操作员密码单元
}
unit ABPubEditPassWordU;

interface
{$I ..\ABInclude.ini}


uses
  ABPubFormU,
  ABPubMessageU,
  ABPubUserU,
  ABPubPassU,

  SysUtils,Variants,Classes,Controls,Forms,StdCtrls;

type
  TABPubEditPassWordForm = class(TABPubForm)
    Button1: TButton;
    Button2: TButton;
    leOldPassword: TEdit;
    leNewPassword: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    leAffirmNewPassword: TEdit;
    Label3: TLabel;
    procedure leOldPasswordKeyPress(Sender: TObject; var Key: Char);
    procedure leAffirmNewPasswordKeyPress(Sender: TObject; var Key: Char);
    procedure leNewPasswordKeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//修改密码并返回新密码
//aOldPassWord=旧密码
//aNewPassWord=新密码
function ABEditPassWord(aOldPassWord:string;var aNewPassWord:string): boolean;

implementation                                                          

{$R *.dfm}

function ABEditPassWord(aOldPassWord:string;var aNewPassWord:string): boolean;
var
  tempEditPassWordForm: TABPubEditPassWordForm;
  i:longint;
begin
  result:=false;
  aNewPassWord:=EmptyStr;
  i:=0;
  tempEditPassWordForm := TABPubEditPassWordForm.Create(nil);
  try
    while tempEditPassWordForm.ShowModal=mrok do
    begin
      if tempEditPassWordForm.leOldPassword.Text=ABUnDoPassword(aOldPassWord) then
      begin
        aNewPassWord:=tempEditPassWordForm.leNewPassword.Text;
        result:=true;
        ABShow('密码修改成功.');
        break;
      end
      else
        ABShow('旧密码不正确,不能修改,请检查.');

      i:=i+1;
      if i>5 then
        break;
    end;
  finally
    tempEditPassWordForm.Free;
  end;
end;

procedure TABPubEditPassWordForm.Button1Click(Sender: TObject);
begin
  if (ABPubUser.IsAdmin) and  (leNewPassword.Text=emptystr) then
  begin
    ABShow('admin管理员密码不能为空,请检查.');
  end
  else if leAffirmNewPassword.Text=leNewPassword.Text then
  begin
    ModalResult:=mrOk;
  end
  else
  begin
    ABShow('新密码验证与新密码不相同,请检查.');
  end;
end;

procedure TABPubEditPassWordForm.Button2Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TABPubEditPassWordForm.FormShow(Sender: TObject);
begin
  if leOldPassword.CanFocus then
    leOldPassword.SetFocus;
end;

procedure TABPubEditPassWordForm.leAffirmNewPasswordKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (key= #13)  then
    Button1Click(nil);
end;

procedure TABPubEditPassWordForm.leNewPasswordKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (key= #13) then
  begin
    if leAffirmNewPassword.CanFocus then
      leAffirmNewPassword.SetFocus;
  end;
end;

procedure TABPubEditPassWordForm.leOldPasswordKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (key= #13) then
    if leNewPassword.CanFocus then
      leNewPassword.SetFocus;
end;


end.
