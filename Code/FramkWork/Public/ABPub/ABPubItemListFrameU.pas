{
单列List列表控件单元
//用Frame+类的方式来做控件是为了采用Frame中子控件的事件与属性设置方便与类的更新方便
}
unit ABPubItemListFrameU;
                             
interface
{$I ..\ABInclude.ini}

uses
  ABPubPanelU,
  abpubfuncU,
  ABPubFrameU,

  SysUtils,
  Windows,Classes,Controls,Forms,ExtCtrls,StdCtrls,clipbrd,Buttons;

type
  TABListBoxDownEvent = procedure(Sender: TObject) of object;
  TABListBoxUpEvent   = procedure(Sender: TObject) of object;

  TABItemListFrame = class(TABPubFrame)
    ListBox1: TListBox;
    Panel2: TPanel;
    ToolButton4: TSpeedButton;
    ToolButton3: TSpeedButton;
    ToolButton2: TSpeedButton;
    ToolButton1: TSpeedButton;
    procedure ListBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
  private
    procedure Down(aStep: Integer=1);
    procedure Up(aStep: Integer=1);
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    { Public declarations }
  end;

  TABItemList = class(TABCustomPanel)
  private
    FFrame: TABItemListFrame;
    FOnItemDown: TABListBoxDownEvent;
    FOnItemUp: TABListBoxDownEvent;
    procedure SetMoveVisible(const Value: Boolean);
    function GetMoveVisible: Boolean;
  protected
    procedure Loaded; override;
    procedure Paint;override;
    procedure Resize; override;

    procedure DoDown(Sender: TObject);dynamic;
    procedure DoUp(Sender: TObject); dynamic;
  public
	 constructor Create(AOwner: TComponent); override;
	 destructor Destroy; override;
  Published
    property Frame: TABItemListFrame     read FFrame     write FFrame ;

    property OnItemDown: TABListBoxDownEvent read FOnItemDown write FOnItemDown;
    property OnItemUp  : TABListBoxDownEvent read FOnItemUp   write FOnItemUp  ;

    property MoveVisible :Boolean read GetMoveVisible write SetMoveVisible;
  end;



implementation
{$R *.dfm}                                           

procedure TABItemListFrame.ListBox1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Shift=[ssCtrl]) then
  begin
    if (Key=VK_DOWN) then
    begin
      ToolButton2Click(nil);
    end
    else if (Key=VK_UP) then
    begin
      ToolButton1Click(nil);
    end
    else if (Key=67) then
    begin
      clipboard.AsText := ListBox1.Items.Text;  
    end;
    Key:=0;
  end;
end;

procedure TABItemListFrame.ToolButton1Click(Sender: TObject);
begin
  Up(1);
end;

procedure TABItemListFrame.ToolButton2Click(Sender: TObject);
begin
  Down(1);
end;

procedure TABItemListFrame.ToolButton3Click(Sender: TObject);
begin
  Up(10);
end;

procedure TABItemListFrame.ToolButton4Click(Sender: TObject);
begin
  Down(10);
end;

constructor TABItemListFrame.Create(AOwner: TComponent);
begin
  inherited;
end;

destructor TABItemListFrame.Destroy;
begin
  inherited;
end;

procedure TABItemListFrame.Down(aStep:LongInt);
var
  i:longint;
begin
  ListBox1.Items.BeginUpdate;
  try
    if ListBox1.MultiSelect then
    begin
      for I :=ListBox1.Count - 1 downto 0 do
      begin
        if ListBox1.Selected[i] then
        begin
          ABListBoxDown(ListBox1,i,aStep);
          if Owner is TABItemList then
            TABItemList(Owner).DoDown(ListBox1);
        end;
      end;
    end
    else
    begin
      ABListBoxDown(ListBox1,ListBox1.ItemIndex,aStep);
      if Owner is TABItemList then
        TABItemList(Owner).DoDown(ListBox1);
    end;

  finally
    ListBox1.Items.EndUpdate;
  end;
end;

procedure TABItemListFrame.Up(aStep:LongInt);
var
  i:longint;
begin
  ListBox1.Items.BeginUpdate;
  try
    if ListBox1.MultiSelect then
    begin
      for I :=0 to ListBox1.Count - 1  do
      begin
        if ListBox1.Selected[i] then
        begin
          ABListBoxUp(ListBox1,i,aStep);
          if Owner is TABItemList then
            TABItemList(Owner).DoUp(ListBox1);
        end;
      end;
    end
    else
    begin
      ABListBoxUp(ListBox1,ListBox1.ItemIndex,aStep);
      if Owner is TABItemList then
        TABItemList(Owner).DoUp(ListBox1);
    end;

  finally
    ListBox1.Items.EndUpdate;
  end;
end;


{ TABItemList }

constructor TABItemList.Create(AOwner: TComponent);
begin
  inherited;
  FFrame:= TABItemListFrame.Create(self);
  FFrame.Parent:=Self;
  FFrame.Show;

  FFrame.Panel2.Tag:=FFrame.Panel2.Height;
end;

destructor TABItemList.Destroy;
begin
  FFrame.Free;
  inherited;
end;

procedure TABItemList.DoDown(Sender: TObject);
begin
  if Assigned(FOnItemDown) then FOnItemDown(Self);
end;

procedure TABItemList.DoUp(Sender: TObject);
begin
  if Assigned(FOnItemUp) then FOnItemUp(Self);
end;

function TABItemList.GetMoveVisible: Boolean;
begin
  result:=FFrame.Panel2.Visible ;
end;

procedure TABItemList.Loaded;
begin
  inherited;
end;

procedure TABItemList.Resize;
begin
  inherited;

end;

procedure TABItemList.SetMoveVisible(const Value: Boolean);
begin
  FFrame.Panel2.Visible:=Value;
  if FFrame.Panel2.Visible then
    FFrame.Panel2.Height:=FFrame.Panel2.Tag
  else
    FFrame.Panel2.Height:=0;
end;

procedure TABItemList.Paint;
begin
  inherited;
  {
  if  FFrame.Width<>Width then
    FFrame.Width:=Width;
  if  FFrame.Height<>Height then
    FFrame.Height:=Height;
    }
end;


end.



