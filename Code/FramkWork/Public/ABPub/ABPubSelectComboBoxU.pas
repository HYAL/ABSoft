{
ComboBox选择窗体单元
}
unit ABPubSelectComboBoxU;

interface                              
{$I ..\ABInclude.ini}

uses
  ABPubFormU,

  ABPubDBU,

  SysUtils,Variants,Classes,Controls,Forms,StdCtrls,ExtCtrls,DB;

type
  TABSelectComboBoxForm = class(TABPubForm)
    ComboBox1: TComboBox;
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

//选择数据集中的字段值，只支持单字段
function ABSelectComboBox(
                           var aValue:string;
                           aDataSet:TDataSet;
                           aDownAndViewFields,
                           aSaveFields:string;
                           aCaption:string=''):boolean;overload;
//选择数据集中的字段名称
function ABSelectComboBox(
                           var aValue:string;
                           aDataSet:TDataSet;
                           aCaption:string=''):boolean;overload;

implementation
{$R *.dfm}

function ABSelectComboBox(var aValue:string;
                          aDataSet:TDataSet;
                          aDownAndViewFields,
                          aSaveFields:string;
                          aCaption:string):boolean;
var
  tempForm:TABSelectComboBoxForm;
  tempOldBeforeScroll:TDataSetNotifyEvent;
  tempOldAfterScroll:TDataSetNotifyEvent;
  tempStrings:TStrings;
  i: LongInt;
begin
  Result:=false;

  tempForm := TABSelectComboBoxForm.Create(nil);
  tempForm.ComboBox1.Clear;

  if aCaption<>EmptyStr then
    tempForm.Caption:= aCaption;

  ABBackAndStopDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
  tempStrings:=TStringList.Create;
  aDataset.DisableControls;
  try
    aDataSet.First;
    while not aDataSet.Eof do
    begin
      tempForm.ComboBox1.Items.Add(ABGetFieldValue(aDataSet,aDownAndViewFields,[]));

      tempStrings.Add(ABGetFieldValue(aDataSet,aSaveFields,[]));
      aDataSet.next;
    end;

    if aValue<>emptystr then
    begin
      i:=tempStrings.IndexOf(aValue);
      if i>=0 then
      begin
        tempForm.ComboBox1.ItemIndex:=i;
      end;
    end;

    if (tempForm.ShowModal=mrok) then
    begin
      if tempForm.ComboBox1.ItemIndex>=0 then
      begin
        aValue:=tempStrings[tempForm.ComboBox1.ItemIndex];
      end;
      Result:=True;
    end;
  finally
    tempStrings.Free;
    ABUnBackDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    aDataset.EnableControls;
    tempForm.Free;
  end;
end;

function ABSelectComboBox(var aValue:string;aDataSet:TDataSet;aCaption:string):boolean;
var
  tempForm:TABSelectComboBoxForm;
  I: Integer;
  tempText:string;
begin
  Result:=false;

  tempForm := TABSelectComboBoxForm.Create(nil);
  tempForm.ComboBox1.Clear;

  if aCaption<>EmptyStr then
    tempForm.Caption:= aCaption;
  try
    for I := 0 to aDataSet.FieldCount-1 do
    begin
      if tempForm.ComboBox1.Items.IndexOf(inttostr(i)+'='+aDataSet.Fields[i].DisplayLabel)<=0 then
        tempForm.ComboBox1.Items.Add(inttostr(i)+'='+aDataSet.Fields[i].DisplayLabel);
    end;

    if aValue<>emptystr then
    begin
      if Assigned(aDataSet.FindField(aValue)) then
      begin
        tempText:=inttostr(aDataSet.FindField(aValue).Index)+'='+aDataSet.FindField(aValue).DisplayLabel;
        i:= tempForm.ComboBox1.Items.IndexOf(tempText);
        if i>=0 then
        begin
          tempForm.ComboBox1.ItemIndex:=i;
        end;
      end;
    end;

    if (tempForm.ShowModal=mrok) then
    begin
      if (tempForm.ComboBox1.Text<>EmptyStr) and
         (tempForm.ComboBox1.ItemIndex>=0) and
         (strtoint(tempForm.ComboBox1.Items.Names[tempForm.ComboBox1.ItemIndex])>=0)   then
      begin
        aValue:=aDataSet.Fields[strtoint(tempForm.ComboBox1.Items.Names[tempForm.ComboBox1.ItemIndex])].FieldName;
        Result:=true;
      end;
    end;
  finally
    tempForm.Free;    
  end;
end;

procedure TABSelectComboBoxForm.Button1Click(Sender: TObject);
begin
  ModalResult:=mrNo;
end;

procedure TABSelectComboBoxForm.Button2Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;


end.
