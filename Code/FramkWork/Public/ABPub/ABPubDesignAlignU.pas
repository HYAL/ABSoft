{
提供对运行期设计控件进行位置与大小调整
}
unit ABPubDesignAlignU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFuncU,
  ABPubMessageU,

  ABPubDesignSelectControlU,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Math,
  Dialogs, ComCtrls, ExtCtrls,ActiveX,Types,TypInfo, ImgList, ToolWin;


type
  TAlignSizeStyle = (
    asAlignLeft, asAlignRight,asAlignTop, asAlignBottom,
    asAlignHCenter,asAlignVCenter,
    asSpaceEquH, asSpaceEquHX, asSpaceIncH, asSpaceDecH, asSpaceRemoveH,
    asSpaceEquV, asSpaceEquVY, asSpaceIncV, asSpaceDecV, asSpaceRemoveV,
    asIncWidth, asDecWidth , asMakeMaxWidth,asMakeMinWidth,asMakeSameWidth,
    asIncHeight, asDecHeight,asMakeMaxHeight,asMakeMinHeight, asMakeSameHeight,
    asMakeSameSize,
    asParentHCenter, asParentVCenter,asSelect);


type
  TABDesignAlignForm = class(TForm)
    ImageList1: TImageList;
    Panel1: TPanel;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    ToolButton18: TToolButton;
    ToolButton19: TToolButton;
    ToolButton20: TToolButton;
    ToolButton21: TToolButton;
    ToolButton22: TToolButton;
    ToolButton23: TToolButton;
    ToolButton24: TToolButton;
    ToolButton25: TToolButton;
    ToolButton26: TToolButton;
    ToolButton27: TToolButton;
    ToolButton28: TToolButton;
    ToolButton29: TToolButton;
    ToolButton30: TToolButton;
    ToolButton31: TToolButton;
    ToolButton32: TToolButton;
    ToolButton33: TToolButton;
    ToolButton34: TToolButton;
    ToolButton35: TToolButton;
    ToolButton36: TToolButton;
    Timer1: TTimer;
    ImageList2: TImageList;
    ToolButton37: TToolButton;
    ToolButton38: TToolButton;
    ToolButton39: TToolButton;
    ToolButton40: TToolButton;
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure ToolButton9Click(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure ToolButton11Click(Sender: TObject);
    procedure ToolButton12Click(Sender: TObject);
    procedure ToolButton13Click(Sender: TObject);
    procedure ToolButton15Click(Sender: TObject);
    procedure ToolButton16Click(Sender: TObject);
    procedure ToolButton17Click(Sender: TObject);
    procedure ToolButton18Click(Sender: TObject);
    procedure ToolButton19Click(Sender: TObject);
    procedure ToolButton21Click(Sender: TObject);
    procedure ToolButton22Click(Sender: TObject);
    procedure ToolButton23Click(Sender: TObject);
    procedure ToolButton24Click(Sender: TObject);
    procedure ToolButton32Click(Sender: TObject);
    procedure ToolButton26Click(Sender: TObject);
    procedure ToolButton27Click(Sender: TObject);
    procedure ToolButton28Click(Sender: TObject);
    procedure ToolButton29Click(Sender: TObject);
    procedure ToolButton31Click(Sender: TObject);
    procedure ToolButton33Click(Sender: TObject);
    procedure ToolButton35Click(Sender: TObject);
    procedure ToolButton36Click(Sender: TObject);
    procedure ToolButton37Click(Sender: TObject);
    procedure ToolButton40Click(Sender: TObject);
  private
    FDockingForm:TCustomForm;
    FControls: TList;

    function SelectsIsSameParent: boolean;
    procedure DoItem(AlignSizeStyle: TAlignSizeStyle);
    procedure ControlListSortByPos(List: TList; IsVert: Boolean;
      Desc: Boolean = False);
    procedure ControlListSortByProp(List: TList; ProName: string;
      Desc: Boolean = False);
    { Private declarations }
  public
    procedure refreshPosition;
    procedure refreshState;
    { Public declarations }
  end;

//显示设计Align
procedure ABShowDesignAlign(aForm:TCustomForm;aControls: TList);
//关闭设计Align
procedure ABCloseDesignAlign;
//刷新设计Align窗体状态
procedure ABRefreshDesignAlignState;
//刷新设计Align窗体位置
procedure ABRefreshDesignAlignPosition;


implementation

{$R *.dfm}
uses ABPubDesignU;

var
  //设计位置的窗体
  FDesignAlignForm: TABDesignAlignForm;

const
  csSpaceIncStep = 1;

function GetDesignAlignForm:TABDesignAlignForm;
begin
  if not Assigned(FDesignAlignForm) then
    FDesignAlignForm := TABDesignAlignForm.Create(nil);

  Result:= FDesignAlignForm;
end;

// 返回控件在屏幕上的坐标区域
function GetControlScreenRect(AControl: TControl): TRect;
var
  AParent: TWinControl;
begin
  Assert(Assigned(AControl));
  AParent := AControl.Parent;
  Assert(Assigned(AParent));
  Result.TopLeft := AParent.ClientToScreen(Point(AControl.Left, AControl.Top));
  Result.BottomRight := AParent.ClientToScreen(Point(AControl.Left + AControl.Width, AControl.Top + AControl.Height));
end;

// 设置控件在屏幕上的坐标区域
procedure SetControlScreenRect(AControl: TControl; ARect: TRect);
var
  AParent: TWinControl;
  P1, P2: TPoint;
begin
  Assert(Assigned(AControl));
  AParent := AControl.Parent;
  Assert(Assigned(AParent));
  P1 := AParent.ScreenToClient(ARect.TopLeft);
  P2 := AParent.ScreenToClient(ARect.BottomRight);
  AControl.SetBounds(P1.x, P1.y, P2.x - P1.x, P2.y - P1.y);
end;

// 获得多显示器情况下，整个桌面相对于主显示器原点的坐标
function GetMultiMonitorDesktopRect: TRect;
var
  I: Integer;
begin
  Result.Left := 0;
  Result.Top := 0;
  Result.Bottom := Screen.DesktopHeight;
  Result.Right := Screen.DesktopWidth;

  for I := 0 to Screen.MonitorCount - 1 do
  begin
    if Screen.Monitors[I].Left < Result.Left then
      Result.Left := Screen.Monitors[I].Left;
    if Screen.Monitors[I].Top < Result.Top then
      Result.Top := Screen.Monitors[I].Top;
    if Screen.Monitors[I].Height + Screen.Monitors[I].Top > Result.Bottom then
      Result.Bottom := Screen.Monitors[I].Height + Screen.Monitors[I].Top;
    if Screen.Monitors[I].Width + Screen.Monitors[I].Left > Result.Right then
      Result.Right := Screen.Monitors[I].Width + Screen.Monitors[I].Left;
  end;
end;

var
  _ProName: string;
  _Desc: Boolean;
  _IsVert: Boolean;

// 比较两个整数，V1 > V2 返回 1，V1 < V2 返回 -1，V1 = V2 返回 0
// 如果 Desc 为 True，返回结果反向
function CompareInt(V1, V2: Integer; Desc: Boolean = False): Integer;
begin
  if V1 > V2 then
    Result := 1
  else if V1 < V2 then
    Result := -1
  else // V1 = V2
    Result := 0;
  if Desc then
    Result := -Result;
end;

function DoSortByProp(Item1, Item2: Pointer): Integer;
var
  V1, V2: Integer;
begin
  V1 := GetOrdProp(TComponent(Item1), _ProName);
  V2 := GetOrdProp(TComponent(Item2), _ProName);
  Result := CompareInt(V1, V2, _Desc);
end;

function DoSortByPos(Item1, Item2: Pointer): Integer;
var
  R1, R2: TRect;
begin
  R1 := GetControlScreenRect(TControl(Item1));
  R2 := GetControlScreenRect(TControl(Item2));
  if _IsVert then
    Result := CompareInt(R1.Top, R2.Top, _Desc)
  else
    Result := CompareInt(R1.Left, R2.Left, _Desc)
end;


{ TABPubDesignAlignForm }

procedure TABDesignAlignForm.ControlListSortByPos(List: TList; IsVert: Boolean;
  Desc: Boolean);
begin
  _IsVert := IsVert;
  _Desc := Desc;
  List.Sort(DoSortByPos);
end;

procedure TABDesignAlignForm.ControlListSortByProp(List: TList; ProName: string;
  Desc: Boolean);
begin
  _ProName := ProName;
  _Desc := Desc;
  List.Sort(DoSortByProp);
end;

procedure TABDesignAlignForm.ToolButton9Click(Sender: TObject);
begin
  DoItem(asSpaceEquH);
end;

procedure TABDesignAlignForm.ToolButton10Click(Sender: TObject);
begin
  DoItem(asSpaceEquHX);
end;

procedure TABDesignAlignForm.ToolButton11Click(Sender: TObject);
begin
  DoItem(asSpaceIncH);
end;

procedure TABDesignAlignForm.ToolButton12Click(Sender: TObject);
begin
  DoItem(asSpaceDecH);
end;

procedure TABDesignAlignForm.ToolButton13Click(Sender: TObject);
begin
  DoItem(asSpaceRemoveH);
end;

procedure TABDesignAlignForm.ToolButton15Click(Sender: TObject);
begin
  DoItem(asSpaceEquV);
end;

procedure TABDesignAlignForm.ToolButton16Click(Sender: TObject);
begin
  DoItem(asSpaceEquVY);
end;

procedure TABDesignAlignForm.ToolButton17Click(Sender: TObject);
begin
  DoItem(asSpaceIncV);
end;

procedure TABDesignAlignForm.ToolButton18Click(Sender: TObject);
begin
  DoItem(asSpaceDecV);
end;

procedure TABDesignAlignForm.ToolButton19Click(Sender: TObject);
begin
  DoItem(asSpaceRemoveV);
end;

procedure TABDesignAlignForm.ToolButton1Click(Sender: TObject);
begin
  DoItem(asAlignLeft);
end;

procedure TABDesignAlignForm.ToolButton21Click(Sender: TObject);
begin
  DoItem(asIncWidth);
end;

procedure TABDesignAlignForm.ToolButton22Click(Sender: TObject);
begin
  DoItem(asDecWidth);
end;

procedure TABDesignAlignForm.ToolButton23Click(Sender: TObject);
begin
  DoItem(asMakeMaxWidth);
end;

procedure TABDesignAlignForm.ToolButton24Click(Sender: TObject);
begin
  DoItem(asMakeMinWidth);
end;

procedure TABDesignAlignForm.ToolButton26Click(Sender: TObject);
begin
  DoItem(asIncHeight);
end;

procedure TABDesignAlignForm.ToolButton27Click(Sender: TObject);
begin
  DoItem(asDecHeight);
end;

procedure TABDesignAlignForm.ToolButton28Click(Sender: TObject);
begin
  DoItem(asMakeMaxHeight);
end;

procedure TABDesignAlignForm.ToolButton29Click(Sender: TObject);
begin
  DoItem(asMakeMinHeight);
end;

procedure TABDesignAlignForm.ToolButton2Click(Sender: TObject);
begin
  DoItem(asAlignRight);
end;

procedure TABDesignAlignForm.ToolButton31Click(Sender: TObject);
begin
  DoItem(asMakeSameHeight);
end;

procedure TABDesignAlignForm.ToolButton32Click(Sender: TObject);
begin
  DoItem(asMakeSameWidth);
end;

procedure TABDesignAlignForm.ToolButton33Click(Sender: TObject);
begin
  DoItem(asMakeSameSize);
end;

procedure TABDesignAlignForm.ToolButton35Click(Sender: TObject);
begin
  DoItem(asParentHCenter);
end;

procedure TABDesignAlignForm.ToolButton36Click(Sender: TObject);
begin
  DoItem(asParentVCenter);
end;

procedure TABDesignAlignForm.ToolButton37Click(Sender: TObject);
begin
  DoItem(asSelect);
end;

procedure TABDesignAlignForm.ToolButton3Click(Sender: TObject);
begin
  DoItem(asAlignTop);
end;

procedure TABDesignAlignForm.ToolButton40Click(Sender: TObject);
begin
  Hide;
end;

procedure TABDesignAlignForm.ToolButton5Click(Sender: TObject);
begin
  DoItem(asAlignBottom);
end;

procedure TABDesignAlignForm.ToolButton6Click(Sender: TObject);
begin
  DoItem(asAlignHCenter);
end;

procedure TABDesignAlignForm.ToolButton7Click(Sender: TObject);
begin
  DoItem(asAlignVCenter);
end;

procedure TABDesignAlignForm.DoItem(AlignSizeStyle: TAlignSizeStyle);

var
  I: Integer;
  AList,
  ControlListB: TList;
  S: string;
  Count, Value: Integer;
  Curr: Double;
  R1, R2, R3: TRect;
  Space: Integer;

  tempSelectControls:TStrings;
begin
  case AlignSizeStyle of
    asAlignLeft, asAlignRight, asAlignTop, asAlignBottom,
    asAlignHCenter, asAlignVCenter:
      begin
        if not SelectsIsSameParent then
        begin
          abshow('选择的控件来源于多个容器，不能操作，请检查。');
          exit;
        end;
        R1 := GetControlScreenRect(TControl(FControls[0]));
        for I := 1 to FControls.Count - 1 do
        begin
          R2 := GetControlScreenRect(TControl(FControls[I]));
          if AlignSizeStyle = asAlignLeft then
            OffsetRect(R2, R1.Left - R2.Left, 0)
          else if AlignSizeStyle = asAlignRight then
            OffsetRect(R2, R1.Right - R2.Right, 0)
          else if AlignSizeStyle = asAlignTop then
            OffsetRect(R2, 0, R1.Top - R2.Top)
          else if AlignSizeStyle = asAlignBottom then
            OffsetRect(R2, 0, R1.Bottom - R2.Bottom)
          else if AlignSizeStyle = asAlignVCenter then
            OffsetRect(R2, 0, (R1.Top + R1.Bottom - R2.Top - R2.Bottom) div 2)
          else // AlignSizeStyle = asAlignHCenter
            OffsetRect(R2, (R1.Left + R1.Right - R2.Left - R2.Right) div 2, 0);
          SetControlScreenRect(TControl(FControls[I]), R2);
        end;
      end;
    asSpaceEquH, asSpaceEquV:
      begin
        if not SelectsIsSameParent then
        begin
          abshow('选择的控件来源于多个容器，不能操作，请检查。');
          exit;
        end;
        if FControls.Count < 3 then Exit;
        ControlListSortByPos(FControls, AlignSizeStyle = asSpaceEquV);

        R1 := GetControlScreenRect(TControl(FControls[0]));
        R2 := GetControlScreenRect(TControl(FControls[FControls.Count - 1]));
        Count := 0;
        for I := 1 to FControls.Count - 2 do
        begin
          R3 := GetControlScreenRect(TControl(FControls[I]));
          if AlignSizeStyle = asSpaceEquH then
            Inc(Count, R3.Right - R3.Left)
          else
            Inc(Count, R3.Bottom - R3.Top);
        end;

        if AlignSizeStyle = asSpaceEquH then
          Curr := R1.Right
        else
          Curr := R1.Bottom;
        for I := 1 to FControls.Count - 2 do
        begin
          if AlignSizeStyle = asSpaceEquH then
            Curr := Curr + (R2.Left - R1.Right - Count) / (FControls.Count - 1)
          else
            Curr := Curr + (R2.Top - R1.Bottom - Count) / (FControls.Count - 1);
          R3 := GetControlScreenRect(TControl(FControls[I]));
          if AlignSizeStyle = asSpaceEquH then
            OffsetRect(R3, Round(Curr) - R3.Left, 0)
          else
            OffsetRect(R3, 0, Round(Curr) - R3.Top);
          SetControlScreenRect(TControl(FControls[I]), R3);
          if AlignSizeStyle = asSpaceEquH then
            Curr := Curr + R3.Right - R3.Left
          else
            Curr := Curr + R3.Bottom - R3.Top;
        end;
      end;
    asSpaceEquHX, asSpaceEquVY:
      begin
        if not SelectsIsSameParent then
        begin
          abshow('选择的控件来源于多个容器，不能操作，请检查。');
          exit;
        end;
        if FControls.Count < 2 then Exit;
        ControlListSortByPos(FControls, AlignSizeStyle = asSpaceEquVY);

        S := '4';
        S:=InputBox('请输入','间隔',S);
        if S<>EmptyStr then
        begin
          if ABIsFloat(S) then
            Space := StrToInt(S)
          else
          begin
            abshow('输入的不是数据类型值，请检查。');
            Exit;
          end;
        end
        else
        begin
          exit;
        end;

        // 获得了手工间距，开始排列
        R1 := GetControlScreenRect(TControl(FControls[0]));
        if AlignSizeStyle = asSpaceEquHX then
          Curr := R1.Right
        else
          Curr := R1.Bottom;

        for I := 1 to FControls.Count - 1 do
        begin
          Curr := Curr + Space;

          R3 := GetControlScreenRect(TControl(FControls[I]));
          if AlignSizeStyle = asSpaceEquHX then
            OffsetRect(R3, Round(Curr) - R3.Left, 0)
          else
            OffsetRect(R3, 0, Round(Curr) - R3.Top);
          SetControlScreenRect(TControl(FControls[I]), R3);

          if AlignSizeStyle = asSpaceEquHX then
            Curr := Curr + R3.Right - R3.Left
          else
            Curr := Curr + R3.Bottom - R3.Top;
        end;
      end;
    asSpaceIncH, asSpaceDecH, asSpaceRemoveH,
    asSpaceIncV, asSpaceDecV, asSpaceRemoveV:
      begin
        if not SelectsIsSameParent then
        begin
          abshow('选择的控件来源于多个容器，不能操作，请检查。');
          exit;
        end;
        ControlListSortByPos(FControls, AlignSizeStyle in
          [asSpaceIncV, asSpaceDecV, asSpaceRemoveV]);
        R1 := GetControlScreenRect(TControl(FControls[0]));
        for I := 1 to FControls.Count - 1 do
        begin
          R2 := GetControlScreenRect(TControl(FControls[I]));
          if AlignSizeStyle = asSpaceIncH then
            OffsetRect(R2, csSpaceIncStep * I, 0)
          else if AlignSizeStyle = asSpaceIncV then
            OffsetRect(R2, 0, csSpaceIncStep * I)
          else if AlignSizeStyle = asSpaceDecH then
            OffsetRect(R2, -csSpaceIncStep * I, 0)
          else if AlignSizeStyle = asSpaceDecV then
            OffsetRect(R2, 0, -csSpaceIncStep * I)
          else if AlignSizeStyle = asSpaceRemoveH then
            OffsetRect(R2, R1.Right - R2.Left, 0)
          else // AlignSizeStyle = asSpaceRemoveV then
            OffsetRect(R2, 0, R1.Bottom - R2.Top);
          SetControlScreenRect(TControl(FControls[I]), R2);
          R1 := R2;
        end;
      end;
    asIncWidth, asDecWidth, asIncHeight, asDecHeight:
     begin
       for i := 0 to FControls.Count - 1 do
       begin
         if AlignSizeStyle = asIncWidth then
           TControl(FControls[I]).Width := TControl(FControls[I]).Width + 1
         else if AlignSizeStyle = asDecWidth then
         begin
           TControl(FControls[I]).Width := TControl(FControls[I]).Width - 1;
         end
         else if AlignSizeStyle = asIncHeight then
           TControl(FControls[I]).Height := TControl(FControls[I]).Height + 1
         else if AlignSizeStyle = asDecHeight then
         begin
           TControl(FControls[I]).Height := TControl(FControls[I]).Height - 1;
         end;
       end;
     end;
    asMakeMinWidth, asMakeMaxWidth, asMakeSameWidth,
    asMakeMinHeight, asMakeMaxHeight, asMakeSameHeight,
    asMakeSameSize:
      begin
        if AlignSizeStyle in [asMakeMinWidth, asMakeMaxWidth] then
          ControlListSortByProp(FControls, 'Width', AlignSizeStyle = asMakeMaxWidth);
        if AlignSizeStyle in [asMakeMinHeight, asMakeMaxHeight] then
          ControlListSortByProp(FControls, 'Height', AlignSizeStyle = asMakeMaxHeight);
        for i := 1 to FControls.Count - 1 do
        begin
          if AlignSizeStyle in [asMakeMinWidth, asMakeMaxWidth,
            asMakeSameWidth, asMakeSameSize] then
            TControl(FControls[I]).Width := TControl(FControls[0]).Width;
          if AlignSizeStyle in [asMakeMinHeight, asMakeMaxHeight,
            asMakeSameHeight, asMakeSameSize] then
            TControl(FControls[I]).Height := TControl(FControls[0]).Height;
        end;
      end;
    asParentHCenter, asParentVCenter:
      begin
        ControlListB := TList.Create;
        AList := TList.Create;
        try
          ControlListB.Assign(FControls);
          while ControlListB.Count > 0 do
          begin
            // 取出 Parent 相同的一组控件
            AList.Clear;
            AList.Add(ControlListB.Extract(ControlListB[0]));
            for i := ControlListB.Count - 1 downto 0 do
              if TControl(ControlListB[I]).Parent = TControl(AList[0]).Parent then
                AList.Add(ControlListB.Extract(ControlListB[I]));

            if AlignSizeStyle = asParentHCenter then
            begin
              // 计算控件组的外接宽度
              R1.Left := MaxInt;
              R1.Right := -MaxInt;
              for i := 0 to AList.Count - 1 do
              begin
                R1.Left := Min(TControl(AList[i]).Left, R1.Left);
                R1.Right := Max(TControl(AList[i]).Left + TControl(AList[I]).Width, R1.Right);
              end;

              // 要移动的距离
              Value := (TControl(AList[0]).Parent.ClientWidth - R1.Left - R1.Right) div 2;
              for I := 0 to AList.Count - 1 do
                TControl(AList[I]).Left := TControl(AList[I]).Left + Value;
            end
            else
            begin
              // 计算控件组的外接高度
              R1.Top := MaxInt;
              R1.Bottom := -MaxInt;
              for I := 0 to AList.Count - 1 do
              begin
                R1.Top := Min(TControl(AList[I]).Top, R1.Top);
                R1.Bottom := Max(TControl(AList[I]).Top + TControl(AList[I]).Height, R1.Bottom);
              end;

              // 要移动的距离
              Value := (TControl(AList[0]).Parent.ClientHeight - R1.Top - R1.Bottom) div 2;
              for i := 0 to AList.Count - 1 do
                TControl(AList[I]).Top := TControl(AList[I]).Top + Value;
            end;
          end;
        finally
          AList.Free;
          ControlListB.Free;
        end;
      end;
      asSelect:
      begin
        tempSelectControls := TStringList.Create;
        try
          for I := 0 to FControls.Count - 1 do
          begin
            tempSelectControls.AddObject(TControl(FControls[I]).Name,FControls[I])
          end;

          if ABPubDesignSelectControl(FDockingForm,tempSelectControls) then
          begin
            ABPubDesignerHook.Clear;
            for I := 0 to tempSelectControls.Count - 1 do
              ABPubDesignerHook.Add(TControl(tempSelectControls.Objects[I]));
          end;
        finally
          tempSelectControls.Free;
        end;
      end;
  end;

  for I := 0 to FControls.Count - 1 do
    TControl(FControls[I]).Invalidate;

  ABPubDesignerHook.ShowGrabHandle(True);
end;


procedure TABDesignAlignForm.refreshPosition;
begin
  if (Assigned(FDockingForm)) and
     (GetDesignAlignForm.Showing) then
  begin
    left:=FDockingForm.ClientOrigin.x;
    top:= FDockingForm.ClientOrigin.y-height;
    if Assigned(Application.MainForm) then
    begin
      top:= top-(Application.MainForm.Height-Application.MainForm.ClientHeight);
    end;
  end;
end;

procedure TABDesignAlignForm.refreshState;
var
  tempMore1,tempMore2,tempMore3:boolean;
begin
  if GetDesignAlignForm.Showing then
  begin
    tempMore1:=FControls.Count>=1;
    tempMore2:=FControls.Count>=2;
    tempMore3:=FControls.Count>=3;

    ToolButton1.Enabled:= tempMore2;
    ToolButton2.Enabled:= tempMore2;
    ToolButton3.Enabled:= tempMore2;
    ToolButton5.Enabled:= tempMore2;
    ToolButton6.Enabled:= tempMore2;
    ToolButton7.Enabled:= tempMore2;

    ToolButton9 .Enabled:= tempMore3;
    ToolButton10.Enabled:= tempMore2;
    ToolButton11.Enabled:= tempMore2;
    ToolButton12.Enabled:= tempMore2;
    ToolButton13.Enabled:= tempMore2;
    ToolButton15.Enabled:= tempMore3;
    ToolButton16.Enabled:= tempMore2;
    ToolButton17.Enabled:= tempMore2;
    ToolButton18.Enabled:= tempMore2;
    ToolButton19.Enabled:= tempMore2;

    ToolButton21.Enabled:= tempMore1;
    ToolButton22.Enabled:= tempMore1;
    ToolButton23.Enabled:= tempMore2;
    ToolButton24.Enabled:= tempMore2;
    ToolButton26.Enabled:= tempMore1;
    ToolButton27.Enabled:= tempMore1;
    ToolButton28.Enabled:= tempMore2;
    ToolButton29.Enabled:= tempMore2;

    ToolButton31.Enabled:= tempMore2;
    ToolButton32.Enabled:= tempMore2;
    ToolButton33.Enabled:= tempMore2;

    ToolButton35.Enabled:= tempMore1;
    ToolButton36.Enabled:= tempMore1;
  //  ToolButton38.Enabled:= tempMore1;
  end;
end;

function TABDesignAlignForm.SelectsIsSameParent:boolean;
var
  i:LongInt;
  tempParnet:TWinControl;
begin
  result:=true;
  tempParnet:=nil;

  for I := 0 to FControls.Count-1 do
  begin
    if (Assigned(tempParnet)) and
       (tempParnet<>TControl(FControls[i]).Parent) then
    begin
      result:=false;
      Break;
    end;
    tempParnet:=TControl(FControls[i]).Parent;
  end;
end;

procedure ABRefreshDesignAlignState;
begin
  if FDesignAlignForm.Showing then
  begin
    GetDesignAlignForm.refreshState;
  end;
end;

procedure ABRefreshDesignAlignPosition;
begin
  if GetDesignAlignForm.Showing then
  begin
    GetDesignAlignForm.refreshPosition;
  end;
end;

procedure ABShowDesignAlign(aForm:TCustomForm;aControls: TList);
begin
  GetDesignAlignForm.FDockingForm:=aForm;
  GetDesignAlignForm.FControls:=aControls;
  if not GetDesignAlignForm.Showing then
    GetDesignAlignForm.Show;

  ABRefreshDesignAlignState;
  ABRefreshDesignAlignPosition;
end;

procedure ABCloseDesignAlign;
begin
  if GetDesignAlignForm.Showing then
    GetDesignAlignForm.hide;

  GetDesignAlignForm.FDockingForm:=nil;
  GetDesignAlignForm.FControls:=nil;
end;

procedure ABFinalization;
begin
  if Assigned(FDesignAlignForm) then
    FDesignAlignForm.Free;
end;


Initialization

Finalization
  ABFinalization;



end.





