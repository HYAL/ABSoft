{
注册文件管理组件单元
}
unit ABPubRegisterFileU;

interface
{$I ..\ABInclude.ini}
uses
  ABPubPakandZipU,
  ABPubMessageU,
  abpubvarU,
  abpubfuncU,
  ABPubLocalParamsU,
  ABPubLogU,
  ABPubFormU,
  ABPubConstU,

  Windows,SysUtils,Classes,Variants,Controls,Forms,ActiveX,DB,System.Win.comobj,AxCtrls,
  ShellAPI;

type
  //文件注册类,提供文件的注册、下载、运行等操作
  PABPackageList = ^TABPackageList;
  TABPackageList = record
    Guid:string;
    FileName: string;
    Hand: HModule;
    Form: TForm;
    IsFree: Boolean;
  end;

  TABRegisterFile = class
  private
    FFlist: TStrings;
    FLastRunFileEndTime: TDateTime;
    FLastRunFileBeginTime: TDateTime;
    //从复合文件（如WORD,EXL)中读摘要信息中的修订版本码
    function ReadRemarkFromFile(const AFileName: string): string;
    procedure SetFlist(const Value: TStrings);
  protected
  public
    constructor Create;
    destructor Destroy; override;

    //最后运行文件的开始结束时间,两个之差就是文件的启动时间
    property LastRunFileBeginTime: TDateTime read FLastRunFileBeginTime write FLastRunFileBeginTime;
    property LastRunFileEndTime: TDateTime read FLastRunFileEndTime write FLastRunFileEndTime;

    //得到文件的版本号或复合文件修订号
    function GetVersion(aFileName: string;aReadVersionsFileOnEmptystr:boolean=true): string;

    //判断是否满足下载的条件
    function IsDown(aLocalFileName,aSubPath,aServerPkgVersion: string;aIsUpdateSameVersion: boolean = false): boolean;

    //下载新的程序包
    function Down(aLocalFileName,aSubPath: string;
                  aServerPkg: TBlobField; aServerPkgVersion: string): boolean; overload;
    function Down(aLocalFileName,aSubPath: string;
                  aStream: TMemoryStream; aServerPkgVersion: string): boolean; overload;

    //加载包到文件流中
    procedure Up( aLocalFile: string;var aMemoryStream:TMemoryStream;var aFormClassName: string);
    //判断是否满足加载的条件
    function IsUp(aLocalPathFileName,aServerPkgVersion: string;aIsUpdateSameVersion: boolean = false): boolean;
    //加载包且运行
    function LoadRun( aFileName: string;aCaption: string='';aSubPath: string='';aGuid: string='';

                      aTemplateName:string='';
                      aIsShowModal: boolean = false;
                      aIsMDIClient: boolean = false): boolean;
    //取模板的窗体类型名
    function GetTemplateFormClass(aFileName: string;aSubPath: string=''): string;
    //通过文件名得到窗体名
    function GetFormNameByFileName(aName: string): string;
    //通过文件名得到Guid
    function GetGuidByFileName(aName: string): string;
    //通过文件名得到结构
    function GetListItemByFileName(aName: string): PABPackageList;
    //通过文件名得到窗体对象
    function GetFormByFileName(aName: string): TForm;
    //通过文件名得到文件句柄
    function GetHandByFileName(aName: string): HMODULE;

    //通过窗体名得到文件名
    function GetFileNameByFormName(aName: string): string;
    //通过窗体名得到窗体对象
    function GetFormByFormName(aName: string): TForm;
    //通过窗体名得到文件句柄
    function GetHandByFormName(aName: string): HMODULE;

    //注册功能
    procedure RegisterForm(aHModule: HModule;aGuid,aFileName: string; aForm: TForm);
    //注册的文件列表
    property Flist: TStrings read FFlist write SetFlist;

    //在文件列表中设置传入窗体名的释放标记(如参数为空则设置所有列表中释放标记),一般在功能窗体释放前做
    procedure SetFreeFlag(aFormName: string);
    //将文件列表中有释放标记的释放,一般在功能窗体释放后做,aFreeComponent=true,表示同时释放控件
    function FreeHaveFreeFlag(aFreeComponent: boolean = true): string;
  end;

var
  //文件服务对象
  ABRegisterFile: TABRegisterFile;
  //功能窗体中注册的过程,用来得到功能窗体中的窗体类名称
  ABRegisterPro: procedure(var aFormClassName:string); stdcall;


implementation

var
  FTemplateNameStrings:TStrings;
  FTemplateHModuleStrings:TStrings;
  FTemplateFormClassStrings:TStrings;




  {
constructor TABPubForm.CreateExt1(AOwner: TComponent; aExtStrings: Tstrings);
begin
  Create(AOwner);
  FExtStrings.Assign(aExtStrings);
end;
  FExtStrings:= TStringList.Create;
procedure TABPubForm.SetExtStrings(const Value: Tstrings);
begin
  FExtStrings.Assign(Value);
end;
    property ExtStrings:Tstrings read FExtStrings write SetExtStrings ;
    constructor CreateExt1(AOwner: TComponent; aExtStrings: TStrings);overload;
    FExtStrings:Tstrings;
}
var
  ABRunBeforeCreateFormPro: procedure(var aCreateForm:Boolean); stdcall;

function TABRegisterFile.IsUp(aLocalPathFileName,aServerPkgVersion: string;aIsUpdateSameVersion: boolean): boolean;
var
  tempLocalVersion, tempMaxVersion: string;
begin
  result := false;
  tempLocalVersion := EmptyStr;
  tempMaxVersion := EmptyStr;

  if (aLocalPathFileName <> EmptyStr) then
  begin
    if (ABCheckFileExists(aLocalPathFileName)) then
    begin
      tempLocalVersion := GetVersion(aLocalPathFileName);
      tempMaxVersion := ABGetMaxVersion(tempLocalVersion, aServerPkgVersion);

      //如果相同版本则更新
      //如果本地版本比服务器新则更新
      if  ((tempLocalVersion = aServerPkgVersion) and (aIsUpdateSameVersion)) or
          ((ABPos('.',aServerPkgVersion)<=0) and (ABPos('.',tempLocalVersion)>0)) or
          ((AnsiCompareText(tempMaxVersion, tempLocalVersion) = 0) and (tempMaxVersion <> EmptyStr)) then
      begin
        result := true;
      end;
    end;
  end;
end;

function TABRegisterFile.IsDown(aLocalFileName,aSubPath: string;
  aServerPkgVersion: string;
  aIsUpdateSameVersion: boolean): boolean;
var
  tempLocalVersion, tempMaxVersion: string;
begin
  result := false;
  tempLocalVersion := EmptyStr;
  tempMaxVersion := EmptyStr;

  if (aLocalFileName <> EmptyStr) then
  begin
    if (ABCheckFileExists(ABAppPath+ABGetpath(aSubPath) + aLocalFileName)) then
    begin
      tempLocalVersion := GetVersion(ABAppPath +ABGetpath(aSubPath)+ aLocalFileName);
    end;

    tempMaxVersion := ABGetMaxVersion(tempLocalVersion, aServerPkgVersion);

    //如果相同版本也下载或
    //  服务器有新的版本则下载
    if (not ABCheckFileExists(ABAppPath+ABGetpath(aSubPath) + aLocalFileName)) or
       ((tempLocalVersion = aServerPkgVersion) and (aIsUpdateSameVersion)) or
       ((ABPos('.',aServerPkgVersion)>0) and (ABPos('.',tempLocalVersion)<=0)) or
       ((AnsiCompareText(tempMaxVersion, aServerPkgVersion) = 0) and (tempMaxVersion <> EmptyStr)) then
    begin
      result := true;
    end;
  end;
end;

function TABRegisterFile.Down(aLocalFileName,aSubPath: string;
  aServerPkg: TBlobField; aServerPkgVersion: string): boolean;
begin
  result := false;
  if (Assigned(aServerPkg)) and (aServerPkg.BlobSize > 0) then
  begin
    ABDoBusy;
    try
      ABMoveFile(ABAppPath+aLocalFileName,ABBackPath+aLocalFileName);

      if aSubPath<>EmptyStr then
      begin
        ABCreateDir(ABAppPath+ABGetpath(aSubPath));
      end;

      aServerPkg.SaveToFile(ABAppPath+ABGetpath(aSubPath) + aLocalFileName);
      if AnsiCompareText(ABGetFileExt(aLocalFileName),'Pak')=0 then
      begin
        ABUnDoPak(ABAppPath +ABGetpath(aSubPath)+ aLocalFileName,ABAppPath +ABGetpath(aSubPath),nil);
      end;

      if Trim(aServerPkgVersion)=EmptyStr then
        aServerPkgVersion:='0';
      ABWriteInI('Version',aLocalFileName,ABIIF(trim(aServerPkgVersion)=EmptyStr,'0',aServerPkgVersion),ABLocalVersionsFile);
      result := true;
    finally
      ABDoBusy(false);
    end;
  end;
end;

function TABRegisterFile.Down(aLocalFileName,aSubPath: string;
  aStream: TMemoryStream; aServerPkgVersion: string): boolean;
begin
  result := false;
  if (Assigned(aStream)) and (aStream.Size > 0) then
  begin
    ABDoBusy;
    try
      ABMoveFile(ABAppPath+aLocalFileName,ABBackPath+aLocalFileName);

      if aSubPath<>EmptyStr then
      begin
        ABCreateDir(ABAppPath+ABGetpath(aSubPath));
      end;
      aStream.SaveToFile(ABAppPath +ABGetpath(aSubPath)+ aLocalFileName);
      if AnsiCompareText(ABGetFileExt(aLocalFileName),'Pak')=0 then
      begin
        ABUnDoPak(ABAppPath +ABGetpath(aSubPath)+ aLocalFileName,ABAppPath +ABGetpath(aSubPath),nil);
      end;

      if Trim(aServerPkgVersion)=EmptyStr then
        aServerPkgVersion:='0';
      ABWriteInI('Version',aLocalFileName,ABIIF(trim(aServerPkgVersion)=EmptyStr,'0',aServerPkgVersion),ABLocalVersionsFile);
      result := true;
    finally
      ABDoBusy(false);
    end;
  end;
end;

function TABRegisterFile.GetVersion(aFileName: string;aReadVersionsFileOnEmptystr:boolean): string;
var
  tempExt:string;
begin
  result :=EmptyStr;
  tempExt:=ExtractFileExt(aFileName);
  if AnsiCompareText(tempExt, '.bpl') = 0 then
  begin
    result:=ABGetFileVersion(aFileName);
  end
  else if AnsiCompareText(tempExt, '.exe') = 0 then
  begin
    result:=ABGetFileVersion(aFileName);
  end
  else if AnsiCompareText(tempExt, '.dll') = 0 then
  begin
    result:=ABGetFileVersion(aFileName);
  end
  else if (AnsiCompareText(tempExt, '.dot') = 0) or
          (AnsiCompareText(tempExt, '.doc') = 0) or
          (AnsiCompareText(tempExt, '.xlt') = 0) or
          (AnsiCompareText(tempExt, '.xls') = 0) then
  begin
    result := ReadRemarkFromFile(aFileName);
  end
  else
    result:=ABGetFileVersion(aFileName);

  result:=Trim(result);

  if (result=EmptyStr) and
     (aReadVersionsFileOnEmptystr)  then
  begin
    result:= Trim(ABReadInI('Version',ABGetFilename(aFileName, false),ABLocalVersionsFile));
  end;

  if result=EmptyStr  then
    result:='0';
end;

function TABRegisterFile.ReadRemarkFromFile(const AFileName: string): string;
  function ParseProperty(propID: dWord; Value: Pointer): string;
  begin
    case propID of
      $00000009: result := string(Value);
    end;
  end;

  function LoadFromStorage(Storage: IStorage; propID: dWord):string;
  var
    OStream: IStream;
    streamName: array[0..MAX_PATH - 1] of WideChar;
    Stream: TOleStream;
    str: string;
    ID, PropertyCount, StartPos, prevPos, offset, TypeID, LenID: dWord;
    i: Integer;
  begin
    result := EmptyStr;

    StringToWideChar(#5'SummaryInformation', streamName, MAX_PATH);

    if Succeeded(Storage.OpenStream(streamName, nil,
      STGM_READ or STGM_SHARE_EXCLUSIVE, 0, OStream)) then
    begin
      Stream := TOleStream.Create(OStream);
      try
        if Stream.Size > 0 then
        begin
          Stream.Seek($2C, soFromBeginning);
          Stream.Read(StartPos, SizeOf(StartPos));

          Stream.Seek(StartPos + 4, soFromBeginning);
          Stream.Read(PropertyCount, SizeOf(PropertyCount));
          for i := 0 to PropertyCount - 1 do
          begin
            Stream.Read(ID, SizeOf(ID));
            Stream.Read(offset, SizeOf(offset));
            prevPos := Stream.Position;

            Stream.Seek(StartPos + offset, soFromBeginning);
            Stream.Read(TypeID, SizeOf(TypeID));

            case TypeID of
              VT_LPSTR:
                begin
                  // null terminated String
                  Stream.Read(LenID, SizeOf(LenID));
                  SetLength(str, LenID);
                  Stream.Read(PChar(str)^, LenID);
                  result := ParseProperty(ID, @str[1]);
                end;
            end;
            //restore position
            Stream.Position := prevPos;
          end;
        end;
      finally
        Stream.Free;
      end;
    end
  end;
var
  Root: IStorage;
  WC: PWideChar;
begin
  GetMem(WC, 512);
  try
    StringToWideChar(AFileName, WC, 512);
    //判断一个文件是否是复合文件
    if StgIsStorageFile(WC) = S_OK then
    begin
      OleCheck(StgOpenStorage(PWideChar(AFileName), nil,STGM_READ or STGM_SHARE_DENY_NONE or STGM_PRIORITY, nil, 0, Root));

      result := LoadFromStorage(Root, $00000009);
    end
    else
      raise Exception.Create('File is not a compound storage');
  finally
    FreeMem(WC, 512);
  end;
end;

procedure TABRegisterFile.Up( aLocalFile: string;var aMemoryStream:TMemoryStream;var aFormClassName: string);
var
  tempHModule: HModule;
begin
  if not ABCheckFileExists(aLocalFile) then
    exit;

  aMemoryStream.Position := 0;
  aMemoryStream.LoadFromFile(aLocalFile);
  aMemoryStream.Position := 0;

  if (AnsiCompareText(ExtractFileExt(aLocalFile), '.bpl') = 0) then
  begin
    tempHModule := loadpackage(aLocalFile);
    ABDoBusy;
    try
      if tempHModule <> null then
      begin
        @ABRegisterPro := GetProcAddress(tempHModule, 'ABRegister');
        if Assigned(@ABRegisterPro) then
        begin
          ABRegisterPro(aFormClassName);
        end;
      end;
    finally
      ABDoBusy(false);
      UnLoadPackage(tempHModule);
    end;
  end;
end;

function TABRegisterFile.GetTemplateFormClass(aFileName: string;aSubPath: string): string;
var
  tempHModule: HModule;
  tempFileFullName,
  tempFormClassName: string;
  i:LongInt;
begin
  i:=FTemplateNameStrings.IndexOf(aFileName);
  if i>=0 then
  begin
    result:=FTemplateFormClassStrings[i];
  end
  else
  begin
    tempFileFullName := aFileName;
    if (ABCheckFileExists(ABAppPath +ABGetpath(aSubPath)+ aFileName)) then
      tempFileFullName := ABAppPath+ABGetpath(aSubPath) + aFileName;

    if (ABCheckFileExists(tempFileFullName)) then
    begin
      if (AnsiCompareText(ExtractFileExt(tempFileFullName), '.bpl') = 0) then
      begin
        tempHModule := loadpackage(tempFileFullName);
        if tempHModule > 0 then
        begin
          @ABRegisterPro := GetProcAddress(tempHModule, 'ABRegister');
          if Assigned(@ABRegisterPro) then
          begin
            ABRegisterPro(tempFormClassName);
            FTemplateNameStrings.Add(aFileName);
            FTemplateHModuleStrings.Add(IntToStr(tempHModule));
            FTemplateFormClassStrings.Add(tempFormClassName);
            result:=tempFormClassName;
          end;
        end;
      end;
    end;
  end;
end;

function TABRegisterFile.LoadRun(aFileName: string;aCaption: string;aSubPath: string;aGuid: string;
  aTemplateName:string;
  aIsShowModal: boolean;
  aIsMDIClient: boolean): boolean;
  function DoCreateForm(aFormClassName: string;aHModule:HModule;aGuid, aFileName: string): Boolean;
  var
    tempVariant:Variant;
    tempFormClass: TPersistentClass;
    tempForm: TForm;
    I: Integer;
  begin
    Result := True;
    tempFormClass := GetClass(aFormClassName);
    if Assigned(tempFormClass) then
    begin
      tempForm := TABFormClass(tempFormClass).CreateAndSetName(Application,aTemplateName,aFileName);
      if aCaption<>EmptyStr then
        tempForm.Caption:=aCaption;
      RegisterForm(aHModule,aGuid, aFileName, tempForm);
      tempForm.Position := poMainFormCenter;

      //会触发ONSHOW等事件,引起不必要的处理
      if aIsMDIClient then
        tempForm.FormStyle := fsMDIChild;

      if aIsShowModal then
        tempForm.ShowModal
      else
        tempForm.Show;
    end
    else
    begin
      ABShow('模块中未注册类型[%s],请检查.',[aFormClassName]);
      Result := false;
    end;
  end;
var
  tempOleVariant: OleVariant;
  tempHModule: HModule;
  tempForm: TForm;
  tempFileFullName,
  tempFormClassName: string;
  tempCreateForm: Boolean;
begin
  if (ABLocalParams.Debug) then
    ABWriteLog('Begin LoadRun '+aFileName);

  Result := false;
  if aFileName = EmptyStr then
    exit;

  Result := true;
  tempFileFullName := aFileName;
  if (ABCheckFileExists(ABAppPath +ABGetpath(aSubPath)+ aFileName)) then
    tempFileFullName := ABAppPath+ABGetpath(aSubPath) + aFileName;

  LastRunFileBeginTime := Now;

  if (ABCheckFileExists(tempFileFullName)) or
     (aTemplateName<>emptystr) then
  begin
    if (AnsiCompareText(ExtractFileExt(tempFileFullName), '.bpl') = 0) then
    begin
      tempForm := GetFormByFileName(aFileName);
      if (Assigned(tempForm)) then
      begin
        if not tempForm.Visible then
        begin
          if aIsShowModal then
            tempForm.ShowModal
          else
            tempForm.Show;
        end
        else
        begin
          if (tempForm.WindowState in [wsMinimized]) then
            tempForm.WindowState := wsnormal;

          tempForm.BringToFront;
        end;
      end
      else
      begin
        if aTemplateName<>EmptyStr then
        begin
          result:=DoCreateForm(GetTemplateFormClass(aTemplateName),0,aGuid, aFileName);
        end
        else
        begin
          tempHModule := loadpackage(tempFileFullName);
          try
            if tempHModule <> null then
            begin
              @ABRegisterPro := GetProcAddress(tempHModule, 'ABRegister');
              if Assigned(@ABRegisterPro) then
              begin
                ABRegisterPro(tempFormClassName);
                tempCreateForm := true;
                @ABRunBeforeCreateFormPro := GetProcAddress(tempHModule,'ABRunBeforeCreateForm');
                if Assigned(@ABRunBeforeCreateFormPro) then
                begin
                  ABRunBeforeCreateFormPro(tempCreateForm);
                end;

                if tempCreateForm then
                begin
                  result:=DoCreateForm(tempFormClassName,tempHModule,aGuid, aFileName);
                end;
              end
              else
              begin
                UnRegisterModuleClasses(tempHModule);
                UnLoadPackage(tempHModule);
                ABShow('包中没有注册函数:ABRegister,请检查');
                Result := false;
              end;
            end;
          except
            UnRegisterModuleClasses(tempHModule);
            UnLoadPackage(tempHModule);
            raise;
          end;
        end;
      end;
    end
    else if (AnsiCompareText(ExtractFileExt(tempFileFullName), '.exe') = 0) then
    begin
      ShellExecute(0, 'open', PChar(tempFileFullName), nil, nil, sw_shownormal);
    end
    else if (AnsiCompareText(ExtractFileExt(tempFileFullName), '.dll') = 0) then
    begin
      //DLL的实现还未加入
    end
    else if (AnsiCompareText(ExtractFileExt(tempFileFullName), '.Dot') = 0) then
    begin
      tempOleVariant := CreateOleObject('Word.Application');
      tempOleVariant.Documents.Open(tempFileFullName, True);
      tempOleVariant.Visible := True;
    end
    else if (AnsiCompareText(ExtractFileExt(tempFileFullName), '.Doc') = 0) then
    begin
      ShellExecute(0, 'open', PChar(tempFileFullName), nil, nil, sw_shownormal);
    end
    else if (AnsiCompareText(ExtractFileExt(tempFileFullName), '.Xlt') = 0) then
    begin
      //XLT打开后会自动生成一个文档,并不是XLT模板,与实际不符???
      tempOleVariant := CreateOleObject('Excel.Application');
      tempOleVariant.Workbooks.Open(tempFileFullName);
      tempOleVariant.Visible := True;
    end
    else if (AnsiCompareText(ExtractFileExt(tempFileFullName), '.Xls') = 0) then
    begin
      ShellExecute(0, 'open', PChar(tempFileFullName), nil, nil, sw_shownormal);
    end
    else
    begin
      ShellExecute(0, 'open', PChar(tempFileFullName), nil, nil, sw_shownormal);
    end;
  end
  else if AnsiCompareText(Copy(tempFileFullName, 1, Length('Run:')), 'Run:') = 0 then
  begin
    ShellExecute(0, 'open', PChar(Copy(tempFileFullName, Length('Run:') + 1,
      Length(tempFileFullName))), nil, nil, sw_shownormal);
  end
  else
  begin
    //ABShow('打开的文件[%s]不存在,请检查.',[tempFileFullName]);
    Result := false;
  end;
  LastRunFileEndTime := Now;

  if (ABLocalParams.Debug) then
    ABWriteLog('End   LoadRun '+aFileName);
end;

constructor TABRegisterFile.Create;
begin
  inherited;
  FFlist := TStringList.Create;
end;

destructor TABRegisterFile.Destroy;
var
  i: longint;
begin
  //释放未关闭的结构
  for i := 0 to FFlist.Count - 1 do
  begin
    PABPackageList(FFlist.Objects[i]).IsFree := true;
  end;
  FreeHaveFreeFlag;
  FFlist.Free;
  inherited;
end;

procedure TABRegisterFile.RegisterForm(aHModule: HModule;aGuid,aFileName: string; aForm: TForm);
var
  tempPackage: PABPackageList;
begin
  if not Assigned(GetFormByFormName(aForm.Name)) then
  begin
    new(tempPackage);
    tempPackage.Guid := aGuid;
    tempPackage.FileName := aFileName;
    tempPackage.Hand := aHModule;

    tempPackage.IsFree := false;
    tempPackage.Form := aForm;
    FFlist.AddObject(aForm.Name, Pointer(tempPackage));
  end;
end;

procedure TABRegisterFile.SetFlist(const Value: TStrings);
begin
  FFlist.Assign(Value);
end;

procedure TABRegisterFile.SetFreeFlag(aFormName: string);
var
  i: Integer;
begin
  for i := 0 to FFlist.Count - 1 do
  begin
    if (AnsiCompareText(FFlist.Strings[i], aFormName) = 0) then
    begin
      PABPackageList(FFlist.Objects[i]).IsFree := true;
      break;
    end;
  end;
end;

function TABRegisterFile.FreeHaveFreeFlag(aFreeComponent: boolean): string;
  procedure DoUnloadPackage(Module: HModule);
  var
    i: Integer;
    M: TMemoryBasicInformation;
  begin
    if Module = 0 then
      exit;

    for i := Application.ComponentCount - 1 downto 0 do
    begin
      VirtualQuery(GetClass(Application.Components[i].ClassName), M, SizeOf(M));
      if (HMODULE(M.AllocationBase) = Module) then
        Application.Components[i].Free;
    end;
  end;

var
  i: Integer;
  tempHModule: HModule;
begin
  result := EmptyStr;
  for i := FFlist.Count - 1 downto 0 do
  begin
    if PABPackageList(FFlist.Objects[i]).IsFree then
    begin
      PABPackageList(FFlist.Objects[i]).IsFree := false;

      if result = EmptyStr then
        result := PABPackageList(FFlist.Objects[i]).FileName
      else
        result := result + ',' + PABPackageList(FFlist.Objects[i]).FileName;

      tempHModule := PABPackageList(FFlist.Objects[i]).Hand;
      if tempHModule>0 then
      begin
        if aFreeComponent then
          DoUnloadPackage(tempHModule);

        UnRegisterModuleClasses(tempHModule);
        UnLoadPackage(tempHModule);
      end;
      Dispose(PABPackageList(FFlist.Objects[i]));
      FFlist.Delete(i);
    end;
  end;
end;

function TABRegisterFile.GetFileNameByFormName(aName: string): string;
var
  i: Integer;
begin
  result := '';
  for i := 0 to FFlist.Count - 1 do
  begin
    if (AnsiCompareText(PABPackageList(FFlist.Objects[i]).Form.Name, aName) = 0) then
    begin
      result := PABPackageList(FFlist.Objects[i]).FileName;
      break;
    end;
  end;
end;

function TABRegisterFile.GetFormByFormName(aName: string): TForm;
var
  i: Integer;
begin
  result := nil;
  for i := 0 to FFlist.Count - 1 do
  begin
    if (AnsiCompareText(PABPackageList(FFlist.Objects[i]).Form.Name, aName) = 0) then
    begin
      result := PABPackageList(FFlist.Objects[i]).Form;
      break;
    end;
  end;
end;

function TABRegisterFile.GetHandByFileName(aName: string): HMODULE;
var
  i: Integer;
begin
  result := 0;
  for i := 0 to FFlist.Count - 1 do
  begin
    if (AnsiCompareText(PABPackageList(FFlist.Objects[i]).FileName, aName) = 0) then
    begin
      result := PABPackageList(FFlist.Objects[i]).Hand;
      break;
    end;
  end;
end;

function TABRegisterFile.GetFormByFileName(aName: string): TForm;
var
  i: Integer;
begin
  result := nil;
  for i := 0 to FFlist.Count - 1 do
  begin
    if (AnsiCompareText(PABPackageList(FFlist.Objects[i]).FileName, aName) = 0) then
    begin
      result := PABPackageList(FFlist.Objects[i]).Form;
      break;
    end;
  end;
end;

function TABRegisterFile.GetListItemByFileName(aName: string): PABPackageList;
var
  i: Integer;
begin
  result := nil;
  for i := 0 to FFlist.Count - 1 do
  begin
    if (AnsiCompareText(PABPackageList(FFlist.Objects[i]).FileName, aName) = 0) then
    begin
      result := PABPackageList(FFlist.Objects[i]);
      break;
    end;
  end;
end;

function TABRegisterFile.GetHandByFormName(aName: string): HMODULE;
var
  i: Integer;
begin
  result := 0;
  for i := 0 to FFlist.Count - 1 do
  begin
    if (AnsiCompareText(PABPackageList(FFlist.Objects[i]).Form.Name, aName) = 0) then
    begin
      result := PABPackageList(FFlist.Objects[i]).Hand;
      break;
    end;
  end;
end;

function TABRegisterFile.GetGuidByFileName(aName: string): string;
var
  i: Integer;
begin
  result := '';
  for i := 0 to FFlist.Count - 1 do
  begin
    if (AnsiCompareText(PABPackageList(FFlist.Objects[i]).FileName, aName) = 0) then
    begin
      result := PABPackageList(FFlist.Objects[i]).Guid;
      break;
    end;
  end;
end;

function TABRegisterFile.GetFormNameByFileName(aName: string): string;
var
  i: Integer;
begin
  result := '';
  for i := 0 to FFlist.Count - 1 do
  begin
    if (AnsiCompareText(PABPackageList(FFlist.Objects[i]).FileName, aName) = 0) then
    begin
      result := PABPackageList(FFlist.Objects[i]).Form.Name;
      break;
    end;
  end;
end;

procedure ABInitialization;
begin
  ABRegisterFile := TABRegisterFile.Create;
  FTemplateNameStrings:=TStringList.Create;
  FTemplateHModuleStrings:=TStringList.Create;
  FTemplateFormClassStrings:=TStringList.Create;
end;

procedure ABFinalization;
var
  i:LongInt;
  tempHModule: HModule;
begin
  ABRegisterFile.Free;
  //释放模板
  for I := 0 to FTemplateHModuleStrings.Count-1 do
  begin
    tempHModule:=StrToInt(FTemplateHModuleStrings[i]);
    UnRegisterModuleClasses(tempHModule);
    UnLoadPackage(tempHModule);
  end;
  FTemplateHModuleStrings.Free;
  FTemplateNameStrings.Free;
  FTemplateFormClassStrings.Free;
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;

end.
