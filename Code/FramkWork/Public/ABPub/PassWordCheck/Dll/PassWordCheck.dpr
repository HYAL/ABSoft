library PassWordCheck;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
  SysUtils,
  Classes,
  Types,
  Math;

{$R *.res}




type
  //X方向位置
  TABXDirection = (xdLeft, xdRight);

const
  //每个密码字符转成的密文数字长度
  ABPassCharIntLength: LongInt= 6;

function ABTrunc(aNum: double): Integer;
begin
  Result := trunc(SimpleRoundTo(aNum));
end;

function ABTicker: DWord; register;
begin
  asm
    push EAX
    push EDX
    db $0f,$31
    mov Result, EAX
    pop EDX
    pop EAX
  end;
end;

//填充字符串到指定长度
function ABFillStr(aStr: string;
                    aSetLength: longint;
                    aAddStr: string = '0';
                    aXDirection: TABXDirection = xdLeft; aTruncation: boolean = true): string;
var
  temstr1: string;
begin
  if length(aStr) >= aSetLength then
  begin
    result := copy(aStr, 1, aSetLength);
  end
  else
  begin
    temstr1 := EmptyStr;
    while length(temstr1 + aStr+aAddStr)<=aSetLength do
    begin
      temstr1 := temstr1 + aAddStr;
    end;

    if length(temstr1)>aSetLength then
      result := copy(temstr1, 1, aSetLength);

    if xdLeft = aXDirection then
      result := temstr1 + aStr
    else if xdright = aXDirection then
      result := aStr + temstr1;
  end;
end;

//加密
function ABDOPassWord(aPassWord: string; aKey: string = ABSoftName): string;stdcall;
var
  temInt, i, j: longint;
begin
  Result := aPassWord;
  if aPassWord = emptystr then
    Exit;

  Result := emptystr;
  Randomize;
  temInt := ABTrunc(abs(Random(ABTicker) mod 128));
  for i := 1 to length(aPassWord) do
  begin
    if i > Length(aKey) then
      j := Length(aKey)
    else
      j := i;

    Result := Result + ABFillStr(IntToStr(Ord(aPassWord[i]) + temInt + i +
      Ord(aKey[j])), ABPassCharIntLength);
  end;
  result := Result + ABFillStr(IntToStr(temInt), ABPassCharIntLength);
end;

//解密
function ABUnDOPassWord(aPassWord: string; aKey: string = ABSoftName): string;stdcall;
var
  temInt, i, j: longint;
begin
  Result := aPassWord;
  if aPassWord = emptystr then
    Exit;

  Result := emptystr;
  temInt := StrToIntDef(Copy(aPassWord, Length(aPassWord) - ABPassCharIntLength +1, ABPassCharIntLength),0);
  aPassWord := Copy(aPassWord, 1, Length(aPassWord) - ABPassCharIntLength);
  for i := 1 to ABTrunc(length(aPassWord) / ABPassCharIntLength) do
  begin
    if i > Length(aKey) then
      j := Length(aKey)
    else
      j := i;

    Result := Result + Chr(StrToIntDef(Copy(aPassWord, (i - 1) * ABPassCharIntLength
      + 1, ABPassCharIntLength),0) - temInt - i - Ord(aKey[j]));
  end;
end;

//检测密码解密后是否与明文一致，  aWord=明文，aPassWord=加密码后的密码，aKey=公钥
//返回值=0表示成功,-1表示失败
function ABOutCheckPassword(aWord,aPassWord: Pansichar): longint;stdcall;
begin
  result:=-1;
  if aWord=ABUnDoPassword(aPassWord) then
    result:=0;
end;

function ABOutDOPassWord(aPassWord: Pansichar): Pansichar;stdcall;
begin
  result:=PAnsiChar(AnsiString(ABDOPassWord(aPassWord,ABSoftName)));
end;

function ABOutUnDOPassWord(aPassWord: Pansichar): Pansichar;stdcall;
begin
  result:=PAnsiChar(AnsiString(ABUnDoPassword(aPassWord,ABSoftName)));
end;

exports
  ABOutCheckPassword Name 'CheckPassword',
  ABOutDOPassWord Name 'DOPassWord',
  ABOutUnDOPassWord Name 'UnDOPassWord';


begin
end.
