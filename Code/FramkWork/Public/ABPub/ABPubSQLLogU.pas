{
记录SQL日志单元
//在所有要记录日志的地方如下调用
//if (Assigned(ABSQLLog)) and (Assigned(ABSQLLog.ExecSQL)) then
//  ABSQLLog.ExecSQL(tempType,tempSQL);
//在SQL监视器中定义一个过程ABSQLLog1ExecSQL，指向ABSQLLog.ExecSQL:=ABSQLLog1ExecSQL;
//这样在过程ABSQLLog1ExecSQL中就能显示所有的SQL日志了
}
unit ABPubSQLLogU;

interface
{$I ..\ABInclude.ini}
uses
  Classes;

type
  TABSQLNotifyEvent = procedure(aType:string;aSQL: string) of object;
  TABSQLLog= class(TComponent )
  private
    FExecSQL: TABSQLNotifyEvent;
    { Private declarations }
  protected
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    { Public declarations }
  published
    //在有与后台的数据交互时都会调用此事件
    property ExecSQL:TABSQLNotifyEvent  read FExecSQL  write FExecSQL;
    { Published declarations }
  end;

var
  ABSQLLog: TABSQLLog;


implementation


{ TABSQLLog }

constructor TABSQLLog.Create(AOwner: TComponent);
begin
  inherited;

end;

destructor TABSQLLog.Destroy;
begin

  inherited;
end;

procedure ABFinalization;
begin
  ABSQLLog.free;
end;

procedure ABInitialization;
begin
  ABSQLLog:= TABSQLLog.Create(nil);
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;


end.
