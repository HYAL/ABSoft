{
注册组件单元
}
unit ABPubRegU;

interface
{$I ..\ABInclude.ini}
uses
  ABPubCheckTreeViewU,
  ABPubPropertyEditU,
  ABPubLedU,
  ABPubItemListsFrameU,
  ABPubItemListFrameU,
  ABPubIPEditU,
  ABPubDBMainDetailPopupMenuU,
  ABPubStringGridU,

  ABPubPanelU,
  ABPubSockU,
  ABPubSockFrameU,
  ABPubScrollBoxU,
  ABPubMultilingualDBNavigatorU,
  ABPubDBLabelsU,
  ABPubCheckedComboBoxU,
  ABPubDownButtonU,

  ABPubFuncU,
  ABPubConstU,

  DesignIntf,

  ToolsApi,Dialogs, Windows, Classes,Forms;

procedure Register;

implementation
{$R ABFramkWorkDelphiLoadIco.dcr}


{$IF (ABDelphiVersion_int>=185)}
var
  FloadSplashed:Boolean;

//在DELPHI的开启画面中加载描述
procedure RegisterSplashItem;
var
  tempVersionFileName:string;
begin
  if FloadSplashed then
    Exit;

  //ABFramkWorkDelphiLoadIco.dcr中的ABSOFTSPLASH代表框架Logo
  Application.ProcessMessages;
  try
    tempVersionFileName:=ABGetpath(ABGetDelphiBplPath(ABDelphiVersion))+'ABPubG.bpl';
    if (ABCheckFileExists(tempVersionFileName))  then
    begin
      SplashScreenServices.AddPluginBitmap(ABSoftName+' Framwork, Build '+ABGetFileVersion(tempVersionFileName),
                                            LoadBitmap(HInstance, 'ABSOFTSPLASH'));
      Sleep(100);
      Application.ProcessMessages;
    end;
  finally
    FloadSplashed:=true;
  end;
end;
{$IFEND}

procedure Register;
begin
  RegisterSplashItem;

  RegisterComponents('ABPub', [TABSocket,
                               TABSockUI]);

  RegisterComponents('ABPub',[TABCheckedComboBox]);
  RegisterComponents('ABPub',[TABScrollBox]);
  RegisterComponents('ABPub',[TABStringGrid]);
  RegisterComponents('ABPub',[TABIPEdit]);
  RegisterComponents('ABPub',[TABItemList]);
  RegisterComponents('ABPub',[TABItemLists]);
  RegisterComponents('ABPub',[TABLed]);
  RegisterComponents('ABPub',[TABCheckTreeView]);
  RegisterComponents('ABPub',[TABDownButton]);
  RegisterComponents('ABPub',[TABCustomPanel]);


  RegisterComponents('ABPubDB',[TABMultilingualDBNavigator]);

  RegisterComponents('ABPubDB',[TABDBMainDetailPopupMenu]);
  RegisterPropertyEditor(TypeInfo(string),TABDBMainDetailPopupMenu , 'ParentField',TABFieldComboxProperty);
  RegisterPropertyEditor(TypeInfo(string),TABDBMainDetailPopupMenu , 'KeyField',TABFieldComboxProperty);

  RegisterComponents('ABPubDB',[TABDBLabels]);
  RegisterPropertyEditor(TypeInfo(string),TABDBLabels ,    'DataField',TABFieldSelectsProperty);
end;


end.
