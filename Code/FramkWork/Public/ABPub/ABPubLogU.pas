{
日志单元 记录异常和普通日志
}
unit ABPubLogU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubConstU,
  ABPubVarU,

  forms,SysUtils,windows,dateutils;

//记录异常日志
//调用Assert(False,'错误类型')会记录异常发生的文件名、所在行号、所在地址
//调用ABWriteExceptLog(消息)以记录特定的异常日志
//aMsg=消息
procedure ABWriteExceptLog(aMsg: string);overload;

//记录普通日志
//aMsg=消息
procedure ABWriteLog(aMsg: string);overload;
//aMsg=消息
//aAppend=是否将消息追加到日志文件尾，如为False则重新创建日志文件
//aAddDatatimeMemory=消息存到日志文件中时是否增加时间与内存信息(当前内存 总计内存 当前时间 总计时间)
procedure ABWriteLog(aMsg: string;
                     aAppend: boolean;
                     aAddDatatimeMemory: boolean); overload;
//aMsg=消息
//aAppend=是否将消息追加到日志文件尾，如为False则重新创建日志文件
//aAddDatatimeMemory=消息存到日志文件中时是否增加时间与内存信息(当前内存 总计内存 当前时间 总计时间)
//aLogFileName=写消息到指定日志文件
procedure ABWriteLog(aMsg: string;
                     aAppend: boolean;
                     aAddDatatimeMemory: boolean;
                     aLogFileName: string);overload;

implementation

var
  ABBeginMemory:LongInt;                    //程序开始运行前的内存(单位:KB)
  ABBeginDatetime:TDateTime;                //程序开始运行前的时间

  ABPriAfterLogMemory:LongInt;                   //上一次记录日志前的程序内存
  ABPriAfterLogDatetime:TDateTime;               //上一次记录日志前的程序时间

var
  FOldAssertErrorProc: TAssertErrorProc;    //自动得到当前单元名

//aFilename=异常发生的文件名；
//aLineNumber=异常所在行号；
//aErrorAddr=异常所在地址
//对AssertErrorProc赋值后,就定义了自己的Assert处理过程,调用Assert(False) 可以得到当前的单元名称
procedure ABWriteExceptLog(const Message: string; aFilename: string;aLineNumber: Integer;aErrorAddr: Pointer);overload;
begin
  aFilename:=extractfilename(ChangeFileExt(aFilename,''));
  ABWriteLog(Format('Error: %s, Addr: %p, in file(%5d): %s',[message, aErrorAddr, aLineNumber, aFilename]),
             true,true,ABExceptLogFile);
end;

function ABGetCanUseMemory: Dword;
var
  memStatus: TMemoryStatus;
begin
  memStatus.dwLength := sizeOf(memStatus);
  GlobalMemoryStatus(memStatus);
  Result := memStatus.dwAvailPhys div 1024;
end;

procedure ABWriteExceptLog(aMsg: string);
begin
  ABWriteLog(aMsg,true,true,ABExceptLogFile);
end;

procedure ABWriteLog(aMsg: string);
begin
  ABWriteLog(aMsg,True,True);
end;

procedure ABWriteLog(aMsg: string;
                     aAppend: boolean; aAddDatatimeMemory: boolean);
begin
  ABWriteLog(aMsg,aAppend,aAddDatatimeMemory,ABLogFile);
end;

procedure ABWriteLog(aMsg: string;
                     aAppend: boolean; aAddDatatimeMemory: boolean;
                     aLogFileName: string);
var
  TempTxtFile: textfile;
  tempMsg,
  tempAppendStr,
  tempBackFileName: string;

  tempMemory:LongInt;
  tempDatetime:TDateTime;
begin
  tempMsg:=emptystr;
  tempAppendStr:=emptystr;
  try
    if aAddDatatimeMemory then
    begin
      tempAppendStr := FormatDateTime('YYYY-MM-DD HH:NN:SS:ZZZ',now);

      //增加使用的内存
      tempMemory:=ABGetCanUseMemory;
      tempAppendStr :=tempAppendStr+' '+Format('%8s',[Format('%.2f', [(ABPriAfterLogMemory-tempMemory) / 1024])])+
                                    ' '+Format('%8s',[Format('%.2f', [(ABBeginMemory-tempMemory) / 1024])]);
      ABPriAfterLogMemory:=tempMemory;

      //增加使用的时间
      tempDatetime:=now();
      tempAppendStr :=tempAppendStr+' '+ Format('%8s',[Format('%.2f', [SecondSpan(tempDatetime,ABPriAfterLogDatetime)])])+
                                    ' '+ Format('%8s',[Format('%.2f', [SecondSpan(tempDatetime,ABBeginDatetime)])]);
      ABPriAfterLogDatetime:=tempDatetime;

      tempMsg:=tempAppendStr;
    end;

    if tempMsg=EmptyStr then
      tempMsg:= aMsg
    else
      tempMsg:=tempMsg+' '+aMsg;

    if not aAppend then
    begin
      if FileExists(aLogFileName) then
      begin
        tempBackFileName:=FormatDateTime('YYYY-MM-DD HH-NN-SS-ZZZ',now);
        CopyFile(PChar(aLogFileName),
                 PChar(ABBackPath+extractfilename(ChangeFileExt(aLogFileName, ''))+'['+tempBackFileName+'].txt'),
                 False);
      end;
    end;

    AssignFile(TempTxtFile, aLogFileName);
    try
      if not aAppend then
        Rewrite(TempTxtFile)
      else
      begin
        if FileExists(aLogFileName) then
          Append(TempTxtFile)
        else
          Rewrite(TempTxtFile);
      end;
      Writeln(TempTxtFile,tempMsg);
    finally
      CloseFile(TempTxtFile);
    end;
  except
    //raise;
  end;
end;

procedure ABInitialization;
var
  tempAppFullName,
  tempAppName,
  tempSoftSetPath:string;
begin
  //设置新的Assert处理过程,以取得当前的单元名称
  FOldAssertErrorProc := AssertErrorProc;
  AssertErrorProc := @ABWriteExceptLog;

  tempAppFullName := Application.EXEName;
  tempAppName     := ChangeFileExt(extractfilename(tempAppFullName), '');
  tempSoftSetPath := ExtractFilePath(tempAppFullName) + ABSoftName + '_Set\'+tempAppName+'\';

  //初始化一些变量以供优化效率
  ABBeginDatetime :=now();
  ABBeginMemory :=ABGetCanUseMemory;
  ABPriAfterLogDatetime:= ABBeginDatetime;
  ABPriAfterLogMemory:= ABBeginMemory;

  ABWriteLog('日志开始 '+FormatDateTime('YYYY-MM-DD HH:NN:SS:ZZZ',now),false,false,ABExceptLogFile);
  ABWriteLog('日志开始 '+FormatDateTime('YYYY-MM-DD HH:NN:SS:ZZZ',now),false,false);
  ABWriteLog('当前内存=上一日志到当前日志所用内存(M)',True,false);
  ABWriteLog('总计内存=日志开始到当前日志所用内存(M)',True,false);
  ABWriteLog('当前时间=上一日志到当前日志所用时间(秒)',True,false);
  ABWriteLog('总计时间=日志开始到当前日志所用时间(秒)',True,false);

  ABWriteLog('',True,false);
  ABWriteLog('       日志时间         '+
             '当前内存 总计内存 当前时间 总计时间 日志行号 日志文件名                               日志内容',True,false);
end;

procedure ABFinalization;
begin
  ABWriteLog('日志结束 '+FormatDateTime('YYYY-MM-DD HH:NN:SS:ZZZ',now),True,false);

  //恢复修改过的 AssertErrorProc
  AssertErrorProc := FOldAssertErrorProc;
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;

end.
