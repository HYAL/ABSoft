{
增加的TScrollBox控件单元(增加OnHScroll、OnVScroll事件)
}
unit ABPubScrollBoxU;

interface
{$I ..\ABInclude.ini}
uses
  Forms,Messages,Classes,Windows;


type
  TScrollEvent = procedure(var Message: TWMScroll) of object;

  //框架的TScrollBox 增加两事件
  TABScrollBox = class(TScrollBox)
  private
    FOnHScroll,
    FOnVScroll: TScrollEvent;

    procedure WMHScroll(var Message: TWMScroll); message WM_HSCROLL;
    procedure WMVScroll(var Message: TWMScroll); message WM_VSCROLL;
  published
    property OnHScroll: TScrollEvent read FOnHScroll write FOnHScroll;
    property OnVScroll: TScrollEvent read FOnVScroll write FOnVScroll;
  end;

implementation

{ TABScrollBox }

procedure TABScrollBox.WMHScroll(var Message: TWMScroll);
begin
  if (Message.ScrollCode = sb_ThumbTrack) or (Message.ScrollCode = SB_PAGERIGHT) then
  begin
    HorzScrollBar.Position := Message.Pos;
    if Assigned(FOnHScroll) then
      FOnHScroll(Message);
  end;
  inherited
end;

procedure TABScrollBox.WMVScroll(var Message: TWMScroll);
begin
  if (Message.ScrollCode = sb_ThumbTrack) or (Message.ScrollCode = SB_PAGERIGHT) then
  begin
    VertScrollBar.Position := Message.Pos;
    if Assigned(FOnVScroll) then
      FOnVScroll(Message);
  end;
  inherited
end;

end.
