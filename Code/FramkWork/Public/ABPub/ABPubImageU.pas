{
��ʾ��༭ͼƬ��Ԫ
}
unit ABPubImageU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,
  ABPubFuncU,

  SysUtils,Classes,Graphics,Controls,Forms,Dialogs,ExtCtrls,StdCtrls;

type
  TABImageForm = class(TABPubForm)
    Panel1: TPanel;
    Button1: TButton;
    Button2: TButton;
    SaveDialog1: TSaveDialog;
    OpenDialog1: TOpenDialog;
    Button3: TButton;
    Image1: TImage;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    Fedit:Boolean;
    { Private declarations }
  public
    { Public declarations }
  end;

//��ʾ��༭ͼƬ
function ABShowEditImage(aPicture: TPicture;aReadOnly:Boolean=false;aCaption:string=''): boolean;

implementation

{$R *.dfm}

function ABShowEditImage(aPicture: TPicture;aReadOnly:Boolean;aCaption:string): boolean;
var
  tempForm:TABImageForm;
begin
  result:=False;
  tempForm := TABImageForm.Create(nil);
  if aCaption<>EmptyStr then
    tempForm.Caption:=aCaption;
  tempForm.Button1.Enabled:= not aReadOnly;
  try
    if (Assigned(aPicture)) then
    begin
      tempForm.Image1.Picture.Assign(aPicture);
    end;

    if tempForm.ShowModal=mrok then
    begin
      if (tempForm.FEdit)  then
      begin
        if (Assigned(tempForm.Image1.Picture)) then
          aPicture.Assign(tempForm.Image1.Picture);
      end;
      result:=true;
    end;
  finally
    tempForm.Free;
  end;
end;

procedure TABImageForm.Button1Click(Sender: TObject);
begin
  if (OpenDialog1.Execute)  then
  begin
    if (FileExists(OpenDialog1.FileName)) then
    begin
      Image1.Picture.LoadFromFile(OpenDialog1.FileName);
      Fedit:=True;
    end;
  end;
end;

procedure TABImageForm.Button2Click(Sender: TObject);
begin
  if (SaveDialog1.Execute)  then
  begin
    Image1.Picture.SaveToFile(ABGetSaveDialogFileName(SaveDialog1));
  end;
end;

procedure TABImageForm.Button3Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;


end.
