object ABShowMessageForm: TABShowMessageForm
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = #31995#32479#25552#31034
  ClientHeight = 318
  ClientWidth = 399
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 399
    Height = 143
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label2: TLabel
      Left = 0
      Top = 0
      Width = 399
      Height = 104
      Align = alClient
      Alignment = taCenter
      AutoSize = False
      BiDiMode = bdLeftToRight
      ParentBiDiMode = False
      Transparent = True
      Layout = tlCenter
      WordWrap = True
      OnDblClick = Label2DblClick
    end
    object Panel5: TPanel
      Left = 0
      Top = 104
      Width = 399
      Height = 39
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object Button1: TButton
        Left = 3
        Top = 3
        Width = 77
        Height = 25
        Caption = 'Button1'
        TabOrder = 0
        OnClick = Button1Click
      end
      object Button2: TButton
        Left = 82
        Top = 3
        Width = 77
        Height = 25
        Caption = 'Button2'
        TabOrder = 1
        OnClick = Button2Click
      end
      object Button3: TButton
        Left = 161
        Top = 3
        Width = 77
        Height = 25
        Caption = 'Button3'
        TabOrder = 2
        OnClick = Button3Click
      end
      object Button4: TButton
        Left = 240
        Top = 3
        Width = 77
        Height = 25
        Caption = 'button4'
        TabOrder = 3
        OnClick = Button4Click
      end
      object Button5: TButton
        Left = 319
        Top = 3
        Width = 77
        Height = 25
        Caption = 'Button5'
        TabOrder = 4
        OnClick = Button5Click
      end
    end
  end
  object ShowSecond_Panel: TPanel
    Left = 0
    Top = 143
    Width = 399
    Height = 25
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 121
      Top = 4
      Width = 17
      Height = 14
      AutoSize = False
      Caption = 'N'
    end
    object CheckBox1: TCheckBox
      Left = 136
      Top = 4
      Width = 97
      Height = 17
      Alignment = taLeftJustify
      Caption = #31186#21518#33258#21160#20851#38381
      TabOrder = 0
      OnClick = CheckBox1Click
    end
  end
  object ReMark_Panel: TPanel
    Left = 0
    Top = 168
    Width = 399
    Height = 150
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 2
    object ReMark_Memo: TMemo
      Left = 1
      Top = 1
      Width = 397
      Height = 148
      Align = alClient
      BorderStyle = bsNone
      ImeName = #24555#20048#20116#31508
      ScrollBars = ssBoth
      TabOrder = 0
      OnKeyDown = ReMark_MemoKeyDown
    end
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 289
    Top = 200
  end
end
