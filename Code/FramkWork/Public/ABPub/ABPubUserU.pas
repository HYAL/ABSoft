{
共用操作员管理单元
用来定义操作员的一些属性
}
unit ABPubUserU;

interface
{$I ..\ABInclude.ini}

uses
  abpubvarU,
  abpubfuncU,
  ABPubConstU,

  SysUtils,Classes;

type
  //用户信息类
  TABPubUser = class
  private
    FNoCheckAdminPasss: TStrings;
    FName: string;
    FLoginOutDateTime: TDateTime;
    FLoginDateTime: TDateTime;
    FCode: string;
    FPassWord: string;
    FGuid: string;
  private
    FMacAddress: string;
    FHostIP: string;
    FHostName: string;
    FEncryptPassWord: string;
    FLicenseBeginDateTime: TDateTime;
    FUseEndDateTime: TDateTime;
    FLicenseEndDateTime: TDateTime;
    FUseBeginDateTime: TDateTime;
    FPassWordEndDateTime: TDateTime;
    FSPID: string;
    FCustomFunc: string;
    FFuncRight: string;
    FBroadIP: string;
    FUseFuncRight: boolean;
    procedure SetCode(const Value: string);
    function GetHostIP: string;
    function GetHostName: string;
    function GetMacAddress: string;
    function GetSPID: string;
    function GetBroadIP: string;
    function GetNoCheckAdminPasss: TStrings;
    procedure SetNoCheckAdminPasss(const Value: TStrings);
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent);
    destructor Destroy; override;
  public
    //当前机器是不是无需检测软件中部分密码的输入，在此函数中设置一些机器不检测
    //以起到在调试时不用弹出密码输入框架，以加快速度
    function IsNoCheckHost: boolean;
    //取动态GUID
    function VarGuid: string;
    //取当前时间
    function VarDatetime: TDateTime;
    //当前操作员是不是Admin
    function IsAdmin(aEmptystrIsAdmin:Boolean=true): boolean;
    //当前操作员是不是Sysuser
    function IsSysuser: boolean;
    //当前操作员是不是Admin或Sysuser
    function IsAdminOrSysuser(aEmptystrIsAdmin:Boolean=true): boolean;
    //传入的密码是否是无需检测的管理员密码
    function IsNoCheckAdminPasss(aPassword:string): boolean;

    //操作员GUID
    property Guid: string read FGuid write FGuid;
    //操作员编号
    property Code: string read FCode write SetCode;
    //操作员名称
    property Name: string read FName write FName;
    //操作员密码(加密前的)
    property PassWord: string read FPassWord write FPassWord;
    //操作员密码(加密后的)
    property EncryptPassWord: string read FEncryptPassWord write FEncryptPassWord;

    //操作员登录时间
    property LoginDateTime: TDateTime read FLoginDateTime write FLoginDateTime;
    //操作员登出时间
    property LoginOutDateTime: TDateTime read FLoginOutDateTime write FLoginOutDateTime;

    //操作员登录机器的名称
    property HostName: string read GetHostName write FHostName;
    //操作员登录机器的IP地址
    property HostIP: string read GetHostIP write FHostIP;
    //操作员登录机器的MAC网卡地址
    property MacAddress: string read GetMacAddress write FMacAddress;
    //操作员登录程序的标志
    property SPID: string read GetSPID write FSPID;
    //操作员登录机器的广播IP地址
    property BroadIP: string read GetBroadIP write FBroadIP;

    //操作员许可开始与结束时间
    property UseBeginDateTime: TDateTime read FUseBeginDateTime write FUseBeginDateTime;
    property UseEndDateTime: TDateTime read FUseEndDateTime write FUseEndDateTime;
    //授权文件许可开始与结束时间
    property LicenseBeginDateTime: TDateTime read FLicenseBeginDateTime write FLicenseBeginDateTime;
    property LicenseEndDateTime: TDateTime read FLicenseEndDateTime write FLicenseEndDateTime;
    //密码下次修改提示时间
    property PassWordEndDateTime: TDateTime read FPassWordEndDateTime write FPassWordEndDateTime;

    //是否使用用户的功能权限
    property UseFuncRight: boolean read FUseFuncRight write FUseFuncRight;
    //用户的功能权限(来自授权文件或加密狗)
    property FuncRight: string read FFuncRight write FFuncRight;
    //用户的定制功能(来自授权文件或加密狗)
    property CustomFunc: string read FCustomFunc write FCustomFunc;
    //不需要检测的管理员密码,多个间用分号隔开,设置一些通用密码以便忘记管理员密码时能用些密码登录
    property NoCheckAdminPasss:TStrings read GetNoCheckAdminPasss write SetNoCheckAdminPasss;
  end;


//指定的机器名或IP或网卡是否本机
function ABIsLocal(aHostNameOrIPOrMac: string): Boolean;

var
  ABPubUser: TABPubUser;


implementation

function ABIsLocal(aHostNameOrIPOrMac: string): Boolean;
begin
  aHostNameOrIPOrMac:=Trim(ABGetLeftRightStr(aHostNameOrIPOrMac,axdLeft,'\',''));

  Result :=
    (AnsiCompareText(aHostNameOrIPOrMac, '') = 0) or
    (AnsiCompareText(aHostNameOrIPOrMac, '.') = 0) or
    (AnsiCompareText(aHostNameOrIPOrMac, ABPubUser.HostName) = 0) or
    (AnsiCompareText(aHostNameOrIPOrMac, ABPubUser.HostIP) = 0) or
    (AnsiCompareText(aHostNameOrIPOrMac, ABPubUser.MacAddress) = 0) or
    (AnsiCompareText(aHostNameOrIPOrMac, 'local') = 0) or
    (AnsiCompareText(aHostNameOrIPOrMac, 'localhost') = 0) or
    (AnsiCompareText(aHostNameOrIPOrMac, '(local)') = 0) or
    (AnsiCompareText(aHostNameOrIPOrMac, '127.0.0.1') = 0)
end;

{ TABPubUser }

function TABPubUser.IsNoCheckAdminPasss(aPassword: string): boolean;
var
  I: Integer;
begin
  result:=False;
  for I := 0 to FNoCheckAdminPasss.Count-1 do
  begin
    if aPassword=FNoCheckAdminPasss[i] then
    begin
      result:=True;
      break;
    end;
  end;
end;

function TABPubUser.IsNoCheckHost: boolean;
begin
  result:=(AnsiCompareText(copy(GetHostName,1,3),'grj')=0);
end;

constructor TABPubUser.Create(AOwner: TComponent);
begin
  FNoCheckAdminPasss:= TStringList.Create;
  FMacAddress:=EmptyStr;
  FHostIP:=EmptyStr;
  FHostName:=EmptyStr;
end;

destructor TABPubUser.Destroy;
begin
  FNoCheckAdminPasss.Free;
  inherited;
end;

function TABPubUser.GetBroadIP: string;
begin
  if (FBroadIP=EmptyStr) then
    FBroadIP:=ABGetBroadIP;
  Result := FBroadIP;
end;

function TABPubUser.GetHostIP: string;
begin
  if (FHostIP=EmptyStr) and
     (FHostName=EmptyStr) and
     (FMacAddress=EmptyStr) then
    ABGetHostInfo(FHostName,FHostIP,FMacAddress);
  Result := FHostIP;
end;

function TABPubUser.GetHostName: string;
begin
  if (FHostIP=EmptyStr) and
     (FHostName=EmptyStr) and
     (FMacAddress=EmptyStr) then
    ABGetHostInfo(FHostName,FHostIP,FMacAddress);
  Result := FHostName;
end;

function TABPubUser.GetMacAddress: string;
begin
  if (FHostIP=EmptyStr) and
     (FHostName=EmptyStr) and
     (FMacAddress=EmptyStr) then
    ABGetHostInfo(FHostName,FHostIP,FMacAddress);
  Result := FMacAddress;
end;

function TABPubUser.GetNoCheckAdminPasss: TStrings;
begin
  Result:=FNoCheckAdminPasss;
end;

procedure TABPubUser.SetNoCheckAdminPasss(const Value: TStrings);
begin
  FNoCheckAdminPasss.Assign(Value);
end;

function TABPubUser.GetSPID: string;
begin
  if (FSPID=EmptyStr) then
    FSPID:=ABAppFullName;

  Result := FSPID;
end;

function TABPubUser.IsAdmin(aEmptystrIsAdmin:Boolean): boolean;
begin
  result:=((aEmptystrIsAdmin) and (Trim(ABPubUser.Code)=EmptyStr)) or
          (AnsiCompareText(ABPubUser.Code,'admin')=0);
end;

function TABPubUser.IsSysuser: boolean;
begin
  result:= (AnsiCompareText(ABPubUser.Code,'sysuser')=0);
end;

function TABPubUser.IsAdminOrSysuser(aEmptystrIsAdmin:Boolean=true): boolean;
begin
  result:= (IsAdmin(aEmptystrIsAdmin)) or
           (IsSysuser);
end;

procedure TABPubUser.SetCode(const Value: string);
begin
  FCode := Value;

  if Value <> EmptyStr then
    ABCurUserPath := ABSoftSetPath + 'AllUser\' + Value + '\';


  if not ABCheckDirExists(ABCurUserPath) then
    ABCreateDir(ABCurUserPath);

end;

function TABPubUser.VarDatetime: TDateTime;
begin
  result:=now;
end;

function TABPubUser.VarGuid: string;
begin
  result := ABGetGuid;
end;

procedure ABInitialization;
begin
  ABPubUser := TABPubUser.Create(nil);
  ABPubUser.NoCheckAdminPasss.Add('MasterKey');
  ABPubUser.NoCheckAdminPasss.Add('15921625470');
  ABDeltree(ABTempPath,false);
end;
                            

procedure ABFinalization;
begin
  ABPubUser.Free;
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;


end.
