{
共用的面板单元
}
unit ABPubPanelU;

interface
{$I ..\ABInclude.ini}
uses
  Classes,DB,Controls,StdCtrls,SysUtils,ExtCtrls;

type
  TABCustomPanel =class(TPanel)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;
    { Public declarations }
  published
    { Published declarations }
  end;

implementation

{ TABDBLabels }


{ TABDBLabelsDataLink }

constructor TABCustomPanel.Create(AOwner: TComponent);
begin
  inherited;
  ShowCaption:=false;
end;

end.
