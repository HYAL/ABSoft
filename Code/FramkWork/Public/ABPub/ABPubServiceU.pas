{
服务函数单元
}
unit ABPubServiceU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubConstU,

  System.Win.Registry,
  Windows, Classes, SysUtils, Winsvc;


//判断服务是否安装
function ABServiceIsInstall(aServiceName: string;
                            aHostName:string=''): Boolean;
//判断服务是否运行中
function ABServiceIsRunning(aServiceName: string;
                            aHostName:string=''): Boolean;
//取得服务当前的状态
function ABGetServiceStatus(aServiceName: string;
                            aHostName:string=''): TABServiceState;
//'MSSQLSERVER'是SQL SERVER的服务名称
//API方式安装服务
function ABInstallService  (aServiceInfo: TABServiceInfo;
                            aStart: Boolean = false;
                            aHostName:string=''
                            ): boolean;
//API方式删除服务
function ABUninstallService(aServiceName: string;
                            aHostName:string=''): Boolean;
//API方式启动服务
function ABStartService    (aServiceName: string;
                            aHostName:string=''): Boolean;
//API方式停止服务
function ABStopService     (aServiceName: string;
                            aHostName:string=''): Boolean;
//增加服务描述
procedure ABSetServiceDescription(aServiceName,aDescription:string;
                            aHostName:string='');


//DOS方式安装服务(直接使用EXE文件中的服务设置，如显示名，描述等)
function ABInstallService_Dos  (aServiceName, aFileName: string;
                                aStart: Boolean = false; aShowType: Integer = SW_HIDE;
                                aShowMessage: Boolean=false): boolean;
//得到局域网中的SQL SERVER列表
procedure ABGetSqlServerList(aStrings: TStrings;aDelOld:Boolean=true);




implementation

function ABServiceIsRunning(aServiceName: string;aHostName:string): Boolean;
var
  SCManager: THandle;
  HService: THandle;
  ServiceStatus: TServiceStatus;
begin
  Result := false;
  SCManager := OpenSCManager(PChar(aHostName), nil, SC_MANAGER_ALL_ACCESS);
  try
    if SCManager <> 0 then
    begin
      HService := OpenService(SCManager, PChar(aServiceName), GENERIC_READ);
      try
        if HService <> 0 then
        begin
          if ControlService(HService, SERVICE_CONTROL_INTERROGATE, ServiceStatus) then
          begin
            if ServiceStatus.dwCurrentState = SERVICE_RUNNING then
            begin
              Result := true;
            end;
          end;
        end;
      finally
        CloseServiceHandle(HService);
      end;
    end;
  finally
    CloseServiceHandle(SCManager);
  end;
end;

function ABServiceIsInstall(aServiceName: string;aHostName:string): Boolean;
var
  SCManager: THandle;
  HService: THandle;
begin
  Result := false;
  SCManager := OpenSCManager(PChar(aHostName), nil, SC_MANAGER_ALL_ACCESS);
  try
    if SCManager <> 0 then
    begin
      HService := OpenService(SCManager, PChar(aServiceName), GENERIC_READ);
      try
        if HService <> 0 then
        begin
          Result := true;
        end;
      finally
        CloseServiceHandle(HService);
      end;
    end;
  finally
    CloseServiceHandle(SCManager);
  end;
end;

function ABGetServiceStatus(aServiceName: string;aHostName:string): TABServiceState;
var
  hService, hSCManager: SC_HANDLE;
  SS: TServiceStatus;
begin
  hSCManager := OpenSCManager(PChar(aHostName), SERVICES_ACTIVE_DATABASE, SC_MANAGER_CONNECT);
  if hSCManager = 0 then
  begin
    result := stNotOpenServiceControl;
  end
  else
  begin
    try
      hService := OpenService(hSCManager, PChar(aServiceName), SERVICE_QUERY_STATUS);
      if hService = 0 then
      begin
        result := stNotOpenService;
      end
      else
      begin
        try
          if not QueryServiceStatus(hService, SS) then
            result := stGetStateError
          else
          begin
            case SS.dwCurrentState of
              SERVICE_CONTINUE_PENDING:result := stContinuePending;
              SERVICE_PAUSE_PENDING:result := stPausePending;
              SERVICE_START_PENDING:result := stStartPending;
              SERVICE_STOP_PENDING:result := stStopPending;

              SERVICE_RUNNING:result := stRunning;
              SERVICE_PAUSED :result := stPaused;
              SERVICE_STOPPED:result := stStopped;
            else
              result := stNull;
            end;
          end;
        finally
          CloseServiceHandle(hService);
        end;
      end;
    finally
      CloseServiceHandle(hSCManager);
    end;
  end;
end;

function ABStartService    (aServiceName: string;
                            aHostName:string): Boolean;
var
  SCManager, hService: SC_HANDLE;
  lpServiceArgVectors: PChar;
begin
  Result :=false ;
  SCManager := OpenSCManager(PChar(aHostName), nil, SC_MANAGER_ALL_ACCESS);
  if SCManager =0 then
    exit;

  hService := OpenService(SCManager, PChar(aServiceName), SERVICE_ALL_ACCESS);
  try
    if (hService = 0) and (GetLastError = ERROR_SERVICE_DOES_NOT_EXIST) then
      Exception.Create('The specified service does not exist');

    if hService <> 0 then
    begin
      try
        lpServiceArgVectors := nil;
        Result := WinSvc.StartService(hService, 0, lpServiceArgVectors);

        if not Result and (GetLastError = ERROR_SERVICE_ALREADY_RUNNING) then
          Result := True;
      finally
        CloseServiceHandle(hService);
      end;
    end;
  finally
    CloseServiceHandle(SCManager);
  end;
end;

function ABStopService     (aServiceName: string;
                            aHostName:string): Boolean;
var
  schService:SC_HANDLE;
  schSCManager:SC_HANDLE;
  ssStatus:TServiceStatus;
begin
  schSCManager:=OpenSCManager(PChar(aHostName),nil,SC_MANAGER_ALL_ACCESS);
  schService:=OpenService(schSCManager,PChar(aServiceName),SERVICE_ALL_ACCESS);
  try
    if ControlService(schService,SERVICE_CONTROL_STOP,ssStatus) then
    begin
      Sleep(100);
      while (QueryServiceStatus(schService,ssStatus)) do
      begin
        if ssStatus.dwCurrentState=SERVICE_STOP_PENDING then
          Sleep(100)
        else
          break;
      end; //while
      if ssStatus.dwCurrentState=SERVICE_STOPPED then
        result := True
      else
        result := False;
    end
    else
      result := False;
  finally
    CloseServiceHandle(schService);
    CloseServiceHandle(schSCManager);
  end;
end;

procedure ABSetServiceDescription(aServiceName,aDescription:string;aHostName:string);
var
  reg: TRegistry;
begin
  reg := TRegistry.Create;
  try
    reg.RootKey := HKEY_LOCAL_MACHINE;
    if reg.OpenKey('SYSTEM\CurrentControlSet\Services\'+aServiceName,false) then
    begin
      reg.WriteString('Description',aDescription);
    end;
    reg.CloseKey;
  finally
    reg.Free;
  end;
end;

function ABInstallService  (aServiceInfo: TABServiceInfo;
                            aStart: Boolean ;
                            aHostName:string
                            ): boolean;
var
  Args: pchar;
begin
  Result := False;
  aServiceInfo.HscManager := OpenSCManager(PChar(aHostName), nil, SC_MANAGER_ALL_ACCESS);
  if aServiceInfo.HscManager = 0 then
    Exit;

  if aServiceInfo.DesireAccess=0 then
    aServiceInfo.DesireAccess:=SERVICE_ALL_ACCESS;
  if aServiceInfo.ServiceType=0 then
    aServiceInfo.ServiceType:=SERVICE_WIN32_OWN_PROCESS;
  if aServiceInfo.StartType=0 then
    aServiceInfo.StartType:=SERVICE_AUTO_START;
  if aServiceInfo.ErrorControl=0 then
    aServiceInfo.ErrorControl:=SERVICE_ERROR_NORMAL;
  try
    //创建服务函数
    aServiceInfo.Service:=CreateService(
        aServiceInfo.HscManager,      // 服务控制管理句柄
        aServiceInfo.aServiceName,    // 服务名称
        aServiceInfo.aDisplayName,    // 显示的服务名称

        aServiceInfo.DesireAccess,    // 服务访问类型,存取权利
        aServiceInfo.ServiceType,     // 服务类型 SERVICE_WIN32_SHARE_PROCESS
        aServiceInfo.StartType,       // 启动类型
        aServiceInfo.ErrorControl,    // 错误控制类型
        aServiceInfo.BinaryPathName,  // 启动的文件名
        aServiceInfo.LoadOrderGroup,  // 组服务名称 'LocalSystem'
        aServiceInfo.TagId,           // 组标识
        aServiceInfo.Dependencies,    // 依赖的服务
        aServiceInfo.ServerStartName, // 启动服务帐号
        aServiceInfo.Password
        ); // 启动服务口令
    try
      Args := nil;
      if aStart then
        StartService(aServiceInfo.Service, 0, Args);

      Result := True;
    finally
      CloseServiceHandle(aServiceInfo.Service);
    end;
  finally
    CloseServiceHandle(aServiceInfo.HscManager);
  end;
end;

function ABUninstallService(aServiceName: string;
                            aHostName:string): Boolean;
var
  SCManager, Service: THandle;
  ServiceStatus: SERVICE_STATUS;
begin
  result:=false;
  SCManager := OpenSCManager(PChar(aHostName), nil, SC_MANAGER_ALL_ACCESS);
  if SCManager = 0 then
    Exit;

  Service := OpenService(SCManager, PChar(aServiceName), SERVICE_ALL_ACCESS);
  try
    ControlService(Service, SERVICE_CONTROL_STOP, ServiceStatus);
    DeleteService(Service);
    result:=true;
  finally
    CloseServiceHandle(Service);
    CloseServiceHandle(SCManager);
  end;
end;

type
  NET_API_STATUS   =   DWORD;
  PServerInfo100   =   ^TServerInfo100;
  _SERVER_INFO_100   =   record
      sv100_platform_id:   DWORD;
      sv100_name:   LPWSTR;
  end;
  {$EXTERNALSYM   _SERVER_INFO_100}
  TServerInfo100   =   _SERVER_INFO_100;
  SERVER_INFO_100   =   _SERVER_INFO_100;
  {$EXTERNALSYM   SERVER_INFO_100}

const
  NERR_Success   =   0;
  MAX_PREFERRED_LENGTH   =   DWORD(-1);
  SV_TYPE_SQLSERVER         =   $00000004;

function   NetApiBufferAllocate(ByteCount:   DWORD;   var   Buffer:   Pointer):
  NET_API_STATUS;   stdcall;   external   'netapi32.dll'   name   'NetApiBufferAllocate';

function   NetServerEnum(ServerName:   LPCWSTR;   Level:   DWORD;   var   BufPtr:   Pointer;
  PrefMaxLen:   DWORD;   var   EntriesRead:   DWORD;   var   TotalEntries:   DWORD;
  ServerType:   DWORD;   Domain:   LPCWSTR;   ResumeHandle:   PDWORD):   NET_API_STATUS;
  stdcall;   external   'netapi32.dll'   name   'NetServerEnum';

function   NetApiBufferFree(Buffer:   Pointer):   NET_API_STATUS;   stdcall;   external
'netapi32.dll'   name   'NetApiBufferFree';

procedure ABGetSqlServerList(aStrings: TStrings;aDelOld:Boolean);
  function   GetSQLServerList(out   AList:   TStrings;   pwcServerName:   PWChar   =   nil;
    pwcDomain:   PWChar   =   nil):   Boolean;
  var
    NetAPIStatus:   DWORD;
    dwLevel:   DWORD;
    pReturnSvrInfo:   Pointer;
    dwPrefMaxLen:   DWORD;
    dwEntriesRead:   DWORD;
    dwTotalEntries:   DWORD;
    dwServerType:   DWORD;
    dwResumeHandle:   PDWORD;
    pCurSvrInfo:   PServerInfo100;
    i,   j:   Integer;
  begin
    Result   :=   True;
    try
      if     Trim(pwcServerName)   =   ''   then
          pwcServerName   :=   nil;

      if   Trim(pwcDomain)   =   ''   then
          pwcDomain   :=   nil;

      dwLevel   :=   100;
      pReturnSvrInfo   :=   nil;
      dwPrefMaxLen   :=   MAX_PREFERRED_LENGTH;
      dwEntriesRead   :=   0;
      dwTotalEntries   :=   0;
      dwServerType   :=   SV_TYPE_SQLSERVER;
      dwResumeHandle   :=   nil;

      NetApiBufferAllocate(SizeOf(pReturnSvrInfo),   pReturnSvrInfo);
      try
        NetAPIStatus   :=   NetServerEnum(pwcServerName,   dwLevel,   pReturnSvrInfo,
            dwPrefMaxLen,   dwEntriesRead,   dwTotalEntries,   dwServerType,   pwcDomain,
            dwResumeHandle);

        if   (NetAPIStatus   =   NERR_Success)   and   (pReturnSvrInfo   <>   nil)   then
        begin
          pCurSvrInfo   :=   pReturnSvrInfo;

          //   循环取得所有服务
          i   :=   0;
          j   :=   dwEntriesRead;
          while   i   <   j   do
          begin
            if   pCurSvrInfo   =   nil   then
                Break;

            if AList.IndexOf(pCurSvrInfo^.sv100_name)<=0 then
              AList.Add(pCurSvrInfo^.sv100_name);

            Inc(i);
            Inc(pCurSvrInfo);
          end;
        end;
      finally
        if   Assigned(pReturnSvrInfo)   then
            NetApiBufferFree(pReturnSvrInfo);
      end;
    except
      Result   :=   False;
    end;
  end;
begin
  if Assigned(aStrings) then
  begin
    if aDelOld then
      aStrings.Clear;

    GetSQLServerList(aStrings);
  end;
end;

function ABInstallService_Dos  (aServiceName, aFileName: string;
                                aStart: Boolean ; aShowType: Integer; aShowMessage: Boolean): boolean;
  function ABWinExecAndWait32(FileName: string; aShowType: Integer): Integer;
  var
    zAppName: array[0..512] of Char;
    zCurDir: array[0..255] of Char;
    WorkDir: string;
    StartupInfo: TStartupInfo;
    ProcessInfo: TProcessInformation;
  begin
    StrPCopy(zAppName, FileName);
    GetDir(0, WorkDir);
    StrPCopy(zCurDir, WorkDir);
    FillChar(StartupInfo, SizeOf(StartupInfo), #0);
    StartupInfo.cb := SizeOf(StartupInfo);

    StartupInfo.dwFlags := STARTF_USESHOWWINDOW;
    StartupInfo.wShowWindow := aShowType;
    if not CreateProcess(nil,
      zAppName, //pointer to command line String
      nil, //pointer to process security attributes
      nil, //pointer to thread security attributes
      False, //handle inheritance flag
      CREATE_NEW_CONSOLE or
      NORMAL_PRIORITY_CLASS,
      nil, // pointer to new environment block
      nil, // pointer to current directory name
      StartupInfo, // pointer to STARTUPINFO
      ProcessInfo) then
      Result := -1 // pointer to PROCESS_INF
    else
    begin
      WaitforSingleObject(ProcessInfo.hProcess, INFINITE);
      GetExitCodeProcess(ProcessInfo.hProcess, Cardinal(Result));
    end;
  end;

var
  tempAddStr:string;
begin
  if not aShowMessage then
    tempAddStr:=' /silent'
  else
    tempAddStr:=EmptyStr;

  ABWinExecAndWait32(aFileName + ' /install'+tempAddStr, aShowType);
  if aStart then
    ABWinExecAndWait32('net start ' + aServiceName, aShowType);

  result:=true;
end;



end.

