{
Indy组件增强函数单元
}
unit ABPubIndyU;

interface
{$I ..\ABInclude.ini}
uses
  ABPubVarU,
  ABPubFuncU,

  IdFTP,IdHTTP,

  SysUtils,Variants;

//根据FTP的地址，连接TIdFTP
//aFtpAddress 格式必须是类似ftp://rec:ooo@192.168.76.11/20050418/abcdef.vox,
function ABSetIDFtpConn(aFtpAddress: string;aFtp:TIdFTP):boolean;
//到外网上跑一下，返回外网的IP
function ABGetWANIP(aGetIPAspFileName:string;aHTTP:TIdHTTP): string;
//通过FTP更新本地文件到FTP服务器器当前目录下
function ABUpToFtp(aLocalFileName,aFTPFileName: string;aFTP: TIdFTP): boolean;
//通过FTP下载FTP服务器器当前目录下文件到本地
function ABDownFromFTP(aFTPFileName, aFTPVersion: string;aLocalSubPath,aLocalFileName: string;aFTP: TIdFTP): boolean;


implementation

function ABUpToFtp(aLocalFileName,aFTPFileName: string;aFTP: TIdFTP): boolean;
begin
  result := false;
  if (Assigned(aFTP)) and (aFTP.Connected) then
  begin
    ABDoBusy;
    try
      aFTP.Put(aLocalFileName,aFTPFileName);
      result := true;
    finally
      ABDoBusy(false);
    end;
  end;
end;

function ABDownFromFTP(aFTPFileName, aFTPVersion: string;aLocalSubPath,aLocalFileName: string;aFTP: TIdFTP): boolean;
begin
  result := false;
  if (Assigned(aFTP)) and (aFTP.Connected) then
  begin
    ABDoBusy;
    try
      ABMoveFile(ABAppPath+aLocalFileName,ABBackPath+aLocalFileName);

      if aLocalSubPath<>EmptyStr then
      begin
        ABCreateDir(ABAppPath+ABGetpath(aLocalSubPath));
      end;
      aFTP.Get(aFTPFileName,ABAppPath+ABGetpath(aLocalSubPath) + aLocalFileName,true);

      if Trim(aFTPVersion)=EmptyStr then
        aFTPVersion:='0';
      ABWriteInI('Version',aLocalFileName,ABIIF(trim(aFTPVersion)=EmptyStr,'0',aFTPVersion),ABLocalVersionsFile);
      result := true;
    finally
      ABDoBusy(false);
    end;
  end;
end;

function ABGetWANIP(aGetIPAspFileName:string;aHTTP:TIdHTTP): string;
begin
  if aGetIPAspFileName=emptystr then
    aGetIPAspFileName:='http://www.aibosoft.cn/getip.asp';     //'http://www.ipseeker.cn'

  //asp文件只取出外网的IP
  if AnsiCompareText(aGetIPAspFileName,'http://www.aibosoft.cn/getip.asp')=0 then
  begin
    result:=aHTTP.Get(aGetIPAspFileName);
  end
  //www.ipseeker.cn取出的外网的IP包含在"查询结果"与"</span>"之间
  else if AnsiCompareText(aGetIPAspFileName,'http://ipseeker.cn/')=0 then
  begin
    result:=trim(ABGetItemValue(aHTTP.Get(aGetIPAspFileName),'查询结果：','','&nbsp'));
  end
  else
  begin
    result:=aHTTP.Get(aGetIPAspFileName);
  end;
end;

function ABSetIDFtpConn(aFtpAddress: string;aFtp:TIdFTP):boolean;
var
  tempUserName,tempPassWord,tempHost,tempPort,tempDir:string;
begin
  if not aFTP.Passive then
    aFTP.Passive:=true;
  try
    ABGetFtpInfo(aFtpAddress, tempUserName, tempPassWord, tempHost, tempPort,tempDir);
    if aFtp.Connected then
      aFtp.Disconnect;
    if tempUserName <> emptystr then
    begin
      aFtp.Username := tempUserName;
    end;
    if tempPassWord <> emptystr then
    begin
      aFtp.Password := tempPassWord;
    end;
    if tempHost <> emptystr then
    begin
      aFtp.Host := tempHost;
    end;
    if tempPort <> emptystr then
    begin
      aFtp.Port := StrToIntDef(tempPort,21);
    end;
    aFtp.Connect;
    if tempDir <> emptystr then
    begin
      aFtp.ChangeDir(tempDir);
    end;
    Result:=true;
  except
    raise;
  end;
end;


end.
