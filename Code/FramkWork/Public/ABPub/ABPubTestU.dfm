object ABTestForm: TABTestForm
  Left = 376
  Top = 135
  Caption = #20013#21326#20849#21644#22269
  ClientHeight = 491
  ClientWidth = 1137
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl3: TPageControl
    Left = 185
    Top = 0
    Width = 952
    Height = 491
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #21333#20803
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel3: TPanel
        Left = 486
        Top = 0
        Width = 162
        Height = 463
        Align = alLeft
        TabOrder = 0
        object GroupBox1: TGroupBox
          Left = 1
          Top = 38
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubAutoProgressBar_ThreadU'
          TabOrder = 0
          object Button3: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = '5'#31186#32447#31243#24490#29615#36827#24230#26465
            TabOrder = 0
            OnClick = Button3Click
          end
        end
        object GroupBox2: TGroupBox
          Left = 1
          Top = 75
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubManualProgressBar_ThreadU'
          TabOrder = 1
          object Button4: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #32447#31243#21487#25511#36827#24230#26465
            TabOrder = 0
            OnClick = Button4Click
          end
        end
        object GroupBox4: TGroupBox
          Left = 1
          Top = 176
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubStartFormU'
          TabOrder = 2
          object Button16: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = '2'#31186#27426#36814#30028#38754
            TabOrder = 0
            OnClick = Button16Click
          end
        end
        object GroupBox5: TGroupBox
          Left = 1
          Top = 213
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubCharacterCodingU'
          TabOrder = 3
          object Button48: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #23383#31526#32534#30721#36716#25442
            TabOrder = 0
            OnClick = Button48Click
          end
        end
        object GroupBox6: TGroupBox
          Left = 1
          Top = 250
          Width = 160
          Height = 129
          Align = alTop
          Caption = 'ABPubPassU'
          TabOrder = 4
          object Label3: TLabel
            Left = 11
            Top = 17
            Width = 24
            Height = 13
            AutoSize = False
            Caption = #26126#25991
          end
          object Label4: TLabel
            Left = 11
            Top = 37
            Width = 24
            Height = 13
            AutoSize = False
            Caption = #23494#25991
          end
          object Button12: TButton
            Left = 5
            Top = 79
            Width = 76
            Height = 25
            Caption = #26816#27979#21152#23494#29399
            TabOrder = 0
            OnClick = Button12Click
          end
          object Edit1: TEdit
            Tag = 91
            Left = 41
            Top = 14
            Width = 116
            Height = 21
            TabOrder = 1
            Text = 'MasterKey'
          end
          object Edit2: TEdit
            Tag = 91
            Left = 41
            Top = 34
            Width = 116
            Height = 21
            TabOrder = 2
            Text = '000258000280000316000346000323000351000313000340000361000115'
          end
          object Button13: TButton
            Left = 5
            Top = 55
            Width = 38
            Height = 25
            Caption = #26126'>'#23494
            TabOrder = 3
            OnClick = Button13Click
          end
          object btn1: TButton
            Left = 43
            Top = 55
            Width = 38
            Height = 25
            Caption = #23494'>'#26126
            TabOrder = 4
            OnClick = btn1Click
          end
          object Button1: TButton
            Left = 80
            Top = 55
            Width = 79
            Height = 25
            Caption = #26126'>'#27880#20876#30721
            TabOrder = 5
            OnClick = Button1Click
          end
          object Button2: TButton
            Left = 80
            Top = 79
            Width = 79
            Height = 25
            Caption = #26816#27979#25480#26435#25991#20214
            TabOrder = 6
            OnClick = Button2Click
          end
          object Button14: TButton
            Left = 5
            Top = 103
            Width = 154
            Height = 25
            Caption = #26816#27979#23494#25991#30001#26126#25991'+'#20844#38053#20135#29983
            TabOrder = 7
            OnClick = Button14Click
          end
        end
        object GroupBox7: TGroupBox
          Left = 1
          Top = 379
          Width = 160
          Height = 62
          Align = alTop
          Caption = 'ABPubPakandZipU'
          TabOrder = 5
          object Button46: TButton
            Left = 5
            Top = 12
            Width = 154
            Height = 25
            Caption = #21387#32553#19982#35299#21387
            TabOrder = 0
          end
          object Button47: TButton
            Left = 5
            Top = 36
            Width = 154
            Height = 25
            Caption = #25171#21253#19982#35299#21253
            TabOrder = 1
            OnClick = Button47Click
          end
        end
        object GroupBox3: TGroupBox
          Left = 1
          Top = 112
          Width = 160
          Height = 64
          Align = alTop
          Caption = 'ABPubManualProgressBarU'
          TabOrder = 6
          object Button7: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #20027#36827#31243#20869#21487#25511#21333#26465#36827#24230#26465
            TabOrder = 0
            OnClick = Button7Click
          end
          object Button11: TButton
            Left = 3
            Top = 36
            Width = 154
            Height = 25
            Caption = #20027#36827#31243#20869#21487#25511#20027#20174#26465#36827#24230#26465
            TabOrder = 1
            OnClick = Button11Click
          end
        end
        object GroupBox13: TGroupBox
          Left = 1
          Top = 1
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubScriptU'
          TabOrder = 7
          object Button36: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #33050#26412#35299#37322
            TabOrder = 0
            OnClick = Button36Click
          end
        end
      end
      object Panel4: TPanel
        Left = 162
        Top = 0
        Width = 162
        Height = 463
        Align = alLeft
        TabOrder = 1
        object GroupBox8: TGroupBox
          Left = 1
          Top = 223
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubImageFieldU'
          TabOrder = 0
          object Button18: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #26174#31034#19982#32534#36753#29031#29255#23383#27573
            TabOrder = 0
            OnClick = Button18Click
          end
        end
        object GroupBox11: TGroupBox
          Left = 1
          Top = 186
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubDesignU'
          TabOrder = 1
          object Button10: TButton
            Left = 4
            Top = 12
            Width = 60
            Height = 25
            Caption = 'OfOwner'
            TabOrder = 0
            OnClick = Button10Click
          end
          object Button22: TButton
            Left = 122
            Top = 12
            Width = 33
            Height = 25
            Caption = #32467#26463
            TabOrder = 1
            OnClick = Button22Click
          end
          object Button63: TButton
            Left = 63
            Top = 12
            Width = 60
            Height = 25
            Caption = 'OfParent'
            TabOrder = 2
            OnClick = Button63Click
          end
        end
        object GroupBox12: TGroupBox
          Left = 1
          Top = 149
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubSQLParseU'
          TabOrder = 2
          object Button43: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'SQL'#35299#37322
            TabOrder = 0
            OnClick = Button43Click
          end
        end
        object GroupBox14: TGroupBox
          Left = 1
          Top = 260
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubImageU'
          TabOrder = 3
          object Button19: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #26174#31034#19982#32534#36753#29031#29255#25511#20214
            TabOrder = 0
            OnClick = Button19Click
          end
        end
        object GroupBox15: TGroupBox
          Left = 1
          Top = 297
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubMemoFieldU'
          TabOrder = 4
          object Button21: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #26174#31034#19982#32534#36753#22791#27880#23383#27573
            TabOrder = 0
            OnClick = Button21Click
          end
        end
        object GroupBox16: TGroupBox
          Left = 1
          Top = 334
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubMemoU'
          TabOrder = 5
          object Button20: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #26174#31034#19982#32534#36753#22791#27880#25511#20214
            TabOrder = 0
            OnClick = Button20Click
          end
        end
        object GroupBox17: TGroupBox
          Left = 1
          Top = 371
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubOrderStrU'
          TabOrder = 6
          object Button30: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #23383#20018#25490#24207
            TabOrder = 0
            OnClick = Button30Click
          end
        end
        object GroupBox18: TGroupBox
          Left = 1
          Top = 408
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubEditPassWordU'
          TabOrder = 7
          object Button31: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #20462#25913#23494#30721
            TabOrder = 0
            OnClick = Button31Click
          end
        end
        object GroupBox27: TGroupBox
          Left = 1
          Top = 38
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubInPutPassWordU'
          TabOrder = 8
          object Button35: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #36755#20837#23494#30721
            TabOrder = 0
            OnClick = Button35Click
          end
        end
        object GroupBox26: TGroupBox
          Left = 1
          Top = 75
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubMessageU'
          TabOrder = 9
          object Button37: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #26174#31034#20449#24687#26694
            TabOrder = 0
            OnClick = Button37Click
          end
        end
        object GroupBox25: TGroupBox
          Left = 1
          Top = 112
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubLoginU'
          TabOrder = 10
          object Button40: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #26174#31034#30331#24405#31383#20307
            TabOrder = 0
            OnClick = Button40Click
          end
        end
        object GroupBox24: TGroupBox
          Left = 1
          Top = 1
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubLoginU'
          TabOrder = 11
          object Button38: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #25554#20837#26085#24535
            TabOrder = 0
            OnClick = Button38Click
          end
        end
      end
      object Panel6: TPanel
        Left = 324
        Top = 0
        Width = 162
        Height = 463
        Align = alLeft
        TabOrder = 2
        object GroupBox30: TGroupBox
          Left = 1
          Top = 1
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubIndyU'
          TabOrder = 0
          object Button45: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'Indy'
            TabOrder = 0
            OnClick = Button45Click
          end
        end
        object GroupBox32: TGroupBox
          Left = 1
          Top = 38
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubSockDemoU'
          TabOrder = 1
          object Button32: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'Sock'
            TabOrder = 0
            OnClick = Button32Click
          end
        end
        object GroupBox42: TGroupBox
          Left = 1
          Top = 75
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubDownButtonU'
          TabOrder = 2
          object ABDownButton1: TABDownButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'DownButton'#21491#38190
            DropDownMenu = PopupMenu1
            TabOrder = 0
          end
        end
        object GroupBox47: TGroupBox
          Left = 1
          Top = 112
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubSelectStrU'
          TabOrder = 3
          object Button29: TButton
            Left = 5
            Top = 12
            Width = 154
            Height = 25
            Caption = #36873#25321#23383#20018
            TabOrder = 0
            OnClick = Button29Click
          end
        end
        object GroupBox46: TGroupBox
          Left = 1
          Top = 297
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubSelectCheckComboBoxU'
          TabOrder = 4
          object Button28: TButton
            Left = 5
            Top = 12
            Width = 154
            Height = 25
            Caption = #36873#25321'CheckComboBox'
            TabOrder = 0
            OnClick = Button28Click
          end
        end
        object GroupBox48: TGroupBox
          Left = 1
          Top = 186
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubSelectCheckTreeViewU'
          TabOrder = 5
          object Button26: TButton
            Left = 5
            Top = 12
            Width = 154
            Height = 25
            Caption = #36873#25321'CheckTreeView'#32467#28857
            TabOrder = 0
            OnClick = Button26Click
          end
        end
        object GroupBox49: TGroupBox
          Left = 1
          Top = 260
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubSelectTwoStringsU'
          TabOrder = 6
          object Button53: TButton
            Left = 5
            Top = 12
            Width = 154
            Height = 25
            Caption = #36873#25321#21452'Combobox'#23545#24212#20851#31995
            TabOrder = 0
            OnClick = Button53Click
          end
        end
        object GroupBox50: TGroupBox
          Left = 1
          Top = 223
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubSelectComboBoxU'
          TabOrder = 7
          object Button27: TButton
            Left = 5
            Top = 12
            Width = 154
            Height = 25
            Caption = #36873#25321'ComboBox'
            TabOrder = 0
            OnClick = Button27Click
          end
        end
        object GroupBox51: TGroupBox
          Left = 1
          Top = 149
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubSelectTreeViewU'
          TabOrder = 8
          object Button25: TButton
            Left = 5
            Top = 12
            Width = 154
            Height = 25
            Caption = #36873#25321'TreeView'#32467#28857
            TabOrder = 0
            OnClick = Button25Click
          end
        end
        object GroupBox72: TGroupBox
          Left = 1
          Top = 334
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'GroupBox8'
          TabOrder = 9
          object Button70: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            TabOrder = 0
          end
        end
        object GroupBox59: TGroupBox
          Left = 1
          Top = 371
          Width = 160
          Height = 86
          Align = alTop
          Caption = 'GroupBox8'
          TabOrder = 10
        end
      end
      object Panel9: TPanel
        Left = 648
        Top = 0
        Width = 162
        Height = 463
        Align = alLeft
        TabOrder = 3
        object GroupBox31: TGroupBox
          Left = 1
          Top = 155
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubFuncU'
          TabOrder = 0
          ExplicitTop = 192
          object Button55: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABPubFuncU'
            TabOrder = 0
            OnClick = Button55Click
          end
        end
        object GroupBox39: TGroupBox
          Left = 1
          Top = 38
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubConstU'
          TabOrder = 1
          ExplicitTop = 75
          object Button23: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABPubConstU'
            TabOrder = 0
            OnClick = Button23Click
          end
        end
        object GroupBox40: TGroupBox
          Left = 1
          Top = 75
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubVarU'
          TabOrder = 2
          ExplicitTop = 112
          object Button24: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABPubVarU'
            TabOrder = 0
            OnClick = Button24Click
          end
        end
        object GroupBox41: TGroupBox
          Left = 1
          Top = 112
          Width = 160
          Height = 43
          Align = alTop
          Caption = 'ABPubDBU'
          TabOrder = 3
          ExplicitTop = 149
          object Button54: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABPubDBU'
            TabOrder = 0
            OnClick = Button54Click
          end
        end
        object GroupBox19: TGroupBox
          Left = 1
          Top = 1
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubInPutStrU'
          TabOrder = 4
          object Button34: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #36755#20837#23383#20018
            TabOrder = 0
            OnClick = Button34Click
          end
        end
        object GroupBox37: TGroupBox
          Left = 1
          Top = 192
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubSQLLogU'
          TabOrder = 5
          ExplicitTop = 229
          object Button56: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABPubSQLLogU'
            TabOrder = 0
            OnClick = Button56Click
          end
        end
        object GroupBox53: TGroupBox
          Left = 1
          Top = 229
          Width = 160
          Height = 69
          Align = alTop
          Caption = 'ABPubDefaultValueU'
          TabOrder = 6
          ExplicitTop = 266
          object Button58: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABPubDefaultValueU'
            TabOrder = 0
            OnClick = Button58Click
          end
          object Edit3: TEdit
            Left = 8
            Top = 40
            Width = 41
            Height = 21
            TabOrder = 1
            Text = 'Edit3'
          end
          object Memo1: TMemo
            Left = 50
            Top = 38
            Width = 105
            Height = 28
            Lines.Strings = (
              'Memo1')
            TabOrder = 2
          end
        end
        object GroupBox61: TGroupBox
          Left = 1
          Top = 298
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubDBPopupMenuU'
          TabOrder = 7
          ExplicitTop = 335
          object Button59: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABPubDBPopupMenuU'
            TabOrder = 0
          end
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 162
        Height = 463
        Align = alLeft
        TabOrder = 4
        object GroupBox9: TGroupBox
          Left = 1
          Top = 112
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubThreadU'
          TabOrder = 0
          object Button39: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #20114#25286#19982#20020#30028#21306
            TabOrder = 0
            OnClick = Button39Click
          end
        end
        object GroupBox20: TGroupBox
          Left = 1
          Top = 149
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubThreadDemoU'
          TabOrder = 1
          object Button44: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #22810#32447#31243#31034#20363
            TabOrder = 0
            OnClick = Button44Click
          end
        end
        object GroupBox21: TGroupBox
          Left = 1
          Top = 75
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubShowEditDatasetU'
          TabOrder = 2
          object Button42: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #26174#31034#25968#25454#38598
            TabOrder = 0
            OnClick = Button42Click
          end
        end
        object GroupBox22: TGroupBox
          Left = 1
          Top = 38
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubShowEditFieldValueU'
          TabOrder = 3
          object Button33: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #20462#25913#25968#25454#38598#23383#27573#20540
            TabOrder = 0
            OnClick = Button33Click
          end
        end
        object GroupBox23: TGroupBox
          Left = 1
          Top = 1
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubUserU'
          TabOrder = 4
          object Button41: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #25805#20316#21592#20449#24687
            TabOrder = 0
            OnClick = Button41Click
          end
        end
        object GroupBox36: TGroupBox
          Left = 1
          Top = 186
          Width = 160
          Height = 62
          Align = alTop
          Caption = 'ABPubLanguageU/ABPubLanguageEditU'
          TabOrder = 5
          object Button17: TButton
            Left = 3
            Top = 36
            Width = 154
            Height = 25
            Caption = #22810#35821#35328#31649#29702
            TabOrder = 0
            OnClick = Button17Click
          end
          object Button8: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Hint = #21253#21547#20027#33756#21333#20013#21019#24314#22810#35821#35328
            Caption = #22810#35821#35328
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = Button8Click
          end
        end
        object GroupBox10: TGroupBox
          Left = 1
          Top = 248
          Width = 160
          Height = 62
          Align = alTop
          Caption = 'ABPubLocalParamsU/ABPubLocalParamsEditU'
          TabOrder = 6
          object Button9: TButton
            Left = 3
            Top = 36
            Width = 154
            Height = 25
            Caption = #26412#22320#21442#25968#31649#29702
            TabOrder = 0
            OnClick = Button9Click
          end
          object Button49: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #35835#20889#26412#22320#21442#25968
            TabOrder = 1
            OnClick = Button49Click
          end
        end
        object GroupBox35: TGroupBox
          Left = 1
          Top = 310
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubRegisterFileU'
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 7
          object Button50: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #27880#20876#25991#20214#31867
            TabOrder = 0
            OnClick = Button50Click
          end
        end
        object GroupBox28: TGroupBox
          Left = 1
          Top = 347
          Width = 160
          Height = 37
          Align = alTop
          Caption = 'ABPubServiceU'
          TabOrder = 8
          object Button51: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = #26381#21153#30456#20851
            TabOrder = 0
            OnClick = Button51Click
          end
        end
      end
      object Button57: TButton
        Left = 832
        Top = 88
        Width = 75
        Height = 25
        Caption = 'Button57'
        TabOrder = 5
        OnClick = Button57Click
      end
    end
    object TabSheet5: TTabSheet
      Caption = #25511#20214
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel28: TPanel
        Left = 0
        Top = 0
        Width = 537
        Height = 463
        Align = alLeft
        TabOrder = 0
        object GroupBox52: TGroupBox
          Left = 1
          Top = 1
          Width = 535
          Height = 145
          Align = alTop
          Caption = #20027#20174#25968#25454#38598
          TabOrder = 0
          object GroupBox45: TGroupBox
            Left = 2
            Top = 15
            Width = 128
            Height = 128
            Align = alLeft
            Caption = #20174#25968#25454#38598
            TabOrder = 0
            object DBGrid1: TDBGrid
              Left = 2
              Top = 15
              Width = 124
              Height = 53
              Align = alTop
              DataSource = DataSource_Main
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
            end
            object DBGrid3: TDBGrid
              Left = 2
              Top = 68
              Width = 124
              Height = 58
              Align = alClient
              DataSource = DataSource_Detail
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
            end
          end
          object GroupBox55: TGroupBox
            Left = 130
            Top = 15
            Width = 403
            Height = 128
            Align = alClient
            Caption = #20027'+'#20174#25968#25454#38598
            TabOrder = 1
            object DBGrid6: TDBGrid
              Left = 2
              Top = 15
              Width = 399
              Height = 111
              Align = alClient
              DataSource = DataSource_MainDetail
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
            end
          end
        end
        object Panel29: TPanel
          Left = 1
          Top = 146
          Width = 535
          Height = 223
          Align = alTop
          TabOrder = 1
          object GroupBox54: TGroupBox
            Left = 1
            Top = 1
            Width = 290
            Height = 221
            Align = alLeft
            Caption = #22270#29255#30456#20851
            TabOrder = 0
            object Panel12: TPanel
              Left = 2
              Top = 15
              Width = 286
              Height = 193
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object TrackBar1: TTrackBar
                Left = 0
                Top = 68
                Width = 286
                Height = 22
                Align = alBottom
                Max = 255
                Position = 128
                TabOrder = 0
                OnChange = TrackBar1Change
              end
              object Panel16: TPanel
                Left = 0
                Top = 0
                Width = 286
                Height = 69
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                object Panel15: TPanel
                  Left = 0
                  Top = 0
                  Width = 49
                  Height = 69
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Label11: TLabel
                    Left = 0
                    Top = 0
                    Width = 20
                    Height = 13
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'bmp'
                  end
                  object Image_begin: TImage
                    Left = 0
                    Top = 13
                    Width = 49
                    Height = 56
                    Align = alClient
                    Picture.Data = {
                      07544269746D617036150000424D361500000000000036000000280000002500
                      000030000000010018000000000000150000130B0000130B0000000000000000
                      0000FFFFFFFFFFFFFFFFFFFFFFFFF7F4F3EBE6E8E8E3E4E8E3E5E8E3E5E9E4E4
                      E9E4E2E9E4E2E9E3E3E9E4E2E9E4E3E9E3E3E9E3E3E9E3E3E8E4E3E9E4E4E9E4
                      E4E8E3E3E9E3E3E9E4E4E8E3E3E8E3E3E9E4E4E9E4E4E9E4E3E7E3E3E8E3E4E8
                      E3E4E8E4E3E9E4E5F2EFEFFDFCFCFFFFFF00FFFFFFFFFFFFFEFEFEEDEAEAD4D3
                      D7D3D1D5D3D1D5D3D0D5D3D1D5D3D1D5D2D0D5D3D0D5D2D0D5D3D1D4D3D0D4D2
                      CFD3D2D0D4D2D0D4D2D0D3D3D0D3D3D0D3D3D0D2D3CFD2D2CFD3D1CED3D2CED3
                      D2CED3D2CED2D1CED1D3CED2D4CFD3DED7D7E0D7D9DFD9D9E0D9D9E8E1E1FBFB
                      FA00FFFFFFE9F1F89CBFDA5E92BE528BBC538CBC528BBA5189B95088B85086B6
                      5185B45084B25083B05082AF5080AD4F7EAC4F7DAB4F7CA94E7AA84E7AA64E78
                      A44E76A24E75A14E749F4E729E4E719D4E709B4D6F994D6D974D6C97506B945C
                      7294969DB0D5D0D3E0D8D9E0D8DAECE7E700E4EFF65797C63483C0439CD941A5
                      E841A8ED40A8ED3EA6ED3DA5EC3BA3EB39A2EB37A1EA369FEA359FE9339DE931
                      9CE92F9BE82E99E72D98E72A97E72895E62794E62593E52392E52291E42290E4
                      228FE3228FE2218EE2208FE1228CDF227CC624548B687997D6D0D3E0D8DAE5DF
                      E00089B8DB398DCA60C1F751BCFF4DBBFE4BB9FE49B8FE47B7FF44B5FF42B5FE
                      40B4FE3FB2FE3DB1FF3AB0FE38AFFE36AEFE34ACFF31ABFE2FAAFE2EA9FE2BA7
                      FE2AA6FE27A5FF24A4FF22A3FE21A1FE21A1FE21A1FE20A1FD20A1FE20A2FE21
                      A1FF219EF72551889EA3B5E0D9D8E4DEDE0068A4D149A0D75DC4FD53BCFE51BC
                      FE4DBAFF4BBAFE4AB8FE48B7FF45B6FF43B5FE40B4FE3FB3FF3DB1FE3BB0FE38
                      AFFE36AEFF34ACFF31ABFE30AAFE2EA9FE2CA8FE2AA6FE28A6FE26A4FF23A3FF
                      22A2FF21A1FE20A1FE20A1FE20A1FE21A2FE20A2FE256DAE8E97ADE1D9D9E4DE
                      DE0066A4D14BA3DA69CAFE56BFFF51BDFE4FBCFE4DBBFF4CBAFF4AB9FF48B7FF
                      46B6FF43B5FE42B4FF3FB3FF3DB2FF3BB0FF39AFFE37AEFF34ACFF32ACFE30AA
                      FE2EA9FF2DA8FF2AA6FE28A6FE26A4FF24A3FF21A2FE20A2FD20A2FE20A2FE21
                      A1FE20A1FE2470B28D98ADE1D9D9E4DEDE0065A5D44CA5DB78D2FF5FC4FF56BF
                      FF52BEFE4FBCFE4EBBFE4CBAFF4AB8FE48B7FE46B6FF44B5FF41B4FE3FB3FE3D
                      B2FF3BB1FF39AFFE37AEFF35ADFF32ACFE30AAFE2EA9FE2DA8FE2AA7FE29A6FE
                      26A4FF24A3FE22A2FE20A2FE21A1FE21A1FE20A1FD2471B38E98AEE1D8D9E5DE
                      DE0065A6D54EA7DD7CD4FE75D2FF5DC3FD55BFFE52BEFE50BDFE4FBBFF4CBAFE
                      4AB9FE48B8FE46B6FE44B5FF41B4FE40B3FE3EB2FE3CB1FF3AB0FE38AFFE36AD
                      FF33ACFF31ABFE2FAAFE2DA9FE2BA7FF29A6FE26A5FF24A4FF22A3FE21A1FE21
                      A1FE1FA2FE2673B68D99AFE0D8D9E3DEDE0065A7D54DA8DD7FD6FE7CD4FF74D0
                      FF61C6FE57C0FE53BEFF52BDFE4FBCFE4CBBFE4BB9FE49B8FE46B7FE45B5FE42
                      B4FF40B3FE3FB3FF3CB1FE3AB0FE39AEFE36AEFF34ACFE31ABFE2FAAFE2EA9FF
                      2CA7FF2AA6FE27A5FF25A4FE23A3FE22A2FE20A1FE2573B68D9AB0E1D8D9E5DE
                      DE0066A8D84FA8DF7FD6FE7FD6FF7BD4FE76D1FE6CCBFE5DC3FF55BFFF51BDFE
                      4EBCFE4DBBFE4BB9FF48B8FF48B7FE46B6FF43B5FE41B4FE3FB3FE3DB2FF3BB0
                      FE38AFFF36AEFF34ACFF32ABFE30AAFE2EA9FF2CA8FF2AA6FE27A5FE25A4FF23
                      A3FF22A2FE2674B88D9BB1E1D9D9E5DDDF0065AADA50AAE082D8FE7FD7FE7DD5
                      FF7BD4FF78D2FF75D0FE6DCBFF61C6FE58C1FE53BDFE4FBBFF4CBAFE49B8FE47
                      B7FF45B6FF43B4FE40B3FE3FB2FE3DB2FF3BB0FE39AFFE37AEFF34ADFF32ABFF
                      31AAFE2FAAFF2DA8FE2AA7FE28A6FE26A5FF24A4FF2775B98E9CB2E1D9D8E4DD
                      DF0065ABDB51ACE184D9FF82D7FE7FD6FE7CD5FF7AD3FE79D2FF76D1FF73CFFE
                      71CEFE6ECDFE6ACBFF63C7FF5FC4FE5BC2FE56C0FE51BDFE4CBAFE48B8FE42B4
                      FE3EB2FF3CB0FF39B0FE36AFFF34ADFF34ABFE31ABFE2FAAFF2DA9FF2BA7FE29
                      A6FF26A5FF2976BA8E9CB3E1D8DAE4DEDE0066ACDD52AEE285DAFF84D9FF82D7
                      FF7FD5FE7CD4FF7AD3FE78D2FF75D0FF73CFFE70CEFE6ECDFF6BCCFF69CAFF67
                      C8FF64C8FF61C6FE5EC4FF5CC2FF58C0FE52BDFE47B7FF3DB2FE3AB0FE37AFFE
                      36ADFF34ACFF31ABFE2FA9FE2DA9FF2BA7FE28A6FF2978BB8E9DB4E0D8DAE4DD
                      DF0066ADDF53AFE487DBFF85DAFF83D8FF81D7FE7ED6FE7CD4FF79D3FF77D2FF
                      75D1FF72CFFE70CEFE6DCCFF6BCCFF69CAFF66C8FF64C7FF61C6FF5EC4FF5CC3
                      FE5BC2FF57C0FF50BCFE42B4FE3BB1FE39AFFE35ADFF34ACFF31ABFE2FAAFE2E
                      A9FE2CA8FF2A7BBD8F9EB5E1D8D9E4DEDF0067AEE054B1E589DDFE88DBFF85D9
                      FF82D8FE7FD7FE7DD5FF7BD4FF79D3FE77D1FF75D0FF73CFFF70CDFF6DCCFE6A
                      CBFF68C9FE67C8FF64C7FF61C5FD5EC4FE5CC2FE5AC1FE58C0FE55BFFE48B8FE
                      3DB1FE38AFFF36AEFF34ACFF32ABFE30AAFF2EA9FE2B7BBD8E9FB5E0D9D9E4DE
                      DE0067AFE256B2E68BDDFE89DCFF87DBFF84D9FE82D8FE80D6FE7DD5FF7BD4FF
                      79D3FE77D1FF75D0FF72CFFF6FCEFE6CCDFE6ACBFF68C9FF67C8FF63C7FE61C6
                      FE5FC4FF5CC3FF5AC1FE58C0FE56BFFE4DBAFE3FB2FE39AFFF36AEFF34ADFF32
                      ABFF30AAFE2C7DBE8E9FB7E0D9D9E4DEDE0066B1E356B3E88DDFFE8BDDFF89DC
                      FF86DAFF84D9FF82D8FE7FD6FE7DD5FF7BD4FF78D2FF77D1FF74D0FF71CFFE6E
                      CDFE6CCCFE69CBFE68C9FF65C8FF63C6FE61C5FF5EC4FF5CC3FE5AC2FE56C0FF
                      54BEFF4EBBFE3FB2FE38AFFE37AFFE35ADFF33ACFE2D7FBF8EA0B7E0D8D9E4DE
                      DF0066B2E456B4E98FDFFE8DDEFF8BDDFF88DCFE86DAFE84D9FF82D7FE7FD6FE
                      7CD5FE7AD4FF78D2FF76D1FF73D0FE70CEFE6ECDFE6CCCFF69CBFF68C9FF65C8
                      FF63C6FF60C5FE5EC3FE5BC2FF59C1FE57C0FF55BEFF4DBAFF3DB2FE39B0FF37
                      AFFF36ADFE2F81C18FA1B8E0D8D9E3DEDE0066B3E556B6EA91E0FE90DFFE8CDE
                      FF8ADDFF87DCFE86DAFF84D9FF81D7FE7FD6FE7CD4FE7AD4FF78D2FF76D1FF73
                      CFFE71CFFE6ECDFF6CCCFF69CAFE67C9FF64C7FF61C6FE60C5FE5EC3FF5BC1FE
                      59C1FE57BFFF54BEFE4CBAFE3DB2FF3BB1FE37AFFE3082C28EA1B9E0D8D9E4DE
                      DE0066B4E656B6EB91E1FE91E0FF8FDFFE8DDEFE8ADDFE87DBFE85DAFF83D9FF
                      80D7FE7ED6FE7CD4FE79D3FE77D2FF75D0FF73CFFE70CEFE6DCDFF6ACCFE68CA
                      FE67C8FF64C7FF62C6FF5FC5FF5DC3FE5BC2FE59C1FE55BFFE54BEFE49B8FE3D
                      B1FF3AB0FE3183C38EA2BAE0D9D8E4DEDE0067B5E756B6EB91E1FE92E0FF91E0
                      FE8EDFFE8CDEFE8ADCFE87DBFE85DAFF83D8FE81D7FE7ED5FE7CD4FF79D3FE77
                      D1FE74D0FF72CFFF70CEFE6DCDFE6ACBFE69CAFF66C8FF64C7FF61C6FE5EC4FE
                      5DC3FF5BC2FF58C1FE55BFFF52BDFE45B6FE3DB1FE3385C58EA3BBE1D9D9E4DE
                      DE0066B4E656B6EC92E0FE92E1FE91E1FE91E0FE8EDEFF8CDEFF89DCFE86DAFF
                      85D9FF83D8FE81D7FF7DD5FF7BD4FE78D3FE77D1FF74D0FF71CFFE6FCEFE6DCC
                      FF6ACBFF68C9FF66C7FF63C7FF61C5FF5FC4FF5CC2FE5AC1FE57C0FF55BFFE4E
                      BBFF40B3FD3486C78EA4BCE1D9D9E4DDDE0066B4E656B7EC92E1FE92E1FF91E1
                      FF91E1FE90E0FE8EDFFE8BDDFF89DCFF86DAFF84D9FE83D8FF80D6FE7DD5FE7A
                      D4FE78D3FE77D1FF74D0FF71CFFE6ECEFE6DCCFF6ACBFF68C9FE65C8FF63C6FF
                      60C5FF5EC4FF5CC2FE5AC1FE59C0FF55BFFF47B8FE3588C88FA4BEDED9D8E6DF
                      E10066B5E656B6EB91E0FE92E0FE91E0FF91E1FE91E0FE90E0FE8DDFFF8BDDFF
                      88DCFE86DAFF84D9FF82D7FF80D6FE7DD5FE7AD4FF78D2FF76D1FF73D0FE71CF
                      FE6ECDFF6BCCFF69CAFE68C9FF65C8FF63C6FF60C5FE5DC4FE5BC2FF5AC1FF58
                      C0FE52BDFF368AC88FA5BEE1D8DAEDE9E9006DB8E752B3EA90E0FE92E1FF91E0
                      FE91E1FE92E1FE91E1FE8FDFFE8EDEFF8BDDFE88DCFE86DAFF84D9FF81D7FE7F
                      D6FE7CD5FF7AD3FF78D2FF75D1FE73D0FE71CFFF6ECDFF6BCCFE69CAFE67C8FF
                      65C7FF62C6FE5FC5FE5DC3FE5CC2FF59C1FF57C0FF327FBB8FA7C0EAE4E6FDFD
                      FD00A5D2F23AA1E174CBF58BDCFC8EDEFD8DDDFD8EDEFE8EDEFE8DDDFD8BDDFD
                      89DBFD86DAFD84D8FD82D7FD80D6FD7ED4FC7BD3FD79D1FD77D0FC75CFFC73CD
                      FC71CDFD6ECCFC6BC9FC68C8FC66C7FC65C5FC63C4FC5FC2FB5DC1FB5BC0FC57
                      BBF746A3DF3072ABC2D1E0FDFDFDFFFFFF00F2F9FD7BBFE9329BDF45A7E74BAF
                      E84BAEE74BAEE84BAEE84BAEE84BACE64BABE44AAAE449A9E348A7E147A5DF46
                      A3DD46A1DC45A0DA449ED9429DD7419BD6419AD54098D33F97D23D95D03E93CF
                      3E91CE3D8FCC3B8EC93A8DC8398BC63380BC2D70AC97B3CEF9FBFCFFFFFFFFFF
                      FF00FFFFFFFFFFFFD9EDF999C4E07EACCC82B1CF82B1CF84B2D072A0BE8EB9D9
                      91BCDB9DC9E8A3CFEEA3CEECA3CEEBA3CDEBA3CCE9A3CBE9A3CAE7A4CAE6A4C9
                      E6A4C9E5A4C9E4A2C7E2A2C7E29BBED95B7D986D8FA97394AF7596AF53738D7A
                      93A9C9CDD3ECE8E9FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFE1E1E2BFBF
                      C0CBCBCBCACACAC9C9C9AAAAA9DAD4D4E0DADAF5F3F3FFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFEFFFFFEFFFFFFFFFFFFFFFFFFFFFEFFFFFEFFFEFFFEFEF1F0F1
                      818181A2A1A2B1B1B1B2B2B3757474A9A5A6E0D8D9ECE8E9FFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFE3E3E4C4C4C4D2D2D2D2D2D2CDCDCDAFADAEDBD4D5
                      E1DADBF5F3F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFEFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFF1F0F1818181A2A2A2B6B6B6B5B6B5757475AA
                      A6A6DFD9D8ECE8E8FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFE5E5E5C7C7
                      C7DADADAD9D9D9D2D1D1B2B1B1DBD5D4E0D9D8F3F0F1FFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F0F0
                      818181A3A3A3BDBDBDB9B9B9747474AAA5A6E0D9D8ECE8E9FFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFE7E7E7CCCCCCE1E1E1E2E2E2D6D6D6B6B5B5DCD5D5
                      E0D8D8EBE7E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF808080A5A5A5C2C2C2BDBDBD747474AA
                      A5A6E0D9D8ECE8E9FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFEAEAEAD0D0
                      D0E7E7E7E9E9E9DDDDDDB5B4B4DBD4D3DFD8D8E3DDDFFEFDFEFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1
                      7B7B7BABABABC7C7C7C0C0C0747474ADA9AADFD9D8EEEAEBFFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFEFEFEFD3D3D3E9EAEAECECECE6E5E5BABABAD3CDCD
                      DFD8D9E1D9D9F4F0F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC797979B6B6B6C8C8C8C0C0C0747474B9
                      B4B5DFD9D8F4F0F1FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFF5F5F5D3D3
                      D3EBEBEBEFEFEFEAEAEAC8C8C8C7C4C4DFD8D8E0D9D8E4DDDEFCFAFAFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFF9E9E9E
                      828282C1C1C1C8C8C8B8B7B7747474BEB9BADFD8D9FCFBFBFFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFFEFEFED5D5D5EAEAEAF2F2F2F0F0F0DCDCDCBFBEBE
                      D5D1D0E1D9D8E0D9D9E5E0E1F9F9F9FFFFFFFFFFFFFFFFFFFDFFFFFDFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFE2E2E26D6D6DA5A5A5C7C7C7C7C8C8A6A6A6797979CA
                      C4C6E5E0E1FFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFE1E1
                      E1E4E4E4F4F4F4F3F3F3ECECECC9C9C9C4C2C2DDD6D7E1D8D9E0D9D8E5E0DFF6
                      F3F3FEFEFEFEFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFF1F1F1929292878787
                      C2C2C2CBCBCBC6C7C7909191848283DBD2D4F2EFF0FFFFFFFFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFFFFFFFEEEEEEDFDFDFF2F2F2F5F5F5F2F2F2E4E4E4
                      C4C4C4C6C4C5DED6D8E0D9D8E0D9D8E1DBDAEAE5E6F5F3F3FAF9F9FBFBFBFBFA
                      FAF8F6F6E5E2E3A5A5A57D7D7DBBBBBBCFCFCFCECECEC0C1C1747575A5A1A2E6
                      E0E2FDFCFDFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFCFC
                      FCE0E0E0ECECECF7F7F7F5F5F5F2F2F2E0E0E0C0C0C0C2C0C1D9D3D3E0D9D8DF
                      D9D8DFD8D8E0D8D8E1D9DAE3DCDDE2DBDCCDC8C9989596848484B9B9B9D2D2D2
                      D2D2D2CECECEA4A4A4717172CEC7C9F5F2F3FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F0F0DFDFDFF4F4F4F8F8F8F6F6F6
                      F3F3F3E7E7E7C9C9C9B8B8B8C0BEBECCC8C8D4CECDD6D0D0D0CACAC3BEBFA7A5
                      A68E8D8EA0A0A0C8C8C8D6D6D6D7D7D7D4D4D4BFBFBF757575A39FA0EFEDEEFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFEFEFEE7E7E7E4E4E4F8F8F8F8F8F8F6F6F6F3F3F3EDEDEDDDDDDDC5C5C5B9
                      B8B9B2B2B2AEADADA9A9A9A7A7A7AFAFAFC7C7C7D5D5D5DCDCDCDBDBDBD9D9D9
                      CBCBCB9191917F7E7FE8E6E6FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8E1E1E1E9E9E9F8F8F8
                      F8F8F8F7F7F7F5F5F5F2F2F2EDEDEDE4E4E4DDDDDDD8D8D8D7D7D7DBDBDBE1E1
                      E1E2E2E2E1E1E1E0E0E0DDDDDDD2D2D2A0A0A0818181D1D0D0FFFEFEFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFF8F8F8DDDDDDE5E5E5F8F8F8F9F9F9F8F8F8F6F6F6F4F4F4F2
                      F2F2EFEFEFEDEDEDEBEBEBE9E9E9E8E8E8E6E6E6E4E4E4E1E1E1D6D6D6A1A1A1
                      898989D2D2D2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9E1E1E1
                      DBDBDBECECECF9F9F9F8F8F8F6F6F6F5F5F5F3F3F3F1F1F1EEEEEEECECECEBEB
                      EBE7E7E7E0E0E0C5C5C59797979C9C9CE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCEBEBEBD8D8D8D9D9D9E8E8E8EEEEEEF1
                      F1F1F4F4F4F2F2F2EFEFEFE8E8E8DCDCDCCACACAACACACA0A0A0BCBCBCF3F3F3
                      FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FEFEFEF7F7F7E9E9E9D4D4D4D0D0D0CDCDCDCDCDCDC8C8C8C3C3C3BCBCBCB4B4
                      B4B0B0B0C8C8C8E7E7E7FDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDF2F2F2E8
                      E8E8E4E4E4DADADADDDDDDE1E1E1EAEAEAFAFAFAFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FF00}
                    Stretch = True
                    ExplicitLeft = 59
                    ExplicitTop = -13
                    ExplicitWidth = 52
                    ExplicitHeight = 54
                  end
                end
                object Panel17: TPanel
                  Left = 49
                  Top = 0
                  Width = 49
                  Height = 69
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Label12: TLabel
                    Left = 0
                    Top = 0
                    Width = 20
                    Height = 13
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'Max'
                  end
                  object Image_Max: TImage
                    Left = 0
                    Top = 13
                    Width = 49
                    Height = 56
                    Align = alClient
                    Stretch = True
                    ExplicitLeft = 6
                    ExplicitTop = 19
                    ExplicitHeight = 48
                  end
                end
                object Panel18: TPanel
                  Left = 98
                  Top = 0
                  Width = 49
                  Height = 69
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 2
                  object Label13: TLabel
                    Left = 0
                    Top = 0
                    Width = 19
                    Height = 13
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'Avg'
                  end
                  object Image_Avg: TImage
                    Left = 0
                    Top = 13
                    Width = 49
                    Height = 56
                    Align = alClient
                    Stretch = True
                    ExplicitLeft = -3
                    ExplicitTop = 7
                    ExplicitWidth = 52
                    ExplicitHeight = 54
                  end
                end
                object Panel19: TPanel
                  Left = 147
                  Top = 0
                  Width = 49
                  Height = 69
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 3
                  object Label14: TLabel
                    Left = 0
                    Top = 0
                    Width = 65
                    Height = 13
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'WeightedAvg'
                  end
                  object Image_WeightedAvg: TImage
                    Left = 0
                    Top = 13
                    Width = 49
                    Height = 56
                    Align = alClient
                    Stretch = True
                    ExplicitLeft = -3
                    ExplicitTop = 7
                    ExplicitWidth = 52
                    ExplicitHeight = 54
                  end
                end
              end
              object RadioGroup2: TRadioGroup
                AlignWithMargins = True
                Left = 3
                Top = 93
                Width = 280
                Height = 29
                Align = alBottom
                BiDiMode = bdLeftToRight
                Columns = 2
                Ctl3D = False
                ItemIndex = 0
                Items.Strings = (
                  #20108#20540#21270
                  #28784#21270)
                ParentBiDiMode = False
                ParentCtl3D = False
                TabOrder = 2
                OnClick = RadioGroup2Click
              end
              object Panel20: TPanel
                Left = 0
                Top = 125
                Width = 286
                Height = 68
                Align = alBottom
                BevelOuter = bvNone
                TabOrder = 3
                object Panel21: TPanel
                  Left = 0
                  Top = 0
                  Width = 49
                  Height = 68
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Label15: TLabel
                    Left = 0
                    Top = 0
                    Width = 20
                    Height = 13
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'bmp'
                  end
                  object Image_bmp: TImage
                    Left = 0
                    Top = 13
                    Width = 49
                    Height = 55
                    Align = alClient
                    Picture.Data = {
                      07544269746D617036150000424D361500000000000036000000280000002500
                      000030000000010018000000000000150000130B0000130B0000000000000000
                      0000FFFFFFFFFFFFFFFFFFFFFFFFF7F4F3EBE6E8E8E3E4E8E3E5E8E3E5E9E4E4
                      E9E4E2E9E4E2E9E3E3E9E4E2E9E4E3E9E3E3E9E3E3E9E3E3E8E4E3E9E4E4E9E4
                      E4E8E3E3E9E3E3E9E4E4E8E3E3E8E3E3E9E4E4E9E4E4E9E4E3E7E3E3E8E3E4E8
                      E3E4E8E4E3E9E4E5F2EFEFFDFCFCFFFFFF00FFFFFFFFFFFFFEFEFEEDEAEAD4D3
                      D7D3D1D5D3D1D5D3D0D5D3D1D5D3D1D5D2D0D5D3D0D5D2D0D5D3D1D4D3D0D4D2
                      CFD3D2D0D4D2D0D4D2D0D3D3D0D3D3D0D3D3D0D2D3CFD2D2CFD3D1CED3D2CED3
                      D2CED3D2CED2D1CED1D3CED2D4CFD3DED7D7E0D7D9DFD9D9E0D9D9E8E1E1FBFB
                      FA00FFFFFFE9F1F89CBFDA5E92BE528BBC538CBC528BBA5189B95088B85086B6
                      5185B45084B25083B05082AF5080AD4F7EAC4F7DAB4F7CA94E7AA84E7AA64E78
                      A44E76A24E75A14E749F4E729E4E719D4E709B4D6F994D6D974D6C97506B945C
                      7294969DB0D5D0D3E0D8D9E0D8DAECE7E700E4EFF65797C63483C0439CD941A5
                      E841A8ED40A8ED3EA6ED3DA5EC3BA3EB39A2EB37A1EA369FEA359FE9339DE931
                      9CE92F9BE82E99E72D98E72A97E72895E62794E62593E52392E52291E42290E4
                      228FE3228FE2218EE2208FE1228CDF227CC624548B687997D6D0D3E0D8DAE5DF
                      E00089B8DB398DCA60C1F751BCFF4DBBFE4BB9FE49B8FE47B7FF44B5FF42B5FE
                      40B4FE3FB2FE3DB1FF3AB0FE38AFFE36AEFE34ACFF31ABFE2FAAFE2EA9FE2BA7
                      FE2AA6FE27A5FF24A4FF22A3FE21A1FE21A1FE21A1FE20A1FD20A1FE20A2FE21
                      A1FF219EF72551889EA3B5E0D9D8E4DEDE0068A4D149A0D75DC4FD53BCFE51BC
                      FE4DBAFF4BBAFE4AB8FE48B7FF45B6FF43B5FE40B4FE3FB3FF3DB1FE3BB0FE38
                      AFFE36AEFF34ACFF31ABFE30AAFE2EA9FE2CA8FE2AA6FE28A6FE26A4FF23A3FF
                      22A2FF21A1FE20A1FE20A1FE20A1FE21A2FE20A2FE256DAE8E97ADE1D9D9E4DE
                      DE0066A4D14BA3DA69CAFE56BFFF51BDFE4FBCFE4DBBFF4CBAFF4AB9FF48B7FF
                      46B6FF43B5FE42B4FF3FB3FF3DB2FF3BB0FF39AFFE37AEFF34ACFF32ACFE30AA
                      FE2EA9FF2DA8FF2AA6FE28A6FE26A4FF24A3FF21A2FE20A2FD20A2FE20A2FE21
                      A1FE20A1FE2470B28D98ADE1D9D9E4DEDE0065A5D44CA5DB78D2FF5FC4FF56BF
                      FF52BEFE4FBCFE4EBBFE4CBAFF4AB8FE48B7FE46B6FF44B5FF41B4FE3FB3FE3D
                      B2FF3BB1FF39AFFE37AEFF35ADFF32ACFE30AAFE2EA9FE2DA8FE2AA7FE29A6FE
                      26A4FF24A3FE22A2FE20A2FE21A1FE21A1FE20A1FD2471B38E98AEE1D8D9E5DE
                      DE0065A6D54EA7DD7CD4FE75D2FF5DC3FD55BFFE52BEFE50BDFE4FBBFF4CBAFE
                      4AB9FE48B8FE46B6FE44B5FF41B4FE40B3FE3EB2FE3CB1FF3AB0FE38AFFE36AD
                      FF33ACFF31ABFE2FAAFE2DA9FE2BA7FF29A6FE26A5FF24A4FF22A3FE21A1FE21
                      A1FE1FA2FE2673B68D99AFE0D8D9E3DEDE0065A7D54DA8DD7FD6FE7CD4FF74D0
                      FF61C6FE57C0FE53BEFF52BDFE4FBCFE4CBBFE4BB9FE49B8FE46B7FE45B5FE42
                      B4FF40B3FE3FB3FF3CB1FE3AB0FE39AEFE36AEFF34ACFE31ABFE2FAAFE2EA9FF
                      2CA7FF2AA6FE27A5FF25A4FE23A3FE22A2FE20A1FE2573B68D9AB0E1D8D9E5DE
                      DE0066A8D84FA8DF7FD6FE7FD6FF7BD4FE76D1FE6CCBFE5DC3FF55BFFF51BDFE
                      4EBCFE4DBBFE4BB9FF48B8FF48B7FE46B6FF43B5FE41B4FE3FB3FE3DB2FF3BB0
                      FE38AFFF36AEFF34ACFF32ABFE30AAFE2EA9FF2CA8FF2AA6FE27A5FE25A4FF23
                      A3FF22A2FE2674B88D9BB1E1D9D9E5DDDF0065AADA50AAE082D8FE7FD7FE7DD5
                      FF7BD4FF78D2FF75D0FE6DCBFF61C6FE58C1FE53BDFE4FBBFF4CBAFE49B8FE47
                      B7FF45B6FF43B4FE40B3FE3FB2FE3DB2FF3BB0FE39AFFE37AEFF34ADFF32ABFF
                      31AAFE2FAAFF2DA8FE2AA7FE28A6FE26A5FF24A4FF2775B98E9CB2E1D9D8E4DD
                      DF0065ABDB51ACE184D9FF82D7FE7FD6FE7CD5FF7AD3FE79D2FF76D1FF73CFFE
                      71CEFE6ECDFE6ACBFF63C7FF5FC4FE5BC2FE56C0FE51BDFE4CBAFE48B8FE42B4
                      FE3EB2FF3CB0FF39B0FE36AFFF34ADFF34ABFE31ABFE2FAAFF2DA9FF2BA7FE29
                      A6FF26A5FF2976BA8E9CB3E1D8DAE4DEDE0066ACDD52AEE285DAFF84D9FF82D7
                      FF7FD5FE7CD4FF7AD3FE78D2FF75D0FF73CFFE70CEFE6ECDFF6BCCFF69CAFF67
                      C8FF64C8FF61C6FE5EC4FF5CC2FF58C0FE52BDFE47B7FF3DB2FE3AB0FE37AFFE
                      36ADFF34ACFF31ABFE2FA9FE2DA9FF2BA7FE28A6FF2978BB8E9DB4E0D8DAE4DD
                      DF0066ADDF53AFE487DBFF85DAFF83D8FF81D7FE7ED6FE7CD4FF79D3FF77D2FF
                      75D1FF72CFFE70CEFE6DCCFF6BCCFF69CAFF66C8FF64C7FF61C6FF5EC4FF5CC3
                      FE5BC2FF57C0FF50BCFE42B4FE3BB1FE39AFFE35ADFF34ACFF31ABFE2FAAFE2E
                      A9FE2CA8FF2A7BBD8F9EB5E1D8D9E4DEDF0067AEE054B1E589DDFE88DBFF85D9
                      FF82D8FE7FD7FE7DD5FF7BD4FF79D3FE77D1FF75D0FF73CFFF70CDFF6DCCFE6A
                      CBFF68C9FE67C8FF64C7FF61C5FD5EC4FE5CC2FE5AC1FE58C0FE55BFFE48B8FE
                      3DB1FE38AFFF36AEFF34ACFF32ABFE30AAFF2EA9FE2B7BBD8E9FB5E0D9D9E4DE
                      DE0067AFE256B2E68BDDFE89DCFF87DBFF84D9FE82D8FE80D6FE7DD5FF7BD4FF
                      79D3FE77D1FF75D0FF72CFFF6FCEFE6CCDFE6ACBFF68C9FF67C8FF63C7FE61C6
                      FE5FC4FF5CC3FF5AC1FE58C0FE56BFFE4DBAFE3FB2FE39AFFF36AEFF34ADFF32
                      ABFF30AAFE2C7DBE8E9FB7E0D9D9E4DEDE0066B1E356B3E88DDFFE8BDDFF89DC
                      FF86DAFF84D9FF82D8FE7FD6FE7DD5FF7BD4FF78D2FF77D1FF74D0FF71CFFE6E
                      CDFE6CCCFE69CBFE68C9FF65C8FF63C6FE61C5FF5EC4FF5CC3FE5AC2FE56C0FF
                      54BEFF4EBBFE3FB2FE38AFFE37AFFE35ADFF33ACFE2D7FBF8EA0B7E0D8D9E4DE
                      DF0066B2E456B4E98FDFFE8DDEFF8BDDFF88DCFE86DAFE84D9FF82D7FE7FD6FE
                      7CD5FE7AD4FF78D2FF76D1FF73D0FE70CEFE6ECDFE6CCCFF69CBFF68C9FF65C8
                      FF63C6FF60C5FE5EC3FE5BC2FF59C1FE57C0FF55BEFF4DBAFF3DB2FE39B0FF37
                      AFFF36ADFE2F81C18FA1B8E0D8D9E3DEDE0066B3E556B6EA91E0FE90DFFE8CDE
                      FF8ADDFF87DCFE86DAFF84D9FF81D7FE7FD6FE7CD4FE7AD4FF78D2FF76D1FF73
                      CFFE71CFFE6ECDFF6CCCFF69CAFE67C9FF64C7FF61C6FE60C5FE5EC3FF5BC1FE
                      59C1FE57BFFF54BEFE4CBAFE3DB2FF3BB1FE37AFFE3082C28EA1B9E0D8D9E4DE
                      DE0066B4E656B6EB91E1FE91E0FF8FDFFE8DDEFE8ADDFE87DBFE85DAFF83D9FF
                      80D7FE7ED6FE7CD4FE79D3FE77D2FF75D0FF73CFFE70CEFE6DCDFF6ACCFE68CA
                      FE67C8FF64C7FF62C6FF5FC5FF5DC3FE5BC2FE59C1FE55BFFE54BEFE49B8FE3D
                      B1FF3AB0FE3183C38EA2BAE0D9D8E4DEDE0067B5E756B6EB91E1FE92E0FF91E0
                      FE8EDFFE8CDEFE8ADCFE87DBFE85DAFF83D8FE81D7FE7ED5FE7CD4FF79D3FE77
                      D1FE74D0FF72CFFF70CEFE6DCDFE6ACBFE69CAFF66C8FF64C7FF61C6FE5EC4FE
                      5DC3FF5BC2FF58C1FE55BFFF52BDFE45B6FE3DB1FE3385C58EA3BBE1D9D9E4DE
                      DE0066B4E656B6EC92E0FE92E1FE91E1FE91E0FE8EDEFF8CDEFF89DCFE86DAFF
                      85D9FF83D8FE81D7FF7DD5FF7BD4FE78D3FE77D1FF74D0FF71CFFE6FCEFE6DCC
                      FF6ACBFF68C9FF66C7FF63C7FF61C5FF5FC4FF5CC2FE5AC1FE57C0FF55BFFE4E
                      BBFF40B3FD3486C78EA4BCE1D9D9E4DDDE0066B4E656B7EC92E1FE92E1FF91E1
                      FF91E1FE90E0FE8EDFFE8BDDFF89DCFF86DAFF84D9FE83D8FF80D6FE7DD5FE7A
                      D4FE78D3FE77D1FF74D0FF71CFFE6ECEFE6DCCFF6ACBFF68C9FE65C8FF63C6FF
                      60C5FF5EC4FF5CC2FE5AC1FE59C0FF55BFFF47B8FE3588C88FA4BEDED9D8E6DF
                      E10066B5E656B6EB91E0FE92E0FE91E0FF91E1FE91E0FE90E0FE8DDFFF8BDDFF
                      88DCFE86DAFF84D9FF82D7FF80D6FE7DD5FE7AD4FF78D2FF76D1FF73D0FE71CF
                      FE6ECDFF6BCCFF69CAFE68C9FF65C8FF63C6FF60C5FE5DC4FE5BC2FF5AC1FF58
                      C0FE52BDFF368AC88FA5BEE1D8DAEDE9E9006DB8E752B3EA90E0FE92E1FF91E0
                      FE91E1FE92E1FE91E1FE8FDFFE8EDEFF8BDDFE88DCFE86DAFF84D9FF81D7FE7F
                      D6FE7CD5FF7AD3FF78D2FF75D1FE73D0FE71CFFF6ECDFF6BCCFE69CAFE67C8FF
                      65C7FF62C6FE5FC5FE5DC3FE5CC2FF59C1FF57C0FF327FBB8FA7C0EAE4E6FDFD
                      FD00A5D2F23AA1E174CBF58BDCFC8EDEFD8DDDFD8EDEFE8EDEFE8DDDFD8BDDFD
                      89DBFD86DAFD84D8FD82D7FD80D6FD7ED4FC7BD3FD79D1FD77D0FC75CFFC73CD
                      FC71CDFD6ECCFC6BC9FC68C8FC66C7FC65C5FC63C4FC5FC2FB5DC1FB5BC0FC57
                      BBF746A3DF3072ABC2D1E0FDFDFDFFFFFF00F2F9FD7BBFE9329BDF45A7E74BAF
                      E84BAEE74BAEE84BAEE84BAEE84BACE64BABE44AAAE449A9E348A7E147A5DF46
                      A3DD46A1DC45A0DA449ED9429DD7419BD6419AD54098D33F97D23D95D03E93CF
                      3E91CE3D8FCC3B8EC93A8DC8398BC63380BC2D70AC97B3CEF9FBFCFFFFFFFFFF
                      FF00FFFFFFFFFFFFD9EDF999C4E07EACCC82B1CF82B1CF84B2D072A0BE8EB9D9
                      91BCDB9DC9E8A3CFEEA3CEECA3CEEBA3CDEBA3CCE9A3CBE9A3CAE7A4CAE6A4C9
                      E6A4C9E5A4C9E4A2C7E2A2C7E29BBED95B7D986D8FA97394AF7596AF53738D7A
                      93A9C9CDD3ECE8E9FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFE1E1E2BFBF
                      C0CBCBCBCACACAC9C9C9AAAAA9DAD4D4E0DADAF5F3F3FFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFEFFFFFEFFFFFFFFFFFFFFFFFFFFFEFFFFFEFFFEFFFEFEF1F0F1
                      818181A2A1A2B1B1B1B2B2B3757474A9A5A6E0D8D9ECE8E9FFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFE3E3E4C4C4C4D2D2D2D2D2D2CDCDCDAFADAEDBD4D5
                      E1DADBF5F3F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFEFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFF1F0F1818181A2A2A2B6B6B6B5B6B5757475AA
                      A6A6DFD9D8ECE8E8FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFE5E5E5C7C7
                      C7DADADAD9D9D9D2D1D1B2B1B1DBD5D4E0D9D8F3F0F1FFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F0F0
                      818181A3A3A3BDBDBDB9B9B9747474AAA5A6E0D9D8ECE8E9FFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFE7E7E7CCCCCCE1E1E1E2E2E2D6D6D6B6B5B5DCD5D5
                      E0D8D8EBE7E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFEFEFEF808080A5A5A5C2C2C2BDBDBD747474AA
                      A5A6E0D9D8ECE8E9FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFEAEAEAD0D0
                      D0E7E7E7E9E9E9DDDDDDB5B4B4DBD4D3DFD8D8E3DDDFFEFDFEFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1E1E1
                      7B7B7BABABABC7C7C7C0C0C0747474ADA9AADFD9D8EEEAEBFFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFEFEFEFD3D3D3E9EAEAECECECE6E5E5BABABAD3CDCD
                      DFD8D9E1D9D9F4F0F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFCCCCCC797979B6B6B6C8C8C8C0C0C0747474B9
                      B4B5DFD9D8F4F0F1FFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFF5F5F5D3D3
                      D3EBEBEBEFEFEFEAEAEAC8C8C8C7C4C4DFD8D8E0D9D8E4DDDEFCFAFAFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFF9E9E9E
                      828282C1C1C1C8C8C8B8B7B7747474BEB9BADFD8D9FCFBFBFFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFFEFEFED5D5D5EAEAEAF2F2F2F0F0F0DCDCDCBFBEBE
                      D5D1D0E1D9D8E0D9D9E5E0E1F9F9F9FFFFFFFFFFFFFFFFFFFDFFFFFDFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFE2E2E26D6D6DA5A5A5C7C7C7C7C8C8A6A6A6797979CA
                      C4C6E5E0E1FFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFE1E1
                      E1E4E4E4F4F4F4F3F3F3ECECECC9C9C9C4C2C2DDD6D7E1D8D9E0D9D8E5E0DFF6
                      F3F3FEFEFEFEFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFF1F1F1929292878787
                      C2C2C2CBCBCBC6C7C7909191848283DBD2D4F2EFF0FFFFFFFFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFFFFFFFEEEEEEDFDFDFF2F2F2F5F5F5F2F2F2E4E4E4
                      C4C4C4C6C4C5DED6D8E0D9D8E0D9D8E1DBDAEAE5E6F5F3F3FAF9F9FBFBFBFBFA
                      FAF8F6F6E5E2E3A5A5A57D7D7DBBBBBBCFCFCFCECECEC0C1C1747575A5A1A2E6
                      E0E2FDFCFDFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFCFC
                      FCE0E0E0ECECECF7F7F7F5F5F5F2F2F2E0E0E0C0C0C0C2C0C1D9D3D3E0D9D8DF
                      D9D8DFD8D8E0D8D8E1D9DAE3DCDDE2DBDCCDC8C9989596848484B9B9B9D2D2D2
                      D2D2D2CECECEA4A4A4717172CEC7C9F5F2F3FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0F0F0DFDFDFF4F4F4F8F8F8F6F6F6
                      F3F3F3E7E7E7C9C9C9B8B8B8C0BEBECCC8C8D4CECDD6D0D0D0CACAC3BEBFA7A5
                      A68E8D8EA0A0A0C8C8C8D6D6D6D7D7D7D4D4D4BFBFBF757575A39FA0EFEDEEFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFEFEFEE7E7E7E4E4E4F8F8F8F8F8F8F6F6F6F3F3F3EDEDEDDDDDDDC5C5C5B9
                      B8B9B2B2B2AEADADA9A9A9A7A7A7AFAFAFC7C7C7D5D5D5DCDCDCDBDBDBD9D9D9
                      CBCBCB9191917F7E7FE8E6E6FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F8F8E1E1E1E9E9E9F8F8F8
                      F8F8F8F7F7F7F5F5F5F2F2F2EDEDEDE4E4E4DDDDDDD8D8D8D7D7D7DBDBDBE1E1
                      E1E2E2E2E1E1E1E0E0E0DDDDDDD2D2D2A0A0A0818181D1D0D0FFFEFEFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFF8F8F8DDDDDDE5E5E5F8F8F8F9F9F9F8F8F8F6F6F6F4F4F4F2
                      F2F2EFEFEFEDEDEDEBEBEBE9E9E9E8E8E8E6E6E6E4E4E4E1E1E1D6D6D6A1A1A1
                      898989D2D2D2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F9F9E1E1E1
                      DBDBDBECECECF9F9F9F8F8F8F6F6F6F5F5F5F3F3F3F1F1F1EEEEEEECECECEBEB
                      EBE7E7E7E0E0E0C5C5C59797979C9C9CE1E1E1FFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFCEBEBEBD8D8D8D9D9D9E8E8E8EEEEEEF1
                      F1F1F4F4F4F2F2F2EFEFEFE8E8E8DCDCDCCACACAACACACA0A0A0BCBCBCF3F3F3
                      FEFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FEFEFEF7F7F7E9E9E9D4D4D4D0D0D0CDCDCDCDCDCDC8C8C8C3C3C3BCBCBCB4B4
                      B4B0B0B0C8C8C8E7E7E7FDFDFDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFDFDF2F2F2E8
                      E8E8E4E4E4DADADADDDDDDE1E1E1EAEAEAFAFAFAFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FF00}
                    Stretch = True
                    ExplicitLeft = 59
                    ExplicitTop = -13
                    ExplicitWidth = 52
                    ExplicitHeight = 54
                  end
                end
                object Panel22: TPanel
                  Left = 49
                  Top = 0
                  Width = 49
                  Height = 68
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Label16: TLabel
                    Left = 0
                    Top = 0
                    Width = 13
                    Height = 13
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'ico'
                  end
                  object Image_ico: TImage
                    Left = 0
                    Top = 13
                    Width = 49
                    Height = 55
                    Align = alClient
                    Picture.Data = {
                      055449636F6E0000010001002020100000000000E80200001600000028000000
                      2000000040000000010004000000000080020000000000000000000000000000
                      0000000000000000000080000080000000808000800000008000800080800000
                      C0C0C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000
                      FFFFFF0000000000000000000000000000800000077777777777777777777777
                      7708000000777777777777777777777777708000080000008770000008770000
                      008708000880BBB03880EEEE06880DDDD05880800880BBB03080EEEE06080DDD
                      D05088080880BBB03080EEEE06080DDDD05088800880BBB03000EEEE06000DDD
                      D05000000880BBB03000EEEE06000DDDD05000000880BBB03000EEEE06000DDD
                      D05000000880BBB03000EEEE06000DDDD05000000880BBB03000EEEE06000DDD
                      D05000000880BBB03000EEEE06000DDDD05000000880BBB03000EEEE06000DDD
                      D05000000880BBB03000EEEE06000DDDD0500000088000003000EEEE06000DDD
                      D050000008880BBB0000EEEE06000DDDD0500000088880000000EEEE06000DDD
                      D0500000088880000000EEEE06000DDDD0500000088880000000EEEE06000DDD
                      D0500000088880000000EEEE06000DDDD0500000088880000000EEEE06000000
                      00500000078880000000EEEE060000DDDD500000007880000000EEEE06000000
                      00000000000780000000EEEE0600000000000000000070000000000006000000
                      000000000000000000000EEEE000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      0000000000000000000000000000000000000000000000000000000000000000
                      000000000000001F0000000F0000000700000003000000010000000000000000
                      000000000020100F0020100F0020100F0020100F0020100F0020100F0020100F
                      0020100F0020100F0020100F03E0100F03E0100F03E0100F03E0100F03E0180F
                      83E01C0FC3E01FFFE3E01FFFF3F01FFFFFF81FFFFFFFFFFFFFFFFFFFFFFFFFFF
                      FFFFFFFF}
                    Stretch = True
                    ExplicitLeft = 6
                    ExplicitTop = 19
                    ExplicitHeight = 48
                  end
                end
                object Panel23: TPanel
                  Left = 98
                  Top = 0
                  Width = 49
                  Height = 68
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 2
                  object Label17: TLabel
                    Left = 0
                    Top = 0
                    Width = 15
                    Height = 13
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'jpg'
                  end
                  object Image_jpg: TImage
                    Left = 0
                    Top = 13
                    Width = 49
                    Height = 55
                    Align = alClient
                    Picture.Data = {
                      0A544A504547496D616765BC040000FFD8FFE000104A46494600010101006000
                      600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
                      0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
                      3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
                      3232323232323232323232323232323232323232323232323232323232323232
                      32323232323232323232323232FFC00011080027003003012200021101031101
                      FFC4001F0000010501010101010100000000000000000102030405060708090A
                      0BFFC400B5100002010303020403050504040000017D01020300041105122131
                      410613516107227114328191A1082342B1C11552D1F02433627282090A161718
                      191A25262728292A3435363738393A434445464748494A535455565758595A63
                      6465666768696A737475767778797A838485868788898A92939495969798999A
                      A2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6
                      D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301
                      01010101010101010000000000000102030405060708090A0BFFC400B5110002
                      0102040403040705040400010277000102031104052131061241510761711322
                      328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728
                      292A35363738393A434445464748494A535455565758595A636465666768696A
                      737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7
                      A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3
                      E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F7FA
                      2BE60B6F8F1E339C0DC74F186DA716C72791EFEE7F4AD483E32F8B650034B641
                      BD05BF4FD7F5A069367D175E0BE03F1FDDF876DEDEDAF15AE34A7553E503F35B
                      7192509E081DD38F51C82090FC57F14C98CCB6FF0030E3F71DCF41D3F1AE4A18
                      962B64897714440338E4A81F8039C1E715D986A2A77523AE8D0BA7CC7D3F637F
                      6BA9D8C57B653A4F6D32EE4910F07FC083C11D4118AB15F39F873C5DA8784AF9
                      A7B63E65A4AC0DC5A313B1FB6E1FDD7ED9E7206083818F76F0F788F4DF13E98B
                      7DA6CC597EEC9138C490B77561D88F6C83D41239A9C4E12A50D5FC2FA9CF529B
                      83B33E27D3864600E7776FC31FAE0F7E9F9F456BCA71C2E091EBDFFCE7BD7A24
                      3FB3A6B109E35EB039E0E617F7FF001FD31DEB4A2F813ABC646756B03F457F6F
                      6FAFE43F0E5145A5B9C25BFDE4E3192303AE7BE075F6FF000ABAA06DF940278E
                      71EDC74FC3F10335DDC5F067588C60EA56073D701FF1EDCF7EBE82B81824F3ED
                      A294AEDF3103ED3D403CE0123B1E3A0EF5E961249BB23D0A338CB4447367CBF9
                      49E9DB3D3F0F6C7E95A9E01B4F13DCF8B636F0C3980C6DFE973C8B9812304FCB
                      2007E6273808083C641182CBADE13F045EF8C1FCF691EDB4C57C497614132107
                      958F70E483C6E208078E48207BA693A458687A745A7E9B6C96F6D10F95179C9F
                      524F249EE4F26BD1C56670A745D082526F7BECBFE0FE472E2669BE545DA28A2B
                      E70E60AF15F87FF0E5B55B58351D5C2A698157C9B7461BAE48E32F8E1538E839
                      3FEC818628AB8CE51BD8B8CE514D2EA7B3C514704290C31AC7146A1511061540
                      E00007414FA28A820FFFD9}
                    Stretch = True
                    ExplicitLeft = -3
                    ExplicitTop = 7
                    ExplicitWidth = 52
                    ExplicitHeight = 54
                  end
                end
                object Panel24: TPanel
                  Left = 147
                  Top = 0
                  Width = 49
                  Height = 68
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 3
                  object Label18: TLabel
                    Left = 0
                    Top = 0
                    Width = 16
                    Height = 13
                    Align = alTop
                    Alignment = taCenter
                    Caption = 'null'
                  end
                  object Image_null: TImage
                    Left = 0
                    Top = 13
                    Width = 49
                    Height = 55
                    Align = alClient
                    Stretch = True
                    ExplicitLeft = -3
                    ExplicitTop = 7
                    ExplicitWidth = 52
                    ExplicitHeight = 54
                  end
                end
              end
            end
          end
          object Panel14: TPanel
            Left = 291
            Top = 1
            Width = 243
            Height = 221
            Align = alClient
            TabOrder = 1
            object GroupBox56: TGroupBox
              Left = 1
              Top = 1
              Width = 241
              Height = 76
              Align = alTop
              Caption = 'TreeView'
              TabOrder = 0
              object TreeView1: TTreeView
                Left = 2
                Top = 15
                Width = 119
                Height = 59
                Align = alLeft
                HideSelection = False
                Indent = 19
                TabOrder = 0
              end
              object TreeView2: TTreeView
                Left = 121
                Top = 15
                Width = 118
                Height = 59
                Align = alClient
                HideSelection = False
                Indent = 19
                PopupMenu = PopupMenu1
                TabOrder = 1
                Items.NodeData = {
                  0302000000220000000000000000000000FFFFFFFFFFFFFFFF00000000000000
                  0000000000010261006100220000000000000000000000FFFFFFFFFFFFFFFF00
                  0000000000000001000000010262006200220000000000000000000000FFFFFF
                  FFFFFFFFFF000000000000000000000000010263006300}
              end
            end
            object GroupBox57: TGroupBox
              Left = 1
              Top = 77
              Width = 241
              Height = 76
              Align = alTop
              Caption = 'ListBox'
              TabOrder = 1
              object ListBox1: TListBox
                Left = 2
                Top = 15
                Width = 119
                Height = 59
                Align = alLeft
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Arial'
                Font.Style = []
                ItemHeight = 14
                Items.Strings = (
                  '111-a1111111111111111'
                  'aaa-a2'
                  #20013#22269#20154'-a3')
                MultiSelect = True
                ParentFont = False
                TabOrder = 0
              end
              object ListBox2: TListBox
                Left = 121
                Top = 15
                Width = 118
                Height = 59
                Align = alClient
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'Arial'
                Font.Style = []
                ItemHeight = 14
                Items.Strings = (
                  '111-a1111111111111111'
                  'aaa-a2'
                  #20013#22269#20154'-a3')
                MultiSelect = True
                ParentFont = False
                TabOrder = 1
              end
            end
            object CheckListBox: TGroupBox
              Left = 1
              Top = 153
              Width = 241
              Height = 67
              Align = alClient
              Caption = 'ListBox'
              TabOrder = 2
              object CheckListBox1: TCheckListBox
                Left = 2
                Top = 15
                Width = 78
                Height = 50
                Align = alLeft
                ItemHeight = 13
                Items.Strings = (
                  '111-a1111111111111111'
                  'aaa-a2'
                  #20013#22269#20154'-a3')
                TabOrder = 0
              end
              object CheckListBox2: TCheckListBox
                Left = 80
                Top = 15
                Width = 77
                Height = 50
                Align = alLeft
                ItemHeight = 13
                Items.Strings = (
                  '111-a1111111111111111'
                  'aaa-a2'
                  #20013#22269#20154'-a3')
                TabOrder = 1
              end
              object CheckListBox3: TCheckListBox
                Left = 157
                Top = 15
                Width = 82
                Height = 50
                Align = alClient
                ItemHeight = 13
                Items.Strings = (
                  '111-a1111111111111111'
                  'aaa-a2'
                  #20013#22269#20154'-a3')
                TabOrder = 2
              end
            end
          end
        end
      end
      object HotKey1: THotKey
        Left = 543
        Top = 3
        Width = 121
        Height = 19
        HotKey = 32833
        TabOrder = 1
      end
      object Memo5: TMemo
        Left = 414
        Top = 3
        Width = 99
        Height = 70
        Enabled = False
        Lines.Strings = (
          '<html>'
          '<head>'
          
            '<!-- Inserted by TRADOS: --><META HTTP-EQUIV="content-type" CONT' +
            'ENT="text/html; charset=GB2312">'
          ''
          #9'<title>UltraEdit - '#20837#38376#25351#21335'</title>'
          #9'<link href="style.css" rel="stylesheet" type="text/css" />'
          '</head>'
          '<body>'
          '<div id="content">'
          ''
          '  <div id="sidebar">'
          '<img src="images\ues_icon.gif" alt="UEStudio" border="0" />'
          '  </div>'
          ''
          '  <div id="main">'
          ''
          '    <div class="top_pane">'
          
            #9'<div class="index_nav"><a href="qsg4.html">&lt;'#19978#19968#39029'</a> | '#31532' 5 '#39029#65288 +
            #20849' 5 '#39029#65289'| '#19979#19968#39029'&gt;</div>'
          '<h2>'#25105#35813#22914#20309'... </h2>'
          '<br /><b style="color: #ac071f">'#38024#23545' UEStudio '#30340#20027#39064#65306'</b>  '#9'  '
          '    </div>'
          ''
          '  '#9'<div class="link_window">'
          '  '#9#9'<ul>'
          
            '<li><a id="Catch1" alt="http://www.ultraedit.com/support/tutoria' +
            'ls_power_tips/uestudio/create_your_first_app.html " target="_bla' +
            'nk">'#21019#24314' Windows '#24212#29992#31243#24207'</a></li>'
          
            '<li><a id="Catch2" alt="http://www.ultraedit.com/support/tutoria' +
            'ls_power_tips/uestudio/integrated_debugger.html" target="_blank"' +
            '>'#20351#29992#38598#25104#30340#35843#35797#22120'</a></li>'
          
            '<li><a id="Catch3" alt="http://www.ultraedit.com/support/tutoria' +
            'ls_power_tips/uestudio/classviewer.html" target="_blank">'#20351#29992#31867#26597#30475#22120'<' +
            '/a></li>'
          
            '<li><a id="Catch4" alt="http://www.ultraedit.com/support/tutoria' +
            'ls_power_tips/uestudio/configure_vcs.html" target="_blank">'#37197#32622#29256#26412#25511 +
            #21046'</a></li>'
          
            '<li><a id="Catch5" alt="http://www.ultraedit.com/support/tutoria' +
            'ls_power_tips/uestudio/develop_java_applications.html" target="_' +
            'blank">'#20351#29992' UEStudio '#24320#21457' Java '#24212#29992#31243#24207'</a></li>'
          
            '<li><a id="Catch6" alt="http://www.ultraedit.com/support/tutoria' +
            'ls_power_tips/uestudio/local_php_mysql_dev_environment.html" tar' +
            'get="_blank">'#37197#32622' PHP/MySQL '#24320#21457#29615#22659'</a></li>'
          
            '<li><a id="Catch7" alt="http://www.ultraedit.com/support/tutoria' +
            'ls_power_tips/uestudio/borland_compiler.html" target="_blank">'#20351#29992 +
            ' Borland C/C++ '#32534#35793#22120'</a></li>'
          '  '#9#9'</ul>'
          '  '#9'</div>'
          ''
          '  '#9'<div class="bottom_pane">'
          #9'  <p>'#27809#26377#25214#21040#24744#38656#35201#30340#20869#23481#65311' </p>'
          #9'<span class="nav">'
          
            #9#9'<a id="Catch8" alt="http://www.ultraedit.com/support/tutorials' +
            '_power_tips.html" target="_blank">http://www.ultraedit.com/suppo' +
            'rt/tutorials_power_tips/uestudio/create_your_first_app.html</a>'
          #9'</span>'
          #9'<span class="nav">'
          
            #9#9'<a id="Catch9" alt="http://www.ultraedit.com/support/forums.ht' +
            'ml" target="_blank"> grjmy@163.com '#35770#22363'</a>'
          #9'</span>'
          #9'<span class="nav">'
          
            #9#9'<a id="Catch10" alt="http://www.ultraedit.com/products/ultraed' +
            'it.html" target="_blank"> grj208981@163.com</a>'
          #9'</span>'
          '  '#9'</div>'
          ''
          '  </div><!-- end main -->'
          ''
          '</div><!-- end content -->'
          '</body>'
          '</html>')
        TabOrder = 2
        Visible = False
        WordWrap = False
      end
      object RadioGroup1: TRadioGroup
        AlignWithMargins = True
        Left = 540
        Top = 28
        Width = 270
        Height = 29
        BiDiMode = bdLeftToRight
        Ctl3D = False
        ParentBiDiMode = False
        ParentCtl3D = False
        TabOrder = 3
        OnClick = RadioGroup2Click
      end
      object GroupBox62: TGroupBox
        Left = 543
        Top = 63
        Width = 121
        Height = 189
        Caption = 'ABPubItemListFrameU'
        TabOrder = 4
        object ABItemList1: TABItemList
          Left = 2
          Top = 15
          Width = 117
          Height = 147
          Align = alClient
          ParentBackground = False
          ShowCaption = False
          TabOrder = 0
          MoveVisible = True
        end
        object Button60: TButton
          Left = 2
          Top = 162
          Width = 117
          Height = 25
          Align = alBottom
          Caption = 'ABPubItemListFrameU'
          TabOrder = 1
          OnClick = Button60Click
        end
      end
      object GroupBox63: TGroupBox
        Left = 687
        Top = 63
        Width = 242
        Height = 258
        Caption = 'ABPubItemListsFrameU'
        TabOrder = 5
        object ABItemLists1: TABItemLists
          Left = 2
          Top = 15
          Width = 238
          Height = 216
          Align = alClient
          ParentBackground = False
          ShowCaption = False
          TabOrder = 0
          LableCount = 0
          LabelVisible = True
          Items1MoveVisible = True
          Items2MoveVisible = True
          Items1Items2MoveVisible = True
        end
        object Button61: TButton
          Left = 2
          Top = 231
          Width = 238
          Height = 25
          Align = alBottom
          Caption = 'ABPubItemListsFrameU'
          TabOrder = 1
          OnClick = Button61Click
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #25511#20214' '#25193#23637
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object GroupBox73: TGroupBox
        Left = 433
        Top = 0
        Width = 511
        Height = 463
        Align = alClient
        Caption = 'GroupBox8'
        TabOrder = 0
        OnClick = GroupBox73Click
        object ABSockUI1: TABSockUI
          Left = 2
          Top = 15
          Width = 507
          Height = 247
          Align = alTop
          Caption = 'ABSockUI1'
          TabOrder = 0
        end
        object ABCheckedComboBox1: TABCheckedComboBox
          Left = 3
          Top = 268
          Width = 145
          Height = 22
          ViewItems.Strings = (
            'name1'
            'name2'
            'name3')
          SaveItems.Strings = (
            'GUID1'
            'GUID2'
            'GUID3')
          ViewSeparatorStr = ','
          SaveSeparatorStr = ','
          DownSeparatorStr = ','
          Style = csOwnerDrawVariable
          ColorNotFocus = clWindow
          Items.Strings = (
            '1'
            '2'
            '3')
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnChange = ABCheckedComboBox1Change
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 225
        Height = 463
        Align = alLeft
        TabOrder = 1
        object GroupBox33: TGroupBox
          Left = 1
          Top = 1
          Width = 223
          Height = 107
          Align = alTop
          Caption = 'ABPubScrollBoxU'
          TabOrder = 0
          object ABScrollBox1: TABScrollBox
            Left = 5
            Top = 12
            Width = 213
            Height = 92
            HorzScrollBar.Position = 4
            VertScrollBar.Position = 79
            TabOrder = 0
            OnHScroll = ABScrollBox1HScroll
            OnVScroll = ABScrollBox1VScroll
            object Panel25: TPanel
              Left = 7
              Top = 11
              Width = 185
              Height = 158
              Caption = 'Panel7'
              TabOrder = 0
            end
          end
        end
        object GroupBox43: TGroupBox
          Left = 1
          Top = 108
          Width = 223
          Height = 45
          Align = alTop
          Caption = 'ABPubLedU'
          TabOrder = 1
          object ABLed1: TABLed
            Left = 0
            Top = 12
            Width = 16
            Height = 16
            FalseColor = clRed
            Bordered = False
            Value = True
          end
          object ABLed2: TABLed
            Left = 15
            Top = 11
            Width = 22
            Height = 22
            FalseColor = clRed
            Bordered = False
            Value = True
            LEDStyle = LEDSqLarge
          end
          object ABLed3: TABLed
            Left = 35
            Top = 12
            Width = 16
            Height = 16
            FalseColor = clRed
            Bordered = False
            Value = True
            LEDStyle = LEDSqSmall
          end
          object ABLed4: TABLed
            Left = 50
            Top = 12
            Width = 16
            Height = 22
            FalseColor = clRed
            Bordered = False
            Value = True
            LEDStyle = LEDVertical
          end
          object ABLed5: TABLed
            Left = 65
            Top = 12
            Width = 22
            Height = 16
            FalseColor = clRed
            Bordered = False
            Value = True
            LEDStyle = LEDHorizontal
          end
          object ABLed6: TABLed
            Left = 86
            Top = 10
            Width = 22
            Height = 22
            FalseColor = clRed
            Blink = False
            Bordered = False
            Value = True
            LEDStyle = LEDLarge
          end
          object Button5: TButton
            Left = 108
            Top = 13
            Width = 110
            Height = 25
            Caption = #24555#38378'10'
            TabOrder = 0
            OnClick = Button5Click
          end
        end
      end
      object Panel26: TPanel
        Left = 225
        Top = 0
        Width = 208
        Height = 463
        Align = alLeft
        TabOrder = 2
        object GroupBox3aaaaa: TGroupBox
          Left = 1
          Top = 100
          Width = 206
          Height = 128
          Align = alTop
          Caption = #21333#25968#25454#38598
          TabOrder = 0
          object ABMultilingualDBNavigator1: TABMultilingualDBNavigator
            Left = 2
            Top = 82
            Width = 202
            Height = 25
            DataSource = FDataSource
            VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
            Align = alBottom
            TabOrder = 0
          end
          object Panel2: TPanel
            Left = 167
            Top = 15
            Width = 37
            Height = 67
            Align = alRight
            TabOrder = 1
            object Image1: TImage
              Left = 1
              Top = 1
              Width = 35
              Height = 32
              Align = alTop
              Picture.Data = {
                0A544A504547496D616765BC040000FFD8FFE000104A46494600010101006000
                600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
                0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
                3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
                3232323232323232323232323232323232323232323232323232323232323232
                32323232323232323232323232FFC00011080027003003012200021101031101
                FFC4001F0000010501010101010100000000000000000102030405060708090A
                0BFFC400B5100002010303020403050504040000017D01020300041105122131
                410613516107227114328191A1082342B1C11552D1F02433627282090A161718
                191A25262728292A3435363738393A434445464748494A535455565758595A63
                6465666768696A737475767778797A838485868788898A92939495969798999A
                A2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6
                D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301
                01010101010101010000000000000102030405060708090A0BFFC400B5110002
                0102040403040705040400010277000102031104052131061241510761711322
                328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728
                292A35363738393A434445464748494A535455565758595A636465666768696A
                737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7
                A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3
                E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F7FA
                2BE60B6F8F1E339C0DC74F186DA716C72791EFEE7F4AD483E32F8B650034B641
                BD05BF4FD7F5A069367D175E0BE03F1FDDF876DEDEDAF15AE34A7553E503F35B
                7192509E081DD38F51C82090FC57F14C98CCB6FF0030E3F71DCF41D3F1AE4A18
                962B64897714440338E4A81F8039C1E715D986A2A77523AE8D0BA7CC7D3F637F
                6BA9D8C57B653A4F6D32EE4910F07FC083C11D4118AB15F39F873C5DA8784AF9
                A7B63E65A4AC0DC5A313B1FB6E1FDD7ED9E7206083818F76F0F788F4DF13E98B
                7DA6CC597EEC9138C490B77561D88F6C83D41239A9C4E12A50D5FC2FA9CF529B
                83B33E27D3864600E7776FC31FAE0F7E9F9F456BCA71C2E091EBDFFCE7BD7A24
                3FB3A6B109E35EB039E0E617F7FF001FD31DEB4A2F813ABC646756B03F457F6F
                6FAFE43F0E5145A5B9C25BFDE4E3192303AE7BE075F6FF000ABAA06DF940278E
                71EDC74FC3F10335DDC5F067588C60EA56073D701FF1EDCF7EBE82B81824F3ED
                A294AEDF3103ED3D403CE0123B1E3A0EF5E961249BB23D0A338CB4447367CBF9
                49E9DB3D3F0F6C7E95A9E01B4F13DCF8B636F0C3980C6DFE973C8B9812304FCB
                2007E6273808083C641182CBADE13F045EF8C1FCF691EDB4C57C497614132107
                958F70E483C6E208078E48207BA693A458687A745A7E9B6C96F6D10F95179C9F
                524F249EE4F26BD1C56670A745D082526F7BECBFE0FE472E2669BE545DA28A2B
                E70E60AF15F87FF0E5B55B58351D5C2A698157C9B7461BAE48E32F8E1538E839
                3FEC818628AB8CE51BD8B8CE514D2EA7B3C514704290C31AC7146A1511061540
                E00007414FA28A820FFFD9}
              Stretch = True
              ExplicitWidth = 52
            end
            object Image2: TImage
              Left = 1
              Top = 33
              Width = 35
              Height = 33
              Align = alClient
              Picture.Data = {
                0A544A504547496D616765AB040000FFD8FFE000104A46494600010101006000
                600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C191213
                0F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F2739
                3D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232
                3232323232323232323232323232323232323232323232323232323232323232
                32323232323232323232323232FFC00011080027003003012200021101031101
                FFC4001F0000010501010101010100000000000000000102030405060708090A
                0BFFC400B5100002010303020403050504040000017D01020300041105122131
                410613516107227114328191A1082342B1C11552D1F02433627282090A161718
                191A25262728292A3435363738393A434445464748494A535455565758595A63
                6465666768696A737475767778797A838485868788898A92939495969798999A
                A2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6
                D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301
                01010101010101010000000000000102030405060708090A0BFFC400B5110002
                0102040403040705040400010277000102031104052131061241510761711322
                328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728
                292A35363738393A434445464748494A535455565758595A636465666768696A
                737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7
                A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3
                E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F7FA
                AF7F7F6BA5D8CB7B7D3A416D10CBC8E781D87D4938007524802A8F88BC49A6F8
                5F4C6BED4A6DABC88E24C19256FEEA8EE7EB803A920735E0FE25F166A3E2DBC5
                9AF311DAC4CC60B58D89588E08C9FEF36370DC71EC064D75E1B0752BEAB48AEA
                694E9B9BB1B5E31F1EDDF89253696BE65AE940F11E7124FCFDE7F6F4419E393C
                E02FBA57CB6F9DA70DB59B382A00E4F719F42076238CD7552FC57F14264ACB68
                79EF08C75F5EDEBCF6A3114542CA3B1D15E85ADCA7BD515F3ACDF197C5B18E24
                B43FF6EF59171F1E7C69127CADA7138EBF67E0F4CFF17BF1ED5C87234D16BC7B
                65E27B5F154B2F89A4F3E4909FB35C46A56068C1DC1507F085C72A7E61D496DC
                1AB322FB9DFAF04E7F0FF3F8FB8FA7755D26C35BD3E4B0D46D92E2DE4EAADD8F
                620F5047623915E17E2CF035EF845FCE566B8D2D982A5D6D00C64E0012631827
                38DC300F038240AFA4C2E670AB49519A516B6B6CFF00E09D3869A4F959CE1FBB
                907A8C03C004E0FD3F1EDC551B820E4EEF4E5BD083EFF8FD7F5BFF0078614293
                D39E38E9CE071D08FC7EA6BBA93E0D6B3275D4EC47BFCFC7407B7D7F215E662E
                493B33A6B4E2B4678E5D7DD233D7B671D39AE76FF3839CE73FD79EFEB91DFF00
                C7DE25F815AC4A30757B119EBF2B9FAF6F73FA5664FF00B3AEB5303FF13CD3D4
                9FFA66FEDC7E99FAD79C79F269EC7D134C9628E785E19A35922914ABA38CAB03
                C1041EA28A2820F1DF1C7C3E3A2AC9AB693B5B4D53BE68246F9ADF9EAA4FDE4C
                9E99C8C0FBC385F65A28AB94DC92B9729B92570A28A2A083FFD9}
              Stretch = True
              ExplicitTop = 48
              ExplicitWidth = 52
              ExplicitHeight = 43
            end
          end
          object DBMemo1: TDBMemo
            Left = 128
            Top = 15
            Width = 39
            Height = 67
            Align = alRight
            DataField = 'Remark'
            DataSource = FDataSource
            TabOrder = 2
          end
          object DBGrid2: TDBGrid
            Left = 2
            Top = 15
            Width = 86
            Height = 67
            Align = alClient
            DataSource = FDataSource
            TabOrder = 3
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
          end
          object DBImage1: TDBImage
            Left = 88
            Top = 15
            Width = 40
            Height = 67
            Align = alRight
            DataField = 'photo_bmp'
            DataSource = FDataSource
            TabOrder = 4
          end
          object Panel1: TPanel
            Left = 2
            Top = 107
            Width = 202
            Height = 19
            Align = alBottom
            TabOrder = 5
            object Label6: TLabel
              Left = 1
              Top = 1
              Width = 96
              Height = 13
              Align = alLeft
              Caption = #22810#23383#27573#26631#31614#25511#20214#65306
            end
            object ABDBLabels1: TABDBLabels
              Left = 97
              Top = 1
              Width = 104
              Height = 17
              Align = alClient
              AutoSize = False
              BiDiMode = bdRightToLeft
              ParentBiDiMode = False
              Transparent = True
              DataSource = FDataSource
              ExplicitLeft = 18
              ExplicitTop = 281
              ExplicitWidth = 248
            end
          end
        end
        object GroupBox34: TGroupBox
          Left = 1
          Top = 1
          Width = 206
          Height = 37
          Align = alTop
          Caption = 'ABPubIPEditU'
          TabOrder = 1
          object ABIPEdit1: TABIPEdit
            Left = 5
            Top = 10
            Width = 154
            Height = 25
            Field0 = 0
            Field1 = 0
            Field2 = 0
            Field3 = 0
            IP = 0
            TabOrder = 0
            TabStop = True
          end
        end
        object GroupBox29: TGroupBox
          Left = 1
          Top = 38
          Width = 206
          Height = 62
          Align = alTop
          Caption = 'ABPubStringGridU'
          TabOrder = 2
          object ABStringGrid1: TABStringGrid
            Left = 2
            Top = 15
            Width = 202
            Height = 45
            Align = alClient
            ColCount = 2
            DefaultRowHeight = 15
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSizing, goColSizing, goRowMoving, goColMoving, goEditing]
            TabOrder = 0
            RowCaption.Strings = (
              ''
              '1')
            ColCaption.Strings = (
              'aa'
              'bb')
          end
        end
        object ABDownButton2: TABDownButton
          Left = 1
          Top = 228
          Width = 206
          Height = 25
          Align = alTop
          Caption = #19979#25289#25968#25454#38598#25805#20316#33756#21333
          DropDownMenu = ABDBMainDetailPopupMenu1
          TabOrder = 3
        end
        object GroupBox58: TGroupBox
          Left = 1
          Top = 253
          Width = 206
          Height = 118
          Align = alTop
          Caption = #20027#25968#25454#38598
          TabOrder = 4
          object ABCheckTreeView1: TABCheckTreeView
            Left = 2
            Top = 15
            Width = 202
            Height = 101
            MoveAfirm = False
            Align = alClient
            Flatness = cfAlwaysFlat
            GrayedIsChecked = False
            Indent = 19
            TabOrder = 0
          end
        end
      end
    end
  end
  object Panel8: TPanel
    Left = 0
    Top = 0
    Width = 185
    Height = 491
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
    object Memo4: TMemo
      Left = 0
      Top = 353
      Width = 185
      Height = 83
      Align = alBottom
      Lines.Strings = (
        'Memo3')
      ScrollBars = ssBoth
      TabOrder = 0
      OnKeyDown = Memo3KeyDown
    end
    object Memo3: TMemo
      Left = 0
      Top = 0
      Width = 185
      Height = 266
      Align = alTop
      Lines.Strings = (
        'Memo3')
      ScrollBars = ssBoth
      TabOrder = 1
      OnKeyDown = Memo3KeyDown
    end
    object ProgressBar2: TProgressBar
      Left = 0
      Top = 436
      Width = 185
      Height = 17
      Align = alBottom
      TabOrder = 2
    end
    object Panel10: TPanel
      Left = 0
      Top = 453
      Width = 185
      Height = 38
      Align = alBottom
      TabOrder = 3
      object Button52: TButton
        Left = 86
        Top = 6
        Width = 92
        Height = 25
        Caption = #35843#29992#25152#26377#20989#25968
        TabOrder = 0
        OnClick = Button52Click
      end
      object Button6: TButton
        Left = 0
        Top = 6
        Width = 80
        Height = 25
        Caption = #28165#38500#26085#24535
        TabOrder = 1
        OnClick = Button6Click
      end
    end
  end
  object FDataSource: TDataSource
    Left = 8
    Top = 224
  end
  object IdFTP1: TIdFTP
    IPVersion = Id_IPv4
    NATKeepAlive.UseKeepAlive = False
    NATKeepAlive.IdleTimeMS = 0
    NATKeepAlive.IntervalMS = 0
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    Left = 16
    Top = 72
  end
  object IdHTTP1: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 80
    Top = 80
  end
  object MainMenu1: TMainMenu
    OwnerDraw = True
    Left = 16
    Top = 24
    object aa1: TMenuItem
      Caption = #35821#35328
    end
    object bb1: TMenuItem
      Caption = 'bb'
      object cc1: TMenuItem
        Caption = 'cc'
      end
    end
  end
  object SaveDialog1: TSaveDialog
    Left = 88
    Top = 136
  end
  object ImageList1: TImageList
    Left = 16
    Top = 136
    Bitmap = {
      494C010104000A00040010001000FFFFFFFFFF00FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000002000000001002000000000000020
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001840
      9000205CA0001840900018409000184090001840900018409000184090001028
      8000184090000000000000000000000000000000000000000000000000001840
      9000205CA0001840900018409000184090001840900018409000184090001028
      8000184090000000000000000000000000000000000000000000000000001840
      9000205CA0001840900018409000184090001840900018409000184090001028
      8000184090000000000000000000000000000000000000000000000000001840
      9000205CA0001840900018409000184090001840900018409000184090001028
      800018409000000000000000000000000000000000000000000000000000205C
      A0002078B000205CA00020202000202020002020200020202000205CA0001840
      9000205CA000000000000000000000000000000000000000000000000000205C
      A0002078B000205CA000205CA000202020002020200018409000205CA0001840
      9000205CA000000000000000000000000000000000000000000000000000205C
      A0002078B0002020200020202000205CA000205CA00020202000202020001840
      9000205CA000000000000000000000000000000000000000000000000000205C
      A0002078B000205CA00020202000202020002020200020202000205CA0001840
      9000205CA0000000000000000000000000000000000000000000000000002078
      B0002890C00018409000303030002078B0002078B0003030300018409000205C
      A0002078B0000000000000000000000000000000000000000000000000002078
      B0002890C0002078B0002078B00030303000303030002078B0002078B000205C
      A0002078B0000000000000000000000000000000000000000000000000002078
      B0002890C00030303000303030002078B0002078B0002078B00030303000205C
      A0002078B0000000000000000000000000000000000000000000000000002078
      B0002890C00018409000303030002078B0002078B0003030300018409000205C
      A0002078B0000000000000000000000000000000000000000000000000002890
      C00030ACD00040404000404040002890C0002890C00040404000404040002078
      B0002890C0000000000000000000000000000000000000000000000000002890
      C00030ACD0002890C0002890C00040404000404040002890C0002890C0002078
      B0002890C0000000000000000000000000000000000000000000000000002890
      C00030ACD0002890C00040404000205CA0002890C0002890C000404040002078
      B0002890C0000000000000000000000000000000000000000000000000002890
      C00030ACD00040404000404040002890C0002890C00040404000404040002078
      B0002890C00000000000000000000000000000000000000000000000000030AC
      D00030C8E000505050005050500030ACD00030ACD00050505000505050002890
      C00030ACD00000000000000000000000000000000000000000000000000030AC
      D00030C8E00030ACD00030ACD000505050005050500030ACD00030ACD0002890
      C00030ACD00000000000000000000000000000000000000000000000000030AC
      D00030C8E00030ACD0002890C000505050005050500030ACD00030ACD0002890
      C00030ACD00000000000000000000000000000000000000000000000000030AC
      D00030C8E00030ACD00030ACD00030ACD00030ACD00050505000505050002890
      C00030ACD00000000000000000000000000000000000000000000000000030C8
      E00038E0F000606060006060600030C8E00030C8E000606060006060600030AC
      D00030C8E00000000000000000000000000000000000000000000000000030C8
      E00038E0F00030C8E00030C8E000606060006060600030C8E00030C8E00030AC
      D00030C8E00000000000000000000000000000000000000000000000000030C8
      E00038E0F00030C8E00030C8E00030C8E0002890C0006060600030C8E00030AC
      D00030C8E00000000000000000000000000000000000000000000000000030C8
      E00038E0F00030C8E00030C8E00030C8E0002890C0006060600030C8E00030AC
      D00030C8E00000000000000000000000000000000000000000000000000038E0
      F00040FCFF00707070007070700038E0F00038E0F000707070007070700030C8
      E00038E0F00000000000000000000000000000000000000000000000000038E0
      F00040FCFF0038E0F00038E0F000707070007070700038E0F00038E0F00030C8
      E00038E0F00000000000000000000000000000000000000000000000000038E0
      F00040FCFF0038E0F00038E0F00038E0F00038E0F000707070007070700030C8
      E00038E0F00000000000000000000000000000000000000000000000000038E0
      F00040FCFF0038E0F00038E0F00070707000707070002890C00038E0F00030C8
      E00038E0F00000000000000000000000000000000000000000000000000038E0
      F00040FCFF00707070007070700038E0F00038E0F000707070007070700030C8
      E00038E0F00000000000000000000000000000000000000000000000000038E0
      F00040FCFF0038E0F00030ACD000707070007070700038E0F00038E0F00030C8
      E00038E0F00000000000000000000000000000000000000000000000000038E0
      F00040FCFF00707070007070700038E0F00038E0F000707070007070700030C8
      E00038E0F00000000000000000000000000000000000000000000000000038E0
      F00040FCFF0038E0F00038E0F00038E0F00038E0F0007070700038E0F00030C8
      E00038E0F00000000000000000000000000000000000000000000000000030C8
      E00038E0F0002890C0006060600030C8E00030C8E000606060002890C00030AC
      D00030C8E00000000000000000000000000000000000000000000000000030C8
      E00038E0F00030C8E00030C8E000606060006060600030C8E00030C8E00030AC
      D00030C8E00000000000000000000000000000000000000000000000000030C8
      E00038E0F0002890C0006060600030C8E00030C8E000606060002890C00030AC
      D00030C8E00000000000000000000000000000000000000000000000000030C8
      E00038E0F00030C8E0006060600030C8E00030C8E000606060006060600030AC
      D00030C8E00000000000000000000000000000000000000000000000000030AC
      D00030C8E00030ACD0005050500050505000505050005050500030ACD0002890
      C00030ACD00000000000000000000000000000000000000000000000000030AC
      D00030C8E00030ACD00030ACD0002890C000505050002078B00030ACD0002890
      C00030ACD00000000000000000000000000000000000000000000000000030AC
      D00030C8E00030ACD0005050500050505000505050005050500030ACD0002890
      C00030ACD00000000000000000000000000000000000000000000000000030AC
      D00030C8E00030ACD000505050002078B00030ACD00050505000505050002890
      C00030ACD0000000000000000000000000000000000000000000000000002890
      C00030ACD0002890C0002890C0002890C0002890C0002890C0002890C0002078
      B0002890C0000000000000000000000000000000000000000000000000002890
      C00030ACD0002890C0002890C0002890C0002890C0002890C0002890C0002078
      B0002890C0000000000000000000000000000000000000000000000000002890
      C00030ACD0002890C0002890C0002890C0002890C0002890C0002890C0002078
      B0002890C0000000000000000000000000000000000000000000000000002890
      C00030ACD0002890C0002890C0002890C0002890C0002890C0002890C0002078
      B0002890C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      900018409000000000000000000000000000424D3E000000000000003E000000
      2800000040000000200000000100010000000000000100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFFFFFFFFFFE003E003E003E003
      E003E003E003E003E003E003E003E003E003E003E003E003E003E003E003E003
      E003E003E003E003E003E003E003E003E003E003E003E003E003E003E003E003
      E003E003E003E003E003E003E003E003E003E003E003E003E003E003E003E003
      E003E003E003E003E003E003E003E003}
  end
  object DataSource_Main: TDataSource
    Left = 88
    Top = 248
  end
  object DataSource_Detail: TDataSource
    Left = 80
    Top = 304
  end
  object DataSource_MainDetail: TDataSource
    Left = 80
    Top = 352
  end
  object PopupMenu1: TPopupMenu
    Left = 80
    Top = 24
    object aa2: TMenuItem
      Caption = 'aa'
    end
    object bb2: TMenuItem
      Caption = 'bb'
      object bb11: TMenuItem
        Caption = 'bb1'
      end
    end
    object cc2: TMenuItem
      Caption = 'cc'
    end
  end
  object ABDBMainDetailPopupMenu1: TABDBMainDetailPopupMenu
    DefaultParentValue = '0'
    Left = 88
    Top = 208
  end
  object ABSocket1: TABSocket
    LocalPort = 0
    ToPort = 0
    SocketType = stUDP
    SocketModel = smSelect
    BuffSize = 1024
    MaxError = 100
    ACKOnData = False
    Left = 8
    Top = 280
  end
end
