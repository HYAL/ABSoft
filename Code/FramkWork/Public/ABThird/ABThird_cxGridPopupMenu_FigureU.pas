{
cxGrid右键图形分析单元
}
unit ABThird_cxGridPopupMenu_FigureU;

interface                                                                   
{$I ..\ABInclude.ini}


uses
  ABPubFuncU,
  ABPubConstU,
  ABPubFormU,

  ABThirdFuncU,

  SysUtils, Variants, Classes, Controls, Forms,
  DB,StdCtrls, ExtCtrls,

  cxLookAndFeelPainters, Spin,
  cxCheckComboBox,
  cxGridCustomTableView,cxGridTableView,cxGridDBDataDefinitions,
  cxCustomData,cxGridDBTableView,


   cxDropDownEdit,

  VclTee.TeEngine, VclTee.Series, VclTee.TeeProcs, VclTee.Chart, cxCheckBox,
  cxGraphics, cxControls, cxLookAndFeels, cxContainer, cxEdit,
  VclTee.TeeGDIPlus, cxTextEdit, cxMaskEdit;

type
  TABcxGridPopupMenu_FigureForm = class(TABPubForm)
    Notebook1: TNotebook;
    GroupBox1: TGroupBox;
    ABcxLabel1: TLabel;
    ABcxLabel2: TLabel;
    ABcxComboBox1: TComboBox;
    ABcxCheckComboBox1: TcxCheckComboBox;
    ABcxButton1: TButton;
    ABcxButton2: TButton;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Button3: TButton;
    RadioGroup1: TRadioGroup;
    Button4: TButton;
    ComboBox1: TComboBox;
    cxSpinEdit1: TSpinEdit;
    Chart1: TChart;
    Series1: TBarSeries;
    procedure FormDestroy(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ABcxButton1Click(Sender: TObject);
    procedure ABcxButton2Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure cxSpinEdit1PropertiesChange(Sender: TObject);
  private
    tempFormWidth:LongInt;
    tempFormHeight:LongInt;
    BarSeries:array[0..10] of TChartSeries;
    procedure FreeBarSeries;
    procedure RefreshChar;
    procedure SetMarksStyle;
    procedure SetAngle;

    { Private declarations }
  public
    Dataset:TDataSet;
    View: TcxGridTableView;
    { Public declarations }
  end;

procedure ABcxGridPopupMenu_Figure(aView: TcxGridTableView);

implementation


{$R *.dfm}

procedure ABcxGridPopupMenu_Figure(aView: TcxGridTableView);
var
  tempForm:TABcxGridPopupMenu_FigureForm ;
  i,j:longint;
  tempSummaryItems: TcxDataGroupSummaryItems;
  tempstr1:string;
begin
  tempForm := TABcxGridPopupMenu_FigureForm.Create(nil);
  try
    tempForm.View:= aView;
    tempForm.Dataset:=TcxGridDBDataController(tempForm.View.DataController).DataSet;
    tempForm.Notebook1.ActivePage:='1';
    tempForm.tempFormWidth:=tempForm.Width;
    tempForm.tempFormHeight:=tempForm.Height;
    j:=(tempForm.tempFormWidth-tempForm.GroupBox1.Width);
    tempForm.Width:=300;
    tempForm.Height:=200;
    tempForm.GroupBox1.Width:=tempForm.Width-j;
    tempForm.ABcxComboBox1.Items.Clear;
    tempForm.ABcxCheckComboBox1.Properties.Items.Clear;
    if tempForm.View.DataController.RecordCount>0  then
    begin
      //处于分组情况下
      if tempForm.View.GroupedColumnCount>=1 then
      begin
        for I := 0 to tempForm.View.ColumnCount - 1 do
        begin
          //如果只有一级分组分析则要GroupIndex=0,否则GroupIndex>=0
          if tempForm.View.Columns[i].GroupIndex=0 then
          begin
            tempForm.ABcxComboBox1.Items.AddObject(
                                                               tempForm.View.Columns[i].Caption,
                                                               Pointer(tempForm.View.Columns[i].GroupIndex)
                                                               );
            //如果只有一级分组分析则要break
            break;
          end;                    
        end;

        tempSummaryItems := tempForm.View.DataController.Summary.GroupSummaryItems[0];
        for i := 0 to tempSummaryItems.Count - 1 do
        begin
          if tempSummaryItems[i].Position = spGroup then
          begin
            tempstr1:= TcxCustomGridTableItem(tempSummaryItems[i].Field.Item).Caption;
            tempForm.ABcxCheckComboBox1.Properties.Items.AddCheckItem(tempstr1);
            tempForm.ABcxCheckComboBox1.Properties.Items[tempForm.ABcxCheckComboBox1.Properties.Items.Count-1].ShortDescription:=tempstr1;
            tempForm.ABcxCheckComboBox1.Properties.Items[tempForm.ABcxCheckComboBox1.Properties.Items.Count-1].Tag:=i;
          end;
        end;
      end
      //未处于分组情况下
      else
      begin
        for I := 0 to tempForm.View.ColumnCount - 1 do
        begin
          if tempForm.View.Columns[i].Visible then
          begin
            tempForm.ABcxComboBox1.Items.AddObject(tempForm.View.Columns[i].Caption,Pointer(i));
            if  (ABPos(','+TcxGridDBColumn(tempForm.View.Columns[i]).DataBinding.ValueType+',', ','+'Smallint,Integer,Float,Currency,BCD,Word,Largeint,FMTBcd,'+',')>0) then
            begin
              tempForm.ABcxCheckComboBox1.Properties.Items.AddCheckItem(tempForm.View.Columns[i].Caption);
              tempForm.ABcxCheckComboBox1.Properties.Items[tempForm.ABcxCheckComboBox1.Properties.Items.Count-1].ShortDescription:=tempForm.View.Columns[i].Caption;
              tempForm.ABcxCheckComboBox1.Properties.Items[tempForm.ABcxCheckComboBox1.Properties.Items.Count-1].Tag:=i
            end;
          end;
        end;
      end;

      if tempForm.ABcxComboBox1.Items.Count>0 then
      begin
        tempForm.ABcxComboBox1.ItemIndex:=0;
      end;

      if tempForm.ABcxCheckComboBox1.Properties.Items.Count>0 then
      begin
        tempForm.ABcxCheckComboBox1.ItemIndex:=0;
        tempForm.ABcxCheckComboBox1.SetItemState(0,cbsChecked);
      end;
    end;

    tempForm.ShowModal ;
  finally
    for I := high(tempForm.BarSeries) downto low(tempForm.BarSeries) do
    begin
      if Assigned(tempForm.BarSeries[i]) then
        tempForm.BarSeries[i].Free;
    end;
    tempForm.Free;
  end;
end;

{ TABcxGridPopupMenu_FigureForm }

procedure TABcxGridPopupMenu_FigureForm.Button4Click(Sender: TObject);
begin
  //EditChart(self,chart1);
end;

procedure TABcxGridPopupMenu_FigureForm.ComboBox1Change(Sender: TObject);
begin
  SetMarksStyle ;
end;

procedure TABcxGridPopupMenu_FigureForm.cxSpinEdit1PropertiesChange(
  Sender: TObject);
begin
  SetAngle;
end;

procedure TABcxGridPopupMenu_FigureForm.SetAngle;
var
  i:LongInt;
begin
  for I := low(BarSeries) to high(BarSeries) do
  begin
    if Assigned(BarSeries[i]) then
    begin
      if (ABIsPublishProp(BarSeries[i].Marks,'Angle')) then
      begin
        ABSetPropValue(BarSeries[i].Marks,'Angle',cxSpinEdit1.Value);
      end;
    end;                
  end;
end;

{
var
  tempDropDownMenu:Tobject;
begin
  inherited;
  if (ABIsPublishProp(self,'DropDownMenu')) then
  begin
    tempDropDownMenu:=ABGetObjectPropValue(self,'DropDownMenu');
    if (Assigned(tempDropDownMenu)) then
      TPopupMenu(tempDropDownMenu).Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
  end;
end;
}

procedure TABcxGridPopupMenu_FigureForm.SetMarksStyle;
var
  i:LongInt;
  tempMarksStyle:TSeriesMarksStyle;
begin
  if ComboBox1.ItemIndex=-1 then
    ComboBox1.ItemIndex:=1;

  case ComboBox1.ItemIndex of
    0:tempMarksStyle:=smsXValue;
    1:tempMarksStyle:=smsValue;
    {$IF (ABDelphiVersion_int>=200)}2:tempMarksStyle:=smsXY; {$IFEND}
    3:tempMarksStyle:=smsPercent;
    4:tempMarksStyle:=smsLabelPercent;
    5:tempMarksStyle:=smsPercentTotal;
    6:tempMarksStyle:=smsLabelPercentTotal;
  else
    tempMarksStyle:=smsValue;
  end;

  for I := low(BarSeries) to high(BarSeries) do
  begin
    if Assigned(BarSeries[i]) then
    begin
      BarSeries[i].Marks.Style:=tempMarksStyle
    end;
  end;
end;

procedure TABcxGridPopupMenu_FigureForm.FormCreate(Sender: TObject);
begin
  ComboBox1.Clear;
  ComboBox1.Items.Add(('X数值'));
  ComboBox1.Items.Add(('Y数值'));
  {$IF (ABDelphiVersion_int>=200)}ComboBox1.Items.Add(('XY数值')); {$IFEND}
  ComboBox1.Items.Add(('百分比'));
  ComboBox1.Items.Add(('X数值+百分比'));
  ComboBox1.Items.Add(('百分比+总数'));
  ComboBox1.Items.Add(('X数值+百分比+总数'));
end;

procedure TABcxGridPopupMenu_FigureForm.FormDestroy(Sender: TObject);
begin
  FreeBarSeries;
end;

procedure TABcxGridPopupMenu_FigureForm.RadioGroup1Click(Sender: TObject);
begin
  Chart1.Title.Visible:= RadioGroup1.ItemIndex =1;
  RefreshChar;
  if RadioGroup1.ItemIndex =1 then
    Chart1.Title.Text.Text:=EmptyStr
  else
    Chart1.Title.Text.Text:=('按下右键可拖动图表,左键可选择放大或缩小.');

  ComboBox1.Enabled:=(RadioGroup1.ItemIndex=0) or (RadioGroup1.ItemIndex=1);
end;

procedure TABcxGridPopupMenu_FigureForm.RefreshChar;
var
  i,j:longint;
  tempXVariant:Variant;
  tempYVariant:Variant;
  tempSummaryItems: TcxDataGroupSummaryItems;
  tempGroups:TcxDataControllerGroups;
begin
  FreeBarSeries;

  j:=0;
  for I := 0 to ABcxCheckComboBox1.Properties.Items.Count - 1 do
  begin
    if ABcxCheckComboBox1.States[i]=cbsChecked  then
    begin
      case RadioGroup1.ItemIndex of
        0:     //柱形
        begin
          BarSeries[j]:= TBarSeries.Create(nil);
        end;
        1:    //饼形
        begin
          BarSeries[j]:= TPieSeries.Create(nil);
        end;
        2:    //线形
        begin
          BarSeries[j]:= TLineSeries.Create(nil);
        end;
        3:    //面积
        begin
          BarSeries[j]:= TAreaSeries.Create(nil);
        end;
      end;


      BarSeries[j].ParentChart:=Chart1;

      BarSeries[j].Title:=ABcxCheckComboBox1.Properties.Items[i].ShortDescription;
      ABcxCheckComboBox1.Properties.Items[i].Description:=IntToStr(j);
      j:=j+1;
    end;
  end;
  SetMarksStyle ;
  SetAngle;


  View.DataController.BeginUpdate;
  try
    //处于分组情况下
    if View.GroupedColumnCount>=1 then
    begin
      tempGroups:=View.DataController.Groups;
      tempSummaryItems := View.DataController.Summary.GroupSummaryItems[0];
     
      for I := 0 to tempGroups.ChildCount[-1] - 1 do
      begin
        tempXVariant:=tempGroups.GroupValues[i];
        if tempXVariant=null  then
           tempXVariant:=EmptyStr;
        if (AnsiCompareText(tempXVariant,('小计'))<>0) and
           (AnsiCompareText(tempXVariant,('总计'))<>0) and
           (AnsiCompareText(tempXVariant,('合计'))<>0)
           then
        begin
          for j := 0 to ABcxCheckComboBox1.Properties.Items.Count - 1 do
          begin
            if ABcxCheckComboBox1.States[j]=cbsChecked  then
            begin
              if tempSummaryItems[ABcxCheckComboBox1.Properties.Items[j].tag].Position = spGroup then
              begin
                tempYVariant:=View.DataController.Summary.GroupSummaryValues[i,ABcxCheckComboBox1.Properties.Items[j].tag];
                if tempYVariant=null  then
                   tempYVariant:=0;

                BarSeries[strtoint(ABcxCheckComboBox1.Properties.Items[j].Description)].Add
                          (tempYVariant,tempXVariant);
              end;
            end;
          end;
        end;
      end;
    end
    //未处于分组情况下
    else
    begin
      for i:=0 to View.DataController.RowCount-1 do
      begin
        tempXVariant:=ABGetColumnValue(View.Columns[LongInt(ABcxComboBox1.Items.Objects[ABcxComboBox1.ItemIndex])],i);

        if tempXVariant=null  then
           tempXVariant:=EmptyStr;
        if (AnsiCompareText(tempXVariant,('小计'))<>0) and
           (AnsiCompareText(tempXVariant,('总计'))<>0) and
           (AnsiCompareText(tempXVariant,('合计'))<>0)
           then
        begin
          for j := 0 to ABcxCheckComboBox1.Properties.Items.Count - 1 do
          begin
            if ABcxCheckComboBox1.States[j]=cbsChecked  then
            begin
              tempYVariant:=ABGetColumnValue(View.Columns[ABcxCheckComboBox1.Properties.Items[j].tag],i);

              if (tempYVariant=null) or (VarToStrDef(tempYVariant,'')=EmptyStr)  then
                 tempYVariant:=0;

              BarSeries[strtoint(ABcxCheckComboBox1.Properties.Items[j].Description)].Add
                        (tempYVariant,tempXVariant);
            end;
          end;
        end;
      end;
    end;
  finally
    View.DataController.EndUpdate;
  end;
                                       
end;

procedure TABcxGridPopupMenu_FigureForm.ABcxButton1Click(Sender: TObject);
begin
  ModalResult:=mrNo;
end;

procedure TABcxGridPopupMenu_FigureForm.ABcxButton2Click(Sender: TObject);
begin
  if trim(ABcxComboBox1.Text)<>EmptyStr then
  begin
    Notebook1.ActivePage:='2';
    Top:=Top-ABTrunc(Height/2);
    Left:=Left-ABTrunc(Width/2);

    Width:=tempFormWidth;
    Height:=tempFormHeight;

    Chart1.Legend.TextStyle:=ltsRightPercent;
    RefreshChar;
  end;
end;

procedure TABcxGridPopupMenu_FigureForm.Button3Click(Sender: TObject);
begin
  close;
end;

procedure TABcxGridPopupMenu_FigureForm.FreeBarSeries;
var
  i :longint;
begin
  Chart1.SeriesList.Clear;
  for I := low(BarSeries) to high(BarSeries) do
  begin
    if Assigned(BarSeries[i]) then
    begin
      BarSeries[i]:=nil;
    end;
  end;
end;


end.
