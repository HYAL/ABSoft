{
cxDataController全文检索单元
DataController.Search.LocateNext方式只在DataController.Search.Locate找到时才会有效
}
unit ABThirdcxGridAndcxTreeViewSearchU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,

  ABThirdFuncU,

  cxCustomData,
  cxGrid,
  cxDBTL,
  cxGridTableView,

  SysUtils,Forms,StdCtrls,Controls,Classes,Buttons;

type
  TABcxDataControllerAllLocalForm = class(TABPubForm)
    Edit1: TEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    procedure Edit1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
  private
    FControl: TWinControl;
    function Locate(aDownUpType: Integer): Boolean;
    { Private declarations }
  public
    property Control:TWinControl read FControl write FControl;
    { Public declarations }
  end;

//在传入控件cxDataController的所有列中查找
procedure ABcxDataControllerAllLocal(aControl:TWinControl);

implementation

{$R *.dfm}
var
  ABcxDataControllerAllLocalForm: TABcxDataControllerAllLocalForm;

function ABGetcxDataControllerAllLocalForm:TABcxDataControllerAllLocalForm;
begin
  if not Assigned(ABcxDataControllerAllLocalForm) then
    ABcxDataControllerAllLocalForm := TABcxDataControllerAllLocalForm.Create(nil);

  Result:=ABcxDataControllerAllLocalForm;
end;

procedure ABcxDataControllerAllLocal(aControl:TWinControl);
begin
  ABGetcxDataControllerAllLocalForm.Control:=aControl;
  ABGetcxDataControllerAllLocalForm.ShowModal;
end;

{ TABcxGridPopupMenu_AllLocalForm }

procedure TABcxDataControllerAllLocalForm.Edit1Change(Sender: TObject);
begin
  Locate(0);
end;

procedure TABcxDataControllerAllLocalForm.FormShow(Sender: TObject);
begin
  if Edit1.CanFocus then
    Edit1.SetFocus;
  Edit1.Text:=EmptyStr;
end;

function TABcxDataControllerAllLocalForm.Locate(aDownUpType: LongInt):Boolean;
begin
  result:=False;
  if Edit1.Text=EmptyStr then
    exit;

  if FControl is TcxDBTreeList then
  begin
    result:=ABSearchIncxTreeList(TcxDBTreeList(FControl),Edit1.Text,aDownUpType);
  end
  else if FControl is Tcxgrid then
  begin
    result:=ABSearchIncxTableView(TcxGridTableView(Tcxgrid(FControl).ActiveView),Edit1.Text,aDownUpType);
  end;
end;

procedure TABcxDataControllerAllLocalForm.SpeedButton1Click(Sender: TObject);
begin
  Locate(0);
end;

procedure TABcxDataControllerAllLocalForm.SpeedButton2Click(Sender: TObject);
begin
  Locate(-1);
end;

procedure TABcxDataControllerAllLocalForm.SpeedButton3Click(Sender: TObject);
begin
  Locate(1);
end;

procedure ABFinalization;
begin
  if Assigned(ABcxDataControllerAllLocalForm) then
    ABcxDataControllerAllLocalForm.Free;
end;

Initialization

Finalization
  ABFinalization;

end.
