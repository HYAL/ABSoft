unit ABThirdTestU;

interface

uses
  ABPubVarU,
  ABPubFuncU,
  ABPubConstU,


  ABPubShowEditDatasetU,
  ABThirdImportDataU,

{
  ABThirdConnServerU                     ,

  ABThirdcxGridAndcxTreeViewSearchU      ,
  ABThird_cxGridPopupMenu_AddColumU      ,
  ABThird_cxGridPopupMenu_ColorSetupU    ,
  ABThird_cxGridPopupMenu_FigureU        ,
  ABThird_cxGridPopupMenu_PrintU         ,
  ABThird_DevExtPopupMenuU               ,

  ABThirdRegU                            ,
  }
  ABThirdDBU                             ,
  ABThirdConnDatabaseU                                        ,
  ABThirdConnU                           ,

  ABThirdFuncU                                                ,

  ABThirdCacheDatasetU                   ,
  ABThirdCustomQueryU                    ,
  ABThirdQueryU                                                           ,
  ABThird_DevExtPopupMenuU                                           ,



  NativeXml,

  TypInfo,Types,
  DBClient, ShlObj,
  Forms, Windows, Messages, SysUtils, Variants, Classes, Controls, ExtCtrls,
  DBGrids, DB, StdCtrls,
  ComCtrls, shellapi, StdConvs, DBCtrls, ImgList, Graphics, jpeg, Menus,
  Dialogs, OleServer, ADODB,
  dxCore,
  CheckLst, Grids, ABPubFormU,  cxGraphics,

  cxFilter, cxGridLevel,
  cxGridCustomTableView, cxGridTableView,
  cxGrid, dxNavBar, dxNavBarCollns,
  cxRadioGroup, cxGridDBBandedTableView, cxCheckListBox,
  cxTextEdit, cxDBEdit, dxBar, cxDropDownEdit, dxStatusBar,
  cxDBPivotGrid,


  FireDAC.Comp.DataSet, FireDAC.Comp.Client, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, cxContainer, cxClasses, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys,
  cxCustomPivotGrid, cxMaskEdit, cxGroupBox, dxNavBarBase,
  cxGridBandedTableView, cxGridCustomView, FireDAC.Moni.Base,
  FireDAC.Moni.RemoteClient, cxCheckBox, cxCheckComboBox;

type
  TABTestForm = class(TABPubForm)
    PageControl3: TPageControl;
    TabSheet1: TTabSheet;
    Panel8: TPanel;
    Memo4: TMemo;
    Memo3: TMemo;
    ProgressBar2: TProgressBar;
    TabSheet5: TTabSheet;
    Panel10: TPanel;
    Button52: TButton;
    Button6: TButton;
    Panel1: TPanel;
    GroupBox38: TGroupBox;
    Button57: TButton;
    GroupBox1: TGroupBox;
    Button1: TButton;
    GroupBox2: TGroupBox;
    Button2: TButton;
    GroupBox3: TGroupBox;
    Button3: TButton;
    GroupBox4: TGroupBox;
    Button4: TButton;
    GroupBox5: TGroupBox;
    Button5: TButton;
    GroupBox6: TGroupBox;
    Button7: TButton;
    GroupBox7: TGroupBox;
    Button8: TButton;
    Panel2: TPanel;
    GroupBox8: TGroupBox;
    Button9: TButton;
    GroupBox9: TGroupBox;
    Button10: TButton;
    GroupBox10: TGroupBox;
    Button11: TButton;
    GroupBox11: TGroupBox;
    Button12: TButton;
    GroupBox12: TGroupBox;
    Button13: TButton;
    GroupBox13: TGroupBox;
    Button14: TButton;
    GroupBox14: TGroupBox;
    Button15: TButton;
    GroupBox15: TGroupBox;
    Button16: TButton;
    Panel3: TPanel;
    GroupBox23: TGroupBox;
    Button17: TButton;
    DataSource1: TDataSource;
    ABThirdQuery1: TABThirdQuery;
    dxNavBar1: TdxNavBar;
    dxNavBar1Group1: TdxNavBarGroup;
    RadioGroup1: TRadioGroup;
    cxRadioGroup1: TcxRadioGroup;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBBandedTableView1: TcxGridDBBandedTableView;
    cxGrid1DBBandedTableView1TA_TI_Guid: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TA_CL_Guid: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TA_Guid: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TA_Type: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TA_ID: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TA_Name: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TA_Caption: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TA_IsTail: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TA_MaxRecord: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TA_CanInsert: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TA_CanDelete: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TA_CanEdit: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TA_CanPrint: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TA_SetInsertCommand: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TA_SetEditCommand: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TA_SetPostCommand: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TA_SetDeleteCommand: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1TA_Order: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1PubID: TcxGridDBBandedColumn;
    ABcxGridPopupMenu1: TABcxGridPopupMenu;
    cxGrid1DBBandedTableView1a: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1b: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1c: TcxGridDBBandedColumn;
    cxCheckListBox1: TcxCheckListBox;
    cxTextEdit1: TcxTextEdit;
    cxDBTextEdit1: TcxDBTextEdit;
    aaaa: TdxBarManager;
    inheritedBar: TdxBar;
    btnCustom1: TdxBarLargeButton;
    btnCustom2: TdxBarLargeButton;
    btnCustom3: TdxBarLargeButton;
    btnCustom4: TdxBarLargeButton;
    btnCustom5: TdxBarLargeButton;
    cxComboBox1: TcxComboBox;
    cxGrid2: TcxGrid;
    cxGridDBBandedTableView1: TcxGridDBBandedTableView;
    cxGridDBBandedColumn1: TcxGridDBBandedColumn;
    cxGridDBBandedColumn2: TcxGridDBBandedColumn;
    cxGridDBBandedColumn3: TcxGridDBBandedColumn;
    cxGridDBBandedColumn4: TcxGridDBBandedColumn;
    cxGridDBBandedColumn5: TcxGridDBBandedColumn;
    cxGridDBBandedColumn6: TcxGridDBBandedColumn;
    cxGridDBBandedColumn7: TcxGridDBBandedColumn;
    cxGridDBBandedColumn8: TcxGridDBBandedColumn;
    cxGridDBBandedColumn9: TcxGridDBBandedColumn;
    cxGridDBBandedColumn10: TcxGridDBBandedColumn;
    cxGridDBBandedColumn11: TcxGridDBBandedColumn;
    cxGridDBBandedColumn12: TcxGridDBBandedColumn;
    cxGridDBBandedColumn13: TcxGridDBBandedColumn;
    cxGridDBBandedColumn14: TcxGridDBBandedColumn;
    cxGridDBBandedColumn15: TcxGridDBBandedColumn;
    cxGridDBBandedColumn16: TcxGridDBBandedColumn;
    cxGridDBBandedColumn17: TcxGridDBBandedColumn;
    cxGridDBBandedColumn18: TcxGridDBBandedColumn;
    cxGridDBBandedColumn19: TcxGridDBBandedColumn;
    cxGridDBBandedColumn20: TcxGridDBBandedColumn;
    cxGridDBBandedColumn21: TcxGridDBBandedColumn;
    cxGridDBBandedColumn22: TcxGridDBBandedColumn;
    cxGridLevel1: TcxGridLevel;
    dxStatusBar1: TdxStatusBar;
    ABcxPivotGridPopupMenu1: TABcxPivotGridPopupMenu;
    ABThirdQuery2: TABThirdQuery;
    cxDBPivotGrid1: TcxDBPivotGrid;
    FireDACConnection1: TFireDACConnection;
    FireDACQuery1: TFireDACQuery;
    FDQuery1: TFDQuery;
    FDMemTable1: TFDMemTable;
    FDMoniRemoteClientLink1: TFDMoniRemoteClientLink;
    cxCheckComboBox1: TcxCheckComboBox;
    procedure Button52Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Memo3KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Button17Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure cxGrid1DBBandedTableView1CustomDrawIndicatorCell(
      Sender: TcxGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxCustomGridIndicatorItemViewInfo; var ADone: Boolean);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button57Click(Sender: TObject);
    procedure Button16Click(Sender: TObject);
    procedure Button15Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cxCheckComboBox1PropertiesEditValueToStates(Sender: TObject;
      const AValue: Variant; var ACheckStates: TcxCheckStates);
    procedure cxCheckComboBox1PropertiesStatesToEditValue(Sender: TObject;
      const ACheckStates: TcxCheckStates; out AValue: Variant);
  private
    tempList: TList;
    FMemoryStream: TMemoryStream;
    FADOConnection: TADOConnection;
    FpriDatetime:TDateTime;
  private
    procedure Addlog(aMemo:TMemo;atext: string);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABTestForm: TABTestForm;

implementation
{$R *.dfm}


procedure TABTestForm.FormCreate(Sender: TObject);
begin
  tempList:= TList.Create;
  FMemoryStream:=TMemoryStream.Create;
  FpriDatetime:=Now;
  FADOConnection:= TADOConnection.Create(nil);
  FADOConnection.LoginPrompt:=false;
end;

procedure TABTestForm.FormDestroy(Sender: TObject);
begin
  tempList.Free;
  FMemoryStream.Free;
end;

procedure TABTestForm.Memo3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Shift=[ssCtrl]) then
  begin
    if  (Key=65) then
    begin
      TMemo(Sender).SelectAll;
    end;
  end;
end;

procedure TABTestForm.Button17Click(Sender: TObject);
begin
  ABThirdQuery1.Close;
  ABThirdQuery1.open;
end;

procedure TABTestForm.Button52Click(Sender: TObject);
begin
  Memo3.Clear;
end;

procedure TABTestForm.Addlog(aMemo:TMemo;atext:string);
var
  tempDatetime:TDateTime;
begin
  tempDatetime:=Now;
  aMemo.Lines.Add('时间:'+ABDateTimeToStr(tempDatetime,23)+' '+
                  '距上次日志['+ABRoundStr(ABGetDateTimeSpan_Float(FpriDatetime,tempDatetime,tuMilliSeconds),2,true)+'毫秒] '+
                  atext);
  FpriDatetime:=tempDatetime;
end;

procedure TABTestForm.Button6Click(Sender: TObject);
begin
  Memo3.Clear;
  Memo4.Clear;
end;

procedure TABTestForm.Button8Click(Sender: TObject);
begin
  Addlog(Memo3,'ABThirdConnDatabaseU ABThirdConnDatabaseU.ABSetConn(false 设置数据库连接列表):'+ABBoolToStr(ABThirdConnDatabaseU.ABSetConn(false)));
  Addlog(Memo3,'ABThirdConnDatabaseU ABThirdConnDatabaseU.ABSetConn(True 设置数据库连接列表):'+ABBoolToStr(ABThirdConnDatabaseU.ABSetConn(True)));
  Addlog(Memo3,'ABThirdConnDatabaseU ABAddToConnList(手工增加连接到两层的连接列表中):'+ABBoolToStr(ABAddToConnList('112233',FireDACConnection1)));

  Addlog(Memo3,'ABThirdConnDatabaseU ABThirdConnU.ABIsConn(Main 检测连接名称是否连接到数据库):'+ABBoolToStr(ABThirdConnU.ABIsConn('Main')));
  Addlog(Memo3,'ABThirdConnDatabaseU ABThirdConnU.ABIsConn(Bug 检测连接名称是否连接到数据库):'+ABBoolToStr(ABThirdConnU.ABIsConn('Bug')));
  Addlog(Memo3,'ABThirdConnDatabaseU ABThirdConnU.ABIsConn(ERP 检测连接名称是否连接到数据库):'+ABBoolToStr(ABThirdConnU.ABIsConn('ERP')));
  Addlog(Memo3,'ABThirdConnDatabaseU ABThirdConnU.ABIsConn(aaaa 检测连接名称是否连接到数据库):'+ABBoolToStr(ABThirdConnU.ABIsConn('aaaa')));

  if ABGetIndexByConnName('ERP')>=0 then
    Addlog(Memo3,'ABThirdConnDatabaseU ABGetIndexByConnName(ERP 根据连接名称从连接列表中取得连接序号):'+inttostr(ABGetIndexByConnName('ERP')));
  if Assigned(ABGetConnInfoByConnName('ERP').Conn) then
    Addlog(Memo3,'ABThirdConnDatabaseU ABGetConnInfoByConnName(ERP 根据连接名称从连接列表中取得连接结构):'+ABGetConnInfoByConnName('ERP').Conninfo.ConnName);
  if Assigned(ABGetConnInfoByConnName('aaaa').Conn) then
    Addlog(Memo3,'ABThirdConnDatabaseU ABGetConnInfoByConnName(aaaa 根据连接名称从连接列表中取得连接结构):'+ABGetConnInfoByConnName('aaaa').Conninfo.ConnName);

  if Assigned(ABGetConnInfoByConn(FireDACConnection1).Conn) then
    Addlog(Memo3,'ABThirdConnDatabaseU ABGetConnInfoByConn(ERP 根据连接对象从连接列表中取得连接结构):'+ABGetConnInfoByConn(FireDACConnection1).Conninfo.ConnName);
 {
  ADQuery1.close;
  Addlog(Memo3,'ABThirdConnDatabaseU ABThirdConnU.ABSetDatasetConnByConnName(ADQuery1,Main 根据连接名称设置数据集的两层连接):'+ABBoolToStr(ABThirdConnU.ABSetDatasetConnByConnName(ADQuery1,'Main')));
  ADQuery1.SQL.Text:=' select * from ABSys_Org_Table ';
  ADQuery1.open;
  Addlog(Memo3,'ABThirdConnDatabaseU ADQuery1.recordcount:'+intToStr(ADQuery1.recordcount));

  ADQuery1.close;
  Addlog(Memo3,'ABThirdConnDatabaseU ABThirdConnU.ABSetDatasetConnByConnName(ADQuery1,ERP 根据连接名称设置数据集的两层连接):'+ABBoolToStr(ABThirdConnU.ABSetDatasetConnByConnName(ADQuery1,'ERP')));
  ADQuery1.SQL.Text:=' select * from ABERP_Base_Cust ';
  ADQuery1.open;
  Addlog(Memo3,'ABThirdConnDatabaseU ADQuery1.recordcount:'+intToStr(ADQuery1.recordcount));
  }
end;

procedure TABTestForm.Button7Click(Sender: TObject);
var
  tempConnParams:TABConnParams;
begin
  Addlog(Memo3,'ABPubDBU ABGetBooleanFilter(到到布尔字段的filter串):'+ABGetBooleanFilter(FireDACQuery1,'code',true));
  Addlog(Memo3,'ABThirdDBU ABGetADOConnectionUDLConnStr(打开UDL连接框并返回ADOConnection连接串):'+ABGetADOConnectionUDLConnStr(FADOConnection));

  Addlog(Memo3,'ABThirdDBU ABOpenADOConnectionByUDL(以按定UDL文件连接,失败时弹出连接框):'+ABBoolToStr(ABOpenADOConnectionByUDL(FADOConnection,'')));
  Addlog(Memo3,'ABThirdDBU ABGetADOConnectionConnStr(根据连接参数得到ADOConnection控件的连接串):'+ABGetADOConnectionConnStr(dtSQLServer,'1','2','3','4'));

  tempConnParams:=ABGetConnParams(FADOConnection);
  Addlog(Memo3,'ABThirdDBU ABGetConnParams(根据TCustomConnection取得Connection的信息):'+tempConnParams.Host+' '+tempConnParams.Database+' '+tempConnParams.UserName+' '+tempConnParams.Password);
  ABSetConnParams(tempConnParams,FADOConnection);
  Addlog(Memo3,'ABThirdDBU ABSetConnParams(设置Connection的信息):');
  tempConnParams:=ABGetConnParams(dtSQLServer,tempConnParams.Host,tempConnParams.Database,tempConnParams.UserName,tempConnParams.Password);
  Addlog(Memo3,'ABThirdDBU ABGetConnParams(根据连接描述取得Connection的信息):'+tempConnParams.Host+' '+tempConnParams.Database+' '+tempConnParams.UserName+' '+tempConnParams.Password);

  ABInitConnection(FADOConnection,dtSQLServer);
  Addlog(Memo3,'ABThirdDBU ABInitConnection(初始化Connection的一些属性):');

  Addlog(Memo3,'ABThirdDBU ABOpenConnection(打开数据库连接):'+ABBoolToStr(ABOpenConnection(FADOConnection,dtSQLServer,tempConnParams.Host,tempConnParams.Database,tempConnParams.UserName,tempConnParams.Password)));
  Addlog(Memo3,'ABThirdDBU ABOpenConnection(打开数据库连接):'+ABBoolToStr(ABOpenConnection(FADOConnection,tempConnParams)));

  {
  ABThirdConnDatabaseU.ABSetConn(false);
  ADQuery1.close;
  Addlog(Memo3,'ABThirdConnDatabaseU ABThirdConnU.ABSetDatasetConnByConnName(ADQuery1,Main 根据连接名称设置数据集的两层连接):'+ABBoolToStr(ABThirdConnU.ABSetDatasetConnByConnName(ADQuery1,'Main')));
  ADQuery1.SQL.Text:=' select Ta_Guid,Ta_Name,Ta_Ti_Guid,Pubid from ABSys_Org_Table ';
  ADQuery1.open;
  ABCloneCursor(ADQuery1);

  ABReFreshQuery(ADQuery1,[]);
  Addlog(Memo3,'ABThirdDBU ABReFreshQuery(ADQuery1 刷新数据集):');

  Addlog(Memo3,'ABThirdDBU ABGetDatasetSQL(取得数据集SQL):'+ABGetDatasetSQL(ADQuery1));
  ABSetDatasetSQL(ADQuery1,'select * from table where [ABUser_HostIP]='''' and ''''=:ABUser_HostName and :tempParam2<>null');
  ADQuery1.Params.FindParam('tempParam2').DataType:=ftBlob;
  Addlog(Memo3,'ABThirdDBU ABGetDatasetSQL(取得数据集SQL):'+ABGetDatasetSQL(ADQuery1));

  Addlog(Memo3,'ABThirdDBU ABGetDatasetSQL_HaveParamValue(取得Dataset的包含参数值的SQL):'+ABGetDatasetSQL_HaveParamValue(ADQuery1));
  ABSetDatasetAutoParamsAndSQLParams(ADQuery1);
  Addlog(Memo3,'ABThirdDBU ABSetDatasetAutoParamsAndSQLParams(设置数据集SQL及数据集中引用的自动参数值):');
  Addlog(Memo3,'ABThirdDBU ABGetDatasetSQL_HaveParamValue(取得Dataset的包含参数值的SQL):'+ABGetDatasetSQL_HaveParamValue(ADQuery1));

  Addlog(Memo3,'ABThirdDBU ABGetParamsCount(取得参数的数目):'+inttostr(ABGetParamsCount(ADQuery1)));
  Addlog(Memo3,'ABThirdDBU ABGetParamType(得到指定参数的类型):'+GetEnumName(TypeInfo(TFieldType), Ord(ABGetParamType(ADQuery1,'ABUser_HostName'))));
  Addlog(Memo3,'ABThirdDBU ABGetParamType(得到指定参数的类型):'+GetEnumName(TypeInfo(TFieldType), Ord(ABGetParamType(ADQuery1,0))));

  ABSetParamType(ADQuery1,'ABUser_HostName',ftInteger);
  Addlog(Memo3,'ABThirdDBU ABSetParamType(设置指定参数的类型):ftInteger');
  Addlog(Memo3,'ABThirdDBU ABGetParamType(得到指定参数的类型):'+GetEnumName(TypeInfo(TFieldType), Ord(ABGetParamType(ADQuery1,'ABUser_HostName'))));
  Addlog(Memo3,'ABThirdDBU ABGetParamType(得到指定参数的类型):'+GetEnumName(TypeInfo(TFieldType), Ord(ABGetParamType(ADQuery1,0))));

  ABSetParamType(ADQuery1,0,ftUnknown);
  Addlog(Memo3,'ABThirdDBU ABSetParamType(设置指定参数的类型):ftUnknown');
  Addlog(Memo3,'ABThirdDBU ABGetParamType(得到指定参数的类型):'+GetEnumName(TypeInfo(TFieldType), Ord(ABGetParamType(ADQuery1,'ABUser_HostName'))));
  Addlog(Memo3,'ABThirdDBU ABGetParamType(得到指定参数的类型):'+GetEnumName(TypeInfo(TFieldType), Ord(ABGetParamType(ADQuery1,0))));

  ABSetUnknownParamType(ADQuery1,nil,ftString);
  Addlog(Memo3,'ABThirdDBU ABSetUnknownParamType(设置未定义的参数类型):ftString');
  Addlog(Memo3,'ABThirdDBU ABGetParamType(得到指定参数的类型):'+GetEnumName(TypeInfo(TFieldType), Ord(ABGetParamType(ADQuery1,'ABUser_HostName'))));
  Addlog(Memo3,'ABThirdDBU ABGetParamType(得到指定参数的类型):'+GetEnumName(TypeInfo(TFieldType), Ord(ABGetParamType(ADQuery1,0))));

  Addlog(Memo3,'ABThirdDBU ABGetParam(取得参数项):'+inttostr(ABGetParam(ADQuery1,'ABUser_HostName').Index));
  Addlog(Memo3,'ABThirdDBU ABGetParamValue(取得参数的值):'+VarToStr(ABGetParamValue(ADQuery1,'ABUser_HostName')));
  Addlog(Memo3,'ABThirdDBU ABGetParamValue(取得参数的值):'+VarToStr(ABGetParamValue(ADQuery1,0)));

  ABSetParamValue(ADQuery1,'ABUser_HostName',200);
  Addlog(Memo3,'ABThirdDBU ABSetParamValue(设置参数的值):200');
  Addlog(Memo3,'ABThirdDBU ABGetParamValue(取得参数的值):'+VarToStr(ABGetParamValue(ADQuery1,'ABUser_HostName')));
  Addlog(Memo3,'ABThirdDBU ABGetParamValue(取得参数的值):'+VarToStr(ABGetParamValue(ADQuery1,0)));

  ABSetParamValue(ADQuery1,0,300);
  Addlog(Memo3,'ABThirdDBU ABSetParamValue(设置参数的值):300');
  Addlog(Memo3,'ABThirdDBU ABGetParamValue(取得参数的值):'+VarToStr(ABGetParamValue(ADQuery1,'ABUser_HostName')));
  Addlog(Memo3,'ABThirdDBU ABGetParamValue(取得参数的值):'+VarToStr(ABGetParamValue(ADQuery1,0)));

  ABSetParamValue(ADQuery1,[400]);
  Addlog(Memo3,'ABThirdDBU ABSetParamValue(设置参数的值):400');
  Addlog(Memo3,'ABThirdDBU ABGetParamValue(取得参数的值):'+VarToStr(ABGetParamValue(ADQuery1,'ABUser_HostName')));
  Addlog(Memo3,'ABThirdDBU ABGetParamValue(取得参数的值):'+VarToStr(ABGetParamValue(ADQuery1,0)));

  ABSetParamValue(ADQuery1,1,FMemoryStream);
  Addlog(Memo3,'ABThirdDBU ABSetParamValue(设置参数的值):FMemoryStream');
  Addlog(Memo3,'ABThirdDBU ABCheckDatasetParamsHaveValue(检测所有的参数是否都已有值):'+abbooltostr(ABCheckDatasetParamsHaveValue(ADQuery1)));

  FMemoryStream.LoadFromFile('C:\WINDOWS\system32\calc.exe');
  ABSetParamValue(ADQuery1,'tempParam2',FMemoryStream);
  Addlog(Memo3,'ABThirdDBU ABSetParamValue(设置参数的值):FMemoryStream');
  Addlog(Memo3,'ABThirdDBU ABCheckDatasetParamsHaveValue(检测所有的参数是否都已有值):'+abbooltostr(ABCheckDatasetParamsHaveValue(ADQuery1)));

  ABSetDatasetAutoParams(ADQuery1);
  Addlog(Memo3,'ABThirdDBU ABSetDatasetAutoParams(设置数据集中引用的自动参数值):');

  Addlog(Memo3,'ABThirdDBU ABGetParamName(取得参数的名称):'+ABGetParamName(ADQuery1,0));
  Addlog(Memo3,'ABThirdDBU ABGetParamName(取得参数的名称):'+ABGetParamName(ADQuery1,1));
  }
end;

procedure TABTestForm.Button5Click(Sender: TObject);
begin
  Addlog(Memo3,'ABVariantArrayToStr(数组转成字串 [''1'',2,true],true,'';''):'+ABVariantArrayToStr(['1',2,true]));
  Addlog(Memo3,'ABVariantArrayToStr(数组转成字串 [''1'',2,true],true,'';''):'+ABVariantArrayToStr(['1',2,true],true,';'));

  Addlog(Memo3,'ABThirdConnU ABInitConn(初始化连接):'+abbooltostr(ABInitConn));
  Addlog(Memo3,'ABThirdConnU ABThirdConnU.ABIsConn(某一连接名称是否已连接数据库 Main):'+abbooltostr(ABThirdConnU.ABIsConn('Main')));
  Addlog(Memo3,'ABThirdConnU ABThirdConnU.ABIsConn(某一连接名称是否已连接数据库 ERP):'+abbooltostr(ABThirdConnU.ABIsConn('ERP')));
  Addlog(Memo3,'ABThirdConnU ABThirdConnU.ABIsConn(某一连接名称是否已连接数据库 aaa):'+abbooltostr(ABThirdConnU.ABIsConn('aaa')));

  Addlog(Memo3,'ABThirdConnU ABGetDataset(得到数据集):'+intToStr(ABGetDataset('ERP','select * from ABERP_Base_Cust',[]).recordcount));
  ABExecSQL('ERP','update ABERP_Base_Cust set cu_name=cu_name+''-11'' ');
  Addlog(Memo3,'ABThirdConnU ABExecSQL(执行SQL语句):');

  Addlog(Memo3,'ABThirdConnU ABGetServerDateTime(得到连接数据库的日期时间 ERP):'+ABDateTimeToStr(ABGetServerDateTime('ERP')));
  Addlog(Memo3,'ABThirdConnU ABGetServerDate(得到连接数据库的日期 ERP):'+ABDateToStr(ABGetServerDate('ERP')));

  Addlog(Memo3,'ABThirdConnU ABExecSQLExecPart(执行SQL中的执行部分,返回余下的内容):'+ABExecSQLExecPart('ERP','[ExceSQLBegin] update ABERP_Base_Cust set cu_name=cu_name+''-11'' [ExceSQLEnd] select * from ABERP_Base_Cust',[]));

  ABExecSQLs('ERP','update ABERP_Base_Cust set cu_name=cu_name+''-11'''  +ABEnterWrapStr+'go'+ABEnterWrapStr+ 'update ABERP_Base_Supplier set Su_name=Su_name+''-11'' ',[]);
  Addlog(Memo3,'ABThirdConnU ABExecSQLs(批量执行SQL语句):');

  ABFillStringsBySQL('Main','select Ta_Name from ABSys_Org_Table',[],nil,cxComboBox1.Properties.Items);
  Addlog(Memo3,'ABThirdConnU ABFillStringsBySQL(将SQL的值填充到列表中):');

  Addlog(Memo3,'ABThirdConnU ABGetSQLValue(得到SQL语句第一个字段的值):'+VarToStr(ABGetSQLValue('Main','select count(*) from ABSys_Org_Table',[],0)));
  Addlog(Memo3,'ABThirdConnU ABGetSQLValue(得到SQL语句第一个字段的值):'+VarToStr(ABGetSQLValue('Main','select Ta_name from ABSys_Org_Table',[],0)));

  Addlog(Memo3,'ABThirdConnU ABGetSQLValues(得到SQL语句多个字段的值并返回到字串中):'+ABGetSQLValues('Main','select Ta_name,Ta_Caption from ABSys_Org_Table',[]));
  Addlog(Memo3,'ABThirdConnU ABGetSQLValues_ByArray(得到SQL语句多个字段的值并返回到变体数组中):'+ABVariantArrayToStr(ABGetSQLValues_ByArray('Main','select Ta_name,Ta_Caption from ABSys_Org_Table',[])));

  ABExecSQL('Main','delete PublicVarTable');
  ABExecSQL('Main','insert into PublicVarTable(name) values(''aa'')');

  Addlog(Memo3,'ABThirdConnU ABUpFileToField(上传本地文件到表字段中):'+inttostr(ABUpFileToField('Main','PublicVarTable','image1','name=''aa''',ABAppFullName)));
  Addlog(Memo3,'ABThirdConnU ABDownToFileFromField(从表字段下载内容到本地文件中):'+abbooltostr(ABDownToFileFromField('Main','PublicVarTable','image1','name=''aa''',ABAppPath+'aa.exe')));

  FMemoryStream.LoadFromFile(ABAppFullName);
  Addlog(Memo3,'ABThirdConnU ABUpStreamToField(上传内存流到表字段中):'+inttostr(ABUpStreamToField('Main','PublicVarTable','image1','name=''aa''',FMemoryStream)));
  Addlog(Memo3,'ABThirdConnU ABDownToStreamFromField(从表字段下载内容到内存流中):'+abbooltostr(ABDownToStreamFromField('Main','PublicVarTable','image1','name=''aa''',FMemoryStream)));
  FMemoryStream.SaveToFile(ABAppPath+'bb.exe');
  Addlog(Memo3,'ABThirdConnU ABUpTextToField(上传字串到表字段中):'+inttostr(ABUpTextToField('Main','PublicVarTable','text1','name=''aa''','abcdefgh')));
  Addlog(Memo3,'ABThirdConnU ABDownToTextFromField(从表字段下载内容到函数返回值中):'+ABDownToTextFromField('Main','PublicVarTable','text1','name=''aa'''));

  ABBegTransaction('Main');
  Addlog(Memo3,'ABThirdConnU ABBegTransaction(开启事务):');
  Addlog(Memo3,'ABThirdConnU ABUpTextToField(上传字串到表字段中):'+inttostr(ABUpTextToField('Main','PublicVarTable','text1','name=''aa''','11111')));
  Addlog(Memo3,'ABThirdConnU ABDownToTextFromField(从表字段下载内容到函数返回值中):'+ABDownToTextFromField('Main','PublicVarTable','text1','name=''aa'''));

  ABRollbackTransaction('Main');
  Addlog(Memo3,'ABThirdConnU ABRollbackTransaction(撤消事务):');
  Addlog(Memo3,'ABThirdConnU ABDownToTextFromField(从表字段下载内容到函数返回值中):'+ABDownToTextFromField('Main','PublicVarTable','text1','name=''aa'''));

  ABBegTransaction('Main');
  Addlog(Memo3,'ABThirdConnU ABBegTransaction(开启事务):');
  Addlog(Memo3,'ABThirdConnU ABUpTextToField(上传字串到表字段中):'+inttostr(ABUpTextToField('Main','PublicVarTable','text1','name=''aa''','22222')));
  Addlog(Memo3,'ABThirdConnU ABDownToTextFromField(从表字段下载内容到函数返回值中):'+ABDownToTextFromField('Main','PublicVarTable','text1','name=''aa'''));

  ABCommitTransaction('Main');
  Addlog(Memo3,'ABThirdConnU ABCommitTransaction(提交事务):');
  Addlog(Memo3,'ABThirdConnU ABDownToTextFromField(从表字段下载内容到函数返回值中):'+ABDownToTextFromField('Main','PublicVarTable','text1','name=''aa'''));

  ABExecSQL('Main','delete ABSys_Log_CurLink');

  {
  ADQuery1.close;
  ADQuery1.SQL.Text:=' select * from ABSys_Log_CurLink ';
  ADQuery1.open;
  ADQuery1.close;
  ADQuery1.open;
  ABShowEditDataset(ADQuery1);

  Addlog(Memo3,'ABThirdConnU ABAddClientConn(增加客户端连接):'+inttostr(ABAddClientConn));
  ADQuery1.close;
  ADQuery1.open;
  ABShowEditDataset(ADQuery1);

  Addlog(Memo3,'ABThirdConnU ABUpdateHeartbeatDatetime(更新心跳):'+abbooltostr(ABUpdateHeartbeatDatetime));
  ADQuery1.close;
  ADQuery1.open;
  ABShowEditDataset(ADQuery1);

  Addlog(Memo3,'ABThirdConnU ABDelClientConn(删除客户端连接):'+abbooltostr(ABDelClientConn));
  ADQuery1.close;
  ADQuery1.open;
  ABShowEditDataset(ADQuery1);

  }
end;

procedure TABTestForm.cxCheckComboBox1PropertiesEditValueToStates(
  Sender: TObject; const AValue: Variant; var ACheckStates: TcxCheckStates);
begin
  ABCheckComboBoxEditValueToStates(cxCheckComboBox1.Properties,AValue,ACheckStates);
end;

procedure TABTestForm.cxCheckComboBox1PropertiesStatesToEditValue(
  Sender: TObject; const ACheckStates: TcxCheckStates; out AValue: Variant);
begin
  ABCheckComboBoxStatesToEditValue(cxCheckComboBox1.Properties,ACheckStates,AValue);
end;

procedure TABTestForm.cxGrid1DBBandedTableView1CustomDrawIndicatorCell(
  Sender: TcxGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxCustomGridIndicatorItemViewInfo; var ADone: Boolean);
begin
//  ABSetAutoIDValue(AViewInfo,ACanvas,ADone);
//  Addlog(Memo3,'ABThirdFuncU ABSetAutoIDValue(根据记录条数设置TableView的记录序号的值)');
end;

procedure TABTestForm.Button4Click(Sender: TObject);
var
  tempXmlDoc: TnativeXml;
  tempRootNode, tempNode: TxmlNode;
  a,b,c,d:Word;
  tempStringDynArray:TStringDynArray;
  temStrings: TStrings;
begin
  if not ABThirdQuery1.Active then
    ABThirdQuery1.open;
//在TableView中查找字串

  Addlog(Memo3,'ABThirdFuncU ABSearchIncxTableView(在TableView中查找字串):'+ABBoolToStr(ABSearchIncxTableView(cxGrid1DBBandedTableView1,'aa',0)));


  Addlog(Memo3,'ABThirdFuncU ABGetColumnDataSource(得到TableItem关联的DataSource):'+ABGetColumnDataSource(cxGrid1DBBandedTableView1TA_TI_Guid).Name);
  Addlog(Memo3,'ABThirdFuncU ABGetColumnField(得到TableItem关联的字段):'+ABGetColumnField(cxGrid1DBBandedTableView1TA_TI_Guid).FieldName);
  Addlog(Memo3,'ABThirdFuncU ABGetColumnField(得到TableView某一序号列关联的字段):'+ABGetColumnField(cxGrid1DBBandedTableView1,1).FieldName);
  cxGrid1DBBandedTableView1TA_TI_Guid.Selected:=true;
  Addlog(Memo3,'ABThirdFuncU ABGetFocusedColumnField(得到TableView焦点列关联的字段):'+ABGetFocusedColumnField(cxGrid1DBBandedTableView1).FieldName);

  Addlog(Memo3,'ABThirdFuncU ABGetColumnByName(通过TableView列名称得到列):'+ABGetColumnByName(cxGrid1DBBandedTableView1,cxGrid1DBBandedTableView1TA_TI_Guid.Name).Name);
  Addlog(Memo3,'ABThirdFuncU ABGetColumnByCaption(通过TableView列标题得到列):'+ABGetColumnByCaption(cxGrid1DBBandedTableView1,cxGrid1DBBandedTableView1TA_TI_Guid.Caption).Name);
  Addlog(Memo3,'ABThirdFuncU ABGetColumnByFieldName(通过TableView关联的字段名得到列):'+ABGetColumnByFieldName(cxGrid1DBBandedTableView1,cxGrid1DBBandedTableView1TA_TI_Guid.DataBinding.FieldName).Name);

  Addlog(Memo3,'ABThirdFuncU ABGetColumnProperty(通过TableView关联的字段名得到列的属性值):'+VarToStr(ABGetColumnProperty(cxGrid1DBBandedTableView1,cxGrid1DBBandedTableView1TA_TI_Guid.DataBinding.FieldName,'Caption')));
  ABSetColumnProperty(cxGrid1DBBandedTableView1,cxGrid1DBBandedTableView1TA_TI_Guid.DataBinding.FieldName,'Caption','aaaa');
  Addlog(Memo3,'ABThirdFuncU ABSetColumnProperty(通过TableView关联的字段名设置列的属性值)');
  Addlog(Memo3,'ABThirdFuncU ABGetColumnProperty(通过TableView关联的字段名得到列的属性值):'+VarToStr(ABGetColumnProperty(cxGrid1DBBandedTableView1,cxGrid1DBBandedTableView1TA_TI_Guid.DataBinding.FieldName,'Caption')));

  Addlog(Memo3,'ABThirdFuncU ABGetColumnValue(通过TableView记录号得到TableItem列的值):'+VarToStr(ABGetColumnValue(cxGrid1DBBandedTableView1TA_TI_Guid,1)));
  Addlog(Memo3,'ABThirdConnU ABIsCaleColumn(判断TableItem列是否是计算列):'+abbooltostr(ABIsCaleColumn(cxGrid1DBBandedTableView1TA_TI_Guid)));
  Addlog(Memo3,'ABThirdConnU ABIsLinkField(判断TableItem列是否有关联字段):'+abbooltostr(ABIsLinkField(cxGrid1DBBandedTableView1TA_TI_Guid)));

  Addlog(Memo3,'ABThirdFuncU ABGetTableViewDataController(得到TableView的DataController):'+ABGetTableViewDataController(cxGrid1DBBandedTableView1).ClassName);
  Addlog(Memo3,'ABThirdFuncU ABGetTableViewDataSource(得到TableView的DataSource):'+ABGetTableViewDataSource(cxGrid1DBBandedTableView1).name);
  Addlog(Memo3,'ABThirdFuncU ABGetTableViewOrder(取得TableView排序的字段名):'+ABGetTableViewOrder(cxGrid1DBBandedTableView1));

  ABSetTableViewOrder(cxGrid1DBBandedTableView1,['Ta_ID','Ta_Name'],[dxCore.soAscending,dxCore.soDescending]);
  Addlog(Memo3,'ABThirdFuncU ABSetTableViewOrder(ABSetTableViewOrder):');
  Addlog(Memo3,'ABThirdFuncU ABGetTableViewOrder(取得TableView排序的字段名串):'+ABGetTableViewOrder(cxGrid1DBBandedTableView1));

  ABDelNotCaleAndNotLinkFieldColumns(cxGrid1DBBandedTableView1);
  Addlog(Memo3,'ABThirdFuncU ABDelNotCaleAndNotLinkFieldColumns(删除TableView中非计算列且非字段关联列)');
  Addlog(Memo3,'ABThirdFuncU ABReplaceColumnCaption(用TableView记录号RecordIndex中的列值替换aCommText中的列标题):'+ABReplaceColumnCaption('if ('':TA_ID'' ==''1993058136'') 255;[end]',cxGrid1DBBandedTableView1,0));

  ABSetColumnPropertiesClass(cxGrid1DBBandedTableView1);
  Addlog(Memo3,'ABThirdFuncU ABSetColumnPropertiesClass(根据TableView中列关联字段的类型设置列的PropertiesClass类别)');

  ABCreateColumns(cxGrid1DBBandedTableView1);
  Addlog(Memo3,'ABThirdFuncU ABCreateColumns(创建TableView中关联字段的列)');

  ABSetAutoIDWidth(cxGrid1DBBandedTableView1);
  Addlog(Memo3,'ABThirdFuncU ABSetAutoIDWidth(根据记录条数设置TableView的记录序号的宽度)');

  //ABSetFieldValue_MultiSelect(cxGrid1DBBandedTableView1.DataController,mtSelect,['a','b','c','d'],['abcd123','Ta_Caption','''[:Ta_Name]'' + ''[:Ta_Caption]''','[:TA_Order]*2+[:TA_ID]'],[False,false,true,true]);
  //Addlog(Memo3,'ABThirdFuncU ABSetFieldValue_MultiSelect(批量修改TableView中选择记录字段的值)');

  //ABSetFieldValue_MultiSelect(cxGrid1DBBandedTableView1.DataController,mtSelect,['Ta_Caption'],['a'],['Ta_Caption'],['b']);
  //Addlog(Memo3,'ABThirdFuncU ABSetFieldValue_MultiSelect(批量修改TableView中选择记录指定字段的值是其它字段的首拼或全拼)');

  Addlog(Memo3,'ABThirdFuncU ABGetSQL_MultiSelect(转换TableView中选择记录为插入或修改数据的SQL):'+ABGetSQL_MultiSelect(cxGrid1DBBandedTableView1.DataController,mtSelect,'ABSys_Org_Table',['Ta_Guid'],['Ta_Name'],['Ta_Name','Ta_Caption'],nil,[]));
  Addlog(Memo3,'ABThirdFuncU ABGetFieldValue_MultiSelect(得到TableView中选择记录的字段值):'+ABGetFieldValue_MultiSelect(cxGrid1DBBandedTableView1.DataController,mtSelect,'Ta_Name'));

  Addlog(Memo3,'ABThirdFuncU ABGetCortrolPropertyValue(得到CX系列控件的属性值(可不停止扩充，现只实现部分cx系列DataField,Color,ReadOnly)):'+VarToStr(ABGetCortrolPropertyValue(cxDBTextEdit1,'DataField')));
  Addlog(Memo3,'ABThirdFuncU ABGetCortrolPropertyValue(得到CX系列控件的属性值(可不停止扩充，现只实现部分cx系列DataField,Color,ReadOnly)):'+VarToStr(ABGetCortrolPropertyValue(cxDBTextEdit1,'Color')));
  Addlog(Memo3,'ABThirdFuncU ABGetCortrolPropertyValue(得到CX系列控件的属性值(可不停止扩充，现只实现部分cx系列DataField,Color,ReadOnly)):'+VarToStr(ABGetCortrolPropertyValue(cxDBTextEdit1,'ReadOnly')));
  ABSetCortrolPropertyValue(cxDBTextEdit1,'DataField','Ta_Name');
  ABSetCortrolPropertyValue(cxDBTextEdit1,'Color',clRed);
  ABSetCortrolPropertyValue(cxDBTextEdit1,'ReadOnly',true);
  Addlog(Memo3,'ABThirdFuncU ABSetCortrolPropertyValue(设置CX系列控件的属性值(可不停止扩充，现只实现部分cx系列DataField,Color,ReadOnly)):');
  Addlog(Memo3,'ABThirdFuncU ABGetCortrolPropertyValue(得到CX系列控件的属性值(可不停止扩充，现只实现部分cx系列DataField,Color,ReadOnly)):'+VarToStr(ABGetCortrolPropertyValue(cxDBTextEdit1,'DataField')));
  Addlog(Memo3,'ABThirdFuncU ABGetCortrolPropertyValue(得到CX系列控件的属性值(可不停止扩充，现只实现部分cx系列DataField,Color,ReadOnly)):'+VarToStr(ABGetCortrolPropertyValue(cxDBTextEdit1,'Color')));
  Addlog(Memo3,'ABThirdFuncU ABGetCortrolPropertyValue(得到CX系列控件的属性值(可不停止扩充，现只实现部分cx系列DataField,Color,ReadOnly)):'+VarToStr(ABGetCortrolPropertyValue(cxDBTextEdit1,'ReadOnly')));

  ABSetTableViewSelect(cxGrid1DBBandedTableView1);
  Addlog(Memo3,'ABThirdFuncU ABSetTableViewSelect(设置TableView的选择(无选择时默认选择当前记录)):');
  ABSetTableViewSelect(cxGrid1DBBandedTableView1,mtAll);
  Addlog(Memo3,'ABThirdFuncU ABSetTableViewSelect(根据多选类型设置TableView的选择):');
  ABSetcxDBDataControllerSelect(cxGrid1DBBandedTableView1.DataController);
  Addlog(Memo3,'ABThirdFuncU ABSetcxDBDataControllerSelect(设置DataController的选择(无选择时默认选择当前记录)):');

  ABSetcxListBoxSelect(cxCheckListBox1,0);
  Addlog(Memo3,'ABThirdFuncU ABSetcxListBoxSelect(设置CheckListBox的操作):');
  ABSetcxListBoxSelect(cxCheckListBox1,1);
  Addlog(Memo3,'ABThirdFuncU ABSetcxListBoxSelect(设置CheckListBox的操作):');
  ABSetcxListBoxSelect(cxCheckListBox1,2);
  Addlog(Memo3,'ABThirdFuncU ABSetcxListBoxSelect(设置CheckListBox的操作):');
  ABSavecxListBoxSelect(cxCheckListBox1);
  Addlog(Memo3,'ABThirdFuncU ABSavecxListBoxSelect(保存CheckListBox的选择到默认的文件中):');
  ABLoadcxListBoxSelect(cxCheckListBox1);
  Addlog(Memo3,'ABThirdFuncU ABLoadcxListBoxSelect(从默认的文件中加载CheckListBox的选择):');

  tempXmlDoc := TnativeXml.Create(nil);
  try
    tempXmlDoc.LoadFromFile(ABSoftSetPath+'tempaa.dproj');
    tempRootNode := tempXmlDoc.Root;
    tempNode := ABFindXMLNode(tempRootNode,'VersionInfo','Name','IncludeVerInfo',
                                           'VersionInfo','','');
    Addlog(Memo3,'ABThirdFuncU ABFindXMLNode(查找XML文档的结点):'+tempNode.Value);

    ABSetXMLNodeValue(tempRootNode,
                      'VersionInfo','Name','IncludeVerInfo',
                      'VersionInfo','','',ABBoolToStr(not (ABStrToBool(tempNode.Value))));
    Addlog(Memo3,'ABThirdFuncU ABSetXMLNodeValue(ABSetXMLNodeValue):');

    tempNode := ABFindXMLNode(tempRootNode,'VersionInfo','Name','IncludeVerInfo',
                                           'VersionInfo','','');
    Addlog(Memo3,'ABThirdFuncU ABFindXMLNode(查找XML文档的结点):'+tempNode.Value);
    tempXmlDoc.SaveToFile(ABSoftSetPath+'tempaa.dproj');
  finally
    tempXmlDoc.Free;
  end;

  ABReadResVer(ABSoftSetPath+'tempaa.res',a,b,c,d);
  Addlog(Memo3,'ABThirdFuncU ABReadResVer(读取资源中的版本号):'+inttostr(a)+'.'+inttostr(b)+'.'+inttostr(c)+'.'+inttostr(d));
  ABModifyResVer(ABSoftSetPath+'tempaa.res',1,1,1,1);
  Addlog(Memo3,'ABThirdFuncU ABModifyResVer(修改资源中的版本号):');
  ABReadResVer(ABSoftSetPath+'tempaa.res',a,b,c,d);
  Addlog(Memo3,'ABThirdFuncU ABReadResVer(读取资源中的版本号):'+inttostr(a)+'.'+inttostr(b)+'.'+inttostr(c)+'.'+inttostr(d));

  ABCreateEmptyRes(ABSoftSetPath+'aaaa.dpr');
  Addlog(Memo3,'ABThirdFuncU ABCreateEmptyRes(创建空的资源文件):');

  tempStringDynArray:=ABGetDelphiProjectinfo(ABSoftSetPath+'tempaa.dproj',bstIncludeVerInfo);
  Addlog(Memo3,'ABThirdFuncU ABGetDelphiProjectinfo(得到ELPHI项目信息(包括dproj、dpr、dpk)):'+tempStringDynArray[0]);
  ABSetDelphiProjectinfo(ABSoftSetPath+'tempaa.dproj',bstIncludeVerInfo,[ABBoolToStr(not ABStrToBool(tempStringDynArray[0]))]);
  Addlog(Memo3,'ABThirdFuncU ABSetDelphiProjectinfo(设置DELPHI项目信息(包括dproj、dpr、dpk)):');

  temStrings:= TStringList.Create;
  temStrings.Clear;
  temStrings.Add(ABSoftSetPath+'tempaa.dproj');
  ABSetDelphiProjectinfos(temStrings,bstIncludeVerInfo,[ABBoolToStr(not ABStrToBool(tempStringDynArray[0]))]);
  Addlog(Memo3,'ABThirdFuncU ABSetDelphiProjectinfos(批量设置DELPHI项目信息(包括dproj、dpr、dpk)):');
  tempStringDynArray:=ABGetDelphiProjectinfo(ABSoftSetPath+'tempaa.dproj',bstIncludeVerInfo);
  Addlog(Memo3,'ABThirdFuncU ABGetDelphiProjectinfo(得到ELPHI项目信息(包括dproj、dpr、dpk)):'+tempStringDynArray[0]);

  temStrings.Clear;
  ABGetDelphiProjectinfoToStrings(ABSoftSetPath+'tempaa.dproj',temStrings);
  Addlog(Memo3,'ABThirdFuncU ABGetDelphiProjectinfoToStrings(得到项目信息的字串列表(包括dproj、dpr、dpk)):'+temStrings.Text);

  ABAddNavBarGroupItem(dxNavBar1,dxNavBar1Group1,'test1','测试1');
  Addlog(Memo3,'ABThirdFuncU ABAddNavBarGroupItem(为TdxNavBarGroup增加新项):');

  ABCopyRadioGroupItems(RadioGroup1,cxRadioGroup1);
  Addlog(Memo3,'ABThirdFuncU ABCopyRadioGroupItems(拷贝TRadioGroup的项到TcxRadioGroup)');

  ABSetdxBarItemsVisible(inheritedBar.ItemLinks,ivNever);
  Addlog(Memo3,'ABThirdFuncU ABSetdxBarItemsVisible(设置dxBarItems中的项可见性 ivNever)');
  ABSetdxBarItemsVisible(inheritedBar.ItemLinks,ivAlways);
  Addlog(Memo3,'ABThirdFuncU ABSetdxBarItemsVisible(设置dxBarItems中的项可见性 ivAlways)');
  ABCleardxBarItems(inheritedBar.ItemLinks);
  Addlog(Memo3,'ABThirdFuncU ABCleardxBarItems(清除dxBarItemLinks中的项)');

  ABRefreshcxListBoxWidth(cxCheckListBox1);
  Addlog(Memo3,'ABThirdFuncU ABRefreshcxListBoxWidth(根据TCustomListBox的行内容字符数目设置水平流动条的流动宽度)');

  ABExportCxgridsToExcel([cxGrid1,cxGrid2],['cxGrid1','cxGrid2']);
  Addlog(Memo3,'ABThirdFuncU ABExportCxgridsToExcel(多个cxGrid的当前TableView合并到一个EXCEL文件的多个Sheet)');

  ABLanguageSetComponentsText(dxStatusBar1);
  Addlog(Memo3,'ABThirdFuncU ABLanguageSetComponentsText(翻译dxStatusBar中项的内容)');
  temStrings.Free;
end;

procedure TABTestForm.Button3Click(Sender: TObject);
begin
  ABThirdQuery2.close;
  ABThirdQuery2.SQL.Text:=' select * from ABSys_Log_CurLink ';
  ABThirdQuery2.Open;

  Addlog(Memo3,'ABThirdImportDataU ABInputDataToDataset(导入数据到数据集):'+
                 ABBoolToStr(ABInputDataToDataset(ABThirdQuery2,'',
                                      '',[])));
  {
  ADQuery1.close;
  ADQuery1.SQL.Text:=' select * from ABSys_Log_CurLink ';
  ADQuery1.Open;
  ADQuery2.close;
  ADQuery2.SQL.Text:=' select * from Table1 ';
  ADQuery2.Open;

  Addlog(Memo3,'ABThirdImportDataU ABGetFieldMultiRowWhere(得到由开窗输入的第一列数据组成的SQL条件):'+ABGetFieldMultiRowWhere('主机'));


  Addlog(Memo3,'ABThirdImportDataU ABInputDataToDataset(导入数据到数据集):'+
                 ABBoolToStr(ABInputDataToDataset(ABThirdQuery2,'',
                                      '',[])));
  Addlog(Memo3,'ABThirdImportDataU ABInputDataToDataset(导入数据到数据集 主键CL_Guid):'+
                 ABBoolToStr(ABInputDataToDataset(ABThirdQuery2,'CL_Guid',
                                      '',[])));

  Addlog(Memo3,'ABThirdImportDataU ABInputDataToDataset(导入数据到数据集 主键CL_Guid 替换数据集):'+
                 ABBoolToStr(ABInputDataToDataset(ABThirdQuery2,'CL_Guid',
                                      'CL_OP_Name=Name=Code',[ADQuery2])));

  Addlog(Memo3,'ABThirdImportDataU ABInputDataToDataset(导入数据到数据集 固定字段CL_PC,CL_SPID,CL_OP_Name,CL_WindowsType,CL_BegDatetime,CL_HeartbeatDatetime):'+
                 ABBoolToStr(ABInputDataToDataset(ABThirdQuery2,'',

                                      '',[],

                                      'CL_PC,CL_SPID,CL_OP_Name,CL_WindowsType,CL_BegDatetime,CL_HeartbeatDatetime'
                                      )));
  Addlog(Memo3,'ABThirdImportDataU ABInputDataToDataset(导入数据到数据集 检测CL_PC,CL_SPID):'+
                 ABBoolToStr(ABInputDataToDataset(ABThirdQuery2,'',

                                      '',[],

                                      '',

                                      ADQuery1,'CL_PC,CL_SPID'
                                      )));

  Addlog(Memo3,'ABThirdImportDataU ABInputDataToDataset(导入数据到数据集 主键CL_Guid 检测CL_PC,CL_SPID):'+
                 ABBoolToStr(ABInputDataToDataset(ABThirdQuery2,'CL_Guid',

                                      '',[],

                                      '',

                                      ADQuery1,'CL_PC,CL_SPID'
                                      )));

  Addlog(Memo3,'ABThirdImportDataU ABInputDataToDataset(导入数据到数据集 主键CL_PC,CL_SPID 检测CL_PC,CL_SPID):'+
                 ABBoolToStr(ABInputDataToDataset(ABThirdQuery2,'CL_PC,CL_SPID',

                                      '',[],

                                      '',

                                      ADQuery1,'CL_PC,CL_SPID',

                                      'aTitleCaption','aFormCaption'
                                      )));

                                      }

end;

procedure TABTestForm.Button2Click(Sender: TObject);
begin
  ABCreatePubDataset('tempPubDataset1','Main','select * from ABSys_Log_CurLink ',[],[],TABThirdReadDataQuery);
  Addlog(Memo3,'ABThirdCacheDatasetU ABCreatePubDataset(创建共用数据集):');
  //ABAddPubDataset('tempPubDataset2',ABThirdQuery1);
  //Addlog(Memo3,'ABThirdCacheDatasetU ABAddPubDataset(增加数据集到列表中):');

  ABCreatePubDataSource('tempPubDataSource1');
  Addlog(Memo3,'ABThirdCacheDatasetU ABCreatePubDataSource(创建共用数据源):');
  //ABAddPubDataSource('tempPubDataSource2',DataSource1);
  //Addlog(Memo3,'ABThirdCacheDatasetU ABAddPubDataSource(增加共用数据源到列表中):');

  Addlog(Memo3,'ABThirdCacheDatasetU ABGetPubDataset(得到共用数据集):'+ABGetPubDataset('tempPubDataset1').Name);
  Addlog(Memo3,'ABThirdCacheDatasetU ABGetPubDataSource(取得共用数据源):'+ABGetPubDataSource('tempPubDataSource1').Name);

  Addlog(Memo3,'ABThirdCacheDatasetU ABGetPubDatasetFieldValue(取共用数据集的字段值):'+VarListArrayToStr(ABGetPubDatasetFieldValue('tempPubDataset1','CL_Guid','11','CL_PC;CL_SPID','')));

  ABEditPubDatasetData('tempPubDataset1',['CL_Guid'],['1'],['CL_OP_Name','CL_PC','CL_SPID'],['12345','1','111']);
  Addlog(Memo3,'ABThirdCacheDatasetU ABEditPubDatasetData(修改、新增、删除共用数据集记录)');
  ABRefreshPubDataset('tempPubDataset1');
  Addlog(Memo3,'ABThirdCacheDatasetU ABRefreshPubDataset(刷新某个共用数据集)');
  ABRefreshPubDataset;
  Addlog(Memo3,'ABThirdCacheDatasetU ABRefreshPubDataset(刷新所有共用数据集)');

  Addlog(Memo3,'ABThirdCacheDatasetU ABPubDatasetList.count()'+inttostr(ABPubDatasetList.count));
  Addlog(Memo3,'ABThirdCacheDatasetU ABPubDataSourceList.count()'+inttostr(ABPubDataSourceList.count));

  ABFreePubDataset('tempPubDataset1');
  ABFreePubDataset;
  Addlog(Memo3,'ABThirdCacheDatasetU ABFreePubDataset(释放共用数据集)');
  ABFreePubDataSource('tempPubDataSource1');
  ABFreePubDataSource;
  Addlog(Memo3,'ABThirdCacheDatasetU ABFreePubDataSource(释放共用数据源)');
end;

procedure TABTestForm.Button1Click(Sender: TObject);
var
  tempThirdReadDataQuery:TABThirdReadDataQuery;
  tempABThirdReadDataQuery:TABThirdReadDataQuery;
begin
  tempThirdReadDataQuery:=TABThirdReadDataQuery.Create(nil);
  tempThirdReadDataQuery.SQL.Text:=' select * from ABSys_Log_CurLink ';
  tempThirdReadDataQuery.open;
  ABShowEditDataset(tempThirdReadDataQuery);

  ABExecSQL('Main','update ABSys_Log_CurLink set CL_PC=getdate() ');

  tempThirdReadDataQuery.Close;
  tempThirdReadDataQuery.SQL.Text:=' select * from ABSys_Log_CurLink ';
  tempThirdReadDataQuery.open;
  ABShowEditDataset(tempThirdReadDataQuery);


  //constructor Create(AOwner: TComponent); override;
  tempABThirdReadDataQuery:=TABThirdReadDataQuery.Create(nil);
  try
    //得到主数据集
    //function GetMainDataset: TABThirdReadDataQuery;
    tempABThirdReadDataQuery.GetMainDataset;
    //设置默认值时，值部分可为列表中数据集的字段
    //property FieldDefaultValuesQuoteDatasetList: TList read FFieldDefaultValuesQuoteDatasetList write FFieldDefaultValuesQuoteDatasetList;
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.FieldDefaultValuesQuoteDatasetList.Count():'+inttostr(tempABThirdReadDataQuery.FieldDefaultValuesQuoteDatasetList.Count));
    //默认值模板中指定字段是否可以设置默认值
    //function FieldDefaultValueCanUse(aFieldName, aFieldValue: string): Boolean; virtual;
    tempABThirdReadDataQuery.FieldDefaultValueCanUse('GL_Guid','');
    //指定字段是否可以拷贝
    //function FieldCanCopy(aFieldName: string): Boolean; virtual;
    tempABThirdReadDataQuery.FieldCanCopy('GL_Guid');

    //取得激活的从数据集
    //procedure GetActiveDetailDataSets(aList: TList); virtual;
    tempList.Clear;
    tempABThirdReadDataQuery.GetActiveDetailDataSets(tempList);

    //行刷新
    //function RowRefresh: boolean;
    tempABThirdReadDataQuery.RowRefresh;

    //数据库连接
    //property ConnName:string read FConnName  write FConnName ;
    tempABThirdReadDataQuery.ConnName:='Main';

    ABExecSQL('Main','delete ABSys_Log_CurLink');

    //修改SQL后做的事情
    //procedure AfterChangSQL(aNewSQL: String; var aDesigning: Boolean;var aLoading: Boolean); override;
    tempABThirdReadDataQuery.SQL.Text:=' select * from ABSys_Log_CurLink ';

    //procedure SetActive(Value: Boolean); override;
    //对原始SQL进行参数替换及合并附加SQL
    //procedure TransactBaseSQL;
    //解析SQL语句到SQL解析结构中
    //procedure ParseSQLToParseObject(aSQL: string);
    //procedure DoAfterOpen; override;
    tempABThirdReadDataQuery.open;

    //procedure DoBeforeInsert; override;
    //新增前检测本数据集
    //function BeforeInsertCheck: Boolean; virtual;
    //procedure DoAfterInsert; override;
    tempABThirdReadDataQuery.Append;
    tempABThirdReadDataQuery.FieldByName('CL_Guid').AsString:=ABGetGuid;
    tempABThirdReadDataQuery.FieldByName('CL_OP_Name').AsString:='ABC';
    tempABThirdReadDataQuery.FieldByName('CL_PC').AsString:='1';
    tempABThirdReadDataQuery.FieldByName('CL_SPID').AsString:='1';
    tempABThirdReadDataQuery.FieldByName('PubID').AsString:=ABGetGuid;
    tempABThirdReadDataQuery.Post;


    //procedure DoBeforeEdit; override;
    //修改前检测父数据集
    //function BeforeEditCheckParent: Boolean; virtual;
    //修改前检测本数据集
    //function BeforeEditCheck: Boolean; virtual;
    //procedure DoAfterEdit; override;
    tempABThirdReadDataQuery.Edit;
    //procedure DoBeforePost; override;
    //保存前检测本数据集
    //function BeforePostCheck: Boolean; virtual;
    //procedure DoAfterPost; override;
    tempABThirdReadDataQuery.Post;
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.LastOperatorType(数据集最后操作的类型):'+ABEnumValueToName(TypeInfo(TABQueryCompareType),ord(tempABThirdReadDataQuery.LastOperatorType)));
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.LastOperatorDatetime(数据集最后操作的时间):'+ABDateTimeToStr(tempABThirdReadDataQuery.LastOperatorDatetime));

    //拷贝数据,支持主从的复制
    //procedure DoCopy;
    tempABThirdReadDataQuery.DoCopy;
    //procedure DoAfterCancel; override;
    tempABThirdReadDataQuery.Cancel;
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.LastOperatorType(数据集最后操作的类型):'+ABEnumValueToName(TypeInfo(TABQueryCompareType),ord(tempABThirdReadDataQuery.LastOperatorType)));
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.LastOperatorDatetime(数据集最后操作的时间):'+ABDateTimeToStr(tempABThirdReadDataQuery.LastOperatorDatetime));

    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.CanEdit(数据集是否能修改):'+ABBoolToStr(tempABThirdReadDataQuery.CanEdit));
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.CanInsert(数据集是否能新增):'+ABBoolToStr(tempABThirdReadDataQuery.CanInsert));
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.CanDelete(数据集是否能删除):'+ABBoolToStr(tempABThirdReadDataQuery.CanDelete));
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.CanPrint(数据集是否能打印):'+ABBoolToStr(tempABThirdReadDataQuery.CanPrint));
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.BaseSQL(基础的SQL):'+tempABThirdReadDataQuery.BaseSQL);

    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.SQLParseObject(当前SQL的结构):'+tempABThirdReadDataQuery.SQLParseObject.SQLParseDef.SQL);

    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.SqlUpdateDatetime(SQL更新时间):'+ABDateTimeToStr(tempABThirdReadDataQuery.SqlUpdateDatetime));
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.AddWhereofNext(下次数据库交互附加SQL):'+tempABThirdReadDataQuery.AddWhereofNext);
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.SQLTransacting(SQL是否正在转换中):'+ABBoolToStr(tempABThirdReadDataQuery.SQLTransacting));

    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.BeforeInsertCheckFieldValues(新增前检测字段值):'+tempABThirdReadDataQuery.BeforeInsertCheckFieldValues);
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.BeforeInsertCheckMsg(新增前检测字段值不通过时弹出的消息):'+tempABThirdReadDataQuery.BeforeInsertCheckMsg);
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.BeforeInsertCheckCommand(新增前检测Scrip命令):'+tempABThirdReadDataQuery.BeforeInsertCheckCommand);

    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.BeforeDeleteCheckFieldValues(删除前检测字段值):'+tempABThirdReadDataQuery.BeforeDeleteCheckFieldValues);
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.BeforeDeleteCheckMsg(删除前检测字段值不通过时弹出的消息):'+tempABThirdReadDataQuery.BeforeDeleteCheckMsg);
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.BeforeDeleteCheckCommand(删除前检测Scrip命令):'+tempABThirdReadDataQuery.BeforeDeleteCheckCommand);

    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.BeforeEditCheckFieldValues(修改前检测字段值):'+tempABThirdReadDataQuery.BeforeEditCheckFieldValues);
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.BeforeEditCheckMsg(修改前检测字段值不通过时弹出的消息):'+tempABThirdReadDataQuery.BeforeEditCheckMsg);
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.BeforeEditCheckCommand(修改前检测Scrip命令):'+tempABThirdReadDataQuery.BeforeEditCheckCommand);

    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.BeforePostCheckFieldValues(保存前检测字段值):'+tempABThirdReadDataQuery.BeforePostCheckFieldValues);
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.BeforePostCheckMsg(保存前检测字段值不通过时弹出的消息):'+tempABThirdReadDataQuery.BeforePostCheckMsg);
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.BeforePostCheckCommand(保存前检测Scrip命令):'+tempABThirdReadDataQuery.BeforePostCheckCommand);

    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.FocusFieldNameOnInsert(新增时的得到焦点的字段):'+tempABThirdReadDataQuery.FocusFieldNameOnInsert);
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.FocusFieldNameOnEdit(编辑时的得到焦点的字段):'+tempABThirdReadDataQuery.FocusFieldNameOnEdit);

    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.BeforeDeleteAsk(删除前是否询问):'+ABBoolToStr(tempABThirdReadDataQuery.BeforeDeleteAsk));
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.BeforeDeleteAskMsg(删除前询问的消息):'+tempABThirdReadDataQuery.BeforeDeleteAskMsg);

    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.FieldDefaultValues(新增时设置字段默认值):'+tempABThirdReadDataQuery.FieldDefaultValues);
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.FieldDefaultValuesTemplate(新增时设置字段默认值模板):'+tempABThirdReadDataQuery.FieldDefaultValuesTemplate.Text);

    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.FieldCaptions(字段的定制标题):'+tempABThirdReadDataQuery.FieldCaptions);
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.MaxRecordCount(数据集记录的最大数量):'+inttostr(tempABThirdReadDataQuery.MaxRecordCount));

    //检测Scrip命令
    //function CheckCommand(aDataset: TDataset; aCheckCommand: string): Boolean; virtual;
    tempABThirdReadDataQuery.CheckCommand(tempABThirdReadDataQuery,'if (1==2) 1');

    //检测当前字段值是否为给出的内容(格式为字段名1=值1;字段名2=值2;...)
    //function CheckFieldValues(aDataset: TDataset;aCheckFieldValues:string;aCheckMsg: string; aType:string ): Boolean; virtual;
    tempABThirdReadDataQuery.CheckFieldValues(tempABThirdReadDataQuery,'CL_Guid=''11''','aa','bb');

    tempABThirdReadDataQuery.FieldByName('CL_OP_Name');
    tempABThirdReadDataQuery.FieldByName('aaaa');
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.FieldByName(CL_OP_Name):');
    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.FieldByName(aaaa):');
    if tempABThirdReadDataQuery.Locate('CL_OP_Name','Abc',[]) then
    begin
      Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.Locate(CL_OP_Name,Abc,[]):');
    end;

    //procedure DoBeforeDelete; override;
    //删除前检测本数据集
    //function BeforeDeleteCheck: Boolean; virtual;
    //procedure DoAfterDelete; override;
    tempABThirdReadDataQuery.Delete;

    Addlog(Memo3,'ABThirdCacheDatasetU TABThirdReadDataQuery.RefreshQuery(将条件增加到SQL后立即刷新数据):'+inttostr(tempABThirdReadDataQuery.RefreshQuery('',[])));

    //恢复原始SQL
    //procedure DoAfterClose; override;
    //procedure RestoreBaseSQL;
    tempABThirdReadDataQuery.Close;
  finally
    //destructor Destroy; override;
    tempABThirdReadDataQuery.Free;
  end;
end;

procedure TABTestForm.Button57Click(Sender: TObject);
var
  tempABThirdQuery:TABThirdQuery;
//  tempIsIDENTITY:boolean;
begin
  tempABThirdQuery:=TABThirdQuery.Create(nil);
  //修改SQL后更新Strings的表名列表,在AfterChangSQL后调用
  //procedure AfterChangSQL(aNewSQL: String; var aDesigning: Boolean;var aLoading: Boolean); override;
  //procedure UpdateTableNameListsAfterChangSQL(aString:TStrings);
  tempABThirdQuery.SQL.Text:=' select * from ABSys_Log_CurLink ';
  tempABThirdQuery.ConnName:='Main';
  tempABThirdQuery.open;
  tempABThirdQuery.FieldByName('CL_Guid').ProviderFlags:=tempABThirdQuery.FieldByName('CL_Guid').ProviderFlags+[pfInKey];

  tempABThirdQuery.Append;
  tempABThirdQuery.FieldByName('CL_Guid').AsString:=ABGetGuid;
  tempABThirdQuery.FieldByName('CL_OP_Name').AsString:='ABC';
  tempABThirdQuery.FieldByName('CL_PC').AsString:='1';
  tempABThirdQuery.FieldByName('CL_SPID').AsString:='1';
  tempABThirdQuery.FieldByName('PubID').AsString:=ABGetGuid;
  //procedure DoBeforePost; override;
  //procedure DoBeforeTrans; virtual;
  //procedure DoAfterStartTrans; virtual;
  //procedure DoBeforeCommitTrans; virtual;
  //procedure DoAfterTrans; virtual;
  //procedure DoBeforeUpdateTransitionTableName(aState: TABDatasetOperateType;var aTableName:string); virtual;
  //procedure DoBeforeUpdateTransitionFieldName(aState: TABDatasetOperateType;var aFieldName:string); virtual;
  //字段是否更新到数据库中
  //function FieldCanUpdateToDatabase(aTableName, aFieldName: String;var aIsIDENTITY: Boolean): Boolean; virtual;
  //更新当前数据到数据库,如果CacheUpdate=True则生成更新SQL到CacheSQLList中,在需要更新时则调用CacheToDatabase
  //function UpdatesToDatabase(aState: TDataSetState): Boolean; virtual;
  //执行数据集
  //function ExecQuery(aUpdateQuery: TADQuery;aSQL:string): Boolean;
  tempABThirdQuery.Post;

  tempABThirdQuery.CacheUpdate:=true;
  tempABThirdQuery.Append;
  tempABThirdQuery.FieldByName('CL_Guid').AsString:=ABGetGuid;
  tempABThirdQuery.FieldByName('CL_OP_Name').AsString:='ABC_1';
  tempABThirdQuery.FieldByName('CL_PC').AsString:='1';
  tempABThirdQuery.FieldByName('CL_SPID').AsString:='1';
  tempABThirdQuery.FieldByName('PubID').AsString:=ABGetGuid;
  tempABThirdQuery.Post;
  tempABThirdQuery.Append;
  tempABThirdQuery.FieldByName('CL_Guid').AsString:=ABGetGuid;
  tempABThirdQuery.FieldByName('CL_OP_Name').AsString:='ABC_2';
  tempABThirdQuery.FieldByName('CL_PC').AsString:='2';
  tempABThirdQuery.FieldByName('CL_SPID').AsString:='2';
  tempABThirdQuery.FieldByName('PubID').AsString:=ABGetGuid;
  tempABThirdQuery.Post;
  //缓存数据更新到数据库中
  //procedure CacheToDatabase;
  tempABThirdQuery.CacheToDatabase;
  tempABThirdQuery.Append;
  tempABThirdQuery.FieldByName('CL_Guid').AsString:=ABGetGuid;
  tempABThirdQuery.FieldByName('CL_OP_Name').AsString:='ABC_3';
  tempABThirdQuery.FieldByName('CL_PC').AsString:='3';
  tempABThirdQuery.FieldByName('CL_SPID').AsString:='3';
  tempABThirdQuery.FieldByName('PubID').AsString:=ABGetGuid;
  tempABThirdQuery.Post;
  //缓存数据清除
  //procedure CacheClear;
  tempABThirdQuery.CacheClear;
  tempABThirdQuery.CacheToDatabase;

  //tempABThirdQuery.OnlyDatasetDelete;
  //tempABThirdQuery.OnlyDatasetPost;

  tempABThirdQuery.CacheUpdate:=False;
  tempABThirdQuery.UpdateDatabase:=False;
  tempABThirdQuery.Append;
  tempABThirdQuery.FieldByName('CL_Guid').AsString:=ABGetGuid;
  tempABThirdQuery.FieldByName('CL_OP_Name').AsString:='ABC_4';
  tempABThirdQuery.FieldByName('CL_PC').AsString:='4';
  tempABThirdQuery.FieldByName('CL_SPID').AsString:='4';
  tempABThirdQuery.FieldByName('PubID').AsString:=ABGetGuid;
  tempABThirdQuery.Post;

  Addlog(Memo3,'ABThirdQueryU TABThirdQuery.UpdateMode(提交更新模式):'+ABEnumValueToName(TypeInfo(TUpdateMode),ord(tempABThirdQuery.UpdateMode)));
  Addlog(Memo3,'ABThirdQueryU TABThirdQuery.UpdateTables(需更新到数据库的表名列表):'+tempABThirdQuery.UpdateTables.Text);
  Addlog(Memo3,'ABThirdQueryU TABThirdQuery.IndexListDefs(数据表索引):'+tempABThirdQuery.IndexListDefs.ToString);


  //procedure DoBeforeDelete; override;
  //更新当前数据到数据库,如果CacheUpdate=True则生成更新SQL到CacheSQLList中,在需要更新时则调用CacheToDatabase
  //function UpdatesToDatabase(aState: TDataSetState): Boolean; virtual;
  tempABThirdQuery.Delete;

end;

procedure TABTestForm.Button15Click(Sender: TObject);
begin
//
end;

procedure TABTestForm.Button16Click(Sender: TObject);
begin
  ABcxGridPopupMenu1.Load;
  ABcxPivotGridPopupMenu1.Load;
end;




end.



