{
数据集相关操作的单元
}
unit ABThirdDBU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubConstU,
  ABPubVarU,
  ABPubDBU,
  ABPubMessageU,
  ABPubUserU,
  ABPubLocalParamsU,
  ABPubFuncU,
  ABPubSQLLogU,

  FireDAC.Stan.ExprFuncs,
  FireDAC.Stan.Expr,

  FireDAC.Stan.StorageBin,
  FireDAC.Stan.StorageXML, FireDAC.Stan.StorageJSON,
  FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Comp.UI,
  FireDAC.Stan.Def,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.Phys.MSSQLDef, FireDAC.Phys, FireDAC.Phys.ODBCBase, FireDAC.Phys.MSSQL,

  Windows,Classes, SysUtils, ADODB,DB,SqlExpr,DBClient,Variants,

  ShellAPI,
  TypInfo;

type
  TFireDACConnection = class(TFDConnection)
  private
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
  end;

  TFireDACQuery = class(TFDCustomQuery)
  private
  protected
    procedure Loaded; override;
  public
    procedure DoBeforeExecute; override;
    procedure SetActive(Value: Boolean); override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property BeforeExecute;
    property BeforePost;
    property AfterInsert;
  end;
  TFireDACQueryClass = class of TFireDACQuery;

  TFireDACMemTable = class(TFDMemTable) ;

  TFireDACParams = class(TFDParams) ;

  TFireDACParam = class(TFDParam) ;

//******************ADOConnection连接相关****************
//打开UDL连接框并返回ADOConnection连接串
function ABGetADOConnectionUDLConnStr(aConn: TADOConnection): string;
//根据连接参数得到ADOConnection控件的连接串
function ABGetADOConnectionConnStr( aDatabaseType:TABDatabaseType;
                                       aHostName,aDataBase,aUserName,aPassword:string;
                                       aProviderName:string='SQLOLEDB.1'):string;
//以按定UDL文件连接ADOConnection,失败时弹出连接框
//aUdlFileName=设置的UDL文件名，如果为空则自动设置为ABSoftSetPath+ABConn.udl,如果没有路径部分则自动加上ABSoftSetPath
//以下参数都在UDL文件存在时才起作用，如文件不存在则弹出UDL连接对话框
//aOpenConn=设置UDL文件后是否打开连接，
//aShowOkInfo，aOkInfoStr=连接成功后是否显示成功信息aOkInfoStr
//aShowAbortInfo，aAbortInfoStr=连接失败后是否显示失败信息aAbortInfoStr ,如连接失败则弹出UDL连接对话框
function ABOpenADOConnectionByUDL(aConn: TADOConnection;aUdlFileName:string='';
                                 aOpenConn:boolean=True;
                                 aShowOkInfo:boolean=True;aOkInfoStr:string='';
                                 aShowAbortInfo:boolean=True;aAbortInfoStr:string=''): booleaN;

//******************Connection连接相关****************
//将连接结构中的连接信息设置到连接控件中
procedure ABSetConnParams(aConnParams:TABConnParams;aConn: TCustomConnection);
//从连接控件中得到连接结构
function ABGetConnParams(aConn: TCustomConnection):TABConnParams;overload;
//从连接信息中得到连接结构
function ABGetConnParams(aDatabaseType:TABDatabaseType;
                         aHostName,aDataBase,aUser_Name,aPassword:string
                         ):TABConnParams;overload;

//初始化连接控件的一些属性
procedure ABInitConnection(aConn:TCustomConnection;aDatabaseType:TABDatabaseType);

//打开连接控件连接
function ABOpenConnection(
                   aConn:TCustomConnection;
                   aDatabaseType:TABDatabaseType;
                   aHostName,aDataBase,aUser_Name,aPassword:string;

                   aOpenConn:boolean=True;
                   aShowOkInfo:boolean=True;aOkInfoStr:string='';
                   aShowAbortInfo:boolean=True;aAbortInfoStr:string=''): booleaN;overload;

function ABOpenConnection(
                   aConn:TCustomConnection;
                   aConnParams:TABConnParams;

                   aOpenConn:boolean=True;
                   aShowOkInfo:boolean=True;aOkInfoStr:string='';
                   aShowAbortInfo:boolean=True;aAbortInfoStr:string=''): booleaN;overload;

//克隆数据集
function ABCloneCursor(aDataset:TDataSet): TDataSet;
//刷新数据集
procedure ABReFreshQuery( aDataSet: TDataSet;
                          aParamValue: array of Variant;aExtParamsDataset: TDataSet=nil;
                          aIsRequery: boolean= true; aIsDisableControls: boolean= true;
                          aIsSavePosition: boolean= true;aReportRaise:Boolean= true);

//取得数据集SQL
function ABGetDatasetSQL(aDataSet: TDataSet): string;
//设置数据集SQL语句
procedure ABSetDatasetSQL(aDataSet: TDataSet; aSql: string);

//取得Dataset的包含参数值的SQL
function ABGetDatasetSQL_HaveParamValue(aDataSet: TDataSet): string;

//设置数据集SQL及数据集中引用的自动参数值
function ABSetDatasetAutoParamsAndSQLParams(aDataSet: TDataSet;aExtParamsDataset: TDataSet=nil;
                                            aSetAutoParams:boolean=true;aSetSQLParams:boolean=true):string;



//取得参数的数目
function ABGetParamsCount(aDataSet: TDataSet): LongInt;
//清除参数
procedure ABClearParams(aDataSet: TDataSet);
//取得参数项
function ABGetParam(aDataSet: TDataSet; aParamName: string): TCollectionItem;

//得到指定参数的类型
function ABGetParamType(aDataSet: TDataSet; aParamName: string): TDataType; overload;
function ABGetParamType(aDataSet: TDataSet; aParamIndex: LongInt): TDataType; overload;
//设置指定参数的类型
procedure ABSetParamType(aDataSet: TDataSet; aParamName: string; aParamType: TDataType = ftString); overload;
procedure ABSetParamType(aDataSet: TDataSet; aParamIndex: LongInt; aParamType: TDataType = ftString);overload;

//取得参数的值
function ABGetParamValue(aDataSet: TDataSet; aParamIndex: LongInt): Variant; overload;
function ABGetParamValue(aDataSet: TDataSet; aParamName: string): Variant; overload;
//设置参数的值
procedure ABSetParamValue(aDataSet: TDataSet; aParamIndex: LongInt; aParamValue: Variant); overload;
procedure ABSetParamValue(aDataSet: TDataSet; aParamName: string; aParamValue: Variant); overload;
procedure ABSetParamValue(aDataSet: TDataSet; aParamIndex: LongInt; aParamValue: TMemoryStream); overload;
procedure ABSetParamValue(aDataSet: TDataSet; aParamName: string; aParamValue: TMemoryStream); overload;
procedure ABSetParamValue(aDataSet: TDataSet; aParamValue: array of Variant); overload;

//设置数据集中引用的自动参数值
//自动的参数包括 :ABParams_参数名 与 :ABUser_属性名
procedure ABSetDatasetAutoParams(aDataset: TDataSet;aExtParamsDataset: TDataSet=nil);

//检测所有的参数是否都已有值
function ABCheckDatasetParamsHaveValue(aDataSet: TDataSet): Boolean;
//取得参数的名称
function ABGetParamName(aDataSet: TDataSet; aParamIndex: LongInt): string;
//设置未定义的参数类型
procedure ABSetUnknownParamType(aDataSet: TDataSet;aExtDataset: TDataSet; aParamType: TDataType = ftString);
//拷贝参数
procedure ABCopyParams(aFromDataSet:TFireDACQuery; aToParams: TParams);overload;
procedure ABCopyParams(aFromQuery:TParams; aToParams: TFDParams);overload;

//布尔字段的filter串(原因是UDAC中要用True/False,其它的用1/0)
function ABGetBooleanFilter(aDataset:Tdataset;aBooleanFieldName:string;aBoolValue:Boolean):string;
//为数据集增加定义的扩展字段
procedure ABAddDatasetExtFields(aDataset:TFireDACQuery;aCalcFieldsDef,aAggregateFieldsDef:string;var aAddBaseField:boolean;aSetExpression:boolean=true);

implementation

procedure ABAddDatasetExtFields(aDataset:TFireDACQuery;aCalcFieldsDef,aAggregateFieldsDef:string;var aAddBaseField:boolean;aSetExpression:boolean);
var
  j,k:LongInt;
  tempDef:TStrings;
  tempCurLineDef,
  tempFieldName,tempFieldCaption,tempFieldType,tempFieldExpression:string;
  tempField:TField;
  tempAggregateField: TFDAggregate;
begin
  tempDef := TStringList.Create;
  try
    if (aCalcFieldsDef<>EmptyStr) then
    begin
      tempDef.Text:=aCalcFieldsDef;
      for j := 0 to tempDef.Count-1 do
      begin
        tempCurLineDef:=tempDef[j];
        tempFieldName:=ABGetLeftRightStr(tempCurLineDef);

        tempCurLineDef:=ABGetLeftRightStr(tempCurLineDef,axdRight);
        tempFieldCaption:=ABGetLeftRightStr(tempCurLineDef);

        tempCurLineDef:=ABGetLeftRightStr(tempCurLineDef,axdRight);
        tempFieldType:=ABGetLeftRightStr(tempCurLineDef);

        tempCurLineDef:=ABGetLeftRightStr(tempCurLineDef,axdRight);
        tempFieldExpression:=ABGetLeftRightStr(tempCurLineDef);

        if (tempFieldName<>EmptyStr) and
           (tempFieldType<>EmptyStr) and
           (tempFieldExpression<>EmptyStr) and
           (not Assigned(aDataset.FindField(tempFieldName))) then
        begin
          if not aAddBaseField then
          begin
            aDataset.FieldDefs.Updated := False;
            aDataset.FieldDefs.Update;
            for k := 0 to aDataset.FieldDefs.Count - 1 do
              aDataset.FieldDefs[k].CreateField(aDataset);
            aAddBaseField:=true;
          end;

          if AnsiCompareText(tempFieldType,'Float')=0 then
            tempField := TFloatField.Create(aDataset)
          else  if AnsiCompareText(tempFieldType,'int')=0 then
            tempField := TIntegerField.Create(aDataset)
          else  if AnsiCompareText(tempFieldType,'bit')=0 then
            tempField := TBooleanField.Create(aDataset)
          else  if AnsiCompareText(tempFieldType,'Date')=0 then
            tempField := TDateField.Create(aDataset)
          else  if AnsiCompareText(tempFieldType,'Time')=0 then
            tempField := TTimeField.Create(aDataset)
          else  if AnsiCompareText(tempFieldType,'DateTime')=0 then
            tempField := TDateTimeField.Create(aDataset)
          else
            tempField := TStringField.Create(aDataset);

          tempField.FieldName := tempFieldName;
          if (tempFieldCaption<>EmptyStr) then
            tempField.DisplayLabel := tempFieldCaption;
          tempField.FieldKind := fkInternalCalc;
          if aSetExpression then
            tempField.DefaultExpression := tempFieldExpression;
          tempField.DataSet := aDataset;
        end;
      end;
    end;

    if (aAggregateFieldsDef<>EmptyStr) then
    begin
      tempDef.Text:=aAggregateFieldsDef;
      for j := 0 to tempDef.Count-1 do
      begin
        tempCurLineDef:=tempDef[j];
        tempFieldName:=ABGetLeftRightStr(tempCurLineDef);

        tempCurLineDef:=ABGetLeftRightStr(tempCurLineDef,axdRight);
        tempFieldExpression:=ABGetLeftRightStr(tempCurLineDef);

        if (tempFieldName<>EmptyStr) and
           (tempFieldExpression<>EmptyStr) and
           (not Assigned(aDataset.Aggregates.FindAggregate(tempFieldName))) then
        begin
          tempAggregateField :=aDataset.Aggregates.Add;
          tempAggregateField.Name := tempFieldName;
          if aSetExpression then
            tempAggregateField.Expression := tempFieldExpression;
          tempAggregateField.Active := True;
        end;
      end;
      aDataset.AggregatesActive := True;
    end;
  finally
    tempDef.Free;
  end;
end;

function ABGetBooleanFilter(aDataset:Tdataset;aBooleanFieldName:string;aBoolValue:Boolean):string;
begin
  if  aDataSet is TFireDACQuery then
  begin
    result:=aBooleanFieldName+'='+abiif(aBoolValue,'True','False');
  end
  else
  begin
    result:=aBooleanFieldName+'='+abiif(aBoolValue,'1','0');
  end;
end;


function ABGetADOConnectionUDLConnStr(aConn: TADOConnection): string;
begin
  aConn.Connected:=false;
  aConn.ConnectionString := PromptDataSource(0, aConn.ConnectionString);
  Result := aConn.ConnectionString;
end;

function ABOpenADOConnectionByUDL(aConn: TADOConnection;aUdlFileName:string;
                                 aOpenConn:boolean;
                                 aShowOkInfo:boolean;aOkInfoStr:string;
                                 aShowAbortInfo:boolean;aAbortInfoStr:string): booleaN;
begin
  Result:=false;

  if aUdlFileName=emptystr then
  begin
    aUdlFileName:= ABSoftSetPath+'ABConn.udl';
  end
  else
  begin
    if ABPos('\',aUdlFileName)<=0 then
      aUdlFileName:= ABSoftSetPath+aUdlFileName;
  end;

  if not ABCheckFileExists(aUdlFileName) then
  begin
    ABWriteTxt(aUdlFileName,'');
    ABWaitAppEnd(aUdlFileName,sw_shownormal);
  end;

  if ABCheckFileExists(aUdlFileName) then
  begin
    try
      aConn.Connected:=False;
      aConn.ConnectionString:='File name='+aUdlFileName;

      if aOpenConn then
      begin
        aConn.Connected:=true;

        if aShowOKInfo then
        begin
          if aOKInfoStr=EmptyStr then
            aOKInfoStr:='文件['+ABGetFilename(aUdlFileName,false)+']'+'连接成功.';
          ABShow(aOKInfoStr);
        end;
        Result:=true;
      end;
    except
      if aShowAbortInfo then
      begin
        if aAbortInfoStr=EmptyStr then
          aAbortInfoStr:='请先设置连接文件['+ABGetFilename(aUdlFileName,false)+']再启动程序.';
        ABShow(aAbortInfoStr);
      end;

      ShellExecute(0,'open',pchar(aUdlFileName) ,'','',SW_SHOWNORMAL);
    end;
  end;
end;

procedure ABInitConnection(aConn: TCustomConnection;aDatabaseType:TABDatabaseType);
begin
  if aConn is TADOConnection then
  begin

  end
  else if aConn is  TFireDACConnection then
  begin
    case aDatabaseType of
      dtSQLServer:
      begin
        TFireDACConnection(aConn).DriverName := 'MSSQL';
      end;
      dtOracle:
      begin
      end;
      dtAccess:
      begin
      end;
      dtDB2:
      begin
      end;
      dtMysql:
      begin
      end;
    end;
  end
  else if aConn is  TSQLConnection then
  begin
    case aDatabaseType of
      dtSQLServer:
      begin
        TSQLConnection(aConn).ConnectionName := 'MSSQLCONNECTION';
        TSQLConnection(aConn).DriverName := 'MSSQL';
        TSQLConnection(aConn).GetDriverFunc := 'getSQLDriverMSSQL';
        TSQLConnection(aConn).LibraryName := 'dbxmss.dll';
        TSQLConnection(aConn).LoginPrompt := false;
        TSQLConnection(aConn).VendorLib := 'oledb';

        TSQLConnection(aConn).Params.Clear;
        TSQLConnection(aConn).Params.Add('SchemaOverride=sa.dbo'              );
        TSQLConnection(aConn).Params.Add('DriverName=MSSQL'                   );
        TSQLConnection(aConn).Params.Add('HostName=ServerName'                );
        TSQLConnection(aConn).Params.Add('DataBase=Database Name'             );
        TSQLConnection(aConn).Params.Add('User_Name=user'                     );
        TSQLConnection(aConn).Params.Add('Password=password'                  );
        TSQLConnection(aConn).Params.Add('BlobSize=-1'                        );
        TSQLConnection(aConn).Params.Add('ErrorResourceFile='                 );
        TSQLConnection(aConn).Params.Add('LocaleCode=0000'                    );
        TSQLConnection(aConn).Params.Add('MSSQL TransIsolation=ReadCommited'  );
        TSQLConnection(aConn).Params.Add('OS Authentication=False'            );
        TSQLConnection(aConn).Params.Add('Prepare SQL=False'                  );
      end;
      dtOracle:
      begin
        TSQLConnection(aConn).ConnectionName := 'OracleConnection';
        TSQLConnection(aConn).DriverName := 'Oracle';
        TSQLConnection(aConn).GetDriverFunc := 'getSQLDriverORACLE';
        TSQLConnection(aConn).LibraryName := 'dbxora.dll';
        TSQLConnection(aConn).LoginPrompt := false;
        TSQLConnection(aConn).VendorLib := 'oci.dll';

        TSQLConnection(aConn).Params.Clear;
        TSQLConnection(aConn).Params.Add('DriverName=Oracle'                  );
        TSQLConnection(aConn).Params.Add('DataBase=Database Name'             );
        TSQLConnection(aConn).Params.Add('User_Name=user'                     );
        TSQLConnection(aConn).Params.Add('Password=password'                  );
        TSQLConnection(aConn).Params.Add('RowsetSize=20'                      );
        TSQLConnection(aConn).Params.Add('BlobSize=-1'                        );
        TSQLConnection(aConn).Params.Add('ErrorResourceFile='                 );
        TSQLConnection(aConn).Params.Add('LocaleCode=0000'                    );
        TSQLConnection(aConn).Params.Add('Oracle TransIsolation=ReadCommited' );
        TSQLConnection(aConn).Params.Add('OS Authentication=False'            );
        TSQLConnection(aConn).Params.Add('Multiple Transaction=False'         );
        TSQLConnection(aConn).Params.Add('Trim Char=False'                    );
        TSQLConnection(aConn).Params.Add('Decimal Separator=.'                );
      end;
      dtAccess:
      begin
      end;
      dtDB2:
      begin
      end;
      dtMysql:
      begin
      end;
    end;
  end;
end;

function ABGetConnParams(
                   aDatabaseType:TABDatabaseType;
                   aHostName,aDataBase,aUser_Name,aPassword:string
                   ):TABConnParams;
begin
  result.DatabaseType:=aDatabaseType;
  result.Host       :=aHostName;
  result.Database   :=aDataBase;
  result.UserName   :=aUser_Name;
  result.Password   :=aPassword;
end;


function ABOpenConnection(
                   aConn:TCustomConnection;
                   aConnParams:TABConnParams;

                   aOpenConn:boolean=True;
                   aShowOkInfo:boolean=True;aOkInfoStr:string='';
                   aShowAbortInfo:boolean=True;aAbortInfoStr:string=''): boolean;
begin
  aConn.Close;
  ABSetConnParams(aConnParams,aConn);
  try
    if aOpenConn then
    begin
      aConn.Connected:=true;

      if aShowOKInfo then
      begin
        if aOKInfoStr=EmptyStr then
          aOKInfoStr:='数据库[%s]连接成功.';

        ABShow(aOKInfoStr,[aConnParams.Database]);
      end;
    end;
    Result:=true;
  except
    Result:=False;
    if aShowAbortInfo then
    begin
      if aAbortInfoStr=EmptyStr then
        aAbortInfoStr:='数据库[%s]连接失败.';

      ABShow(aAbortInfoStr,[aConnParams.Database]);
    end;
  end;
end;

function ABOpenConnection(
                   aConn:TCustomConnection;aDatabaseType:TABDatabaseType;
                   aHostName,aDataBase,aUser_Name,aPassword:string;
                   aOpenConn:boolean;
                   aShowOkInfo:boolean;aOkInfoStr:string;
                   aShowAbortInfo:boolean;aAbortInfoStr:string): booleaN;
var
  tempConnParams:TABConnParams;
begin
  aConn.Close;
  tempConnParams:=ABGetConnParams( aDatabaseType,aHostName,aDataBase,aUser_Name,aPassword);
  Result:=ABOpenConnection(
                 aConn,
                 tempConnParams ,
                 aOpenConn,
                 aShowOkInfo,aOkInfoStr,
                 aShowAbortInfo,aABortInfoStr);
end;

function ABGetADOConnectionConnStr( aDatabaseType:TABDatabaseType;
                                       aHostName,aDataBase,aUserName,aPassword:string;
                                       aProviderName:string):string;
begin
  result:='Provider='+aProviderName;
  case aDatabaseType of
    dtSQLServer:
    begin
      if aUserName=EmptyStr then
      begin
        result:=result+';Integrated Security=SSPI;Persist Security Info=False';
      end
      else
      begin
        if aPassword<>EmptyStr then
          result:=result+';Password='+aPassword;
        result:=result+';Persist Security Info=True';
        if aUserName<>EmptyStr then
          result:=result+';User ID='+aUserName;
      end;
      if aDatabase<>EmptyStr then
        result:=result+';Initial Catalog='+aDatabase;
      if aHostName<>EmptyStr then
        result:=result+';Data Source='+aHostName;
    end;
    dtOracle:
    begin
      if aPassword<>EmptyStr then
        result:=result+';Password='+aPassword;
      if aUserName<>EmptyStr then
        result:=result+';User ID='+aUserName;
      if aHostName<>EmptyStr then
        result:=result+';Data Source='+aHostName;
      result:=result+';Persist Security Info=True';
    end;
    dtAccess:
    begin

    end;
    dtDB2:
    begin

    end;
    dtMysql:
    begin

    end;
  end;

end;

procedure ABSetConnParams(aConnParams:TABConnParams;aConn: TCustomConnection);
var
  tempStr1,tempUDLFileName,
  tempDriverName:string;
begin
  aConn.LoginPrompt:=false;
  if aConn is  TADOConnection then
  begin
    tempStr1:=ABGetADOConnectionConnStr(aConnParams.DatabaseType,
                                           aConnParams.Host,aConnParams.Database,aConnParams.UserName,aConnParams.Password);
    if AnsiCompareText('FILE NAME',copy(TADOConnection(aConn).ConnectionString,1,length('FILE NAME')))=0 then
    begin
      tempUDLFileName:=ABGetLeftRightStr(TADOConnection(aConn).ConnectionString,axdright,'FILE NAME','=');
      if tempUDLFileName<>emptystr then
      begin
        tempStr1:='[oledb]'+ABEnterWrapStr+
                  '; Everything after this line is an OLE DB initstring'+ABEnterWrapStr+
                  tempStr1;

        ABWriteTxt(tempUDLFileName,tempStr1,tfUnicode);
      end
    end
    else
    begin
      TADOConnection(aConn).Connected:=false;
      TADOConnection(aConn).ConnectionString:=tempStr1;
    end;
  end
  else if aConn is  TFireDACConnection then
  begin
    tempDriverName:=emptystr;
    case aConnParams.DatabaseType of
      dtSQLServer:
      begin
        tempDriverName:='MSSQL';
      end;
      dtOracle:
      begin
        tempDriverName:='';
      end;
      dtAccess:
      begin
      end;
      dtDB2:
      begin
      end;
      dtMysql:
      begin
      end;
    end;
    TFireDACConnection(aConn).DriverName :=tempDriverName;
    ABWriteItemInStrings('Server'   ,aConnParams.Host,TFireDACConnection(aConn).Params);
    ABWriteItemInStrings('Database' ,aConnParams.Database,TFireDACConnection(aConn).Params);
    ABWriteItemInStrings('OSAuthent',ABIIF(aConnParams.UserName=EmptyStr,'Yes','No'),TFireDACConnection(aConn).Params);
    ABWriteItemInStrings('User_Name',aConnParams.UserName,TFireDACConnection(aConn).Params);
    ABWriteItemInStrings('Password' ,aConnParams.Password,TFireDACConnection(aConn).Params);
  end
  else if aConn is  TSQLConnection then
  begin
    tempDriverName:=emptystr;
    case aConnParams.DatabaseType of
      dtSQLServer:
      begin
        tempDriverName:='MSSQL';
      end;
      dtOracle:
      begin
        tempDriverName:='Oracle';
      end;
      dtAccess:
      begin
      end;
      dtDB2:
      begin
      end;
      dtMysql:
      begin
      end;
    end;
    TSQLConnection(aConn).DriverName                        :=tempDriverName;
    ABWriteItemInStrings('HostName'   ,aConnParams.Host,TSQLConnection(aConn).Params);
    ABWriteItemInStrings('DataBase' ,aConnParams.Database,TSQLConnection(aConn).Params);
    ABWriteItemInStrings('OS Authentication',ABBoolToStr(aConnParams.UserName=EmptyStr),TSQLConnection(aConn).Params);
    ABWriteItemInStrings('User_Name',aConnParams.UserName,TSQLConnection(aConn).Params);
    ABWriteItemInStrings('Password' ,aConnParams.Password,TSQLConnection(aConn).Params);
  end;
end;

function ABGetConnParams(aConn: TCustomConnection):TABConnParams;
  function GetDatabaseType(aDatabaseTypeName:string):TABDatabaseType;
  begin
    result:=dtNullDatabase;
    if aConn is  TADOConnection then
    begin
      if AnsiCompareText(aDatabaseTypeName,'SQLOLEDB.1')=0 then
      begin
        result:=dtSQLServer;
      end
      else if AnsiCompareText(aDatabaseTypeName,'MSDAORA.1')=0 then
      begin
        result:=dtOracle;
      end
      else if AnsiCompareText(aDatabaseTypeName,'')=0 then
      begin
        result:=dtAccess;
      end
      else if AnsiCompareText(aDatabaseTypeName,'')=0 then
      begin
        result:=dtDB2;
      end
      else if AnsiCompareText(aDatabaseTypeName,'')=0 then
      begin
        result:=dtMysql;
      end
    end
    else if aConn is  TFireDACConnection then
    begin
      if AnsiCompareText(aDatabaseTypeName,'MSSQL')=0 then
      begin
        result:=dtSQLServer;
      end
      else if AnsiCompareText(aDatabaseTypeName,'Ora')=0 then
      begin
        result:=dtOracle;
      end
      else if AnsiCompareText(aDatabaseTypeName,'')=0 then
      begin
        result:=dtAccess;
      end
      else if AnsiCompareText(aDatabaseTypeName,'')=0 then
      begin
        result:=dtDB2;
      end
      else if AnsiCompareText(aDatabaseTypeName,'')=0 then
      begin
        result:=dtMysql;
      end
    end
    else if aConn is  TSQLConnection then
    begin
      if AnsiCompareText(aDatabaseTypeName,'MSSQLConnection')=0 then
      begin
        result:=dtSQLServer;
      end
      else if AnsiCompareText(aDatabaseTypeName,'OracleConnection')=0 then
      begin
        result:=dtOracle;
      end
      else if AnsiCompareText(aDatabaseTypeName,'')=0 then
      begin
        result:=dtAccess;
      end
      else if AnsiCompareText(aDatabaseTypeName,'')=0 then
      begin
        result:=dtDB2;
      end
      else if AnsiCompareText(aDatabaseTypeName,'')=0 then
      begin
        result:=dtMysql;
      end
    end;
  end;
  function GetUDLConnectionStr(aUDLFileName: string):string;
  var
    tempStr1:string;
    i:longint;
  begin
    result:=emptystr;
    if ABCheckFileExists(aUDLFileName) then
    begin
      tempStr1:= ABReadtxt(aUDLFileName);
      i:=abpos('Provider',tempStr1);
      if i>0 then
      begin
        result:=Copy(tempStr1,i,MaxInt);
      end;
    end
  end;

var
  tempStr1:string;
begin
  if aConn is TADOConnection then
  begin
    tempStr1:= TADOConnection(aConn).ConnectionString;
    if AnsiCompareText('FILE NAME',copy(tempStr1,1,length('FILE NAME')))=0 then
    begin
      tempStr1:=GetUDLConnectionStr(ABGetLeftRightStr(tempStr1,axdright,'FILE NAME','='));
    end;

    Result.DatabaseType:=GetDatabaseType(ABGetItemValue(tempStr1,'Provider','=',';'));
    Result.Host        :=ABGetItemValue(tempStr1,'Data Source','=');
    Result.Database    :=ABGetItemValue(tempStr1,'Initial Catalog','=',';');
    Result.UserName    :=ABGetItemValue(tempStr1,'User ID','=',';');
    Result.Password    :=ABGetItemValue(tempStr1,'Password','=',';');

    if ABGetItemValue(tempStr1,'Integrated Security','=',';')<>EmptyStr then
    begin
      Result.UserName:=EmptyStr;
      Result.Password:=EmptyStr;
    end;
  end
  else if aConn is  TFireDACConnection then
  begin
    Result.DatabaseType:=GetDatabaseType(TFireDACConnection(aconn).DriverName);
    Result.Host        :=TFireDACConnection(aConn).Params.Values['Server'];
    Result.Database    :=TFireDACConnection(aConn).Params.Values['DataBase'];
    Result.UserName    :=TFireDACConnection(aConn).Params.Values['User_Name'];
    Result.Password    :=TFireDACConnection(aConn).Params.Values['Password'];
    if AnsiCompareText(TFireDACConnection(aConn).Params.Values['OSAuthent'],'Yes')=0 then
    begin
      Result.UserName:=EmptyStr;
      Result.Password:=EmptyStr;
    end;
  end
  else if aConn is  TSQLConnection then
  begin
    Result.DatabaseType:=GetDatabaseType(TSQLConnection(aconn).DriverName);
    Result.Host        :=TSQLConnection(aConn).Params.Values['HostName'];
    Result.Database    :=TSQLConnection(aConn).Params.Values['DataBase'];
    Result.UserName    :=TSQLConnection(aConn).Params.Values['User_Name'];
    Result.Password    :=TSQLConnection(aConn).Params.Values['Password'];
    if AnsiCompareText(TSQLConnection(aConn).Params.Values['OS Authentication'],'true')=0 then
    begin
      Result.UserName:=EmptyStr;
      Result.Password:=EmptyStr;
    end;
  end;
end;

procedure ABSetDatasetAutoParams(aDataset: TDataSet;aExtParamsDataset: TDataSet);
var
  tempCurParamName: string;
  tempStr1: string;
  i: LongInt;
begin
  if (Assigned(aDataset))then
  begin
    for i := 0 to ABGetParamsCount(aDataset) - 1 do
    begin
      tempCurParamName := ABGetParamName(aDataset, i);

      //如果参数是检测数据集的其它字段中,则替换之
      if (Assigned(aExtParamsDataset)) and
         (Assigned(aExtParamsDataset.FindField(tempCurParamName))) then
      begin
        ABSetParamValue(aDataset, i, aExtParamsDataset.FindField(tempCurParamName).Value);
      end
        //如果参数在父数据集中,则替换之
      else if (Assigned(aExtParamsDataset)) and
              (Assigned(aExtParamsDataset.DataSource)) and
              (Assigned(aExtParamsDataset.DataSource.DataSet)) and
              (Assigned(aExtParamsDataset.DataSource.DataSet.FindField(tempCurParamName))) then
      begin
        ABSetParamValue(aDataset, i, aExtParamsDataset.DataSource.DataSet.FindField(tempCurParamName).Value);
      end
      //如果参数是前数据集的其它字段中,则替换之
      else if Assigned(aDataSet.FindField(tempCurParamName)) then
      begin
        ABSetParamValue(aDataset, i, aDataSet.FindField(tempCurParamName).Value);
      end
      //如果参数在参数数据集中,则替换之
      else if abpos('ABParams_',tempCurParamName)>0 then
      begin
        if (Assigned(ABPubParamDataset)) and
           (ABPubParamNameFieldName<>emptystr) and
           (ABPubParamValueFieldName<>emptystr) then
        begin
          tempStr1 := ABStringReplace(tempCurParamName, 'ABParams_', EmptyStr);
          if ABPubParamDataset.Locate(ABPubParamNameFieldName, tempStr1, [loCaseInsensitive]) then
          begin
            ABSetParamValue(aDataSet,tempCurParamName,ABPubParamDataset.FindField(ABPubParamValueFieldName).AsString);
          end;
        end;
      end
      //如果参数是ABUser的参数值
      else if abpos('ABUser_',tempCurParamName)>0 then
      begin
             if AnsiCompareText(tempCurParamName, 'ABUser_Guid'                  )=0 then tempStr1:=ABPubUser.Guid
        else if AnsiCompareText(tempCurParamName, 'ABUser_Code'                  )=0 then tempStr1:=ABPubUser.Code
        else if AnsiCompareText(tempCurParamName, 'ABUser_Name'                  )=0 then tempStr1:=ABPubUser.Name
        else if AnsiCompareText(tempCurParamName, 'ABUser_PassWord'              )=0 then tempStr1:=ABPubUser.PassWord
        else if AnsiCompareText(tempCurParamName, 'ABUser_EncryptPassWord'       )=0 then tempStr1:=ABPubUser.EncryptPassWord
        else if AnsiCompareText(tempCurParamName, 'ABUser_FuncRight'             )=0 then tempStr1:=ABPubUser.FuncRight
        else if AnsiCompareText(tempCurParamName, 'ABUser_CustomFunc'            )=0 then tempStr1:=ABPubUser.CustomFunc

        else if AnsiCompareText(tempCurParamName, 'ABUser_UseBeginDateTime'      )=0 then tempStr1:=ABDateTimeToStr(ABPubUser.UseBeginDateTime)
        else if AnsiCompareText(tempCurParamName, 'ABUser_UseEndDateTime'        )=0 then tempStr1:=ABDateTimeToStr(ABPubUser.UseEndDateTime)
        else if AnsiCompareText(tempCurParamName, 'ABUser_LicenseBeginDateTime'  )=0 then tempStr1:=ABDateTimeToStr(ABPubUser.LicenseBeginDateTime)
        else if AnsiCompareText(tempCurParamName, 'ABUser_LicenseEndDateTime'    )=0 then tempStr1:=ABDateTimeToStr(ABPubUser.LicenseEndDateTime)
        else if AnsiCompareText(tempCurParamName, 'ABUser_PassWordEndDateTime'   )=0 then tempStr1:=ABDateTimeToStr(ABPubUser.PassWordEndDateTime)
        else if AnsiCompareText(tempCurParamName, 'ABUser_LoginDateTime'         )=0 then tempStr1:=ABDateTimeToStr(ABPubUser.LoginDateTime)
        else if AnsiCompareText(tempCurParamName, 'ABUser_LoginOutDateTime'      )=0 then tempStr1:=ABDateTimeToStr(ABPubUser.LoginOutDateTime)
        else if AnsiCompareText(tempCurParamName, 'ABUser_SysLanguage'           )=0 then tempStr1:=ABGetSysLanguageStr(ABGetSysLanguage)
        else if AnsiCompareText(tempCurParamName, 'ABUser_SysVersion'            )=0 then tempStr1:=ABGetSysVersion
        else if AnsiCompareText(tempCurParamName, 'ABUser_HostName'              )=0 then tempStr1:=ABPubUser.HostName
        else if AnsiCompareText(tempCurParamName, 'ABUser_HostIP'                )=0 then tempStr1:=ABPubUser.HostIP
        else if AnsiCompareText(tempCurParamName, 'ABUser_MacAddress'            )=0 then tempStr1:=ABPubUser.MacAddress
        else if AnsiCompareText(tempCurParamName, 'ABUser_BroadIP'               )=0 then tempStr1:=ABPubUser.BroadIP
        else if AnsiCompareText(tempCurParamName, 'ABUser_LoginType'             )=0 then tempStr1:=GetEnumName(TypeInfo(TABLoginType), Ord(ABLocalParams.LoginType))
        else if AnsiCompareText(tempCurParamName, 'ABUser_VarGuid'               )=0 then tempStr1:=ABPubUser.VarGuid
        else if AnsiCompareText(tempCurParamName, 'ABUser_VarDatetime'           )=0 then tempStr1:=ABDateTimeToStr(ABPubUser.VarDatetime)


        else if AnsiCompareText(tempCurParamName, 'ABUser_IsNoCheckHost'         )=0 then tempStr1:=IntToStr(ABBoolToInt(ABPubUser.IsNoCheckHost))
        else if AnsiCompareText(tempCurParamName, 'ABUser_IsAdmin'               )=0 then tempStr1:=IntToStr(ABBoolToInt(ABPubUser.IsAdmin))
        else if AnsiCompareText(tempCurParamName, 'ABUser_IsSysuser'             )=0 then tempStr1:=IntToStr(ABBoolToInt(ABPubUser.IsSysuser))
        else if AnsiCompareText(tempCurParamName, 'ABUser_IsAdminOrSysuser'      )=0 then tempStr1:=IntToStr(ABBoolToInt(ABPubUser.IsAdminOrSysuser))
        else if AnsiCompareText(tempCurParamName, 'ABUser_UseFuncRight'          )=0 then tempStr1:=IntToStr(ABBoolToInt(ABPubUser.UseFuncRight))
        else if AnsiCompareText(tempCurParamName, 'ABUser_SPID'                  )=0 then tempStr1:=ABPubUser.SPID
        else if AnsiCompareText(tempCurParamName, 'ABUser_NoCheckAdminPasss'     )=0 then tempStr1:=ABPubUser.NoCheckAdminPasss.text
        else continue;


        ABSetParamValue(aDataSet, tempCurParamName,tempStr1);
      end;
    end;
  end;
end;

function ABGetParamType(aDataSet: TDataSet; aParamName: string): TDataType;
begin
  if (aDataSet is TFireDACQuery) then
    Result := TFireDACQuery(aDataSet).FindParam(aParamName).DataType
  else if (aDataSet is TADOQuery) then
    Result := TADOQuery(aDataSet).Parameters.FindParam(aParamName).DataType
  else if (aDataSet is TClientDataSet) then
    Result := TClientDataSet(aDataSet).Params.FindParam(aParamName).DataType
  else
    Result := ftUnknown;
end;

function ABGetParamType(aDataSet: TDataSet; aParamIndex: LongInt): TDataType;
begin
  if (aDataSet is TFireDACQuery) then
    Result := TFireDACQuery(aDataSet).Params[aParamIndex].DataType
  else if (aDataSet is TADOQuery) then
    Result := TADOQuery(aDataSet).Parameters[aParamIndex].DataType
  else if (aDataSet is TClientDataSet) then
    Result := TClientDataSet(aDataSet).Params.Items[aParamIndex].DataType
  else
    Result := ftUnknown;
end;

procedure ABSetParamType(aDataSet: TDataSet; aParamIndex: LongInt; aParamType:
  TDataType = ftString);
begin
  if (aDataSet is TFireDACQuery) then
    TFireDACQuery(aDataSet).Params[aParamIndex].DataType:=aParamType
  else if (aDataSet is TADOQuery) then
    TADOQuery(aDataSet).Parameters[aParamIndex].DataType:=aParamType
  else if (aDataSet is TClientDataSet) then
    TClientDataSet(aDataSet).Params.Items[aParamIndex].DataType:=aParamType;
end;

procedure ABSetParamType(aDataSet: TDataSet; aParamName: string; aParamType:
  TDataType = ftString);
begin
  if (aDataSet is TFireDACQuery) then
    TFireDACQuery(aDataSet).FindParam(aParamName).DataType:=aParamType
  else if (aDataSet is TADOQuery) then
    TADOQuery(aDataSet).Parameters.FindParam(aParamName).DataType:=aParamType
  else if (aDataSet is TClientDataSet) then
    TClientDataSet(aDataSet).Params.FindParam(aParamName).DataType:=aParamType;
end;

procedure ABCopyParams(aFromDataSet:TFireDACQuery; aToParams: TParams);
var
  I:LongInt;
  tempMemoryStream :TMemoryStream;
  tempFindField:Tfield;
begin
  if aFromDataSet.ParamCount>0 then
  begin
    aToParams.Clear;
    tempMemoryStream :=TMemoryStream.Create;
    try
      for I := 0 to aFromDataSet.ParamCount-1 do
      begin
        aToParams.CreateParam(aFromDataSet.Params[i].DataType,aFromDataSet.Params[i].Name,aFromDataSet.Params[i].ParamType);

        tempFindField:=nil;
        if (Assigned(aFromDataSet.MasterSource)) and
           (Assigned(aFromDataSet.MasterSource.DataSet))
            then
          tempFindField:=aFromDataSet.MasterSource.DataSet.FindField(aFromDataSet.Params[i].Name);

        if aFromDataSet.Params[i].DataType = ftBlob then
        begin
          tempMemoryStream.Position:=0;
          if Assigned(tempFindField) then
          begin
            TBlobField(tempFindField).SaveToStream(tempMemoryStream);
          end
          else
          begin
            TBlobField(aFromDataSet.Params[i]).SaveToStream(tempMemoryStream);
          end;
          tempMemoryStream.Position:=0;
          aToParams[i].LoadFromStream(tempMemoryStream,ftBlob);
        end
        else
        begin
          if Assigned(tempFindField) then
          begin
            if tempFindField.IsNull then
              aToParams[i].Clear
            else
              aToParams[i].Value:=tempFindField.Value;
          end
          else
          begin
            if aFromDataSet.Params[i].IsNull then
              aToParams[i].Clear
            else
              aToParams[i].Value:=aFromDataSet.Params[i].Value;
          end;
        end;
      end;
    finally
      tempMemoryStream.Free;
    end;
  end;
end;

procedure ABCopyParams(aFromQuery:TParams; aToParams: TFDParams);
var
  I:LongInt;
  tempMemoryStream :TMemoryStream;
begin
  if aFromQuery.Count>0 then
  begin
    aToParams.Clear;
    tempMemoryStream :=TMemoryStream.Create;
    try
      for I := 0 to aFromQuery.Count-1 do
      begin
        aToParams.CreateParam(aFromQuery[i].DataType,aFromQuery[i].Name,aFromQuery[i].ParamType);

        if aFromQuery[i].DataType = ftBlob then
        begin
          tempMemoryStream.Position:=0;
          TBlobField(aFromQuery[i]).SaveToStream(tempMemoryStream);
          tempMemoryStream.Position:=0;
          aToParams[i].LoadFromStream(tempMemoryStream,ftBlob);
        end
        else
        begin
          if aFromQuery[i].IsNull then
            aToParams[i].Clear
          else
            aToParams[i].Value:=aFromQuery[i].Value;
        end;
      end;
    finally
      tempMemoryStream.Free;
    end;
  end;
end;

procedure ABSetUnknownParamType(aDataSet: TDataSet;aExtDataset: TDataSet; aParamType: TDataType = ftString);
var
  i: LongInt;
begin
  for I := 0 to ABGetParamsCount(aDataSet) - 1 do
  begin
    if ABGetParamType(aDataSet,i) = ftUnknown then
    begin
      if Assigned(aDataSet.FindField(ABGetParamName(aDataSet,i))) then
      begin
        ABSetParamType(aDataSet,i,aDataSet.FindField(ABGetParamName(aDataSet,i)).DataType);
      end
      else if (Assigned(aExtDataset)) and
              (Assigned(aExtDataset.FindField(ABGetParamName(aDataSet,i)))) then
      begin
        ABSetParamType(aDataSet,i,aExtDataset.FindField(ABGetParamName(aDataSet,i)).DataType);
      end
      else
      begin
        ABSetParamType(aDataSet,i,aParamType);
      end;
    end;
  end;
end;

procedure ABSetParamValue(aDataSet: TDataSet; aParamValue: array of Variant);
var
  i: LongInt;
begin
  for I := 0 to ABGetParamsCount(aDataSet) - 1 do
  begin
    if High(aParamValue) < i then
      Break;
    if Length(VarToStrDef(aParamValue[i], '')) > 100 then
      ABSetParamType(aDataSet,i,ftMemo);

    if (aDataSet is TFireDACQuery) then
    begin
      TFireDACQuery(aDataSet).Params[i].Value := aParamValue[i];
    end
    else if (aDataSet is TADOQuery) then
    begin
      TADOQuery(aDataSet).Parameters[i].Value := aParamValue[i];
    end
    else if (aDataSet is TClientDataSet) then
    begin
      TClientDataSet(aDataSet).Params[i].Value := aParamValue[i];
    end;
  end;
end;

function ABGetParamsCount(aDataSet: TDataSet): LongInt;
begin
  if (aDataSet is TFireDACQuery) then
    Result := TFireDACQuery(aDataSet).Params.Count
  else if (aDataSet is TADOQuery) then
    Result := TADOQuery(aDataSet).Parameters.Count
  else if (aDataSet is TClientDataSet) then
    Result := TClientDataSet(aDataSet).Params.Count
  else
    Result := 0;
end;

procedure ABClearParams(aDataSet: TDataSet);
begin
  if (aDataSet is TFireDACQuery) then
    TFireDACQuery(aDataSet).Params.Clear
  else if (aDataSet is TADOQuery) then
    TADOQuery(aDataSet).Parameters.Clear
  else if (aDataSet is TClientDataSet) then
    TClientDataSet(aDataSet).Params.Clear;
end;

function ABGetParamValue(aDataSet: TDataSet; aParamName: string): Variant;
begin
  if (aDataSet is TFireDACQuery) then
    Result := TFireDACQuery(aDataSet).FindParam(aParamName).Value
  else if (aDataSet is TADOQuery) then
    Result := TADOQuery(aDataSet).Parameters.FindParam(aParamName).Value
  else if (aDataSet is TClientDataSet) then
    Result := TClientDataSet(aDataSet).Params.FindParam(aParamName).Value
  else
    Result := null;
end;

function ABGetParamValue(aDataSet: TDataSet; aParamIndex: LongInt): Variant;
begin
  if (aDataSet is TFireDACQuery) then
    Result := TFireDACQuery(aDataSet).Params[aParamIndex].Value
  else if (aDataSet is TADOQuery) then
    Result := TADOQuery(aDataSet).Parameters[aParamIndex].Value
  else if (aDataSet is TClientDataSet) then
    Result := TClientDataSet(aDataSet).Params.Items[aParamIndex].Value
  else
    Result := null;
end;

function ABGetParamName(aDataSet: TDataSet; aParamIndex: LongInt): string;
begin
  if (aDataSet is TFireDACQuery) then
    Result := TFireDACQuery(aDataSet).Params[aParamIndex].Name
  else if (aDataSet is TADOQuery) then
    Result := TADOQuery(aDataSet).Parameters[aParamIndex].Name
  else if (aDataSet is TClientDataSet) then
    Result := TClientDataSet(aDataSet).Params.Items[aParamIndex].Name
  else
    Result := EmptyStr;
end;

function ABCheckDatasetParamsHaveValue(aDataSet: TDataSet): Boolean;
var
  tempParamValue: Variant;
  i: LongInt;
begin
  Result := true;
  for I := 0 to ABGetParamsCount(aDataSet) - 1 do
  begin
    tempParamValue := ABGetParamValue(aDataSet, i);
    if ABVarIsNull(tempParamValue) then
    begin
      Result := false;
      break;
    end;
  end;
end;

procedure ABSetParamValue(aDataSet: TDataSet; aParamIndex: LongInt; aParamValue:
  TMemoryStream);
begin
  if (aDataSet is TFireDACQuery) then
    TFireDACQuery(aDataSet).Params[aParamIndex].LoadFromStream(aParamValue,ftBlob)
  else if (aDataSet is TADOQuery) then
    TADOQuery(aDataSet).Parameters[aParamIndex].LoadFromStream(aParamValue, ftBlob)
  else if (aDataSet is TClientDataSet) then
    TClientDataSet(aDataSet).Params[aParamIndex].LoadFromStream(aParamValue, ftBlob);
end;

procedure ABSetParamValue(aDataSet: TDataSet; aParamName: string; aParamValue:
  TMemoryStream);
begin
  if (aDataSet is TFireDACQuery) then
    TFireDACQuery(aDataSet).FindParam(aParamName).LoadFromStream(aParamValue,ftBlob)
  else if (aDataSet is TADOQuery) then
    TADOQuery(aDataSet).Parameters.FindParam(aParamName).LoadFromStream(aParamValue, ftBlob)
  else if (aDataSet is TClientDataSet) then
    TClientDataSet(aDataSet).Params.FindParam(aParamName).LoadFromStream(aParamValue, ftBlob);
end;

procedure ABSetParamValue(aDataSet: TDataSet; aParamIndex: LongInt; aParamValue:Variant);
begin
  if (aDataSet is TFireDACQuery) then
    TFireDACQuery(aDataSet).Params[aParamIndex].Value := aParamValue
  else if (aDataSet is TADOQuery) then
    TADOQuery(aDataSet).Parameters[aParamIndex].Value := aParamValue
  else if (aDataSet is TClientDataSet) then
    TClientDataSet(aDataSet).Params[aParamIndex].Value := aParamValue;
end;

procedure ABSetParamValue(aDataSet: TDataSet; aParamName: string; aParamValue:
  Variant);
begin
  if (aDataSet is TFireDACQuery) then
    TFireDACQuery(aDataSet).FindParam(aParamName).Value := aParamValue
  else if (aDataSet is TADOQuery) then
    TADOQuery(aDataSet).Parameters.FindParam(aParamName).Value := aParamValue
  else if (aDataSet is TClientDataSet) then
    TClientDataSet(aDataSet).Params.FindParam(aParamName).Value := aParamValue;
end;

function ABGetParam(aDataSet: TDataSet; aParamName: string): TCollectionItem;
begin
  if (aDataSet is TFireDACQuery) then
    result := TFireDACQuery(aDataSet).FindParam(aParamName)
  else if (aDataSet is TADOQuery) then
    result := TADOQuery(aDataSet).Parameters.FindParam(aParamName)
  else if (aDataSet is TClientDataSet) then
    result := TClientDataSet(aDataSet).Params.FindParam(aParamName)
  else
    result :=nil;
end;

function ABGetDatasetSQL(aDataSet: TDataSet): string;
begin
  result := EmptyStr;
  if (aDataSet is TFireDACQuery) then
    result := TFireDACQuery(aDataSet).SQL.Text
  else if (aDataSet is TADOQuery) then
    result := TADOQuery(aDataSet).SQL.Text
  else if (aDataSet is TClientDataSet) then
    result := TClientDataSet(aDataSet).CommandText;
end;

procedure ABSetDatasetSQL(aDataSet: TDataSet; aSql: string);
begin
  if aDataSet.active then
    aDataSet.Close;

  if (aDataSet is TFireDACQuery) then
    TFireDACQuery(aDataSet).SQL.Text := aSql
  else if (aDataSet is TADOQuery) then
    TADOQuery(aDataSet).SQL.Text := aSql
  else if (aDataSet is TClientDataSet) then
    TClientDataSet(aDataSet).CommandText := aSql;
end;

function ABGetDatasetSQL_HaveParamValue(aDataset: TDataSet): string;
var
  i, j: LongInt;
  tempParamValue: Variant;
begin
  result := ABGetDatasetSQL(aDataset);
  j := ABGetParamsCount(aDataset) - 1;
  for i := 0 to j do
  begin
    if (ABGetParamType(aDataset, i) in ABBlobDataType) then
    begin
      tempParamValue := 'Blob Value...';
    end
    else
    begin
      tempParamValue := ABGetParamValue(aDataset, i);
    end;

    if i = 0 then
    begin
      result := result + ABEnterWrapStr + '[';
    end;
    result := result + QuotedStr(VarToStrDef(tempParamValue, ''));

    if i = j then
    begin
      result := result + ']';
    end
    else
      result := result + ',';
  end;
end;

function ABCloneCursor(aDataset:TDataSet): TDataSet;
var
  tempMemoryStream :TMemoryStream;
begin
  Result:=nil;
  if (aDataSet is TFireDACQuery) then
  begin
    Result := TFireDACQuery.Create(nil);
    TFireDACQuery(Result).Connection:=TFireDACConnection(TFireDACQuery(aDataSet).Connection);
    tempMemoryStream :=TMemoryStream.Create;
    try
      tempMemoryStream.Position:=0;
      TFireDACQuery(aDataSet).SaveToStream(tempMemoryStream);
      tempMemoryStream.Position:=0;
      TFireDACQuery(Result).LoadFromStream(tempMemoryStream);
    finally
      tempMemoryStream.Free;
    end;
  end
  else if (aDataSet is TADOQuery) then
  begin
    Result := TADOQuery.Create(nil);
    TADOQuery(Result).Clone(TADOQuery(aDataset));
  end
  else if (aDataSet is TClientDataSet) then
  begin
    Result := TClientDataSet.Create(nil);
    TClientDataSet(Result).FetchOnDemand := False;
    TClientDataSet(Result).CloneCursor(TClientDataSet(aDataset), False,true);
  end;

  if (Assigned(Result)) and
     (Result.Active) then
    ABCopyFieldDisplayLabel(aDataSet,Result);
end;

procedure ABReFreshQuery(aDataSet: TDataSet; aParamValue: array of Variant;aExtParamsDataset: TDataSet;
  aIsRequery: boolean; aIsDisableControls: boolean;
  aIsSavePosition: boolean;aReportRaise:Boolean);
var
  tempCommandtext: string;
  tempBookmark: TBookmark;
begin
  if not Assigned(aDataSet) then
    exit;

  tempCommandtext:= ABSetDatasetAutoParamsAndSQLParams(aDataSet,aExtParamsDataset);
  if tempCommandtext<>emptystr then
  begin
    try
      ABDoBusy;
      if aIsDisableControls then
        aDataSet.DisableControls;
      try
        if high(aParamValue) > -1 then
          ABSetParamValue(aDataSet, aParamValue);

        if (aDataSet.Active) then
        begin
          if (aIsSavePosition) and (aDataSet.RecordCount > 0) then
          begin
            tempBookmark := aDataSet.GetBookmark;
          end;
          try
            if (aIsRequery) and
               (ABLocalParams.LoginType=ltCS_Two) then
            begin
              ABPostDataset(aDataSet);
              if aDataSet is TFireDACQuery then
              begin
                TFireDACQuery(aDataSet).Refresh;
              end
              else if aDataSet is TADOQuery then
              begin
                TADOQuery(aDataSet).Refresh;
              end
              else if aDataSet is TClientDataSet then
              begin
                TClientDataSet(aDataSet).Refresh;
              end;
            end
            else
            begin
              ABCloseDataset(aDataSet);
              aDataSet.Active := true;
            end;
          finally
            if (not aDataSet.IsEmpty) and (aIsSavePosition) and
               (aDataSet.BookmarkValid(tempBookmark))  then
            begin
              try
                aDataSet.GotoBookmark(tempBookmark);
                aDataSet.FreeBookmark(tempBookmark);
              except
              end;
            end;
          end;
        end
        else
        begin
          ABCloseDataset(aDataSet);
          aDataSet.Active := true;
        end;
      finally
        ABDoBusy(false);
        if aIsDisableControls then
          aDataSet.EnableControls;
      end;
    except
      if aReportRaise then
        ABShow('错误SQL语句提示.',tempCommandtext);
      raise;
    end;
  end;
end;

function ABSetDatasetAutoParamsAndSQLParams(aDataSet: TDataSet;aExtParamsDataset: TDataSet;
                                            aSetAutoParams:boolean;aSetSQLParams:boolean):string;
var
  tempNewCommandtext:string;
begin
  Result:= trim(ABGetDatasetSQL(aDataSet));
  if Result<>EmptyStr then
  begin
    if aSetSQLParams then
    begin
      tempNewCommandtext := ABReplaceSqlExtParams(Result);
      tempNewCommandtext := trim(ABReplaceSqlFieldNameParams(tempNewCommandtext,[aExtParamsDataset]));

      if Result<>tempNewCommandtext then
      begin
        ABSetDatasetSQL(aDataset,tempNewCommandtext);
        Result:=tempNewCommandtext;
      end;
    end;

    if aSetAutoParams then
    begin
      ABSetUnknownParamType(aDataset,aExtParamsDataset);
      ABSetDatasetAutoParams(aDataset,aExtParamsDataset);
    end;
  end;
end;


{ TFireDACQuery }

constructor TFireDACQuery.Create(AOwner: TComponent);
begin
  inherited;
  FetchOptions.Mode:=fmAll;
  FetchOptions.Items:=FetchOptions.Items - [fiMeta];
end;

destructor TFireDACQuery.Destroy;
begin
  inherited;
end;

procedure TFireDACQuery.DoBeforeExecute;
begin
  inherited;
  if (Assigned(ABSQLLog)) and (Assigned(ABSQLLog.ExecSQL)) then
    ABSQLLog.ExecSQL('DoBeforeExecute',SQL.text);
end;

procedure TFireDACQuery.Loaded;
begin
  inherited;

end;

procedure TFireDACQuery.SetActive(Value: Boolean);
begin
  if Value then
  begin
    if (Assigned(ABSQLLog)) and (Assigned(ABSQLLog.ExecSQL)) then
      ABSQLLog.ExecSQL('SetActive',SQL.text);
  end;
  inherited;
end;

{ TFireDACConnection }

constructor TFireDACConnection.Create(AOwner: TComponent);
begin
  inherited;
  ResourceOptions.AutoReconnect:=True;
end;

destructor TFireDACConnection.Destroy;
begin

  inherited;
end;

function ABRound_(const AArgs: array of Variant): Variant;
begin
  Result :=ABRound(AArgs[0],AArgs[1])
end;

function ABIsNull_(const AArgs: array of Variant): Variant;
begin
  Result :=ABIsNull(AArgs[0],AArgs[1])
end;

procedure ABFinalization;
begin
end;

procedure ABInitialization;
var
  oMan: TFDExpressionManager;
begin
  oMan := FDExpressionManager();
  oMan.RemoveFunction('ABRound');
  oMan.RemoveFunction('ABIsNull');
  oMan.AddFunction('ABRound',       ckUnknown, 0, dtDouble,    -1, 2, 2, 'ii', @ABRound_);
  oMan.AddFunction('ABIsNull',       ckUnknown, 0, dtDouble,    -1, 2, 2, 'ii', @ABIsNull_);
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;


end.
