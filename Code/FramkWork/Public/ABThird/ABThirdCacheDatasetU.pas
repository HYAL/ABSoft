{
缓存数据集单元
}
unit ABThirdCacheDatasetU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFuncU,
  ABPubDBU,
  ABPubManualProgressBarU,

  ABThirdDBU,
  ABThirdCustomQueryU,

  SysUtils,Variants,Classes,DB;

//创建共用数据集
function ABCreatePubDataset(aName: string;
                            aConnName: string;
                            aSql: string;
                            aPropertyNames: array of string;
                            aPropertyValues: array of Variant;
                            aDataSetClass: TDataSetClass=nil;
                            aActive: Boolean = true;
                            aDeleteOld:Boolean=False
                            ): TDataSet;

//增加数据集到列表中
procedure ABAddPubDataset(aName: string;aDataset: TDataset);

//创建共用数据源
function ABCreatePubDataSource(aName: string): TDataSource;
//增加共用数据源到列表中
procedure ABAddPubDataSource(aName: string;aDataSource: TDataSource);

//得到共用数据集
function ABGetPubDataset(aName: string;aFilter:string='';aGoFirst: Boolean =false): TDataSet;
//取得共用数据源
function ABGetPubDataSource(aName: string): TDataSource;

//取共用数据集的字段值
function ABGetPubDatasetFieldValue(aName: string;aKeyFields: string; aKeyValues: Variant;
                                   aResultFields: string;aNullValue:Variant): Variant;
//修改、新增、删除共用数据集记录(全为NULL时删除，不存在时新增，存在时修改)
procedure ABEditPubDatasetData(
                                aName: string;
                                aKeyFieldNames: array of string;
                                aKeyFieldValues: array of Variant;
                                aFieldNames: array of string;
                                aFieldValues: array of Variant
                                  );
//刷新某个共用数据集
procedure ABRefreshPubDataset;overload;
//刷新所有共用数据集
procedure ABRefreshPubDataset(aName: string);overload;

//释放共用数据集
procedure ABFreePubDataset; overload;
procedure ABFreePubDataset(aName: string); overload;
//释放共用数据源
procedure ABFreePubDataSource; overload;
procedure ABFreePubDataSource(aName: string); overload;
                            
var
  ABPubDatasetList: TStrings;      //共用数据集列表
  ABPubDataSourceList: TStrings;   //共用数据源列表


implementation

function ABGetPubDataset_Single(aName: string): TDataSet;
var
  i: LongInt;
begin
  Result := nil;
  if aName<>EmptyStr then
  begin
    aName := Trim(aName);
    i := ABPubDatasetList.IndexOf(aName);
    if i >= 0 then
    begin
      Result := TDataSet(ABPubDatasetList.Objects[i]);
    end;
  end;
end;


procedure ABEditPubDatasetData(
                          aName: string;
                          aKeyFieldNames: array of string;
                          aKeyFieldValues: array of Variant;
                          aFieldNames: array of string;
                          aFieldValues: array of Variant
                            );
var
  tempDataset:TDataSet;
begin
  tempDataset:=ABGetPubDataset_Single(aName);
  if Assigned(tempDataset) then
  begin
    ABSetFieldValue(
                    tempDataset,
                    aKeyFieldNames,
                    aKeyFieldValues,
                    aFieldNames,
                    aFieldValues
                      );
  end;
end;

function ABCreatePubDataSource(aName: string): TDataSource;
begin
  aName := Trim(aName);
  Result := ABGetPubDataSource(aName);
  if not Assigned(Result) then
  begin
    Result := TDataSource.Create(nil);
    ABPubDataSourceList.AddObject(aName, result)
  end;
end;

//增加共用数据源
procedure ABAddPubDataSource(aName: string;aDataSource: TDataSource);
begin
  aName := Trim(aName);
  if ABPubDataSourceList.IndexOf(aName)<0 then
    ABPubDataSourceList.AddObject(aName, aDataSource)
end;

//增加共用数据集
procedure ABAddPubDataset(aName: string;aDataset: TDataset);
begin
  aName := Trim(aName);
  if (ABPubDatasetList.IndexOf(aName)<0) and
     (Assigned(aDataset)) then
    ABPubDatasetList.AddObject(aName, aDataset)
end;

function ABCreatePubDataset(aName: string;
                            aConnName: string;
                            aSql: string;
                            aPropertyNames: array of string;
                            aPropertyValues: array of Variant;
                            aDataSetClass: TDataSetClass ;
                            aActive: Boolean;
                            aDeleteOld:Boolean
                            ): TDataSet;
var
  i: LongInt;                    
begin
  result := nil;
  aName := Trim(aName);
  if aDeleteOld then
    ABFreePubDataset(aName)
  else
    result:=ABGetPubDataset_Single(aName);

  if (not Assigned(result)) and
     (aName <> EmptyStr) then
  begin
    if not Assigned(aDataSetClass) then
    begin
      Result:=TABThirdReadDataQuery.Create(nil);
    end
    else
    begin
      Result := aDatasetClass.Create(nil);
    end;
    ABSetPropValue(Result,'ConnName',aConnName);
    ABSetDatasetSQL(Result, aSql);

    for i := Low(aPropertyNames) to High(aPropertyNames) do
    begin
      if ABIsPublishProp(Result, aPropertyNames[i]) then
        ABSetPropValue(Result, aPropertyNames[i],aPropertyValues[i]);
    end;

    if aActive then
      result.Open;

    ABPubDatasetList.AddObject(aName, result)
  end;
end;

procedure ABFreePubDataset;
var
  i: LongInt;
begin
  for I := ABPubDatasetList.Count - 1 downto 0 do
  begin
    ABFreePubDataset(ABPubDatasetList.Strings[i]);
  end;
  ABPubDatasetList.Clear;
end;

procedure ABFreePubDataSource;
var
  i: LongInt;
begin
  for I := ABPubDataSourceList.Count - 1 downto 0 do
  begin
    ABFreePubDataSource(ABPubDataSourceList.Strings[i]);
  end;
  ABPubDataSourceList.Clear;
end;

procedure ABFreePubDataset(aName: string);
var
  i: LongInt;
begin
  aName := Trim(aName);
  i := ABPubDatasetList.IndexOf(aName);
  if i >= 0 then
  begin
    ABPubDatasetList.Objects[i].Free;
    ABPubDatasetList.Delete(i);
  end;
end;

procedure ABFreePubDataSource(aName: string);
var
  i: LongInt;
begin
  aName := Trim(aName);
  i := ABPubDataSourceList.IndexOf(aName);
  if i >= 0 then
  begin
    ABPubDataSourceList.Objects[i].Free;
    ABPubDataSourceList.Delete(i);
  end;
end;

function ABGetPubDataset(aName: string; aFilter:string;aGoFirst: Boolean): TDataSet;
var
  tempOldBeforeScroll: TDataSetNotifyEvent;
  tempOldAfterScroll: TDataSetNotifyEvent;
  tempSetFilter:boolean;
  //當然你也可以加入其他DataSet不想觸發的事件,但必須避免有其他影響
begin
  aName := Trim(aName);
  Result := ABGetPubDataset_Single(aName);                
  if (Assigned(Result)) then
  begin
    aFilter:=Trim(aFilter);
    if AnsiCompareText(Copy(aFilter,1,5),'Where')=0 then
    begin
      aFilter:=Trim(Copy(aFilter,6,maxint));
    end;

    tempSetFilter:=(ABGetDatasetFilter(Result) <> aFilter);
    aGoFirst:=(aGoFirst) and (Result.Active) and (not Result.Bof);
    if tempSetFilter or aGoFirst then
    begin
      ABBackAndStopDatasetEvent(Result,tempOldBeforeScroll,tempOldAfterScroll);
      if aGoFirst then
        Result.First;
      try
        if tempSetFilter then
        begin
          ABSetDatasetFilter(Result, aFilter);
        end;
      finally
        ABUnBackDatasetEvent(Result,tempOldBeforeScroll,tempOldAfterScroll);
      end;
    end;
  end;
end;

function ABGetPubDatasetFieldValue(aName: string;aKeyFields: string; aKeyValues: Variant;
                                           aResultFields: string;aNullValue:Variant): Variant;
begin
  Result := ABGetFieldValue(ABGetPubDataset_Single(aName),
                            aKeyFields,aKeyValues,
                            aResultFields,aNullValue);
end;

procedure ABRefreshPubDataset;
var
  i:LongInt;
begin
  ABGetPubManualProgressBarForm.Title:='刷新中';
  ABGetPubManualProgressBarForm.MainMin:=0;
  ABGetPubManualProgressBarForm.MainPosition:=0;
  ABGetPubManualProgressBarForm.MainMax:=ABPubDatasetList.Count;
  ABGetPubManualProgressBarForm.Run(true);
  try
    for I := 0 to ABPubDatasetList.Count-1 do
    begin
      if ABGetPubManualProgressBarForm.ExitRun then
        break;
      ABGetPubManualProgressBarForm.NextMainIndex;

      ABReFreshQuery(TDataSet(ABPubDatasetList.Objects[i]),[],nil,false);
    end;
  finally
    ABGetPubManualProgressBarForm.Stop;
  end;
end;

procedure ABRefreshPubDataset(aName: string);
var
  tempDataset: TDataSet;
begin
  aName := Trim(aName);
  tempDataset := ABGetPubDataset_Single(aName);
  if Assigned(tempDataset) then
  begin
    ABReFreshQuery(tempDataset, []);
  end;
end;             

function ABGetPubDataSource(aName: string): TDataSource;
var
  i: LongInt;
begin
  Result := nil;
  aName := Trim(aName);
  i := ABPubDataSourceList.IndexOf(aName);
  if i >= 0 then
  begin
    Result := TDataSource(ABPubDataSourceList.Objects[i]);
  end;
end;

procedure ABFinalization;
begin
  ABFreePubDataset;
  ABFreePubDataSource;

  ABPubDatasetList.Free;
  ABPubDataSourceList.Free;
end;

procedure ABInitialization;
begin
  ABPubDatasetList := TstringList.Create;
  ABPubDataSourceList := TstringList.Create;
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;


end.
