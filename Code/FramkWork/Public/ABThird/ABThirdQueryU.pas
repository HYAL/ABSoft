{
三方数据集单元
}
unit ABThirdQueryU;

interface

uses
  ABPubMessageU,
  ABPubVarU,
  ABPubConstU,
  ABPubFuncU,
  ABPubDBU,
  ABPubUserU,

  ABThirdDBU,
  ABThirdConnU,
  ABThirdConnDatabaseU,
  ABThirdCustomQueryU,

  FireDAC.Stan.ExprFuncs,
  FireDAC.Stan.StorageBin,
  FireDAC.Stan.StorageXML, FireDAC.Stan.StorageJSON,
  FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Comp.UI,
  FireDAC.Stan.Def,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.Phys.MSSQLDef, FireDAC.Phys, FireDAC.Phys.ODBCBase, FireDAC.Phys.MSSQL,


  SysUtils,Variants, Classes, DB, TypInfo;

type
  TABUpdatesEvent = procedure(aState: TDataSetState) of object;
  TABObjectTransitionEvent=procedure(aState: TABDatasetOperateType;var aObjectName:string) of object;

  PABCacheUpdateItem = ^TABCacheUpdateItem;
  TABCacheUpdateItem = record
    SQL: string;
    Params: array of Variant;
  end;
  //在三方数据集上增加框架更新数据库功能，更换三方数据集时此类尽可能不用变动
  TABThirdQuery = class(TABThirdReadDataQuery)
  private
    //缓存模式下保存的更新Query
    FCacheUpdateList: array of TABCacheUpdateItem;
  private
    FUpdateDatabase: Boolean;
    FCacheUpdate: Boolean;
    FBeforeUpdateTransitionTableName: TABObjectTransitionEvent;
    FIndexListDefs: TIndexDefs;
    FBeforeUpdateTransitionFieldName: TABObjectTransitionEvent;
    FUpdateTables: TStrings;
    FUpdateMode: TUpdateMode;

    FBeginTrans: TABUpdatesEvent;
    FEndTrans: TABUpdatesEvent;
    FBeforeStartTrans: TABUpdatesEvent;
    FAfterStartTrans: TABUpdatesEvent;
    FBeforeCommitTrans: TABUpdatesEvent;
    FAfterCommitTrans: TABUpdatesEvent;
    FBeforeRollbackTrans: TABUpdatesEvent;
    FAfterRollbackTrans: TABUpdatesEvent;
    FUseTrans: Boolean;

    procedure SetUpdateMode(const Value: TUpdateMode);
    procedure SetUpdateTables(const Value: TStrings);
  protected
    procedure DoBeforePost; override;
    procedure DoBeforeDelete; override;

    procedure DoBeginTrans; virtual;
    procedure DoEndTrans; virtual;

    procedure DoBeforeStartTrans; virtual;
    procedure DoAfterStartTrans; virtual;

    procedure DoBeforeCommitTrans; virtual;
    procedure DoAfterCommitTrans; virtual;

    procedure DoBeforeRollbackTrans; virtual;
    procedure DoAfterRollbackTrans; virtual;

    procedure DoBeforeUpdateTransitionTableName(aState: TABDatasetOperateType;var aTableName:string); virtual;
    procedure DoBeforeUpdateTransitionFieldName(aState: TABDatasetOperateType;var aFieldName:string); virtual;

    //SQL更改后触发函数
    procedure AfterChangSQL(aNewSQL: String; var aDesigning: Boolean;var aLoading: Boolean); override;
    //修改SQL后更新Strings的表名列表,在AfterChangSQL后调用
    procedure UpdateTableNameListsAfterChangSQL(aString:TStrings);

    //字段是否更新到数据库中
    function FieldCanUpdateToDatabase(aTableName, aFieldName: String;var aIsIDENTITY: Boolean): Boolean; virtual;
    //更新当前数据到数据库,如果CacheUpdate=True则生成更新SQL到CacheSQLList中,在需要更新时则调用CacheToDatabase
    function UpdatesToDatabase(aState: TDataSetState): Boolean; virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    //执行数据集
    function ExecQuery(aCacheUpdateLists:array of TABCacheUpdateItem): Boolean;
    //缓存数据更新到数据库中
    procedure CacheToDatabase;
    //缓存数据清除
    procedure CacheClear;
    //仅仅在数据集中删除
    procedure OnlyDatasetDelete;
    //仅仅在数据集中保存
    procedure OnlyDatasetPost;
  published

    //是否使用事务更新到数据库中
    property UseTrans:Boolean read FUseTrans  write FUseTrans ;
    //是否更新到数据库，为False时仅更新在数据集中
    property UpdateDatabase:Boolean read FUpdateDatabase  write FUpdateDatabase ;
    //是否缓存更新模式，此模式下当前只更新在数据集中，要显式调用CacheToDatabase才能更新更改的数据到数据库中
    property CacheUpdate:Boolean read FCacheUpdate  write FCacheUpdate ;
    //提交更新模式(如upWhereAll, upWhereChanged, upWhereKeyOnly),生成更新SQL中WHERE条件的依据
    property UpdateMode: TUpdateMode read FUpdateMode write SetUpdateMode;
    //需更新到数据库的表名列表
    property UpdateTables: TStrings read FUpdateTables write SetUpdateTables;

    //在下发SQL更新数据库数据前进行必要的表名或字段名转换,如因为数据库对多表视图的删除支持不够，在删除时要将视图变换为表名
    property BeforeUpdateTransitionTableName: TABObjectTransitionEvent read FBeforeUpdateTransitionTableName write FBeforeUpdateTransitionTableName;
    property BeforeUpdateTransitionFieldName: TABObjectTransitionEvent read FBeforeUpdateTransitionFieldName write FBeforeUpdateTransitionFieldName;

    //数据集相关事件
    //事务前事件
    property BeginTrans: TABUpdatesEvent read FBeginTrans write FBeginTrans;
    //事务后事件
    property EndTrans: TABUpdatesEvent read FEndTrans write FEndTrans;

    //开始事务前
    property BeforeStartTrans: TABUpdatesEvent read FBeforeStartTrans write FBeforeStartTrans;
    //开始事务后（在事务中）
    property AfterStartTrans: TABUpdatesEvent read FAfterStartTrans write FAfterStartTrans;

    //提交事务前（在事务中）
    property BeforeCommitTrans: TABUpdatesEvent read FBeforeCommitTrans write FBeforeCommitTrans;
    //提交事务后
    property AfterCommitTrans: TABUpdatesEvent read FAfterCommitTrans write FAfterCommitTrans;

    //撤回事务前（在事务中）
    property BeforeRollbackTrans: TABUpdatesEvent read FBeforeRollbackTrans write FBeforeRollbackTrans;
    //撤回事务后
    property AfterRollbackTrans: TABUpdatesEvent read FAfterRollbackTrans write FAfterRollbackTrans;

    //数据表索引,当更新出错时可以快速显示原因
    property IndexListDefs: TIndexDefs read FIndexListDefs  write FIndexListDefs;
  end;

implementation

  { TABThirdQuery }

var
  FParams:TParams;

function TABThirdQuery.FieldCanUpdateToDatabase(aTableName, aFieldName: String;var aIsIDENTITY: Boolean): Boolean;
begin
  result := true;
end;

procedure TABThirdQuery.OnlyDatasetDelete;
var
  tempBeforeDeleteAsk,
  tempUpdateDatabase:Boolean;
begin
  tempBeforeDeleteAsk:=BeforeDeleteAsk;
  BeforeDeleteAsk:=false;
  tempUpdateDatabase:=FUpdateDatabase;
  FUpdateDatabase:=False;
  try
    if not ABDatasetIsEmpty(self) then
      Delete;
  finally
    FUpdateDatabase:=tempUpdateDatabase;
    BeforeDeleteAsk:=tempBeforeDeleteAsk;
  end;
end;

procedure TABThirdQuery.OnlyDatasetPost;
var
  tempUpdateDatabase:Boolean;
begin
  tempUpdateDatabase:=FUpdateDatabase;
  FUpdateDatabase:=False;
  try
    ABPostDataset(self);
  finally
    FUpdateDatabase:=tempUpdateDatabase;
  end;
end;

procedure TABThirdQuery.SetUpdateTables(const Value: TStrings);
begin
  FUpdateTables.Assign(Value);
end;

procedure TABThirdQuery.AfterChangSQL(aNewSQL: String; var aDesigning,
  aLoading: Boolean);
begin
  inherited;
  UpdateTableNameListsAfterChangSQL(FUpdateTables);
end;

procedure TABThirdQuery.DoBeforeDelete;
begin
  inherited;
  UpdatesToDatabase(State);
end;

procedure TABThirdQuery.CacheClear;
var
  i:LongInt;
begin
  for I := Low(FCacheUpdateList) to High(FCacheUpdateList) do
  begin
    SetLength(FCacheUpdateList[i].Params,0);
  end;
  SetLength(FCacheUpdateList,0);
end;

procedure TABThirdQuery.CacheToDatabase;
begin
  if  ExecQuery(FCacheUpdateList) then
    CacheClear;
end;

procedure TABThirdQuery.DoBeginTrans;
begin
  if Assigned(FBeginTrans) then
    FBeginTrans(State);
end;

procedure TABThirdQuery.DoEndTrans;
begin
  if Assigned(FEndTrans) then
    FEndTrans(State);
end;

procedure TABThirdQuery.DoBeforeStartTrans;
begin
  if Assigned(FBeforeStartTrans) then
    FBeforeStartTrans(State);
end;

procedure TABThirdQuery.DoAfterStartTrans;
begin
  if Assigned(FAfterStartTrans) then
    FAfterStartTrans(State);
end;

procedure TABThirdQuery.DoBeforeCommitTrans;
begin
  if Assigned(FBeforeCommitTrans) then
    FBeforeCommitTrans(State);
end;

procedure TABThirdQuery.DoAfterCommitTrans;
begin
  if Assigned(FAfterCommitTrans) then
    FAfterCommitTrans(State);
end;

procedure TABThirdQuery.DoBeforeRollbackTrans;
begin
  if Assigned(FBeforeRollbackTrans) then
    FBeforeRollbackTrans(State);
end;

procedure TABThirdQuery.DoAfterRollbackTrans;
begin
  if Assigned(FAfterRollbackTrans) then
    FAfterRollbackTrans(State);
end;

procedure TABThirdQuery.DoBeforeUpdateTransitionTableName(aState: TABDatasetOperateType; var aTableName: string);
begin
  if Assigned(FBeforeUpdateTransitionTableName) then
    FBeforeUpdateTransitionTableName(aState,aTableName);
end;

procedure TABThirdQuery.DoBeforeUpdateTransitionFieldName(aState: TABDatasetOperateType; var aFieldName: string);
begin
  if Assigned(FBeforeUpdateTransitionFieldName) then
    FBeforeUpdateTransitionFieldName(aState,aFieldName);
end;

procedure TABThirdQuery.DoBeforePost;
begin
  inherited;
  UpdatesToDatabase(State);
end;

procedure TABThirdQuery.UpdateTableNameListsAfterChangSQL(aString:TStrings);
var
  I: LongInt;
begin
  if Assigned(aString) then
  begin
    if (aString.Count>0) then
    begin
      for I :=aString.Count-1 downto 0 do
      begin
        if (ABPos(aString[i] + ' ', Sql.Text + ' ') <= 0) and
           (ABPos(aString[i] + ABEnterWrapStr, Sql.Text + ABEnterWrapStr) <= 0) then
        begin
          aString.Delete(i);
        end;
      end;
    end;

    if (aString.Count<=0) then
    begin
      if (Assigned(SQLParseObject)) then
      begin
        ParseSQLToParseObject(Sql.Text);
        if SQLParseObject.SQLParseDef.SingleTableName<>emptystr then
          aString.add(SQLParseObject.SQLParseDef.SingleTableName);
      end;
    end;
  end;
end;

procedure TABThirdQuery.SetUpdateMode(const Value: TUpdateMode);
begin
  FUpdateMode := Value;
end;

function TABThirdQuery.ExecQuery(aCacheUpdateLists:array of TABCacheUpdateItem): Boolean;
var
  tempSQL:string;
  tempUseTrans:boolean;
  I: LongInt;
  //更新出错的处理
  procedure ShowUpdatesToDatabaseSQL(aError: string);
  var
    tempFieldName, tempstr1, tempStr2: string;
    I, j: LongInt;
    tempField: TField;
    tempFind: Boolean;
  begin
    //ABNotNullUnique是触发器中检测字段不能重复杂的标记
    if ABPos('ABNotNullUnique', aError) > 0 then
    begin
      for I := 0 to FieldCount - 1 do
      begin
        if ABPos(fields[I].FieldName + ',', aError + ',') > 0 then
        begin
          abshow('字段[%s]的值[%s]在数据库中已存在,此值不允许存在重复.',
            [fields[I].DisplayLabel, fields[I].AsString], tempSQL);
          ABSetControlFocusByField(fields[I]);
          Break;
        end;
      end;
    end
    else
    begin
      tempFind := false;
      for I := 0 to IndexListDefs.Count - 1 do
      begin
        if ABPos(QuotedStr(IndexListDefs[I].Name), aError) > 0 then
        begin
          tempstr1 := IndexListDefs[I].fields;
          tempStr2 := emptystr;
          for j := 1 to ABGetSpaceStrCount(tempstr1, ';') do
          begin
            tempFieldName := ABGetSpaceStr(tempstr1, j, ';');
            tempField := FindField(tempFieldName);
            if (Assigned(tempField)) then
            begin
              ABAddstr(tempStr2, tempField.DisplayLabel, '+');
            end;
          end;
          if tempStr2 <> emptystr then
          begin
            abshow('字段[%s]的值在数据库中已存在,这些值不允许存在重复.', [tempStr2], tempSQL);
            tempFind := true;
            Break;
          end;
        end;
      end;
      if not tempFind then
      begin
        abshow('SQL更新出错', aError + ABEnterWrapStr + tempSQL);
      end;

      //示例报错
      //SQL State: 01000, SQL Error Code: 3621'#$A'语句已终止。'#$A'SQL State: 23000,
      //SQL Error Code: 547'#$A'UPDATE 语句与 COLUMN FOREIGN KEY 约束 'FK_AIC_Access_Card_AIC_Access_AccessGroup'
      //冲突。该冲突发生于数据库 'NewAICDatabase'，表 'AIC_Access_AccessGroup', column 'AG_Guid' +
    end;
  end;
begin
  result := true;
  tempUseTrans:=FUseTrans;
  //开始事务 提交更新
  try
    if tempUseTrans then
    begin
      DoBeginTrans;

      DoBeforeStartTrans;
      ABBegTransaction(ConnName);
      DoAfterStartTrans;

      try
        for I := Low(aCacheUpdateLists) to High(aCacheUpdateLists) do
        begin
          ABExecSQL(ConnName,aCacheUpdateLists[i].SQL,aCacheUpdateLists[i].Params);
        end;

        DoBeforeCommitTrans;
        ABCommitTransaction(ConnName);
        DoAfterCommitTrans;

      except
        DoBeforeRollbackTrans;
        ABRollbackTransaction(ConnName);
        DoAfterRollbackTrans;

        raise;
      end;
      DoEndTrans;
    end
    else
    begin
      for I := Low(aCacheUpdateLists) to High(aCacheUpdateLists) do
      begin
        ABExecSQL(ConnName,aCacheUpdateLists[i].SQL,aCacheUpdateLists[i].Params);
      end;
    end;
  except
    on E: Exception do
    begin
      result := false;
      ShowUpdatesToDatabaseSQL(E.Message);
      Abort;
    end;
  end;
end;

function TABThirdQuery.UpdatesToDatabase(aState: TDataSetState): Boolean;
var
  tempTableName,tempFieldName, tempSQL: string;
  tempIDENTITYField: TField;
  //tempIDENTITYValue: Variant;
  tempIDENTITY: Boolean;
  I,j: LongInt;
  //生成更新需执行的SQL
  function GetUpdatesToDatabaseSQL(aTableName: string;var aOutUpdateSQL:string): Boolean;
  //生成修改语句
    function GetModifiedSQL: Boolean;
    var
      tempstr1, tempStr2, tempStr3: string;
      I: LongInt;
      tempField: TField;
      tempOldValue:Variant;
    begin
      result := false;
      for I := 0 to FieldCount - 1 do
      begin
        tempField := fields[I];
        if (tempField.FieldKind = fkCalculated) or
          (tempField.FieldKind = fkInternalCalc) or
          (tempField.FieldKind = fkAggregate) then
          Continue;

        if not FieldCanUpdateToDatabase(aTableName, tempField.FieldName,tempIDENTITY) then
          Continue;
        //得到Where部分
        if (pfInWhere in tempField.ProviderFlags) and
           (not (tempField.DataType in ABMemoDataType)) and
           (not (tempField.DataType in ABBlobDataType)) then
        begin
          tempStr3 := emptystr;
          if (UpdateMode = upWhereAll) or
            ((UpdateMode = upWhereChanged) and
            ((pfInKey in tempField.ProviderFlags) or (
            //(Assigned(tempFieldDef)) and (tempFieldDef.Fi_Edited) or
            (not ABComparisonFieldOldNewValueSame(tempField))))) or
            ((UpdateMode = upWhereKeyOnly) and
            (pfInKey in tempField.ProviderFlags)) then
          begin
            tempFieldName:=tempField.FieldName;
            DoBeforeUpdateTransitionFieldName(dtEdit,tempFieldName);

            tempOldValue:=ABGetFieldOldValue(tempField);
            if (ABVarIsNull(tempOldValue)) or
              ((VarIsStr(tempOldValue)) and
              (VarToStrDef(tempOldValue, emptystr) = emptystr)) then
              tempStr3 := tempFieldName + ' is null '
            else
              tempStr3 := tempFieldName + ' =:Old_' + tempField.FieldName;
          end;

          if tempStr3 <> emptystr then
          begin
            ABAddstr(tempstr1, tempStr3, ' and ');
          end;
        end;
        //得到update部分
        if (pfInUpdate in tempField.ProviderFlags) and (
           //(Assigned(tempFieldDef)) and (tempFieldDef.Fi_Edited) or
           (not ABComparisonFieldOldNewValueSame(tempField))) then
        begin
          tempStr3 := emptystr;
          tempFieldName:=tempField.FieldName;
          DoBeforeUpdateTransitionFieldName(dtEdit,tempFieldName);

          if (ABVarIsNull(ABGetFieldNewValue(tempField))) or
             ((VarIsStr(ABGetFieldNewValue(tempField))) and (VarToStrDef(ABGetFieldNewValue(tempField),emptystr) = emptystr)) then
            tempStr3 := tempFieldName + ' = null '
          else
            tempStr3 := tempFieldName + ' =:New_' + tempField.FieldName;

          if tempStr3 <> emptystr then
          begin
            ABAddstr(tempStr2, tempStr3);
          end;
        end;
      end;

      DoBeforeUpdateTransitionTableName(dtEdit,aTableName);

      aOutUpdateSQL := ' update ' + aTableName + ' set    ' + tempStr2 + ' where  '+ tempstr1;
      //得到更新的SQL语句
      if (tempstr1 = emptystr) then
      begin
        abshow('更新语句缺少Where部分,请检查.',aOutUpdateSQL);
      end
      else if (tempStr2 = emptystr) then
      begin
        //无set部分部分是很正常的，如没有更新的内容，所以不用提示
        //ABShow('更新语句缺少set部分,请检查.');
        aOutUpdateSQL :=emptystr;
        result := true;
      end
      else
      begin
        result := true;
      end;
    end;
  //生成删除语句
    function GetDeletedSQL: Boolean;
    var
      tempstr1, tempStr3: string;
      I: LongInt;
      tempField: TField;
      tempOldValue:Variant;
    begin
      result := false;
      for I := 0 to FieldCount - 1 do
      begin
        tempField := fields[I];
        if (tempField.FieldKind = fkCalculated) or
           (tempField.FieldKind = fkInternalCalc) or
           (tempField.FieldKind = fkAggregate) then
          Continue;

        if not FieldCanUpdateToDatabase(aTableName, tempField.FieldName,tempIDENTITY) then
          Continue;

        //得到Where部分
        if pfInWhere in tempField.ProviderFlags then
        begin
          tempStr3 := emptystr;
          if //计算,备注与二进制字段不能作为WHERE条件出现在SQL语句中
             (not (tempField.DataType in ABMemoDataType)) and
             (not (tempField.DataType in ABBlobDataType)) and
             //upWhereAll下字段或upWhereChanged下的修改能作为WHERE条件出现在SQL语句中
             ((UpdateMode = upWhereAll) or ((UpdateMode = upWhereChanged) and
             ((pfInKey in tempField.ProviderFlags) or (
             //(Assigned(tempFieldDef)) and (tempFieldDef.Fi_Edited) or
             (not ABComparisonFieldOldNewValueSame(tempField))))) or
             ((UpdateMode = upWhereKeyOnly) and
             (pfInKey in tempField.ProviderFlags))) then
          begin
            tempFieldName:=tempField.FieldName;
            DoBeforeUpdateTransitionFieldName(dtDelete,tempFieldName);

            tempOldValue:=ABGetFieldOldValue(tempField);
            if (ABVarIsNull(tempOldValue)) or
               ((VarIsStr(tempOldValue)) and
               (VarToStrDef(tempOldValue, emptystr) = emptystr)) then
              tempStr3 := tempFieldName + ' is null '
            else
              tempStr3 := tempFieldName + ' =:Old_' + tempField.FieldName;
          end;

          if tempStr3 <> emptystr then
          begin
            ABAddstr(tempstr1, tempStr3, ' and ');
          end;
        end;
      end;

      DoBeforeUpdateTransitionTableName(dtDelete,aTableName);

      aOutUpdateSQL := ' delete ' + aTableName + ' where  ' + tempstr1;
      //得到删除的SQL语句
      if (tempstr1 = emptystr) then
      begin
        abshow('删除语句缺少Where部分,请检查.',aOutUpdateSQL);
      end
      else
      begin
        result := true;
      end;
    end;
  //生成新增语句
    function GetInsertedSQL: Boolean;
    var
      tempstr1, tempStr2, tempStr3: string;
      I: LongInt;
      tempField: TField;
    begin
      result := false;
      for I := 0 to FieldCount - 1 do
      begin
        tempField := fields[I];
        if (tempField.FieldKind = fkCalculated) or
           (tempField.FieldKind = fkInternalCalc) or
           (tempField.FieldKind = fkAggregate) then
          Continue;

        tempIDENTITY := false;
        if not FieldCanUpdateToDatabase(aTableName, tempField.FieldName,tempIDENTITY) then
        begin
          if (tempIDENTITY) and (not Assigned(tempIDENTITYField)) then
          begin
            tempIDENTITYField := tempField;
          end;
          Continue;
        end;

        //新增时忽略null值的字段
        if ABVarIsNull(ABGetFieldNewValue(tempField)) then
          Continue;

        //得到新增的字段部分
        tempFieldName:=tempField.FieldName;
        DoBeforeUpdateTransitionFieldName(dtAppend,tempFieldName);
        ABAddstr(tempstr1, tempFieldName);

        //得到新增的字段值部分
        tempStr3 := emptystr;

        if (ABVarIsNull(ABGetFieldNewValue(tempField))) or
          ((VarIsStr(ABGetFieldNewValue(tempField))) and (VarToStrDef(ABGetFieldNewValue(tempField),
          emptystr) = emptystr)) then
          tempStr3 := ' null '
        else
          tempStr3 := ' :New_' + tempField.FieldName;

        if tempStr3 <> emptystr then
        begin
          ABAddstr(tempStr2, tempStr3);
        end;
      end;

      DoBeforeUpdateTransitionTableName(dtAppend,aTableName);

      aOutUpdateSQL := ' insert into ' + aTableName + '(' + tempstr1 + ')' +' values( ' + tempStr2 + ')';
      //得到新增的SQL语句
      if (tempstr1 = emptystr) then
      begin
        abshow('新增语句缺少新增的字段名部分,请检查.',aOutUpdateSQL);
      end
      else if (tempStr2 = emptystr) then
      begin
        abshow('新增语句缺少新增的字段值部分,请检查.',aOutUpdateSQL);
      end
      else
      begin
        result := true;
      end;
    end;
  begin
    result:=false;
    aOutUpdateSQL := emptystr;
    case aState of
      dsEdit:
        begin
          result:=GetModifiedSQL
        end;
      dsBrowse:
        begin
          result := GetDeletedSQL;
        end;
      dsInsert:
        begin
          result := GetInsertedSQL;
        end;
    end;
  end;
  procedure SetUpdateQuery(var aCacheUpdateItem:TABCacheUpdateItem;aSQL:String);
  var
    tempFieldName, tempParamFlag, tempParamName: string;
    tempParamVariant: Variant;
    I: LongInt;
    tempField: TField;
    tempMemoryStream:TMemoryStream;
  begin
    //设置参数值
    aCacheUpdateItem.SQL:=aSQL;
    FParams.ParseSQL(aSQL,true);
    SetLength(aCacheUpdateItem.Params,FParams.Count);
    for I := 0 to FParams.Count-1 do
    begin
      tempParamName :=FParams[i].Name;
      tempParamFlag := copy(tempParamName, 1, 4);
      tempFieldName := copy(tempParamName, 5, maxint);
      tempField := FindField(tempFieldName);
      FParams[i].DataType:=tempField.DataType;
      if (tempField.DataType in ABBlobDataType) then
      begin
        tempMemoryStream:=TMemoryStream.Create;
        try
          TBlobField(tempField).SaveToStream(tempMemoryStream);
          aCacheUpdateItem.Params[i]:=ABStreamToVariant(tempMemoryStream);
        finally
          tempMemoryStream.Free;
        end;
      end
      else
      begin
        if AnsiCompareText(tempParamFlag, 'New_') = 0 then
          tempParamVariant := ABGetFieldNewValue(tempField)
        else if AnsiCompareText(tempParamFlag, 'Old_') = 0 then
          tempParamVariant :=ABGetFieldOldValue(tempField);

        if (tempField.DataType = ftDateTime) or
           (tempField.DataType = ftTimeStamp) then
        begin
          aCacheUpdateItem.Params[i]:=VarToDateTime(tempParamVariant);
        end
        else
        begin
          aCacheUpdateItem.Params[i]:=tempParamVariant;
        end;
      end;
    end;
  end;
begin
  result := True;
  if not FUpdateDatabase then
    exit;

  tempIDENTITYField := nil;
  for I := 0 to FUpdateTables.Count-1 do
  begin
    tempTableName := trim(FUpdateTables[i]);
    if tempTableName <> emptystr then
    begin
      result := GetUpdatesToDatabaseSQL(tempTableName,tempSQL);
      if (result) and (tempSQL <> emptystr) then
      begin
        if FCacheUpdate then
        begin
          j:=high(FCacheUpdateList)+1;
          SetLength(FCacheUpdateList,j+1);

          SetUpdateQuery(FCacheUpdateList[j],tempSQL);
        end
        else
        begin
          if high(FCacheUpdateList)+1=0 then
          begin
            SetLength(FCacheUpdateList,1);
          end;
          SetUpdateQuery(FCacheUpdateList[0],tempSQL);

          result := ExecQuery([FCacheUpdateList[0]]);
          if result then
          begin
          {
            if (aState = dsInsert) and (State = dsInsert) and
               (Assigned(tempIDENTITYField)) then
            begin
              tempIDENTITYValue := ABGetSQLValue(ConnName,'select IDENT_CURRENT(' + QuotedStr(tempTableName) + ')',[], null);
              if not ABVarIsNull(tempIDENTITYValue) then
              begin
                tempIDENTITYField.Value := tempIDENTITYValue;
              end;
            end;
            }
          end;
        end;
      end;

      if not result then
      begin
        //Abort;
        Break;
      end;
    end;
  end;
end;

constructor TABThirdQuery.Create(AOwner: TComponent);
begin
  inherited;
  FUpdateDatabase := true;
  FUpdateMode := upWhereKeyOnly;

  FIndexListDefs := TIndexDefs.Create(self);
  FUpdateTables:= TStringList.Create;
end;

destructor TABThirdQuery.Destroy;
begin
  CacheClear;
  FIndexListDefs.Free;
  FUpdateTables.Free;

  inherited;
end;



procedure ABFinalization;
begin
  if Assigned(FParams) then
    FParams.Free;
end;

procedure ABInitialization;
begin
  FParams:= TParams.Create(nil);
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;

end.




