//
// Created by the DataSnap proxy generator.
// 2015-4-24 ���� 11:06:01
//

unit ABThirdServerMethodsU;


interface

uses System.JSON, Data.DBXCommon, Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON, Datasnap.DSProxy, System.Classes, System.SysUtils, Data.DB, Data.SqlExpr, Data.DBXDBReaders, Data.DBXCDSReaders, Data.DBXJSONReflect;

type
  TABServerClient = class(TDSAdminClient)
  private
    FGetConnStringsCommand: TDBXCommand;
    FGetDatasetStreamCommand: TDBXCommand;
    FExecSQLCommand: TDBXCommand;
    FStartTransactionCommand: TDBXCommand;
    FCommitCommand: TDBXCommand;
    FRollbackCommand: TDBXCommand;
    FSetTimeoutCommand: TDBXCommand;
    FLongTimeRunFuncCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetConnStrings: string;
    function GetDatasetStream(aConnName: string; aSQL: string;aInitTableNames: string; aParams: TParams; var aResult: Boolean): TStream;
    function ExecSQL(aConnName: string; aSQL: string; aParams: OleVariant; aParamTypes: OleVariant): Integer;
    procedure StartTransaction(aConnName: string);
    procedure Commit(aConnName: string);
    procedure Rollback(aConnName: string);
    procedure SetTimeout(aConnName: string; aTimeout: Integer);
    procedure LongTimeRunFunc(aCallback: TDBXCallback);
  end;

implementation

function TABServerClient.GetConnStrings: string;
begin
  if FGetConnStringsCommand = nil then
  begin
    FGetConnStringsCommand := FDBXConnection.CreateCommand;
    FGetConnStringsCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetConnStringsCommand.Text := 'TABServer.GetConnStrings';
    FGetConnStringsCommand.Prepare;
  end;
  FGetConnStringsCommand.ExecuteUpdate;
  Result := FGetConnStringsCommand.Parameters[0].Value.GetWideString;
end;

function TABServerClient.GetDatasetStream(aConnName: string; aSQL: string;aInitTableNames: string; aParams: TParams; var aResult: Boolean): TStream;
begin
  if FGetDatasetStreamCommand = nil then
  begin
    FGetDatasetStreamCommand := FDBXConnection.CreateCommand;
    FGetDatasetStreamCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetDatasetStreamCommand.Text := 'TABServer.GetDatasetStream';
    FGetDatasetStreamCommand.Prepare;
  end;
  FGetDatasetStreamCommand.Parameters[0].Value.SetWideString(aConnName);
  FGetDatasetStreamCommand.Parameters[1].Value.SetWideString(aSQL);
  FGetDatasetStreamCommand.Parameters[2].Value.SetWideString(aInitTableNames);
  FGetDatasetStreamCommand.Parameters[3].Value.SetDBXReader(TDBXParamsReader.Create(aParams, FInstanceOwner), True);
  FGetDatasetStreamCommand.Parameters[4].Value.SetBoolean(aResult);
  FGetDatasetStreamCommand.ExecuteUpdate;
  aResult := FGetDatasetStreamCommand.Parameters[4].Value.GetBoolean;
  Result := FGetDatasetStreamCommand.Parameters[5].Value.GetStream(FInstanceOwner);
end;

function TABServerClient.ExecSQL(aConnName: string; aSQL: string; aParams: OleVariant; aParamTypes: OleVariant): Integer;
begin
  if FExecSQLCommand = nil then
  begin
    FExecSQLCommand := FDBXConnection.CreateCommand;
    FExecSQLCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FExecSQLCommand.Text := 'TABServer.ExecSQL';
    FExecSQLCommand.Prepare;
  end;
  FExecSQLCommand.Parameters[0].Value.SetWideString(aConnName);
  FExecSQLCommand.Parameters[1].Value.SetWideString(aSQL);
  FExecSQLCommand.Parameters[2].Value.AsVariant := aParams;
  FExecSQLCommand.Parameters[3].Value.AsVariant := aParamTypes;
  FExecSQLCommand.ExecuteUpdate;
  Result := FExecSQLCommand.Parameters[4].Value.GetInt32;
end;

procedure TABServerClient.StartTransaction(aConnName: string);
begin
  if FStartTransactionCommand = nil then
  begin
    FStartTransactionCommand := FDBXConnection.CreateCommand;
    FStartTransactionCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FStartTransactionCommand.Text := 'TABServer.StartTransaction';
    FStartTransactionCommand.Prepare;
  end;
  FStartTransactionCommand.Parameters[0].Value.SetWideString(aConnName);
  FStartTransactionCommand.ExecuteUpdate;
end;

procedure TABServerClient.Commit(aConnName: string);
begin
  if FCommitCommand = nil then
  begin
    FCommitCommand := FDBXConnection.CreateCommand;
    FCommitCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FCommitCommand.Text := 'TABServer.Commit';
    FCommitCommand.Prepare;
  end;
  FCommitCommand.Parameters[0].Value.SetWideString(aConnName);
  FCommitCommand.ExecuteUpdate;
end;

procedure TABServerClient.Rollback(aConnName: string);
begin
  if FRollbackCommand = nil then
  begin
    FRollbackCommand := FDBXConnection.CreateCommand;
    FRollbackCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FRollbackCommand.Text := 'TABServer.Rollback';
    FRollbackCommand.Prepare;
  end;
  FRollbackCommand.Parameters[0].Value.SetWideString(aConnName);
  FRollbackCommand.ExecuteUpdate;
end;

procedure TABServerClient.SetTimeout(aConnName: string; aTimeout: Integer);
begin
  if FSetTimeoutCommand = nil then
  begin
    FSetTimeoutCommand := FDBXConnection.CreateCommand;
    FSetTimeoutCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FSetTimeoutCommand.Text := 'TABServer.SetTimeout';
    FSetTimeoutCommand.Prepare;
  end;
  FSetTimeoutCommand.Parameters[0].Value.SetWideString(aConnName);
  FSetTimeoutCommand.Parameters[1].Value.SetInt32(aTimeout);
  FSetTimeoutCommand.ExecuteUpdate;
end;

procedure TABServerClient.LongTimeRunFunc(aCallback: TDBXCallback);
begin
  if FLongTimeRunFuncCommand = nil then
  begin
    FLongTimeRunFuncCommand := FDBXConnection.CreateCommand;
    FLongTimeRunFuncCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FLongTimeRunFuncCommand.Text := 'TABServer.LongTimeRunFunc';
    FLongTimeRunFuncCommand.Prepare;
  end;
  FLongTimeRunFuncCommand.Parameters[0].Value.SetCallbackValue(aCallback);
  FLongTimeRunFuncCommand.ExecuteUpdate;
end;


constructor TABServerClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TABServerClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TABServerClient.Destroy;
begin
  FGetConnStringsCommand.DisposeOf;
  FGetDatasetStreamCommand.DisposeOf;
  FExecSQLCommand.DisposeOf;
  FStartTransactionCommand.DisposeOf;
  FCommitCommand.DisposeOf;
  FRollbackCommand.DisposeOf;
  FSetTimeoutCommand.DisposeOf;
  FLongTimeRunFuncCommand.DisposeOf;
  inherited;
end;

end.

