{
三方数据集定义单元
}
unit ABThirdCustomQueryU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubLogU,
  ABPubShowEditFieldValueU,
  ABPubUserU,
  ABPubScriptU,
  ABPubSQLParseU,
  ABPubMessageU,
  ABPubConstU,
  ABPubFuncU,
  ABPubDBU,
  ABPubVarU,
  ABPubLocalParamsU,
  ABPubSQLLogU,

  ABThirdDBU,
  ABThirdConnU,
  ABThirdConnDatabaseU,
  ABThirdConnServerU,

  ADODB,
  Dialogs,
  Variants, DateUtils,

  FireDAC.Comp.Client,
  FireDAC.Comp.DataSet,
  FireDAC.Stan.ExprFuncs,
  FireDAC.Stan.Expr,
  FireDAC.Stan.Intf,
  SysUtils,Classes,DB,DBClient;


type
  //重载三方基类数据集的方法,更换三方数据集时此类需变动
  TABThirdCustomQuery = class(TFireDACQuery)
  private
    function GetReadOnly: Boolean;
    procedure SetReadOnly(const Value: Boolean);
  protected
    procedure OpenCursor(InfoQuery: Boolean); override;

    procedure DoAfterDelete; override;
    procedure DoAfterPost;override;

    //SQL修改的过程
    procedure QueryChanged(Sender: TObject);
    //修改SQL后做的事情
    procedure AfterChangSQL(aNewSQL: String; var aDesigning: Boolean;var aLoading: Boolean); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property ActiveStoredUsage;
    property ReadOnly :Boolean read GetReadOnly write SetReadOnly;

    property Active;
    property AutoCalcFields;
    property BeforeOpen;
    property AfterOpen;
    property BeforeClose;
    property AfterClose;
    property BeforeInsert;
    property AfterInsert;
    property BeforeEdit;
    property AfterEdit;
    property BeforePost;
    property AfterPost;
    property BeforeCancel;
    property AfterCancel;
    property BeforeDelete;
    property AfterDelete;
    property BeforeScroll;
    property AfterScroll;
    property BeforeRefresh;
    property AfterRefresh;
    property OnCalcFields;
    property OnDeleteError;
    property OnEditError;
    property OnNewRecord;
    property OnPostError;
    property Filtered;
    property FilterOptions;
    property Filter;
    property OnFilterRecord;

    property IndexName;
    property IndexFieldNames;

    property MasterSource;
    //使用多层查询时，一定要设置从数据集的 MasterFields、DetailFields属性
    property MasterFields;
    property DetailFields;

    property BeforeExecute;
    property AfterExecute;
    property SQL;

    property Params;
  end;

  //在三方数据集上增加框架的相关功能(无更新数据库功能)，更换三方数据集时此类尽可能不用变动
  TABThirdReadDataQuery = class(TABThirdCustomQuery)
  private
    FExecOrOpenLock:Boolean;
    FBaseSQL: string;
  private
    FFocusFieldNameOnInsert: string;
    FBeforeInsertCheckFieldValues: string;
    FAddWhereofNext: string;
    FBeforeDeleteAsk: Boolean;
    FBeforePostCheckMsg: string;
    FSqlUpdateDatetime: TDateTime;
    FCanPrint: Boolean;
    FFieldDefaultValues: string;
    FBeforeDeleteCheckMsg: string;
    FBeforePostCheckCommand: string;
    FBeforeEditCheckMsg: string;
    FSQLParseObject: TABSQLParse;
    FBeforeDeleteAskMsg: string;
    FCanDelete: Boolean;
    FFieldDefaultValuesTemplate: TStrings;
    FCanEdit: Boolean;
    FFieldDefaultValuesQuoteDatasetList: TList;
    FBeforeInsertCheckMsg: string;
    FBeforeDeleteCheckCommand: string;
    FBeforeEditCheckCommand: string;
    FCanInsert: Boolean;
    FBeforePostCheckFieldValues: string;
    FFieldCaptions: String;
    FBeforeInsertCheckCommand: string;
    FFocusFieldNameOnEdit: string;
    FBeforeDeleteCheckFieldValues: string;
    FBeforeEditCheckFieldValues: string;
    FLastOperatorDatetime: TDateTime;
    FLastOperatorType: TABDatasetOperateType;
    FSQLTransacting: Boolean;
    FConnName: string;
    FMaxRecordCount: Longint;
    FStopSQLDefaultValueAfterInsert: Boolean;
    FMasterRangeIng: Boolean;
    FLoadTables: TStrings;
    function GetBaseSQL: string;
    procedure SetFieldDefaultValuesTemplate(const Value: TStrings);
    procedure SetConnName(const Value: string);
    procedure SetLoadTables(const Value: TStrings);
  protected
    //当FieldByName字段不存在,可写入日志或报错
    function FieldByName(const aFieldName: string): TField;

    procedure Loaded; override;

    procedure CheckMasterRange; override;
    procedure DoBeforeDelete; override;
    procedure DoBeforeEdit; override;
    procedure DoBeforeInsert; override;
    procedure DoBeforePost; override;
    procedure DoAfterInsert; override;
    procedure DoAfterDelete; override;
    procedure DoAfterEdit; override;
    procedure DoAfterPost; override;
    procedure DoAfterOpen; override;
    procedure DoAfterClose; override;
    procedure DoAfterCancel; override;

    //更新基本的SQL
    function UpdateBaseSQL(aNewSQL:string): Boolean;
    //修改SQL后做的事情
    //aDesigning为true表示当前是设计期,aLoading为true表示控件加载时
    procedure AfterChangSQL(aNewSQL: String; var aDesigning: Boolean;var aLoading: Boolean); override;

    //对原始SQL进行参数替换及合并附加SQL
    procedure TransactBaseSQL;
    //对原始SQL进行其它再操作
    procedure DoTransactBaseSQL(var aSQL: string);virtual;
    //解析SQL语句到SQL解析结构中
    procedure ParseSQLToParseObject(aSQL: string);
    //恢复原始SQL
    procedure RestoreBaseSQL;

    //新增前检测本数据集
    function BeforeInsertCheck: Boolean; virtual;
    //删除前检测本数据集
    function BeforeDeleteCheck: Boolean; virtual;
    //修改前检测父数据集
    function BeforeEditCheckParent: Boolean; virtual;
    //修改前检测本数据集
    function BeforeEditCheck: Boolean; virtual;
    //保存前检测本数据集
    function BeforePostCheck: Boolean; virtual;
  public
    procedure SetActive(Value: Boolean); override;

    //是否在新增后对需要执行SQL的默认值不做操作(大批量插入而又能回避执行SQL的默认值时设置为True增加速度)
    property StopSQLDefaultValueAfterInsert:Boolean  read FStopSQLDefaultValueAfterInsert  write FStopSQLDefaultValueAfterInsert;
    //设置字段的扩展默认值
    function GetFieldExtDefaultValue(aFieldName,aFieldValue:string):Variant;virtual;
    //检测Scrip命令
    function CheckCommand(aDataset: TDataset; aCheckCommand: string): Boolean; virtual;
    //检测当前字段值是否为给出的内容(格式为字段名1=值1;字段名2=值2;...)
    function CheckFieldValues(aDataset: TDataset;aCheckFieldValues:string;aCheckMsg: string; aType:string ): Boolean; virtual;

    //Options=[]时默认为不区分大小写
    function Locate(const KeyFields: string; const KeyValues: Variant;Options: TLocateOptions): Boolean; override;

    //数据集最后操作的类型
    property LastOperatorType: TABDatasetOperateType read FLastOperatorType;
    //数据集最后操作的时间
    property LastOperatorDatetime: TDateTime read FLastOperatorDatetime;

    //得到主数据集
    function GetMainDataset: TABThirdReadDataQuery;
    //设置默认值时，值部分可为列表中数据集的字段
    property FieldDefaultValuesQuoteDatasetList: TList read FFieldDefaultValuesQuoteDatasetList write FFieldDefaultValuesQuoteDatasetList;
    //字段是否可以设置默认值
    function FieldDefaultValueCanUse(aFieldName, aFieldValue: string): Boolean; virtual;
    //指定字段是否可以拷贝
    function FieldCanCopy(aFieldName: string): Boolean; virtual;

    //取得激活的从数据集(只取下一层)
    procedure GetActiveDetailDataSets(aList: TList); virtual;

    //拷贝数据,支持主从的复制
    procedure DoCopy;
    //行刷新
    function RowRefresh: boolean;

    //数据集是否能修改
    property CanEdit: Boolean read FCanEdit write FCanEdit;
    //数据集是否能新增
    property CanInsert: Boolean read FCanInsert write FCanInsert;
    //数据集是否能删除
    property CanDelete: Boolean read FCanDelete write FCanDelete;
    //数据集是否能打印
    property CanPrint: Boolean read FCanPrint write FCanPrint;

    //是否主表记录改变中
    property MasterRangeIng: Boolean read FMasterRangeIng write FMasterRangeIng;

    //基础的SQL(未替换参数前的原始SQL)
    property BaseSQL: string read GetBaseSQL;
    //当前SQL的结构
    property SQLParseObject: TABSQLParse read FSQLParseObject;
    //下次数据库交互附加Where
    property AddWhereofNext: string read FAddWhereofNext write FAddWhereofNext;

    //SQL是否正在转换中
    property SQLTransacting: Boolean read FSQLTransacting;

    //打开查询时初始化此表名下的字段的框架信息
    property LoadTables:TStrings read FLoadTables write SetLoadTables;

    //以传入的条件为AddWhereofNext刷新数据
    function RefreshQuery(aAddWhere: string;
                          aSyncQuery: array of TDataset;
                          aSyncQueryCheckParamsDataset: TDataset = nil;
                          aSyncQueryIsRequery: Boolean = true;
                          aSyncQueryIsDisableControls: Boolean = true;
                          aSyncQueryIsSavePosition: Boolean = true): LongInt;


    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    //SQL更新时间
    property  SqlUpdateDatetime:TDateTime read FSqlUpdateDatetime  write FSqlUpdateDatetime;

    //新增前检测的优先级为1.新增前检测字段值,2.检测Scrip命令
    //新增前检测字段值(格式为字段名1=值1;字段名2=值2;...)
    property BeforeInsertCheckFieldValues: string read FBeforeInsertCheckFieldValues write FBeforeInsertCheckFieldValues;
    //新增前检测字段值不通过时弹出的消息
    property BeforeInsertCheckMsg: string read FBeforeInsertCheckMsg write FBeforeInsertCheckMsg;
    //新增前检测Scrip命令，如检测不通过则弹出Scrip命令返回的字串
    property BeforeInsertCheckCommand: string read FBeforeInsertCheckCommand write FBeforeInsertCheckCommand;

    //删除前检测的优先级为1.删除前检测字段值,2.检测Scrip命令
    //删除前检测字段值(格式为字段名1=值1;字段名2=值2;...)
    property BeforeDeleteCheckFieldValues: string read FBeforeDeleteCheckFieldValues write FBeforeDeleteCheckFieldValues;
    //删除前检测字段值不通过时弹出的消息
    property BeforeDeleteCheckMsg: string read FBeforeDeleteCheckMsg write FBeforeDeleteCheckMsg;
    //删除前检测Scrip命令，如检测不通过则弹出Scrip命令返回的字串
    property BeforeDeleteCheckCommand: string read FBeforeDeleteCheckCommand write FBeforeDeleteCheckCommand;

    //修改前检测的优先级为1.修改前检测字段值,2.检测Scrip命令
    //修改前检测字段值(格式为字段名1=值1;字段名2=值2;...)
    property BeforeEditCheckFieldValues: string read FBeforeEditCheckFieldValues write FBeforeEditCheckFieldValues;
    //修改前检测字段值不通过时弹出的消息
    property BeforeEditCheckMsg: string read FBeforeEditCheckMsg write FBeforeEditCheckMsg;
    //修改前检测Scrip命令，如检测不通过则弹出Scrip命令返回的字串
    property BeforeEditCheckCommand: string read FBeforeEditCheckCommand write FBeforeEditCheckCommand;

    //保存前检测的优先级为1.保存前检测字段值,2.检测Scrip命令
    //保存前检测字段值(格式为字段名1=值1;字段名2=值2;...)
    property BeforePostCheckFieldValues: string read FBeforePostCheckFieldValues write FBeforePostCheckFieldValues;
    //保存前检测字段值不通过时弹出的消息
    property BeforePostCheckMsg: string read FBeforePostCheckMsg write FBeforePostCheckMsg;
    //保存前检测Scrip命令，如检测不通过则弹出Scrip命令返回的字串
    property BeforePostCheckCommand: string read FBeforePostCheckCommand write FBeforePostCheckCommand;

    //新增时的得到焦点的字段
    property FocusFieldNameOnInsert: string read FFocusFieldNameOnInsert write FFocusFieldNameOnInsert;
    //编辑时的得到焦点的字段
    property FocusFieldNameOnEdit: string read FFocusFieldNameOnEdit write FFocusFieldNameOnEdit;

    //删除前是否询问
    property BeforeDeleteAsk: Boolean read FBeforeDeleteAsk write FBeforeDeleteAsk;
    //删除前询问的消息
    property BeforeDeleteAskMsg: string read FBeforeDeleteAskMsg write FBeforeDeleteAskMsg;

    //新增时设置字段值的优先级为1.字段默认值,2.字段默认值模板,3.afterInsert事件中设置
    //新增时设置字段默认值(格式为字段名1=默认值1;字段名2=默认值2;...)
    property FieldDefaultValues: string read FFieldDefaultValues write FFieldDefaultValues;
    //新增时设置字段默认值模板(格式为字段名1=默认值1;字段名2=默认值2;...)
    property FieldDefaultValuesTemplate: TStrings read FFieldDefaultValuesTemplate write SetFieldDefaultValuesTemplate;

    //字段的定制标题(格式为字段名1=标题1;字段名2=标题2;...)
    property FieldCaptions: String read FFieldCaptions write FFieldCaptions;
    //数据集记录的最大数量
    property MaxRecordCount: Longint read FMaxRecordCount write FMaxRecordCount;

    //数据库连接
    property ConnName:string read FConnName  write SetConnName ;
  end;

//得到数据集,有自动解析SQL中参数的功能
function ABGetDataset(aConnName: string;
                      aSql: string;
                      aParamValues: array of Variant;
                      aExtParamsDataset: TDataSet=nil;
                      aActive: Boolean = true;
                      aDataSetClass: TDataSetClass = nil): TDataSet;

//得到连接数据库的日期时间
function ABGetServerDateTime(aConnName: string=''): TDateTime;
//得到连接数据库的日期
function ABGetServerDate(aConnName: string=''): TDateTime;
//执行SQL中的执行部分,返回余下的内容（[ExceSQLBegin] [ExceSQLEnd] 中的为执行部分）
function ABExecSQLExecPart(aConnName: string;
                             aSQL: string;
                             aParamValues: array of Variant;
                             aReplaceSqlExtParams:Boolean=True ):string;
//执行SQL语句 ,有自动解析SQL中参数的功能
function ABExecSQL(aConnName: string; aSql: string; aReplaceSqlExtParams:Boolean=True; aRaiseLog:Boolean=True):longint;overload;
function ABExecSQL(aConnName: string; aSql: string; aParamValues: array of Variant;aReplaceSqlExtParams:Boolean=True; aRaiseLog:Boolean=True):longint;overload;
function ABExecSQL(aConnName: string; aSql: string; aParamValues: array of Variant;aParamTypes: array of TFieldType;aReplaceSqlExtParams:Boolean=True; aRaiseLog:Boolean=True):longint;overload;
//批量执行SQL语句，由单独行aGoFlag来分隔 ,SQL中支持ConnName=的方式重定义连接
function ABExecSQLs(aConnName: string; aSql: string;aParamValues: array of Variant;
                    aReplaceSqlExtParams:Boolean=True;
                    aGoFlag:string='go'):longint;

//将SQL的值填充到列表中
procedure ABFillStringsBySQL( aConnName: string;
                              aSql: string;
                              aParamValues: array of Variant;
                              aExtParamsDataset: TDataSet;
                              aStrings: TStrings;
                              SpaceSign: string = ',';
                              aNoFillSame: boolean = true;
                              aIsDelOld: boolean = true
                              );

//得到SQL语句第一个字段的值
function ABGetSQLValue(aConnName: string;
                       aSql: string;
                       aParamValues:array of Variant;
                       aNullValue:Variant;
                       aExtParamsDataset: TDataSet=nil
                       ): Variant;
//得到SQL语句多个字段的值并返回到字串中
function ABGetSQLValues(aConnName: string;
                        aSql: string;
                        aParamValues: array of Variant;
                        aExtParamsDataset: TDataSet=nil;
                        aFieldSpaceSign: string = ',';
                        aOnlyHaveField: string = '';  //空表示所有字段
                        aAllRecord: Boolean = false;
                        aRecordSpaceSign: string = ABEnterWrapStr
                        ): string;
//得到SQL语句多个字段的值并返回到变体数组中
function ABGetSQLValues_ByArray(
                        aConnName: string;
                        aSql: string;
                        aParamValues: array of Variant;
                        aExtParamsDataset: TDataSet=nil):Variant;

//上传本地文件到表字段中
function ABUpFileToField( aConnName: string;
                          aTableName, aFieldName, aWhereSql,
                          aFileName: string): LongInt;
//上传内存流到表字段中
function ABUpStreamToField( aConnName: string;
                            aTableName, aFieldName, aWhereSql: string;
                            aStream: TMemoryStream): LongInt;
//上传字串到表字段中
function ABUpTextToField( aConnName: string;
                          aTableName, aFieldName, aWhereSql: string;
                          aText: string): LongInt;

//从表字段下载内容到本地文件中
function ABDownToFileFromField( aConnName: string;
                                aTableName, aFieldName, aWhereSql,
                                aDownFileName: string): Boolean;
//从表字段下载内容到内存流中
function ABDownToStreamFromField( aConnName: string;
                                  aTableName, aFieldName, aWhereSql: string;
                                  aStream: TMemoryStream
                                  ): Boolean;
//从表字段下载内容到函数返回值中
function ABDownToTextFromField( aConnName: string;
                                aTableName, aFieldName, aWhereSql: string
                                ): string;


//增加客户端连接
function ABAddClientConn:boolean;
//删除客户端连接
function ABDelClientConn:boolean;
//更新心跳
function ABUpdateHeartbeatDatetime:boolean;
//增加错误日志的数据
procedure ABAppendLog_ErrorInfo(aType:string;aTitle:string;aTitleReMark:string='');
//增加自定义日志的数据
procedure ABAppendLog_CustomEvent(aType:string;aTitle:string;aTitleReMark:string='');


implementation

function ABExecSQL(aConnName: string; aSql: string;aReplaceSqlExtParams:Boolean; aRaiseLog:Boolean):longint;
begin
  Result:=ABExecSQL(aConnName,aSql,[],[],aReplaceSqlExtParams,aRaiseLog);
end;

function ABExecSQL(aConnName: string; aSql: string; aParamValues: array of Variant; aReplaceSqlExtParams:Boolean;aRaiseLog:Boolean):longint;
begin
  Result:=ABExecSQL(aConnName,aSql,aParamValues,[],aReplaceSqlExtParams,aRaiseLog);
end;

function ABExecSQL(aConnName: string; aSql: string; aParamValues: array of Variant;aParamTypes: array of TFieldType; aReplaceSqlExtParams:Boolean;aRaiseLog:Boolean):longint;
var
  i:LongInt;
  tempParams:OleVariant;
  tempParamTypes:OleVariant;
  tempConn:TCustomConnection;
begin
  result:=-1;
  if trim(aSql)=EmptyStr then
    Exit;

  if aReplaceSqlExtParams then
    aSql := ABReplaceSqlExtParams(aSql);
  if ABInitConn then
  begin
    try
      if (Assigned(ABSQLLog)) and (Assigned(ABSQLLog.ExecSQL)) then
        ABSQLLog.ExecSQL('ABExecSQL',aSql);

      if ABLocalParams.LoginType=ltCS_Two then
      begin
        tempConn:=ABGetConnInfoByConnName(aConnName).Conn;
        if Assigned(tempConn) then
        begin
          if tempConn is TFireDACConnection then
          begin
            result:=TFireDACConnection(tempConn).ExecSQL(aSql,aParamValues,aParamTypes);
          end
          else if tempConn is TADOConnection then
          begin
            TADOConnection(tempConn).Execute(aSql);
            result:=0;
          end;
        end;
      end
      else if ABLocalParams.LoginType=ltCS_Three then
      begin
        if High(aParamValues)>=0 then
        begin
          tempParams := VarArrayCreate([0, High(aParamValues)], varVariant);
          for I := VarArrayLowBound(tempParams,1) to VarArrayHighBound(tempParams,1) do
          begin
            tempParams[i]:=aParamValues[i];
          end;

          if High(aParamTypes)>=0 then
          begin
            tempParamTypes := VarArrayCreate([0, High(aParamTypes)], varVariant);
            for I := VarArrayLowBound(tempParamTypes,1) to VarArrayHighBound(tempParamTypes,1) do
            begin
              tempParamTypes[i]:=aParamTypes[i];
            end;
          end;
        end;
        result:=ABGetServerClient.ExecSQL(aConnName,ABStringReplace(aSql,ABEnterWrapStr,' '),tempParams,tempParamTypes);
      end;
    except
      if aRaiseLog then
        ABAppendLog_ErrorInfo('ABExecSQL','[HostIP:'+ABPubUser.HostIP+']'+'[HostName:'+ABPubUser.HostName+']'+'[ConnName:'+aConnName+']',aSql);
      raise;
    end;
  end;
end;

function ABDelClientConn:boolean;
var
  tempStr:string;
begin
  result:=False;
  try
    //删除当前客户端连接
    tempStr:= ' exec Proc_PubDelete '+
              QuotedStr('ABSys_Log_CurLink')+','+
              QuotedStr('CL_PC='+QuotedStr('[HostIP:'+ABPubUser.HostIP+']'+'[HostName:'+ABPubUser.HostName+']')+' and cast(Cl_SPID as varchar(500))='+QuotedStr(ABPubUser.SPID));
    ABExecSQL('Main',tempStr);
    result:=true;
  except
  end;
end;

function ABAddClientConn:boolean;
var
  tempStr:string;
begin
  tempStr:= ' exec Proc_PubInsert '+
            QuotedStr('ABSys_Log_CurLink')+','+
            QuotedStr('Cl_Guid,Cl_PC,Cl_SPID,Cl_Op_Name,Cl_BegDatetime,CL_HeartbeatDatetime,Cl_WindowsType,PubID')+','+
            QuotedStr('newid(),'+
                      QuotedStr('[HostIP:'+ABPubUser.HostIP+']'+'[HostName:'+ABPubUser.HostName+']')+','+
                      QuotedStr(ABPubUser.SPID)+','+
                      QuotedStr(ABPubUser.Name)+','+
                      QuotedStr(ABDateTimeToStr(ABPubUser.LoginDateTime))+','+
                      QuotedStr(ABDateTimeToStr(ABPubUser.LoginDateTime))+','+
                      QuotedStr(ABGetSysLanguageStr(ABGetSysLanguage))+',newid()'
                      )+','+
            QuotedStr('CL_PC='+QuotedStr('[HostIP:'+ABPubUser.HostIP+']'+'[HostName:'+ABPubUser.HostName+']')+' and cast(Cl_SPID as varchar(500))='+QuotedStr(ABPubUser.SPID));
  ABExecSQL('Main',tempStr);
  result:=true;
end;

function ABUpdateHeartbeatDatetime:boolean;
begin
  result:=False;
  try
    ABExecSQL('Main',
              ' exec Proc_PubUpdate '+
              ABQuotedStr('ABSys_Log_CurLink')+','+
              ABQuotedStr('CL_HeartbeatDatetime=getdate() ')+','+
              ABQuotedStr('CL_PC='+QuotedStr('[HostIP:'+ABPubUser.HostIP+']'+'[HostName:'+ABPubUser.HostName+']')+' and '+
                          'CL_SPID='+QuotedStr(ABPubUser.SPID)));
    result:=true;
  except
  end;
end;

procedure ABAppendLog_ErrorInfo(aType:string;aTitle:string;aTitleReMark:string);
var
  tempStr:string;
begin
  tempStr:= ' exec Proc_PubInsert '+
            QuotedStr('ABSys_Log_ErrorInfo')+','+
            QuotedStr('Ei_Guid,Ei_Type,Ei_Op_Name,Ei_PC,Ei_Datetime,Ei_Title,Ei_IsDispose,Ei_ReMark,PubID')+','+
            QuotedStr(
                      'newid()'+','+
                      QuotedStr(aType)+','+
                      QuotedStr(ABPubUser.Name)+','+
                      QuotedStr('Name:['+ABPubUser.HostName+']IP:['+ABPubUser.HostIP+']Mac:['+ABPubUser.MacAddress+']')+','+
                      'getdate(),'+
                      QuotedStr(aTitle)+','+
                      '0'+','+
                      QuotedStr(Copy(aTitleReMark,1800))+',newid()'
                      );
  ABExecSQL('Main',tempStr,False);
end;

procedure ABAppendLog_CustomEvent(aType:string;aTitle:string;aTitleReMark:string);
var
  tempStr:string;
begin
  tempStr:= ' exec Proc_PubInsert '+
            QuotedStr('ABSys_Log_CustomEvent')+','+
            QuotedStr('Ce_Guid,Ce_Type,Ce_Op_Name,Ce_PC,Ce_DateTime,Ce_Title,Ce_ReMark,PubID')+','+
            QuotedStr(
                      'newid()'+','+
                      QuotedStr(aType)+','+
                      QuotedStr(ABPubUser.Name)+','+
                      QuotedStr('Name:['+ABPubUser.HostName+']IP:['+ABPubUser.HostIP+']Mac:['+ABPubUser.MacAddress+']')+',getdate(),'+
                      QuotedStr(aTitle)+','+
                      QuotedStr(aTitleReMark)+',newid()'
                      );
  ABExecSQL('Main',tempStr);
end;

function ABExecSQLExecPart(aConnName: string;
                             aSQL: string;
                             aParamValues: array of Variant;
                             aReplaceSqlExtParams:Boolean):string;
var
  tempPosBeginIndex,
  tempSubStrEndIndex,tempSubStrBegIndex: Integer;
  tempSubItemStr:string;
begin
  tempSubStrBegIndex:=abpos(ABExceSQLBegin,aSQL);
  while tempSubStrBegIndex>0 do
  begin
    tempSubStrEndIndex:=ABPos(ABExceSQLEnd,aSQL,tempSubStrBegIndex);
    if tempSubStrEndIndex<=0 then
      break;

    tempSubItemStr:=Copy(aSQL,tempSubStrBegIndex+length(ABExceSQLBegin),
                              tempSubStrEndIndex-(tempSubStrBegIndex+length(ABExceSQLBegin)));

    ABExecSQL(aConnName,tempSubItemStr,aParamValues,aReplaceSqlExtParams);

    aSQL:=copy(aSQL,1,tempSubStrBegIndex-1)+copy(aSQL,tempSubStrEndIndex+length(ABExceSQLEnd),length(aSQL));
    tempPosBeginIndex:=tempSubStrBegIndex+length(ABExceSQLBegin);
    tempSubStrBegIndex:=abpos(ABExceSQLBegin,aSQL,tempPosBeginIndex);
  end;

  result:=aSQL;
end;

function ABGetServerDateTime(aConnName: string): TDateTime;
begin
  result := ABStrToDateTime(ABGetSQLValue(aConnName, 'select convert(varchar,getdate(),121)',[],''));
end;

function ABGetServerdate(aConnName: string): TDateTime;
begin
  result := DateOf(ABGetServerDateTime(aConnName));
end;

function ABGetSQLValues(aConnName: string; aSql: string;
                        aParamValues: array of Variant;
                        aExtParamsDataset: TDataSet;
                        aFieldSpaceSign: string;
                        aOnlyHaveField: string ;  //空表示所有字段
                        aAllRecord: Boolean ;
                        aRecordSpaceSign: string
                        ): string;
var
  tempDataSet: TDataSet;
begin
  result := EmptyStr;
  if ABIsSelectSQL(aSql) then
  begin
    tempDataSet := ABGetDataset(aConnName, aSql, aParamValues,aExtParamsDataset);
    try
      if aOnlyHaveField=EmptyStr then
        aOnlyHaveField:= ABGetDatasetFieldNames(tempDataSet);

      if aAllRecord then
        result := ABGetDatasetValue( tempDataSet,
                                     ABStrToStringArray(aOnlyHaveField),
                                     [],
                                     [],
                                     aRecordSpaceSign,
                                     aFieldSpaceSign)
      else
        result := ABGetFieldValue( tempDataSet,
                                    ABStrToStringArray(aOnlyHaveField),
                                    [],
                                    aFieldSpaceSign);
    finally
      tempDataSet.Free;
    end;
  end;
end;

function ABGetSQLValues_ByArray(aConnName: string; aSql: string; aParamValues: array of Variant;aExtParamsDataset: TDataSet):Variant;
var
  tempDataSet: TDataSet;
  i:LongInt;
begin
  Result := Null;
  if ABIsSelectSQL(aSql) then
  begin
    tempDataSet := ABGetDataset(aConnName, aSql, aParamValues,aExtParamsDataset);
    try
      Result := VarArrayCreate([0, tempDataset.FieldCount - 1], varVariant);
      for i := 0 to tempDataset.FieldCount-1 do
      begin
        Result[i]:=tempDataset.Fields[i].Value;
      end;
    finally
      tempDataSet.Free;
    end;
  end;
end;

function ABGetSQLValue(aConnName: string; aSql: string; aParamValues: array of Variant;aNullValue:Variant; aExtParamsDataset: TDataSet): Variant;
var
  tempDataSet: TDataSet;
begin
  result := aNullValue;
  if ABIsSelectSQL(aSql) then
  begin
    tempDataSet := ABGetDataset(aConnName, aSql,aParamValues, aExtParamsDataset);
    tempDataSet.first;
    try
      if tempDataSet.Fields[0].IsNull then
        result :=aNullValue
      else
        result := tempDataSet.Fields[0].Value;
    finally
      tempDataSet.Free;
    end;
  end;
end;

procedure ABFillStringsBySQL(aConnName: string; aSql: string;
  aParamValues: array of Variant;aExtParamsDataset: TDataSet;
  aStrings: TStrings;
  SpaceSign: string;
  aNoFillSame: boolean;
  aIsDelOld: boolean
  );
var
  tempDataSet: TDataSet;
begin
  tempDataSet := ABGetDataset(aConnName, aSql, aParamValues,aExtParamsDataset);
  try
    ABDatasetToStrings(tempDataSet, [], aStrings, SpaceSign, aNoFillSame,aIsDelOld);
  finally
    tempDataSet.Free;
  end;
end;

function ABExecSQLs(aConnName: string; aSql: string;aParamValues: array of Variant;
                    aReplaceSqlExtParams:Boolean;
                    aGoFlag:string):longint;
var
  tempStrings:TStrings;
  I: Integer;
  tempCurLine,
  tempSQL:string;
begin
  result:=0;
  tempStrings:=TStringList.Create;
  try
    tempStrings.Text:= aSql;
    tempSQL:=EmptyStr;
    for I := 0 to tempStrings.Count - 1 do
    begin
      tempCurLine:=tempStrings.Strings[i];
      if AnsiCompareText(trim(tempCurLine),aGoFlag)=0 then
      begin
        result:=result+ABExecSQL(aConnName,tempSQL,aParamValues,aReplaceSqlExtParams);
        tempSQL:=EmptyStr;
      end
      else
      begin
        if AnsiCompareText(Copy(trim(tempCurLine),1,length('ConnName')),'ConnName')=0 then
        begin
          aConnName:=ABGetLeftRightStr(trim(tempCurLine),axdRight);
        end;
        abaddstr(tempSQL,tempCurLine,ABEnterWrapStr);
      end;
    end;
    if tempSQL<>EmptyStr then
    begin
      result:=result+ABExecSQL(aConnName,tempSQL,aParamValues,aReplaceSqlExtParams);
    end;
  finally
    tempStrings.Free;
  end;
end;

function ABGetDataset(aConnName: string;
                      aSql: string; aParamValues: array of Variant;
                      aExtParamsDataset: TDataSet;
                      aActive: Boolean;
                      aDatasetClass:TDataSetClass): TDataset;
var
  tempConn:TCustomConnection;
begin
  Result:=nil;
  tempConn:=ABThirdConnU.ABGetConnByConnName(aConnName);
  if tempConn is TFireDACConnection then
  begin
    if not Assigned(aDataSetClass) then
    begin
      Result:=TABThirdReadDataQuery.Create(nil);
    end
    else
    begin
      Result := aDatasetClass.Create(nil);
    end;
  end
  else if tempConn is TADOConnection then
  begin
    Result := TADOQuery.Create(nil);
  end;

  if ABThirdConnU.ABSetDatasetConn(Result,tempConn) then
  begin
    try
      ABSetDatasetSQL(result,Trim(aSql));
      if high(aParamValues) > -1 then
        ABSetParamValue(result, aParamValues);

      aSql:=ABSetDatasetAutoParamsAndSQLParams(result,aExtParamsDataset);
      if (aActive) and (aSql<>emptystr) then
      begin
        Result.Open;
      end;
    except
      ABAppendLog_ErrorInfo('ABGetDataset','[HostIP:'+ABPubUser.HostIP+']'+'[HostName:'+ABPubUser.HostName+']'+'[ConnName:'+aConnName+']',aSql);
      if Assigned(result) then
      begin
        result.Free;
      end;
      raise;
    end;
  end;
end;


function ABDownToTextFromField(aConnName: string;
  aTableName, aFieldName, aWhereSql: string
  ): string;
var
  tempDataSet: TDataSet;
begin
  Result := EmptyStr;
  if (AnsiCompareText(Copy(trim(aWhereSql), 1, 5), 'Where') <> 0) and (aWhereSql<> EmptyStr) then
    aWhereSql := ' where ' + aWhereSql;

  tempDataSet := ABGetDataset(aConnName, 'select ' + aFieldName + ' from ' +  aTableName + ' ' + aWhereSql, []);
  try
    if (not ABDatasetIsEmpty(tempDataSet)) and (not tempDataSet.Fields[0].IsNull) then
    begin
      Result := tempDataSet.Fields[0].AsString;
    end;
  finally
    tempDataSet.Free;
  end;
end;

function ABUpTextToField(aConnName: string;
  aTableName, aFieldName, aWhereSql: string;
  aText: string): LongInt;
begin
  if (AnsiCompareText(Copy(trim(aWhereSql), 1, 5), 'Where') <> 0) and (aWhereSql<> EmptyStr) then
    aWhereSql := ' where ' + aWhereSql;

  Result :=ABExecSQL(aConnName,' update ' + aTableName +' set ' + aFieldName + '=:tempFieldValue ' +aWhereSql, [aText]);
end;

function ABUpFileToField(aConnName: string;
  aTableName, aFieldName, aWhereSql,
  aFileName: string): LongInt;
var
  tempMemoryStream:TMemoryStream;
begin
  tempMemoryStream:=TMemoryStream.Create;
  try
    if ABCheckFileExists(aFileName) then
      tempMemoryStream.LoadFromFile(aFileName);

    Result := ABUpStreamToField(aConnName,
                                aTableName, aFieldName, aWhereSql,tempMemoryStream);
  finally
    tempMemoryStream.Free;
  end;
end;

function ABUpStreamToField(aConnName: string;
  aTableName, aFieldName, aWhereSql: string;
  aStream: TMemoryStream): LongInt;
begin
  if (AnsiCompareText(Copy(trim(aWhereSql), 1, 5), 'Where') <> 0) and (aWhereSql <> EmptyStr) then
    aWhereSql := ' where ' + aWhereSql;

  if (Assigned(aStream)) and
     (aStream.Size>0) then
  begin
    Result :=ABExecSQL(aConnName,' update ' + aTableName +' set ' + aFieldName + '=:tempFieldValue ' +aWhereSql,[ABStreamToVariant(aStream)]);
  end
  else
  begin
    Result :=ABExecSQL(aConnName,' update ' + aTableName +' set ' + aFieldName + '=null ' +aWhereSql);
  end;
end;

function ABDownToFileFromField(aConnName: string;
  aTableName, aFieldName, aWhereSql,
  aDownFileName: string): Boolean;
var
  tempMemoryStream:TMemoryStream;
begin
  tempMemoryStream:=TMemoryStream.Create;
  try
    ABDeleteFile(aDownFileName);
    Result := ABDownToStreamFromField(aConnName,
                                      aTableName, aFieldName, aWhereSql,tempMemoryStream
                                      );
    if Result then
    begin
      tempMemoryStream.SaveToFile(aDownFileName)
    end;
  finally
    tempMemoryStream.Free;
  end;
end;

function ABDownToStreamFromField(aConnName: string;
  aTableName, aFieldName, aWhereSql: string;
  aStream: TMemoryStream
  ): Boolean;
var
  tempDataSet: TDataSet;
begin
  Result := false;
  if (AnsiCompareText(Copy(trim(aWhereSql), 1, 5), 'Where') <> 0) and (aWhereSql<> EmptyStr) then
    aWhereSql := ' where ' + aWhereSql;
  tempDataSet := ABGetDataset(aConnName, 'select ' + aFieldName + ' from ' +aTableName + ' ' + aWhereSql, []);
  try
    if (not ABDatasetIsEmpty(tempDataSet)) and (not tempDataSet.Fields[0].IsNull) then
    begin
      aStream.Position := 0;
      TBlobField(tempDataSet.Fields[0]).SaveToStream(aStream);
      aStream.Position := 0;
      Result := true;
    end;
  finally
    tempDataSet.Free;
  end;
end;

{ TABThirdCustomQuery }

constructor TABThirdCustomQuery.Create(AOwner: TComponent);
begin
  inherited;
  CachedUpdates:=true;
  ActiveStoredUsage:=[];
  OnCommandChanged := QueryChanged;
end;

destructor TABThirdCustomQuery.Destroy;
begin

  inherited;
end;


procedure TABThirdCustomQuery.AfterChangSQL(aNewSQL: String; var aDesigning: Boolean;var aLoading: Boolean);
begin

end;

procedure TABThirdCustomQuery.QueryChanged(Sender: TObject);
var
  tempDesigning: Boolean;
  tempLoading: Boolean;
begin
  AfterChangSQL(SQL.Text,tempDesigning,tempLoading);
end;

procedure TABThirdCustomQuery.SetReadOnly(const Value: Boolean);
begin
  UpdateOptions.ReadOnly:=Value;
end;

procedure TABThirdCustomQuery.DoAfterPost;
begin
  inherited;
  //清除缓存模式下产生的更新日志
  CommitUpdates;
end;

function TABThirdCustomQuery.GetReadOnly: Boolean;
begin
  Result:=UpdateOptions.ReadOnly;
end;

procedure TABThirdCustomQuery.OpenCursor(InfoQuery: Boolean);
begin
  inherited;
end;

procedure TABThirdCustomQuery.DoAfterDelete;
begin
  inherited;
  //清除缓存模式下产生的更新日志
  CommitUpdates;
end;

{ TABThirdReadDataQuery }

function TABThirdReadDataQuery.GetBaseSQL: string;
begin
  if trim(FBaseSQL) = emptystr then
  begin
    result := trim(ABGetDatasetSQL(self));
  end
  else
  begin
    result := trim(FBaseSQL);
  end;
end;

procedure TABThirdReadDataQuery.GetActiveDetailDataSets(aList: TList);
var
  i:LongInt;
begin
  GetDetailDataSets(aList);
  for I := aList.Count - 1  downto 0 do
  begin
    if (not TDataSet(aList[i]).Active) then
    begin
      aList.Delete(i);
    end;
  end;
end;

procedure TABThirdReadDataQuery.SetFieldDefaultValuesTemplate(const Value: TStrings);
begin
  FFieldDefaultValuesTemplate.Assign(Value);
end;

procedure TABThirdReadDataQuery.SetLoadTables(const Value: TStrings);
begin
  FLoadTables := Value;
end;

procedure TABThirdReadDataQuery.DoCopy;
var
  I, i2, j, k: Integer;
  tempFieldValus: array of array of array of Variant;
  tempList: TList;
  tempAllDetailIsEmpty: Boolean;
  tempOldBeforeScroll: TDataSetNotifyEvent;
  tempOldAfterScroll: TDataSetNotifyEvent;
begin
  if not FCanInsert then
  begin
    abshow('数据集不能新增,请检查.');
    Abort;
  end;

  tempList := TList.Create;
  try
    GetActiveDetailDataSets(tempList);
    for I := 1 to tempList.Count do
    begin
      TDataset(tempList.Items[I - 1]).DisableControls;
    end;

    //为tempFieldValus分配tempList.Count行
    SetLength(tempFieldValus, tempList.Count + 1);

    //主检测POST操作
    if State in [dsEdit, dsInsert] then
      Post;
    //主表分配
    SetLength(tempFieldValus[0], 1);
    SetLength(tempFieldValus[0][0], FieldCount);

    tempAllDetailIsEmpty := true;
    //从表测POST与分配
    for I := 1 to tempList.Count do
    begin
      if TDataset(tempList.Items[I - 1]).State in [dsEdit, dsInsert] then
        TDataset(tempList.Items[I - 1]).Post;

      if (tempAllDetailIsEmpty) and
        (not ABDatasetIsEmpty(TDataset(tempList.Items[I - 1]))) then
        tempAllDetailIsEmpty := false;

      SetLength(tempFieldValus[I], TDataset(tempList.Items[I - 1]).RecordCount);

      i2 := 0;
      ABBackAndStopDatasetEvent(TDataset(tempList.Items[I - 1]),tempOldBeforeScroll,tempOldAfterScroll);
      TDataset(tempList.Items[I - 1]).DisableControls;
      try
        TDataset(tempList.Items[I - 1]).First;
        while not TDataset(tempList.Items[I - 1]).Eof do
        begin
          SetLength(tempFieldValus[I][i2], TDataset(tempList.Items[I - 1])
            .FieldCount);
          i2 := i2 + 1;
          TDataset(tempList.Items[I - 1]).next;
        end;
      finally
        ABUnBackDatasetEvent(TDataset(tempList.Items[I - 1]),tempOldBeforeScroll,tempOldAfterScroll);
        TDataset(tempList.Items[I - 1]).EnableControls;
      end;
    end;

    //复制当前记录
    for I := 0 to FieldCount - 1 do
    begin
      tempFieldValus[0][0][I] := fields[I].Value;
    end;

    for I := 1 to tempList.Count do
    begin
      i2 := 0;
      ABBackAndStopDatasetEvent(TDataset(tempList.Items[I - 1]),tempOldBeforeScroll,tempOldAfterScroll);
      TDataset(tempList.Items[I - 1]).DisableControls;
      try
        TDataset(tempList.Items[I - 1]).First;
        while not TDataset(tempList.Items[I - 1]).Eof do
        begin
          for j := 0 to TDataset(tempList.Items[I - 1]).FieldCount - 1 do
          begin
            tempFieldValus[I][i2][j] := TDataset(tempList.Items[I - 1])
              .fields[j].Value;
          end;
          i2 := i2 + 1;
          TDataset(tempList.Items[I - 1]).next;
        end;
      finally
        ABUnBackDatasetEvent(TDataset(tempList.Items[I - 1]),tempOldBeforeScroll,tempOldAfterScroll);
        TDataset(tempList.Items[I - 1]).EnableControls;
      end;
    end;

    //将复制的记录粘贴到新记录中
    Insert;
    for I := 0 to FieldCount - 1 do
    begin
      if (not ABVarIsNull(tempFieldValus[0][0][I])) and
        (FieldCanCopy(fields[I].FieldName)) then
      begin
        ABSetFieldValue(tempFieldValus[0][0][I], fields[I]);
      end;
    end;

    if (tempList.Count > 0) and (not tempAllDetailIsEmpty) then
    begin
      k := abshow(' 主表复制完成,是否修改主表的数据(在复制从表前会先保存主表的数据)?', [],
        ['是', '否', '仅复制主表'], 2);
      if k = 1 then
      begin
        if ABShowEditFieldValue(self) then
        begin
          if (State in [dsEdit,dsInsert]) then
            Post;
        end;
      end
      else if k = 2 then
      begin
        Post;
      end
      else
      begin
      end;

      if (k = 1) or (k = 2) then
      begin
        for I := 1 to tempList.Count do
        begin
          for i2 := Low(tempFieldValus[I]) to High(tempFieldValus[I]) do
          begin
            TDataset(tempList.Items[I - 1]).Append;
            for j := 0 to TDataset(tempList.Items[I - 1]).FieldCount - 1 do
            begin
              if (not ABVarIsNull(tempFieldValus[I][i2][j])) and
                (TABThirdReadDataQuery(tempList.Items[I - 1]).FieldCanCopy(TDataset(tempList.Items[I - 1]).fields[j].FieldName)) then
              begin
                ABSetFieldValue(tempFieldValus[I][i2][j],TDataset(tempList.Items[I - 1]).fields[j]);
              end;
            end;
            TDataset(tempList.Items[I - 1]).Post;
          end;
        end;
      end;
    end;
  finally
    for I := 1 to tempList.Count do
    begin
      TDataset(tempList.Items[I - 1]).EnableControls;
    end;
    tempList.Free;
  end;
end;

procedure TABThirdReadDataQuery.DoTransactBaseSQL(var aSQL: string);
begin

end;

function TABThirdReadDataQuery.FieldCanCopy(aFieldName: string): Boolean;
begin
  result := true;
end;

function TABThirdReadDataQuery.FieldDefaultValueCanUse(aFieldName,
  aFieldValue: string): Boolean;
begin
  result := true;
end;

function TABThirdReadDataQuery.GetMainDataset: TABThirdReadDataQuery;
begin
  result:=nil;
  if (Assigned(DataSource)) and
     (Assigned(DataSource.DataSet)) then
  begin
    result:=TABThirdReadDataQuery(DataSource.DataSet);
  end;
end;

function TABThirdReadDataQuery.CheckCommand(aDataset: TDataset;
  aCheckCommand: string): Boolean;
begin
  result := true;
  if (aCheckCommand <> emptystr) then
  begin
    aCheckCommand := ABReplaceSqlExtParams(aCheckCommand);
    aCheckCommand := trim(ABReplaceSqlFieldNameParams(aCheckCommand,[aDataset]));
    if not ABExecScripts(aCheckCommand,true) then
    begin
      result := false;
    end;
  end;
end;

function TABThirdReadDataQuery.CheckFieldValues(aDataset: TDataset; aCheckFieldValues,aCheckMsg, aType: string): Boolean;
var
  I: Integer;
  tempstr1: string;
  tempFieldName: string;
  tempFieldValue: string;
  tempField: TField;
begin
  result := true;
  if aCheckFieldValues <> emptystr then
  begin
    aCheckFieldValues := ABStringReplace(aCheckFieldValues, ';', ',');
    aCheckFieldValues := ABReplaceSqlExtParams(aCheckFieldValues);
    aCheckFieldValues := trim(ABReplaceSqlFieldNameParams(aCheckFieldValues,[aDataset]));

    for I := 1 to ABGetSpaceStrCount(aCheckFieldValues, ',') do
    begin
      tempstr1 := ABGetSpaceStr(aCheckFieldValues, I, ',');
      tempFieldName := ABGetLeftRightStr(tempstr1,  axdleft);
      tempFieldValue := ABGetLeftRightStr(tempstr1, axdright);
      tempField := aDataset.FindField(tempFieldName);
      if (Assigned(tempField)) and
         ((tempField.AsString = tempFieldValue) or
          (tempField.DataType = ftBoolean) and
          (tempField.AsBoolean = ABStrToBool(ABIIF(tempFieldValue = emptystr, '1',tempFieldValue)))
          ) then
      begin
        if aCheckMsg = emptystr then
        begin
          abshow('字段[%s]为[%s]时不能' + aType + ',请检查.',[tempField.DisplayLabel, tempFieldValue]);
        end
        else
        begin
          abshow(aCheckMsg);
        end;
        result := false;
        Break;
      end;
    end;
  end;
end;

procedure TABThirdReadDataQuery.CheckMasterRange;
var
  tempStream:TStream;
  tempOK:boolean;
  tempParams:TParams;
begin
  inherited;
  if FExecOrOpenLock then
  begin
    exit;
  end;

  if ABLocalParams.LoginType=ltCS_Three then
  begin
    FMasterRangeIng:=True;
    try
      tempParams := TParams.Create(nil);
      try
        ABCopyParams(self,tempParams);
        tempStream:=ABGetServerClient.GetDatasetStream(FConnName,ABStringReplace(Sql.Text,ABEnterWrapStr,' '),FLoadTables.Text,tempParams,tempOK);
        if tempOK then
        begin
          LoadFromStream(ABGetServerStreamToMemoryStream(tempStream));
        end;
      finally
        //tempParams.Free;
      end;
    finally
      FMasterRangeIng:=False;
    end;
  end
end;

function TABThirdReadDataQuery.BeforeInsertCheck: Boolean;
begin
  result := CheckFieldValues(self, FBeforeInsertCheckFieldValues, '新增',FBeforeInsertCheckMsg);
end;

function TABThirdReadDataQuery.BeforeDeleteCheck: Boolean;
begin
  result := CheckFieldValues(self, FBeforeDeleteCheckFieldValues, '删除',FBeforeDeleteCheckMsg);
end;

function TABThirdReadDataQuery.BeforeEditCheck: Boolean;
begin
  result := CheckFieldValues(self, FBeforeEditCheckFieldValues, '修改',FBeforeEditCheckMsg);
end;

function TABThirdReadDataQuery.BeforePostCheck: Boolean;
begin
  result := CheckFieldValues(self, FBeforePostCheckFieldValues, '保存',FBeforePostCheckMsg);
end;

function TABThirdReadDataQuery.BeforeEditCheckParent: Boolean;
begin
  result := true;
  if (Assigned(DataSource)) and (Assigned(DataSource.DataSet)) then
  begin
    result := CheckFieldValues(DataSource.DataSet,
                               TABThirdReadDataQuery(DataSource.DataSet).BeforeEditCheckFieldValues, '修改',
                               '从表不能操作');
  end;
end;

function TABThirdReadDataQuery.RefreshQuery(aAddWhere: string;
                      aSyncQuery: array of TDataset;
                      aSyncQueryCheckParamsDataset: TDataset;
                      aSyncQueryIsRequery: Boolean;
                      aSyncQueryIsDisableControls: Boolean;
                      aSyncQueryIsSavePosition: Boolean): LongInt;
var
  I: LongInt;
begin
  result := 0;
  FAddWhereofNext := aAddWhere;
  ABReFreshQuery(self, [], nil, false);
  if Active then
  begin
    for I := Low(aSyncQuery) to High(aSyncQuery) do
    begin
      ABReFreshQuery(aSyncQuery[I], [], aSyncQueryCheckParamsDataset, aSyncQueryIsRequery,aSyncQueryIsDisableControls, aSyncQueryIsSavePosition);
    end;
    result := RecordCount;
  end;
end;

function TABThirdReadDataQuery.UpdateBaseSQL(aNewSQL:string): Boolean;
begin
  result:=false;
  if (not FSQLTransacting) then
  begin
    if (AnsiCompareText(trim(FBaseSQL), trim(aNewSQL)) <> 0) then
    begin
      FBaseSQL := trim(aNewSQL);
      result:=True;
    end;
  end;
end;

procedure TABThirdReadDataQuery.AfterChangSQL(aNewSQL: String;var aDesigning: Boolean; var aLoading: Boolean);
begin
  aDesigning := false;
  aLoading := false;
  if UpdateBaseSQL(aNewSQL) then
  begin
    if (Assigned(Owner)) and
       (Owner.ClassName<>EmptyStr) and
       (Name<>EmptyStr) then
    begin
      if  //在设计时修改SQL时组件的状态（触发1次)
          (ABGetSetCount(ComponentState,sizeof(TComponentState))=2) and
          (csDesigning in ComponentState) and (csFreeNotification in ComponentState)  then
      begin
        aDesigning:=True;
        FSqlUpdateDatetime:=Now;
      end
      else if  //在窗体显示时加载
               (ABGetSetCount(ComponentState,sizeof(TComponentState))=3) and
               (csDesigning in ComponentState) and (csLoading in ComponentState) and
               (csReading in ComponentState)  then
      begin
        aLoading:=True;
      end;
    end;
  end;
end;

procedure TABThirdReadDataQuery.ParseSQLToParseObject(aSQL: string);
begin
  if (Assigned(FSQLParseObject)) and
     (
       (not Assigned(FSQLParseObject.SQLParseDef)) or
       (AnsiCompareText(trim(FSQLParseObject.SQLParseDef.Sql), trim(aSQL)) <> 0)
     ) then
  begin
    FSQLParseObject.Parse(aSQL);
  end;
end;

procedure TABThirdReadDataQuery.TransactBaseSQL;
var
  tempSQL: string;
begin
  if FSQLTransacting then
    Exit;

  FSQLTransacting := true;
  try
    tempSQL := GetBaseSQL;
    ParseSQLToParseObject(tempSQL);
    if FAddWhereofNext <> emptystr then
    begin
      tempSQL := FSQLParseObject.AddWhereNoEditOldSQL(FAddWhereofNext);
    end;

    DoTransactBaseSQL(tempSQL);

    tempSQL := ABReplaceSqlExtParams(tempSQL);

    if (AnsiCompareText(trim(tempSQL), trim(Sql.Text)) <> 0) then
      Sql.Text := trim(tempSQL);

    ABSetUnknownParamType(self, nil);
    ABSetDatasetAutoParams(self, nil);
  finally
    FSQLTransacting := false;
  end;
end;

procedure TABThirdReadDataQuery.RestoreBaseSQL;
begin
  if FSQLTransacting then
    Exit;

  FSQLTransacting := true;
  try
    if (trim(FBaseSQL) <> emptystr) and
       (AnsiCompareText(trim(FBaseSQL), trim(Sql.Text)) <> 0) then
      Sql.Text := trim(FBaseSQL);
  finally
    FSQLTransacting := false;
  end;
end;

constructor TABThirdReadDataQuery.Create(AOwner: TComponent);
begin
  inherited;
  FLoadTables:=TStringList.Create;
  FCanDelete := true;
  FCanPrint := true;
  FCanInsert := true;
  FCanEdit := true;
  FBeforeDeleteAsk := true;
  FLastOperatorDatetime := 0;

  FFieldDefaultValuesTemplate := TStringList.Create;
  FFieldDefaultValuesQuoteDatasetList:= TList.Create;
  FSQLParseObject := TABSQLParse.Create(nil);
end;

destructor TABThirdReadDataQuery.Destroy;
begin
  FFieldDefaultValuesTemplate.Free;
  FFieldDefaultValuesQuoteDatasetList.Free;
  FSQLParseObject.Free;
  FSQLParseObject:=nil;
  FLoadTables.Free;

  inherited;
end;

function TABThirdReadDataQuery.RowRefresh: boolean;
begin

  result:=true;
end;

procedure TABThirdReadDataQuery.SetActive(Value: Boolean);
var
  tempStream:TStream;
  tempOK:boolean;
  tempParams:TParams;
begin
  if FExecOrOpenLock then
  begin
    inherited;
    exit;
  end;

  FExecOrOpenLock:=true;
  try
    //打开数据集
    if (not Active) and
       (Value) then
    begin
      TransactBaseSQL;
      ABThirdConnU.ABSetDatasetConnByConnName(self,FConnName);
      if (ABLocalParams.LoginType=ltCS_Three) and
         (not FMasterRangeIng) then
      begin
        tempParams := TParams.Create(nil);
        try
          ABCopyParams(self,tempParams);
          tempStream:=ABGetServerClient.GetDatasetStream(FConnName,ABStringReplace(Sql.Text,ABEnterWrapStr,' '),FLoadTables.Text,tempParams,tempOK);
          if tempOK then
          begin
            LoadFromStream(ABGetServerStreamToMemoryStream(tempStream));
          end;
        finally
          //tempParams.Free;
        end;
      end
      else
      begin
        inherited;
      end;
    end
    //关闭数据集
    else
    begin
      if (Active) and (not Value) then
        RestoreBaseSQL;

      inherited;
    end;
  finally
    FExecOrOpenLock:=False;
  end;
end;


procedure TABThirdReadDataQuery.SetConnName(const Value: string);
begin
  FConnName := Value;
  Connection:=nil;
end;

procedure TABThirdReadDataQuery.DoAfterCancel;
begin
  inherited;

  FLastOperatorType := dtBrowse;
  FLastOperatorDatetime := now;
end;

procedure TABThirdReadDataQuery.DoAfterPost;
begin
  inherited;
  FLastOperatorType := dtBrowse;
  FLastOperatorDatetime := now;
end;

procedure TABThirdReadDataQuery.DoAfterClose;
begin
  inherited;

  FLastOperatorType := dtClose;
  FLastOperatorDatetime := now;
end;

procedure TABThirdReadDataQuery.DoAfterDelete;
begin
  inherited;
  FLastOperatorType := dtDelete;
  FLastOperatorDatetime := now;
end;

procedure TABThirdReadDataQuery.DoAfterEdit;
var
  tempField: TField;
begin
  inherited;

  if (FocusFieldNameOnEdit <> emptystr) then
  begin
    tempField := FindField(FocusFieldNameOnEdit);
    if (Assigned(tempField)) then
      ABSetControlFocusByField(tempField);
  end;

  FLastOperatorType := dtEdit;
  FLastOperatorDatetime := now;
end;

function TABThirdReadDataQuery.GetFieldExtDefaultValue(aFieldName,aFieldValue:string):Variant;
var
  I: LongInt;
  tempField: TField;
begin
  result:=aFieldValue;
  if (aFieldValue <> emptystr) then
  begin
    if (FieldDefaultValueCanUse(aFieldName,aFieldValue)) then
    begin
      aFieldValue:=ABReplaceSqlExtParams(aFieldValue);
      aFieldValue := trim(ABReplaceSqlFieldNameParams(aFieldValue,[self]));

      if (ABIsSelectSQL(aFieldValue)) then
      begin
        if (not FStopSQLDefaultValueAfterInsert) then
          result:=ABGetSQLValue(ConnName,aFieldValue,[],'');
      end
      else
      begin
        result:= aFieldValue;
        for i := 0 to FFieldDefaultValuesQuoteDatasetList.Count-1 do
        begin
          tempField := Tdataset(FFieldDefaultValuesQuoteDatasetList[i]).FindField(aFieldValue);
          if (Assigned(tempField)) then
          begin
            result:=tempField.AsString;
            break;
          end;
        end;
      end;
    end;
  end;
end;

procedure TABThirdReadDataQuery.DoAfterInsert;
var
  I: LongInt;
  tempstr1: string;
  tempField: TField;
  tempFieldDefaultValues: string;
begin
  inherited;

  DisableControls;
  try
    //处理输入模板
    for I := 0 to FFieldDefaultValuesTemplate.Count - 1 do
    begin
      ABSetFieldValue(GetFieldExtDefaultValue(FFieldDefaultValuesTemplate.Names[I],
                                           FFieldDefaultValuesTemplate.ValueFromIndex[I]),
                      FindField(FFieldDefaultValuesTemplate.Names[I]));
    end;

    //数据集默认值字段处理
    tempFieldDefaultValues := ABStringReplace(FFieldDefaultValues, ';', ',');
    for I := 1 to ABGetSpaceStrCount(tempFieldDefaultValues, ',') do
    begin
      tempstr1 := ABGetSpaceStr(tempFieldDefaultValues, I, ',');
      ABSetFieldValue(GetFieldExtDefaultValue(ABGetLeftRightStr(tempstr1,  axdleft),
                                           ABGetLeftRightStr(tempstr1 ,axdright)),
                      FindField(ABGetLeftRightStr(tempstr1,  axdleft)));
    end;

    {
    //主从表的关键字处理
    for I := 1 to ABGetSpaceStrCount(DetailFields, ';') do
    begin
      if Assigned(DataSource) then
      begin
        tempField1 := FindField(ABGetSpaceStr(DetailFields, I, ';'));
        tempField2 := DataSource.DataSet.FindField
          (ABGetSpaceStr(MasterFields, I, ';'));
        if (Assigned(tempField1)) and (Assigned(tempField2)) then
          ABSetFieldValue(tempField2.Value, tempField1);
      end;
    end;
    }
  finally
    EnableControls;
  end;

  if (FFocusFieldNameOnInsert <> emptystr) then
  begin
    tempField := FindField(FFocusFieldNameOnInsert);
    if (Assigned(tempField)) then
    begin
      ABSetControlFocusByField(tempField);
    end;
  end;

  FLastOperatorType := dtAppend;
  FLastOperatorDatetime := now;
end;

procedure TABThirdReadDataQuery.DoAfterOpen;
//设置字段的自定义标题
  procedure SetFieldCaptions;
  var
    I: LongInt;
    tempstr1, tempFieldName, tempFieldCaption: string;
  begin
    if FFieldCaptions <> emptystr then
    begin
      for I := 1 to ABGetSpaceStrCount(FFieldCaptions, ',') do
      begin
        tempstr1 := ABGetSpaceStr(FFieldCaptions, I, ',');
        tempFieldName := trim(ABGetLeftRightStr(tempstr1,  axdleft));
        tempFieldCaption := trim(ABGetLeftRightStr(tempstr1,  axdright));
        tempFieldCaption := ABReplaceSqlExtParams(tempFieldCaption);
        tempFieldCaption := trim(ABReplaceSqlFieldNameParams(tempFieldCaption,[self]));

        if (tempFieldCaption <> emptystr) and
          (Assigned(FindField(tempFieldName))) and
          (AnsiCompareText(FindField(tempFieldName).DisplayLabel,tempFieldCaption) <> 0) then
        begin
          FindField(tempFieldName).DisplayLabel := tempFieldCaption;
        end;
      end;
    end;
  end;

begin
  inherited;
  SetFieldCaptions;

  FLastOperatorType := dtBrowse;
  FLastOperatorDatetime := now;
end;

procedure TABThirdReadDataQuery.DoBeforeDelete;
var
  I: LongInt;
  tempList: TList;
begin
  //检测数据集是否可以删除记录
  if not FCanDelete then
  begin
    abshow('数据不能删除,请检查.');
    Abort;
  end;

  if (IsEmpty) then
  begin
    abshow('数据为空时不能删除,请检查.');
    Abort;
  end;

  //检测父数据集是否能修改
  if not BeforeEditCheckParent then
    Abort;

  //检测删除时字段的值是否许可删除,并提示ABDeleteNotMsg消息
  if not BeforeDeleteCheck then
    Abort;

  //检测删除检测的命令
  if not CheckCommand(self, FBeforeDeleteCheckCommand) then
    Abort;

  //删除时的确认消息(只针对窗体中的数据集面言)
  if (FBeforeDeleteAsk) and (Name <> emptystr) then
  begin
    tempList := TList.Create;
    try
      GetActiveDetailDataSets(tempList);

      //有从表时的提示
      if (tempList.Count > 0) and
        (ABGetDataSetRecordCount(tempList) > 0) then
      begin
        if abshow('确定删除吗?', [], ['是', '否'], 1) = 2 then
        begin
          Abort;
        end
        else
        begin
          for I := 0 to tempList.Count - 1 do
          begin
            TABThirdReadDataQuery(tempList[I]).FLastOperatorDatetime := now;
            TABThirdReadDataQuery(tempList[I]).FLastOperatorType := dtDelete;
          end;
        end;
      end
      else
      begin
        //如果没有从表且不是刚新增则提示
        if (not(State in [dsInsert])) then
        begin
          if BeforeDeleteAskMsg = emptystr then
          begin
            if abshow('确定删除吗?', [], ['是', '否'], 1) = 2 then
              Abort;
          end
          else
          begin
            if abshow(BeforeDeleteAskMsg, [], ['是', '否'], 1) = 2 then
              Abort;
          end;
        end;
      end;
    finally
      tempList.Free;
    end;
  end;

  inherited;
end;

procedure TABThirdReadDataQuery.DoBeforeEdit;
begin
  if not FCanEdit then
  begin
    abshow('数据集不能修改,请检查.');
    Abort;
  end;

  if not BeforeEditCheckParent then
    Abort;

  if not BeforeEditCheck then
    Abort;

  if not CheckCommand(self, FBeforeEditCheckCommand) then
    Abort;

  inherited;
end;

procedure TABThirdReadDataQuery.DoBeforeInsert;
var
  tempDataSet: TDataset;
begin
  if not FCanInsert then
  begin
    abshow('数据集不能新增,请检查.');
    Abort;
  end;

  //检测是否超过最大记录数
  if (FMaxRecordCount > 0) and (RecordCount + 1 > FMaxRecordCount) then
  begin
    abshow('已超过最大记录数[%s],不能新增,请检查.', [inttostr(FMaxRecordCount)]);
    Abort;
  end;

  if not BeforeEditCheckParent then
    Abort;

  if not BeforeInsertCheck then
    Abort;

  if not CheckCommand(self, FBeforeInsertCheckCommand) then
    Abort;

  if (Assigned(DataSource)) and (Assigned(DataSource.DataSet)) then
  begin
    tempDataSet := TDataset(DataSource.DataSet);
    if (tempDataSet.State in [dsEdit, dsInsert]) then
    begin
      try
        tempDataSet.Post;
      except
        abshow('请先保存主表数据,再增加从表数据.');
        Abort;
      end;
    end;

    if ABDatasetIsEmpty(tempDataSet) then
    begin
      abshow('请先增加主表数据,再增加从表数据.');
      Abort;
    end;
  end;

  inherited;
end;

procedure TABThirdReadDataQuery.DoBeforePost;
var
  tempDataSet: TDataset;
  I: Integer;
  tempList: TList;
begin
  //当由于一些原因在BeforePost前保存过的则不再进行检测与数据库操作
  if (State in [dsEdit, dsInsert]) then
  begin
    if not BeforePostCheck then
      Abort;

    if not CheckCommand(self, FBeforePostCheckCommand) then
      Abort;

    //保存从表数据
    tempList := TList.Create;
    try
      GetActiveDetailDataSets(tempList);

      for I := 0 to tempList.Count - 1 do
      begin
        tempDataSet := TDataset(tempList[I]);
        if tempDataSet.State in [dsEdit, dsInsert] then
          tempDataSet.Post;
      end;
    finally
      tempList.Free;
    end;
  end;

  inherited;
end;

procedure TABThirdReadDataQuery.Loaded;
begin
  inherited;
  if (trim(FBaseSQL) = emptystr) and
     (trim(SQL.Text) <> emptystr) then
    FBaseSQL := trim(SQL.Text);
end;

function TABThirdReadDataQuery.FieldByName(const aFieldName: string): TField;
begin
  result := inherited FindField(trim(aFieldName));

  if (not Assigned(result)) then
  begin
    ABAppendLog_ErrorInfo('FieldByName','[HostIP:'+ABPubUser.HostIP+']'+'[HostName:'+ABPubUser.HostName+']'+'[ConnName:'+ConnName+']',ABGetOwnerLongName(self)+' FieldByName['+aFieldName+']');
  end;
end;

function TABThirdReadDataQuery.Locate(const KeyFields: string; const KeyValues: Variant;
  Options: TLocateOptions): Boolean;
begin
  if Options = [] then
  begin
    Options := [loCaseInsensitive];
  end;
  result := inherited Locate(KeyFields, KeyValues, Options);
end;

procedure ABFinalization;
begin
  unRegisterClass(TABThirdReadDataQuery           );
end;

procedure ABInitialization;
begin
  RegisterClass(TABThirdReadDataQuery           );
end;


Initialization
  ABInitialization;

Finalization
  ABFinalization;


end.
