{
cxGrid右键打印单元

  dxPScxGrid6Lnk或dxPScxGridLnk,
  dxPSBaseGridLnk,
  dxPSGrLnks, dxPSLbxLnk,dxPSContainerLnk,dxPSTextLnk,

  以上单元会关联一些控件的类型,不能少 次序不能错
}
unit ABThird_cxGridPopupMenu_PrintU;

interface              
{$I ..\ABInclude.ini}

uses
  ABPubFormU,
  ABPubVarU,
  ABPubFuncU,

  dxPScxGridLnk,
  dxPSBaseGridLnk,
  dxPSGrLnks, dxPSLbxLnk,dxPSContainerLnk,dxPSTextLnk,

  dxPgsDlg,
  dxPSCore,
  SysUtils,Classes,Graphics,Controls,Forms,StdCtrls,ExtCtrls,jpeg, dxPSGlbl,
  dxPSUtl, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, cxClasses;

type
  TABcxGridPopupMenu_PrintForm = class(TABPubForm)
    Panel2: TPanel;
    Panel1: TPanel;
    cxListBox1: TListBox;
    GroupBox1: TGroupBox;
    SpeedButton1: TButton;
    SpeedButton2: TButton;
    SpeedButton3: TButton;
    SpeedButton4: TButton;
    GroupBox2: TGroupBox;
    ListBox1: TListBox;
    dxPrintStyleManager1: TdxPrintStyleManager;
    dxPrintStyleManager1Style1: TdxPSPrintStyle;
    dxPrintStyleManager1Style2: TdxPSPrintStyle;
    SpeedButton6: TButton;
    SpeedButton7: TButton;
    SpeedButton8: TButton;
    SpeedButton5: TButton;
    procedure SpeedButton8Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    function SaveStyles(ShowDialog: Boolean): Boolean;
    procedure SpeedButton6Click(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure cxListBox1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure dxPrintStyleManager1ChangeCurrentStyle(Sender: TObject);
    procedure dxPrintStyleManager1StyleListChanged(Sender: TObject);
    { Private declarations }
  private
    FdxComponentPrinter:TdxComponentPrinter;
    FLinkControl:TControl;
    procedure BuildStylesMenu;
  public
    procedure SetLink(aControl:TControl);
    { Public declarations }
  end;

implementation

{$R *.dfm}


{ TPrintItem }

procedure TABcxGridPopupMenu_PrintForm.BuildStylesMenu;
var
  I: Integer;
begin
  ListBox1.Clear;

  for I := dxPrintStyleManager1.Count - 1 downto 0 do
  begin
    ListBox1.Items.Add(dxPrintStyleManager1.Styles[I].StyleCaption);
  end;
end;

procedure TABcxGridPopupMenu_PrintForm.dxPrintStyleManager1ChangeCurrentStyle(Sender: TObject);
begin
  ListBox1.ItemIndex:=dxPrintStyleManager1.CurrentStyleIndex;

end;

procedure TABcxGridPopupMenu_PrintForm.dxPrintStyleManager1StyleListChanged(Sender: TObject);
begin
  BuildStylesMenu;
end;

procedure TABcxGridPopupMenu_PrintForm.FormCreate(Sender: TObject);
begin
  FdxComponentPrinter:=TdxComponentPrinter.Create(nil);
  BuildStylesMenu;
  Width:=165;
  if dxPrintStyleManager1.Count > 0 then
    dxPrintStyleManager1.CurrentStyleIndex := 0;
end;

procedure TABcxGridPopupMenu_PrintForm.FormDestroy(Sender: TObject);
var
  tempStr1:string;
begin
  if (FLinkControl=nil) or (FLinkControl.Owner=nil) then
    exit;

  tempStr1:=ABConfigPath+FLinkControl.Owner.Name+'_'+FLinkControl.Name+'_PrintSet.ini';
  if ABCheckFileExists(tempStr1) then
  begin
    DeleteFile(tempStr1);
  end;
  if tempStr1<>emptystr then
  begin
    dxPrintStyleManager1.StorageName := tempStr1;
    dxPrintStyleManager1.SaveToFile(dxPrintStyleManager1.StorageName);
  end;
  FdxComponentPrinter.Free;
end;

function TABcxGridPopupMenu_PrintForm.SaveStyles(ShowDialog: Boolean): Boolean;
var
  tempstr:string;
begin
  Result := True;

  tempstr := ABSelectFile('',dxPrintStyleManager1.StorageName,'sdf');

  if ( tempstr<>emptystr) or ShowDialog then
  begin
    dxPrintStyleManager1.StorageName :=tempstr;
    dxPrintStyleManager1.SaveToFile(dxPrintStyleManager1.StorageName);
  end;
end;

procedure TABcxGridPopupMenu_PrintForm.SetLink(aControl: TControl);
var
  i:LongInt;
begin
  FLinkControl:=aControl;

  FdxComponentPrinter.DeleteAllLinks;
  FdxComponentPrinter.AddLink(FlinkControl);
  if  FdxComponentPrinter.LinkCount>0 then
  begin
    FdxComponentPrinter.CurrentLink :=FdxComponentPrinter.ReportLink[0];
    FdxComponentPrinter.CurrentLink.StyleManager:= dxPrintStyleManager1;
    cxListBox1.Clear;
    if Assigned(dxPSLinkClassByCompClass(TComponentClass(FlinkControl.Owner.ClassType))) then
      cxListBox1.Items.AddObject(FlinkControl.Owner.Name,
                                          FlinkControl.Owner);
    for I := 0 to FlinkControl.Owner.ComponentCount - 1 do
    begin
      if FlinkControl.Owner.Components[i] is Twincontrol then
      begin
        if Assigned(dxPSLinkClassByCompClass(TComponentClass(FlinkControl.Owner.Components[i].ClassType))) then
          cxListBox1.Items.AddObject(FlinkControl.Owner.Components[i].Name,
                                              FlinkControl.Owner.Components[i]);
      end;
    end;
    if cxListBox1.Items.IndexOf(FlinkControl.Name)>=0 then
      cxListBox1.ItemIndex:= cxListBox1.Items.IndexOf(FlinkControl.Name);
    cxListBox1Click(cxListBox1);

    GroupBox1.Enabled:=true;
    GroupBox1.Enabled:=true;
    Panel2.Enabled:=true;
  end
  else
  begin
    GroupBox1.Enabled:=False;
    GroupBox1.Enabled:=False;
    Panel2.Enabled:=False;
  end;
end;

procedure TABcxGridPopupMenu_PrintForm.SpeedButton8Click(Sender: TObject);
var
  i:longint;
  Pages:array of Integer;
  graphicClass:  TgraphicClass ;
var
  tempstr,tempstr1:string;
begin
  tempstr := ABSelectFile('',dxPrintStyleManager1.StorageName,'jpg');
  if tempstr<>emptystr then
  begin
    graphicClass:= TgraphicClass(Tjpegimage);
    FdxComponentPrinter.RebuildReport();
    SetLength(Pages,FdxComponentPrinter.CurrentLink.PageCount);
    for  i:= low(Pages) to high(Pages) do
    begin
      Pages[i]:=i;
    end;

    tempstr1:=ABGetFileExt(tempstr);
    if tempstr1=emptystr then
      tempstr1:='jpg';

    FdxComponentPrinter.SavePagesAsImagesToDisk
    (Pages,graphicClass,false,ABGetFilepath(tempstr)+ABGetFilename(tempstr)+'_%d.'+tempstr1,nil,nil,nil);
  end;

end;

procedure TABcxGridPopupMenu_PrintForm.SpeedButton4Click(Sender: TObject);
begin
  FdxComponentPrinter.PageSetup(nil);
end;

procedure TABcxGridPopupMenu_PrintForm.SpeedButton5Click(Sender: TObject);
begin
  Panel1.Visible:=not Panel1.Visible;
  if panel1.Visible then
  begin
    Width:=450;
  end
  else
    Width:=165;
end;

procedure TABcxGridPopupMenu_PrintForm.SpeedButton2Click(Sender: TObject);
begin
  FdxComponentPrinter.Preview(True, nil);
end;

procedure TABcxGridPopupMenu_PrintForm.SpeedButton3Click(Sender: TObject);
begin
  FdxComponentPrinter.Print(True, nil, nil);
end;

procedure TABcxGridPopupMenu_PrintForm.cxListBox1Click(Sender: TObject);
var
  tempStr1:string;
begin
  if dxPrintStyleManager1.StorageName<>emptystr then
    dxPrintStyleManager1.SaveToFile(dxPrintStyleManager1.StorageName);

  if cxListBox1.ItemIndex>=0 then
  begin
    FdxComponentPrinter.DeleteAllLinks;
    FdxComponentPrinter.AddLink(TComponent(cxListBox1.Items.Objects[cxListBox1.ItemIndex]));
    FdxComponentPrinter.CurrentLink :=FdxComponentPrinter.ReportLink[0];
    FdxComponentPrinter.CurrentLink.StyleManager:= dxPrintStyleManager1;
    FlinkControl:=TControl(cxListBox1.Items.Objects[cxListBox1.ItemIndex]);

    tempStr1:=ABConfigPath+FLinkControl.Owner.Name+'_'+FLinkControl.Name+'_PrintSet.ini';

    if tempStr1<>emptystr then
    begin
      dxPrintStyleManager1.StorageName := tempStr1;
      if ABCheckFileExists(tempStr1) then
      begin
        dxPrintStyleManager1.LoadFromFile(dxPrintStyleManager1.StorageName);
      end;
    end;
  end;
end;

procedure TABcxGridPopupMenu_PrintForm.SpeedButton7Click(Sender: TObject);
begin
  SaveStyles(True);
end;

procedure TABcxGridPopupMenu_PrintForm.SpeedButton6Click(Sender: TObject);
var
  tempstr:string;
begin
  tempstr := ABSelectFile('',dxPrintStyleManager1.StorageName,'sdf');
  if tempstr<>emptystr then
  begin
    dxPrintStyleManager1.StorageName := tempstr;
    dxPrintStyleManager1.LoadFromFile(dxPrintStyleManager1.StorageName);
  end;
end;

procedure TABcxGridPopupMenu_PrintForm.ListBox1Click(Sender: TObject);
begin
  dxPrintStyleManager1.Styles[ListBox1.ItemIndex].IsCurrentStyle:=true;
end;

procedure TABcxGridPopupMenu_PrintForm.ListBox1DblClick(Sender: TObject);
var
  tempB1,tempB2:Boolean;
begin
  tempB1:=true;
  tempB2:=true;
  dxPrintStyleManager1.DefinePrintStylesDlg(tempB1, tempB2);
end;


end.
