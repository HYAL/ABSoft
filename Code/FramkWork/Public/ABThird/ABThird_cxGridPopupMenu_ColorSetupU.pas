{
cxGrid�Ҽ���ɫ���õ�Ԫ
}
unit ABThird_cxGridPopupMenu_ColorSetupU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,
  ABPubMemoU,


  ABPubScriptU,
  ABPubFuncU,

  ABThirdFuncU,


  cxStyleSheetEditor,
  cxLookAndFeels,
  cxGridTableView,
  cxStyles,
  cxGridDBTableView,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGridCustomTableView,
  cxgrid,

  Windows,Messages,Forms,SysUtils,Menus,Variants,Dialogs,StdCtrls,DB,Controls,
  ExtCtrls,Classes,ActnList, cxClasses;


const
  ColumnColorFlag='OnlyViewColumn ';

type
  TABcxGridPopupMenu_ColorSetupForm = class(TABPubForm)
    pnlLeft: TPanel;
    gbUserDefined: TGroupBox;
    cbUserStyleSheets: TComboBox;
    btnLoad: TButton;
    btnSave: TButton;
    btnEdit: TButton;
    RadioGroup: TRadioGroup;
    gbPredefined: TGroupBox;
    lbPredefinedStyleSheets: TListBox;
    Panel2: TPanel;
    SpeedButton1: TButton;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cxComboBox1: TComboBox;
    cxComboBox2: TComboBox;
    cxButton1: TButton;
    cxListBox1: TListBox;
    mmMain: TMainMenu;
    miFile: TMenuItem;
    miExit: TMenuItem;
    miOptions: TMenuItem;
    miGridLookFeel: TMenuItem;
    miKind: TMenuItem;
    miFlat: TMenuItem;
    miStandard: TMenuItem;
    miUltraFlat: TMenuItem;
    miNativeStyle: TMenuItem;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle378: TcxStyle;
    cxStyle379: TcxStyle;
    cxStyle380: TcxStyle;
    cxStyle381: TcxStyle;
    cxStyle382: TcxStyle;
    cxStyle383: TcxStyle;
    cxStyle384: TcxStyle;
    cxStyle385: TcxStyle;
    cxStyle386: TcxStyle;
    cxStyle387: TcxStyle;
    cxStyle388: TcxStyle;
    cxStyle389: TcxStyle;
    cxStyle390: TcxStyle;
    cxStyle391: TcxStyle;
    cxStyle392: TcxStyle;
    cxStyle393: TcxStyle;
    cxStyle394: TcxStyle;
    cxStyle395: TcxStyle;
    cxStyle396: TcxStyle;
    cxStyle397: TcxStyle;
    cxStyle398: TcxStyle;
    cxStyle399: TcxStyle;
    cxStyle400: TcxStyle;
    cxStyle401: TcxStyle;
    cxStyle402: TcxStyle;
    cxStyle403: TcxStyle;
    cxStyle404: TcxStyle;
    cxStyle405: TcxStyle;
    cxGridTableViewStyleSheet1: TcxGridTableViewStyleSheet;
    cxGridTableViewStyleSheet2: TcxGridTableViewStyleSheet;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyle8: TcxStyle;
    cxStyle9: TcxStyle;
    cxStyle10: TcxStyle;
    cxStyle11: TcxStyle;
    cxStyle12: TcxStyle;
    cxStyle13: TcxStyle;
    cxStyle14: TcxStyle;
    cxStyle15: TcxStyle;
    cxStyle16: TcxStyle;
    cxStyle17: TcxStyle;
    cxStyle18: TcxStyle;
    cxStyle19: TcxStyle;
    cxStyle20: TcxStyle;
    cxStyle21: TcxStyle;
    cxStyle22: TcxStyle;
    cxStyle23: TcxStyle;
    cxStyle24: TcxStyle;
    cxStyle25: TcxStyle;
    cxStyle26: TcxStyle;
    cxStyle27: TcxStyle;
    cxStyle28: TcxStyle;
    cxStyle29: TcxStyle;
    cxStyle30: TcxStyle;
    cxStyle31: TcxStyle;
    cxStyle32: TcxStyle;
    cxStyle33: TcxStyle;
    cxStyle34: TcxStyle;
    cxStyle35: TcxStyle;
    cxStyle36: TcxStyle;
    cxStyle37: TcxStyle;
    cxStyle38: TcxStyle;
    cxStyle39: TcxStyle;
    cxStyle40: TcxStyle;
    cxStyle41: TcxStyle;
    cxStyle42: TcxStyle;
    cxStyle43: TcxStyle;
    cxStyle44: TcxStyle;
    cxStyle45: TcxStyle;
    cxStyle46: TcxStyle;
    cxStyle47: TcxStyle;
    cxStyle48: TcxStyle;
    cxStyle49: TcxStyle;
    cxStyle50: TcxStyle;
    cxStyle51: TcxStyle;
    cxStyle52: TcxStyle;
    cxStyle53: TcxStyle;
    cxStyle54: TcxStyle;
    cxStyle55: TcxStyle;
    cxStyle56: TcxStyle;
    cxStyle57: TcxStyle;
    cxStyle58: TcxStyle;
    cxStyle59: TcxStyle;
    cxStyle60: TcxStyle;
    cxStyle61: TcxStyle;
    cxStyle62: TcxStyle;
    cxStyle63: TcxStyle;
    cxStyle64: TcxStyle;
    cxStyle65: TcxStyle;
    cxStyle66: TcxStyle;
    cxStyle67: TcxStyle;
    cxStyle68: TcxStyle;
    cxStyle69: TcxStyle;
    cxStyle70: TcxStyle;
    cxStyle71: TcxStyle;
    cxStyle72: TcxStyle;
    cxStyle73: TcxStyle;
    cxStyle74: TcxStyle;
    cxStyle75: TcxStyle;
    cxStyle76: TcxStyle;
    cxStyle77: TcxStyle;
    cxStyle78: TcxStyle;
    cxStyle79: TcxStyle;
    cxStyle80: TcxStyle;
    cxStyle81: TcxStyle;
    cxStyle82: TcxStyle;
    cxStyle83: TcxStyle;
    cxStyle84: TcxStyle;
    cxStyle85: TcxStyle;
    cxStyle86: TcxStyle;
    cxStyle87: TcxStyle;
    cxStyle88: TcxStyle;
    cxStyle89: TcxStyle;
    cxStyle90: TcxStyle;
    cxStyle91: TcxStyle;
    cxStyle92: TcxStyle;
    cxStyle93: TcxStyle;
    cxStyle94: TcxStyle;
    cxStyle95: TcxStyle;
    cxStyle96: TcxStyle;
    cxStyle97: TcxStyle;
    cxStyle98: TcxStyle;
    cxStyle99: TcxStyle;
    cxStyle100: TcxStyle;
    cxStyle101: TcxStyle;
    cxStyle102: TcxStyle;
    cxStyle103: TcxStyle;
    cxStyle104: TcxStyle;
    cxStyle105: TcxStyle;
    cxStyle106: TcxStyle;
    cxStyle107: TcxStyle;
    cxStyle108: TcxStyle;
    cxStyle109: TcxStyle;
    cxStyle110: TcxStyle;
    cxStyle111: TcxStyle;
    cxStyle112: TcxStyle;
    cxStyle113: TcxStyle;
    cxStyle114: TcxStyle;
    cxStyle115: TcxStyle;
    cxStyle116: TcxStyle;
    cxStyle117: TcxStyle;
    cxStyle118: TcxStyle;
    cxStyle119: TcxStyle;
    cxStyle120: TcxStyle;
    cxStyle121: TcxStyle;
    cxStyle122: TcxStyle;
    cxStyle123: TcxStyle;
    cxStyle124: TcxStyle;
    cxStyle125: TcxStyle;
    cxStyle126: TcxStyle;
    cxStyle127: TcxStyle;
    cxStyle128: TcxStyle;
    cxStyle129: TcxStyle;
    cxStyle130: TcxStyle;
    cxStyle131: TcxStyle;
    cxStyle132: TcxStyle;
    cxStyle133: TcxStyle;
    cxStyle134: TcxStyle;
    cxStyle135: TcxStyle;
    cxStyle136: TcxStyle;
    cxStyle137: TcxStyle;
    cxStyle138: TcxStyle;
    cxStyle139: TcxStyle;
    cxStyle140: TcxStyle;
    cxStyle141: TcxStyle;
    cxStyle142: TcxStyle;
    cxStyle143: TcxStyle;
    cxStyle144: TcxStyle;
    cxStyle145: TcxStyle;
    cxStyle146: TcxStyle;
    cxStyle147: TcxStyle;
    cxStyle148: TcxStyle;
    cxStyle149: TcxStyle;
    cxStyle150: TcxStyle;
    cxStyle151: TcxStyle;
    cxStyle152: TcxStyle;
    cxStyle153: TcxStyle;
    cxStyle154: TcxStyle;
    cxStyle155: TcxStyle;
    cxStyle156: TcxStyle;
    cxStyle157: TcxStyle;
    cxStyle158: TcxStyle;
    cxStyle159: TcxStyle;
    cxStyle160: TcxStyle;
    cxStyle161: TcxStyle;
    cxStyle162: TcxStyle;
    cxStyle163: TcxStyle;
    cxStyle164: TcxStyle;
    cxStyle165: TcxStyle;
    cxStyle166: TcxStyle;
    cxStyle167: TcxStyle;
    cxStyle168: TcxStyle;
    cxStyle169: TcxStyle;
    cxStyle170: TcxStyle;
    cxStyle171: TcxStyle;
    cxStyle172: TcxStyle;
    cxStyle173: TcxStyle;
    cxStyle174: TcxStyle;
    cxStyle175: TcxStyle;
    cxStyle176: TcxStyle;
    cxStyle177: TcxStyle;
    cxStyle178: TcxStyle;
    cxStyle179: TcxStyle;
    cxStyle180: TcxStyle;
    cxStyle181: TcxStyle;
    cxStyle182: TcxStyle;
    cxStyle183: TcxStyle;
    cxStyle184: TcxStyle;
    cxStyle185: TcxStyle;
    cxStyle186: TcxStyle;
    cxStyle187: TcxStyle;
    cxStyle188: TcxStyle;
    cxStyle189: TcxStyle;
    cxStyle190: TcxStyle;
    cxStyle191: TcxStyle;
    cxStyle192: TcxStyle;
    cxStyle193: TcxStyle;
    cxStyle194: TcxStyle;
    cxStyle195: TcxStyle;
    cxStyle196: TcxStyle;
    cxStyle197: TcxStyle;
    cxStyle198: TcxStyle;
    cxStyle199: TcxStyle;
    cxStyle200: TcxStyle;
    cxStyle201: TcxStyle;
    cxStyle202: TcxStyle;
    cxStyle203: TcxStyle;
    cxStyle204: TcxStyle;
    cxStyle205: TcxStyle;
    cxStyle206: TcxStyle;
    cxStyle207: TcxStyle;
    cxStyle208: TcxStyle;
    cxStyle209: TcxStyle;
    cxStyle210: TcxStyle;
    cxStyle211: TcxStyle;
    cxStyle212: TcxStyle;
    cxStyle213: TcxStyle;
    cxStyle214: TcxStyle;
    cxStyle215: TcxStyle;
    cxStyle216: TcxStyle;
    cxStyle217: TcxStyle;
    cxStyle218: TcxStyle;
    cxStyle219: TcxStyle;
    cxStyle220: TcxStyle;
    cxStyle221: TcxStyle;
    cxStyle222: TcxStyle;
    cxStyle223: TcxStyle;
    cxStyle224: TcxStyle;
    cxStyle225: TcxStyle;
    cxStyle226: TcxStyle;
    cxStyle227: TcxStyle;
    cxStyle228: TcxStyle;
    cxStyle229: TcxStyle;
    cxStyle230: TcxStyle;
    cxStyle231: TcxStyle;
    cxStyle232: TcxStyle;
    cxStyle233: TcxStyle;
    cxStyle234: TcxStyle;
    cxStyle235: TcxStyle;
    cxStyle236: TcxStyle;
    cxStyle237: TcxStyle;
    cxStyle238: TcxStyle;
    cxStyle239: TcxStyle;
    cxStyle240: TcxStyle;
    cxStyle241: TcxStyle;
    cxStyle242: TcxStyle;
    cxStyle243: TcxStyle;
    cxStyle244: TcxStyle;
    cxStyle245: TcxStyle;
    cxStyle246: TcxStyle;
    cxStyle247: TcxStyle;
    cxStyle248: TcxStyle;
    cxStyle249: TcxStyle;
    cxStyle250: TcxStyle;
    cxStyle251: TcxStyle;
    cxStyle252: TcxStyle;
    cxStyle253: TcxStyle;
    cxStyle254: TcxStyle;
    cxStyle255: TcxStyle;
    cxStyle256: TcxStyle;
    cxStyle257: TcxStyle;
    cxStyle258: TcxStyle;
    cxStyle259: TcxStyle;
    cxStyle260: TcxStyle;
    cxStyle261: TcxStyle;
    cxStyle262: TcxStyle;
    cxStyle263: TcxStyle;
    cxStyle264: TcxStyle;
    cxStyle265: TcxStyle;
    cxStyle266: TcxStyle;
    cxStyle267: TcxStyle;
    cxStyle268: TcxStyle;
    cxStyle269: TcxStyle;
    cxStyle270: TcxStyle;
    cxStyle271: TcxStyle;
    cxStyle272: TcxStyle;
    cxStyle273: TcxStyle;
    cxStyle274: TcxStyle;
    cxStyle275: TcxStyle;
    cxStyle276: TcxStyle;
    cxStyle277: TcxStyle;
    cxStyle278: TcxStyle;
    cxStyle279: TcxStyle;
    cxStyle280: TcxStyle;
    cxStyle281: TcxStyle;
    cxStyle282: TcxStyle;
    cxStyle283: TcxStyle;
    cxStyle284: TcxStyle;
    cxStyle285: TcxStyle;
    cxStyle286: TcxStyle;
    cxStyle287: TcxStyle;
    cxStyle288: TcxStyle;
    cxStyle289: TcxStyle;
    cxStyle290: TcxStyle;
    cxStyle291: TcxStyle;
    cxStyle292: TcxStyle;
    cxStyle293: TcxStyle;
    cxStyle294: TcxStyle;
    cxStyle295: TcxStyle;
    cxStyle296: TcxStyle;
    cxStyle297: TcxStyle;
    cxStyle298: TcxStyle;
    cxStyle299: TcxStyle;
    cxStyle300: TcxStyle;
    cxStyle301: TcxStyle;
    cxStyle302: TcxStyle;
    cxStyle303: TcxStyle;
    cxStyle304: TcxStyle;
    cxStyle305: TcxStyle;
    cxStyle306: TcxStyle;
    cxStyle307: TcxStyle;
    cxStyle308: TcxStyle;
    cxStyle309: TcxStyle;
    cxStyle310: TcxStyle;
    cxStyle311: TcxStyle;
    cxStyle312: TcxStyle;
    cxStyle313: TcxStyle;
    cxStyle314: TcxStyle;
    cxStyle315: TcxStyle;
    cxStyle316: TcxStyle;
    cxStyle317: TcxStyle;
    cxStyle318: TcxStyle;
    cxStyle319: TcxStyle;
    cxStyle320: TcxStyle;
    cxStyle321: TcxStyle;
    cxStyle322: TcxStyle;
    cxStyle323: TcxStyle;
    cxStyle324: TcxStyle;
    cxStyle325: TcxStyle;
    cxStyle326: TcxStyle;
    cxStyle327: TcxStyle;
    cxStyle328: TcxStyle;
    cxStyle329: TcxStyle;
    cxStyle330: TcxStyle;
    cxStyle331: TcxStyle;
    cxStyle332: TcxStyle;
    cxStyle333: TcxStyle;
    cxStyle334: TcxStyle;
    cxStyle335: TcxStyle;
    cxStyle336: TcxStyle;
    cxStyle337: TcxStyle;
    cxStyle338: TcxStyle;
    cxStyle339: TcxStyle;
    cxStyle340: TcxStyle;
    cxStyle341: TcxStyle;
    cxStyle342: TcxStyle;
    cxStyle343: TcxStyle;
    cxStyle344: TcxStyle;
    cxStyle345: TcxStyle;
    cxStyle346: TcxStyle;
    cxStyle347: TcxStyle;
    cxStyle348: TcxStyle;
    cxStyle349: TcxStyle;
    cxStyle350: TcxStyle;
    cxStyle351: TcxStyle;
    cxStyle352: TcxStyle;
    cxStyle353: TcxStyle;
    cxStyle354: TcxStyle;
    cxStyle355: TcxStyle;
    cxStyle356: TcxStyle;
    cxStyle357: TcxStyle;
    cxStyle358: TcxStyle;
    cxStyle359: TcxStyle;
    cxStyle360: TcxStyle;
    cxStyle361: TcxStyle;
    cxStyle362: TcxStyle;
    cxStyle363: TcxStyle;
    cxStyle364: TcxStyle;
    cxStyle365: TcxStyle;
    cxStyle366: TcxStyle;
    cxStyle367: TcxStyle;
    cxStyle368: TcxStyle;
    cxStyle369: TcxStyle;
    cxStyle370: TcxStyle;
    cxStyle371: TcxStyle;
    cxStyle372: TcxStyle;
    cxStyle373: TcxStyle;
    cxStyle374: TcxStyle;
    cxStyle375: TcxStyle;
    cxStyle376: TcxStyle;
    cxStyle377: TcxStyle;
    GridTableViewStyleSheetDevExpress: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetUserFormat1: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetUserFormat2: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetUserFormat3: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetUserFormat4: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetBrick: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetDesert: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetEggplant: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetLilac: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetMaple: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetMarinehighcolor: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetPlumhighcolor: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetPumpkinlarge: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetRainyDay: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetRedWhiteandBlueVGA: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetRose: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetRoselarge: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetSlate: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetSpruce: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetStormVGA: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetTealVGA: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetWheat: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetWindowsClassic: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetWindowsClassiclarge: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetWindowsStandard: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetWindowsStandardlarge: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetHighContrast1: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetHighContrast1large: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetHighContrast2: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetHighContrast2large: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetHighContrastBlack: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetHighContrastBlacklarge: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetHighContrastWhite: TcxGridTableViewStyleSheet;
    GridTableViewStyleSheetHighContrastWhitelarge: TcxGridTableViewStyleSheet;
    cxStyleRepository3: TcxStyleRepository;
    cxStyle406: TcxStyle;
    cxStyle407: TcxStyle;
    cxStyle408: TcxStyle;
    cxStyle409: TcxStyle;
    cxStyle410: TcxStyle;
    cxStyle411: TcxStyle;
    cxStyle412: TcxStyle;
    cxStyle413: TcxStyle;
    cxStyle414: TcxStyle;
    cxStyle415: TcxStyle;
    cxStyle416: TcxStyle;
    cxStyle417: TcxStyle;
    cxStyle418: TcxStyle;
    cxStyle419: TcxStyle;
    cxStyle420: TcxStyle;
    cxStyle421: TcxStyle;
    cxStyle422: TcxStyle;
    cxStyle423: TcxStyle;
    cxStyle424: TcxStyle;
    cxStyle425: TcxStyle;
    cxStyle426: TcxStyle;
    cxStyle427: TcxStyle;
    cxStyle428: TcxStyle;
    cxStyle429: TcxStyle;
    cxStyle430: TcxStyle;
    cxStyle431: TcxStyle;
    cxStyle432: TcxStyle;
    cxStyle433: TcxStyle;
    cxStyle811: TcxStyle;
    cxStyle812: TcxStyle;
    cxStyle813: TcxStyle;
    cxStyle814: TcxStyle;
    cxStyle815: TcxStyle;
    cxStyle816: TcxStyle;
    cxStyle817: TcxStyle;
    cxStyle818: TcxStyle;
    cxStyle819: TcxStyle;
    cxStyle820: TcxStyle;
    cxStyle821: TcxStyle;
    cxStyle822: TcxStyle;
    cxStyle823: TcxStyle;
    cxStyle824: TcxStyle;
    cxStyle825: TcxStyle;
    cxStyle826: TcxStyle;
    cxStyle827: TcxStyle;
    cxStyle828: TcxStyle;
    cxStyle829: TcxStyle;
    cxStyle830: TcxStyle;
    cxStyle831: TcxStyle;
    cxStyle832: TcxStyle;
    cxStyle833: TcxStyle;
    cxStyle834: TcxStyle;
    cxStyle835: TcxStyle;
    cxStyle836: TcxStyle;
    cxStyle837: TcxStyle;
    cxStyle838: TcxStyle;
    cxStyle839: TcxStyle;
    cxStyle840: TcxStyle;
    cxStyle841: TcxStyle;
    cxStyle842: TcxStyle;
    cxStyle843: TcxStyle;
    cxStyle844: TcxStyle;
    cxStyle845: TcxStyle;
    cxStyle846: TcxStyle;
    cxStyle847: TcxStyle;
    cxStyle848: TcxStyle;
    cxStyle849: TcxStyle;
    cxStyle850: TcxStyle;
    cxStyle851: TcxStyle;
    cxStyle852: TcxStyle;
    cxStyle853: TcxStyle;
    cxStyle854: TcxStyle;
    cxStyle855: TcxStyle;
    cxStyle856: TcxStyle;
    cxStyle857: TcxStyle;
    cxStyle858: TcxStyle;
    cxStyle859: TcxStyle;
    cxStyle860: TcxStyle;
    cxStyle861: TcxStyle;
    cxStyle862: TcxStyle;
    cxStyle863: TcxStyle;
    cxStyle864: TcxStyle;
    cxStyle865: TcxStyle;
    cxStyle866: TcxStyle;
    cxStyle867: TcxStyle;
    cxStyle868: TcxStyle;
    cxStyle869: TcxStyle;
    cxStyle870: TcxStyle;
    cxStyle871: TcxStyle;
    cxStyle872: TcxStyle;
    cxStyle873: TcxStyle;
    cxStyle874: TcxStyle;
    cxStyle875: TcxStyle;
    cxStyle876: TcxStyle;
    cxStyle877: TcxStyle;
    cxStyle878: TcxStyle;
    cxStyle879: TcxStyle;
    cxStyle880: TcxStyle;
    cxStyle881: TcxStyle;
    cxStyle882: TcxStyle;
    cxStyle883: TcxStyle;
    cxStyle884: TcxStyle;
    cxStyle885: TcxStyle;
    cxStyle886: TcxStyle;
    cxStyle887: TcxStyle;
    cxStyle888: TcxStyle;
    cxStyle889: TcxStyle;
    cxStyle890: TcxStyle;
    cxStyle891: TcxStyle;
    cxStyle892: TcxStyle;
    cxStyle893: TcxStyle;
    cxStyle894: TcxStyle;
    cxStyle895: TcxStyle;
    cxStyle896: TcxStyle;
    cxStyle897: TcxStyle;
    cxStyle898: TcxStyle;
    cxStyle899: TcxStyle;
    cxGridBandedTableViewStyleSheet1: TcxGridBandedTableViewStyleSheet;
    cxGridBandedTableViewStyleSheet2: TcxGridBandedTableViewStyleSheet;
    cxStyleRepository4: TcxStyleRepository;
    cxStyle434: TcxStyle;
    cxStyle435: TcxStyle;
    cxStyle436: TcxStyle;
    cxStyle437: TcxStyle;
    cxStyle438: TcxStyle;
    cxStyle439: TcxStyle;
    cxStyle440: TcxStyle;
    cxStyle441: TcxStyle;
    cxStyle442: TcxStyle;
    cxStyle443: TcxStyle;
    cxStyle444: TcxStyle;
    cxStyle445: TcxStyle;
    cxStyle446: TcxStyle;
    cxStyle447: TcxStyle;
    cxStyle448: TcxStyle;
    cxStyle449: TcxStyle;
    cxStyle450: TcxStyle;
    cxStyle451: TcxStyle;
    cxStyle452: TcxStyle;
    cxStyle453: TcxStyle;
    cxStyle454: TcxStyle;
    cxStyle455: TcxStyle;
    cxStyle456: TcxStyle;
    cxStyle457: TcxStyle;
    cxStyle458: TcxStyle;
    cxStyle459: TcxStyle;
    cxStyle460: TcxStyle;
    cxStyle461: TcxStyle;
    cxStyle462: TcxStyle;
    cxStyle463: TcxStyle;
    cxStyle464: TcxStyle;
    cxStyle465: TcxStyle;
    cxStyle466: TcxStyle;
    cxStyle467: TcxStyle;
    cxStyle468: TcxStyle;
    cxStyle469: TcxStyle;
    cxStyle470: TcxStyle;
    cxStyle471: TcxStyle;
    cxStyle472: TcxStyle;
    cxStyle473: TcxStyle;
    cxStyle474: TcxStyle;
    cxStyle475: TcxStyle;
    cxStyle476: TcxStyle;
    cxStyle477: TcxStyle;
    cxStyle478: TcxStyle;
    cxStyle479: TcxStyle;
    cxStyle480: TcxStyle;
    cxStyle481: TcxStyle;
    cxStyle482: TcxStyle;
    cxStyle483: TcxStyle;
    cxStyle484: TcxStyle;
    cxStyle485: TcxStyle;
    cxStyle486: TcxStyle;
    cxStyle487: TcxStyle;
    cxStyle488: TcxStyle;
    cxStyle489: TcxStyle;
    cxStyle490: TcxStyle;
    cxStyle491: TcxStyle;
    cxStyle492: TcxStyle;
    cxStyle493: TcxStyle;
    cxStyle494: TcxStyle;
    cxStyle495: TcxStyle;
    cxStyle496: TcxStyle;
    cxStyle497: TcxStyle;
    cxStyle498: TcxStyle;
    cxStyle499: TcxStyle;
    cxStyle500: TcxStyle;
    cxStyle501: TcxStyle;
    cxStyle502: TcxStyle;
    cxStyle503: TcxStyle;
    cxStyle504: TcxStyle;
    cxStyle505: TcxStyle;
    cxStyle506: TcxStyle;
    cxStyle507: TcxStyle;
    cxStyle508: TcxStyle;
    cxStyle509: TcxStyle;
    cxStyle510: TcxStyle;
    cxStyle511: TcxStyle;
    cxStyle512: TcxStyle;
    cxStyle513: TcxStyle;
    cxStyle514: TcxStyle;
    cxStyle515: TcxStyle;
    cxStyle516: TcxStyle;
    cxStyle517: TcxStyle;
    cxStyle518: TcxStyle;
    cxStyle519: TcxStyle;
    cxStyle520: TcxStyle;
    cxStyle521: TcxStyle;
    cxStyle522: TcxStyle;
    cxStyle523: TcxStyle;
    cxStyle524: TcxStyle;
    cxStyle525: TcxStyle;
    cxStyle526: TcxStyle;
    cxStyle527: TcxStyle;
    cxStyle528: TcxStyle;
    cxStyle529: TcxStyle;
    cxStyle530: TcxStyle;
    cxStyle531: TcxStyle;
    cxStyle532: TcxStyle;
    cxStyle533: TcxStyle;
    cxStyle534: TcxStyle;
    cxStyle535: TcxStyle;
    cxStyle536: TcxStyle;
    cxStyle537: TcxStyle;
    cxStyle538: TcxStyle;
    cxStyle539: TcxStyle;
    cxStyle540: TcxStyle;
    cxStyle541: TcxStyle;
    cxStyle542: TcxStyle;
    cxStyle543: TcxStyle;
    cxStyle544: TcxStyle;
    cxStyle545: TcxStyle;
    cxStyle546: TcxStyle;
    cxStyle547: TcxStyle;
    cxStyle548: TcxStyle;
    cxStyle549: TcxStyle;
    cxStyle550: TcxStyle;
    cxStyle551: TcxStyle;
    cxStyle552: TcxStyle;
    cxStyle553: TcxStyle;
    cxStyle554: TcxStyle;
    cxStyle555: TcxStyle;
    cxStyle556: TcxStyle;
    cxStyle557: TcxStyle;
    cxStyle558: TcxStyle;
    cxStyle559: TcxStyle;
    cxStyle560: TcxStyle;
    cxStyle561: TcxStyle;
    cxStyle562: TcxStyle;
    cxStyle563: TcxStyle;
    cxStyle564: TcxStyle;
    cxStyle565: TcxStyle;
    cxStyle566: TcxStyle;
    cxStyle567: TcxStyle;
    cxStyle568: TcxStyle;
    cxStyle569: TcxStyle;
    cxStyle570: TcxStyle;
    cxStyle571: TcxStyle;
    cxStyle572: TcxStyle;
    cxStyle573: TcxStyle;
    cxStyle574: TcxStyle;
    cxStyle575: TcxStyle;
    cxStyle576: TcxStyle;
    cxStyle577: TcxStyle;
    cxStyle578: TcxStyle;
    cxStyle579: TcxStyle;
    cxStyle580: TcxStyle;
    cxStyle581: TcxStyle;
    cxStyle582: TcxStyle;
    cxStyle583: TcxStyle;
    cxStyle584: TcxStyle;
    cxStyle585: TcxStyle;
    cxStyle586: TcxStyle;
    cxStyle587: TcxStyle;
    cxStyle588: TcxStyle;
    cxStyle589: TcxStyle;
    cxStyle590: TcxStyle;
    cxStyle591: TcxStyle;
    cxStyle592: TcxStyle;
    cxStyle593: TcxStyle;
    cxStyle594: TcxStyle;
    cxStyle595: TcxStyle;
    cxStyle596: TcxStyle;
    cxStyle597: TcxStyle;
    cxStyle598: TcxStyle;
    cxStyle599: TcxStyle;
    cxStyle600: TcxStyle;
    cxStyle601: TcxStyle;
    cxStyle602: TcxStyle;
    cxStyle603: TcxStyle;
    cxStyle604: TcxStyle;
    cxStyle605: TcxStyle;
    cxStyle606: TcxStyle;
    cxStyle607: TcxStyle;
    cxStyle608: TcxStyle;
    cxStyle609: TcxStyle;
    cxStyle610: TcxStyle;
    cxStyle611: TcxStyle;
    cxStyle612: TcxStyle;
    cxStyle613: TcxStyle;
    cxStyle614: TcxStyle;
    cxStyle615: TcxStyle;
    cxStyle616: TcxStyle;
    cxStyle617: TcxStyle;
    cxStyle618: TcxStyle;
    cxStyle619: TcxStyle;
    cxStyle620: TcxStyle;
    cxStyle621: TcxStyle;
    cxStyle622: TcxStyle;
    cxStyle623: TcxStyle;
    cxStyle624: TcxStyle;
    cxStyle625: TcxStyle;
    cxStyle626: TcxStyle;
    cxStyle627: TcxStyle;
    cxStyle628: TcxStyle;
    cxStyle629: TcxStyle;
    cxStyle630: TcxStyle;
    cxStyle631: TcxStyle;
    cxStyle632: TcxStyle;
    cxStyle633: TcxStyle;
    cxStyle634: TcxStyle;
    cxStyle635: TcxStyle;
    cxStyle636: TcxStyle;
    cxStyle637: TcxStyle;
    cxStyle638: TcxStyle;
    cxStyle639: TcxStyle;
    cxStyle640: TcxStyle;
    cxStyle641: TcxStyle;
    cxStyle642: TcxStyle;
    cxStyle643: TcxStyle;
    cxStyle644: TcxStyle;
    cxStyle645: TcxStyle;
    cxStyle646: TcxStyle;
    cxStyle647: TcxStyle;
    cxStyle648: TcxStyle;
    cxStyle649: TcxStyle;
    cxStyle650: TcxStyle;
    cxStyle651: TcxStyle;
    cxStyle652: TcxStyle;
    cxStyle653: TcxStyle;
    cxStyle654: TcxStyle;
    cxStyle655: TcxStyle;
    cxStyle656: TcxStyle;
    cxStyle657: TcxStyle;
    cxStyle658: TcxStyle;
    cxStyle659: TcxStyle;
    cxStyle660: TcxStyle;
    cxStyle661: TcxStyle;
    cxStyle662: TcxStyle;
    cxStyle663: TcxStyle;
    cxStyle664: TcxStyle;
    cxStyle665: TcxStyle;
    cxStyle666: TcxStyle;
    cxStyle667: TcxStyle;
    cxStyle668: TcxStyle;
    cxStyle669: TcxStyle;
    cxStyle670: TcxStyle;
    cxStyle671: TcxStyle;
    cxStyle672: TcxStyle;
    cxStyle673: TcxStyle;
    cxStyle674: TcxStyle;
    cxStyle675: TcxStyle;
    cxStyle676: TcxStyle;
    cxStyle677: TcxStyle;
    cxStyle678: TcxStyle;
    cxStyle679: TcxStyle;
    cxStyle680: TcxStyle;
    cxStyle681: TcxStyle;
    cxStyle682: TcxStyle;
    cxStyle683: TcxStyle;
    cxStyle684: TcxStyle;
    cxStyle685: TcxStyle;
    cxStyle686: TcxStyle;
    cxStyle687: TcxStyle;
    cxStyle688: TcxStyle;
    cxStyle689: TcxStyle;
    cxStyle690: TcxStyle;
    cxStyle691: TcxStyle;
    cxStyle692: TcxStyle;
    cxStyle693: TcxStyle;
    cxStyle694: TcxStyle;
    cxStyle695: TcxStyle;
    cxStyle696: TcxStyle;
    cxStyle697: TcxStyle;
    cxStyle698: TcxStyle;
    cxStyle699: TcxStyle;
    cxStyle700: TcxStyle;
    cxStyle701: TcxStyle;
    cxStyle702: TcxStyle;
    cxStyle703: TcxStyle;
    cxStyle704: TcxStyle;
    cxStyle705: TcxStyle;
    cxStyle706: TcxStyle;
    cxStyle707: TcxStyle;
    cxStyle708: TcxStyle;
    cxStyle709: TcxStyle;
    cxStyle710: TcxStyle;
    cxStyle711: TcxStyle;
    cxStyle712: TcxStyle;
    cxStyle713: TcxStyle;
    cxStyle714: TcxStyle;
    cxStyle715: TcxStyle;
    cxStyle716: TcxStyle;
    cxStyle717: TcxStyle;
    cxStyle718: TcxStyle;
    cxStyle719: TcxStyle;
    cxStyle720: TcxStyle;
    cxStyle721: TcxStyle;
    cxStyle722: TcxStyle;
    cxStyle723: TcxStyle;
    cxStyle724: TcxStyle;
    cxStyle725: TcxStyle;
    cxStyle726: TcxStyle;
    cxStyle727: TcxStyle;
    cxStyle728: TcxStyle;
    cxStyle729: TcxStyle;
    cxStyle730: TcxStyle;
    cxStyle731: TcxStyle;
    cxStyle732: TcxStyle;
    cxStyle733: TcxStyle;
    cxStyle734: TcxStyle;
    cxStyle735: TcxStyle;
    cxStyle736: TcxStyle;
    cxStyle737: TcxStyle;
    cxStyle738: TcxStyle;
    cxStyle739: TcxStyle;
    cxStyle740: TcxStyle;
    cxStyle741: TcxStyle;
    cxStyle742: TcxStyle;
    cxStyle743: TcxStyle;
    cxStyle744: TcxStyle;
    cxStyle745: TcxStyle;
    cxStyle746: TcxStyle;
    cxStyle747: TcxStyle;
    cxStyle748: TcxStyle;
    cxStyle749: TcxStyle;
    cxStyle750: TcxStyle;
    cxStyle751: TcxStyle;
    cxStyle752: TcxStyle;
    cxStyle753: TcxStyle;
    cxStyle754: TcxStyle;
    cxStyle755: TcxStyle;
    cxStyle756: TcxStyle;
    cxStyle757: TcxStyle;
    cxStyle758: TcxStyle;
    cxStyle759: TcxStyle;
    cxStyle760: TcxStyle;
    cxStyle761: TcxStyle;
    cxStyle762: TcxStyle;
    cxStyle763: TcxStyle;
    cxStyle764: TcxStyle;
    cxStyle765: TcxStyle;
    cxStyle766: TcxStyle;
    cxStyle767: TcxStyle;
    cxStyle768: TcxStyle;
    cxStyle769: TcxStyle;
    cxStyle770: TcxStyle;
    cxStyle771: TcxStyle;
    cxStyle772: TcxStyle;
    cxStyle773: TcxStyle;
    cxStyle774: TcxStyle;
    cxStyle775: TcxStyle;
    cxStyle776: TcxStyle;
    cxStyle777: TcxStyle;
    cxStyle778: TcxStyle;
    cxStyle779: TcxStyle;
    cxStyle780: TcxStyle;
    cxStyle781: TcxStyle;
    cxStyle782: TcxStyle;
    cxStyle783: TcxStyle;
    cxStyle784: TcxStyle;
    cxStyle785: TcxStyle;
    cxStyle786: TcxStyle;
    cxStyle787: TcxStyle;
    cxStyle788: TcxStyle;
    cxStyle789: TcxStyle;
    cxStyle790: TcxStyle;
    cxStyle791: TcxStyle;
    cxStyle792: TcxStyle;
    cxStyle793: TcxStyle;
    cxStyle794: TcxStyle;
    cxStyle795: TcxStyle;
    cxStyle796: TcxStyle;
    cxStyle797: TcxStyle;
    cxStyle798: TcxStyle;
    cxStyle799: TcxStyle;
    cxStyle800: TcxStyle;
    cxStyle801: TcxStyle;
    cxStyle802: TcxStyle;
    cxStyle803: TcxStyle;
    cxStyle804: TcxStyle;
    cxStyle805: TcxStyle;
    cxStyle806: TcxStyle;
    cxStyle807: TcxStyle;
    cxStyle808: TcxStyle;
    cxStyle809: TcxStyle;
    cxStyle810: TcxStyle;
    cxStyle900: TcxStyle;
    cxStyle901: TcxStyle;
    cxStyle902: TcxStyle;
    cxStyle903: TcxStyle;
    cxStyle904: TcxStyle;
    cxStyle905: TcxStyle;
    cxStyle906: TcxStyle;
    cxStyle907: TcxStyle;
    cxStyle908: TcxStyle;
    cxStyle909: TcxStyle;
    cxStyle910: TcxStyle;
    cxStyle911: TcxStyle;
    cxStyle912: TcxStyle;
    cxStyle913: TcxStyle;
    cxStyle914: TcxStyle;
    cxStyle915: TcxStyle;
    cxStyle916: TcxStyle;
    cxStyle917: TcxStyle;
    cxStyle918: TcxStyle;
    cxStyle919: TcxStyle;
    cxStyle920: TcxStyle;
    cxStyle921: TcxStyle;
    cxStyle922: TcxStyle;
    cxStyle923: TcxStyle;
    cxStyle924: TcxStyle;
    cxStyle925: TcxStyle;
    cxStyle926: TcxStyle;
    cxStyle927: TcxStyle;
    cxStyle928: TcxStyle;
    cxStyle929: TcxStyle;
    cxStyle930: TcxStyle;
    cxStyle931: TcxStyle;
    cxStyle932: TcxStyle;
    cxStyle933: TcxStyle;
    cxStyle934: TcxStyle;
    cxStyle935: TcxStyle;
    cxStyle936: TcxStyle;
    cxStyle937: TcxStyle;
    cxStyle938: TcxStyle;
    cxStyle939: TcxStyle;
    cxStyle940: TcxStyle;
    cxStyle941: TcxStyle;
    cxStyle942: TcxStyle;
    cxStyle943: TcxStyle;
    cxStyle944: TcxStyle;
    cxStyle945: TcxStyle;
    cxStyle946: TcxStyle;
    cxStyle947: TcxStyle;
    cxStyle948: TcxStyle;
    cxStyle949: TcxStyle;
    cxStyle950: TcxStyle;
    cxStyle951: TcxStyle;
    cxStyle952: TcxStyle;
    cxStyle953: TcxStyle;
    cxStyle954: TcxStyle;
    cxStyle955: TcxStyle;
    cxStyle956: TcxStyle;
    cxStyle957: TcxStyle;
    cxStyle958: TcxStyle;
    cxStyle959: TcxStyle;
    cxStyle960: TcxStyle;
    cxStyle961: TcxStyle;
    cxStyle962: TcxStyle;
    cxStyle963: TcxStyle;
    cxStyle964: TcxStyle;
    cxStyle965: TcxStyle;
    cxStyle966: TcxStyle;
    cxStyle967: TcxStyle;
    cxStyle968: TcxStyle;
    cxStyle969: TcxStyle;
    cxStyle970: TcxStyle;
    cxStyle971: TcxStyle;
    cxStyle972: TcxStyle;
    cxStyle973: TcxStyle;
    cxStyle974: TcxStyle;
    cxStyle975: TcxStyle;
    cxStyle976: TcxStyle;
    cxStyle977: TcxStyle;
    cxStyle978: TcxStyle;
    cxStyle979: TcxStyle;
    cxStyle980: TcxStyle;
    cxStyle981: TcxStyle;
    cxStyle982: TcxStyle;
    cxStyle983: TcxStyle;
    cxStyle984: TcxStyle;
    cxStyle985: TcxStyle;
    cxStyle986: TcxStyle;
    cxStyle987: TcxStyle;
    cxStyle988: TcxStyle;
    cxStyle989: TcxStyle;
    cxStyle990: TcxStyle;
    cxStyle991: TcxStyle;
    cxStyle992: TcxStyle;
    cxStyle993: TcxStyle;
    cxStyle994: TcxStyle;
    cxStyle995: TcxStyle;
    cxStyle996: TcxStyle;
    cxStyle997: TcxStyle;
    cxStyle998: TcxStyle;
    cxStyle999: TcxStyle;
    cxStyle1000: TcxStyle;
    cxStyle1001: TcxStyle;
    cxStyle1002: TcxStyle;
    cxStyle1003: TcxStyle;
    cxStyle1004: TcxStyle;
    cxStyle1005: TcxStyle;
    cxStyle1006: TcxStyle;
    cxStyle1007: TcxStyle;
    cxStyle1008: TcxStyle;
    cxStyle1009: TcxStyle;
    cxStyle1010: TcxStyle;
    cxStyle1011: TcxStyle;
    cxStyle1012: TcxStyle;
    cxStyle1013: TcxStyle;
    cxStyle1014: TcxStyle;
    cxStyle1015: TcxStyle;
    cxStyle1016: TcxStyle;
    cxStyle1017: TcxStyle;
    cxStyle1018: TcxStyle;
    cxStyle1019: TcxStyle;
    cxStyle1020: TcxStyle;
    cxStyle1021: TcxStyle;
    cxStyle1022: TcxStyle;
    cxStyle1023: TcxStyle;
    cxStyle1024: TcxStyle;
    cxStyle1025: TcxStyle;
    cxStyle1026: TcxStyle;
    cxStyle1027: TcxStyle;
    cxStyle1028: TcxStyle;
    cxStyle1029: TcxStyle;
    cxStyle1030: TcxStyle;
    cxStyle1031: TcxStyle;
    cxStyle1032: TcxStyle;
    cxStyle1033: TcxStyle;
    cxStyle1034: TcxStyle;
    cxStyle1035: TcxStyle;
    cxStyle1036: TcxStyle;
    cxStyle1037: TcxStyle;
    cxStyle1038: TcxStyle;
    cxStyle1039: TcxStyle;
    cxStyle1040: TcxStyle;
    cxStyle1041: TcxStyle;
    cxStyle1042: TcxStyle;
    cxStyle1043: TcxStyle;
    cxStyle1044: TcxStyle;
    cxStyle1045: TcxStyle;
    cxStyle1046: TcxStyle;
    cxStyle1047: TcxStyle;
    cxStyle1048: TcxStyle;
    cxStyle1049: TcxStyle;
    cxStyle1050: TcxStyle;
    cxStyle1051: TcxStyle;
    cxStyle1052: TcxStyle;
    cxStyle1053: TcxStyle;
    cxStyle1054: TcxStyle;
    cxStyle1055: TcxStyle;
    cxStyle1056: TcxStyle;
    cxStyle1057: TcxStyle;
    cxStyle1058: TcxStyle;
    cxStyle1059: TcxStyle;
    cxStyle1060: TcxStyle;
    cxStyle1061: TcxStyle;
    cxStyle1062: TcxStyle;
    cxStyle1063: TcxStyle;
    cxStyle1064: TcxStyle;
    cxStyle1065: TcxStyle;
    cxStyle1066: TcxStyle;
    cxStyle1067: TcxStyle;
    cxStyle1068: TcxStyle;
    cxStyle1069: TcxStyle;
    cxStyle1070: TcxStyle;
    cxStyle1071: TcxStyle;
    cxStyle1072: TcxStyle;
    cxStyle1073: TcxStyle;
    cxStyle1074: TcxStyle;
    cxStyle1075: TcxStyle;
    cxStyle1076: TcxStyle;
    cxStyle1077: TcxStyle;
    cxStyle1078: TcxStyle;
    cxStyle1079: TcxStyle;
    cxStyle1080: TcxStyle;
    cxStyle1081: TcxStyle;
    cxStyle1082: TcxStyle;
    cxStyle1083: TcxStyle;
    cxStyle1084: TcxStyle;
    cxStyle1085: TcxStyle;
    cxStyle1086: TcxStyle;
    cxStyle1087: TcxStyle;
    cxStyle1088: TcxStyle;
    cxStyle1089: TcxStyle;
    cxStyle1090: TcxStyle;
    cxStyle1091: TcxStyle;
    cxStyle1092: TcxStyle;
    cxStyle1093: TcxStyle;
    cxStyle1094: TcxStyle;
    cxStyle1095: TcxStyle;
    cxStyle1096: TcxStyle;
    cxStyle1097: TcxStyle;
    cxStyle1098: TcxStyle;
    cxStyle1099: TcxStyle;
    cxStyle1100: TcxStyle;
    cxStyle1101: TcxStyle;
    cxStyle1102: TcxStyle;
    cxStyle1103: TcxStyle;
    cxStyle1104: TcxStyle;
    cxStyle1105: TcxStyle;
    cxStyle1106: TcxStyle;
    cxStyle1107: TcxStyle;
    cxStyle1108: TcxStyle;
    cxStyle1109: TcxStyle;
    cxStyle1110: TcxStyle;
    cxStyle1111: TcxStyle;
    cxStyle1112: TcxStyle;
    cxStyle1113: TcxStyle;
    cxStyle1114: TcxStyle;
    cxStyle1115: TcxStyle;
    cxStyle1116: TcxStyle;
    cxStyle1117: TcxStyle;
    cxStyle1118: TcxStyle;
    cxStyle1119: TcxStyle;
    cxStyle1120: TcxStyle;
    cxStyle1121: TcxStyle;
    cxStyle1122: TcxStyle;
    cxStyle1123: TcxStyle;
    cxStyle1124: TcxStyle;
    cxStyle1125: TcxStyle;
    cxStyle1126: TcxStyle;
    cxStyle1127: TcxStyle;
    cxStyle1128: TcxStyle;
    cxStyle1129: TcxStyle;
    cxStyle1130: TcxStyle;
    cxStyle1131: TcxStyle;
    cxStyle1132: TcxStyle;
    cxStyle1133: TcxStyle;
    cxStyle1134: TcxStyle;
    cxStyle1135: TcxStyle;
    cxStyle1136: TcxStyle;
    cxStyle1137: TcxStyle;
    cxStyle1138: TcxStyle;
    cxStyle1139: TcxStyle;
    cxStyle1140: TcxStyle;
    cxStyle1141: TcxStyle;
    cxStyle1142: TcxStyle;
    cxStyle1143: TcxStyle;
    cxStyle1144: TcxStyle;
    cxStyle1145: TcxStyle;
    cxStyle1146: TcxStyle;
    cxStyle1147: TcxStyle;
    cxStyle1148: TcxStyle;
    cxStyle1149: TcxStyle;
    cxStyle1150: TcxStyle;
    cxStyle1151: TcxStyle;
    cxStyle1152: TcxStyle;
    cxStyle1153: TcxStyle;
    cxStyle1154: TcxStyle;
    cxStyle1155: TcxStyle;
    cxStyle1156: TcxStyle;
    cxStyle1157: TcxStyle;
    cxStyle1158: TcxStyle;
    cxStyle1159: TcxStyle;
    cxStyle1160: TcxStyle;
    cxStyle1161: TcxStyle;
    cxStyle1162: TcxStyle;
    cxStyle1163: TcxStyle;
    cxStyle1164: TcxStyle;
    cxStyle1165: TcxStyle;
    cxStyle1166: TcxStyle;
    cxStyle1167: TcxStyle;
    cxStyle1168: TcxStyle;
    cxStyle1169: TcxStyle;
    cxStyle1170: TcxStyle;
    cxStyle1171: TcxStyle;
    cxStyle1172: TcxStyle;
    cxStyle1173: TcxStyle;
    cxStyle1174: TcxStyle;
    cxStyle1175: TcxStyle;
    cxStyle1176: TcxStyle;
    cxStyle1177: TcxStyle;
    cxStyle1178: TcxStyle;
    cxStyle1179: TcxStyle;
    cxStyle1180: TcxStyle;
    cxStyle1181: TcxStyle;
    cxStyle1182: TcxStyle;
    cxStyle1183: TcxStyle;
    cxStyle1184: TcxStyle;
    cxStyle1185: TcxStyle;
    cxStyle1186: TcxStyle;
    cxStyle1187: TcxStyle;
    cxStyle1188: TcxStyle;
    cxStyle1189: TcxStyle;
    cxStyle1190: TcxStyle;
    cxStyle1191: TcxStyle;
    cxStyle1192: TcxStyle;
    cxStyle1193: TcxStyle;
    cxStyle1194: TcxStyle;
    cxStyle1195: TcxStyle;
    cxStyle1196: TcxStyle;
    cxStyle1197: TcxStyle;
    cxStyle1198: TcxStyle;
    cxStyle1199: TcxStyle;
    cxStyle1200: TcxStyle;
    cxStyle1201: TcxStyle;
    cxStyle1202: TcxStyle;
    cxStyle1203: TcxStyle;
    cxStyle1204: TcxStyle;
    cxStyle1205: TcxStyle;
    cxStyle1206: TcxStyle;
    cxStyle1207: TcxStyle;
    cxStyle1208: TcxStyle;
    cxStyle1209: TcxStyle;
    cxStyle1210: TcxStyle;
    cxStyle1211: TcxStyle;
    cxStyle1212: TcxStyle;
    cxStyle1213: TcxStyle;
    cxStyle1214: TcxStyle;
    cxStyle1215: TcxStyle;
    cxStyle1216: TcxStyle;
    cxStyle1217: TcxStyle;
    cxStyle1218: TcxStyle;
    cxStyle1219: TcxStyle;
    cxStyle1220: TcxStyle;
    cxStyle1221: TcxStyle;
    cxStyle1222: TcxStyle;
    cxStyle1223: TcxStyle;
    cxStyle1224: TcxStyle;
    cxStyle1225: TcxStyle;
    cxStyle1226: TcxStyle;
    cxStyle1227: TcxStyle;
    cxStyle1228: TcxStyle;
    cxStyle1229: TcxStyle;
    cxStyle1230: TcxStyle;
    cxStyle1231: TcxStyle;
    cxStyle1232: TcxStyle;
    cxStyle1233: TcxStyle;
    cxStyle1234: TcxStyle;
    cxStyle1235: TcxStyle;
    cxStyle1236: TcxStyle;
    cxStyle1237: TcxStyle;
    cxStyle1238: TcxStyle;
    cxStyle1239: TcxStyle;
    cxStyle1240: TcxStyle;
    cxStyle1241: TcxStyle;
    cxStyle1242: TcxStyle;
    cxStyle1243: TcxStyle;
    cxStyle1244: TcxStyle;
    cxStyle1245: TcxStyle;
    cxStyle1246: TcxStyle;
    cxStyle1247: TcxStyle;
    cxStyle1248: TcxStyle;
    cxStyle1249: TcxStyle;
    cxStyle1250: TcxStyle;
    cxStyle1251: TcxStyle;
    cxStyle1252: TcxStyle;
    cxStyle1253: TcxStyle;
    cxStyle1254: TcxStyle;
    cxStyle1255: TcxStyle;
    cxStyle1256: TcxStyle;
    cxStyle1257: TcxStyle;
    cxStyle1258: TcxStyle;
    cxStyle1259: TcxStyle;
    cxStyle1260: TcxStyle;
    cxStyle1261: TcxStyle;
    cxStyle1262: TcxStyle;
    cxStyle1263: TcxStyle;
    cxStyle1264: TcxStyle;
    cxStyle1265: TcxStyle;
    cxStyle1266: TcxStyle;
    cxStyle1267: TcxStyle;
    cxStyle1268: TcxStyle;
    cxStyle1269: TcxStyle;
    cxStyle1270: TcxStyle;
    cxStyle1271: TcxStyle;
    cxStyle1272: TcxStyle;
    cxStyle1273: TcxStyle;
    cxStyle1274: TcxStyle;
    cxStyle1275: TcxStyle;
    cxStyle1276: TcxStyle;
    cxStyle1277: TcxStyle;
    cxStyle1278: TcxStyle;
    cxStyle1279: TcxStyle;
    cxStyle1280: TcxStyle;
    cxStyle1281: TcxStyle;
    cxStyle1282: TcxStyle;
    cxStyle1283: TcxStyle;
    cxStyle1284: TcxStyle;
    cxStyle1285: TcxStyle;
    cxStyle1286: TcxStyle;
    cxStyle1287: TcxStyle;
    cxStyle1288: TcxStyle;
    cxStyle1289: TcxStyle;
    cxStyle1290: TcxStyle;
    cxStyle1291: TcxStyle;
    cxStyle1292: TcxStyle;
    cxStyle1293: TcxStyle;
    cxStyle1294: TcxStyle;
    cxStyle1295: TcxStyle;
    cxStyle1296: TcxStyle;
    cxStyle1297: TcxStyle;
    cxStyle1298: TcxStyle;
    cxStyle1299: TcxStyle;
    cxStyle1300: TcxStyle;
    cxStyle1301: TcxStyle;
    cxStyle1302: TcxStyle;
    cxStyle1303: TcxStyle;
    cxStyle1304: TcxStyle;
    cxStyle1305: TcxStyle;
    cxStyle1306: TcxStyle;
    cxStyle1307: TcxStyle;
    cxStyle1308: TcxStyle;
    cxStyle1309: TcxStyle;
    cxStyle1310: TcxStyle;
    cxStyle1311: TcxStyle;
    GridBandedTableViewStyleSheetDevExpress: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetUserFormat1: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetUserFormat2: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetUserFormat3: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetUserFormat4: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetBrick: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetDesert: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetEggplant: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetLilac: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetMaple: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetMarinehighcolor: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetPlumhighcolor: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetPumpkinlarge: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetRainyDay: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetRedWhiteandBlueVGA: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetRose: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetRoselarge: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetSlate: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetSpruce: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetStormVGA: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetTealVGA: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetWheat: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetWindowsClassic: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetWindowsClassiclarge: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetWindowsStandard: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetWindowsStandardlarge: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetHighContrast1: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetHighContrast1large: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetHighContrast2: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetHighContrast2large: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetHighContrastBlack: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetHighContrastBlacklarge: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetHighContrastWhite: TcxGridBandedTableViewStyleSheet;
    GridBandedTableViewStyleSheetHighContrastWhitelarge: TcxGridBandedTableViewStyleSheet;
    cxButton2: TButton;
    cxButton3: TButton;
    CheckBox1: TCheckBox;
    cxButton4: TButton;
    cxButton5: TButton;
    cxButton6: TButton;
    CheckBox2: TCheckBox;
    ColorBox1: TColorBox;
    procedure actExitExecute(Sender: TObject);
    procedure actGridNativeStyleExecute(Sender: TObject);
    procedure actSaveToFileExecute(Sender: TObject);
    procedure actLoadFromFileExecute(Sender: TObject);
    procedure RadioGroupClick(Sender: TObject);
    procedure cbUserStyleSheetsChange(Sender: TObject);
    procedure lbPredefinedStyleSheetsClick(Sender: TObject);
    procedure actEditStyleSheetExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure miUltraFlatClick(Sender: TObject);
    procedure miStandardClick(Sender: TObject);
    procedure miFlatClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cxListBox1DblClick(Sender: TObject);
    procedure cxComboBox1PropertiesChange(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
  private
    FLinkTableView: TcxGridTableView;
    function GetCurrentStyleSheet: TcxGridTableViewStyleSheet;
    procedure CreateUserStyleSheetsList;
    procedure CreatePredefinedStyleSheetsList;
    procedure UpdateGridStyleSheets(const AStyleSheet: TcxGridTableViewStyleSheet);
    procedure ChangeVisibility(AType: Integer);
    procedure SetPredefinedStyleSheets;
    procedure SetUserDefinedStyleSheets;
    procedure ClearUserDefinedStyleSheets;
    procedure LoadUserDefinedStyleSheets(AFileName: TFileName);
    procedure SaveUserDefinedStyleSheets(AFileName: TFileName);
    procedure PopulateStyleSheetsList(const AList: TList);
    function GetcxStyleRepository1:TcxStyleRepository;
    function GetcxStyleRepository2:TcxStyleRepository;
    procedure DestroycxComboBox1;
    function GetExpressions(Sender: TObject): string;
    procedure SetLinkTableView(const Value: TcxGridTableView);
  public
    property  LinkTableView: TcxGridTableView read FLinkTableView write SetLinkTableView;
    procedure SetcxListBox1Width;
  end;


implementation


{$R *.dfm}

const
  cNone = 0;
  cPredefined = 1;
  cUserDefined = 2;
type
  PColumnData=^TColumnData;
  TColumnData=record
    ID:LongInt;
    Name    ,
    Caption:string;
    FieldName:string;
  end;

procedure TABcxGridPopupMenu_ColorSetupForm.actExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.actGridNativeStyleExecute(Sender: TObject);
begin
  TcxGrid(FLinkTableView.Control).LookAndFeel.NativeStyle := not TcxGrid(FLinkTableView.Control).LookAndFeel.NativeStyle;
  TCustomAction(Sender).Checked := TcxGrid(FLinkTableView.Control).LookAndFeel.NativeStyle;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.CreateUserStyleSheetsList;
var
  I: Integer;
begin        
  cbUserStyleSheets.Clear;
  for I := 0 to GetcxStyleRepository1.StyleSheetCount - 1 do
    cbUserStyleSheets.Items.AddObject(GetcxStyleRepository1.StyleSheets[I].Caption, GetcxStyleRepository1.StyleSheets[I]);
  cbUserStyleSheets.ItemIndex := 0;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.cxButton1Click(Sender: TObject);
var
  tempStr1 :string;
begin
  tempStr1:=GetExpressions(Sender);
  if (tempStr1<>EmptyStr)   then
  begin
    cxListBox1.Items.Add(tempStr1);
  end;
  SetcxListBox1Width;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.SetcxListBox1Width;
var
  i,k:LongInt;
begin
  k:=0;
  for i:=0 to cxListBox1.Count-1 do
  begin
    if k<Length(cxListBox1.Items[i]) then
      k:=Length(cxListBox1.Items[i]);
  end;
  SendMessage(cxListBox1.Handle, LB_SETHORIZONTALEXTENT,k*9, 0);
end;

procedure TABcxGridPopupMenu_ColorSetupForm.cxButton2Click(Sender: TObject);
begin
  if cxListBox1.ItemIndex>=0 then
    cxListBox1.Items.Delete(cxListBox1.ItemIndex);

  SetcxListBox1Width;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.cxButton3Click(Sender: TObject);
var
  tempstr1:string;
  i:LongInt;
begin
  if cxListBox1.ItemIndex>=0   then
  begin
    tempstr1:=ABGetItemValue(cxListBox1.Items[cxListBox1.ItemIndex],':','''');
    tempstr1:=trim(cxListBox1.Items[cxListBox1.ItemIndex]);
    i:= ABPos('[end]',tempstr1) ;
    if i>0 then
    begin
      Delete(tempstr1,i,MaxInt);
    end;

    i:=ABPos(ColumnColorFlag,tempstr1) ;
    if i>0 then
    begin
      Delete(tempstr1,i,Length(ColumnColorFlag));
    end;

    tempstr1:=
      ABExecScript(
        ABReplaceColumnCaption(tempstr1,
                               TcxGridTableView(FLinkTableView),
                               FLinkTableView.DataController.FocusedRowIndex
                               )
                        );
    ColorBox1.Selected:=StrToIntDef(tempstr1,0);
  end;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.cxButton4Click(Sender: TObject);
var
  tempStr1 :string;
begin
  tempStr1:=GetExpressions(Sender);
  if (tempStr1<>EmptyStr)   then
  begin
    cxListBox1.Items[cxListBox1.ItemIndex]:=tempStr1;
  end;

  SetcxListBox1Width;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.cxButton5Click(Sender: TObject);
begin
  inherited;
  ModalResult:=mrNo;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.cxButton6Click(Sender: TObject);
begin
  inherited;
  ModalResult:=mrOk;
end;

function TABcxGridPopupMenu_ColorSetupForm.GetExpressions(Sender: TObject):string;
var
  tempStr1,tempStr2,tempStr3 :string;
  tempColumn:TcxCustomGridTableItem;
begin
  result:=emptystr;
  tempStr1:=cxComboBox1.Text;
  if (tempStr1<>EmptyStr) and ((Sender=cxButton1) or ((Sender=cxButton4) and (cxListBox1.ItemIndex>=0))) then
  begin
    tempColumn:=ABGetColumnByCaption(TcxGridTableView(FLinkTableView),tempStr1);
    if Assigned(tempColumn ) then
    begin
      tempStr2:= tempColumn.DataBinding.ValueType;
      if  (ABPos(','+tempStr2+',',','+'Memo,FmtMemo,WideMemo,String,Guid,FixedChar,WideString,FixedWideChar,'+',')>0) then
      begin
        tempStr3:='if ('+QuotedStr(':'+cxComboBox1.Text)+' =='+QuotedStr(cxComboBox2.Text)+') '+
                      IntToStr(ColorBox1.Selected)+';[end] ';
      end
      else
      begin
        tempStr3:='if ('+':'+cxComboBox1.Text+' =='+cxComboBox2.Text+') '+
                      IntToStr(ColorBox1.Selected)+';[end] ';
      end;
      if CheckBox1.Checked then
        tempStr3:=ColumnColorFlag+tempStr3;
      result:=tempStr3;
    end;
  end;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.cxComboBox1PropertiesChange(Sender: TObject);
var
  i,j: LongInt;
  tempstr:string;
begin
  if (Assigned(FLinkTableView)) and (cxComboBox1.ItemIndex>=0) then
  begin
    cxComboBox2.Items.Clear;
    for I := 0 to FLinkTableView.DataController.RecordCount - 1 do
    begin
      j:=  PColumnData(cxComboBox1.Items.Objects[cxComboBox1.ItemIndex]).ID;
      j:=  TcxGridTableView(FLinkTableView).FindItemByID(J).Index;
      //�����ֶ�
      if ((FLinkTableView is TcxGridDBBandedTableView) and
          (not Assigned(TcxGridDBBandedColumn(TcxGridDBBandedTableView(FLinkTableView).Columns[j]).DataBinding.Field))) or
         ((FLinkTableView is TcxGridDBTableView) and
          (not Assigned(TcxGridDBColumn(TcxGridDBTableView(FLinkTableView).Columns[j]).DataBinding.Field))) then
      begin
        tempstr:= vartostrdef(FLinkTableView.DataController.GetDisplayText(i,j),'');
      end
      //�Ǽ����ֶ�
      else
        tempstr:= vartostrdef(FLinkTableView.DataController.Values[i,j],'');

      if cxComboBox2.Items.IndexOf(tempstr)<0 then
        cxComboBox2.Items.Add(tempstr);

      if cxComboBox2.Items.Count>100 then
         Break;
    end;
  end;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.cxListBox1DblClick(Sender: TObject);
var
  tempForm:TABMemoForm;
begin
  if cxListBox1.ItemIndex>=0 then
  begin
    tempForm := TABMemoForm.Create(nil);
    try
      tempForm.Caption:='Javascrip.';
      tempForm.Memo1.Text:=cxListBox1.Items[cxListBox1.ItemIndex];
      if tempForm.ShowModal=mrok then
      begin
        cxListBox1.Items[cxListBox1.ItemIndex]:=tempForm.Memo1.Text;
      end;
    finally
      tempForm.Free;
    end;
  end;

  SetcxListBox1Width;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.CreatePredefinedStyleSheetsList;
var
  I: Integer;
begin
  lbPredefinedStyleSheets.Clear;
  for I := 0 to GetcxStyleRepository2.StyleSheetCount - 1 do
    lbPredefinedStyleSheets.Items.AddObject(GetcxStyleRepository2.StyleSheets[I].Caption, GetcxStyleRepository2.StyleSheets[I]);
  lbPredefinedStyleSheets.ItemIndex := 0;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.UpdateGridStyleSheets(const AStyleSheet: TcxGridTableViewStyleSheet);

  procedure UpdateView(const AView: TcxGridTableView);
  begin
    AView.BeginUpdate;
    AView.Styles.StyleSheet := AStyleSheet;
    AView.EndUpdate;
  end;
var
  i:longint;
begin
{ remove/add the closing brace on this line to disable/enable the following code}

  if GetCurrentStyleSheet = AStyleSheet then
    Exit;

  if CheckBox2.Checked then
  begin
    UpdateView(TcxGridTableView(TcxGrid(FLinkTableView.Control).FocusedView));
    TcxGridTableView(TcxGrid(FLinkTableView.Control).FocusedView).DataController.ClearDetails;  // refresh detail level
  end
  else
  begin
    for i := 0 to TcxGrid(FLinkTableView.Control).ViewCount-1 do
    begin
      UpdateView(TcxGridTableView(TcxGrid(FLinkTableView.Control).Views[i]));
      TcxGridTableView(TcxGrid(FLinkTableView.Control).Views[i]).DataController.ClearDetails;  // refresh detail level
    end;
  end;


//}
end;

procedure TABcxGridPopupMenu_ColorSetupForm.actSaveToFileExecute(Sender: TObject);
begin
  if SaveDialog.Execute then
    SaveUserDefinedStyleSheets(SaveDialog.FileName);
end;

procedure TABcxGridPopupMenu_ColorSetupForm.actLoadFromFileExecute(Sender: TObject);
begin
  if OpenDialog.Execute then
    LoadUserDefinedStyleSheets(OpenDialog.FileName);
end;

procedure TABcxGridPopupMenu_ColorSetupForm.RadioGroupClick(Sender: TObject);
begin
  case RadioGroup.ItemIndex of
    cNone:
      UpdateGridStyleSheets(nil);
    cPredefined:
      SetPredefinedStyleSheets;
    cUserDefined:
      SetUserDefinedStyleSheets;
  end;
  if RadioGroup.ItemIndex >=0 then
    ChangeVisibility(RadioGroup.ItemIndex);
end;

procedure TABcxGridPopupMenu_ColorSetupForm.SetUserDefinedStyleSheets;
begin
  if cbUserStyleSheets.Items.Count > 0 then
    UpdateGridStyleSheets(TcxGridTableViewStyleSheet(cbUserStyleSheets.Items.Objects[cbUserStyleSheets.ItemIndex]));
end;

procedure TABcxGridPopupMenu_ColorSetupForm.SpeedButton1Click(Sender: TObject);
begin
  Panel1.Visible:=not Panel1.Visible;
  if panel1.Visible then
  begin
    Width:=460;
  end
  else
    Width:=157;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.SetPredefinedStyleSheets;
begin
  if lbPredefinedStyleSheets.Items.Count > 0 then
    UpdateGridStyleSheets(TcxGridTableViewStyleSheet(lbPredefinedStyleSheets.Items.Objects[lbPredefinedStyleSheets.ItemIndex]));
end;

procedure TABcxGridPopupMenu_ColorSetupForm.ChangeVisibility(AType: Integer);
begin
  cbUserStyleSheets.Enabled := AType = cUserDefined;
  gbUserDefined.Enabled := AType = cUserDefined;
  btnEdit.Enabled := AType = cUserDefined;
  btnLoad.Enabled := AType = cUserDefined;
  btnSave.Enabled := AType = cUserDefined;

  lbPredefinedStyleSheets.Enabled := AType = cPredefined;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.cbUserStyleSheetsChange(
  Sender: TObject);
begin
  SetUserDefinedStyleSheets;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.lbPredefinedStyleSheetsClick(
  Sender: TObject);
begin
  SetPredefinedStyleSheets;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.actEditStyleSheetExecute(
  Sender: TObject);
begin
{ remove/add the closing brace on this line to disable/enable the following code}

  ShowcxStyleSheetEditor(TcxGridTableViewStyleSheet(cbUserStyleSheets.Items.Objects[cbUserStyleSheets.ItemIndex]), nil);

//}
end;
                                                             
procedure TABcxGridPopupMenu_ColorSetupForm.FormShow(Sender: TObject);
begin
  RadioGroupClick(RadioGroup);
end;

function TABcxGridPopupMenu_ColorSetupForm.GetCurrentStyleSheet: TcxGridTableViewStyleSheet;
begin
  Result := TcxGridTableViewStyleSheet(TcxGridTableView(FLinkTableView).Styles.StyleSheet);
end;

procedure TABcxGridPopupMenu_ColorSetupForm.LoadUserDefinedStyleSheets(AFileName: TFileName);
begin
{ remove/add the closing brace on this line to disable/enable the following code}

  UpdateGridStyleSheets(nil);
  ClearUserDefinedStyleSheets;

  LoadStyleSheetsFromIniFile(AFileName, GetcxStyleRepository2,
    TcxGridTableViewStyleSheet);

  CreateUserStyleSheetsList;
  SetUserDefinedStyleSheets;
//}
end;

procedure TABcxGridPopupMenu_ColorSetupForm.SaveUserDefinedStyleSheets(AFileName: TFileName);
var
  AList: TList;
begin
{ remove/add the closing brace on this line to disable/enable the following code}

  AList := TList.Create;
  try
    PopulateStyleSheetsList(AList);
    SaveStyleSheetsToIniFile(AFileName, AList);
  finally
    AList.Free;
  end;

//}
end;

procedure TABcxGridPopupMenu_ColorSetupForm.ClearUserDefinedStyleSheets;
begin
  GetcxStyleRepository2.Clear;
  GetcxStyleRepository2.ClearStyleSheets;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.FormCreate(Sender: TObject);
begin
  Width:=157;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.FormDestroy(Sender: TObject);
begin
  DestroycxComboBox1;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.DestroycxComboBox1;
var
  i:LongInt;
begin
  for I := cxComboBox1.Items.Count - 1 downto 0 do
  begin
    Dispose(PColumnData(cxComboBox1.Items.Objects[i]));
  end;
  cxComboBox1.Items.Clear;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.miUltraFlatClick(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  TcxGrid(FLinkTableView.Control).LookAndFeel.Kind := lfUltraFlat;
  TMenuItem(Sender).Checked := True;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.miStandardClick(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  TcxGrid(FLinkTableView.Control).LookAndFeel.Kind := lfStandard;
  TMenuItem(Sender).Checked := True;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.miFlatClick(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  TcxGrid(FLinkTableView.Control).LookAndFeel.Kind := lfFlat;
  TMenuItem(Sender).Checked := True;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.PopulateStyleSheetsList(const AList: TList);
var
 I: Integer;
begin
  if AList <> nil then
  begin
    AList.Clear;
    for I:= 0 to GetcxStyleRepository2.StyleSheetCount - 1 do
      AList.Add(GetcxStyleRepository2.StyleSheets[I]);
  end;
end;

procedure TABcxGridPopupMenu_ColorSetupForm.SetLinkTableView(
  const Value: TcxGridTableView);
var
  i:LongInt;
  tempStr1:string;
  tempField:TField;
  tempColumnData:PColumnData;
begin
  FLinkTableView := Value;
  if Assigned(FLinkTableView) then
  begin
    RadioGroup.ItemIndex:=-1;
    lbPredefinedStyleSheets.Enabled:=false;

    CreateUserStyleSheetsList;
    CreatePredefinedStyleSheetsList;
    if RadioGroup.ItemIndex>=0 then
      SetPredefinedStyleSheets;

    cxComboBox1.Items.Clear;
    cxComboBox2.Items.Clear;
    DestroycxComboBox1;
    for I := 0 to FLinkTableView.ColumnCount - 1 do
    begin
      New(tempColumnData);
      tempColumnData.ID:= FLinkTableView.Columns[i].ID;
      tempColumnData.Name:= FLinkTableView.Columns[i].Name;
      tempColumnData.Caption:= FLinkTableView.Columns[i].Caption;

      tempStr1:=FLinkTableView.Columns[i].Caption;
      tempField:=ABGetColumnField(FLinkTableView.Columns[i]);
      if (Assigned(tempField))   then
        tempColumnData.FieldName:= tempField.FieldName;

      cxComboBox1.Items.AddObject(tempColumnData.Caption,Pointer(tempColumnData));
    end;
  end;
end;

function TABcxGridPopupMenu_ColorSetupForm.GetcxStyleRepository1: TcxStyleRepository;
begin
  result:=nil;
  if FLinkTableView is TcxGridDBTableView then
  begin
    result:=cxStyleRepository1;
  end
  else if FLinkTableView is TcxGridDBBandedTableView then
  begin
    result:=cxStyleRepository3;
  end;
end;

function TABcxGridPopupMenu_ColorSetupForm.GetcxStyleRepository2: TcxStyleRepository;
begin
  result:=nil;
  if  FLinkTableView is TcxGridDBTableView  then
  begin
    result:=cxStyleRepository2;
  end
  else if  FLinkTableView is TcxGridDBBandedTableView  then
  begin
    result:=cxStyleRepository4;
  end;
end;

end.
