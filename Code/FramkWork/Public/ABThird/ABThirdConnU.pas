{
连接单元
两层或三层兼容相关
}
unit ABThirdConnU;

interface
{$I ..\ABInclude.ini}
uses
  ABPubConstU,
  ABPubFuncU,
  ABPubVarU,
  ABPubUserU,
  ABPubDBU,
  ABPubLogU,
  ABPubLocalParamsU,
  ABPubSQLLogU,

  ABThirdDBU,
  ABThirdConnDatabaseU,
  ABThirdConnServerU,

  ADODB,
  DB,DBClient,
  FireDAC.Comp.Client,
  Dialogs,
  Variants, DateUtils,SysUtils,Classes;



//过程内有两层或三层分别处理
//开启事务
procedure ABBegTransaction(aConnName:string);
//撤消事务
procedure ABRollbackTransaction(aConnName:string);
//提交事务
procedure ABCommitTransaction(aConnName:string);

//设置连接控件的超时
procedure ABSetConnTimeout(aConnName: string;aTimeout:LongInt);
//通过连接名称得到数据库名称
function ABGetDatabaseByConnName(aConnName:String):String;

//初始化连接
function ABInitConn(aDoAbort:Boolean=False):boolean;
//检测连接名称是否连接
function ABIsConn(aConnName:string):Boolean; overload;
function ABIsConn:Boolean; overload;
//得到连接名称列表
function ABGetConnStrings:TStrings;
//根据连接名称设置数据集的连接
function ABSetDatasetConnByConnName(aDataset:TDataset;aConnName:String):boolean;
function ABSetDatasetConn(aDataset:TDataset;aConn:TCustomConnection):boolean;
//根据连接名称从连接列表中取得连接
function ABGetConnByConnName(aConnName:String):TCustomConnection;

type
  TABAfterConnNotifyEvent = procedure();
var
  ABAfterConnProc: TABAfterConnNotifyEvent;


implementation
var
  FFireDACConnection: TFireDACConnection;

function ABGetConnStrings:TStrings;
begin
  result:=nil;
  if ABLocalParams.LoginType=ltCS_Two then
  begin
    result:=ABThirdConnDatabaseU.ABGetConnStrings;
  end
  else if ABLocalParams.LoginType=ltCS_Three then
  begin
    result:=ABThirdConnServerU.ABGetConnStrings;
  end;
end;

procedure ABBegTransaction(aConnName:string);
var
  tempConn:TCustomConnection;
begin
  if ABLocalParams.LoginType=ltCS_Two then
  begin
    tempConn:=ABGetConnInfoByConnName(aConnName).Conn;
    if Assigned(tempConn) then
    begin
      if tempConn is TFireDACConnection then
      begin
        TFireDACConnection(tempConn).StartTransaction;
      end
      else if tempConn is TADOConnection then
      begin
        TADOConnection(tempConn).BeginTrans;
      end;
    end;
  end
  else if ABLocalParams.LoginType=ltCS_Three then
  begin
    ABGetServerClient.StartTransaction(aConnName);
  end;
end;

procedure ABCommitTransaction(aConnName:string);
var
  tempConn:TCustomConnection;
begin
  if ABLocalParams.LoginType=ltCS_Two then
  begin
    tempConn:=ABGetConnInfoByConnName(aConnName).Conn;
    if Assigned(tempConn) then
    begin
      if tempConn is TFireDACConnection then
      begin
        TFireDACConnection(tempConn).Commit;
      end
      else if tempConn is TADOConnection then
      begin
        TADOConnection(tempConn).CommitTrans;
      end;
    end;
  end
  else if ABLocalParams.LoginType=ltCS_Three then
  begin
    ABGetServerClient.Commit(aConnName);
  end;
end;

procedure ABRollbackTransaction(aConnName:string);
var
  tempConn:TCustomConnection;
begin
  if ABLocalParams.LoginType=ltCS_Two then
  begin
    tempConn:=ABGetConnInfoByConnName(aConnName).Conn;
    if Assigned(tempConn) then
    begin
      if tempConn is TFireDACConnection then
      begin
        TFireDACConnection(tempConn).Rollback;
      end
      else if tempConn is TADOConnection then
      begin
        TADOConnection(tempConn).RollbackTrans;
      end;
    end;
  end
  else if ABLocalParams.LoginType=ltCS_Three then
  begin
    ABGetServerClient.Rollback(aConnName);
  end;
end;

procedure ABSetConnTimeout(aConnName: string;aTimeout:LongInt);
var
  tempConn:TCustomConnection;
begin
  if ABLocalParams.LoginType=ltCS_Two then
  begin
    tempConn:=ABGetConnInfoByConnName(aConnName).Conn;
    if Assigned(tempConn) then
    begin
      if tempConn is TFireDACConnection then
      begin
        ABWriteItemInStrings('LoginTimeout',inttostr(aTimeout),TFireDACConnection(tempConn).Params);
      end
      else if tempConn is TADOConnection then
      begin
      end;
    end;
  end
  else if ABLocalParams.LoginType=ltCS_Three then
  begin
    ABGetServerClient.SetTimeout(aConnName,aTimeout);
  end;
end;

function ABGetConnByConnName(aConnName:String):TCustomConnection;
begin
  result:=nil;
  if ABInitConn then
  begin
    if (ABLocalParams.LoginType=ltCS_Two) then
    begin
      result:=ABThirdConnDatabaseU.ABGetConnByConnName(aConnName);
    end
    else if ABLocalParams.LoginType=ltCS_Three then
    begin
      result:=FFireDACConnection;
    end;
  end;
end;

function ABSetDatasetConnByConnName(aDataset:TDataset;aConnName:String):boolean;
begin
  result:=false;
  if ABInitConn then
  begin
    if (ABLocalParams.LoginType=ltCS_Two)  then
    begin
      result:=ABThirdConnDatabaseU.ABSetDatasetConnByConnName(aDataset,aConnName);
    end
    else if ABLocalParams.LoginType=ltCS_Three then
    begin
      //三层下不设置Connection而直接LoadFromStream会报错
      TFireDACQuery(aDataset).Connection:=FFireDACConnection;
      result:=true;
    end;
  end;
end;

function ABSetDatasetConn(aDataset:TDataset;aConn:TCustomConnection):boolean;
begin
  result:=false;
  if Assigned(aConn) then
  begin
    if (aDataset is TFireDACQuery) then
    begin
      TFireDACQuery(aDataset).Connection:=TFireDACConnection(aConn);
      result:=true;
    end
    else if (aDataset is TADOQuery) then
    begin
      TADOQuery(aDataset).Connection:=TADOConnection(aConn);
      result:=true;
    end;
  end;
end;

function ABInitConn(aDoAbort:Boolean):boolean;
var
  tempDo:boolean;
begin
  tempDo:=false;
  result:=true;
  if (not ABIsConn)  then
  begin
    if ABLocalParams.LoginType=ltCS_Two then
    begin
      tempDo:=true;
      Result:=ABThirdConnDatabaseU.ABSetConn(False);
    end
    else if ABLocalParams.LoginType=ltCS_Three then
    begin
      tempDo:=true;
      Result:=ABThirdConnServerU.ABSetConn(False);
    end;
  end;

  if (tempDo) then
  begin
    if Result then
    begin
      if Assigned(ABAfterConnProc) then
        ABAfterConnProc;
    end
    else
    begin
      if (aDoAbort) then
        Abort;
    end;
  end;
end;

function ABGetDatabaseByConnName(aConnName:String):String;
begin
  if (aConnName=EmptyStr) then
    aConnName:='Main';

  Result:= ABThirdConnU.ABGetConnStrings.Values[aConnName];
end;

function ABIsConn:Boolean;
begin
  Result:=(ABThirdConnU.ABGetConnStrings.Count>0);
end;

function ABIsConn(aConnName:string):Boolean;
var
  i:LongInt;
  tempStrings:TStrings;
begin
  Result:=false;
  tempStrings:=ABThirdConnU.ABGetConnStrings;
  for I := 0 to tempStrings.Count-1 do
  begin
    if (aConnName=EmptyStr) or
       (AnsiCompareText(aConnName,tempStrings.Names[i])=0) then
    begin
      Result:=true;
      Break;
    end;
  end;
end;

procedure ABFinalization;
begin
  FFireDACConnection.Free;
end;

procedure ABInitialization;
begin
  FFireDACConnection:=TFireDACConnection.Create(nil);
  FFireDACConnection.DriverName:='MSSQL';
end;

Initialization
  ABInitialization;
Finalization
  ABFinalization;


end.
