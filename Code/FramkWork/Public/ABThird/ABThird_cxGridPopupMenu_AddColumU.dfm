object ABcxGridPopupMenu_AddColumForm: TABcxGridPopupMenu_AddColumForm
  Left = 339
  Top = 314
  ClientHeight = 234
  ClientWidth = 514
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 197
    Width = 514
    Height = 37
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      514
      37)
    object Label2: TLabel
      Left = 4
      Top = 11
      Width = 29
      Height = 13
      AutoSize = False
      Caption = #21015#21517
      Transparent = True
    end
    object Label4: TLabel
      Left = 113
      Top = 11
      Width = 31
      Height = 13
      AutoSize = False
      Caption = #31867#22411
      Transparent = True
    end
    object Button1: TButton
      Left = 432
      Top = 6
      Width = 66
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #21462#28040
      TabOrder = 2
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 342
      Top = 6
      Width = 69
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #30830#23450
      TabOrder = 1
      OnClick = Button2Click
    end
    object Edit1: TEdit
      Left = 35
      Top = 7
      Width = 72
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
    end
    object ComboBox1: TComboBox
      Left = 144
      Top = 7
      Width = 96
      Height = 21
      TabOrder = 3
      Text = 'String'
      Items.Strings = (
        'String'
        'Integer'
        'Float'
        'Boolean'
        'DateTime')
    end
    object cxButton7: TButton
      Left = 292
      Top = 6
      Width = 42
      Height = 25
      Anchors = [akTop, akRight]
      Caption = 'Help'
      TabOrder = 4
      OnClick = cxButton7Click
    end
    object cxButton1: TButton
      Left = 246
      Top = 6
      Width = 42
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #27979#35797
      TabOrder = 5
      OnClick = cxButton1Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 294
    Height = 197
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 0
    object Label3: TLabel
      Left = 1
      Top = 1
      Width = 292
      Height = 13
      Align = alTop
      Caption = #21015#30340#35745#31639#20844#24335'('#22914':'#23383#27573#33853'+:'#23383#27573'2)'
      Transparent = True
      ExplicitWidth = 168
    end
    object Memo1: TMemo
      Left = 1
      Top = 14
      Width = 292
      Height = 182
      Align = alClient
      TabOrder = 0
    end
  end
  object Panel3: TPanel
    Left = 294
    Top = 0
    Width = 220
    Height = 197
    Align = alRight
    Caption = 'Panel3'
    TabOrder = 2
    object Label1: TLabel
      Left = 1
      Top = 1
      Width = 218
      Height = 13
      Align = alTop
      Caption = #21487#29992#30340#23383#27573#21517'('#21452#20987#22686#21152#21040#20844#24335')'
      Transparent = True
      ExplicitWidth = 162
    end
    object ListBox1: TListBox
      Left = 1
      Top = 14
      Width = 218
      Height = 137
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ItemHeight = 14
      ParentFont = False
      TabOrder = 0
      OnDblClick = ListBox1DblClick
    end
    object Panel4: TPanel
      Left = 1
      Top = 151
      Width = 218
      Height = 45
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object speedButton1: TSpeedButton
        Left = 1
        Top = 1
        Width = 43
        Height = 21
        Caption = '+'
        Flat = True
        OnClick = speedButton1Click
      end
      object speedButton2: TSpeedButton
        Left = 44
        Top = 1
        Width = 43
        Height = 21
        Caption = '-'
        Flat = True
        OnClick = speedButton1Click
      end
      object speedButton3: TSpeedButton
        Left = 87
        Top = 1
        Width = 43
        Height = 21
        Caption = '*'
        Flat = True
        OnClick = speedButton1Click
      end
      object speedButton4: TSpeedButton
        Left = 130
        Top = 1
        Width = 43
        Height = 21
        Caption = '/'
        Flat = True
        OnClick = speedButton1Click
      end
      object SpeedButton5: TSpeedButton
        Left = 44
        Top = 22
        Width = 43
        Height = 21
        Hint = 'ABGetAllPY'
        Caption = #20840#25340
        Flat = True
        OnClick = SpeedButton6Click
      end
      object SpeedButton6: TSpeedButton
        Left = 1
        Top = 22
        Width = 43
        Height = 21
        Hint = 'ABGetFirstPY'
        Caption = #31616#25340
        Flat = True
        OnClick = SpeedButton6Click
      end
      object SpeedButton7: TSpeedButton
        Left = 87
        Top = 22
        Width = 43
        Height = 21
        Hint = 'ABGetFirstWB'
        Caption = #20116#31508#31616
        Flat = True
        OnClick = SpeedButton6Click
      end
      object SpeedButton8: TSpeedButton
        Left = 130
        Top = 22
        Width = 43
        Height = 21
        Hint = 'ABGetAllWB'
        Caption = #20116#31508#20840
        Flat = True
        OnClick = SpeedButton6Click
      end
    end
  end
end
