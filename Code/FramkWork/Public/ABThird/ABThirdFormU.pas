{
共用窗体单元,
关闭时默认释放且从注册文件列表中删除
关闭时默认释放,不可show、SHOWMODEL后再处理数据再释放,如果需要show、SHOWMODEL后再处理数据要设置 FCloseAction:=caHide;
}
unit ABThirdFormU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubRegisterFileU,
  ABPubUserU,
  ABPubFormU,
  ABPubFuncU,
  ABPubDBU,

  DB,Controls,Forms,SysUtils,Classes,Windows,Messages;
  
const
  //功能模块释放的消息
  ABThirdFormFree_Msg = WM_User + 110;

type
  TABThirdForm = class(TABPubForm)
  private
    { Private declarations }
  protected
    procedure DoCreate; override;
    procedure DoDestroy; override;
    procedure DoShow; override;
    procedure DoClose(var Action: TCloseAction); override;
    procedure Loaded; override;
     { Private declarations }
  public
    function CloseQuery:Boolean; override;
    constructor Create(AOwner: TComponent);override;
    destructor Destroy; override;
    { Public declarations }
  published
  end;
                         


implementation

{$R *.dfm}

{ TABPubForm }

function TABThirdForm.CloseQuery: Boolean;
begin
  Result:=inherited CloseQuery;
end;
                                                                               
procedure TABThirdForm.DoCreate;
begin
  inherited;
end;

procedure TABThirdForm.DoDestroy;
begin
  inherited;
end;

procedure TABThirdForm.DoShow;
begin
  inherited;
end;

procedure TABThirdForm.Loaded;
begin
  inherited;
end;

constructor TABThirdForm.Create(AOwner: TComponent);
begin
  inherited;
  CloseAction:=caFree;
end;

destructor TABThirdForm.Destroy;
begin
  inherited;
  ABRegisterFile.SetFreeFlag(name);

  if Assigned(Application.MainForm) then
    postMessage(Application.MainForm.Handle,ABThirdFormFree_Msg,0,0);
end;

procedure TABThirdForm.DoClose(var Action: TCloseAction);
begin
  inherited;
end;


end.
