{
框架审批单元
}
unit ABFramkWorkApproveU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubUserU,
  ABPubMessageU,
  ABPubVarU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubFormU,

  ABThirdQueryU,
  ABThirdCacheDatasetU,
  ABThirdConnU,
  ABThirdConnServerU,
  ABThirdDBU,
  ABThirdCustomQueryU,

  ABFramkWorkConstU,
  ABFramkWorkVarU,
  ABFramkWorkFuncU,

  DB,SysUtils,Variants,Classes,Forms;

//单据撤消审批
procedure ABApprovedRollback(aBillCode:string;aDataset:TABThirdQuery;aGuidFieldName,aStateFieldName:string);
//单据审批
procedure ABApprovedCommit(aBillCode:string;aDataset:TABThirdQuery;aGuidFieldName,aStateFieldName:string);

implementation

procedure ABApprovedCommit(aBillCode:string;aDataset:TABThirdQuery;aGuidFieldName,aStateFieldName:string);
var
  tempIsApprove:Boolean;
begin
  if (not aDataset.IsEmpty) then
  begin
    tempIsApprove:=ABGetSQLValue( 'Main',
                               ' declare @tempIsApprove bit '+ABEnterWrapStr+
                               ' exec Proc_ApprovedBill '+ABEnterWrapStr+
                               '                        '+QuotedStr(aBillCode)+','+ABEnterWrapStr+
                               '                        '+QuotedStr(aDataset.FieldByName(aGuidFieldName).AsString)+','+ABEnterWrapStr+
                               '                         @tempIsApprove output '+ABEnterWrapStr+
                               ' select @tempIsApprove ',[],0);
    if not tempIsApprove then
    begin
      ABSetFieldValue('Finish',aDataset.FieldByName(aStateFieldName),true);
    end;
    aDataset.OnlyDatasetDelete;
  end;
end;

procedure ABApprovedRollback(aBillCode:string;aDataset:TABThirdQuery;aGuidFieldName,aStateFieldName:string);
var
  tempOutMsg:string;
begin
  if (not aDataset.IsEmpty) then
  begin
    tempOutMsg:=ABGetSQLValue( 'Main',
                   ' declare @tempOutMsg NVarchar(1000) '+ABEnterWrapStr+
                   ' exec Proc_CheckCancelBill '+ABEnterWrapStr+
                   '                        '+QuotedStr(aBillCode)+','+  ABEnterWrapStr+
                   '                        '+QuotedStr(aDataset.FieldByName(aGuidFieldName).AsString)+','+ABEnterWrapStr+
                   '                         @tempOutMsg output '+ABEnterWrapStr+
                   ' select isnull(@tempOutMsg,'''') ',[],'');
    if tempOutMsg<>EmptyStr then
    begin
      ABShow(tempOutMsg);
    end
    else
    begin
      ABSetFieldValue('Input',aDataset.FieldByName(aStateFieldName),true);
      aDataset.OnlyDatasetDelete;
    end;
  end;
end;

end.
