{
框架登录用戶单元
}
unit ABFramkWorkUserU;

interface
{$I ..\ABInclude.ini}

uses
  abpubDBU,
  abpubVarU,
  abpubMessageU,
  ABPubPassU,
  abPubUserU,
  ABPubManualProgressBarU,
  ABPubFuncU,
  ABPubConstU,
  ABPubEditPassWordU,
  ABPubLoginU,
  ABPubLocalParamsU,
  ABPubLogU,

  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdQueryU,

  ABFramkWorkFuncU,

  SysUtils,Variants,Classes,Controls,db,StdConvs,DBClient,DateUtils;

type
  TLongType=(ltLongin,ltChangUser,ltAfreshLogin,ltLock);

type
  TABUser=class
  private
    FFuncTreeDataSet: TClientDataSet;
    FOperatorDataSet: TDataSet;
    FOperatorKeyDataSet: TDataSet;
    FOperatorKeyTableDataSet: TDataSet;
    FOperatorKeyFieldDataSet: TDataSet;
    FOperatorKeyExtendReportDataSet: TDataSet;
    FOperatorKeyFunctionDataSet: TDataSet;
    FOperatorKeyCustomObjectDataSet: TDataSet;
    FOperatorKeyTableRowQueryDataSet: TDataSet;

    procedure FillFuncTreeDataSet;
    function GetOperatorDataSet: TDataSet;
    function GetOperatorKeyDataSet: TDataSet;
    function GetOperatorKeyExtendReportDataSet: TDataSet;
    function GetOperatorKeyFieldDataSet: TDataSet;
    function GetOperatorKeyFunctionDataSet: TDataSet;
    function GetOperatorKeyTableDataSet: TDataSet;
    function GetOperatorKeyCustomObjectDataSet: TDataSet;
    function GetOperatorKeyTableRowQueryDataSet: TDataSet;
    { Private declarations }
  protected
    //刷新用户相关的数据集
    procedure RefreshUserDataset;
    //在登入时做一些事情
    procedure DoingOnLogin;
    //在登出时做一些事情
    procedure DoingOnLoginOut;
    { Protected declarations }
  public
    constructor Create;
    destructor Destroy; override;

  public
    //登入
    function Login(aType:TLongType):boolean;
    //登出                   
    procedure LoginOut;

    //修改密码
    function EditPassWord:boolean;
    //检测用户名与对应的密码是否正确
    function LoginCheck(aCode, aPassword: string;aShowMessage:boolean=true): boolean;

    //功能菜单的树形结构内存数据集
    property FuncTreeDataSet :TClientDataSet read FFuncTreeDataSet;

    //当前操作员数据集
    property OperatorDataSet :TDataSet read GetOperatorDataSet;
    //当前操作员具有的角色数据集
    property OperatorKeyDataSet :TDataSet read GetOperatorKeyDataSet;
    //当前操作员具有的角色下功能权限数据集
    property OperatorKeyFunctionDataSet :TDataSet read GetOperatorKeyFunctionDataSet;
    //当前操作员具有的角色下报表权限数据集
    property OperatorKeyExtendReportDataSet :TDataSet read GetOperatorKeyExtendReportDataSet;
    //当前操作员具有的角色下定制对象权限数据集
    property OperatorKeyCustomObjectDataSet :TDataSet read GetOperatorKeyCustomObjectDataSet;
    //当前操作员具有的角色下表权限数据集
    property OperatorKeyTableDataSet :TDataSet read GetOperatorKeyTableDataSet;
    //当前操作员具有的角色下表行查询权限数据集
    property OperatorKeyTableRowQueryDataSet :TDataSet read GetOperatorKeyTableRowQueryDataSet;
    //当前操作员具有的角色下字段权限数据集
    property OperatorKeyFieldDataSet :TDataSet read GetOperatorKeyFieldDataSet;

    //检测本地授权文件License.key
    function CheckClientLicenseFile: boolean;
    //检测服务器授权文件License.key
    function CheckServerLicenseFile: boolean;
    //检测客户端加密狗
    function CheckClientUsbDog: boolean;
    { Public declarations }
  end;

//检测传入的功能点编号是否在当前操作员角色的授权内
function ABCheckFuncPoint(aFuncPointCodes:array of string):Boolean;

var
  ABUser:TABUser;

implementation

function ABCheckFuncPoint(aFuncPointCodes:array of string):Boolean;
var
  i:LongInt;
begin
  result:=False ;
  if (ABPubUser.IsAdminOrSysuser) or
     (High(aFuncPointCodes)<0) then
  begin
    result:=True ;
    exit;
  end;

  for I := Low(aFuncPointCodes) to High(aFuncPointCodes) do
  begin
    if ABUser.OperatorKeyCustomObjectDataSet.Locate('Ti_Code',aFuncPointCodes[i],[]) then
    begin
      result:=True ;
      Break;
    end;
  end;
end;

{ TABUser }
function TABUser.CheckClientLicenseFile: boolean;
var
  tempLinkClientCount,
  tempSubsequentClientCount:longint;
begin
  Result:=true;
  if ABPubUser.IsNoCheckHost then
  begin

  end
  //检测此运行是否符合本地的授权文件License.key
  else if not ABCheckLicense(ABPublicSetPath+'License.key',ABGetFieldValue(ABGetConstSqlPubDataset('ABSys_Org_Parameter',[]),['Pa_Name'],['RegName'],['Pa_Value'],''),
                        now(),tempSubsequentClientCount) then
  begin
    ABWriteInI(['Cur'],['AutoRun'],['False'],ABUserFile);
    Result:=false;
    exit;
  end
  else
  begin
    //检测客户端数量
    if (tempSubsequentClientCount>0) then
    begin
      //已经连上的客户端的数量
      tempLinkClientCount:=
          StrToInt(
            vartostrdef(ABGetSQLValue('Main',
              ' SELECT count(*)                                                                 '+
              ' FROM ABSys_Log_CurLink                                                          '+
              ' WHERE (ABS(DATEDIFF([minute], getdate(), CL_HeartbeatDatetime))<=                '+
              '       (isnull((SELECT Pa_Value FROM ABSys_Org_Parameter where Pa_Name=''Heartbeat''),20)*3))',[],0),'0'));
      if (tempLinkClientCount+1>tempSubsequentClientCount) then
      begin
        ABShow('系统只能连接[%s]个客户端,请联系软件供应商.',[tempSubsequentClientCount]);
        Result:=false;
        exit;
      end;
    end;
  end;
end;

function TABUser.CheckServerLicenseFile: boolean;
var
  tempOutMsg:string;
begin
  Result:=true;
  if ABPubUser.IsNoCheckHost then
  begin

  end
  else
  begin
    //调用过程Proc_CheckLicense，此过程中可在数据库服务器端检测授权的情况，
    //当不满足授权要求时返回描述的字串，为空时表示此客户端通过服务器的授权检测
    tempOutMsg:=ABGetSQLValue('Main',
       ' declare @tempOutMsg NVarchar(100) '+
       //Proc_CheckLicense 参数
       ' exec Proc_CheckServiceLicenseFile '+
       '                        '+QuotedStr(ABPubUser.HostName)+','+
       '                        '+QuotedStr(ABPubUser.HostIP)+','+
       '                        '+QuotedStr(ABPubUser.MacAddress)+','+
       '                        '+QuotedStr(ABPubUser.SPID)+','+
       '                        '+IntToStr(ABBoolToInt(ABPubUser.UseFuncRight))+','+
       '                        '+QuotedStr(ABPubUser.FuncRight)+','+
       '                        '+QuotedStr(ABPubUser.CustomFunc)+','+
       '                         @tempOutMsg output '+
       ' select isnull(@tempOutMsg,'''') ',[],'');

    if tempOutMsg<>emptystr then
    begin
      Result:=false;
      ABShow(tempOutMsg);
    end;

  end;
end;

constructor TABUser.Create;
begin
  FFuncTreeDataSet:=ABCreateClientDataSet(
   ['Fu_Ti_Guid' ,'Fu_Guid','Fu_Name','Fu_FileName','Fu_Order','Fu_IsBeginGroup','Fu_Code','IsFunc','Level'],
   [],
   [ftString     ,ftString ,ftString ,ftString     , ftInteger, ftBoolean       ,ftString      , ftBoolean, ftInteger],
   [100          ,100      ,100      ,100          ,100       ,100              ,100           ,100       ,100       ]
                                 );
  FFuncTreeDataSet.IndexDefs.Clear;
  FFuncTreeDataSet.AddIndex('IndexName1', 'Level;Fu_Ti_Guid;Fu_Order',[], '','',0);
  FFuncTreeDataSet.IndexName:= 'IndexName1';
end;

destructor TABUser.Destroy;
begin
  if (Assigned(FOperatorDataSet)) and
     (FOperatorDataSet.Active) and
     (not FOperatorDataSet.IsEmpty)  then
    LoginOut;

  FFuncTreeDataSet.Free;

  if Assigned(FOperatorKeyTableDataSet        ) then FOperatorKeyTableDataSet        .Free;
  if Assigned(FOperatorKeyFieldDataSet        ) then FOperatorKeyFieldDataSet        .Free;
  if Assigned(FOperatorKeyFunctionDataSet     ) then FOperatorKeyFunctionDataSet     .Free;
  if Assigned(FOperatorKeyExtendReportDataSet ) then FOperatorKeyExtendReportDataSet .Free;
  if Assigned(FOperatorKeyCustomObjectDataSet ) then FOperatorKeyCustomObjectDataSet .Free;
  if Assigned(FOperatorKeyTableRowQueryDataSet) then FOperatorKeyTableRowQueryDataSet.Free;
  if Assigned(FOperatorKeyDataSet             ) then FOperatorKeyDataSet             .Free;
  if Assigned(FOperatorDataSet                ) then FOperatorDataSet                .Free;
  inherited;
end;

function TABUser.EditPassWord: boolean;
var
  tempOP_PassWordEditDay:longint;
  tempNewPass:string;
begin
  result:=false;
  if ABEditPassWord(OperatorDataSet.FindField('Op_PassWord').AsString,tempNewPass) then
  begin
    tempOP_PassWordEditDay:=OperatorDataSet.FindField('OP_PassWordEditDay').AsInteger;
    if tempOP_PassWordEditDay<=0 then
      tempOP_PassWordEditDay:=100;

    ABExecSQL('Main',
     ' update ABSys_Org_Operator '+
     ' set Op_PassWord=' +QuotedStr(ABDoPassword(tempNewPass))+','+
     '     OP_PassWordEndDateTime='+
     '      case when OP_PassWordEndDateTime is null then null '+
     '           else OP_PassWordEndDateTime+'+inttostr(tempOP_PassWordEditDay)+
     '      end '+
     ' where Op_Code='+QuotedStr(ABPubUser.Code)+' or Op_Name='+QuotedStr(ABPubUser.Code));
    OperatorDataSet.Close;
    OperatorDataSet.Open;

    ABPubUser.EncryptPassWord :=OperatorDataSet.FindField('Op_PassWord').AsString;
    ABPubUser.PassWord:=tempNewPass;
    if AnsiCompareText(ABReadInI('Cur','AutoRun',ABUserFile),'True')=0  then
    begin
      ABWriteInI(['Cur'],['User'],[ABPubUser.Code],ABUserFile);
      ABWriteInI(['Cur'],['PassWord'],[ABPubUser.EncryptPassWord],ABUserFile);
    end;
    result:=True;
  end;
end;

//根据操作员CODE得到功能表的DataSet
procedure TABUser.FillFuncTreeDataSet;
var
  tempDatasetTree,
  tempDatasetFunction,
  tempCreateDatasetFunction:TDataSet;
  tempPubFu_Ti_Guid:string;
  tempIsAdminOrSysuser:boolean;
  tempKeyItem :string;
  tempFu_BitmapList:Tstrings;
  tempOtherWhere:string;
  tempLevel:LongInt;
  tempStrings:TStrings;
begin
  tempIsAdminOrSysuser:=False;
  FFuncTreeDataSet.EmptyDataSet;

  tempStrings:=TStringList.Create;
  tempCreateDatasetFunction:=nil;
  tempDatasetFunction:=nil;                      
  tempDatasetTree:=ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['Function Dir']);
  ABSetDatasetFilter(tempDatasetTree,' Ti_Code<>'+QuotedStr('Public')+' and '+ABGetBooleanFilter(tempDatasetTree,'TI_bit1',True));
  try
    tempOtherWhere:=ABIIF(ABPubUser.UseFuncRight,' and Fu_FileName in (select tempFilename from dbo.Func_GetFuncFileNameList('+QuotedStr(ABPubUser.FuncRight)+'))','');
    if ABPubUser.IsAdminOrSysuser then     
    begin                                                                       
      tempIsAdminOrSysuser:=true;
      if ABPubUser.IsAdmin then
      begin
        tempDatasetFunction:= ABGetDataset('Main',
                                  ' SELECT Fu_Bitmap,Fu_FileName,Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_Order,Fu_IsBeginGroup,Fu_Code  '+
                                  ' FROM ABSys_Org_Function '+
                                  ' where Fu_IsView=1 ' ,[],nil,true);
      end
      else
      begin
        tempDatasetFunction:= ABGetDataset('Main',
                                  ' SELECT Fu_Bitmap,Fu_FileName,Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_Order,Fu_IsBeginGroup,Fu_Code  '+
                                  ' FROM ABSys_Org_Function '+
                                  ' where Fu_SysUserUse=1 and Fu_IsView=1  '+tempOtherWhere,[],nil,true);
      end;
      tempCreateDatasetFunction:=tempDatasetFunction;
    end
    else
    begin
      tempDatasetFunction:= ABGetDataset('Main',
                                ' SELECT Fu_Bitmap,Fu_FileName,Fu_Ti_Guid,Fu_Guid,Fu_Name,Fu_Order,Fu_IsBeginGroup,Fu_Code  '+
                                ' FROM ABSys_Org_Function '+
                                ' where Fu_IsView=1 '+tempOtherWhere ,[],nil,true);
      //取消这一段注释则"帮助与其它“的菜单项目对所有用户开放
      tempPubFu_Ti_Guid:=emptystr;
      {
      if tempDatasetTree.Locate('Ti_Code','Help Other',[]) then
      begin
        tempPubFu_Ti_Guid:=tempDatasetTree.FindField('Ti_Guid').AsString;
        if tempDatasetTree.Locate('Ti_Code','Tools',[]) then
        begin
          tempPubFu_Ti_Guid:=tempPubFu_Ti_Guid+','+tempDatasetTree.FindField('Ti_Guid').AsString;
        end;
      end;
        }
      tempCreateDatasetFunction:=
        ABCreateClientDataSet(
         ['Fu_Ti_Guid' ,'Fu_Guid','Fu_Name','Fu_FileName','Fu_Order','Fu_IsBeginGroup','Fu_Code','IsFunc'],
         [],
         [ftString     ,ftString ,ftString ,ftString     , ftInteger, ftBoolean       ,ftString      , ftBoolean],
         [100          ,100      ,100      ,100          ,100       ,100              ,100           ,100]
                                   );
      //tempCreateDatasetFunction排序
      TClientDataSet(tempCreateDatasetFunction).IndexDefs.Clear;
      TClientDataSet(tempCreateDatasetFunction).AddIndex('IndexName1', 'Fu_Ti_Guid;Fu_Order',[], '','',0);
      TClientDataSet(tempCreateDatasetFunction).IndexName:= 'IndexName1';
      //增加当前用户有权限的菜单
      OperatorKeyFunctionDataSet.First;
      while not OperatorKeyFunctionDataSet.Eof do
      begin
        tempKeyItem:=OperatorKeyFunctionDataSet.FindField('KF_Fu_Guid').AsString;
        if (tempDatasetFunction.Locate('Fu_Guid',tempKeyItem,[])) and
           (not tempCreateDatasetFunction.Locate('Fu_Guid',tempKeyItem,[]))  then
        begin
          ABSetFieldValue(
                      tempDatasetFunction,['Fu_Ti_Guid' ,'Fu_Guid','Fu_Name','Fu_FileName','Fu_Order','Fu_IsBeginGroup','Fu_Code'],
                      tempCreateDatasetFunction,['Fu_Ti_Guid' ,'Fu_Guid','Fu_Name','Fu_FileName','Fu_Order','Fu_IsBeginGroup','Fu_Code'],
                      true,true
                      );
        end;


        OperatorKeyFunctionDataSet.Next;
      end;

      if tempPubFu_Ti_Guid<>emptystr then
      begin
        //增加tempPubFu_Ti_Guid的子菜单
        tempDatasetFunction.First;
        while not tempDatasetFunction.Eof do
        begin
          if (ABPos(','+tempDatasetFunction.FindField('Fu_Ti_Guid').AsString+',',','+tempPubFu_Ti_Guid+',')>0) and
             (not tempCreateDatasetFunction.Locate('Fu_Guid',tempDatasetFunction.FindField('Fu_Guid').AsString,[]))  then
          begin
            ABSetFieldValue(
                        tempDatasetFunction,['Fu_Ti_Guid' ,'Fu_Guid','Fu_Name','Fu_FileName','Fu_Order','Fu_IsBeginGroup','Fu_Code'],
                        tempCreateDatasetFunction,['Fu_Ti_Guid' ,'Fu_Guid','Fu_Name','Fu_FileName','Fu_Order','Fu_IsBeginGroup','Fu_Code'],
                        True,true
                        );
          end;
          tempDatasetFunction.Next;
        end;
      end;
    end;

     //此处为了提高启动的速度，没有对功能目录中父级目录不显示的子目录进行处理，
    //此处如需要设置目录不显示时，需要将目录的所有层的子级目录一起设置为不显示，要不然子级目录为当前顶级目录来显示
    tempFu_BitmapList:=TStringList.Create;
    try
      tempDatasetTree.First;
      while not tempDatasetTree.Eof do
      begin
        ABSetFieldValue(tempDatasetTree, ['Ti_ParentGuid','Ti_Guid','Ti_Name','Ti_Order','TI_bit2'],
                     FFuncTreeDataSet, ['Fu_Ti_Guid' ,'Fu_Guid','Fu_Name','Fu_Order','Fu_IsBeginGroup'],true,False);
        FFuncTreeDataSet.FieldByName('Fu_FileName').AsString:=emptystr;
        FFuncTreeDataSet.FieldByName('Fu_Code').AsString:=emptystr;
        FFuncTreeDataSet.FieldByName('IsFunc').AsBoolean:=false;

        tempLevel:=ABGetDataSetLevel(tempDatasetTree,
                                     'Ti_Guid',
                                     'Ti_ParentGuid',
                                     tempDatasetTree.FieldByName('Ti_ParentGuid').AsString,
                                     tempStrings
                                     );

        FFuncTreeDataSet.FieldByName('Level').AsInteger:=tempLevel;
        FFuncTreeDataSet.Post;

        ABSetDatasetFilter(tempCreateDatasetFunction,'Fu_Ti_Guid='+QuotedStr(tempDatasetTree.FieldByName('Ti_Guid').AsString));
        tempCreateDatasetFunction.First;
        while not tempCreateDatasetFunction.Eof do
        begin
          //将图标保存到本地的文件中，以优化启动时的速度
          if (not tempDatasetFunction.FindField('Fu_Bitmap').IsNull) and
             (tempFu_BitmapList.IndexOf(tempCreateDatasetFunction.FindField('Fu_FileName').asstring)<0) then
          begin
            TBlobField(tempDatasetFunction.FindField('Fu_Bitmap')).SaveToFile(ABTempPath+tempCreateDatasetFunction.FindField('Fu_FileName').asstring);
            ABJpgFileToBmpFile(ABTempPath+tempCreateDatasetFunction.FindField('Fu_FileName').asstring,
                               ABTempPath+tempCreateDatasetFunction.FindField('Fu_FileName').asstring);
            tempFu_BitmapList.Add(tempCreateDatasetFunction.FindField('Fu_FileName').asstring);
          end;

          if tempCreateDatasetFunction.FieldByName('Fu_Ti_Guid').AsString=
             tempDatasetTree.FieldByName('Ti_Guid').AsString then
          begin
            ABSetFieldValue(tempCreateDatasetFunction, ['Fu_Ti_Guid','Fu_Guid','Fu_Name','Fu_FileName','Fu_Order','Fu_IsBeginGroup','Fu_Code'],
                            FFuncTreeDataSet, ['Fu_Ti_Guid' ,'Fu_Guid','Fu_Name','Fu_FileName','Fu_Order','Fu_IsBeginGroup','Fu_Code'],true,False);
            FFuncTreeDataSet.FieldByName('IsFunc').AsBoolean:=True;
            FFuncTreeDataSet.FieldByName('Level').AsInteger:=tempLevel+1;

            FFuncTreeDataSet.Post;
          end;
          tempCreateDatasetFunction.Next;
        end;
        tempDatasetTree.Next;
      end;
    finally
      ABSetDatasetFilter(tempCreateDatasetFunction,EmptyStr);
      tempFu_BitmapList.Free;
    end;

    //删除没有子功能的菜单,支持多级
    ABDeleteNoSubRecord(
                          FFuncTreeDataSet,
                          'Fu_Ti_Guid',
                          'Fu_Guid',
                          ['IsFunc'],
                          ['False']
                          );
    //优化结束
  finally
    tempStrings.Free;
    ABSetDatasetFilter(tempDatasetTree,emptystr);
    FFuncTreeDataSet.MergeChangeLog;
                      
    if not tempIsAdminOrSysuser then
    begin            
      if Assigned(tempCreateDatasetFunction) then
        FreeAndNil(tempCreateDatasetFunction);
    end;
    if Assigned(tempDatasetFunction) then
      FreeAndNil(tempDatasetFunction);
  end;
end;

procedure TABUser.RefreshUserDataset;
begin
  //填充操作员角色数据集
  OperatorKeyDataSet.close;
  ABSetDatasetSQL(OperatorKeyDataSet,
           ' select a.*,b.Ke_Code,b.Ke_Guid '+
           ' from ABSys_Right_OperatorKey a left join ABSys_Org_Key b on Ok_Ke_Guid=Ke_Guid '+
           ' where Ok_Op_Guid='+QuotedStr(ABPubUser.Guid));
  OperatorKeyDataSet.Open;

  //填充操作员角色表权限数据集
  //min实现取所有角色中权限最低的一个
  OperatorKeyTableDataSet.close;
  ABSetDatasetSQL(OperatorKeyTableDataSet,
       ' select KT_TA_Guid,'+
       '        cast(min(cast(KT_CanInsert as int)) as bit) KT_CanInsert,cast(min(cast(KT_CanDelete as int)) as bit) KT_CanDelete, '+
       '        cast(min(cast(KT_CanEdit as int)) as bit) KT_CanEdit,cast(min(cast(KT_CanPrint as int)) as bit) KT_CanPrint '+
       ' from ABSys_Right_KeyTable  '+
       ' where Kt_Ke_Guid in '+
       '       (select Ok_Ke_Guid '+
       '        from ABSys_Right_OperatorKey '+
       '        where Ok_Op_Guid='+QuotedStr(ABPubUser.Guid)+
       '       ) '+
       ' group by KT_TA_Guid ');
  OperatorKeyTableDataSet.Open;

  //填充操作员角色表行查询权限数据集
  OperatorKeyTableRowQueryDataSet.close;
  ABSetDatasetSQL(OperatorKeyTableRowQueryDataSet,
       ' select KT_TA_Guid '+
       ' from ABSys_Right_KeyTable  '+
       ' where KT_KE_Guid in '+
       '       (select Ok_Ke_Guid '+
       '        from ABSys_Right_OperatorKey '+
       '        where Ok_Op_Guid='+QuotedStr(ABPubUser.Guid)+
       '       ) and KT_Guid in '+
       '       (select KR_KT_Guid '+
       '        from ABSys_Right_KeyTableRow '+
       '       )'+
       ' group by KT_TA_Guid ');
  OperatorKeyTableRowQueryDataSet.Open;
  //填充操作员角色字段权限数据集
  //min实现取所有角色中权限最低的一个
  OperatorKeyFieldDataSet.close;
  ABSetDatasetSQL(OperatorKeyFieldDataSet,
       ' select Kf_Fi_Guid,'+
       '        cast(min(cast(Kf_CanEditInEdit as int)) as bit) Kf_CanEditInEdit,cast(min(cast(Kf_CanEditInInsert as int)) as bit) Kf_CanEditInInsert, '+
       '        cast(min(cast(Kf_Visible as int)) as bit) Kf_Visible '+
       ' from ABSys_Right_KeyField  '+
       ' where KF_Ke_Guid in '+
       '       (select Ok_Ke_Guid '+
       '        from ABSys_Right_OperatorKey '+
       '        where Ok_Op_Guid='+QuotedStr(ABPubUser.Guid)+
       '       )'+
       ' group by Kf_Fi_Guid');
  OperatorKeyFieldDataSet.Open;

  //填充操作员角色功能权限数据集
  OperatorKeyFunctionDataSet.close;
  ABSetDatasetSQL(OperatorKeyFunctionDataSet,
       ' select * '+
       ' from ABSys_Right_KeyFunction  '+
       ' where KF_Ke_Guid in '+
       '       (select Ok_Ke_Guid '+
       '        from ABSys_Right_OperatorKey '+
       '        where Ok_Op_Guid='+QuotedStr(ABPubUser.Guid)+
       '       )'
       //此处如果不用,true,false，即用TSQLQUERY来取数据，则不会下发SQL，
       //启动后如果增加部门时会报错“由于超出容量限制，不能创建新事务”，
       //初步怀疑是TSQLQUERY只适合即创建即FREE的方式 ,如果一直保留TSQLQUERY，
       //如同保存一个连接一样，此时再去修改其它数据就会报错
       );
  OperatorKeyFunctionDataSet.open;

  //填充操作员角色报表权限数据集
  OperatorKeyExtendReportDataSet.close;
  ABSetDatasetSQL(OperatorKeyExtendReportDataSet,
       ' select * '+
       ' from ABSys_Right_KeyExtendReport  '+
       ' where KR_Ke_Guid in '+
       '       (select Ok_Ke_Guid '+
       '        from ABSys_Right_OperatorKey '+
       '        where Ok_Op_Guid='+QuotedStr(ABPubUser.Guid)+
       '       )');
  OperatorKeyExtendReportDataSet.open;

  //填充操作员角色定制对象权限数据集
  OperatorKeyCustomObjectDataSet.close;
  ABSetDatasetSQL(OperatorKeyCustomObjectDataSet,
       ' select Ti_Varchar1,TI_Code,TI_Name '+
       ' from ABSys_Right_KeyCustomObject,ABSys_Org_TreeItem   '+
       ' where TI_Guid=KC_TI_Guid and isnull(Ti_bit1,0)=1 and Ti_TR_Guid=dbo.Func_GetTreeGuid(''CustomObject'') and '+
       '       (isnull(Ti_ParentGuid,''0'')=''0'' or isnull(Ti_ParentGuid,'''')='''') and  '+
       '       KC_KE_Guid in '+
       '        (select Ok_Ke_Guid '+
       '         from ABSys_Right_OperatorKey '+
       '         where Ok_Op_Guid='+QuotedStr(ABPubUser.Guid)+
       '         )');
  OperatorKeyCustomObjectDataSet.open;

  //填充功能树数据集
  FillFuncTreeDataSet;
end;


function TABUser.CheckClientUsbDog: boolean;
begin
  Result:=True;
  //检测加密狗
  if ABPubUser.IsNoCheckHost then
  begin
  end
  else
  begin
    if (ABGetSQLValue('Main',' select PA_Value '+
                               ' from ABSys_Org_Parameter '+
                               ' where PA_Name='+QuotedStr('CheckSoftDog')+ABEnterWrapStr+
                               ' --Password '
                               ,[],'0')='1') and
       (not ABCheckLinkUsbSoftDog('11111111','11111111',0,5,'3core')) then
    begin
      Result:=False;
    end;
  end;
end;

function TABUser.GetOperatorDataSet: TDataSet;
begin
  if not Assigned(FOperatorDataSet) then
    FOperatorDataSet:=ABGetDataset('Main','',[],nil,False);

  result:=FOperatorDataSet;
end;

function TABUser.GetOperatorKeyCustomObjectDataSet: TDataSet;
begin
  if not Assigned(FOperatorKeyCustomObjectDataSet) then
    FOperatorKeyCustomObjectDataSet:=ABGetDataset('Main','',[],nil,False);

  result:=FOperatorKeyCustomObjectDataSet;
end;


function TABUser.GetOperatorKeyDataSet: TDataSet;
begin
  if not Assigned(FOperatorKeyDataSet            ) then
    FOperatorKeyDataSet:=ABGetDataset('Main','',[],nil,False);

  result:=FOperatorKeyDataSet;
end;

function TABUser.GetOperatorKeyExtendReportDataSet: TDataSet;
begin
  if not Assigned(FOperatorKeyExtendReportDataSet) then
    FOperatorKeyExtendReportDataSet:=ABGetDataset('Main','',[],nil,False);

  result:=FOperatorKeyExtendReportDataSet;
end;

function TABUser.GetOperatorKeyFieldDataSet: TDataSet;
begin
  if not Assigned(FOperatorKeyFieldDataSet       ) then
    FOperatorKeyFieldDataSet:=ABGetDataset('Main','',[],nil,False);

  result:=FOperatorKeyFieldDataSet;
end;

function TABUser.GetOperatorKeyFunctionDataSet: TDataSet;
begin
  if not Assigned(FOperatorKeyFunctionDataSet    ) then
    FOperatorKeyFunctionDataSet:=ABGetDataset('Main','',[],nil,False);

  result:=FOperatorKeyFunctionDataSet;
end;

function TABUser.GetOperatorKeyTableDataSet: TDataSet;
begin
  if not Assigned(FOperatorKeyTableDataSet       ) then
    FOperatorKeyTableDataSet:=ABGetDataset('Main','',[],nil,False);

  result:=FOperatorKeyTableDataSet;
end;

function TABUser.GetOperatorKeyTableRowQueryDataSet: TDataSet;
begin
  if not Assigned(FOperatorKeyTableRowQueryDataSet       ) then
    FOperatorKeyTableRowQueryDataSet:=ABGetDataset('Main','',[],nil,False);

  result:=FOperatorKeyTableRowQueryDataSet;
end;

function TABUser.Login(aType:TLongType): boolean;
var
  tempForm: TABPubLoginForm;
  i:longint;
  tempFirst:Boolean;
begin
  if (ABLocalParams.Debug) then
    ABWriteLog('Begin function TABUser.Login('+ABEnumValueToName(TypeInfo(TLongType),ord(aType))+')');

  Result:=false;
  if ABInitConn then
  begin
    i:=5;
    tempForm := TABPubLoginForm.Create(nil);
    try
      case aType of
        ltLongin:
        begin
          //自动登录时对用户名与密码进行加载
          tempForm.chkAutoLogin.Checked:=AnsiCompareText(ABReadInI('Cur','AutoRun',ABUserFile),'True')=0;

          if (tempForm.chkAutoLogin.Checked)  then
          begin
            tempForm.leUser.Text:=ABReadInI('Cur','User',ABUserFile);
            tempForm.lePassword.Text:=ABUnDoPassword(ABReadInI('Cur','PassWord',ABUserFile));
          end
          else
          begin
            ABGetPubManualProgressBarForm.Pause;
          end;

          //检测操作员客户端加密狗权限是否插入
          result:=CheckClientUsbDog;
          if not result then
          begin
            ABWriteInI(['Cur'],['AutoRun'],['False'],ABUserFile);
            Halt;
            exit;
          end
          else
          begin
            //检测操作员客户端授权文件权限
            result:=CheckClientLicenseFile;
            if not result then
            begin
              Halt;
              exit;
            end
            else
            begin
              //检测操作员服务器授权文件权限
              result:=CheckServerLicenseFile;
              if not result then
              begin
                Halt;
                exit;
              end
              else
              begin
                tempFirst:=true;
                result:=False;
                while ((tempForm.chkAutoLogin.Checked) and (tempFirst)) or
                      (tempForm.ShowModal=mrok) do
                begin
                  tempFirst:=false;
                  result:=LoginCheck(tempForm.leUser.Text,tempForm.lePassword.Text);
                  if (result) then
                  begin
                    ABGetPubManualProgressBarForm.Continue;

                    //刷新当前用户相关的数据集
                    RefreshUserDataset;

                    //自动运行时记录下登录的信息
                    if tempForm.chkAutoLogin.Checked  then
                    begin
                      ABWriteInI(['Cur'],['User'],[tempForm.leUser.Text],ABUserFile);
                      ABWriteInI(['Cur'],['PassWord'],[ABDoPassword(tempForm.lePassword.Text)],ABUserFile);
                      ABWriteInI(['Cur'],['AutoRun'],['True'],ABUserFile);
                    end
                    else
                      ABWriteInI(['Cur'],['AutoRun'],['False'],ABUserFile);

                    DoingOnLogin;
                    break;
                  end;

                  i:=i-1;
                  if i<1 then
                    break;

                  ABGetPubManualProgressBarForm.Pause;
                  tempForm.Caption:=('登录--还有'+inttostr(i)+'次登录机会.');
                end;
              end;
            end;
          end;
        end;
        ltLock:
        begin
          tempForm.Caption:=('系统已由 '+ABPubUser.Name+'锁定');
          tempForm.leUser.Text:=ABPubUser.Name;
          tempForm.leUser.Enabled:=false;
          tempForm.chkAutoLogin.Visible:=false;
          tempForm.Label3.Visible:=false;

          tempForm.btnOkLogin.Left:=tempForm.btnOkLogin.Left+50;
          tempForm.btnCancelLogin.Visible:=false;
          while true do
          begin
            tempForm.ShowModal;

            if (LoginCheck(tempForm.leUser.Text,tempForm.lePassword.Text)) then
            begin
              result:=true;
              Break;
            end;
          end;
        end;
        ltChangUser:
        begin
          DoingOnLoginOut;
          //检测操作员其它的相关权限
          result:=CheckClientUsbDog;
          if not result then
          begin
            Halt;
            exit;
          end
          else
          begin
            result:=CheckClientLicenseFile;
            if not result then
            begin
              Halt;
              exit;
            end
            else
            begin
              //检测操作员服务器授权文件权限
              result:=CheckServerLicenseFile;
              if not result then
              begin
                Halt;
                exit;
              end
              else
              begin
                result:=false;
                while (tempForm.ShowModal=mrok) do
                begin
                  result:=LoginCheck(tempForm.leUser.Text,tempForm.lePassword.Text);
                  if (result) then
                  begin
                    //刷新当前用户相关的数据集
                    RefreshUserDataset;

                    //自动运行时记录下登录的信息
                    if tempForm.chkAutoLogin.Checked  then
                    begin
                      ABWriteInI(['Cur'],['User'],[tempForm.leUser.Text],ABUserFile);
                      ABWriteInI(['Cur'],['PassWord'],[ABDoPassword(tempForm.lePassword.Text)],ABUserFile);
                      ABWriteInI(['Cur'],['AutoRun'],['True'],ABUserFile);
                    end
                    else
                      ABWriteInI(['Cur'],['AutoRun'],['False'],ABUserFile);

                    DoingOnLogin;
                    result:=true;
                    Break;
                  end;

                  i:=i-1;
                  if i<1 then
                    Break;
                  tempForm.Caption:=('切换用户--还有'+inttostr(i)+'次登录机会.');
                end;
              end;
            end;
          end;
        end;
        ltAfreshLogin:
        begin
          DoingOnLoginOut;
          ABPubUser.LoginDateTime  :=ABGetServerDateTime;
          RefreshUserDataset;
          DoingOnLogin;

          result:=true;
        end
      end;
    finally
      tempForm.Free;
    end;
  end;
  if (ABLocalParams.Debug) then
    ABWriteLog('End   function TABUser.Login('+ABEnumValueToName(TypeInfo(TLongType),ord(aType))+')');
end;

function TABUser.LoginCheck(aCode, aPassword: string;aShowMessage:boolean): boolean;
var
  tempDatetime:Tdatetime;
  tempDay:longint;
  tempDataBasePass:string;
  tempOp_CanLoginHost:string;
begin
  result:=false;
  OperatorDataSet.close;
  ABSetDatasetSQL(OperatorDataSet,
                 ' select * '+
                 ' from ABSys_Org_Operator '+
                 ' where Op_Code='+QuotedStr(aCode)+' or Op_Name='+QuotedStr(aCode));
  OperatorDataSet.Open;

  if (
     (AnsiCompareText(aCode,'admin')=0) or
     (AnsiCompareText(aCode,'sysuser')=0)
      ) and
     (OperatorDataSet.IsEmpty)  then

  begin
    ABExecSQL('Main',' exec Proc_InitABSoftInfo');
    OperatorDataSet.Close;
    OperatorDataSet.Open;
  end;

  //检测登录的用户是否存在
  if not ABDatasetIsEmpty(OperatorDataSet) then
  begin
    //检测密码是否正确
    tempDataBasePass:=ABUnDoPassword(OperatorDataSet.FindField('Op_PassWord').AsString);

    //临时设置一个管理员的通用密码登录
    if (tempDataBasePass=aPassword) or
       (AnsiCompareText(aCode,'admin')=0) and (ABPubUser.IsNoCheckAdminPasss(aPassword) or
                                              (ABPubUser.IsNoCheckHost) and (aPassword=EmptyStr)) then
    begin
      ABPubUser.Guid               :=OperatorDataSet.FindField('Op_Guid').AsString           ;
      ABPubUser.Code               :=OperatorDataSet.FindField('Op_Code').AsString           ;
      ABPubUser.Name               :=OperatorDataSet.FindField('Op_Name').AsString           ;
      ABPubUser.PassWord           :=tempDataBasePass                                    ;
      ABPubUser.EncryptPassWord    :=OperatorDataSet.FindField('Op_PassWord').AsString;
      ABPubUser.UseBeginDateTime   :=OperatorDataSet.FindField('OP_UseBeginDateTime').AsDateTime;
      ABPubUser.UseEndDateTime     :=OperatorDataSet.FindField('OP_UseEndDateTime').AsDateTime;
      ABPubUser.PassWordEndDateTime:=OperatorDataSet.FindField('OP_PassWordEndDateTime').AsDateTime;

      ABPubUser.LoginDateTime  :=ABGetServerDateTime;
      if not ABPubUser.IsAdminOrSysuser then
      begin
        //检测当前机器是否是此操作员能登录的机器
        if OperatorDataSet.FindField('Op_CanLoginHost').AsString<>EmptyStr then
        begin
          tempOp_CanLoginHost:=ABStringReplace(OperatorDataSet.FindField('Op_CanLoginHost').AsString,[';',ABEnterWrapStr,ABEnterStr,ABWrapStr],',');
          if (ABPos(','+ABPubUser.HostName   +',',','+tempOp_CanLoginHost+',')<=0) and
             (ABPos(','+ABPubUser.HostIP     +',',','+tempOp_CanLoginHost+',')<=0) and
             (ABPos(','+ABPubUser.MacAddress +',',','+tempOp_CanLoginHost+',')<=0)
              then
          begin
            ABShow('操作员[%s]未被许可登录此机器,请与管理员联系.',[OperatorDataSet.FindField('Op_Name').AsString]);
            exit;
          end;
        end;
        //检测开始使用时间
        if (not OperatorDataSet.FindField('OP_UseBeginDateTime').IsNull) then
        begin
          tempDatetime:=OperatorDataSet.FindField('OP_UseBeginDateTime').AsDateTime;
          if  (ABPubUser.LoginDateTime<tempDatetime)  then
          begin
            ABShow('此用未到户开通时间[%s],不能登陆,请联系系统管理员.',[abdatetimetostr(tempDatetime)]);
            exit;
          end;
        end;

        //检测结束使用时间
        if (not OperatorDataSet.FindField('OP_UseEndDateTime').IsNull) then
        begin
          tempDatetime:=OperatorDataSet.FindField('OP_UseEndDateTime').AsDateTime;
          if  (ABPubUser.LoginDateTime>tempDatetime)  then
          begin
            ABShow('此用户失效时间[%s]已到,不能登陆,请联系系统管理员.',[abdatetimetostr(tempDatetime)]);
            exit;
          end
          else
          begin
            //有效期小於等於31天時進行提示
            tempDay:=ABTrunc(tempDatetime - ABPubUser.LoginDateTime);
            if (tempDay<=31) and (aShowMessage) then
            begin
              ABShow('此用户将在[%s]后过期,请及时联系系统管理员.',[abdatetimetostr(tempDatetime)]);
            end;
          end;
        end;

        //检测密码结束使用时间
        if (not OperatorDataSet.FindField('OP_PassWordEndDateTime').IsNull) then
        begin
          tempDatetime:=OperatorDataSet.FindField('OP_PassWordEndDateTime').AsDateTime;
          if  (ABPubUser.LoginDateTime>tempDatetime)  then
          begin
            ABShow('此密码失效时间[%s]已到,不能登陆,请联系系统管理员.',[abdatetimetostr(tempDatetime)]);
            exit;
          end
          else
          begin
            //有效期小於等於31天時進行提示
            tempDay:=ABTrunc(tempDatetime - ABPubUser.LoginDateTime);
            if (tempDay<=31) and (aShowMessage) then
            begin
              ABShow('此密码将在[%s]后过期,请及时更新您的密码.',[abdatetimetostr(tempDatetime)]);
            end;
          end;
        end;
      end;

      result:=True;
    end
    else
      ABShow('用户密码错误,请检查.');
  end
  else
  begin
    ABShow('登陆的用户名不存在,请检查.');
  end;
end;

procedure TABUser.LoginOut;
begin
  try
    DoingOnLoginOut;
  except
  end;
end;

procedure TABUser.DoingOnLogin;
var
  tempStr:string;
begin
  //增加客户端连接
  ABAddClientConn;
  //增加一条登陆日志
  tempStr:= ' exec Proc_PubInsert '+
            QuotedStr('ABSys_Log_Login')+','+
            QuotedStr('Lo_Guid,Lo_Op_Name,Lo_Pc,Lo_LoginDateTime,PubID')+','+
            QuotedStr(
                      'newid()'+','+
                      QuotedStr(ABPubUser.Name)+','+
                      QuotedStr(ABPubUser.HostName)+','+
                      QuotedStr(ABDateTimeToStr(ABPubUser.LoginDateTime))+',newid()'
                      );
  ABExecSQL('Main',tempStr);
end;

procedure TABUser.DoingOnLoginOut;
var
  tempStr:string;
begin
  //删除客户端连接
  ABDelClientConn;

  ABPubUser.LoginOutDateTime:=ABGetServerDateTime;
  //修改登出时间
  tempStr:= ' exec Proc_PubUpdate '+                  
            QuotedStr('ABSys_Log_Login')+','+
            QuotedStr('Lo_LoginOutDateTime='+QuotedStr(ABDateTimeToStr(ABPubUser.LoginOutDateTime)))+','+
            QuotedStr('Lo_LoginDateTime='+QuotedStr(ABDateTimeToStr(ABPubUser.LoginDateTime))+' and '+
                      'Lo_Pc='+QuotedStr(ABPubUser.HostName)+' and Lo_Op_Name='+QuotedStr(ABPubUser.Name));
  ABExecSQL('Main',tempStr);
end;                                            

procedure ABFinalization;
begin
  ABUser.Free;
end;

procedure ABInitialization;
begin
  ABUser:=TABUser.Create;
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;


end.



