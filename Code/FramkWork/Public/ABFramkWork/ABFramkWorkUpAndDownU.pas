{
框架更新/下载单元
将程序包更新到服务器或从服务器下载程序包
实现了客户端程序的自动更新功能
}
unit ABFramkWorkUpAndDownU;

interface
{$I ..\ABInclude.ini}


uses
  ABPubLocalParamsU,
  ABPubRegisterFileU,
  ABPubVarU,
  ABPubDBU,
  ABPubManualProgressBar_ThreadU,
  ABPubManualProgressBarU,
  ABPubMessageU,
  ABPubFuncU,
  ABPubConstU,

  ABThirdConnU,
  ABThirdCustomQueryU,
  ABThirdDBU,

  ShellAPI,
  Windows,forms,
  SysUtils,Variants,DB;

//运行传入的主程序文件,同时做一些开始与结尾操作
//在客户定制时传入客户的定制文件名就可
procedure ABInitAndRun(aFileName:string);

//下载所有的程序包到本地
procedure ABDownAllPkgs(aAddWhere:string='');
//更新本地所有的程序包到服务器
procedure ABUpdateAllPkgs;

implementation


procedure ABUpdateAllPkgs;
  procedure ABUpPkgs(aSleepNum:LongInt);
  var
    tempCurProgress: Integer;
    tempDataset:TDataSet;
    tempFileExt:string;
    tempName,tempLocalFileName: string;
    tempUpCount:longint;
  begin
    tempDataset:=ABGetDataset('Main',
                              ' SELECT Fu_Name,Fu_SubPath,Fu_FileName,Fu_Version,Fu_Guid  '+
                              ' FROM ABSys_Org_Function '+
                              ' where Fu_Pkg is not null and FU_IsUpdate=1 and '+
                              '       RIGHT(rtrim(Fu_Filename),3) not in (''ini'',''txt'') ',[],nil,true);

    ABPubManualProgressBar_ThreadU.ABRunProgressBar(tempCurProgress,tempdataset.RecordCount,'上传中');
    try
      ABBegTransaction('Main');
      try
        tempUpCount:=0;
        tempdataset.first;
        while not tempdataset.Eof do
        begin
          tempCurProgress:=tempCurProgress+1;
          tempName:=tempDataset.FindField('Fu_Name').AsString;
          tempFileExt:=ABGetFileExt(tempName);
          tempLocalFileName:=ABAppPath+ABGetpath(tempDataset.FindField('Fu_SubPath').AsString) +
                                 tempDataset.FindField('Fu_FileName').AsString;
          if (ABRegisterFile.IsUp(tempLocalFileName,
                                 tempdataset.FindField('Fu_Version').AsString)) then
          begin
            if (aSleepNum>0)  then
              Sleep(aSleepNum);

            ABExecSQL('Main',
                      ' update ABSys_Org_Function set Fu_Version='+QuotedStr(ABRegisterFile.GetVersion(tempLocalFileName))+
                      ' where Fu_Guid='+QuotedStr(tempdataset.FindField('Fu_Guid').AsString)
                      );
            ABUpFileToField('Main',
                            'ABSys_Org_Function','Fu_Pkg',
                            'Fu_Guid='+QuotedStr(tempdataset.FindField('Fu_Guid').AsString),
                            tempLocalFileName);
            tempUpCount:=tempUpCount+1;
          end;
          tempdataset.Next;
        end;
        if tempUpCount>0 then
          ABExecSQL('Main','update ABSys_Org_Parameter set Pa_Value=isnull(Pa_Value,0)+1 where Pa_Name=''FuncUpdateCount''');

        ABCommitTransaction('Main');
      except
        ABRollbackTransaction('Main');
        ABShow('模块[%s]上传失败,请重新上传.',[tempName]);
        raise;
      end;
    finally
      ABPubManualProgressBar_ThreadU.ABStopProgressBar;
      tempDataset.Free;
    end;
  end;
begin
  if ABInitConn then
  begin
    ABDoBusy;
    try
      ABUpPkgs(0);
    finally
      ABDoBusy(false);
    end;
  end;
end;

procedure ABInitAndRun(aFileName:string);
var
  tempFileName,tempBatFileName:string;
begin
  ABDoBusy;
  try
    //运行下载前脚本
    tempBatFileName:=ABAppPath+'BeforeDown.bat';
    if (ABCheckFileExists(tempBatFileName)) and
       (ABReadTxt(tempBatFileName)<>EmptyStr) then
    begin
      ABWaitAppEnd(tempBatFileName, SW_Hide);
    end;
    //运行下载
    ABDownAllPkgs(' isnull(Fu_RunPosition,'''')='''' or Fu_RunPosition=''Client'' '+
                  ' OR Fu_RunPosition=''ServerClient''' );
    //运行下载后脚本
    tempBatFileName:=ABAppPath+'AfterDown.bat';
    if (ABCheckFileExists(tempBatFileName)) and
       (ABReadTxt(tempBatFileName)<>EmptyStr) then
    begin
      ABWaitAppEnd(tempBatFileName, SW_Hide);
    end;

    //调用的主程序文件名(无扩展名)
    aFileName:=ABAppPath+aFileName;
    tempFileName:= EmptyStr;
    if ABCheckFileExists(aFileName+'.exe') then
      tempFileName:=aFileName+'.exe'
    else if ABCheckFileExists(aFileName) then
      tempFileName:=aFileName;

    if tempFileName<>EmptyStr then
    begin
      //windows 7下程序用XP兼容方式运行，以解决CXGRID导出不了EXCEL的BUG
      if AnsiCompareText(ABGetSysVersion,'windows 7')=0 then
      begin
        ABWriteRegKey('Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers',
                      tempFileName,
                      'WINXPSP3 RUNASADMIN',
                      rtString,
                      HKEY_CURRENT_USER);
      end;

      //运行运行前脚本
      tempBatFileName:=ABAppPath+'BeforeRun.bat';
      if (ABCheckFileExists(tempBatFileName)) and
         (ABReadTxt(tempBatFileName)<>EmptyStr) then
      begin
        ABWaitAppEnd(tempBatFileName, SW_Hide);
      end;

      ShellExecute(Application.handle,nil,PChar(tempFileName),nil,nil,sw_shownormal);

      //运行运行后脚本
      tempBatFileName:=ABAppPath+'AfterRun.bat';
      if (ABCheckFileExists(tempBatFileName)) and
         (ABReadTxt(tempBatFileName)<>EmptyStr) then
      begin
        ABWaitAppEnd(tempBatFileName, SW_Hide);
      end;
    end
    else
    begin
      ABShow('未找到主程序文件['+aFileName+']或['+aFileName+'.exe'+'],请检查.');
    end;
  finally
    ABDoBusy(false);
  end;
end;

procedure ABDownAllPkgs(aAddWhere:string);
  function ABDownPkgs(aAddWhere:string):boolean;
  var
    tempDataset:TDataSet;
    tempPkgDataset:TDataSet;
    tempName: string;
  begin
    result:=true;

    //如果程序运行前没有参数文件,则删除可能自动创建的参数文件,以使得数据库中的文件能下载下来
    if not ABLocalParams.HaveFileBeforeRun then
      ABDeleteFile(ABLocalParamsFile);


    if aAddWhere<>EmptyStr then
      aAddWhere:=' and ('+aAddWhere+')';

    tempdataset:=ABGetDataset('Main',
    ' SELECT Fu_Name,Fu_Version,Fu_FileName,Fu_SubPath,FU_IsUpdate '+
    ' from ABSys_Org_Function '+
    ' where Fu_Public=1  and FU_IsUpdate=1 and '+
    '       Fu_Pkg is not null '+aAddWhere,[],nil,true);
    tempPkgDataset:=ABGetDataset('Main','',[]);

    ABGetPubManualProgressBarForm.Title:='系统检测更新中';
    ABGetPubManualProgressBarForm.MainMin:=0;
    ABGetPubManualProgressBarForm.MainMax:=tempdataset.RecordCount;
    ABGetPubManualProgressBarForm.MainPosition:=0;
    ABGetPubManualProgressBarForm.Run;
    try
      try
        tempdataset.first;
        while not tempdataset.Eof do
        begin
          ABGetPubManualProgressBarForm.NextMainIndex;

          tempName:=tempDataset.FindField('Fu_Name').AsString;

          if (not (ABCheckFileExists(ABAppPath+ABGetpath(tempDataset.FindField('Fu_SubPath').AsString) +
                                tempDataset.FindField('Fu_FileName').AsString))) or
             ((tempDataset.FindField('FU_IsUpdate').AsBoolean) and
              (ABRegisterFile.IsDown(tempDataset.FindField('Fu_FileName').AsString,
                                    tempDataset.FindField('Fu_SubPath').AsString,
                                    tempdataset.FindField('Fu_Version').AsString))
              ) then
          begin
            ABSetDatasetSQL(tempPkgDataset,
              ' SELECT Fu_Pkg '+
              ' from ABSys_Org_Function '+
              ' where Fu_FileName='+QuotedStr(tempDataset.FindField('Fu_FileName').AsString)
                            );
            tempPkgDataset.Open;
            if (not ABDatasetIsEmpty(tempPkgDataset)) then
            begin
              ABRegisterFile.Down(
                                tempDataset.FindField('Fu_FileName').AsString,
                                tempDataset.FindField('Fu_SubPath').AsString,
                                TBlobField(tempPkgDataset.Fields[0]),
                                tempdataset.FindField('Fu_Version').AsString
                                                  );
            end;
          end;
          tempdataset.Next;
        end;
      except
        result:=false;
        ABShow('客户端更新失败,请重新启动客户端完成更新.');
      end;
    finally
      ABGetPubManualProgressBarForm.Stop;
      tempPkgDataset.free;
      tempdataset.free;
    end;
  end;
var
  tempServerFuncUpdateCount:LongInt;
  tempAutoDownPkg:string;
begin
  if ABInitConn then
  begin
    tempServerFuncUpdateCount:=
      StrToInt64Def(ABGetSQLValue('Main',
                                  'select Pa_Value from ABSys_Org_Parameter  where Pa_Name=''FuncUpdateCount'' ',
                                  [],'0'),0);
    tempAutoDownPkg:= ABGetSQLValue('Main',
                                  'select Pa_Value from ABSys_Org_Parameter  where Pa_Name=''AutoDownPkg'' ',
                                  [],'Auto');

    if ABLocalParams.FuncUpdateCount>tempServerFuncUpdateCount then
      ABSetLocalParamsValue('FuncUpdateCount',inttostr(tempServerFuncUpdateCount),'Params');

    if ABLocalParams.FuncUpdateCount<tempServerFuncUpdateCount then
    begin
      if (AnsiCompareText(tempAutoDownPkg,'Down')=0) or
         (AnsiCompareText(tempAutoDownPkg,'Auto')=0) and
         (ABLocalParams.AutoDownPkg) then
      begin
        if ABDownPkgs(aAddWhere) then
        begin
          ABSetLocalParamsValue('FuncUpdateCount',inttostr(tempServerFuncUpdateCount),'Params');
        end;
      end;
    end;
  end;
end;

end.
