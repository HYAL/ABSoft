{
框架字典数据集单元
}
unit ABFramkWorkDictionaryQueryU;

interface
{$I ..\ABInclude.ini}

uses

  ABPubSQLParseU,
  ABPubVarU,
  ABPubUserU,
  ABPubMessageU,
  ABPubConstU,
  ABPubDBU,
  ABPubFuncU,
  ABPubLocalParamsU,

  ABThirdQueryU,
  ABThirdCacheDatasetU,
  ABThirdDBU,
  ABThirdConnU,
  ABThirdConnDatabaseU,
  ABThirdCustomQueryU,

  ABFramkWorkConstU,
  ABFramkWorkFuncU,
  ABFramkWorkUserU,

  cxDBEdit,
  Menus,StdCtrls,
  FireDAC.Comp.DataSet,
  FireDAC.Stan.ExprFuncs,
  FireDAC.Stan.Expr,
  FireDAC.Stan.Intf,
  SysUtils,Variants,Classes,Forms,Controls,DB,DateUtils;

type
  //增加加载表功能的框架数据集，数据集字段标题及索引组成都是根据加载表来截入
  TABCustomDictionaryQuery  = class(TABThirdQuery)
  private
  protected
    procedure AfterChangSQL( aNewSQL: String;var aDesigning:Boolean;var aLoading:Boolean);override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property LoadTables;
  end;

  //在框架起始数据集的基础上取出字段的显示标题
  TABFieldCaptionQuery = class(TABCustomDictionaryQuery)
  private
  protected
    procedure DoAfterOpen; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
  end;

  //在框架起始数据集的基础上取出字段的主键，可主键增删改，因为字段标题没有，一般不用来作界面显示
  TABInsideQuery = class(TABCustomDictionaryQuery)
  private
    FPrimaryKeyFieldNames:string;
  protected
    procedure DoAfterOpen; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property PrimaryKeyFieldNames:string read FPrimaryKeyFieldNames;
  end;

  //数据集中Image或二进制等大数据类型字段处理类
  //因为在select中如果有Image或二进制等大数据类型字段,则SQL的速度会很慢,所以要对这些字段进行单独处理
  //1.主数据集不取大字段，大字段在SQL中可用 '' as  字段名样式
  //2.大字段用主数据集的明细表来取数据，此时只取出一条大字段数据,所以速度不会受太大影响
  TBlobFieldDef = class(TCollectionItem)
  private
    FDatasource: TDatasource;
    FFieldName: string;
  protected
  public
  published
    //取大字段数据的明细表数据源
    property Datasource:TDatasource read FDatasource write FDatasource;
    //大数据字段的字段名,明细表数据集和主数据集中的大数据字段的字段名要一致
    property FieldName:string read FFieldName write FFieldName;
  end;

  TBlobFieldDefs = class(TCollection)
  private
    function GetItem(Index: Integer): TBlobFieldDef;
    procedure SetItem(Index: Integer; Value: TBlobFieldDef);
  protected
  public
    function FindFieldName(aFieldName:string):TBlobFieldDef;
    property Items[Index: Integer]: TBlobFieldDef read GetItem write SetItem; default;
    constructor Create;
  end;

  //在多层查询取明细的条件时触发,可以此事件中去编辑查询SQL
  TABOnMultiLevelDetailWhereEvent=procedure(var aMasterFields: string;var aDetailFields:string;var aFromTables:string;var aInitWhere:string) of object;
  //此数据集为数据字典数据集(无界面输入、关联功能)
  TABDictionaryQuery =class(TABCustomDictionaryQuery,IABQuery)
  private
    //内部使用的一些变量
    //插入方式新增记录时本记录的序号字段值
    FInsertOrderValue:LongInt;

    //初始化表的相关信息,如索引,主键,物理排序等
    procedure SetTableInfo;
    //根据不同的类型取得特殊作用的字段名(如插入时的序号字段名、限制修改的系统使用字段名)
    function GetEspecialFieldName(aType:string):string;

    //连接BlobField的数据集
    procedure LinkBlobFieldDatasets;
    //编辑BlobField的数据集
    procedure EditBlobFieldDatasets;
    //取消修改BlobField的数据集
    procedure CancelBlobFieldDatasets;
    //保存BlobField的数据集
    procedure SaveBlobFieldDatasets;
  private
    FBlobFieldDefs: TBlobFieldDefs;
    FLoadFieldsDefed:boolean;
    FAfterLoadFieldDownDef: TABAfterLoadFieldDownDefEvent;
    FOnFieldSetText: TFieldSetTextEvent;
    FPrimaryKeyFieldNames: TStrings;
    FCanCheckFieldValue: Boolean;
    FAfterLoadFieldBaseDef: TABAfterLoadFieldBaseDefEvent;
    FOnFieldValidate: TFieldNotifyEvent;
    FOnFieldGetText: TFieldGetTextEvent;
    FOnFieldChange: TFieldNotifyEvent;
    FAfterLoadFieldMetaDef: TABAfterLoadFieldMetaDefEvent;
    FClusteredFieldNames: TStrings;
    FLinkDBTableViewList: Tlist;
    FLinkDBPanelList: Tlist;
    FFuncGuid: String;
    FFuncFileName: String;
    FOnFieldInitPopup: TABOnFieldInitPopupEvent;
    FDefList: TStrings;
    FOnMultiLevelDetailWhereEvent: TABOnMultiLevelDetailWhereEvent;
    FMultiLevelOrder: LongInt;
    FMultiLevelQuery: Boolean;
    FMultiLevelCaption: string;
    FMultiLevelAlign: TAlign;
    FInitActiveOK: Boolean;
    FAddTableRowQueryRight: Boolean;
    FTableRowQueryWhere: String;
    FSysUseQueryWhere: String;
    function GetMultiLevelCaption: string;
    function GetFieldDef(Index: Integer): PABFieldDef;
    //取得数据集所在功能模块的模块GUID
    function GetFuncGuid: String;
    procedure ADDDatasetExtFields;
    { Private declarations }
  protected
    procedure AfterChangSQL( aNewSQL: String;var aDesigning:Boolean;var aLoading:Boolean);override;
    procedure DoTransactBaseSQL(var aSQL: string);override;
    //字段相关事件触发的过程
    procedure FieldGetText(aField: TField; var Text: String;DisplayText: Boolean);
    procedure FieldSetText(aField: TField; const Text: string);
    procedure FieldValidate(aField: TField);
    procedure FieldChange(aField: TField);

    //字段相关事件后触发的过程
    //aDo为true表示在过程中已做了事情
    procedure AfterFieldGetText(aField: TField; var Text: String;DisplayText: Boolean;var aDo:Boolean);virtual;
    procedure AfterFieldSetText(aField: TField; const Text: string;var aDo:Boolean);virtual;
    procedure AfterFieldValidate(aField: TField;var aDo:Boolean); virtual;
    procedure AfterFieldChange(aField: TField;var aDo:Boolean);  virtual;

    procedure DoBeforePost; override;
    procedure DoAfterOpen; override;
    procedure DoAfterInsert; override;
    procedure DoAfterEdit; override;
    procedure DoAfterCancel; override;
    procedure DoBeforeCommitTrans;override;
    function FieldCanUpdateToDatabase(aTableName,aFieldName:String;var aIsIDENTITY:boolean): boolean;override;

    procedure DoBeforeOpen; override;
    procedure CreateFields; override;
    procedure InternalInitFieldDefs; override;
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  public
    //开窗查询数据
    procedure OpenQuery;virtual;
    //创建加载字段框架信息
    procedure CreateFieldExtInfos;virtual;
    //清除字段框架信息,下次打开数据集时会重新加载
    procedure FreeFieldExtInfos;virtual;

    //设置指定字段关联控件的Enabled
    //aFieldNames=emptystr表示不做动作
    procedure SetEnabled(aFieldNames:string);
    //设置指定字段关联控件的Visible
    //aFieldNames=emptystr表示不做动作
    procedure SetVisible(aFieldNames:string);

    //以面板方式显示当前记录的数据(包含显示前与显示后事件调用)
    //aCanEdit=显示时是否可编辑
    //aActiveValue=多页时，当前激活的值
    //aAllValues=多页时，所有页的值
    //aEnabledTrueFieldNames，aEnabledFalseFieldNames=只读与非只读字段名串
    //aVisibleTrueFieldNames，aVisibleFalseFieldNames=显示与不显示字段名串
    //aCreateControlForEveryOne=每次都重新创建DBPanel中的控件
    procedure ShowDBPanel(aCanEdit:boolean;
                          aActivePageCaption:string='';
                          aEnabledFieldNames:string='';
                          aVisibleFieldNames:string=''
                          );virtual;

    //取得数据集所在功能模块的模块文件名
    function GetFuncFileName: String;
    //取得数据集所在功能模块的字段名
    function GetFuncFieldValue(aFieldName: String): String;

    //重写TABDictionaryQuery基类的一些方法
    procedure GetActiveDetailDataSets(aList: TList);  override;
    function FieldDefaultValueCanUse(aFieldName,aFieldValue:string): boolean; override;
    function FieldCanCopy(aFieldName:string): boolean; override;

    //框架信息部分
    function FieldDefByFieldName(const FieldName: String): PABFieldDef;
    property FieldDefItems[Index: Integer]: PABFieldDef read GetFieldDef ;
    function FieldDefBySetOrder(aSetOrder: Integer): PABFieldDef;
    function FieldsIsEdited: Boolean;
    procedure SetFieldsEdited(aValue: Boolean);

    //新增后或保存前(包括新增与修改的保存)填充默认值
    procedure FillDefaultValue(aIsAfterAppend,aIsBeforePost:Boolean;aHaveField:string='';aNoHaveField:string='');

    //序号字段的设置值处理
    procedure SetInsertOrderValue;
    procedure GetInsertOrderValue;

    //字段扩展定义列表
    property DefList: TStrings  read FDefList;
    //关联数据显示的控件列表
    property LinkDBPanelList:Tlist  read FLinkDBPanelList  write FLinkDBPanelList;
    property LinkDBTableViewList:Tlist  read FLinkDBTableViewList  write FLinkDBTableViewList;

    //是否加载了字段的定义
    property LoadFieldsDefed: Boolean read FLoadFieldsDefed;
    //表的主键字段
    property PrimaryKeyFieldNames:TStrings read FPrimaryKeyFieldNames;
    //表的物理排序字段
    property ClusteredFieldNames:TStrings read FClusteredFieldNames;

    //此查询是属于哪一个功能的
    property FuncFileName:String read GetFuncFileName  write FFuncFileName;
    property FuncGuid:String read GetFuncGuid write FFuncGuid;
    property TableRowQueryWhere:String read FTableRowQueryWhere;
    property SysUseQueryWhere:String read FSysUseQueryWhere;

  published
    //初始化激活是否完成
    property InitActiveOK: Boolean read FInitActiveOK write FInitActiveOK;
    //Image或其它指针型字段集合类
    property BlobFieldDefs:TBlobFieldDefs read FBlobFieldDefs write FBlobFieldDefs;

    //多层表查询相关
    //在查询开窗中是否使用多层表查询
    //如订单的主表数据集MultiLevelQuery=True，则主表查询时可通过订单从表的品号来查询
    property MultiLevelQuery:Boolean  read FMultiLevelQuery   write FMultiLevelQuery;
    //多层开窗查询时标识本数据集的标题,为空时取第一个加载表的Caption
    property MultiLevelCaption:string  read GetMultiLevelCaption   write FMultiLevelCaption;
    //多层开窗查询时子数据集查询控件容器的排序
    property MultiLevelOrder:LongInt  read FMultiLevelOrder   write FMultiLevelOrder;
    //多层开窗查询时子数据集查询控件容器的排列类型
    property MultiLevelAlign:TAlign  read FMultiLevelAlign   write FMultiLevelAlign;

    //多层开窗查询事件，可在此事件中自定义查询的信息
    property OnMultiLevelDetailWhereEvent: TABOnMultiLevelDetailWhereEvent read FOnMultiLevelDetailWhereEvent write FOnMultiLevelDetailWhereEvent;

    //在保存及字段修改时是否进行字段值合法性检查
    property CanCheckFieldValue:Boolean  read FCanCheckFieldValue   write FCanCheckFieldValue;

    //字段相关事件
    //这些事件是字段事件的统一入口,在事件内可判断字段名以执行特定字段的事件
    property OnFieldChange  : TFieldNotifyEvent   read FOnFieldChange   write FOnFieldChange   ;
    property OnFieldGetText : TFieldGetTextEvent  read FOnFieldGetText  write FOnFieldGetText  ;
    property OnFieldSetText : TFieldSetTextEvent  read FOnFieldSetText  write FOnFieldSetText  ;
    property OnFieldValidate: TFieldNotifyEvent   read FOnFieldValidate write FOnFieldValidate ;

    //字段框架信息加载后的事件
    //加载字段框架基本信息后触发,可在此事件中更改字段框架的基本信息
    property AfterLoadFieldBaseDef:TABAfterLoadFieldBaseDefEvent  read FAfterLoadFieldBaseDef  write FAfterLoadFieldBaseDef;
    //加载字段框架下拉信息后触发,可在此事件中更改字段框架的下拉信息
    property AfterLoadFieldDownDef:TABAfterLoadFieldDownDefEvent  read FAfterLoadFieldDownDef  write FAfterLoadFieldDownDef;
    //加载字段框架元信息后触发,可在此事件中更改字段框架的元信息
    property AfterLoadFieldMetaDef:TABAfterLoadFieldMetaDefEvent  read FAfterLoadFieldMetaDef  write FAfterLoadFieldMetaDef;

    //在下拉字段关联控件下拉时触发,可在此事件中更改下拉的过滤
    property OnFieldInitPopup:TABOnFieldInitPopupEvent  read FOnFieldInitPopup  write FOnFieldInitPopup;
    //SQL中加入表行查询
    property AddTableRowQueryRight: Boolean read FAddTableRowQueryRight write FAddTableRowQueryRight;
    { Published declarations }
  end;

//在保存前检测所有字段的改变是否有效
function ABCheckAllFieldValue(aDataSet:TABDictionaryQuery):Boolean;
//在保存或修改前检测单个字段的改变是否有效
function ABCheckFieldValue(aFieldDef:PABFieldDef):Boolean;
//字段默认值是否会不停改变的
function ABFieldDefaultValueIsVar(aDataset:TABDictionaryQuery;aFieldName:string):boolean;
//字段的默认值在保存前是否需要重计算,如max,min,datetime之类的需要重计算
function ABNeedResetDefaultValueBeforePost(aDefaultValue: string): boolean;
//取得表名的显示名称
function ABGetTableCaption(aDatabasename,aTablename: String):String;
//取共用的下拉组别数据集
function ABGetPubFieldDownGroup:TDataSet;
//检测数据集当前记录的检测字段是否有重复的检测字段值
//aKeyFieldNames=主键字段
//aCheckFieldNames=检测字段
//aCheckFieldValues=检测值
//aFilter=检测前过滤条件
function ABCheckDatasetRepeat(aDataset: TABThirdReadDataQuery;
                              aKeyFieldNames: string;
                              aCheckFieldNames: string;
                              aCheckFieldValues: Variant): Boolean;

implementation

var
  FPubFieldDownGroup :TDataSet;
  //临时保存字段下拉的定义
  FPubFieldDownDef:PABFieldDownDef;

function ABCheckDatasetRepeat(aDataset:TABThirdReadDataQuery ;
  aKeyFieldNames: string;
  aCheckFieldNames: string;
  aCheckFieldValues: Variant): Boolean;
var
  tempDataSet: TDataSet;
  tempSQL:string;

  tempOldKeyFieldValues: Variant;
  tempNewKeyFieldValues: Variant;
  tempParamValues: array of Variant;
  I: Integer;
begin
  Result := False;
  tempSQL:=ABGetDatasetSQL(aDataset);
  if aDataset.ParamCount>0 then
  begin
    SetLength(tempParamValues,aDataset.ParamCount);
    for I := 0 to aDataset.ParamCount-1 do
    begin
      tempParamValues[i]:=aDataset.Params[i].Value;
    end;
  end;

  tempDataSet := ABGetDataset(aDataset.ConnName, tempSQL,tempParamValues, aDataset.DataSource.DataSet);
  try
    ABSetDatasetFilter(tempDataSet,aKeyFieldNames+'<>'+QuotedStr(aDataset.FieldByName(aKeyFieldNames).AsString));

    if tempDataSet.Locate(aCheckFieldNames,aCheckFieldValues,[loCaseInsensitive]) then
    begin
      tempNewKeyFieldValues :=tempDataSet.FieldByName(aKeyFieldNames).Value;
    end;
    if (not abvarisnull(tempNewKeyFieldValues)) and
      (tempOldKeyFieldValues <> tempNewKeyFieldValues) then
      Result := true;
  finally
    tempDataSet.Free;
  end;
end;

function ABGetPubFieldDownGroup:TDataSet;
begin
  if not Assigned(FPubFieldDownGroup) then
    FPubFieldDownGroup:=ABGetConstSqlPubDataset('ABSys_Org_FieldDownGroup',[ABGetDatabaseByConnName('Main')]);

  Result:=FPubFieldDownGroup;
end;

function ABGetTableCaption(aDatabasename,aTablename: String):String;
var
  tempDataset:TDataSet;
begin
  tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_Table',[aDatabasename,aTablename]);
  Result:=tempDataset.FieldByName('Ta_Caption').AsString;
end;

function ABNeedResetDefaultValueBeforePost(aDefaultValue:string):boolean;
begin
  Result:=false;
  if AnsiCompareText(ABLeftStr(aDefaultValue,Length('DatetimeNumber')),'DatetimeNumber')=0 then
  begin
    Result:=true;
  end
  else if AnsiCompareText(ABLeftStr(aDefaultValue,Length('Max')),'Max')=0 then
  begin
    Result:=true;
  end
  else if AnsiCompareText(ABLeftStr(aDefaultValue,Length('Min')),'Min')=0 then
  begin
    Result:=true;
  end
  else if AnsiCompareText(ABLeftStr(aDefaultValue,Length('Count')),'Count')=0 then
  begin
    Result:=true;
  end
  else if AnsiCompareText(ABLeftStr(aDefaultValue,Length('AVG')),'AVG')=0 then
  begin
    Result:=true;
  end
  else if AnsiCompareText(ABLeftStr(aDefaultValue,Length('SUM')),'SUM')=0 then
  begin
    Result:=true;
  end
  else if AnsiCompareText(ABLeftStr(aDefaultValue,Length('Max_Detail')),'Max_Detail')=0 then
  begin
    Result:=true;
  end
  else if AnsiCompareText(ABLeftStr(aDefaultValue,Length('Min_Detail')),'Min_Detail')=0 then
  begin
    Result:=true;
  end
  else if AnsiCompareText(ABLeftStr(aDefaultValue,Length('Count_Detail')),'Count_Detail')=0 then
  begin
    Result:=true;
  end
  else if AnsiCompareText(ABLeftStr(aDefaultValue,Length('SUM_Detail')),'SUM_Detail')=0 then
  begin
    Result:=true;
  end
  else if AnsiCompareText(ABLeftStr(aDefaultValue,Length('AVG_Detail')),'AVG_Detail')=0 then
  begin
    Result:=true;
  end;
end;

function ABFieldDefaultValueIsVar(aDataset:TABDictionaryQuery;aFieldName:string):boolean;
var
  tempFieldDef:PABFieldDef;
  tempDefaultValue:string;
begin
  Result:=false;
  tempFieldDef:=aDataset.FieldDefByFieldName(aFieldName);
  if Assigned(tempFieldDef) then
  begin
    tempDefaultValue:= tempFieldDef.Fi_DefaultValue;
    if ABNeedResetDefaultValueBeforePost(tempDefaultValue) then
    begin
      Result:=true;
    end
    else if (AnsiCompareText('Guid'   ,tempDefaultValue)=0) then
    begin
      Result:=true;
    end
    else if (AnsiCompareText('Datetime'   ,tempDefaultValue)=0) then
    begin
      Result:=true;
    end
    else if (AnsiCompareText('Date'   ,tempDefaultValue)=0) then
    begin
      Result:=true;
    end
    else if (AnsiCompareText('time'   ,tempDefaultValue)=0) then
    begin
      Result:=true;
    end
    else if ABPos('RecNo',tempDefaultValue)>0 then
    begin
      Result:=true;
    end
    else if AnsiCompareText(ABLeftStr(tempDefaultValue,Length('Recordcount')),'Recordcount')=0 then
    begin
      Result:=true;
    end
    else if AnsiCompareText(ABLeftStr(tempDefaultValue,Length('RecNo_Parent')),'RecNo_Parent')=0 then
    begin
      Result:=true;
    end
    else if AnsiCompareText(ABLeftStr(tempDefaultValue,Length('Recordcount_Parent')),'Recordcount_Parent')=0 then
    begin
      Result:=true;
    end;
  end;
end;

function ABCheckAllFieldValue(aDataSet:TABDictionaryQuery):Boolean;
var
  i:LongInt;
  tempFieldDef:PABFieldDef;
begin
  result:=True;
  if not aDataSet.CanCheckFieldValue then
    exit;

  for I := 0 to aDataSet.FieldCount - 1 do
  begin
    tempFieldDef:=aDataSet.FieldDefByFieldName(aDataSet.Fields[i].FieldName);
    if not Assigned(tempFieldDef) then  Continue;
    if (Assigned(tempFieldDef)) and
       (Assigned(tempFieldDef.PMetaDef)) and
       (tempFieldDef.PMetaDef.Fi_IsAutoAdd) then
      Continue;

    result:= ABCheckFieldValue(tempFieldDef);
    if not result then
      break;
  end;
end;

function ABCheckFieldValue(aFieldDef:PABFieldDef):Boolean;
var
  tempKeyFieldNames,
  tempErrorMsg :string;
  tempDictionaryQuery:TABDictionaryQuery;
begin
  result:=true;
  if not Assigned(aFieldDef) then
    exit;

  tempDictionaryQuery:=TABDictionaryQuery(aFieldDef.Field.DataSet);
  if (Assigned(aFieldDef)) and
     (Assigned(aFieldDef.PMetaDef)) and
     (aFieldDef.PMetaDef.Fi_IsAutoAdd) then
    exit;

  if not aFieldDef.IsCheck then
    exit;

  if not tempDictionaryQuery.CanCheckFieldValue then
    exit;

  if tempDictionaryQuery.State=dssetkey then
    exit;

  if (not aFieldDef.Field.IsNull) then
  begin
    if (aFieldDef.Fi_Max<>'') and (ABCompareNumberOrText(aFieldDef.Field.AsString,aFieldDef.Fi_Max)>0)  then
    begin
      tempErrorMsg :=Format('字段[%s]的值[%s]超出了最大值[%s],请检查.',[aFieldDef.Field.DisplayLabel,aFieldDef.Field.AsString,aFieldDef.Fi_Max]);
      ABShow(tempErrorMsg);
      ABSetControlFocusByField(aFieldDef.Field);
      result:=false;
    end
    else if (aFieldDef.Fi_Min<>'') and (ABCompareNumberOrText(aFieldDef.Field.AsString,aFieldDef.Fi_Min)<0)   then
    begin
      tempErrorMsg :=Format('字段[%s]的值[%s]超出了最小值[%s],请检查.',[aFieldDef.Field.DisplayLabel,aFieldDef.Field.AsString,aFieldDef.Fi_Min]);
      ABShow(tempErrorMsg);
      ABSetControlFocusByField(aFieldDef.Field);
      result:=false;
    end
  end;

  //检测数据字典中不能为空的处理
  if (result) and
     (not aFieldDef.Fi_CanNull) and
     ((aFieldDef.Field.IsNull) or
      (
        (aFieldDef.Field.DataType in ABStrDataType ) and
        (aFieldDef.Field.AsString=EmptyStr )
       )
      )  then
  begin
    tempErrorMsg :=Format('数据字典字段[%s]不能为空,请检查.',[aFieldDef.Field.DisplayLabel]);
    ABShow(tempErrorMsg);
    ABSetControlFocusByField(aFieldDef.Field);
    result:=false;
  end;

  //不能为空的检测在字段修改和保存前都要检测
  //因为新增后如字段未作修改的空值字段如在保存前不检测,则就会触发数据库异常
  if (result) and
     (aFieldDef.IsDown) then
  begin
    if (aFieldDef.PDownDef.Fi_CheckInDown) and
       (not aFieldDef.Field.IsNull) and
       (aFieldDef.Field.AsString<>EmptyStr) then
    begin
      if not aFieldDef.PDownDef.Fi_DataSet.Locate(aFieldDef.PDownDef.Fi_SaveField,
                                                      aFieldDef.Field.AsString,[]) then
      begin
        tempErrorMsg :=Format('字段[%s]的值[%s]不在列表中,请检查.',[aFieldDef.Field.DisplayLabel,aFieldDef.Field.Text]);
        ABShow(tempErrorMsg);
        ABSetControlFocusByField(aFieldDef.Field);
        result:=false;
      end
    end;
  end;

  //检测数据库中不能为空的处理
  if (result) and
     (Assigned(aFieldDef.PMetaDef)) and
     (not aFieldDef.PMetaDef.Fi_CanNull) and
     (aFieldDef.PMetaDef.Fi_DefaultValue='') and
     (
       (aFieldDef.Field.IsNull) or
      (
        (aFieldDef.Field.DataType in ABStrDataType ) and
        (aFieldDef.Field.AsString=EmptyStr )
       )
      )  then
  begin
    tempErrorMsg :=Format('数据库字段[%s]不能为空,请检查.',[aFieldDef.Field.DisplayLabel]);
    ABShow(tempErrorMsg);
    ABSetControlFocusByField(aFieldDef.Field);
    result:=false;
  end;

  //修改时检测的命令
  if (result) and
     (aFieldDef.Fi_SetEditCommand<>EmptyStr) then
  begin
    if not tempDictionaryQuery.CheckCommand(aFieldDef.Field.DataSet,aFieldDef.Fi_SetEditCommand) then
    begin
      ABSetControlFocusByField(aFieldDef.Field);
      result:=false;
    end;
  end;

  //检测数据集唯一值
  //框架的数据值唯一值设置不支持备注、二进制字段
  //当字段值为Null或
  //当字段为布尔型且值为False或
  //当字段为字符型且值为空或
  //当字段为为数据型且为0时不进行检测
  if (result) and
     (aFieldDef.Fi_DatasetUnique) and
     (not aFieldDef.Field.IsNull) then
  begin
    if ((aFieldDef.Field.DataType in ABStrAndMemoDataType) and (aFieldDef.Field.AsString<>EmptyStr)) or
       ((aFieldDef.Field.DataType in ABNumDataType) and (aFieldDef.Field.AsFloat<>0)) or
       ((aFieldDef.Field.DataType=ftBoolean) and (aFieldDef.Field.AsBoolean)) then
    begin
      if tempDictionaryQuery.PrimaryKeyFieldNames.Text<>EmptyStr then
      begin
        tempKeyFieldNames:=tempDictionaryQuery.PrimaryKeyFieldNames.Values[aFieldDef.Fi_Ta_Name];
        if  (tempKeyFieldNames<>EmptyStr) and
            (ABCheckDatasetRepeat(
            TABThirdReadDataQuery(aFieldDef.Field.DataSet),
            tempKeyFieldNames,
            aFieldDef.Fi_Name,
            VarArrayOf([aFieldDef.Field.Value])
            )) then
        begin
          tempErrorMsg :=Format('字段[%s]在数据集是唯一值,不能设置多值,请检查.',[aFieldDef.Field.DisplayLabel]);
          ABShow(tempErrorMsg);
          ABSetControlFocusByField(aFieldDef.Field);
          result:=false;
        end;
      end;
    end;
  end;
end;

{ TABCustomDictionaryQuery }

constructor TABCustomDictionaryQuery.Create(AOwner: TComponent);
begin
  inherited;
end;

destructor TABCustomDictionaryQuery.Destroy;
begin
  inherited;
end;

procedure TABCustomDictionaryQuery.AfterChangSQL(aNewSQL: String;var aDesigning:Boolean;var aLoading:Boolean);
begin
  inherited;
  UpdateTableNameListsAfterChangSQL(LoadTables);
end;

{ TABFieldCaptionQuery }
constructor TABFieldCaptionQuery.Create(AOwner: TComponent);
begin
  inherited;
  UpdateOptions.ReadOnly:=true;
end;

destructor TABFieldCaptionQuery.Destroy;
begin

  inherited;
end;

procedure TABFieldCaptionQuery.DoAfterOpen;
var
  i:longint;
  tempTableName:string;
  tempFi_Name:string;
  tempDataset:TDataSet;
  tempDoFieldList:TStrings;
  tempField:TField;
begin
  inherited;

  tempDoFieldList:=TStringList.Create;
  try
    for i := 0 to LoadTables.Count-1 do
    begin
      tempTableName:=trim(LoadTables[i]);
      if (tempTableName<>EmptyStr) then
      begin
        tempDataset:= ABGetConstSqlPubDataset('ABSys_Org_FieldCaption',[ABGetDatabaseByConnName(ConnName),tempTableName]);
        if (Assigned(tempDataset))  then
        begin
          tempDataset.First;
          while not tempDataset.Eof do
          begin
            tempFi_Name:=tempDataset.FindField(trim('Fi_Name')).AsString;
            tempField:= FindField(tempFi_Name);
            if (Assigned(tempField)) and
               (tempDoFieldList.IndexOf(tempFi_Name)<0)then
            begin
              tempField.DisplayLabel:= tempDataset.FindField(trim('Fi_Caption')).AsString;
              tempDoFieldList.Add(tempFi_Name);
            end;
            tempDataset.Next;
          end;
        end;
      end;
    end;
  finally
    tempDoFieldList.Free;
  end;
end;

{ TABDictionaryQuery }

function TABDictionaryQuery.GetFieldDef(Index: Integer): PABFieldDef;
begin
  Result:=nil;
  if Index<FDefList.Count then
    Result:=PABFieldDef(FDefList.Objects[Index]);
end;

function TABDictionaryQuery.GetFuncFileName: String;
begin
  if (FFuncFileName=emptystr) then
  begin
    FFuncFileName:=ABGetBPLFileName(Owner);
  end;
  result:=FFuncFileName;
end;

function TABDictionaryQuery.GetFuncGuid: String;
begin
  if (FFuncGuid=emptystr) then
  begin
    FFuncGuid:=ABGetFuncFieldValueByFuncFileName(GetFuncFileName,'Fu_Guid');
  end;
  result:=FFuncGuid;
end;

function TABDictionaryQuery.GetFuncFieldValue(aFieldName: String): String;
begin
  result:=ABGetFuncFieldValueByFuncFileName(GetFuncFileName,aFieldName);
end;

procedure TABDictionaryQuery.FieldGetText(aField: TField; var Text: String; DisplayText: Boolean);
var
  tempBoolean:Boolean;
begin
  tempBoolean:=false;
  AfterFieldGetText(aField,Text,DisplayText,tempBoolean);
end;

procedure TABDictionaryQuery.FieldSetText(aField: TField; const Text: string);
var
  tempBoolean:Boolean;
begin
  tempBoolean:=false;
  AfterFieldSetText(aField,Text,tempBoolean);
end;

procedure TABDictionaryQuery.FieldValidate(aField: TField);
var
  tempBoolean:Boolean;
begin
  tempBoolean:=false;
  AfterFieldValidate(aField,tempBoolean);
end;

function TABDictionaryQuery.FieldCanCopy(aFieldName: string): boolean;
var
  tempFieldDef:PABFieldDef;
begin
  result:=inherited FieldCanCopy(aFieldName);
  if result then
  begin
    tempFieldDef:=FieldDefByFieldName(aFieldName);
    if (Assigned(tempFieldDef))  and
       (tempFieldDef.Fi_Copy) and
       (not ABFieldDefaultValueIsVar(self,aFieldName)) then
    begin
      result:=true;
    end
    else
    begin
      result:=False;
    end;
  end;
end;

procedure TABDictionaryQuery.FieldChange(aField: TField);
var
  tempBoolean:Boolean;
begin
  tempBoolean:=false;
  AfterFieldChange(aField,tempBoolean);
end;

procedure TABDictionaryQuery.DoAfterCancel;
begin
  inherited;
  CancelBlobFieldDatasets;
end;

procedure TABDictionaryQuery.DoAfterEdit;
begin
  inherited;
  SetFieldsEdited(false);
  EditBlobFieldDatasets;
end;

procedure TABDictionaryQuery.DoAfterInsert;
begin
  SetFieldsEdited(false);
  //数据字典默认值字段处理
  FillDefaultValue(True,False);

  inherited;
end;

procedure TABDictionaryQuery.ADDDatasetExtFields;
var
  i:LongInt;
  tempTableName:string;
  tempAddBaseField:boolean;
  tempDataset:TDataSet;
begin
  if (ABLocalParams.LoginType=ltCS_Two) and
     (LoadTables.Count>0) then
  begin
    tempAddBaseField:=false;
    for i := 0 to LoadTables.Count-1 do
    begin
      tempTableName:=trim(LoadTables[i]);
      if tempTableName=emptystr then
        exit;

      tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_Table',[ABGetDatabaseByConnName(ConnName),tempTableName]);
      if (Assigned(tempDataset)) and
         ((tempDataset.FieldByName('TA_CalcFieldsDef').AsString<>EmptyStr) or
          (tempDataset.FieldByName('TA_AggregateFieldsDef').AsString<>EmptyStr)
          )  then
      begin
        ABAddDatasetExtFields( self,
                               tempDataset.FieldByName('TA_CalcFieldsDef').AsString,
                               tempDataset.FieldByName('TA_AggregateFieldsDef').AsString,
                               tempAddBaseField);
      end;
    end;
  end;
end;

//Active:=true时的顺序(9)-->从2结束来
procedure TABDictionaryQuery.DoAfterOpen;
  procedure ABSetDatasetExtFields(aDataset:TFireDACQuery;aCalcFieldsDef:string);
  var
    j:LongInt;
    tempDef:TStrings;
    tempCurLineDef,
    tempFieldName,tempFieldCaption:string;
    tempField:TField;
  begin
    tempDef := TStringList.Create;
    try
      if (aCalcFieldsDef<>EmptyStr) then
      begin
        tempDef.Text:=aCalcFieldsDef;
        for j := 0 to tempDef.Count-1 do
        begin
          tempCurLineDef:=tempDef[j];
          tempFieldName:=ABGetLeftRightStr(tempCurLineDef);
          tempCurLineDef:=ABGetLeftRightStr(tempCurLineDef,axdRight);
          tempFieldCaption:=ABGetLeftRightStr(tempCurLineDef);
          if (tempFieldName<>EmptyStr) and
             (tempFieldCaption<>EmptyStr) and
             (Assigned(aDataset.FindField(tempFieldName))) then
          begin
            tempField :=aDataset.FindField(tempFieldName);
            tempField.DisplayLabel := tempFieldCaption;
          end;
        end;
      end;
    finally
      tempDef.Free;
    end;
  end;
  procedure SetDatasetExtFields;
  var
    i:LongInt;
    tempTableName:string;
    tempDataset:TDataSet;
  begin
    if (ABLocalParams.LoginType=ltCS_Three) and
       (LoadTables.Count>0) then
    begin
      for i := 0 to LoadTables.Count-1 do
      begin
        tempTableName:=trim(LoadTables[i]);
        if tempTableName=emptystr then
          exit;

        tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_Table',[ABGetDatabaseByConnName(ConnName),tempTableName]);
        if (Assigned(tempDataset)) and
           ((tempDataset.FieldByName('TA_CalcFieldsDef').AsString<>EmptyStr) 
            )  then
        begin
          ABSetDatasetExtFields( self,
                                 tempDataset.FieldByName('TA_CalcFieldsDef').AsString);
        end;
      end;
    end;
  end;

begin
  inherited;

  CreateFieldExtInfos;
  LinkBlobFieldDatasets;

  SetDatasetExtFields;
end;

procedure TABDictionaryQuery.DoBeforePost;
begin
  if (State in [dsEdit,dsInsert]) then
  begin
    FillDefaultValue(False,true);

    if not ABCheckAllFieldValue(self) then
      Abort;
  end;

  inherited;
end;

//Active:=true时的顺序(1)
procedure TABDictionaryQuery.AfterFieldGetText(aField: TField;
  var Text: String; DisplayText: Boolean; var aDo: Boolean);
var
  tempFieldDef:PABFieldDef;
  tempStrSUM1:String;
begin
  aDo:=false;
  tempFieldDef:=FieldDefByFieldName(aField.FieldName);
  if (Assigned(tempFieldDef)) then
  begin
    if (not tempFieldDef.Fi_LockGetText) then
    begin
      tempFieldDef.Fi_LockGetText:=true;
      try
        if Text<>aField.AsString then
        begin
          Text:=aField.AsString;
        end;

        if (tempFieldDef.IsDown) and
           (tempFieldDef.PDownDef.Fi_SaveField<>tempFieldDef.PDownDef.Fi_ViewField) and
           (tempFieldDef.Fi_ControlType='TABcxDBTreeViewPopupEdit')
               then
        begin
          tempStrSUM1:=VarToStrDef(tempFieldDef.PDownDef.Fi_DataSet.Lookup(
                                   tempFieldDef.PDownDef.Fi_SaveField,aField.AsString,
                                   ABStringReplace(tempFieldDef.PDownDef.Fi_ViewField,',',';')),'');

          Text:=tempStrSUM1;

          aDo:=true;
        end;
      finally
        tempFieldDef.Fi_LockGetText:=false;
      end;
    end;
  end;

  //字段原有的FOnGetText事件
  if (Assigned(tempFieldDef.PEventDef)) and
     (Assigned(tempFieldDef.PEventDef.FOnGetText)) then
    tempFieldDef.PEventDef.FOnGetText(aField,Text,DisplayText);

  //数据集加入的字段FOnGetText事件
  if Assigned(FOnFieldGetText) then
    FOnFieldGetText(aField, Text, DisplayText);
end;

procedure TABDictionaryQuery.AfterFieldSetText(aField: TField;
  const Text: string; var aDo: Boolean);
var
  tempFieldDef:PABFieldDef;
  tempStrSUM1:String;
begin
  aDo:=false;
  tempFieldDef:=FieldDefByFieldName(aField.FieldName);
  if (Assigned(tempFieldDef)) then
  begin
    if (not tempFieldDef.Fi_LockSetText) then
    begin
      tempFieldDef.Fi_LockSetText:=true;
      try
        if Text<>aField.AsString then
        begin
          aField.AsString := Text;
        end;

        if (tempFieldDef.IsDown) and
           (tempFieldDef.PDownDef.Fi_SaveField<>tempFieldDef.PDownDef.Fi_ViewField) and
           (tempFieldDef.Fi_ControlType='TABcxDBTreeViewPopupEdit')  then
        begin
          tempStrSUM1:=VarToStrDef(tempFieldDef.PDownDef.Fi_DataSet.Lookup(
                                ABStringReplace(tempFieldDef.PDownDef.Fi_ViewField,',',';'),Text,
                                tempFieldDef.PDownDef.Fi_SaveField),'');
          aField.AsString:=tempStrSUM1;
          aDo:=true;
        end;
      finally
        tempFieldDef.Fi_LockSetText:=false;
      end;
    end;
  end;

  //字段原有的FOnSetText事件
  if (Assigned(tempFieldDef.PEventDef)) and
     (Assigned(tempFieldDef.PEventDef.FOnSetText)) then
    tempFieldDef.PEventDef.FOnSetText(aField,Text);
  //数据集加入的字段FOnSetText事件
  if Assigned(FOnFieldSetText) then
    FOnFieldSetText(aField, Text);
end;

procedure TABDictionaryQuery.AfterFieldValidate(aField: TField;
  var aDo: Boolean);
var
  tempFieldDef:PABFieldDef;
begin
  aDo:=False;
  tempFieldDef:=FieldDefByFieldName(aField.FieldName);
  if (Assigned(tempFieldDef)) then
  begin
    if (not tempFieldDef.Fi_LockValidate)  then
    begin
      tempFieldDef.Fi_LockValidate:=true;
      try
        if FCanCheckFieldValue then
        begin
          aDo:=true;
          if not ABCheckFieldValue(tempFieldDef) then
            Abort;
        end;
      finally
        tempFieldDef.Fi_LockValidate:=false;
      end;
    end;
  end;

  //字段原有的FOnValidate事件
  if (Assigned(tempFieldDef.PEventDef)) and
     (Assigned(tempFieldDef.PEventDef.FOnValidate)) then
    tempFieldDef.PEventDef.FOnValidate(aField);

  //数据集加入的字段FOnValidate事件
  if Assigned(FOnFieldValidate) then
    FOnFieldValidate(aField);
end;

procedure TABDictionaryQuery.AfterChangSQL(aNewSQL: String; var aDesigning,
  aLoading: Boolean);
begin
  inherited;
  FTableRowQueryWhere:=EmptyStr;
  FSysUseQueryWhere  :=EmptyStr;
end;

procedure TABDictionaryQuery.AfterFieldChange(aField: TField;
  var aDo: Boolean);
var
  tempFieldDef:PABFieldDef;
begin
  aDo:=false;
  tempFieldDef:=FieldDefByFieldName(aField.FieldName);
  if (Assigned(tempFieldDef)) then
  begin
    if (not tempFieldDef.Fi_LockChange)  then
    begin
      tempFieldDef.Fi_LockChange:=true;
      try
        tempFieldDef.Fi_Edited:=true;
        if  State<>dssetkey then
        begin
          aDo:=true;
        end;
      finally
        tempFieldDef.Fi_LockChange:=false;
      end;
    end;
  end;

  //字段原有的FOnChange事件
  if (Assigned(tempFieldDef.PEventDef)) and
     (Assigned(tempFieldDef.PEventDef.FOnChange)) then
    tempFieldDef.PEventDef.FOnChange(aField);

  //数据集加入的字段FOnChange事件
  if Assigned(FOnFieldChange) then
    FOnFieldChange(aField);
end;

function TABDictionaryQuery.GetEspecialFieldName(aType:string):string;
var
  I: Integer;
  tempFieldName:String;
begin
  result:=EmptyStr;
  tempFieldName:=EmptyStr;
  for I := 0 to FieldCount-1 do
  begin
    if abpos('_',fields[i].FieldName)>0 then
    begin
      tempFieldName:=ABGetLeftRightStr(fields[i].FieldName,axdLeft,'_');
      Break;
    end
  end;

  if tempFieldName<>EmptyStr then
  begin
    if AnsiCompareText(aType,'Order')=0 then
    begin
      tempFieldName:=tempFieldName+'_'+'Order';
    end
    else if AnsiCompareText(aType,'SysUse')=0 then
    begin
      tempFieldName:=tempFieldName+'_'+'SysUse';
    end;

    if Assigned(FindField(tempFieldName)) then
    begin
      result:=tempFieldName;
    end;
  end;
end;

procedure TABDictionaryQuery.GetInsertOrderValue;
var
  tempFieldName:String;
begin
  if (FieldCount>1) then
  begin
    tempFieldName:=GetEspecialFieldName('Order');

    if (Assigned(FindField(tempFieldName))) and
       (FindField(tempFieldName).DataType in ABNumDataType ) then
    begin
      FInsertOrderValue:=FindField(tempFieldName).AsInteger;
    end;
  end;
end;

procedure TABDictionaryQuery.SetInsertOrderValue;
var
  tempFieldName:String;
begin
  if (FieldCount>1) then
  begin
    tempFieldName:=GetEspecialFieldName('Order');
    if (Assigned(FindField(tempFieldName))) and
       (FindField(tempFieldName).DataType in ABNumDataType ) then
    begin
      FindField(tempFieldName).AsInteger:=FInsertOrderValue-1;
    end;
  end;
end;

function TABDictionaryQuery.FieldDefBySetOrder(aSetOrder: Integer): PABFieldDef;
var
  i:Longint;
begin
  Result:=nil;
  for I := 0 to FDefList.Count - 1 do
  begin
    if PABFieldDef(FDefList.Objects[i]).fi_SetOrder= aSetOrder then
    begin
      Result:=PABFieldDef(FDefList.Objects[i]);
      break;
    end;
  end;
end;

procedure TABDictionaryQuery.SetFieldsEdited(aValue: Boolean);
var
  i:longint;
begin
  for i := 0 to FDefList.Count-1 do
  begin
    PABFieldDef(FDefList.Objects[i]).Fi_Edited:=aValue;
  end;
end;

function TABDictionaryQuery.FieldsIsEdited: Boolean;
var
  i:longint;
begin
  result:=False;
  for i := 0 to FDefList.Count-1 do
  begin
    if PABFieldDef(FDefList.Objects[i]).Fi_Edited then
    begin
      result:=true;
      break;
    end;
  end;
end;

procedure TABDictionaryQuery.FillDefaultValue(aIsAfterAppend,aIsBeforePost:Boolean;
                                                 aHaveField:string;aNoHaveField:string);
var
  i:longint;
  tempFieldDef:PABFieldDef;
  tempVariant:Variant ;
  tempExpressionStr:string;
  tempDefaultValue,tempStr1,tempStr2,tempStr3,
  tempLength,
  tempBeginStr,
  tempEndStr,
  tempBeginWhere,
  tempEndWhere:string;

  tempBeginStrLength,tempEndStrLength:LongInt;

  tempDetail:Boolean;
  tempDoPubID,
  tempError:boolean;

  function GetSQL(aType,aAddNumber:string):string;
  var
    i:LongInt;
    tempMainField,tempDetailField:string;
    tempSelect,tempWhere,tempDetailWhere:string;
  begin
    tempSelect:=EmptyStr;
    tempWhere:=EmptyStr;
    tempDetailWhere:=EmptyStr;

    if tempDetail then
    begin
      if  (Assigned(MasterSource)) and
          (Assigned(MasterSource.DataSet)) and
          (DetailFields<>EmptyStr) and (MasterFields<>EmptyStr) then
      begin
        for i := 1 to ABGetSpaceStrCount(DetailFields,';') do
        begin
          tempMainField:=ABGetSpaceStr(MasterFields,i,';');
          tempDetailField:=ABGetSpaceStr(DetailFields,i,';');
          tempDetailWhere:=tempDetailWhere+' and '+tempDetailField+'='+QuotedStr(MasterSource.DataSet.FieldByName(tempMainField).AsString);
        end;
      end;
    end;

    if (tempBeginStr=EmptyStr) and (tempEndStr=EmptyStr) then
    begin
      tempSelect:=' isnull('+aType+'(cast('+tempFieldDef.Fi_Name+' as int)),0) '+aAddNumber+' ';   //无前后缀
    end
    else if (tempBeginStr<>EmptyStr) and (tempEndStr=EmptyStr) then
    begin
      tempSelect:=' isnull('+aType+'(cast(substring('+tempFieldDef.Fi_Name+','+inttostr(tempBeginStrLength+1)+',len('+tempFieldDef.Fi_Name+')) as int)),0) '+aAddNumber+' ';   //仅有前缀
    end
    else if (tempBeginStr=EmptyStr) and (tempEndStr<>EmptyStr) then
    begin
      tempSelect:=' isnull('+aType+'(cast(substring('+tempFieldDef.Fi_Name+',1,len('+tempFieldDef.Fi_Name+')-'+inttostr(tempEndStrLength)+') as int)),0) '+aAddNumber+' ';   //仅有后缀
    end
    else if (tempBeginStr<>EmptyStr) and (tempEndStr<>EmptyStr) then
    begin
      tempSelect:=' isnull('+aType+'(cast(substring('+tempFieldDef.Fi_Name+','+inttostr(tempBeginStrLength+1)+',len('+tempFieldDef.Fi_Name+')-'+inttostr(tempBeginStrLength)+'-'+inttostr(tempEndStrLength)+') as int)),0) '+aAddNumber+' ';   //有前后缀
    end;

    if (tempBeginStr=EmptyStr) and (tempEndStr=EmptyStr) then
    begin
      tempWhere:=' ISNUMERIC('+tempFieldDef.Fi_Name+')=1 ';                         //无前后缀
    end
    else if (tempBeginStr<>EmptyStr) and (tempEndStr=EmptyStr) then
    begin
      tempWhere:=' ISNUMERIC(substring('+tempFieldDef.Fi_Name+','+inttostr(tempBeginStrLength+1)+',len('+tempFieldDef.Fi_Name+')))=1 ' ; //仅有前缀
    end
    else if (tempBeginStr=EmptyStr) and (tempEndStr<>EmptyStr) then
    begin
      tempWhere:=' ISNUMERIC(substring('+tempFieldDef.Fi_Name+',1,len('+tempFieldDef.Fi_Name+')-'+inttostr(tempEndStrLength)+'))=1 ' ; //仅有后缀
    end
    else if (tempBeginStr<>EmptyStr) and (tempEndStr<>EmptyStr) then
    begin
      tempWhere:=' ISNUMERIC(substring('+tempFieldDef.Fi_Name+','+inttostr(tempBeginStrLength+1)+',len('+tempFieldDef.Fi_Name+')-'+inttostr(tempBeginStrLength)+'-'+inttostr(tempEndStrLength)+'))=1 '; //有前后缀
    end;

    result:='select '+tempSelect+' from '+tempFieldDef.Fi_Ta_Name+' where '+tempWhere+' '+tempDetailWhere+tempBeginWhere+tempEndWhere;
  end;
begin
  if not ((Active) and (state in [dsEdit,dsInsert])) then
    exit;

  tempDoPubID:=False;
  for i := 0 to FieldCount-1  do
  begin
    if (not tempDoPubID) and
       (state in [dsInsert]) and
       (AnsiCompareText('PubID',Fields[i].FieldName)=0) then
    begin
      Fields[i].AsString:=ABGetGuid;
      tempDoPubID:=true;
      Continue;
    end;

    tempFieldDef:=FieldDefByFieldName(Fields[i].FieldName);
    if not Assigned(tempFieldDef) then Continue;

    if ((ABPos(','+Fields[i].FieldName+',',','+aNoHaveField  +',')<=0)) and
       ((ABPos(','+Fields[i].FieldName+',',','+aHaveField+',')> 0)) or
       (
         (aIsAfterAppend)  and (dtAfterAppend in tempFieldDef.Fi_DefaultValueMakeTime) or
         (
          (state=dsInsert) and (aIsBeforePost) and
          (
           (dtBeforeAppendSave in tempFieldDef.Fi_DefaultValueMakeTime) or
           (ABNeedResetDefaultValueBeforePost(tempFieldDef.Fi_DefaultValue) and (not tempFieldDef.Fi_Edited))
           )
          ) or
         (state=dsedit)   and (dtBeforeEditSave in tempFieldDef.Fi_DefaultValueMakeTime)
       ) then
    begin
      tempDefaultValue:=tempFieldDef.Fi_DefaultValue;
      if (tempDefaultValue<>EmptyStr)    then
      begin
        tempExpressionStr:=EmptyStr;
        tempVariant:=Null;
        tempBeginStrLength:=0;
        tempEndStrLength:=0;
        tempBeginStr :=Emptystr;
        tempEndStr   :=Emptystr;
        tempLength   :=Emptystr;
        tempBeginWhere  :=Emptystr;
        tempEndWhere    :=Emptystr;
        if length(tempDefaultValue)<=2 then
        begin
          tempVariant:=tempDefaultValue;
        end
        else
        begin
          tempBeginStr :=Trim(ABGetItemValue(tempDefaultValue,'BeginStr','=',' '));
          if tempBeginStr<>EmptyStr then
          begin
            tempDefaultValue:=Trim(ABStringReplace(tempDefaultValue,'BeginStr='+tempBeginStr,EmptyStr));
            tempBeginStrLength:=Length(tempBeginStr);
            tempBeginWhere:=' and left('+tempFieldDef.Fi_Name+','+inttostr(tempBeginStrLength)+')='+QuotedStr(tempBeginStr);
          end;

          tempEndStr :=Trim(ABGetItemValue(tempDefaultValue,'EndStr','=',' '));
          if tempEndStr<>EmptyStr then
          begin
            tempDefaultValue:=Trim(ABStringReplace(tempDefaultValue,'EndStr='+tempEndStr,EmptyStr));
            tempEndStrLength:=Length(tempEndStr);
            tempEndWhere:=' and right('+tempFieldDef.Fi_Name+','+inttostr(tempEndStrLength)+')='+QuotedStr(tempEndStr);
          end;

          tempLength :=Trim(ABGetItemValue(tempDefaultValue,'SetLength','=',' '));
          if tempLength<>EmptyStr then
          begin
            tempDefaultValue:=Trim(ABStringReplace(tempDefaultValue,'SetLength='+tempLength,EmptyStr));
          end;

          if (AnsiCompareText('Operator_Guid' ,tempDefaultValue)=0) then
          begin
            tempVariant:=ABPubUser.Guid;
          end
          else if (AnsiCompareText('Operator_Code' ,tempDefaultValue)=0) then
          begin
            tempVariant:=ABPubUser.Code;
          end
          else if (AnsiCompareText('Operator_Name' ,tempDefaultValue)=0) then
          begin
            tempVariant:=ABPubUser.Name;
          end
          else if (AnsiCompareText('Guid'   ,tempDefaultValue)=0) then
          begin
            tempVariant:=ABGetGuid;
          end
          else if (AnsiCompareText('Datetime'   ,tempDefaultValue)=0) then
          begin
            if StopSQLDefaultValueAfterInsert then
              Continue;
            tempVariant:=ABGetServerDateTime;
          end
          else if (AnsiCompareText('Date'   ,tempDefaultValue)=0) then
          begin
            if StopSQLDefaultValueAfterInsert then
              Continue;
            tempVariant:=dateof(ABGetServerDateTime);
          end
          else if (AnsiCompareText('time'   ,tempDefaultValue)=0) then
          begin
            if StopSQLDefaultValueAfterInsert then
              Continue;
            tempVariant:=ABTimeToStr(ABGetServerDateTime);
          end
          else if (AnsiCompareText('Computer_Name'   ,tempDefaultValue)=0) then
          begin
            tempVariant:=ABPubUser.HostName;
          end
          else if (AnsiCompareText('Computer_IP'     ,tempDefaultValue)=0) then
          begin
            tempVariant:=ABPubUser.HostIP;
          end
          else if (AnsiCompareText('Computer_MAC'     ,tempDefaultValue)=0) then
          begin
            tempVariant:=ABPubUser.MacAddress;
          end
          else if AnsiCompareText(ABLeftStr(tempDefaultValue,Length('Locate:')),'Locate:')=0 then
          begin
            if (tempFieldDef.IsDown) and
               (tempFieldDef.PDownDef.Fi_DataSet.Active) then
            begin
              tempStr1:=ABStringReplace(tempDefaultValue,'Locate:',EmptyStr);

              tempStr2 :=Trim(ABGetLeftRightStr(tempStr1,axdRight));
              tempStr1 :=Trim(ABGetLeftRightStr(tempStr1,axdLeft));
              if (tempStr1<>EmptyStr) and
                 (tempFieldDef.PDownDef.Fi_SaveField<>EmptyStr) and
                 (Assigned(tempFieldDef.PDownDef.Fi_DataSet.FindField(tempStr1))) then
                tempVariant:=tempFieldDef.PDownDef.Fi_DataSet.Lookup(tempStr1,tempStr2,tempFieldDef.PDownDef.Fi_SaveField);
            end;
          end
          else if (AnsiCompareText(ABLeftStr(tempDefaultValue,Length('DatetimeNumber')),'DatetimeNumber')=0) then
          begin
            if StopSQLDefaultValueAfterInsert then
              Continue;

            tempStr2 :=Trim(ABGetItemValue(tempDefaultValue,'DateLength','=',' '));
            tempStr3 :=Trim(ABGetItemValue(tempDefaultValue,'NumberLength','=',' '));

            if tempStr2=emptystr then
              tempStr2:='8';
            if tempStr3=emptystr then
              tempStr3:='4';

            tempVariant:=ABGetSQLValue(ConnName,
              ' select  convert(nvarchar('+tempStr2+'),getdate(),112)+'+
              '  RIGHT(''0000000000000000000000000000000000000000000000000000000000000000''+'+
              '  cast(isnull((max(cast(substring('+tempFieldDef.Fi_Name+','+tempStr2+'+'+inttostr(tempBeginStrLength+1)+','+tempStr3+') as int))),0)+1 as nvarchar),'+tempStr3+')'+
              ' from '+tempFieldDef.Fi_Ta_Name+' '+
              ' where substring('+tempFieldDef.Fi_Name+','+inttostr(tempBeginStrLength+1)+','+tempStr2+')=convert(nvarchar('+tempStr2+'),getdate(),112) and '+
              '       ISNUMERIC(substring('+tempFieldDef.Fi_Name+','+tempStr2+'+'+inttostr(tempBeginStrLength+1)+','+tempStr3+'))=1 '+tempBeginWhere+tempEndWhere,
                         [],'0');
          end
          else if (AnsiCompareText(ABLeftStr(tempDefaultValue,Length('Max')),'Max')=0) then
          begin
            if StopSQLDefaultValueAfterInsert then
              Continue;

            tempStr1 :=Trim(ABGetItemValue(tempDefaultValue,'(',')',')'));
            tempDetail:=abpos('_Detail',tempDefaultValue)>0;

            if ABIsFloat(tempStr1) then
            begin
              tempStr1:='+('+tempStr1+')';
            end
            else
            begin

            end;

            tempVariant:=ABGetSQLValue(ConnName,GetSQL('max',tempStr1),[],0);
          end
          else if AnsiCompareText(ABLeftStr(tempDefaultValue,Length('Min')),'Min')=0 then
          begin
            if StopSQLDefaultValueAfterInsert then
              Continue;

            tempStr1 :=Trim(ABGetItemValue(tempDefaultValue,'(',')',')'));
            tempDetail:=abpos('_Detail',tempDefaultValue)>0;

            if ABIsFloat(tempStr1) then
            begin
              tempStr1:='+('+tempStr1+')';
            end
            else
            begin

            end;

            tempVariant:=ABGetSQLValue(ConnName,GetSQL('Min',tempStr1),[],0);
          end
          else if AnsiCompareText(ABLeftStr(tempDefaultValue,Length('Count')),'Count')=0 then
          begin
            if StopSQLDefaultValueAfterInsert then
              Continue;

            tempStr1 :=Trim(ABGetItemValue(tempDefaultValue,'(',')',')'));
            tempDetail:=abpos('_Detail',tempDefaultValue)>0;

            if ABIsFloat(tempStr1) then
            begin
              tempStr1:='+('+tempStr1+')';
            end
            else
            begin

            end;

            tempVariant:=ABGetSQLValue(ConnName,GetSQL('Count',tempStr1),[],0);
          end
          else if AnsiCompareText(ABLeftStr(tempDefaultValue,Length('SUM')),'SUM')=0 then
          begin
            if StopSQLDefaultValueAfterInsert then
              Continue;

            tempStr1 :=Trim(ABGetItemValue(tempDefaultValue,'(',')',')'));
            tempDetail:=abpos('_Detail',tempDefaultValue)>0;

            if ABIsFloat(tempStr1) then
            begin
              tempStr1:='+('+tempStr1+')';
            end
            else
            begin

            end;

            tempVariant:=ABGetSQLValue(ConnName,GetSQL('SUM',tempStr1),[],0);
          end
          else if AnsiCompareText(ABLeftStr(tempDefaultValue,Length('AVG')),'AVG')=0 then
          begin
            if StopSQLDefaultValueAfterInsert then
              Continue;

            tempStr1 :=Trim(ABGetItemValue(tempDefaultValue,'(',')',')'));
            tempDetail:=abpos('_Detail',tempDefaultValue)>0;

            if ABIsFloat(tempStr1) then
            begin
              tempStr1:='+('+tempStr1+')';
            end
            else
            begin

            end;

            tempVariant:=ABGetSQLValue(ConnName,GetSQL('AVG',tempStr1),[],0);
          end
          else if AnsiCompareText(ABLeftStr(tempDefaultValue,Length('RecNo')),'RecNo')=0 then
          begin
            tempExpressionStr:=ABStringReplace(tempDefaultValue,'RecNo',IntToStr(ABTrimInt(RecNo,0,RecNo)));
          end
          else if AnsiCompareText(ABLeftStr(tempDefaultValue,Length('Recordcount')),'Recordcount')=0 then
          begin
            tempExpressionStr:=ABStringReplace(tempDefaultValue,'Recordcount',IntToStr(ABTrimInt(RecordCount,0,RecordCount)+1));
          end
          else if AnsiCompareText(ABLeftStr(tempDefaultValue,Length('RecNo_Parent')),'RecNo_Parent')=0 then
          begin
            tempExpressionStr:=ABStringReplace(tempDefaultValue,'RecNo_Parent',IntToStr(ABTrimInt(DataSource.DataSet.RecNo,0,DataSource.DataSet.RecNo)));
          end
          else if AnsiCompareText(ABLeftStr(tempDefaultValue,Length('Recordcount_Parent')),'Recordcount_Parent')=0 then
          begin
            tempExpressionStr:=ABStringReplace(tempDefaultValue,'Recordcount_Parent',IntToStr(ABTrimInt(DataSource.DataSet.RecordCount,0,DataSource.DataSet.RecordCount)));
          end
          else
          begin
            tempVariant:=GetFieldExtDefaultValue(Fields[i].FieldName,tempDefaultValue);
          end;

          if tempExpressionStr<>EmptyStr then
          begin
            tempVariant:= ABExpressionEval(tempExpressionStr,tempError);
          end;
        end;

        if (VarToStrDef(tempVariant,EmptyStr)<>EmptyStr) then
        begin
          if (tempLength<>EmptyStr) and (StrToInt(tempLength)>0) then
          begin
            tempVariant:=ABFillStr(VarToStrDef(tempVariant,EmptyStr),StrToInt(tempLength));
          end;

          if tempBeginStr<>Emptystr then
            tempVariant:=tempBeginStr+VarToStrDef(tempVariant,EmptyStr);
          if tempEndStr<>Emptystr then
            tempVariant:=VarToStrDef(tempVariant,EmptyStr)+tempEndStr;

          //如果值不在当前数据集中则不设置此值，以防出现主从不一致的情况
          if (AnsiCompareText(VarToStrDef(tempVariant,EmptyStr),'0')<>0) and
             (Assigned(tempFieldDef.PDownDef)) and
             (Assigned(tempFieldDef.PDownDef.Fi_DataSet))  and
             (tempFieldDef.PDownDef.Fi_DataSet.Active) and
             (AnsiCompareText(tempFieldDef.PDownDef.Fi_SaveField,tempFieldDef.PDownDef.Fi_ViewField)<>0) and
             (abpos(',',VarToStrDef(tempVariant,EmptyStr))<=0) then
          begin
            if not tempFieldDef.PDownDef.Fi_DataSet.Locate(tempFieldDef.PDownDef.Fi_SaveField,VarToStrDef(tempVariant,EmptyStr),[]) then
              Continue;
          end;
          ABSetFieldValue(tempVariant,Fields[i]);
        end;
      end;
    end;
  end;
end;

function TABDictionaryQuery.FieldDefByFieldName(const FieldName: String): PABFieldDef;
var
  i:Longint;
begin
  Result:=nil;
  i:=FDefList.IndexOf(FieldName);
  if i>=0  then
  begin
    Result:=PABFieldDef(FDefList.Objects[i]);
  end;
end;

procedure TABDictionaryQuery.SetTableInfo;
  procedure ClearIndexDefs;
  var
    i:longint;
  begin
    for I :=IndexListDefs.Count - 1 downto  0  do
    begin
      IndexListDefs.Delete(i);
    end;
  end;
var
  ii:LongInt;
  tempTableName:string;
  tempDataSet: TDataSet;
  tempABPRIMARY_KEY_FieldNames,
  tempABClustered_FieldNames,
  IdxName, FieldNames: string;
  Options: TIndexOptions;
  IdxDef: TIndexDef;
  tempField:TField;
begin
  if (not CanModify) then
    exit;

  tempABPRIMARY_KEY_FieldNames:=EmptyStr;
  ClearIndexDefs;

  for ii := 0 to LoadTables.Count-1 do
  begin
    tempTableName:=trim(LoadTables[ii]);
    tempDataSet:=ABGetConstSqlPubDataset('ABSys_Temp_GetTableIndexInfo',[ABGetDatabaseByConnName(ConnName),tempTableName]);
    if (Assigned(tempDataset))  then
    begin
      tempDataSet.First;
      while not tempDataSet.EOF do
      begin
        Options := [];
        IdxName := tempDataSet.FindField('INDEX_NAME').Value;

        if tempDataSet.FindField('PRIMARY_KEY').AsBoolean then
          ABAddstr(tempABPRIMARY_KEY_FieldNames,tempDataSet.FindField('COLUMN_NAME').AsString,';');
        if tempDataSet.FindField('clustered').AsBoolean then
          ABAddstr(tempABClustered_FieldNames,tempDataSet.FindField('COLUMN_NAME').AsString,';');
        //if ('' = '') or (IdxName = '') then
        begin
          if IndexListDefs.IndexOf(IdxName) = -1 then
          begin
            FieldNames := tempDataSet.FindField('COLUMN_NAME').Value;
            // don't add indexes on fields not in result set
            if Assigned(FindField(FieldNames)) then
            begin
              if (tempDataSet.FindField('PRIMARY_KEY').AsBoolean) then
              begin
                Options := Options + [ixPrimary];
              end;
              if (tempDataSet.FindField('UNIQUE').AsBoolean) then
              begin
                Options := Options + [ixUnique];
              end;

              if tempDataSet.FindField('COLLATION').AsInteger = 2 then
                Options := Options + [ixDescending];
              IndexListDefs.Add(IdxName, FieldNames, Options+[]);
            end;
          end
          else
          begin
            IdxDef := IndexListDefs.Find(IdxName);
            IdxDef.Fields := IdxDef.Fields + ';' + tempDataSet.FindField('COLUMN_NAME').Value;
          end;

          if (tempDataSet.FindField('PRIMARY_KEY').AsBoolean) then
          begin
            tempField:=FindField(tempDataSet.FindField('COLUMN_NAME').Value);
            if Assigned(tempField) then
            begin
              tempField.ProviderFlags:=tempField.ProviderFlags + [pfInKey];
            end;
          end;
        end;
        tempDataSet.Next;
      end;

      if tempABPRIMARY_KEY_FieldNames<>emptystr then
        FPrimaryKeyFieldNames.Add(tempTableName+'='+tempABPRIMARY_KEY_FieldNames);
      if tempABClustered_FieldNames<>emptystr then
        FClusteredFieldNames.Add(tempTableName+'='+tempABClustered_FieldNames);
    end;
  end;
end;

procedure TABDictionaryQuery.ShowDBPanel(
  aCanEdit: boolean;
  aActivePageCaption,
  aEnabledFieldNames,
  aVisibleFieldNames:string);
begin

end;

procedure TABDictionaryQuery.FreeFieldExtInfos;
var
  i:LongInt;
  tempFieldDef:PABFieldDef;
begin
  FLoadFieldsDefed:=false;
  for I := FDefList.Count - 1 downto 0 do
  begin
    tempFieldDef:=PABFieldDef(FDefList.Objects[i]);
    if Assigned(tempFieldDef) then
    begin
      if tempFieldDef.IsDown then
      begin
        if (Assigned(tempFieldDef.PDownDef.Fi_Datasource))  then
          tempFieldDef.PDownDef.Fi_Datasource.Free;

        if (Assigned(tempFieldDef.PDownDef.Fi_DataSet))  then
          tempFieldDef.PDownDef.Fi_DataSet.Free;

        Dispose(tempFieldDef.PDownDef);
      end;

      if Assigned(tempFieldDef.PMetaDef) then
      begin
        Dispose(tempFieldDef.PMetaDef);
      end;

      if Assigned(tempFieldDef.PEventDef) then
      begin
        if Assigned(tempFieldDef.PEventDef.FOnChange) then
          tempFieldDef.PEventDef.FOnChange:=nil;
        if Assigned(tempFieldDef.PEventDef.FOnGetText) then
          tempFieldDef.PEventDef.FOnGetText:=nil;
        if Assigned(tempFieldDef.PEventDef.FOnSetText) then
          tempFieldDef.PEventDef.FOnSetText:=nil;
        if Assigned(tempFieldDef.PEventDef.FOnValidate) then
          tempFieldDef.PEventDef.FOnValidate:=nil;

        Dispose(tempFieldDef.PEventDef);
      end;
      Dispose(tempFieldDef);
    end;

    FDefList.Delete(i);
  end;
  FDefList.Clear;
end;

procedure TABDictionaryQuery.CreateFieldExtInfos;
var
  ii:longint;
  tempCanUseTableAndFieldKeyInfo:Boolean;
  tempContinue:Boolean;

  tempField:TField;
  tempFieldDef:PABFieldDef;

  temp_Func_GetFieldInfo,
  tempTableDataset,
  tempFieldDataset :TDataSet;

  tempTableName,tempStr1:string;
  tempFi_DefaultValueMakeTime:string;
  tempLocalDownGroup:boolean;
  tempUseSingerTr_CodeValue:string;

  procedure LoadFieldinfo_Meta;
  begin
    if not Assigned(temp_Func_GetFieldInfo) then
    begin
      temp_Func_GetFieldInfo:=ABGetConstSqlPubDataset('ABSys_Temp_GetFieldInfo',[ABGetDatabaseByConnName(ConnName),tempTableName]);
    end;

    if temp_Func_GetFieldInfo.Locate('Name',tempField.FieldName,[loCaseInsensitive]) then
    begin
      new(tempFieldDef.PMetaDef);

      tempFieldDef.PMetaDef.Fi_ID       	  	   :=temp_Func_GetFieldInfo.FindField(trim('ID	           ')).AsInteger  ;
      tempFieldDef.PMetaDef.Fi_IsAutoAdd	  	   :=temp_Func_GetFieldInfo.FindField(trim('IsAutoAdd	     ')).AsBoolean  ;
      tempFieldDef.PMetaDef.Fi_IsKey    	  	   :=temp_Func_GetFieldInfo.FindField(trim('IsKey    	     ')).AsBoolean  ;
      tempFieldDef.PMetaDef.Fi_Type 	      	   :=temp_Func_GetFieldInfo.FindField(trim('Type 	         ')).AsString  ;
      tempFieldDef.PMetaDef.Fi_Byte	        	   :=temp_Func_GetFieldInfo.FindField(trim('Byte	         ')).AsInteger  ;
      tempFieldDef.PMetaDef.Fi_Length   	  	   :=temp_Func_GetFieldInfo.FindField(trim('Length   	     ')).AsInteger  ;
      tempFieldDef.PMetaDef.Fi_EecimalDigits	   :=temp_Func_GetFieldInfo.FindField(trim('EecimalDigits  ')).AsInteger  ;
      tempFieldDef.PMetaDef.Fi_CanNull   	  	   :=temp_Func_GetFieldInfo.FindField(trim('CanNull   	   ')).AsBoolean  ;
      tempFieldDef.PMetaDef.Fi_DefaultValue 	   :=temp_Func_GetFieldInfo.FindField(trim('DefaultValue   ')).AsString  ;
      tempContinue:=true;
      if Assigned(FAfterLoadFieldMetaDef) then
        FAfterLoadFieldMetaDef(tempFieldDef.PMetaDef,tempContinue);

      if tempContinue then
      begin
        if (tempFieldDef.Fi_ControlType=EmptyStr) then
        begin
          tempFieldDef.Fi_ControlType:=tempFieldDef.PMetaDef.Fi_Type;
          if ABStrInArray(tempFieldDef.Fi_ControlType,
                     ['binary','varbinary','ntext','text'])>=0 then
          begin
            tempFieldDef.Fi_ControlType:='TABcxDBMemo';
          end
          else if (ABStrInArray(tempFieldDef.Fi_ControlType,
                   ['bigint','int','smallint','tinyint','decimal','numeric','float','real','ABDefaultDecimal'])>=0) then
          begin
            tempFieldDef.Fi_ControlType:='TABcxDBCalcEdit';
          end
          else if (ABStrInArray(tempFieldDef.Fi_ControlType,['bit'])>=0) then
          begin
            tempFieldDef.Fi_ControlType:='TABcxDBCheckBox';
          end
          else if (ABStrInArray(tempFieldDef.Fi_ControlType,['money','smallmoney'])>=0) then
          begin
            tempFieldDef.Fi_ControlType:='TABcxDBCurrencyEdit';
          end
          else if (ABStrInArray(tempFieldDef.Fi_ControlType,['datetime','smalldatetime','timestamp'])>=0) then
          begin
            tempFieldDef.Fi_ControlType:='TABcxDBDateEdit';
          end
          else if (ABStrInArray(tempFieldDef.Fi_ControlType,['image'])>=0) then
          begin
            tempFieldDef.Fi_ControlType:='TABcxDBImage';
          end
          else if (ABStrInArray(tempFieldDef.Fi_ControlType,['char','varchar','sql_variant','uniqueidentifier'])>=0) and
                  (tempFieldDef.PMetaDef.Fi_Byte>=200) or
                  (ABStrInArray(tempFieldDef.Fi_ControlType,['nchar','nvarchar'])>=0) and
                  (tempFieldDef.PMetaDef.Fi_Byte>=200*2)
                   then
          begin
            tempFieldDef.Fi_ControlType:='TABcxDBMemo';
          end
          else
          begin
            if tempFieldDef.Fi_Mask<>EmptyStr then
              tempFieldDef.Fi_ControlType:='TABcxDBMaskEdit'
            else
              tempFieldDef.Fi_ControlType:='TABcxDBTextEdit';
          end;
        end;
      end;
    end;
  end;
  procedure LoadFieldinfo_Down;
    function CreateInsideDataset(aSQL:string;aUseTr_Code:string=''):TDataset;
    var
      tempField:TField;
      tempDataset:TDataset;
    begin
      if (tempFieldDef.PDownDef.FI_CacleDataset) and
         (not tempFieldDef.PDownDef.FI_DownRefresh) then
      begin
        if aUseTr_Code<>emptystr then
        begin
          tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_TreeItem',[aUseTr_Code]);
          result:=ABCloneCursor(tempDataset);
          tempField:=result.FindField('TI_Code');
          if Assigned(tempField) then
            tempField.DisplayLabel:='编号';
          tempField:=result.FindField('TI_Name');
          if Assigned(tempField) then
            tempField.DisplayLabel:='名称';
        end
        else
        begin
          tempDataset:=ABCreatePubDataset(aSQL,tempFieldDef.PDownDef.FI_ConnName,aSQL,[],[],TABFieldCaptionQuery);
          result:=ABCloneCursor(tempDataset);
        end;
      end
      else
      begin
        result:=TABFieldCaptionQuery.Create(self);
        TABFieldCaptionQuery(result).ConnName:=tempFieldDef.PDownDef.FI_ConnName;
        TFireDACQuery(result).Sql.Text:=aSQL;
      end;
    end;
  var
    tempUseSingerTr_Code:Boolean;
  begin
    tempUseSingerTr_Code:=false;
    FPubFieldDownDef.FI_ConnName          :=trim(ABGetConnNameByConnGuid(tempFieldDataset.FindField(trim('FI_ConnName          ')).AsString));
    FPubFieldDownDef.Fi_DownGroup         :=trim(tempFieldDataset.FindField(trim('Fi_DownGroup         ')).AsString);
    FPubFieldDownDef.Fi_Tr_Code           :=trim(tempFieldDataset.FindField(trim('Fi_Tr_Code           ')).AsString);
    FPubFieldDownDef.Fi_DownSQL           :=trim(tempFieldDataset.FindField(trim('Fi_DownSQL           ')).AsString);
    FPubFieldDownDef.Fi_Filter            :=trim(tempFieldDataset.FindField(trim('Fi_Filter            ')).AsString);
    FPubFieldDownDef.Fi_DownFields        :=trim(tempFieldDataset.FindField(trim('Fi_DownFields        ')).AsString);
    FPubFieldDownDef.Fi_DownCaptions      :=trim(tempFieldDataset.FindField(trim('Fi_DownCaptions      ')).AsString);
    FPubFieldDownDef.Fi_ViewField         :=trim(tempFieldDataset.FindField(trim('Fi_ViewField         ')).AsString);
    FPubFieldDownDef.Fi_SaveField         :=trim(tempFieldDataset.FindField(trim('Fi_SaveField         ')).AsString);
    FPubFieldDownDef.Fi_ParentField       :=trim(tempFieldDataset.FindField(trim('Fi_ParentField       ')).AsString);
    FPubFieldDownDef.Fi_CanSelectParent   :=tempFieldDataset.FindField(trim('Fi_CanSelectParent       ')).AsBoolean;

    FPubFieldDownDef.Fi_RelatingFromFields:=trim(tempFieldDataset.FindField(trim('Fi_RelatingFromFields')).AsString);
    FPubFieldDownDef.Fi_RelatingToFields  :=trim(tempFieldDataset.FindField(trim('Fi_RelatingToFields  ')).AsString);
    FPubFieldDownDef.Fi_CheckInDown       :=tempFieldDataset.FindField(trim('Fi_CheckInDown       ')).AsBoolean;
    FPubFieldDownDef.FI_DownRefresh       :=tempFieldDataset.FindField(trim('FI_DownRefresh  ')).AsBoolean;
    FPubFieldDownDef.FI_CacleDataset      :=tempFieldDataset.FindField(trim('FI_CacleDataset  ')).AsBoolean;

    FPubFieldDownDef.Fi_IsAutoRemember	  :=tempFieldDataset.FindField(trim('Fi_IsAutoRemember	   ')).AsBoolean;
    FPubFieldDownDef.Fi_DownType	        :=tempFieldDataset.FindField(trim('Fi_DownType	   ')).AsString;

    //加载共用下拉组别的信息
    tempLocalDownGroup:=FPubFieldDownDef.Fi_DownGroup<>emptystr;
    if tempLocalDownGroup then
    begin
      tempLocalDownGroup:=ABGetPubFieldDownGroup.Locate('Fi_Name',Trim(FPubFieldDownDef.Fi_DownGroup),[]);
    end;

    //存在下拉的几种情况
    if  (FPubFieldDownDef.Fi_IsAutoRemember) or
        (Trim(FPubFieldDownDef.Fi_DownSQL) <>EmptyStr) or
        (Trim(FPubFieldDownDef.Fi_Tr_Code)<>EmptyStr) or
        (tempLocalDownGroup)
          then
    begin
      tempFieldDef.IsDown:=true;
      new(tempFieldDef.PDownDef);

      tempFieldDef.PDownDef.Fi_DataSet:=nil;
      tempFieldDef.PDownDef.Fi_Datasource:=nil;
      tempFieldDef.PDownDef.SingerTr_Code:=false;

      tempFieldDef.PDownDef.FI_ConnName          :=FPubFieldDownDef.FI_ConnName          ;
      tempFieldDef.PDownDef.Fi_DownGroup         :=FPubFieldDownDef.Fi_DownGroup         ;
      tempFieldDef.PDownDef.Fi_Tr_Code           :=FPubFieldDownDef.Fi_Tr_Code           ;
      tempFieldDef.PDownDef.Fi_DownSQL           :=FPubFieldDownDef.Fi_DownSQL           ;
      tempFieldDef.PDownDef.Fi_Filter            :=FPubFieldDownDef.Fi_Filter            ;
      tempFieldDef.PDownDef.Fi_DownFields        :=FPubFieldDownDef.Fi_DownFields        ;
      tempFieldDef.PDownDef.Fi_DownCaptions      :=FPubFieldDownDef.Fi_DownCaptions      ;
      tempFieldDef.PDownDef.Fi_ViewField         :=FPubFieldDownDef.Fi_ViewField         ;
      tempFieldDef.PDownDef.Fi_SaveField         :=FPubFieldDownDef.Fi_SaveField         ;
      tempFieldDef.PDownDef.Fi_ParentField       :=FPubFieldDownDef.Fi_ParentField       ;
      tempFieldDef.PDownDef.Fi_CanSelectParent   :=FPubFieldDownDef.Fi_CanSelectParent   ;
      tempFieldDef.PDownDef.Fi_RelatingFromFields:=FPubFieldDownDef.Fi_RelatingFromFields;
      tempFieldDef.PDownDef.Fi_RelatingToFields  :=FPubFieldDownDef.Fi_RelatingToFields  ;
      tempFieldDef.PDownDef.Fi_CheckInDown       :=FPubFieldDownDef.Fi_CheckInDown       ;
      tempFieldDef.PDownDef.FI_DownRefresh       :=FPubFieldDownDef.FI_DownRefresh  ;
      tempFieldDef.PDownDef.FI_CacleDataset      :=FPubFieldDownDef.FI_CacleDataset  ;
      tempFieldDef.PDownDef.Fi_IsAutoRemember	   :=FPubFieldDownDef.Fi_IsAutoRemember	  ;
      tempFieldDef.PDownDef.Fi_DownType	         :=FPubFieldDownDef.Fi_DownType	        ;

      //加载组别的处理
      if tempLocalDownGroup  then
      begin
        tempStr1:=  trim(tempFieldDef.PDownDef.Fi_DownSQL);
        if (AnsiCompareText(Copy(tempStr1,1,5),'where')=0) or
           (AnsiCompareText(Copy(tempStr1,1,2),'or')=0) or
           (AnsiCompareText(Copy(tempStr1,1,3),'and')=0) then
        begin
          tempFieldDef.PDownDef.Fi_DownSQL:=
            ABPubSQLParse.AddWhere(tempFieldDef.PDownDef.Fi_DownSQL,FPubFieldDownGroup.FieldByName('Fi_DownSQL').AsString);
        end
        else
        begin
          if tempFieldDef.PDownDef.Fi_DownSQL=EmptyStr then
            tempFieldDef.PDownDef.Fi_DownSQL:=FPubFieldDownGroup.FieldByName('Fi_DownSQL').AsString;
        end;

        //如果共用组别是默认值则取组别对应字段的数据
        if tempFieldDef.PDownDef.FI_ConnName          =EmptyStr then tempFieldDef.PDownDef.FI_ConnName             :=ABGetConnNameByConnGuid(FPubFieldDownGroup.FieldByName(trim('FI_ConnName          ')).AsString);
        if tempFieldDef.PDownDef.Fi_Filter            =EmptyStr then tempFieldDef.PDownDef.Fi_Filter               :=FPubFieldDownGroup.FieldByName(trim('Fi_Filter            ')).AsString;
        if tempFieldDef.PDownDef.Fi_DownFields        =EmptyStr then tempFieldDef.PDownDef.Fi_DownFields           :=FPubFieldDownGroup.FieldByName(trim('Fi_DownFields        ')).AsString;
        if tempFieldDef.PDownDef.Fi_DownCaptions      =EmptyStr then tempFieldDef.PDownDef.Fi_DownCaptions         :=FPubFieldDownGroup.FieldByName(trim('Fi_DownCaptions      ')).AsString;
        if tempFieldDef.PDownDef.Fi_ViewField         =EmptyStr then tempFieldDef.PDownDef.Fi_ViewField            :=FPubFieldDownGroup.FieldByName(trim('Fi_ViewField         ')).AsString;
        if tempFieldDef.PDownDef.Fi_SaveField         =EmptyStr then tempFieldDef.PDownDef.Fi_SaveField            :=FPubFieldDownGroup.FieldByName(trim('Fi_SaveField         ')).AsString;
        if tempFieldDef.PDownDef.Fi_ParentField       =EmptyStr then tempFieldDef.PDownDef.Fi_ParentField          :=FPubFieldDownGroup.FieldByName(trim('Fi_ParentField       ')).AsString;

        if tempFieldDef.PDownDef.Fi_RelatingFromFields=EmptyStr then tempFieldDef.PDownDef.Fi_RelatingFromFields   :=FPubFieldDownGroup.FieldByName(trim('Fi_RelatingFromFields')).AsString;
        if tempFieldDef.PDownDef.Fi_RelatingToFields  =EmptyStr then tempFieldDef.PDownDef.Fi_RelatingToFields     :=FPubFieldDownGroup.FieldByName(trim('Fi_RelatingToFields  ')).AsString;
        if tempFieldDef.PDownDef.Fi_Tr_Code           =EmptyStr then tempFieldDef.PDownDef.Fi_Tr_Code              :=FPubFieldDownGroup.FieldByName(trim('Fi_Tr_Code           ')).AsString;

        if tempFieldDef.PDownDef.Fi_IsAutoRemember    =false    then tempFieldDef.PDownDef.Fi_IsAutoRemember       :=FPubFieldDownGroup.FieldByName(trim('Fi_IsAutoRemember    ')).AsBoolean;
        if tempFieldDef.PDownDef.Fi_CheckInDown       =false    then tempFieldDef.PDownDef.Fi_CheckInDown          :=FPubFieldDownGroup.FieldByName(trim('Fi_CheckInDown       ')).AsBoolean;
        if tempFieldDef.PDownDef.Fi_CanSelectParent             then tempFieldDef.PDownDef.Fi_CanSelectParent      :=FPubFieldDownGroup.FieldByName(trim('Fi_CanSelectParent   ')).AsBoolean;
        if tempFieldDef.PDownDef.FI_DownRefresh       =false    then tempFieldDef.PDownDef.FI_DownRefresh          :=FPubFieldDownGroup.FieldByName(trim('FI_DownRefresh       ')).AsBoolean;
        if tempFieldDef.PDownDef.FI_CacleDataset                then tempFieldDef.PDownDef.FI_CacleDataset         :=FPubFieldDownGroup.FieldByName(trim('FI_CacleDataset      ')).AsBoolean;
      end;

      //生成下拉的SQL语句相关
      if tempFieldDef.PDownDef.Fi_IsAutoRemember then
      begin
        tempFieldDef.PDownDef.Fi_DownSQL:=
            ' select DISTINCT '+tempFieldDef.Fi_Name+
            ' from ' +tempTableName+
            ' order by '+tempFieldDef.Fi_Name;
        tempFieldDef.PDownDef.Fi_DownFields     	  :=tempFieldDef.Fi_Name;
        tempFieldDef.PDownDef.Fi_ViewField      	  :=tempFieldDef.Fi_Name;
        tempFieldDef.PDownDef.Fi_SaveField	        :=tempFieldDef.Fi_Name;
      end
      else if tempFieldDef.PDownDef.Fi_Tr_Code<>EmptyStr then
      begin
        if tempFieldDef.PDownDef.Fi_DownFields=EmptyStr then
        begin
          tempStr1:=ABGetConstSqlPubDataset('ABSys_Org_Tree',[tempFieldDef.PDownDef.Fi_Tr_Code]).fieldbyname('Tr_DownFields').asstring;

          if tempStr1=EmptyStr then
            tempStr1:='Ti_Code,Ti_Name';

          tempFieldDef.PDownDef.Fi_DownFields:=tempStr1;
        end;
        if tempFieldDef.PDownDef.Fi_ViewField=EmptyStr then
        begin
          tempStr1:=ABGetConstSqlPubDataset('ABSys_Org_Tree',[tempFieldDef.PDownDef.Fi_Tr_Code]).fieldbyname('Tr_ViewFields').asstring;
          if tempStr1=EmptyStr then
            tempStr1:='Ti_Name';
          tempFieldDef.PDownDef.Fi_ViewField:=tempStr1;
        end;
        if tempFieldDef.PDownDef.Fi_SaveField=EmptyStr then
        begin
          tempStr1:=ABGetConstSqlPubDataset('ABSys_Org_Tree',[tempFieldDef.PDownDef.Fi_Tr_Code]).fieldbyname('Tr_SaveFields').asstring;
          if tempStr1=EmptyStr then
            tempStr1:='Ti_Code';
          tempFieldDef.PDownDef.Fi_SaveField:=tempStr1;
        end;

        tempStr1:=  trim(tempFieldDef.PDownDef.Fi_DownSQL);
        tempUseSingerTr_Code:=(tempStr1=EmptyStr);
        if (tempStr1=EmptyStr) or
           (AnsiCompareText(Copy(tempStr1,1,2),'or')=0) or
           (AnsiCompareText(Copy(tempStr1,1,3),'and')=0)  then
        begin
          tempStr1:=tempFieldDef.PDownDef.Fi_DownFields;
          if ABPos(',Ti_Code,',','+tempStr1+',')<=0 then
            tempStr1:=tempStr1+','+'Ti_Code';
          if ABPos(',Ti_Name,',','+tempStr1+',')<=0 then
            tempStr1:=tempStr1+','+'Ti_Name';

          tempFieldDef.PDownDef.Fi_DownSQL :=
            ' select * '+
            ' from ABSys_Org_TreeItem '+
            ' where Ti_Tr_Guid='+QuotedStr(
                                           ABGetConstSqlPubDataset('ABSys_Org_Tree',[tempFieldDef.PDownDef.Fi_Tr_Code]).fieldbyname('Tr_Guid').asstring
                                            )+tempFieldDef.PDownDef.Fi_DownSQL;
        end;

        if tempFieldDef.PDownDef.Fi_DownType=''  then
          tempFieldDef.PDownDef.Fi_DownType:='lsEditList';

        if (AnsiCompareText(tempFieldDef.PDownDef.Fi_DownType,'AddDownItem')=0) then
          tempFieldDef.IsCanAppendTreeItemDown:=true;
      end;

      tempContinue:=true;
      if (Assigned(FAfterLoadFieldDownDef))  then
        FAfterLoadFieldDownDef(tempFieldDef.PDownDef,tempContinue);

      //创建字段的内部数据集
      if (tempContinue)  then
      begin
        tempUseSingerTr_CodeValue:= ABIIF(tempUseSingerTr_Code,tempFieldDef.PDownDef.Fi_Tr_Code,emptystr);
        tempFieldDef.PDownDef.SingerTr_Code:=(tempUseSingerTr_CodeValue<>emptystr);

        //创建下拉数据集
        tempFieldDef.PDownDef.Fi_Datasource :=TDataSource.Create(self);
        tempFieldDef.PDownDef.Fi_DataSet    :=CreateInsideDataset(tempFieldDef.PDownDef.Fi_DownSQL,tempUseSingerTr_CodeValue);
        tempFieldDef.PDownDef.Fi_Datasource.DataSet :=tempFieldDef.PDownDef.Fi_DataSet;

        if (tempFieldDef.PDownDef.Fi_ParentField<>EmptyStr)  then
        begin
          TABDictionaryQuery(tempFieldDef.PDownDef.Fi_DataSet).UpdateOptions.ReadOnly:=True;
        end;

        if (not tempFieldDef.PDownDef.Fi_DataSet.Active)  then
        begin
          ABReFreshQuery(tempFieldDef.PDownDef.Fi_DataSet,[]);
        end;

        if (tempFieldDef.PDownDef.Fi_DataSet.FieldCount=1) then
        begin
          tempFieldDef.PDownDef.Fi_DownFields:=tempFieldDef.PDownDef.Fi_DataSet.Fields[0].FieldName;
          tempFieldDef.PDownDef.Fi_SaveField:=tempFieldDef.PDownDef.Fi_DataSet.Fields[0].FieldName;
          tempFieldDef.PDownDef.Fi_ViewField:=tempFieldDef.PDownDef.Fi_DataSet.Fields[0].FieldName;
        end
        else if (tempFieldDef.PDownDef.Fi_DataSet.FieldCount>1) then
        begin
          if (tempFieldDef.PDownDef.Fi_ParentField=EmptyStr) then
          begin
            if tempFieldDef.PDownDef.Fi_DownFields=EmptyStr then
            begin
              tempFieldDef.PDownDef.Fi_DownFields:=ABGetDatasetFieldNames(tempFieldDef.PDownDef.Fi_DataSet);
            end
          end
          else
          begin
            if tempFieldDef.PDownDef.Fi_DownFields=EmptyStr then
            begin
              tempFieldDef.PDownDef.Fi_DownFields:=tempFieldDef.PDownDef.Fi_ViewField;
            end
          end;
        end;

        //使字段间隔支持","与";"
        tempFieldDef.PDownDef.Fi_DownFields        :=ABStringReplace(tempFieldDef.PDownDef.Fi_DownFields        ,';',',');
        tempFieldDef.PDownDef.Fi_DownCaptions      :=ABStringReplace(tempFieldDef.PDownDef.Fi_DownCaptions      ,';',',');
        tempFieldDef.PDownDef.Fi_RelatingFromFields:=ABStringReplace(tempFieldDef.PDownDef.Fi_RelatingFromFields,';',',');
        tempFieldDef.PDownDef.Fi_RelatingToFields  :=ABStringReplace(tempFieldDef.PDownDef.Fi_RelatingToFields  ,';',',');
      end;

      if (tempFieldDef.IsDown) then
      begin
        if (tempFieldDef.Fi_ControlType=EmptyStr) then
        begin
          if (tempFieldDef.PDownDef.Fi_ParentField<>EmptyStr)  then
          begin
            tempFieldDef.Fi_ControlType:='TABcxDBTreeViewPopupEdit';
          end
          else
          begin
            tempFieldDef.Fi_ControlType:='TABcxDBExtLookupComboBox';
          end;
        end;
      end;
    end;
  end;
  function LoadFieldinfo_Base:boolean;
  begin
    tempFieldDef.Fi_Ta_Name	               :=tempTableName                                                    ;
    tempFieldDef.Fi_Ta_Guid	               :=tempFieldDataset.FindField(trim('Fi_Ta_Guid      ')).AsString        ;
    tempFieldDef.Fi_Name	                 :=tempFieldDataset.FindField(trim('Fi_Name	        ')).AsString        ;
    tempFieldDef.Fi_Hint                   :=tempFieldDataset.FindField(trim('Fi_Hint         ')).AsString ;
    tempFieldDef.Fi_Order                  :=tempFieldDataset.FindField(trim('Fi_Order        ')).AsInteger       ;
    tempFieldDef.Fi_CanNull	               :=tempFieldDataset.FindField(trim('Fi_CanNull      ')).AsBoolean       ;
    tempFieldDef.Fi_Guid	                 :=tempFieldDataset.FindField(trim('Fi_Guid	        ')).AsString        ;
    tempFieldDef.Fi_DefaultValue	         :=tempFieldDataset.FindField(trim('Fi_DefaultValue ')).AsString        ;
    tempFieldDef.Fi_BatchEdit  	           :=tempFieldDataset.FindField(trim('Fi_BatchEdit    ')).AsBoolean       ;

    tempFi_DefaultValueMakeTime            :=tempFieldDataset.FindField(trim('Fi_DefaultValueMakeTime')).AsString ;
    tempFieldDef.Fi_DefaultValueMakeTime   :=[];
    if tempFi_DefaultValueMakeTime=emptystr then
    begin
      tempFieldDef.Fi_DefaultValueMakeTime:=[dtAfterAppend];
    end
    else
    begin
      if ABPos(',After Append,',','+tempFi_DefaultValueMakeTime+',')>0 then
        tempFieldDef.Fi_DefaultValueMakeTime:=tempFieldDef.Fi_DefaultValueMakeTime+[dtAfterAppend];
      if ABPos(',Before AppendSave,',','+tempFi_DefaultValueMakeTime+',')>0 then
        tempFieldDef.Fi_DefaultValueMakeTime:=tempFieldDef.Fi_DefaultValueMakeTime+[dtBeforeAppendSave];
      if ABPos(',Before EditSave,',','+tempFi_DefaultValueMakeTime+',')>0 then
        tempFieldDef.Fi_DefaultValueMakeTime:=tempFieldDef.Fi_DefaultValueMakeTime+[dtBeforeEditSave];
    end;

    if Assigned(tempFieldDataset.FindField(trim('Fi_Length       '))) then
    begin
      tempFieldDef.Fi_MaxLength	             :=tempFieldDataset.FindField(trim('Fi_Length       ')).AsInteger       ;
      tempFieldDef.Fi_MinLength:=0;
    end
    else
    begin
      tempFieldDef.Fi_MaxLength	             :=tempFieldDataset.FindField(trim('Fi_MaxLength       ')).AsInteger       ;
      tempFieldDef.Fi_MinLength	             :=tempFieldDataset.FindField(trim('Fi_MinLength       ')).AsInteger       ;
    end;
    tempFieldDef.Fi_DatasetUnique          :=tempFieldDataset.FindField(trim('Fi_DatasetUnique')).AsBoolean       ;

    if CanEdit then
    begin
      //设置字段的只读与隐藏的权限
      tempFieldDef.Fi_CanEditInEdit      :=true;
      tempFieldDef.Fi_CanEditInInsert    :=true;
      //先判断ABSys_Org_Table表中的新增,删除,修改,打印的权限
      if tempFieldDef.Fi_CanEditInEdit then
        tempFieldDef.Fi_CanEditInEdit:=tempFieldDataset.FindField('Fi_CanEditInEdit').AsBoolean;
      if tempFieldDef.Fi_CanEditInInsert then
        tempFieldDef.Fi_CanEditInInsert:=tempFieldDataset.FindField('Fi_CanEditInInsert').AsBoolean;
    end
    else
    begin
      tempFieldDef.Fi_CanEditInEdit      :=false;
      tempFieldDef.Fi_CanEditInInsert    :=false;
    end;

    if tempCanUseTableAndFieldKeyInfo  then
    begin
      //先判断ABSys_org_Field 表中的新增,修改,显示权限
      if (not tempFieldDef.Fi_CanEditInEdit) and
         (not tempFieldDef.Fi_CanEditInInsert) and
         (not tempFieldDef.Fi_IsInputPanelView) and
         (not tempFieldDef.Fi_IsGridView) then
      begin

      end
      else
      begin
        //再判断ABSys_Right_KeyField 表中的新增,修改,显示权限
        if (Assigned(ABUser.OperatorKeyFieldDataSet)) and
           (not ABUser.OperatorKeyFieldDataSet.IsEmpty) and
           (ABUser.OperatorKeyFieldDataSet.Locate('Kf_Fi_Guid',tempFieldDef.Fi_Guid,[]))  then
        begin
          if tempFieldDef.Fi_CanEditInEdit then
            tempFieldDef.Fi_CanEditInEdit:=ABUser.OperatorKeyFieldDataSet.FindField('Kf_CanEditInEdit').AsBoolean;
          if tempFieldDef.Fi_CanEditInInsert then
            tempFieldDef.Fi_CanEditInInsert:=ABUser.OperatorKeyFieldDataSet.FindField('Kf_CanEditInInsert').AsBoolean;

          if (tempFieldDef.Fi_IsInputPanelView) then
            tempFieldDef.Fi_IsInputPanelView:=ABUser.OperatorKeyFieldDataSet.FindField('Kf_Visible').AsBoolean;
          if (tempFieldDef.Fi_IsGridView) then
            tempFieldDef.Fi_IsGridView:=ABUser.OperatorKeyFieldDataSet.FindField('Kf_Visible').AsBoolean;
        end;
      end;
    end;

    tempFieldDef.Fi_ExtSetup               :=tempFieldDataset.FindField(trim('Fi_ExtSetup     ')).AsString        ;

    tempFieldDef.Fi_LabelLeft	             :=tempFieldDataset.FindField(trim('Fi_LabelLeft	  ')).AsInteger       ;
    tempFieldDef.Fi_LabelTop	             :=tempFieldDataset.FindField(trim('Fi_LabelTop	    ')).AsInteger       ;
    tempFieldDef.Fi_LabelWidth	           :=tempFieldDataset.FindField(trim('Fi_LabelWidth	  ')).AsInteger       ;
    tempFieldDef.Fi_LabelHeight   	       :=tempFieldDataset.FindField(trim('Fi_LabelHeight  ')).AsInteger       ;
    tempFieldDef.Fi_ControlType            :=tempFieldDataset.FindField(trim('Fi_ControlType  ')).AsString        ;
    tempFieldDef.Fi_ControlLeft	           :=tempFieldDataset.FindField(trim('Fi_ControlLeft	')).AsInteger       ;
    tempFieldDef.Fi_ControlTop	           :=tempFieldDataset.FindField(trim('Fi_ControlTop	  ')).AsInteger       ;
    tempFieldDef.Fi_ControlWidth	         :=tempFieldDataset.FindField(trim('Fi_ControlWidth ')).AsInteger       ;
    tempFieldDef.Fi_ControlHeight   	     :=tempFieldDataset.FindField(trim('Fi_ControlHeight')).AsInteger       ;
    tempFieldDef.Fi_PassChar	             :=tempFieldDataset.FindField(trim('Fi_PassChar	    ')).AsString        ;
    tempFieldDef.Fi_Max                    :=tempFieldDataset.FindField(trim('Fi_Max    	    ')).AsString        ;
    tempFieldDef.Fi_Min                    :=tempFieldDataset.FindField(trim('Fi_Min	        ')).AsString        ;
    tempFieldDef.Fi_Mask                   :=tempFieldDataset.FindField(trim('Fi_Mask         ')).AsString        ;
    tempFieldDef.PubID     	               :=tempFieldDataset.FindField(trim('PubID     	    ')).AsString        ;
    tempFieldDef.Fi_IsQueryView	           :=tempFieldDataset.FindField(trim('Fi_IsQueryView	   ')).AsBoolean    ;
    tempFieldDef.Fi_BegEndQuery	           :=tempFieldDataset.FindField(trim('Fi_BegEndQuery     ')).AsBoolean    ;

    tempFieldDef.Fi_IsGridView	           :=tempFieldDataset.FindField(trim('Fi_IsGridView	     ')).AsBoolean    ;
    tempFieldDef.Fi_IsInputPanelView	     :=tempFieldDataset.FindField(trim('Fi_IsInputPanelView')).AsBoolean    ;
    tempFieldDef.Fi_IsUpperChar            :=tempFieldDataset.FindField(trim('Fi_IsUpperChar     ')).AsBoolean    ;
    tempFieldDef.Fi_IsLowerChar            :=tempFieldDataset.FindField(trim('Fi_IsLowerChar     ')).AsBoolean    ;

    tempFieldDef.Fi_SetReadOnlyCommand     :=trim(tempFieldDataset.FindField(trim('Fi_SetReadOnlyCommand    ')).AsString);
    tempFieldDef.Fi_SetEditCommand         :=trim(tempFieldDataset.FindField(trim('Fi_SetEditCommand        ')).AsString);
    tempFieldDef.Fi_ExecSQLWhenFieldChange :=trim(tempFieldDataset.FindField(trim('Fi_ExecSQLWhenFieldChange')).AsString);
    tempFieldDef.Fi_GetValueSQL            :=trim(tempFieldDataset.FindField(trim('Fi_GetValueSQL           ')).AsString);

    tempContinue:=true;
    if Assigned(FAfterLoadFieldBaseDef) then
      FAfterLoadFieldBaseDef(tempFieldDef,tempContinue);

    result:=tempContinue;
  end;
  procedure LoadFieldinfo;
  begin
    tempFieldDataset.First;
    while not tempFieldDataset.Eof do
    begin
      //检测此字段是否不在当前的数据集中
      tempField:=FindField(tempFieldDataset.FindField(trim('Fi_Name')).AsString);
      if not Assigned(tempField)  then
      begin
        tempFieldDataset.Next;
        Continue;
      end;

      if not FLoadFieldsDefed then
      begin
        //初始化框架信息的结构
        tempFieldDef:= FieldDefByFieldName(tempField.FieldName);
        if not Assigned(tempFieldDef) then
        begin
          new(tempFieldDef);
          FDefList.AddObject(tempField.FieldName,TObject(tempFieldDef));
        end
        else
        begin
          //如果数据集中有相同字段且来源表名不是更新表时则跳过（即更新表与加载表同时有一样字段时，优先加载更新表中的）
          if UpdateTables.IndexOf(tempTableName)<0 then
          begin
            tempFieldDataset.Next;
            Continue;
          end;
        end;
        tempFieldDef.Field       :=tempField;
        tempFieldDef.Guid        :=ABGetGuid;
        if Assigned(tempField.DataSet.Owner) then
          tempFieldDef.Flag        :=tempField.DataSet.Owner.Name+'_'+tempField.DataSet.Name+'_'+tempField.FieldName
        else
          tempFieldDef.Flag        :='NoOwner_'+tempField.DataSet.Name+'_'+tempField.FieldName;

        tempFieldDef.PDownDef    :=nil;
        tempFieldDef.PMetaDef    :=nil;
        tempFieldDef.PEventDef   :=nil;

        tempFieldDef.Fi_Label    :=nil;
        tempFieldDef.Fi_Control  :=nil;
        tempFieldDef.Fi_GridColumn :=nil;
        tempFieldDef.Fi_LockInputPanelView :=false;
        tempFieldDef.Fi_LockGridView :=false;

        tempFieldDef.Fi_Update    :=(UpdateTables.IndexOf(tempTableName)>=0) and
                                    (tempFieldDataset.FindField(trim('Fi_Update ')).AsBoolean) and
                                    (AnsiCompareText(tempFieldDataset.FindField(trim('Fi_ControlType  ')).AsString,'TABcxDBFileBlobField')<>0) ;

        tempFieldDef.Fi_Copy      :=tempFieldDataset.FindField(trim('Fi_Copy ')).AsBoolean;

        tempFieldDef.IsCheck     :=true;

        tempFieldDef.IsDown      :=false;
        tempFieldDef.IsCanAppendTreeItemDown               :=false;

        tempFieldDef.fi_SetOrder :=-1;
        tempFieldDef.Fi_Lock     :=false;
        tempFieldDef.Fi_LockValidate     :=false;
        tempFieldDef.Fi_LockChange       :=false;
        tempFieldDef.Fi_LockGetText      :=false;
        tempFieldDef.Fi_LockSetText      :=false;

        tempFieldDef.Fi_Edited   :=false;
        tempFieldDef.Fi_Caption	               :=tempFieldDataset.FindField(trim('Fi_Caption	    ')).AsString ;

        //加载基本信息相关,如果返回True则继续加载
        if LoadFieldinfo_Base then
        begin
          //加载下拉信息相关
          LoadFieldinfo_Down;

          //加载元信息相关
          LoadFieldinfo_Meta;
        end;
      end
      else
      begin
        tempFieldDef:= FieldDefByFieldName(tempField.FieldName);
        if Assigned(tempFieldDef) then
          tempFieldDef.Field:=tempField;
      end;

      if Assigned(tempFieldDef) then
      begin
        //HOOK字段相关事件
        new(tempFieldDef.PEventDef);
        tempFieldDef.PEventDef.FOnChange   :=nil;
        tempFieldDef.PEventDef.FOnGetText  :=nil;
        tempFieldDef.PEventDef.FOnSetText  :=nil;
        tempFieldDef.PEventDef.FOnValidate :=nil;
        //当库中字段类型是VARCHAR（100），字典中指定为ABcxDBTimeEdit,
        //当系统设置日间格式为h:mm:ss时，use24HourFormat 属性不起作用，所以暂时回避
        //TABcxDBImage类型 OnSetText 时会当成字符处理，从而报错，所以暂时回避
        if  (AnsiCompareText(tempFieldDef.Fi_ControlType,'TABcxDBImage')<>0) and
            (AnsiCompareText(tempFieldDef.Fi_ControlType,'TABcxDBTimeEdit')<>0) and
            (AnsiCompareText(tempFieldDef.Fi_ControlType,'TABcxDBDateEdit')<>0) and
            (AnsiCompareText(tempFieldDef.Fi_ControlType,'TABcxDBDateTimeEdit')<>0)  then
        begin
        {
         TAIC_Access_CardPictureForm.UpdateIsViewPicture
          ABReFreshQuery(ABQuery2_1,[],nil,false);
          ABReFreshQuery(ABQuery2_2,[],nil,false);
         因为ABReFreshQuery(ABQuery2_1,[],nil,false);会 导致原来的OnChange等事件循环，所以暂时回避
          tempFieldDef.PEventDef.FOnChange   :=tempField.OnChange;
          tempFieldDef.PEventDef.FOnGetText  :=tempField.OnGetText;
          tempFieldDef.PEventDef.FOnSetText  :=tempField.OnSetText;
          tempFieldDef.PEventDef.FOnValidate :=tempField.OnValidate;
         }
          tempField.OnGetText  :=FieldGetText;
          tempField.OnSetText  :=FieldSetText;
          tempField.OnChange   :=FieldChange;
          tempField.OnValidate :=FieldValidate;
        end;

        //设置字段Required:=false,接管所有的字段值检测
        tempField.Required    :=false;
        //tempField.AutoGenerateValue    :=arNone;
        tempField.ReadOnly    :=false;

        //设置字段显示标题
        if tempFieldDef.Fi_Caption<>emptyStr then
          tempField.DisplayLabel:=tempFieldDef.Fi_Caption;

        if ABIsPublishProp(tempField,'DisplayFormat') then
        begin
          ABSetPropValue(tempField,'DisplayFormat',tempFieldDef.Fi_Mask);
        end;
      end;
      tempFieldDataset.Next;
    end;
  end;
  procedure LoadTableinfo;

    //设置数据能否修改的固定检测字段
    procedure SetCheckField;
    var
      tempFieldName,
      tempFieldValue:string;
      tempField:Tfield;
      tempFieldDef:PABFieldDef;
    begin
      if not ABPubUser.IsAdmin then
      begin
        tempFieldName:=tempTableDataset.FieldByName('TA_SysUseField').AsString;
        if (tempFieldName<>emptystr) and
           (Assigned(FindField(tempFieldName)))
             then
        begin
          tempField:= FindField(tempFieldName);
          if tempField.DataType=ftBoolean then
          begin
            tempFieldValue:='True';
          end
          else
          begin
            tempFieldValue:='1';
          end;

          if BeforeDeleteCheckFieldValues=EmptyStr then
          begin
            BeforeDeleteCheckFieldValues:=tempFieldName+'='+tempFieldValue;
          end;

          if BeforePostCheckFieldValues=EmptyStr then
          begin
            BeforePostCheckFieldValues:=tempFieldName+'='+tempFieldValue;
          end;

          //非系统管理员时设置此字段不可修改
          tempFieldDef:=FieldDefByFieldName(tempFieldName);
          if Assigned(tempFieldDef) then
          begin
            tempFieldDef.Fi_CanEditInEdit:=false;
            tempFieldDef.Fi_CanEditInInsert:=false;
          end;
        end;
      end;
    end;
  begin
    if (tempTableName<>EmptyStr) then
    begin
      tempTableDataset:=ABGetConstSqlPubDataset('ABSys_Org_Table',[ABGetDatabaseByConnName(ConnName),tempTableName]);
      tempFieldDataset:=ABGetConstSqlPubDataset('ABSys_Org_Field',[ABGetDatabaseByConnName(ConnName),tempTableName]);

      if (Assigned(tempTableDataset)) and
         (Assigned(tempFieldDataset)) then
      begin
        //取框架的数据字典信息

        //当前表在更新表中时取得表的框架信息
        if (UpdateTables.IndexOf(tempTableName)>=0) then
        begin
          MaxRecordCount:=tempTableDataset.FindField(trim('Ta_MaxRecord')).AsInteger;

          BeforeDeleteCheckCommand:=tempTableDataset.FindField(trim('TA_SetDeleteCommand')).asstring;
          BeforeEditCheckCommand  :=tempTableDataset.FindField(trim('TA_SetEditCommand  ')).asstring;
          BeforeInsertCheckCommand:=tempTableDataset.FindField(trim('TA_SetInsertCommand')).asstring;
          BeforePostCheckCommand  :=tempTableDataset.FindField(trim('TA_SetPostCommand  ')).asstring;
        end;

        //数据字典相关表一定要是可修改,否则就不能在数据字典中修改表的只读等信息了
        tempCanUseTableAndFieldKeyInfo:= (AnsiCompareText(tempTableName,'ABSys_Org_Table')<>0) and
                                         (AnsiCompareText(tempTableName,'ABSys_Org_Field')<>0) and
                                         //ABPubUser.Guid=EmptyStr为DELPHI环境
                                         (ABPubUser.Guid<>EmptyStr) and
                                         (not ABPubUser.IsAdminOrSysuser);
        if tempCanUseTableAndFieldKeyInfo then
        begin
          //设置表的新增,删除,修改,打印的权限
          if CanInsert then  CanInsert:=tempTableDataset.FindField('Ta_CanInsert').AsBoolean;
          if CanDelete then  CanDelete:=tempTableDataset.FindField('Ta_CanDelete').AsBoolean;
          if CanEdit   then  CanEdit  :=tempTableDataset.FindField('Ta_CanEdit').AsBoolean  ;
          if CanPrint  then  CanPrint :=tempTableDataset.FindField('Ta_CanPrint').AsBoolean ;

          //先判断ABSys_Org_Table表中的新增,删除,修改,打印的权限
          if (not CanInsert) and
             (not CanDelete) and
             (not CanEdit) and
             (not CanPrint)  then
          begin

          end
          else
          begin
            //再判断ABSys_Right_KeyTable 表中的新增,删除,修改,打印的权限
            if (Assigned(ABUser.OperatorKeyTableDataSet)) and
               (not ABUser.OperatorKeyTableDataSet.IsEmpty) and
               (ABUser.OperatorKeyTableDataSet.Locate('KT_TA_Guid',tempTableDataset.FindField('Ta_Guid').AsString,[])) then
            begin
              if CanInsert then
                CanInsert:=ABUser.OperatorKeyTableDataSet.FindField('Kt_CanInsert').AsBoolean;
              if CanDelete then
                CanDelete:=ABUser.OperatorKeyTableDataSet.FindField('Kt_CanDelete').AsBoolean;
              if CanEdit then
                CanEdit:=ABUser.OperatorKeyTableDataSet.FindField('Kt_CanEdit').AsBoolean;
              if CanPrint then
                CanPrint :=ABUser.OperatorKeyTableDataSet.FindField('Kt_CanPrint').AsBoolean ;
            end;

            if (not CanInsert) and (not CanDelete) and (not CanEdit) and (not UpdateOptions.ReadOnly)then
            begin
              UpdateOptions.ReadOnly:=true;
            end;
          end;
        end;

        LoadFieldinfo;

        SetCheckField;
      end;
    end;
  end;
begin
  inherited;
  //取得表的信息
  FPrimaryKeyFieldNames.Clear;
  FClusteredFieldNames.Clear;

  SetTableInfo;

  //当前表是否是表定义与字段定义(这两个表不能使用框架中定义的权限,因为如使用则在数据字典中就可能不能修改内容了)
  tempCanUseTableAndFieldKeyInfo:=False;
  CanPrint :=true;
  CanInsert:=not UpdateOptions.ReadOnly;
  CanDelete:=not UpdateOptions.ReadOnly;
  CanEdit  :=not UpdateOptions.ReadOnly;
  //遍历初始化的表,得到数据字典定义
  for ii := 0 to LoadTables.Count-1 do
  begin
    tempTableName:=trim(LoadTables[ii]);
    temp_Func_GetFieldInfo:=nil;
    tempTableDataset  :=nil;
    tempFieldDataset  :=nil;

    LoadTableinfo;
  end;

  FLoadFieldsDefed:=true;
end;

procedure TABDictionaryQuery.CreateFields;
begin
  inherited;
end;

procedure TABDictionaryQuery.SetEnabled(aFieldNames:string);
var
  i:longint;
  tempFieldDef:PABFieldDef;
  procedure SetEnabled(aEnabled:Boolean);
  begin
    if (Assigned(tempFieldDef.Fi_Control)) and
       (tempFieldDef.Fi_Control.Enabled<>aEnabled) then
    begin
      tempFieldDef.Fi_Control.Enabled:=aEnabled;
    end;
    if (Assigned(tempFieldDef.Fi_Label)) and
       (tempFieldDef.Fi_Label.Enabled<>aEnabled) then
    begin
      tempFieldDef.Fi_Label.Enabled:=aEnabled;
    end;

    if (Assigned(tempFieldDef.Fi_GridColumn)) and
       (tempFieldDef.Fi_GridColumn.Options.Editing<>aEnabled) then
    begin
      tempFieldDef.Fi_GridColumn.Options.Editing:=aEnabled;
    end;
  end;
begin
  if (active)  then
  begin
    for i := 0 to DefList.Count-1 do
    begin
      tempFieldDef:=PABFieldDef(DefList.Objects[i]);
      if (Assigned(tempFieldDef)) then
      begin
        SetEnabled((aFieldNames=emptystr) or (abpos(','+tempFieldDef.fi_name+',',','+aFieldNames+',')>0));
      end;
    end;
  end;
end;

procedure TABDictionaryQuery.SetVisible(aFieldNames:string);
var
  i:longint;
  tempFieldDef:PABFieldDef;
  procedure SetVisible(aVisible:Boolean);
  begin
    if (Assigned(tempFieldDef.Fi_Control)) and
       (tempFieldDef.Fi_Control.Visible<>aVisible) then
    begin
      tempFieldDef.Fi_Control.Visible:=aVisible;
    end;
    if (Assigned(tempFieldDef.Fi_Label)) and
       (tempFieldDef.Fi_Label.Visible<>aVisible) then
    begin
      tempFieldDef.Fi_Label.Visible:=aVisible;
    end;

    if (Assigned(tempFieldDef.Fi_GridColumn)) and
       (tempFieldDef.Fi_GridColumn.Visible<>aVisible) then
    begin
      tempFieldDef.Fi_GridColumn.Visible:=aVisible;
    end;
  end;
begin
  if (active) then
  begin
    for i := 0 to DefList.Count-1 do
    begin
      tempFieldDef:=PABFieldDef(DefList.Objects[i]);
      if (Assigned(tempFieldDef)) then
      begin
        SetVisible((aFieldNames=emptystr) or (abpos(','+tempFieldDef.fi_name+',',','+aFieldNames+',')>0));
      end;
    end;
  end;
end;

constructor TABDictionaryQuery.Create(AOwner: TComponent);
begin
  inherited;
  FLoadFieldsDefed:=false;
  FAddTableRowQueryRight:=True;
  FBlobFieldDefs:= TBlobFieldDefs.Create;

  FMultiLevelAlign:=alTop;
  FMultiLevelQuery:=True;
  FMultiLevelOrder:=0;

  FCanCheckFieldValue:=true;

  FDefList:=TStringList.Create;
  FPrimaryKeyFieldNames:=TStringList.Create;
  FClusteredFieldNames:=TStringList.Create;

  FLinkDBTableViewList:= Tlist.Create;
  FLinkDBPanelList:= Tlist.Create;
end;

procedure TABDictionaryQuery.LinkBlobFieldDatasets;
var
  i:LongInt;
  tempFieldDef:PABFieldDef;
begin
  for I := 0  to FBlobFieldDefs.Count-1 do
  begin
    if (Assigned(FBlobFieldDefs.Items[i].Datasource)) and
       (Assigned(FBlobFieldDefs.Items[i].Datasource.DataSet)) then
    begin
      tempFieldDef:=FieldDefByFieldName(FBlobFieldDefs.Items[i].FieldName);
      if (Assigned(tempFieldDef)) and
         (Assigned(tempFieldDef.Fi_Control)) and
         (tempFieldDef.Fi_Control is TcxDBImage)
          then
      begin
        TcxDBImage(tempFieldDef.Fi_Control).DataBinding.DataSource:=FBlobFieldDefs.Items[i].Datasource;
        TcxDBImage(tempFieldDef.Fi_Control).DataBinding.DataField:=FBlobFieldDefs.Items[i].FieldName;
      end;
    end;
  end;
end;

procedure TABDictionaryQuery.OpenQuery;
begin

end;

procedure TABDictionaryQuery.DoTransactBaseSQL(var aSQL: string);
  function ReplaceSql(aSQL:string):string;
  var
    tempParamStr,
    tempParamValue,
    tempParam1,tempParam2:string;
    tempBeginPos,k,l: Integer;
  begin
    Result:= aSQL;
    k:=0;
    l:=0;
    repeat
      tempBeginPos:=abpos('GetNoBigDateFieldNames',Result);
      if tempBeginPos>0 then
      begin
        k:= abpos('(',Copy(Result,tempBeginPos,maxint));
        l:= abpos(')',Copy(Result,tempBeginPos,maxint));
        if (k>0) and (L>0) then
        begin
          k:=tempBeginPos+k-1;
          l:=tempBeginPos+l-1;

          tempParamStr:=Copy(Result,k+1,l-k-1);
          tempParam1:=ABUnQuotedFirstlastStr(trim(ABGetSpaceStr(tempParamStr,1,',')));
          tempParam2:=ABUnQuotedFirstlastStr(trim(ABGetSpaceStr(tempParamStr,2,',')));
          tempParamValue:= ABGetTableNoTypeFieldNames(tempParam1,tempParam2,true,False,False,ConnName);


          Result:=ABStringReplace(Result,tempBeginPos,l-tempBeginPos+1,tempParamValue);
        end;
      end;
    until (tempBeginPos<=0) or (k<=0) or (l<=0);

    k:=0;
    l:=0;
    repeat
      tempBeginPos:=abpos('GetFieldNames',Result);
      if tempBeginPos>0 then
      begin
        k:= abpos('(',Copy(Result,tempBeginPos,maxint));
        l:= abpos(')',Copy(Result,tempBeginPos,maxint));
        if (k>0) and (L>0) then
        begin
          k:=tempBeginPos+k-1;
          l:=tempBeginPos+l-1;

          tempParamStr:=Copy(Result,k+1,l-k-1);
          tempParam1:=ABUnQuotedFirstlastStr(trim(ABGetSpaceStr(tempParamStr,1,',')));
          tempParam2:=ABUnQuotedFirstlastStr(trim(ABGetSpaceStr(tempParamStr,2,',')));
          tempParamValue:= ABGetTableFieldNames(tempParam1,ABStrToStringArray(tempParam2,';'),False,false,ConnName);


          Result:=ABStringReplace(Result,tempBeginPos,l-tempBeginPos+1,tempParamValue);
        end;
      end;
    until (tempBeginPos<=0) or (k<=0) or (l<=0);
  end;

  procedure AddCumtomWhere(var aSQL: string);
  var
    tempTA_TableRowFilterFieldNames,
    tempTA_SysUseField:string;
    tempTableDataset,tempFieldDataset:TDataSet;
    tempAddWhere:string;
  begin
    if (SQLParseObject.SQLParseDef.SingleTableName<> emptystr)then
    begin
      tempAddWhere:= emptystr;
      if (FTableRowQueryWhere = emptystr) and
         (FAddTableRowQueryRight) and
         (ABUser.OperatorKeyTableRowQueryDataSet.Active)
         then
      begin
        tempTableDataset:=ABGetConstSqlPubDataset('ABSys_Org_Table',[ABGetDatabaseByConnName(ConnName),SQLParseObject.SQLParseDef.SingleTableName]);
        if (not ABDatasetIsEmpty(tempTableDataset)) and
           (ABUser.OperatorKeyTableRowQueryDataSet.Locate('KT_TA_Guid',tempTableDataset.FindField('Ta_Guid').AsString,[])) then
        begin
          tempTA_TableRowFilterFieldNames:=tempTableDataset.FieldByName('TA_TableRowFilterFieldNames').AsString;
          if tempTA_TableRowFilterFieldNames=EmptyStr then
            tempTA_TableRowFilterFieldNames:= ABGetTablePrimaryKeyFieldNames(SQLParseObject.SQLParseDef.SingleTableName,ConnName,'+','cast( ',' as nvarchar)');

          if tempTA_TableRowFilterFieldNames <> emptystr then
          begin
            FTableRowQueryWhere:= tempTA_TableRowFilterFieldNames+' in '+
                                 ' ('+
                                 '  select KR_TA_KeyFieldValue '+
                                 '  from ABSys_Right_KeyTableRow  '+
                                 '  where KR_KT_Guid in '+
                                 '     (select KT_Guid '+
                                 '      from ABSys_Right_KeyTable '+
                                 '      where KT_KE_Guid in '+
                                 '        (select Ok_Ke_Guid  '+
                                 '         from ABSys_Right_OperatorKey '+
                                 '         where Ok_Op_Guid='+QuotedStr(ABPubUser.Guid)+
                                 '        ) and KT_TA_Guid='+QuotedStr(tempTableDataset.FindField('Ta_Guid').AsString)+
                                 '       ) '+
                                 ' )' ;
          end;
        end;
      end;

      if (FSysUseQueryWhere = emptystr) and
         (not ABPubUser.IsAdmin)
         then
      begin
        tempTableDataset:=ABGetConstSqlPubDataset('ABSys_Org_Table',[ABGetDatabaseByConnName(ConnName),SQLParseObject.SQLParseDef.SingleTableName]);
        if (not ABDatasetIsEmpty(tempTableDataset))  then
        begin
          tempTA_SysUseField:=tempTableDataset.FieldByName('TA_SysUseField').AsString;
          if tempTA_SysUseField <> emptystr then
          begin
            tempFieldDataset:=ABGetConstSqlPubDataset('ABSys_Org_Field',[ABGetDatabaseByConnName(ConnName),SQLParseObject.SQLParseDef.SingleTableName]);
            if (not ABDatasetIsEmpty(tempFieldDataset)) and
               (tempFieldDataset.Locate('Fi_Guid',tempTA_SysUseField,[]))  then
            begin
              FSysUseQueryWhere:=' isnull('+tempFieldDataset.FieldByName('Fi_Name').AsString +',0)=0 '
            end;
          end;
        end;
      end;

      if FTableRowQueryWhere <> emptystr then
      begin
        ABAddstr(tempAddWhere,FTableRowQueryWhere,' and ');
      end;
      if FSysUseQueryWhere <> emptystr then
      begin
        ABAddstr(tempAddWhere,FSysUseQueryWhere,' and ');
      end;
      if tempAddWhere <> emptystr then
      begin
        aSQL := SQLParseObject.AddWhereNoEditOldSQL(tempAddWhere);
      end;
    end;
  end;
begin
  inherited;
  AddCumtomWhere(aSQL);

  aSQL:=ReplaceSql(aSQL);
end;

procedure TABDictionaryQuery.EditBlobFieldDatasets;
var
  i:LongInt;
begin
  for I := 0 to FBlobFieldDefs.Count-1 do
  begin
    if (Assigned(FBlobFieldDefs.Items[i].Datasource)) and
       (Assigned(FBlobFieldDefs.Items[i].Datasource.DataSet)) and
       (FBlobFieldDefs.Items[i].Datasource.DataSet.Active)
        then
      FBlobFieldDefs.Items[i].Datasource.DataSet.Edit;
  end;
end;

procedure TABDictionaryQuery.CancelBlobFieldDatasets;
var
  i:LongInt;
begin
  for I := 0 to FBlobFieldDefs.Count-1 do
  begin
    if (Assigned(FBlobFieldDefs.Items[i].Datasource)) and
       (Assigned(FBlobFieldDefs.Items[i].Datasource.DataSet)) and
       (FBlobFieldDefs.Items[i].Datasource.DataSet.Active)
        then
      FBlobFieldDefs.Items[i].Datasource.DataSet.Cancel;
  end;
end;

procedure TABDictionaryQuery.SaveBlobFieldDatasets;
var
  i:LongInt;
  tempFieldDef:PABFieldDef;
  tempPictureStream: TMemoryStream;
begin
  for I := 0  to FBlobFieldDefs.Count-1 do
  begin
    if (Assigned(FBlobFieldDefs.Items[i].Datasource)) and
       (Assigned(FBlobFieldDefs.Items[i].Datasource.DataSet)) and
       ((FBlobFieldDefs.Items[i].Datasource.AutoEdit) or (ABCheckDataSetInEdit(FBlobFieldDefs.Items[i].Datasource.DataSet)))   then
    begin
      tempFieldDef:=FieldDefByFieldName(FBlobFieldDefs.Items[i].FieldName);
      if (Assigned(tempFieldDef)) and
         (Assigned(tempFieldDef.Fi_Control)) and
         (tempFieldDef.Fi_Control is TcxDBImage) then
      begin
        //控件是否编辑过
        if (TcxDBImage(tempFieldDef.Fi_Control).EditModified) then
        begin
          tempPictureStream:=TMemoryStream.create;
          try
            if (Assigned(TcxDBImage(tempFieldDef.Fi_Control).Picture)) and
               (Assigned(TcxDBImage(tempFieldDef.Fi_Control).Picture.Bitmap))  then
            begin
              TcxDBImage(tempFieldDef.Fi_Control).Properties.OnCustomClick(tempFieldDef.Fi_Control);
            end;

            if (FBlobFieldDefs.Items[i].Datasource.DataSet.State=dsInsert) and
               (Assigned(TcxDBImage(tempFieldDef.Fi_Control).Picture)) and
               (Assigned(TcxDBImage(tempFieldDef.Fi_Control).Picture.Graphic))  then
            begin
              //如果是新增因为BlobFieldDatasets中没有主表新增的数据,所以需要刷新一下BlobFieldDatasets
              //再转修改后再载入照片
              FBlobFieldDefs.Items[i].Datasource.DataSet.Cancel;
              ABReFreshQuery(FBlobFieldDefs.Items[i].Datasource.DataSet,[],self,false);
              FBlobFieldDefs.Items[i].Datasource.DataSet.Edit;

              TBlobField(FBlobFieldDefs.Items[i].Datasource.DataSet.FieldByName(FBlobFieldDefs.Items[i].fieldname)).LoadFromStream(tempPictureStream);
            end;
            FBlobFieldDefs.Items[i].Datasource.DataSet.Post;
          finally
            tempPictureStream.free;
          end;
        end
        else
        begin
          FBlobFieldDefs.Items[i].Datasource.DataSet.Cancel;
        end;
      end;
    end;
  end;
end;

procedure TABDictionaryQuery.GetActiveDetailDataSets(aList: TList);
var
  i:LongInt;
begin
  inherited;

  //从明细表中删除BlobField字段的数据集
  for I := 0  to FBlobFieldDefs.Count-1 do
  begin
    if (Assigned(FBlobFieldDefs.Items[i].Datasource)) and
       (Assigned(FBlobFieldDefs.Items[i].Datasource.DataSet)) then
    begin
      if (aList.IndexOf(FBlobFieldDefs.Items[i].Datasource.DataSet)>=0) then
      begin
        aList.Delete(i);
      end;
    end;
  end;
end;

function TABDictionaryQuery.GetMultiLevelCaption: string;
begin
  Result := FMultiLevelCaption;
end;

procedure TABDictionaryQuery.InternalInitFieldDefs;
begin
  inherited;
end;

function TABDictionaryQuery.FieldDefaultValueCanUse(aFieldName,
  aFieldValue: string): boolean;
var
  tempFieldDef:PABFieldDef;
begin
  result:=inherited FieldDefaultValueCanUse(aFieldName,aFieldValue);

  tempFieldDef:=FieldDefByFieldName(aFieldName);
  if (AnsiCompareText(aFieldValue,'0')<>0)  and
     (Assigned(tempFieldDef)) and
     (Assigned(tempFieldDef.PDownDef)) and
     (Assigned(tempFieldDef.PDownDef.Fi_DataSet)) and
     (tempFieldDef.PDownDef.Fi_DataSet.Active) and
     (AnsiCompareText(tempFieldDef.PDownDef.Fi_SaveField,tempFieldDef.PDownDef.Fi_ViewField)<>0) and
     (abpos(',',aFieldValue)<=0) then
  begin
    //如果模板中的值不在当前数据集中则不设置此值，以防出现主从不一致的情况
    if not tempFieldDef.PDownDef.Fi_DataSet.Locate(tempFieldDef.PDownDef.Fi_SaveField,aFieldValue,[]) then
      result:=false;
  end;
end;

destructor TABDictionaryQuery.Destroy;
begin
  FBlobFieldDefs.Free;

  FLinkDBTableViewList.Free;
  FLinkDBPanelList.Free;

  FPrimaryKeyFieldNames.Free;
  FClusteredFieldNames.Free;

  FreeFieldExtInfos;

  FDefList.Free;
  inherited;
end;

function TABDictionaryQuery.FieldCanUpdateToDatabase(aTableName,aFieldName: String;var aIsIDENTITY:boolean): boolean;
var
  tempFieldDef:PABFieldDef;
begin
  result:=true;
  aIsIDENTITY:=false;
  tempFieldDef:=FieldDefByFieldName(aFieldName);
  //当定义没找到(如在SELECT '' as 临时字段名1 form) ,或者字段所属表不是当前更新表
  if (not Assigned(tempFieldDef)) or
     (not tempFieldDef.Fi_Update) then
  begin
    result:=false;
  end
  else
  begin
    aIsIDENTITY:=
       (Assigned(tempFieldDef)) and
       (Assigned(tempFieldDef.PMetaDef)) and
       (tempFieldDef.PMetaDef.Fi_IsAutoAdd);
    if aIsIDENTITY then
    begin
      result:=false;
    end;
  end;
end;

procedure TABDictionaryQuery.DoBeforeCommitTrans;
begin
  inherited;
  SaveBlobFieldDatasets;
end;

procedure TABDictionaryQuery.DoBeforeOpen;
begin
  inherited;
  ADDDatasetExtFields;
end;

{ TBlobFieldDefs }

constructor TBlobFieldDefs.Create;
begin
  inherited Create(TBlobFieldDef);
end;

function TBlobFieldDefs.FindFieldName(aFieldName: string): TBlobFieldDef;
var
  i:LongInt;
begin
  result:=nil;
  for I := 0  to Count-1 do
  begin
    if (AnsiCompareText(aFieldName,Items[i].FieldName)=0)  then
    begin
      result:=Items[i];
    end;
  end;
end;

function TBlobFieldDefs.GetItem(Index: Integer): TBlobFieldDef;
begin
  Result := TBlobFieldDef(inherited Items[Index]);
end;

procedure TBlobFieldDefs.SetItem(Index: Integer;
  Value: TBlobFieldDef);
begin
  inherited Items[Index] := Value;
end;

{ TABInsideQuery }

constructor TABInsideQuery.Create(AOwner: TComponent);
begin
  inherited;
  FPrimaryKeyFieldNames:=EmptyStr;
end;

destructor TABInsideQuery.Destroy;
begin

  inherited;
end;

procedure TABInsideQuery.DoAfterOpen;
var
  i,j:LongInt;
  tempTableName:string;
  tempField:TField;
begin
  inherited;
  if (not CanModify) then
    exit;

  FPrimaryKeyFieldNames:=EmptyStr;
  for i := 0 to LoadTables.Count-1 do
  begin
    tempTableName:=trim(LoadTables[i]);
    if tempTableName=emptystr then
      exit;

    FPrimaryKeyFieldNames:= ABGetTablePrimaryKeyFieldNames(tempTableName,ConnName,',');
    for j := 1 to ABGetSpaceStrCount(FPrimaryKeyFieldNames,',') do
    begin
      tempField:=FindField(ABGetSpaceStr(FPrimaryKeyFieldNames, j, ','));
      if Assigned(tempField) then
      begin
        tempField.ProviderFlags:=tempField.ProviderFlags + [pfInKey];
      end;
    end;
  end;
end;

procedure ABFinalization;
begin
  Dispose(FPubFieldDownDef);
end;

procedure ABInitialization;
begin
  new(FPubFieldDownDef);
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;


  
end.

