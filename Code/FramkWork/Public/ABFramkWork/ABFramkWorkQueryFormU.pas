{
窗体查询单元
通过数据字典中查询字段来查询数据集数据
}
unit ABFramkWorkQueryFormU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFormU,
  ABPubFuncU,

  ABFramkWorkControlU,
  ABFramkWorkDictionaryQueryU,

  Classes,Controls,Forms,ExtCtrls,Menus,
  StdCtrls, db, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, cxButtons;

type
  TABQueryForm = class(TABPubForm)
    Panel2: TPanel;
    Button1: TABcxButton;
    Button2: TABcxButton;
    Panel1: TPanel;
    Button3: TABcxButton;
    PopupMenu1: TPopupMenu;
    DataSource1: TDataSource;
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    FWhereRemark: string;
    procedure SelectMenuItemOfGroup(Sender: TObject);
    { Private declarations }
  public
    { Public declarations }
  Published
    property WhereRemark : string read FWhereRemark;
  end;

function ABQueryFormQuery(var aOutWhere:string; aDataSet: TABDictionaryQuery;var aQueryForm:TABQueryForm):Boolean;

implementation
{$R *.dfm}

function ABQueryFormQuery(var aOutWhere:string; aDataSet: TABDictionaryQuery;var aQueryForm:TABQueryForm):Boolean;
begin
  Result:=false;
  if not Assigned(aQueryForm) then
  begin
    aQueryForm := TABQueryForm.Create(nil);
    aQueryForm.DataSource1.DataSet:=aDataSet;
    aQueryForm.Height:= ABCreateMultiLevelQueryControls(aQueryForm.Panel1,aQueryForm.Panel1.Owner,aDataSet);
    ABCreateMultiLevelQueryInputMenuItem(aQueryForm.Panel1,aDataSet,aQueryForm.PopupMenu1.Items,aQueryForm.SelectMenuItemOfGroup);
  end;

  if aQueryForm.ShowModal=mrOk then
  begin
    aOutWhere:=ABGetMultiLevelQueryWhere(aQueryForm.Panel1,aDataSet,aQueryForm.FWhereRemark);
    Result:=true;
  end;
end;

procedure TABQueryForm.Button3Click(Sender: TObject);
begin
  ABInputMultiLevelQueryControlValue(Panel1,TABDictionaryQuery(DataSource1.DataSet),PopupMenu1.Items);
end;

procedure TABQueryForm.FormDestroy(Sender: TObject);
begin
  ABFreeMultiLevelQueryControls(Panel1,TABDictionaryQuery(DataSource1.DataSet));
end;

procedure TABQueryForm.SelectMenuItemOfGroup(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
end;

procedure TABQueryForm.Button1Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TABQueryForm.Button2Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

end.


