{
下拉设计面板查询单元
通过数据字典中指定的查询字段生成查询面板来查询数据集数据,需关联TABQuerySelectFieldPanel一起使用
}
unit ABFramkWorkQuerySelectFieldPopupEditU;

interface
{$I ..\ABInclude.ini}

uses
  ABFramkWorkQuerySelectFieldPanelU,

  cxContainer,
  cxDropDownEdit,
  cxEdit,
  Menus,SysUtils,Classes,Controls,DB,ExtCtrls;

type
  TABQuerySelectFieldPopupEdit = class(TcxPopupEdit)
  private
    FPanelQuery: TABQuerySelectFieldPanel;
    FDataSource: TDataSource;
    FDFMControl: string;
    function GetDataSource: TDataSource;
    function GetDFMControl: string;
    procedure SetDataSource(const Value: TDataSource);
    procedure SetDFMControl(const Value: string);
  protected
    procedure DoCloseUp; override;
    procedure DoInitPopup; override;
    procedure Loaded; override;
  public
    property PanelQuery : TABQuerySelectFieldPanel read FPanelQuery write FPanelQuery;
  public
    //弹出设计输入控件的界面
    procedure DesignInputControls;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  Published
    //关联的数据源
    property DataSource : TDataSource read GetDataSource write SetDataSource;
    //序列化到DFM形式的控件信息
    property DFMControl : string read GetDFMControl write SetDFMControl;
  end;

implementation

{ TABQueryPanel }

constructor TABQuerySelectFieldPopupEdit.Create(AOwner: TComponent);
begin
  inherited;
  Text:=EmptyStr;
  FPanelQuery:= TABQuerySelectFieldPanel.Create(self);
  FPanelQuery.Visible:=false;
  Properties.PopupAutoSize:=False;
  Properties.PopupWidth:=0;
end;

procedure TABQuerySelectFieldPopupEdit.DesignInputControls;
begin
  FPanelQuery.DFMControl:=FDFMControl;
  FPanelQuery.DesignInputControls;
  FDFMControl:=FPanelQuery.DFMControl;
end;

destructor TABQuerySelectFieldPopupEdit.Destroy;
begin
  FPanelQuery.Free;
  inherited;
end;

function TABQuerySelectFieldPopupEdit.GetDataSource: TDataSource;
begin
  Result := FDataSource;
end;

function TABQuerySelectFieldPopupEdit.GetDFMControl: string;
begin
  Result := FDFMControl;
end;

procedure TABQuerySelectFieldPopupEdit.SetDataSource(const Value: TDataSource);
begin
  FDataSource:=Value;
  FPanelQuery.DataSource:=FDataSource;
end;

procedure TABQuerySelectFieldPopupEdit.SetDFMControl(const Value: string);
begin
  FDFMControl:=Value;
  FPanelQuery.DFMControl:=FDFMControl;
end;

procedure TABQuerySelectFieldPopupEdit.Loaded;
begin
  inherited;
  Text:=EmptyStr;
end;

procedure TABQuerySelectFieldPopupEdit.DoInitPopup;
begin
  inherited;
  if not Assigned(Properties.PopupControl) then
    Properties.PopupControl:=FPanelQuery;

  if (Assigned(Properties.PopupControl)) and
     (Properties.PopupHeight=200)
      then
    Properties.PopupHeight:=TABQuerySelectFieldPanel(Properties.PopupControl).GetWidth;
end;

procedure TABQuerySelectFieldPopupEdit.DoCloseUp;
begin
  inherited;
  Text:=FPanelQuery.WhereRemark;
end;


end.





