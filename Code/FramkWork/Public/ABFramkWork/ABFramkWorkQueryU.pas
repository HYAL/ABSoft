{
框架数据集单元
}
unit ABFramkWorkQueryU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFuncU,
  ABpubconstU,
  ABPubDBU,
  ABPubScriptU,
  ABPubMessageU,
  ABPubLocalParamsU,
  ABPubLogU,
  ABPubFormU,

  ABThirdCustomQueryU,
  ABThirdQueryU,
  ABThirdDBU,
  ABThirdConnU,
  ABThirdFuncU,

  ABFramkWorkVarU,
  ABFramkWorkControlU,
  ABFramkWorkFuncU,
  ABFramkWorkDictionaryQueryU,
  ABFramkWorkConstU,
  ABFramkWorkcxGridU,
  ABFramkWorkDBPanelU,
  ABFramkWorkQueryFormU,
  ABFramkWorkDBPanelFormU,
  ABFramkWorkDBNavigatorU,

  math,
  Types,StdConvs,Graphics,cxGridLevel,

  SysUtils,Variants,Classes,Controls,Forms,DB,StdCtrls,ExtCtrls,
  cxCustomData,
  cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC,
  cxSplitter, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu;


type
  //默认不能自动进入修改，需按修改按钮才能修改数据
  TABDatasource            = class(TDatasource           )
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    { Public declarations }
  published
    { Published declarations }
  end;

  //框架桌面数据集的子类
  //1.关联表格与面板的显示控件
  //2.加载与保存框架表中设置的SQL
  TABQuery = class(TABDictionaryQuery)
  private
    //面板方式显示数据的窗体
    FDBPanelForm:TForm;
    //关联的GRID多选更新相关
    FMultiSelectFieldNames:array of string;
    FMultiSelectFieldValues:array of Variant;
    FMultiSelectValuesIsCale:array of Boolean;
    FMultiSelectTableView: TABcxGridDBBandedTableView;
    //IsCanAppendTreeItemDown用到的标志值
    FAddNewItemStrGuid:string;
    //查询窗体
    FQueryForm: TABQueryForm;
  private
    FStopMultiSelectUpdate:Boolean;
    FOnAfterShowDBPanel: TNotifyEvent;
    FOnBeforeShowDBPanel: TNotifyEvent;
    { Private declarations }
  protected
    procedure DoBeforeCommitTrans;override;

    procedure Loaded; override;
    procedure DoBeforePost; override;

    procedure DoAfterOpen;   override;
    procedure DoAfterCancel; override;
    procedure DoAfterEdit;  override;
    procedure DoAfterInsert; override;
    procedure DoAfterPost;   override;
    procedure AfterFieldChange(aField: TField; var aDo: Boolean);override;
    { Protected declarations }
    { Public declarations }
  public
    procedure CreateFieldExtInfos;override;
    procedure FreeFieldExtInfos;override;
    //停用关联的GRID多选时的更新
    property StopMultiSelectUpdate:Boolean  read FStopMultiSelectUpdate  write FStopMultiSelectUpdate;

    //开窗查询数据
    procedure OpenQuery;override;
    //以面板方式显示当前记录的数据
    //aCanEdit=显示的数据是否能编辑
    //aActivePageCaption=当前页面标题
    procedure ShowDBPanel(aCanEdit:boolean;
                          aActivePageCaption:string='';
                          aEnabledFieldNames:string='';
                          aVisibleFieldNames:string=''
                          );override;


    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    //以面板方式显示当前记录事件
    property OnBeforeShowDBPanel: TNotifyEvent read FOnBeforeShowDBPanel write FOnBeforeShowDBPanel;
    property OnAfterShowDBPanel: TNotifyEvent read FOnAfterShowDBPanel write FOnAfterShowDBPanel;
    { Published declarations }
  end;

type
  TTWODatasourceArray= array of array of TDatasource;
  TTWODBPanelArray=array of array of TABDBPanel;
  TTWOTableViewArray= array of array of TABcxGridDBBandedTableView;
  TTWOboolean= array of array of boolean;
  TTWOString= array of array of String;

  TThreeDBPanelArray= array of array of array of TABDBPanel;
  TThreeTableViewArray= array of array of array of TABcxGridDBBandedTableView;



//字段对有定义取值SQL的影响
procedure ABFieldForGetValueField(aFieldDef:PABFieldDef;aCheckDataset: TDataSet;aOnlySetOnNull:boolean=false);
//处理字段的编辑对其它引用到此字段的字段取值的影响,aFieldDef为其它字段SQL中引用到的字段名
procedure ABFieldChangeForCiteField(aFieldDef:PABFieldDef);
//关联替换字段的处理
procedure ABFieldChangeForRelatingField(aFieldDef:PABFieldDef;aOnlySetOnNull:boolean=false);

//设置DataSet关联控件控制，为了提高效率,只在数据集的AfterEdit,AfterInsert,AfterCancel,AfterPost中调用
procedure ABSetDataSetControlOnSateChange(aDataSet:TDataSet;
                              aDBPanel:Boolean=False;aDBGrid:Boolean=False;
                              aInit:boolean=false);

//设置DataSet动态控制（指根据其它字段的值决定）
//为了提高效率,只在数据集的AfterEdit AfterFieldChange中调用
procedure ABSetDataSetCiteControl(aDataSet:TDataSet;aFieldDef:PABFieldDef=nil;
                                  aDBPanel:Boolean=False;aDBGrid:Boolean=False);

//设置字段的颜色控制
procedure ABSetFieldReadOnlyAndColor(aFieldDef: PABFieldDef;
                           aReadOnly: boolean;
                           aDBPanel:Boolean=False;aDBGrid:Boolean=False;aCancel:boolean=false);

//功能中窗体数据集初始化
//aDBPanels=关联的面板输入控件
//aTableViews=关联的面板表格控件
//aDataSet=数据集
//aGetData=打开后是否取得数据
//aLoadAutoSQL=是否加载框架表中的SQL
//aSetActive=是否将数据集打开
procedure ABInitFormDataSet(  aDBPanels: array of TABDBPanel;
                              aTableViews:array of TABcxGridDBBandedTableView;
                              aDataSet: TABDictionaryQuery;
                              aGetData:Boolean=true;
                              aLoadAutoSQL:boolean=true;
                              aSetActive:boolean=true
                              );

//功能中多页窗体数据集初始化
procedure ABInitFormPageControl(
                                aPageControl:TcxPageControl;

                                aNavigator:TABDBNavigator;
                                aDBStatusBar:TABdxDBStatusBar;

                                aMainDatasources:array of TDatasource;
                                aMainDBPanels: TTWODBPanelArray;
                                aMainTableViews:TTWOTableViewArray;
                                aMainGetData:array of Boolean;
                                aMainLoadAutoSQL:array of boolean;
                                aMainSetActive:array of boolean;

                                aDetailDatasources:TTWODatasourceArray;
                                aDetailDBPanels:TThreeDBPanelArray;
                                aDetailTableViews:TThreeTableViewArray;
                                aDetailGetData:TTWOboolean;
                                aDetailLoadAutoSQL:TTWOboolean;
                                aDetailSetActive:TTWOboolean;

                                aChangePageControlRefreshMainData:array of  boolean
                                );


implementation

procedure ABInitFormPageControl(
                                aPageControl:TcxPageControl;

                                aNavigator:TABDBNavigator;
                                aDBStatusBar:TABdxDBStatusBar;

                                aMainDatasources:array of TDatasource;
                                aMainDBPanels: TTWODBPanelArray;
                                aMainTableViews:TTWOTableViewArray;

                                aMainGetData:array of Boolean;
                                aMainLoadAutoSQL:array of boolean;
                                aMainSetActive:array of boolean;

                                aDetailDatasources:TTWODatasourceArray;
                                aDetailDBPanels:TThreeDBPanelArray;
                                aDetailTableViews:TThreeTableViewArray;

                                aDetailGetData:TTWOboolean;
                                aDetailLoadAutoSQL:TTWOboolean;
                                aDetailSetActive:TTWOboolean;

                                aChangePageControlRefreshMainData:array of  boolean
                                );
var
  tempIndex,j: Integer;
begin
  tempIndex:=aPageControl.ActivePageIndex;
  if aPageControl.ActivePageIndex<=High(aMainDatasources)-low(aMainDatasources) then
  begin
    ABSetObjectPropValue(aNavigator,'DataSource',aMainDatasources[tempIndex]);
    ABSetObjectPropValue(aDBStatusBar,'DataSource',aMainDatasources[tempIndex]);
  end;

  if not TABQuery(aMainDatasources[tempIndex].DataSet).Active then
  begin
    ABInitFormDataSet(aMainDBPanels[tempIndex],aMainTableViews[tempIndex],TABQuery(aMainDatasources[tempIndex].DataSet),
                      aMainGetData[tempIndex],aMainLoadAutoSQL[tempIndex],aMainSetActive[tempIndex]);

    if  tempIndex<= High(aDetailDatasources)-Low(aDetailDatasources) then
    begin
      for j := Low(aDetailDatasources[tempIndex]) to High(aDetailDatasources[tempIndex]) do
      begin
        if  (tempIndex<= High(aDetailDBPanels)-Low(aDetailDBPanels)) and
            (j<= High(aDetailDBPanels[tempIndex])-Low(aDetailDBPanels[tempIndex])) and
            (tempIndex<= High(aDetailTableViews)-Low(aDetailTableViews)) and
            (j<= High(aDetailTableViews[tempIndex])-Low(aDetailTableViews[tempIndex])) and
            (tempIndex<= High(aDetailDatasources)-Low(aDetailDatasources)) and
            (j<= High(aDetailDatasources[tempIndex])-Low(aDetailDatasources[tempIndex])) and
            (tempIndex<= High(aDetailGetData)-Low(aDetailGetData)) and
            (j<= High(aDetailGetData[tempIndex])-Low(aDetailGetData[tempIndex])) and
            (tempIndex<= High(aDetailLoadAutoSQL)-Low(aDetailLoadAutoSQL)) and
            (j<= High(aDetailLoadAutoSQL[tempIndex])-Low(aDetailLoadAutoSQL[tempIndex])) and
            (tempIndex<= High(aDetailSetActive)-Low(aDetailSetActive)) and
            (j<= High(aDetailSetActive[tempIndex])-Low(aDetailSetActive[tempIndex])) then
        begin
          ABInitFormDataSet(aDetailDBPanels[tempIndex][j],aDetailTableViews[tempIndex][j],TABQuery(aDetailDatasources[tempIndex][j].DataSet),
                            aDetailGetData[tempIndex][j],aDetailLoadAutoSQL[tempIndex][j],aDetailSetActive[tempIndex][j]);
        end;
      end;
    end;
  end
  else if aChangePageControlRefreshMainData[tempIndex] then
  begin
    ABReFreshQuery(TABQuery(aMainDatasources[tempIndex].DataSet),[]);
  end;
end;

procedure ABSetDataSetControlOnSateChange(aDataSet:TDataSet;
                              aDBPanel:Boolean;aDBGrid:Boolean;
                              aInit:boolean);
var
  i:LongInt;
  tempFieldDef:PABFieldDef;
  tempReadOnly:Boolean;
  procedure SetFieldUniqueNotNullColor(aFieldDef: PABFieldDef;aDBPanel:Boolean=False;aDBGrid:Boolean=False);
  var
    tempABDBWinControl:TWinControl;
    tempABDBLabel:TWinControl;
  begin
    if (aFieldDef.Fi_IsInputPanelView)  or
       (aFieldDef.Fi_IsGridView) then
    begin
      if (Assigned(aFieldDef.PMetaDef)) and (not aFieldDef.PMetaDef.Fi_CanNull) or
         (not aFieldDef.Fi_CanNull) then
      begin
        if (aFieldDef.Fi_IsInputPanelView) and (aDBPanel) then
        begin
          tempABDBLabel:=aFieldDef.Fi_Label;
          tempABDBWinControl:=aFieldDef.Fi_Control;
          if (Assigned(tempABDBLabel))  then
          begin
            if (ABQueryParams_ColorInLableOrControl=1)  or
               (ABQueryParams_ColorInLableOrControl>2)   then
            begin
              ABSetCortrolPropertyValue(tempABDBLabel,'Color',ABQueryParams_NotNullColor_Label);
            end;
          end;

          if (Assigned(tempABDBWinControl)) then
          begin
            if (ABQueryParams_ColorInLableOrControl>=2) then
            begin
              ABSetCortrolPropertyValue(tempABDBWinControl,'Color',ABQueryParams_NotNullColor_Control);
            end;
          end;
        end;

        if (aFieldDef.Fi_IsGridView) and (aDBGrid) then
        begin
          if (Assigned(aFieldDef.Fi_GridColumn)) then
          begin
            //设置列的非空字段颜色
          end;
        end;
      end;
    end;
  end;
begin
  for I := 0 to aDataSet.FieldCount - 1 do
  begin
    tempFieldDef:=ABFieldDefByFieldName(aDataSet,aDataSet.Fields[i].FieldName);
    if (not Assigned(tempFieldDef)) then
      Continue;

    //设置只读与非空字段
    tempReadOnly:= ((not tempFieldDef.Fi_CanEditInInsert) and (aDataSet.State in [dsInsert])) or
                   ((not tempFieldDef.Fi_CanEditInEdit) and (aDataSet.State in [dsEdit])) or
                   (not tempFieldDef.Fi_CanEditInEdit) and (not tempFieldDef.Fi_CanEditInInsert) ;
    ABSetFieldReadOnlyAndColor( tempFieldDef,tempReadOnly,
                                (aDBPanel) and (not tempFieldDef.Fi_LockInputPanelView),
                                (aDBGrid) and (not tempFieldDef.Fi_LockGridView),
                                 aInit);

    if (aDataSet.State in [dsEdit,dsInsert]) then
      SetFieldUniqueNotNullColor( tempFieldDef,
                                 (aDBPanel) and (not tempFieldDef.Fi_LockInputPanelView),
                                 (aDBGrid) and (not tempFieldDef.Fi_LockGridView));
  end;
end;

procedure ABSetDataSetCiteControl(aDataSet:TDataSet;
                                   aFieldDef:PABFieldDef;
                                   aDBPanel:Boolean;aDBGrid:Boolean);
var
  i:LongInt;
  tempFieldDef:PABFieldDef;
  tempReadOnly:Boolean;
  tempJavaCommand:string;
begin
  for I := 0 to aDataSet.FieldCount - 1 do
  begin
    tempFieldDef:=ABFieldDefByFieldName(aDataSet,aDataSet.Fields[i].FieldName);
    if not Assigned(tempFieldDef) then
      Continue;

    tempJavaCommand:= tempFieldDef.Fi_SetReadOnlyCommand;
    if (tempJavaCommand<>EmptyStr) then
    begin
      if  ((aFieldDef=nil) or
          (abpos(':'+aFieldDef.Fi_Name,tempJavaCommand)>0)
          ) then
      begin
        tempJavaCommand:=ABReplaceSqlExtParams(tempJavaCommand);
        tempJavaCommand:= ABReplaceSqlFieldNameParams(tempJavaCommand,[aDataSet]);
        tempReadOnly:=ABStrToBool(ABExecScript(tempJavaCommand));

        ABSetFieldReadOnlyAndColor(tempFieldDef,tempReadOnly,
                                   (aDBPanel) and (not tempFieldDef.Fi_LockInputPanelView),
                                   (aDBGrid) and (not tempFieldDef.Fi_LockGridView)
                                    );
      end;
    end;
  end;
end;

procedure ABSetFieldReadOnlyAndColor(aFieldDef: PABFieldDef;
                                     aReadOnly: boolean;aDBPanel:Boolean=False;aDBGrid:Boolean=False;aCancel:boolean=false);
var
  tempABDBWinControl:TWinControl;
  tempABDBLabel:TWinControl;
  tempColor_Label,tempColor_Control:LongInt;
begin
  if (aFieldDef.Fi_IsInputPanelView)  or
     (aFieldDef.Fi_IsGridView) then
  begin
    if (aCancel) or (not aReadOnly) then
    begin
      tempColor_Label:=clWindowText;
      tempColor_Control:=clWindow;
      aReadOnly:=false;
    end
    else
    begin
      tempColor_Label:=ABQueryParams_ReadOnlyColor_Label;
      tempColor_Control:=ABQueryParams_ReadOnlyColor_Control;
    end;
    //设置只读的字段颜色
    if (aFieldDef.Fi_IsInputPanelView) and (aDBPanel) then
    begin
      tempABDBLabel:=aFieldDef.Fi_Label;
      tempABDBWinControl:=aFieldDef.Fi_Control;
      if (Assigned(tempABDBLabel))  then
      begin
        if (ABQueryParams_ColorInLableOrControl=1)  or
           (ABQueryParams_ColorInLableOrControl>2)   then
        begin
          ABSetCortrolPropertyValue(tempABDBLabel,'Color',tempColor_Label);
        end;
      end;
      if (Assigned(tempABDBWinControl)) then
      begin
        if (ABQueryParams_ColorInLableOrControl>=2)   then
        begin
          ABSetCortrolPropertyValue(tempABDBWinControl,'Color',tempColor_Control);
        end;
        ABSetCortrolPropertyValue(tempABDBWinControl,'ReadOnly',aReadOnly);
      end;
    end;

    if (aFieldDef.Fi_IsGridView) and (aDBGrid) then
    begin
      if  (Assigned(aFieldDef.Fi_GridColumn)) then
      begin
        //aFieldDef.Fi_GridColumn.Properties.ReadOnly:=aReadOnly;
        aFieldDef.Fi_GridColumn.Options.Editing:=not aReadOnly;
      end;
    end;
  end;
end;

procedure ABFieldChangeForRelatingField(aFieldDef:PABFieldDef;aOnlySetOnNull:boolean);
var
  tempStr1,tempStr2:string;
  i:longint;
  tempABOnFieldValidate: TFieldNotifyEvent;
  tempABOnFieldChange: TFieldNotifyEvent;
  tempField:Tfield;
begin
  if (aFieldDef.IsDown) and
     (aFieldDef.PDownDef.Fi_RelatingFromFields<>EmptyStr) and
     (aFieldDef.PDownDef.Fi_RelatingToFields<>EmptyStr)  then
  begin
    if aFieldDef.PDownDef.Fi_DataSet.Locate(aFieldDef.PDownDef.Fi_SaveField,aFieldDef.Field.AsString,[]) then
    begin
      for i := 1 to ABGetSpaceStrCount(aFieldDef.PDownDef.Fi_RelatingFromFields,',') do
      begin
        tempStr1:=ABGetSpaceStr(aFieldDef.PDownDef.Fi_RelatingFromFields,i,',');
        tempStr2:=ABGetSpaceStr(aFieldDef.PDownDef.Fi_RelatingToFields,i,',');
        if (not aOnlySetOnNull) or
           (
            (aFieldDef.field.DataSet.FindField(tempStr2).IsNull) or
            (aFieldDef.field.DataSet.FindField(tempStr2).AsString=EmptyStr)
            ) then
        begin
          tempField:=aFieldDef.field.DataSet.FindField(tempStr2);
          tempABOnFieldChange:=tempField.OnChange;
          tempABOnFieldValidate:=tempField.OnValidate;
          tempField.OnChange:=nil;
          tempField.OnValidate:=nil;
          try
            ABSetFieldValue(aFieldDef.PDownDef.Fi_DataSet.FindField(tempStr1).Value,
                            tempField);
          finally
            tempField.OnChange:=tempABOnFieldChange;
            tempField.OnValidate:=tempABOnFieldValidate;
          end;
        end;
      end;
    end;
  end;
end;

procedure ABFieldForGetValueField(aFieldDef:PABFieldDef;aCheckDataset: TDataSet;aOnlySetOnNull:boolean);
var
  tempABOnFieldValidate: TFieldNotifyEvent;
  tempABOnFieldChange: TFieldNotifyEvent;
begin
  if (aFieldDef.Field.IsNull) or (aFieldDef.Field.AsString=EmptyStr) or (not aOnlySetOnNull)  then
  begin
    tempABOnFieldChange:=aFieldDef.Field.OnChange;
    tempABOnFieldValidate:=aFieldDef.Field.OnValidate;
    aFieldDef.Field.OnChange:=nil;
    aFieldDef.Field.OnValidate:=nil;
    try
      if aFieldDef.Fi_GetValueSQL<>emptystr then
        ABSetFieldValue(ABGetSQLValue(TABThirdReadDataQuery(aFieldDef.Field.DataSet).ConnName,aFieldDef.Fi_GetValueSQL,[],null,aCheckDataset),aFieldDef.Field);
    finally
      aFieldDef.Field.OnChange:=tempABOnFieldChange;
      aFieldDef.Field.OnValidate:=tempABOnFieldValidate;
    end;
  end;
end;

procedure ABFieldChangeForCiteField(aFieldDef:PABFieldDef);
var
  i,j:LongInt;
  tempFieldDef:PABFieldDef;
  tempstr,tempstr1,tempstr2,tempFieldName,tempFieldValue:string;

  tempOnGetText: TFieldGetTextEvent;
  tempOnSetText: TFieldSetTextEvent;
  tempOnValidate: TFieldNotifyEvent;
  tempOnChange: TFieldNotifyEvent;
begin
  for I := 0 to aFieldDef.field.DataSet.FieldCount - 1 do
  begin
    tempFieldDef:=ABFieldDefByFieldName(aFieldDef.field.DataSet,aFieldDef.field.DataSet.Fields[i].FieldName);
    if not Assigned(tempFieldDef) then  Continue;

    if (aFieldDef.Field<>aFieldDef.field.DataSet.Fields[i]) then
    begin
      if (tempFieldDef.Field.FieldKind<>fkCalculated) and
         (tempFieldDef.Fi_GetValueSQL<>EmptyStr) and
         (abpos(':'+aFieldDef.Fi_Name,tempFieldDef.Fi_GetValueSQL)>0) then
      begin
        ABFieldForGetValueField(tempFieldDef,aFieldDef.field.DataSet);
      end;

      if (tempFieldDef.IsDown)  then
      begin
        //下拉数据集包含当前字段的引用时的处理
        if (Assigned(ABGetParam(tempFieldDef.PDownDef.Fi_DataSet,aFieldDef.Field.FieldName))) then
        begin
          ABReFreshQuery(tempFieldDef.PDownDef.Fi_DataSet,[],aFieldDef.field.DataSet);
          if tempFieldDef.PDownDef.Fi_DataSet.FieldCount=1 then
          begin
            ABSetFieldValue(tempFieldDef.PDownDef.Fi_DataSet.Fields[0].Value,tempFieldDef.Field);
          end
          else
          begin
            if (tempFieldDef.PDownDef.Fi_CheckInDown) and
               (not tempFieldDef.Field.IsNull) and
               (tempFieldDef.Field.asstring<>emptystr) then
            begin
              if not tempFieldDef.PDownDef.Fi_DataSet.Locate(
                                                        tempFieldDef.PDownDef.Fi_SaveField,
                                                        tempFieldDef.Field.AsString,
                                                        []) then
              begin
                ABBackAndStopFieldEvent(tempFieldDef.Field,tempOnGetText,tempOnSetText,tempOnValidate,tempOnChange);
                try
                  tempFieldDef.Field.Clear;
                finally
                  ABUnBackFieldEvent(tempFieldDef.Field,tempOnGetText,tempOnSetText,tempOnValidate,tempOnChange);
                end;
              end;
            end;
          end;
        end;

        //过滤条件包含当前字段的引用时的处理
        if abpos(':'+aFieldDef.Field.FieldName,tempFieldDef.PDownDef.Fi_Filter)>0 then
        begin
          if (tempFieldDef.PDownDef.Fi_CheckInDown) and
             (not tempFieldDef.Field.IsNull) and
             (tempFieldDef.Field.asstring<>emptystr) then
          begin
            tempstr1:=tempFieldDef.PDownDef.Fi_SaveField;
            tempstr2:=tempFieldDef.Field.AsString;
            for j := 1 to ABGetSpaceStrCount(tempFieldDef.PDownDef.Fi_Filter, ' and ') do
            begin
              tempstr := ABGetSpaceStr(tempFieldDef.PDownDef.Fi_Filter, j, ' and ');
              if ABPos(':' ,tempstr)>0 then
              begin
                tempstr:=ABReplaceSqlFieldNameParams(tempstr,[aFieldDef.Field.DataSet]);

                tempFieldName:= ABGetLeftRightStr(tempstr,axdLeft);
                if tempFieldName<>EmptyStr then
                begin
                  tempstr1:=tempFieldName+';'+tempstr1;
                  tempFieldValue:= ABGetLeftRightStr(tempstr,axdRight);
                  tempstr2:=tempFieldValue+';'+tempstr2;
                end;
              end;
            end;
            if not tempFieldDef.PDownDef.Fi_DataSet.Locate(
                                                      tempstr1,
                                                      ABStrToStringArray(tempstr2,';'),
                                                      []) then
            begin
              ABBackAndStopFieldEvent(tempFieldDef.Field,tempOnGetText,tempOnSetText,tempOnValidate,tempOnChange);
              try
                tempFieldDef.Field.Clear;
              finally
                ABUnBackFieldEvent(tempFieldDef.Field,tempOnGetText,tempOnSetText,tempOnValidate,tempOnChange);
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure ABInitFormDataSet( aDBPanels: array of TABDBPanel;
                      aTableViews:array of TABcxGridDBBandedTableView;
                      aDataSet: TABDictionaryQuery;
                      aGetData:Boolean;
                      aLoadAutoSQL:boolean;
                      aSetActive:boolean
                      );
var
  i: Integer;

  tempOldBeforeScroll:TDataSetNotifyEvent;
  tempOldAfterScroll:TDataSetNotifyEvent;
  tempDataset:TDataSet;
begin
  if (ABLocalParams.Debug) then
    ABWriteLog('Begin procedure '+aDataSet.Owner.Name+' '+aDataSet.Name+'.ABInitFormDataSet');

  ABBackAndStopDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
  try
    if (aLoadAutoSQL) then
    begin
      if (aDataSet.Name<>EmptyStr) and
         (Assigned(aDataSet.Owner)) and
         ((aDataSet.Owner is TForm)) and
         (aDataSet.Owner.Name<>EmptyStr) then
      begin
        tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_FuncSQL',[aDataSet.Owner.Name],TABInsideQuery);
        if (Assigned(tempDataset))   then
        begin
          if  (tempDataset.Locate('FS_Name',aDataSet.Name,[])) then
          begin
            //从数据库中取新的SQL更新到数据集SQL中
            if  (tempDataset.FieldByName('FS_Sql').AsString<>EmptyStr) and
                (
                  (TABDictionaryQuery(aDataSet).Sql.Text=EmptyStr) or
                  (TABDictionaryQuery(aDataSet).SqlUpdateDatetime=0) or
                  (ABDateTimeToStr(tempDataset.FieldByName('FS_UpdateDatetime').AsDateTime)>ABDateTimeToStr(TABDictionaryQuery(aDataSet).SqlUpdateDatetime))
                  ) then
            begin
              TABDictionaryQuery(aDataSet).ConnName:=ABGetConnNameByConnGuid(tempDataset.FieldByName('FS_ConnName').AsString);
              TABDictionaryQuery(aDataSet).Sql.Text:=tempDataset.FieldByName('FS_Sql').AsString;
              TABDictionaryQuery(aDataSet).SqlUpdateDatetime:=tempDataset.FieldByName('FS_UpdateDatetime').AsDateTime;
            end
            //将数据集SQL更新到数据库中
            else  if  (TABDictionaryQuery(aDataSet).Sql.Text<>EmptyStr) and
                      (
                        (tempDataset.IsEmpty) or
                        (tempDataset.FieldByName('FS_Sql').IsNull) or
                        (tempDataset.FieldByName('FS_Sql').AsString=EmptyStr) or
                        (ABDateTimeToStr(tempDataset.FieldByName('FS_UpdateDatetime').AsDateTime)<ABDateTimeToStr(TABDictionaryQuery(aDataSet).SqlUpdateDatetime))
                        ) then
            begin
               ABSetFieldValue(tempDataset,
                                ['FS_FU_Guid','FS_FormName','FS_Name'],
                                [TABDictionaryQuery(aDataSet).FuncGuid,aDataSet.Owner.Name,aDataSet.name],
                                ['FS_ConnName','FS_Sql','FS_UpdateDatetime'],
                                [ABGetConnGuidByConnName(TABQuery(aDataSet).ConnName),TABQuery(aDataSet).Sql.Text,TABQuery(aDataSet).SqlUpdateDatetime]
                                  );
            end;
          end
          else
          begin
            //将数据集SQL更新到数据库中
            if TABDictionaryQuery(aDataSet).Sql.Text<>EmptyStr then
              ABSetFieldValue(tempDataset,
                                ['FS_FU_Guid','FS_FormName','FS_Name'],
                                [TABDictionaryQuery(aDataSet).FuncGuid,aDataSet.Owner.Name,aDataSet.name],
                                ['FS_ConnName','FS_Sql','FS_UpdateDatetime'],
                                [ABGetConnGuidByConnName(TABQuery(aDataSet).ConnName),TABQuery(aDataSet).Sql.Text,TABQuery(aDataSet).SqlUpdateDatetime]
                                  );
          end;
        end;
      end;
    end;
    
    if aSetActive then
    begin
      ABCloseDataset(aDataSet);

      if not aGetData then
        TABThirdReadDataQuery(aDataSet).AddWhereofNext:='1=2';

      if (ABLocalParams.Debug) then
      begin
        ABWriteLog('Begin '+aDataSet.Owner.Name+' '+aDataSet.Name+'.ABReFreshQuery');
        ABWriteLog(aDataSet.Owner.Name+' '+aDataSet.Name+'.SQL.Text '+TABQuery(aDataSet).Sql.Text);
      end;

      ABReFreshQuery(aDataSet,[]);

      if not aGetData then
        TABThirdReadDataQuery(aDataSet).AddWhereofNext:=EmptyStr;

      if (ABLocalParams.Debug) then
        ABWriteLog('End   '+aDataSet.Owner.Name+' '+aDataSet.Name+'.ABReFreshQuery');
      aDataSet.InitActiveOK:=true;
    end;

    if aDataSet.Active then
    begin
      for I := Low(aDBPanels) to high(aDBPanels) do
      begin
        if (Assigned(aDBPanels[i])) and
           (Assigned(aDBPanels[i].Parent)) and
           (Assigned(aDBPanels[i].Parent.Parent)) and
           (aDBPanels[i].Parent.Parent is TcxPageControl) and
           (TcxPageControl(aDBPanels[i].Parent.Parent).Height>0) and
           (aDBPanels[i].AutoHeight) then
        begin
          TcxPageControl(aDBPanels[i].Parent.Parent).Height:=0;
        end;
      end;

      for I := Low(aDBPanels) to high(aDBPanels) do
      begin
        if Assigned(aDBPanels[i]) then
        begin
          if (ABLocalParams.Debug) then
            ABWriteLog('Begin '+aDataSet.Owner.Name+' '+aDBPanels[i].Name+'.RefreshControls');

          aDBPanels[i].RefreshControls(1,1,false);

          if (ABLocalParams.Debug) then
            ABWriteLog('End   '+aDataSet.Owner.Name+' '+aDBPanels[i].Name+'.RefreshControls');
        end;
      end;

      for I := Low(aTableViews) to high(aTableViews) do
      begin
        if Assigned(aTableViews[i]) then
        begin
          if (aTableViews[i] is TABcxGridDBBandedTableView) then
          begin
            if (ABLocalParams.Debug) then
              ABWriteLog('Begin '+aDataSet.Owner.Name+' '+aTableViews[i].Name+'.CreateAllColumn');

            TABcxGridDBBandedTableView(aTableViews[i]).CreateAllColumn;

            if (ABLocalParams.Debug) then
              ABWriteLog('End   '+aDataSet.Owner.Name+' '+aTableViews[i].Name+'.CreateAllColumn');
          end;
        end;
      end;
    end;
  finally
    ABUnBackDatasetEvent(aDataSet,tempOldBeforeScroll,tempOldAfterScroll);
    if aDataSet.Active then
    begin
      if Assigned(aDataSet.AfterScroll) then aDataSet.AfterScroll(aDataSet);
    end;
  end;
  if (ABLocalParams.Debug) then
    ABWriteLog('End   procedure '+aDataSet.Owner.Name+' '+aDataSet.Name+'.ABInitFormDataSet');
end;

{ TABDatasource }

constructor TABDatasource.Create(AOwner: TComponent);
begin
  inherited;
end;

destructor TABDatasource.Destroy;
begin

  inherited;
end;

{ TABQuery }

procedure TABQuery.ShowDBPanel(aCanEdit: boolean;
  aActivePageCaption,
  aEnabledFieldNames,
  aVisibleFieldNames:string);
var
  tempResult:LongInt;
begin
  if (active)   then
  begin
    if Assigned(OnBeforeShowDBPanel) then
      OnBeforeShowDBPanel(self);
    try
      if not Assigned(FDBPanelForm) then
      begin
        FDBPanelForm:= TABDBPanelForm.Create(self);
      end;

      TABDBPanelForm(FDBPanelForm).SetActivePageView(aActivePageCaption);
      TABDBPanelForm(FDBPanelForm).EnabledFieldNames:=aEnabledFieldNames;
      TABDBPanelForm(FDBPanelForm).VisibleFieldNames:=aVisibleFieldNames;

      if (aCanEdit) and
         (ABCheckDataSetCanEdit(self)) and
         (not (State in [dsEdit,dsInsert])) then
      begin
        if (not IsEmpty) or
           (CanInsert) then
          Edit;
      end;

      tempResult:=FDBPanelForm.ShowModal;
      case tempResult of
        mrNo,mrCancel:
        begin
          if (State in [dsEdit,dsInsert]) then
            Cancel;
        end;
      end;
    finally
      if Assigned(OnAfterShowDBPanel) then
       OnAfterShowDBPanel(self);
    end;
  end;
end;

constructor TABQuery.Create(AOwner: TComponent);
begin
  inherited;
  FQueryForm:=nil;
end;

procedure TABQuery.DoBeforeCommitTrans;
begin
  inherited;
end;

destructor TABQuery.Destroy;
begin
  if Assigned(FDBPanelForm) then
    FreeAndNil(FDBPanelForm);
  if Assigned(FQueryForm) then
    FreeAndNil(FQueryForm);

  inherited;
end;

procedure TABQuery.Loaded;
var
  tempSQL:string;
  tempDataset:TDataSet;
begin
  inherited;

  //使得在设计界面打开时总是关闭数据集
  if Active then
    close;

  if (csDesigning in ComponentState)  then
  begin
    if (Name<>EmptyStr) and
       (Assigned(Owner)) and
       ((Owner is TForm)) and
       (Owner.Name<>EmptyStr)  then
    begin
      //属性加载完成后取最数据库中可能新的SQL
      //数据集显示时显示最新的SQL
      //当重编译框架包时再显示模块中的数据集则会在此过程中报错，先注释一下
      if ABIsConn('Main') then
      begin
        tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_FuncSQL',[Owner.Name],TABInsideQuery);
        if (Assigned(tempDataset)) and
           (tempDataset.Locate('FS_Name',Name,[])) then
        begin
          if  (Sql.Text=EmptyStr) or
              (SqlUpdateDatetime=0) or
              (ABDateTimeToStr(tempDataset.FieldByName('FS_UpdateDatetime').AsDateTime)>ABDateTimeToStr(SqlUpdateDatetime)) then
          begin
            tempSql:= VarToStrDef(tempDataset.FieldByName('FS_Sql').AsString,'');

            if tempSQL<>EmptyStr then
            begin
              ConnName:=ABGetConnNameByConnGuid(tempDataset.FieldByName('FS_ConnName').AsString);
              Sql.Text:=tempSQL;
              SqlUpdateDatetime:=tempDataset.FieldByName('FS_UpdateDatetime').AsDateTime;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TABQuery.OpenQuery;
var
  tempWhere:string;
begin
  if ABQueryFormQuery(tempWhere,self,FQueryForm) then
  begin
    RefreshQuery(tempWhere,[]);
  end;
end;

procedure TABQuery.DoAfterCancel;
begin
  inherited;
  ABSetDataSetControlOnSateChange(self,True,True,true);
end;

procedure TABQuery.DoAfterEdit;
begin
  inherited;
  ABSetDataSetControlOnSateChange(self,True,True);

  ABSetDataSetCiteControl(self,nil,true,true);
end;

procedure TABQuery.DoAfterInsert;
begin
  inherited;
  ABSetDataSetControlOnSateChange(self,True,True);
end;

procedure TABQuery.DoAfterOpen;
begin
  inherited;
end;

procedure TABQuery.DoAfterPost;
  procedure UpdateMultiSelect;
  begin
    if FStopMultiSelectUpdate then
      exit;

    FStopMultiSelectUpdate := true;
    try
      if (Assigned(FMultiSelectTableView)) and
         (FMultiSelectTableView.DataController.GetSelectedCount>1) and
         (High(FMultiSelectFieldNames)>=0) then
      begin
        ABSetFieldValue_MultiSelect(FMultiSelectTableView.DataController,
                                    mtSelect,
                                    FMultiSelectFieldNames,
                                    FMultiSelectFieldValues,
                                    FMultiSelectValuesIsCale
                                    );

      end;
    finally
      FStopMultiSelectUpdate := false;
    end;
  end;
var
  tempLastOperatorType: TABDatasetOperateType;
begin
  tempLastOperatorType:=LastOperatorType;
  inherited;
  ABSetDataSetControlOnSateChange(self,True,True,True);

  if (tempLastOperatorType=dtedit) then
  begin
    UpdateMultiSelect;
  end;
end;

procedure TABQuery.DoBeforePost;
  procedure InitMultiSelect;
  var
    ii,i, j: LongInt;
    tempFieldDef:PABFieldDef;
  begin
    if FStopMultiSelectUpdate then
      exit;

    j:=0;
    FMultiSelectTableView:=nil;
    setlength(FMultiSelectFieldNames, j);
    setlength(FMultiSelectFieldValues, j);
    setlength(FMultiSelectValuesIsCale, j);
    try
      for ii := 0 to LinkDBTableViewList.Count-1 do
      begin
        if (Assigned(LinkDBTableViewList[ii])) and
           (TObject(LinkDBTableViewList[ii]) is TABcxGridDBBandedTableView)  then
        begin
          FMultiSelectTableView:=TABcxGridDBBandedTableView(LinkDBTableViewList[ii]);
          //如果关联的是从TableView则取主TableView的活动TableView
          if FMultiSelectTableView.DataController.MasterKeyFieldNames<>EmptyStr then
            FMultiSelectTableView:=
              TABcxGridDBBandedTableView(TABcxGridDBBandedTableView(FMultiSelectTableView.MasterGridView).
                ViewData.Rows[TABcxGridDBBandedTableView(FMultiSelectTableView.MasterGridView).
                                DataController.FocusedRowIndex].AsMasterDataRow.ActiveDetailGridView);

          if FMultiSelectTableView.DataController.GetSelectedCount>1 then
          begin
            for I := 0 to FieldCount-1 do
            begin
              tempFieldDef:=FieldDefByFieldName(Fields[i].FieldName);
              if (Assigned(tempFieldDef)) and
                 (tempFieldDef.Fi_Edited) and
                 (tempFieldDef.Fi_BatchEdit)  then
              begin
                j:=j+1;
                setlength(FMultiSelectFieldNames, j);
                setlength(FMultiSelectFieldValues, j);
                setlength(FMultiSelectValuesIsCale, j);
                FMultiSelectFieldNames[j-1]:=Fields[i].FieldName;
                FMultiSelectFieldValues[j-1]:=tempFieldDef.Field.Value;
                FMultiSelectValuesIsCale[j-1]:=false;
              end;
            end;
          end;
        end;
        //暂时只处理一个关联
        Break;
      end;
    finally
      FStopMultiSelectUpdate := false;
    end;
  end;
begin
  inherited;

  if (State =dsedit) then
  begin
    InitMultiSelect;
  end;
end;

procedure TABQuery.AfterFieldChange(aField: TField;var aDo:Boolean);
var
  tempFieldDef:PABFieldDef;
begin
  inherited;
  tempFieldDef:=FieldDefByFieldName(aField.FieldName);
  if Assigned(tempFieldDef) then
  begin
    if aDo then
    begin
      ABFieldChangeForRelatingField(tempFieldDef);
      ABFieldChangeForCiteField(tempFieldDef);

      ABSetDataSetCiteControl(self,tempFieldDef,true,true);
    end;
  end;
end;

procedure TABQuery.CreateFieldExtInfos;
var
  i:LongInt;
  tempFieldDef:PABFieldDef;
  procedure AppendDownListItem_Append(aDataSet: TDataSet;aTi_Tr_Guid:string);
  begin
    if not Assigned(aDataSet.FindField('Ti_Code')) then
      exit;

    if not aDataSet.Active then
      aDataSet.Active:=true;

    if (aDataSet.Active) and
       (aDataSet.FieldCount > 0) and
       (FAddNewItemStrGuid=emptystr) then
    begin
      FAddNewItemStrGuid:=ABGetGuid;
      aDataSet.Append;
      if Assigned(aDataSet.FindField('Ti_Tr_Guid')) then
        aDataSet.FindField('Ti_Tr_Guid').AsString :=aTi_Tr_Guid;

      aDataSet.FindField('TI_ParentGuid').AsString :='0';
      aDataSet.FindField('Ti_Guid').AsString :=FAddNewItemStrGuid;
      aDataSet.FindField('PubID').AsString :=FAddNewItemStrGuid;
      aDataSet.FindField('Ti_Name').AsString := ABcxLookupComboBoxAddNewItemStr;
      aDataSet.Post;
    end;
  end;
begin
  inherited;

  for I := 0 to DefList.Count - 1 do
  begin
    tempFieldDef:=PABFieldDef(DefList.Objects[i]);
    if (Assigned(tempFieldDef)) AND
       (tempFieldDef.IsDown) then
    begin
      if tempFieldDef.IsCanAppendTreeItemDown then
        AppendDownListItem_Append(tempFieldDef.PDownDef.Fi_DataSet,tempFieldDef.PDownDef.Fi_Tr_Code);
    end;
  end;
end;

procedure TABQuery.FreeFieldExtInfos;
var
  i:LongInt;
  tempFieldDef:PABFieldDef;
  procedure DelDownListItem_Append(aDataSet: TDataSet);
  begin
    if not Assigned(aDataSet.FindField('Ti_Code')) then
      exit;

    if not aDataSet.Active then
      aDataSet.Active:=true;

    if (aDataSet.Active) and
       (aDataSet.FieldCount > 0) and
       (FAddNewItemStrGuid<>EmptyStr) and
       (aDataSet.Locate('Ti_Guid', FAddNewItemStrGuid, [])) then
    begin
      aDataSet.Delete;
      FAddNewItemStrGuid:=EmptyStr;
    end;
  end;
begin
  for I := 0 to DefList.Count - 1 do
  begin
    tempFieldDef:=PABFieldDef(DefList.Objects[i]);
    if Assigned(tempFieldDef) then
    begin
      if tempFieldDef.IsDown then
      begin
        ABFreePubDownEdit(tempFieldDef.Flag);

        if Assigned(tempFieldDef.PDownDef.Fi_DataSet)  then
        begin
          if tempFieldDef.IsCanAppendTreeItemDown then
            DelDownListItem_Append(tempFieldDef.PDownDef.Fi_DataSet);
        end;
      end;
    end;
  end;

  inherited;
end;



end.



