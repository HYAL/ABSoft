inherited ABQueryAllFieldComboxFrame: TABQueryAllFieldComboxFrame
  ParentFont = False
  ParentShowHint = False
  ShowHint = True
  object Splitter1: TSplitter
    Left = 308
    Top = 0
    Height = 300
    ExplicitLeft = 309
    ExplicitTop = 1
    ExplicitHeight = 23
  end
  object Panel33: TPanel
    Left = 413
    Top = 0
    Width = 128
    Height = 300
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 0
    object CheckBox1: TCheckBox
      Left = 49
      Top = 3
      Width = 79
      Height = 20
      BiDiMode = bdLeftToRight
      Caption = #21253#21547#24050#36807#28388
      Ctl3D = False
      ParentBiDiMode = False
      ParentCtl3D = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object Button1: TButton
      Left = 1
      Top = 1
      Width = 46
      Height = 24
      Caption = #26597#35810
      TabOrder = 1
      OnClick = Button1Click
    end
  end
  object Panel44: TPanel
    Left = 0
    Top = 0
    Width = 230
    Height = 300
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
    object Label2: TLabel
      Left = 0
      Top = 0
      Width = 30
      Height = 300
      Align = alLeft
      Alignment = taCenter
      AutoSize = False
      Caption = #26597#25214':'
      Layout = tlCenter
      ExplicitLeft = -3
      ExplicitTop = -2
      ExplicitHeight = 30
    end
    object ComboBox1: TComboBox
      AlignWithMargins = True
      Left = 33
      Top = 3
      Width = 194
      Height = 21
      Align = alClient
      Style = csDropDownList
      DropDownCount = 20
      TabOrder = 0
      OnDropDown = ComboBox1DropDown
      OnKeyPress = ComboBox1KeyPress
      OnSelect = ComboBox1Select
    end
  end
  object ComboBox2: TComboBox
    AlignWithMargins = True
    Left = 233
    Top = 3
    Width = 72
    Height = 21
    Align = alLeft
    Style = csDropDownList
    DropDownCount = 20
    ItemIndex = 0
    TabOrder = 2
    Text = #31561#20110
    OnKeyPress = ComboBox2KeyPress
    OnSelect = ComboBox2Select
    Items.Strings = (
      #31561#20110
      #19981#31561#20110
      #23567#20110
      #23567#20110#31561#20110
      #22823#20110
      #22823#20110#31561#20110
      #21253#21547
      #19981#21253#21547
      #24038#21253#21547
      #24038#19981#21253#21547
      #21491#21253#21547
      #21491#19981#21253#21547
      #20174'*'#21040'*'
      #19981#20174'*'#21040'*'
      #22312#21015#34920
      #19981#22312#21015#34920
      #20026#31354
      #19981#20026#31354)
  end
  object Panel11: TPanel
    Left = 321
    Top = 111
    Width = 285
    Height = 23
    BevelOuter = bvNone
    TabOrder = 3
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 70
      Height = 23
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      object Edit1: TABcxTextEdit
        Left = 0
        Top = 0
        Align = alClient
        TabOrder = 0
        OnDblClick = Edit1DblClick
        OnKeyPress = Edit1KeyPress
        Width = 70
      end
    end
    object Panel2: TPanel
      Left = 70
      Top = 0
      Width = 70
      Height = 23
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      object Label1: TLabel
        Left = 25
        Top = 0
        Width = 16
        Height = 23
        Align = alLeft
        Alignment = taCenter
        AutoSize = False
        Caption = #27490
        Layout = tlCenter
      end
      object Edit2: TABcxTextEdit
        Left = 0
        Top = 0
        Align = alLeft
        TabOrder = 0
        OnKeyPress = Edit2KeyPress
        Width = 25
      end
      object Edit3: TABcxTextEdit
        Left = 41
        Top = 0
        Align = alClient
        TabOrder = 1
        OnKeyPress = Edit3KeyPress
        Width = 29
      end
    end
    object Panel3: TPanel
      Left = 140
      Top = 0
      Width = 70
      Height = 23
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 2
      Visible = False
      object Label3: TLabel
        Left = 33
        Top = 0
        Width = 16
        Height = 23
        Align = alLeft
        Alignment = taCenter
        AutoSize = False
        Caption = #27490
        Layout = tlCenter
        ExplicitLeft = 25
      end
      object ABcxDateTimeEdit1: TABcxDateTimeEdit
        Left = 0
        Top = 0
        Align = alLeft
        Properties.Kind = ckDateTime
        TabOrder = 0
        OnKeyPress = ABcxDateTimeEdit1KeyPress
        Width = 33
      end
      object ABcxDateTimeEdit2: TABcxDateTimeEdit
        Left = 49
        Top = 0
        Align = alClient
        Properties.Kind = ckDateTime
        TabOrder = 1
        OnKeyPress = ABcxDateTimeEdit2KeyPress
        Width = 21
      end
    end
  end
end
