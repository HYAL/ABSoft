{
框架控件单元
}
unit ABFramkWorkControlU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubSelectCheckComboBoxU,
  ABPubManualProgressBar_ThreadU,
  ABPubAutoProgressBar_ThreadU,
  ABPubInPutStrU,
  ABPubOrderStrU,
  ABPubSelectStrU,
  ABPubConstU,
  ABPubFuncU,
  ABPubDBU,
  ABPubDBMainDetailPopupMenuU,
  ABPubLogU,
  ABPubMessageU,
  ABPubVarU,
  ABPubUserU,
  ABPubMemoU,
  ABPubPanelU,

  ABThird_DevExtPopupMenuU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdFuncU,
  ABThirdImportDataU,
  ABThirdcxGridAndcxTreeViewSearchU,
  ABThirdCacheDatasetU,
  ABThirdCustomQueryU,

  ABFramkWorkConstU,
  ABFramkWorkVarU,
  ABFramkWorkFuncU,
  ABFramkWorkDictionaryQueryU,
  ABFramkWorkTreeItemEditU,
  ABFramkWorkDownManagerU,
  ABFramkWorkUserU,

  cxGridDBTableView,
  cxGridCustomView,
  cxCustomPivotGrid,
  cxPivotGrid,
  dxdbtree,
  cxDBData,
  cxTLExportLink,
  cxLookAndFeels,
  cxPC,
  cxInplaceContainer,
  cxDBTL,
  cxTLData,
  cxGridCustomTableView,
  cxControls,
  cxClasses,
  cxGrid,
  cxTextEdit,
  cxMaskEdit,
  cxMemo,
  cxCalendar,
  cxButtonEdit,
  cxCheckBox,
  cxImageComboBox,
  cxSpinEdit,
  cxCalc,
  cxHyperLinkEdit,
  cxTimeEdit,
  cxCurrencyEdit,
  cxImage,
  cxBlobEdit,
  cxMRUEdit,
  cxDropDownEdit,
  cxDBLookupEdit,
  cxLookupEdit,
  cxRadioGroup,
  cxListBox,
  cxLabel,
  cxProgressBar,
  cxTrackBar,
  cxCheckListBox,
  cxColorComboBox,
  cxFontNameComboBox,
  cxCheckComboBox,
  cxCheckGroup,
  cxRichEdit,
  cxShellComboBox,
  cxDBLookupComboBox,
  cxDBEdit,
  cxDBLabel,
  cxDBProgressBar,
  cxDBTrackBar,
  cxDBCheckListBox,
  cxDBColorComboBox,
  cxDBFontNameComboBox,
  cxDBCheckGroup,
  cxDBRichEdit,
  cxDBShellComboBox,
  cxButtons,
  cxGroupBox,
  cxContainer,
  cxSpinButton,
  cxMCListBox,
  cxListView,
  cxTreeView,
  cxHeader,
  cxSplitter,
  cxShellListView,
  cxShellTreeView,
  cxEdit,
  cxLookAndFeelPainters,
  cxTL,
  dxStatusBar,
  dxtree,
  dxdbtrel,
  cxFilter,
  cxGridDBBandedTableView,
  cxCustomData,
  cxGridDBDataDefinitions,
  cxGridTableView,
  cxDBCheckComboBox,
  cxDBExtLookupComboBox,
  cxDBPivotGrid,

  printers,
  Types,
  Messages,
  Forms,math,Windows,SysUtils,Variants,Classes,Graphics,Dialogs,Controls,DB,DBCtrls,
  ComCtrls,Menus,StdCtrls,ExtCtrls,TypInfo,DateUtils;


type
  //时间控件
  //表格列设置为TABcxTimeEditProperties的注册名时,当激活表格列单元格内的控件时GetContainerClass调用并创建单元格内部控件
  //在创建的单元格内部控件中通过InplaceParams.Position.recordindex,InplaceParams.Position.Item来访问表格列对象和记录号
  //TABcxTimeEditProperties通过GetRegisteredEditProperties.Register(TABcxTimeEditProperties, 'ABTimeEdit|ABTimeEdit');注册
  TABcxTimeEditProperties = class(TcxTimeEditProperties)
  private
  protected
  public
    constructor Create(AOwner: TPersistent); override;
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxTimeEdit = class(TcxTimeEdit)
  private
  protected
  public
    procedure PrepareEditValue(const ADisplayValue: TcxEditValue;
      out EditValue: TcxEditValue; AEditFocused: Boolean); override;
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBTimeEdit = class(TcxDBTimeEdit)
  private
  protected
  public
    procedure PrepareEditValue(const ADisplayValue: TcxEditValue;
      out EditValue: TcxEditValue; AEditFocused: Boolean); override;
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //日期控件
  TABcxDateEditProperties = class(TcxDateEditProperties)
  private
  protected
  public
    constructor Create(AOwner: TPersistent); override;
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxDateEdit = class(TcxDateEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBDateEdit = class(TcxDBDateEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //日期时间控件
  TABcxDateTimeEditProperties = class(TcxDateEditProperties)
  private
  protected
  public
    constructor Create(AOwner: TPersistent); override;
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxDateTimeEdit = class(TcxDateEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBDateTimeEdit = class(TcxDBDateEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //单选组控件
  TABcxRadioGroupProperties = class(TcxRadioGroupProperties)
  private
  protected
  public
    constructor Create(AOwner: TPersistent); override;
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxRadioGroup = class(TcxRadioGroup)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBRadioGroup = class(TcxDBRadioGroup)
  private
  protected
  public
    constructor Create(AOwner: TComponent); override;
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //复选框控件,按删除键时清除选择
  TABcxCheckBoxProperties = class(TcxCheckBoxProperties)
  private
  protected
  public
    constructor Create(AOwner: TPersistent); override;
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxCheckBox = class(TcxCheckBox)
  private
  protected
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
  public
    constructor Create(AOwner: TComponent); override;
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBCheckBox = class(TcxDBCheckBox)
  private
  protected
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
  public
    constructor Create(AOwner: TComponent); override;
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //备注控件
  TABcxMemoProperties = class(TcxMemoProperties)
  private
  protected
  public
    constructor Create(AOwner: TPersistent); override;
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxMemo = class(TcxMemo)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBMemo = class(TcxDBMemo)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //有下拉功能的备注控件,按第一个按钮时弹出备注框
  TABcxComBoBoxMemoProperties = class(TcxMRUEditProperties)
  private
  protected
  public
    constructor Create(AOwner: TPersistent); override;
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxComBoBoxMemo = class(TcxMRUEdit)
  private
  protected
    procedure DblClick; override;
    procedure DoButtonClick(AButtonVisibleIndex: Integer); override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBComBoBoxMemo = class(TcxDBMRUEdit)
  private
  protected
    procedure DblClick; override;
    procedure DoButtonClick(AButtonVisibleIndex: Integer); override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //目录名选择控件,按第一个按钮时选择目录
  TABcxDirSelectProperties = class(TcxMRUEditProperties)
  private
  protected
  public
    constructor Create(AOwner: TPersistent); override;
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxDirSelect = class(TcxMRUEdit)
  private
    FBegPath: string;
  protected
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure DoButtonClick(AButtonVisibleIndex: Integer); override;
  public
    constructor Create(AOwner: TComponent); override;
    property BegPath:string  read FBegPath write FBegPath;
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBDirSelect = class(TcxDBMRUEdit)
  private
    FBegPath: string;
  protected
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure DoButtonClick(AButtonVisibleIndex: Integer); override;
  public
    constructor Create(AOwner: TComponent); override;
    property BegPath:string  read FBegPath write FBegPath;
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //文件名选择控件,按第一个按钮时选择文件
  TABcxFileSelectProperties = class(TcxMRUEditProperties)
  private
  protected
  public
    constructor Create(AOwner: TPersistent); override;
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxFileSelect = class(TcxMRUEdit)
  private
    FBegPath: string;
  protected
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure DoButtonClick(AButtonVisibleIndex: Integer); override;
  public
    constructor Create(AOwner: TComponent); override;
    property BegPath:string  read FBegPath write FBegPath;
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBFileSelect = class(TcxDBMRUEdit)
  private
    FBegPath: string;
  protected
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure DoButtonClick(AButtonVisibleIndex: Integer); override;
  public
    constructor Create(AOwner: TComponent); override;
    property BegPath:string  read FBegPath write FBegPath;
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //文件字段控件
  TABcxFileBlobFieldProperties = class(TcxButtonEditProperties)
  private
  protected
  public
    constructor Create(AOwner: TPersistent); override;
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxFileBlobField = class(TcxButtonEdit)
  private
  protected
    procedure DoButtonClick(AButtonVisibleIndex: Integer); override;
  public
    constructor Create(AOwner: TComponent); override;
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBFileBlobField = class(TcxDBButtonEdit)
  private
  protected
    procedure DoButtonClick(AButtonVisibleIndex: Integer); override;
  public
    constructor Create(AOwner: TComponent); override;
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //字段名选择控件,按第一个按钮时选择所关联的数据集的字段名
  TABcxFieldSelectsProperties = class(TcxButtonEditProperties)
  private
  protected
  public
    constructor Create(AOwner: TPersistent); override;
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxFieldSelects = class(TcxButtonEdit)
  private
  protected
    procedure DoButtonClick(AButtonVisibleIndex: Integer); override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBFieldSelects = class(TcxDBButtonEdit)
  private
  protected
    procedure DoButtonClick(AButtonVisibleIndex: Integer); override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //框架下拉SQL字段名选择控件,按第一个按钮时选择字段框架SQL的SQL字段名
  //如果关联的是ABSys_Org_Field表则取关联数据集Fi_DownSQL字段值的字段名
  TABcxSQLFieldSelectsProperties = class(TcxButtonEditProperties)
  private
  protected
  public
    constructor Create(AOwner: TPersistent); override;
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxSQLFieldSelects              =class(TcxButtonEdit)
  private
  protected
    procedure DoButtonClick(AButtonVisibleIndex: Integer); override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBSQLFieldSelects              =class(TcxDBButtonEdit)
  private
  protected
    procedure DoButtonClick(AButtonVisibleIndex: Integer); override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //排序字段控件,按第一个按钮时设置数据集排序
  TABcxFieldOrderProperties = class(TcxButtonEditProperties)
  private
  protected
  public
    constructor Create(AOwner: TPersistent); override;
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxFieldOrder              =class(TcxButtonEdit)
  private
  protected
    procedure DoButtonClick(AButtonVisibleIndex: Integer); override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBFieldOrder              =class(TcxDBButtonEdit)
  private
  protected
    procedure DoButtonClick(AButtonVisibleIndex: Integer); override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //多列下拉控件(因为有一些限制，如不能保存配制)所以框架统一使用 TABcxExtLookupComboBoxProperties
  TABcxLookupComboBoxProperties = class(TcxLookupComboBoxProperties)
  private
    FDisplayField: string;
  protected
    function GetDisplayLookupText(const AKey: TcxEditValue): string; override;
  published
  public
    constructor Create(AOwner: TPersistent); override;
    class function GetContainerClass: TcxContainerClass; override;
  published
    property DisplayField:string read FDisplayField write FDisplayField;
  end;

  TABcxLookupComboBox = class(TcxLookupComboBox)
  private
  protected
    procedure DoCloseUp; override;
    procedure DoInitPopup; override;
    procedure DblClick; override;
    procedure DoOnChange; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBLookupComboBox = class(TcxDBLookupComboBox)
  private
  protected
    procedure DoCloseUp; override;
    procedure DoInitPopup; override;
    procedure DblClick; override;
    procedure DoOnChange; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //表格下拉控件
  //对于框架中有引用前列过滤或可编辑的下拉列项，仅能使用 TABcxExtLookupComboBoxProperties
  TABcxCustomComboboxInnerEdit              =class(TcxCustomComboboxInnerEdit)
  private
    { Private declarations }
  protected
    procedure Change; override;
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;
    { Public declarations }
  published
    { Published declarations }
  end;

  TABcxExtLookupComboBoxProperties = class(TcxExtLookupComboBoxProperties)
  private
    FDisplayField: string;
  protected
    function GetDisplayLookupText(const AKey: TcxEditValue): string; override;
  public
    constructor Create(AOwner: TPersistent); override;
    class function GetContainerClass: TcxContainerClass; override;
  published
    property DisplayField:string read FDisplayField write FDisplayField;
  end;

  TABcxExtLookupComboBox  =class(TcxExtLookupComboBox)
  private
  protected
    procedure DoInitPopup; override;
    procedure DoCloseUp; override;
    procedure DblClick;override;
    procedure DoOnChange; override;
    function GetInnerEditClass: TControlClass; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBExtLookupComboBox     = class(TcxDBExtLookupComboBox    )
  private
  protected
    procedure DblClick; override;
    procedure DoCloseUp; override;
    procedure DoInitPopup; override;
    procedure DoOnChange; override;
    function GetInnerEditClass: TControlClass; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //复选框列表控件
  TABcxCheckComboBoxProperties = class(TcxCheckComboBoxProperties)
  private
  protected
    procedure CalculateCheckStatesByEditValue(Sender: TObject;
      const AEditValue: TcxEditValue; var ACheckStates: TcxCheckStates); override;
    function CalculateEditValueByCheckStates(Sender: TObject;
      const ACheckStates: TcxCheckStates): TcxEditValue; override;
  public
    class function GetContainerClass: TcxContainerClass; override;
    constructor Create(AOwner: TPersistent); override;
  published
  end;

  TABcxCheckComboBox = class(TcxCheckComboBox)
  private
  protected
    procedure DoInitPopup; override;
    procedure DblClick; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBCheckComboBox = class(TcxDBCheckComboBox)
  private
  protected
    procedure DoInitPopup; override;
    procedure DblClick; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //下拉框列表控件
  TABcxComboBoxProperties = class(TcxComboBoxProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
    constructor Create(AOwner: TPersistent); override;
  published
  end;

  TABcxComboBox = class(TcxComboBox)
  private
  protected
    procedure DoInitPopup; override;
    procedure DblClick; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBComboBox = class(TcxDBComboBox)
  private
  protected
    procedure DoInitPopup; override;
    procedure DblClick; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //打印机选择控
  TABcxPrintSelectProperties = class(TcxComboBoxProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
    constructor Create(AOwner: TPersistent); override;
  published
  end;

  TABcxPrintSelect = class(TcxComboBox)
  private
  protected
    procedure DoInitPopup; override;
    procedure DblClick; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBPrintSelect = class(TcxDBComboBox)
  private
  protected
    procedure DoInitPopup; override;
    procedure DblClick; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //树形列表控件
  TABcxTreeViewPopupEditProperties = class(TcxPopupEditProperties)
  private
  protected
  public
    constructor Create(AOwner: TPersistent); override;
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  //框架使用时在表格列
  //在下拉开窗前会先定位
  TABcxTreeViewPopupEdit               =class(TcxPopupEdit)
  private
  protected
    procedure DoInitPopup; override;
    procedure DblClick;override;
    procedure SetupPopupWindow; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //加强了右键菜单的主从数据树形感知树形控件
  //在下拉开窗前会先定位
  TABcxDBTreeViewPopupEdit               =class(TcxDBPopupEdit)
  private
  protected
    procedure DoInitPopup; override;
    procedure DblClick;override;
    procedure SetupPopupWindow; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  //图片控件,增加压缩图片的功能
  TABcxImageProperties = class(TcxImageProperties)
  private
    FZipWidth: longint;
    FZipHeight: longint;
    FAfterZipStream: TMemoryStream;
    function GetAfterZipStream: TMemoryStream;
  protected
  public
    constructor Create(AOwner: TPersistent); override;
    destructor Destroy; override;
    class function GetContainerClass: TcxContainerClass; override;

    //对照片压缩
    procedure DoCustomClick;
    //压缩后的照片流
    property AfterZipStream:TMemoryStream read GetAfterZipStream;
  published
    property ZipWidth:longint read FZipWidth write FZipWidth;
    property ZipHeight:longint read FZipHeight write FZipHeight;
  end;

  TABcxImage = class(TcxImage)
  private
  protected
  public
    procedure CustomClick; override;
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBImage = class(TcxDBImage)
  private
  protected
  public
    procedure CustomClick; override;
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxTextEditProperties = class(TcxTextEditProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxTextEdit = class(TcxTextEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBTextEdit = class(TcxDBTextEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxMaskEditProperties = class(TcxMaskEditProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxMaskEdit = class(TcxMaskEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBMaskEdit = class(TcxDBMaskEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxButtonEditProperties = class(TcxButtonEditProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxButtonEdit = class(TcxButtonEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBButtonEdit = class(TcxDBButtonEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxSpinEditProperties = class(TcxSpinEditProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxSpinEdit = class(TcxSpinEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBSpinEdit = class(TcxDBSpinEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxCalcEditProperties = class(TcxCalcEditProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxCalcEdit = class(TcxCalcEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBCalcEdit = class(TcxDBCalcEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxHyperLinkEditProperties = class(TcxHyperLinkEditProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxHyperLinkEdit = class(TcxHyperLinkEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBHyperLinkEdit = class(TcxDBHyperLinkEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxCurrencyEditProperties = class(TcxCurrencyEditProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxCurrencyEdit = class(TcxCurrencyEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBCurrencyEdit = class(TcxDBCurrencyEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxBlobEditProperties = class(TcxBlobEditProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxBlobEdit = class(TcxBlobEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBBlobEdit = class(TcxDBBlobEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxMRUEditProperties = class(TcxMRUEditProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxMRUEdit = class(TcxMRUEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBMRUEdit = class(TcxDBMRUEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxProgressBarProperties = class(TcxProgressBarProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxProgressBar = class(TcxProgressBar)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBProgressBar = class(TcxDBProgressBar)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxTrackBarProperties = class(TcxTrackBarProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxTrackBar = class(TcxTrackBar)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBTrackBar = class(TcxDBTrackBar)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxRichEditProperties = class(TcxRichEditProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxRichEdit = class(TcxRichEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBRichEdit = class(TcxDBRichEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxPopupEditProperties = class(TcxPopupEditProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxPopupEdit = class(TcxPopupEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBPopupEdit = class(TcxDBPopupEdit)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxCheckGroupProperties = class(TcxCheckGroupProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxCheckGroup = class(TcxCheckGroup)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBCheckGroup = class(TcxDBCheckGroup)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxLabelProperties = class(TcxLabelProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxLabel = class(TcxLabel)
  private
  protected
  public
    constructor Create(AOwner: TComponent); override;
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBLabel = class(TcxDBLabel)
  private
  protected
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxImageComboBoxProperties = class(TcxImageComboBoxProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxImageComboBox = class(TcxImageComboBox)
  private
  protected
    procedure DblClick; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBImageComboBox = class(TcxDBImageComboBox)
  private
  protected
    procedure DblClick; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxColorComboBoxProperties = class(TcxColorComboBoxProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxColorComboBox = class(TcxColorComboBox)
  private
  protected
    procedure DblClick; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBColorComboBox = class(TcxDBColorComboBox)
  private
  protected
    procedure DblClick; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxFontNameComboBoxProperties = class(TcxFontNameComboBoxProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxFontNameComboBox = class(TcxFontNameComboBox)
  private
  protected
    procedure DblClick; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBFontNameComboBox = class(TcxDBFontNameComboBox)
  private
  protected
    procedure DblClick; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxShellComboBoxProperties = class(TcxShellComboBoxProperties)
  private
  protected
  public
    class function GetContainerClass: TcxContainerClass; override;
  published
  end;

  TABcxShellComboBox = class(TcxShellComboBox)
  private
  protected
    procedure DblClick; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxDBShellComboBox = class(TcxDBShellComboBox)
  private
  protected
    procedure DblClick; override;
  public
    class function GetPropertiesClass: TcxCustomEditPropertiesClass; override;
  published
  end;

  TABcxCheckListBox = class(TcxCheckListBox)
  private
  protected
  public
    constructor Create(AOwner: TComponent); override;
  published
  end;

  TABcxDBCheckListBox = class(TcxDBCheckListBox)
  private
  protected
  public
    constructor Create(AOwner: TComponent); override;
  published
  end;

  TABcxTreeList = class(TcxTreeList)
  private
  protected
  public
  published
  end;

  TABcxDBTreeList = class(TcxDBTreeList)
  private
  protected
  public
  published
  end;

  //为列表中项增加描述
  //如果描述是单行则Remarks[ItemIndex]就能返回描述
  //如果描述是多行且格式为"[Items[0]]第一项描述[Items[1]]第二项描述"则GetCurRemark就能返回指定项的描述
  TABcxListBox = class(TcxListBox)
  private
    FRemarks: TStrings;
    function GetRemarks: TStrings;
    procedure SetRemarks(const Value: TStrings);
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function GetCurRemark(aIndex:LongInt): string;
  published
    property Remarks:TStrings read GetRemarks write SetRemarks ;
  end;

  TABcxDBListBox = class(TcxDBListBox)
  private
  protected
  public
  published
  end;

  TABcxPivotGrid = class(TcxPivotGrid)
  private
  protected
  public
  published
  end;

  //增强右键菜单的交叉表控件，需调用右键菜单的load来加载表格配制
  TABcxDBPivotGrid            = class(TcxDBPivotGrid           )
  private
    FExtPopupMenu: TABcxPivotGridPopupMenu;
  protected
  public
    //交叉表右键菜单
    property ExtPopupMenu:TABcxPivotGridPopupMenu read FExtPopupMenu write FExtPopupMenu;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure ExpandAll;
    procedure CollapseALL;
  published
  end;

  //增加一全部展开的属性,True时表示全部展开，False时表示全收起
  TABcxTreeView = class(TcxTreeView)
  private
    FExtFullExpand: Boolean;
    procedure SetExtFullExpand(const Value: Boolean);
  protected
  public
  published
    property ExtFullExpand: Boolean read FExtFullExpand write SetExtFullExpand;
  end;

  //主从数据树形感知的右键菜单
  TABcxDBTreeViewPopupMen = class(TABDBMainDetailPopupMenu)
  private
    FSetupFullFileName: string;
    FSetupFileNameSuffix: string;
    procedure OutFile(aOutFileType: TABOutFileType);
    //得到配制文件名
    function GetFilename(aTypeFlag: string): string;
  protected
    procedure CreatePopupMenu; override;
    procedure DoPopup(Sender: TObject); override;
  public
    //输出文件 ,一般由右键菜单调用
    procedure PopupMenu_OutExcel(Sender: TObject);
    procedure PopupMenu_OutXML(Sender: TObject);
    procedure PopupMenu_OutText(Sender: TObject);
    procedure PopupMenu_OutHtml(Sender: TObject);
    //查询内容 ,一般由右键菜单调用
    procedure PopupMenu_Search(Sender: TObject);
    //保存配制文件,一般由右键菜单调用
    procedure PopupMenu_SaveSetup(Sender: TObject);
    //加载配制文件配制，一般由引用的功能模块调用
    procedure LoadViewProperty;
  public
    //配制文件全名，当不为空时取得的配件文件名=SetupFullFileName+文件类型标识
    property  SetupFullFileName: string read FSetupFullFileName write FSetupFullFileName;
    //配制文件后缀名,当SetupFullFileName为空时取得的配件文件名=配制文件目录+所在窗体名+控件名+SetupFileNameSuffix+文件类型标识
    property  SetupFileNameSuffix: string read FSetupFileNameSuffix write FSetupFileNameSuffix;
  published
  end;

  //因为TdxDBTreeView有一些缺陷，所以统一使用TcxDBTreeList
  TABcxDBTreeView = class(TcxDBTreeList)
  private
    FDataSource: TDataSource;
    FDataField: string;
    FPopupMenu: TABcxDBTreeViewPopupMen;
    FActive: Boolean;
    FExtFullExpand: Boolean;
    FLinkPopupEdit: TcxCustomPopupEdit;
    FCanSelectParent: Boolean;
    procedure SetExtFullExpand(const Value: Boolean);

    //初始化右键菜单的字段
    procedure InitAutoPopupMenu;
    procedure SetActive(const Value: Boolean);
    procedure SetLinkPopupEdit(const Value: TcxCustomPopupEdit);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure KeyPress(var Key: Char); override;
    procedure DoFocusedItemChanged(APrevFocusedItem,
      AFocusedItem: TcxCustomInplaceEditContainer); override;

    procedure DblClick; override;
    procedure DoEnter; override;

    procedure DoEndDrag(Target: TObject; X, Y: Integer); override;
    procedure DoStartDrag(var DragObject: TDragObject); override;
  public
    procedure DragOver(Source: TObject; X, Y: Integer; State: TDragState;
      var Accept: Boolean); override;
    constructor Create(AOwner: TComponent); override;

    property LinkPopupEdit: TcxCustomPopupEdit read FLinkPopupEdit write SetLinkPopupEdit;
  published
    //关联的数据源和字段名
    property DataSource: TDataSource read FDataSource write FDataSource;
    property DataField: string read FDataField write FDataField;

    //是否激活，当没有数据列时激活会创建数据列
    property Active: Boolean read FActive write SetActive;
    //是否全展开
    property ExtFullExpand: Boolean read FExtFullExpand write SetExtFullExpand;
    //是否能选择有子结点的父结点
    property CanSelectParent: Boolean read FCanSelectParent write FCanSelectParent;
  end;

  //数据感知的标签，通过调用RefreshCaption过程刷新标签内容
  TABDBFieldCaption = class(TABcxLabel)
  private
    FDataSource: TDataSource;
    FDataField: string;
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    procedure RefreshCaption;
  published
    property DataField: string read FDataField write FDataField;
    property DataSource: TDataSource read FDataSource write FDataSource;
  end;

  //增加的状态栏，可显示关联的数据集的当前记录号与总记录数,如果关联了表格则显示表格当前记录号与总记录数
  TABdxDBStatusBar=class;
  TABdxDBStatusBarDataLink = class(TDataLink)
  private
    FStatusBar: TABdxDBStatusBar;
  protected
    procedure ActiveChanged; override;
    procedure RecordChanged(Field: TField); override;
  public
  published
  end;

  //与数据集关联的状态栏
  TABOnRefreshNotifyEvent = procedure(Sender: TObject;var aPanelIndex:LongInt) of object;
  TABdxDBStatusBar = class(TdxStatusBar)
  private
    FDataLink: TABdxDBStatusBarDataLink;

    FStopUpdate: Boolean;
    FOnRefresh: TABOnRefreshNotifyEvent;

    function GetDataSource: TDataSource;
    procedure SetDataSource(const Value: TDataSource);

    procedure Refresh;
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    procedure DoRefresh(aPanelIndex: Integer; aDataSource: TDataSource);

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property StopUpdate: Boolean read FStopUpdate write FStopUpdate;

    property DataSource: TDataSource read GetDataSource write SetDataSource;

    property OnRefresh  : TABOnRefreshNotifyEvent   read FOnRefresh   write FOnRefresh   ;
  end;

  TABcxHeader = class(TcxHeader)
  private
  protected
  public
  published
  end;

  TABcxSpinButton = class(TcxSpinButton)
  private
  protected
  public
  published
  end;

  TABcxMCListBox = class(TcxMCListBox)
  private
  protected
  public
  published
  end;

  TABcxListView = class(TcxListView)
  private
  protected
  public
  published
  end;

  TABcxSplitter = class(TcxSplitter)
  private
  protected
  public
    constructor Create(AOwner: TComponent); override;
  published
  end;

  TABcxGroupBox = class(TcxGroupBox)
  private
  protected
  public
    constructor Create(AOwner: TComponent); override;
  published
  end;

  TABdxStatusBar = class(TdxStatusBar)
  private
  protected
  public
    constructor Create(AOwner: TComponent); override;
  published
  end;

  TABcxPageControl = class(TcxPageControl)
  private
  protected
    procedure AfterLoaded; override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property ActivePageIndex;
  end;

  TABcxShellListView = class(TcxShellListView)
  private
  protected
  public
  published
  end;

  TABcxShellTreeView = class(TcxShellTreeView)
  private
  protected
  public
  published
  end;

  TABcxTabControl = class(TcxTabControl)
  private
  protected
  public
  published
  end;

  //cxVirtualTreeList是需自己动态增加结点的树列表, GetChildCount事件用于设置某父结点下的子结点数量
  {
  procedure GetChildCount(Sender: TcxCustomTreeList; AParentNode: TcxTreeListNode; var ACount: Integer);
  begin
    //当父结点层小于4时每层为10个字节点
    if AParentNode.Level < 4 then
      ACount := 10;
  end;

  //cxVirtualTreeList是需自己动态增加结点的树列表, GetNodeValue事件用于设置结点下的值
  procedure TABTestForm.ABcxVirtualTreeList1GetNodeValue(
    Sender: TcxCustomTreeList; ANode: TcxTreeListNode; AColumn: TcxTreeListColumn;
    var AValue: Variant);
  begin
    case AColumn.ItemIndex of
      0:AValue := ANode.VisibleIndex;
      1:
        AValue := 'Level:' + IntToStr(ANode.Level);
      2:
        AValue := Now + ANode.VisibleIndex * 0.001
    else
      AValue := ''
    end;
  end;
  }
  TABcxVirtualTreeList = class(TcxVirtualTreeList)
  private
  protected
  public
  published
  end;

  TABcxRadioButton = class(TcxRadioButton)
  private
  protected
  public
  published
  end;

  //增加点击时日志与显示进度条
  TABcxButton = class(TcxButton)
  private
    FProgressBarCaption: string;
    FShowProgressBar: Boolean;
    FFuncPoints: string;
    procedure SetFuncPoints(const Value: string);
  protected
    procedure Loaded;override;
  public
    procedure Click; override;
    constructor Create(AOwner: TComponent); override;
  published
    //是否点击时显示进度
    property ShowProgressBar: Boolean read FShowProgressBar write FShowProgressBar;
    //显示进度窗的标题
    property ProgressBarCaption: string read FProgressBarCaption write FProgressBarCaption;
    //授权的功能点
    property FuncPoints: string read FFuncPoints write SetFuncPoints;
  end;

//根据aDataSet去刷新容器控件中数据感知标签的Caption
procedure ABRefreshLabelCaption(aParent:Twincontrol;aDataSet:TDataSet);

//解释创建输入参数的控件 ,参数格式
//{[ClassName][Caption][Name][ConnName][DefaultValue][FillSql][Paren tField][Keyfield][ListField][DownField]
//[Fix] 固定输入
//[LabelTop][Labelleft][LabelWidth][LabelHeight][ControlTop][Controlleft][ControlWidth][ControlHeight]}
//[ValueReplaceName],[ValueReplaceValue] 根据值去计算出ValueReplaceValue的结果，然后替换后续SQL中ValueReplaceName标识的部分
//aAutoParentHeight为True 时返回创建的控件数量,aAutoParentHeight为False 时返回aParent的高度
//[CloumnCount] TABcxRadioGroup 类型的列数[SetEndTime] 23:59:59
procedure ABCreateInputParams(
                              aText:string;
                              aParent:Twincontrol;
                              aOwner:TComponent;

                              aMinLabelCharNum:longint=12;
                              aRowOnlyOneControl:Boolean=True;

                              aMinParentWidth:longint=0;
                              aAutoParentWidth:Boolean=True;

                              aMinParentHeight:longint=0;
                              aAutoParentHeight:Boolean=True;

                              aleftspacing:LongInt=5;
                              aTopspacing:LongInt=5;
                              aRighttspacing:longint=5;
                              aBottomspacing:longint=8;
                              aXspacing:longint=1;
                              aYspacing:longint=1;
                              aAutoSuit:Boolean=true;
                              aSetakRight:Boolean=true;
                              aSetakBottom:Boolean=true
                              );
//设置控件中子控件的每行最后一个和最后一行的尺寸及Anchors
//aAutoSuit 是否自适应边界,
procedure ABSetRightControlAndLastControl(  aPanel:Twincontrol;
                                            aMaxWidth:longint;
                                            aMaxHeight:longint;

                                            aRighttspacing:longint;
                                            aBottomspacing:longint;

                                            aAutoSuit:Boolean=false;
                                            aSetakRight:Boolean=true;
                                            aSetakBottom:Boolean=true
                                            );
//调整控件容器尺寸
procedure ABSetParentControlSize( aPanel:Twincontrol;
                                  aMaxWidth:longint;
                                  aMaxHeight:longint;

                                  aMinParentWidth:longint=0;
                                  aAutoParentWidth:Boolean=True;
                                  aMinParentHeight:longint=0;
                                  aAutoParentHeight:Boolean=True
                                    );
//得到控件容器最适应的宽度和高度
procedure ABGetParentControlWidthAndHeight(
                                  aPanel:Twincontrol;
                                  var aMaxWidth:longint;
                                  var aMaxHeight:longint
                                    );

//设置控件的大小
//预留12个字符（6个汉字的宽度）为标签的宽度
procedure ABSetControlSize(var aLableWidth,aLableHeight,aControlWidth,aControlHeight:LongInt;
                           aLabel,aControl:TWinControl;
                           aMinLabelCharNum:longint=12;
                           aInitZero:Boolean=true;
                           aSetWidthHeight:Boolean=true;
                           aUseLabelHightSameControlHeight :Boolean=true
                           );
//将文本aText中的参数定义替换为对应控件的值
function ABReplaceInputParamsValue(var aIsFind:boolean;var aFindControl:TControl;
                                   aText:string;aParent,aQuoteParent:Twincontrol;
                                   aQuoted:boolean=true):string;
//根据输入参数值转变成WHERE条件返回
function ABGetInputParamsWhere(aDataset:TDataSet;aText:string;aParent:Twincontrol;var aWhereRemark:string): string;
//保存面板上的控件位置为SQL并返回
function ABSaveControlPositionToSQL(aSQL: string;aParentPanel:TWinControl;aOnlyControlLeft:boolean): string;


//创建多层查询的控件,返回所有多层的高度
function ABCreateMultiLevelQueryControls( aParent:TWinControl;
                                          aOwner:TComponent;
                                          aDataset:TABDictionaryQuery;
                                          aRowOnlyOneControl:Boolean=True;

                                          aMinParentWidth:longint=0;
                                          aAutoParentWidth:Boolean=True;

                                          aMinParentHeight:longint=0;
                                          aAutoParentHeight:Boolean=True;

                                          aAutoSuit:Boolean=true;
                                          aSetakRight:Boolean=true;
                                          aSetakBottom:Boolean=true
                                          ):LongInt;
//创建多层查询的导入菜单项
procedure ABCreateMultiLevelQueryInputMenuItem(aParent:TWinControl;
                                           aDataset:TABDictionaryQuery;
                                           aInputMenuItem:TMenuItem;
                                           aMenuItemOnClick:TNotifyEvent
                                           );
//导入多行数据到多层查询的控件中
procedure ABInputMultiLevelQueryControlValue(aParent:TWinControl;
                                             aDataset:TABDictionaryQuery;
                                             aInputMenuItem:TMenuItem);
//取得多层查询的输入条件
function ABGetMultiLevelQueryWhere( aParent:TWinControl;
                                    aDataset:TABDictionaryQuery;
                                    var aWhereRemark:string): string;

//创建多层查询的字段下拉列表
procedure ABCreateMultiLevelQueryFieldNameStrings( aDataset:TABDictionaryQuery;
                                          aStrings:TStrings
                                          );
//释放多层查询的控件
procedure ABFreeMultiLevelQueryControls(aParent:TWinControl;
                                        aDataset:TABDictionaryQuery);

//得到多层查询的控件显示名称
function ABGetMultiLevelCaption(aDataset: TABDictionaryQuery):String;

//设置不同类型Properties属性的默认值
procedure ABSetPropertiesPublicValue(aProperties:TcxCustomEditProperties);
//取得显示字段返回的显示内容
function ABGetDisplayLookupText(aDataController:TcxDBDataController;aDisplayFields: string): string;
//当传入的为Ctrl和Delete按键时将列表中的指定内容删除
procedure ABDeleteItemONKeyDown(aItems:TStrings;aText:string;aCurKey: Word; aCurShift: TShiftState;aDeleteOfKey: Word=VK_DELETE; aDeleteOfShift: TShiftState=[ssCtrl]);

//填入数据集数据到控件中
function ABFillDatasetToControl( aObject:TObject;aProperties:TcxCustomEditProperties;aCloumnCount:longint;aDataset:TDataset;aViewFields:string):boolean;
//息创建字段的输入控件和CXGRID列
//aType=1 创建，
//aType=2 刷新
//aType=3 创建且刷新
function ABCreateOrReFreshCloumnAndControl( aType:LongInt;
                                            aDataSource:TDataSource;
                                            aControlDataSource:TDataSource;
                                            aControlType,aControlName,
                                            aFieldName,aDownFields,aViewFields,aSaveFields:string;

                                            aDownType,
                                            aFilter,
                                            aParentField,
                                            aDownCaption,
                                            aPassChar,aMask:string;

                                            aMaxLength,aCloumnCount:longint;

                                            aReadOnly,
                                            aCanSelectParent:boolean;

                                            aOwner:TComponent;
                                            aCloumnAndControl:TObject):TObject;
//根据框架字段信息创建字段的输入控件和CXGRID列
function ABCreateCloumnAndControl(aDataSource:TDataSource;
                                  aField:TField;
                                  aOwner:TComponent):TObject;
//根据框架字段信息刷新字段的输入控件和CXGRID列
procedure ABReFreshCloumnAndControl( aDataSource:TDataSource;
                                    aField:TField;
                                    aCloumnAndControl:TObject);
//注册常用的类型
procedure ABRegisterClass(aClass:array of TPersistentClass);


//取得下拉项管理的窗体
function ABGetDownManagerForm: TABDownManagerForm;
//取得下拉项组件
function ABGetPubDownEdit(aName:string;
                          aDataSource:TDataSource;
                          aControlType,
                          aDownFields,aViewFields,aSaveFields:string;

                          aFilter:string;

                          aCanSelectParent:boolean;
                          aParentField:string): TComponent;
//释放下拉项
procedure ABFreePubDownEdit(aName:string);overload;
procedure ABFreePubDownEdit(aIndex:longint);overload;
//释放所有下拉项
procedure ABFreePubDownEdit;overload;

//得到控件的值
function ABGetControlValue(aControl:TWinControl):Variant;
//设置控件的值
procedure ABSetControlValue(aControl:TWinControl;aValue:Variant);
//清除控件的值
procedure ABClearControlValue(aControl:TWinControl);


implementation

var
  FABDownManagerForm: TABDownManagerForm;
  FPubDownEditList: TStrings;

procedure ABFileBlobFieldButtonClick(aDataSource:TDataSource; aField: TField;aButtonIndex: Integer);
var
  tempKeyFieldNames,
  tempWhere: string;
  tempFieldName:string;
  tempFieldDef:PABFieldDef;
begin
  if (Assigned(aDataSource)) and
     (Assigned(aDataSource.DataSet)) and
     (Assigned(aField)) then
  begin
    tempFieldDef:=ABFieldDefByFieldName(aField.DataSet,aField.FieldName);
    if (Assigned(tempFieldDef)) then
    begin
      tempKeyFieldNames:=TABDictionaryQuery(aField.DataSet).PrimaryKeyFieldNames.Values[tempFieldDef.Fi_Ta_Name];
      if tempKeyFieldNames<>EmptyStr then
      begin
        tempWhere:= ABFieldNamesAndFieldValuesToWhere(
                                        aField.DataSet,
                                        ABStrToStringArray(tempKeyFieldNames),
                                        [],
                                        [],
                                        ' and ');
        if tempWhere<>EmptyStr then
        begin
          if aButtonIndex=0 then
          begin
            if (aDataSource.AutoEdit) or (ABCheckDataSetInEdit(aDataSource.DataSet)) then
            begin
              tempFieldName:=ABSelectFile('','','All Files|*.*');
              if ABCheckFileExists(tempFieldName)  then
              begin
                ABPostDataset(aField.DataSet);
                ABUpFileToField(TABThirdReadDataQuery(aField.DataSet).ConnName,
                                tempFieldDef.Fi_Ta_Name,aField.FieldName,
                                tempWhere,
                                tempFieldName);
                ABReFreshQuery(aField.DataSet,[]);
              end;
            end;
          end
          else if aButtonIndex=1 then
          begin
            tempFieldName:=ABSaveFile('','','All Files|*.*');
            if tempFieldName<>EmptyStr  then
            begin
              ABDownToFileFromField(TABThirdReadDataQuery(aField.DataSet).ConnName,
                              tempFieldDef.Fi_Ta_Name,aField.FieldName,
                              tempWhere,
                              tempFieldName);
            end;
          end
          else  if aButtonIndex=2 then
          begin
            if (aDataSource.AutoEdit) or (ABCheckDataSetInEdit(aDataSource.DataSet)) then
            begin
              ABPostDataset(aField.DataSet);
              ABUpFileToField(TABThirdReadDataQuery(aField.DataSet).ConnName,
                              tempFieldDef.Fi_Ta_Name,aField.FieldName,
                              tempWhere,
                              EmptyStr);
              ABReFreshQuery(aField.DataSet,[]);
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure ABInitFileBlobFieldButton(aProperties: TcxButtonEditProperties);
var
  tempButton:TcxEditButton;
begin
  aProperties.ReadOnly:=True;
  if aProperties.Buttons.Count<>3 then
  begin
    while aProperties.Buttons.Count>0 do
    begin
      aProperties.Buttons.Delete(0);
    end;

    tempButton:=aProperties.Buttons.Add;
    tempButton.Caption:='上传';
    tempButton.Kind:=bkText;

    tempButton:=aProperties.Buttons.Add;
    tempButton.Caption:='下载';
    tempButton.Kind:=bkText;

    tempButton:=aProperties.Buttons.Add;
    tempButton.Caption:='清除';
    tempButton.Kind:=bkText;
  end;
end;

function ABGetControlValue(aControl:TWinControl):Variant;
var
  I:longint;
begin
  Result:=emptystr;
  if aControl is TABcxTreeViewPopupEdit then
  begin
    if (TABcxTreeViewPopupEdit(aControl).Text<>emptystr) and
       (Assigned(TABcxTreeViewPopupEdit(aControl).Properties)) and
       (Assigned(TABcxTreeViewPopupEdit(aControl).Properties.PopupControl)) and
       (TABcxTreeViewPopupEdit(aControl).Properties.PopupControl is TABcxDBTreeView) then

    Result:=TABcxDBTreeView(TABcxTreeViewPopupEdit(aControl).Properties.PopupControl).DataController.DataSource.DataSet.FindField(
            TABcxDBTreeView(TABcxTreeViewPopupEdit(aControl).Properties.PopupControl).DataController.KeyField
              ).AsString;;
  end
  else if (aControl is TcxDateEdit) and
     (not TcxDateEdit(aControl).Properties.ShowTime) then
  begin
    Result:=TcxDateEdit(aControl).EditValue;
    if not ABVarIsNull(Result) then
      Result:=ABDateToStr(TcxDateEdit(aControl).Date);
  end
  else if (aControl is TcxDateEdit) and
          (TcxDateEdit(aControl).Properties.ShowTime) then
  begin
    Result:=TcxDateEdit(aControl).EditValue;
    if not ABVarIsNull(Result) then
      Result:=ABDateTimeToStr(TcxDateEdit(aControl).Date,23);
  end
  else if aControl is TcxTimeEdit then
  begin
    Result:=ABTimeToStr(TcxTimeEdit(aControl).Time);
  end
  else if aControl is TcxCalcEdit  then
  begin
    Result:=TcxCalcEdit(aControl).EditValue;
    if not ABVarIsNull(Result) then
      Result:=FloatToStr(Result)
  end
  else if aControl is TcxSpinEdit  then
  begin
    Result:=TcxSpinEdit(aControl).EditValue;
    if not ABVarIsNull(Result) then
      Result:=FloatToStr(Result)
  end
  else if aControl is TcxCurrencyEdit  then
  begin
    Result:=TcxCurrencyEdit(aControl).EditValue;
    if not ABVarIsNull(Result) then
      Result:=FloatToStr(Result)
  end
  else if aControl is TcxTextEdit  then
  begin
    Result:=TcxTextEdit(aControl).EditValue;
  end
  else if aControl is TcxMaskEdit  then
  begin
    Result:=TcxMaskEdit(aControl).EditValue;
  end
  else if aControl is TcxLabel  then
  begin
    Result:=TcxLabel(aControl).EditValue;
  end
  else if aControl is TcxMemo  then
  begin
    Result:=TcxMemo(aControl).EditValue;
  end
  else if aControl is TcxRichEdit  then
  begin
    Result:=TcxRichEdit(aControl).EditValue;
  end
  else if aControl is TcxCheckBox  then
  begin
    case TcxCheckBox(aControl).State of
      cbsUnchecked:
      begin
        Result:='0';
      end;
      cbsChecked:
      begin
        Result:='1';
      end;
      cbsGrayed:
      begin
        Result:='';
      end;
    end;
  end
  else if aControl is TcxCheckComboBox  then
  begin
    Result:=TcxCheckComboBox(aControl).EditValue;
  end
  else if aControl is TcxCheckListBox  then
  begin
    Result:=TcxCheckListBox(aControl).EditValue;
  end
  else if aControl is TcxCheckGroup  then
  begin
    Result:=TcxCheckGroup(aControl).EditValue;
  end
  else if aControl is TcxRadioGroup  then
  begin
    Result:=TcxRadioGroup(aControl).ItemIndex;
  end
  else if aControl is TcxComboBox  then
  begin
    Result:=TcxComboBox(aControl).EditValue;
  end
  else if aControl is TcxColorComboBox  then
  begin
    Result:=TcxColorComboBox(aControl).EditValue;
  end
  else if aControl is TcxFontNameComboBox then
  begin
    Result:=TcxFontNameComboBox(aControl).EditValue;
  end
  else if aControl is TcxShellComboBox then
  begin
    Result:=TcxShellComboBox(aControl).EditValue;
  end
  else if aControl is TcxExtLookupComboBox  then
  begin
    Result:=TcxExtLookupComboBox(aControl).EditValue;
  end
  else if aControl is TcxImageComboBox  then
  begin
    Result:=TcxImageComboBox(aControl).EditValue;
  end
  else if aControl is TcxMRUEdit  then
  begin
    Result:=TcxMRUEdit(aControl).EditValue;
  end
  else if aControl is TcxListBox  then
  begin
    for I := 0 to TcxListBox(aControl).Count - 1 do
    begin
      if TcxListBox(aControl).Selected[i] then
      begin
        Result:=ABAddstr_Ex(Result,TcxListBox(aControl).items[i]);
      end;
    end;
  end
  else if aControl is TdxLookupTreeView then
  begin
    if (Assigned(TdxLookupTreeView(aControl).ListSource)) and
       (Assigned(TdxLookupTreeView(aControl).ListSource.DataSet)) and
       (TdxLookupTreeView(aControl).KeyField<>EmptyStr) then
    begin
      if TdxLookupTreeView(aControl).ListSource.DataSet.Locate(TdxLookupTreeView(aControl).ListField,
                                                                 TdxLookupTreeView(aControl).text,[]) then
        Result:=TdxLookupTreeView(aControl).ListSource.DataSet.FindField(TdxLookupTreeView(aControl).KeyField).AsString;
    end;
  end
  else if aControl is TcxHyperLinkEdit  then
  begin
    Result:=TcxHyperLinkEdit(aControl).EditValue;
  end
  else if aControl is TcxBlobEdit  then
  begin
    Result:=TcxBlobEdit(aControl).EditValue;
  end
  else if aControl is TcxPopupEdit  then
  begin
    Result:=TcxPopupEdit(aControl).EditValue;
  end
  else if aControl is TcxButtonEdit  then
  begin
    Result:=TcxButtonEdit(aControl).EditValue;
  end
  else if aControl is TcxImage  then
  begin
    Result:=TcxImage(aControl).EditValue;
  end
  {
  else if aControl is TcxComBoBoxMemo  then
  begin
    Result:=TcxComBoBoxMemo(aControl).EditValue;
  end
  else if aControl is TcxFieldSelects  then
  begin
    Result:=TcxFieldSelects(aControl).EditValue;
  end
  else if aControl is TcxFieldOrder  then
  begin
    Result:=TcxFieldOrder(aControl).EditValue;
  end
  else if aControl is TcxSQLFieldSelects  then
  begin
    Result:=TcxSQLFieldSelects(aControl).EditValue;
  end
  else if aControl is TcxDirSelect  then
  begin
    Result:=TcxDirSelect(aControl).EditValue;
  end
  else if aControl is TcxFileSelect  then
  begin
    Result:=TcxFileSelect(aControl).EditValue;
  end
  }
  else if aControl is TcxProgressBar  then
  begin
    Result:=TcxProgressBar(aControl).EditValue;
  end
  else if aControl is TcxTrackBar  then
  begin
    Result:=TcxTrackBar(aControl).EditValue;
  end
  else if aControl is TcxLookupComboBox then
  begin
    Result:=TcxLookupComboBox(aControl).EditValue;
  end
  else if aControl is TcxListView  then
  begin
    for I := 0 to TcxListView(aControl).Items.Count - 1 do
    begin
      if TcxListView(aControl).Items[i].Selected then
      begin
        Result:=ABAddstr_Ex(Result,TcxListView(aControl).items[i].Caption);
      end;
    end;
  end
  else if aControl is TEdit  then
  begin
    Result:=TEdit(aControl).Text;
  end;
end;

procedure ABClearControlValue(aControl:TWinControl);
begin
  if (aControl is TcxDateEdit) then
  begin
    TcxDateEdit(aControl).Clear;
  end
  else if aControl is TcxTimeEdit then
  begin
    TcxTimeEdit(aControl).Clear;
  end
  else if aControl is TcxCalcEdit  then
  begin
    TcxCalcEdit(aControl).Clear;
  end
  else if aControl is TcxSpinEdit  then
  begin
    TcxSpinEdit(aControl).Clear;
  end
  else if aControl is TcxCurrencyEdit  then
  begin
    TcxCurrencyEdit(aControl).Clear;
  end
  else if aControl is TcxTextEdit  then
  begin
    TcxTextEdit(aControl).Clear;
  end
  else if aControl is TcxMaskEdit  then
  begin
    TcxMaskEdit(aControl).Clear;
  end
  else if aControl is TcxLabel  then
  begin
    TcxLabel(aControl).Clear;
  end
  else if aControl is TcxMemo  then
  begin
    TcxMemo(aControl).Clear;
  end
  else if aControl is TcxRichEdit  then
  begin
    TcxRichEdit(aControl).Clear;
  end
  else if aControl is TcxCheckBox  then
  begin
    TcxCheckBox(aControl).State:=cbsGrayed;
  end
  else if aControl is TcxCheckComboBox  then
  begin
    TcxCheckComboBox(aControl).Clear;
  end
  else if aControl is TcxCheckListBox  then
  begin
    TcxCheckListBox(aControl).Clear;
  end
  else if aControl is TcxCheckGroup  then
  begin
    TcxCheckGroup(aControl).Clear;
  end
  else if aControl is TcxRadioGroup  then
  begin
    TcxRadioGroup(aControl).Clear;
  end
  else if aControl is TcxComboBox  then
  begin
    TcxComboBox(aControl).Clear;
  end
  else if aControl is TcxColorComboBox  then
  begin
    TcxColorComboBox(aControl).Clear;
  end
  else if aControl is TcxFontNameComboBox then
  begin
    TcxFontNameComboBox(aControl).Clear;
  end
  else if aControl is TcxShellComboBox then
  begin
    TcxShellComboBox(aControl).Clear;
  end
  else if aControl is TcxExtLookupComboBox  then
  begin
    TcxExtLookupComboBox(aControl).Clear;
  end
  else if aControl is TcxImageComboBox  then
  begin
    TcxImageComboBox(aControl).Clear;
  end
  else if aControl is TcxMRUEdit  then
  begin
    TcxMRUEdit(aControl).Clear;
  end
  else if aControl is TcxListBox  then
  begin
    TcxListBox(aControl).Clear;
  end
  else if aControl is TdxLookupTreeView then
  begin
    TdxLookupTreeView(aControl).Text:=EmptyStr;
  end
  else if aControl is TcxHyperLinkEdit  then
  begin
    TcxHyperLinkEdit(aControl).Clear;
  end
  else if aControl is TcxBlobEdit  then
  begin
    TcxBlobEdit(aControl).Clear;
  end
  else if aControl is TcxPopupEdit  then
  begin
    TcxPopupEdit(aControl).Clear;
  end
  else if aControl is TcxButtonEdit  then
  begin
    TcxButtonEdit(aControl).Clear;
  end
  else if aControl is TcxImage  then
  begin
    TcxImage(aControl).Clear;
  end
  {
  else if aControl is TcxComBoBoxMemo  then
  begin
    TcxComBoBoxMemo(aControl).Clear;
  end
  else if aControl is TcxFieldSelects  then
  begin
    TcxFieldSelects(aControl).Clear;
  end
  else if aControl is TcxFieldOrder  then
  begin
    TcxFieldOrder(aControl).Clear;
  end
  else if aControl is TcxSQLFieldSelects  then
  begin
    TcxSQLFieldSelects(aControl).Clear;
  end
  else if aControl is TcxDirSelect  then
  begin
    TcxDirSelect(aControl).Clear;
  end
  else if aControl is TcxFileSelect  then
  begin
    TcxFileSelect(aControl).Clear;
  end
  }
  else if aControl is TcxProgressBar  then
  begin
    TcxProgressBar(aControl).Clear;
  end
  else if aControl is TcxTrackBar  then
  begin
    TcxTrackBar(aControl).Clear;
  end
  else if aControl is TcxLookupComboBox then
  begin
    TcxLookupComboBox(aControl).Clear;
  end
  else if aControl is TcxListView  then
  begin
    TcxListView(aControl).Clear;
  end
  else if aControl is TEdit  then
  begin
    TEdit(aControl).Text;
  end;
end;

procedure ABSetControlValue(aControl:TWinControl;aValue:Variant);
var
  i:LongInt;
  tempValue:string;
begin
  tempValue:=VarToStr(aValue);

  if (aControl is TcxDateEdit) then
  begin
    TcxDateEdit(aControl).EditValue:=aValue;
  end
  else if aControl is TcxTimeEdit then
  begin
    TcxTimeEdit(aControl).EditValue:=aValue;
  end
  else if aControl is TcxCalcEdit  then
  begin
    TcxCalcEdit(aControl).EditValue:=aValue;
  end
  else if aControl is TcxSpinEdit  then
  begin
    TcxSpinEdit(aControl).EditValue:=aValue;
  end
  else if aControl is TcxCurrencyEdit  then
  begin
    TcxCurrencyEdit(aControl).EditValue:=aValue;
  end
  else if aControl is TcxTextEdit  then
  begin
    TcxTextEdit(aControl).EditValue:=aValue;
  end
  else if aControl is TcxMaskEdit  then
  begin
    TcxMaskEdit(aControl).EditValue:=aValue;
  end
  else if aControl is TcxLabel  then
  begin
    TcxLabel(aControl).EditValue:=aValue;
  end
  else if aControl is TcxMemo  then
  begin
    TcxMemo(aControl).Text:=aValue;
  end
  else if aControl is TcxRichEdit  then
  begin
    TcxRichEdit(aControl).EditValue:=aValue;
  end
  else if aControl is TcxCheckBox  then
  begin
    TcxCheckBox(aControl).State:=aValue;
  end
  else if aControl is TcxCheckComboBox  then
  begin
    TcxCheckComboBox(aControl).EditValue:=aValue;
  end
  else if aControl is TcxCheckListBox  then
  begin
    TcxCheckListBox(aControl).EditValue:=aValue;
  end
  else if aControl is TcxCheckGroup  then
  begin
    TcxCheckGroup(aControl).EditValue:=aValue;
  end
  else if aControl is TcxRadioGroup  then
  begin
    TcxRadioGroup(aControl).ItemIndex:=StrToIntDef(VarToStr(aValue),0);
  end
  else if aControl is TcxComboBox  then
  begin
    TcxComboBox(aControl).EditValue:=aValue;
  end
  else if aControl is TcxColorComboBox  then
  begin
    TcxColorComboBox(aControl).EditValue:=aValue;
  end
  else if aControl is TcxFontNameComboBox then
  begin
    TcxFontNameComboBox(aControl).EditValue:=aValue;
  end
  else if aControl is TcxShellComboBox then
  begin
    TcxShellComboBox(aControl).EditValue:=aValue;
  end
  else if aControl is TcxExtLookupComboBox  then
  begin
    TcxExtLookupComboBox(aControl).EditValue:=aValue;
  end
  else if aControl is TcxImageComboBox  then
  begin
    TcxImageComboBox(aControl).EditValue:=aValue;
  end
  else if aControl is TcxMRUEdit  then
  begin
    TcxMRUEdit(aControl).Text:=aValue;
  end
  else if aControl is TcxListBox  then
  begin
    for I := 0 to TcxListView(aControl).Items.Count - 1 do
    begin
      TcxListBox(aControl).Selected[i]:=(ABPos(','+TcxListBox(aControl).Items[i]+',',','+tempValue+',')>0);
    end;
  end
  else if aControl is TdxLookupTreeView then
  begin
    if (Assigned(TdxLookupTreeView(aControl).ListSource)) and
       (Assigned(TdxLookupTreeView(aControl).ListSource.DataSet)) and
       (TdxLookupTreeView(aControl).KeyField<>EmptyStr) then
    begin
      TdxLookupTreeView(aControl).ListSource.DataSet.Locate(TdxLookupTreeView(aControl).ListField,aValue,[]);
    end;
  end
  else if aControl is TcxHyperLinkEdit  then
  begin
    TcxHyperLinkEdit(aControl).EditValue:=aValue;
  end
  else if aControl is TcxBlobEdit  then
  begin
    TcxBlobEdit(aControl).EditValue:=aValue;
  end
  else if aControl is TcxPopupEdit  then
  begin
    TcxPopupEdit(aControl).EditValue:=aValue;
  end
  else if aControl is TcxButtonEdit  then
  begin
    TcxButtonEdit(aControl).EditValue:=aValue;
  end
  else if aControl is TcxImage  then
  begin
    TcxImage(aControl).EditValue:=aValue;
  end
  {
  else if aControl is TcxComBoBoxMemo  then
  begin
    TcxComBoBoxMemo(aControl).EditValue:=aValue;
  end
  else if aControl is TcxFieldSelects  then
  begin
    TcxFieldSelects(aControl).EditValue:=aValue;
  end
  else if aControl is TcxFieldOrder  then
  begin
    TcxFieldOrder(aControl).EditValue:=aValue;
  end
  else if aControl is TcxSQLFieldSelects  then
  begin
    TcxSQLFieldSelects(aControl).EditValue:=aValue;
  end
  else if aControl is TcxDirSelect  then
  begin
    TcxDirSelect(aControl).EditValue:=aValue;
  end
  else if aControl is TcxFileSelect  then
  begin
    TcxFileSelect(aControl).EditValue:=aValue;
  end
  }
  else if aControl is TcxProgressBar  then
  begin
    TcxProgressBar(aControl).EditValue:=aValue;
  end
  else if aControl is TcxTrackBar  then
  begin
    TcxTrackBar(aControl).EditValue:=aValue;
  end
  else if aControl is TcxLookupComboBox then
  begin
    TcxLookupComboBox(aControl).EditValue:=aValue;
  end
  else if aControl is TcxListView  then
  begin
    for I := 0 to TcxListView(aControl).Items.Count - 1 do
    begin
      TcxListView(aControl).Items[i].Selected:=(ABPos(','+TcxListView(aControl).Items[i].Caption+',',','+tempValue+',')>0);
    end;
  end
  else if aControl is TEdit  then
  begin
    TEdit(aControl).Text:=aValue;
  end;
end;

function ABGetDownManagerForm: TABDownManagerForm;
begin
  if not Assigned(FABDownManagerForm) then
    FABDownManagerForm:= TABDownManagerForm.Create(nil);

  result:=FABDownManagerForm;
end;

function ABGetPubDownEdit(aName:string;
                          aDataSource:TDataSource;
                          aControlType,
                          aDownFields,aViewFields,aSaveFields:string;

                          aFilter:string;

                          aCanSelectParent:boolean;
                          aParentField:string): TComponent;
var
  tempTabSheet: TTabSheet;
  tempTableView:TcxGridDBBandedTableView;
  tempTreeList:TABcxDBTreeView;
  tempControlName,
  tempCreateFieldNames,
  tempMainField:string;
  i:LongInt;
  tempSaveColumn:TcxCustomGridTableItem;

  tempPopupMenu:TABcxGridPopupMenu;
begin
  i:=FPubDownEditList.IndexOf(aName);
  if i<0 then
  begin
    tempControlName:= 'PubDownEditName_'+ABStrToName(aName);
    //创建cxGrid或cxDBTreeList
    if (AnsiCompareText(aControlType,'TABcxTreeViewPopupEdit')=0) or
       (AnsiCompareText(aControlType,'TABcxDBTreeViewPopupEdit')=0)  then
    begin
      //创建TabSheet
      tempTabSheet := TTabSheet.Create(ABGetDownManagerForm);
      tempTabSheet.PageControl:= ABGetDownManagerForm.PageControl2;
      tempTabSheet.Caption := aName;

      tempTreeList:=TABcxDBTreeView.Create(ABGetDownManagerForm);
      tempTreeList.CanSelectParent:=aCanSelectParent;
      tempTreeList.Align   :=  alClient;
      tempTreeList.Name:=tempControlName;
      tempTreeList.Bands.Add;
      tempTreeList.OptionsBehavior.ExpandOnIncSearch:=true;
      tempTreeList.OptionsBehavior.IncSearch := True;

      tempTreeList.OptionsData.Inserting := False         ;
      tempTreeList.OptionsData.Editing := False           ;
      tempTreeList.OptionsData.Deleting := False          ;

      tempTreeList.OptionsView.ColumnAutoWidth := True    ;

      tempTreeList.DataController.DataSource := aDataSource;
      tempTreeList.DataController.ParentField := aParentField;
      tempTreeList.DataController.KeyField := aSaveFields;
      tempTreeList.Parent  :=   tempTabSheet ;

      FPubDownEditList.AddObject(aName,tempTreeList);
      Result:=tempTreeList;
    end
    else
    begin
      tempTableView     :=   TcxGridDBBandedTableView(ABGetDownManagerForm.Sheet1_Grid1.CreateView(TcxCustomGridViewClass(GetClass('TABcxGridDBBandedTableView'))));
      ABGetDownManagerForm.Sheet1_Level1.GridView:=tempTableView;
      tempTableView.Name:=tempControlName;
      tempTableView.OptionsData.Editing:=false;
      tempTableView.OptionsView.BandHeaders:=False;
      tempTableView.OptionsView.GroupByBox:=False;

      tempTableView.Bands.Add;
      tempTableView.DataController.DataSource   :=  aDataSource;
      tempTableView.FilterBox.Visible           :=  fvNever;

      tempMainField:= ABGetLeftRightStr(aFilter,axdLeft);
      if (Assigned(aDataSource.DataSet)) and
         (Assigned(aDataSource.DataSet.FindField(tempMainField))) and
         (ABPos(','+tempMainField+',',','+aDownFields+',')<=0) then
        tempCreateFieldNames:=tempMainField+','+aDownFields
      else
        tempCreateFieldNames:=aDownFields;

      ABCreateColumns(tempTableView,tempCreateFieldNames);

      if ABPos(','+tempMainField+',',','+tempCreateFieldNames+',')>0 then
      begin
        tempSaveColumn:=ABGetColumnByFieldName(tempTableView,tempMainField);
        tempSaveColumn.Visible:=False;
        tempSaveColumn.Index:=0;
      end;

      tempPopupMenu:=TABcxGridPopupMenu(ABGetObjectPropValue(tempTableView,'ExtPopupMenu'));
      tempPopupMenu.Load;
      if tempPopupMenu.IsLoadViewProperty then
      begin

      end
      else
      begin
        tempTableView.OptionsView.ColumnAutoWidth:=true;
        tempPopupMenu.OpenRowNum;
        if tempTableView.VisibleColumnCount=1 then
        begin
          if tempTableView.VisibleColumns[0].Width<200 then
            tempTableView.VisibleColumns[0].Width:=200;
        end;
      end;

      FPubDownEditList.AddObject(aName,tempTableView);
      ABGetDownManagerForm.ListBox1.Items.Add(aName);
      ABSetListBoxWidth(ABGetDownManagerForm.ListBox1);
      Result:=tempTableView;
    end;
  end
  else
  begin
    Result:=TComponent(FPubDownEditList.Objects[i]);

    if (AnsiCompareText(aControlType,'TABcxTreeViewPopupEdit')=0) or
       (AnsiCompareText(aControlType,'TABcxDBTreeViewPopupEdit')=0)  then
    begin
      TABcxDBTreeView(Result).CanSelectParent:=aCanSelectParent;
      if TABcxDBTreeView(Result).DataController.DataSource<>aDataSource then
        TABcxDBTreeView(Result).DataController.DataSource := aDataSource;
      if TABcxDBTreeView(Result).DataController.ParentField<>aParentField then
        TABcxDBTreeView(Result).DataController.ParentField := aParentField;
      if TABcxDBTreeView(Result).DataController.KeyField<>aSaveFields then
        TABcxDBTreeView(Result).DataController.KeyField := aSaveFields;
    end
    else
    begin
      TcxGridDBBandedTableView(Result).DataController.DataSource   :=  aDataSource;
    end;
  end;
end;

procedure ABFreePubDownEdit(aIndex:longint);
var
  i,j:LongInt;
begin
  if aIndex>=0 then
  begin
    if FPubDownEditList.Objects[aIndex] is TABcxDBTreeView  then
    begin
      for i := ABGetDownManagerForm.PageControl2.PageCount - 1 downto 0 do
      begin
        if AnsiCompareText(FPubDownEditList[aIndex],ABGetDownManagerForm.PageControl2.Pages[i].Caption)=0 then
        begin
          FPubDownEditList.Objects[aIndex].Free;
          ABGetDownManagerForm.PageControl2.Pages[i].Free;
          Break;
        end;
      end;
    end
    else if FPubDownEditList.Objects[aIndex] is TcxGridDBBandedTableView  then
    begin
      j:=ABGetDownManagerForm.ListBox1.Items.indexof(FPubDownEditList[aIndex]);
      if j>=0 then
      begin
        FPubDownEditList.Objects[aIndex].Free;
        ABGetDownManagerForm.ListBox1.Items.delete(j);
      end;
      ABSetListBoxWidth(ABGetDownManagerForm.ListBox1);
    end;
    FPubDownEditList.Delete(aIndex);
  end;
end;

procedure ABFreePubDownEdit(aName:string);
var
  i:LongInt;
begin
  i:=FPubDownEditList.IndexOf(aName);
  if i>=0 then
    ABFreePubDownEdit(i);
end;

procedure ABFreePubDownEdit;
var
  i:LongInt;
begin
  for I := FPubDownEditList.Count - 1 downto 0 do
  begin
    ABFreePubDownEdit(i);
  end;
  FPubDownEditList.free;
end;

procedure ABDeleteItemONKeyDown(aItems:TStrings;aText:string;aCurKey: Word; aCurShift: TShiftState;aDeleteOfKey: Word; aDeleteOfShift: TShiftState);
var
  i: LongInt;
begin
  if (aCurShift=aDeleteOfShift) and
     (aCurKey = aDeleteOfKey)  then
  begin
    i := aItems.IndexOf(aText);
    if i >= 0 then
    begin
      aItems.Delete(i);
    end;
  end;
end;

function ABGetDisplayLookupText(aDataController:TcxDBDataController;aDisplayFields: string): string;
var
  I: Integer;
  tempFieldName:string;
  tempItem:TcxCustomGridTableItem;
begin
  Result := EmptyStr;
  for I := 1 to ABGetSpaceStrCount(aDisplayFields, ',') do
  begin
    tempFieldName := ABGetSpaceStr(aDisplayFields, i, ',');
    if tempFieldName <> EmptyStr then
    begin
      tempItem:=TcxCustomGridTableItem(aDataController.GetItemByFieldName(tempFieldName));
      if Assigned(tempItem) then
      begin
        ABAddstr(Result,aDataController.DisplayTexts[aDataController.FocusedRecordIndex, tempItem.Index],' ');
      end;
    end;
  end;
end;

procedure ABSetPropertiesPublicValue(aProperties:TcxCustomEditProperties);
begin
  if aProperties is TcxCustomComboBoxProperties then
  begin
    TcxCustomComboBoxProperties(aProperties).DropDownRows := 20;
  end;

  if aProperties is TABcxSQLFieldSelectsProperties then
  begin
  end
  else if aProperties is TABcxCheckComboBoxProperties then
  begin
    TABcxCheckComboBoxProperties(aProperties).EditValueFormat := cvfCaptions;
    TABcxCheckComboBoxProperties(aProperties).ShowEmptyText := false;
    TABcxCheckComboBoxProperties(aProperties).DropDownSizeable := true;
    TABcxCheckComboBoxProperties(aProperties).ClearKey := 46;
  end
  else if aProperties is TABcxFieldOrderProperties then
  begin
  end
  else if aProperties is TABcxTreeViewPopupEditProperties then
  begin
    TABcxTreeViewPopupEditProperties(aProperties).PopupSysPanelStyle:=True;
  end
  else if aProperties is TABcxComboBoxProperties then
  begin
    TABcxComboBoxProperties(aProperties).ClearKey := 46;
  end
  else if aProperties is TABcxDateEditProperties then
  begin
	  TABcxDateEditProperties(aProperties).ShowTime := false;
	  TABcxDateEditProperties(aProperties).SaveTime := false;
  end
  else if aProperties is TABcxDateTimeEditProperties then
  begin
    TABcxDateTimeEditProperties(aProperties).Kind := ckDateTime;
  end
  else if aProperties is TABcxMemoProperties then
  begin
    TABcxMemoProperties(aProperties).ScrollBars := ssVertical;
  end
  else if aProperties is TABcxComBoBoxMemoProperties then
  begin
    TABcxComBoBoxMemoProperties(aProperties).DropDownSizeable := true;
  end
  else if aProperties is TABcxFieldSelectsProperties then
  begin
  end
  else if aProperties is TABcxImageProperties then
  begin
    TABcxImageProperties(aProperties).Stretch := true;
    TABcxImageProperties(aProperties).GraphicClassName:='TJPEGImage';
  end
  else if aProperties is TABcxTimeEditProperties then
  begin
    TABcxTimeEditProperties(aProperties).ShowDate:=false;
  end
  else if aProperties is TABcxCheckBoxProperties then
  begin
    TABcxCheckBoxProperties(aProperties).NullStyle := nssUnchecked;
  end
  else if aProperties is TABcxRadioGroupProperties then
  begin
    TABcxRadioGroupProperties(aProperties).DefaultValue := 0;
  end
  else if aProperties is TcxExtLookupComboBoxProperties then
  begin
    TcxExtLookupComboBoxProperties(aProperties).DropDownAutoWidth:=true;
    TcxExtLookupComboBoxProperties(aProperties).DropDownSizeable:=true;
    TcxExtLookupComboBoxProperties(aProperties).IncrementalFiltering:=false;
    TcxExtLookupComboBoxProperties(aProperties).IncrementalSearch:=false;
    TcxExtLookupComboBoxProperties(aProperties).AutoSearchOnPopup :=false;
    TcxExtLookupComboBoxProperties(aProperties).FocusPopup:=false;
    TcxExtLookupComboBoxProperties(aProperties).Revertable:=True;
  end;
end;

procedure ABInputMultiLevelQueryControlValue(aParent:TWinControl;aDataset:TABDictionaryQuery;aInputMenuItem:TMenuItem);
var
  tempList1: TList;
  i,j:longint;
  tempQueryPanelControl,tempControl:TwinControl;
  tempWhere,
  tempLabel1Caption:string;
  tempIsQuotedStr:boolean;
begin
  tempIsQuotedStr:=true;
  tempControl:=nil;
  for i := 0 to aInputMenuItem.Count-1 do
  begin
    if aInputMenuItem[i].Checked then
    begin
      tempList1 := TList.Create;
      try
        ABGetDetailDatasetList(aDataset,tempList1,true);
        for j := 0 to tempList1.Count - 1 do
        begin
          tempQueryPanelControl:=TwinControl(aParent.FindChildControl(aParent.Name+'_'+TDataset(tempList1[j]).Name+'_'+'InnerQueryPanel'));
          if  (Assigned(tempQueryPanelControl)) then
          begin
            tempControl:=TwinControl(tempQueryPanelControl.FindChildControl(aInputMenuItem[i].Hint));
            if (Assigned(tempControl)) then
            begin
              Break;
            end;
          end;
        end;
      finally
        tempList1.free;
      end;

      tempLabel1Caption:=aInputMenuItem[i].Caption;
      tempIsQuotedStr:=aInputMenuItem[i].Tag<>1;
      break;
    end;
  end;

  tempWhere:=ABGetFieldMultiRowWhere(tempLabel1Caption,',',tempIsQuotedStr);
  if tempWhere<>EmptyStr then
    ABSetControlValue(tempControl,tempWhere);
end;

function ABGetMultiLevelCaption(aDataset: TABDictionaryQuery):String;
begin
  result:=aDataset.MultiLevelCaption;
  if (result=emptystr) and
     (aDataset.LoadTables.Count>0) then
    result:=ABGetTableCaption(ABGetDatabaseByConnName(aDataset.ConnName),aDataset.LoadTables[0]);
  if result=emptystr then
    result:=aDataset.name;
end;

//释放多层查询的控件
procedure ABFreeMultiLevelQueryControls(aParent:TWinControl;aDataset:TABDictionaryQuery);
var
  i,j:longint;
  tempList1: TList;
  tempQueryPanelControl:TwinControl;
begin
  tempList1 := TList.Create;
  try
    ABGetDetailDatasetList(aDataset,tempList1,true);
    for i := 0 to tempList1.Count - 1 do
    begin
      tempQueryPanelControl:=TwinControl(aParent.FindChildControl(aParent.Name +'_'+TDataset(tempList1[i]).Name+'_'+'InnerQueryPanel'));
      if (Assigned(tempQueryPanelControl)) then
      begin
        for j := tempQueryPanelControl.ControlCount-1 downto 0 do
        begin
          tempQueryPanelControl.Controls[j].Free;
        end;
        tempQueryPanelControl.Free;
      end;
    end;
  finally
    tempList1.free;
  end;
end;

procedure ABOrderMultiLevelQuery(aList1: TList);
var
  I,j,tempMaxOrder,tempMaxIndex: Integer;
begin
  j:=0;
  //子数据集排序
  for I := aList1.Count-1 downto 1 do
  begin
    aList1.Move(aList1.Count-1,j);
    j:=j+1;
  end;

  for I := 0 to aList1.Count-1 do
  begin
    tempMaxOrder:=0;
    tempMaxIndex:=i;
    for j := i to aList1.Count-1 do
    begin
      if tempMaxOrder<TABDictionaryQuery(aList1[j]).MultiLevelOrder then
      begin
        tempMaxOrder:=TABDictionaryQuery(aList1[j]).MultiLevelOrder;
        tempMaxIndex:=j;
      end;
    end;
    if (tempMaxOrder<>0) and
       (tempMaxIndex<>0) then
    aList1.Move(tempMaxIndex,0);
  end;
end;

procedure ABCreateMultiLevelQueryFieldNameStrings(
                                          aDataset:TABDictionaryQuery;
                                          aStrings:TStrings
                                          );
  procedure CreateStrings(aDataset:TABDictionaryQuery);
  var
    i,tempBeginStringsCount,tempAddStringsCount:LongInt;
    tempList:TList;
    tempFieldDef:PABFieldDef;
    tempMultiLevelCaption:string;
  begin
    if (Assigned(aDataset)) and
       (aDataset.Active) then
    begin
      tempBeginStringsCount:=aStrings.Count;
      tempAddStringsCount:=0;

      for i := 0 to aDataset.FieldCount-1  do
      begin
        tempFieldDef:=ABFieldDefByFieldName(aDataSet,aDataSet.Fields[i].FieldName);
        if (Assigned(tempFieldDef)) and
           (tempFieldDef.Fi_IsQueryView) then
        begin
          tempAddStringsCount:=tempAddStringsCount+1;
          tempMultiLevelCaption:=ABGetMultiLevelCaption(aDataset);

          aStrings.AddObject(tempMultiLevelCaption+':'+tempFieldDef.Fi_Caption,aDataset);
        end;
      end;

      if (tempBeginStringsCount>0) and
         (tempAddStringsCount>0) then
      begin
        aStrings.Insert(tempBeginStringsCount,'---------------');
      end;
      if aDataset.MultiLevelQuery then
      begin
        tempList := TList.Create;
        try
          aDataset.GetDetailDataSets(tempList);
          ABOrderMultiLevelQuery(tempList);

          for I := 0 to tempList.Count-1 do
          begin
            CreateStrings(TABDictionaryQuery(tempList[i]));
          end;
        finally
          tempList.Free;
        end;
      end;
    end;
  end;
begin
  aStrings.Clear;
  CreateStrings(aDataset);
end;

procedure ABCreateMultiLevelQueryInputMenuItem(aParent:TWinControl;
                                          aDataset:TABDictionaryQuery;
                                          aInputMenuItem:TMenuItem;
                                          aMenuItemOnClick:TNotifyEvent
                                          );
  procedure CreatePopupMenus(aDataset:TABDictionaryQuery);
  var
    i,tempBeginMenuItemCount,tempAddMenuItemCount:LongInt;
    tempControl_Lable,
    tempControl_Control:TWinControl;
    tempMenuItem: TMenuItem;
    tempList:TList;
    tempGroupBox:TABcxGroupBox;
    tempMultiLevelCaption:string;
  begin
    tempGroupBox:=TABcxGroupBox(aParent.FindChildControl(aParent.Name +'_'+aDataset.Name+'_'+'InnerQueryPanel'));
    if Assigned(tempGroupBox) then
    begin
      tempBeginMenuItemCount:=aInputMenuItem.Count;
      tempAddMenuItemCount:=0;

      tempList := TList.Create;
      try
        if aDataset.MultiLevelQuery then
        begin
          aDataset.GetDetailDataSets(tempList);
          ABOrderMultiLevelQuery(tempList);
        end;

        //创建导入查询按钮的菜单
        for i := 0 to tempGroupBox.ControlCount-1  do
        begin
          tempControl_Lable:=TwinControl(tempGroupBox.Controls[i]);
          if (tempControl_Lable is TcxLabel)  then
          begin
            //根据Label得到控件
            tempControl_Control:=TwinControl(tempGroupBox.FindChildControl(ABStringReplace(tempControl_Lable.Name,'_Label','_Control')));
            if (Assigned(tempControl_Control)) and
               ((tempControl_Control is TcxTextEdit) or
                (tempControl_Control is TcxMemo)
                ) then
            begin
              tempMultiLevelCaption:=ABGetMultiLevelCaption(aDataset);

              tempAddMenuItemCount:=tempAddMenuItemCount+1;
              tempMenuItem := TMenuItem.Create(nil);

              if (not Assigned(aDataset.DataSource)) and
                 (tempList.Count<=0) then
                tempMenuItem.Caption :=TcxLabel(tempControl_Lable).Caption
              else
                tempMenuItem.Caption :=tempMultiLevelCaption+':'+ TcxLabel(tempControl_Lable).Caption;

              tempMenuItem.Hint :=ABStringReplace(tempControl_Lable.Name,'_Label','_Control');
              tempMenuItem.GroupIndex := 1;
              tempMenuItem.RadioItem := true;
              tempMenuItem.AutoCheck := true;
              tempMenuItem.OnClick := aMenuItemOnClick;

              if aInputMenuItem.Count=0 then
                tempMenuItem.Checked := true;
              aInputMenuItem.Add(tempMenuItem);
            end;
          end;
        end;

        if (tempBeginMenuItemCount>0) and
           (tempAddMenuItemCount>0) then
        begin
          tempMenuItem := TMenuItem.Create(nil);
          tempMenuItem.Caption := '-';
          tempMenuItem.Hint := EmptyStr;
          aInputMenuItem.Insert(tempBeginMenuItemCount,tempMenuItem);
        end;

        if aDataset.MultiLevelQuery then
        begin
          for I := 0 to tempList.Count-1 do
          begin
            CreatePopupMenus(TABDictionaryQuery(tempList[i]));
          end;
        end;
      finally
        tempList.Free;
      end;
    end;
  end;
begin
  aInputMenuItem.Clear;
  CreatePopupMenus(aDataset);
end;

function ABCreateMultiLevelQueryControls( aParent:TWinControl;
                                          aOwner:TComponent;
                                          aDataset:TABDictionaryQuery;
                                          aRowOnlyOneControl:Boolean;

                                          aMinParentWidth:longint;
                                          aAutoParentWidth:Boolean;

                                          aMinParentHeight:longint;
                                          aAutoParentHeight:Boolean;
                                          aAutoSuit:Boolean;
                                          aSetakRight:Boolean;
                                          aSetakBottom:Boolean
                                          ):LongInt;
var
  tempkAllPanelHeight:longint;
  tempCount:LongInt;
  tempGroupBox:TABcxGroupBox;
  procedure CreatePanels(aDataset:TABDictionaryQuery);
  var
    tempList:TList;
    I: Integer;
    tempQueryConst:string;
    tempMultiLevelCaption:string;
  begin
    tempQueryConst:=ABGetDatasetQueryConst(aDataset,'','');
    if tempQueryConst<>EmptyStr then
    begin
      tempMultiLevelCaption:=ABGetMultiLevelCaption(aDataset);

      tempCount:=tempCount+1;
      tempGroupBox:=TABcxGroupBox.Create(aOwner);
      tempGroupBox.Style.BorderStyle:=ebsFlat;
      tempGroupBox.Parent:=aParent;
      tempGroupBox.Name:=aParent.Name +'_'+aDataset.Name+'_'+'InnerQueryPanel';
      tempGroupBox.Align:=aDataset.MultiLevelAlign;
      tempGroupBox.Width:=TControl(aOwner).Width;
      tempGroupBox.Caption:=tempMultiLevelCaption;
      tempGroupBox.SetSubComponent(true);

      ABCreateInputParams(tempQueryConst,
                          tempGroupBox,
                          aOwner,

                          10,
                          aRowOnlyOneControl,

                          aMinParentWidth,aAutoParentWidth,
                          aMinParentHeight,aAutoParentHeight,

                          ABDBPanelParams_Leftspacing    ,
                          15+ABDBPanelParams_Topspacing  ,
                          ABDBPanelParams_Righttspacing  ,
                          ABDBPanelParams_Bottomspacing  ,
                          ABDBPanelParams_Xspacing       ,
                          ABDBPanelParams_Yspacing,

                          aAutoSuit,
                          aSetakRight,
                          aSetakBottom
                          );
      tempGroupBox.Top:=tempkAllPanelHeight+1;
      tempkAllPanelHeight:=max(tempkAllPanelHeight,tempGroupBox.Top+tempGroupBox.Height);
      tempGroupBox.Left:=1;
    end;

    if aDataset.MultiLevelQuery then
    begin
      tempList := TList.Create;
      try
        aDataset.GetDetailDataSets(tempList);
        ABOrderMultiLevelQuery(tempList);
        for I := 0 to tempList.Count-1 do
        begin
          CreatePanels(TABDictionaryQuery(tempList[i]));
        end;
      finally
        tempList.Free;
      end;
    end;
  end;
begin
  tempCount:=0;
  ABFreeMultiLevelQueryControls(aParent,aDataset);
  tempkAllPanelHeight:=0;
  CreatePanels(aDataset);
  Result:= tempkAllPanelHeight+75;
  if tempCount=1 then
    tempGroupBox.Caption:=EmptyStr;
end;

function ABGetMultiLevelQueryWhere(aParent:TWinControl;aDataset:TABDictionaryQuery;var aWhereRemark:string): string;
  function DoInptuWhere(aInputDataset:TABDictionaryQuery):string;
  var
    tempWhereStr,
    tempMasterFields,
    tempDetailFields,
    tempFromTables,
    tempInitWhere:string;

    tempList:TList;
    I: Integer;
    tempQueryConst:string;
    tempQueryPanelControl:TwinControl;
    tempWhere,tempWhereRemark:string;

  begin
    Result:=EmptyStr;
    tempQueryConst:=ABGetDatasetQueryConst(aInputDataset);
    tempQueryPanelControl:=TwinControl(aParent.FindChildControl(aParent.Name +'_'+aInputDataset.Name+'_'+'InnerQueryPanel'));
    if aInputDataset.MultiLevelQuery then
    begin
      tempList := TList.Create;
      try
        aInputDataset.GetDetailDataSets(tempList);
        for I := 0 to tempList.Count-1 do
        begin
          tempWhereStr:=DoInptuWhere(TABDictionaryQuery(tempList[i]));
          if tempWhereStr<>EmptyStr then
          begin
            tempMasterFields:=TABDictionaryQuery(tempList[i]).MasterFields;
            tempDetailFields:=TABDictionaryQuery(tempList[i]).DetailFields;
            tempFromTables:=TABDictionaryQuery(tempList[i]).UpdateTables.Text;
            if Assigned(TABDictionaryQuery(tempList[i]).OnMultiLevelDetailWhereEvent) then
               TABDictionaryQuery(tempList[i]).OnMultiLevelDetailWhereEvent(tempMasterFields,tempDetailFields,tempFromTables,tempInitWhere);

            tempWhereStr:=Trim(tempWhereStr);
            if AnsiCompareText(copy(tempWhereStr,1,3),'and')=0 then
              tempWhereStr:=Trim(copy(tempWhereStr,4,Length(tempWhereStr)));

            tempWhereStr:=ABIIF(tempInitWhere=EmptyStr,tempWhereStr,tempInitWhere+' and '+tempWhereStr);

            ABAddstr(Result,
              ' and '+tempMasterFields+
                         ' in ( '+ ABEnterWrapStr+
                         '     select '+tempDetailFields+ ABEnterWrapStr+
                         '     from '+ABGetDetailTableDatabaseSQLFlag(aDataset,TABDictionaryQuery(tempList[i]))+tempFromTables+ABEnterWrapStr+
                         '     where '+tempWhereStr+ABEnterWrapStr+
                         '   )',' ');
          end;
        end;

        tempWhere:=ABGetInputParamsWhere(
                         aInputDataset,
                         tempQueryConst,
                         tempQueryPanelControl,
                         tempWhereRemark);

        ABAddstr(Result,tempWhere,' ');
        ABAddstr(aWhereRemark,tempWhereRemark,' ');
      finally
        tempList.Free;
      end;
    end
    else
    begin
      Result:=ABGetInputParamsWhere(
                         aInputDataset,
                         tempQueryConst,
                         tempQueryPanelControl,
                         tempWhereRemark
                                   );
      aWhereRemark:=tempWhereRemark;
    end;
  end;
begin
  aWhereRemark:=EmptyStr;
  Result:=DoInptuWhere(aDataset);
end;

function ABReplaceInputParamsValue(var aIsFind:boolean;var aFindControl:TControl;
                                   aText:string;aParent,aQuoteParent:Twincontrol;aQuoted:boolean):string;
var
  i,j,
  tempPosBeginIndex,
  tempSubStrEndIndex,tempSubStrBegIndex: Integer;
  tempValueReplaceNameIndex,
  tempSubItemStr,tempSubStr:string;

  tempConnName:string;
  tempCaption,
  tempName:string;
  tempValueReplaceName,
  tempValueReplaceValue,
  tempQuoteName,
  tempQuoteCaption:string;  //引用PANEL中控件名称
  tempValue:Variant;

  tempControl:TWinControl;
  tempControl_1:TControl;
  tempParamList:TStrings;
begin
  Result:=EmptyStr;
  if aText=EmptyStr then
    exit;

  aIsFind:=false;
  aFindControl:=nil;
  tempParamList:=TStringlist.Create;
  try
    Result:=trim(ABReplaceSqlExtParams((aText)));
    tempPosBeginIndex:=1;
    tempSubStrBegIndex:=abpos(ABSubStrBegin,Result);
    while tempSubStrBegIndex>0 do
    begin
      tempSubStrEndIndex:=ABPos(ABSubStrEnd,Result,tempPosBeginIndex);
      if tempSubStrEndIndex<=0 then
        break;

      tempSubItemStr:=Copy(Result,tempSubStrBegIndex,tempSubStrEndIndex-tempSubStrBegIndex+length(ABSubStrEnd));
      tempSubStr:=tempSubItemStr;
      tempSubStr:=     ABStringReplace(tempSubStr,ABSubStrBegin,EmptyStr);
      tempSubStr:=Trim(ABStringReplace(tempSubStr,ABSubStrEnd  ,EmptyStr));

      tempConnName    :=ABGetItemValue(tempSubStr,'[ConnName]','','[');
      tempName        :=ABGetItemValue(tempSubStr,'[Name]','','[');
      tempCaption     :=ABGetItemValue(tempSubStr,'[Caption]','','[');


      if ABIsOkSQL(tempCaption) then
        tempCaption:=ABGetSQLValue(tempConnName,tempCaption,[],'');
      if (tempName=EmptyStr) and (tempCaption<>EmptyStr) then
        tempName        := 'Name_'+ABStrToName(tempCaption);
      if (tempName<>EmptyStr) then
        tempName:=aParent.Name+'_'+tempName+'_Control';

      tempQuoteName   :=ABGetItemValue(tempSubStr,'[QuoteName]','','[');
      tempQuoteCaption:=ABGetItemValue(tempSubStr,'[QuoteCaption]','','[');
      if (tempQuoteName=EmptyStr) and (tempQuoteCaption<>EmptyStr) then
        tempQuoteName        := 'Name_'+ABStrToName(tempQuoteCaption);
      if (tempQuoteName<>EmptyStr) and (Assigned(aQuoteParent)) then
        tempQuoteName:=aQuoteParent.Name+'_'+tempQuoteName+'_Control';

      tempValue:=EmptyStr;
      if (Assigned(aQuoteParent)) and
         (tempQuoteName<>EmptyStr) then
      begin
        tempControl_1:=aQuoteParent.FindChildControl(tempQuoteName);
        if (Assigned(tempControl_1)) and
           (tempControl_1 is TWinControl) then
        begin
          tempControl:=TWinControl(tempControl_1);
          tempValue:=ABGetControlValue(tempControl);
          if ABVarIsNull(tempValue) then
            tempValue:='';

          aIsFind:=true;
          aFindControl:=tempControl;
        end;
      end
      else if (tempName<>EmptyStr)  then
      begin
        tempControl_1:=aParent.FindChildControl(tempName);
        if (Assigned(tempControl_1)) and
           (tempControl_1 is TWinControl) then
        begin
          tempControl:=TWinControl(tempControl_1);
          tempValue:=ABGetControlValue(tempControl);
          if ABVarIsNull(tempValue) then
            tempValue:='';

          j:=0;
          tempValueReplaceNameIndex:=emptystr;
          while abpos('[ValueReplaceName'+tempValueReplaceNameIndex+']',tempSubStr)>0 do
          begin
            tempValueReplaceName        :=trim(ABGetItemValue(tempSubStr,'[ValueReplaceName'+tempValueReplaceNameIndex+']','','['));
            tempValueReplaceValue        :=trim(ABGetItemValue(tempSubStr,'[ValueReplaceValue'+tempValueReplaceNameIndex+']','','['));
            if (tempValueReplaceName<>emptystr) and
               (tempValueReplaceValue<>emptystr) then
            begin
              tempValueReplaceValue:=ABStringReplace(tempValueReplaceValue,'CurValue',tempValue);
              for I := 0 to tempParamList.Count - 1 do
              begin
                tempValueReplaceValue:=ABStringReplace(tempValueReplaceValue,tempParamList.Names[i],tempParamList.ValueFromIndex[i]);
              end;

              tempValueReplaceValue:=ABGetSQLValue(tempConnName,tempValueReplaceValue,[],'');
              Result:=ABStringReplace(Result,tempValueReplaceName,tempValueReplaceValue,tempSubStrEndIndex);
            end;

            j:=j+1;
            tempValueReplaceNameIndex:=inttostr(j);
          end;

          aIsFind:=true;
          aFindControl:=tempControl;
        end;
      end;

      if aQuoted then
        tempValue:=QuotedStr(tempValue);

      tempParamList.Add(tempCaption+'='+tempValue);
      Result:=ABStringReplace(Result,tempSubItemStr,tempValue);
      tempPosBeginIndex:=tempSubStrEndIndex+length(ABSubStrEnd)+length(tempValue)-length(tempSubItemStr);
      tempSubStrBegIndex:=abpos(ABSubStrBegin,Result,tempPosBeginIndex);
    end;
    if (aQuoted) then
      Result:=QuotedStr(Result);

  finally
    tempParamList.Free;
  end;
end;

function ABSaveControlPositionToSQL(aSQL: string;aParentPanel:TWinControl;aOnlyControlLeft:boolean): string;
var
  tempControl:Tcontrol;
  tempPosBeginIndex,
  tempSubStrEndIndex,tempSubStrBegIndex: Integer;
  tempSubItemStr:string;
  tempClassName,
  tempCaption,
  tempName:string;
  tempConnName:string;

  tempInt1:longint;
begin
  tempSubStrBegIndex:=abpos(ABSubStrBegin,aSQL);
  while tempSubStrBegIndex>0 do
  begin
    tempSubStrEndIndex:=ABPos(ABSubStrEnd,aSQL,tempSubStrBegIndex);
    if tempSubStrEndIndex<=0 then
      break;

    tempSubItemStr:=Copy(aSQL,tempSubStrBegIndex,tempSubStrEndIndex-tempSubStrBegIndex+length(ABSubStrEnd));
    tempClassName   :=trim(ABGetItemValue(tempSubItemStr,'[ClassName]','','['));
    tempCaption     :=     ABGetItemValue(tempSubItemStr,'[Caption]','','[');
    tempConnName   :=      ABGetItemValue(tempSubItemStr,'[ConnName]','','[');
    if ABIsOkSQL(tempCaption) then
      tempCaption:=ABGetSQLValue(tempConnName,tempCaption,[],'');
    tempName        :=     ABGetItemValue(tempSubItemStr,'[Name]','','[');
    if tempName=EmptyStr then
      tempName        := 'Name_'+ABStrToName(tempCaption);

    if (tempName<>EmptyStr) and (tempClassName<>EmptyStr) and (tempCaption<>EmptyStr) then
    begin
      if aOnlyControlLeft then
      begin
        tempInt1:=0;
        tempControl:=TWinControl(aParentPanel.FindChildControl(aParentPanel.Name+'_'+tempName+'_Label'));
        if Assigned(tempControl) then
        begin
          ABDelItemValue(tempSubItemStr,'[LabelTop]','[LabelTop]','[');
          ABDelItemValue(tempSubItemStr,'[Labelleft]','[Labelleft]','[');
          ABDelItemValue(tempSubItemStr,'[LabelWidth]','[LabelWidth]','[');
          ABDelItemValue(tempSubItemStr,'[LabelHeight]','[LabelHeight]','[');

          tempInt1:=tempControl.Left+tempControl.Width+1
        end;

        tempControl:=TWinControl(aParentPanel.FindChildControl(aParentPanel.Name+'_'+tempName+'_Control'));
        if Assigned(tempControl) then
        begin
          ABDelItemValue(tempSubItemStr,'[ControlTop]','[ControlTop]','[');
          ABDelItemValue(tempSubItemStr,'[Controlleft]','[Controlleft]','[');
          ABDelItemValue(tempSubItemStr,'[ControlWidth]','[ControlWidth]','[');
          ABDelItemValue(tempSubItemStr,'[ControlHeight]','[ControlHeight]','[');

          if tempInt1<>tempControl.Left then
            tempSubItemStr:= tempSubItemStr+'[Controlleft]'+inttostr(tempControl.Left);
        end;
      end
      else
      begin
        tempControl:=TWinControl(aParentPanel.FindChildControl(aParentPanel.Name+'_'+tempName+'_Label'));
        if Assigned(tempControl) then
        begin
          ABDelItemValue(tempSubItemStr,'[LabelTop]','[LabelTop]','[');
          ABDelItemValue(tempSubItemStr,'[Labelleft]','[Labelleft]','[');
          ABDelItemValue(tempSubItemStr,'[LabelWidth]','[LabelWidth]','[');
          ABDelItemValue(tempSubItemStr,'[LabelHeight]','[LabelHeight]','[');

          tempSubItemStr:= tempSubItemStr+'[LabelTop]'+inttostr(tempControl.Top);
          tempSubItemStr:= tempSubItemStr+'[Labelleft]'+inttostr(tempControl.Left);
          tempSubItemStr:= tempSubItemStr+'[LabelWidth]'+inttostr(tempControl.Width);
          tempSubItemStr:= tempSubItemStr+'[LabelHeight]'+inttostr(tempControl.Height);
        end;

        tempControl:=TWinControl(aParentPanel.FindChildControl(aParentPanel.Name+'_'+tempName+'_Control'));
        if Assigned(tempControl) then
        begin
          ABDelItemValue(tempSubItemStr,'[ControlTop]','[ControlTop]','[');
          ABDelItemValue(tempSubItemStr,'[Controlleft]','[Controlleft]','[');
          ABDelItemValue(tempSubItemStr,'[ControlWidth]','[ControlWidth]','[');
          ABDelItemValue(tempSubItemStr,'[ControlHeight]','[ControlHeight]','[');

          tempSubItemStr:= tempSubItemStr+'[ControlTop]'+inttostr(tempControl.Top);
          tempSubItemStr:= tempSubItemStr+'[Controlleft]'+inttostr(tempControl.Left);
          tempSubItemStr:= tempSubItemStr+'[ControlWidth]'+inttostr(tempControl.Width);
          tempSubItemStr:= tempSubItemStr+'[ControlHeight]'+inttostr(tempControl.Height);
        end;
      end;

      tempSubItemStr:=ABStringReplace(tempSubItemStr,ABSubStrEnd,'');
      tempSubItemStr:=tempSubItemStr+ABSubStrEnd;
      aSQL:=copy(aSQL,1,tempSubStrBegIndex-1)+tempSubItemStr+copy(aSQL,tempSubStrEndIndex+length(ABSubStrEnd),length(aSQL));
    end;
    tempPosBeginIndex:=tempSubStrBegIndex+length(ABSubStrBegin);
    tempSubStrBegIndex:=abpos(ABSubStrBegin,aSQL,tempPosBeginIndex);
  end;

  result:=aSQL;
end;

procedure ABSetControlSize(var aLableWidth,aLableHeight,aControlWidth,aControlHeight:LongInt;
                           aLabel,aControl:TWinControl;
                           aMinLabelCharNum:longint;
                           aInitZero:Boolean;
                           aSetWidthHeight:Boolean;
                           aUseLabelHightSameControlHeight:boolean
                           );
var
  tempCaption:string;
  tempCaptionWidth:LongInt;
begin
  if aInitZero then
  begin
    aLableWidth:=0;
    //aLableHeight:=0;
    aLableHeight:=21;
    aControlWidth:=0;
    aControlHeight:=0;
  end;

  if (aLableWidth=0) then
  begin
    aLableWidth:=7+aMinLabelCharNum*7;

    tempCaption:=GetPropValue(aLabel,'Caption');
    tempCaptionWidth:=Length(AnsiString(tempCaption))*7+7;
    if tempCaptionWidth>aLableWidth then
    begin
      aLableWidth:=tempCaptionWidth;
    end;
  end
  else
  begin
    if aLableWidth<=3 then
    begin
      aLableWidth:=0;
    end;
  end;

  if (aControlWidth=0)    then
  begin
    if (aControl is TcxCustomCheckBox) then
      aControlWidth:=22
//        else if (aControl is TcxCustomDateEdit) then
//          aControlWidth:=125
//        else if (aControl is TcxCustomTimeEdit) then
//          aControlWidth:=70
//        else if (aControl is TcxCustomCalcEdit) then
//          aControlWidth:=100
//        else if (aControl is TcxCustomCurrencyEdit) then
//          aControlWidth:=100
//        else if (aControl is TcxCustomSpinEdit) then
//          aControlWidth:=70
    else
      aControlWidth:=135;
  end
  else
  begin
    if aControlWidth<=3 then
    begin
      aControlWidth:=0;
    end;
  end;
  if (aControlHeight=0)   then
  begin
    if (aControl is TcxListBox) then
      aControlHeight:=21*4
    else if (aControl is TcxCustomMemo) then
      aControlHeight:=21*2
    else if (aControl is TcxCustomImage) then
      aControlHeight:=21*5
    else if (aControl is TcxCustomCheckGroup) then
      aControlHeight:=18+TcxCustomCheckGroup(aControl).Properties.Items.Count*17
    else if (aControl is TcxCustomRadioGroup) then
      aControlHeight:=18+TcxCustomRadioGroup(aControl).Properties.Items.Count*17
    else
      aControlHeight:=21;
  end;


  if aUseLabelHightSameControlHeight then
  begin
    aLableHeight:=aControlHeight ;
  end;

  if aSetWidthHeight then
  begin
    if Assigned(aLabel) then
    begin
      aLabel.Width:= aLableWidth;
      aLabel.Height:=aLableHeight  ;
    end;

    if Assigned(aControl) then
    begin
      aControl.Width:=aControlWidth ;
      aControl.Height:= aControlHeight;
    end;
  end;
end;

procedure ABSetParentControlSize( aPanel:Twincontrol;
                                  aMaxWidth:longint;
                                  aMaxHeight:longint;

                                  aMinParentWidth:longint;
                                  aAutoParentWidth:Boolean;
                                  aMinParentHeight:longint;
                                  aAutoParentHeight:Boolean
                                    );
  procedure ABSetHeight(aHeight:longint);
  var
    tempSetParentPageControlHeight:LongInt;
  begin
    if aHeight<aMinParentHeight  then
      aHeight:=aMinParentHeight;
    aPanel.Height:=aHeight;

    if (Assigned(aPanel.Parent)) and
       (aPanel.Parent.ControlCount=1)  then
    begin
      if aPanel.Parent is TcxCustomGroupBox then
        aPanel.Parent.Height:=aPanel.Height+20
      else  if (aPanel.Parent is TcxTabSheet) and
               (Assigned(aPanel.Parent.Parent)) and
               (aPanel.Parent.Parent is TcxPageControl) then
      begin
        tempSetParentPageControlHeight:=aHeight;
        if (not TcxPageControl(aPanel.Parent.Parent).Properties.HideTabs) then
          tempSetParentPageControlHeight:=tempSetParentPageControlHeight+22;

        if TcxPageControl(aPanel.Parent.Parent).Height<tempSetParentPageControlHeight then
          TcxPageControl(aPanel.Parent.Parent).Height:=tempSetParentPageControlHeight;
      end;
    end;
  end;
  procedure ABSetWidth(aWidth:longint);
  begin
    if aWidth<aMinParentWidth  then
      aWidth:=aMinParentWidth;
    aPanel.ClientWidth:=aWidth;

    if (Assigned(aPanel.Parent)) and
       (aPanel.Parent.ControlCount=1)  then
    begin
      aPanel.Parent.ClientWidth:=aWidth;
      if (Assigned(aPanel.Parent.Parent)) and
         (aPanel.Parent.Parent.ControlCount=1) then
      begin
        aPanel.Parent.Parent.ClientWidth:=aWidth;
      end;
    end;
  end;
begin
  if (aAutoParentHeight) then
  begin
    ABSetHeight(aMaxHeight);
  end;
  if (aAutoParentWidth) then
  begin
    ABSetWidth(aMaxWidth);
  end;
end;

procedure ABGetParentControlWidthAndHeight(
                                  aPanel:Twincontrol;
                                  var aMaxWidth:longint;
                                  var aMaxHeight:longint
                                    );
var
  i:longint;
begin
  aMaxHeight:=0;
  aMaxWidth:=0;
  for i := 0 to aPanel.ControlCount - 1 do
  begin
    if not (aPanel.Controls[i] is TBevel) then
    begin
      aMaxHeight:=Max(aMaxHeight,aPanel.Controls[i].Top+aPanel.Controls[i].Height);
      aMaxWidth:=Max(aMaxWidth,aPanel.Controls[i].Left+aPanel.Controls[i].Width);
    end;
  end;
end;

procedure ABSetRightControlAndLastControl(  aPanel:Twincontrol;
                                            aMaxWidth:longint;
                                            aMaxHeight:longint;

                                            aRighttspacing:longint;
                                            aBottomspacing:longint;

                                            aAutoSuit:Boolean;
                                            aSetakRight:Boolean;
                                            aSetakBottom:Boolean
                                          );
var
  i,j,k,tempTabOrder:longint;
  tempMaxCurRowLeftPosition:LongInt;
  tempControl,
  tempRightWinControl:TControl;
  tempCurRowComtrols:TList;
  tempMaxHeight_PriRow:longint;
  tempLastRowISRemark:boolean;
begin
  i:=0;
  tempTabOrder:=0;
  tempCurRowComtrols:=TList.Create;
  try
    tempMaxHeight_PriRow:=0 ;
    While i<=aMaxHeight+10 do
    Begin
      tempRightWinControl:=nil;
      tempMaxCurRowLeftPosition:=0;
      k:=0;
      While k<=aMaxWidth+10 do
      begin

        for j := 0 to aPanel.ControlCount - 1 do
        begin
          if ((Assigned(aPanel.Controls[j]))) and
             (aPanel.Controls[j].Top>=i) and (aPanel.Controls[j].Top<i+10) and
             (aPanel.Controls[j].Left>=k) and (aPanel.Controls[j].Left<k+10) then
          begin
            if tempMaxCurRowLeftPosition=0 then
              tempCurRowComtrols.Clear;

            tempCurRowComtrols.Add(aPanel.Controls[j]);
            tempMaxCurRowLeftPosition:=Max(tempMaxCurRowLeftPosition,aPanel.Controls[j].Left+aPanel.Controls[j].Width);
            if tempMaxCurRowLeftPosition=aPanel.Controls[j].Left+aPanel.Controls[j].Width then
              tempRightWinControl:= aPanel.Controls[j];

            //按从上到下,从左到右的位置,设置控件的 TabOrder
            if (aPanel.Controls[j] is TWinControl) then
            begin
              TWinControl(aPanel.Controls[j]).TabOrder:=tempTabOrder;
              tempTabOrder:=tempTabOrder+1;
            end;
          end;
        end;
        //循环读本行下一个的控件
        inc(k,10);
      end;

      //设置每行最后一个控件的宽度与Anchors中加入akRight
      if  (Assigned(tempRightWinControl)) then
      begin
        if (tempMaxCurRowLeftPosition<>0) and
           (not (tempRightWinControl is TcxCustomLabel)) and
           (not (tempRightWinControl is TcxCustomCheckBox))  then
        begin
          if (tempRightWinControl.Top>=tempMaxHeight_PriRow-2) then
          begin
            if (aAutoSuit) or
               (tempRightWinControl.Width>aPanel.Width-aRighttspacing -tempRightWinControl.Left) then
            begin
              tempRightWinControl.Width:=aPanel.Width-aRighttspacing -tempRightWinControl.Left;
            end;
            if aSetakRight  then
            begin
              if not (tempRightWinControl is TcxCustomImage) then
                tempRightWinControl.Anchors:=tempRightWinControl.Anchors+[akRight];
            end;
          end;
          tempMaxHeight_PriRow:=Max(tempMaxHeight_PriRow,tempRightWinControl.Top+tempRightWinControl.Height);
        end;
      end;

      //循环读下一行的控件
      inc(i,10);
    End;

    //设置最后一行全备注的Anchors
    tempLastRowISRemark:=true;
    for I := 0 to tempCurRowComtrols.Count - 1 do
    begin
      tempControl:=TControl(tempCurRowComtrols.Items[i]);
      if (tempControl is TcxCustomMemo) or
         (tempControl is TcxCustomLabel) then
      begin

      end
      else
      begin
        tempLastRowISRemark:=False;
        break;
      end;
    end;
    if tempLastRowISRemark then
    begin
      for I := 0 to tempCurRowComtrols.Count - 1 do
      begin
        tempControl:=TControl(tempCurRowComtrols.Items[i]);
        if (tempControl is TcxCustomMemo) or
           (tempControl is TcxCustomLabel) then
        begin
          tempControl:=TControl(tempCurRowComtrols.Items[i]);
          if aAutoSuit then
          begin
            tempControl.Height:=aPanel.Height-tempControl.Top-aBottomspacing ;
          end;

          if aSetakBottom  then
          begin
            if not (tempControl is TcxCustomImage) then
              tempControl.Anchors:=tempControl.Anchors+[akBottom];
          end;
        end;
      end;
    end;
  finally
    tempCurRowComtrols.Free;
  end;
end;

procedure ABCreateInputParams(
                              aText:string;
                              aParent:Twincontrol;
                              aOwner:TComponent;

                              aMinLabelCharNum:longint;
                              aRowOnlyOneControl:Boolean;

                              aMinParentWidth:longint;
                              aAutoParentWidth:Boolean;
                              aMinParentHeight:longint;
                              aAutoParentHeight:Boolean;

                              aleftspacing:LongInt;
                              aTopspacing:LongInt;
                              aRighttspacing:longint;
                              aBottomspacing:longint;
                              aXspacing:longint;
                              aYspacing:longint;
                              aAutoSuit:Boolean;
                              aSetakRight:Boolean;
                              aSetakBottom:Boolean
                              );
var
  tempCanSelectParent:boolean;

  I,tempCloumnCount,
  tempPosBeginIndex,
  tempSubStrEndIndex,tempSubStrBegIndex,tempMinCharNum: Integer;
  tempSubItemStr,tempSubStr,

  tempStr1,
  tempClassName,
  tempCaption,
  tempName,
  tempOperateFlags,
  tempDefaultValue,

  tempFix,

  tempConnName,
  tempCacleDataset,
  tempDownType,
  tempFilter,
  tempFillSql,
  tempParentField,
  tempKeyfield,
  tempListField,
  tempDownCaption,
  tempPassChar,tempMask,
  tempDownField:string;

  tempMaxLength:longint;

  tempSetCurDateTime,
  tempSetLabelTop,tempSetLabelleft,tempSetLabelWidth,tempSetLabelHeight:string;
  tempSetControlTop,tempSetControlleft,tempSetControlWidth,tempSetControlHeight:string;
  tempSetMinCharNum:string;

  tempLabel:TcxLabel;
  tempControl:TWinControl;
  tempOperateFlagControl:TcxComboBox;
  tempPriWinControl:TWinControl;

  tempDataset:TDataSet;
  tempDataSource:TDataSource;
  tempObject: TObject;

  tempLableWidth,tempLableHeight:longint;
  tempControlWidth,tempControlHeight:longint;
  tempTop,templeft:Integer;

  tempRowMaxHeight:Integer;
  tempMaxWidth,tempMaxHeight:Integer;

  tempHeight:longint;
  tempWidth:longint;
  tempDefaultValueSQLVariant:Variant;
  function GetLabelWidth :LongInt;
  begin
    Result:=tempLabel.Width;
    if Assigned(tempOperateFlagControl)  then
      Result:=tempLabel.Width+tempOperateFlagControl.Width;
  end;
begin
  for I := aParent.ControlCount - 1  downto 0 do
  begin
    aParent.Controls[i].Free;
  end;

  if aText=emptystr then
  begin
    exit;
  end;
  tempMaxWidth:=0;
  tempMaxHeight:=0;
  tempRowMaxHeight:=0;
  tempPriWinControl :=nil;

  templeft:=aleftspacing;
  tempTop :=aTopspacing;

  aText:=trim(ABReplaceSqlExtParams((aText)));
  tempPosBeginIndex:=1;
  tempSubStrBegIndex:=abpos(ABSubStrBegin,aText);
  while tempSubStrBegIndex>0 do
  begin
    tempSubStrEndIndex:=ABPos(ABSubStrEnd,aText,tempPosBeginIndex);
    if tempSubStrEndIndex<=0 then
      break;

    tempSubItemStr:=Copy(aText,tempSubStrBegIndex,tempSubStrEndIndex-tempSubStrBegIndex+length(ABSubStrEnd));
    tempSubStr:=tempSubItemStr;
    tempSubStr:=     ABStringReplace(tempSubStr,ABSubStrBegin,EmptyStr);
    tempSubStr:=Trim(ABStringReplace(tempSubStr,ABSubStrEnd  ,EmptyStr));

    tempConnName   :=ABGetItemValue(tempSubStr,'[ConnName]','','[');
    tempCacleDataset:=trim(ABGetItemValue(tempSubStr,'[CacleDataset]','','['));

    tempClassName   :=trim(ABGetItemValue(tempSubStr,'[ClassName]','','['));
    tempCaption     :=     ABGetItemValue(tempSubStr,'[Caption]','','[');
    if ABIsOkSQL(tempCaption) then
      tempCaption:=ABGetSQLValue(tempConnName,tempCaption,[],'');

    tempName        :=     ABGetItemValue(tempSubStr,'[Name]','','[');
    if tempName=EmptyStr then
      tempName        := 'Name_'+ABStrToName(tempCaption);

    if (tempName<>EmptyStr) and (tempClassName<>EmptyStr) and (tempCaption<>EmptyStr) then
    begin
      tempOperateFlags:=     ABGetItemValue(tempSubStr,'[OperateFlags]','','[');
      tempSetMinCharNum     :=trim(ABGetItemValue(tempSubStr,'[MinCharNum]','','['));
      if tempSetMinCharNum<>EmptyStr then
        aMinLabelCharNum:=StrToIntDef(tempSetMinCharNum,aMinLabelCharNum);

      tempDefaultValue:=trim(ABGetItemValue(tempSubStr,'[DefaultValue]','','['));
      tempFillSql     :=trim(ABGetItemValue(tempSubStr,'[FillSql]','','['));
      tempParentField :=trim(ABGetItemValue(tempSubStr,'[ParentField]','','['));
      tempKeyfield    :=trim(ABGetItemValue(tempSubStr,'[Keyfield]','','['));
      tempListField   :=trim(ABGetItemValue(tempSubStr,'[ListField]','','['));
      tempDownField   :=trim(ABGetItemValue(tempSubStr,'[DownField]','','['));

      tempDownCaption   :=trim(ABGetItemValue(tempSubStr,'[DownCaption]','','['));
      tempDownType   :=trim(ABGetItemValue(tempSubStr,'[DownType]','','['));
      tempFilter   :=trim(ABGetItemValue(tempSubStr,'[Filter]','','['));
      tempCanSelectParent   :=ABStrToBool(trim(ABGetItemValue(tempSubStr,'[CanSelectParent]','','[')));
      tempPassChar   :=trim(ABGetItemValue(tempSubStr,'[PassChar]','','['));
      tempMask   :=trim(ABGetItemValue(tempSubStr,'[Mask]','','['));
      tempMaxLength   :=StrToIntDef(trim(ABGetItemValue(tempSubStr,'[MaxLength]','','[')),0);

      tempFix   :=trim(ABGetItemValue(tempSubStr,'[Fix]','','['));

      tempSetLabelTop      :=trim(ABGetItemValue(tempSubStr,'[LabelTop]','','['));
      tempSetLabelleft     :=trim(ABGetItemValue(tempSubStr,'[Labelleft]','','['));
      tempSetLabelWidth    :=trim(ABGetItemValue(tempSubStr,'[LabelWidth]','','['));
      tempSetLabelHeight   :=trim(ABGetItemValue(tempSubStr,'[LabelHeight]','','['));

      tempSetControlTop      :=trim(ABGetItemValue(tempSubStr,'[ControlTop]','','['));
      tempSetControlleft     :=trim(ABGetItemValue(tempSubStr,'[Controlleft]','','['));
      tempSetControlWidth    :=trim(ABGetItemValue(tempSubStr,'[ControlWidth]','','['));
      tempSetControlHeight   :=trim(ABGetItemValue(tempSubStr,'[ControlHeight]','','['));

      tempCloumnCount       :=strtointdef(ABGetItemValue(tempSubStr,'[CloumnCount]','','['),1);
      tempSetCurDateTime    :=trim(ABGetItemValue(tempSubStr,'[SetCurDateTime]','','['));

      //创建使用的数据集
      tempDataset:=nil;
      tempDataSource :=nil;
      //设置填充的SQL与相关的保存显示字段
      if (tempFillSql<>EmptyStr) then
      begin
        tempDataset:=ABGetPubDataset(tempFillSql,'',true);
        if not Assigned(tempDataset) then
        begin
          if ABStrToBool(tempCacleDataset) then
          begin
            tempDataset:=ABCreatePubDataset(tempFillSql,tempConnName,tempFillSql,[],[]);
          end
          else
          begin
            tempDataset:=TABThirdReadDataQuery.Create(aOwner);
            TABThirdReadDataQuery(tempDataset).ConnName:=tempConnName;
            TABThirdReadDataQuery(tempDataset).Sql.Text:=tempFillSql;
            tempDataset.Open;
          end;
        end;
        tempDataset.First;
        tempDataSource:=ABGetPubDataSource(tempFillSql);
        if not Assigned(tempDataSource) then
        begin
          tempDataSource := TDataSource.Create(nil);
          ABAddPubDataSource(tempFillSql,tempDataSource);
        end;
        tempDataSource.DataSet:=tempDataset;
      end;

      //设置下拉列与主键列
      if Assigned(tempDataset) then
      begin
        if tempDataset.FieldCount=1 then
        begin
          tempListField:=tempDataset.Fields[0].FieldName;
          tempKeyfield:=tempListField;
        end;

        if tempListField=EmptyStr then
        begin
          tempStr1:=ABGetDatasetFieldNames(tempDataset);
          if ABGetSpaceStrCount(tempStr1,',')=1 then
          begin
            tempKeyfield    :=tempStr1;
            tempListField   :=tempStr1;
          end;
        end;
      end;

      //创建控件
      tempOperateFlagControl:=nil;
      tempControl:=TWinControl(ABCreateOrReFreshCloumnAndControl(3,
                                                         nil,
                                                         tempDataSource,
                                                         tempClassName,aParent.Name+'_'+tempName+'_Control',
                                                         '',tempDownField,tempListField,tempKeyfield,
                                                         tempDownType,
                                                         tempFilter,
                                                         tempParentField,
                                                         tempDownCaption,
                                                         tempPassChar,tempMask,

                                                         tempMaxLength,
                                                         tempCloumnCount,

                                                         False,
                                                         tempCanSelectParent,

                                                         aOwner,nil));


      //创建操作符
      if Assigned(tempControl) then
      begin
        {
        if ABPos('_BegEndQuery',tempName)<=0 then
        begin
          tempOperateFlagControl:=TABcxComboBox(ABCreateControl('TABcxComboBox',aOwner));
          if tempOperateFlags=emptystr then
          begin
            tempOperateFlags:='=,=;<,<;>,>';
          end;
          if ABPos('=,=',tempOperateFlags)<=0 then
            tempOperateFlags :='=,='+';'+tempOperateFlags;

          for I := 1 to ABGetSpaceStrCount(tempOperateFlags, ';') do
          begin
            tempOperateFlagControl.Properties.Items.Add(ABGetLeftRightStr(ABGetSpaceStr(tempOperateFlags, i, ';'),axdLeft,','));
          end;
          tempOperateFlagControl.Text:='=';
          tempOperateFlagControl.Properties.DropDownListStyle:=lsFixedList;
          tempOperateFlagControl.Parent:= aParent;
          tempOperateFlagControl.Name:= aParent.Name+'_'+tempName+'_OperateFlag';
          tempOperateFlagControl.Width:=100;
          tempOperateFlagControl.Style.BorderStyle:=ebsNone;
          tempOperateFlagControl.Style.Color:=clBtnFace;

        end;
         }

        if ABStrToBool(tempFix) then
        begin
          ABSetPropValue(tempControl,'Properties.ClearKey',0);
          ABSetPropValue(tempControl,'Properties.DropDownListStyle',2);
        end;
        tempControl.ShowHint:=false;
        ABSetPropValue(tempControl,'Caption',EmptyStr);
        tempLabel:=TABcxLabel.Create(aOwner);

        tempControl.Parent:= nil;
        tempLabel.Parent:=nil;
        if Assigned(tempOperateFlagControl) then
          tempOperateFlagControl.Parent:=nil;
        try
          tempLabel.ShowHint:=false;
          tempLabel.FocusControl:=tempControl;
          tempLabel.Name:= aParent.Name+'_'+tempName+'_Label';
          tempLabel.Caption:=tempCaption;
          tempLabel.AutoSize:=false;
          tempLabel.Properties.WordWrap:=true;

          if ABPos('_BegEndQueryEnd',tempName)>0 then
          begin
            tempLabel.Caption:=('止');
            tempMinCharNum:= 0;
          end
          else
            tempMinCharNum:= aMinLabelCharNum;

          //设置默认值
          tempDefaultValueSQLVariant:= null;
          if tempDefaultValue<>EmptyStr then
          begin
            if (Assigned(tempDataset)) and
               (Assigned(tempDataset.FindField(tempDefaultValue))) then
            begin
              tempDataset.Locate(tempDefaultValue,'True',[]);

              tempDefaultValue:=tempDataset.FieldByName(ABIIF(tempKeyfield<>emptystr,tempKeyfield,tempListField)).AsString;
            end
            else
            begin
              if ABIsOkSQL(tempDefaultValue) then
              begin
                tempDefaultValueSQLVariant:= ABGetSQLValue(tempConnName,tempDefaultValue,[],'');
                tempDefaultValue:=tempDefaultValueSQLVariant;
              end;
            end;

            if (tempDefaultValue<>EmptyStr) and
               (tempControl is TcxCustomDateEdit) and
               (TcxCustomDateEdit(tempControl).Properties.SaveTime) then
            begin
              if (ABPos('_BegEndQueryEnd',tempName)>0) or
                 (AnsiCompareText(ABRightStr(trim(tempCaption),1),'止')=0)  then
              begin
                if (AnsiCompareText(trim(tempSetCurDateTime),'1')=0) then
                  ABSetPropValue(tempControl,'EditValue',tempDefaultValue)
                else
                begin
                  if tempDefaultValueSQLVariant<>Null then
                    tempDefaultValue:=ABDateTimeToStr(tempDefaultValueSQLVariant);
                  ABSetPropValue(tempControl,'EditValue',ABDateToStr(DateOf(ABStrToDate(tempDefaultValue)))+' 23:59:59');
                end;
              end
              else
              begin
                if (AnsiCompareText(trim(tempSetCurDateTime),'1')=0) then
                  ABSetPropValue(tempControl,'EditValue',tempDefaultValue)
                else
                begin
                  if tempDefaultValueSQLVariant<>Null then
                    tempDefaultValue:=ABDateTimeToStr(tempDefaultValueSQLVariant);
                  ABSetPropValue(tempControl,'EditValue',ABDateToStr(DateOf(ABStrToDate(tempDefaultValue)))+' 00:00:00');
                end;
              end;
            end
            else if ABIsPublishProp(tempControl,'ItemIndex') then
              ABSetPropValue(tempControl,'ItemIndex',tempDefaultValue)
            else if ABIsPublishProp(tempControl,'EditValue') then
              ABSetPropValue(tempControl,'EditValue',tempDefaultValue)
            else if ABIsPublishProp(tempControl,'text') then
              ABSetPropValue(tempControl,'text',tempDefaultValue)
            else if ABIsPublishProp(tempControl,'value') then
              ABSetPropValue(tempControl,'value',tempDefaultValue)
            else if ABIsPublishProp(tempControl,'Checked') then
              ABSetPropValue(tempControl,'Checked',ABBoolToStr(ABStrToBool(tempDefaultValue)))
            else if ABIsPublishProp(tempControl,'ColorValue') then
              ABSetPropValue(tempControl,'ColorValue',tempDefaultValue)
            else
            begin
              if ABIsPublishProp(tempControl,'Lines') then
              begin
                tempObject:=ABGetObjectPropValue(tempControl,'Lines');
                TStrings(tempObject).Text:=tempDefaultValue;
              end;
            end;
          end;

          //设置输入参数的控件的位置与大小 (如果在定义串中有定义则采用,如没有则用ABSetControlSize函数中设置的大小)
          ABSetControlSize(tempLableWidth,tempLableHeight,
                           tempControlWidth,tempControlHeight,
                           tempLabel,tempControl,tempMinCharNum,True,True,false);

          tempLabel.Left:=StrToIntDef(tempSetLabelleft,tempLeft);
          tempLabel.Top :=StrToIntDef(tempSetLabelTop,tempTop);
          if tempSetLabelWidth<>EmptyStr then
            tempLabel.Width:=StrToIntDef(tempSetLabelWidth,tempLabel.Width);

          if tempSetLabelHeight<>EmptyStr then
            tempLabel.Height:=StrToIntDef(tempSetLabelHeight,tempLabel.Height);

          tempLeft:=tempLabel.Left+GetLabelWidth+aXspacing;

          if tempSetControlleft<>EmptyStr then
          begin
            if (Copy(tempSetControlleft,1,1)='+') then
            begin
              tempSetControlleft:=IntToStr(tempLabel.Left+strtoint(Copy(tempSetControlleft,2,maxint)));
            end
            else if (Copy(tempSetControlleft,1,1)='-') then
            begin
              tempSetControlleft:=IntToStr(tempLabel.Left-strtoint(Copy(tempSetControlleft,2,maxint)));
            end;
          end;
          if tempSetControlTop<>EmptyStr then
          begin
            if (Copy(tempSetControlTop,1,1)='+') then
            begin
              tempSetControlTop:=IntToStr(tempLabel.Top+strtoint(Copy(tempSetControlTop,2,maxint)));
            end
            else if (Copy(tempSetControlTop,1,1)='-') then
            begin
              tempSetControlTop:=IntToStr(tempLabel.Top-strtoint(Copy(tempSetControlTop,2,maxint)));
            end;
          end;
          tempControl.Left:=StrToIntDef(tempSetControlleft,tempLeft);
          tempControl.Top:=StrToIntDef(tempSetControlTop,tempLabel.Top);
          //在同一行 标签的宽度太长时缩短一下
          if (abs(tempControl.Top-tempLabel.Top)<tempLabel.Height) and
             (tempControl.Left<tempLabel.Left+GetLabelWidth) then
          begin
            tempLabel.Width:=tempControl.Left-tempLabel.Left-ABIIF(Assigned(tempOperateFlagControl),100,0);
          end;

          if tempSetControlWidth<>EmptyStr then
            tempControl.Width:=StrToIntDef(tempSetControlWidth,tempControl.Width);
          if tempSetControlHeight<>EmptyStr then
            tempControl.Height:=StrToIntDef(tempSetControlHeight,tempControl.Height);

          //每行是否仅显示一个控件
          if aRowOnlyOneControl then
          begin
            if tempSetControlWidth=EmptyStr then
            begin
              tempControlWidth:=aParent.Width-tempControl.Left-5;
              tempControl.Width:=tempControlWidth;
            end;

            if (ABPos('_BegEndQueryBegin',tempName)>0)  then
            begin
              if aParent.Width>0 then
              begin
                tempSetControlWidth:= inttostr(ABTrunc((aParent.Width-tempLabel.Left-GetLabelWidth-1-5-(7+2*7))/2));
                tempControl.Width:=StrToIntDef(tempSetControlWidth,tempControl.Width);
              end;
              templeft:=tempControl.Left+tempControl.Width+aXspacing;
            end
            else
            begin
              tempTop:=max(tempLabel.top+tempLabel.Height,tempControl.top+tempControl.Height)+aYspacing;
              templeft:=5;
            end;
          end
          else
          begin
            //控件所放位置是否超出最大宽度时换行
            if tempControl.Left+tempControl.Width-ABTrunc(tempControl.Width/5)>aParent.Width then
            begin
              //本次控件换行时调整上一个控件的宽度
              if (Assigned(tempPriWinControl)) then
              begin
                tempPriWinControl.Width:=aParent.Width-tempPriWinControl.Left-5;
              end;

              //换行时实始当前左边与上边的值
              templeft:=aleftspacing;
              tempTop:=tempTop+tempRowMaxHeight+aYspacing;
              //重新设置控件位置
              tempLabel.Left:=tempLeft;
              tempLabel.Top :=tempTop;
              tempLeft:=tempLabel.Left+GetLabelWidth+aXspacing;
              tempControl.Left:=tempLeft;
              tempControl.Top:=tempTop;

              //设置下一控件的左边
              tempLeft:=tempLeft+tempControl.Width+aXspacing;
              tempRowMaxHeight:=tempControl.Height;
            end
            else
            begin
              //设置下一控件的左边
              tempLeft:=tempLeft+tempControl.Width+aXspacing;
            end;
            tempRowMaxHeight:=Max(tempRowMaxHeight,Max(tempLabel.Height,tempControl.Height));
            tempPriWinControl:= tempControl;
          end;

          if Assigned(tempOperateFlagControl) then
          begin
            tempOperateFlagControl.Left:=tempLabel.Left+tempLabel.Width ;
            tempOperateFlagControl.Top:=tempLabel.Top;
          end;

        finally
          tempControl.Parent:= aParent;
          tempLabel.Parent:=aParent;
          if Assigned(tempOperateFlagControl) then
            tempOperateFlagControl.Parent:=aParent;

          ABClearControlValue(tempControl);
        end;

        tempMaxWidth:=max(tempMaxWidth,tempControl.Left+tempControl.Width+5);
        tempMaxHeight:=max(tempMaxHeight,tempControl.Top+tempControl.Height+8);
      end;
    end;
    aText:=ABStringReplace(aText,tempSubItemStr,'');
    tempPosBeginIndex:=tempSubStrEndIndex+length(ABSubStrEnd)-length(tempSubItemStr);
    tempSubStrBegIndex:=abpos(ABSubStrBegin,aText,tempPosBeginIndex);
  end;

  if (aAutoParentWidth) and (aParent.Width<>tempMaxWidth) then
  begin
    aParent.Width:=tempMaxWidth;
  end;

  if (aMinParentWidth>0) and (aParent.Width<aMinParentWidth) then
  begin
    aParent.Width:=aMinParentWidth;
  end;

  if (aAutoParentHeight) and (aParent.Height<>tempMaxHeight) then
  begin
    aParent.Height:=tempMaxHeight;
  end;

  if (aMinParentHeight>0) and (aParent.Height<aMinParentHeight) then
  begin
    aParent.Height:=aMinParentHeight;
  end;

  ABGetParentControlWidthAndHeight(
                                  aParent,
                                  tempWidth,
                                  tempHeight
                                    );

  ABSetRightControlAndLastControl(aParent,
                                  tempWidth,
                                  tempHeight,
                                  aRighttspacing,
                                  aBottomspacing,

                                  aAutoSuit,aSetakRight,aSetakBottom
                                  );
end;

function ABGetInputParamsWhere(aDataset:TDataSet;aText:string;aParent:Twincontrol;var aWhereRemark:string): string;
var
  tempPosBeginIndex,
  tempSubStrBegIndex,tempSubStrEndIndex: Integer;
  tempSubStr,tempSubItemStr:string;

  tempConnName:string;
  tempName,tempCaption,tempFieldName,tempFieldValue,tempFieldValue_NotFlag:string;

  tempFirstLink,tempLastLink,tempNot,tempIn:boolean;
  tempBoolean:boolean;
  tempControl:TControl;
begin
  Result:=EmptyStr;
  aWhereRemark:=EmptyStr;
  if aText=emptystr then
  begin
    exit;
  end;
  if not Assigned(aParent) then
  begin
    exit;
  end;

  aText:=trim(ABReplaceSqlExtParams((aText)));
  tempPosBeginIndex:=1;
  tempSubStrBegIndex:=abpos(ABSubStrBegin,aText);
  while tempSubStrBegIndex>0 do
  begin
    tempSubStrEndIndex:=ABPos(ABSubStrEnd,aText,tempPosBeginIndex);
    if tempSubStrEndIndex<=0 then
      break;

    tempSubItemStr:=Copy(aText,tempSubStrBegIndex,tempSubStrEndIndex-tempSubStrBegIndex+length(ABSubStrEnd));
    tempSubStr:=tempSubItemStr;
    tempFieldValue:= trim(ABReplaceInputParamsValue(tempBoolean,tempControl,tempSubStr,aParent,nil,false));

    tempSubStr:=     ABStringReplace(tempSubStr,ABSubStrBegin,EmptyStr);
    tempSubStr:=Trim(ABStringReplace(tempSubStr,ABSubStrEnd  ,EmptyStr));
    tempCaption     :=     ABGetItemValue(tempSubStr,'[Caption]','','[');
    tempConnName   :=ABGetItemValue(tempSubStr,'[ConnName]','','[');
    if ABIsOkSQL(tempCaption) then
      tempCaption:=ABGetSQLValue(tempConnName,tempCaption,[],'');
    tempName        :=     ABGetItemValue(tempSubStr,'[Name]','','[');
    if tempName=EmptyStr then
      tempName        := 'Name_'+ABStrToName(tempCaption);

    if (tempName<>EmptyStr) and
       (tempFieldValue<>EmptyStr) and
       (tempFieldValue<>QuotedStr(EmptyStr))
        then
    begin
      //区间开始
      if ABPos('_BegEndQueryBegin',tempName)>0 then
      begin
        if (Assigned(tempControl)) and
           (tempControl is TcxCustomDateEdit) then
        begin
          if not TcxCustomDateEdit(tempControl).Properties.SaveTime then
          begin
            tempFieldValue:=ABDateToStr(DateOf(TcxCustomDateEdit(tempControl).Date))+' 00:00:00';
          end;
        end;
        tempFieldName:=ABStringReplace(tempName,'_BegEndQueryBegin','');
        Result:=Result+' and '+tempFieldName+' >= '+QuotedStr(tempFieldValue);

        ABAddstr(aWhereRemark,aDataset.FieldByName(tempFieldName).DisplayLabel+' 大于等于 '+QuotedStr(tempFieldValue),' 并且 ');
      end
      //区间结束
      else if ABPos('_BegEndQueryEnd',tempName)>0 then
      begin
        if (Assigned(tempControl)) and
           (tempControl is TcxCustomDateEdit) then
        begin
          if not TcxCustomDateEdit(tempControl).Properties.SaveTime then
          begin
            tempFieldValue:=ABDateToStr(DateOf(TcxCustomDateEdit(tempControl).Date))+' 23:59:59';
          end;
        end;
        tempFieldName:=ABStringReplace(tempName,'_BegEndQueryEnd','');
        Result:=Result+' and '+tempFieldName+' <= '+QuotedStr(tempFieldValue);
        ABAddstr(aWhereRemark,aDataset.FieldByName(tempFieldName).DisplayLabel+' 小于等于 '+QuotedStr(tempFieldValue),' 并且 ');
      end
      else
      begin
        if (Assigned(tempControl)) then
        begin
          tempFieldValue_NotFlag:=tempFieldValue;
          tempNot:=  (AnsiCompareText(Copy(tempFieldValue_NotFlag,1,4),'not ')=0);
          if tempNot then
            tempFieldValue_NotFlag:=trim(Copy(tempFieldValue_NotFlag,5,Length(tempFieldValue_NotFlag)));

          tempIn:=     (AnsiCompareText(Copy(tempFieldValue_NotFlag,1,3),'in ')=0) and
                       (AnsiCompareText(Copy(tempFieldValue_NotFlag,Length(tempFieldValue_NotFlag),1),')')=0) ;
          if tempIn then
            tempFieldValue_NotFlag:=trim(Copy(tempFieldValue_NotFlag,4,Length(tempFieldValue_NotFlag)));

          if (tempIn) then
          begin
            Result:=Result+' and '+ABIIF(tempNot,' not ',EmptyStr)+tempName+' in '+tempFieldValue_NotFlag;
            ABAddstr(aWhereRemark,aDataset.FieldByName(tempName).DisplayLabel+' '+ABIIF(tempNot,'不',EmptyStr)+'在 '+QuotedStr(tempFieldValue_NotFlag)+' 中 ',' 并且 ');
          end
          else
          begin
            //日期时间
            if (tempControl is TcxCustomDateEdit) then
            begin
              //没有时间的日期
              if abpos(':',tempFieldValue_NotFlag)<=0 then
              begin
                tempName:='convert(varchar(10),'+tempName+',121)';
              end;
              Result:=Result+' and '+ABIIF(tempNot,' not ',EmptyStr)+tempName+'='+QuotedStr(tempFieldValue_NotFlag);
              ABAddstr(aWhereRemark,aDataset.FieldByName(tempName).DisplayLabel+' '+ABIIF(tempNot,'不',EmptyStr)+'等于 '+QuotedStr(tempFieldValue_NotFlag),' 并且 ');
            end
            else
            begin
              tempFirstLink:=  (AnsiCompareText(Copy(tempFieldValue_NotFlag,1,1),'%')=0) or
                               (AnsiCompareText(Copy(tempFieldValue_NotFlag,1,1),'*')=0);
              tempLastLink :=  (AnsiCompareText(Copy(tempFieldValue_NotFlag,Length(tempFieldValue_NotFlag),1),'%')=0) or
                               (AnsiCompareText(Copy(tempFieldValue_NotFlag,Length(tempFieldValue_NotFlag),1),'*')=0);
              if tempFirstLink then
                tempFieldValue_NotFlag:=trim(Copy(tempFieldValue_NotFlag,2,Length(tempFieldValue_NotFlag)));
              if tempLastLink then
                tempFieldValue_NotFlag:=trim(Copy(tempFieldValue_NotFlag,1,Length(tempFieldValue_NotFlag)-1));

              //备注字段输入,防止备注中输入了通配符
              if (tempControl is TcxCustomMemo) then
              begin
                Result:=Result+' and '+ABIIF(tempNot,' not ',EmptyStr)+tempName+' like '+QuotedStr('%'+tempFieldValue_NotFlag+'%');
                ABAddstr(aWhereRemark,aDataset.FieldByName(tempName).DisplayLabel+' '+ABIIF(tempNot,'不',EmptyStr)+'包含 '+QuotedStr(tempFieldValue_NotFlag),' 并且 ');
              end
              else
              begin
                if (tempFirstLink) and (tempLastLink)  then
                begin
                  Result:=Result+' and '+ABIIF(tempNot,' not ',EmptyStr)+tempName+' like '+QuotedStr('%'+tempFieldValue_NotFlag+'%');
                  ABAddstr(aWhereRemark,aDataset.FieldByName(tempName).DisplayLabel+' '+ABIIF(tempNot,'不',EmptyStr)+'包含 '+QuotedStr(tempFieldValue_NotFlag),' 并且 ');
                end
                else if (tempFirstLink) then
                begin
                  Result:=Result+' and '+ABIIF(tempNot,' not ',EmptyStr)+tempName+' like '+QuotedStr('%'+tempFieldValue_NotFlag);
                  ABAddstr(aWhereRemark,aDataset.FieldByName(tempName).DisplayLabel+' 右边'+ABIIF(tempNot,'不',EmptyStr)+'是 '+QuotedStr(tempFieldValue_NotFlag),' 并且 ');
                end
                else if (tempLastLink) then
                begin
                  Result:=Result+' and '+ABIIF(tempNot,' not ',EmptyStr)+tempName+' like '+QuotedStr(tempFieldValue_NotFlag+'%');
                  ABAddstr(aWhereRemark,aDataset.FieldByName(tempName).DisplayLabel+' 左边'+ABIIF(tempNot,'不',EmptyStr)+'是 '+QuotedStr(tempFieldValue_NotFlag),' 并且 ');
                end
                else
                begin
                  Result:=Result+' and '+ABIIF(tempNot,' not ',EmptyStr)+tempName+'='+QuotedStr(tempFieldValue_NotFlag);
                  ABAddstr(aWhereRemark,aDataset.FieldByName(tempName).DisplayLabel+' '+ABIIF(tempNot,'不',EmptyStr)+'等于 '+QuotedStr(tempFieldValue_NotFlag),' 并且 ');
                end;
              end;
            end;
          end;
        end;
      end
    end;

    aText:=ABStringReplace(aText,tempSubItemStr,'');
    tempPosBeginIndex:=tempSubStrEndIndex+length(ABSubStrEnd)-length(tempSubItemStr);
    tempSubStrBegIndex:=abpos(ABSubStrBegin,aText,tempPosBeginIndex);
  end;
end;

procedure ABRegisterClass(aClass:array of TPersistentClass);
var
  I:LongInt;
begin
  for I := low(aClass) to High(aClass) do
  begin
    RegisterClass(aClass[i]);
  end;

  if not Assigned(GetClass('TABCustomPanel')) then
  begin
    RegisterClass(TABcxGroupBox            );
    RegisterClass(TGroupBox                );
    RegisterClass(TcxLabel                 );
    RegisterClass(TABCustomPanel           );

    RegisterClass(TABcxDBDateEdit           );
    RegisterClass(TABcxDBTimeEdit           );
    RegisterClass(TABcxDBDateTimeEdit       );
    RegisterClass(TABcxDBCalcEdit           );
    RegisterClass(TABcxDBSpinEdit           );
    RegisterClass(TABcxDBCurrencyEdit       );
    RegisterClass(TABcxDBTextEdit           );
    RegisterClass(TABcxDBMaskEdit           );
    RegisterClass(TABcxDBLabel              );
    RegisterClass(TABcxDBMemo               );
    RegisterClass(TABcxDBRichEdit           );
    RegisterClass(TABcxDBCheckBox           );
    RegisterClass(TABcxDBCheckComboBox      );
    RegisterClass(TABcxDBCheckListBox       );
    RegisterClass(TABcxDBCheckGroup         );
    RegisterClass(TABcxDBRadioGroup         );
    RegisterClass(TABcxDBComboBox           );
    RegisterClass(TABcxDBColorComboBox      );
    RegisterClass(TABcxDBFontNameComboBox   );
    RegisterClass(TABcxDBShellComboBox      );
    RegisterClass(TABcxDBExtLookupComboBox  );
    RegisterClass(TABcxDBImageComboBox      );
    RegisterClass(TABcxDBMRUEdit            );
    RegisterClass(TABcxDBListBox            );
    RegisterClass(TABcxDBHyperLinkEdit      );
    RegisterClass(TABcxDBBlobEdit           );
    RegisterClass(TABcxDBPopupEdit          );
    RegisterClass(TABcxDBButtonEdit         );
    RegisterClass(TABcxDBImage              );
    RegisterClass(TABcxDBComBoBoxMemo       );
    RegisterClass(TABcxDBFieldSelects       );
    RegisterClass(TABcxDBFieldOrder         );
    RegisterClass(TABcxDBSQLFieldSelects   );
    RegisterClass(TABcxDBPrintSelect        );
    RegisterClass(TABcxDBDirSelect          );
    RegisterClass(TABcxDBFileSelect         );
    RegisterClass(TABcxDBTreeViewPopupEdit  );
    RegisterClass(TABcxDBProgressBar        );
    RegisterClass(TABcxDBTrackBar           );

    RegisterClass(TABcxDateEdit           );
    RegisterClass(TABcxTimeEdit           );
    RegisterClass(TABcxDateTimeEdit       );
    RegisterClass(TABcxCalcEdit           );
    RegisterClass(TABcxSpinEdit           );
    RegisterClass(TABcxCurrencyEdit       );
    RegisterClass(TABcxTextEdit           );
    RegisterClass(TABcxMaskEdit           );
    RegisterClass(TABcxLabel              );
    RegisterClass(TABcxMemo               );
    RegisterClass(TABcxRichEdit           );
    RegisterClass(TABcxCheckBox           );
    RegisterClass(TABcxCheckComboBox      );
    RegisterClass(TABcxCheckListBox       );
    RegisterClass(TABcxCheckGroup         );
    RegisterClass(TABcxRadioGroup         );
    RegisterClass(TABcxComboBox           );
    RegisterClass(TABcxColorComboBox      );
    RegisterClass(TABcxFontNameComboBox   );
    RegisterClass(TABcxShellComboBox      );
    RegisterClass(TABcxExtLookupComboBox  );
    RegisterClass(TABcxImageComboBox      );
    RegisterClass(TABcxMRUEdit            );
    RegisterClass(TABcxListBox            );
    RegisterClass(TABcxHyperLinkEdit      );
    RegisterClass(TABcxBlobEdit           );
    RegisterClass(TABcxPopupEdit          );
    RegisterClass(TABcxButtonEdit         );
    RegisterClass(TABcxImage              );
    RegisterClass(TABcxComBoBoxMemo       );
    RegisterClass(TABcxFieldSelects       );
    RegisterClass(TABcxFieldOrder         );
    RegisterClass(TABcxSQLFieldSelects   );
    RegisterClass(TABcxPrintSelect        );
    RegisterClass(TABcxDirSelect          );
    RegisterClass(TABcxFileSelect         );
    RegisterClass(TABcxTreeViewPopupEdit  );
    RegisterClass(TABcxProgressBar        );
    RegisterClass(TABcxTrackBar           );
  end;
end;

function ABFillDatasetToControl( aObject:TObject;aProperties:TcxCustomEditProperties;aCloumnCount:longint;aDataset:TDataset;aViewFields:string):boolean;
begin
  result:=True;

  aDataset.DisableControls;
  try
         if (aObject is TcxCustomListBox) then
    begin
      TcxCustomListBox(aObject).Items.BeginUpdate;
      TcxCustomListBox(aObject).MultiSelect:=true;
      TcxCustomListBox(aObject).Items.Clear;
      aDataset.First;
      while not aDataset.Eof do
      begin
        TcxCustomListBox(aObject).Items.Add(ABGetFieldValue(aDataset,aViewFields,[]));
        aDataset.Next;
      end;
      TcxCustomListBox(aObject).Items.EndUpdate;
    end
    else if (aObject is TcxCustomCheckListBox) then
    begin
      TcxCustomCheckListBox(aObject).Items.BeginUpdate;
      TcxCustomCheckListBox(aObject).Items.Clear;
      aDataset.First;
      while not aDataset.Eof do
      begin
        TcxCustomCheckListBox(aObject).AddItem(ABGetFieldValue(aDataset,aViewFields,[]));
        aDataset.Next;
      end;
      TcxCustomCheckListBox(aObject).Items.EndUpdate;
    end
    else if aProperties is TcxMRUEditProperties then
    begin
      TcxMRUEditProperties(aProperties).BeginUpdate;
      TcxMRUEditProperties(aProperties).Items.BeginUpdate;
      TcxMRUEditProperties(aProperties).Items.Clear;
      aDataset.First;
      while not aDataset.Eof do
      begin
        TcxMRUEditProperties(aProperties).Items.Add(ABGetFieldValue(aDataset,aViewFields,[]));
        aDataset.Next;
      end;
      TcxMRUEditProperties(aProperties).EndUpdate;
      TcxMRUEditProperties(aProperties).Items.EndUpdate;
    end
    else if aProperties is TcxCheckGroupProperties then
    begin
      TcxCheckGroupProperties(aProperties).BeginUpdate;
      TcxCheckGroupProperties(aProperties).Items.BeginUpdate;
      if aCloumnCount>1 then
        TcxCheckGroupProperties(aProperties).Columns:=aCloumnCount;
      TcxCheckGroupProperties(aProperties).Items.Clear;
      aDataset.First;
      while not aDataset.Eof do
      begin
        TcxCheckGroupProperties(aProperties).Items.Add;
        TcxCheckGroupProperties(aProperties).Items
          [TcxCheckGroupProperties(aProperties).Items.Count-1].Caption:=ABGetFieldValue(aDataset,aViewFields,[]);
        aDataset.Next;
      end;
      TcxCheckGroupProperties(aProperties).EndUpdate;
      TcxCheckGroupProperties(aProperties).Items.EndUpdate;
    end
    else if aProperties is TcxComboBoxProperties then
    begin
      TcxComboBoxProperties(aProperties).BeginUpdate;
      TcxComboBoxProperties(aProperties).Items.BeginUpdate;
      TcxComboBoxProperties(aProperties).Items.Clear;
      aDataset.First;
      while not aDataset.Eof do
      begin
        TcxComboBoxProperties(aProperties).Items.Add(ABGetFieldValue(aDataset,aViewFields,[]));
        aDataset.Next;
      end;
      TcxComboBoxProperties(aProperties).EndUpdate;
      TcxComboBoxProperties(aProperties).Items.EndUpdate;
    end
    else if aProperties is TcxCheckComboBoxProperties then
    begin
      TcxCheckComboBoxProperties(aProperties).BeginUpdate;
      TcxCheckComboBoxProperties(aProperties).Items.BeginUpdate;
      TcxCheckComboBoxProperties(aProperties).ShowEmptyText :=false;
      TcxCheckComboBoxProperties(aProperties).EditValueFormat :=cvfCaptions;
      TcxCheckComboBoxProperties(aProperties).Items.Clear;
      TcxCheckComboBoxProperties(aProperties).Delimiter:=',';
      aDataset.First;
      while not aDataset.Eof do
      begin
        TcxCheckComboBoxProperties(aProperties).Items.AddCheckItem(ABGetFieldValue(aDataset,aViewFields,[]));
        aDataset.Next;
      end;
      TcxCheckComboBoxProperties(aProperties).EndUpdate;
      TcxCheckComboBoxProperties(aProperties).Items.EndUpdate;
    end
    else if aProperties is TcxImageComboBoxProperties then
    begin
      TcxImageComboBoxProperties(aProperties).BeginUpdate;
      TcxImageComboBoxProperties(aProperties).Items.BeginUpdate;
      TcxImageComboBoxProperties(aProperties).Items.Clear;
      aDataset.First;
      while not aDataset.Eof do
      begin
        TcxImageComboBoxProperties(aProperties).Items.Add;
        TcxImageComboBoxProperties(aProperties).Items
          [TcxImageComboBoxProperties(aProperties).Items.Count-1].Description:=ABGetFieldValue(aDataset,aViewFields,[]);
        aDataset.Next;
      end;
      TcxImageComboBoxProperties(aProperties).EndUpdate;
      TcxImageComboBoxProperties(aProperties).Items.EndUpdate;
    end
    else if aProperties is TcxRadioGroupProperties then
    begin
      TcxRadioGroupProperties(aProperties).Items.BeginUpdate;
      TcxRadioGroupProperties(aProperties).BeginUpdate;
      if aCloumnCount>1 then
        TcxRadioGroupProperties(aProperties).Columns:=aCloumnCount;

      TcxRadioGroupProperties(aProperties).Items.Clear;
      aDataset.First;
      while not aDataset.Eof do
      begin
        TcxRadioGroupProperties(aProperties).Items.Add;
        TcxRadioGroupProperties(aProperties).Items[TcxRadioGroupProperties(aProperties).Items.Count-1].Caption:=ABGetFieldValue(aDataset,aViewFields,[]);
        aDataset.Next;
      end;
      TcxRadioGroupProperties(aProperties).EndUpdate;
      TcxRadioGroupProperties(aProperties).Items.EndUpdate;
    end
    else if aProperties is TcxShellComboBoxProperties then
    begin
      TcxShellComboBoxProperties(aProperties).BeginUpdate;
      TcxShellComboBoxProperties(aProperties).LookupItems.BeginUpdate;
      TcxShellComboBoxProperties(aProperties).LookupItems.Clear;
      aDataset.First;
      while not aDataset.Eof do
      begin
        TcxShellComboBoxProperties(aProperties).LookupItems.Add(ABGetFieldValue(aDataset,aViewFields,[]));
        aDataset.Next;
      end;
      TcxShellComboBoxProperties(aProperties).EndUpdate;
      TcxShellComboBoxProperties(aProperties).LookupItems.EndUpdate;
    end
    else if aProperties is TcxFontNameComboBoxProperties then
    begin
      TcxFontNameComboBoxProperties(aProperties).BeginUpdate;
      TcxFontNameComboBoxProperties(aProperties).Items.BeginUpdate;
      TcxFontNameComboBoxProperties(aProperties).Items.Clear;
      aDataset.First;
      while not aDataset.Eof do
      begin
        TcxFontNameComboBoxProperties(aProperties).Items.Add(ABGetFieldValue(aDataset,aViewFields,[]));
        aDataset.Next;
      end;
      TcxFontNameComboBoxProperties(aProperties).EndUpdate;
      TcxFontNameComboBoxProperties(aProperties).Items.EndUpdate;
    end
    else
    begin
      result:=false;
    end;
  finally
    aDataset.EnableControls;
  end;
end;

function ABCreateOrReFreshCloumnAndControl( aType:LongInt;
                                            aDataSource:TDataSource;
                                            aControlDataSource:TDataSource;
                                            aControlType,aControlName,
                                            aFieldName,aDownFields,aViewFields,aSaveFields:string;

                                            aDownType,
                                            aFilter,
                                            aParentField,
                                            aDownCaption,
                                            aPassChar,aMask:string;

                                            aMaxLength,aCloumnCount:longint;

                                            aReadOnly,
                                            aCanSelectParent:boolean;

                                            aOwner:TComponent;
                                            aCloumnAndControl:TObject):TObject;
  procedure DoCreateTreeListColumn(aFieldName:string;aPopupControl:Tcontrol);
  var
    tempTreeListColumn:TcxDBTreeListColumn;
  begin
    tempTreeListColumn:=TcxDBTreeListColumn(TABcxDBTreeView(aPopupControl).GetColumnByFieldName(aFieldName));
    if not Assigned(tempTreeListColumn) then
    begin
      tempTreeListColumn:=TcxDBTreeListColumn(TABcxDBTreeView(aPopupControl).CreateColumn(TABcxDBTreeView(aPopupControl).Bands.Items[0]));
      tempTreeListColumn.DataBinding.FieldName :=aFieldName;
    end;
  end;
  function ABCreateCloumnAndControl:TObject;
  begin
    result:=nil;
          if aOwner is TcxGridDBBandedTableView then
    begin
      result:=TcxGridDBBandedTableView(aOwner).CreateColumn;
    end
    else  if aOwner is TcxGridTableView then
    begin
      result:=TcxGridTableView(aOwner).CreateColumn;
    end
    else if ABGetDBControlNameDataset.Locate('Ti_Name',aControlType,[]) then
    begin
      result:=TWinControlClass(GetClass(aControlType)).Create(aOwner);
    end
    else if ABGetDBControlNameDataset.Locate('Ti_Varchar1',aControlType,[]) then
    begin
      result:=TWinControlClass(GetClass(aControlType)).Create(aOwner);
    end;

    if (Assigned(result)) then
    begin
      if result is TcxCustomTextEdit then
        TcxCustomTextEdit(result).Style.BorderStyle:=ebsFlat;

      TComponent(result).name:=aControlName;
    end;
  end;
  function ABReFreshCloumnAndControl:TObject;
  var
    tempPopupControl:Tcontrol;
    tempFirstViewFieldName:string;
    tempDataBinding:TObject;
    tempProperties:TcxCustomEditProperties;
    i,j:LongInt;
    tempDownCaption:string;
    tempDownCaptionIndex:longint;
  begin
    if (Assigned(aDataSource)) and
       (Assigned(aDataSource.Dataset)) then
      aDataSource.Dataset.DisableControls;
    try
      result:=aCloumnAndControl;
      //处理共性问题
      if (Assigned(result)) then
      begin
        //设置列的类型
        if (result is TcxGridColumn) and
           (ABGetDBControlNameDataset.Locate('Ti_Name',aControlType,[])) then
        begin
          TcxGridColumn(result).PropertiesClass:=TcxCustomEditPropertiesClass(GetRegisteredEditProperties.FindByClassName(ABGetDBControlNameDataset.FieldByName('Ti_Varchar2').AsString));
        end;

        //取出类型
        tempFirstViewFieldName:= ABGetLeftRightStr(ABStringReplace(aViewFields,';',','),axdLeft,',');
        tempProperties:=nil;
        if (result is TcxCustomGridTableItem) then
        begin
          tempProperties:=TcxCustomGridTableItem(result).Properties;
        end
        else if (result is TcxCustomTextEdit) then
        begin
          tempProperties:=TcxCustomTextEdit(result).Properties;
        end;

        //设置数据感知属性
        tempDataBinding:=ABGetObjectPropValue(result,'DataBinding');
        if (Assigned(tempDataBinding)) then
        begin
          if (tempDataBinding is TcxDBEditDataBinding) then
          begin
            if TcxDBEditDataBinding(tempDataBinding).DataSource<>aDataSource then
              TcxDBEditDataBinding(tempDataBinding).DataSource:=aDataSource;
            if TcxDBEditDataBinding(tempDataBinding).DataField<>aFieldName then
              TcxDBEditDataBinding(tempDataBinding).DataField:=aFieldName;
          end
          else if (tempDataBinding is TcxGridItemDBDataBinding) then
          begin
            if TcxGridItemDBDataBinding(tempDataBinding).FieldName<>aFieldName then
              TcxGridItemDBDataBinding(tempDataBinding).FieldName:=aFieldName;
          end;
        end;

        //特定处理
        if tempProperties is TcxTimeEditProperties then
        begin
          if AnsiCompareText(aMask,'hh')=0 then
          begin
            TcxTimeEditProperties(tempProperties).TimeFormat:=tfHour;
          end
          else if AnsiCompareText(aMask,'hh:mm')=0 then
          begin
            TcxTimeEditProperties(tempProperties).TimeFormat:=tfHourMin;
          end
          else if AnsiCompareText(aMask,'hh:mm:ss')=0 then
          begin
            TcxTimeEditProperties(tempProperties).TimeFormat:=tfHourMinSec;
          end;
        end
        else if tempProperties is TABcxPrintSelectProperties then
        begin
          TABcxPrintSelectProperties(tempProperties).Items.Clear;
          for I := 0 to printers.printer.printers.Count-1 do
          begin
            TcxComboBoxProperties(tempProperties).Items.Add(printers.printer.Printers[i]);
          end;
        end
        else if tempProperties is TcxCalcEditProperties then
        begin
          if (ABStrInArray(aControlType,['bigint','int','smallint','tinyint'])>=0) then
            TcxCalcEditProperties(tempProperties).Precision:=0;
        end
        else  if (Assigned(aControlDataSource)) and
                 (Assigned(aControlDataSource.Dataset)) and
                 (ABFillDatasetToControl(result,tempProperties,aCloumnCount,aControlDataSource.Dataset,aViewFields)) then
        begin

        end
        else if tempProperties is TABcxTreeViewPopupEditProperties then
        begin
          tempPopupControl:=Tcontrol(ABGetPubDownEdit( aControlName,
                                              aControlDataSource,
                                              aControlType,
                                              aDownFields,aViewFields,aSaveFields,

                                              aFilter,

                                              aCanSelectParent,
                                              aParentField ));
          if TABcxTreeViewPopupEditProperties(tempProperties).PopupControl<>tempPopupControl then
            TABcxTreeViewPopupEditProperties(tempProperties).PopupControl:=tempPopupControl;
          if (Assigned(tempPopupControl)) and
             (tempPopupControl is TABcxDBTreeView) then
          begin
            if ABGetSpaceStrCount(aDownFields,',')>1 then
            begin
              for j := 1 to ABGetSpaceStrCount(aDownFields, ',') do
              begin
                DoCreateTreeListColumn(ABGetSpaceStr(aDownFields, j, ','),tempPopupControl);
              end;
              TABcxDBTreeView(tempPopupControl).OptionsView.Headers:=True;
            end
            else
            begin
              DoCreateTreeListColumn(aDownFields,tempPopupControl);
              TABcxDBTreeView(tempPopupControl).OptionsView.Headers:=false;
            end;
            TABcxDBTreeView(tempPopupControl).OptionsBehavior.IncSearchItem:=TABcxDBTreeView(tempPopupControl).Columns[0];
          end;
        end
        else if tempProperties is TcxExtLookupComboBoxProperties then
        begin
          if AnsiCompareText(aDownType,'lsFixedList')=0 then
            TcxExtLookupComboBoxProperties(tempProperties).DropDownListStyle:=lsFixedList
          else  if AnsiCompareText(aDownType,'lsEditFixedList')=0 then
            TcxExtLookupComboBoxProperties(tempProperties).DropDownListStyle:=lsEditFixedList
          else
            TcxExtLookupComboBoxProperties(tempProperties).DropDownListStyle:=lsEditList;

          TcxExtLookupComboBoxProperties(tempProperties).View:= TcxGridTableView(ABGetPubDownEdit(aControlName,
                                                                                                  aControlDataSource,
                                                                                                  aControlType,
                                                                                                  aDownFields,aViewFields,aSaveFields,

                                                                                                  aFilter,

                                                                                                  aCanSelectParent,
                                                                                                  aParentField ));

          TcxExtLookupComboBoxProperties(tempProperties).KeyFieldNames:=aSaveFields;
          TcxExtLookupComboBoxProperties(tempProperties).ListFieldItem:=
              ABGetColumnByFieldName(TcxGridTableView(TcxExtLookupComboBoxProperties(tempProperties).View),tempFirstViewFieldName);

          TcxGridTableOptionsView(TcxExtLookupComboBoxProperties(tempProperties).View.OptionsView).Header:=ABGetSpaceStrCount(aDownFields,',')>1;

          if (TcxGridTableView(TcxExtLookupComboBoxProperties(tempProperties).View).ColumnCount>0) and
             (aDownCaption<>emptystr) then
          begin
            for i := 1 to ABGetSpaceStrCount(aDownCaption, ',')  do
            begin
              tempDownCaption:=ABGetSpaceStr(aDownCaption,i,',');
              if abpos('=',tempDownCaption)>0 then
              begin
                tempDownCaptionIndex       :=
                  ABGetColumnByFieldName(TcxGridTableView(TcxExtLookupComboBoxProperties(tempProperties).View),
                                         ABGetLeftRightStr(tempDownCaption,axdleft)
                                         ).Index;
                tempDownCaption:=ABGetLeftRightStr(tempDownCaption,axdRight);
              end
              else
              begin
                tempDownCaptionIndex       :=i-1;
              end;

              if tempDownCaption<>emptystr then
              begin
                if (tempDownCaptionIndex<TcxGridTableView(TcxExtLookupComboBoxProperties(tempProperties).View).ColumnCount) then
                  TcxGridTableView(TcxExtLookupComboBoxProperties(tempProperties).View).Columns[tempDownCaptionIndex].Caption:=tempDownCaption;
              end;
            end;
          end;
        end;

        if (tempProperties is TcxCustomTextEditProperties) then
        begin
          //处理密码字符的问题
          if aPassChar<>EmptyStr then
          begin
            TcxCustomTextEditProperties(tempProperties).EchoMode:=eemPassword;
            TcxCustomTextEditProperties(tempProperties).PasswordChar:=aPassChar[1];
          end;

          //处理只读
          TcxCustomTextEditProperties(tempProperties).ReadOnly:=aReadOnly;
          if (result is TcxCustomGridTableItem) then
            TcxCustomGridTableItem(result).Options.Editing:=(not aReadOnly);

          //处理显示格式
          if (aMask<>emptystr) then
          begin
            TcxCustomTextEditProperties(tempProperties).DisplayFormat:=aMask;
          end;

          //设置字段最大长度
          if (TcxCustomTextEditProperties(tempProperties).MaxLength<>aMaxLength) then
          begin
            TcxCustomTextEditProperties(tempProperties).MaxLength:=aMaxLength;
          end;
        end;

        if tempProperties is TcxPopupEditProperties then
        begin
          TcxPopupEditProperties(tempProperties).ImmediatePopup:=false;
        end;


        {
        //特定类型控件单独处理
             if (AnsiCompareText(aControlType,trim('TABcxDBDateEdit         '))=0)   then
        begin
        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBTimeEdit         '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBDateTimeEdit     '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBCalcEdit         '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBSpinEdit         '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBCurrencyEdit     '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBTextEdit         '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBMaskEdit         '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBLabel            '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBMemo             '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBRichEdit         '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBCheckBox         '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBCheckComboBox    '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBCheckListBox     '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBCheckGroup       '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBRadioGroup       '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBComboBox         '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBColorComboBox    '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBFontNameComboBox '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBShellComboBox    '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBExtLookupComboBox'))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBImageComboBox    '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBMRUEdit          '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBListBox          '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBTreeViewPopupEdit'))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBHyperLinkEdit    '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBBlobEdit         '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBPopupEdit        '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBButtonEdit       '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBImage            '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBComBoBoxMemo     '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBFieldSelects     '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBFieldOrder       '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBSQLFieldSelects '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBPrintSelect '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBDirSelect        '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDBFileSelect       '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDateEdit           '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxTimeEdit           '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDateTimeEdit       '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxCalcEdit           '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxSpinEdit           '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxCurrencyEdit       '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxTextEdit           '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxMaskEdit           '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxLabel              '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxMemo               '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxRichEdit           '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxCheckBox           '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxCheckComboBox      '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxCheckListBox       '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxCheckGroup         '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxRadioGroup         '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxComboBox           '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxColorComboBox      '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxFontNameComboBox   '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxShellComboBox      '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxExtLookupComboBox  '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxImageComboBox      '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxMRUEdit            '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxListBox            '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxHyperLinkEdit      '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxBlobEdit           '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxPopupEdit          '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxButtonEdit         '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxImage              '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxComBoBoxMemo       '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxFieldSelects       '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxFieldOrder         '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxSQLFieldSelects   '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxPrintSelect        '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxDirSelect          '))=0)   then
        begin

        end
        else if (AnsiCompareText(aControlType,trim('TABcxFileSelect         '))=0)   then
        begin

        end;
             }
      end;
    finally
      if (Assigned(aDataSource)) and
         (Assigned(aDataSource.Dataset)) then
        aDataSource.Dataset.EnableControls;
    end;
  end;
begin
  result:=nil;
  if (aType=1) then
  begin
    result:=ABCreateCloumnAndControl;
  end
  else if (aType=2) then
  begin
    result:=ABReFreshCloumnAndControl;
  end
  else if (aType=3) then
  begin
    aCloumnAndControl:=ABCreateCloumnAndControl;
    result:=ABReFreshCloumnAndControl;
  end;
end;

function DoCreateOrReFreshCloumnAndControl( aType:LongInt;
                                            aDataSource:TDataSource;
                                            aField:TField;
                                            aOwner:TComponent;
                                            aCloumnAndControl:TObject):TObject;
var
  tempFieldDef:PABFieldDef;
  tempDataSource:TDataSource;
  tempControlType,tempControlName,
  tempFieldName,tempDownFields,tempViewFields,tempSaveFields:string;

  tempDownType,
  tempFilter,
  tempParentField,
  tempDownCaption,
  tempPassChar,tempMask:string;

  tempMaxLength,tempCloumnCount:longint;

  tempCanEditInEdit,
  tempCanEditInInsert,
  tempCanSelectParent:boolean;
begin
  result:= nil;
  if (Assigned(aField)) then
  begin
    tempFieldDef:=ABFieldDefByFieldName(aField.DataSet,aField.FieldName);
    if (Assigned(tempFieldDef)) then
    begin
      tempDataSource       :=nil;
      tempControlType      :=tempFieldDef.Fi_ControlType;
      tempControlName      :=tempFieldDef.Flag;
      tempFieldName        :=tempFieldDef.Fi_Name;
      tempDownFields       :=EmptyStr;
      tempViewFields       :=EmptyStr;
      tempSaveFields       :=EmptyStr;

      tempDownType         :=EmptyStr;
      tempFilter           :=EmptyStr;
      tempParentField      :=EmptyStr;
      tempDownCaption      :=EmptyStr;
      tempPassChar         :=tempFieldDef.Fi_PassChar;
      tempMask             :=tempFieldDef.Fi_Mask;

      tempMaxLength        :=tempFieldDef.Fi_MaxLength;
      tempCloumnCount      :=1;

      tempCanEditInEdit    :=tempFieldDef.Fi_CanEditInEdit;
      tempCanEditInInsert  :=tempFieldDef.Fi_CanEditInInsert;
      tempCanSelectParent  :=false;

      if (Assigned(tempFieldDef.PDownDef))  then
      begin
        tempDataSource:=tempFieldDef.PDownDef.Fi_Datasource;
        tempDownFields       :=tempFieldDef.PDownDef.FI_DownFields;
        tempViewFields       :=tempFieldDef.PDownDef.FI_ViewField;
        tempSaveFields       :=tempFieldDef.PDownDef.FI_SaveField;

        tempDownType         :=tempFieldDef.PDownDef.Fi_DownType;
        tempFilter           :=tempFieldDef.PDownDef.Fi_Filter;
        tempParentField      :=tempFieldDef.PDownDef.Fi_ParentField;
        tempDownCaption      :=tempFieldDef.PDownDef.Fi_DownCaptions;
        tempCanSelectParent  :=tempFieldDef.PDownDef.Fi_CanSelectParent;
      end;

      result:= ABCreateOrReFreshCloumnAndControl( aType,
                                                  aDataSource,
                                                  tempDataSource,
                                                  tempControlType,tempControlName,
                                                  tempFieldName,tempDownFields,tempViewFields,tempSaveFields,

                                                  tempDownType,tempFilter,
                                                  tempParentField,
                                                  tempDownCaption,
                                                  tempPassChar,tempMask,

                                                  tempMaxLength,tempCloumnCount,

                                                  (not tempCanEditInEdit) and  (not tempCanEditInInsert),
                                                  tempCanSelectParent,
                                                  aOwner,
                                                  aCloumnAndControl);
    end;
  end;
end;

function ABCreateCloumnAndControl(aDataSource:TDataSource;
                                  aField:TField;
                                  aOwner:TComponent):TObject;
begin
  result:= DoCreateOrReFreshCloumnAndControl( 1,
                                              aDataSource,
                                              aField,
                                              aOwner,
                                              nil);
end;

procedure ABReFreshCloumnAndControl( aDataSource:TDataSource;
                                    aField:TField;
                                    aCloumnAndControl:TObject);
begin
  DoCreateOrReFreshCloumnAndControl(2,
                                    aDataSource,
                                    aField,
                                    nil,
                                    aCloumnAndControl);
end;

procedure ABRefreshLabelCaption(aParent:Twincontrol;aDataSet:TDataSet);
var
  I: Integer;
  tempFieldName,tempName,tempCaption:string;
begin
  for I := aParent.ControlCount - 1  downto 0 do
  begin
    if (aParent.Controls[i] is TABDBFieldCaption) then
    begin
      tempName:=TABDBFieldCaption(aParent.Controls[i]).Name;
      tempFieldName:=TABDBFieldCaption(aParent.Controls[i]).DataField;
      if tempFieldName<>EmptyStr then
      begin
        tempCaption:=EmptyStr;
        if (Assigned(aDataSet.FindField(tempFieldName))) then
        begin
          tempCaption:=aDataSet.FindField(tempFieldName).DisplayLabel;
          if ABPos('_BegEndQueryBegin',tempName)>0 then
          begin
            tempCaption:=tempCaption+('起');
          end
          else if ABPos('_BegEndQueryEnd',tempName)>0 then
          begin
            tempCaption:=('止');
          end;
          if (tempCaption<>EmptyStr) and
             (tempCaption<>TABDBFieldCaption(aParent.Controls[i]).Caption) then
            TABDBFieldCaption(aParent.Controls[i]).Caption:=tempCaption;
        end;
      end;
    end;
  end;
end;

//根据当前值定位关联的TreeView焦点,DoInitPopup中调用以在显示前定位
function ABTreeViewPopupEdit_Locate(aPopupEdit: TcxCustomPopupEdit):Boolean;
var
  tempField:TField;
  tempValue:string;
  tempTreeView:TABcxDBTreeView;
begin
  result:=false;
  if (not (Assigned(aPopupEdit))) or
     (not (Assigned(aPopupEdit.Properties))) or
     (not (aPopupEdit.Properties.PopupControl is TABcxDBTreeView))  then
    exit;

  tempTreeView:= TABcxDBTreeView(aPopupEdit.Properties.PopupControl);
  if (Assigned(tempTreeView)) then
  begin
    if (Assigned(tempTreeView.DataController.DataSource)) and
       (Assigned(tempTreeView.DataController.DataSource.DataSet)) and
       (not ABDataSetIsEmpty(tempTreeView.DataController.DataSource.DataSet)) then
    begin
      tempValue:=EmptyStr;
      if aPopupEdit is TcxPopupEdit then
      begin
        if (Assigned(aPopupEdit.InplaceParams.Position.Item)) then
        begin
          tempField:= ABGetColumnField(TcxCustomGridTableItem(aPopupEdit.InplaceParams.Position.Item));
          if (Assigned(tempField)) and (not abvarisnull(tempField.Value)) then
            tempValue:=tempField.Value;
        end
        else
        begin
          tempValue:=aPopupEdit.Text;
        end;
      end
      else if aPopupEdit is TcxDBPopupEdit then
      begin
        tempField:= TcxDBPopupEdit(aPopupEdit).DataBinding.Field;
        if (Assigned(tempField))  and (not abvarisnull(tempField.Value)) then
          tempValue:=tempField.Value;
      end;

      if tempValue<>EmptyStr then
      begin
        if tempTreeView.DataController.LocateByKey(tempValue) then
        begin
          result:=true;
        end;
      end;
    end;
  end;
end;

//ComboBox下拉前做的事情
procedure ABComboBox_InitPopup(aField:TField;aObject:TObject);
var
  tempFieldDef:PABFieldDef;
  tempFilter:string;
  tempBoolean:Boolean;
  tempProperties:TcxCustomEditProperties;
  procedure ABSetTableViewFilter(aTableView:TcxCustomGridTableView;aFilter:string;acxFilterOperatorKind:TcxFilterOperatorKind);
  var
    tempstr,tempFieldName,tempFieldValue:string;
    tempColumn:TcxCustomGridTableItem;
    i:LongInt;
  begin
    for I := 1 to ABGetSpaceStrCount(aFilter, ' and ') do
    begin
      tempstr := ABGetSpaceStr(aFilter, i, ' and ');

      tempFieldName:= ABGetLeftRightStr(tempstr,axdLeft);
      if tempFieldName<>EmptyStr then
      begin
        tempFieldValue:= ABUnQuotedFirstlastStr(ABGetLeftRightStr(tempstr,axdRight));
        tempColumn:=ABGetColumnByFieldName(aTableView,tempFieldName);
        if (Assigned(tempColumn)) then
        begin
          tempColumn.DataBinding.AddToFilter(nil,acxFilterOperatorKind , tempFieldValue);
        end;
      end;
    end;

    if not aTableView.DataController.Filter.Active  then
      aTableView.DataController.Filter.Active := true;
  end;
begin
  if (Assigned(aField)) then
  begin
    tempFieldDef:=ABFieldDefByFieldName(aField.DataSet,aField.FieldName);
    if (Assigned(tempFieldDef)) and (Assigned(tempFieldDef.PDownDef)) then
    begin
      tempProperties:=nil;
      if (Assigned(aObject)) then
      begin
        if (aObject is TcxCustomGridTableItem) then
        begin
          tempProperties:=TcxCustomGridTableItem(aObject).Properties;
        end
        else if (aObject is TcxCustomTextEdit) then
        begin
          tempProperties:=TcxCustomTextEdit(aObject).Properties;
        end;
      end;

      if tempFieldDef.PDownDef.FI_DownRefresh then
      begin
        ABReFreshQuery(tempFieldDef.PDownDef.Fi_DataSet,[],aField.DataSet);
        if (Assigned(tempFieldDef.PDownDef.Fi_Dataset)) then
          ABFillDatasetToControl(aObject,tempProperties,1,tempFieldDef.PDownDef.Fi_Dataset,tempFieldDef.PDownDef.FI_ViewField);
      end;

      //暂时只支持TcxExtLookupComboBoxProperties的字段间过滤
      if (tempProperties is TcxExtLookupComboBoxProperties) and
         (Assigned(TcxExtLookupComboBoxProperties(tempProperties).View)) then
      begin
        //在ExtLookupComboBox开窗前处理过滤操作
        tempFilter:=tempFieldDef.PDownDef.Fi_Filter;
        if (tempFilter<>EmptyStr) then
        begin
          if ABPos(':' ,tempFilter)>0 then
          begin
            tempFilter:=ABReplaceSqlFieldNameParams(tempFilter,[aField.DataSet]);
          end;

          if ABPos(':' ,tempFilter)<=0 then
          begin
            //此处用TableViewFilter(AutoDataSetFilter=False)而不用数据集Filter是因为在其它记录中还要显示出下拉框的内容
            //此处设置的过滤仅是由and连接的不带括号的字段值过滤
            if (TcxExtLookupComboBoxProperties(tempProperties).View is TcxGridDBBandedTableView) then
              TcxGridDBBandedTableView(TcxExtLookupComboBoxProperties(tempProperties).View).DataController.Filter.AutoDataSetFilter :=False
            else if (TcxExtLookupComboBoxProperties(tempProperties).View is TcxGridDBTableView) then
              TcxGriddbTableView(TcxExtLookupComboBoxProperties(tempProperties).View).DataController.Filter.AutoDataSetFilter:=False;

            ABSetTableViewFilter(TcxExtLookupComboBoxProperties(tempProperties).View,tempFilter,foEqual);
          end;
        end;
      end;

      if Assigned(TABDictionaryQuery(aField.DataSet).OnFieldInitPopup) then
        TABDictionaryQuery(aField.DataSet).OnFieldInitPopup(tempFieldDef,tempBoolean);
    end;
  end;
end;

//TABcxLookupComboBox、TABcxDBLookupComboBox、TABcxExtLookupComboBox、TABcxDBExtLookupComboBox 调用
procedure ABLookupComboBox_Change(aLookupEdit:TcxCustomDBLookupEdit);
var
  tempColumnCount,
  I:LongInt;
  tempInputText:string;
  tempTableView: TcxGridTableView;
begin
  if (not Assigned(aLookupEdit.Properties)) or
     (not Assigned(aLookupEdit.Properties.DataController)) or
     (aLookupEdit.Properties.DataController.DataSet.ControlsDisabled)
         then
    exit;

  tempTableView:=nil;
  tempColumnCount:=0;
  tempInputText:=EmptyStr;
  if aLookupEdit is TcxLookupComboBox then
  begin
    tempColumnCount:=TcxLookupComboBox(aLookupEdit).Properties.ListColumns.Count;
    tempInputText:=TcxLookupComboBox(aLookupEdit).EditText;
  end
  else if aLookupEdit is TcxExtLookupComboBox then
  begin
    tempTableView:=TcxGridTableView(TcxExtLookupComboBox(aLookupEdit).Properties.View);
    if (Assigned(tempTableView)) then
      tempColumnCount:=tempTableView.VisibleColumnCount;
    tempInputText:=TcxExtLookupComboBox(aLookupEdit).EditText;
  end
  else if aLookupEdit is TcxDBLookupComboBox then
  begin
    tempColumnCount:=TcxDBLookupComboBox(aLookupEdit).Properties.ListColumns.Count;
    tempInputText:=TcxDBLookupComboBox(aLookupEdit).EditText;
  end
  else if aLookupEdit is TcxDBExtLookupComboBox then
  begin
    tempTableView:=TcxGridTableView(TcxDBExtLookupComboBox(aLookupEdit).Properties.View);
    if (Assigned(tempTableView)) then
      tempColumnCount:=tempTableView.VisibleColumnCount;

    tempInputText:=TcxDBExtLookupComboBox(aLookupEdit).EditText;
  end
  else
  begin
    exit;
  end;

  if (Length(tempInputText) = 0) then
  begin
    aLookupEdit.DroppedDown := false;
    Exit;
  end;

  if (Assigned(tempTableView)) then
  begin
    ABSearchIncxTableView(tempTableView,'*'+tempInputText+'*');
  end
  else
  begin
    //使用CX列Search.Locate查找符合字串的记录,仅支持AB*的方式
    if (Assigned(aLookupEdit.Properties.DataController)) and
       (Assigned(aLookupEdit.Properties.DataController.Search)) then
    begin
      for I := 0 to tempColumnCount-1 do
      begin
        if (aLookupEdit.Properties.DataController.Search.Locate(i,tempInputText)) then
        begin
          Break;
        end;
      end;
    end;
  end;
end;

//TABcxSQLFieldSelects、TABcxDBSQLFieldSelects 调用
procedure ABDownFieldNameSelects_ButtonClick(AButtonVisibleIndex: Integer;aField:TField;aDataSource:TDataSource);
  procedure GetDownSQL(aDataset:TABDictionaryQuery;var aConnName:string;var aSQL:string);
  begin
    if aConnName=EmptyStr then
      aConnName := ABGetConnNameByConnGuid(aDataset.FindField('FI_ConnName').AsString);

    if aDataset.FindField('Fi_IsAutoRemember').AsBoolean then
    begin
      aSQL:=
          ' select DISTINCT '+aDataset.FindField('Fi_Name').AsString+
          ' from ' +aDataset.FieldDefByFieldName('FI_Ta_Guid').PDownDef.Fi_DataSet.Lookup('Ta_Guid',aDataset.FieldByName('FI_Ta_Guid').AsString,'Ta_Name')+
          ' order by '+aDataset.FindField('Fi_Name').AsString;
    end
    else if aDataset.FindField('Fi_Tr_Code').AsString<>EmptyStr then
    begin
      aSQL:=
          ' select * '+
          ' from ABSys_Org_TreeItem '+
          ' where Ti_Tr_Guid='+QuotedStr(
                                         ABGetConstSqlPubDataset('ABSys_Org_Tree',[aDataset.FindField('Fi_Tr_Code').AsString]).fieldbyname('Tr_Guid').asstring
                                          );
    end
    else if aDataset.FindField('Fi_DownGroup').AsString<>EmptyStr then
    begin
      if ABGetPubFieldDownGroup.Locate('Fi_Name',Trim(aDataset.FindField('Fi_DownGroup').AsString),[]) then
        GetDownSQL(TABDictionaryQuery(ABGetPubFieldDownGroup),aConnName,aSQL);
    end
    else if aDataset.FindField('Fi_DownSQL').AsString<>EmptyStr then
    begin
      aSQL:=aDataset.FindField('Fi_DownSQL').AsString;
    end;
  end;
var
  tempConnName,
  tempStr1,tempCommandText:string;
  tempDataSet: TDataSet;
  tempFieldDef:PABFieldDef;
begin
  if (AButtonVisibleIndex=0) and
     (Assigned(aField)) and
     (Assigned(aDataSource)) and
     (Assigned(aDataSource.DataSet)) and
     ((aDataSource.AutoEdit) or (ABCheckDataSetInEdit(aDataSource.DataSet)))   then
  begin
    tempFieldDef:=ABFieldDefByFieldName(aField.DataSet,aField.FieldName);
    if Assigned(tempFieldDef) then
    begin
      if (AnsiCompareText(tempFieldDef.Fi_Ta_Name,'ABSys_Org_Field')=0) then
      begin
        tempConnName:=EmptyStr;
        tempCommandText:=EmptyStr;
        GetDownSQL(TABDictionaryQuery(aField.DataSet),tempConnName,tempCommandText);
      end
      else
      begin
        if (tempFieldDef.IsDown) then
        begin
          tempCommandText := tempFieldDef.PDownDef.Fi_DownSQL;
          tempConnName := tempFieldDef.PDownDef.FI_ConnName
        end;
      end;

      tempDataSet:=ABGetDataset(tempConnName,tempCommandText,[]);
      try
        tempStr1:=aField.AsString;
        if (ABSelectFieldNames(tempStr1,tempDataSet)) and (tempStr1<>aField.AsString) then
        begin
          ABSetFieldValue(tempStr1,aField);
        end;
      finally
        tempDataSet.Free;
      end;
    end;
  end;
end;

//TABcxFieldOrder、TABcxDBFieldOrder调用
procedure ABFieldOrder_ButtonClick(AButtonVisibleIndex:LongInt;aField:TField;aDataSource:TDataSource);
var
  tempCurProgress: Integer;
  tempSumFieldNames,
  tempSumFieldNamesBack,
  tempLocalFieldNames,tempLocalFieldValues,
  tempCurFieldName,tempSignFieldName,
  tempFilterFieldNames,tempFilterFieldValues,
  tempStr1,tempStr2:string;
  tempFilterFieldArray,tempFilterFieldValueArray:TStringDynArray;

  i,j:LongInt;
  tempCurInt,tempAddInt:LongInt;
  tempFieldDef,tempFieldDef_Select:PABFieldDef;
  tempRecNo:LongInt;
begin
  if (AButtonVisibleIndex=0) and
     (Assigned(aField)) and
     (Assigned(aDataSource)) and
     (Assigned(aDataSource.DataSet)) and
     ((aDataSource.AutoEdit) or (ABCheckDataSetInEdit(aDataSource.DataSet)))   then
  begin
    tempSumFieldNames:=EmptyStr;

    ABSelectCheckComboBox(tempSignFieldName,aField.DataSet,'请选择设置排序的标识字段');
    if tempSignFieldName<>EmptyStr then
    begin
      tempFieldDef:=ABFieldDefByFieldName(aField.DataSet,aField.FieldName);
      if (Assigned(tempFieldDef)) then
      begin
        if  (tempFieldDef.Fi_ExtSetup<>EmptyStr)  then
        begin
          tempFilterFieldNames:=ABReadIniInText('TABcxDBFieldOrder','FilterFieldNames',tempFieldDef.Fi_ExtSetup);
          tempFilterFieldValues:=ABReadIniInText('TABcxDBFieldOrder','FilterFieldValues',tempFieldDef.Fi_ExtSetup);
          tempFilterFieldValues:=ABReplaceSqlFieldNameParams(tempFilterFieldValues,[aField.DataSet]);

          tempFilterFieldArray     :=ABStrToStringArray(tempFilterFieldNames);
          tempFilterFieldValueArray:=ABStrToStringArray(tempFilterFieldValues);
        end;
        tempSumFieldNames:=ABGetDatasetValue(  aField.DataSet,
                                               ABStrToStringArray(tempSignFieldName),
                                               tempFilterFieldArray,
                                               tempFilterFieldValueArray,
                                               ';');
        if tempSumFieldNames<>EmptyStr then
        begin
          //tempSumFieldNames翻译到显示名称
          if (abpos(';',tempSignFieldName)<=0) and (abpos(',',tempSignFieldName)<=0) then
          begin
            tempFieldDef_Select:=ABFieldDefByFieldName(aField.DataSet,tempSignFieldName);
            if (Assigned(tempFieldDef_Select.PDownDef)) and
               (Assigned(tempFieldDef_Select.PDownDef.Fi_DataSet)) and
               (AnsiCompareText(tempFieldDef_Select.PDownDef.Fi_SaveField,tempFieldDef_Select.PDownDef.Fi_ViewField)<>0) and
               (not tempFieldDef_Select.PDownDef.Fi_DataSet.IsEmpty) then
            begin
              tempSumFieldNamesBack:=emptystr;
              j:= ABGetSpaceStrCount(tempSumFieldNames, ';');
              for I := 1 to j do
              begin
                tempCurFieldName := ABGetSpaceStr(tempSumFieldNames, i, ';');
                if tempCurFieldName <> EmptyStr then
                begin
                  ABAddstr(tempSumFieldNamesBack,
                           VarToStrDef(tempFieldDef_Select.PDownDef.Fi_DataSet.Lookup(tempFieldDef_Select.PDownDef.Fi_SaveField,
                                                                   tempCurFieldName,ABStringReplace(tempFieldDef_Select.PDownDef.Fi_ViewField,',',';')),''),
                           ';');

                end;
              end;
              tempSumFieldNames:=tempSumFieldNamesBack;
            end;
          end;
          if ABOrderStrs(tempSumFieldNames,';') then
          begin
            //tempSumFieldNames翻译到保存值
            if (abpos(';',tempSignFieldName)<=0) and (abpos(',',tempSignFieldName)<=0) then
            begin
              tempFieldDef_Select:=ABFieldDefByFieldName(aField.DataSet,tempSignFieldName);
              if (Assigned(tempFieldDef_Select.PDownDef)) and (Assigned(tempFieldDef_Select.PDownDef.Fi_DataSet)) and
                 (AnsiCompareText(tempFieldDef_Select.PDownDef.Fi_SaveField,tempFieldDef_Select.PDownDef.Fi_ViewField)<>0) and
                 (not tempFieldDef_Select.PDownDef.Fi_DataSet.IsEmpty) then
              begin
                tempSumFieldNamesBack:=emptystr;
                j:= ABGetSpaceStrCount(tempSumFieldNames, ';');
                for I := 1 to j do
                begin
                  tempCurFieldName := ABGetSpaceStr(tempSumFieldNames, i, ';');
                  if tempCurFieldName <> EmptyStr then
                  begin
                    ABAddstr(tempSumFieldNamesBack,
                             VarToStrDef(tempFieldDef_Select.PDownDef.Fi_DataSet.Lookup(ABStringReplace(tempFieldDef_Select.PDownDef.Fi_ViewField,',',';'),
                                                                     tempCurFieldName,tempFieldDef_Select.PDownDef.Fi_SaveField),''),
                             ';');

                  end;
                end;
                tempSumFieldNames:=tempSumFieldNamesBack;
              end;
            end;

            tempStr1:='10';
            tempStr2:='10';
            if (ABInPutStr('序号的起始值',tempStr1)) and
               (ABInPutStr('序号的增量',tempStr2)) and
               (tempStr1<>EmptyStr) and
               (tempStr2<>EmptyStr) then
            begin
              tempCurInt:= StrToIntDef(tempStr1,10);
              tempAddInt:= StrToIntDef(tempStr2,10);

              tempRecNo :=aField.DataSet.RecNo;
              j:= ABGetSpaceStrCount(tempSumFieldNames, ';');

              ABPubManualProgressBar_ThreadU.ABRunProgressBar(tempCurProgress,j);
              aField.DataSet.DisableControls;
              try
                for I := 1 to j do
                begin
                  tempCurProgress:=tempCurProgress+1;

                  tempCurFieldName := ABGetSpaceStr(tempSumFieldNames, i, ';');
                  if tempCurFieldName <> EmptyStr then
                  begin
                    tempLocalFieldNames:=tempSignFieldName;
                    if tempFilterFieldNames<>EmptyStr then
                      tempLocalFieldNames:=tempSignFieldName+','+tempFilterFieldNames;

                    tempLocalFieldNames:=ABStringReplace(tempLocalFieldNames,',',';');

                    tempLocalFieldValues:=tempCurFieldName;
                    if tempFilterFieldValues<>EmptyStr then
                    tempLocalFieldValues:=tempCurFieldName+','+tempFilterFieldValues;

                    if aField.DataSet.Locate(tempLocalFieldNames,
                                                        ABStrToStringArray(tempLocalFieldValues),
                                                        []) then
                    begin
                      ABSetFieldValue(tempCurInt,aField,true);
                      tempCurInt:=tempCurInt+tempAddInt;
                    end;
                  end;
                end;

                ABSetDatasetRecno(aField.DataSet,tempRecNo);
              finally
                ABPubManualProgressBar_ThreadU.ABStopProgressBar;
                aField.DataSet.EnableControls;
              end;
            end;
          end;
        end;
      end;
    end;
  end;
end;

{ TABcxFieldOrder }

procedure TABcxFieldOrder.DoButtonClick(AButtonVisibleIndex: Integer);
var
  tempDataSource:TDataSource;
begin
  inherited;
  if (Assigned(InplaceParams.Position.Item)) and
     (not Properties.ReadOnly) then
  begin
    tempDataSource:=ABGetColumnDataSource(TcxCustomGridTableItem(InplaceParams.Position.Item));
    if (Assigned(tempDataSource)) and
       (Assigned(tempDataSource.DataSet)) and
       ((tempDataSource.AutoEdit) or (ABCheckDataSetInEdit(tempDataSource.DataSet))) then
    begin
      ABFieldOrder_ButtonClick(AButtonVisibleIndex,
                               ABGetColumnField(TcxCustomGridTableItem(InplaceParams.Position.Item)),
                               ABGetColumnDataSource(TcxCustomGridTableItem(InplaceParams.Position.Item)));
    end;
  end;
end;

{ TABcxDBExtLookupComboBox }
procedure TABcxDBExtLookupComboBox.DblClick;
begin
  inherited;
  DroppedDown:=not DroppedDown;
end;

procedure TABcxDBExtLookupComboBox.DoCloseUp;
var
  tempFieldDef:PABFieldDef;
  tempStr1:string;
begin
  inherited;
  if (Assigned(DataBinding.Field))  then
  begin
    tempFieldDef:=ABFieldDefByFieldName(DataBinding.DataSource.DataSet,DataBinding.Field.FieldName);
    if (Assigned(tempFieldDef))  then
    begin
      if (tempFieldDef.IsCanAppendTreeItemDown) and
         (AnsiCompareText(Text,ABcxLookupComboBoxAddNewItemStr)=0) then
      begin
        Text:=EmptyStr;
        if ABShowDownListItem(tempFieldDef.PDownDef.Fi_Tr_Code,tempFieldDef.PDownDef.Fi_SaveField,tempStr1) then
          Text:=tempStr1;
      end;
    end;
  end;
end;

procedure TABcxDBExtLookupComboBox.DoInitPopup;
begin
  inherited;
  ABComboBox_InitPopup(DataBinding.Field,self);
end;

procedure TABcxDBExtLookupComboBox.DoOnChange;
begin
  inherited;
end;

function TABcxDBExtLookupComboBox.GetInnerEditClass: TControlClass;
begin
  Result := TABcxCustomComboboxInnerEdit;
end;

class function TABcxDBExtLookupComboBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxExtLookupComboBoxProperties;
end;

procedure TABcxDBSQLFieldSelects.DoButtonClick(AButtonVisibleIndex: Integer);
begin
  inherited;
  if (not Properties.ReadOnly) and
     (Assigned(DataBinding.DataSource)) and
     (Assigned(DataBinding.DataSource.DataSet)) and
     ((DataBinding.DataSource.AutoEdit) or (ABCheckDataSetInEdit(DataBinding.DataSource.DataSet)))  then
    ABDownFieldNameSelects_ButtonClick(AButtonVisibleIndex,DataBinding.Field,DataBinding.DataSource);
end;

{ TABcxSQLFieldSelects }

procedure TABcxSQLFieldSelects.DoButtonClick(AButtonVisibleIndex: Integer);
var
  tempDataSource:TDataSource;
begin
  inherited;
  if (Assigned(InplaceParams.Position.Item)) and
     (not Properties.ReadOnly) then
  begin
    tempDataSource:=ABGetColumnDataSource(TcxCustomGridTableItem(InplaceParams.Position.Item));
    if (Assigned(tempDataSource)) and
       (Assigned(tempDataSource.DataSet)) and
       ((tempDataSource.AutoEdit) or (ABCheckDataSetInEdit(tempDataSource.DataSet))) then
    begin
      ABDownFieldNameSelects_ButtonClick(AButtonVisibleIndex,
                                     ABGetColumnField(TcxCustomGridTableItem(InplaceParams.Position.Item)),
                                     ABGetColumnDataSource(TcxCustomGridTableItem(InplaceParams.Position.Item)));
    end;
  end;
end;

class function TABcxSQLFieldSelects.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxSQLFieldSelectsProperties;
end;

{ TABcxSQLFieldSelectsProperties }

constructor TABcxSQLFieldSelectsProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxSQLFieldSelectsProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxSQLFieldSelects;
end;

{ TABcxFieldOrderProperties }

constructor TABcxFieldOrderProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxFieldOrderProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxFieldOrder;
end;

procedure TABcxDBFieldOrder.DoButtonClick(AButtonVisibleIndex: Integer);
begin
  inherited;
  if (not Properties.ReadOnly) and
     (Assigned(DataBinding.DataSource)) and
     (Assigned(DataBinding.DataSource.DataSet)) and
     ((DataBinding.DataSource.AutoEdit) or (ABCheckDataSetInEdit(DataBinding.DataSource.DataSet)))  then
    ABFieldOrder_ButtonClick(AButtonVisibleIndex,DataBinding.Field,DataBinding.DataSource);
end;

{ TABcxDBComboBox }

procedure TABcxDBComboBox.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

procedure TABcxDBComboBox.DoInitPopup;
begin
  inherited;
  ABComboBox_InitPopup(DataBinding.Field,self);
end;

class function TABcxDBComboBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxComboBoxProperties;
end;

{ TABcxDBTreeViewPopupEdit }

procedure TABcxDBTreeViewPopupEdit.DblClick;
begin
  inherited;
  DroppedDown:=not DroppedDown;
end;

procedure TABcxDBTreeViewPopupEdit.DoInitPopup;
begin
  inherited;
end;

class function TABcxDBTreeViewPopupEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxTreeViewPopupEditProperties;
end;

procedure TABcxDBTreeViewPopupEdit.SetupPopupWindow;
begin
  inherited;
  if (not (Assigned(Properties))) or
     (not (Properties.PopupControl is TABcxDBTreeView))  then
    exit;

  TABcxDBTreeView(Properties.PopupControl).LinkPopupEdit   :=Self;
  ABTreeViewPopupEdit_Locate(self);
end;

{ TABcxTreeViewPopupEditProperties }

constructor TABcxTreeViewPopupEditProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxTreeViewPopupEditProperties.GetContainerClass: TcxContainerClass;
begin
  result:=TABcxTreeViewPopupEdit;
end;

{ TABcxTreeViewPopupEdit }

procedure TABcxTreeViewPopupEdit.DblClick;
begin
  inherited;
  DroppedDown:=not DroppedDown;
end;

procedure TABcxTreeViewPopupEdit.DoInitPopup;
begin
  inherited;
end;


class function TABcxTreeViewPopupEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxTreeViewPopupEditProperties;
end;

procedure TABcxTreeViewPopupEdit.SetupPopupWindow;
begin
  inherited;
  if (not (Assigned(Properties))) or
     (not (Properties.PopupControl is TABcxDBTreeView))  then
    exit;

  TABcxDBTreeView(Properties.PopupControl).LinkPopupEdit   :=Self;
  ABTreeViewPopupEdit_Locate(self);
end;

{ TABcxDBCheckComboBox }

procedure TABcxDBCheckComboBox.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

procedure TABcxDBCheckComboBox.DoInitPopup;
begin
  inherited;
  ABComboBox_InitPopup(DataBinding.Field,self);
end;

class function TABcxDBCheckComboBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxCheckComboBoxProperties;
end;

{ TABcxCheckComboBoxProperties }

function TABcxCheckComboBoxProperties.CalculateEditValueByCheckStates(
  Sender: TObject; const ACheckStates: TcxCheckStates): TcxEditValue;
var
  I: Integer;
  tempStr,tempEditValue:string;

  tempDo:Boolean;
  tempField1:Tfield;
  tempFieldDef:PABFieldDef;
  tempDownDataset:TDataset;
begin
  tempDo:=true;
  tempField1:=nil;
  tempDownDataset:=nil;
  tempFieldDef:=nil;
  if Owner is TcxCustomGridTableItem then
    tempField1:=ABGetColumnField(TcxCustomGridTableItem(owner))
  else if Owner is TcxDBCheckComboBox then
    tempField1:=TcxDBCheckComboBox(owner).DataBinding.Field;

  if (not Assigned(tempField1)) or
     (not Assigned(tempField1.Dataset)) then
  begin
    tempDo:=false
  end;

  if tempDo then
  begin
    tempFieldDef:=ABFieldDefByFieldName(tempField1.Dataset,tempField1.FieldName);
    if (not Assigned(tempFieldDef)) or
       (not Assigned(tempFieldDef.PDownDef)) then
    begin
      tempDo:=false
    end;

    if tempDo then
    begin
      tempDownDataset:=tempFieldDef.PDownDef.Fi_DataSet;
      if (not Assigned(tempDownDataset)) or
         (not tempDownDataset.Active)  then
      begin
        tempDo:=false
      end;
    end;
  end;

  if not tempDo then
  begin
    Result:= inherited CalculateEditValueByCheckStates(Sender,ACheckStates);
  end
  else
  begin
    if Assigned(OnStatesToEditValue) then
      OnStatesToEditValue(Sender, ACheckStates, Result)
    else
    begin
      tempEditValue:=EmptyStr;
      tempDownDataset.DisableControls;
      try
        tempDownDataset.First;
        for I := 0 to Items.Count-1 do
        begin
          if (ACheckStates[i]=cbsChecked) then
          begin
            tempStr:=tempDownDataset.FieldByName(tempFieldDef.PDownDef.Fi_SaveField).AsString;
            if (tempStr<>EmptyStr) then
            begin
              ABAddstr(tempEditValue,tempStr,Delimiter);
            end;
          end;
          tempDownDataset.Next;
        end;
        Result:=tempEditValue;
      finally
        tempDownDataset.EnableControls;
      end;
    end;
  end;
end;

procedure TABcxCheckComboBoxProperties.CalculateCheckStatesByEditValue(Sender: TObject;
  const AEditValue: TcxEditValue; var ACheckStates: TcxCheckStates);
var
  I: Integer;
  tempStr,tempEditValue:string;

  tempDo:Boolean;
  tempField1:TField;
  tempFieldDef:PABFieldDef;
  tempDownDataset:TDataSet;
begin
  tempDo:=true;
  tempField1:=nil;
  tempDownDataset:=nil;
  tempFieldDef:=nil;
  if Owner is TcxCustomGridTableItem then
    tempField1:=ABGetColumnField(TcxCustomGridTableItem(owner))
  else if Owner is TcxDBCheckComboBox then
    tempField1:=TcxDBCheckComboBox(owner).DataBinding.Field;

  if (not Assigned(tempField1)) or
     (not Assigned(tempField1.Dataset)) then
  begin
    tempDo:=false
  end;

  if tempDo then
  begin
    tempFieldDef:=ABFieldDefByFieldName(tempField1.Dataset,tempField1.FieldName);
    if (not Assigned(tempFieldDef)) or
       (not Assigned(tempFieldDef.PDownDef)) then
    begin
      tempDo:=false
    end;

    if tempDo then
    begin
      tempDownDataset:=tempFieldDef.PDownDef.Fi_DataSet;
      if (not Assigned(tempDownDataset)) or
         (not tempDownDataset.Active)  then
      begin
        tempDo:=false
      end;
    end;
  end;

  if not tempDo then
  begin
    inherited;
  end
  else
  begin
    if Assigned(OnEditValueToStates) then
    begin
      SetLength(ACheckStates, Items.Count);
      OnEditValueToStates(Sender, AEditValue, ACheckStates)
    end
    else
    begin
      SetLength(ACheckStates, Items.Count);
      tempEditValue:=VarToStrDef(AEditValue,'');
      tempDownDataset.DisableControls;
      try
        tempDownDataset.First;
        for I := 0 to Items.Count-1 do
        begin
          tempStr:=tempDownDataset.FieldByName(tempFieldDef.PDownDef.Fi_SaveField).AsString;
          if (tempStr<>EmptyStr) and
             (ABPos(Delimiter+tempStr+Delimiter,Delimiter+tempEditValue+Delimiter)>0) then
          begin
            ACheckStates[i]:=cbsChecked
          end
          else
          begin
            ACheckStates[i]:=cbsUnChecked;
          end;
          tempDownDataset.Next;
        end;
      finally
        tempDownDataset.EnableControls;
      end;
    end;
  end;
end;

constructor TABcxCheckComboBoxProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxCheckComboBoxProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxCheckComboBox;
end;

{ TABcxDBPivotGrid }

constructor TABcxDBPivotGrid.Create(AOwner: TComponent);
begin
  inherited;
  OptionsView.ColumnFields:=False;
  OptionsView.DataFields:=False;
  OptionsView.FilterFields:=False;
  OptionsView.RowFields:=True;

  OptionsView.RowGrandTotalText:='总计';
  OptionsView.ColumnGrandTotalText:='总计';

  FExtPopupMenu:=TABcxPivotGridPopupMenu.Create(self); //所有者不能是AOwner,要不然就会显示在窗体中了
  FExtPopupMenu.SetSubComponent(True);
  PopupMenu:=FExtPopupMenu;
end;

destructor TABcxDBPivotGrid.Destroy;
begin
  FExtPopupMenu.Free;
  inherited;
end;

procedure TABcxDBPivotGrid.ExpandAll;
var
  i:LongInt;
begin
  for I := 0 to FieldCount - 1 do
    Fields[i].ExpandAll;
end;

procedure TABcxDBPivotGrid.CollapseALL;
var
  i:LongInt;
begin
  for I := 0 to FieldCount - 1 do
    Fields[i].CollapseAll;
end;

{ TABcxComboBoxProperties }

constructor TABcxComboBoxProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxComboBoxProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxComboBox;
end;

{ TABdxDBStatusBarDataLink }

procedure TABdxDBStatusBarDataLink.ActiveChanged;
begin
  inherited;
  TABdxDBStatusBar(FStatusBar).Refresh;
end;

procedure TABdxDBStatusBarDataLink.RecordChanged(Field: TField);
begin
  inherited;
  TABdxDBStatusBar(FStatusBar).Refresh;
end;

{ TABdxDBStatusBar }
procedure TABdxDBStatusBar.DoRefresh(aPanelIndex:LongInt;aDataSource:TDataSource);
var
  tempCurPanelText: string;
  I: Integer;
  tempRec, tempRecordCount: LongInt;
begin
  if (Panels[aPanelIndex].PanelStyleClassName = emptystr) then
  begin
    Panels[aPanelIndex].PanelStyleClassName := 'TdxStatusBarTextPanelStyle';
  end;

  if aDataSource.DataSet.Active then
  begin
    if (Panels.Items[aPanelIndex].Text = EmptyStr) or
       (ABPos('[', Panels.Items[aPanelIndex].Text) <= 0) and (ABPos(']', Panels.Items[aPanelIndex].Text) <= 0) then
      tempCurPanelText := '[RecNo]/[Count]'
    else
      tempCurPanelText := Panels.Items[aPanelIndex].Text;

    //替换引用的字段名
    for I := 0 to aDataSource.DataSet.FieldCount - 1 do
    begin
      if ABPos('[' + aDataSource.DataSet.Fields[i].FieldName + ']', tempCurPanelText) > 0 then
      begin
        tempCurPanelText := ABStringReplace(tempCurPanelText,
                                           '[' + aDataSource.DataSet.Fields[i].FieldName + ']',
                                            aDataSource.DataSet.Fields[i].DisplayText);
      end;
    end;

      {
    //优先表格记录号与记录数
    if (Assigned(aCxGrid)) and
       (Assigned(aCxGrid.ActiveView)) and
       (Assigned(aCxGrid.ActiveView.DataController)) then
    begin
      //在此时机 数据集的RecNo已改变,但FocusedRecordIndex还没有改变,所以暂时采用数据集的RecNo
      FocusedRowIndex->表示当前的行的索引号,是指数据GRID中的行,
      cxGrid1.ActiveView.DataController.Values[]中用的是记录的索引号,要用FocusedRecordIndex

      cxGrid1.ActiveView.DataController.recordcount是指所有记录的数量,包括过滤掉的
      cxGrid1.ActiveView.DataController.FilteredRecordCount是指过滤后的记录的数量,不包括过滤掉的


      tempRec := FCurcxGrid.ActiveView.DataController.FocusedRecordIndex;
      tempRecordCount := FCurcxGrid.ActiveView.DataController.RecordCount;

      if ABDatasetIsEmpty(aDataSource.DataSet) then
        tempRec := 0
      else
        tempRec := ABTrimInt(aDataSource.DataSet.RecNo,0,aDataSource.DataSet.RecNo);
      tempRecordCount := aDataSource.DataSet.RecordCount;
    end
    else
    begin
      if ABDatasetIsEmpty(aDataSource.DataSet) then
        tempRec := 0
      else
        tempRec := ABTrimInt(aDataSource.DataSet.RecNo,0,aDataSource.DataSet.RecNo);
      tempRecordCount := aDataSource.DataSet.RecordCount;
    end;
    }
    if ABDatasetIsEmpty(aDataSource.DataSet) then
      tempRec := 0
    else
      tempRec := ABTrimInt(aDataSource.DataSet.RecNo,0,aDataSource.DataSet.RecNo);
    tempRecordCount := aDataSource.DataSet.RecordCount;

    if ABPos('[RecNo]', tempCurPanelText) > 0 then
      tempCurPanelText := ABStringReplace(tempCurPanelText, '[RecNo]', IntToStr(tempRec));
    if ABPos('[Count]', tempCurPanelText) > 0 then
      tempCurPanelText := ABStringReplace(tempCurPanelText, '[Count]', IntToStr(tempRecordCount));
  end
  else
  begin
    tempCurPanelText := '';
  end;

  Panels[aPanelIndex].Text := tempCurPanelText;
end;

procedure TABdxDBStatusBar.Refresh;
var
  tempPanelIndex: Integer;
begin
  if (FStopUpdate) then
    exit;

  if (not Assigned(FDataLink.DataSource)) or
     (not Assigned(FDataLink.DataSource.DataSet)) or
     (FDataLink.DataSource.DataSet.ControlsDisabled) then
    exit;

  if (Panels.Count < 1) then
  begin
    Panels.Add;
  end;


  tempPanelIndex:=0;
  if Assigned(FOnRefresh) then
    FOnRefresh(Self,tempPanelIndex);

  DoRefresh(tempPanelIndex,DataSource);
end;

constructor TABdxDBStatusBar.Create(AOwner: TComponent);
begin
  inherited;
  FDataLink := TABdxDBStatusBarDataLink.Create;
  FDataLink.FStatusBar := self;

  PaintStyle:=stpsUseLookAndFeel;
end;

destructor TABdxDBStatusBar.Destroy;
begin
  FDataLink.Free;
  FDataLink := nil;
  inherited;
end;

function TABdxDBStatusBar.GetDataSource: TDataSource;
begin
  Result := FDataLink.DataSource;
end;

procedure TABdxDBStatusBar.SetDataSource(const Value: TDataSource);
begin
  if not (FDataLink.DataSourceFixed and (csLoading in ComponentState)) then
  begin
    FDataLink.DataSource := Value;
  end;
  if Value <> nil then
  begin
    Value.FreeNotification(Self);
  end;

  if Assigned(Value) then
    Refresh;
end;

procedure TABdxDBStatusBar.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if (Operation = opRemove) then
  begin
    if (Assigned(FDataLink)) and (FDataLink.DataSource =AComponent)   then
      FDataLink.DataSource:=nil;
  end;
end;

{ TABDBFieldCaption }

procedure TABDBFieldCaption.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited ;
  if (Operation = opRemove) and (FDataSource =AComponent)   then
    FDataSource:=nil;
end;

procedure TABDBFieldCaption.RefreshCaption;
begin
  if (Assigned(FDataSource)) and
     (Assigned(FDataSource.DataSet)) and
     (Assigned(FDataSource.DataSet.FindField(FDataField))) and
     (AnsiCompareText(Caption, FDataSource.DataSet.FindField(FDataField).DisplayLabel) <> 0) then
  begin
    Caption := FDataSource.DataSet.FindField(FDataField).DisplayLabel;
  end;
end;

{ TABcxDBImage }

procedure TABcxDBImage.CustomClick;
begin
  inherited;
  TABcxImageProperties(Properties).DoCustomClick;
end;

class function TABcxDBImage.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxImageProperties;
end;

{ TABcxPageControl }

procedure TABcxPageControl.AfterLoaded;
var
  tempOnChange: TNotifyEvent;
begin
  inherited;
  if PageCount>0 then
  begin
    tempOnChange:=OnChange;
    OnChange:=nil;
    try
      ActivePageIndex:=0;
    finally
      OnChange:=tempOnChange;
    end;
  end;
end;

constructor TABcxPageControl.Create(AOwner: TComponent);
begin
  inherited;
  LookAndFeel.Kind := lfFlat;
end;

{ TABcxGroupBox }

constructor TABcxGroupBox.Create(AOwner: TComponent);
begin
  inherited;
  if (ABIsPublishProp(self,'AlignWithMargins')) then
  begin
    ABSetPropValue(self,'AlignWithMargins',true);
  end;
end;


{ TABcxDBFieldSelects }

procedure TABcxDBFieldSelects.DoButtonClick(AButtonVisibleIndex: Integer);
begin
  inherited;
  if (not Properties.ReadOnly) and
     (Assigned(DataBinding.DataSource)) and
     (Assigned(DataBinding.DataSource.DataSet)) and
     ((DataBinding.DataSource.AutoEdit) or (ABCheckDataSetInEdit(DataBinding.DataSource.DataSet)))  then
    ABSelectFieldNamesToFieldValue(DataBinding.Field, AButtonVisibleIndex);
end;

class function TABcxDBFieldSelects.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxFieldSelectsProperties;
end;

{ TABcxDBDateTimeEdit }

class function TABcxDBDateTimeEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxDateTimeEditProperties;
end;

{ TABcxDBDateEdit }

class function TABcxDBDateEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxDateEditProperties;
end;

{ TABcxDateEditProperties }

constructor TABcxDateEditProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxDateEditProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxDateEdit;

end;

{ TABcxDateTimeEditProperties }

constructor TABcxDateTimeEditProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxDateTimeEditProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxDateTimeEdit;
end;

{ TABcxMemoProperties }

constructor TABcxMemoProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxMemoProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxMemo;
end;

{ TABcxComBoBoxMemoProperties }

constructor TABcxComBoBoxMemoProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxComBoBoxMemoProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxComBoBoxMemo;
end;

{ TABcxFieldSelectsProperties }

constructor TABcxFieldSelectsProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxFieldSelectsProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxFieldSelects;
end;

{ TABcxImageProperties }

constructor TABcxImageProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxImageProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxImage;
end;

procedure TABcxImageProperties.DoCustomClick;
var
  tempCanModify:Boolean;
  tempImage:TcxCustomImage;
begin
  tempCanModify:=false;
  tempImage:=nil;
  if Owner is TABcxImage then
  begin
    if TABcxImage(Owner).CanModify then
    begin
      tempCanModify:=True;
      tempImage:=TcxCustomImage(Owner)
    end;
  end
  else if Owner is TABcxDBImage then
  begin
    if  (Assigned(TABcxDBImage(Owner).DataBinding.Field)) and
        (Assigned(TABcxDBImage(Owner).DataBinding.Field.DataSet)) and
        ((TABcxDBImage(Owner).DataBinding.Field.CanModify) or
         (ABCheckDataSetInEdit(TABcxDBImage(Owner).DataBinding.Field.DataSet))
         ) and
        (TABcxDBImage(Owner).DataBinding.Field.DataSet.State in [dsedit, dsinsert]) then
    begin
      tempCanModify:=True;
      tempImage:=TcxCustomImage(Owner)
    end;
  end;

  if tempCanModify then
  begin
    if (Assigned(tempImage)) and
       (Assigned(tempImage.Picture)) and
       (Assigned(tempImage.Picture.Bitmap))  then
    begin
      ABChangPictureSize(tempImage.Picture.Bitmap,tempImage.Picture.Bitmap,
                         ABIIF((FZipWidth<=0) or (tempImage.Picture.Width<=0) or (tempImage.Picture.Height<=0),tempImage.Picture.Width,tempImage.Picture.Width/max(tempImage.Picture.Width/ABIIF(FZipWidth=0,1,FZipWidth),tempImage.Picture.Height/ABIIF(FZipHeight=0,1,FZipHeight))),
                         ABIIF((FZipHeight<=0) or (tempImage.Picture.Width<=0) or (tempImage.Picture.Height<=0),tempImage.Picture.Height,tempImage.Picture.Height/max(tempImage.Picture.Width/ABIIF(FZipWidth=0,1,FZipWidth),tempImage.Picture.Height/ABIIF(FZipHeight=0,1,FZipHeight)) ));
      tempImage.Invalidate;
      tempImage.Picture.Bitmap.SaveToStream(AfterZipStream);

      if Owner is TABcxDBImage then
        TBlobField(TABcxDBImage(Owner).DataBinding.Field).LoadFromStream(AfterZipStream);
    end;
  end
  else
  begin
    ABShow('请先设置数据为修改状态,才能压缩图片.');
  end;
end;

destructor TABcxImageProperties.Destroy;
begin
  if Assigned(FAfterZipStream) then
    FAfterZipStream.free;

  inherited;
end;

function TABcxImageProperties.GetAfterZipStream: TMemoryStream;
begin
  if not Assigned(FAfterZipStream) then
   FAfterZipStream:=TMemoryStream.create;

  result:=FAfterZipStream;
end;

{ TABcxSplitter }

constructor TABcxSplitter.Create(AOwner: TComponent);
begin
  inherited;

  HotZoneClassName := 'TcxMediaPlayer8Style';
  InvertDirection := true;


end;

{ TABcxButton }

procedure TABcxButton.Click;
  procedure AppendLog(aFlag:string);
  begin
    ABWriteLog('TABcxButton.Click('+aFlag+'):'+ABGetOwnerLongName(self));
  end;
var
  tempCaption: string;
begin
  if FShowProgressBar then
  begin
    tempCaption := FProgressBarCaption;
    if tempCaption = EmptyStr then
      tempCaption := '正在运行';

    ABPubAutoProgressBar_ThreadU.ABRunProgressBar(tempCaption);
    try
      AppendLog('Before');
      inherited;
      AppendLog('After');
    finally
      ABPubAutoProgressBar_ThreadU.ABStopProgressBar;
    end;
  end
  else
  begin
    ABDoBusy;
    try
      AppendLog('Before');
      inherited;
      AppendLog('After');
    finally
      ABDoBusy(false);
    end;
  end;
end;

constructor TABcxButton.Create(AOwner: TComponent);
begin
  inherited;
  LookAndFeel.Kind:=lfFlat;
end;

procedure TABcxButton.Loaded;
begin
  inherited;
end;

procedure TABcxButton.SetFuncPoints(const Value: string);
var
  tempName,tempValue:string;
begin
  FFuncPoints := Value;
  if (not (csDesigning in ComponentState)) and
     (Value<>EmptyStr)  then
  begin
    if Pos('=',Value)>0 then
    begin
      tempName:=ABGetLeftRightStr(Value,axdLeft);
      tempValue:=ABGetLeftRightStr(Value,axdRight);
    end
    else
    begin
      tempName:=Value;
      tempValue:=Value;
    end;
    if (not ABPubUser.IsAdminOrSysuser) and
       (not ABUser.OperatorKeyCustomObjectDataSet.Locate('Ti_Varchar1;TI_Code',VarArrayOf(['FuncPoint',tempName]),[]))  and
       (not ABUser.OperatorKeyCustomObjectDataSet.Locate('Ti_Varchar1;TI_Name',VarArrayOf(['FuncPoint',tempName]),[])) then
    begin
      Visible:=false;
    end;
  end;
end;

{ TABcxDBLookupComboBox }

procedure TABcxDBLookupComboBox.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

procedure TABcxDBLookupComboBox.DoCloseUp;
var
  tempFieldDef:PABFieldDef;
  tempStr1:string;
begin
  inherited;
  if (Assigned(DataBinding.Field))  then
  begin
    tempFieldDef:=ABFieldDefByFieldName(DataBinding.DataSource.DataSet,DataBinding.Field.FieldName);
    if (Assigned(tempFieldDef))  then
    begin
      if (tempFieldDef.IsCanAppendTreeItemDown) and
         (AnsiCompareText(Text,ABcxLookupComboBoxAddNewItemStr)=0) then
      begin
        Text:=EmptyStr;
        if ABShowDownListItem(tempFieldDef.PDownDef.Fi_Tr_Code,tempFieldDef.PDownDef.Fi_SaveField,tempStr1) then
          Text:=tempStr1;
      end;
    end;
  end;
end;

procedure TABcxDBLookupComboBox.DoInitPopup;
begin
  inherited;
  ABComboBox_InitPopup(DataBinding.Field,self);
end;

procedure TABcxDBLookupComboBox.DoOnChange;
begin
  inherited;
  ABLookupComboBox_Change(self);
end;

class function TABcxDBLookupComboBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxLookupComboBoxProperties;
end;

{ TABcxDBShellComboBox }

procedure TABcxDBShellComboBox.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

class function TABcxDBShellComboBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxShellComboBoxProperties;
end;

{ TABcxDBFontNameComboBox }

procedure TABcxDBFontNameComboBox.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

class function TABcxDBFontNameComboBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxFontNameComboBoxProperties;
end;

{ TABcxDBColorComboBox }

procedure TABcxDBColorComboBox.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

class function TABcxDBColorComboBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxColorComboBoxProperties;
end;

{ TABcxDBImageComboBox }

procedure TABcxDBImageComboBox.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

class function TABcxDBImageComboBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxImageComboBoxProperties;
end;

{ TABcxShellComboBox }

procedure TABcxShellComboBox.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

class function TABcxShellComboBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxShellComboBoxProperties;
end;

{ TABcxCheckComboBox }

procedure TABcxCheckComboBox.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

procedure TABcxCheckComboBox.DoInitPopup;
var
  tempColumn:TcxGridDBBandedColumn;
begin
  inherited;
  if Assigned(InplaceParams.Position.Item) then
  begin
    tempColumn:=TcxGridDBBandedColumn(InplaceParams.Position.Item);
    if Assigned(tempColumn) then
    begin
      ABComboBox_InitPopup(tempColumn.DataBinding.Field,tempColumn);
    end;
  end;
end;

class function TABcxCheckComboBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxCheckComboBoxProperties;
end;

{ TABcxFontNameComboBox }

procedure TABcxFontNameComboBox.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

class function TABcxFontNameComboBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxFontNameComboBoxProperties;
end;

{ TABcxColorComboBox }

procedure TABcxColorComboBox.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

class function TABcxColorComboBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxColorComboBoxProperties;
end;

{ TABcxLookupComboBox }

procedure TABcxLookupComboBox.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

procedure TABcxLookupComboBox.DoCloseUp;
var
  tempColumn:TcxGridDBBandedColumn;
  tempFieldDef:PABFieldDef;
  tempStr1:string;
begin
  inherited;
  if Assigned(InplaceParams.Position.Item) then
  begin
    tempColumn:=TcxGridDBBandedColumn(InplaceParams.Position.Item);
    if (Assigned(tempColumn)) and
       (Assigned(tempColumn.DataBinding)) and
       (Assigned(tempColumn.DataBinding.Field)) and
       (Assigned(tempColumn.DataBinding.Field.DataSet))
        then
    begin
      tempFieldDef:=ABFieldDefByFieldName(tempColumn.DataBinding.Field.DataSet,tempColumn.DataBinding.Field.FieldName);
      if (Assigned(tempFieldDef))  then
      begin
        if (tempFieldDef.IsCanAppendTreeItemDown) and
           (AnsiCompareText(Text,ABcxLookupComboBoxAddNewItemStr)=0) then
        begin
          Text:=EmptyStr;
          if ABShowDownListItem(tempFieldDef.PDownDef.Fi_Tr_Code,tempFieldDef.PDownDef.Fi_SaveField,tempStr1) then
            Text:=tempStr1;
        end;
      end;
    end;
  end;
end;

procedure TABcxLookupComboBox.DoInitPopup;
var
  tempColumn:TcxGridDBBandedColumn;
begin
  inherited;
  if Assigned(InplaceParams.Position.Item) then
  begin
    tempColumn:=TcxGridDBBandedColumn(InplaceParams.Position.Item);
    if Assigned(tempColumn) then
    begin
      ABComboBox_InitPopup(tempColumn.DataBinding.Field,tempColumn);
    end;
  end;
end;

procedure TABcxLookupComboBox.DoOnChange;
begin
  inherited;
  ABLookupComboBox_Change(self);
end;

class function TABcxLookupComboBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxLookupComboBoxProperties;
end;

{ TABcxImageComboBox }

procedure TABcxImageComboBox.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

class function TABcxImageComboBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxImageComboBoxProperties;
end;

{ TABcxCheckListBox }

constructor TABcxCheckListBox.Create(AOwner: TComponent);
begin
  inherited;
  EditValueFormat := cvfCaptions;
  ScrollBars:=ssBoth;
end;

{ TABcxDBCheckListBox }

constructor TABcxDBCheckListBox.Create(AOwner: TComponent);
begin
  inherited;
  EditValueFormat := cvfCaptions;
end;

{ TABcxDBTreeView }

constructor TABcxDBTreeView.Create(AOwner: TComponent);
begin
  inherited;

  FCanSelectParent:=true;
  DragMode := dmAutomatic;
  OptionsSelection.CellSelect:=false;

  FPopupMenu := TABcxDBTreeViewPopupMen.Create(self);
  FPopupMenu.SetSubComponent(True);
  PopupMenu := FPopupMenu;
end;

procedure TABcxDBTreeView.DblClick;
  function GetLinkDataSource:TDataSource;
  begin
    result:=nil;
    if (Assigned(FLinkPopupEdit)) then
    begin
      if FLinkPopupEdit is TABcxTreeViewPopupEdit then
      begin
        if (Assigned(FLinkPopupEdit.InplaceParams.Position.Item)) then
          result := ABGetColumnDataSource(TcxCustomGridTableItem(FLinkPopupEdit.InplaceParams.Position.Item));
      end
      else if FLinkPopupEdit is TABcxDBTreeViewPopupEdit then
      begin
        if (Assigned(TcxDBPopupEdit(FLinkPopupEdit).DataBinding)) and
           (Assigned(TcxDBPopupEdit(FLinkPopupEdit).DataBinding.Field)) then
          result := TcxDBPopupEdit(FLinkPopupEdit).DataBinding.DataSource;
      end;
    end;
  end;
var
  tempField: TField;
  tempDataSource:TDataSource;
begin
  inherited;
  if (Assigned(FLinkPopupEdit)) and
     (not FLinkPopupEdit.Properties.ReadOnly)
     then
  begin
    if (Assigned(DataController)) and
       (Assigned(DataController.DataSource)) and
       (Assigned(DataController.DataSource.DataSet))   then
    begin
      tempField:= DataController.DataSource.DataSet.FindField('IsParent');
      if (FCanSelectParent) or
         (Assigned(tempField)) and (
                                     (tempField.DataType=ftBoolean) and (not tempField.AsBoolean) or
                                     (tempField.AsInteger=0)
                                     ) or
         ((not Assigned(tempField)) and (Assigned(FocusedNode)) and (not FocusedNode.HasChildren)) then
      begin
        tempDataSource:=GetLinkDataSource;
        FLinkPopupEdit.DroppedDown := false;
        if   (Assigned(tempDataSource)) then
        begin
          if  (Assigned(tempDataSource.DataSet)) and
              ((tempDataSource.AutoEdit) or (ABCheckDataSetInEdit(tempDataSource.DataSet)))
               then
          begin
            tempField := nil;
            if FLinkPopupEdit is TABcxTreeViewPopupEdit then
            begin
              if (Assigned(FLinkPopupEdit.InplaceParams.Position.Item)) then
                tempField := ABGetColumnField(TcxCustomGridTableItem(FLinkPopupEdit.InplaceParams.Position.Item));
            end
            else if FLinkPopupEdit is TABcxDBTreeViewPopupEdit then
            begin
              if (Assigned(TcxDBPopupEdit(FLinkPopupEdit).DataBinding)) and
                 (Assigned(TcxDBPopupEdit(FLinkPopupEdit).DataBinding.Field)) then
                tempField := TcxDBPopupEdit(FLinkPopupEdit).DataBinding.Field;
            end;

            if (Assigned(tempField)) then
            begin
              ABSetFieldValue(DataController.DataSource.DataSet.FindField(DataController.KeyField).Value, tempField);
            end;
          end;
        end
        else
        begin
          if FLinkPopupEdit is TABcxTreeViewPopupEdit then
          begin
            TABcxTreeViewPopupEdit(FLinkPopupEdit).Text:=DataController.DataSource.DataSet.FindField(FPopupMenu.ListField).AsString;
          end;
        end;
      end;
    end;
  end;
end;

procedure TABcxDBTreeView.DoEndDrag(Target: TObject; X, Y: Integer);
begin
  inherited;
end;

procedure TABcxDBTreeView.DoEnter;
begin
  inherited;
  InitAutoPopupMenu;
end;

procedure TABcxDBTreeView.DoFocusedItemChanged(APrevFocusedItem,
  AFocusedItem: TcxCustomInplaceEditContainer);
begin
  inherited;
  OptionsBehavior.IncSearchItem := TcxTreeListColumn(AFocusedItem);
  if (Assigned(DataController.DataSource)) and
     (Assigned(DataController.DataSource.DataSet)) and
     ((DataController.DataSource.AutoEdit) or (ABCheckDataSetInEdit(DataController.DataSource.DataSet)))   then
  begin
    DragMode := dmAutomatic;
  end
  else
  begin
    DragMode := dmManual;
  end;
end;

procedure TABcxDBTreeView.DoStartDrag(var DragObject: TDragObject);
begin
  if (Assigned(DataController.DataSource)) and
     (Assigned(DataController.DataSource.DataSet)) and
     ((DataController.DataSource.AutoEdit) or (ABCheckDataSetInEdit(DataController.DataSource.DataSet)))   then
  begin
    inherited;
  end
  else
  begin
    Abort;
  end;
end;

procedure TABcxDBTreeView.DragOver(Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  inherited;
  Accept:=true;
end;

procedure TABcxDBTreeView.InitAutoPopupMenu;
begin
  if (Assigned(DataController.DataSource)) and
    (FPopupMenu.DataSource <> DataController.DataSource) then
    FPopupMenu.DataSource := DataController.DataSource;

  if (FPopupMenu.ParentField <> DataController.ParentField) then
    FPopupMenu.ParentField := DataController.ParentField;

  if (FPopupMenu.KeyField <> DataController.KeyField) then
    FPopupMenu.KeyField := DataController.KeyField;

  if (ColumnCount>0) and
     (FPopupMenu.ListField <> TcxDBTreeListColumn(Columns[0]).DataBinding.FieldName) then
    FPopupMenu.ListField := TcxDBTreeListColumn(Columns[0]).DataBinding.FieldName;
end;

procedure TABcxDBTreeView.KeyPress(var Key: Char);
begin
  inherited;
  if (Assigned(FLinkPopupEdit)) and (Key = #13) then
    DblClick;
   //SelectNext(ActiveControl,True,True);
end;

procedure TABcxDBTreeView.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited;
  if (Operation = opRemove) and (FDataSource =AComponent)   then
    FDataSource:=nil;
end;

procedure TABcxDBTreeView.SetActive(const Value: Boolean);
begin
  FActive := Value;
  if (FActive) and (Bands.Count = 0) and (ColumnCount = 0) then
  begin
    OptionsBehavior.ExpandOnIncSearch := true;
    OptionsBehavior.IncSearch := True;

    OptionsData.Inserting := False;
    OptionsData.Editing := False;
    OptionsData.Deleting := False;

    OptionsView.ColumnAutoWidth := True;

    Bands.Add;
    CreateColumn(Bands.Items[Bands.Count - 1]);
    Columns[ColumnCount - 1].Position.ColIndex := 0;
    Columns[ColumnCount - 1].Position.RowIndex := 0;
    Columns[ColumnCount - 1].Position.BandIndex := 0;
    OptionsBehavior.IncSearchItem := Columns[ColumnCount - 1];
    if Name <> emptystr then
    begin
      if (ColumnCount = 1) and (Columns[ColumnCount - 1].Name = emptystr) then
      begin
        Columns[ColumnCount - 1].Name := name + 'cxDBTreeListColumn1';
      end;
    end;
  end;
end;

procedure TABcxDBTreeView.SetExtFullExpand(const Value: Boolean);
begin
  FExtFullExpand := Value;
  if Value then
    inherited FullExpand
  else
    FullCollapse;
end;

procedure TABcxDBTreeView.SetLinkPopupEdit(const Value: TcxCustomPopupEdit);
begin
  if FLinkPopupEdit <> Value then
    FLinkPopupEdit := Value;
end;


{ TABcxComBoBoxMemo }

procedure TABcxComBoBoxMemo.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

procedure TABcxComBoBoxMemo.DoButtonClick(AButtonVisibleIndex: Integer);
var
  temptext:string;
begin
  inherited;
  if (AButtonVisibleIndex = 1) and
     (not Properties.ReadOnly) then
  begin
    temptext:=text;
    if ABShowEditMemo(temptext) then
    begin
      text:=temptext;
      if Properties.Items.IndexOf(Text)<0 then
        Properties.Items.Add(Text);

      if (Assigned(InplaceParams.Position.Item)) then
      begin
        ABSetFieldValue(Text,ABGetColumnField(TcxCustomGridTableItem(InplaceParams.Position.Item)));
      end;
    end;
  end;
end;

class function TABcxComBoBoxMemo.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxComBoBoxMemoProperties;
end;

{ TABcxDBComBoBoxMemo }

procedure TABcxDBComBoBoxMemo.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

procedure TABcxDBComBoBoxMemo.DoButtonClick(AButtonVisibleIndex: Integer);
var
  temptext:string;
begin
  inherited;
  if (AButtonVisibleIndex = 1) and
     (not Properties.ReadOnly) and
     (Assigned(DataBinding.DataSource)) and
     (Assigned(DataBinding.DataSource.DataSet)) and
     ((DataBinding.DataSource.AutoEdit) or (ABCheckDataSetInEdit(DataBinding.DataSource.DataSet)))   then
  begin
    temptext:=text;
    if ABShowEditMemo(temptext) then
    begin
      text:=temptext;
      if Properties.Items.IndexOf(Text)<0 then
        Properties.Items.Add(Text);

      ABSetFieldValue(Text,DataBinding.Field);
    end;
  end;
end;

class function TABcxDBComBoBoxMemo.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxComBoBoxMemoProperties;
end;

{ TABcxFieldSelects }

procedure TABcxFieldSelects.DoButtonClick(AButtonVisibleIndex: Integer);
var
  tempDataSource:TDataSource;
begin
  inherited;
  if (Assigned(InplaceParams.Position.Item)) and
     (not Properties.ReadOnly) then
  begin
    tempDataSource:=ABGetColumnDataSource(TcxCustomGridTableItem(InplaceParams.Position.Item));
    if (Assigned(tempDataSource)) and
       (Assigned(tempDataSource.DataSet)) and
       ((tempDataSource.AutoEdit) or (ABCheckDataSetInEdit(tempDataSource.DataSet))) then
    begin
      ABSelectFieldNamesToFieldValue(
                 ABGetColumnField(TcxCustomGridTableItem(InplaceParams.Position.Item)),
                 AButtonVisibleIndex);
    end;
  end;
end;

class function TABcxFieldSelects.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxFieldSelectsProperties;
end;

{ TABcxTimeEditProperties }

constructor TABcxTimeEditProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxTimeEditProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxTimeEdit;
end;


{ TABcxCheckBoxProperties }

constructor TABcxCheckBoxProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxCheckBoxProperties.GetContainerClass: TcxContainerClass;
begin
  result := TABcxCheckBox;
end;


{ TABcxDBCheckBox }

constructor TABcxDBCheckBox.Create(AOwner: TComponent);
begin
  inherited;
  Transparent := True;
end;

class function TABcxDBCheckBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxCheckBoxProperties;
end;

procedure TABcxDBCheckBox.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_DELETE) then  State := cbsGrayed;
end;

{ TABcxDBTreeViewPopupMen }

procedure TABcxDBTreeViewPopupMen.OutFile(aOutFileType:TABOutFileType);
var
  temSavedialog1:TSaveDialog;
  temFileName:string;
begin
  temSavedialog1:=TSaveDialog.Create(self);
  temSavedialog1.FileName:=TABcxDBTreeView(Owner).Owner.Name+'_'+TABcxDBTreeView(Owner).Name+'_'+ABDateToStr(Now);
  try
    if aOutFileType =otExcel then
      temSavedialog1.Filter:='*.xls|*.xls'
    else if aOutFileType =otXML then
      temSavedialog1.Filter:='*.xml|*.xml'
    else if aOutFileType =otText then
      temSavedialog1.Filter:='*.txt|*.txt'
    else if aOutFileType =otHtml then
      temSavedialog1.Filter:='*.Html|*.Html';

    if temSavedialog1.Execute then
    begin
      temFileName:=ExtractFileName(temSavedialog1.FileName);
      if aOutFileType =otExcel then
        cxExportTLToExcel(temFileName,TABcxDBTreeView(Owner),true,true)
      else if aOutFileType =otXML then
        cxExportTLToXML(temFileName,TABcxDBTreeView(Owner),true,true)
      else if aOutFileType =otText then
        cxExportTLToTEXT(temFileName,TABcxDBTreeView(Owner),true,true)
      else if aOutFileType =otHtml then
        cxExportTLToHTML(temFileName,TABcxDBTreeView(Owner),true,true);
    end;
  finally
    temSavedialog1.free;
  end;
end;

procedure TABcxDBTreeViewPopupMen.PopupMenu_OutExcel(Sender: TObject);
begin
  OutFile(otExcel);
end;

procedure TABcxDBTreeViewPopupMen.PopupMenu_OutHtml(Sender: TObject);
begin
  OutFile(otHtml);
end;

procedure TABcxDBTreeViewPopupMen.PopupMenu_OutText(Sender: TObject);
begin
  OutFile(otText);
end;

procedure TABcxDBTreeViewPopupMen.PopupMenu_OutXml(Sender: TObject);
begin
  OutFile(otXML);
end;

procedure TABcxDBTreeViewPopupMen.CreatePopupMenu ;
var
  tempMenuItem: TMenuItem;
begin
  inherited;
  if csDesigning in ComponentState then exit;

  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Caption:='-';
  Items.Add(tempMenuItem);

  tempMenuItem := TMenuItem.Create(self);
  tempMenuItem.Name := 'PopupMenu_Search';
  tempMenuItem.Caption := ('全文检索');
  tempMenuItem.OnClick := PopupMenu_Search;
  Items.Add(tempMenuItem);

  tempMenuItem := TMenuItem.Create(self);
  tempMenuItem.Name := 'PopupMenu_SaveSetup';
  tempMenuItem.Caption := ('保存设置');
  tempMenuItem.OnClick := PopupMenu_SaveSetup;
  Items.Add(tempMenuItem);

  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Caption:='-';
  Items.Add(tempMenuItem);

  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Caption:=('输出到文件');
  Items.Add(tempMenuItem);

  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Name:='PopupMenu_Excel';
  tempMenuItem.Caption:='Excel';
  tempMenuItem.OnClick:=PopupMenu_OutExcel;
  Items[Items.Count-1].Add(tempMenuItem);

  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Name:='PopupMenu_XML';
  tempMenuItem.Caption:='XML';
  tempMenuItem.OnClick:=PopupMenu_OutXML;
  Items[Items.Count-1].Add(tempMenuItem);

  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Name:='PopupMenu_Text';
  tempMenuItem.Caption:='Text';
  tempMenuItem.OnClick:=PopupMenu_OutText;
  Items[Items.Count-1].Add(tempMenuItem);

  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Name:='PopupMenu_Html';
  tempMenuItem.Caption:='Html';
  tempMenuItem.OnClick:=PopupMenu_OutHtml;
  Items[Items.Count-1].Add(tempMenuItem);
end;

procedure TABcxDBTreeViewPopupMen.DoPopup(Sender: TObject);
begin
  inherited;
end;

procedure TABcxDBTreeViewPopupMen.PopupMenu_Search(Sender: TObject);
begin
  ABcxDataControllerAllLocal(TABcxDBTreeView(Owner));
end;

function TABcxDBTreeViewPopupMen.GetFilename(aTypeFlag:string):string;
begin
  if FSetupFullFileName<>EmptyStr then
  begin
    Result:=FSetupFullFileName+aTypeFlag;
  end
  else
  begin
    Result:=ABConfigPath+TABcxDBTreeView(Owner).Owner.Name+'_'+TABcxDBTreeView(Owner).Name+FSetupFileNameSuffix +aTypeFlag;
  end;
end;

procedure TABcxDBTreeViewPopupMen.PopupMenu_SaveSetup(Sender: TObject);
var
  tempStr1:string;
begin
  tempStr1:=GetFilename('_Property.ini');
  if ABCheckFileExists(tempStr1) then
  begin
    DeleteFile(tempStr1);
  end;
  TABcxDBTreeView(Owner).StoreToIniFile(tempStr1,True);
end;

procedure TABcxDBTreeViewPopupMen.LoadViewProperty;
var
  tempStr1:string;
begin
  tempStr1:=GetFilename('_Property.ini');
  if ABCheckFileExists(tempStr1) then
  begin
    if (TABcxDBTreeView(Owner).ColumnCount<=0) then
      TABcxDBTreeView(Owner).CreateAllItems;
    TABcxDBTreeView(Owner).BeginUpdate;
    try
      //AChildrenCreating=TRUE时会将INI有的而FLinkTableView没有列创建出来
      //此处使用AChildrenCreating=TRUE是因为要自动创建计算列的关系
      //AChildrenDeleting=TRUE时会将INI没有的而FLinkTableView有列删除
      TABcxDBTreeView(Owner).RestoreFromIniFile(tempStr1,True,false);
    finally
      TABcxDBTreeView(Owner).EndUpdate;
    end;
  end
  else
  begin
    if (TABcxDBTreeView(Owner).ColumnCount<=0) then
    begin
      TABcxDBTreeView(Owner).CreateAllItems;
    end;
  end;
end;

{ TABcxDBTimeEdit }

class function TABcxDBTimeEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxTimeEditProperties;
end;

procedure TABcxDBTimeEdit.PrepareEditValue(const ADisplayValue: TcxEditValue;
  out EditValue: TcxEditValue; AEditFocused: Boolean);
begin
  //inherited;
  EditValue:= ADisplayValue;
end;

{ TABcxDBRadioGroup }

constructor TABcxDBRadioGroup.Create(AOwner: TComponent);
begin
  inherited;
  Caption := EmptyStr;
  Alignment := alCenterCenter;
end;

class function TABcxDBRadioGroup.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxRadioGroupProperties;
end;

{ TABcxRadioGroupProperties }

constructor TABcxRadioGroupProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxRadioGroupProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxRadioGroup;
end;

{ TABcxComboBox }

procedure TABcxComboBox.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

procedure TABcxComboBox.DoInitPopup;
var
  tempColumn:TcxGridDBBandedColumn;
begin
  inherited;
  if Assigned(InplaceParams.Position.Item) then
  begin
    tempColumn:=TcxGridDBBandedColumn(InplaceParams.Position.Item);
    if Assigned(tempColumn) then
    begin
      ABComboBox_InitPopup(tempColumn.DataBinding.Field,tempColumn);
    end;
  end;
end;

class function TABcxComboBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxComboBoxProperties;
end;

{ TABcxCheckBox }

constructor TABcxCheckBox.Create(AOwner: TComponent);
begin
  inherited;
  Transparent := True;
  State:=cbsGrayed;
end;

class function TABcxCheckBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxCheckBoxProperties;
end;

procedure TABcxCheckBox.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_DELETE) then  State := cbsGrayed;
end;

{ TABcxLabel }

constructor TABcxLabel.Create(AOwner: TComponent);
begin
  inherited;
  Transparent := True;
  //ABDBPanel.AutoSize:=True; 时在运行状态设置.Properties.Alignment.Vert时会奇怪地将ABDBPanel.height值修改
  Properties.Alignment.Vert:=taVCenter;

  Style.Font.Name:=('宋体');
  Style.Font.Size:=11;
  Style.Font.Height:=-14;
end;

class function TABcxLabel.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxLabelProperties;
end;

{ TABcxDateTimeEdit }

class function TABcxDateTimeEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxDateTimeEditProperties;
end;

{ TABcxDateEdit }

class function TABcxDateEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxDateEditProperties;
end;

{ TABcxListBox }

constructor TABcxListBox.Create(AOwner: TComponent);
begin
  inherited;
  ScrollBars:=ssBoth;
  FRemarks:= TStringList.Create;
end;

destructor TABcxListBox.Destroy;
begin
  FRemarks.Free;
  inherited;
end;

function TABcxListBox.GetCurRemark(aIndex:LongInt): string;
var
  tempItem:string;
begin
  Result:=EmptyStr;
  if aIndex>=0 then
  begin
    tempItem:=trim(Items[aIndex]);
    if AnsiCompareText(Copy(tempItem,1,1),'[')<>0 then
    begin
      tempItem:='['+tempItem+']';
    end;
    Result:=ABGetItemValue(FRemarks.Text,tempItem,'','[');
  end;
end;

function TABcxListBox.GetRemarks: TStrings;
begin
  Result:=FRemarks;
end;

procedure TABcxListBox.SetRemarks(const Value: TStrings);
begin
  FRemarks.Assign(Value);
end;

{ TABcxTreeView }

procedure TABcxTreeView.SetExtFullExpand(const Value: Boolean);
begin
  FExtFullExpand := Value;
  if FExtFullExpand then
    FullExpand
  else
    FullCollapse;
end;

{ TABcxTimeEdit }

class function TABcxTimeEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxTimeEditProperties;
end;

procedure TABcxTimeEdit.PrepareEditValue(const ADisplayValue: TcxEditValue;
  out EditValue: TcxEditValue; AEditFocused: Boolean);
begin
  //inherited;
  EditValue:= ADisplayValue;
end;

{ TABcxImage }

procedure TABcxImage.CustomClick;
begin
  inherited;
  TABcxImageProperties(Properties).DoCustomClick;
end;

class function TABcxImage.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxImageProperties;
end;

{ TABdxStatusBar }

constructor TABdxStatusBar.Create(AOwner: TComponent);
begin
  inherited;
  PaintStyle:=stpsUseLookAndFeel;
end;

{ TABcxDBDirSelect }

constructor TABcxDBDirSelect.Create(AOwner: TComponent);
begin
  inherited;
  if FBegPath=emptystr then
    FBegPath:=ABAppPath;
end;

procedure TABcxDBDirSelect.DoButtonClick(AButtonVisibleIndex: Integer);
var
  tempStr1: string;
begin
  inherited;
  if (AButtonVisibleIndex = 0) and
     (not Properties.ReadOnly) and
     (Assigned(DataBinding.DataSource)) and
     (Assigned(DataBinding.DataSource.DataSet)) and
     ((DataBinding.DataSource.AutoEdit) or (ABCheckDataSetInEdit(DataBinding.DataSource.DataSet)))   then
  begin
    tempStr1:=ABSelectDirectory(FBegPath+DataBinding.Field.AsString);
    if (tempStr1 <> EmptyStr) then
    begin
      tempStr1:=ABStringReplace(tempStr1,FBegPath,'');
      if (tempStr1 <> DataBinding.Field.AsString) then
      begin
        ABSetFieldValue(tempStr1, DataBinding.Field);
      end;
    end;
  end;
end;

class function TABcxDBDirSelect.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxDirSelectProperties;
end;

procedure TABcxDBDirSelect.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;
  ABDeleteItemONKeyDown(Properties.Items,Text,Key,Shift);
end;

{ TABcxDirSelect }

constructor TABcxDirSelect.Create(AOwner: TComponent);
begin
  inherited;
  if FBegPath=emptystr then
    FBegPath:=ABAppPath;
end;

procedure TABcxDirSelect.DoButtonClick(AButtonVisibleIndex: Integer);
var
  temptext:string;
  tempDataSource:TDataSource;
begin
  inherited;
  if (AButtonVisibleIndex = 1) and
     (not Properties.ReadOnly) then
  begin
    temptext:=ABSelectDirectory(FBegPath+vartostrdef(EditValue,''));
    if (temptext <> EmptyStr) then
    begin
      temptext:=ABStringReplace(temptext,FBegPath,'');
      if (temptext <> vartostrdef(EditValue,'')) then
      begin
        EditValue:=temptext;
        if Properties.Items.IndexOf(EditValue)<0 then
          Properties.Items.Add(EditValue);

        if (Assigned(InplaceParams.Position.Item)) then
        begin
          tempDataSource:=ABGetColumnDataSource(TcxCustomGridTableItem(InplaceParams.Position.Item));
          if (Assigned(tempDataSource)) and
             (Assigned(tempDataSource.DataSet)) and
             ((tempDataSource.AutoEdit) or (ABCheckDataSetInEdit(tempDataSource.DataSet)))   then
          begin
            ABSetFieldValue(EditValue,ABGetColumnField(TcxCustomGridTableItem(InplaceParams.Position.Item)));
          end;
        end;
      end;
    end;
  end;
end;

class function TABcxDirSelect.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxDirSelectProperties;
end;

procedure TABcxDirSelect.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;
  ABDeleteItemONKeyDown(Properties.Items,Text,Key,Shift);
end;

{ TABcxDirSelectProperties }

constructor TABcxDirSelectProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxDirSelectProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxDirSelect;
end;

{ TABcxDBFileSelect }

constructor TABcxDBFileSelect.Create(AOwner: TComponent);
begin
  inherited;
  if FBegPath=emptystr then
    FBegPath:=ABAppPath;
end;

procedure TABcxDBFileSelect.DoButtonClick(AButtonVisibleIndex: Integer);
var
  tempStr1: string;
begin
  inherited;
  if (AButtonVisibleIndex = 0) and
     (not Properties.ReadOnly) and
     (Assigned(DataBinding.DataSource)) and
     (Assigned(DataBinding.DataSource.DataSet)) and
     ((DataBinding.DataSource.AutoEdit) or (ABCheckDataSetInEdit(DataBinding.DataSource.DataSet)))   then
  begin
    tempStr1:=ABSelectFile(FBegPath,DataBinding.Field.AsString);
    if (tempStr1 <> EmptyStr) then
    begin
      tempStr1:=ABStringReplace(tempStr1,FBegPath,'');
      if (tempStr1 <> DataBinding.Field.AsString) then
      begin
        ABSetFieldValue(tempStr1, DataBinding.Field);
      end;
    end;
  end;
end;

class function TABcxDBFileSelect.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxFileSelectProperties;
end;

procedure TABcxDBFileSelect.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;
  ABDeleteItemONKeyDown(Properties.Items,Text,Key,Shift);
end;

{ TABcxFileSelect }

constructor TABcxFileSelect.Create(AOwner: TComponent);
begin
  inherited;
  if FBegPath=emptystr then
    FBegPath:=ABAppPath;
end;

procedure TABcxFileSelect.DoButtonClick(AButtonVisibleIndex: Integer);
var
  temptext:string;
  tempDataSource:TDataSource;
begin
  inherited;
  if (AButtonVisibleIndex = 1) and
     (not Properties.ReadOnly) then
  begin
    temptext:=ABSelectFile(FBegPath,vartostrdef(EditValue,''));
    if (temptext <> EmptyStr) then
    begin
      temptext:=ABStringReplace(temptext,FBegPath,'');
      if (temptext <> vartostrdef(EditValue,'')) then
      begin
        EditValue:=temptext;
        if Properties.Items.IndexOf(EditValue)<0 then
          Properties.Items.Add(EditValue);

        if (Assigned(InplaceParams.Position.Item)) then
        begin
          tempDataSource:=ABGetColumnDataSource(TcxCustomGridTableItem(InplaceParams.Position.Item));
          if (Assigned(tempDataSource)) and
             (Assigned(tempDataSource.DataSet)) and
             ((tempDataSource.AutoEdit) or (ABCheckDataSetInEdit(tempDataSource.DataSet)))   then
          begin
            ABSetFieldValue(EditValue,ABGetColumnField(TcxCustomGridTableItem(InplaceParams.Position.Item)));
          end;
        end;
      end;
    end;
  end;
end;


class function TABcxFileSelect.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxFileSelectProperties;
end;

procedure TABcxFileSelect.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited;
  ABDeleteItemONKeyDown(Properties.Items,Text,Key,Shift);
end;

{ TABcxExtLookupComboBox }

procedure TABcxExtLookupComboBox.DoOnChange;
begin
  inherited;
  ABLookupComboBox_Change(self);
end;

function TABcxExtLookupComboBox.GetInnerEditClass: TControlClass;
begin
  Result := TABcxCustomComboboxInnerEdit;
end;

class function TABcxExtLookupComboBox.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxExtLookupComboBoxProperties;
end;

procedure TABcxExtLookupComboBox.DblClick;
begin
  inherited;
  DroppedDown:=not DroppedDown;
end;

procedure TABcxExtLookupComboBox.DoCloseUp;
var
  tempColumn:TcxGridDBBandedColumn;
  tempFieldDef:PABFieldDef;
  tempStr1:string;
begin
  inherited;
  if Assigned(InplaceParams.Position.Item) then
  begin
    tempColumn:=TcxGridDBBandedColumn(InplaceParams.Position.Item);
    if (Assigned(tempColumn)) and
       (Assigned(tempColumn.DataBinding)) and
       (Assigned(tempColumn.DataBinding.Field)) and
       (Assigned(tempColumn.DataBinding.Field.DataSet))
        then
    begin
      tempFieldDef:=ABFieldDefByFieldName(tempColumn.DataBinding.Field.DataSet,tempColumn.DataBinding.Field.FieldName);
      if (Assigned(tempFieldDef))  then
      begin
        if (tempFieldDef.IsCanAppendTreeItemDown) and
           (AnsiCompareText(Text,ABcxLookupComboBoxAddNewItemStr)=0) then
        begin
          Text:=EmptyStr;
          if ABShowDownListItem(tempFieldDef.PDownDef.Fi_Tr_Code,tempFieldDef.PDownDef.Fi_SaveField,tempStr1) then
            Text:=tempStr1;
        end;
      end;
    end;
  end;
end;

procedure TABcxExtLookupComboBox.DoInitPopup;
var
  tempColumn:TcxGridDBBandedColumn;
begin
  inherited;
  if Assigned(InplaceParams.Position.Item) then
  begin
    tempColumn:=TcxGridDBBandedColumn(InplaceParams.Position.Item);
    if Assigned(tempColumn) then
    begin
      ABComboBox_InitPopup(tempColumn.DataBinding.Field,tempColumn);
    end;
  end;
end;

{ TABcxExtLookupComboBoxProperties }

constructor TABcxExtLookupComboBoxProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxExtLookupComboBoxProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxExtLookupComboBox;
end;

function TABcxExtLookupComboBoxProperties.GetDisplayLookupText(
  const AKey: TcxEditValue): string;
begin
  Result := inherited GetDisplayLookupText(AKey);
  if FDisplayField <> EmptyStr then
  begin
    Result := ABGetDisplayLookupText(DataController,FDisplayField);
  end;
end;

{ TABcxLookupComboBoxProperties }

constructor TABcxLookupComboBoxProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxLookupComboBoxProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxLookupComboBox;
end;

function TABcxLookupComboBoxProperties.GetDisplayLookupText(
  const AKey: TcxEditValue): string;
begin
  Result := inherited GetDisplayLookupText(AKey);
  if FDisplayField <> EmptyStr then
  begin
    Result := ABGetDisplayLookupText(DataController,FDisplayField);
  end;
end;

{ TABcxFileSelectProperties }

constructor TABcxFileSelectProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxFileSelectProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxFileSelect;
end;

{ TABcxRadioGroup }

class function TABcxRadioGroup.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxRadioGroupProperties;
end;

{ TABcxMemo }

class function TABcxMemo.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxMemoProperties;
end;

{ TABcxDBMemo }

class function TABcxDBMemo.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxMemoProperties;
end;

class function TABcxDBSQLFieldSelects.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxSQLFieldSelectsProperties;
end;

class function TABcxFieldOrder.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxFieldOrderProperties;
end;

class function TABcxDBFieldOrder.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxFieldOrderProperties;
end;


{ TABcxTextEditProperties }

class function TABcxTextEditProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxTextEdit;
end;

{ TABcxMaskEditProperties }

class function TABcxMaskEditProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxMaskEdit;
end;

{ TABcxButtonEditProperties }

class function TABcxButtonEditProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxButtonEdit;
end;

{ TABcxCalcEditProperties }

class function TABcxCalcEditProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxCalcEdit;
end;

{ TABcxCurrencyEditProperties }

class function TABcxCurrencyEditProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxCurrencyEdit;
end;

{ TABcxBlobEditProperties }

class function TABcxBlobEditProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxBlobEdit;
end;

{ TABcxMRUEditProperties }

class function TABcxMRUEditProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxMRUEdit;
end;

{ TABcxProgressBarProperties }

class function TABcxProgressBarProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxProgressBar;
end;

{ TABcxTrackBarProperties }

class function TABcxTrackBarProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxTrackBar;
end;

{ TABcxRichEditProperties }

class function TABcxRichEditProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxRichEdit;
end;

{ TABcxPopupEditProperties }

class function TABcxPopupEditProperties.GetContainerClass: TcxContainerClass;
begin
  Result :=TABcxPopupEdit ;
end;

{ TABcxCheckGroupProperties }

class function TABcxCheckGroupProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxCheckGroup;
end;

{ TABcxLabelProperties }

class function TABcxLabelProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxLabel;
end;

{ TABcxImageComboBoxProperties }

class function TABcxImageComboBoxProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxImageComboBox;
end;

{ TABcxColorComboBoxProperties }

class function TABcxColorComboBoxProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxColorComboBox;
end;

{ TABcxFontNameComboBoxProperties }

class function TABcxFontNameComboBoxProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxFontNameComboBox;
end;

{ TABcxShellComboBoxProperties }

class function TABcxShellComboBoxProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxShellComboBox;
end;

{ TABcxTextEdit }
class function TABcxTextEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxTextEditProperties;
end;

{ TABcxDBTextEdit }

class function TABcxDBTextEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxTextEditProperties;
end;

{ TABcxMaskEdit }

class function TABcxMaskEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxMaskEditProperties;
end;

{ TABcxDBMaskEdit }

class function TABcxDBMaskEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxMaskEditProperties;
end;

{ TABcxButtonEdit }

class function TABcxButtonEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxButtonEditProperties;
end;

{ TABcxDBButtonEdit }

class function TABcxDBButtonEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxButtonEditProperties;
end;

{ TABcxSpinEdit }

class function TABcxSpinEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxSpinEditProperties;
end;

{ TABcxDBSpinEdit }

class function TABcxDBSpinEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxSpinEditProperties;
end;

{ TABcxCalcEdit }

class function TABcxCalcEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxCalcEditProperties;
end;

{ TABcxDBCalcEdit }

class function TABcxDBCalcEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxCalcEditProperties;
end;

{ TABcxHyperLinkEdit }

class function TABcxHyperLinkEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxHyperLinkEditProperties;
end;

{ TABcxDBHyperLinkEdit }

class function TABcxDBHyperLinkEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxHyperLinkEditProperties;
end;

{ TABcxCurrencyEdit }

class function TABcxCurrencyEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxCurrencyEditProperties;
end;

{ TABcxDBCurrencyEdit }

class function TABcxDBCurrencyEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxCurrencyEditProperties;
end;

{ TABcxBlobEdit }

class function TABcxBlobEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxBlobEditProperties;
end;

{ TABcxDBBlobEdit }

class function TABcxDBBlobEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxBlobEditProperties;
end;

{ TABcxMRUEdit }

class function TABcxMRUEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxMRUEditProperties;
end;

{ TABcxDBMRUEdit }

class function TABcxDBMRUEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxMRUEditProperties;
end;

{ TABcxProgressBar }

class function TABcxProgressBar.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxProgressBarProperties;
end;

{ TABcxDBProgressBar }

class function TABcxDBProgressBar.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxProgressBarProperties;
end;

{ TABcxTrackBar }

class function TABcxTrackBar.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxTrackBarProperties;
end;

{ TABcxDBTrackBar }

class function TABcxDBTrackBar.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxTrackBarProperties;
end;

{ TABcxRichEdit }

class function TABcxRichEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxRichEditProperties;
end;

{ TABcxDBRichEdit }

class function TABcxDBRichEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxRichEditProperties;
end;

{ TABcxPopupEdit }

class function TABcxPopupEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxPopupEditProperties;
end;

{ TABcxDBPopupEdit }

class function TABcxDBPopupEdit.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxPopupEditProperties;
end;

{ TABcxCheckGroup }

class function TABcxCheckGroup.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxCheckGroupProperties;
end;

{ TABcxDBCheckGroup }

class function TABcxDBCheckGroup.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxCheckGroupProperties;
end;

{ TABcxDBLabel }

class function TABcxDBLabel.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxLabelProperties;
end;

{ TABcxSpinEditProperties }

class function TABcxSpinEditProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxSpinEdit;
end;

{ TABcxHyperLinkEditProperties }

class function TABcxHyperLinkEditProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxHyperLinkEdit;
end;


{ TABcxPrintSelectProperties }

constructor TABcxPrintSelectProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
end;

class function TABcxPrintSelectProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxPrintSelect;
end;

{ TABcxPrintSelect }

procedure TABcxPrintSelect.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

procedure TABcxPrintSelect.DoInitPopup;
var
  tempColumn:TcxGridDBBandedColumn;
begin
  inherited;
  if Assigned(InplaceParams.Position.Item) then
  begin
    tempColumn:=TcxGridDBBandedColumn(InplaceParams.Position.Item);
    if Assigned(tempColumn) then
    begin
      ABComboBox_InitPopup(tempColumn.DataBinding.Field,tempColumn);
    end;
  end;
end;

class function TABcxPrintSelect.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxPrintSelectProperties;
end;

{ TABcxDBPrintSelect }

procedure TABcxDBPrintSelect.DblClick;
begin
  inherited;
  DroppedDown := not DroppedDown;
end;

procedure TABcxDBPrintSelect.DoInitPopup;
begin
  inherited;
  ABComboBox_InitPopup(DataBinding.Field,self);
end;

class function TABcxDBPrintSelect.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxPrintSelectProperties;
end;

procedure ABFinalization;
begin
  ABFreePubDownEdit;
  if Assigned(FABDownManagerForm) then
    FABDownManagerForm.free;

  GetRegisteredEditProperties.Unregister(TABcxTimeEditProperties);
  GetRegisteredEditProperties.Unregister(TABcxDateEditProperties);
  GetRegisteredEditProperties.Unregister(TABcxDateTimeEditProperties);
  GetRegisteredEditProperties.Unregister(TABcxRadioGroupProperties);
  GetRegisteredEditProperties.Unregister(TABcxCheckBoxProperties);
  GetRegisteredEditProperties.Unregister(TABcxMemoProperties);
  GetRegisteredEditProperties.Unregister(TABcxComBoBoxMemoProperties);
  GetRegisteredEditProperties.Unregister(TABcxDirSelectProperties);
  GetRegisteredEditProperties.Unregister(TABcxFileSelectProperties);
  GetRegisteredEditProperties.Unregister(TABcxFieldSelectsProperties);
  GetRegisteredEditProperties.Unregister(TABcxSQLFieldSelectsProperties);
  GetRegisteredEditProperties.Unregister(TABcxFileBlobFieldProperties);
  GetRegisteredEditProperties.Unregister(TABcxPrintSelectProperties);
  GetRegisteredEditProperties.Unregister(TABcxFieldOrderProperties);
  GetRegisteredEditProperties.Unregister(TABcxLookupComboBoxProperties);
  GetRegisteredEditProperties.Unregister(TABcxExtLookupComboBoxProperties);
  GetRegisteredEditProperties.Unregister(TABcxCheckComboBoxProperties);
  GetRegisteredEditProperties.Unregister(TABcxComboBoxProperties);
  GetRegisteredEditProperties.Unregister(TABcxTreeViewPopupEditProperties);
  GetRegisteredEditProperties.Unregister(TABcxImageProperties);
  GetRegisteredEditProperties.Unregister(TABcxTextEditProperties);
  GetRegisteredEditProperties.Unregister(TABcxMaskEditProperties);
  GetRegisteredEditProperties.Unregister(TABcxButtonEditProperties);
  GetRegisteredEditProperties.Unregister(TABcxSpinEditProperties);
  GetRegisteredEditProperties.Unregister(TABcxCalcEditProperties);
  GetRegisteredEditProperties.Unregister(TABcxHyperLinkEditProperties);
  GetRegisteredEditProperties.Unregister(TABcxCurrencyEditProperties);
  GetRegisteredEditProperties.Unregister(TABcxBlobEditProperties);
  GetRegisteredEditProperties.Unregister(TABcxMRUEditProperties);
  GetRegisteredEditProperties.Unregister(TABcxProgressBarProperties);
  GetRegisteredEditProperties.Unregister(TABcxTrackBarProperties);
  GetRegisteredEditProperties.Unregister(TABcxRichEditProperties);
  GetRegisteredEditProperties.Unregister(TABcxPopupEditProperties);
  GetRegisteredEditProperties.Unregister(TABcxCheckGroupProperties);
  GetRegisteredEditProperties.Unregister(TABcxLabelProperties);
  GetRegisteredEditProperties.Unregister(TABcxImageComboBoxProperties);
  GetRegisteredEditProperties.Unregister(TABcxColorComboBoxProperties);
  GetRegisteredEditProperties.Unregister(TABcxFontNameComboBoxProperties);
  GetRegisteredEditProperties.Unregister(TABcxShellComboBoxProperties);
end;

procedure ABInitialization;
begin
  ABRegisterClass([]);

  FPubDownEditList := TstringList.Create;

  GetRegisteredEditProperties.Register(TABcxTimeEditProperties            , trim('ABcxTimeEdit            ')+'|'+trim('ABcxTimeEdit            '));
  GetRegisteredEditProperties.Register(TABcxDateEditProperties            , trim('ABcxDateEdit            ')+'|'+trim('ABcxDateEdit            '));
  GetRegisteredEditProperties.Register(TABcxDateTimeEditProperties        , trim('ABcxDateTimeEdit        ')+'|'+trim('ABcxDateTimeEdit        '));
  GetRegisteredEditProperties.Register(TABcxRadioGroupProperties          , trim('ABcxRadioGroup          ')+'|'+trim('ABcxRadioGroup          '));
  GetRegisteredEditProperties.Register(TABcxCheckBoxProperties            , trim('ABcxCheckBox            ')+'|'+trim('ABcxCheckBox            '));
  GetRegisteredEditProperties.Register(TABcxMemoProperties                , trim('ABcxMemo                ')+'|'+trim('ABcxMemo                '));
  GetRegisteredEditProperties.Register(TABcxComBoBoxMemoProperties        , trim('ABcxComBoBoxMemo        ')+'|'+trim('ABcxComBoBoxMemo        '));
  GetRegisteredEditProperties.Register(TABcxDirSelectProperties           , trim('ABcxDirSelect           ')+'|'+trim('ABcxDirSelect           '));
  GetRegisteredEditProperties.Register(TABcxFileSelectProperties          , trim('ABcxFileSelect          ')+'|'+trim('ABcxFileSelect          '));
  GetRegisteredEditProperties.Register(TABcxFieldSelectsProperties        , trim('ABcxFieldSelects        ')+'|'+trim('ABcxFieldSelects        '));
  GetRegisteredEditProperties.Register(TABcxFileBlobFieldProperties       , trim('ABcxFileBlobField       ')+'|'+trim('ABcxFileBlobField       '));
  GetRegisteredEditProperties.Register(TABcxSQLFieldSelectsProperties     , trim('ABcxSQLFieldSelects     ')+'|'+trim('ABcxSQLFieldSelects     '));
  GetRegisteredEditProperties.Register(TABcxPrintSelectProperties         , trim('ABcxPrintSelect         ')+'|'+trim('ABcxPrintSelect         '));
  GetRegisteredEditProperties.Register(TABcxFieldOrderProperties          , trim('ABcxFieldOrder          ')+'|'+trim('ABcxFieldOrder          '));
  GetRegisteredEditProperties.Register(TABcxLookupComboBoxProperties      , trim('ABcxLookupComboBox      ')+'|'+trim('ABcxLookupComboBox      '));
  GetRegisteredEditProperties.Register(TABcxExtLookupComboBoxProperties   , trim('ABcxExtLookupComboBox   ')+'|'+trim('ABcxExtLookupComboBox   '));
  GetRegisteredEditProperties.Register(TABcxCheckComboBoxProperties       , trim('ABcxCheckComboBox       ')+'|'+trim('ABcxCheckComboBox       '));
  GetRegisteredEditProperties.Register(TABcxComboBoxProperties            , trim('ABcxComboBox            ')+'|'+trim('ABcxComboBox            '));
  GetRegisteredEditProperties.Register(TABcxTreeViewPopupEditProperties   , trim('ABcxTreeViewPopupEdit   ')+'|'+trim('ABcxTreeViewPopupEdit   '));
  GetRegisteredEditProperties.Register(TABcxImageProperties               , trim('ABcxImage               ')+'|'+trim('ABcxImage               '));
  GetRegisteredEditProperties.Register(TABcxTextEditProperties            , trim('ABcxTextEdit            ')+'|'+trim('ABcxTextEdit            '));
  GetRegisteredEditProperties.Register(TABcxMaskEditProperties            , trim('ABcxMaskEdit            ')+'|'+trim('ABcxMaskEdit            '));
  GetRegisteredEditProperties.Register(TABcxButtonEditProperties          , trim('ABcxButtonEdit          ')+'|'+trim('ABcxButtonEdit          '));
  GetRegisteredEditProperties.Register(TABcxSpinEditProperties            , trim('ABcxSpinEdit            ')+'|'+trim('ABcxSpinEdit            '));
  GetRegisteredEditProperties.Register(TABcxCalcEditProperties            , trim('ABcxCalcEdit            ')+'|'+trim('ABcxCalcEdit            '));
  GetRegisteredEditProperties.Register(TABcxHyperLinkEditProperties       , trim('ABcxHyperLinkEdit       ')+'|'+trim('ABcxHyperLinkEdit       '));
  GetRegisteredEditProperties.Register(TABcxCurrencyEditProperties        , trim('ABcxCurrencyEdit        ')+'|'+trim('ABcxCurrencyEdit        '));
  GetRegisteredEditProperties.Register(TABcxBlobEditProperties            , trim('ABcxBlobEdit            ')+'|'+trim('ABcxBlobEdit            '));
  GetRegisteredEditProperties.Register(TABcxMRUEditProperties             , trim('ABcxMRUEdit             ')+'|'+trim('ABcxMRUEdit             '));
  GetRegisteredEditProperties.Register(TABcxProgressBarProperties         , trim('ABcxProgressBar         ')+'|'+trim('ABcxProgressBar         '));
  GetRegisteredEditProperties.Register(TABcxTrackBarProperties            , trim('ABcxTrackBar            ')+'|'+trim('ABcxTrackBar            '));
  GetRegisteredEditProperties.Register(TABcxRichEditProperties            , trim('ABcxRichEdit            ')+'|'+trim('ABcxRichEdit            '));
  GetRegisteredEditProperties.Register(TABcxPopupEditProperties           , trim('ABcxPopupEdit           ')+'|'+trim('ABcxPopupEdit           '));
  GetRegisteredEditProperties.Register(TABcxCheckGroupProperties          , trim('ABcxCheckGroup          ')+'|'+trim('ABcxCheckGroup          '));
  GetRegisteredEditProperties.Register(TABcxLabelProperties               , trim('ABcxLabel               ')+'|'+trim('ABcxLabel               '));
  GetRegisteredEditProperties.Register(TABcxImageComboBoxProperties       , trim('ABcxImageComboBox       ')+'|'+trim('ABcxImageComboBox       '));
  GetRegisteredEditProperties.Register(TABcxColorComboBoxProperties       , trim('ABcxColorComboBox       ')+'|'+trim('ABcxColorComboBox       '));
  GetRegisteredEditProperties.Register(TABcxFontNameComboBoxProperties    , trim('ABcxFontNameComboBox    ')+'|'+trim('ABcxFontNameComboBox    '));
  GetRegisteredEditProperties.Register(TABcxShellComboBoxProperties       , trim('ABcxShellComboBox       ')+'|'+trim('ABcxShellComboBox       '));

end;

{ TABcxFileBlobFieldProperties }

constructor TABcxFileBlobFieldProperties.Create(AOwner: TPersistent);
begin
  inherited;
  ABSetPropertiesPublicValue(self);
  ABInitFileBlobFieldButton(self);
end;

class function TABcxFileBlobFieldProperties.GetContainerClass: TcxContainerClass;
begin
  Result := TABcxFileBlobField;
end;

{ TABcxFileBlobField }
constructor TABcxFileBlobField.Create(AOwner: TComponent);
begin
  inherited;
  ABInitFileBlobFieldButton(Properties);
end;

procedure TABcxFileBlobField.DoButtonClick(AButtonVisibleIndex: Integer);
begin
  inherited;
  if (Assigned(InplaceParams.Position.Item))  then
  begin
    ABFileBlobFieldButtonClick(
         ABGetColumnDataSource(TcxCustomGridTableItem(InplaceParams.Position.Item)),
         ABGetColumnField(TcxCustomGridTableItem(InplaceParams.Position.Item)),
         AButtonVisibleIndex);
  end;
end;

class function TABcxFileBlobField.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxFileBlobFieldProperties;
end;

{ TABcxDBFileBlobField }

constructor TABcxDBFileBlobField.Create(AOwner: TComponent);
begin
  inherited;
  ABInitFileBlobFieldButton(Properties);
end;

procedure TABcxDBFileBlobField.DoButtonClick(AButtonVisibleIndex: Integer);
begin
  inherited;
  ABFileBlobFieldButtonClick(DataBinding.DataSource,DataBinding.Field, AButtonVisibleIndex);
end;

class function TABcxDBFileBlobField.GetPropertiesClass: TcxCustomEditPropertiesClass;
begin
  Result := TABcxFileBlobFieldProperties;
end;

{ TABcxCustomComboboxInnerEdit }

procedure TABcxCustomComboboxInnerEdit.Change;
begin
  inherited;
  if Container is TcxCustomDBLookupEdit  then
  begin
    ABLookupComboBox_Change(TcxCustomDBLookupEdit(Container));
  end;
end;

constructor TABcxCustomComboboxInnerEdit.Create(AOwner: TComponent);
begin
  inherited;

end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;


end.
