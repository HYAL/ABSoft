{
框架输入Panel窗体单元
}
unit ABFramkWorkDBPanelFormU;

interface
{$I ..\ABInclude.ini}


uses
  abPubUserU,
  abpubFuncU,
  ABPubFormU,
  ABPubDBU,
  ABPubMessageU,
  ABPubDesignU,
  ABPubDesignAlignU,

  ABFramkWorkControlU,
  ABFramkWorkDictionaryQueryU,
  ABFramkWorkFuncU,
  ABFramkWorkDBPanelU,

  cxPC,Math,Messages,
  SysUtils,Variants,Classes,Controls,Forms,ExtCtrls,db,StdCtrls,

  Vcl.Buttons, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, ABPubPanelU, Vcl.Menus, cxButtons;

type
  TABDBPanelForm = class(TABPubForm)
    DataSource1: TDataSource;
    ABcxPageControl1: TABcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    cxTabSheet4: TcxTabSheet;
    cxTabSheet5: TcxTabSheet;
    cxTabSheet6: TcxTabSheet;
    cxTabSheet7: TcxTabSheet;
    cxTabSheet8: TcxTabSheet;
    cxTabSheet9: TcxTabSheet;
    cxTabSheet10: TcxTabSheet;

    Page1_ABDBPanel: TABDBPanel;
    Page2_ABDBPanel: TABDBPanel;
    Page3_ABDBPanel: TABDBPanel;
    Page4_ABDBPanel: TABDBPanel;
    Page5_ABDBPanel: TABDBPanel;
    Page6_ABDBPanel: TABDBPanel;
    Page7_ABDBPanel: TABDBPanel;
    Page8_ABDBPanel: TABDBPanel;
    Page9_ABDBPanel: TABDBPanel;
    Page10_ABDBPanel: TABDBPanel;

    Page1_Bevel1: TBevel;
    Page1_Bevel2: TBevel;
    Page1_Bevel3: TBevel;
    Page1_Bevel4: TBevel;
    Page1_Bevel5: TBevel;
    Page1_Bevel6: TBevel;
    Page1_Bevel7: TBevel;
    Page1_Bevel8: TBevel;
    Page2_Bevel1: TBevel;
    Page2_Bevel2: TBevel;
    Page2_Bevel3: TBevel;
    Page2_Bevel4: TBevel;
    Page2_Bevel5: TBevel;
    Page2_Bevel6: TBevel;
    Page2_Bevel7: TBevel;
    Page2_Bevel8: TBevel;
    Page3_Bevel1: TBevel;
    Page3_Bevel2: TBevel;
    Page3_Bevel3: TBevel;
    Page3_Bevel4: TBevel;
    Page3_Bevel5: TBevel;
    Page3_Bevel6: TBevel;
    Page3_Bevel7: TBevel;
    Page3_Bevel8: TBevel;

    Page4_Bevel1: TBevel;
    Page4_Bevel2: TBevel;
    Page4_Bevel3: TBevel;
    Page4_Bevel4: TBevel;
    Page4_Bevel5: TBevel;
    Page4_Bevel6: TBevel;
    Page4_Bevel7: TBevel;
    Page4_Bevel8: TBevel;

    Page5_Bevel1: TBevel;
    Page5_Bevel2: TBevel;
    Page5_Bevel3: TBevel;
    Page5_Bevel4: TBevel;
    Page5_Bevel5: TBevel;
    Page5_Bevel6: TBevel;
    Page5_Bevel7: TBevel;
    Page5_Bevel8: TBevel;

    Page6_Bevel1: TBevel;
    Page6_Bevel2: TBevel;
    Page6_Bevel3: TBevel;
    Page6_Bevel4: TBevel;
    Page6_Bevel5: TBevel;
    Page6_Bevel6: TBevel;
    Page6_Bevel7: TBevel;
    Page6_Bevel8: TBevel;

    Page7_Bevel1: TBevel;
    Page7_Bevel2: TBevel;
    Page7_Bevel3: TBevel;
    Page7_Bevel4: TBevel;
    Page7_Bevel5: TBevel;
    Page7_Bevel6: TBevel;
    Page7_Bevel7: TBevel;
    Page7_Bevel8: TBevel;

    Page8_Bevel1: TBevel;
    Page8_Bevel2: TBevel;
    Page8_Bevel3: TBevel;
    Page8_Bevel4: TBevel;
    Page8_Bevel5: TBevel;
    Page8_Bevel6: TBevel;
    Page8_Bevel7: TBevel;
    Page8_Bevel8: TBevel;

    Page9_Bevel1: TBevel;
    Page9_Bevel2: TBevel;
    Page9_Bevel3: TBevel;
    Page9_Bevel4: TBevel;
    Page9_Bevel5: TBevel;
    Page9_Bevel6: TBevel;
    Page9_Bevel7: TBevel;
    Page9_Bevel8: TBevel;

    Page10_Bevel1: TBevel;
    Page10_Bevel2: TBevel;
    Page10_Bevel3: TBevel;
    Page10_Bevel4: TBevel;
    Page10_Bevel5: TBevel;
    Page10_Bevel6: TBevel;
    Page10_Bevel7: TBevel;
    Page10_Bevel8: TBevel;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    Panel1: TPanel;
    Button2: TABcxButton;
    SpeedButton1: TABcxButton;
    SpeedButton2: TABcxButton;
    Panel2: TPanel;
    LabeledEdit2: TLabeledEdit;
    LabeledEdit1: TLabeledEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
  private
    FInitWidth,FInitHeight:LongInt;
    FSaveWidth,FSaveHeight:LongInt;
    FFuncControlPropertyDataset:Tdataset;
    FEditEnabledOrVisibleFieldNames:Boolean;
    FOldPageText:string;
    FOldDetailHeight:longint;

    FBackHeight:longint;
    FEnabledFieldNames: string;
    FVisibleFieldNames: string;
    procedure SetTabStop;
    procedure UpdateLabeledEdit;

    //显示前加载保存的大小和位置
    procedure LoadOther;
    //在设计后保存控件大小与位置的同时保存分隔条与当前窗体的大小和位置
    procedure SaveOther;
    //创建控件
    procedure CreateControls;
    procedure SetEnabledFieldNames(const Value: string);
    procedure SetVisibleFieldNames(const Value: string);
    function GetFuncControlPropertyDataset: Tdataset;
    { Private declarations }
  public
    procedure   WMMOVE(var   Msg:   TMessage);   message   WM_MOVE;
    constructor Create(AOwner: TComponent);override;
    //设置指定的页面为当前页面
    //设置序号为aIndex的页面为当前页面
    procedure SetActivePageView(aIndex:LongInt);overload;
    //根据当前激活的值在所有值中的位置序号来设置当前页面
    procedure SetActivePageView(aActivePageCaption:string);overload;

    //设置指定字段关联控件的Enabled
    //aFieldNames=emptystr表示不做动作
    property EnabledFieldNames:string read FEnabledFieldNames write SetEnabledFieldNames;
    //设置指定字段关联控件的Visible
    //aFieldNames=emptystr表示不做动作
    property VisibleFieldNames:string read FVisibleFieldNames write SetVisibleFieldNames;
    { Public declarations }
  end;


implementation

{$R *.dfm}
constructor TABDBPanelForm.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FEditEnabledOrVisibleFieldNames:=true;

  FBackHeight:=0;
  DataSource1.DataSet:= TDataset(AOwner);
  Name:=ABGetOwnerLongName(AOwner,'_')+'_'+'DBPanel';

  LoadOther;
  CreateControls;
  FBackHeight:=Height;
end;

procedure TABDBPanelForm.FormShow(Sender: TObject);
var
  i: Integer;
begin
  if (FOldPageText<>LabeledEdit2.Text)then
  begin
    UpdateLabeledEdit;
    CreateControls
  end;

  if FEditEnabledOrVisibleFieldNames then
  begin
    for i := 1 to 10 do
    begin
      if ABcxPageControl1.Pages[i-1].TabVisible then
      begin
        TABDBPanel(FindComponent('Page'+inttostr(i)+'_ABDBPanel')).LinkFieldDefLabelControls(False);
        TABDictionaryQuery(DataSource1.DataSet).SetVisible(FVisibleFieldNames);
        TABDictionaryQuery(DataSource1.DataSet).SetEnabled(FEnabledFieldNames);
      end;
    end;
    FEditEnabledOrVisibleFieldNames:=False;
  end;
end;

procedure TABDBPanelForm.Button2Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TABDBPanelForm.SetTabStop;
var
  I,j: longint;
  tempPanel:TWinControl;
begin
  for I := 1 to 10 do
  begin
    if ABcxPageControl1.Pages[i-1].TabVisible then
    begin
      tempPanel:=TWinControl(FindComponent('Page'+inttostr(i)+'_ABDBPanel'));
      for j := 0 to tempPanel.ControlCount - 1 do
      begin
        if ((tempPanel.Controls[j] is TWinControl)) then
        begin
          TWinControl(tempPanel.Controls[j]).TabStop:=(tempPanel.Controls[j].Top+tempPanel.Controls[j].Height)<Height-22-tempPanel.Height;
        end;
      end;
    end;
  end;
end;

procedure TABDBPanelForm.SetVisibleFieldNames(const Value: string);
begin
  FVisibleFieldNames := Value;
  FEditEnabledOrVisibleFieldNames:=true;
end;

procedure TABDBPanelForm.SpeedButton1Click(Sender: TObject);
begin
  if abpos('>>',SpeedButton1.Caption)>0 then
  begin
    Height:=FBackHeight+ StrToIntDef(LabeledEdit1.Text,0);
    SpeedButton1.Caption:=ABStringReplace(SpeedButton1.Caption,'>>','<<');
  end
  else
  begin
    Height:=FBackHeight;
    SpeedButton1.Caption:=ABStringReplace(SpeedButton1.Caption,'<<','>>');
  end;
  SetTabStop;
end;

procedure TABDBPanelForm.SetActivePageView(aIndex: Integer);
begin
  ABcxPageControl1.ActivePageIndex:=aIndex;
end;

procedure TABDBPanelForm.SetActivePageView(aActivePageCaption:string);
var
  I: Integer;
begin
  if (aActivePageCaption<>emptystr) then
  begin
    ABcxPageControl1.ActivePageIndex:=-1;
    for I := 0 to ABcxPageControl1.PageCount-1 do
    begin
      if AnsiCompareText(ABcxPageControl1.Pages[i].Caption,aActivePageCaption)=0 then
      begin
        ABcxPageControl1.ActivePageIndex:=i;
        Break;
      end;
    end;
  end;
end;

procedure TABDBPanelForm.SetEnabledFieldNames(const Value: string);
begin
  FEnabledFieldNames := Value;
  FEditEnabledOrVisibleFieldNames:=true;
end;

procedure TABDBPanelForm.CreateControls;
var
  i: Integer;
  tempAddWidth,tempAddHeight:LongInt;
  tempMaxPanelWidth,tempMaxPanelHeight:LongInt;
  procedure  CreateControls_Page_ABDBPanel(acxTabSheet: TcxTabSheet;aDBPane: TABDBPanel);
  begin
    if (acxTabSheet.TabVisible)  then
    begin
      aDBPane.DataSource:=DataSource1;
      aDBPane.AddAnchors_akBottom:=false;
      aDBPane.RefreshControls(1,1,false);
      tempMaxPanelWidth:=Max(tempMaxPanelWidth,aDBPane.AutoCaleWidth);
      tempMaxPanelHeight:=Max(tempMaxPanelHeight,aDBPane.AutoCaleHeight);
    end;
  end;
begin
  tempAddWidth:=width-Page1_ABDBPanel.Width;
  tempAddHeight:=Height-Page1_ABDBPanel.Height;
  tempMaxPanelWidth :=0;
  tempMaxPanelHeight:=0;
  for i := 1 to 10 do
  begin
    CreateControls_Page_ABDBPanel(ABcxPageControl1.Pages[i-1],TABDBPanel(FindComponent('Page'+inttostr(i)+'_ABDBPanel')));
  end;

  if FSaveWidth=0 then
    width:=tempMaxPanelWidth+tempAddWidth;
  if FSaveHeight=0 then
    Height:=tempMaxPanelHeight+tempAddHeight;
  SetTabStop;
end;

procedure TABDBPanelForm.UpdateLabeledEdit;
var
  tempVisabledCount:longint;
  tempStr1:string;
  i,j: Integer;
begin
  FOldPageText:=LabeledEdit2.Text;
  FOldDetailHeight:=StrToIntDef(LabeledEdit1.Text,0);

  for j := 1 to 9 do
  begin
    ABcxPageControl1.Pages[j].TabVisible:=false;
  end;

  tempVisabledCount:=0;
  if LabeledEdit2.Text<>emptystr then
  begin
    for I := 1 to ABGetSpaceStrCount(LabeledEdit2.Text,',') do
    begin
      tempStr1:=ABGetSpaceStr(LabeledEdit2.Text, i,',');
      if tempStr1<>emptystr then
      begin
        tempVisabledCount:=tempVisabledCount+1;
        ABcxPageControl1.Pages[i-1].Caption:=tempStr1;
        ABcxPageControl1.Pages[i-1].TabVisible:=True;
      end;
    end;
  end;
  ABcxPageControl1.HideTabs:=tempVisabledCount<=1;
  ABcxPageControl1.ActivePageIndex:=0;
end;

procedure TABDBPanelForm.WMMOVE(var Msg: TMessage);
begin
  Inherited;
  if (Assigned(ABPubDesignerHook)) and
     (Assigned(ABPubDesignerHook.Form))  then
  begin
    ABRefreshDesignAlignPosition;
  end;
end;

function TABDBPanelForm.GetFuncControlPropertyDataset:Tdataset;
begin
  if not Assigned(FFuncControlPropertyDataset) then
    FFuncControlPropertyDataset:= ABGetConstSqlPubDataset('ABSys_Org_FuncControlProperty',
                                                          [TABDictionaryQuery(DataSource1.DataSet).FuncGuid,
                                                          name],TABInsideQuery);

  result:=FFuncControlPropertyDataset;
end;


procedure TABDBPanelForm.LoadOther;
var
  i,j:longint;
  tempBevel:TBevel;
begin
  FInitWidth :=Width;
  FInitHeight:=Height;

  if (name<>EmptyStr)  and
    (TABDictionaryQuery(DataSource1.DataSet).FuncGuid<>emptystr) then
  begin
    FSaveWidth  := ABGetFieldValue( GetFuncControlPropertyDataset,
                               ['FP_ControlName','FP_PropertyName'],
                               ['Self','Width'],
                               ['FP_PropertyValue'],0);
    FSaveHeight  :=ABGetFieldValue( GetFuncControlPropertyDataset,
                               ['FP_ControlName','FP_PropertyName'],
                               ['Self','Height'],
                               ['FP_PropertyValue'],0);

    Width  :=ABIIF(FSaveWidth=0,FInitWidth,FSaveWidth);
    Height :=ABIIF(FSaveHeight=0,FInitHeight,FSaveHeight);;

    LabeledEdit1.Text:=ABGetFieldValue( GetFuncControlPropertyDataset,
                                        ['FP_ControlName','FP_PropertyName'],
                                         ['Expand','Height'],
                                        ['FP_PropertyValue'],0);
    LabeledEdit2.Text:=ABGetFieldValue( GetFuncControlPropertyDataset,
                                        ['FP_ControlName','FP_PropertyName'],
                                         ['PageText','Captions'],
                                        ['FP_PropertyValue'],'');
    for I := 1 to 10 do
    begin
      TABDBPanel(FindComponent('Page'+inttostr(i)+'_ABDBPanel')).Width:=ABcxPageControl1.Pages[i-1].Width;
      for j := 1 to 8 do
      begin
        tempBevel:=TBevel(FindComponent('Page'+inttostr(i)+'_Bevel'+inttostr(j)));
        if Assigned(tempBevel) then
        begin
          tempBevel.Left:=  max(1 ,ABGetFieldValue(GetFuncControlPropertyDataset,
                                                   ['FP_ControlName','FP_PropertyName'],
                                                   [tempBevel.name,'Left'],
                                                   ['FP_PropertyValue'],1));
          tempBevel.Top :=  max(0 ,ABGetFieldValue(GetFuncControlPropertyDataset,
                                                   ['FP_ControlName','FP_PropertyName'],
                                                   [tempBevel.name,'Top'],
                                                   ['FP_PropertyValue'],0));
          tempBevel.Width:=  max(10 ,ABGetFieldValue(GetFuncControlPropertyDataset,
                                                   ['FP_ControlName','FP_PropertyName'],
                                                   [tempBevel.name,'Width'],
                                                   ['FP_PropertyValue'],420));
          tempBevel.Height:=  max(2 ,ABGetFieldValue(GetFuncControlPropertyDataset,
                                                   ['FP_ControlName','FP_PropertyName'],
                                                   [tempBevel.name,'Height'],
                                                   ['FP_PropertyValue'],2));
        end;
      end;
    end;

    if ABPubUser.IsAdmin then
    begin
      LabeledEdit1.Visible:=true;
      LabeledEdit2.Visible:=true;
      SpeedButton1.Visible:=true;
      SpeedButton2.Visible:=true;
    end
    else
    begin
      LabeledEdit1.Visible:=False;
      LabeledEdit2.Visible:=False;
      SpeedButton1.Visible:=StrToIntDef(LabeledEdit1.Text,0)>0;
      SpeedButton2.Visible:=False;
    end;
    UpdateLabeledEdit;
  end;
end;

procedure TABDBPanelForm.N1Click(Sender: TObject);
begin
  ABBeginDesignClientOfParent(self,ABcxPageControl1);
end;

procedure TABDBPanelForm.N2Click(Sender: TObject);
begin
  ABEndDesign(self);
end;

procedure TABDBPanelForm.N3Click(Sender: TObject);
begin
  SaveOther;

  abshow('保存完成.');
end;

procedure TABDBPanelForm.SaveOther;
var
  i,j:longint;
  tempBevel:TBevel;
  tempHeight: Integer;
begin
  if (name<>EmptyStr)  and
    (TABDictionaryQuery(DataSource1.DataSet).FuncGuid<>emptystr) then
  begin
    if abpos('>>',SpeedButton1.Caption)>0 then
    begin
      tempHeight:=Height;
    end
    else
    begin
      tempHeight:=Height- StrToIntDef(LabeledEdit1.Text,0);
    end;

    for I := 1 to 10 do
    begin
      if ABcxPageControl1.Pages[i-1].TabVisible then
      begin
        for j := 1 to 8 do
        begin
          tempBevel:=TBevel(FindComponent('Page'+inttostr(i)+'_Bevel'+inttostr(j)));
          if Assigned(tempBevel) then
          begin
            ABSetFieldValue(GetFuncControlPropertyDataset,['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],[TABDictionaryQuery(DataSource1.DataSet).FuncGuid,name,tempBevel.name,'Left'],['FP_PropertyValue'],[ABIIF(tempBevel.Left=1,EmptyStr,IntToStr(tempBevel.Left))]);
            ABSetFieldValue(GetFuncControlPropertyDataset,['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],[TABDictionaryQuery(DataSource1.DataSet).FuncGuid,name,tempBevel.name,'Top'],['FP_PropertyValue'],[ABIIF(tempBevel.top=0,EmptyStr,IntToStr(tempBevel.top))]);
            ABSetFieldValue(GetFuncControlPropertyDataset,['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],[TABDictionaryQuery(DataSource1.DataSet).FuncGuid,name,tempBevel.name,'Width'],['FP_PropertyValue'],[ABIIF(tempBevel.Width=420,EmptyStr,IntToStr(tempBevel.Width))]);
            ABSetFieldValue(GetFuncControlPropertyDataset,['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],[TABDictionaryQuery(DataSource1.DataSet).FuncGuid,name,tempBevel.name,'Height'],['FP_PropertyValue'],[ABIIF(tempBevel.Height=2,EmptyStr,IntToStr(tempBevel.Height))]);
          end;
        end;
      end;
    end;
    ABSetFieldValue(GetFuncControlPropertyDataset,['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],[TABDictionaryQuery(DataSource1.DataSet).FuncGuid,name,'Self','Width'],['FP_PropertyValue'],[Width]);
    ABSetFieldValue(GetFuncControlPropertyDataset,['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],[TABDictionaryQuery(DataSource1.DataSet).FuncGuid,name,'Self','Height'],['FP_PropertyValue'],[tempHeight]);

    //修改详细信息的高度
    ABSetFieldValue(GetFuncControlPropertyDataset,
                    ['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],
                    [TABDictionaryQuery(DataSource1.DataSet).FuncGuid,name,'Expand','Height'],
                    ['FP_PropertyValue'],
                    [ABIIF(LabeledEdit1.Text='0',EmptyStr,LabeledEdit1.Text)]
                      );
    //修改所有显示页面的显示标签
    ABSetFieldValue(
                  GetFuncControlPropertyDataset,
                  ['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],
                  [TABDictionaryQuery(DataSource1.DataSet).FuncGuid,name,'PageText','Captions'],
                  ['FP_PropertyValue'],
                  [LabeledEdit2.Text]
                    );
  end;
end;


end.

