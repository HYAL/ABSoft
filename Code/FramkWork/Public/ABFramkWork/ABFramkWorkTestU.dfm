object ABTestForm: TABTestForm
  Left = 376
  Top = 135
  Caption = #20013#21326#20849#21644#22269
  ClientHeight = 532
  ClientWidth = 1055
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel8: TPanel
    Left = 0
    Top = 0
    Width = 209
    Height = 532
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object Memo4: TMemo
      Left = 0
      Top = 394
      Width = 209
      Height = 83
      Align = alBottom
      Lines.Strings = (
        'Memo3')
      ScrollBars = ssBoth
      TabOrder = 0
      OnKeyDown = Memo3KeyDown
    end
    object Memo3: TMemo
      Left = 0
      Top = 0
      Width = 209
      Height = 394
      Align = alClient
      Lines.Strings = (
        'Memo3')
      ScrollBars = ssBoth
      TabOrder = 1
      OnKeyDown = Memo3KeyDown
    end
    object ProgressBar2: TProgressBar
      Left = 0
      Top = 477
      Width = 209
      Height = 17
      Align = alBottom
      TabOrder = 2
    end
    object Panel10: TPanel
      Left = 0
      Top = 494
      Width = 209
      Height = 38
      Align = alBottom
      TabOrder = 3
      object Button52: TButton
        Left = 111
        Top = 6
        Width = 95
        Height = 25
        Caption = #35843#29992#25152#26377#20989#25968
        TabOrder = 0
        OnClick = Button52Click
      end
      object Button6: TButton
        Left = 0
        Top = 6
        Width = 105
        Height = 25
        Caption = #28165#38500#26085#24535
        TabOrder = 1
        OnClick = Button6Click
      end
    end
  end
  object pgc1: TPageControl
    Left = 209
    Top = 0
    Width = 846
    Height = 532
    ActivePage = ts1
    Align = alClient
    TabOrder = 1
    object ts1: TTabSheet
      Caption = #21333#20803
      ImageIndex = 2
      object pnl3: TPanel
        Left = 0
        Top = 0
        Width = 177
        Height = 504
        Align = alLeft
        TabOrder = 0
        object grp3: TGroupBox
          Left = 1
          Top = 143
          Width = 175
          Height = 37
          Align = alTop
          Caption = 'ABFramkWorkDictionaryQueryU'
          TabOrder = 0
          object btn2: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABFramkWorkDictionaryQueryU'
            TabOrder = 0
            OnClick = btn2Click
          end
        end
        object grp7: TGroupBox
          Left = 1
          Top = 106
          Width = 175
          Height = 37
          Align = alTop
          Caption = 'ABFramkWorkUserU'
          TabOrder = 1
          object btn11: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABFramkWorkUserU'
            TabOrder = 0
            OnClick = btn11Click
          end
        end
        object grp8: TGroupBox
          Left = 1
          Top = 38
          Width = 175
          Height = 68
          Align = alTop
          Caption = 'ABFramkWorkFuncU'
          TabOrder = 2
          object btn12: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABFramkWorkFuncU'
            TabOrder = 0
            OnClick = btn12Click
          end
          object ComboBox3: TComboBox
            Left = 5
            Top = 41
            Width = 153
            Height = 21
            TabOrder = 1
            Text = 'ComboBox2'
          end
        end
        object grp9: TGroupBox
          Left = 1
          Top = 1
          Width = 175
          Height = 37
          Align = alTop
          Caption = 'ABFramkWorkUpAndDownU'
          TabOrder = 3
          object btn13: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABFramkWorkUpAndDownU'
            TabOrder = 0
            OnClick = btn13Click
          end
        end
        object grp20: TGroupBox
          Left = 1
          Top = 180
          Width = 175
          Height = 317
          Align = alTop
          Caption = 'ABFramkWorkControlU'
          TabOrder = 4
          DesignSize = (
            175
            317)
          object btn24: TButton
            Left = 3
            Top = 12
            Width = 154
            Height = 25
            Caption = 'ABFramkWorkControlU'
            TabOrder = 0
            OnClick = btn24Click
          end
          object Panel7: TPanel
            Left = 5
            Top = 43
            Width = 165
            Height = 170
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 1
          end
          object ABcxButton1: TABcxButton
            Left = 3
            Top = 219
            Width = 74
            Height = 25
            Caption = #23548#20837#26597#35810
            DropDownMenu = PopupMenu1
            Kind = cxbkDropDownButton
            LookAndFeel.Kind = lfOffice11
            LookAndFeel.NativeStyle = False
            TabOrder = 2
            OnClick = ABcxButton1Click
            ShowProgressBar = False
          end
          object btn25: TButton
            Left = 4
            Top = 245
            Width = 76
            Height = 25
            Caption = #37322#25918
            TabOrder = 3
            OnClick = btn25Click
          end
          object btn26: TButton
            Left = 80
            Top = 245
            Width = 79
            Height = 25
            Caption = #22810#23618#23383#27573#21015
            TabOrder = 4
            OnClick = btn26Click
          end
          object btn27: TButton
            Left = 80
            Top = 219
            Width = 79
            Height = 25
            Caption = #26597#35810
            TabOrder = 5
            OnClick = btn27Click
          end
          object ABDBcxLabel1: TABDBFieldCaption
            Left = 2
            Top = 297
            Align = alBottom
            Caption = 'ABDBcxLabel1'
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -14
            Style.Font.Name = #23435#20307
            Style.Font.Style = []
            Style.IsFontAssigned = True
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            DataField = 'Tr_Name'
            DataSource = DataSource2
            ExplicitTop = 294
            AnchorY = 306
          end
          object ComboBox1: TComboBox
            Left = 3
            Top = 271
            Width = 149
            Height = 21
            TabOrder = 7
            Text = 'ComboBox1'
          end
        end
      end
      object pnl7: TPanel
        Left = 177
        Top = 0
        Width = 208
        Height = 504
        Align = alLeft
        TabOrder = 1
        object grp13: TGroupBox
          Left = 1
          Top = 38
          Width = 206
          Height = 37
          Align = alTop
          Caption = 'ABFramkWorkQueryFormU'
          TabOrder = 0
          object btn17: TButton
            Left = 3
            Top = 12
            Width = 190
            Height = 25
            Caption = 'ABFramkWorkQueryFormU'
            TabOrder = 0
            OnClick = btn17Click
          end
        end
        object grp14: TGroupBox
          Left = 1
          Top = 1
          Width = 206
          Height = 37
          Align = alTop
          Caption = 'ABFramkWorkTreeItemEditU'
          TabOrder = 1
          object btn18: TButton
            Left = 3
            Top = 12
            Width = 190
            Height = 25
            Caption = 'ABFramkWorkTreeItemEditU'
            TabOrder = 0
            OnClick = btn18Click
          end
        end
        object grp4: TGroupBox
          Left = 1
          Top = 75
          Width = 206
          Height = 235
          Align = alTop
          Caption = 'ABFramkWorkFastReportU'
          TabOrder = 2
          object grp5: TGroupBox
            Left = 2
            Top = 56
            Width = 202
            Height = 137
            Align = alTop
            Caption = #25193#23637#25253#34920
            TabOrder = 0
            object Panel4: TPanel
              Left = 2
              Top = 50
              Width = 198
              Height = 85
              Align = alClient
              TabOrder = 0
            end
            object pnl5: TPanel
              Left = 2
              Top = 15
              Width = 198
              Height = 35
              Align = alTop
              TabOrder = 1
              object btn3: TButton
                Left = 64
                Top = 1
                Width = 40
                Height = 25
                Caption = 'Design'
                TabOrder = 0
                OnClick = btn3Click
              end
              object btn4: TButton
                Left = 3
                Top = 1
                Width = 62
                Height = 25
                Caption = #37325#24314#36755#20837
                TabOrder = 1
                OnClick = btn4Click
              end
              object btn5: TButton
                Left = 103
                Top = 1
                Width = 50
                Height = 25
                Caption = 'Preview'
                TabOrder = 2
                OnClick = btn5Click
              end
              object btn6: TButton
                Left = 152
                Top = 1
                Width = 37
                Height = 25
                Caption = 'Print'
                TabOrder = 3
                OnClick = btn6Click
              end
            end
          end
          object grp6: TGroupBox
            Left = 2
            Top = 193
            Width = 202
            Height = 50
            Align = alTop
            Caption = #21151#33021#25253#34920
            TabOrder = 1
            object btn7: TButton
              Left = 3
              Top = 14
              Width = 44
              Height = 25
              Caption = 'Design'
              TabOrder = 0
              OnClick = btn7Click
            end
            object btn8: TButton
              Left = 46
              Top = 14
              Width = 52
              Height = 25
              Caption = 'Preview'
              TabOrder = 1
              OnClick = btn8Click
            end
            object btn9: TButton
              Left = 97
              Top = 14
              Width = 44
              Height = 25
              Caption = 'Print'
              TabOrder = 2
              OnClick = btn9Click
            end
          end
          object pnl6: TPanel
            Left = 2
            Top = 15
            Width = 202
            Height = 41
            Align = alTop
            TabOrder = 2
            object lbl1: TLabel
              Left = 1
              Top = 1
              Width = 60
              Height = 13
              Caption = #40664#35748#25171#21360#26426
            end
            object btn10: TButton
              Left = 67
              Top = 0
              Width = 88
              Height = 17
              Caption = #21151#33021'-'#25171#21360#26426
              TabOrder = 0
              OnClick = btn10Click
            end
            object ComboBox2: TComboBox
              Left = 1
              Top = 17
              Width = 153
              Height = 21
              TabOrder = 1
              Text = 'ComboBox2'
            end
          end
        end
        object grp10: TGroupBox
          Left = 1
          Top = 310
          Width = 206
          Height = 37
          Align = alTop
          Caption = 'ABFramkWorkDownManagerU'
          TabOrder = 3
          object btn14: TButton
            Left = 4
            Top = 11
            Width = 190
            Height = 25
            Caption = 'ABFramkWorkDownManagerU'
            TabOrder = 0
            OnClick = btn14Click
          end
        end
      end
      object pnl8: TPanel
        Left = 385
        Top = 0
        Width = 208
        Height = 504
        Align = alLeft
        TabOrder = 2
        object GroupBox3: TGroupBox
          Left = 1
          Top = 192
          Width = 206
          Height = 37
          Align = alTop
          Caption = 'ABFramkWorkOperatorDatasetU'
          TabOrder = 0
          object Button1: TButton
            Left = 3
            Top = 12
            Width = 190
            Height = 25
            Caption = 'ABFramkWorkOperatorDatasetU'
            TabOrder = 0
            OnClick = Button1Click
          end
        end
        object GroupBox5: TGroupBox
          Left = 1
          Top = 155
          Width = 206
          Height = 37
          Align = alTop
          Caption = 'ABFramkWorkQueryU'
          TabOrder = 1
          object Button3: TButton
            Left = 4
            Top = 13
            Width = 190
            Height = 25
            Caption = 'ABFramkWorkQueryU'
            TabOrder = 0
            OnClick = Button3Click
          end
        end
        object grp15: TGroupBox
          Left = 1
          Top = 1
          Width = 206
          Height = 154
          Align = alTop
          Caption = 'ABFramkWorkDBPanelU'
          TabOrder = 2
          object ABDBPanel1: TABDBPanel
            Left = 2
            Top = 15
            Width = 202
            Height = 137
            Align = alClient
            Caption = 'ABDBPanel1'
            ParentBackground = False
            ShowCaption = False
            TabOrder = 0
            ReadOnly = False
            DataSource = DataSource2
            AddAnchors_akRight = True
            AddAnchors_akBottom = True
            AutoHeight = True
            AutoWidth = True
          end
        end
        object DBGrid1: TDBGrid
          Left = 1
          Top = 229
          Width = 206
          Height = 120
          Align = alTop
          DataSource = DataSource2
          TabOrder = 3
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
        end
        object DBGrid2: TDBGrid
          Left = 1
          Top = 349
          Width = 206
          Height = 120
          Align = alTop
          DataSource = DataSource3
          TabOrder = 4
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
        end
      end
    end
    object ts6: TTabSheet
      Caption = #25511#20214
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object pgc3: TPageControl
        Left = 0
        Top = 0
        Width = 838
        Height = 504
        ActivePage = ts9
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'FramkWorkControl'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object pnl10: TPanel
            Left = 0
            Top = 0
            Width = 830
            Height = 476
            Align = alClient
            TabOrder = 0
            object ABcxDateEdit1: TABcxDateEdit
              Left = 8
              Top = 49
              Properties.SaveTime = False
              Properties.ShowTime = False
              TabOrder = 0
              Width = 121
            end
            object ABcxDateTimeEdit1: TABcxDateTimeEdit
              Left = 8
              Top = 95
              Properties.Kind = ckDateTime
              TabOrder = 1
              Width = 121
            end
            object ABcxComBoBoxMemo1: TABcxComBoBoxMemo
              Left = 8
              Top = 164
              Properties.DropDownRows = 20
              Properties.DropDownSizeable = True
              Properties.LookupItems.Strings = (
                'a'
                'b'
                'c'
                'd')
              TabOrder = 2
              Text = 'ABcxComBoBoxMemo1'
              Width = 121
            end
            object ABcxDirSelect1: TABcxDirSelect
              Left = 8
              Top = 210
              Properties.DropDownRows = 20
              TabOrder = 3
              Text = 'ABcxDirSelect1'
              Width = 121
            end
            object ABcxFileSelect1: TABcxFileSelect
              Left = 8
              Top = 233
              Properties.DropDownRows = 20
              TabOrder = 4
              Text = 'ABcxFileSelect1'
              Width = 121
            end
            object ABcxLabel1: TABcxLabel
              Left = 8
              Top = 2
              Caption = 'ABcxLabel1'
              ParentFont = False
              Style.Font.Charset = DEFAULT_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -14
              Style.Font.Name = #23435#20307
              Style.Font.Style = []
              Style.IsFontAssigned = True
              Properties.Alignment.Vert = taVCenter
              Transparent = True
              AnchorY = 11
            end
            object ABcxTextEdit1: TABcxTextEdit
              Left = 8
              Top = 256
              TabOrder = 6
              Text = 'ABcxTextEdit1'
              Width = 121
            end
            object ABcxMaskEdit1: TABcxMaskEdit
              Left = 8
              Top = 279
              Properties.EditMask = '!99/99/00;1;_'
              TabOrder = 7
              Text = '66-66-66'
              Width = 121
            end
            object ABcxButtonEdit1: TABcxButtonEdit
              Left = 8
              Top = 302
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              TabOrder = 8
              Text = 'ABcxButtonEdit1'
              Width = 121
            end
            object ABcxCheckBox1: TABcxCheckBox
              Left = 8
              Top = 26
              Caption = 'ABcxCheckBox1'
              Properties.NullStyle = nssUnchecked
              TabOrder = 9
              Transparent = True
              Width = 121
            end
            object ABcxComboBox1: TABcxComboBox
              Left = 8
              Top = 325
              Properties.ClearKey = 46
              Properties.DropDownRows = 20
              Properties.DropDownSizeable = True
              Properties.Items.Strings = (
                'a'
                'b'
                'c')
              TabOrder = 10
              Text = 'ABcxComboBox1'
              Width = 121
            end
            object ABcxImageComboBox1: TABcxImageComboBox
              Left = 8
              Top = 348
              Properties.Images = ImageList1
              Properties.Items = <
                item
                  Description = '1111'
                  ImageIndex = 0
                  Value = '1'
                end
                item
                  Description = '222'
                  ImageIndex = 1
                  Value = '3'
                end
                item
                  Description = '333'
                  ImageIndex = 2
                  Value = '3'
                end
                item
                  Description = '444'
                  ImageIndex = 3
                  Value = '2'
                end
                item
                  Description = '555'
                  ImageIndex = 4
                  Value = '1'
                end
                item
                  Description = '666'
                  ImageIndex = 5
                  Value = '1'
                end
                item
                  Description = '777'
                  ImageIndex = 6
                  Value = '2'
                end
                item
                  Description = '888'
                  ImageIndex = 7
                  Value = '1'
                end>
              TabOrder = 11
              Width = 121
            end
            object ABcxSpinEdit1: TABcxSpinEdit
              Left = 8
              Top = 371
              TabOrder = 12
              Width = 121
            end
            object ABcxCalcEdit1: TABcxCalcEdit
              Left = 160
              Top = 305
              EditValue = 0.000000000000000000
              TabOrder = 13
              Width = 121
            end
            object ABcxHyperLinkEdit1: TABcxHyperLinkEdit
              Left = 160
              Top = 327
              Cursor = crHandPoint
              ParentShowHint = False
              Properties.LookupItems.Strings = (
                'a'
                'b'
                'c')
              Properties.SingleClick = True
              Properties.Prefix = 'http://www.5599.net/?23'
              ShowHint = True
              TabOrder = 14
              Text = 'ABcxHyperLinkEdit1'
              TextHint = 'aaaaaa'
              Width = 121
            end
            object ABcxCurrencyEdit1: TABcxCurrencyEdit
              Left = 160
              Top = 349
              EditValue = 222.000000000000000000
              TabOrder = 15
              Width = 121
            end
            object ABcxBlobEdit1: TABcxBlobEdit
              Left = 160
              Top = 3
              Properties.BlobEditKind = bekPict
              TabOrder = 16
              Width = 121
            end
            object ABcxMRUEdit1: TABcxMRUEdit
              Left = 160
              Top = 24
              Properties.LookupItems.Strings = (
                'a'
                'b'
                'c'
                'd')
              TabOrder = 17
              Text = 'ABcxMRUEdit1'
              Width = 121
            end
            object ABcxLookupComboBox1: TABcxLookupComboBox
              Left = 160
              Top = 197
              Properties.DropDownListStyle = lsEditList
              Properties.DropDownRows = 20
              Properties.DropDownSizeable = True
              Properties.IncrementalFiltering = False
              Properties.KeyFieldNames = 'CL_Guid;CL_OP_Name'
              Properties.ListColumns = <
                item
                  FieldName = 'CL_PC'
                end
                item
                  FieldName = 'CL_OP_Name'
                end>
              TabOrder = 18
              Width = 121
            end
            object ABcxRadioButton1: TABcxRadioButton
              Left = 160
              Top = 46
              Width = 121
              Height = 21
              Caption = 'ABcxRadioButton1'
              Checked = True
              TabOrder = 19
              TabStop = True
            end
            object ABcxProgressBar1: TABcxProgressBar
              Left = 160
              Top = 68
              Position = 22.000000000000000000
              Properties.PeakValue = 22.000000000000000000
              TabOrder = 20
              Width = 121
            end
            object ABcxColorComboBox1: TABcxColorComboBox
              Left = 160
              Top = 89
              Properties.AllowSelectColor = True
              Properties.CustomColors = <>
              Properties.DefaultColor = clHighlight
              Properties.DropDownSizeable = True
              TabOrder = 21
              Width = 121
            end
            object ABcxFontNameComboBox1: TABcxFontNameComboBox
              Left = 160
              Top = 133
              Properties.DropDownSizeable = True
              TabOrder = 22
              Width = 121
            end
            object ABcxCheckComboBox1: TABcxCheckComboBox
              Left = 160
              Top = 155
              Properties.ShowEmptyText = False
              Properties.ClearKey = 46
              Properties.DropDownRows = 20
              Properties.DropDownSizeable = True
              Properties.EditValueFormat = cvfCaptions
              Properties.Items = <
                item
                  Description = 'aa'
                  ShortDescription = 'a'
                end
                item
                  Description = 'bb'
                  ShortDescription = 'b'
                end>
              TabOrder = 23
              Width = 121
            end
            object ABcxPopupEdit1: TABcxPopupEdit
              Left = 160
              Top = 177
              Properties.LookupItems.Strings = (
                'a'
                'b'
                'c')
              TabOrder = 24
              Text = 'ABcxPopupEdit1'
              Width = 121
            end
            object ABcxShellComboBox1: TABcxShellComboBox
              Left = 160
              Top = 111
              TabOrder = 25
              Width = 121
            end
            object ABcxButton2: TABcxButton
              Left = 160
              Top = 283
              Width = 121
              Height = 21
              Caption = 'ABcxButton2'
              LookAndFeel.Kind = lfFlat
              TabOrder = 26
              ShowProgressBar = False
            end
            object ABcxSpinButton1: TABcxSpinButton
              Left = 81
              Top = 72
              Associate = edt1
              AutoSize = False
              TabOrder = 27
              Height = 21
              Width = 21
            end
            object ABcxHeader1: TABcxHeader
              Left = 8
              Top = 394
              Width = 121
              Height = 28
              Images = ImageList1
              ParentShowHint = False
              Sections = <
                item
                  ImageIndex = 0
                  Width = 30
                end
                item
                  ImageIndex = 3
                  Width = 33
                end
                item
                  BiDiMode = bdLeftToRight
                  ImageIndex = 9
                  ParentBiDiMode = False
                  SortOrder = soAscending
                  Width = 33
                end>
              ShowHint = True
            end
            object ABcxExtLookupComboBox1: TABcxExtLookupComboBox
              Left = 160
              Top = 217
              Properties.AutoSearchOnPopup = False
              Properties.ClearKey = 46
              Properties.DropDownAutoSize = True
              Properties.DropDownRows = 20
              Properties.DropDownSizeable = True
              Properties.IncrementalFiltering = False
              Properties.Revertable = True
              TabOrder = 29
              Width = 121
            end
            object ABcxTimeEdit1: TABcxTimeEdit
              Left = 160
              Top = 240
              EditValue = '00:00:00'
              TabOrder = 30
              Width = 121
            end
            object edt1: TEdit
              Left = 32
              Top = 72
              Width = 49
              Height = 21
              TabOrder = 31
              Text = '0'
            end
            object ABcxTreeView1: TABcxTreeView
              Left = 408
              Top = 4
              Width = 100
              Height = 80
              TabOrder = 32
              Items.NodeData = {
                0303000000220000000000000000000000FFFFFFFFFFFFFFFF00000000000000
                0001000000010261006100240000000000000000000000FFFFFFFFFFFFFFFF00
                00000000000000000000000103610061003100220000000000000000000000FF
                FFFFFFFFFFFFFF00000000000000000000000001026200620022000000000000
                0000000000FFFFFFFFFFFFFFFF000000000000000000000000010263006300}
              ExtFullExpand = False
            end
            object ABcxMCListBox1: TABcxMCListBox
              Left = 302
              Top = 262
              Width = 100
              Height = 80
              HeaderSections = <
                item
                  Text = 'aaa'
                  Width = 30
                end
                item
                  Text = 'bbb'
                  Width = 33
                end
                item
                  AllowClick = True
                  AutoSize = True
                  Text = 'ccc'
                  Width = 33
                end>
              Items.Strings = (
                '1;2;3;4;5'
                'a;b;c;')
              OverflowEmptyColumn = False
              TabOrder = 33
            end
            object ABcxGroupBox1: TABcxGroupBox
              AlignWithMargins = True
              Left = 408
              Top = 90
              Caption = 'ABcxGroupBox1'
              TabOrder = 34
              Height = 80
              Width = 100
            end
            object ABcxRichEdit1: TABcxRichEdit
              Left = 302
              Top = 348
              Lines.Strings = (
                #20013#20849#20013#22830#25919#27835#23616
                #22996
                #21592)
              TabOrder = 35
              Height = 80
              Width = 100
            end
            object ABcxCheckGroup1: TABcxCheckGroup
              Left = 302
              Top = 4
              Caption = 'ABcxCheckGroup1'
              Properties.EditValueFormat = cvfCaptions
              Properties.Items = <
                item
                  Caption = 'aaaaaaaaaaaaaaaaa'
                end
                item
                  Caption = 'bbb'
                end>
              Properties.WordWrap = True
              TabOrder = 36
              Height = 80
              Width = 100
            end
            object ABcxCheckListBox1: TABcxCheckListBox
              Left = 302
              Top = 176
              Width = 100
              Height = 80
              Columns = 2
              EditValueFormat = cvfCaptions
              Items = <
                item
                  Text = 'a'
                end
                item
                  Text = 'b'
                end
                item
                  Text = 'c'
                end
                item
                  Text = 'd'
                end
                item
                  Text = 'e'
                end
                item
                  Text = 'f'
                end
                item
                  Text = 'w'
                end
                item
                  Text = 'g'
                end>
              TabOrder = 37
            end
            object ABcxMemo1: TABcxMemo
              Left = 514
              Top = 4
              Lines.Strings = (
                #20013#20849#20013#22830#25919
                #27835
                #23616#24120#22996)
              Properties.ScrollBars = ssVertical
              TabOrder = 38
              Height = 80
              Width = 100
            end
            object ABcxTrackBar1: TABcxTrackBar
              Left = 514
              Top = 90
              Position = 3
              TabOrder = 39
              Height = 80
              Width = 100
            end
            object ABcxRadioGroup1: TABcxRadioGroup
              Left = 408
              Top = 262
              Caption = 'ABcxRadioGroup1'
              Properties.Columns = 2
              Properties.DefaultValue = False
              Properties.Items = <
                item
                  Caption = 'aaaaaaaaaaaaa'
                end
                item
                  Caption = 'bb'
                end
                item
                  Caption = 'cc'
                end
                item
                  Caption = 'dd'
                end
                item
                  Caption = 'ee'
                end>
              Properties.WordWrap = True
              ItemIndex = 1
              TabOrder = 40
              Height = 80
              Width = 100
            end
            object ABcxListBox1: TABcxListBox
              Left = 408
              Top = 176
              Width = 100
              Height = 80
              ItemHeight = 13
              Items.Strings = (
                'a'
                'b'
                'c'
                'd')
              TabOrder = 41
              Remarks.Strings = (
                'aaaaaaaaaaaa'
                'bbbbbbbbbbbb'
                'ccccccccccccc')
            end
            object ABcxImage1: TABcxImage
              Left = 514
              Top = 176
              Picture.Data = {
                055449636F6E0000010009003030100001000400680600009600000020201000
                01000400E8020000FE060000101010000100040028010000E609000030300000
                01000800A80E00000E0B00002020000001000800A8080000B619000010100000
                01000800680500005E2200003030000001002000A8250000C627000020200000
                01002000A81000006E4D0000101000000100200068040000165E000028000000
                3000000060000000010004000000000080040000000000000000000000000000
                0000000000000000000080000080000000808000800000008000800080800000
                80808000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000
                FFFFFF0000000000000000000000000000000000000000000000000000000000
                0000000000000000000770778000000000000000000000000000000000000000
                007044047000000000000000000000000000000000000000004C8EC008000000
                00000000000000000000000000000007467EC7C4407000000000000000000000
                0000000000000064C7C77CCC4477000000000000000000000000000000000048
                CE7ECC4CCC0080000000000000000000000000000000006C8C78C4CCC4444700
                0000000000000000000000000000006CECEC7CC4CCC443700000000000000000
                000000000000007C8C88CC4CC4CCC400000000000000007707000000000070C7
                CECCC4CCC4CC44000000000000000700040000000007606C877C4CCC4CCCC077
                00000000000000004440000000005C8CEC7CCCCC4CC4C00000000000774646C0
                4444700000006CEC8ECC4CCC4CC44000000000000048C8C60CC44400007067C8
                C8CC4CCC4CC400000000000006CECEC50C4CC070070047CEC8C6CCC4C4C40000
                000000000747C8C6644CC4400465CEC8CECCC4CCCCC4000000000000074C8CEC
                7044444647CEC8C8E5C4CC4C4C44700000000000070EC7C8C0440047CEC8CEC7
                7CC4CCC4CC4700000000000000747CEC60446C7C8C8C7C8C8CCCCC4CCC070000
                0000000000006C8C7407C8CEC6CECECE8C44CCC4CC4300000000000000006C8C
                EC8CECEC8C057C8C8CCCCC4CCC4700000000000000007CEC8CEC7C744206C8CE
                CC4C4CC4C443000000000000000746C8CEC8C656410CEC8C4CCCC4CCC4400000
                000000000000747CEC8C00687C25CE8CC6CCC4CC4000000000000000000074EC
                8CC6044CE7067C8EC4CCC4CC40000000000000000000056C7C8C644C564C7CE5
                CCCCC4CC400000000000000000000007CEC8C40C447CEC8CC4CC4C4C40000000
                0000000000000006C8CEC70C04EC87C6CC4CCCCC400000000000000000000074
                7CEC8444047CE8CC4CC4C4C44700000000000000000000074EC8CE40047C8C7C
                CCCC4CC47000000000000000000000070C8CC8C006CEC8CC4CC4CCC070000000
                0F0000000000000074CE7C64657C8EC4CCCC4CC4300000F77880000000000000
                007CEC8C7CEC87CC4CC4CCC47000007C6C8F000000000000006C8CECEC8CECC4
                CC4CC4C0700008CECE68800000000000006C7C8C8C88CC4CCCC4CC44700076C6
                6C6C8F0000000000007CECEC7E888CCCC4CCC40000F7CE6C6C6E788000000000
                00657C8887E7E7C4CCC4C400087EC6C6C6CC6C88000000000074E8C8E8C8787C
                C4CC440008C6C6C66C66C6E7000000000070888E787E7E88CC4CC40006C66C6C
                E6C66C7C00000000000767EDEC87777E7CCCC400087C6CE6C6CEC87800000000
                0000747E88C8EC8787C44670088EC6C6CEC67C880000000000000748C8E788C8
                E88C450008C76CEC66C6CE8800000000000000747EDEC8E7C8E74700086CE6C6
                C6EC66C8000000000000000747E87EDE8C77700007E7C66CEC67C87E00000000
                00000000748C87E8C700000008887CE6C6CE888800000000000000000747E7C4
                77000000000086C76C77800000000000000000000007C84000000000000087C7
                C86CF000FFFFFFFFFFFF0000FFFFFFE07FFF0000FFFFFFC07FFF0000FFFFFF00
                3FFF0000FFFFFE001FFF0000FFFFFC000FFF0000FFFFF80007FF0000FFFFF800
                03FF0000FFFFF80001FF0000FFFFF80001FF0000FC3FF00001FF0000F81FE000
                00FF0000E00FE00001FF00000007E00007FF00000003C00007FF000000018000
                07FF00008000000007FF00008000000007FF0000800000000FFF0000C0000000
                0FFF0000E00000000FFF0000E00000000FFF0000E00000000FFF0000E0000000
                1FFF0000F00000003FFF0000F00000003FFF0000F80000003FFF0000FC000000
                3FFF0000FC0000003FFF0000FC0000003FFF0000FE0000007FFF0000FE000000
                7FBF0000FF0000007C1F0000FF8000007C0F0000FF80000078070000FF800000
                70030000FF800001C0010000FFC0000180000000FFC0000180000000FFC00001
                80000000FFE0000180000000FFF0000180000000FFF8000380000000FFFC0003
                80000000FFFE000780000000FFFF000F80000000FFFF803FF0070000FFFFC07F
                F007000028000000200000004000000001000400000000000002000000000000
                0000000000000000000000000000000000008000008000000080800080000000
                800080008080000080808000C0C0C0000000FF0000FF000000FFFF00FF000000
                FF00FF00FFFF0000FFFFFF000000000000000000000000000000000000000000
                0000000000425700000000000000000000000000048C44700000000000000000
                00000007C67C644700000000000000000000000CEC8CCC448000000000000000
                00000047C8CCCC4447000000000000000000000ECE7C4CCC4400000000000070
                0000074C8CCCCC4C40000000000404440000047EC8C4CCCC4700000006C76444
                400070C8C8CCC4C470000000567C74CC440706CEC7CCC4C47000000004ECE504
                C4247C8C8CC4CCC470000000075C7C4406C8CEC8CC4C4C4700000000006CE746
                7C8C67CE7CCCCC44000000000007CC7CEC6C4C8C7CCC4C4700000000006C8EC8
                C7430EC8CCC4CC40000000000004C7C6048C47CEC4CC4C7000000000000767EC
                44C6477CCCC4C4700000000000004C7C7444ECE7C4CCC47000000000000076C8
                C6407C8CCC4CC470000000000000048CE5047C8C4CC4C60000000000000007C6
                C646CECCCC4C45000088000000000007EC8C8C8C4CCC460008C688000000004C
                8CEC8EC4CC4C47008C6CE80000000076C77787CCCCC47008C66C6C8800000004
                8E8E778C4C447076C6C6C6E70000000778C8CE8ECCC4406C6666C6C700000000
                677E87787CC47088C6CEC6880000000007C8C8C8E847008CEC6C6CE800000000
                00768E8C8C770076C6EC67C7000000000007C87E777000F88C66C8F000000000
                00004EC54000000086C7E800FFFFFFFFFFFFC3FFFFFF01FFFFFE00FFFFFC007F
                FFFC003FFFFC003FF1F8003FC0F8003F0070007F0020007F8000007F800000FF
                C00000FFC00000FFC00001FFE00001FFE00001FFF00001FFF00001FFF80003FF
                F80003CFFC000383FC000303FC000600FE000400FE000400FF000400FF800C00
                FFC00C00FFE01C01FFF07F032800000010000000200000000100040000000000
                8000000000000000000000000000000000000000000000000000800000800000
                0080800080000000800080008080000080808000C0C0C0000000FF0000FF0000
                00FFFF00FF000000FF00FF00FFFF0000FFFFFF00000000000770000000000000
                47C7000000000006CCC48000007000048CCC40007444007CEC4C70007C64476C
                7CC4000006746C77CCC4000004C8C6CEC4C7000000EC4747CC400000005E54C7
                CCC00000000C648CC440000000067CECCC7086800007C88C4C07CC6800007E88
                C40E66C7000007CE8607C6E70000007C70008C70FF9F0000FF0F0000FE070000
                DE0700000C070000000F0000800F0000800F0000C01F0000C01F0000E01F0000
                E0110000E0200000F0200000F8200000FC710000280000003000000060000000
                0100080000000000000900000000000000000000000100000001000000000000
                160000001A1A1A002D00000038160B0038211600272727003E27270043000000
                4400000049000000550000005A0000006B0000006F0D0D00770B05007E0D0D00
                65160B004F22160054211000542D1C004F2D220054322100533125004B343400
                53343400583434007E393900544332005544330051403A004E4E4E0055555500
                644A4100616161006D6D6D00756D6D0072727200777777007A7A7A0087000000
                880000008F0000008D0B0500930000009A0000009E000000811D1D00991D1D00
                AA000000AF000000B1000000B5000000BA000000A4110B00872D16008D381C00
                8D3E2200C0000000CC000000D1000000D7000000DC000000D1110B00E2000000
                E8000000ED000000FF000000D7221600D72D2100DD332200B74C0300B64B0400
                B74C0500B94E0200B44E0F00B84E0800BD510100BF540200B9500A00BD540900
                AC4B1B00B04C1500814430008D4F33008D553800A9432100AA442200B5442D00
                BD602400BA643300BA643400C0530200C1550100C1560500C6590000C3580400
                C55B0700C65C0500C85B0000C95D0200C95F0700C0580A00C65E0B00C65F0D00
                C35F1500CC600000CA600600CC610500C7610F00C9610A00CA630D00C6621300
                C9641100C9661600C5631800C7661A00C4641C00C9671800C64F2700E2442D00
                E25A2D00E84F3300C0622100C0632400C7682000C3682700C4692700C86B2400
                C1672B00C86D2900C46B3000C16F3D00C6713500C7723800C2713E00C8753B00
                FF663300FF6B3800FF713E0081594400A9654300AA664400AA714F00907E6D00
                81787400817B7800817E7B00C6764300CA794000C0744B00C6774F00C67A4C00
                C67E5600C67F5900EE664400E86B4F00F36B4400EE714F00FF764300F3774F00
                FF7C4900AA886600CD804900CD814D00CB805000C8815700C5825F00CE895E00
                D08A5900FF875400C2806300C5846300CA876000C8876500C6886000CF8D6300
                CA896700CA8D6E00D1906500D1936E00D2956F00C88D7200CE957700CC947B00
                D39D7F00ED876500EE886600FF986500FF9E6B00F99E7100FFA47100FFA97600
                FFAF7C009E9E9E00BFA09000A0A0A000A4A4A400B8B8B800D9A68500D2A08800
                DBAB8C00DBAC8F00D7A89000D6A89500DBAD9100DCAE9300D7AC9800DAB09C00
                DCB49F00FFBA8700FFBB8800DDB7A700DFBDAE00FFCC9900E2C4B600E3C6BA00
                E5C7B900E6CBBF00E7CDBF00E8CDBF00E7CDC000EBD4C700E9D3C900EEDED500
                F0DDD200F0E1D900F2E4DD00F4E9E50000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000000000000000000000000000000000000000000000FFFFFF0000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000000000002020202026C600000000000000000000000000000000000000
                0000000000000000000000000000000000000000000091131412081823000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000001389AA9B3B0806C5000000000000000000000000000000000000
                000000000000000000000000000000000000002156567789AA9B3B2E0E072400
                0000000000000000000000000000000000000000000000000000000000000000
                0000213889898A9F9D783B3D330C192300000000000000000000000000000000
                000000000000000000000000000000000000568989899FBC463B3B42423B0806
                C500000000000000000000000000000000000000000000000000000000000000
                0000568989899FBC463B3B42423B2E0E07240000000000000000000000000000
                000000000000000000000000000000000000568989899FBC463B3B42423B3B31
                0D1A230000000000000000000000000000000000000000000000000000000000
                0000568989899FBC463B3B42423B3B3E430B0600000000000000000000000000
                000020202020000000000000000000002200568989A19D463F3D42403D3B4042
                3E0A0600000000000000000000000000002600000108180000000000000000C2
                1E13778989AA9B3B3B3E433E3B3B42423B0806C5000000000000000000000000
                00000000083B080600000000000000061389898989AA9B3B3B3E433E3B3B4242
                3B08060000000000000000002020215656565604083B2E0E0700000000000006
                1389898989AA9B3B3B3E433E3B3B423A08000000000000000000000000043889
                89898913083B3B310D1A0000000022021389898989AA9B3B3B3E433E3B3B4231
                0000000000000000000000000056898989898913083B3B3E430B060000220000
                1389898989AA9B3B3B3E433E3B3B423100000000000000000000000000213889
                8989897737082E3D3E351006061756567789898BAA7A443B40423E3D3D42402C
                000000000000000000000000002013898989898956002833292E2B1313388989
                8989899FBC463B3B42423B3B3E43312F20000000000000000000000000201389
                898989895600282900001389898989898989899FBC463B3B42423B3B3E430B20
                0000000000000000000000000000911377898989560003115656778989898989
                8989899FBC463B3B42423B3B3E430B2000000000000000000000000000000000
                568989897713133889898989897956798989899FBC463B3B42423B3B3E430B20
                0000000000000000000000000000000056898989898989898989898989560056
                8989899FBC463B3B42423B3B3E430B2000000000000000000000000000000000
                568989898989898989897713130400568989A19D463F3D42403D3B40423E0A20
                0000000000000000000000000000002053778989898989795656391716050056
                8989AA9B3B3B3E433E3B3B423D281B0000000000000000000000000000000000
                2013898989898956000017BCBC8E00568989AA9B3B3B3E433E3B3B4231000000
                000000000000000000000000000000002013898989898956000C3646A08E0056
                8989AA9B3B3B3E433E3B3B423100000000000000000000000000000000000000
                0021567989898977130F2E3B585413778989AA9B3B3B3E433E3B3B4231000000
                0000000000000000000000000000000000000056898989898913083B08138989
                8989AA9B3B3B3E433E3B3B423100000000000000000000000000000000000000
                00000056898989898913083B081389898BAA7A443B40423E3D3D42402C000000
                0000000000000000000000000000000000002053778989898938112903138989
                9FBC463B3B42423B3B3E43312F20000000000000000000000000000000000000
                000000201389898989895600001389899FBC463B3B42423B3B3E430B20000000
                0000000000000000000000000000000000000020138989898989560000138989
                9FBC463B3B42423B3B3E430B1F0000000000000000DE00000000000000000000
                000000002156798989897713133889899FBC463B3B42423B3B3E430B20000000
                0000E1A8A8C7E000000000000000000000000000000056898989898989898989
                9FBC463B3B42423B3B3E430B200000000000B0666685CBE30000000000000000
                000000000000568989898989898989A19D463F3D42403D3B40423E0A20000000
                00B38264647288CDE000000000000000000000000000568989898989898BAABD
                9E3B3B3E433E3B3B423D281B27000000B4826664646266A4CDE4000000000000
                00000000000056898989898989AAD6D6BB3B3B3E433E3B3B423100000000E3B4
                696B644D4D5D6A6B95D0E3000000000000000000000021388989A1C0C0C0C0C0
                BEBA453D3E40403B3D2E010000DBB3826B625D4A4A4D62627488CDD900000000
                0000000000002014AAAABCD3D3C1BCBCBFD39C453B40423E3D29010000B2815D
                624D4D4D5D4D4D5D625098B200000000000000000000201CD6D6D3BCBCBCBCBC
                BCBCD2BB3B3B3E433E29000000514B4C484A5D6B6B644A484850525100000000
                00000000000000931CAFBFBCBCBCBCBCBCBCBFBEBA453D3E402E000000C8B084
                505D64646A64644D5999B7C8000000000000000000000000208C97BCBCBCBCBC
                BCBCBCBFD39C453B3330200000DAB9945064646A6A6A6A667FADD0DA00000000
                0000000000000000002017BCBCBCBCBCBCBCBCBCBCD2BB3B0820000000DCA874
                6B6B6A6A6A6A6A6A6B7FCADC0000000000000000000000000000911798BCBCBC
                BCBCBCBCBCBFBEBA1520000000AD85746B6B6A6A6A6A6A6B727498AD00000000
                0000000000000000000000208C97BCBDBCBCBDBCBC978FA29000000000A7A595
                83726A6A6A6A6A8088A39AA7000000000000000000000000000000002017BCBC
                BCBCBCBCBC1700000000000000D7D5CBA780646A6A6A6A94B8D7DFDF00000000
                00000000000000000000000000911798BCBC551717C300000000000000000000
                B6947B7B7B7B819ACC000000000000000000000000000000000000000000008E
                BCBD1700000000000000000000000000D4965B5B5B5B5BADD9000000FFFFFFFF
                FFFF0000FFFFFFE07FFF0000FFFFFFC07FFF0000FFFFFF003FFF0000FFFFFE00
                1FFF0000FFFFFC000FFF0000FFFFF80007FF0000FFFFF80003FF0000FFFFF800
                01FF0000FFFFF80001FF0000FC3FF00001FF0000F81FE00000FF0000E00FE000
                01FF00000007E00007FF00000003C00007FF00000001800007FF000080000000
                07FF00008000000007FF0000800000000FFF0000C00000000FFF0000E0000000
                0FFF0000E00000000FFF0000E00000000FFF0000E00000001FFF0000F0000000
                3FFF0000F00000003FFF0000F80000003FFF0000FC0000003FFF0000FC000000
                3FFF0000FC0000003FFF0000FE0000007FFF0000FE0000007FBF0000FF000000
                7C1F0000FF8000007C0F0000FF80000078070000FF80000070030000FF800001
                C0010000FFC0000180000000FFC0000180000000FFC0000180000000FFE00001
                80000000FFF0000180000000FFF8000380000000FFFC000380000000FFFE0007
                80000000FFFF000F80000000FFFF803FF0070000FFFFC07FF007000028000000
                2000000040000000010008000000000000040000000000000000000000010000
                00010000000000000A0000001D1D1D0023000000320201003E0000002B110800
                38160A003D160A00241D1D00311A1A00351A1A0027201E0035201500450A0500
                490B05004911080049160A004E160A004E1F0F004A14140054141400531A1100
                760402007B000000641A0D0041211400452317004E2F1F00542C1B0064271300
                6D251200682F1C004B2C2C00552121005E2121004E3025004B332B004E322800
                5C3426005C382B004D3636004D3939004A3C3B0052363600503B3300523C3500
                543E370053383800663526006C3434004E433D00404040004E4B4B0051464100
                634D4200784E4000794A4A006352470065524B0060515100645751006D555500
                645D57007D5555007E5957006C6C6C0078746F00777777007872700079757500
                8F0603008F0D0D00920705009E000000970D0D008D121200A3010100A5000000
                A8000000AE000000A70D0D00BA000000BD0000009A3D1E00A7231A00C0000000
                C5000000C9000000CC000000D1000000D5000000D5050300DB000000DD000000
                D10D0800D1110B00D51B1200E1000000E6000000EA000000ED000000F1000000
                CF271A00D4231A00D9302300D9302400B84E0500BD510100B9510B00BB5B1F00
                AC472500B0462300B8472300BD4F2A00A7563800BC612900BC632D00BD683600
                B9653900B9663C00BF6D3F00C0530200C0540100C55A0100C75C0200C2590700
                C45B0700C75E0700C85C0100CC5F0000C0570800C65E0C00CC600000CB600400
                C8610A00C1611B00C7641800C6661E00C64F2700D2542900E25A2D00E55B2D00
                E85D2E00E85F3000EB5B3B00EC5B3B00EA5C3D00C3652100C7692300C76A2500
                C86A2000C86C2500C86E2A00C46D3200CA733300C8743900C9783F00F2603000
                F8633100FF663300FF683500FF6F3C00FF703D0085585800AA6B4B00A67A5E00
                80726E00807A780094777700947978009A7B7B009B7F7D00C9794200C5734C00
                C0765000C07A5400C47C5400CC7A5100C87F5300D27E5400FC704000F3714900
                F4704800F3774F00F4764D00FF7C4900F47A5200F67E5400C7805800C6825C00
                CB825A00E5895B00FF835000FF885500FF8B5800CD906E00CC907100CF947100
                CF987D00D4967300D1997B00F18D6800F2916100F8956300FF996600FF9E6B00
                F69B7100F69F7500D7A17D00D2A67C00FEA06E00FFAF7C00A7908F00A8939200
                D7A48400DAA78500D8A88D00D7AC9600DAAC9100DBAF9500DCAF9400DAAF9A00
                DEB39900F8B48200FFB58200DCB6A300FFC08D00EBCEBA00E6CDC200ECD2C000
                EDDBD300EFE0D900000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                FFFFFF0000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000026262146000000000000
                0000000000000000000000000000000000000000071EA3B64F14D60000000000
                00000000000000000000000000000000000000388D90B593584F22AA00000000
                000000000000000000000000000000000000078DA0A3B8605B654F14D5000000
                000000000000000000000000000000000000078DA0A3B6605D65574D23A90000
                000000000000000000000000000000000000078DA1B593605D63585D530B0000
                000000000000000002092C000000000000A71E8FA2B6605B665B5B654D0B0000
                000000000000070706044F1400000000001BA0A0A3B6585B665B5B634CA40000
                000000000C318D8D700E574D32000000441BA0A0A3B6585D665B5B5739000000
                000000002F72A0A08C194F58581500A8071EA0A1B5935B5D6558635240000000
                000000000027A0A0A07005524D491B208D8FA0A2B6615B65585D634BAB000000
                00000000003B8C9FA08B0317071EA0A0A0A0A0A3BB605B65585B572A00000000
                000000000000138DA08C071F8D8FA09F8C9FA0A3B6605B65585D572A00000000
                000000000000078DA0A0A0A0A09F8B54078DA1B593605D65585D532A00000000
                0000000000002F72A0A0A09F8B6F1D1A078DA2B6605B665B5B63480000000000
                0000000000000024A0A0A08B0016BBAE078DA3B6585D665B5B53400000000000
                000000000000003B8CA0A08C104767731E8FA3B6585B665B5B57400000000000
                0000000000000000078DA0A08B0F4A10A1A1B5935B5D65585D53410000000000
                00000000000000002D72A0A08C191813A0A2B6615B65585D634BAB0000000000
                00000000000000000024A0A0A08B0007A0A3BB605B65585D5729000000000000
                0000000000000000003B8CA0A08C071EA0A3B6605B65585B572C00000000D9DF
                00000000000000000000078DA0A0A0A0A1B593605D65585D5330000000D87E8A
                DBE60000000000000000078DA0A0A0A1C1BB605B665B5D63483E0000C8977E86
                9BDB00000000000000002F72A0A1BAC3E3D0685B655B5B52350000D1847E7B6C
                8697DFE40000000000000028BAC2E1D4CED3CA6A5B635B4D3C00C6947E7B6C6C
                6C849AC9000000000000003FD1E0CDCDCDCDD3D0685B654F3400776E6B7B8586
                6B6D747700000000000000003AB0CDCDCDCDCDCECA6A58514200DEB37B868686
                8688C4E20000000000000000003FB4CCCDCDCDCDD3D0552A0000D79985868686
                86809DDB0000000000000000000037AFCDCDCDCDCBBFA6430000BD9C8A858686
                8599ADBD00000000000000000000003CB4CCCBB3A53346000000E7E5B1898087
                97C5E8000000000000000000000000001CC0B30D3600000000000000C7767475
                79DA0000FFFFFFFFFFFFC3FFFFFF01FFFFFE00FFFFFC007FFFFC003FFFFC003F
                F1F8003FC0F8003F0070007F0020007F8000007F800000FFC00000FFC00000FF
                C00001FFE00001FFE00001FFF00001FFF00001FFF80003FFF80003CFFC000383
                FC000303FC000600FE000400FE000400FF000400FF800C00FFC00C00FFE01C01
                FFF07F0328000000100000002000000001000800000000000001000000000000
                0000000000010000000100000000000035140A004D0C07005010070055190C00
                6C0B0500710D0D007D371D007A50460078625C00960505009900000099050500
                861B1B00811D1D008D381C0091321900993D1E0081272700863D240090363600
                C3000000C2040400C4040400CB000000D1000000D5000000D9000000D91E1400
                E1000000DD392A00BD520100BF570B00BA462300BF4C2600C0550400C4590400
                CC5F0000C65E0B00C55E0C00CD600000CA620900CA630E00C2621C00D9572C00
                CC5B3D00E2442D00E55B2D00EC573700F2603000FB643200FF663300FF673400
                FF693600FF6B3800FB6E3E00875D4F00885F500080655C00B0694600BF734C00
                B67250008A726700887774008C787200AA6A6A00C6794900C57C5100F66B4100
                F9714300FF754200FB7546009C817B00B6856300CC825000C7835D00CA855B00
                CC885C00D5987700EC946700FF996600FF9B6800FC9D6D00FCA87700AB818100
                AB868500AB939100D8A28100DCAA870000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000FFFFFF000000000000000000004856000000000000000000
                0000000011440C5500000000000000000000000F362E1D0A5500000000003F00
                00000011361C1D1B06000000400F040A0000093445191D19410000003834210C
                0A3A1135301B1B170000000000222F03113431362E1D190E0000000000133434
                2C070F361C1D1B14000000000000312F022D1145191D19000000000000001334
                100535301B1B170000000000000000312F01362E1D190E00000000000000000F
                3434451C1D1B120057265800000000394551531E1D17004E241F265700000000
                495050511E1700432325204B00000000003D50514F08004A2928294D00000000
                00003C3B3E000000422B4C00FF9F0000FF0F0000FE070000DE0700000C070000
                000F0000800F0000800F0000C01F0000C01F0000E01F0000E0110000E0200000
                F0200000F8200000FC7100002800000030000000600000000100200000000000
                8025000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000AA000000AA000000AA000000AA343434AA777777840000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000000000000000000000000000000000000000000005000000131C0B058D
                542110FF542D1CFF4F2216FF430000FF4B3434FF5C5C5CE30000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000007000000FF000000FF542110FF
                FF6633FFFF8754FFEE6644FFCC0000FF440000FF272727FF777777AA77777702
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000001A38160BC6A94321FFA94321FFC64F27FF
                FF6633FFFF8754FFEE6644FFCC0000FF9E0000FF6F0D0DFF3E2727FF645C5CE3
                7777770300000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000838160BC68D381CFFFF6633FFFF6633FFFF6B38FF
                FF7643FFF36B44FFE2442DFFCC0000FFD70000FFB10000FF5A0000FF533434FF
                5C5C5CE300000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000FFA94321FFFF6633FFFF6633FFFF6633FFFF7643FF
                FF9966FFDD3322FFCC0000FFCC0000FFED0000FFEE0000FFCC0000FF440000FF
                272727FF777777AA777777030000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000FFA94321FFFF6633FFFF6633FFFF6633FFFF7643FF
                FF9966FFDD3322FFCC0000FFCC0000FFED0000FFEE0000FFCC0000FF9E0000FF
                6F0D0DFF3E2727FF645C5CE37777770300000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000FFA94321FFFF6633FFFF6633FFFF6633FFFF7643FF
                FF9966FFDD3322FFCC0000FFCC0000FFED0000FFEE0000FFCC0000FFCC0000FF
                AB0000FF6B0000FF583434FF5C5C5CE300000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000008000000FFA94321FFFF6633FFFF6633FFFF6633FFFF7643FF
                FF9966FFDD3322FFCC0000FFCC0000FFED0000FFEE0000FFCC0000FFCC0000FF
                DC0000FFFF0000FF550000FF272727FF77777704000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000000000000000000001000000A9000000A9000000A9000000A934343444
                0000000000000000000000000000000000000000000000000000000000000000
                77777704343434C6000000FFA94321FFFF6633FFFF6633FFFF7C49FFF36B44FF
                DD3322FFD1110BFFD70000FFED0000FFE20000FFD70000FFCC0000FFE20000FF
                E80000FFDD0000FF490000FF272727FF77777772000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000000000000C0000008D000000FF000000FF160000FF430000FF4B3434FF
                5C5C5C0500000000000000000000000000000000000000000000000000000000
                5C5C5C9751403AFF542110FFC64F27FFFF6633FFFF6633FFFF8754FFEE6644FF
                CC0000FFCC0000FFDC0000FFFF0000FFDD0000FFCC0000FFCC0000FFED0000FF
                EE0000FFCC0000FF440000FF272727FF777777B1000000000000000000000000
                000000000000000000000000000000000000000000000000000000000000001A
                000000FF000000FF000000FF000000FF000000FF430000FFCC0000FF440000FF
                272727FF00000000000000000000000000000000000000000000000000000000
                272727FF542110FFFF6633FFFF6633FFFF6633FFFF6633FFFF8754FFEE6644FF
                CC0000FFCC0000FFDC0000FFFF0000FFDD0000FFCC0000FFCC0000FFED0000FF
                EE0000FFCC0000FF440000FF272727FF7777773D000000000000000000000000
                0000000000000000000000000000000000000000000000A9000000A938160BC6
                A94321FFA94321FFA94321FFA94321FF38160BFF430000FFCC0000FF9E0000FF
                6F0D0DFF3E2727FF645C5C15000000000000000000000000000000007777770D
                272727FF542110FFFF6633FFFF6633FFFF6633FFFF6633FFFF8754FFEE6644FF
                CC0000FFCC0000FFDC0000FFFF0000FFDD0000FFCC0000FFCC0000FFED0000FF
                C00000FF440000FF710000620000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000FF38160BFF8D381CFF
                FF6633FFFF6633FFFF6633FFFF6633FF552211FF430000FFCC0000FFCC0000FF
                AB0000FF6B0000FF583434FF5C5C5C1000000000000000006969690A4F4F4FE3
                1A1A1AFF542110FFFF6633FFFF6633FFFF6633FFFF6633FFFF8754FFEE6644FF
                CC0000FFCC0000FFDC0000FFFF0000FFDD0000FFCC0000FFCC0000FFED0000FF
                AA0000FF000000FF000000010000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000FFAA4422FFFF6633FF
                FF6633FFFF6633FFFF6633FFFF6633FF552211FF430000FFCC0000FFCC0000FF
                DD0000FFFF0000FF550000FF272727FF77777704777777104F4F4FE3000000FF
                000000FF542110FFFF6633FFFF6633FFFF6633FFFF6633FFFF8754FFEE6644FF
                CC0000FFCC0000FFDC0000FFFF0000FFDD0000FFCC0000FFCC0000FFED0000FF
                AA0000FF000000FF000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000000000004238160BC68D381CFF
                FF6633FFFF6633FFFF6633FFFF6633FFC64F27FF872D16FF440000FF9E0000FF
                D10000FFDD0000FFBA0000FF7E0D0DFF272727FF272727FF533125FFA94321FF
                A94321FFC64F27FFFF6633FFFF6633FFFF713EFFFF8754FFE84F33FFD72216FF
                CC0000FFE20000FFE80000FFDD0000FFD10000FFD70000FFED0000FFE20000FF
                930000FF000000FF000000000000000000000000000000000000000000000000
                000000000000000000000000000000000000000000000000000000AA542110FF
                FF6633FFFF6633FFFF6633FFFF6633FFFF6633FFAA4422FF000000FF870000FF
                B50000FF880000FF9E0000FF8D0B05FF542110FF542110FF8D381CFFFF6633FF
                FF6633FFFF6633FFFF6633FFFF6633FFFF7643FFFF9966FFDD3322FFCC0000FF
                CC0000FFED0000FFEE0000FFCC0000FFCC0000FFDC0000FFFF0000FFAF0000FF
                710000E2000000AA000000000000000000000000000000000000000000000000
                000000000000000000000000000000000000000000000000000000AA542110FF
                FF6633FFFF6633FFFF6633FFFF6633FFFF6633FFAA4422FF000000FF870000FF
                880000FF000000FF000000FF542110FFFF6633FFFF6633FFFF6633FFFF6633FF
                FF6633FFFF6633FFFF6633FFFF6633FFFF7643FFFF9966FFDD3322FFCC0000FF
                CC0000FFED0000FFEE0000FFCC0000FFCC0000FFDC0000FFFF0000FF550000FF
                000000AA00000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000001F1C0B058D
                552211FFC64F27FFFF6633FFFF6633FFFF6633FFAA4422FF000000FF2D0000FF
                66160BFFA94321FFA94321FFC64F27FFFF6633FFFF6633FFFF6633FFFF6633FF
                FF6633FFFF6633FFFF6633FFFF6633FFFF7643FFFF9966FFDD3322FFCC0000FF
                CC0000FFED0000FFEE0000FFCC0000FFCC0000FFDC0000FFFF0000FF550000FF
                000000AA00000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000054
                000000FFA94321FFFF6633FFFF6633FFFF6633FFC64F27FF542110FF542110FF
                8D381CFFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FFE25A2DFFAA4422FF
                E25A2DFFFF6633FFFF6633FFFF6633FFFF7643FFFF9966FFDD3322FFCC0000FF
                CC0000FFED0000FFEE0000FFCC0000FFCC0000FFDC0000FFFF0000FF550000FF
                000000AA00000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000054
                000000FFA94321FFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FF
                FF6633FFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FFAA4422FF000000FF
                A94321FFFF6633FFFF6633FFFF6633FFFF7643FFFF9966FFDD3322FFCC0000FF
                CC0000FFED0000FFEE0000FFCC0000FFCC0000FFDC0000FFFF0000FF550000FF
                000000AA00000000000000000000000000000000000000000000000000000000
                000000000000000000000000000000000000000000000000000000000000004A
                000000FFA94321FFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FF
                FF6633FFFF6633FFFF6633FFC64F27FF552211FF552211FF38160BFF000000FF
                A94321FFFF6633FFFF6633FFFF7C49FFF36B44FFDD3322FFD1110BFFD70000FF
                ED0000FFE20000FFD70000FFCC0000FFE20000FFE80000FFDD0000FF490000FF
                000000AA00000000000000000000000000000000000000000000000000000000
                000000000000000000000000000000000000000000000000000000000000001F
                000000AA712D16E2C64F27FFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FF
                E25A2DFFAA4422FFAA4422FF8D3E22FF543221FF543221FF382116FF000000FF
                A94321FFFF6633FFFF6633FFFF8754FFEE6644FFCC0000FFCC0000FFDC0000FF
                FF0000FFDD0000FFCC0000FFCC0000FFED0000FFD70000FF880000FF5A0000C6
                0000000200000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000A9542110FFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FF
                AA4422FF000000FF000000FF543221FFFF9966FFFF9966FFAA6644FF000000FF
                A94321FFFF6633FFFF6633FFFF8754FFEE6644FFCC0000FFCC0000FFDC0000FF
                FF0000FFDD0000FFCC0000FFCC0000FFED0000FFAA0000FF000000FF00000002
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000A9542110FFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FF
                AA4422FF000000FF5A0000FFA4110BFFDD3322FFF3774FFFAA6644FF000000FF
                A94321FFFF6633FFFF6633FFFF8754FFEE6644FFCC0000FFCC0000FFDC0000FF
                FF0000FFDD0000FFCC0000FFCC0000FFED0000FFAA0000FF000000FF00000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000000000006438160BC6AA4422FFE25A2DFFFF6633FFFF6633FFFF6633FF
                C64F27FF542110FF770B05FF9E0000FFCC0000FFB5442DFF8D4F33FF542110FF
                C64F27FFFF6633FFFF6633FFFF8754FFEE6644FFCC0000FFCC0000FFDC0000FF
                FF0000FFDD0000FFCC0000FFCC0000FFED0000FFAA0000FF000000FF00000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000004A000000FFA94321FFFF6633FFFF6633FFFF6633FF
                FF6633FFFF6633FF552211FF430000FFCC0000FF440000FF542110FFFF6633FF
                FF6633FFFF6633FFFF6633FFFF8754FFEE6644FFCC0000FFCC0000FFDC0000FF
                FF0000FFDD0000FFCC0000FFCC0000FFED0000FFAA0000FF000000FF00000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000000000000000000038000000FFA94321FFFF6633FFFF6633FFFF6633FF
                FF6633FFFF6633FF552211FF430000FFCC0000FF440000FF542110FFFF6633FF
                FF6633FFFF713EFFFF8754FFE84F33FFD72216FFCC0000FFE20000FFE80000FF
                DD0000FFD10000FFD70000FFED0000FFE20000FF930000FF000000FF00000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000001F000000AA712D16E2C64F27FFFF6633FFFF6633FF
                FF6633FFFF6633FF8D381CFF65160BFF880000FF2D0000FF542110FFFF6633FF
                FF6633FFFF7643FFFF9966FFDD3322FFCC0000FFCC0000FFED0000FFEE0000FF
                CC0000FFCC0000FFDC0000FFFF0000FFAF0000FF710000E2000000AA00000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000A9542110FFFF6633FFFF6633FF
                FF6633FFFF6633FFFF6633FFAA4422FF000000FF000000FF542110FFFF6633FF
                FF6633FFFF7643FFFF9966FFDD3322FFCC0000FFCC0000FFED0000FFEE0000FF
                CC0000FFCC0000FFDC0000FFFF0000FF550000FF000000AA0000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000A9542110FFFF6633FFFF6633FF
                FF6633FFFF6633FFFF6633FFAA4422FF000000FF000000FF542110FFFF6633FF
                FF6633FFFF7643FFFF9966FFDD3322FFCC0000FFCC0000FFED0000FFEE0000FF
                CC0000FFCC0000FFDC0000FFFF0000FF550000FF383838E20000000000000000
                0000000000000000000000000000000000000000D5A2882CDDB6A096FCF9F733
                0000000000000000000000000000000000000000000000000000000000000000
                000000000000000000000000000000000000006438160BC6AA4422FFE25A2DFF
                FF6633FFFF6633FFFF6633FFC64F27FF542110FF542110FF8D381CFFFF6633FF
                FF6633FFFF7643FFFF9966FFDD3322FFCC0000FFCC0000FFED0000FFEE0000FF
                CC0000FFCC0000FFDC0000FFFF0000FF550000FF545454FF0000000000000000
                000000000000000000000000E4C1AD8BCE895EFFCE895EFFD9A685FFEFDED4FF
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000000000000000000004A000000FFA94321FF
                FF6633FFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FF
                FF6633FFFF7643FFFF9966FFDD3322FFCC0000FFCC0000FFED0000FFEE0000FF
                CC0000FFCC0000FFDC0000FFFF0000FF550000FF545454FF0000000000000000
                0000000000000000E4C5B51ECF8D63FFC0580AFFC0580AFFC77236FFD7A890FF
                F1E1D9E300000000000000000000000000000000000000000000000000000000
                000000000000000000000000000000000000000000000038000000FFA94321FF
                FF6633FFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FF
                FF7C49FFF36B44FFDD3322FFD1110BFFD70000FFED0000FFE20000FFD70000FF
                CC0000FFE20000FFE80000FFDD0000FF490000FF545454FF0000000000000000
                00000000E5C8BC02D29165FFC96D28FFC85D03FFC95D03FFC96616FFC9763BFF
                DBAD91FFEDDBD2E3000000000000000000000000000000000000000000000000
                000000000000000000000000000000000000000000000038000000FFA94321FF
                FF6633FFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FFFF713EFFFF8754FF
                FF9E6BFFEE714FFFCC0000FFCC0000FFDC0000FFFF0000FFDD0000FFCC0000FF
                CC0000FFED0000FFD70000FF880000FF5A0000C6545454C60000000000000000
                E7C9B842D1936EFFC96F2BFFC65D07FFC75B00FFC85B00FFC75C04FFC65E0BFF
                CD814DFFDCAE93FFF2E4DEC70000000000000000000000000000000000000000
                000000000000000000000000000000000000000000000038000000FFA94321FF
                FF6633FFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FFFF8754FFFFCC99FF
                FFCC99FFEE8866FFCC0000FFCC0000FFDC0000FFFF0000FFDD0000FFCC0000FF
                CC0000FFED0000FFAA0000FF000000FF0000002CFDFBFB0400000000E5C9BB8B
                D2956FFFC35F15FFC95F07FFC75B00FFBD5202FFBD5202FFC25601FFCD6000FF
                C65F0DFFCA7941FFDAB09CFFF1E1DBE300000000000000000000000000000000
                0000000000000000000000000000000000000000000000090000004B38160BC6
                8D381CFFFF6633FFFF6633FFFF7C49FFFFA976FFFFA976FFFFA976FFFFAA77FF
                FFAA77FFF99E71FFED8765FFD72D21FFD10000FFDD0000FFE80000FFE20000FF
                CC0000FFD70000FF9A0000FF160000FF0700000800000000E5C7B9E3D19065FF
                C86D29FFCA6007FFC55903FFC05401FFBA4F02FFBA4F02FFBD5102FFC35601FF
                C75D05FFC96718FFC8743BFFDBAC8FFFE5C7B9FF000000000000000000000000
                00000000000000000000000000000000000000000000000000000000000000A9
                542D1CFFFF8754FFFF8754FFFF9865FFFFBB88FFFFBB88FFFFAF7CFFFF9966FF
                FF9966FFFFA471FFFFBB88FFE86B4FFFD72D21FFCC0000FFE20000FFE80000FF
                DC0000FFD10000FF8F0000FF160000FF1E00001200000000CA8D6EFFC1672BFF
                C05607FFC65901FFBF5301FFBD5102FFBF5302FFBF5402FFBE5302FFBC5002FF
                C25601FFC35804FFBD540AFFC67A4DFFCA8D6EFF000000000000000000000000
                00000000000000000000000000000000000000000000000000000000000000A9
                544332FFFFCC99FFFFCC99FFFFBB88FFFF9966FFFF9966FFFF9966FFFF9966FF
                FF9966FFFF9966FFFF9966FFFFBA87FFEE8866FFCC0000FFCC0000FFDC0000FF
                FF0000FFDD0000FF880000FF000000FF00000038AC4B1B12AC4B1BFFB44E0FFF
                B84E08FFB64B04FFB84D03FFC05302FFCD6000FFCD6000FFC75A00FFBA4E03FF
                B74C03FFB74C05FFB9500AFFB04C15FFAC4B1BFF000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000025
                1C16118D554433FFC68860FFFFA471FFFF9966FFFF9966FFFF9966FFFF9966FF
                FF9966FFFF9966FFFF9966FFFFA471FFF99E71FFED8765FFD72D22FFD10000FF
                DD0000FFE80000FF9E0000FF000000FF00000055B8673F38D2A088FFCA8967FF
                C16F3DFFB9510BFFC25704FFC85B00FFCD6000FFCD6000FFCB5E00FFC65901FF
                BD5408FFBD6024FFC67E56FFCE9577FFD2A088FF000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000025000000AA71442DE2C6774FFFFF9966FFFF9966FFFF9966FFFF9966FF
                FF9966FFFF9966FFFF9966FFFF9966FFFFA471FFFFBB88FFE86B4FFFD72D21FF
                CC0000FFB50000FF8D0000E2000000AA00000019E6CBBF38E6CBBFFFD39D7FFF
                C77743FFC0580BFFC85D03FFCD6000FFCD6000FFCD6000FFCD6000FFCD6000FF
                C55B07FFC36827FFCA8760FFDCB49FFFE6CBBFFF000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000A9543221FFFF9966FFFF9966FFFF9966FFFF9966FF
                FF9966FFFF9966FFFF9966FFFF9966FFFF9966FFFF9966FFFFBA87FFEE8866FF
                CC0000FF440000FF000000AA0000000000000000E8CDBF1CE8CDBFFFD08A59FF
                C7661AFFCD6101FFCD6000FFCD6000FFCD6000FFCD6000FFCD6000FFCD6000FF
                CD6000FFCA630DFFC46927FFDBAB8CFFE7CDBFFF000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000251C110B8D553322FFC6774FFFFF9966FFFF9966FF
                FF9966FFFF9966FFFF9966FFFF9966FFFF9966FFFF9966FFFFA471FFF99E71FF
                ED8765FF4F2D22FF000000AA0000000000000000DDB5A11CC88765FFC67034FF
                C56318FFC7610FFFCB6105FFCD6000FFCD6000FFCD6000FFCD6000FFCD6000FF
                C9610AFFC66213FFC4641CFFC77B4CFFC88765FF000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000000000000000000000000025000000AA71442DE2C6774FFFFF9966FF
                FF9966FFFF9966FFFF9966FFFF9966FFFF9966FFFF9966FFC6774FFFAA714FFF
                AA8866FF715A44C6000000640000000000000000BD6E451CC5825FFFCB8050FF
                CA7940FFC46B30FFC96411FFCC6001FFCD6000FFCD6000FFCD6100FFCC6102FF
                C76820FFC77238FFCD8049FFC88157FFC5825FFF000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000A9543221FFFF9966FF
                FF9966FFFF9966FFFF9966FFFF9966FFFF9966FFFF9966FF553322FF000000FF
                000000FF0000004B000000000000000000000000DFBDAE1CDFBDAEE3DFBDAEFF
                D6A895FFC28063FFC86B24FFCC6003FFCD6000FFCD6000FFCD6101FFCC6105FF
                C67643FFCC947BFFDFBDAEE3DFBDAEAADFBDAEAA000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000251C110B8D553322FF
                C6774FFFFF9966FFFF9966FF8D5538FF553322FF553322FF8D54388D00000055
                0000004B00000006000000000000000000000000E5CBBF1CF3E8E344F4E8E319
                E6CABE42C88D72FFC2713EFFC06223FFC06221FFC16221FFC06322FFC06324FF
                C67F59FFD7AC98FFF3E8E327F3E8E322F3E8E318000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000004A000000FF
                A96543FFFF9966FFFF9966FF553322FF000000FF000000FF0000004200000000
                0000000000000000000000000000000000000000FEFEFE1CFEFEFE5500000000
                EEDBD36ECC947BAAC0744BFFBA6433FFBA6433FFBA6433FFBA6433FFBA6434FF
                C58463FFDCB7A7C7FEFEFE580000000000000000FFFFFFFFFFFF0000FFFFFFE0
                7FFF0000FFFFFF007FFF0000FFFFFE001FFF0000FFFFFC000FFF0000FFFFF800
                0FFF0000FFFFF80003FF0000FFFFF80001FF0000FFFFF80001FF0000FFFFF000
                00FF0000F81FE00000FF0000F00FE00000FF0000C00FE00000FF00000003C000
                03FF00000001800003FF00000000000007FF00000000000007FF000080000000
                07FF0000800000000FFF0000800000000FFF0000C00000000FFF0000C0000000
                0FFF0000C00000000FFF0000C00000000FFF0000F00000001FFF0000F0000000
                3FFF0000F00000003FFF0000F80000003FFF0000F80000003FFF0000F8000000
                3FFF0000FE0000007FFF0000FE0000007F1F0000FE0000007C1F0000FF000000
                780F0000FF00000070070000FF00000060030000FF00000040010000FF000000
                80000000FFC0000080000000FFC0000000000000FFC0000000000000FFE00000
                00000000FFF8000300000000FFF8000300000000FFFC000300000000FFFF0007
                00000000FFFF000700000000FFFF803F20030000280000002000000040000000
                0100200000000000801000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000065201438632015386C271C3887413730000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000082291706
                43150C4238180DE2351910E2341111E2595454CD000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000983E22027F2D193238160BFF
                642713FFFF713EFFF37149FFA80000FF4A1414FF73504E9D9F29231A00000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000000000000000000000000000000000000032140B32572211CCE25A2DFF
                E85F30FFFC7141FFEB5B3BFFC90000FFA80202FF531E1EFB6B4544B700000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000000000000000000000000000000000000038160BFFE25A2DFFFF6633FF
                FF703DFFF3774FFFD1110BFFD70000FFED0000FFA80000FF4A1414FF724C4B9E
                9B1A1A1A00000000000000000000000000000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000000000000000000000000000000000000038160BFFE25A2DFFFF6633FF
                FF703DFFF3774FFFD1110BFFD70000FFED0000FFC60000FFA30202FF5C1E1EFB
                6B4242B700000000000000000000000000000000000000000000000000000000
                0000000000000000000000004D140B384C140C38561C142D72261B0700000000
                000000000000000000000000000000004322193038160BFFE25A2DFFFF6835FF
                FC7040FFEA5C3DFFD20D08FFDD0000FFE70000FFCE0000FFDB0000FFBD0000FF
                351A1AFF9428281B000000000000000000000000000000000000000000000000
                000000005F1D0F02290B0541000000E2090000E2341111D76F2C224200000000
                0000000000000000000000009546322256443EBF642713FFE85D2EFFFF6F3CFF
                F47048FFD00D08FFD60101FFF00000FFD40000FFD50000FFEB0000FFA30000FF
                311A1AFF82545468000000000000000000000000000000000000000058231138
                2910084A38160BFF38160BFF2B1108FF320201FFA80000FF4A1414FF7330243B
                82322202000000000000000074453857452317FFFF6633FFFF6633FFFF713EFF
                F37149FFCC0000FFD60000FFF30000FFD10000FFD70000FFE30000FF850000ED
                531313B48D3B3B1B00000000000000000000000000000000000000000C0402E2
                572211E8E25A2DFFE25A2DFFB04623FF450A05FFC60000FFA30202FF5C1E1EE5
                6649444200000000894F4104474747BC422014FFFF6633FFFF6633FFFF713EFF
                F37149FFCC0000FFD60000FFF30000FFD10000FFD70000FFC50000FF430101B5
                A706060B0000000000000000000000000000000000000000000000002C1108CE
                BC4B25F8FF6633FFFF6633FFD2542AFF641A0DFFA80000FFCF0000FFCA0000FF
                541414FF6D40355A544C4ABD38160BFF642713FFFF6633FFFF6835FFFC7241FF
                EC5B3BFFCE0000FFDB0000FFEA0000FFD20000FFDD0000FFBA0000FF3B0101AA
                00000000000000000000000000000000000000000000000000000000441B0D4A
                4A1D0EE5FF6633FFFF6633FFF86331FFB84723FF3E0000FFAE0000FFA50000FF
                920705FF452317FF682F1CFFE25A2DFFE85D2EFFFF6633FFFF6F3CFFF4764DFF
                D51B12FFD50000FFEB0000FFD00000FFD60000FFE60000FF920000F23C010184
                000000000000000000000000000000000000000000000000000000005D251229
                2B1108B9D2542AFFF86331FFFF6633FFC64F27FF230000FF760402FF38160BFF
                642713FFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FFFF703DFFF3774FFF
                D1110BFFD70000FFED0000FFCC0000FFD60000FFC60000FF1C0000C600000000
                0000000000000000000000000000000000000000000000000000000081331903
                270F07484E1F0FFFE55B2DFFFF6633FFD25429FF3D160AFF6D2512FFE25A2DFF
                E85D2EFFFF6633FFF26030FFD2542AFFF86331FFFF6633FFFF703DFFF3774FFF
                D1110BFFD70000FFED0000FFCC0000FFD60000FFC60000FF1C0000C600000000
                0000000000000000000000000000000000000000000000000000000000000000
                34140A3538160BFFE25A2DFFFF6633FFFF6633FFFF6633FFFF6633FFFF6633FF
                F26030FFC64F27FF9A3D1EFF38160BFFE25A2DFFFF6835FFFC7040FFEA5C3DFF
                D20D08FFDD0000FFE70000FFCE0000FFDB0000FFBD0000FF1B0000C600000000
                0000000000000000000000000000000000000000000000000000000000000000
                34140A1E2B1108D2BC4B25F8FF6633FFFF6633FFFF6633FFF26030FFC64F27FF
                AC4725FF542C1BFF412215FF38160BFFE25A2DFFFF6F3CFFF47048FFD00D08FF
                D60101FFF00000FFD40000FFD50000FFE00000FF890000F26D00006F00000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000006D2A153838160AE2FF6633FFFF6633FFFF6633FFC64F27FF0A0000FF
                531A11FFF67E54FFC5734CFF38160BFFE25A2DFFFF713EFFF37149FFCC0000FF
                D60000FFF30000FFD10000FFD70000FFC00000FF3E0101AA0000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000006B2A14292B1108B9D2542AFFF86331FFFF6633FFD25429FF491108FF
                8F0603FFCF271AFFA75638FF642713FFE85D2EFFFF713EFFF37149FFCC0000FF
                D60000FFF30000FFD10000FFD70000FFC00000FF3E0302AA0000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000000000000037150A2D38160BFFE25A2DFFFF6633FFFF6633FFC64F27FF
                490B05FF9E0000FF4E160AFFFF6633FFFF6835FFFC7241FFEC5B3BFFCE0000FF
                DB0000FFEA0000FFD20000FFDD0000FFBA0000FF3E0603AA0000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                000000000000000037160B1A2B1108D2BC4B25F8FF6633FFFF6633FFD2542AFF
                641A0DFF7B0000FF49160AFFFF6633FFFF6F3CFFF4764DFFD51B12FFD50000FF
                EB0000FFD00000FFD60000FFE60000FF920000F23E0804840000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                0000000000000000000000006F2B163838160AE2FF6633FFFF6633FFFF6633FF
                C64F27FF000000FF38160AFFFF6633FFFF703DFFF3774FFFD1110BFFD70000FF
                ED0000FFCC0000FFD60000FFC60000FF280C0CD2000000000000000000000000
                00000000AF603C04C0825C26D4986D0B00000000000000000000000000000000
                0000000000000000000000006C2C16292B1108B9D2542AFFF86331FFFF6633FF
                D25429FF38160AFF642713FFFF6633FFFF703DFFF3774FFFD1110BFFD70000FF
                ED0000FFCC0000FFD60000FFC60000FF4E3131F8000000000000000000000000
                C57B603DC78058AED7A282D0E5C1A77C00000000000000000000000000000000
                0000000000000000000000000000000036160C2D38160BFFE25A2DFFFF6633FF
                FF6633FFFF6633FFFF6633FFFF6835FFFC7040FFEA5C3DFFD20D08FFDD0000FF
                E70000FFCE0000FFDB0000FFBD0000FF533838FF0000000000000000BF654523
                D29268CDC25907FFC6671FFFDAAA8DF8DBA88481000000000000000000000000
                0000000000000000000000000000000035170C2538160BFFE25A2DFFFF6633FF
                FF6633FFFF6633FFFF6A37FFFF8350FFF47A52FFD00D08FFD60101FFF00000FF
                D40000FFD50000FFE00000FF890000F2543838D900000000CA775E1DC8784BC6
                C86A20FFC75B01FFC75E07FFCA7333FFDCAD91F8DBA47D580000000000000000
                0000000000000000000000000000000034170C1A2B1108D0BC4B25F8FF6633FF
                FF6A37FFFF7C49FFFF8B58FFFFC08DFFF69F75FFD4231AFFD50503FFED0000FF
                D70000FFD40000FFBA0000FF050000B47B22110CCE80696DD3976FE5C65E0CFF
                C55A02FFBC5102FFBE5301FFC85D03FFC86C25FFDBAD93F8DBA47E8700000000
                000000000000000000000000000000007A381F024E25154C4A2213E5FF7C49FF
                FF8855FFFFB582FFFFAF7CFFFF9E6BFFFDA06FFFF18D68FFD93023FFD70000FF
                E50000FFD50000FFA80000FF160000AE81281504CE916EF8C36521FFC55902FF
                BF5301FFBD5102FFBD5202FFC05401FFC45B07FFC46D32FFD1997BFF00000000
                00000000000000000000000000000000000000006C351F292B221ABAD2A67CFF
                F8B482FFFF9966FFFF9966FFFF9966FFFF9966FFFFA16EFFF69B71FFD4231AFF
                D50503FFED0000FFAA0000FF000000BF75381A2AB96539FFBB5B1FFFB84E05FF
                C05302FFCD6000FFC95C00FFBC5003FFB9510BFFBC6129FFB9663CFF00000000
                0000000000000000000000000000000000000000924A2C042C180F33422D20D2
                BF7750F8FF9966FFFF9966FFFF9966FFFF9966FFFF9A67FFFDA06EFFF18D68FF
                D93024FFCD0000FFA30000F2000000938F716234DAAF9AFFC87F53FFC05708FF
                C95D01FFCD6000FFCC5F00FFC75C02FFC1611BFFCD906EFFDCB6A3FF00000000
                000000000000000000000000000000000000000000000000000000007B452C29
                2B1A11BAD27E54FFF89563FFFF9966FFFF9966FFFF9966FFFF9966FFFFA16EFF
                F69B71FFA7231AFF170503C600000000D49C8712D7A484FFC86E2AFFCB6004FF
                CC6000FFCD6000FFCD6000FFCC6001FFC9610BFFC9783FFFD9AC92FF00000000
                00000000000000000000000000000000000000000000000000000000A3603F04
                301C133342271AD2BF734CF8FF9966FFFF9966FFFF9966FFFF9966FFF29262FF
                CB825AFFA27356F22119129A00000000C0735212C78058FFC87439FFC6661DFF
                CB6104FFCD6000FFCD6000FFCA6209FFC76A25FFC97942FFC6825CFF00000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000834F35292B1A11BAD27E54FFF89563FFF29160FFCC7A51FFA66442F2
                22140DCC150B07939048321100000000D19D8812E5CBBFAFDEBBACBBC47C54FF
                C76418FFC8600BFFC8610BFFC76923FFCC9071FFE5CBBF97E5CBBF7A00000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000AD6A4804321E14444E2F1FFFE5895BFFCC7A51FF352015FF22140DC8
                6538283194513A110000000000000000DFBCAE12E8D2C537DEBBAA42C78767D9
                BD6836FFBC632DFFBC632DFFBF6D3FFFD3A38BE6EED6C433D7966409FFFFC3FF
                FFFF03FFFFFC00FFFFFC00FFFFFC003FFFFC003FF0F8001FC0F0001F0030001F
                0020003F0000007F0000007F000000FF000000FF800000FF800000FFC00001FF
                C00001FFE00001FFE00001FFF00003C7F0000387F8000303F8000201F8000000
                F8000000FC000000FC000000FF000800FF000800FFC00800FFC0180028000000
                1000000020000000010020000000000040040000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                00000000812817124D1C118D60332E8600000000000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                6828164C993D1FFFF66B41FF990505FF7437359A000000000000000000000000
                0000000000000000000000000000000000000000000000000000000000000000
                8D381CFFFF6B38FFE2442DFFE20000FF970505FF7531319B0000000000000000
                00000000000000005C1A0D11290A058D5B2018530000000000000000743B2C44
                993D1EFFFB6E3EFFD91E14FFE20000FFDA0000FF710D0DFF8F28282000000000
                0000000039160B938D381CFF55190CFF970505FF6E322A59943C270150342BC4
                FF6633FFF97143FFD10000FFE20000FFD50000FF710606989517170600000000
                000000005E2512BDFF6633FFBA4623FF990000FF950706FF5B382DC5993D1EFF
                FF6936FFEC5737FFDA0000FFD90000FFC40000FB7503034B0000000000000000
                000000004C1E0E4BBF4C26FFE55B2DFF501007FF993D1EFFFB6432FFF26030FF
                FF6B38FFE2442DFFE20000FFD10000FF710000E2000000000000000000000000
                0000000067281415803319F2FF6633FFFB6432FFD9572CFF7D371DFF8D381CFF
                FB6E3EFFD91E14FFE20000FFD80000FF730000C9000000000000000000000000
                00000000000000004F1F0F7FF26030FFE55B2DFF4D0C07FFCC5B3DFF993D1EFF
                F97143FFD10000FFE20000FFCB0000FF7B060555000000000000000000000000
                00000000000000006B2A1511803319F2FF6633FF913219FF6C0B05FFFF6936FF
                EC5737FFDA0000FFD90000FFC40000FB7B100A4B000000000000000000000000
                0000000000000000000000004F1F0F7FF26030FFE55B2DFF35140AFFFF6B38FF
                E2442DFFE20000FFD10000FF800F0FF200000000B44B2F0FC3815D6AD4936322
                0000000000000000000000006A2D18148D381CFFFF6633FFFF6734FFFB7546FF
                D91E14FFE20000FFD80000FF7B1C1CF2BA402707C87646ADC65E0BFFD79D75DC
                CE7F4416000000000000000077351D07602915BEFF7542FFFF9B68FFFCA877FF
                DD392AFFE00000FFC30000FF4612095DCD835AD2C45904FFBD5201FFC55E0CFF
                D3956FDF000000000000000000000000552E1D47B37F5BF2FF9966FFFF9966FF
                FC9D6DFFDD392AFFC20000FB412A1F6CC57C51FFC05504FFCC5F00FFBF570BFF
                C7835DFF000000000000000000000000000000005E372447B36B47F2FF9966FF
                FF9966FFEC9467FF602D21D4C06B4F09CC8250FFCA6209FFCD6000FFCA630EFF
                CC885CFF0000000000000000000000000000000000000000633C284BBF734CFF
                B06946FF543121AE7D422E2DC6836A09E3C5B779C47442F5C2621CFFC98257F8
                E4C0AA53FF1F0000FE0F0000FE0700008C0300000003000000070000000F0000
                000F0000800F0000800F0000C0110000C0000000C0000000E0000000F0000000
                F8000000}
              Properties.FitMode = ifmProportionalStretch
              Properties.GraphicClassName = 'TJPEGImage'
              Properties.ZipWidth = 0
              Properties.ZipHeight = 0
              TabOrder = 42
              Height = 80
              Width = 100
            end
            object ABcxTabControl1: TABcxTabControl
              Left = 703
              Top = 262
              Width = 100
              Height = 80
              TabOrder = 43
              Properties.CustomButtons.Buttons = <>
              Properties.MultiLine = True
              Properties.MultiSelect = True
              Properties.Options = [pcoAlwaysShowGoDialogButton, pcoCloseButton, pcoGradient, pcoGradientClientArea, pcoRedrawOnResize]
              Properties.TabIndex = 2
              Properties.Tabs.Strings = (
                'aa'
                'bb'
                'cc'
                'dd')
              ClientRectBottom = 80
              ClientRectRight = 100
              ClientRectTop = 24
              object lbl2: TLabel
                Left = 16
                Top = 32
                Width = 16
                Height = 13
                Caption = 'lbl2'
              end
            end
            object ABcxShellListView1: TABcxShellListView
              Left = 620
              Top = 89
              Width = 157
              Height = 80
              Root.BrowseFolder = bfProfile
              Root.CustomPath = 'C:\Documents and Settings\All Users\Documents\RAD Studio\8.0\Bpl'
              Sorting = True
              TabOrder = 44
            end
            object ABcxShellTreeView1: TABcxShellTreeView
              Left = 620
              Top = 176
              Width = 157
              Height = 80
              Indent = 19
              RightClickSelect = True
              TabOrder = 45
            end
            object ABcxPageControl1: TABcxPageControl
              Left = 620
              Top = 4
              Width = 157
              Height = 80
              TabOrder = 46
              Properties.ActivePage = cxTabSheet1
              Properties.CustomButtons.Buttons = <>
              LookAndFeel.Kind = lfFlat
              ActivePageIndex = 0
              ClientRectBottom = 79
              ClientRectLeft = 1
              ClientRectRight = 156
              ClientRectTop = 21
              object cxTabSheet1: TcxTabSheet
                Caption = 'cxTabSheet1'
                ImageIndex = 0
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object lbl3: TLabel
                  Left = 16
                  Top = 32
                  Width = 31
                  Height = 13
                  Caption = 'Label2'
                end
              end
              object cxTabSheet2: TcxTabSheet
                Caption = 'cxTabSheet2'
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
              end
              object cxTabSheet3: TcxTabSheet
                Caption = 'cxTabSheet3'
                ImageIndex = 2
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
              end
            end
            object ABcxTreeList1: TABcxTreeList
              Left = 514
              Top = 262
              Width = 100
              Height = 80
              Bands = <
                item
                end>
              Navigator.Buttons.CustomButtons = <>
              TabOrder = 47
              Data = {
                000005002D0100000F00000044617461436F6E74726F6C6C6572310300000012
                000000546378537472696E6756616C7565547970651200000054637853747269
                6E6756616C75655479706512000000546378537472696E6756616C7565547970
                6503000000445855464D54000001000000310000020000003100310000030000
                00310031003100445855464D5400000200000031006100000300000031003100
                610000040000003100310031006100445855464D540000020000003200320000
                0200000032003200000200000032003200020000000000000002000100000000
                00000000000000FFFFFFFFFFFFFFFFFFFFFFFF01000000080000000000000000
                00FFFFFFFFFFFFFFFFFFFFFFFF0200000008000000000000000000FFFFFFFFFF
                FFFFFFFFFFFFFF1A0002000000}
              object ABcxTreeList1Column1: TcxTreeListColumn
                Caption.Text = 'aa'
                DataBinding.ValueType = 'String'
                Width = 54
                Position.ColIndex = 0
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object ABcxTreeList1Column2: TcxTreeListColumn
                Caption.Text = 'bb'
                DataBinding.ValueType = 'String'
                Width = 46
                Position.ColIndex = 1
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object ABcxTreeList1Column3: TcxTreeListColumn
                Caption.Text = 'cc'
                DataBinding.ValueType = 'String'
                Width = 36
                Position.ColIndex = 2
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
            end
            object ABcxVirtualTreeList1: TABcxVirtualTreeList
              Left = 620
              Top = 262
              Width = 77
              Height = 80
              Bands = <
                item
                  Caption.Text = 'aa'
                end>
              Navigator.Buttons.ConfirmDelete = True
              Navigator.Buttons.CustomButtons = <>
              Navigator.Buttons.Append.Visible = True
              Navigator.Visible = True
              OptionsData.SmartLoad = True
              TabOrder = 48
              OnGetChildCount = ABcxVirtualTreeList1GetChildCount
              OnGetNodeValue = ABcxVirtualTreeList1GetNodeValue
              object ABcxVirtualTreeList1Column1: TcxTreeListColumn
                Caption.Text = 'a'
                DataBinding.ValueType = 'String'
                Width = 49
                Position.ColIndex = 0
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object ABcxVirtualTreeList1Column2: TcxTreeListColumn
                Caption.Text = 'b'
                DataBinding.ValueType = 'String'
                Width = 44
                Position.ColIndex = 1
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
              object ABcxVirtualTreeList1Column3: TcxTreeListColumn
                Caption.Text = 'c'
                DataBinding.ValueType = 'String'
                Width = 44
                Position.ColIndex = 2
                Position.RowIndex = 0
                Position.BandIndex = 0
                Summary.FooterSummaryItems = <>
                Summary.GroupFooterSummaryItems = <>
              end
            end
            object ABcxListView1: TABcxListView
              Left = 302
              Top = 90
              Width = 100
              Height = 80
              Columns = <>
              Items.ItemData = {
                05600000000300000000000000FFFFFFFFFFFFFFFF00000000FFFFFFFF000000
                000361006100610000000000FFFFFFFFFFFFFFFF00000000FFFFFFFF00000000
                0362006200620000000000FFFFFFFFFFFFFFFF00000000FFFFFFFF0000000003
                630063006300}
              TabOrder = 49
            end
            object ABcxPivotGrid1: TABcxPivotGrid
              Left = 514
              Top = 348
              Width = 295
              Height = 141
              Groups = <>
              TabOrder = 50
            end
            object ABcxTreeViewPopupEdit1: TABcxTreeViewPopupEdit
              Left = 160
              Top = 421
              Properties.PopupSysPanelStyle = True
              TabOrder = 51
              Text = 'ABcxTreeViewPopupEdit1'
              Width = 121
            end
            object ABcxFieldOrder1: TABcxFieldOrder
              Left = 160
              Top = 394
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              TabOrder = 52
              Text = 'ABcxFieldOrder1'
              Width = 121
            end
            object ABcxSQLFieldSelects1: TABcxSQLFieldSelects
              Left = 160
              Top = 370
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              TabOrder = 53
              Text = 'ABcxSQLFieldSelects1'
              Width = 121
            end
          end
        end
        object ts7: TTabSheet
          Caption = 'FramkWorkDBControl'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object ABcxDBTimeEdit1: TABcxDBTimeEdit
            Left = 16
            Top = 42
            DataBinding.DataField = 'CL_BegDatetime'
            TabOrder = 0
            Width = 121
          end
          object ABcxDBDateEdit1: TABcxDBDateEdit
            Left = 16
            Top = 65
            DataBinding.DataField = 'CL_HeartbeatDatetime'
            Properties.SaveTime = False
            Properties.ShowTime = False
            TabOrder = 1
            Width = 121
          end
          object ABcxDBDateTimeEdit1: TABcxDBDateTimeEdit
            Left = 16
            Top = 87
            DataBinding.DataField = 'CL_HeartbeatDatetime'
            Properties.Kind = ckDateTime
            TabOrder = 2
            Width = 121
          end
          object ABcxDBCalcEdit1: TABcxDBCalcEdit
            Left = 16
            Top = 110
            DataBinding.DataField = 'CL_SPID'
            TabOrder = 3
            Width = 121
          end
          object ABcxDBSpinEdit1: TABcxDBSpinEdit
            Left = 16
            Top = 132
            DataBinding.DataField = 'CL_SPID'
            TabOrder = 4
            Width = 121
          end
          object ABcxDBCurrencyEdit1: TABcxDBCurrencyEdit
            Left = 16
            Top = 154
            DataBinding.DataField = 'CL_SPID'
            TabOrder = 5
            Width = 121
          end
          object ABcxDBTextEdit1: TABcxDBTextEdit
            Left = 16
            Top = 177
            DataBinding.DataField = 'CL_SPID'
            TabOrder = 6
            Width = 121
          end
          object ABcxDBMaskEdit1: TABcxDBMaskEdit
            Left = 16
            Top = 199
            DataBinding.DataField = 'CL_OP_Name'
            TabOrder = 7
            Width = 121
          end
          object ABcxDBLabel1: TABcxDBLabel
            Left = 16
            Top = 221
            DataBinding.DataField = 'CL_OP_Name'
            Height = 21
            Width = 121
          end
          object ABcxDBCheckBox1: TABcxDBCheckBox
            Left = 16
            Top = 15
            Caption = 'ABcxDBCheckBox1'
            DataBinding.DataField = 'CL_SPID'
            Properties.NullStyle = nssUnchecked
            TabOrder = 9
            Transparent = True
            Width = 121
          end
          object ABcxDBComboBox1: TABcxDBComboBox
            Left = 16
            Top = 265
            DataBinding.DataField = 'CL_OP_Name'
            Properties.ClearKey = 46
            Properties.DropDownRows = 20
            Properties.DropDownSizeable = True
            Properties.Items.Strings = (
              'a'
              'b'
              'c')
            TabOrder = 10
            Width = 121
          end
          object ABcxDBColorComboBox1: TABcxDBColorComboBox
            Left = 16
            Top = 286
            DataBinding.DataField = 'CL_OP_Name'
            Properties.AllowSelectColor = True
            Properties.CustomColors = <>
            Properties.DropDownSizeable = True
            TabOrder = 11
            Width = 121
          end
          object ABcxDBFontNameComboBox1: TABcxDBFontNameComboBox
            Left = 16
            Top = 309
            DataBinding.DataField = 'CL_OP_Name'
            Properties.DropDownSizeable = True
            TabOrder = 12
            Width = 121
          end
          object ABcxDBShellComboBox1: TABcxDBShellComboBox
            Left = 16
            Top = 331
            DataBinding.DataField = 'CL_OP_Name'
            TabOrder = 13
            Width = 121
          end
          object ABcxDBImageComboBox1: TABcxDBImageComboBox
            Left = 16
            Top = 353
            DataBinding.DataField = 'CL_OP_Name'
            Properties.Images = ImageList1
            Properties.Items = <
              item
                ImageIndex = 0
                Value = '0'
              end
              item
                ImageIndex = 1
                Value = '1'
              end
              item
                ImageIndex = 2
                Value = '2'
              end>
            TabOrder = 14
            Width = 121
          end
          object ABcxDBMRUEdit1: TABcxDBMRUEdit
            Left = 16
            Top = 375
            DataBinding.DataField = 'CL_OP_Name'
            Properties.LookupItems.Strings = (
              'a'
              'b'
              'c')
            TabOrder = 15
            Width = 121
          end
          object ABcxDBHyperLinkEdit1: TABcxDBHyperLinkEdit
            Left = 143
            Top = 199
            DataBinding.DataField = 'CL_OP_Name'
            TabOrder = 16
            Width = 121
          end
          object ABcxDBBlobEdit1: TABcxDBBlobEdit
            Left = 143
            Top = 227
            DataBinding.DataField = 'CL_OP_Name'
            Properties.BlobEditKind = bekMemo
            TabOrder = 17
            Width = 121
          end
          object ABcxDBButtonEdit1: TABcxDBButtonEdit
            Left = 143
            Top = 256
            DataBinding.DataField = 'CL_OP_Name'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            TabOrder = 18
            Width = 121
          end
          object ABcxDBComBoBoxMemo1: TABcxDBComBoBoxMemo
            Left = 143
            Top = 284
            DataBinding.DataField = 'CL_OP_Name'
            Properties.DropDownRows = 20
            Properties.DropDownSizeable = True
            TabOrder = 19
            Width = 121
          end
          object ABcxDBFieldSelects1: TABcxDBFieldSelects
            Left = 143
            Top = 314
            DataBinding.DataField = 'CL_OP_Name'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            TabOrder = 20
            Width = 121
          end
          object ABcxDBDirSelect1: TABcxDBDirSelect
            Left = 143
            Top = 341
            DataBinding.DataField = 'CL_OP_Name'
            Properties.DropDownRows = 20
            TabOrder = 21
            Width = 121
          end
          object ABcxDBLookupComboBox1: TABcxDBLookupComboBox
            Left = 143
            Top = 15
            DataBinding.DataField = 'CL_OP_Name'
            Properties.DropDownListStyle = lsEditList
            Properties.DropDownRows = 20
            Properties.DropDownSizeable = True
            Properties.IncrementalFiltering = False
            Properties.KeyFieldNames = 'TR_Code'
            Properties.ListColumns = <
              item
                FieldName = 'TR_Code'
              end
              item
                FieldName = 'TR_Name'
              end>
            Properties.ListSource = DataSource2
            TabOrder = 22
            Width = 121
          end
          object ABcxDBProgressBar1: TABcxDBProgressBar
            Left = 143
            Top = 370
            DataBinding.DataField = 'CL_SPID'
            Properties.PeakValue = 2.000000000000000000
            TabOrder = 23
            Width = 121
          end
          object ABcxDBPopupEdit1: TABcxDBPopupEdit
            Left = 143
            Top = 399
            DataBinding.DataField = 'CL_OP_Name'
            TabOrder = 24
            Width = 121
          end
          object ABdxDBStatusBar1: TABdxDBStatusBar
            Left = 0
            Top = 456
            Width = 830
            Height = 20
            Panels = <>
            PaintStyle = stpsUseLookAndFeel
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            StopUpdate = False
          end
          object ABcxDBSQLFieldSelects1: TABcxDBSQLFieldSelects
            Left = 143
            Top = 113
            DataBinding.DataField = 'CL_OP_Name'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            TabOrder = 26
            Width = 121
          end
          object ABcxDBFieldOrder1: TABcxDBFieldOrder
            Left = 143
            Top = 141
            DataBinding.DataField = 'CL_OP_Name'
            Properties.Buttons = <
              item
                Default = True
                Kind = bkEllipsis
              end>
            TabOrder = 28
            Width = 121
          end
          object ABcxDBCheckComboBox1: TABcxDBCheckComboBox
            Left = 143
            Top = 170
            DataBinding.DataField = 'CL_OP_Name'
            Properties.ShowEmptyText = False
            Properties.ClearKey = 46
            Properties.DropDownRows = 20
            Properties.DropDownSizeable = True
            Properties.EditValueFormat = cvfCaptions
            Properties.Items = <>
            TabOrder = 27
            Width = 121
          end
          object ABcxDBExtLookupComboBox1: TABcxDBExtLookupComboBox
            Left = 143
            Top = 38
            DataBinding.DataField = 'CL_OP_Name'
            Properties.AutoSearchOnPopup = False
            Properties.DropDownAutoSize = True
            Properties.DropDownRows = 20
            Properties.DropDownSizeable = True
            Properties.IncrementalFiltering = False
            Properties.Revertable = True
            TabOrder = 29
            Width = 121
          end
          object ABcxDBMemo1: TABcxDBMemo
            Left = 373
            Top = 15
            DataBinding.DataField = 'CL_OP_Name'
            Properties.ScrollBars = ssVertical
            TabOrder = 30
            Height = 80
            Width = 100
          end
          object ABcxDBRichEdit1: TABcxDBRichEdit
            Left = 267
            Top = 324
            DataBinding.DataField = 'CL_OP_Name'
            TabOrder = 31
            Height = 51
            Width = 100
          end
          object ABcxDBCheckListBox1: TABcxDBCheckListBox
            Left = 267
            Top = 381
            Width = 100
            Height = 55
            DataBinding.DataField = 'CL_OP_Name'
            EditValueFormat = cvfCaptions
            Items = <
              item
                Text = 'aa'
              end
              item
                Text = 'bb'
              end>
            ParentColor = False
            TabOrder = 32
          end
          object ABcxDBRadioGroup1: TABcxDBRadioGroup
            Left = 267
            Top = 273
            Alignment = alCenterCenter
            DataBinding.DataField = 'CL_OP_Name'
            Properties.DefaultValue = 0
            Properties.Items = <
              item
                Caption = 'aa'
              end
              item
                Caption = 'bb'
              end>
            TabOrder = 33
            Height = 45
            Width = 100
          end
          object ABcxDBCheckGroup1: TABcxDBCheckGroup
            Left = 267
            Top = 15
            Caption = 'ABcxDBCheckGroup1'
            Properties.EditValueFormat = cvfCaptions
            Properties.Items = <
              item
                Caption = 'aa'
              end
              item
                Caption = 'bb'
              end>
            TabOrder = 34
            DataBinding.DataField = 'CL_OP_Name'
            Height = 80
            Width = 100
          end
          object ABcxDBImage1: TABcxDBImage
            Left = 267
            Top = 187
            Properties.FitMode = ifmProportionalStretch
            Properties.GraphicClassName = 'TJPEGImage'
            Properties.PopupMenuLayout.MenuItems = [pmiCut, pmiCopy, pmiPaste, pmiDelete, pmiLoad, pmiSave, pmiCustom]
            Properties.PopupMenuLayout.CustomMenuItemCaption = #21387#32553#22270#29255
            Properties.ZipWidth = 0
            Properties.ZipHeight = 0
            TabOrder = 35
            Height = 80
            Width = 100
          end
          object ABcxDBListBox1: TABcxDBListBox
            Left = 267
            Top = 101
            Width = 100
            Height = 80
            ItemHeight = 13
            Items.Strings = (
              'a'
              'b'
              'c'
              'd')
            MultiSelect = True
            TabOrder = 36
            DataBinding.DataField = 'CL_OP_Name'
          end
          object ABcxDBTreeList1: TABcxDBTreeList
            Left = 373
            Top = 101
            Width = 218
            Height = 94
            Bands = <
              item
              end>
            DataController.DataSource = DataSource3
            DataController.ParentField = 'TI_ParentGuid'
            DataController.KeyField = 'TI_Guid'
            Navigator.Buttons.CustomButtons = <>
            Navigator.Visible = True
            OptionsData.Editing = False
            OptionsData.Deleting = False
            OptionsData.Inserting = True
            RootValue = -1
            TabOrder = 37
            object ABcxDBTreeList1cxDBTreeListColumn1: TcxDBTreeListColumn
              DataBinding.FieldName = 'TI_Name'
              Position.ColIndex = 0
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
          end
          object ABcxDBTrackBar1: TABcxDBTrackBar
            Left = 479
            Top = 15
            AutoSize = False
            DataBinding.DataField = 'CL_SPID'
            TabOrder = 38
            Height = 80
            Width = 100
          end
          object ABcxDBTreeView1: TABcxDBTreeView
            Left = 373
            Top = 196
            Width = 250
            Height = 150
            Bands = <>
            DragMode = dmAutomatic
            Navigator.Buttons.CustomButtons = <>
            OptionsSelection.CellSelect = False
            PopupMenu.DefaultParentValue = '0'
            RootValue = -1
            TabOrder = 39
            Active = False
            ExtFullExpand = False
            CanSelectParent = True
          end
        end
        object ts9: TTabSheet
          Caption = 'FramkWorkDBControl '#25193#23637
          ImageIndex = 2
          object pnl11: TPanel
            Left = 0
            Top = 255
            Width = 830
            Height = 221
            Align = alClient
            TabOrder = 0
            object pnl12: TPanel
              Left = 1
              Top = 1
              Width = 288
              Height = 219
              Align = alLeft
              TabOrder = 0
              object ABcxDBPivotGrid1: TABcxDBPivotGrid
                Left = 1
                Top = 1
                Width = 286
                Height = 217
                Align = alClient
                DataSource = DataSource2
                Groups = <>
                OptionsView.ColumnFields = False
                OptionsView.ColumnGrandTotalText = #24635#35745
                OptionsView.DataFields = False
                OptionsView.FilterFields = False
                OptionsView.RowGrandTotalText = #24635#35745
                PopupMenu.LinkcxDBPivotGrid = ABcxDBPivotGrid1
                TabOrder = 0
              end
            end
            object pnl13: TPanel
              Left = 289
              Top = 1
              Width = 540
              Height = 219
              Align = alClient
              TabOrder = 1
              object GroupBox6: TGroupBox
                Left = 1
                Top = 1
                Width = 538
                Height = 217
                Align = alClient
                Caption = 'ABFramkWorkcxGridU'
                TabOrder = 0
                object ABcxGrid1: TABcxGrid
                  Left = 2
                  Top = 15
                  Width = 534
                  Height = 200
                  Align = alClient
                  TabOrder = 0
                  object ABcxGrid1ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
                    PopupMenu.AutoHotkeys = maManual
                    PopupMenu.CloseFootStr = False
                    PopupMenu.LinkTableView = ABcxGrid1ABcxGridDBBandedTableView1
                    PopupMenu.AutoApplyBestFit = True
                    PopupMenu.AutoCreateAllItem = True
                    Navigator.Buttons.CustomButtons = <>
                    Navigator.Buttons.First.Visible = True
                    Navigator.Buttons.PriorPage.Visible = True
                    Navigator.Buttons.Prior.Visible = True
                    Navigator.Buttons.Next.Visible = True
                    Navigator.Buttons.NextPage.Visible = True
                    Navigator.Buttons.Last.Visible = True
                    Navigator.Buttons.Insert.Visible = True
                    Navigator.Buttons.Append.Visible = False
                    Navigator.Buttons.Delete.Visible = True
                    Navigator.Buttons.Edit.Visible = True
                    Navigator.Buttons.Post.Visible = True
                    Navigator.Buttons.Cancel.Visible = True
                    Navigator.Buttons.Refresh.Visible = True
                    Navigator.Buttons.SaveBookmark.Visible = True
                    Navigator.Buttons.GotoBookmark.Visible = True
                    Navigator.Buttons.Filter.Visible = True
                    DataController.DataSource = DataSource2
                    DataController.Filter.Options = [fcoCaseInsensitive]
                    DataController.Filter.AutoDataSetFilter = True
                    DataController.Filter.TranslateBetween = True
                    DataController.Filter.TranslateIn = True
                    DataController.Filter.TranslateLike = True
                    DataController.KeyFieldNames = 'TR_Code'
                    DataController.Summary.DefaultGroupSummaryItems = <>
                    DataController.Summary.FooterSummaryItems = <>
                    DataController.Summary.SummaryGroups = <>
                    OptionsBehavior.AlwaysShowEditor = True
                    OptionsBehavior.FocusCellOnTab = True
                    OptionsBehavior.GoToNextCellOnEnter = True
                    OptionsCustomize.ColumnsQuickCustomization = True
                    OptionsCustomize.DataRowSizing = True
                    OptionsSelection.MultiSelect = True
                    OptionsView.GroupByBox = False
                    OptionsView.Indicator = True
                    OptionsView.BandHeaders = False
                    Bands = <
                      item
                      end>
                    ExtPopupMenu.AutoHotkeys = maManual
                    ExtPopupMenu.CloseFootStr = False
                    ExtPopupMenu.LinkTableView = ABcxGrid1ABcxGridDBBandedTableView1
                    ExtPopupMenu.AutoApplyBestFit = True
                    ExtPopupMenu.AutoCreateAllItem = True
                  end
                  object ABcxGrid1DBBandedTableView1: TcxGridDBBandedTableView
                    Navigator.Buttons.CustomButtons = <>
                    Navigator.Buttons.First.Visible = True
                    Navigator.Buttons.PriorPage.Visible = True
                    Navigator.Buttons.Prior.Visible = True
                    Navigator.Buttons.Next.Visible = True
                    Navigator.Buttons.NextPage.Visible = True
                    Navigator.Buttons.Last.Visible = True
                    Navigator.Buttons.Insert.Visible = True
                    Navigator.Buttons.Append.Visible = False
                    Navigator.Buttons.Delete.Visible = True
                    Navigator.Buttons.Edit.Visible = True
                    Navigator.Buttons.Post.Visible = True
                    Navigator.Buttons.Cancel.Visible = True
                    Navigator.Buttons.Refresh.Visible = True
                    Navigator.Buttons.SaveBookmark.Visible = True
                    Navigator.Buttons.GotoBookmark.Visible = True
                    Navigator.Buttons.Filter.Visible = True
                    DataController.DataSource = DataSource2
                    DataController.KeyFieldNames = 'TR_CODE'
                    DataController.Summary.DefaultGroupSummaryItems = <>
                    DataController.Summary.FooterSummaryItems = <>
                    DataController.Summary.SummaryGroups = <>
                    OptionsCustomize.ColumnsQuickCustomization = True
                    OptionsData.Appending = True
                    OptionsData.Deleting = False
                    OptionsData.DeletingConfirmation = False
                    OptionsData.Editing = False
                    OptionsData.Inserting = False
                    OptionsView.GroupByBox = False
                    OptionsView.Indicator = True
                    OptionsView.BandHeaders = False
                    Bands = <
                      item
                      end>
                    object ABcxGrid1DBBandedTableView1TR_Guid: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_Guid'
                      Position.BandIndex = 0
                      Position.ColIndex = 0
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1TR_Group: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_Group'
                      Position.BandIndex = 0
                      Position.ColIndex = 1
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1TR_SysUse: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_SysUse'
                      Position.BandIndex = 0
                      Position.ColIndex = 2
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1TR_Code: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_Code'
                      Position.BandIndex = 0
                      Position.ColIndex = 3
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1TR_Name: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_Name'
                      Position.BandIndex = 0
                      Position.ColIndex = 4
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1TR_DownFields: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_DownFields'
                      Position.BandIndex = 0
                      Position.ColIndex = 5
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1TR_ViewFields: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_ViewFields'
                      Position.BandIndex = 0
                      Position.ColIndex = 6
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1TR_SaveFields: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_SaveFields'
                      Position.BandIndex = 0
                      Position.ColIndex = 7
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1TR_int1: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_int1'
                      Position.BandIndex = 0
                      Position.ColIndex = 8
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1TR_int2: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_int2'
                      Position.BandIndex = 0
                      Position.ColIndex = 9
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1TR_decimal1: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_decimal1'
                      Position.BandIndex = 0
                      Position.ColIndex = 10
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1TR_decimal2: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_decimal2'
                      Position.BandIndex = 0
                      Position.ColIndex = 11
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1TR_Varchar1: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_Varchar1'
                      Position.BandIndex = 0
                      Position.ColIndex = 12
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1TR_Varchar2: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_Varchar2'
                      Position.BandIndex = 0
                      Position.ColIndex = 13
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1TR_bit1: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_bit1'
                      Position.BandIndex = 0
                      Position.ColIndex = 14
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1TR_bit2: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_bit2'
                      Position.BandIndex = 0
                      Position.ColIndex = 15
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1TR_Remark: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_Remark'
                      Position.BandIndex = 0
                      Position.ColIndex = 16
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1TR_Order: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'TR_Order'
                      Position.BandIndex = 0
                      Position.ColIndex = 17
                      Position.RowIndex = 0
                    end
                    object ABcxGrid1DBBandedTableView1PubID: TcxGridDBBandedColumn
                      DataBinding.FieldName = 'PubID'
                      Position.BandIndex = 0
                      Position.ColIndex = 18
                      Position.RowIndex = 0
                    end
                  end
                  object ABcxGrid1Level1: TcxGridLevel
                    GridView = ABcxGrid1ABcxGridDBBandedTableView1
                  end
                  object ABcxGrid1Level2: TcxGridLevel
                    GridView = ABcxGrid1DBBandedTableView1
                  end
                end
              end
            end
          end
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 830
            Height = 209
            Align = alTop
            TabOrder = 1
            object Panel3: TPanel
              Left = 339
              Top = 1
              Width = 490
              Height = 207
              Align = alClient
              TabOrder = 0
              object grp11: TGroupBox
                Left = 1
                Top = 1
                Width = 488
                Height = 40
                Align = alTop
                Caption = 'ABFramkWorkQuerySelectFieldPopupEditU'
                TabOrder = 0
                object ABQuerySelectFieldPopupEdit1: TABQuerySelectFieldPopupEdit
                  Left = 2
                  Top = 15
                  Align = alClient
                  Properties.PopupAutoSize = False
                  Properties.PopupHeight = 215
                  Properties.PopupWidth = 0
                  TabOrder = 0
                  DataSource = DataSource2
                  DFMControl = 
                    'object ABQuerySelectFieldPanelDesignForm: TABQuerySelectFieldPan' +
                    'elDesignForm'#13#10'  Left = 329'#13#10'  Top = 100'#13#10'  HorzScrollBar.Visible' +
                    ' = False'#13#10'  VertScrollBar.Visible = False'#13#10'  ActiveControl = But' +
                    'ton2'#13#10'  BorderIcons = [biMinimize, biMaximize]'#13#10'  Caption = #357' +
                    '74#35745#26597#35810#39029#38754'#13#10'  ClientHeight = 253'#13#10'  Client' +
                    'Width = 700'#13#10'  Color = clBtnFace'#13#10'  Font.Charset = DEFAULT_CHARS' +
                    'ET'#13#10'  Font.Color = clWindowText'#13#10'  Font.Height = -11'#13#10'  Font.Nam' +
                    'e = '#39'MS Sans Serif'#39#13#10'  Font.Style = []'#13#10'  KeyPreview = True'#13#10'  O' +
                    'ldCreateOrder = False'#13#10'  Position = poDesigned'#13#10'  ShowHint = Tru' +
                    'e'#13#10'  PixelsPerInch = 96'#13#10'  TextHeight = 13'#13#10'  object InnerQueryC' +
                    'ontrolPanel: TABCustomPanel'#13#10'    Left = 0'#13#10'    Top = 0'#13#10'    Widt' +
                    'h = 700'#13#10'    Height = 215'#13#10'    Align = alClient'#13#10'    Caption = '#39 +
                    'InnerQueryControlPanel'#39#13#10'    ShowCaption = False'#13#10'    TabOrder =' +
                    ' 1'#13#10'    ExplicitHeight = 462'#13#10'    object InnerQueryControlPanel_' +
                    'ABQuery2_InnerQueryPanel: TGroupBox'#13#10'      Left = 1'#13#10'      Top =' +
                    ' 1'#13#10'      Width = 698'#13#10'      Height = 115'#13#10'      Align = alTop'#13#10 +
                    '      Caption = #21015#39033#20027#34920'#13#10'      TabOrder = 0'#13#10'  ' +
                    '    DesignSize = ('#13#10'        698'#13#10'        115)'#13#10'      object Inne' +
                    'rQueryControlPanel_ABQuery2_InnerQueryPanel_TR_Group_Control: TA' +
                    'BcxExtLookupComboBox'#13#10'        Left = 83'#13#10'        Top = 20'#13#10'     ' +
                    '   Anchors = [akLeft, akTop, akRight]'#13#10'        Properties.AutoSe' +
                    'archOnPopup = False'#13#10'        Properties.DropDownAutoSize = True'#13 +
                    #10'        Properties.DropDownListStyle = lsEditList'#13#10'        Prop' +
                    'erties.DropDownRows = 20'#13#10'        Properties.DropDownSizeable = ' +
                    'True'#13#10'        Properties.IncrementalFiltering = False'#13#10'        P' +
                    'roperties.MaxLength = 0'#13#10'        Properties.ReadOnly = False'#13#10'  ' +
                    '      Properties.Revertable = True'#13#10'        Style.BorderStyle = ' +
                    'ebsFlat'#13#10'        TabOrder = 1'#13#10'        Width = 610'#13#10'      end'#13#10' ' +
                    '     object InnerQueryControlPanel_ABQuery2_InnerQueryPanel_TR_G' +
                    'roup_Label: TABcxLabel'#13#10'        Left = 5'#13#10'        Top = 20'#13#10'    ' +
                    '    AutoSize = False'#13#10'        Caption = #20998#32452#21517#31216' +
                    #13#10'        FocusControl = InnerQueryControlPanel_ABQuery2_InnerQu' +
                    'eryPanel_TR_Group_Control'#13#10'        ParentFont = False'#13#10'        S' +
                    'tyle.Font.Charset = DEFAULT_CHARSET'#13#10'        Style.Font.Color = ' +
                    'clWindowText'#13#10'        Style.Font.Height = -14'#13#10'        Style.Fon' +
                    't.Name = #23435#20307'#13#10'        Style.Font.Style = []'#13#10'        St' +
                    'yle.IsFontAssigned = True'#13#10'        Properties.Alignment.Vert = t' +
                    'aVCenter'#13#10'        Properties.WordWrap = True'#13#10'        Transparen' +
                    't = True'#13#10'        Height = 21'#13#10'        Width = 77'#13#10'        Ancho' +
                    'rY = 31'#13#10'      end'#13#10'      object InnerQueryControlPanel_ABQuery2' +
                    '_InnerQueryPanel_TR_SysUse_Control: TABcxCheckBox'#13#10'        Left ' +
                    '= 83'#13#10'        Top = 42'#13#10'        Anchors = [akLeft, akTop, akRigh' +
                    't]'#13#10'        Properties.NullStyle = nssUnchecked'#13#10'        State =' +
                    ' cbsGrayed'#13#10'        TabOrder = 3'#13#10'        Transparent = True'#13#10'  ' +
                    '      Width = 610'#13#10'      end'#13#10'      object InnerQueryControlPane' +
                    'l_ABQuery2_InnerQueryPanel_TR_SysUse_Label: TABcxLabel'#13#10'        ' +
                    'Left = 5'#13#10'        Top = 42'#13#10'        AutoSize = False'#13#10'        Ca' +
                    'ption = #31995#32479#20351#29992'#13#10'        FocusControl = InnerQu' +
                    'eryControlPanel_ABQuery2_InnerQueryPanel_TR_SysUse_Control'#13#10'    ' +
                    '    ParentFont = False'#13#10'        Style.Font.Charset = DEFAULT_CHA' +
                    'RSET'#13#10'        Style.Font.Color = clWindowText'#13#10'        Style.Fon' +
                    't.Height = -14'#13#10'        Style.Font.Name = #23435#20307'#13#10'        ' +
                    'Style.Font.Style = []'#13#10'        Style.IsFontAssigned = True'#13#10'    ' +
                    '    Properties.Alignment.Vert = taVCenter'#13#10'        Properties.Wo' +
                    'rdWrap = True'#13#10'        Transparent = True'#13#10'        Height = 21'#13#10 +
                    '        Width = 77'#13#10'        AnchorY = 53'#13#10'      end'#13#10'      objec' +
                    't InnerQueryControlPanel_ABQuery2_InnerQueryPanel_TR_Code_Contro' +
                    'l: TABcxTextEdit'#13#10'        Left = 83'#13#10'        Top = 64'#13#10'        A' +
                    'nchors = [akLeft, akTop, akRight]'#13#10'        Properties.ReadOnly =' +
                    ' False'#13#10'        Style.BorderStyle = ebsFlat'#13#10'        TabOrder = ' +
                    '5'#13#10'        Width = 610'#13#10'      end'#13#10'      object InnerQueryContro' +
                    'lPanel_ABQuery2_InnerQueryPanel_TR_Code_Label: TABcxLabel'#13#10'     ' +
                    '   Left = 5'#13#10'        Top = 64'#13#10'        AutoSize = False'#13#10'       ' +
                    ' Caption = #32534#21495'#13#10'        FocusControl = InnerQueryContro' +
                    'lPanel_ABQuery2_InnerQueryPanel_TR_Code_Control'#13#10'        ParentF' +
                    'ont = False'#13#10'        Style.Font.Charset = DEFAULT_CHARSET'#13#10'     ' +
                    '   Style.Font.Color = clWindowText'#13#10'        Style.Font.Height = ' +
                    '-14'#13#10'        Style.Font.Name = #23435#20307'#13#10'        Style.Font.' +
                    'Style = []'#13#10'        Style.IsFontAssigned = True'#13#10'        Propert' +
                    'ies.Alignment.Vert = taVCenter'#13#10'        Properties.WordWrap = Tr' +
                    'ue'#13#10'        Transparent = True'#13#10'        Height = 21'#13#10'        Wid' +
                    'th = 77'#13#10'        AnchorY = 75'#13#10'      end'#13#10'      object InnerQuer' +
                    'yControlPanel_ABQuery2_InnerQueryPanel_TR_Name_Control: TABcxTex' +
                    'tEdit'#13#10'        Left = 83'#13#10'        Top = 86'#13#10'        Anchors = [a' +
                    'kLeft, akTop, akRight]'#13#10'        Properties.ReadOnly = False'#13#10'   ' +
                    '     Style.BorderStyle = ebsFlat'#13#10'        TabOrder = 7'#13#10'        ' +
                    'Width = 610'#13#10'      end'#13#10'      object InnerQueryControlPanel_ABQu' +
                    'ery2_InnerQueryPanel_TR_Name_Label: TABcxLabel'#13#10'        Left = 5' +
                    #13#10'        Top = 86'#13#10'        AutoSize = False'#13#10'        Caption = ' +
                    '#21517#31216'#13#10'        FocusControl = InnerQueryControlPanel_ABQu' +
                    'ery2_InnerQueryPanel_TR_Name_Control'#13#10'        ParentFont = False' +
                    #13#10'        Style.Font.Charset = DEFAULT_CHARSET'#13#10'        Style.Fo' +
                    'nt.Color = clWindowText'#13#10'        Style.Font.Height = -14'#13#10'      ' +
                    '  Style.Font.Name = #23435#20307'#13#10'        Style.Font.Style = []'#13 +
                    #10'        Style.IsFontAssigned = True'#13#10'        Properties.Alignme' +
                    'nt.Vert = taVCenter'#13#10'        Properties.WordWrap = True'#13#10'       ' +
                    ' Transparent = True'#13#10'        Height = 21'#13#10'        Width = 77'#13#10'  ' +
                    '      AnchorY = 97'#13#10'      end'#13#10'    end'#13#10'    object InnerQueryCon' +
                    'trolPanel_ABQuery3_InnerQueryPanel: TGroupBox'#13#10'      Left = 1'#13#10' ' +
                    '     Top = 116'#13#10'      Width = 698'#13#10'      Height = 93'#13#10'      Alig' +
                    'n = alTop'#13#10'      Caption = #21015#39033#26126#32454#34920'#13#10'     ' +
                    ' TabOrder = 1'#13#10'      DesignSize = ('#13#10'        698'#13#10'        93)'#13#10' ' +
                    '     object InnerQueryControlPanel_ABQuery3_InnerQueryPanel_TI_G' +
                    'roup_Control: TABcxTextEdit'#13#10'        Left = 83'#13#10'        Top = 20' +
                    #13#10'        Anchors = [akLeft, akTop, akRight]'#13#10'        Properties' +
                    '.ReadOnly = False'#13#10'        Style.BorderStyle = ebsFlat'#13#10'        ' +
                    'TabOrder = 1'#13#10'        Width = 610'#13#10'      end'#13#10'      object Inner' +
                    'QueryControlPanel_ABQuery3_InnerQueryPanel_TI_Group_Label: TABcx' +
                    'Label'#13#10'        Left = 5'#13#10'        Top = 20'#13#10'        AutoSize = Fa' +
                    'lse'#13#10'        Caption = #20998#32452#21517#31216'#13#10'        FocusCo' +
                    'ntrol = InnerQueryControlPanel_ABQuery3_InnerQueryPanel_TI_Group' +
                    '_Control'#13#10'        ParentFont = False'#13#10'        Style.Font.Charset' +
                    ' = DEFAULT_CHARSET'#13#10'        Style.Font.Color = clWindowText'#13#10'   ' +
                    '     Style.Font.Height = -14'#13#10'        Style.Font.Name = #23435#2' +
                    '0307'#13#10'        Style.Font.Style = []'#13#10'        Style.IsFontAssigne' +
                    'd = True'#13#10'        Properties.Alignment.Vert = taVCenter'#13#10'       ' +
                    ' Properties.WordWrap = True'#13#10'        Transparent = True'#13#10'       ' +
                    ' Height = 21'#13#10'        Width = 77'#13#10'        AnchorY = 31'#13#10'      en' +
                    'd'#13#10'      object InnerQueryControlPanel_ABQuery3_InnerQueryPanel_' +
                    'TI_Code_Control: TABcxTextEdit'#13#10'        Left = 83'#13#10'        Top =' +
                    ' 42'#13#10'        Anchors = [akLeft, akTop, akRight]'#13#10'        Propert' +
                    'ies.ReadOnly = False'#13#10'        Style.BorderStyle = ebsFlat'#13#10'     ' +
                    '   TabOrder = 3'#13#10'        Width = 610'#13#10'      end'#13#10'      object In' +
                    'nerQueryControlPanel_ABQuery3_InnerQueryPanel_TI_Code_Label: TAB' +
                    'cxLabel'#13#10'        Left = 5'#13#10'        Top = 42'#13#10'        AutoSize = ' +
                    'False'#13#10'        Caption = #32534#21495'#13#10'        FocusControl = In' +
                    'nerQueryControlPanel_ABQuery3_InnerQueryPanel_TI_Code_Control'#13#10' ' +
                    '       ParentFont = False'#13#10'        Style.Font.Charset = DEFAULT_' +
                    'CHARSET'#13#10'        Style.Font.Color = clWindowText'#13#10'        Style.' +
                    'Font.Height = -14'#13#10'        Style.Font.Name = #23435#20307'#13#10'     ' +
                    '   Style.Font.Style = []'#13#10'        Style.IsFontAssigned = True'#13#10' ' +
                    '       Properties.Alignment.Vert = taVCenter'#13#10'        Properties' +
                    '.WordWrap = True'#13#10'        Transparent = True'#13#10'        Height = 2' +
                    '1'#13#10'        Width = 77'#13#10'        AnchorY = 53'#13#10'      end'#13#10'      ob' +
                    'ject InnerQueryControlPanel_ABQuery3_InnerQueryPanel_TI_Name_Con' +
                    'trol: TABcxTextEdit'#13#10'        Left = 83'#13#10'        Top = 64'#13#10'      ' +
                    '  Anchors = [akLeft, akTop, akRight]'#13#10'        Properties.ReadOnl' +
                    'y = False'#13#10'        Style.BorderStyle = ebsFlat'#13#10'        TabOrder' +
                    ' = 5'#13#10'        Width = 610'#13#10'      end'#13#10'      object InnerQueryCon' +
                    'trolPanel_ABQuery3_InnerQueryPanel_TI_Name_Label: TABcxLabel'#13#10'  ' +
                    '      Left = 5'#13#10'        Top = 64'#13#10'        AutoSize = False'#13#10'    ' +
                    '    Caption = #21517#31216'#13#10'        FocusControl = InnerQueryCon' +
                    'trolPanel_ABQuery3_InnerQueryPanel_TI_Name_Control'#13#10'        Pare' +
                    'ntFont = False'#13#10'        Style.Font.Charset = DEFAULT_CHARSET'#13#10'  ' +
                    '      Style.Font.Color = clWindowText'#13#10'        Style.Font.Height' +
                    ' = -14'#13#10'        Style.Font.Name = #23435#20307'#13#10'        Style.Fo' +
                    'nt.Style = []'#13#10'        Style.IsFontAssigned = True'#13#10'        Prop' +
                    'erties.Alignment.Vert = taVCenter'#13#10'        Properties.WordWrap =' +
                    ' True'#13#10'        Transparent = True'#13#10'        Height = 21'#13#10'        ' +
                    'Width = 77'#13#10'        AnchorY = 75'#13#10'      end'#13#10'    end'#13#10'  end'#13#10'end'
                  Width = 484
                end
              end
              object grp18: TGroupBox
                Left = 1
                Top = 41
                Width = 488
                Height = 47
                Align = alTop
                Caption = 'ABFramkWorkQueryAllFieldComboxU'
                TabOrder = 1
                object ABQueryAllFieldCombox1: TABQueryAllFieldCombox
                  Left = 2
                  Top = 15
                  Width = 484
                  Height = 27
                  Align = alTop
                  Caption = 'ABQueryAllFieldCombox1'
                  Constraints.MaxHeight = 27
                  Constraints.MinHeight = 27
                  ParentBackground = False
                  ShowCaption = False
                  TabOrder = 0
                  DataSource = DataSource2
                  FieldComboboxWidth = 200
                end
              end
              object BitBtn1: TBitBtn
                Left = 72
                Top = 96
                Width = 75
                Height = 25
                Caption = 'BitBtn1'
                TabOrder = 2
                OnClick = BitBtn1Click
              end
            end
            object grp17: TGroupBox
              Left = 1
              Top = 1
              Width = 338
              Height = 207
              Align = alLeft
              Caption = 'ABFramkWorkQuerySelectFieldPanelU'
              TabOrder = 1
              object ABQuerySelectFieldPanel1: TABQuerySelectFieldPanel
                Left = 2
                Top = 15
                Width = 334
                Height = 190
                Align = alClient
                AutoSize = True
                Caption = 'InnerQueryControlPanel'
                ParentBackground = False
                ShowCaption = False
                TabOrder = 0
                DataSource = DataSource2
                DFMControl = 
                  'object ABQuerySelectFieldPanelDesignForm_1: TABQuerySelectFieldP' +
                  'anelDesignForm'#13#10'  Left = 353'#13#10'  Top = 124'#13#10'  HorzScrollBar.Visib' +
                  'le = False'#13#10'  VertScrollBar.Visible = False'#13#10'  ActiveControl = B' +
                  'utton2'#13#10'  BorderIcons = [biMinimize, biMaximize]'#13#10'  Caption = #3' +
                  '5774#35745#26597#35810#39029#38754'#13#10'  ClientHeight = 500'#13#10'  Clie' +
                  'ntWidth = 233'#13#10'  Color = clBtnFace'#13#10'  Font.Charset = DEFAULT_CHA' +
                  'RSET'#13#10'  Font.Color = clWindowText'#13#10'  Font.Height = -11'#13#10'  Font.N' +
                  'ame = '#39'MS Sans Serif'#39#13#10'  Font.Style = []'#13#10'  KeyPreview = True'#13#10' ' +
                  ' OldCreateOrder = False'#13#10'  Position = poDesigned'#13#10'  ShowHint = T' +
                  'rue'#13#10'  PixelsPerInch = 96'#13#10'  TextHeight = 13'#13#10'  object InnerQuer' +
                  'yControlPanel: TABCustomPanel'#13#10'    Left = 0'#13#10'    Top = 0'#13#10'    Wi' +
                  'dth = 233'#13#10'    Height = 462'#13#10'    Align = alClient'#13#10'    Caption =' +
                  ' '#39'InnerQueryControlPanel'#39#13#10'    ShowCaption = False'#13#10'    TabOrder' +
                  ' = 1'#13#10'    object InnerQueryControlPanel_ABQuery2_InnerQueryPanel' +
                  ': TGroupBox'#13#10'      Left = 1'#13#10'      Top = 1'#13#10'      Width = 231'#13#10' ' +
                  '     Height = 115'#13#10'      Align = alTop'#13#10'      Caption = #21015#3' +
                  '9033#20027#34920'#13#10'      TabOrder = 0'#13#10'      DesignSize = ('#13#10'    ' +
                  '    231'#13#10'        115)'#13#10'      object InnerQueryControlPanel_ABQue' +
                  'ry2_InnerQueryPanel_TR_Group_Control: TABcxExtLookupComboBox'#13#10'  ' +
                  '      Left = 83'#13#10'        Top = 20'#13#10'        Anchors = [akLeft, ak' +
                  'Top, akRight]'#13#10'        Properties.AutoSearchOnPopup = False'#13#10'   ' +
                  '     Properties.DropDownAutoSize = True'#13#10'        Properties.Drop' +
                  'DownListStyle = lsEditList'#13#10'        Properties.DropDownRows = 20' +
                  #13#10'        Properties.DropDownSizeable = True'#13#10'        Properties' +
                  '.IncrementalFiltering = False'#13#10'        Properties.View = ABDownM' +
                  'anagerForm.PubDownEditName_InnerQueryControlPanel_ABQuery2_Inner' +
                  'QueryPanel_TR_Group_Control'#13#10'        Properties.KeyFieldNames = ' +
                  #39'TR_Group'#39#13#10'        Properties.ListFieldItem = ABDownManagerForm' +
                  '.PubDownEditName_InnerQueryControlPanel_ABQuery2_InnerQueryPanel' +
                  '_TR_Group_ControlTR_Group'#13#10'        Properties.MaxLength = 0'#13#10'   ' +
                  '     Properties.ReadOnly = False'#13#10'        Properties.Revertable ' +
                  '= True'#13#10'        Style.BorderStyle = ebsFlat'#13#10'        TabOrder = ' +
                  '1'#13#10'        Width = 143'#13#10'      end'#13#10'      object InnerQueryContro' +
                  'lPanel_ABQuery2_InnerQueryPanel_TR_Group_Label: TABcxLabel'#13#10'    ' +
                  '    Left = 5'#13#10'        Top = 20'#13#10'        AutoSize = False'#13#10'      ' +
                  '  Caption = #20998#32452#21517#31216'#13#10'        FocusControl = Inn' +
                  'erQueryControlPanel_ABQuery2_InnerQueryPanel_TR_Group_Control'#13#10' ' +
                  '       ParentFont = False'#13#10'        Style.Font.Charset = DEFAULT_' +
                  'CHARSET'#13#10'        Style.Font.Color = clWindowText'#13#10'        Style.' +
                  'Font.Height = -14'#13#10'        Style.Font.Name = #23435#20307'#13#10'     ' +
                  '   Style.Font.Style = []'#13#10'        Style.IsFontAssigned = True'#13#10' ' +
                  '       Properties.Alignment.Vert = taVCenter'#13#10'        Properties' +
                  '.WordWrap = True'#13#10'        Transparent = True'#13#10'        Height = 2' +
                  '1'#13#10'        Width = 77'#13#10'        AnchorY = 31'#13#10'      end'#13#10'      ob' +
                  'ject InnerQueryControlPanel_ABQuery2_InnerQueryPanel_TR_SysUse_C' +
                  'ontrol: TABcxCheckBox'#13#10'        Left = 83'#13#10'        Top = 42'#13#10'    ' +
                  '    Anchors = [akLeft, akTop, akRight]'#13#10'        Properties.NullS' +
                  'tyle = nssUnchecked'#13#10'        State = cbsGrayed'#13#10'        TabOrder' +
                  ' = 3'#13#10'        Transparent = True'#13#10'        Width = 143'#13#10'      end' +
                  #13#10'      object InnerQueryControlPanel_ABQuery2_InnerQueryPanel_T' +
                  'R_SysUse_Label: TABcxLabel'#13#10'        Left = 5'#13#10'        Top = 42'#13#10 +
                  '        AutoSize = False'#13#10'        Caption = #31995#32479#20351#2' +
                  '9992'#13#10'        FocusControl = InnerQueryControlPanel_ABQuery2_Inn' +
                  'erQueryPanel_TR_SysUse_Control'#13#10'        ParentFont = False'#13#10'    ' +
                  '    Style.Font.Charset = DEFAULT_CHARSET'#13#10'        Style.Font.Col' +
                  'or = clWindowText'#13#10'        Style.Font.Height = -14'#13#10'        Styl' +
                  'e.Font.Name = #23435#20307'#13#10'        Style.Font.Style = []'#13#10'     ' +
                  '   Style.IsFontAssigned = True'#13#10'        Properties.Alignment.Ver' +
                  't = taVCenter'#13#10'        Properties.WordWrap = True'#13#10'        Trans' +
                  'parent = True'#13#10'        Height = 21'#13#10'        Width = 77'#13#10'        ' +
                  'AnchorY = 53'#13#10'      end'#13#10'      object InnerQueryControlPanel_ABQ' +
                  'uery2_InnerQueryPanel_TR_Code_Control: TABcxTextEdit'#13#10'        Le' +
                  'ft = 83'#13#10'        Top = 64'#13#10'        Anchors = [akLeft, akTop, akR' +
                  'ight]'#13#10'        Properties.ReadOnly = False'#13#10'        Style.Border' +
                  'Style = ebsFlat'#13#10'        TabOrder = 5'#13#10'        Width = 143'#13#10'    ' +
                  '  end'#13#10'      object InnerQueryControlPanel_ABQuery2_InnerQueryPa' +
                  'nel_TR_Code_Label: TABcxLabel'#13#10'        Left = 5'#13#10'        Top = 6' +
                  '4'#13#10'        AutoSize = False'#13#10'        Caption = #32534#21495'#13#10'   ' +
                  '     FocusControl = InnerQueryControlPanel_ABQuery2_InnerQueryPa' +
                  'nel_TR_Code_Control'#13#10'        ParentFont = False'#13#10'        Style.F' +
                  'ont.Charset = DEFAULT_CHARSET'#13#10'        Style.Font.Color = clWind' +
                  'owText'#13#10'        Style.Font.Height = -14'#13#10'        Style.Font.Name' +
                  ' = #23435#20307'#13#10'        Style.Font.Style = []'#13#10'        Style.Is' +
                  'FontAssigned = True'#13#10'        Properties.Alignment.Vert = taVCent' +
                  'er'#13#10'        Properties.WordWrap = True'#13#10'        Transparent = Tr' +
                  'ue'#13#10'        Height = 21'#13#10'        Width = 77'#13#10'        AnchorY = 7' +
                  '5'#13#10'      end'#13#10'      object InnerQueryControlPanel_ABQuery2_Inner' +
                  'QueryPanel_TR_Name_Control: TABcxTextEdit'#13#10'        Left = 83'#13#10'  ' +
                  '      Top = 86'#13#10'        Anchors = [akLeft, akTop, akRight]'#13#10'    ' +
                  '    Properties.ReadOnly = False'#13#10'        Style.BorderStyle = ebs' +
                  'Flat'#13#10'        TabOrder = 7'#13#10'        Width = 143'#13#10'      end'#13#10'    ' +
                  '  object InnerQueryControlPanel_ABQuery2_InnerQueryPanel_TR_Name' +
                  '_Label: TABcxLabel'#13#10'        Left = 5'#13#10'        Top = 86'#13#10'        ' +
                  'AutoSize = False'#13#10'        Caption = #21517#31216'#13#10'        FocusC' +
                  'ontrol = InnerQueryControlPanel_ABQuery2_InnerQueryPanel_TR_Name' +
                  '_Control'#13#10'        ParentFont = False'#13#10'        Style.Font.Charset' +
                  ' = DEFAULT_CHARSET'#13#10'        Style.Font.Color = clWindowText'#13#10'   ' +
                  '     Style.Font.Height = -14'#13#10'        Style.Font.Name = #23435#2' +
                  '0307'#13#10'        Style.Font.Style = []'#13#10'        Style.IsFontAssigne' +
                  'd = True'#13#10'        Properties.Alignment.Vert = taVCenter'#13#10'       ' +
                  ' Properties.WordWrap = True'#13#10'        Transparent = True'#13#10'       ' +
                  ' Height = 21'#13#10'        Width = 77'#13#10'        AnchorY = 97'#13#10'      en' +
                  'd'#13#10'    end'#13#10'    object InnerQueryControlPanel_ABQuery3_InnerQuer' +
                  'yPanel: TGroupBox'#13#10'      Left = 1'#13#10'      Top = 116'#13#10'      Width ' +
                  '= 231'#13#10'      Height = 345'#13#10'      Align = alClient'#13#10'      Caption' +
                  ' = #21015#39033#26126#32454#34920'#13#10'      TabOrder = 1'#13#10'      Des' +
                  'ignSize = ('#13#10'        231'#13#10'        345)'#13#10'      object InnerQueryC' +
                  'ontrolPanel_ABQuery3_InnerQueryPanel_TI_Group_Control: TABcxText' +
                  'Edit'#13#10'        Left = 83'#13#10'        Top = 20'#13#10'        Anchors = [ak' +
                  'Left, akTop, akRight]'#13#10'        Properties.ReadOnly = False'#13#10'    ' +
                  '    Style.BorderStyle = ebsFlat'#13#10'        TabOrder = 1'#13#10'        W' +
                  'idth = 143'#13#10'      end'#13#10'      object InnerQueryControlPanel_ABQue' +
                  'ry3_InnerQueryPanel_TI_Group_Label: TABcxLabel'#13#10'        Left = 5' +
                  #13#10'        Top = 20'#13#10'        AutoSize = False'#13#10'        Caption = ' +
                  '#20998#32452#21517#31216'#13#10'        FocusControl = InnerQueryContr' +
                  'olPanel_ABQuery3_InnerQueryPanel_TI_Group_Control'#13#10'        Paren' +
                  'tFont = False'#13#10'        Style.Font.Charset = DEFAULT_CHARSET'#13#10'   ' +
                  '     Style.Font.Color = clWindowText'#13#10'        Style.Font.Height ' +
                  '= -14'#13#10'        Style.Font.Name = #23435#20307'#13#10'        Style.Fon' +
                  't.Style = []'#13#10'        Style.IsFontAssigned = True'#13#10'        Prope' +
                  'rties.Alignment.Vert = taVCenter'#13#10'        Properties.WordWrap = ' +
                  'True'#13#10'        Transparent = True'#13#10'        Height = 21'#13#10'        W' +
                  'idth = 77'#13#10'        AnchorY = 31'#13#10'      end'#13#10'      object InnerQu' +
                  'eryControlPanel_ABQuery3_InnerQueryPanel_TI_Code_Control: TABcxT' +
                  'extEdit'#13#10'        Left = 83'#13#10'        Top = 42'#13#10'        Anchors = ' +
                  '[akLeft, akTop, akRight]'#13#10'        Properties.ReadOnly = False'#13#10' ' +
                  '       Style.BorderStyle = ebsFlat'#13#10'        TabOrder = 3'#13#10'      ' +
                  '  Width = 143'#13#10'      end'#13#10'      object InnerQueryControlPanel_AB' +
                  'Query3_InnerQueryPanel_TI_Code_Label: TABcxLabel'#13#10'        Left =' +
                  ' 5'#13#10'        Top = 42'#13#10'        AutoSize = False'#13#10'        Caption ' +
                  '= #32534#21495'#13#10'        FocusControl = InnerQueryControlPanel_AB' +
                  'Query3_InnerQueryPanel_TI_Code_Control'#13#10'        ParentFont = Fal' +
                  'se'#13#10'        Style.Font.Charset = DEFAULT_CHARSET'#13#10'        Style.' +
                  'Font.Color = clWindowText'#13#10'        Style.Font.Height = -14'#13#10'    ' +
                  '    Style.Font.Name = #23435#20307'#13#10'        Style.Font.Style = [' +
                  ']'#13#10'        Style.IsFontAssigned = True'#13#10'        Properties.Align' +
                  'ment.Vert = taVCenter'#13#10'        Properties.WordWrap = True'#13#10'     ' +
                  '   Transparent = True'#13#10'        Height = 21'#13#10'        Width = 77'#13#10 +
                  '        AnchorY = 53'#13#10'      end'#13#10'    end'#13#10'  end'#13#10'end'
                QueryButtonPanelWidth = 165
                HideInputQueryButton = False
              end
            end
          end
          object GroupBox4: TGroupBox
            Left = 0
            Top = 209
            Width = 830
            Height = 46
            Align = alTop
            Caption = 'ABFramkWorkDBNavigatorU'
            TabOrder = 2
            object ABDBNavigator1: TABDBNavigator
              Left = 2
              Top = 15
              Width = 826
              Height = 25
              Align = alTop
              BevelOuter = bvNone
              Caption = 'ABDBNavigator1'
              ShowCaption = False
              TabOrder = 0
              BigGlyph = False
              ImageLayout = blGlyphLeft
              DataSource = DataSource2
              VisibleButtons = [nbFirstRecord, nbPreviousRecord, nbNextRecord, nbLastRecord, nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel, nbQuerySpacer, nbQuery, nbReport, nbCustomSpacer, nbCustom1, nbCustom2, nbCustom3, nbCustom4, nbCustom5, nbCustom6, nbCustom7, nbCustom8, nbCustom9, nbCustom10, nbExitSpacer, nbExit]
              ButtonRangeType = RtAll
              BtnCustom1ImageIndex = -1
              BtnCustom2ImageIndex = -1
              BtnCustom3ImageIndex = -1
              BtnCustom4ImageIndex = -1
              BtnCustom5ImageIndex = -1
              BtnCustom6ImageIndex = -1
              BtnCustom7ImageIndex = -1
              BtnCustom8ImageIndex = -1
              BtnCustom9ImageIndex = -1
              BtnCustom10ImageIndex = -1
              BtnCustom1Caption = #33258#23450#20041'1'
              BtnCustom2Caption = #33258#23450#20041'2'
              BtnCustom3Caption = #33258#23450#20041'3'
              BtnCustom4Caption = #33258#23450#20041'4'
              BtnCustom5Caption = #33258#23450#20041'5'
              BtnCustom6Caption = #33258#23450#20041'6'
              BtnCustom7Caption = #33258#23450#20041'7'
              BtnCustom8Caption = #33258#23450#20041'8'
              BtnCustom9Caption = #33258#23450#20041'9'
              BtnCustom10Caption = #33258#23450#20041'10'
              BtnCustom1Kind = cxbkStandard
              BtnCustom2Kind = cxbkStandard
              BtnCustom3Kind = cxbkStandard
              BtnCustom4Kind = cxbkStandard
              BtnCustom5Kind = cxbkStandard
              BtnCustom6Kind = cxbkStandard
              BtnCustom7Kind = cxbkStandard
              BtnCustom8Kind = cxbkStandard
              BtnCustom9Kind = cxbkStandard
              BtnCustom10Kind = cxbkStandard
              ApprovedRollbackButton = nbNull
              ApprovedCommitButton = nbNull
              ButtonControlType = ctSingle
            end
          end
        end
      end
    end
  end
  object ABQuery2: TABQuery
    Tag = 111111
    ActiveStoredUsage = []
    ReadOnly = False
    SQL.Strings = (
      'select * from ABSys_Org_Tree')
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Tree')
    IndexListDefs = <
      item
        Name = 'IX_ABSys_Org_Tree'
        Fields = 'TR_Order'
      end
      item
        Name = 'IX_ABSys_Org_Tree_1'
        Fields = 'TR_Name'
        Options = [ixUnique]
      end
      item
        Name = 'IX_ABSys_Org_Tree_2'
        Fields = 'TR_Code'
        Options = [ixUnique]
      end
      item
        Name = 'PK_ABSys_Org_Tree'
        Fields = 'TR_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ABSys_Org_Tree')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelCaption = #21015#39033#20027#34920
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    OnBeforeShowDBPanel = ABQuery2BeforeShowDBPanel
    OnAfterShowDBPanel = ABQuery2AfterShowDBPanel
    Left = 120
    Top = 80
  end
  object DataSource2: TDataSource
    DataSet = ABQuery2
    Left = 28
    Top = 80
  end
  object DataSource3: TDataSource
    DataSet = ABQuery3
    Left = 28
    Top = 136
  end
  object PopupMenu1: TPopupMenu
    Left = 135
    Top = 409
  end
  object ABQuery3: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    MasterSource = DataSource2
    MasterFields = 'TR_Guid'
    DetailFields = 'TI_TR_Guid'
    SQL.Strings = (
      'select * from ABSys_Org_TreeItem'
      'where Ti_Tr_Guid=:Tr_Guid')
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_TreeItem')
    IndexListDefs = <
      item
        Name = 'IX_ABSys_Org_TreeItem'
        Fields = 'TI_Name;TI_TR_Guid'
        Options = [ixUnique]
      end
      item
        Name = 'IX_ABSys_Org_TreeItem_1'
        Fields = 'TI_Order;TI_ParentGuid;TI_TR_Guid'
      end
      item
        Name = 'IX_ABSys_Org_TreeItem_2'
        Fields = 'TI_Code;TI_TR_Guid'
        Options = [ixUnique]
      end
      item
        Name = 'PK_ABSys_Org_TreeItem'
        Fields = 'TI_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ABSys_Org_TreeItem')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelCaption = #21015#39033#26126#32454#34920
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 120
    Top = 128
    ParamData = <
      item
        Name = 'TR_GUID'
        DataType = ftWideString
        ParamType = ptInput
        Size = 100
        Value = '{0010FE68-22E2-47EA-8239-DB5BF04E9E18}'
      end>
  end
  object ABFrxReport1: TABFrxReport
    Version = '4.15.6'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #39044#35774
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41905.707218159720000000
    ReportOptions.LastChange = 41905.707218159720000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    ReportType = rtExtend
    Left = 48
    Top = 408
    Datasets = <>
    Variables = <>
    Style = <>
  end
  object ImageList1: TImageList
    Left = 48
    Top = 264
    Bitmap = {
      494C01010B001900040010001000FFFFFFFFFF00FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001002000000000000030
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00003E3E3E008ED4FF0000737300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000003E3E3E0000DC
      DC008ED4FF006BC6FF006BF7FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001840
      9000205CA0001840900018409000184090001840900018409000184090001028
      8000184090000000000000000000000000000000000000000000000000001840
      9000205CA0001840900018409000184090001840900018409000184090001028
      8000184090000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003E3E3E0000DCDC008ED4FF008ED4
      FF0099FFFF00000000006BF7FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000205C
      A0002078B000205CA00020202000202020002020200020202000205CA0001840
      9000205CA000000000000000000000000000000000000000000000000000205C
      A0002078B000205CA0002020200020202000205CA000205CA000205CA0001840
      9000205CA0000000000000000000000000000000000000000000000000000000
      000000000000000000003E3E3E0000DCDC0099FFFF008ED4FF008ED4FF0099FF
      FF006BC6FF00AAAAAA006BF7FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002078
      B0002890C00018409000303030002078B0002078B0003030300018409000205C
      A0002078B0000000000000000000000000000000000000000000000000002078
      B0002890C0002078B0002078B0001840900030303000205CA0002078B000205C
      A0002078B0000000000000000000000000000000000000000000000000000000
      00003E3E3E0000DCDC0099FFFF0099FFFF0099FFFF008ED4FF0099FFFF008ED4
      FF0000000000AAAAAA006BF7FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002890
      C00030ACD00040404000404040002890C0002890C00040404000404040002078
      B0002890C0000000000000000000000000000000000000000000000000002890
      C00030ACD0002890C0002890C0002890C00040404000404040002890C0002078
      B0002890C0000000000000000000000000000000000000000000000000006262
      620099FFFF0099FFFF008ED4FF0099FFFF008ED4FF0099FFFF008ED4FF006BC6
      FF00DADADA00000000006BF7FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000030AC
      D00030C8E000505050005050500030ACD00030ACD00050505000505050002890
      C00030ACD00000000000000000000000000000000000000000000000000030AC
      D00030C8E00030ACD0002890C0005050500050505000505050002078B0002890
      C00030ACD00000000000000000000000000000000000000000000000000099FF
      FF0099FFFF0099FFFF0099FFFF0099FFFF008ED4FF0099FFFF0099FFFF000000
      000000000000DADADA006BF7FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000030C8
      E00038E0F0002890C000606060002890C0002890C000606060002890C00030AC
      D00030C8E00000000000000000000000000000000000000000000000000030C8
      E00038E0F00030C8E000606060002890C0002890C000606060006060600030AC
      D00030C8E00000000000000000000000000000000000000000006262620099FF
      FF0099FFFF0099FFFF0099FFFF0099FFFF0099FFFF00FFFFFF0062626200FFFF
      FF00FFFFFF00000000006BF7FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000038E0
      F00040FCFF0038E0F0007070700070707000707070007070700038E0F00030C8
      E00038E0F00000000000000000000000000000000000000000000000000038E0
      F00040FCFF00707070007070700038E0F00038E0F000707070007070700030C8
      E00038E0F000000000000000000000000000000000000000000099FFFF0099FF
      FF0099FFFF0099FFFF0099FFFF00FFFFFF0062626200FFFFFF00FF734800FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000038E0
      F00040FCFF0030ACD0007070700030ACD00030ACD0007070700030ACD00030C8
      E00038E0F00000000000000000000000000000000000000000000000000038E0
      F00040FCFF00707070007070700038E0F00038E0F000707070007070700030C8
      E00038E0F000000000000000000000000000000000006262620099FFFF0099FF
      FF0099FFFF00FFFFFF0062626200FFFFFF00FF734800FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000006BF7FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000030C8
      E00038E0F000606060006060600030C8E00030C8E000606060006060600030AC
      D00030C8E00000000000000000000000000000000000000000000000000030C8
      E00038E0F0002890C0006060600030C8E00030C8E000606060002890C00030AC
      D00030C8E0000000000000000000000000000000000099FFFF0099FFFF00FFFF
      FF0062626200FFFFFF00FF734800FFFFFF00FFFFFF00FF734800FFFFFF00FFFF
      FF00000000006BF7FF0056565600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000030AC
      D00030C8E00030ACD0005050500050505000505050005050500030ACD0002890
      C00030ACD00000000000000000000000000000000000000000000000000030AC
      D00030C8E00030ACD0005050500050505000505050005050500030ACD0002890
      C00030ACD00000000000000000000000000062626200FFFFFF00626262000000
      00006262620000000000FFFFFF00FF734800FFFFFF00FFFFFF00FF7348000000
      00006BF7FF00D4FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002890
      C00030ACD0002890C0002890C0002890C0002890C0002890C0002890C0002078
      B0002890C0000000000000000000000000000000000000000000000000002890
      C00030ACD0002890C0002890C0002890C0002890C0002890C0002890C0002078
      B0002890C0000000000000000000000000000000000000000000000000000000
      0000626262006BF7FF0000000000FFFFFF00FF734800FFFFFF0000000000D4FF
      FF00626262000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000626262006BF7FF006BF7FF0000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000000000
      000062626200D4FFFF0062626200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000003232320032323200323232003232
      3200323232003232320032323200323232003232320032323200323232003232
      3200323232000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B6B6B600B6B6B600B6B6B600B6B6
      B600B6B6B600B6B6B600B6B6B600B6B6B600B6B6B600B6B6B600B6B6B600B6B6
      B600B6B6B6003232320000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001840
      9000205CA0001840900018409000184090001840900018409000184090001028
      8000184090000000000000000000000000000000000000000000000000001840
      9000205CA0001840900018409000184090001840900018409000184090001028
      800018409000000000000000000000000000B6B6B600B6B6B600B6B6B600B6B6
      B6007A7A7A009E9E9E00E6E6E600E6E6E600E6E6E600B6B6B600B6B6B600B6B6
      B600B6B6B6003232320032323200000000000000000000000000000000001840
      9000205CA0001840900018409000184090001840900018409000184090001028
      800018409000000000000000000000000000000000000000000000000000205C
      A0002078B000205CA00020202000202020002020200020202000205CA0001840
      9000205CA000000000000000000000000000000000000000000000000000205C
      A0002078B000205CA000205CA000205CA0001840900020202000202020001840
      9000205CA000000000000000000000000000B6B6B60062626200626262006262
      62007A7A7A007A7A7A009E9E9E009E9E9E009E9E9E0062626200626262006262
      6200B6B6B600323232003232320000000000000000000000000000000000205C
      A0002078B000205CA00020202000202020002020200020202000205CA0001840
      9000205CA0000000000000000000000000000000000000000000000000002078
      B0002890C00018409000303030002078B0002078B0003030300018409000205C
      A0002078B0000000000000000000000000000000000000000000000000002078
      B0002890C0002078B0002078B0002078B0002078B0003030300030303000205C
      A0002078B000000000000000000000000000B6B6B600B6B6B600B6B6B600B6B6
      B600B6B6B600B6B6B600B6B6B600B6B6B6007A7A7A007A7A7A002525FF007A7A
      7A007A7A7A003232320032323200000000000000000000000000000000002078
      B0002890C00018409000303030002078B0002078B0003030300018409000205C
      A0002078B0000000000000000000000000000000000000000000000000002890
      C00030ACD00040404000404040002890C0002890C00040404000404040002078
      B0002890C0000000000000000000000000000000000000000000000000002890
      C00030ACD00040404000404040002890C0002890C00040404000404040002078
      B0002890C000000000000000000000000000CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE0080808000626262006262620062626200626262006262
      6200626262006262620062626200323232000000000000000000000000002890
      C00030ACD00040404000404040002890C0002890C00040404000404040002078
      B0002890C00000000000000000000000000000000000000000000000000030AC
      D00030C8E00030ACD00030ACD00030ACD00030ACD00050505000505050002890
      C00030ACD00000000000000000000000000000000000000000000000000030AC
      D00030C8E00030ACD0005050500030ACD00030ACD00050505000505050002890
      C00030ACD00000000000000000000000000000000000F2F2F200DADADA00DADA
      DA00DADADA00DADADA0080808000626262006262620062626200626262007A7A
      7A006262620062626200626262003232320000000000000000000000000030AC
      D00030C8E00030ACD00030ACD00030ACD00030ACD00050505000505050002890
      C00030ACD00000000000000000000000000000000000000000000000000030C8
      E00038E0F00030C8E00030C8E00030C8E0002890C0006060600030C8E00030AC
      D00030C8E00000000000000000000000000000000000000000000000000030C8
      E00038E0F00030C8E000606060002890C00030C8E000606060006060600030AC
      D00030C8E000000000000000000000000000000000000000000000000000F2F2
      F200F2F2F200F2F2F20080808000626262006262620062626200626262007A7A
      7A006262620062626200626262003232320000000000000000000000000030C8
      E00038E0F00030C8E00030C8E00030C8E0002890C000606060002890C00030AC
      D00030C8E00000000000000000000000000000000000000000000000000038E0
      F00040FCFF0038E0F00038E0F00070707000707070002890C00038E0F00030C8
      E00038E0F00000000000000000000000000000000000000000000000000038E0
      F00040FCFF0038E0F00038E0F0007070700038E0F000707070007070700030C8
      E00038E0F0000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000626262006262620062626200626262007A7A
      7A006262620062626200626262003232320000000000000000000000000038E0
      F00040FCFF0038E0F0007070700070707000707070007070700038E0F00030C8
      E00038E0F00000000000000000000000000000000000000000000000000038E0
      F00040FCFF0038E0F00038E0F00038E0F00038E0F0007070700038E0F00030C8
      E00038E0F00000000000000000000000000000000000000000000000000038E0
      F00040FCFF0038E0F00038E0F0007070700030C8E000707070007070700030C8
      E00038E0F0000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000626262006262620062626200000000000000
      00008080800062626200626262003232320000000000000000000000000038E0
      F00040FCFF0038E0F000707070007070700038E0F00038E0F00038E0F00030C8
      E00038E0F00000000000000000000000000000000000000000000000000030C8
      E00038E0F00030C8E0006060600030C8E00030C8E000606060006060600030AC
      D00030C8E00000000000000000000000000000000000000000000000000030C8
      E00038E0F00030C8E00030C8E00030C8E00060606000606060006060600030AC
      D00030C8E0000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000626262006262620062626200626262007A7A
      7A006262620062626200626262003232320000000000000000000000000030C8
      E00038E0F00030C8E000606060006060600030C8E0006060600030C8E00030AC
      D00030C8E00000000000000000000000000000000000000000000000000030AC
      D00030C8E00030ACD000505050002078B00030ACD00050505000505050002890
      C00030ACD00000000000000000000000000000000000000000000000000030AC
      D00030C8E00030ACD00030ACD00030ACD0005050500050505000505050002890
      C00030ACD0000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000B6B6B600CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00B6B6B6003232320000000000000000000000000030AC
      D00030C8E00030ACD00050505000505050002078B0005050500030ACD0002890
      C00030ACD0000000000000000000000000000000000000000000000000002890
      C00030ACD0002890C0002890C0002890C0002890C0002890C0002890C0002078
      B0002890C0000000000000000000000000000000000000000000000000002890
      C00030ACD0002890C0002890C0002890C0002890C0002890C0002890C0002078
      B0002890C0000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00323232000000000000000000000000002890
      C00030ACD0002890C0002890C0002890C0002890C0002890C0002890C0002078
      B0002890C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000626262006262620062626200626262006262
      6200626262006262620062626200323232000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000080800072D0D600000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000003232320032323200323232003232
      3200323232003232320032323200323232003232320032323200323232003232
      3200323232000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000072D0
      D6000096960072D0D60072D0D600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B6B6B600B6B6B600B6B6B600B6B6
      B600B6B6B600B6B6B600B6B6B600B6B6B600B6B6B600B6B6B600B6B6B600B6B6
      B600B6B6B6003232320000000000000000000000000000000000000000001840
      9000205CA0001840900018409000184090001840900018409000184090001028
      8000184090000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000072D0D60072D0D60072D0
      D6000000000078E4F30072D0D600000000000000000000000000000000001840
      9000205CA0001840900018409000184090001840900018409000184090001028
      800018409000000000000000000000000000B6B6B600B6B6B600B6B6B600B6B6
      B6007A7A7A0092929200DADADA00DADADA00DADADA00B6B6B600B6B6B600B6B6
      B600B6B6B600323232003232320000000000000000000000000000000000205C
      A0002078B000205CA000205CA000202020002020200018409000205CA0001840
      9000205CA0000000000000000000000000000000000000000000000000000000
      000000000000000000000000000072D0D60072D0D60072D0D60072D0D6000080
      800078E4F30078E4F30072D0D60000000000000000000000000000000000205C
      A0002078B0002020200020202000205CA000205CA00020202000202020001840
      9000205CA000000000000000000000000000B6B6B60062626200626262006262
      62007A7A7A007A7A7A0092929200929292009292920062626200626262006262
      6200B6B6B6003232320032323200000000000000000000000000000000002078
      B0002890C0002078B0002078B00030303000303030002078B0002078B000205C
      A0002078B0000000000000000000000000000000000000000000000000000000
      00000000000072D0D60072D0D60072D0D60072D0D60072D0D60072D0D6000000
      000078E4F30078E4F30072D0D600000000000000000000000000000000002078
      B0002890C00030303000303030002078B0002078B0002078B00030303000205C
      A0002078B000000000000000000000000000B6B6B600B6B6B600B6B6B600B6B6
      B600B6B6B600B6B6B600B6B6B600B6B6B6007A7A7A007A7A7A002525FF007A7A
      7A007A7A7A003232320032323200000000000000000000000000000000002890
      C00030ACD0002890C0002890C00040404000404040002890C0002890C0002078
      B0002890C00000000000000000000000000000000000000000000000000072D0
      D60072D0D60072D0D60072D0D60072D0D60072D0D60072D0D6000080800078E4
      F30078E4F30078E4F30072D0D600000000000000000000000000000000002890
      C00030ACD0002890C00040404000205CA0002890C0002890C000404040002078
      B0002890C000000000000000000000000000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C000DADADA00A4A0A000B6B6B600DADADA0092929200E6E6
      E600DADADA0086868600A4A0A0006262620000000000000000000000000030AC
      D00030C8E00030ACD00030ACD000505050005050500030ACD00030ACD0002890
      C00030ACD000000000000000000000000000000000000000000078E4F30072D0
      D60072D0D60072D0D60072D0D60072D0D60078E4F300D4FFFF000000000078E4
      F30078E4F30078E4F30072D0D6000000000000000000000000000000000030AC
      D00030C8E00030ACD0002890C000505050005050500030ACD00030ACD0002890
      C00030ACD00000000000000000000000000000000000FFFFFF00DADADA00DADA
      DA00DADADA00DADADA00DADADA00B6B6B600B6B6B600DADADA0092929200E6E6
      E600DADADA0086868600B6B6B6006262620000000000000000000000000030C8
      E00038E0F00030C8E00030C8E000606060006060600030C8E00030C8E00030AC
      D00030C8E000000000000000000000000000000000000000000072D0D60072D0
      D60072D0D60072D0D60078E4F300D4FFFF000000000078E4F30078E4F30078E4
      F30078E4F30078E4F30072D0D6000000000000000000000000000000000030C8
      E00038E0F00030C8E00030C8E00030C8E0002890C0006060600030C8E00030AC
      D00030C8E000000000000000000000000000000000000000000000000000E6E6
      E600E6E6E600E6E6E600DADADA00B6B6B600B6B6B600DADADA00DADADA00E6E6
      E600DADADA0086868600B6B6B6006262620000000000000000000000000038E0
      F00040FCFF0038E0F00038E0F000707070007070700038E0F00038E0F00030C8
      E00038E0F0000000000000000000000000000000000078E4F30072D0D60072D0
      D60078E4F300D4FFFF000000000078E4F30078E4F30078E4F30078E4F30078E4
      F30078E4F30078E4F300D4FFFF000000000000000000000000000000000038E0
      F00040FCFF0038E0F00038E0F00038E0F00038E0F000707070007070700030C8
      E00038E0F0000000000000000000000000000000000000000000000000000000
      00000000000000000000DADADA00B6B6B600B6B6B600B6B6B600B6B6B600B6B6
      B600B6B6B600B6B6B600B6B6B6006262620000000000000000000000000038E0
      F00040FCFF0038E0F00030ACD000707070007070700038E0F00038E0F00030C8
      E00038E0F0000000000000000000000000000000000072D0D60078E4F300D4FF
      FF000000000078E4F30078E4F30078E4F30078E4F30078E4F30078E4F30078E4
      F300D4FFFF00D4FFFF00000000000000000000000000000000000000000038E0
      F00040FCFF00707070007070700038E0F00038E0F000707070007070700030C8
      E00038E0F0000000000000000000000000000000000000000000000000000000
      00000000000000000000DADADA00DADADA00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00B6B6B6006262620000000000000000000000000030C8
      E00038E0F00030C8E00030C8E000606060006060600030C8E00030C8E00030AC
      D00030C8E000000000000000000000000000D4FFFF00D4FFFF000000000078E4
      F30078E4F30078E4F30078E4F30078E4F30078E4F30078E4F300D4FFFF00D4FF
      FF000000000000000000000000000000000000000000000000000000000030C8
      E00038E0F0002890C0006060600030C8E00030C8E000606060002890C00030AC
      D00030C8E0000000000000000000000000000000000000000000000000000000
      00000000000000000000DADADA00DADADA00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00B6B6B6006262620000000000000000000000000030AC
      D00030C8E00030ACD00030ACD0002890C000505050002078B00030ACD0002890
      C00030ACD00000000000000000000000000000000000000000000000000078E4
      F30078E4F30078E4F30078E4F30078E4F30078E4F300D4FFFF00000000000000
      00000000000000000000000000000000000000000000000000000000000030AC
      D00030C8E00030ACD0005050500050505000505050005050500030ACD0002890
      C00030ACD0000000000000000000000000000000000000000000000000000000
      00000000000000000000DADADA00DADADA00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00B6B6B600626262000000000000000000000000002890
      C00030ACD0002890C0002890C0002890C0002890C0002890C0002890C0002078
      B0002890C00000000000000000000000000000000000000000000000000078E4
      F30078E4F30078E4F30078E4F30078E4F3000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002890
      C00030ACD0002890C0002890C0002890C0002890C0002890C0002890C0002078
      B0002890C0000000000000000000000000000000000000000000000000000000
      00000000000000000000DADADA00DADADA00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00B6B6B600626262000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D4FF
      FF0078E4F30078E4F300D4FFFF00D4FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DADADA00DADADA00DCDC0000DCDC0000DCDC0000DCDC
      0000DCDC0000DCDC000062626200626262000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000000000
      0000D4FFFF00D4FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001840
      9000184090001840900018409000184090001840900018409000184090001840
      9000184090000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFF80000E003E003FFE00000
      E003E003FF800000E003E003FE000000E003E003F8000000E003E003E0000000
      E003E003E0000000E003E003C0000000E003E003C0000000E003E00380000000
      E003E00380000000E003E00300010000E003E00310010000E003E003F0030000
      E003E003F03F0000E003E003F17F0000FFFFFFFFFFFFFFFFE003E0030003E003
      E003E0030001E003E003E0030001E003E003E0030001E003E003E0030000E003
      E003E0030000E003E003E0038000E003E003E003E000E003E003E003FC00E003
      E003E003FC10E003E003E003FC00E003E003E003FC00E003E003E003FC00E003
      E003E003FC00E003E003E003FFFFE003FFFFFFF9FFFFFFFFE003FFE0E0030003
      E003FF80E0030001E003FE00E0030001E003F800E0030001E003E000E0030000
      E003C000E0030000E003C000E0038000E0038000E003E000E0038000E003FC00
      E0030001E003FC00E0030007E003FC00E003001FE003FC00E003C07FE003FC00
      E003C07FE003FC00E003F1FFE003FFFF}
  end
  object ABFieldCaptionQuery1: TABFieldCaptionQuery
    ActiveStoredUsage = []
    ReadOnly = True
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    IndexListDefs = <>
    Left = 128
    Top = 320
  end
  object ABDictionaryQuery1: TABDictionaryQuery
    ActiveStoredUsage = []
    ReadOnly = False
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    IndexListDefs = <>
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 128
    Top = 264
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    Left = 128
    Top = 216
  end
  object FDPhysMSSQLDriverLink1: TFDPhysMSSQLDriverLink
    Left = 942
    Top = 224
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 926
    Top = 160
  end
  object FDQuery1: TFDQuery
    Aggregates = <
      item
      end>
    SQL.Strings = (
      'select * from ABSys_Org_Tree')
    Left = 877
    Top = 312
  end
end
