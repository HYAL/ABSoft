{
注册组件单元
}
unit ABFramkWorkRegU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubPropertyEditU,

  ABThirdConnU,

  ABFramkWorkQueryU,
  ABFramkWorkDictionaryQueryU,
  ABFramkWorkQuerySelectFieldPanelU,
  ABFramkWorkQuerySelectFieldPopupEditU,
  ABFramkWorkFastReportU,
  ABFramkWorkcxGridU,
  ABFramkWorkDBPanelU,
  ABFramkWorkDBNavigatorU,
  ABFramkWorkControlU,
  ABFramkWorkQueryAllFieldComboxU,
  ABFramkWorkVarU,
  ABFramkWorkFuncU,
  ABFramkWorkBillInputU,

  DesignIntf,Classes,DesignEditors;

type
  //数据固定查询容器设计期编辑
  TABQuerySelectFieldPanelComponentEditor = class(TComponentEditor)
    function GetVerb(Index: Integer): string; override;
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerbCount: Integer; override;
  end;

  //数据编辑容器设计期编辑
  TABDBPanelComponentEditor = class(TComponentEditor)
    function GetVerb(Index: Integer): string; override;
    procedure ExecuteVerb(Index: Integer); override;
    function GetVerbCount: Integer; override;
  end;
  //功能点下拉列框的属性编辑器
  TABcxButtonFuncPointsProperty = class(TStringProperty)
  public
    function  GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;
  //审批单据列表编辑器
  TABApproveBillProperty = class(TStringProperty)
  public
    function  GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;
  //单据导入编号编辑器
  TABBillInputProperty = class(TStringProperty)
  public
    function  GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;


procedure Register;

implementation



{ TABDBPanelComponentEditor }

procedure TABDBPanelComponentEditor.ExecuteVerb(Index: Integer);
begin
  inherited;
  case Index of
    1:
    begin
      if Component is TABDBPanel then TABDBPanel(Component).RefreshControls(2,2,true);
    end;
    2:
    begin
      if Component is TABDBPanel then TABDBPanel(Component).RefreshControls(2,1,true);
    end;
    3:
    begin
      if Component is TABDBPanel then TABDBPanel(Component).RefreshControls(3,2,true);
    end;
    4:
    begin
      if Component is TABDBPanel then TABDBPanel(Component).RefreshControls(3,1,true);
    end;
    5:
    begin
      if Component is TABDBPanel then TABDBPanel(Component).RefreshControls(1,2,true);
    end;
    6:
    begin
      if Component is TABDBPanel then TABDBPanel(Component).RefreshControls(1,1,true);
    end;
    7:
    begin
      if Component is TABDBPanel then TABDBPanel(Component).RefreshControls(1,1,False);
    end;
    8:
    begin
      if Component is TABDBPanel then TABDBPanel(Component).FreeControls;
    end;
    10:
    begin
      if Component is TABDBPanel then
      begin
        TABDBPanel(Component).PublicSizePosition:=true;
        TABDBPanel(Component).SaveControls;
      end;
    end;
    11:
    begin
      if Component is TABDBPanel then
      begin
        TABDBPanel(Component).PublicSizePosition:=false;
        TABDBPanel(Component).SaveControls;
      end;
    end;
    13:
    begin
      if Component is TABDBPanel then TABDBPanel(Component).SetupHaveField;
    end;
    14:
    begin
      if Component is TABDBPanel then TABDBPanel(Component).SetupNoHaveField;
    end;
    15:
    begin
      if Component is TABDBPanel then TABDBPanel(Component).SetupReadOnlyField;
    end;
    16:
    begin
      if Component is TABDBPanel then TABDBPanel(Component).SetupEditField;
    end;
    17:
    begin
      if Component is TABDBPanel then TABDBPanel(Component).ClearSetupField;
    end;
  end;
end;

function TABDBPanelComponentEditor.GetVerb(Index: Integer): string;
begin
  Result := ABDBPanelPopupMenuStrs[Index];
end;

function TABDBPanelComponentEditor.GetVerbCount: Integer;
begin
  Result := Length(ABDBPanelPopupMenuStrs);
end;

{ TABQuerySelectFieldPanelComponentEditor }

procedure TABQuerySelectFieldPanelComponentEditor.ExecuteVerb(Index: Integer);
begin
  inherited;
  case Index of
    0:
    begin
      if Component is TABQuerySelectFieldPanel then
        TABQuerySelectFieldPanel(Component).DesignInputControls
      else if Component is TABQuerySelectFieldPopupEdit then
        TABQuerySelectFieldPopupEdit(Component).DesignInputControls;
    end;
  end;
end;

function TABQuerySelectFieldPanelComponentEditor.GetVerb(Index: Integer): string;
begin
  Result := ABDBQueryPanelPopupMenuStrs[Index];
end;

function TABQuerySelectFieldPanelComponentEditor.GetVerbCount: Integer;
begin
  Result := Length(ABDBQueryPanelPopupMenuStrs);
end;


{ TABcxButtonFuncPointsProperty }

function TABcxButtonFuncPointsProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paSortList];
end;

procedure TABcxButtonFuncPointsProperty.GetValues(Proc: TGetStrProc);
var
  i:LongInt;
begin
  if ABInitConn then
  begin
    if Assigned(ABFuncPointStrings) then
    begin
      for I := 0 to ABFuncPointStrings.Count-1 do
      begin
        Proc(ABFuncPointStrings[i]);
      end;
    end;
  end;
end;

{ TABApproveBillProperty }

function TABApproveBillProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paSortList];
end;

procedure TABApproveBillProperty.GetValues(Proc: TGetStrProc);
var
  i:LongInt;
begin
  if ABInitConn then
  begin
    if Assigned(ABApproveBillStrings) then
    begin
      for I := 0 to ABApproveBillStrings.Count-1 do
      begin
        Proc(ABApproveBillStrings[i]);
      end;
    end;
  end;
end;

{ TABBillInputProperty }

function TABBillInputProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paSortList];
end;

procedure TABBillInputProperty.GetValues(Proc: TGetStrProc);
var
  i:LongInt;
begin
  if ABInitConn then
  begin
    if Assigned(ABBillInputStrings) then
    begin
      for I := 0 to ABBillInputStrings.Count-1 do
      begin
        Proc(ABBillInputStrings[i]);
      end;
    end;
  end;
end;

procedure Register;
begin
  //ABFramkWorkControlU
  RegisterComponents('ABFramkWork',
   [
    TABcxTimeEdit                        ,
    TABcxDateEdit                        ,
    TABcxDateTimeEdit                    ,
    TABcxRadioGroup                      ,
    TABcxCheckBox                        ,
    TABcxMemo                            ,
    TABcxComBoBoxMemo                    ,
    TABcxDirSelect                       ,
    TABcxFileSelect                      ,
  //  TABcxFieldSelects                  ,
  //  TABcxFileBlobField                 ,
    TABcxSQLFieldSelects                 ,
    TABcxPrintSelect                     ,
    TABcxFieldOrder                      ,
    TABcxLookupComboBox                  ,
    TABcxExtLookupComboBox               ,
    TABcxCheckComboBox                   ,
    TABcxComboBox                        ,
    TABcxTreeViewPopupEdit               ,
    TABcxImage                           ,
    TABcxTextEdit                        ,
    TABcxMaskEdit                        ,
    TABcxButtonEdit                      ,
    TABcxSpinEdit                        ,
    TABcxCalcEdit                        ,
    TABcxHyperLinkEdit                   ,
    TABcxCurrencyEdit                    ,
    TABcxBlobEdit                        ,
    TABcxMRUEdit                         ,
    TABcxProgressBar                     ,
    TABcxTrackBar                        ,
    TABcxRichEdit                        ,
    TABcxPopupEdit                       ,
    TABcxCheckGroup                      ,
    TABcxLabel                           ,
    TABcxImageComboBox                   ,
    TABcxColorComboBox                   ,
    TABcxCheckListBox                    ,
    TABcxFontNameComboBox                ,
    TABcxShellComboBox                   ,
    TABcxTreeList                        ,
    TABcxListBox                         ,
    TABcxPivotGrid                       ,
    TABcxHeader                          ,
    TABcxSpinButton                      ,
    TABcxMCListBox                       ,
    TABcxListView                        ,
    TABcxSplitter                        ,
    TABcxGroupBox                        ,
    TABdxStatusBar                       ,
    TABcxPageControl                     ,
    TABcxShellListView                   ,
    TABcxShellTreeView                   ,
    TABcxTabControl                      ,
    TABcxVirtualTreeList                 ,
    TABcxRadioButton                     ,
    TABcxButton                          ,
    TABcxTreeView
    ]);
  RegisterComponents('ABFramkWorkDB',
   [
    TABcxDBTimeEdit                      ,
    TABcxDBDateEdit                      ,
    TABcxDBDateTimeEdit                  ,
    TABcxDBRadioGroup                    ,
    TABcxDBCheckBox                      ,
    TABcxDBMemo                          ,
    TABcxDBComBoBoxMemo                  ,
    TABcxDBDirSelect                     ,
    TABcxDBFileSelect                    ,
    TABcxDBFieldSelects                  ,
    TABcxDBSQLFieldSelects               ,
    TABcxDBFileBlobField                 ,
    TABcxDBPrintSelect                   ,
    TABcxDBFieldOrder                    ,
    TABcxDBLookupComboBox                ,
    TABcxDBExtLookupComboBox             ,
    TABcxDBCheckComboBox                 ,
    TABcxDBComboBox                      ,
    TABcxDBTreeViewPopupEdit             ,
    TABcxDBImage                         ,
    TABcxDBTextEdit                      ,
    TABcxDBMaskEdit                      ,
    TABcxDBButtonEdit                    ,
    TABcxDBSpinEdit                      ,
    TABcxDBCalcEdit                      ,
    TABcxDBHyperLinkEdit                 ,
    TABcxDBCurrencyEdit                  ,
    TABcxDBBlobEdit                      ,
    TABcxDBMRUEdit                       ,
    TABcxDBProgressBar                   ,
    TABcxDBTrackBar                      ,
    TABcxDBRichEdit                      ,
    TABcxDBPopupEdit                     ,
    TABcxDBCheckGroup                    ,
    TABcxDBLabel                         ,
    TABcxDBImageComboBox                 ,
    TABcxDBColorComboBox                 ,
    TABcxDBFontNameComboBox              ,
    TABcxDBShellComboBox                 ,
    TABcxDBCheckListBox                  ,
    TABcxDBTreeList                      ,
    TABcxDBListBox                       ,
    TABcxDBPivotGrid                     ,
    TABcxDBTreeView                      ,
    TABdxDBStatusBar                     ,
    TABDBFieldCaption
    ]);


  RegisterComponents('ABFramkWork',
   [
    TABFrxReport
    ]);

  RegisterComponents('ABFramkWorkDB',
   [
    TABcxGrid           ,
    TABDBPanel            ,
    TABDBNavigator     ,

    TABQuerySelectFieldPanel    ,
    TABQuerySelectFieldPopupEdit,
    TABQueryAllFieldCombox       ,

    TABFieldCaptionQuery  ,
    TABDictionaryQuery    ,
    TABQuery              ,
    TABDatasource         ,
    TABBillInputButton
    ]);

  RegisterPropertyEditor(TypeInfo(string),TABcxButton ,'FuncPoints',TABcxButtonFuncPointsProperty);

  RegisterComponentEditor(TABQuerySelectFieldPanel      , TABQuerySelectFieldPanelComponentEditor);
  RegisterComponentEditor(TABQuerySelectFieldPopupEdit  , TABQuerySelectFieldPanelComponentEditor);
  RegisterComponentEditor(TABDBPanel                    , TABDBPanelComponentEditor);

  RegisterPropertyEditor(TypeInfo(string),TABDBNavigator , 'ApproveBill',TABApproveBillProperty);
  RegisterPropertyEditor(TypeInfo(string),TABDBNavigator , 'ApproveMainGuidField',TABFieldComboxProperty);
  RegisterPropertyEditor(TypeInfo(string),TABDBNavigator , 'ApproveStateField',TABFieldComboxProperty);

  RegisterPropertyEditor(TypeInfo(string),TABBillInputButton , 'Code',TABBillInputProperty);

end;


end.










