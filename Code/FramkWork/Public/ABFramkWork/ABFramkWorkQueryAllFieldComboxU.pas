{
下拉框查询单元
通过数据字典中查询字段来查询数据集数据
}
unit ABFramkWorkQueryAllFieldComboxU;

interface
{$I ..\ABInclude.ini}


uses
  ABPubPanelU,
  ABPubConstU,
  ABPubFuncU,
  ABPubFrameU,
  ABPubDBU,

  ABThirdFuncU,
  ABThirdQueryU,
  ABThirdCustomQueryU,
  ABThirdImportDataU,

  ABFramkWorkControlU,
  ABFramkWorkConstU,
  ABFramkWorkFuncU,
  ABFramkWorkDictionaryQueryU,

  SysUtils,Classes,Controls,Forms,ExtCtrls,StdCtrls,
  DB,DateUtils,

  Variants,
  cxContainer, ComCtrls, cxCalendar, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxEdit, dxCore, cxDateUtils, cxMaskEdit,
  cxDropDownEdit, cxTextEdit;

type
  TABQueryAllFieldComboxFrame = class(TABPubFrame)
    Panel33: TPanel;
    CheckBox1: TCheckBox;
    Button1: TButton;
    Panel44: TPanel;
    Label2: TLabel;
    ComboBox1: TComboBox;
    Splitter1: TSplitter;
    ComboBox2: TComboBox;
    Panel11: TPanel;
    Panel1: TPanel;
    Edit1: TABcxTextEdit;
    Panel2: TPanel;
    Label1: TLabel;
    Edit2: TABcxTextEdit;
    Edit3: TABcxTextEdit;
    Panel3: TPanel;
    Label3: TLabel;
    ABcxDateTimeEdit1: TABcxDateTimeEdit;
    ABcxDateTimeEdit2: TABcxDateTimeEdit;
    procedure ComboBox1DropDown(Sender: TObject);
    procedure ComboBox1Select(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ComboBox2Select(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure Edit3KeyPress(Sender: TObject; var Key: Char);
    procedure ABcxDateTimeEdit2KeyPress(Sender: TObject; var Key: Char);
    procedure Edit2KeyPress(Sender: TObject; var Key: Char);
    procedure ABcxDateTimeEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure Edit1DblClick(Sender: TObject);
    procedure ComboBox1KeyPress(Sender: TObject; var Key: Char);
    procedure ComboBox2KeyPress(Sender: TObject; var Key: Char);
  private
    FFieldDef:PABFieldDef;
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    { Public declarations }
  end;

  TABQueryAllFieldCombox = class(TABCustomPanel)
  private
    FFrame: TABQueryAllFieldComboxFrame;
    FDataSource: TDataSource;
    function GetFieldComboboxWidth: longint;
    procedure SetFieldComboboxWidth(const Value: longint);
  protected
    procedure Loaded; override;
  public
 	  constructor Create(AOwner: TComponent); override;
 	  destructor Destroy; override;
  Published
    property Frame: TABQueryAllFieldComboxFrame     read FFrame     write FFrame     ;
    property DataSource: TDataSource read FDataSource write FDataSource;
    property FieldComboboxWidth: longint read GetFieldComboboxWidth write SetFieldComboboxWidth;
  end;


implementation
{$R *.dfm}

procedure TABQueryAllFieldComboxFrame.ABcxDateTimeEdit1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if (key= #13)  then
    if ABcxDateTimeEdit2.CanFocus then
      ABcxDateTimeEdit2.SetFocus;
end;

procedure TABQueryAllFieldComboxFrame.ABcxDateTimeEdit2KeyPress(Sender: TObject;
  var Key: Char);
begin
  if (key= #13)  then
  begin
    Button1Click(Button1);
    if ABcxDateTimeEdit2.CanFocus then
      ABcxDateTimeEdit2.SetFocus;
  end;
end;

procedure TABQueryAllFieldComboxFrame.Button1Click(Sender: TObject);
var
  tempQuoted:Boolean;
  tempWhere,
  tempFieldWhere:string;
  tempFieldValue1,
  tempFieldValue2,
  tempFieldValue_QuotedStr:string;
  i,j:longint;
  function GetWhere(aDataset:TDataSet;aMultiLevelQuery:boolean):string;
  var
    tempWhereStr,
    tempMasterFields,
    tempDetailFields,
    tempFromTables,
    tempInitWhere:string;

    tempList:TList;
    I: Integer;
  begin
    Result:=EmptyStr;
    if aMultiLevelQuery then
    begin
      tempList := TList.Create;
      try
        aDataset.GetDetailDataSets(tempList);
        for I := 0 to tempList.Count-1 do
        begin
          tempWhereStr:=GetWhere(TDataSet(tempList[i]),aMultiLevelQuery);
          if tempWhereStr<>EmptyStr then
          begin
            tempMasterFields:=TABThirdQuery(tempList[i]).MasterFields;
            tempDetailFields:=TABThirdQuery(tempList[i]).DetailFields;
            tempFromTables:=TABThirdQuery(tempList[i]).UpdateTables.Text;

            //if Assigned(TABDictionaryQuery(tempList[i]).OnMultiLevelQueryEvent) then
            //   TABDictionaryQuery(tempList[i]).OnMultiLevelQueryEvent(tempMasterFields,tempDetailFields,tempFromTables,tempInitWhere);

            tempWhereStr:=Trim(tempWhereStr);
            if AnsiCompareText(copy(tempWhereStr,1,3),'and')=0 then
              tempWhereStr:=Trim(copy(tempWhereStr,4,Length(tempWhereStr)));

            tempWhereStr:=ABIIF(tempInitWhere=EmptyStr,tempWhereStr,tempInitWhere+' and '+tempWhereStr);

            ABAddstr(Result,
              ' and '+tempMasterFields+
                         ' in ( '+ ABEnterWrapStr+
                         '     select '+tempDetailFields+ ABEnterWrapStr+
                         '     from '+tempFromTables+ABEnterWrapStr+
                         '     where '+tempWhereStr+ABEnterWrapStr+
                         '   )',' ');
          end;
        end;

        if (FFieldDef.Field.DataSet=aDataset) then
          ABAddstr(Result,tempFieldWhere,' ');
      finally
        tempList.Free;
      end;
    end
    else
    begin
      if (FFieldDef.Field.DataSet=aDataset) then
        ABAddstr(Result,tempFieldWhere,' ');
    end;
  end;
begin
  if Button1.CanFocus then
    Button1.SetFocus;

  if (ComboBox1.ItemIndex>=0) and
     (ComboBox2.ItemIndex>=0) and
     (Assigned(FFieldDef)) and
     (Assigned(FFieldDef.PMetaDef))   then
  begin
    if ABStrInArray(FFieldDef.PMetaDef.Fi_Type,
               ['binary','varbinary','ntext','text','datetime','smalldatetime','timestamp',
                'char','varchar','sql_variant','uniqueidentifier','nchar','nvarchar'])>=0 then
    begin
      tempQuoted:=True;
    end
    else if (ABStrInArray(FFieldDef.PMetaDef.Fi_Type,
             ['bigint','int','smallint','tinyint','decimal','numeric','float','real','ABDefaultDecimal',
              'bit','money','smallmoney'])>=0) then
    begin
      tempQuoted:=false;
    end
    else
    begin
      exit;
    end;

    tempFieldWhere:=EmptyStr;
    case ComboBox2.ItemIndex of
      12,13:
      begin
        if (ComboBox2.ItemIndex=12) or
           (ComboBox2.ItemIndex=13) then
        begin
          if (AnsiCompareText(FFieldDef.Fi_ControlType,'TABcxDBDateEdit')=0) or
             (AnsiCompareText(FFieldDef.Fi_ControlType,'TABcxDBDateTimeEdit')=0) then
          begin
            tempFieldValue1:=vartostrdef(ABGetControlValue(ABcxDateTimeEdit1),'');
            if ABVarIsNull(tempFieldValue1) then
              tempFieldValue1:='';
            if (tempFieldValue1<>EmptyStr) and
               (tempFieldValue1<>QuotedStr(EmptyStr)) then
            begin
              if (AnsiCompareText(FFieldDef.Fi_ControlType,'TABcxDBDateEdit')=0) then
              begin
                tempFieldValue1:=ABDateToStr(DateOf(ABcxDateTimeEdit1.Date))+' 00:00:00';
              end
              else if (AnsiCompareText(FFieldDef.Fi_ControlType,'TABcxDBDateTimeEdit')=0) then
              begin
                tempFieldValue1:=ABDateTimeToStr(ABcxDateTimeEdit1.Date);
              end;

              tempFieldValue1:=FFieldDef.Fi_Name+' >= '+ABIIF(tempQuoted,QuotedStr(tempFieldValue1),tempFieldValue1);
            end;

            tempFieldValue2:=vartostrdef(ABGetControlValue(ABcxDateTimeEdit2),'');
            if ABVarIsNull(tempFieldValue2) then
              tempFieldValue2:='';
            if (tempFieldValue2<>EmptyStr) and
               (tempFieldValue2<>QuotedStr(EmptyStr)) then
            begin
              if (AnsiCompareText(FFieldDef.Fi_ControlType,'TABcxDBDateEdit')=0) then
              begin
                tempFieldValue2:=ABDateToStr(DateOf(ABcxDateTimeEdit2.Date))+' 23:59:59';
              end
              else if (AnsiCompareText(FFieldDef.Fi_ControlType,'TABcxDBDateTimeEdit')=0) then
              begin
                tempFieldValue2:=ABDateTimeToStr(ABcxDateTimeEdit2.Date);
              end;
              tempFieldValue2:=FFieldDef.Fi_Name+' <= '+ABIIF(tempQuoted,QuotedStr(tempFieldValue2),tempFieldValue2);
            end;

            tempFieldWhere:=ABAddstr_Ex(tempFieldValue1,tempFieldValue2,' and ');
          end
          else
          begin
            tempFieldValue1:=vartostrdef(ABGetControlValue(Edit2),'');
            if ABVarIsNull(tempFieldValue1) then
              tempFieldValue1:='';
            if (tempFieldValue1<>EmptyStr) and
               (tempFieldValue1<>QuotedStr(EmptyStr)) then
            begin
              tempFieldValue1:= ABUnQuotedFirstlastStr(tempFieldValue1);
              tempFieldValue1:=FFieldDef.Fi_Name+' >= '+ABIIF(tempQuoted,QuotedStr(tempFieldValue1),tempFieldValue1);
            end;

            tempFieldValue2:=vartostrdef(ABGetControlValue(Edit3),'');
            if ABVarIsNull(tempFieldValue2) then
              tempFieldValue2:='';
            if (tempFieldValue2<>EmptyStr) and
               (tempFieldValue2<>QuotedStr(EmptyStr)) then
            begin
              tempFieldValue2:= ABUnQuotedFirstlastStr(tempFieldValue2);
              tempFieldValue2:=FFieldDef.Fi_Name+' <= '+ABIIF(tempQuoted,QuotedStr(tempFieldValue2),tempFieldValue2);
            end;

            tempFieldWhere:=ABAddstr_Ex(tempFieldValue1,tempFieldValue2,' and ');
          end;

          if (ComboBox2.ItemIndex=13) and
             (tempFieldWhere<>EmptyStr) then
          begin
            tempFieldWhere:=' not ('+tempFieldWhere+')';
          end;
        end
      end;
      16:
      begin
        tempFieldWhere:= FFieldDef.Fi_Name+' is null ';
      end;
      17:
      begin
        tempFieldWhere:= FFieldDef.Fi_Name+' is not null ';
      end
      else
      begin
        tempFieldValue1:=vartostrdef(ABGetControlValue(Edit1),'');
        if ABVarIsNull(tempFieldValue1) then
          tempFieldValue1:='';
        if (tempFieldValue1<>EmptyStr) and
           (tempFieldValue1<>QuotedStr(EmptyStr)) then
        begin
          case ComboBox2.ItemIndex of
            //14.在列表中
            //15.不在列表中
            14,15:
            begin
              tempFieldWhere:=EmptyStr;
              tempFieldValue2:=tempFieldValue1;
              j := ABGetSpaceStrCount(tempFieldValue2,',');
              for I := 1 to j do
              begin
                tempFieldValue1 := ABGetSpaceStr(tempFieldValue2, i, ',');
                tempFieldValue1:= ABUnQuotedFirstlastStr(tempFieldValue1);
                ABAddstr(tempFieldWhere,ABIIF(tempQuoted,QuotedStr(tempFieldValue1),tempFieldValue1),',');
              end;

              if tempFieldWhere<>EmptyStr then
                tempFieldWhere:= FFieldDef.Fi_Name+ABIIF(ComboBox2.ItemIndex=14,'',' not ')+' in ('+tempFieldWhere+')';
            end
            else
            begin
              tempFieldValue1:= ABUnQuotedFirstlastStr(tempFieldValue1);
              tempFieldValue_QuotedStr:=ABIIF(tempQuoted,QuotedStr(tempFieldValue1),tempFieldValue1);
              case ComboBox2.ItemIndex of
                //0.等于
                0:
                begin
                  tempFieldWhere:= FFieldDef.Fi_Name+' = '+tempFieldValue_QuotedStr;
                end;
                //1 .不等于
                1:
                begin
                  tempFieldWhere:= FFieldDef.Fi_Name+' <> '+tempFieldValue_QuotedStr;
                end;
                //2 .小于
                2:
                begin
                  tempFieldWhere:= FFieldDef.Fi_Name+' < '+tempFieldValue_QuotedStr;
                end;
                //3 .小于或等于
                3:
                begin
                  tempFieldWhere:= FFieldDef.Fi_Name+' <= '+tempFieldValue_QuotedStr;
                end;
                //4 .大于
                4:
                begin
                  tempFieldWhere:= FFieldDef.Fi_Name+' > '+tempFieldValue_QuotedStr;
                end;
                //5 .大于或等于
                5:
                begin
                  tempFieldWhere:= FFieldDef.Fi_Name+' >= '+tempFieldValue_QuotedStr;
                end;
                //6 .包含
                6:
                begin
                  tempFieldWhere:= FFieldDef.Fi_Name+' like '+QuotedStr('%'+tempFieldValue1+'%');
                end;
                //7 .不包含
                7:
                begin
                  tempFieldWhere:= FFieldDef.Fi_Name+' not like '+QuotedStr('%'+tempFieldValue1+'%');
                end;
                //8 .左包含
                8:
                begin
                  tempFieldWhere:= FFieldDef.Fi_Name+' like '+QuotedStr(tempFieldValue1+'%');
                end;
                //9 .左不包含
                9:
                begin
                  tempFieldWhere:= FFieldDef.Fi_Name+' not like '+QuotedStr(tempFieldValue1+'%');
                end;
                //10.右包含
                10:
                begin
                  tempFieldWhere:= FFieldDef.Fi_Name+' like '+QuotedStr('%'+tempFieldValue1);
                end;
                //11.右不包含
                11:
                begin
                  tempFieldWhere:= FFieldDef.Fi_Name+' not like '+QuotedStr('%'+tempFieldValue1);
                end;
              end;
            end;
          end;
        end;
      end;
    end;

    tempWhere:=EmptyStr;
    if tempFieldWhere<>EmptyStr then
    begin
      tempWhere:=GetWhere(TABQueryAllFieldCombox(Owner).DataSource.DataSet,TABDictionaryQuery(TABQueryAllFieldCombox(Owner).DataSource.DataSet).MultiLevelQuery);
    end;

    if (CheckBox1.Checked) and
       (TABThirdQuery(TABQueryAllFieldCombox(Owner).DataSource.DataSet).AddWhereofNext<>emptystr) and
       (AnsiCompareText(TABThirdQuery(TABQueryAllFieldCombox(Owner).DataSource.DataSet).AddWhereofNext,'1=2')<>0) then
      tempWhere:=TABThirdQuery(TABQueryAllFieldCombox(Owner).DataSource.DataSet).AddWhereofNext+ABIIF(tempWhere<>emptystr, ' and '+ tempWhere,emptystr);

    TABThirdQuery(TABQueryAllFieldCombox(Owner).DataSource.DataSet).RefreshQuery(tempWhere,[]);
  end;
end;

procedure TABQueryAllFieldComboxFrame.ComboBox1DropDown(Sender: TObject);
begin
  if (Assigned(TABQueryAllFieldCombox(Owner).DataSource)) and
     (Assigned(TABQueryAllFieldCombox(Owner).DataSource.DataSet)) and
     (TABQueryAllFieldCombox(Owner).DataSource.DataSet.Active) and
     (ComboBox1.items.count<=0) then
  begin
    ABCreateMultiLevelQueryFieldNameStrings(TABDictionaryQuery(TABQueryAllFieldCombox(Owner).DataSource.DataSet),ComboBox1.items);
    if Panel11.Align<>alClient then
      Panel11.Align:=alClient;
  end;
end;

procedure TABQueryAllFieldComboxFrame.ComboBox1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if (key= #13)  then
    if ComboBox2.CanFocus then
      ComboBox2.SetFocus;
end;

procedure TABQueryAllFieldComboxFrame.ComboBox2KeyPress(Sender: TObject;
  var Key: Char);
begin
  if (key= #13)  then
  begin
    if (Panel1.Visible) and (Edit1.CanFocus) then
      Edit1.SetFocus
    else if (Panel2.Visible) and (Edit2.CanFocus) then
      Edit2.SetFocus
    else if (Panel2.Visible) and (ABcxDateTimeEdit1.CanFocus) then
      ABcxDateTimeEdit1.SetFocus;
  end;
end;

procedure TABQueryAllFieldComboxFrame.ComboBox2Select(Sender: TObject);
var
  tempControl1,
  tempControl2,
  tempControl3:TControl;
begin
  if (ComboBox1.ItemIndex>=0) and
     (ComboBox2.ItemIndex>=0) then
  begin
    if Assigned(FFieldDef) then
    begin
      Panel1.Visible:=false;
      Panel2.Visible:=false;
      Panel3.Visible:=false;
      if (ComboBox2.ItemIndex=12) or
         (ComboBox2.ItemIndex=13) then
      begin
        if (AnsiCompareText(FFieldDef.Fi_ControlType,'TABcxDBDateEdit')=0) or
           (AnsiCompareText(FFieldDef.Fi_ControlType,'TABcxDBDateTimeEdit')=0) then
        begin
          Panel3.Visible:=true;
          Panel3.Align:=alClient;
          tempControl1:=ABcxDateTimeEdit1;
          tempControl2:=Label3;
          tempControl3:=ABcxDateTimeEdit2;
        end
        else
        begin
          Panel2.Visible:=true;
          Panel2.Align:=alClient;

          tempControl1:=Edit2;
          tempControl2:=Label1;
          tempControl3:=Edit3;
        end;
        tempControl1.Left :=0;
        tempControl1.Top :=0;
        tempControl1.Width :=ABTrunc((Panel1.Width-tempControl2.Width-2)/2);
        tempControl1.Height :=Panel1.Height;

        tempControl2.Left :=tempControl1.Left+tempControl1.Width+1;
        tempControl2.Top :=0;
        tempControl2.Height :=Panel1.Height;

        tempControl3.Left :=tempControl2.Left+tempControl2.Width+1;
        tempControl3.Top :=0;
        tempControl3.Width :=Panel1.Width-(tempControl2.Left+tempControl2.Width+1);
        tempControl3.Height :=Panel1.Height;
      end
      else if (ComboBox2.ItemIndex=16) or
              (ComboBox2.ItemIndex=17) then
      begin
      end
      else
      begin
        Panel1.Visible:=true;
        Panel1.Align:=alClient;
      end;
    end;
  end;
end;

procedure TABQueryAllFieldComboxFrame.ComboBox1Select(Sender: TObject);
var
  tempDataset:TDataSet;
  tempFieldName:string;
begin
  FFieldDef:=nil;
  if (ComboBox1.ItemIndex>=0) then
  begin
    if ABPos('------',ComboBox1.Text)>0 then
      exit;

    tempDataset:=TDataSet(ComboBox1.Items.Objects[ComboBox1.ItemIndex]);
    tempFieldName:=ABGetFieldNamesByDisplayLabels(tempDataset,ABGetLeftRightStr(ComboBox1.Text,axdRight,':',''));
    FFieldDef:=ABFieldDefByFieldName(tempDataset,tempFieldName);
    if Assigned(FFieldDef) then
    begin
      if (AnsiCompareText(FFieldDef.Fi_ControlType,'TABcxDBDateEdit')=0) or
         (AnsiCompareText(FFieldDef.Fi_ControlType,'TABcxDBDateTimeEdit')=0) then
      begin
        //ComboBox2.ItemIndex:=12;
      end
      else
      begin
        //ComboBox2.ItemIndex:=0;
      end;
      ComboBox2Select(ComboBox2);
    end;
  end;
end;

constructor TABQueryAllFieldComboxFrame.Create(AOwner: TComponent);
begin
  inherited;
end;

destructor TABQueryAllFieldComboxFrame.Destroy;
begin
  inherited;
end;

procedure TABQueryAllFieldComboxFrame.Edit1DblClick(Sender: TObject);
var
  tempWhere:string;
begin
  if (ComboBox1.ItemIndex>=0) and
     ((ComboBox2.ItemIndex=14) or (ComboBox2.ItemIndex=15)) then
  begin
    if Assigned(FFieldDef) then
    begin
      tempWhere:=ABGetFieldMultiRowWhere(FFieldDef.Fi_Caption,',',true,'','');
      if tempWhere<>EmptyStr then
        ABSetControlValue(Edit1,tempWhere);
    end;
  end;
end;

procedure TABQueryAllFieldComboxFrame.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  if (key= #13)  then
  begin
    Button1Click(Button1);
    if Edit1.CanFocus then
      Edit1.SetFocus;
  end;
end;

procedure TABQueryAllFieldComboxFrame.Edit2KeyPress(Sender: TObject; var Key: Char);
begin
  if (key= #13)  then
    if Edit3.CanFocus then
      Edit3.SetFocus;
end;

procedure TABQueryAllFieldComboxFrame.Edit3KeyPress(Sender: TObject; var Key: Char);
begin
  if (key= #13)  then
  begin
    Button1Click(Button1);
    if Edit3.CanFocus then
      Edit3.SetFocus;
  end;
end;

{ TABQueryAllFieldCombox }

constructor TABQueryAllFieldCombox.Create(AOwner: TComponent);
begin
  inherited;
  Height:=27;
  Width:=500;
  Constraints.MinHeight:=27;
  Constraints.MaxHeight:=27;

  FFrame:= TABQueryAllFieldComboxFrame.Create(self);
  FFrame.Align:=alClient;
  FFrame.Parent:=Self;
  FFrame.Show;
end;

destructor TABQueryAllFieldCombox.Destroy;
begin
  FFrame.Free;
  inherited;
end;

function TABQueryAllFieldCombox.GetFieldComboboxWidth: longint;
begin
  result:=FFrame.Panel44.Width-30;
end;

procedure TABQueryAllFieldCombox.Loaded;
begin
  inherited;
end;

procedure TABQueryAllFieldCombox.SetFieldComboboxWidth(const Value: longint);
begin
  FFrame.Panel44.Width:=Value+30;
end;

end.




