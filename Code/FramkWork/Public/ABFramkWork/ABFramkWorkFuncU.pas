{
框架函数定义单元
}
unit ABFramkWorkFuncU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubUserU,
  ABPubMessageU,
  ABPubVarU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubFormU,

  ABThirdQueryU,
  ABThirdCacheDatasetU,
  ABThirdConnU,
  ABThirdConnServerU,
  ABThirdDBU,
  ABThirdCustomQueryU,

  ABFramkWorkConstU,
  ABFramkWorkVarU,

  FireDAC.Stan.StorageBin,
  FireDAC.Stan.StorageXML, FireDAC.Stan.StorageJSON,
  FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Comp.UI,
  FireDAC.Stan.Def,
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  FireDAC.Phys.MSSQLDef, FireDAC.Phys, FireDAC.Phys.ODBCBase, FireDAC.Phys.MSSQL,

  DB,SysUtils,Variants,Classes,Forms;


//取得数据感知数据关联控件名称列表数据集
function ABGetDBControlNameDataset:TDataSet;

//取数据集字段的框架信息
function ABFieldDefByFieldName(aDataset:TDataSet;const FieldName: String): PABFieldDef;

//取得数据集查询的定义
function ABGetDatasetQueryConst(aDataSet:TDataSet;aHaveField:string='';aNoHaveField:string=''):string;

//取得SQL共用数据集(不存在时会自动创建)
//aTableName=数据集的表名
//aConstFieldWheres=字段的扩展条件
//aDatasetClass=使用的数据类型
function ABGetConstSqlPubDataset(aTableName: string;aConstFieldWheres:array of string;aDatasetClass:TDataSetClass=nil): TDataSet;

//根据功能的文件名得到功能表中字段值
function ABGetFuncFieldValueByFuncFileName(aFuncFileName:string;aResturnFieldName:string):string;
//取得Owner窗体所在的BPL文件名
function ABGetBPLFileName(aOwner:TComponent): String;

//将下拉项编号下的明细填充到字串列表中
procedure ABFillTreeItemByTr_Code(aCode:string;
                                  aFillFieldNames:array of string;
                                  aStrings:TStrings;
                                  aNoFillSame: boolean = true;
                                  aIsDelOld: boolean = true;
                                  aSpec: string = ','
                                  );
//根据连接名和表名得到排除指定字段外的字段名
function ABGetTableFieldNames(aTableName:String;aNoHaveFieldNames:array of string;
                              aCheckIsGridView:Boolean=False;aCheckIsInputPanelView:Boolean=False;
                              aConnName:String='Main';aSpaceSign:String=',';aBeginSign:String='';aEndSign:String=''): String;overload;
//根据连接名和表名得到排除的字段类型外的字段名
function ABGetTableNoTypeFieldNames(aTableName:String;
                                    aNoHaveFieldTypes:string='image;ntext;text';aAddNoTypeFieldNameNull:Boolean=False;
                                    aCheckIsGridView:Boolean=False;aCheckIsInputPanelView:Boolean=False;
                                    aConnName:String='Main'; aSpaceSign:String=',';aBeginSign:String='';aEndSign:String=''): String;overload;
//根据连接名和表名得到排除指定字段外的取数据SQL
function ABGetTableFieldNamesSQL(aTableName:String;
                                 aNoHaveFieldNames:array of string;
                                 aCheckIsGridView:Boolean=False;aCheckIsInputPanelView:Boolean=False;
                                 aConnName:String='Main';aSpaceSign:String=',';aBeginSign:String='';aEndSign:String=''): String;
//取数据表的主键字段，字段前面加aBeginSign，字段后面加aEndSign，多个字段时以aSpaceSign连结
function ABGetTablePrimaryKeyFieldNames(aTableName:String;aConnName:String='Main';aSpaceSign:String='';aBeginSign:String='';aEndSign:String=''): String;
//执行报表前执行SQL
procedure ABExecBeforeReportSQL;
//通过连接GUID取得连接名称
function ABGetConnNameByConnGuid(aConnGuid:String): String;
//通过连接名称取得连接GUID
function ABGetConnGuidByConnName(aConnName:String): String;
//根据传入的主从数据集，得到从集数据库的SQL前置标识，如:数据库.dbo.
function ABGetDetailTableDatabaseSQLFlag(aMainDataset,aDetailDataset:TABThirdQuery):string;


var
  ABFuncPointStrings:TStrings;
  ABApproveBillStrings:TStrings;
  ABBillInputStrings:TStrings;
implementation
var
  FDBControlNameDataset:TDataSet;

function ABGetDetailTableDatabaseSQLFlag(aMainDataset,aDetailDataset:TABThirdQuery):string;
var
  tempMainConnName,tempDetailConnName:string;
begin
  result:=EmptyStr;
  tempMainConnName:= aMainDataset.ConnName;
  if tempMainConnName=EmptyStr then
    tempMainConnName:='Main';
  tempDetailConnName:= aDetailDataset.ConnName;
  if tempDetailConnName=EmptyStr then
    tempDetailConnName:='Main';

  if AnsiCompareText(tempMainConnName,tempDetailConnName)<>0 then
  begin
    result:=ABGetDatabaseByConnName(tempDetailConnName)+'.dbo.';
  end;
end;

function ABGetConnNameByConnGuid(aConnGuid:String): String;
var
  tempDataset:TDataSet;
begin
  result:=EmptyStr;
  tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_ConnList',[]);
  if tempDataset.Locate('CL_Guid',aConnGuid,[]) then
  begin
    result:=tempDataset.FieldByName('Cl_Name').AsString;
  end;
end;

function ABGetConnGuidByConnName(aConnName:String): String;
var
  tempDataset:TDataSet;
begin
  result:=EmptyStr;
  tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_ConnList',[]);
  if tempDataset.Locate('Cl_Name',aConnName,[]) then
  begin
    result:=tempDataset.FieldByName('CL_Guid').AsString;
  end;
end;

procedure ABExecBeforeReportSQL;
var
  tempDataset:TDataset;
  tempSQL:string;
begin
  tempDataset       :=ABGetConstSqlPubDataset('ABSys_Org_FuncSQL',['BeforeReport']);
  tempDataset.First;
  while not tempDataset.Eof do
  begin
    tempSQL:=tempDataset.FieldByName(ABPubReplaceSQLValueFieldName).AsString;
    if tempSQL<>EmptyStr then
    begin
      ABExecSQLs(ABGetConnNameByConnGuid(tempDataset.FieldByName('FS_ConnName').AsString),tempSQL,[]);
    end;

    tempDataset.next;
  end;
end;

function ABGetTablePrimaryKeyFieldNames(aTableName:String;aConnName:String;aSpaceSign:String;aBeginSign:String;aEndSign:String): String;
var
  tempDataSet: TDataSet;
begin
  result:=EmptyStr;
  tempDataSet:=ABGetConstSqlPubDataset('ABSys_Temp_GetTableIndexInfo',[ABGetDatabaseByConnName(aConnName),aTableName]);
  if (Assigned(tempDataset))  then
  begin
    tempDataSet.First;
    while not tempDataSet.EOF do
    begin
      if tempDataSet.FindField('PRIMARY_KEY').AsBoolean then
      begin
        ABAddstr(result,aBeginSign+tempDataSet.FindField('COLUMN_NAME').AsString+aEndSign,aSpaceSign);
      end;
      tempDataSet.Next;
    end;
  end;
end;

function ABGetTableFieldNames(aTableName:String;aNoHaveFieldNames:array of string;aCheckIsGridView:Boolean;aCheckIsInputPanelView:Boolean;aConnName:String;aSpaceSign:String;aBeginSign:String;aEndSign:String): String;
var
  j:LongInt;
  tempFieldDataset:TDataSet;
begin
  result:=EmptyStr;
  j:= high(aNoHaveFieldNames);

  tempFieldDataset:=ABGetConstSqlPubDataset('ABSys_Org_Field',[ABGetDatabaseByConnName(aConnName),aTableName]);
  tempFieldDataset.First;
  while not tempFieldDataset.Eof do
  begin
    if ((j<0) or (ABStrInArray(tempFieldDataset.FieldByName('Fi_Name').AsString,aNoHaveFieldNames)<0)) and
       ((not aCheckIsGridView) or (tempFieldDataset.FieldByName('FI_IsGridView').AsBoolean)) and
       ((not aCheckIsInputPanelView) or (tempFieldDataset.FieldByName('FI_IsInputPanelView').AsBoolean)) then
    begin
      result:=ABAddstr_Ex(result,aBeginSign+tempFieldDataset.FieldByName('Fi_Name').AsString+aEndSign,aSpaceSign);
    end;
    tempFieldDataset.Next
  end;
end;

function ABGetTableNoTypeFieldNames(aTableName:String;aNoHaveFieldTypes:string;aAddNoTypeFieldNameNull:Boolean; aCheckIsGridView:Boolean;aCheckIsInputPanelView:Boolean;aConnName:String;aSpaceSign:String;aBeginSign:String;aEndSign:String): String;
var
  tempFieldDataset:TDataSet;
  tempFieldDataset1:TDataSet;
begin
  result:=EmptyStr;
  if aNoHaveFieldTypes=EmptyStr then
    aNoHaveFieldTypes:='image;ntext;text';
  aNoHaveFieldTypes:=ABStringReplace(aNoHaveFieldTypes,';',',');

  tempFieldDataset1:=ABGetConstSqlPubDataset('ABSys_Org_Field',[ABGetDatabaseByConnName(aConnName),aTableName]);
  tempFieldDataset:=ABGetConstSqlPubDataset('ABSys_Temp_GetFieldInfo',[ABGetDatabaseByConnName(aConnName),aTableName]);
  tempFieldDataset.First;
  while not tempFieldDataset.Eof do
  begin
    if tempFieldDataset1.Locate('Fi_Name',tempFieldDataset.FieldByName('Name').AsString,[]) then
    begin
      if (ABPos(','+tempFieldDataset.FieldByName('Type').AsString+',',','+aNoHaveFieldTypes+',')<=0) and
         ((not aCheckIsGridView) or (tempFieldDataset1.FieldByName('FI_IsGridView').AsBoolean)) and
         ((not aCheckIsInputPanelView) or (tempFieldDataset1.FieldByName('FI_IsInputPanelView').AsBoolean)) then
      begin
        result:=ABAddstr_Ex(result,aBeginSign + tempFieldDataset.FieldByName('Name').AsString+aEndSign,aSpaceSign);
      end
      else
      begin
        if aAddNoTypeFieldNameNull then
        begin
          result:=ABAddstr_Ex(result,' case when  '+tempFieldDataset.FieldByName('Name').AsString+' is null then ''无内容'' else ''有内容''  end '+tempFieldDataset.FieldByName('Name').AsString,aSpaceSign);
        end;
      end;
    end;
    
    tempFieldDataset.Next
  end;
end;

function ABGetTableFieldNamesSQL(aTableName:String;aNoHaveFieldNames:array of string;aCheckIsGridView:Boolean;aCheckIsInputPanelView:Boolean;aConnName:String;aSpaceSign:String;aBeginSign:String;aEndSign:String): String;
begin
  result:='select '+ABGetTableFieldNames(aTableName,aNoHaveFieldNames,aCheckIsGridView,aCheckIsInputPanelView,aConnName,aSpaceSign,aBeginSign,aEndSign)+' from '+aTableName;
end;

function ABGetBPLFileName(aOwner:TComponent): String;
begin
  result:=EmptyStr;
  if (Assigned(aOwner)) and
     (aOwner.ClassName<>EmptyStr) and
     (aOwner is TABPubForm) then
  begin
    result:=TABPubForm(aOwner).FileName;
  end;
end;

procedure ABFillTreeItemByTr_Code(aCode:string;aFillFieldNames:array of string;
                                  aStrings:TStrings;
                                  aNoFillSame: boolean ;
                                  aIsDelOld: boolean;
                                  aSpec: string
                                  );
var
  tempDataset:TDataSet;
begin
  tempDataset:= ABGetConstSqlPubDataset('ABSys_Org_TreeItem',[aCode]);
  ABDatasetToStrings(tempDataset,
                         aFillFieldNames,
                         aStrings,
                         aSpec,
                         aNoFillSame,
                         aIsDelOld);
end;

function ABGetFuncFieldValueByFuncFileName(aFuncFileName:string;aResturnFieldName:string):string;
var
  tempDataset:TDataSet;
begin
  Result:=EmptyStr;
  if (aFuncFileName<>EmptyStr)then
  begin
    tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_Function',[aFuncFileName]);
    if Assigned(tempDataset)then
    begin
      Result:=tempDataset.FieldByName(aResturnFieldName).AsString;
    end;
  end;
end;

procedure ABCustomAfterConnProc;
  procedure SetPublicParamsVariant;
    procedure SetClientTypeUse;
    var
      i:LongInt;
      tempClientType:string;
    begin
      if ABPubParamDataset.Locate('Pa_Name','ClientType',[])   then
      begin
        tempClientType:=ABPubParamDataset.FieldByName('Pa_Value').AsString;
        //第一位表是是否全部启用
        //第2位消费部分
        //门禁部分
        //考勤部分
        //访客部分
        //巡更部分
        //ERP
        //Bug管理
        //功能自动测试
        //会议签到
        //是否使用权限Service

        if tempClientType<>'' then
        begin
          for I := Low(ABClientTypeUse) to High(ABClientTypeUse) do
          begin
            if i>100 then
              Break;

            ABClientTypeUse[i]:=(tempClientType[i]='1');
          end;
        end;
      end;
    end;
  begin
    SetClientTypeUse;
  end;
var
  i:LongInt;
  tempDataset:TDataset;
  tempStrings:TStrings;
  tempStr:string;
begin
  //此数据集中的名称可出现在SQL或参数中，执行前要自动替换参数值
  ABPubParamDataset       :=ABGetConstSqlPubDataset('ABSys_Org_Parameter',['']);
  ABPubParamNameFieldName :='PA_Name';
  ABPubParamValueFieldName:='PA_Value';

  //此数据集中的名称可出现在SQL中，执行前要自动替换SQL内容
  ABPubReplaceSQLDataset       :=ABGetConstSqlPubDataset('ABSys_Org_FuncSQL',['ABPublicSQL']);
  ABPubReplaceSQLNameFieldName :='FS_Name';
  ABPubReplaceSQLValueFieldName:='FS_Sql';

  tempStrings:= ABThirdConnU.ABGetConnStrings;
  if (tempStrings.Count>0)  then
  begin
    for I := 0 to tempStrings.Count-1 do
    begin
      tempStr:='['+tempStrings.Names[i]+']='+tempStrings.ValueFromIndex[i];
      if ABPubReplaceSQLStrings.indexof(tempStr)<0 then
        ABPubReplaceSQLStrings.Add(tempStr);
    end;
  end;

  tempDataset:= ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['CustomObject']);
  if Assigned(tempDataset) then
  begin
    ABFuncPointStrings.Clear;
    tempDataset.first;
    while not tempDataset.Eof  do
    begin
      if (AnsiCompareText(tempDataset.FieldByName('Ti_Varchar1').AsString,'FuncPoint')=0) and
         (tempDataset.FieldByName('Ti_Bit1').AsBoolean) then
      begin
        ABFuncPointStrings.Add(tempDataset.FieldByName('Ti_Code').AsString+'='+tempDataset.FieldByName('Ti_Name').AsString);
      end;
      tempDataset.next;
    end;
  end;

  tempDataset:=ABGetDataset('Main','select Bi_Code from ABSys_Approved_Bill',[]);
  try
    if Assigned(tempDataset) then
    begin
      ABApproveBillStrings.Clear;
      tempDataset.first;
      while not tempDataset.Eof  do
      begin
        ABApproveBillStrings.Add(tempDataset.FieldByName('Bi_Code').AsString);
        tempDataset.next;
      end;
    end;
  finally
    tempDataset.Free;
  end;

  tempDataset:=ABGetDataset('Main','select Bi_Code from ABSys_Org_BillInput',[]);
  try
    if Assigned(tempDataset) then
    begin
      ABBillInputStrings.Clear;
      tempDataset.first;
      while not tempDataset.Eof  do
      begin
        ABBillInputStrings.Add(tempDataset.FieldByName('Bi_Code').AsString);
        tempDataset.next;
      end;
    end;
  finally
    tempDataset.Free;
  end;

  SetPublicParamsVariant;
end;

function ABGetConstSqlPubDataset(aTableName: string;aConstFieldWheres:array of string;aDatasetClass:TDataSetClass): TDataSet;
  procedure SetReadOnly(aDataset:TDataSet);
  var
    tempDataset:TFireDACQuery;
    tempReadOnly:Boolean;
  begin
    if aDataset is TFireDACQuery then
    begin
      tempReadOnly:=true;
      tempDataset:=TFireDACQuery(aDataset);
      if AnsiCompareText(aTableName,'ABSys_Org_TreeItem')=0 then
      begin
        tempReadOnly:=false;
      end
      else if AnsiCompareText(aTableName,'ABSys_Org_FuncReport')=0 then
      begin
        tempReadOnly:=false;
      end
      else if AnsiCompareText(aTableName,'ABSys_Org_FuncTemplate')=0 then
      begin
        tempReadOnly:=false;
      end
      else if AnsiCompareText(aTableName,'ABSys_Org_FuncSQL')=0 then
      begin
        tempReadOnly:=false;
      end
      else if AnsiCompareText(aTableName,'ABSys_Org_FuncControlProperty')=0 then
      begin
        tempReadOnly:=false;
      end;
      if tempDataset.UpdateOptions.ReadOnly<>tempReadOnly then
        tempDataset.UpdateOptions.ReadOnly:=tempReadOnly;
    end;
  end;

  procedure SetFieldDefaultValues(aDataset:TDataSet);
  var
    tempFieldDefaultValues:string;
  begin
    if aDataset is TABThirdReadDataQuery then
    begin
      tempFieldDefaultValues:=emptystr;
      if AnsiCompareText(aTableName,'ABSys_Org_FuncReport')=0 then
      begin
        tempFieldDefaultValues:='FR_Guid=[ABUser_VarGuid];PubID=[ABUser_VarGuid]';
      end
      else if AnsiCompareText(aTableName,'ABSys_Org_FuncTemplate')=0 then
      begin
        tempFieldDefaultValues:='FT_Guid=[ABUser_VarGuid];PubID=[ABUser_VarGuid]';
      end
      else if AnsiCompareText(aTableName,'ABSys_Org_FuncSQL')=0 then
      begin
        tempFieldDefaultValues:='FS_Guid=[ABUser_VarGuid];PubID=[ABUser_VarGuid]';
      end
      else if AnsiCompareText(aTableName,'ABSys_Org_FuncControlProperty')=0 then
      begin
        tempFieldDefaultValues:='FP_Guid=[ABUser_VarGuid];FP_AutoLoad=0;PubID=[ABUser_VarGuid]';
      end;
      if tempFieldDefaultValues<>emptystr then
        TABThirdReadDataQuery(aDataset).FieldDefaultValues:= tempFieldDefaultValues;
    end;
  end;
  //对于一些共用数据集名称的SQL是固定的
  function GetConstSql:string;
  var
    tempDataset:TDataSet;
  begin
    if AnsiCompareText(aTableName,'ABSys_Org_Key')=0 then
    begin
      Result:=  ' SELECT * '+
                ' FROM ABSys_Org_Key ';
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_ConnList')=0 then
    begin
      Result:=  ' SELECT * '+
                ' FROM ABSys_Org_ConnList ';
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_Table')=0 then
    begin
      tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_ConnList',[]);
      if tempDataset.Locate('CL_DatabaseName',aConstFieldWheres[0],[]) then
      begin
        Result:=  ' SELECT * '+
                  ' FROM ABSys_Org_Table '+
                  ' where TA_CL_Guid='+QuotedStr(tempDataset.FieldByName('CL_Guid').AsString)+' and '+
                  '       Ta_Name='+QuotedStr(aConstFieldWheres[1]);
      end;
    end
    else if AnsiCompareText(aTableName,'ABSys_Temp_GetTableIndexInfo')=0 then
    begin
      Result:=  ' SELECT INDEX_NAME,COLUMN_NAME,PRIMARY_KEY,[UNIQUE],COLLATION,[clustered],TableName '+
                ' FROM ABSys_Temp_GetTableIndexInfo '+
                ' where DatabaseName='+QuotedStr(aConstFieldWheres[0])+' and '+
                '       TableName='+QuotedStr(aConstFieldWheres[1]);
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_Field')=0 then
    begin
      tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_Table',[aConstFieldWheres[0],aConstFieldWheres[1]]);
      Result:=  ' SELECT * '+
                ' FROM ABSys_Org_Field '+
                ' where Fi_Ta_Guid='+QuotedStr(tempDataset.FieldByName('Ta_Guid').AsString);
    end
    else if AnsiCompareText(aTableName,'ABSys_Temp_GetFieldInfo')=0 then
    begin
      Result:=  ' SELECT [Name],[ID],IsAutoAdd,IsKey,Type,Byte,Length,EecimalDigits,CanNull,DefaultValue '+
                ' FROM ABSys_Temp_GetFieldInfo '+
                ' where DatabaseName='+QuotedStr(aConstFieldWheres[0])+' and '+
                '       TableName='+QuotedStr(aConstFieldWheres[1]);
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_FieldCaption')=0 then
    begin
      tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_Table',[aConstFieldWheres[0],aConstFieldWheres[1]]);
      Result:=    ' SELECT Fi_Name,Fi_Caption '+
                  ' FROM ABSys_Org_Field '+
                  ' where Fi_Ta_Guid='+QuotedStr(tempDataset.FieldByName('Ta_Guid').AsString);
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_FieldDownGroup')=0 then
    begin                                                              
      tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_Table',[aConstFieldWheres[0],'ABSys_Org_DownGroup']);
      Result:=    ' select Fi_Ta_Guid,Fi_Name,Fi_Caption,Fi_DownSQL,Fi_Filter,Fi_DownFields,'+
                  '        Fi_DownCaptions,Fi_ViewField,Fi_SaveField,Fi_DownGroup,'+
                  '        Fi_ParentField,Fi_RelatingFromFields,Fi_RelatingToFields,Fi_Tr_Code,'+
                  '        Fi_IsAutoRemember,Fi_CheckInDown,Fi_CanSelectParent,FI_ConnName,FI_DownRefresh,FI_CacleDataset,Fi_ConnName '+
                  ' from ABSys_Org_Field '+                                
                  ' where Fi_Ta_Guid='+QuotedStr(tempDataset.FieldByName('Ta_Guid').AsString)+' and '+
                  '      (Fi_DownSQL<>'''' or Fi_Tr_Code<>'''' ) ';
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_Tree')=0 then
    begin
      Result:=  ' SELECT * '+
                ' FROM ABSys_Org_Tree '+
                ' where Tr_Code='+QuotedStr(aConstFieldWheres[0]);
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_TreeItem')=0 then
    begin
      tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_Tree',[aConstFieldWheres[0]]);
      Result:=  ' SELECT * '+
                ' FROM ABSys_Org_TreeItem '+
                ' where Ti_Tr_Guid='+QuotedStr(tempDataset.FieldByName('Tr_Guid').AsString)+
                ' order by Ti_Tr_Guid,TI_ParentGuid,Ti_Order';
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_FuncReport')=0 then
    begin
      Result:=  ' SELECT FR_FU_Guid,FR_Guid,FR_GroupName,FR_Name,FR_OutType,FR_DefaultPrint,FR_PrintRange,FR_IsDefault,FR_SysUse,FR_Order,PubID '+
                ' FROM ABSys_Org_FuncReport '+
                ' where Fr_Fu_Guid='+QuotedStr(aConstFieldWheres[0])+' and '+
                '       Fr_GroupName='+QuotedStr(aConstFieldWheres[1]);
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_FuncTemplate')=0 then
    begin
      Result:=  ' SELECT FT_FU_Guid,FT_Guid,FT_GroupName,FT_Name,FT_IsDefault,FT_SysUse,FT_Order,PubID '+
                ' FROM ABSys_Org_FuncTemplate '+
                ' where Ft_Fu_Guid='+QuotedStr(aConstFieldWheres[0])+' and '+
                '       Ft_GroupName='+QuotedStr(aConstFieldWheres[1]);
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_FuncSQL')=0 then
    begin
      {
      if (aConstFieldWheres[0]=EmptyStr) then
        aConstFieldWheres[0]:= ABGetFuncFieldValueByFuncFileName('ABClientP.exe','Fu_Guid');
      Result:=  ' SELECT * '+
                ' FROM ABSys_Org_FuncSQL '+
                ' where  FS_FU_Guid='+QuotedStr(aConstFieldWheres[0])+' and '+
                '        FS_FormName='+QuotedStr(aConstFieldWheres[1]);
          }
      //因为在设计期，无法取出数据集所属的BPL文件GUID，所以这里只能用窗体名称进行过滤
      Result:=  ' SELECT * '+
                ' FROM ABSys_Org_FuncSQL '+
                ' where  FS_FormName='+QuotedStr(aConstFieldWheres[0]);
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_FuncControlProperty')=0 then
    begin
      if (aConstFieldWheres[0]=EmptyStr) then
        aConstFieldWheres[0]:= ABGetFuncFieldValueByFuncFileName('ABClientP.exe','Fu_Guid');

      Result:=  ' SELECT * '+
                ' FROM ABSys_Org_FuncControlProperty '+
                ' where FP_FU_Guid='+QuotedStr(aConstFieldWheres[0])+' and '+
                '        FP_FormName='+QuotedStr(aConstFieldWheres[1]);
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_ExtendReport')=0 then
    begin
      Result:=  ' select  ER_TI_Guid,ER_Guid,ER_Name,ER_OutType,ER_DefaultPrint,ER_PrintRange,ER_SysUse,ER_ShowType,ER_Order,PubID          '+
                ' from ABSys_Org_ExtendReport '+
                ' where ER_TI_Guid='+QuotedStr(aConstFieldWheres[0]);
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_ExtendReportDatasets')=0 then
    begin
      Result:=  ' select ED_ER_Guid,ED_Guid,ED_ParentGuid,ED_ConnName,ED_Name,ED_MainFields,ED_DetailFields,ED_Order,PubID            '+
                ' from ABSys_Org_ExtendReportDatasets '+
                ' where ED_ER_Guid='+QuotedStr(aConstFieldWheres[0]);
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_Parameter')=0 then
    begin
      Result:= ' SELECT * '+
               ' FROM ABSys_Org_Parameter ';
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_Function')=0 then
    begin
      Result:= ' SELECT Fu_Guid,Fu_Code,Fu_Name,Fu_FileName,Fu_SubPath,Fu_RunAVI,Fu_Version,FU_IsUpdate,FU_SysUse,FU_TemplateName,cast(case when Fu_Pkg is null then 1 else 0 end as bit) Fu_PkgIsNull '+
               ' FROM ABSys_Org_Function '+
               ' where FU_FileName='+QuotedStr(aConstFieldWheres[0]);
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_FuncMainTemplateSetup')=0 then
    begin
      Result:= ' SELECT * '+
               ' FROM ABSys_Org_FuncMainTemplateSetup '+
               ' where FM_FU_Guid='+QuotedStr(aConstFieldWheres[0]);
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_FuncMainTemplatePanelSetup')=0 then
    begin
      Result:= ' SELECT * '+
               ' FROM ABSys_Org_FuncMainTemplatePanelSetup '+
               ' where FP_FM_Guid='+QuotedStr(aConstFieldWheres[0]);
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_FuncDetailTemplateSetup')=0 then
    begin
      Result:= ' SELECT * '+
               ' FROM ABSys_Org_FuncDetailTemplateSetup '+
               ' where FD_FU_Guid='+QuotedStr(aConstFieldWheres[0]);
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_FuncDetailTemplatePanelSetup')=0 then
    begin
      Result:= ' SELECT * '+
               ' FROM ABSys_Org_FuncDetailTemplatePanelSetup '+
               ' where FD_FM_Guid='+QuotedStr(aConstFieldWheres[0]);
    end
    else if AnsiCompareText(aTableName,'ABSys_Approved_Bill')=0 then
    begin
      Result:= ' SELECT *, '+
               '  (SELECT CL_Name FROM ABSys_Org_Table,ABSys_Org_ConnList WHERE Ta_CL_Guid=CL_Guid and Ta_Guid = Bi_MainTable ) Bi_MainConnName,     '+
               '  (SELECT Ta_Name FROM ABSys_Org_Table WHERE Ta_Guid = Bi_MainTable ) Bi_MainTableName,                                              '+
               '  (SELECT CL_Name FROM ABSys_Org_Table,ABSys_Org_ConnList WHERE Ta_CL_Guid=CL_Guid and Ta_Guid = Bi_MainTable ) Bi_DetailConnName,   '+
               '  (SELECT Ta_Name FROM ABSys_Org_Table WHERE Ta_Guid = Bi_DetailTable ) Bi_DetailTableName                                           '+
               ' FROM ABSys_Approved_Bill '+
               ' where Bi_Code='+QuotedStr(aConstFieldWheres[0]);
    end
    else if AnsiCompareText(aTableName,'ABSys_Org_BillInput')=0 then
    begin
      Result:= ' SELECT *          '+
               ' FROM ABSys_Org_BillInput '+
               ' where Bi_Code='+QuotedStr(aConstFieldWheres[0]);
    end;
  end;
var
  tempSql:string;
  tempAllName:string;
begin
  tempAllName := Trim(aTableName)+ABStringArrayToStr(aConstFieldWheres,False,'-');
  Result := ABGetPubDataset(tempAllName);
  if not (Assigned(Result)) then
  begin
    tempSql:=GetConstSql;
    if (tempSql <> EmptyStr) then
    begin
      if not Assigned(aDataSetClass) then
      begin
        Result:=TABThirdReadDataQuery.Create(nil);
      end
      else
      begin
        Result := aDatasetClass.Create(nil);
      end;

      SetFieldDefaultValues(Result);
      SetReadOnly(Result);
      TFireDACQuery(Result).Sql.Text:=tempSql;
      Result.Open;
    end
    else
    begin
      ABShow('获取定制共用数据集[%s]的SQL失败.',[tempAllName]);
    end;

    ABPubDatasetList.AddObject(tempAllName, result);
  end;
end;
                          
function ABFieldDefByFieldName(aDataset:TDataSet;const FieldName: String): PABFieldDef;
var
  tempI :IABQuery ;
begin
  result:=nil;
  if aDataset.getinterface(IABQuery,tempI) then
  begin
    result:=tempI.FieldDefByFieldName(FieldName);
  end;
end;

function ABGetDBControlNameDataset:TDataSet;
begin
  if not Assigned(FDBControlNameDataset) then
    FDBControlNameDataset:= ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['DBControl']);

  Result:=FDBControlNameDataset;
end;

function ABGetDatasetQueryConst(aDataSet:TDataSet;aHaveField:string='';aNoHaveField:string=''):string;
var
  tempFieldDef:PABFieldDef;
  i:longint;

  tempStr1,
  tempClassName,
  tempCaption,
  tempName,
  tempDefaultValue,

  tempConnName,
  tempCacleDataset,
  tempFillSql,
  tempParentField,
  tempKeyfield,
  tempListField,
  tempDownField:string;

  function DBControlTypeToNoDBControlType(aClassName:string):string;
  begin
    Result:=EmptyStr;
    if ABGetDBControlNameDataset.Locate('Ti_Name',aClassName,[]) then
      Result:=ABGetDBControlNameDataset.FieldByName('Ti_Varchar1').AsString;
  end;
begin
  result:=EmptyStr;
  for i := 0 to aDataSet.FieldCount-1 do
  begin
    if ((aNoHaveField=EmptyStr)  or (ABPos(','+aDataSet.Fields[i].FieldName+',',','+aNoHaveField  +',')<=0)) and
       ((aHaveField=EmptyStr)    or (ABPos(','+aDataSet.Fields[i].FieldName+',',','+aHaveField+',')> 0)) then
    begin
      tempFieldDef:=ABFieldDefByFieldName(aDataSet,aDataSet.Fields[i].FieldName);
      if Assigned(tempFieldDef) then
      begin
        if (tempFieldDef.Fi_IsQueryView) then
        begin
          tempClassName   :=DBControlTypeToNoDBControlType(tempFieldDef.Fi_ControlType);
          tempCaption     :=aDataSet.Fields[i].DisplayLabel;
          tempName        :=tempFieldDef.Fi_Name;
          tempDefaultValue:='';

          if tempFieldDef.IsDown then
          begin
            tempFillSql     :=tempFieldDef.PDownDef.Fi_DownSQL;
            tempParentField :=tempFieldDef.PDownDef.Fi_ParentField;
            tempKeyfield    :=tempFieldDef.PDownDef.Fi_SaveField;
            tempListField   :=tempFieldDef.PDownDef.Fi_ViewField;
            tempDownField   :=tempFieldDef.PDownDef.Fi_DownFields;
            tempCacleDataset:=IntToStr(ABBoolToInt(tempFieldDef.PDownDef.FI_CacleDataset));
            tempConnName    :=tempFieldDef.PDownDef.Fi_ConnName;
          end
          else
          begin
            tempFillSql     :='';
            tempParentField :='';
            tempKeyfield    :='';
            tempListField   :='';
            tempDownField   :='';
            tempCacleDataset:='';
            tempConnName    :='';
          end;

          if (tempFieldDef.Fi_BegEndQuery) then
          begin
            tempStr1:=ABSubStrBegin+
                        '[ConnName]'+tempConnName+
                        '[CacleDataset]'+tempCacleDataset+
                        '[Name]'+tempName+'_BegEndQueryBegin'+
                        '[ClassName]'+tempClassName+
                        '[Caption]'+tempCaption+('起')+
                        '[Name]'+tempName+'_BegEndQueryBegin'+
                        '[DefaultValue]'+tempDefaultValue+
                        '[FillSql]'+tempFillSql+
                        '[ParentField]'+tempParentField+
                        '[Keyfield]'+tempKeyfield+
                        '[ListField]'+tempListField+
                        '[DownField]'+tempDownField+
                        ABSubStrEnd+ABEnterWrapStr+
                        ABSubStrBegin+
                        '[ConnName]'+tempConnName+
                        '[CacleDataset]'+tempCacleDataset+
                        '[ClassName]'+tempClassName+
                        '[Caption]'+tempCaption+('止')+
                        '[Name]'+tempName+'_BegEndQueryEnd'+
                        '[DefaultValue]'+tempDefaultValue+
                        '[FillSql]'+tempFillSql+
                        '[ParentField]'+tempParentField+
                        '[Keyfield]'+tempKeyfield+
                        '[ListField]'+tempListField+
                        '[DownField]'+tempDownField+
                        ABSubStrEnd;
          end
          else
          begin
            tempStr1:=ABSubStrBegin+
                        '[ConnName]'+tempConnName+
                        '[CacleDataset]'+tempCacleDataset+
                        '[ClassName]'+tempClassName+
                        '[Caption]'+tempCaption+
                        '[Name]'+tempName+
                        '[OperateFlags]'+
                        '[DefaultValue]'+tempDefaultValue+
                        '[FillSql]'+tempFillSql+
                        '[ParentField]'+tempParentField+
                        '[Keyfield]'+tempKeyfield+
                        '[ListField]'+tempListField+
                        '[DownField]'+tempDownField+
                        ABSubStrEnd;
          end;
          result:=result+tempStr1+ABEnterWrapStr;
        end;
      end;
    end;
  end;
end;

procedure ABFinalization;
begin
  ABAfterConnProc:=nil;
  ABFuncPointStrings.Free;
  ABApproveBillStrings.Free;
  ABBillInputStrings.Free;
end;

procedure ABInitialization;
begin
  FDBControlNameDataset:=nil;
  ABAfterConnProc:=ABCustomAfterConnProc;
  ABFuncPointStrings:=TStringList.Create;
  ABApproveBillStrings:=TStringList.Create;
  ABBillInputStrings :=TStringList.Create;
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;


end.



