unit ABFramkWorkTestU;

interface

uses
  ABPubVarU                             ,
  ABPubAutoProgressBar_ThreadU          ,
  ABPubCharacterCodingU                 ,
  ABPubCheckedComboBoxU                 ,
  ABPubCheckTreeViewU                   ,
  ABPubConstU                           ,
  ABPubDBLabelsU                        ,
  ABPubMultilingualDBNavigatorU         ,
  ABPubDBMainDetailPopupMenuU           ,
  ABPubDefaultValueU                    ,
  ABPubDesignSelectControlU             ,
  ABPubDesignU                          ,
  ABPubDogKeyU                          ,
  ABPubDownButtonU                      ,
  ABPubEditPassWordU                    ,
  ABPubFileRunOneU                      ,
  ABPubFormU                            ,
  ABPubFrameU                           ,
  ABPubFuncU                            ,
  ABPubImageFieldU                      ,
  ABPubImageU                           ,
  ABPubIndyU                            ,
  ABPubInPutPassWordU                   ,
  ABPubInPutStrU                        ,
  ABPubIPEditU                          ,
  ABPubItemListFrameU                   ,
  ABPubItemListsFrameU                  ,
  ABPubLanguageEditU                    ,
  ABPubLanguageU                        ,
  ABPubLedU                             ,
  ABPubLocalParamsEditU                 ,
  ABPubLocalParamsU                     ,
  ABPubLoginU                           ,
  ABPubLogU                             ,
  ABPubManualProgressBar_ThreadU        ,
  ABPubManualProgressBarU               ,
  ABPubMd5U                             ,
  ABPubMemoFieldU                       ,
  ABPubMemoU                            ,
  ABPubMessageU                         ,
  ABPubOrderStrU                        ,
  ABPubPakandZipU                       ,
  ABPubPassU                            ,
  ABPubPropertyEditU                    ,
  ABPubRegisterFileU                    ,
  ABPubRegU                             ,
  ABPubScriptU                          ,
  ABPubScrollBoxU                       ,
  ABPubSelectCheckComboBoxU             ,
  ABPubSelectCheckTreeViewU             ,
  ABPubSelectComboBoxU                  ,
  ABPubSelectStrU                       ,
  ABPubSelectTreeViewU                  ,
  ABPubSelectNameAndValuesU             ,
  ABPubServiceU                         ,
  ABPubShowEditDatasetU                 ,
  ABPubShowEditFieldValueU              ,
  ABPubSockDemoU                        ,
  ABPubSockFrameU                       ,
  ABPubSockU                            ,
  ABPubSQLLogU                          ,
  ABPubSQLParseU                        ,
  ABPubStartFormU                       ,
  ABPubStringGridU                      ,
  ABPubThreadDemoU                      ,
  ABPubThreadU                          ,
  ABPubUserU                            ,
  ABPubDBU                              ,
  ABPubSelectListBoxU                   ,

  ABThirdFuncU                          ,
  ABThirdImportDataU                    ,
  ABThirdCustomQueryU                   ,
  ABThirdRegU                           ,
  ABThird_cxGridPopupMenu_AddColumU     ,
  ABThird_cxGridPopupMenu_ColorSetupU   ,
  ABThird_cxGridPopupMenu_FigureU       ,
  ABThird_cxGridPopupMenu_PrintU        ,
  ABThird_DevExtPopupMenuU              ,
  ABThirdCacheDatasetU                  ,
  ABThirdConnDatabaseU                  ,
  ABThirdConnServerU                    ,
  ABThirdConnU                          ,
  ABThirdcxGridAndcxTreeViewSearchU     ,
  ABThirdQueryU                         ,
  ABThirdDBU                            ,

  ABFramkWorkQueryU                     ,
  ABFramkWorkConstU                     ,
  ABFramkWorkControlU                   ,
  ABFramkWorkQueryFormU            ,
  ABFramkWorkDBPanelU                   ,
  ABFramkWorkQuerySelectFieldPanelU           ,
  ABFramkWorkQuerySelectFieldPopupEditU       ,
  ABFramkWorkQueryAllFieldComboxU         ,
  ABFramkWorkcxGridU               ,
  ABFramkWorkDictionaryQueryU           ,
  ABFramkWorkDownManagerU               ,
  ABFramkWorkFastReportU                ,
  ABFramkWorkFuncFormU                  ,
  ABFramkWorkFuncU                      ,
  ABFramkWorkTreeItemEditU              ,
  ABFramkWorkUpAndDownU                 ,
  ABFramkWorkUserU                      ,
  ABFramkWorkVarU                       ,
  ABFramkWorkDBNavigatorU               ,
  ABFramkWorkDBPanelFormU    ,
  ABFramkWorkOperatorDatasetU           ,
  ABFramkWorkRegU                       ,


  printers,
  Variants,
  DateUtils,SysUtils, StdConvs,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  ComCtrls, StdCtrls, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, DBCtrls,
  Grids, DBGrids, Controls, ExtCtrls, Classes, Menus,Forms, cxButtons,
  cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCheckBox, cxLabel,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxCustomPivotGrid,
  cxDBPivotGrid, cxCheckComboBox, cxDBCheckComboBox, cxDBEdit,
  cxDBExtLookupComboBox, cxButtonEdit, dxCore, cxDateUtils,
  cxTL, cxTLdxBarBuiltInMenu, ShlObj, cxShellCommon, frxClass, dxStatusBar,
  dxdbtrel, cxShellTreeView, cxShellListView, cxSplitter, cxHeader, cxTreeView,
  cxListView, cxMCListBox, cxSpinButton, cxGroupBox, cxShellComboBox,
  cxRichEdit, cxCheckGroup, cxFontNameComboBox, cxColorComboBox, cxCheckListBox,
  cxTrackBar, cxProgressBar, cxListBox, cxRadioGroup, cxBlobEdit, cxImage,
  cxCurrencyEdit, cxHyperLinkEdit, cxCalc, cxSpinEdit, cxImageComboBox, cxMemo,
  dxtree, cxTLData, cxInplaceContainer, cxPC, cxMRUEdit, cxCalendar, cxTimeEdit,
  cxGridBandedTableView, cxGridDBBandedTableView, cxDBTrackBar, cxDBProgressBar,
  cxDBTL, cxDBShellComboBox, cxDBFontNameComboBox, cxDBColorComboBox,
  cxDBCheckGroup, cxDBCheckListBox, cxDBRichEdit, cxDBLabel, Graphics, ImgList,
  ABPubPanelU, dxBarBuiltInMenu, cxNavigator, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, dxBar, dxdbtree, cxPivotGrid,
  FireDAC.Phys.MSSQLDef, FireDAC.Phys, FireDAC.Phys.ODBCBase, FireDAC.Phys.MSSQL,
  FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Comp.UI, Vcl.Buttons;

type
  TABTestForm = class(TABPubForm)
    Panel8: TPanel;
    Memo4: TMemo;
    Memo3: TMemo;
    ProgressBar2: TProgressBar;
    Panel10: TPanel;
    Button52: TButton;
    Button6: TButton;
    ABQuery2: TABQuery;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    PopupMenu1: TPopupMenu;
    ABQuery3: TABQuery;
    ABFrxReport1: TABFrxReport;
    ImageList1: TImageList;
    pgc1: TPageControl;
    ts1: TTabSheet;
    pnl3: TPanel;
    grp3: TGroupBox;
    btn2: TButton;
    grp7: TGroupBox;
    btn11: TButton;
    grp8: TGroupBox;
    btn12: TButton;
    ComboBox3: TComboBox;
    grp9: TGroupBox;
    btn13: TButton;
    pnl7: TPanel;
    grp13: TGroupBox;
    btn17: TButton;
    grp14: TGroupBox;
    btn18: TButton;
    pnl8: TPanel;
    ts6: TTabSheet;
    pgc3: TPageControl;
    ts7: TTabSheet;
    ABcxDBTimeEdit1: TABcxDBTimeEdit;
    ABcxDBDateEdit1: TABcxDBDateEdit;
    ABcxDBDateTimeEdit1: TABcxDBDateTimeEdit;
    ABcxDBCalcEdit1: TABcxDBCalcEdit;
    ABcxDBSpinEdit1: TABcxDBSpinEdit;
    ABcxDBCurrencyEdit1: TABcxDBCurrencyEdit;
    ABcxDBTextEdit1: TABcxDBTextEdit;
    ABcxDBMaskEdit1: TABcxDBMaskEdit;
    ABcxDBLabel1: TABcxDBLabel;
    ABcxDBCheckBox1: TABcxDBCheckBox;
    ABcxDBComboBox1: TABcxDBComboBox;
    ABcxDBColorComboBox1: TABcxDBColorComboBox;
    ABcxDBFontNameComboBox1: TABcxDBFontNameComboBox;
    ABcxDBShellComboBox1: TABcxDBShellComboBox;
    ABcxDBImageComboBox1: TABcxDBImageComboBox;
    ABcxDBMRUEdit1: TABcxDBMRUEdit;
    ABcxDBHyperLinkEdit1: TABcxDBHyperLinkEdit;
    ABcxDBBlobEdit1: TABcxDBBlobEdit;
    ABcxDBButtonEdit1: TABcxDBButtonEdit;
    ABcxDBComBoBoxMemo1: TABcxDBComBoBoxMemo;
    ABcxDBFieldSelects1: TABcxDBFieldSelects;
    ABcxDBDirSelect1: TABcxDBDirSelect;
    ABcxDBLookupComboBox1: TABcxDBLookupComboBox;
    ABcxDBProgressBar1: TABcxDBProgressBar;
    ABcxDBPopupEdit1: TABcxDBPopupEdit;
    ABdxDBStatusBar1: TABdxDBStatusBar;
    ABcxDBSQLFieldSelects1: TABcxDBSQLFieldSelects;
    ABcxDBFieldOrder1: TABcxDBFieldOrder;
    ABcxDBCheckComboBox1: TABcxDBCheckComboBox;
    ABcxDBExtLookupComboBox1: TABcxDBExtLookupComboBox;
    ts9: TTabSheet;
    pnl11: TPanel;
    pnl12: TPanel;
    ABcxDBPivotGrid1: TABcxDBPivotGrid;
    pnl13: TPanel;
    grp20: TGroupBox;
    btn24: TButton;
    Panel7: TPanel;
    ABcxButton1: TABcxButton;
    btn25: TButton;
    btn26: TButton;
    btn27: TButton;
    ABDBcxLabel1: TABDBFieldCaption;
    grp4: TGroupBox;
    grp5: TGroupBox;
    Panel4: TPanel;
    pnl5: TPanel;
    btn3: TButton;
    btn4: TButton;
    btn5: TButton;
    btn6: TButton;
    grp6: TGroupBox;
    btn7: TButton;
    btn8: TButton;
    btn9: TButton;
    pnl6: TPanel;
    lbl1: TLabel;
    btn10: TButton;
    ComboBox2: TComboBox;
    grp10: TGroupBox;
    btn14: TButton;
    Panel1: TPanel;
    GroupBox3: TGroupBox;
    Button1: TButton;
    GroupBox5: TGroupBox;
    Button3: TButton;
    GroupBox6: TGroupBox;
    ABcxGrid1: TABcxGrid;
    ABcxGrid1ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    ABcxGrid1DBBandedTableView1: TcxGridDBBandedTableView;
    ABcxGrid1DBBandedTableView1TR_Guid: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1TR_Group: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1TR_SysUse: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1TR_Code: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1TR_Name: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1TR_DownFields: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1TR_ViewFields: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1TR_SaveFields: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1TR_int1: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1TR_int2: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1TR_decimal1: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1TR_decimal2: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1TR_Varchar1: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1TR_Varchar2: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1TR_bit1: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1TR_bit2: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1TR_Remark: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1TR_Order: TcxGridDBBandedColumn;
    ABcxGrid1DBBandedTableView1PubID: TcxGridDBBandedColumn;
    ABcxGrid1Level1: TcxGridLevel;
    ABcxGrid1Level2: TcxGridLevel;
    GroupBox4: TGroupBox;
    ABDBNavigator1: TABDBNavigator;
    ABcxDBMemo1: TABcxDBMemo;
    ABcxDBRichEdit1: TABcxDBRichEdit;
    ABcxDBCheckListBox1: TABcxDBCheckListBox;
    ABcxDBRadioGroup1: TABcxDBRadioGroup;
    ABcxDBCheckGroup1: TABcxDBCheckGroup;
    ABcxDBImage1: TABcxDBImage;
    ABcxDBListBox1: TABcxDBListBox;
    ABcxDBTreeList1: TABcxDBTreeList;
    ABcxDBTreeList1cxDBTreeListColumn1: TcxDBTreeListColumn;
    ABcxDBTrackBar1: TABcxDBTrackBar;
    ABFieldCaptionQuery1: TABFieldCaptionQuery;
    ABDictionaryQuery1: TABDictionaryQuery;
    ABDatasource1: TABDatasource;
    ABcxDBTreeView1: TABcxDBTreeView;
    FDPhysMSSQLDriverLink1: TFDPhysMSSQLDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    Panel3: TPanel;
    grp11: TGroupBox;
    grp18: TGroupBox;
    ABQueryAllFieldCombox1: TABQueryAllFieldCombox;
    ComboBox1: TComboBox;
    grp15: TGroupBox;
    ABDBPanel1: TABDBPanel;
    DBGrid1: TDBGrid;
    TabSheet1: TTabSheet;
    grp17: TGroupBox;
    ABQuerySelectFieldPanel1: TABQuerySelectFieldPanel;
    pnl10: TPanel;
    ABcxDateEdit1: TABcxDateEdit;
    ABcxDateTimeEdit1: TABcxDateTimeEdit;
    ABcxComBoBoxMemo1: TABcxComBoBoxMemo;
    ABcxDirSelect1: TABcxDirSelect;
    ABcxFileSelect1: TABcxFileSelect;
    ABcxLabel1: TABcxLabel;
    ABcxTextEdit1: TABcxTextEdit;
    ABcxMaskEdit1: TABcxMaskEdit;
    ABcxButtonEdit1: TABcxButtonEdit;
    ABcxCheckBox1: TABcxCheckBox;
    ABcxComboBox1: TABcxComboBox;
    ABcxImageComboBox1: TABcxImageComboBox;
    ABcxSpinEdit1: TABcxSpinEdit;
    ABcxCalcEdit1: TABcxCalcEdit;
    ABcxHyperLinkEdit1: TABcxHyperLinkEdit;
    ABcxCurrencyEdit1: TABcxCurrencyEdit;
    ABcxBlobEdit1: TABcxBlobEdit;
    ABcxMRUEdit1: TABcxMRUEdit;
    ABcxLookupComboBox1: TABcxLookupComboBox;
    ABcxRadioButton1: TABcxRadioButton;
    ABcxProgressBar1: TABcxProgressBar;
    ABcxColorComboBox1: TABcxColorComboBox;
    ABcxFontNameComboBox1: TABcxFontNameComboBox;
    ABcxCheckComboBox1: TABcxCheckComboBox;
    ABcxPopupEdit1: TABcxPopupEdit;
    ABcxShellComboBox1: TABcxShellComboBox;
    ABcxButton2: TABcxButton;
    ABcxSpinButton1: TABcxSpinButton;
    ABcxHeader1: TABcxHeader;
    ABcxExtLookupComboBox1: TABcxExtLookupComboBox;
    ABcxTimeEdit1: TABcxTimeEdit;
    edt1: TEdit;
    ABcxTreeView1: TABcxTreeView;
    ABcxMCListBox1: TABcxMCListBox;
    ABcxGroupBox1: TABcxGroupBox;
    ABcxRichEdit1: TABcxRichEdit;
    ABcxCheckGroup1: TABcxCheckGroup;
    ABcxCheckListBox1: TABcxCheckListBox;
    ABcxMemo1: TABcxMemo;
    ABcxTrackBar1: TABcxTrackBar;
    ABcxRadioGroup1: TABcxRadioGroup;
    ABcxListBox1: TABcxListBox;
    ABcxImage1: TABcxImage;
    ABcxTabControl1: TABcxTabControl;
    lbl2: TLabel;
    ABcxShellListView1: TABcxShellListView;
    ABcxShellTreeView1: TABcxShellTreeView;
    ABcxPageControl1: TABcxPageControl;
    cxTabSheet1: TcxTabSheet;
    lbl3: TLabel;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    ABcxTreeList1: TABcxTreeList;
    ABcxTreeList1Column1: TcxTreeListColumn;
    ABcxTreeList1Column2: TcxTreeListColumn;
    ABcxTreeList1Column3: TcxTreeListColumn;
    ABcxVirtualTreeList1: TABcxVirtualTreeList;
    ABcxVirtualTreeList1Column1: TcxTreeListColumn;
    ABcxVirtualTreeList1Column2: TcxTreeListColumn;
    ABcxVirtualTreeList1Column3: TcxTreeListColumn;
    ABcxListView1: TABcxListView;
    ABcxPivotGrid1: TABcxPivotGrid;
    ABcxTreeViewPopupEdit1: TABcxTreeViewPopupEdit;
    ABcxFieldOrder1: TABcxFieldOrder;
    ABcxSQLFieldSelects1: TABcxSQLFieldSelects;
    BitBtn1: TBitBtn;
    ABQuerySelectFieldPopupEdit1: TABQuerySelectFieldPopupEdit;
    DBGrid2: TDBGrid;
    FDQuery1: TFDQuery;
    procedure Button52Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Memo3KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btn13Click(Sender: TObject);
    procedure btn12Click(Sender: TObject);
    procedure btn11Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure btn6Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn7Click(Sender: TObject);
    procedure btn8Click(Sender: TObject);
    procedure btn9Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure ABcxButton1Click(Sender: TObject);
    procedure btn27Click(Sender: TObject);
    procedure btn25Click(Sender: TObject);
    procedure btn26Click(Sender: TObject);
    procedure btn18Click(Sender: TObject);
    procedure btn17Click(Sender: TObject);
    procedure btn14Click(Sender: TObject);
    procedure btn24Click(Sender: TObject);
    procedure btn10Click(Sender: TObject);
    procedure ABcxVirtualTreeList1GetChildCount(Sender: TcxCustomTreeList;
      AParentNode: TcxTreeListNode; var ACount: Integer);
    procedure ABcxVirtualTreeList1GetNodeValue(Sender: TcxCustomTreeList;
      ANode: TcxTreeListNode; AColumn: TcxTreeListColumn; var AValue: Variant);
    procedure Button3Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ABQuery2AfterShowDBPanel(Sender: TObject);
    procedure ABQuery2BeforeShowDBPanel(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    FpriDatetime:TDateTime;
  private
    procedure Addlog(aMemo:TMemo;atext: string);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABTestForm: TABTestForm;

implementation
{$R *.dfm}

procedure TABTestForm.Memo3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Shift=[ssCtrl]) then
  begin
    if  (Key=65) then
    begin
      TMemo(Sender).SelectAll;
    end;
  end;
end;

procedure TABTestForm.Button1Click(Sender: TObject);
var
  tempQuery_Absys_org_tree,
  tempQuery_Absys_org_treeItem,
  tempQuery_ABSys_Org_Table,
  tempQuery_ABSys_Org_Field: TDataSet;
begin

  tempQuery_Absys_org_tree     :=ABGetDataset('main','select * from Absys_org_tree',[]);
  tempQuery_Absys_org_treeItem :=ABGetDataset('main','select * from Absys_org_treeItem ',[]);
  tempQuery_ABSys_Org_Table    :=ABGetDataset('main','select * from ABSys_Org_Table',[]);
  tempQuery_ABSys_Org_Field    :=ABGetDataset('main','select * from Absys_org_Field',[]);
  try
    ABOperatorDataset('下拉列项',
                      ['下拉列项主'],
                      [''],
                      ['select * from Absys_org_tree'],
                      [],

                      [['下拉列项明细']],
                      [['']],
                      [['select * from Absys_org_treeItem  where Ti_Tr_Guid=:Tr_Guid']],
                      [['Tr_Guid']],
                      [['Ti_Tr_Guid']],
                      [[]],
                      [],[]

                      );
    Addlog(Memo3,'ABFramkWorkOperatorDatasetU ABOperatorDataset(操作传入SQL的数据):');

    ABOperatorDataset('下拉列项+数据字典',
                      ['下拉列项主','数据字典主'],
                      ['',''],
                      ['select * from Absys_org_tree','select * from ABSys_Org_Table'],
                      [],

                      [['下拉列项明细','数据字典明细']],
                      [['','']],
                      [['select * from Absys_org_treeItem  where Ti_Tr_Guid=:Tr_Guid','select * from Absys_org_Field where Fi_Ta_Guid=:Ta_Guid ']],
                      [['Tr_Guid','Ta_Guid']],
                      [['Ti_Tr_Guid','Fi_Ta_Guid']],
                      [[]],
                      [],[]

                      );
    Addlog(Memo3,'ABFramkWorkOperatorDatasetU ABOperatorDataset(操作传入SQL的数据):');

    ABOperatorDataset('下拉列项+数据字典',
                      ['下拉列项主','数据字典主'],
                      ['',''],
                      ['select * from Absys_org_tree','select * from ABSys_Org_Table'],
                      ['Tr_Code,Tr_Name',''],

                      [['下拉列项明细','数据字典明细']],
                      [['','']],
                      [['select * from Absys_org_treeItem  where Ti_Tr_Guid=:Tr_Guid','select * from Absys_org_Field where Fi_Ta_Guid=:Ta_Guid ']],
                      [['Tr_Guid','Ta_Guid']],
                      [['Ti_Tr_Guid','Fi_Ta_Guid']],
                      [['Ti_Code,Ti_Name','']],
                      [],[]
                      );
    Addlog(Memo3,'ABFramkWorkOperatorDatasetU ABOperatorDataset(操作传入SQL的数据):');

  finally
    tempQuery_Absys_org_tree     .free;
    tempQuery_Absys_org_treeItem .free;
    tempQuery_ABSys_Org_Table    .free;
    tempQuery_ABSys_Org_Field    .free;
  end;
end;

procedure TABTestForm.Button3Click(Sender: TObject);
begin
  Addlog(Memo3,'ABFramkWorkQueryU TABDatasource.AutoEdit()'+ABBoolToStr(ABDatasource1.AutoEdit));
  Addlog(Memo3,'ABFramkWorkQueryU TABQuery.StopMultiSelectUpdate(停用关联的GRID多选时的更新)'+ABBoolToStr(ABQuery2.StopMultiSelectUpdate));

  ABQuery2.SetEnabled('');
  Addlog(Memo3,'ABFramkWorkQueryU TABQuery.SetEnabled(设置指定字段关联控件的Enabled '''')');
  ABShow('1');
  ABQuery2.SetEnabled('aaaaaaa');
  Addlog(Memo3,'ABFramkWorkQueryU TABQuery.SetEnabled(设置指定字段关联控件的Enabled ''aaaaaaa'')');
  ABShow('2');
  ABQuery2.SetEnabled('Tr_Code,Tr_Name');
  Addlog(Memo3,'ABFramkWorkQueryU TABQuery.SetEnabled(设置指定字段关联控件的Enabled ''Tr_Code,Tr_Name'')');
  ABShow('3');

  ABQuery2.SetVisible('');
  Addlog(Memo3,'ABFramkWorkQueryU TABQuery.SetVisible(设置指定字段关联控件的Visible'''')');
  ABShow('4');
  ABQuery2.SetVisible('aaaaaaa');
  Addlog(Memo3,'ABFramkWorkQueryU TABQuery.SetVisible(设置指定字段关联控件的Visible''aaaaaaa'')');
  ABShow('5');
  ABQuery2.SetVisible('Tr_Code,Tr_Name');
  Addlog(Memo3,'ABFramkWorkQueryU TABQuery.SetVisible(设置指定字段关联控件的Visible''Tr_Code,Tr_Name'')');
  ABShow('6');
  ABQuery2.OpenQuery;
  Addlog(Memo3,'ABFramkWorkQueryU TABQuery.OpenQuery(开窗查询数据)');
  ABShow('7');
  ABQuery2.ShowDBPanel(false);
  Addlog(Memo3,'ABFramkWorkQueryU TABQuery.ShowDBPanel(以面板方式显示当前记录的数据 false)');
  ABShow('8');
  ABQuery2.ShowDBPanel(True);
  Addlog(Memo3,'ABFramkWorkQueryU TABQuery.ShowDBPanel(以面板方式显示当前记录的数据 True)');
  ABShow('9');
  ABQuery2.ShowDBPanel(True,'1');
  Addlog(Memo3,'ABFramkWorkQueryU TABQuery.ShowDBPanel(以面板方式显示当前记录的数据True,1)');
  ABShow('10');
  ABQuery2.ShowDBPanel(True,'2');
  Addlog(Memo3,'ABFramkWorkQueryU TABQuery.ShowDBPanel(以面板方式显示当前记录的数据True,2)');
  ABShow('11');
  ABQuery2.ShowDBPanel(True,'3');
  Addlog(Memo3,'ABFramkWorkQueryU TABQuery.ShowDBPanel(以面板方式显示当前记录的数据True,3)');
  ABShow('12');
  ABQuery2.ShowDBPanel(True,'1','Tr_Code,Tr_Name','');
  Addlog(Memo3,'ABFramkWorkQueryU TABQuery.ShowDBPanel(以面板方式显示当前记录的数据True,1,''Tr_Code,Tr_Name'')');
  ABShow('13');
  ABQuery2.ShowDBPanel(True,'1','','Tr_Code,Tr_Name');
  Addlog(Memo3,'ABFramkWorkQueryU TABQuery.ShowDBPanel(以面板方式显示当前记录的数据True,1,'''',''Tr_Code,Tr_Name'')');
  ABShow('14');

  ABFieldDefByFieldName(ABQuery2,'Tr_Code').Fi_GetValueSQL:='select 10*:Tr_Name';
  ABQuery2.append;
  ABQuery2.fieldbyname('Tr_Name').asstring:='12345';
  ABFieldDefByFieldName(ABQuery2,'Tr_Code').Fi_GetValueSQL:='select 20*:Tr_Name';
  ABFieldForGetValueField(ABFieldDefByFieldName(ABQuery2,'Tr_Code'),ABQuery2);
  Addlog(Memo3,'ABFramkWorkQueryU ABFieldForGetValueField(字段对有定义取值SQL的影响)');

  ABQuery2.fieldbyname('Tr_Code').asstring:='111';
  ABFieldChangeForCiteField(ABFieldDefByFieldName(ABQuery2,'Tr_Name'));
  Addlog(Memo3,'ABFramkWorkQueryU ABFieldChangeForCiteField(处理字段的编辑对其它引用到此字段的字段取值的影响)');
  ABFieldChangeForRelatingField(ABFieldDefByFieldName(ABQuery2,'Tr_Code'));
  Addlog(Memo3,'ABFramkWorkQueryU ABFieldChangeForRelatingField(关联替换字段的处理)');
  ABQuery2.cancel;

  ABSetDataSetControlOnSateChange(ABQuery2);
  Addlog(Memo3,'ABFramkWorkQueryU ABSetDataSetControlOnSateChange(设置DataSet关联控件控制)');
  ABSetDataSetCiteControl(ABQuery2);
  Addlog(Memo3,'ABFramkWorkQueryU ABSetDataSetCiteControl(设置DataSet动态控制)');

  ABSetFieldReadOnlyAndColor(ABFieldDefByFieldName(ABQuery2,'Tr_Code'),true);
  Addlog(Memo3,'ABFramkWorkQueryU ABSetFieldReadOnlyAndColor(设置字段的颜色控制)');


  ABInitFormDataSet([ABDBPanel1],[ABcxGrid1ABcxGridDBBandedTableView1],ABQuery2);
  Addlog(Memo3,'ABFramkWorkQueryU ABInitFormDataSet(功能中窗体中的初始化)');
end;

procedure TABTestForm.Button52Click(Sender: TObject);
begin
  Memo3.Clear;
end;

procedure TABTestForm.Addlog(aMemo:TMemo;atext:string);
var
  tempDatetime:TDateTime;
begin
  tempDatetime:=Now;
  aMemo.Lines.Add('时间:'+ABDateTimeToStr(tempDatetime,23)+' '+
                  '距上次日志['+ABRoundStr(ABGetDateTimeSpan_Float(FpriDatetime,tempDatetime,tuMilliSeconds),2,true)+'毫秒] '+
                  atext);
  FpriDatetime:=tempDatetime;
end;

procedure TABTestForm.BitBtn1Click(Sender: TObject);
begin
  ABcxGrid1DBBandedTableView1.DataController.Filter.Active := False;
  ABcxGrid1DBBandedTableView1.DataController.Filter.FilterText := 'Tr_Code=''Sex'' ';
  ABcxGrid1DBBandedTableView1.DataController.Filter.Active := true;
end;

procedure TABTestForm.Button6Click(Sender: TObject);
begin
  Memo3.Clear;
  Memo4.Clear;
end;

//cxVirtualTreeList是需自己动态增加结点的树列表, GetChildCount事件用于设置某父结点下的子结点数量
procedure TABTestForm.ABcxVirtualTreeList1GetChildCount(
  Sender: TcxCustomTreeList; AParentNode: TcxTreeListNode; var ACount: Integer);
begin
{
  //当父结点层小于4时每层为10个字节点
  if AParentNode.Level < 4 then
    ACount := 10;
    }
end;

//cxVirtualTreeList是需自己动态增加结点的树列表, GetNodeValue事件用于设置结点下的值
procedure TABTestForm.ABcxVirtualTreeList1GetNodeValue(
  Sender: TcxCustomTreeList; ANode: TcxTreeListNode; AColumn: TcxTreeListColumn;
  var AValue: Variant);
begin
{
  case AColumn.ItemIndex of
    0:AValue := ANode.VisibleIndex;
    1:
      AValue := 'Level:' + IntToStr(ANode.Level);
    2:
      AValue := Now + ANode.VisibleIndex * 0.001
  else
    AValue := ''
  end;
  }
end;

procedure TABTestForm.ABQuery2AfterShowDBPanel(Sender: TObject);
begin
  Addlog(Memo3,'ABFramkWorkQueryU TABQuery.OnAfterShowDBPanel');
end;

procedure TABTestForm.ABQuery2BeforeShowDBPanel(Sender: TObject);
begin
  Addlog(Memo3,'ABFramkWorkQueryU TABQuery.OnBeforeShowDBPanel');
end;

procedure TABTestForm.btn13Click(Sender: TObject);
begin
  //ABDownAllPkgs;
  //Addlog(Memo3,'ABFramkWorkUpAndDownU ABDownAllPkgs(下载所有的程序包到本地):');
  //ABUpdateAllPkgs;
  //Addlog(Memo3,'ABFramkWorkUpAndDownU ABUpdateAllPkgs(更新本地所有的程序包到服务器):');

end;

procedure TABTestForm.btn12Click(Sender: TObject);
begin
  ABAppendLog_ErrorInfo('1号单元','1号单元的林业厅','1号单元的林业厅aaaa');
  Addlog(Memo3,'ABFramkWorkFuncU ABAppendLog_ErrorInfo(增加错误日志的数据):');

  ABAppendLog_CustomEvent('1号单元','1号单元的林业厅','1号单元的林业厅aaaa');
  Addlog(Memo3,'ABFramkWorkFuncU ABAppendLog_CustomEvent(增加自定义日志的数据):');

  Addlog(Memo3,'ABFramkWorkFuncU ABFieldDefByFieldName(取数据集字段的框架信息 Fi_Ta_Name):'+ABFieldDefByFieldName(ABQuery2,'Tr_Name').Fi_Ta_Name);

  Addlog(Memo3,'ABFramkWorkFuncU ABGetDatasetQueryConst(取得数据集查询的定义):'+ABGetDatasetQueryConst(ABQuery2));
  Addlog(Memo3,'ABFramkWorkFuncU ABGetConstSqlPubDataset(取得SQL共用数据集):'+ABGetConstSqlPubDataset('ABSys_Org_Key',[]).Name);

  Addlog(Memo3,'ABFramkWorkFuncU ABGetFuncFieldValueByFuncFileName(根据功能的文件名得到功能GUID):'+ABGetFuncFieldValueByFuncFileName('ABClient.exe','Fu_Guid'));
  Addlog(Memo3,'ABFramkWorkFuncU ABGetBPLFileName(取得Owner窗体所在的BPL文件名):'+ABGetBPLFileName(self));

  Addlog(Memo3,'ABFramkWorkFuncU ABGetTableFieldNames(根据连接名和表名得到排除指定字段外的字段名):'+ABGetTableFieldNames('ABSys_Org_table',[]));
  Addlog(Memo3,'ABFramkWorkFuncU ABGetTableNoTypeFieldNames(根据连接名和表名得到排除的字段类型外的字段名):'+ABGetTableNoTypeFieldNames('ABSys_Org_table'));
  Addlog(Memo3,'ABFramkWorkFuncU ABGetTableFieldNamesSQL(根据连接名和表名得到排除指定字段外的取数据SQL):'+ABGetTableFieldNamesSQL('ABSys_Org_table',[]));
  Addlog(Memo3,'ABFramkWorkFuncU ABGetTablePrimaryKeyFieldNames(取数据表的主键字段):'+ABGetTablePrimaryKeyFieldNames('ABSys_Org_table'));

  ABExecBeforeReportSQL;
  Addlog(Memo3,'ABFramkWorkFuncU ABExecBeforeReportSQL(执行报表前执行SQL):');
  Addlog(Memo3,'ABFramkWorkFuncU ABGetConnNameByConnGuid(通过连接GUID取得连接名称):'+ABGetConnNameByConnGuid(''));
  Addlog(Memo3,'ABFramkWorkFuncU ABGetConnGuidByConnName(通过连接名称取得连接GUID):'+ABGetConnGuidByConnName(''));
  Addlog(Memo3,'ABFramkWorkFuncU ABGetDetailTableDatabaseSQLFlag(根据传入的主从数据集，得到从集数据库的SQL前置标识):'+ABGetDetailTableDatabaseSQLFlag(ABQuery2,ABQuery2));

  ABFillTreeItemByTr_Code('Sex',['Ti_Code'],ComboBox3.Items);
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU ABFillTreeItemByTr_Code;');
end;

procedure TABTestForm.btn11Click(Sender: TObject);
begin
  ABUser.Login(ltLongin);
  Addlog(Memo3,'ABFramkWorkUserU ABUser.Login(ltLongin);');

  ABUser.Login(ltChangUser);
  Addlog(Memo3,'ABFramkWorkUserU ABUser.Login(ltChangUser);');
  ABUser.Login(ltAfreshLogin);
  Addlog(Memo3,'ABFramkWorkUserU ABUser.Login(ltAfreshLogin);');
  ABUser.Login(ltLock);
  Addlog(Memo3,'ABFramkWorkUserU ABUser.Login(ltLock);');

  ABUser.EditPassWord;
  Addlog(Memo3,'ABFramkWorkUserU ABUser.EditPassWord;');
  ABUser.LoginCheck('sysuser','aaaa');
  Addlog(Memo3,'ABFramkWorkUserU ABUser.LoginCheck;');

  Addlog(Memo3,'ABFramkWorkUserU ABUser.FuncTreeDataSet;'+inttostr(ABUser.FuncTreeDataSet.RecordCount));
  Addlog(Memo3,'ABFramkWorkUserU ABUser.OperatorDataSet;'+inttostr(ABUser.OperatorDataSet.RecordCount));
  Addlog(Memo3,'ABFramkWorkUserU ABUser.OperatorKeyDataSet;'+inttostr(ABUser.OperatorKeyDataSet.RecordCount));
  Addlog(Memo3,'ABFramkWorkUserU ABUser.OperatorKeyTableDataSet;'+inttostr(ABUser.OperatorKeyTableDataSet.RecordCount));
  Addlog(Memo3,'ABFramkWorkUserU ABUser.OperatorKeyFieldDataSet;'+inttostr(ABUser.OperatorKeyFieldDataSet.RecordCount));
  Addlog(Memo3,'ABFramkWorkUserU ABUser.OperatorKeyFunctionDataSet;'+inttostr(ABUser.OperatorKeyFunctionDataSet.RecordCount));
  Addlog(Memo3,'ABFramkWorkUserU ABUser.OperatorKeyExtendReportDataSet;'+inttostr(ABUser.OperatorKeyExtendReportDataSet.RecordCount));
  Addlog(Memo3,'ABFramkWorkUserU ABUser.OperatorKeyCustomObjectDataSet;'+inttostr(ABUser.OperatorKeyCustomObjectDataSet.RecordCount));

  Addlog(Memo3,'ABFramkWorkUserU ABUser.CheckClientLicenseFile;'+ABBoolToStr(ABUser.CheckClientLicenseFile));
  Addlog(Memo3,'ABFramkWorkUserU ABUser.CheckServerLicenseFile;'+ABBoolToStr(ABUser.CheckServerLicenseFile));
  Addlog(Memo3,'ABFramkWorkUserU ABUser.CheckClientUsbDog;'+ABBoolToStr(ABUser.CheckClientUsbDog));


  ABUser.LoginOut;
  Addlog(Memo3,'ABFramkWorkUserU ABUser.LoginOut;');
end;

procedure TABTestForm.btn2Click(Sender: TObject);
var
  tempCustomDictionaryQuery:TABCustomDictionaryQuery;
  tempFieldCaptionQuery:TABFieldCaptionQuery;
  tempInsideQuery:TABInsideQuery;
  tempDictionaryQuery:TABDictionaryQuery;
  tempList: TList;
begin
  tempList:= TList.Create;
  tempCustomDictionaryQuery:=TABCustomDictionaryQuery.Create(nil);
  tempCustomDictionaryQuery.SQL.Text:='select * from ABSys_Org_FuncReport';
  tempCustomDictionaryQuery.open;
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU TABCustomDictionaryQuery.LoadTables;'+tempCustomDictionaryQuery.LoadTables.Text);

  tempFieldCaptionQuery:=TABFieldCaptionQuery.Create(nil);
  tempFieldCaptionQuery.SQL.Text:='select * from ABSys_Org_FuncReport';
  tempFieldCaptionQuery.open;
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU TABFieldCaptionQuery.FieldByName(FR_GroupName).DisplayLabel;'+tempFieldCaptionQuery.FieldByName('FR_GroupName').DisplayLabel);

  tempInsideQuery:=TABInsideQuery.Create(nil);
  tempInsideQuery.SQL.Text:='select * from ABSys_Org_FuncReport';
  tempInsideQuery.open;
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU TABInsideQuery.PrimaryKeyFieldNames'+tempInsideQuery.PrimaryKeyFieldNames);

  tempDictionaryQuery:=TABDictionaryQuery.Create(nil);
  tempDictionaryQuery.SQL.Text:='select * from ABSys_Org_FuncReport';
  tempDictionaryQuery.open;

  tempDictionaryQuery.GetActiveDetailDataSets(tempList);
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU GetActiveDetailDataSets;'+inttostr(tempList.Count));
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU FieldDefaultValueCanUse;'+ABBoolToStr(tempDictionaryQuery.FieldDefaultValueCanUse('FR_Guid','')));
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU FieldCanCopy;'+ABBoolToStr(tempDictionaryQuery.FieldCanCopy('FR_Guid')));
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU FieldDefByFieldName;'+tempDictionaryQuery.FieldDefByFieldName('FR_Guid').Fi_Name);
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU FieldDefItems[0];'+tempDictionaryQuery.FieldDefItems[0].Fi_Name);
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU FieldDefBySetOrder(0);'+tempDictionaryQuery.FieldDefBySetOrder(-1).Fi_Name);
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU FieldsIsEdited;'+ABBoolToStr(tempDictionaryQuery.FieldsIsEdited));
  tempDictionaryQuery.SetFieldsEdited(true);
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU SetFieldsEdited;');
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU FieldsIsEdited;'+ABBoolToStr(tempDictionaryQuery.FieldsIsEdited));

  tempDictionaryQuery.Append;
  tempDictionaryQuery.FillDefaultValue(true,false);
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU FillDefaultValue;');
  tempDictionaryQuery.FillDefaultValue(false,true);
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU FillDefaultValue;');
  tempDictionaryQuery.SetInsertOrderValue;
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU SetInsertOrderValue;');
  tempDictionaryQuery.GetInsertOrderValue;
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU GetInsertOrderValue;');

  Addlog(Memo3,'ABFramkWorkDictionaryQueryU LinkDBPanelList;'+IntToStr(tempDictionaryQuery.LinkDBPanelList.Count));
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU LinkDBTableViewList;'+IntToStr(tempDictionaryQuery.LinkDBTableViewList.Count));
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU LoadFieldsDefed;'+ABBoolToStr(tempDictionaryQuery.LoadFieldsDefed));
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU PrimaryKeyFieldNames;'+IntToStr(tempDictionaryQuery.PrimaryKeyFieldNames.Count));
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU ClusteredFieldNames;'+IntToStr(tempDictionaryQuery.ClusteredFieldNames.Count));
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU DefList;'+IntToStr(tempDictionaryQuery.DefList.Count));

  Addlog(Memo3,'ABFramkWorkDictionaryQueryU FuncFileName;'+tempDictionaryQuery.FuncFileName);
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU FuncGuid;'+tempDictionaryQuery.FuncGuid);

  Addlog(Memo3,'ABFramkWorkDictionaryQueryU MultiLevelQuery;'+ABBoolToStr(tempDictionaryQuery.MultiLevelQuery));
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU MultiLevelCaption;'+tempDictionaryQuery.MultiLevelCaption);
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU MultiLevelOrder;'+IntToStr(tempDictionaryQuery.MultiLevelOrder));

  Addlog(Memo3,'ABFramkWorkDictionaryQueryU CanCheckFieldValue;'+ABBoolToStr(tempDictionaryQuery.CanCheckFieldValue));
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU StopSQLDefaultValueAfterInsert;'+ABBoolToStr(tempDictionaryQuery.StopSQLDefaultValueAfterInsert));
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU BlobFieldDefs;'+IntToStr(tempDictionaryQuery.BlobFieldDefs.Count));

  //ABCheckAllFieldValue(tempDictionaryQuery);
  //Addlog(Memo3,'ABFramkWorkDictionaryQueryU ABCheckAllFieldValue;');
  //ABCheckFieldValue(tempDictionaryQuery.FieldDefItems[0]);
  //Addlog(Memo3,'ABFramkWorkDictionaryQueryU ABCheckFieldValue;');

  ABFieldDefaultValueIsVar(tempDictionaryQuery,'FR_Guid');
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU ABFieldDefaultValueIsVar;');
  ABNeedResetDefaultValueBeforePost('Max');
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU ABNeedResetDefaultValueBeforePost;');
  ABGetTableCaption('','');
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU ABGetTableCaption;');
  ABGetPubFieldDownGroup;
  Addlog(Memo3,'ABFramkWorkDictionaryQueryU ABGetPubFieldDownGroup;');

  Addlog(Memo3,'ABThirdConnU ABCheckDatasetRepeat(false 检测数据集当前记录的检测字段是否有重复的检测字段值):'+ABBoolToStr(ABCheckDatasetRepeat(tempDictionaryQuery,'Ta_Guid','Ta_Name','ABSys_Org_Field')));


  tempList.Free;
end;

procedure TABTestForm.btn24Click(Sender: TObject);
begin
  ABCreateMultiLevelQueryControls(Panel7,Panel7.Owner,ABQuery2);
  Addlog(Memo3,'ABFramkWorkControlU ABCreateMultiLevelQueryControls;');
  ABCreateMultiLevelQueryInputMenuItem(Panel7,ABQuery2,PopupMenu1.Items,nil);
  Addlog(Memo3,'ABFramkWorkControlU ABCreateMultiLevelQueryInputMenuItem;');

  ABcxDBPivotGrid1.ExtPopupMenu.load;
  ABDBcxLabel1.RefreshCaption;


  ABcxTextEdit1.Text:='11';
  Addlog(Memo3,'ABThirdFuncU ABGetControlValue(得到控件的编辑值(可不停止扩充，现只实现部分cx系列)):'+VarToStr(ABGetControlValue(ABcxTextEdit1)));
  ABSetControlValue(ABcxTextEdit1,'22');
  Addlog(Memo3,'ABThirdFuncU ABSetControlValue(设置控件的编辑值(可不停止扩充，现只实现部分cx系列)):');
  Addlog(Memo3,'ABThirdFuncU ABGetControlValue(得到控件的编辑值(可不停止扩充，现只实现部分cx系列)):'+VarToStr(ABGetControlValue(ABcxTextEdit1)));
end;

procedure TABTestForm.btn25Click(Sender: TObject);
begin
  ABFreeMultiLevelQueryControls(Panel7,ABQuery2);
  Addlog(Memo3,'ABFramkWorkControlU ABFreeMultiLevelQueryControls;');
end;

procedure TABTestForm.ABcxButton1Click(Sender: TObject);
begin
  ABInputMultiLevelQueryControlValue(Panel7,ABQuery2,PopupMenu1.Items);
  Addlog(Memo3,'ABFramkWorkControlU ABInputMultiLevelQueryControlValue;');
end;

procedure TABTestForm.btn27Click(Sender: TObject);
var
  tempWhere,tempWhereRemark:string;
begin
  tempWhere:=ABGetMultiLevelQueryWhere(Panel7,ABQuery2,tempWhereRemark);
  Addlog(Memo3,'ABFramkWorkControlU ABGetMultiLevelQueryWhere;');
  ABQuery2.RefreshQuery(tempWhere,[]);
  Caption:= tempWhereRemark;
end;

procedure TABTestForm.btn26Click(Sender: TObject);
begin
  ABCreateMultiLevelQueryFieldNameStrings(ABQuery2,ComboBox1.Items);
  Addlog(Memo3,'ABFramkWorkControlU ABCreateMultiLevelQueryFieldNameStrings;');
end;

procedure TABTestForm.btn18Click(Sender: TObject);
var
  tempValue:string;
begin
  Addlog(Memo3,'ABFramkWorkTreeItemEditU ABShowDownListItem;'+ABBoolToStr(ABShowDownListItem('Enterprise Dir','Ti_Name',tempValue)));
end;

procedure TABTestForm.btn17Click(Sender: TObject);
var
  tempForm: TABQueryForm;
  tempWhere:string;
begin
  tempForm:=nil;
  if ABQueryFormQuery(tempWhere,ABQuery2,tempForm) then
  begin
    ABQuery2.RefreshQuery(tempWhere,[]);
  end;
end;

procedure TABTestForm.btn10Click(Sender: TObject);
var
  tempStr:string;
  temStrings: TStrings;
begin
  temStrings:= TStringList.Create;
  try
    ABStrsToStrings('报表,证卡,访客',',',temStrings);

    tempStr:=ABStringsToStrs(ABPubFuncPrintNames);
    if ABSelectNameAndValues(temStrings,printers.printer.printers,
                          tempStr) then
    begin
      ABStrsToStrings(tempStr,',',ABPubFuncPrintNames);
    end;
  finally
    temStrings.Free;
  end;
end;

procedure TABTestForm.btn5Click(Sender: TObject);
begin
  ABPubFrxReport.Init;
  ABPubFrxReport.DefaultPrint:=ComboBox2.Text;
  ABPubFrxReport.CreateOwner:= self;
  ABPubFrxReport.InputParamParent:= Panel4;
  ABPubFrxReport.ReportType:= rtExtend;
  ABPubFrxReport.PreviewReport(ABGetSQLValue('Main','select top 1 Er_Guid from ABSys_Org_ExtendReport',[],''));
end;

procedure TABTestForm.btn6Click(Sender: TObject);
begin
  ABPubFrxReport.Init;
  ABPubFrxReport.DefaultPrint:=ComboBox2.Text;
  ABPubFrxReport.CreateOwner:= self;
  ABPubFrxReport.InputParamParent:= Panel4;
  ABPubFrxReport.ReportType:= rtExtend;
  ABPubFrxReport.PrintReport(ABGetSQLValue('Main','select top 1 Er_Guid from ABSys_Org_ExtendReport',[],''));
end;

procedure TABTestForm.btn3Click(Sender: TObject);
begin
  ABPubFrxReport.Init;
  ABPubFrxReport.DefaultPrint:=ComboBox2.Text;
  ABPubFrxReport.CreateOwner:= self;
  ABPubFrxReport.InputParamParent:= Panel4;
  ABPubFrxReport.ReportType:= rtExtend;
  ABPubFrxReport.DesignReport(ABGetSQLValue('Main','select top 1 Er_Guid from ABSys_Org_ExtendReport',[],''));
end;

procedure TABTestForm.btn4Click(Sender: TObject);
begin
  ABPubFrxReport.CreateExtendReportInputControl(ABGetSQLValue('Main','select top 1 Er_Guid from ABSys_Org_ExtendReport',[],''),Panel4);
end;

procedure TABTestForm.btn7Click(Sender: TObject);
begin
  ABPubFrxReport.Init;
  ABPubFrxReport.DefaultPrint:=ComboBox2.Text;
  ABPubFrxReport.CreateOwner:= self;
  ABPubFrxReport.DataSource:= DataSource2;
  ABPubFrxReport.ReportType:= rtFunc;
  ABPubFrxReport.DesignReport(ABGetSQLValue('Main','select top 1 Fr_Guid from ABSys_Org_FuncReport',[],''));
end;

procedure TABTestForm.btn8Click(Sender: TObject);
begin
  ABPubFrxReport.Init;
  ABPubFrxReport.DefaultPrint:=ComboBox2.Text;
  ABPubFrxReport.CreateOwner:= self;
  ABPubFrxReport.DataSource:= DataSource2;
  ABPubFrxReport.ReportType:= rtFunc;
  ABPubFrxReport.PreviewReport(ABGetSQLValue('Main','select top 1 Fr_Guid from ABSys_Org_FuncReport',[],''));
end;

procedure TABTestForm.btn9Click(Sender: TObject);
begin
  ABPubFrxReport.Init;
  ABPubFrxReport.DefaultPrint:=ComboBox2.Text;
  ABPubFrxReport.CreateOwner:= self;
  ABPubFrxReport.DataSource:= DataSource2;
  ABPubFrxReport.ReportType:= rtFunc;
  ABPubFrxReport.PrintReport(ABGetSQLValue('Main','select top 1 Fr_Guid from ABSys_Org_FuncReport',[],''));
end;

procedure TABTestForm.btn14Click(Sender: TObject);
begin
  ABGetDownManagerForm.ShowModal;
end;

procedure TABTestForm.FormCreate(Sender: TObject);
begin
  ABQuery2.FuncGuid:=ABGetFuncFieldValueByFuncFileName('ABClientP.exe','Fu_Guid');
  FpriDatetime:=Now;
  ABInitFormDataSet([ABDBPanel1],[ABcxGrid1ABcxGridDBBandedTableView1],ABQuery2,True,false);
  ABQuery3.Close;
  ABQuery3.open;

  ComboBox2.Text:=EmptyStr;
  ComboBox2.Items.Assign(printers.printer.printers);
end;


end.








