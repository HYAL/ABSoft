{
框架操作传入SQL数据单元
}
unit ABFramkWorkOperatorDatasetU;

interface
{$I ..\ABInclude.ini}


uses
  ABPubFormU,
  ABPubFuncU,
  ABPubConstU,
  ABPubUserU,
  ABPubDBU,
  ABPubPanelU,

  ABThirdQueryU,
  ABThirdCustomQueryU,

  ABFramkWorkConstU,
  ABFramkWorkControlU,
  ABFramkWorkcxGridU,
  ABFramkWorkQueryAllFieldComboxU,
  ABFramkWorkDictionaryQueryU,
  ABFramkWorkUserU,
  ABFramkWorkFuncU,
  ABFramkWorkDBPanelU,
  ABFramkWorkQueryU,
  ABFramkWorkDBNavigatorU,

  cxGridLevel,

  types,
  SysUtils,Variants,Classes,Controls,Forms,DB,StdCtrls,ExtCtrls,
  cxCustomData,
  cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGrid, cxPC,
  cxSplitter, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxBarBuiltInMenu, Vcl.Menus, cxButtons,
  ABPubMultilingualDBNavigatorU, cxContainer, cxEdit, cxGroupBox, cxRadioGroup;

type
  TABOperatorDatasetForm = class(TABPubForm)
    ParentPageControl: TABcxPageControl;
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TABcxButton;
    Button2: TABcxButton;
    ABcxRadioGroup1: TABcxRadioGroup;
    procedure Button1Click(Sender: TObject);
    procedure ParentPageControlChange(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ABcxRadioGroup1Click(Sender: TObject);
  private
    FFilterCaptions:TStringDynArray;
    FFilterWheres:TStringDynArray;
    FDefaultFilterIndex:LongInt;
    { Private declarations }
  public
    { Public declarations }
  end;

//自动创建多页窗体数据集的页
//aPageControl=多页PageControl
//aCaption=显示名称
//aNameFlag=名称标志

//aConnName=数据连接名
//aSQL=数据SQL
//aInitTableName=初始化表名

//aOperateTypes=数据集操作类型
//aSheetMainDetailType=Sheet主从类型
//aDetailPageControlOnChange=从PageControl切换事件
procedure ABCreatePageControlSheet(
                             aPageControl:TABcxPageControl;
                             aCaption:string;
                             aNameFlag:string;

                             aConnName,
                             aSQL:string;
                             aInitTableName:string;

                             aDatasetOperateTypes:TABDatasetOperateTypes;
                             aSheetMainDetailType:TABCreateMainDetailTableType;
                             aDetailPageControlOnChange:TNotifyEvent
                             );
//模板功能中窗体中的初始化
procedure ABInitFormTemplate(aForm:TForm;aPageControl:TABcxPageControl;aDetailPageControlChange:TNotifyEvent);

//自动创建的多页窗体数据集切换
procedure ABPageControlChange(aForm:TForm;aMainIndex,aDetailIndex:longint);
//操作传入SQL的数据
procedure ABShowDataset(aName:string;                           //主调名,不同的主调不一样

                           aMainNames:array of string;             //主SQL名数组，一个Sheet名对应一个PageControl页面
                           aMainConnNames:array of string;         //主SQL连接名数组
                           aMainSQLs:array of string;              //主SQL数组
                           aMainInitTables:array of string;        //主SQL初始化表名数组

                           aDetailNames:TTWOString;                //从SQL名数组,一个主表可以有多个从表
                           aDetailConnNames:TTWOString;            //从SQL连接名数组
                           aDetailSQLs:TTWOString;                 //从SQL数组
                           aDetailInitTables:TTWOString;           //从SQL初始化表名数组

                           aDetailMasterFields:TTWOString;         //从SQL连接主字段
                           aDetailDetailFields:TTWOString;         //从SQL连接从字段

                           aFilterCaptions:array of string;        //过滤显示名数组
                           aFilterWheres:array of string;          //过滤条件数组
                           aDefaultFilterIndex:LongInt=0           //默认过滤索引号
                           );
function ABOperatorDataset(aName:string;                           //主调名,不同的主调不一样

                           aMainNames:array of string;             //主SQL名数组，一个Sheet名对应一个PageControl页面
                           aMainConnNames:array of string;         //主SQL连接名数组
                           aMainSQLs:array of string;              //主SQL数组
                           aMainInitTables:array of string;        //主SQL初始化表名数组

                           aDetailNames:TTWOString;                //从SQL名数组,一个主表可以有多个从表
                           aDetailConnNames:TTWOString;            //从SQL连接名数组
                           aDetailSQLs:TTWOString;                 //从SQL数组
                           aDetailInitTables:TTWOString;           //从SQL初始化表名数组

                           aDetailMasterFields:TTWOString;         //从SQL连接主字段
                           aDetailDetailFields:TTWOString;         //从SQL连接从字段

                           aFilterCaptions:array of string;        //过滤显示名数组
                           aFilterWheres:array of string;          //过滤条件数组
                           aDefaultFilterIndex:LongInt=0;          //默认过滤索引号

                           aDatasetOperateTypes:TABDatasetOperateTypes=[dtBrowse] //操作的类型
                           ):TABOperatorDatasetForm;


implementation

{$R *.dfm}

procedure ABShowDataset(aName:string;

                           aMainNames:array of string;
                           aMainConnNames:array of string;
                           aMainSQLs:array of string;
                           aMainInitTables:array of string;

                           aDetailNames:TTWOString;
                           aDetailConnNames:TTWOString;
                           aDetailSQLs:TTWOString;
                           aDetailInitTables:TTWOString;

                           aDetailMasterFields:TTWOString;
                           aDetailDetailFields:TTWOString;

                           aFilterCaptions:array of string;
                           aFilterWheres:array of string;
                           aDefaultFilterIndex:LongInt
                           );
var
  tempForm:TABOperatorDatasetForm;
begin
  tempForm:=ABOperatorDataset( aName,

                               aMainNames,
                               aMainConnNames,
                               aMainSQLs,
                               aMainInitTables,

                               aDetailNames,
                               aDetailConnNames,
                               aDetailSQLs,
                               aDetailInitTables,

                               aDetailMasterFields,
                               aDetailDetailFields,

                               aFilterCaptions,
                               aFilterWheres,
                               aDefaultFilterIndex
                               );
  try
    tempForm.ShowModal;
  finally
    tempForm.Free;
  end;
end;

function ABOperatorDataset(aName:string;

                           aMainNames:array of string;
                           aMainConnNames:array of string;
                           aMainSQLs:array of string;
                           aMainInitTables:array of string;

                           aDetailNames:TTWOString;
                           aDetailConnNames:TTWOString;
                           aDetailSQLs:TTWOString;
                           aDetailInitTables:TTWOString;

                           aDetailMasterFields:TTWOString;
                           aDetailDetailFields:TTWOString;

                           aFilterCaptions:array of string;
                           aFilterWheres:array of string;
                           aDefaultFilterIndex:LongInt;

                           aDatasetOperateTypes:TABDatasetOperateTypes
                           ):TABOperatorDatasetForm;
var
  i,j:LongInt;

  tempDataSet:TABDictionaryQuery;
  tempDatasource:TDataSource;
  tempPageControl: TABcxPageControl;
  tempName,tempValue:string;
  tempHaveDetail:Boolean;
begin
  Result := TABOperatorDatasetForm.Create(nil);
  if aName<>EmptyStr then
  begin
    Result.Name:='temp'+ ABStrToName(aName);
    Result.Caption:= aName;
  end;
  Result.Panel2.Visible:=(dtselect in aDatasetOperateTypes);
  Result.ABcxRadioGroup1.Visible:=(High(aFilterCaptions)-Low(aFilterCaptions)>=0);
  Result.Panel1.Visible:=((Result.Panel2.Visible) and (Result.ABcxRadioGroup1.Visible));

  if Result.ABcxRadioGroup1.Visible then
  begin
    ABCopyStringArray(aFilterCaptions,Result.FFilterCaptions);
    ABCopyStringArray(aFilterWheres,Result.FFilterWheres);
    Result.FDefaultFilterIndex:=aDefaultFilterIndex;

    Result.ABcxRadioGroup1.Properties.Items.BeginUpdate;
    Result.ABcxRadioGroup1.Properties.BeginUpdate;
    Result.ABcxRadioGroup1.Properties.Columns:=(High(aFilterCaptions)-Low(aFilterCaptions)+1);

    Result.ABcxRadioGroup1.Properties.Items.Clear;
    for I := Low(aFilterCaptions) to High(aFilterCaptions) do
    begin
      Result.ABcxRadioGroup1.Properties.Items.Add;
      Result.ABcxRadioGroup1.Properties.Items[Result.ABcxRadioGroup1.Properties.Items.Count-1].Caption:=aFilterCaptions[i];
    end;
    Result.ABcxRadioGroup1.Properties.EndUpdate;
    Result.ABcxRadioGroup1.Properties.Items.EndUpdate;

    Result.ABcxRadioGroup1.ItemIndex:=aDefaultFilterIndex;
  end;

  //循环对象名称数组创建各元素下的控件
  for I := Low(aMainNames) to High(aMainNames) do
  begin
    if Pos('=',aMainNames[i])>0 then
    begin
      tempName:=ABGetLeftRightStr(aMainNames[i],axdLeft);
      tempValue:=ABGetLeftRightStr(aMainNames[i],axdRight);
    end
    else
    begin
      tempName:=aMainNames[i];
      tempValue:=aMainNames[i];
    end;

    tempHaveDetail:= (i<=High(aDetailNames)-low(aDetailNames)) and
                     (High(aDetailNames[i])-low(aDetailNames[i])>=0);
    //创建对象名称下的主从表控件
    ABCreatePageControlSheet(
                      Result.ParentPageControl,
                      tempValue,
                      inttostr(i+1),

                      aMainConnNames[i],
                      aMainSQLs[i],
                      aMainInitTables[i],

                      aDatasetOperateTypes,
                      ABIIF(tempHaveDetail,ctMainOfHaveDetail,ctMain),
                      Result.ParentPageControlChange
                      );

    if tempHaveDetail then
    begin
      tempDataSource:=TDataSource(Result.FindComponent('ABDataSource'+inttostr(i+1)));
      tempPageControl:=TABcxPageControl(Result.FindComponent('ABPageControl'+inttostr(i+1)));
      for j := low(aDetailNames[i]) to High(aDetailNames[i]) do
      begin
        ABCreatePageControlSheet(
                          tempPageControl,
                          aDetailNames[i][j],
                          inttostr(i+1)+'_'+inttostr(j+1),

                          aDetailConnNames[i][j],
                          aDetailSQLs[i][j],
                          aDetailInitTables[i][j],

                          aDatasetOperateTypes,
                          ctDetail,
                          Result.ParentPageControlChange
                          );
        tempDataSet:=TABDictionaryQuery(Result.FindComponent('ABQuery'+inttostr(i+1)+'_'+inttostr(j+1)));

        TABThirdReadDataQuery(tempDataSet).MasterFields:=aDetailMasterFields[i][j];
        TABThirdReadDataQuery(tempDataSet).DetailFields:=aDetailDetailFields[i][j];
        TABThirdReadDataQuery(tempDataSet).MasterSource:=tempDataSource;
      end;
      tempPageControl.HideTabs:=(tempPageControl.PageCount<=1);
    end;
  end;
  Result.ParentPageControl.HideTabs:=(Result.ParentPageControl.PageCount<=1);
  Result.ParentPageControlChange(Result.ParentPageControl);
end;

procedure ABCreatePageControlSheet(
                             aPageControl:TABcxPageControl;
                             aCaption:string;
                             aNameFlag:string;

                             aConnName,
                             aSQL:string;
                             aInitTableName:string;

                             aDatasetOperateTypes:TABDatasetOperateTypes;
                             aSheetMainDetailType:TABCreateMainDetailTableType;
                             aDetailPageControlOnChange:TNotifyEvent
                              );
var
  tempDataSet:TABQuery;
  tempDatasource:TABDataSource;

  tempTabSheet : TcxTabSheet;
  tempNavigator: TABDBNavigator;
  tempGrid: TABcxGrid;
  tempPanel,tempPanel1:TPanel;
  tempABDBPanel:TABDBPanel;
  tempTableView: TABcxGridDBBandedTableView;
  tempLevel: TcxGridLevel;
  tempPageControl: TABcxPageControl;
  tempSplitter: TABcxSplitter;
  tempOnChange: TNotifyEvent;
begin
  tempDataSet:=TABQuery.Create(aPageControl.Owner);
  tempDataSet.Name:='ABQuery'+aNameFlag;
  tempDataSet.MultiLevelCaption:=aCaption;
  if (not (dtDelete in aDatasetOperateTypes)) and
     (not (dtAppend in aDatasetOperateTypes)) and
     (not (dtEdit in aDatasetOperateTypes)) then
  begin
    tempDataSet.UpdateOptions.ReadOnly:=true;
  end;
  tempDataSet.ConnName:=aConnName;
  tempDataSet.Sql.Text:=aSQL;
  if aInitTableName<>EmptyStr then
    ABStrsToStrings(aInitTableName,',',tempDataSet.LoadTables,True,false);

  tempDatasource:=TABDataSource.Create(aPageControl.Owner);
  tempDatasource.DataSet:=tempDataSet;
  tempDatasource.Name:='ABDataSource'+aNameFlag;

  tempOnChange:=aPageControl.OnChange;
  aPageControl.OnChange:=nil;
  try
    tempTabSheet := TcxTabSheet.Create(aPageControl.Owner);
    tempTabSheet.Name:='ABTabSheet'+aNameFlag;
    tempTabSheet.PageControl:= aPageControl;
    tempTabSheet.Caption :=aCaption ;
  finally
    aPageControl.OnChange:=tempOnChange;
  end;

  tempNavigator:= TABDBNavigator.Create(aPageControl.Owner);
  tempNavigator.VisibleButtons := [nbInsert, nbCopy,nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel];
  tempNavigator.Name:='ABNavigator'+aNameFlag;
  tempNavigator.Parent         := tempTabSheet ;
  tempNavigator.Align          := alTop ;
  tempNavigator.TabOrder       := 0 ;
  tempNavigator.DataSource     := tempDatasource;

  tempPanel:= TPanel.Create(aPageControl.Owner);
  tempPanel.Parent         := tempTabSheet ;
  tempPanel.Align          := alClient ;
  tempPanel.BevelOuter     := bvNone ;
  tempPanel.Caption        := EmptyStr ;

  tempGrid       := TABcxGrid.Create(aPageControl.Owner);
  tempGrid.Name:='ABGrid'+aNameFlag;
  tempGrid.Parent:= tempPanel ;
  tempGrid.TabOrder:=1;

  tempTableView     := TABcxGridDBBandedTableView(tempGrid.CreateView(TABcxGridDBBandedTableView));
  tempTableView.Name:='ABTableView'+aNameFlag;
  tempLevel         := tempGrid.Levels.Add;
  tempLevel.Name    :='ABLevel'+aNameFlag;
  tempLevel.GridView:=tempTableView;
  tempTableView.OptionsView.BandHeaders:=False;
  tempTableView.OptionsView.GroupByBox:=False;
  tempTableView.OptionsView.Indicator:=true;
  tempTableView.ExtPopupMenu.SetupFileNameSuffix:=TForm(aPageControl.Owner).Caption;
  tempTableView.Bands.Add;
  tempTableView.DataController.DataSource   :=  tempDatasource;
  if (dtAppend in aDatasetOperateTypes) then
  begin
    tempNavigator.VisibleButtons :=tempNavigator.VisibleButtons+ [nbInsertSpacer,nbInsert,nbCopy];
    tempTableView.OptionsData.Appending :=true;
    tempTableView.OptionsData.Inserting :=true;
  end
  else
  begin
    tempNavigator.VisibleButtons :=tempNavigator.VisibleButtons- [nbInsertSpacer,nbInsert,nbCopy];
    tempTableView.OptionsData.Appending :=false;
    tempTableView.OptionsData.Inserting :=false;
  end;
  if (dtEdit in aDatasetOperateTypes) then
  begin
    tempNavigator.VisibleButtons :=tempNavigator.VisibleButtons+ [nbEdit];
    tempTableView.OptionsData.Editing :=true;
  end
  else
  begin
    tempNavigator.VisibleButtons :=tempNavigator.VisibleButtons- [nbEdit];
    tempTableView.OptionsData.Editing :=false;
  end;
  if (dtDelete in aDatasetOperateTypes) then
  begin
    tempNavigator.VisibleButtons :=tempNavigator.VisibleButtons+ [nbDelete];
    tempTableView.OptionsData.Deleting :=true;
  end
  else
  begin
    tempNavigator.VisibleButtons :=tempNavigator.VisibleButtons- [nbDelete];
    tempTableView.OptionsData.Deleting :=false;
  end;
  if ( dtAppend in aDatasetOperateTypes) or
     ( dtEdit in aDatasetOperateTypes)  then
    tempNavigator.VisibleButtons:=tempNavigator.VisibleButtons+[nbPostSpacer,nbPost,nbCancel]
  else if not (dtDelete in aDatasetOperateTypes) then
  begin
    tempNavigator.VisibleButtons:=tempNavigator.VisibleButtons-[nbPostSpacer,nbPost,nbCancel];
  end;

  tempNavigator.Visible :=not (tempNavigator.VisibleButtons=[]);
  if (dtselect in aDatasetOperateTypes) then
  begin
    tempTableView.OptionsSelection.CellSelect  :=not (dtselect in aDatasetOperateTypes);
    tempTableView.OptionsSelection.MultiSelect :=(dtselect in aDatasetOperateTypes);
  end;

  tempGrid.Align:=alClient;
  if (aSheetMainDetailType=ctMainOfHaveDetail)  then
  begin
    tempGrid.Align:=alLeft;
    tempNavigator.VisibleButtons:=tempNavigator.VisibleButtons+[nbQuerySpacer,nbQuery,nbReport];

    tempSplitter   := TABcxSplitter.Create(aPageControl.Owner);
    tempSplitter.Control:= tempGrid ;
    tempSplitter.Parent:= tempPanel ;
    tempSplitter.Name:='ABSplitter'+aNameFlag;
    tempSplitter.HotZoneClassName := 'TcxMediaPlayer8Style' ;
    tempSplitter.AlignSplitter := salLeft ;
    tempSplitter.Left:=tempGrid.Left+tempGrid.Width;

    tempPanel1:= TPanel.Create(aPageControl.Owner);
    tempPanel1.Parent         := tempPanel ;
    tempPanel1.Align          := alClient ;
    tempPanel1.BevelOuter     := bvNone ;
    tempPanel1.Caption        := EmptyStr ;

    tempABDBPanel:= TABDBPanel.Create(aPageControl.Owner);
    tempABDBPanel.Parent         := tempPanel1 ;
    tempABDBPanel.Name           :='ABDBPanel'+aNameFlag;
    tempABDBPanel.Align          := alTop ;
    tempABDBPanel.BevelOuter     := bvNone ;
    tempABDBPanel.Caption        := EmptyStr ;
    tempABDBPanel.DataSource     := tempDatasource ;

    tempPageControl:= TABcxPageControl.Create(aPageControl.Owner);
    tempPageControl.Parent:= tempPanel1 ;
    tempPageControl.Name:='ABPageControl'+aNameFlag;
    tempPageControl.Align:=alClient;
    tempPageControl.TabOrder:=2;
    tempPageControl.OnChange:=aDetailPageControlOnChange;
  end;
end;

procedure ABInitFormTemplate(aForm:TForm;aPageControl:TABcxPageControl;aDetailPageControlChange:TNotifyEvent);
var
  i,ii,tempCount1,tempCount11:LongInt;
  tempDataset:TDataSet;
  tempDataset_Func:TDataSet;
  tempStrings: TStrings;
  tempConnName:string;
  tempSql:string;
  tempQuery:TABQuery;
  tempMainDataSource:TDataSource;
  tempPageControl: TABcxPageControl;
begin
  tempDataset_Func:=ABGetConstSqlPubDataset('ABSys_Org_Function',[ABGetBPLFileName(aForm)]);
  tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_FuncSQL',[aForm.Name],TABInsideQuery);
  if (Assigned(tempDataset)) and
     (Assigned(tempDataset_Func)) then
  begin
    if AnsiCompareText(tempDataset_Func.FindField('FU_TemplateName').AsString,'ABBaseTableTemplateG.bpl')=0 then
    begin
      if (tempDataset.Locate('FS_Name','ABQuery1',[])) then
      begin
        TABQuery(aForm.FindComponent('ABQuery1')).ConnName:=ABGetConnNameByConnGuid(tempDataset.FieldByName('FS_ConnName').AsString);
        TABQuery(aForm.FindComponent('ABQuery1')).SQL.Text:=tempDataset.FieldByName('FS_Sql').AsString;
      end;
    end
    else if AnsiCompareText(tempDataset_Func.FindField('FU_TemplateName').AsString,'ABMainDetailTableTemplateG.bpl')=0 then
    begin
      tempQuery:=TABQuery(aForm.FindComponent('ABQuery1'));
      tempMainDataSource:=TABDataSource(aForm.FindComponent('ABDataSource1'));
      if (Assigned(tempQuery)) and
         (Assigned(tempMainDataSource)) then
      begin
        if (tempDataset.Locate('FS_Name','ABQuery1',[])) then
        begin
          tempQuery.ConnName:=ABGetConnNameByConnGuid(tempDataset.FieldByName('FS_ConnName').AsString);
          tempQuery.SQL.Text:=tempDataset.FieldByName('FS_Sql').AsString;
        end;

        tempStrings:= TStringList.Create;
        try
          tempStrings.Text:=tempDataset_Func.FindField('FU_TemplateReMark').AsString;
          tempCount1:=StrToIntDef(ABReadIniInStrings('Setup','DetailCount',tempStrings),1);
          for I := 1 to tempCount1 do
          begin
            tempSql:=emptystr;
            if (tempDataset.Locate('FS_Name','ABQuery1_'+inttostr(i),[])) then
            begin
              tempConnName:=ABGetConnNameByConnGuid(tempDataset.FieldByName('FS_ConnName').AsString);
              tempSql:=tempDataset.FieldByName('FS_Sql').AsString;
            end;
            //创建对象名称下的主从表控件
            ABCreatePageControlSheet(
                              aPageControl,
                              ABReadIniInStrings('Detail'+inttostr(i),'Caption',tempStrings),
                              '1_'+inttostr(i),

                              tempConnName,
                              tempSql,
                              '',

                              [dtAppend,dtEdit,dtDelete],
                              ctDetail,
                              aDetailPageControlChange);

            tempQuery:=TABQuery(aForm.FindComponent('ABQuery1_'+inttostr(i)));
            tempQuery.DataSource:=tempMainDataSource;
            tempQuery.MasterFields:=ABReadIniInStrings('Detail'+inttostr(i),'MasterFields',tempStrings);
            tempQuery.DetailFields:=ABReadIniInStrings('Detail'+inttostr(i),'DetailFields',tempStrings);
          end;
          aPageControl.HideTabs:=(aPageControl.PageCount<=1);
          aPageControl.ActivePageIndex:=0;
        finally
          tempStrings.Free;
        end;
      end;
    end
    else if AnsiCompareText(tempDataset_Func.FindField('FU_TemplateName').AsString,'ABMultiMainDetailTableTemplateG.bpl')=0 then
    begin
      tempStrings:= TStringList.Create;
      try
        tempStrings.Text:=tempDataset_Func.FindField('FU_TemplateReMark').AsString;
        tempCount11:=StrToIntDef(ABReadIniInStrings('Setup','MainCount',tempStrings),1);
        for ii := 1 to tempCount11 do
        begin
          tempSql:=emptystr;
          if (tempDataset.Locate('FS_Name','ABQuery'+inttostr(ii),[])) then
          begin
            tempConnName:=ABGetConnNameByConnGuid(tempDataset.FieldByName('FS_ConnName').AsString);
            tempSql:=tempDataset.FieldByName('FS_Sql').AsString;
          end;
          ABCreatePageControlSheet(
                            aPageControl,
                            ABReadIniInStrings('Main'+inttostr(ii),'Caption',tempStrings),
                            inttostr(ii),

                            tempConnName,
                            tempSql,
                              '',

                            [dtAppend,dtEdit,dtDelete],
                            ctMainOfHaveDetail ,
                            aDetailPageControlChange);
          tempPageControl:=TABcxPageControl(aForm.FindComponent('ABPageControl'+inttostr(ii)));
          tempMainDataSource:=TABDataSource(aForm.FindComponent('ABDataSource'+inttostr(ii)));
          if (Assigned(tempMainDataSource)) and
             (Assigned(tempPageControl)) then
          begin
            try
              tempCount1:=StrToIntDef(ABReadIniInStrings('Main'+inttostr(ii),'DetailCount',tempStrings),1);
              for I := 1 to tempCount1 do
              begin
                tempSql:=emptystr;
                if (tempDataset.Locate('FS_Name','ABQuery'+inttostr(ii)+'_'+inttostr(i),[])) then
                begin
                  tempConnName:=ABGetConnNameByConnGuid(tempDataset.FieldByName('FS_ConnName').AsString);
                  tempSql:=tempDataset.FieldByName('FS_Sql').AsString;
                end;
                //创建对象名称下的主从表控件
                ABCreatePageControlSheet(
                                  tempPageControl,
                                  ABReadIniInStrings('Main'+inttostr(ii)+'Detail'+inttostr(i),'Caption',tempStrings),
                                  inttostr(ii)+'_'+inttostr(i),

                                  tempConnName,
                                  tempSql,
                                  '',

                                  [dtAppend,dtEdit,dtDelete],
                                  ctDetail,
                                  aDetailPageControlChange );

                tempQuery:=TABQuery(aForm.FindComponent('ABQuery'+inttostr(ii)+'_'+inttostr(i)));
                tempQuery.DataSource:=tempMainDataSource;
                tempQuery.MasterFields:=ABReadIniInStrings('Main'+inttostr(ii)+'Detail'+inttostr(i),'MasterFields',tempStrings);
                tempQuery.DetailFields:=ABReadIniInStrings('Main'+inttostr(ii)+'Detail'+inttostr(i),'DetailFields',tempStrings);
              end;
              tempPageControl.HideTabs:=(tempPageControl.PageCount<=1);
              tempPageControl.ActivePageIndex:=0;
            finally
            end;
          end;
        end;
        aPageControl.HideTabs:=(aPageControl.PageCount<=1);
        aPageControl.ActivePageIndex:=0;
      finally
        tempStrings.Free;
      end;
    end;
  end;
end;
procedure ABPageControlChange(aForm:TForm;aMainIndex,aDetailIndex:longint);
var
  tempDataSource:TDataSource;
  tempQuery:TABQuery;
  tempTableView: TABcxGridDBBandedTableView;
  tempDBPanel: TABDBPanel;
  tempMainFlag,
  tempDetailFlag:string;
begin
  tempMainFlag:=inttostr(aMainIndex+1);
  tempDataSource:=TDataSource(aForm.FindComponent('ABDataSource'+tempMainFlag));
  if (Assigned(tempDataSource)) and
     (Assigned(tempDataSource.DataSet)) then
  begin
    tempQuery:= TABQuery(tempDataSource.DataSet);
    if not tempQuery.Active then
    begin
      tempTableView :=TABcxGridDBBandedTableView(aForm.FindComponent('ABTableView'+tempMainFlag));
      tempDBPanel :=TABDBPanel(aForm.FindComponent('ABDBPanel'+tempMainFlag));
      ABInitFormDataSet([tempDBPanel],[tempTableView],tempQuery,True,False);
    end;

    tempDetailFlag:=tempMainFlag+'_'+inttostr(aDetailIndex+1);
    tempDataSource:=TDataSource(aForm.FindComponent('ABDataSource'+tempDetailFlag));
    if (Assigned(tempDataSource)) and
       (Assigned(tempDataSource.DataSet)) then
    begin
      tempQuery:= TABQuery(tempDataSource.DataSet);
      if not tempQuery.Active then
      begin
        tempTableView :=TABcxGridDBBandedTableView(aForm.FindComponent('ABTableView'+tempDetailFlag));
        tempDBPanel :=TABDBPanel(aForm.FindComponent('ABDBPanel'+tempDetailFlag));
        ABInitFormDataSet([tempDBPanel],[tempTableView],tempQuery,True,False);
      end;
    end;
  end;
end;

//对象名称切换时进行数据集的刷新
procedure TABOperatorDatasetForm.ParentPageControlChange(Sender: TObject);
var
  tempPageControl: TABcxPageControl;
begin
  tempPageControl:=TABcxPageControl(FindComponent('ABPageControl'+inttostr(ParentPageControl.ActivePageIndex+1)));
  if (Assigned(tempPageControl)) then
    ABPageControlChange(self,ParentPageControl.ActivePageIndex,ABIIF(Assigned(tempPageControl),tempPageControl.ActivePageIndex,0));
end;

procedure TABOperatorDatasetForm.ABcxRadioGroup1Click(Sender: TObject);
var
  tempDataSource:TDataSource;
  tempQuery:TABQuery;
begin
  if ABcxRadioGroup1.ItemIndex>=0 then
  begin
    tempDataSource:=TDataSource(FindComponent('ABDataSource'+inttostr(ParentPageControl.ActivePageIndex+1)));
    if (Assigned(tempDataSource)) and
       (Assigned(tempDataSource.DataSet)) then
    begin
      tempQuery:= TABQuery(tempDataSource.DataSet);
      if not tempQuery.Active then
      begin
        tempQuery.Filter:=FFilterWheres[ABcxRadioGroup1.ItemIndex];
      end;
    end;
  end;
end;

procedure TABOperatorDatasetForm.Button1Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TABOperatorDatasetForm.Button2Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

end.




