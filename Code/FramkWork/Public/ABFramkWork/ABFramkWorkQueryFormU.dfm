object ABQueryForm: TABQueryForm
  Left = 795
  Top = 246
  Caption = #25968#25454#26597#35810'('#24320#22987#19982#32467#26463#30340'*'#34920#31034#20219#24847#23383#31526')'
  ClientHeight = 314
  ClientWidth = 403
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 273
    Width = 403
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 134
    DesignSize = (
      403
      41)
    object Button1: TABcxButton
      Left = 305
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #21462#28040
      LookAndFeel.Kind = lfFlat
      TabOrder = 1
      OnClick = Button1Click
      ShowProgressBar = False
    end
    object Button2: TABcxButton
      Left = 225
      Top = 8
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #30830#23450
      Default = True
      LookAndFeel.Kind = lfFlat
      TabOrder = 0
      OnClick = Button2Click
      ShowProgressBar = False
    end
    object Button3: TABcxButton
      Left = 15
      Top = 8
      Width = 88
      Height = 25
      Caption = #23548#20837#26597#35810
      DropDownMenu = PopupMenu1
      Kind = cxbkDropDownButton
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
      TabOrder = 2
      OnClick = Button3Click
      ShowProgressBar = False
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 403
    Height = 273
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 134
  end
  object PopupMenu1: TPopupMenu
    Left = 263
    Top = 49
  end
  object DataSource1: TDataSource
    Left = 336
    Top = 88
  end
end
