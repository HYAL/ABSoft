object ABDownManagerForm: TABDownManagerForm
  Left = 101
  Top = 77
  Caption = #19979#25289#31649#29702
  ClientHeight = 550
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ABcxPageControl1: TcxPageControl
    Left = 0
    Top = 0
    Width = 750
    Height = 550
    Align = alClient
    TabOrder = 0
    Properties.ActivePage = cxTabSheet2
    Properties.CustomButtons.Buttons = <>
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = False
    ClientRectBottom = 549
    ClientRectLeft = 1
    ClientRectRight = 749
    ClientRectTop = 21
    object cxTabSheet1: TcxTabSheet
      Caption = #26641#24418#19979#25289
      ImageIndex = 0
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object PageControl2: TPageControl
        Left = 0
        Top = 0
        Width = 748
        Height = 528
        Align = alClient
        TabOrder = 0
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = #21015#34920#19979#25289
      ImageIndex = 1
      object Splitter1: TSplitter
        Left = 297
        Top = 0
        Height = 528
        ExplicitLeft = 189
      end
      object ListBox1: TListBox
        Left = 0
        Top = 0
        Width = 297
        Height = 528
        Align = alLeft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ImeName = #24555#20048#20116#31508
        ItemHeight = 14
        ParentFont = False
        TabOrder = 0
        OnClick = ListBox1Click
      end
      object Panel1: TPanel
        Left = 300
        Top = 0
        Width = 448
        Height = 528
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Panel1'
        TabOrder = 1
        object Panel2: TPanel
          Left = 0
          Top = 503
          Width = 448
          Height = 25
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 0
          DesignSize = (
            448
            25)
          object ABcxLabel1: TcxLabel
            Left = 0
            Top = 3
            AutoSize = False
            Caption = 'KeyFieldNames:'
            Transparent = True
            Height = 19
            Width = 81
          end
          object ABcxTextEdit1: TcxTextEdit
            Left = 87
            Top = 3
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 1
            Text = 'ABcxTextEdit1'
            Width = 278
          end
          object ABcxButton1: TcxButton
            Left = 473
            Top = 3
            Width = 80
            Height = 20
            Caption = #35774#32622
            LookAndFeel.Kind = lfFlat
            TabOrder = 2
            OnClick = ABcxButton1Click
          end
        end
        object Sheet1_Grid1: TcxGrid
          Left = 0
          Top = 0
          Width = 448
          Height = 503
          Align = alClient
          TabOrder = 1
          LookAndFeel.Kind = lfFlat
          LookAndFeel.NativeStyle = False
          ExplicitWidth = 556
          object Sheet1_Level1: TcxGridLevel
          end
        end
      end
    end
  end
  object ABcxGridPopupMenu1: TABcxGridPopupMenu
    AutoHotkeys = maManual
    CloseFootStr = False
    AutoApplyBestFit = True
    AutoCreateAllItem = True
    Left = 392
    Top = 240
  end
end
