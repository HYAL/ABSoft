{
框架记录浏览条单元
}
unit ABFramkWorkDBNavigatorU;

interface
{$I ..\ABInclude.ini}


uses
  ABPubPanelU,
  ABPubConstU,
  ABPubSelectStrU,
  ABPubMemoU,
  ABPubShowEditFieldValueU,
  ABPubFuncU,
  ABPubMessageU,
  ABPubInPutStrU,
  abpubuserU,
  ABPubFrameU,
  ABPubDBU,
  ABPubSelectComboBoxU,
  ABPubSelectListBoxU,
  ABPubLogU,
  ABPubFormU,

  ABThirdConnU,
  ABThirdCustomQueryU,
  ABThirdImportDataU,
  ABThirdQueryU,
  ABThirdDBU,

  ABFramkWorkDictionaryQueryU,
  ABFramkWorkFuncU,
  ABFramkWorkcxGridU,
  ABFramkWorkFastReportU,
  ABFramkWorkConstU,
  ABFramkWorkDBPanelU,
  ABFramkWorkControlU,
  ABFramkWorkApproveU,

  TypInfo,

  printers,
  cxpC,
  cxGrid,
  cxGridTableView,

  Windows,
  Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,ExtCtrls,DB,
  ImgList, Vcl.Buttons,
  Vcl.Menus, Vcl.StdCtrls, cxButtons,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters;

type
  TABDBNavigatorButton=(
    nbFirstRecord           ,
    nbPreviousRecord        ,
    nbNextRecord            ,
    nbLastRecord            ,

    nbInsertSpacer          ,
    nbInsert                ,
    nbCopy                  ,
    nbDelete                ,
    nbEdit                  ,

    nbPostSpacer            ,
    nbPost                  ,
    nbCancel                ,

    nbQuerySpacer           ,
    nbQuery                 ,
    nbReport               ,

    nbCustomSpacer         ,
    nbCustom1            ,
    nbCustom2            ,
    nbCustom3            ,
    nbCustom4            ,
    nbCustom5            ,
    nbCustom6            ,
    nbCustom7            ,
    nbCustom8            ,
    nbCustom9            ,
    nbCustom10           ,

    nbExitSpacer         ,
    nbExit               ,
    nbNull
    );
  TABDBNavigatorButtonSet = set of TABDBNavigatorButton;
  TABDBNavigator = class;
  TABChickBtnEvent = procedure(Sender: TObject;aButtonType:TABDBNavigatorButton;var aContinue:Boolean) of object;
  TABUpdateButtonsEvent = procedure(Sender: TObject;var aButtonTypes:TABDBNavigatorButtonSet) of object;

  TABButtonControlType=(ctSingle,ctFromMain);
  TABButtonRangeType=(RtMain,RtDetail,RtReportAndExit,RtQueryAndPriNext,RtMainPage,RtAll,RtCustom);

  TABDBNavigatorFrame = class(TABPubFrame)
    ImageList_16: TImageList;
    ImageList_32: TImageList;
    PopupMenu_BtnInsert: TPopupMenu;
    PopupMenu_BtnEdit: TPopupMenu;
    PopupMenu_BtnReport: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N16: TMenuItem;
    BtnFirstRecord: TABcxButton;
    BtnPreviousRecord: TABcxButton;
    BtnNextRecord: TABcxButton;
    BtnLastRecord: TABcxButton;
    btnInsertSpacer: TBevel;
    btnPostSpacer: TBevel;
    btnQuerySpacer: TBevel;
    btnCustomSpacer: TBevel;
    btnExitSpacer: TBevel;
    BtnInsert: TABcxButton;
    btnCopy: TABcxButton;
    BtnDelete: TABcxButton;
    BtnEdit: TABcxButton;
    BtnPost: TABcxButton;
    BtnCancel: TABcxButton;
    btnQuery: TABcxButton;
    BtnReport: TABcxButton;
    BtnCustom1: TABcxButton;
    BtnCustom2: TABcxButton;
    BtnCustom3: TABcxButton;
    BtnCustom4: TABcxButton;
    BtnCustom5: TABcxButton;
    BtnCustom6: TABcxButton;
    BtnCustom7: TABcxButton;
    BtnCustom8: TABcxButton;
    BtnCustom9: TABcxButton;
    BtnCustom10: TABcxButton;
    BtnExit: TABcxButton;
    N15: TMenuItem;
    N17: TMenuItem;
    procedure N2N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N13N14Click(Sender: TObject);
    procedure N9N10N11Click(Sender: TObject);
    procedure BtnFirstRecordClick(Sender: TObject);
    procedure BtnPreviousRecordClick(Sender: TObject);
    procedure BtnNextRecordClick(Sender: TObject);
    procedure BtnLastRecordClick(Sender: TObject);
    procedure BtnInsertClick(Sender: TObject);
    procedure BtnCopyClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnEditClick(Sender: TObject);
    procedure BtnPostClick(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure btnQueryClick(Sender: TObject);
    procedure BtnReportClick(Sender: TObject);
    procedure BtnExitClick(Sender: TObject);
    procedure BtnCustom2Click(Sender: TObject);
    procedure BtnCustom1Click(Sender: TObject);
    procedure BtnCustom3Click(Sender: TObject);
    procedure BtnCustom4Click(Sender: TObject);
    procedure BtnCustom5Click(Sender: TObject);
    procedure BtnCustom6Click(Sender: TObject);
    procedure BtnCustom7Click(Sender: TObject);
    procedure BtnCustom8Click(Sender: TObject);
    procedure BtnCustom9Click(Sender: TObject);
    procedure BtnCustom10Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N17Click(Sender: TObject);
  private
    { Private declarations }
    FCustomButtons:array[0..9] of TABcxButton;
    FIsContinue:boolean;
    procedure DoAfterClick(aSender: TObject; aButtonType: TABDBNavigatorButton;var aContinue: Boolean);
    procedure DoBeforeClick(aSender: TObject; aButtonType: TABDBNavigatorButton;var aContinue: Boolean);
  public
    constructor Create(AOwner: TComponent); override;
    function IsLinkDataSourceOK: boolean;
    { Public declarations }
  end;
  TABBarDBNavDataLink = class(TDataLink)
  private
    FNavigator: TABDBNavigator;
  protected
    procedure EditingChanged; override;
    procedure DataSetChanged; override;
    procedure RecordChanged(Field: TField); override;
    procedure ActiveChanged; override;
    procedure DataSetScrolled(Distance: Integer); override;
  public
    constructor Create(aNavigator: TABDBNavigator);
  end;

  TABDBNavigator = class(TABCustomPanel)
  private
    FLoaded:Boolean;
    FFuncControlPropertyDataset:Tdataset;

    //关联的数据集连接
    FDataLink : TABBarDBNavDataLink;
    //将窗体中原始的按钮备份一下，在此基础上去应用表权限的设置
    FBackVisibleButtons: TABDBNavigatorButtonSet;

    //报表数据集
    FReportDataset: Tdataset;
    //模板数据集
    FTemplateDataset: Tdataset;

    //以下六项目保存于数据库中
    //增加数据是插入还是追回
    FAddType: TABDatasetAddType;
    //增加数据时显示DBPanel输入数据
    FShowDBPanelInAdd: boolean;
    //修改数据时显示DBPanel输入数据
    FShowDBPanelInEdit: boolean;
    //打印输出方式
    FPrintOutType: TABPrintOutType;
    //打印时默认的打印机
    FDefaultPrint: string;
    //打印数据的范围是当前数据集记录还是所有记录
    FPrintRange: TABPrintRange;

    FAddType_Default          :TABDatasetAddType ;
    FShowDBPanelInAdd_Default :boolean  ;
    FShowDBPanelInEdit_Default:boolean    ;
    FPrintOutType_Default     :TABPrintOutType;
    FDefaultPrint_Default     :string      ;
    FPrintRange_Default       :TABPrintRange    ;
    FTableView                : TcxGridTableView;
  private
    FFrame: TABDBNavigatorFrame;
    FVisibleButtons: TABDBNavigatorButtonSet;
    FOnBeforeClick: TABChickBtnEvent;
    FOnAfterClick: TABChickBtnEvent;
    FButtonControlType: TABButtonControlType;
    FButtonRangeType: TABButtonRangeType;
    FOnUpdateEnabledButtons: TABUpdateButtonsEvent;
    FOnUpdateVisableButtons: TABUpdateButtonsEvent;
    FApprovedCommitButton: TABDBNavigatorButton;
    FApprovedRollbackButton: TABDBNavigatorButton;
    FApproveBill: string;
    FApproveStateField: string;
    FApproveMainGuidField: string;
    procedure SetBigGlyph(const Value: boolean);
    procedure SetImageLayout(const Value: TButtonLayout);
    procedure SetVisibleButtons(const Value: TABDBNavigatorButtonSet);
    function GetBigGlyph: boolean;
    function GetImageLayout: TButtonLayout;
    procedure SetBtnEnabled(aButtons: TABDBNavigatorButtonSet);
    procedure SetDatabasePropertyValue(aPropertyName, aPropertyValue: string);
    function GetDataSource: TDataSource;
    procedure SetDataSource(const Value: TDataSource);
    function GetFuncControlPropertyDataset: Tdataset;
    procedure SetButtonControlType(const Value: TABButtonControlType);
    procedure SetButtonRangeType(const Value: TABButtonRangeType);
    procedure SetBtnCustomInfo;
    function IsCustomButton(aButton: TABcxButton): Boolean;

    function GetBtnCustom10Caption: string;
    function GetBtnCustom10DropDownMenu: TComponent;
    function GetBtnCustom10Kind: TcxButtonKind;
    function GetBtnCustom1Caption: string;
    function GetBtnCustom1DropDownMenu: TComponent;
    function GetBtnCustom1Kind: TcxButtonKind;
    function GetBtnCustom2Caption: string;
    function GetBtnCustom2DropDownMenu: TComponent;
    function GetBtnCustom2Kind: TcxButtonKind;
    function GetBtnCustom3Caption: string;
    function GetBtnCustom3DropDownMenu: TComponent;
    function GetBtnCustom3Kind: TcxButtonKind;
    function GetBtnCustom4Caption: string;
    function GetBtnCustom4DropDownMenu: TComponent;
    function GetBtnCustom4Kind: TcxButtonKind;
    function GetBtnCustom5Caption: string;
    function GetBtnCustom5DropDownMenu: TComponent;
    function GetBtnCustom5Kind: TcxButtonKind;
    function GetBtnCustom6Caption: string;
    function GetBtnCustom6DropDownMenu: TComponent;
    function GetBtnCustom6Kind: TcxButtonKind;
    function GetBtnCustom7Caption: string;
    function GetBtnCustom7DropDownMenu: TComponent;
    function GetBtnCustom7Kind: TcxButtonKind;
    function GetBtnCustom8Caption: string;
    function GetBtnCustom8DropDownMenu: TComponent;
    function GetBtnCustom8Kind: TcxButtonKind;
    function GetBtnCustom9Caption: string;
    function GetBtnCustom9DropDownMenu: TComponent;
    function GetBtnCustom9Kind: TcxButtonKind;
    procedure SetBtnCustom10DropDownMenu(const Value: TComponent);
    procedure SetBtnCustom10Kind(const Value: TcxButtonKind);
    procedure SetBtnCustom1DropDownMenu(const Value: TComponent);
    procedure SetBtnCustom1Kind(const Value: TcxButtonKind);
    procedure SetBtnCustom2DropDownMenu(const Value: TComponent);
    procedure SetBtnCustom2Kind(const Value: TcxButtonKind);
    procedure SetBtnCustom3DropDownMenu(const Value: TComponent);
    procedure SetBtnCustom3Kind(const Value: TcxButtonKind);
    procedure SetBtnCustom4DropDownMenu(const Value: TComponent);
    procedure SetBtnCustom4Kind(const Value: TcxButtonKind);
    procedure SetBtnCustom5DropDownMenu(const Value: TComponent);
    procedure SetBtnCustom5Kind(const Value: TcxButtonKind);
    procedure SetBtnCustom6DropDownMenu(const Value: TComponent);
    procedure SetBtnCustom6Kind(const Value: TcxButtonKind);
    procedure SetBtnCustom7DropDownMenu(const Value: TComponent);
    procedure SetBtnCustom7Kind(const Value: TcxButtonKind);
    procedure SetBtnCustom8DropDownMenu(const Value: TComponent);
    procedure SetBtnCustom8Kind(const Value: TcxButtonKind);
    procedure SetBtnCustom9DropDownMenu(const Value: TComponent);
    procedure SetBtnCustom9Kind(const Value: TcxButtonKind);
    procedure SetBtnCustom10Caption(const Value: string);
    procedure SetBtnCustom1Caption(const Value: string);
    procedure SetBtnCustom2Caption(const Value: string);
    procedure SetBtnCustom3Caption(const Value: string);
    procedure SetBtnCustom4Caption(const Value: string);
    procedure SetBtnCustom5Caption(const Value: string);
    procedure SetBtnCustom6Caption(const Value: string);
    procedure SetBtnCustom7Caption(const Value: string);
    procedure SetBtnCustom8Caption(const Value: string);
    procedure SetBtnCustom9Caption(const Value: string);
    function GetBtnCustom10ImageIndex: longint;
    function GetBtnCustom1ImageIndex: longint;
    function GetBtnCustom2ImageIndex: longint;
    function GetBtnCustom3ImageIndex: longint;
    function GetBtnCustom4ImageIndex: longint;
    function GetBtnCustom5ImageIndex: longint;
    function GetBtnCustom6ImageIndex: longint;
    function GetBtnCustom7ImageIndex: longint;
    function GetBtnCustom8ImageIndex: longint;
    function GetBtnCustom9ImageIndex: longint;
    procedure SetBtnCustom10ImageIndex(const Value: longint);
    procedure SetBtnCustom1ImageIndex(const Value: longint);
    procedure SetBtnCustom2ImageIndex(const Value: longint);
    procedure SetBtnCustom3ImageIndex(const Value: longint);
    procedure SetBtnCustom4ImageIndex(const Value: longint);
    procedure SetBtnCustom5ImageIndex(const Value: longint);
    procedure SetBtnCustom6ImageIndex(const Value: longint);
    procedure SetBtnCustom7ImageIndex(const Value: longint);
    procedure SetBtnCustom8ImageIndex(const Value: longint);
    procedure SetBtnCustom9ImageIndex(const Value: longint);
    function GetBtnCustomCaption(aIndex: Integer): string;
    function GetBtnCustomDropDownMenu(aIndex: Integer): TComponent;
    function GetBtnCustomImageIndex(aIndex: Integer): longint;
    function GetBtnCustomKind(aIndex: Integer): TcxButtonKind;
    procedure SetBtnCustomCaption(aIndex: Integer; aValue: string);
    procedure SetBtnCustomDropDownMenu(aIndex: Integer; aValue: TComponent);
    procedure SetBtnCustomImageIndex(aIndex, aValue: Integer);
    procedure SetBtnCustomKind(aIndex: Integer; aValue: TcxButtonKind);
    function GetBtnCustomImages: TImageList;
    procedure SetBtnCustomImages(const Value: TImageList);
  protected
    procedure Loaded; override;
    procedure Notification(AComponent: TComponent;Operation: TOperation); override;
    procedure Resize; override;
    procedure ShowControl(AControl: TControl); override;

    //因为控件中的Frame下的控件在设计环境下设置Visible时不能看到效果，所以用设置控件宽度的方式为0来模仿不显示控件的效果
    procedure RefreshButtonsSize(aOnlyDesigning:Boolean);
    //从报表中选择操作的报表
    function PopupMenuReport_SelectName(aCaption: string): string;
    //从模板中选择操作的报表
    function PopupMenuTemplate_SelectName(aCaption: string): string;
    //设置报表或模板的默认报表或默认模板
    procedure PopupMenu_SetDefault(aName, aNameFieldName,aIsDefaultFieldName: string;aDefaultValue:boolean; aDataset: TDataset);
  public
    procedure RefreshSubItem;

    function GetLinkTableView: TcxGridTableView;
    //刷新报表子菜单
    procedure RefreshPopupMenuReport;
    //增加报表
    procedure PopupMenuReport_Add(Sender: TObject);
    //删除报表
    procedure PopupMenuReport_Del(Sender: TObject);
    //编辑属性
    procedure PopupMenuReport_EditProperty(Sender: TObject);
    //设计报表
    procedure PopupMenuReport_Design(Sender: TObject);
    //打印报表
    procedure PopupMenuReport_Print(Sender: TObject);
    //切换默认打印机
    procedure PopupMenuReport_DefaultPrint(Sender: TObject);
    //报表列表点击时切换当前使用的报表
    procedure PopupMenuReport_List(Sender: TObject);

    //刷新模板子菜单
    procedure RefreshTemplateItem;
    //另存新模板
    procedure PopupMenuTemplate_Add(Sender: TObject);
    //删除模板
    procedure PopupMenuTemplate_Del(Sender: TObject);
    //设计模板
    procedure PopupMenuTemplate_Design(Sender: TObject);
    //编辑属性
    procedure PopupMenuTemplate_EditProperty(Sender: TObject);
    //模板列表点击时切换当前使用的模板
    procedure PopupMenuTemplate_List(Sender: TObject);

    //根据表权限去设置按钮的可见属性
    procedure UpdateVisableButtons;
    //根据数据集状态去设置按钮的可使用属性
    procedure UpdateEnabledButtons;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  public
    //内部框架对象
    property Frame: TABDBNavigatorFrame  read FFrame     write FFrame     ;
  Published
    //大图标显示
    property BigGlyph: boolean read GetBigGlyph write SetBigGlyph ;
    //按钮图标位置
    property ImageLayout: TButtonLayout read GetImageLayout write SetImageLayout;

    //关联的数据源
    property DataSource: TDataSource read GetDataSource write SetDataSource;

    //显示的按钮的集合
    property VisibleButtons: TABDBNavigatorButtonSet read FVisibleButtons write SetVisibleButtons;

    //显示的按钮的集合
    property ButtonRangeType: TABButtonRangeType read FButtonRangeType write SetButtonRangeType;
    //扩展按钮信息
    property BtnCustomImages     :TImageList      read GetBtnCustomImages    write SetBtnCustomImages     ;

    property BtnCustom1ImageIndex     :LongInt        read GetBtnCustom1ImageIndex      write SetBtnCustom1ImageIndex     ;
    property BtnCustom2ImageIndex     :LongInt        read GetBtnCustom2ImageIndex      write SetBtnCustom2ImageIndex     ;
    property BtnCustom3ImageIndex     :LongInt        read GetBtnCustom3ImageIndex      write SetBtnCustom3ImageIndex     ;
    property BtnCustom4ImageIndex     :LongInt        read GetBtnCustom4ImageIndex      write SetBtnCustom4ImageIndex     ;
    property BtnCustom5ImageIndex     :LongInt        read GetBtnCustom5ImageIndex      write SetBtnCustom5ImageIndex     ;
    property BtnCustom6ImageIndex     :LongInt        read GetBtnCustom6ImageIndex      write SetBtnCustom6ImageIndex     ;
    property BtnCustom7ImageIndex     :LongInt        read GetBtnCustom7ImageIndex      write SetBtnCustom7ImageIndex     ;
    property BtnCustom8ImageIndex     :LongInt        read GetBtnCustom8ImageIndex      write SetBtnCustom8ImageIndex     ;
    property BtnCustom9ImageIndex     :LongInt        read GetBtnCustom9ImageIndex      write SetBtnCustom9ImageIndex     ;
    property BtnCustom10ImageIndex    :LongInt        read GetBtnCustom10ImageIndex      write SetBtnCustom10ImageIndex     ;

    property BtnCustom1Caption     :string        read GetBtnCustom1Caption      write SetBtnCustom1Caption     ;
    property BtnCustom2Caption     :string        read GetBtnCustom2Caption      write SetBtnCustom2Caption     ;
    property BtnCustom3Caption     :string        read GetBtnCustom3Caption      write SetBtnCustom3Caption     ;
    property BtnCustom4Caption     :string        read GetBtnCustom4Caption      write SetBtnCustom4Caption     ;
    property BtnCustom5Caption     :string       read GetBtnCustom5Caption      write SetBtnCustom5Caption     ;
    property BtnCustom6Caption     :string        read GetBtnCustom6Caption      write SetBtnCustom6Caption     ;
    property BtnCustom7Caption     :string        read GetBtnCustom7Caption      write SetBtnCustom7Caption     ;
    property BtnCustom8Caption     :string        read GetBtnCustom8Caption      write SetBtnCustom8Caption     ;
    property BtnCustom9Caption     :string        read GetBtnCustom9Caption      write SetBtnCustom9Caption     ;
    property BtnCustom10Caption    :string        read GetBtnCustom10Caption      write SetBtnCustom10Caption     ;

    property BtnCustom1DropDownMenu:TComponent    read GetBtnCustom1DropDownMenu write SetBtnCustom1DropDownMenu;
    property BtnCustom2DropDownMenu:TComponent    read GetBtnCustom2DropDownMenu write SetBtnCustom2DropDownMenu;
    property BtnCustom3DropDownMenu:TComponent    read GetBtnCustom3DropDownMenu write SetBtnCustom3DropDownMenu;
    property BtnCustom4DropDownMenu:TComponent    read GetBtnCustom4DropDownMenu write SetBtnCustom4DropDownMenu;
    property BtnCustom5DropDownMenu:TComponent    read GetBtnCustom5DropDownMenu write SetBtnCustom5DropDownMenu;
    property BtnCustom6DropDownMenu:TComponent    read GetBtnCustom6DropDownMenu write SetBtnCustom6DropDownMenu;
    property BtnCustom7DropDownMenu:TComponent    read GetBtnCustom7DropDownMenu write SetBtnCustom7DropDownMenu;
    property BtnCustom8DropDownMenu:TComponent    read GetBtnCustom8DropDownMenu write SetBtnCustom8DropDownMenu;
    property BtnCustom9DropDownMenu:TComponent    read GetBtnCustom9DropDownMenu write SetBtnCustom9DropDownMenu;
    property BtnCustom10DropDownMenu:TComponent    read GetBtnCustom10DropDownMenu write SetBtnCustom10DropDownMenu;

    property BtnCustom1Kind        :TcxButtonKind read GetBtnCustom1Kind         write SetBtnCustom1Kind        ;
    property BtnCustom2Kind        :TcxButtonKind read GetBtnCustom2Kind         write SetBtnCustom2Kind        ;
    property BtnCustom3Kind        :TcxButtonKind read GetBtnCustom3Kind         write SetBtnCustom3Kind        ;
    property BtnCustom4Kind        :TcxButtonKind read GetBtnCustom4Kind         write SetBtnCustom4Kind        ;
    property BtnCustom5Kind        :TcxButtonKind read GetBtnCustom5Kind         write SetBtnCustom5Kind        ;
    property BtnCustom6Kind        :TcxButtonKind read GetBtnCustom6Kind         write SetBtnCustom6Kind        ;
    property BtnCustom7Kind        :TcxButtonKind read GetBtnCustom7Kind         write SetBtnCustom7Kind        ;
    property BtnCustom8Kind        :TcxButtonKind read GetBtnCustom8Kind         write SetBtnCustom8Kind        ;
    property BtnCustom9Kind        :TcxButtonKind read GetBtnCustom9Kind         write SetBtnCustom9Kind        ;
    property BtnCustom10Kind       :TcxButtonKind read GetBtnCustom10Kind       write SetBtnCustom10Kind        ;

    property ApproveBill           :string        read FApproveBill            write FApproveBill           ;
    property ApproveMainGuidField  :string        read FApproveMainGuidField   write FApproveMainGuidField  ;
    property ApproveStateField     :string        read FApproveStateField      write FApproveStateField     ;

    property ApprovedRollbackButton   :TABDBNavigatorButton        read FApprovedRollbackButton    write FApprovedRollbackButton     ;
    property ApprovedCommitButton     :TABDBNavigatorButton        read FApprovedCommitButton      write FApprovedCommitButton     ;
  Published
    //按钮控制类型(是数据集内自己控制还是受主数据集控制)
    property ButtonControlType  :TABButtonControlType read FButtonControlType    write SetButtonControlType   ;
    //点击按钮前事件
    property OnBeforeClick  :TABChickBtnEvent read FOnBeforeClick    write FOnBeforeClick   ;
    //点击按钮后事件
    property OnAfterClick   :TABChickBtnEvent read FOnAfterClick    write FOnAfterClick   ;

    //点击按钮后事件
    property OnUpdateVisableButtons  :TABUpdateButtonsEvent read FOnUpdateVisableButtons    write FOnUpdateVisableButtons   ;
    property OnUpdateEnabledButtons  :TABUpdateButtonsEvent read FOnUpdateEnabledButtons    write FOnUpdateEnabledButtons   ;
  end;


//设置关联到数据集的子数据集连接控件的编辑状态
procedure ABSetDatasetEditState(aDataSet:TDataSet;aCanEdit:Boolean);

implementation

{$R *.dfm}

{ TABDBNavigatorFrame }

procedure TABDBNavigatorFrame.DoBeforeClick(aSender: TObject;aButtonType:TABDBNavigatorButton;var aContinue:Boolean);
begin
  aContinue:=true;
  if Assigned(TABDBNavigator(Owner).FOnBeforeClick) then
    TABDBNavigator(Owner).FOnBeforeClick(aSender,aButtonType,aContinue);

  ABWriteLog(GetEnumName(TypeInfo(TABDBNavigatorButton), Ord(aButtonType))+ABGetOwnerLongName(owner)+' DoBeforeClick');
end;

procedure TABDBNavigatorFrame.DoAfterClick(aSender: TObject;aButtonType:TABDBNavigatorButton;var aContinue:Boolean);
begin
  aContinue:=true;
  if Assigned(TABDBNavigator(Owner).FOnAfterClick) then
    TABDBNavigator(Owner).FOnAfterClick(aSender,aButtonType,aContinue);

  if  (TABDBNavigator(Owner).ApproveMainGuidField<>emptystr) and
      (TABDBNavigator(Owner).ApproveStateField<>emptystr)  then
  begin
    if aButtonType=TABDBNavigator(Owner).ApprovedCommitButton then
    begin
      ABApprovedCommit(TABDBNavigator(Owner).ApproveBill,TABThirdQuery(TABDBNavigator(Owner).DataSource.DataSet),TABDBNavigator(Owner).ApproveMainGuidField,TABDBNavigator(Owner).ApproveStateField);
    end
    else if aButtonType=TABDBNavigator(Owner).ApprovedRollbackButton then
    begin
      ABApprovedRollback(TABDBNavigator(Owner).ApproveBill,TABThirdQuery(TABDBNavigator(Owner).DataSource.DataSet),TABDBNavigator(Owner).ApproveMainGuidField,TABDBNavigator(Owner).ApproveStateField);
    end;
  end;

  ABWriteLog(GetEnumName(TypeInfo(TABDBNavigatorButton), Ord(aButtonType))+ABGetOwnerLongName(owner)+' DoAfterClick');
end;


procedure TABDBNavigatorFrame.BtnFirstRecordClick(Sender: TObject);
begin
  DoBeforeClick(Sender,nbFirstRecord,FIsContinue);

  if (not FIsContinue) then
    exit;

  if TABDBNavigator(Owner).GetLinkTableView<>nil then
  begin
    TABDBNavigator(Owner).GetLinkTableView.DataController.GotoFirst;
  end
  else if IsLinkDataSourceOK then
  begin
    TABDBNavigator(Owner).DataSource.DataSet.First;
  end;

  DoAfterClick(Sender,nbFirstRecord,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnPreviousRecordClick(Sender: TObject);
begin
  DoBeforeClick(Sender,nbPreviousRecord,FIsContinue );

  if (not FIsContinue) then
    exit;

  if TABDBNavigator(Owner).GetLinkTableView<>nil then
  begin
    TABDBNavigator(Owner).GetLinkTableView.DataController.GotoPrev;
  end
  else if IsLinkDataSourceOK then
  begin
    TABDBNavigator(Owner).DataSource.DataSet.Prior;
  end;

  DoAfterClick(Sender,nbPreviousRecord,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnLastRecordClick(Sender: TObject);
begin
  DoBeforeClick(Sender,nbLastRecord,FIsContinue);

  if (not FIsContinue) then
    exit;

  if TABDBNavigator(Owner).GetLinkTableView<>nil then
  begin
    TABDBNavigator(Owner).GetLinkTableView.DataController.GotoLast;
  end
  else if IsLinkDataSourceOK then
  begin
    TABDBNavigator(Owner).DataSource.DataSet.Last;
  end;

  DoAfterClick(Sender,nbLastRecord,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnNextRecordClick(Sender: TObject);
begin
  DoBeforeClick(Sender,nbNextRecord,FIsContinue);

  if (not FIsContinue) then
    exit;

  if TABDBNavigator(Owner).GetLinkTableView<>nil then
  begin
    TABDBNavigator(Owner).GetLinkTableView.DataController.GotoNext;
  end
  else if IsLinkDataSourceOK then
  begin
    TABDBNavigator(Owner).DataSource.DataSet.Next;
  end;

  DoAfterClick(Sender,nbNextRecord,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnInsertClick(Sender: TObject);
var
  tempGroupFieldName:array of string;
  tempGroupValues:array of Variant;
  tempGroupValueCount:longint;
  procedure BackGroupValues;
  var
    I: Integer;
  begin
    tempGroupValueCount:=0;
    if TABDBNavigator(Owner).GetLinkTableView.DataController.RecordCount>0  then
    begin
      if TABcxGridDBBandedTableView(TABDBNavigator(Owner).GetLinkTableView).ColumnCount>=1 then
      begin
        for I := 0 to TABcxGridDBBandedTableView(TABDBNavigator(Owner).GetLinkTableView).ColumnCount - 1 do
        begin
          if TABcxGridDBBandedTableView(TABDBNavigator(Owner).GetLinkTableView).Columns[i].GroupIndex>=0 then
          begin
            tempGroupValueCount:=tempGroupValueCount+1;
            SetLength(tempGroupFieldName,tempGroupValueCount);
            SetLength(tempGroupValues,tempGroupValueCount);
            tempGroupFieldName[tempGroupValueCount-1]:=TABcxGridDBBandedTableView(TABDBNavigator(Owner).GetLinkTableView).Columns[i].DataBinding.Field.FieldName;
            tempGroupValues[tempGroupValueCount-1]:=TABcxGridDBBandedTableView(TABDBNavigator(Owner).GetLinkTableView).Columns[i].DataBinding.Field.Value;
          end;
        end;
      end;
    end;
  end;

  procedure SetGroupValues;
  var
    I: Integer;
  begin
    if (tempGroupValueCount>0) and
       (TABDBNavigator(Owner).DataSource.DataSet.State in [dsEdit,dsInsert]) then
    begin
      for I := Low(tempGroupFieldName) to High(tempGroupFieldName) do
      begin
        TABDBNavigator(Owner).DataSource.DataSet.FieldByName(tempGroupFieldName[i]).Value:=tempGroupValues[i];
      end;
    end;
  end;
begin
  DoBeforeClick(Sender,nbInsert,FIsContinue);

  if (not FIsContinue) then
    exit;

  if TABDBNavigator(Owner).FAddType=daAppend then
  begin
    if TABDBNavigator(Owner).GetLinkTableView<>nil then
    begin
      if TABcxGridDBBandedTableView(TABDBNavigator(Owner).GetLinkTableView).OptionsSelection.MultiSelect then
      begin
        TABcxGridDBBandedTableView(TABDBNavigator(Owner).GetLinkTableView).OptionsSelection.MultiSelect:=false;
        try
          BackGroupValues;
          TABDBNavigator(Owner).GetLinkTableView.DataController.Append;
          SetGroupValues;
        finally
          TABcxGridDBBandedTableView(TABDBNavigator(Owner).GetLinkTableView).OptionsSelection.MultiSelect:=True;
        end;
      end
      else
      begin
        BackGroupValues;
        TABDBNavigator(Owner).GetLinkTableView.DataController.Append;
        SetGroupValues;
      end;
    end
    else if IsLinkDataSourceOK then
    begin
      TABDBNavigator(Owner).DataSource.DataSet.Append;
    end;
  end
  else
  begin
    //序号字段的取值处理
    TABDictionaryQuery(TABDBNavigator(Owner).DataSource.DataSet).GetInsertOrderValue;
    try
      if TABDBNavigator(Owner).GetLinkTableView<>nil then
      begin
        if TABcxGridDBBandedTableView(TABDBNavigator(Owner).GetLinkTableView).OptionsSelection.MultiSelect then
        begin
          TABcxGridDBBandedTableView(TABDBNavigator(Owner).GetLinkTableView).OptionsSelection.MultiSelect:=false;
          try
            BackGroupValues;
            TABDBNavigator(Owner).GetLinkTableView.DataController.Insert;
            SetGroupValues;
          finally
            TABcxGridDBBandedTableView(TABDBNavigator(Owner).GetLinkTableView).OptionsSelection.MultiSelect:=True;
          end;
        end
        else
        begin
          BackGroupValues;
          TABDBNavigator(Owner).GetLinkTableView.DataController.Insert;
          SetGroupValues;
        end;
      end
      else if IsLinkDataSourceOK then
      begin
        TABDBNavigator(Owner).DataSource.DataSet.Insert;
      end;
    finally
      //序号字段的设置值处理
      if (TABDBNavigator(Owner).DataSource.DataSet.State in [dsInsert]) then
        TABDictionaryQuery(TABDBNavigator(Owner).DataSource.DataSet).SetInsertOrderValue;
    end;
  end;

  if TABDBNavigator(Owner).FShowDBPanelInAdd then
    TABDictionaryQuery(TABDBNavigator(Owner).DataSource.DataSet).ShowDBPanel(True);

  DoAfterClick(Sender,nbInsert,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnCopyClick(Sender: TObject);
begin
  DoBeforeClick(Sender,nbCopy,FIsContinue);

  if (not FIsContinue) then
    exit;

  if  IsLinkDataSourceOK then
  begin
    TABDictionaryQuery(TABDBNavigator(Owner).DataSource.DataSet).DoCopy;
    if TABDBNavigator(Owner).FShowDBPanelInAdd then
      TABDictionaryQuery(TABDBNavigator(Owner).DataSource.DataSet).ShowDBPanel(True);
  end;

  if TABDBNavigator(Owner).GetLinkTableView<>nil then
    TABDBNavigator(Owner).GetLinkTableView.DataController.Refresh;

  DoAfterClick(Sender,nbCopy,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnCustom1Click(Sender: TObject);
begin
  DoBeforeClick(Sender,nbCustom1,FIsContinue);

  if (not FIsContinue) then
    exit;

  DoAfterClick(Sender,nbCustom1,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnCustom2Click(Sender: TObject);
begin
  DoBeforeClick(Sender,nbCustom2,FIsContinue);

  if (not FIsContinue) then
    exit;

  DoAfterClick(Sender,nbCustom2,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnCustom3Click(Sender: TObject);
begin
  DoBeforeClick(Sender,nbCustom3,FIsContinue);

  if (not FIsContinue) then
    exit;

  DoAfterClick(Sender,nbCustom3,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnCustom4Click(Sender: TObject);
begin
  DoBeforeClick(Sender,nbCustom4,FIsContinue);

  if (not FIsContinue) then
    exit;

  DoAfterClick(Sender,nbCustom4,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnCustom5Click(Sender: TObject);
begin
  DoBeforeClick(Sender,nbCustom5,FIsContinue);

  if (not FIsContinue) then
    exit;

  DoAfterClick(Sender,nbCustom5,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnCustom6Click(Sender: TObject);
begin
  DoBeforeClick(Sender,nbCustom6,FIsContinue);

  if (not FIsContinue) then
    exit;

  DoAfterClick(Sender,nbCustom6,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnCustom7Click(Sender: TObject);
begin
  DoBeforeClick(Sender,nbCustom7,FIsContinue);

  if (not FIsContinue) then
    exit;

  DoAfterClick(Sender,nbCustom7,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnCustom8Click(Sender: TObject);
begin
  DoBeforeClick(Sender,nbCustom8,FIsContinue);

  if (not FIsContinue) then
    exit;

  DoAfterClick(Sender,nbCustom8,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnCustom9Click(Sender: TObject);
begin
  DoBeforeClick(Sender,nbCustom9,FIsContinue);

  if (not FIsContinue) then
    exit;

  DoAfterClick(Sender,nbCustom9,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnCustom10Click(Sender: TObject);
begin
  DoBeforeClick(Sender,nbCustom10,FIsContinue);

  if (not FIsContinue) then
    exit;

  DoAfterClick(Sender,nbCustom10,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnDeleteClick(Sender: TObject);
var
  tempABBeforeDeleteAsk:boolean;
begin
  DoBeforeClick(Sender,nbDelete,FIsContinue);

  if (not FIsContinue) then
    exit;

  if TABDBNavigator(Owner).GetLinkTableView<>nil then
  begin
    if TABDBNavigator(Owner).GetLinkTableView.DataController.GetSelectedCount>1 then
    begin
      if ABShow('是否确认删除多笔数据?',[],['是','否'],1)=1 then
      begin
        tempABBeforeDeleteAsk:=TABDictionaryQuery(TABDBNavigator(Owner).DataSource.DataSet).BeforeDeleteAsk;
        TABDictionaryQuery(TABDBNavigator(Owner).DataSource.DataSet).BeforeDeleteAsk:=false;
        try
          TABDBNavigator(Owner).GetLinkTableView.DataController.DeleteSelection
        finally
          TABDictionaryQuery(TABDBNavigator(Owner).DataSource.DataSet).BeforeDeleteAsk:=tempABBeforeDeleteAsk;
        end;
      end;
    end
    else
    begin
      TABDBNavigator(Owner).GetLinkTableView.DataController.DeleteFocused;
    end;
  end
  else if IsLinkDataSourceOK then
  begin
    if not ABDatasetIsEmpty(TABDBNavigator(Owner).DataSource.DataSet) then
      TABDBNavigator(Owner).DataSource.DataSet.Delete;
  end;

  DoAfterClick(Sender,nbDelete,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnEditClick(Sender: TObject);
begin
  DoBeforeClick(Sender,nbEdit,FIsContinue);

  if (not FIsContinue) then
    exit;

  if  IsLinkDataSourceOK then
  begin
    if TABDBNavigator(Owner).DataSource.DataSet.State in [dsBrowse] then
    begin
      TABDBNavigator(Owner).DataSource.DataSet.Edit;
      if TABDBNavigator(Owner).FShowDBPanelInEdit then
        TABDictionaryQuery(TABDBNavigator(Owner).DataSource.DataSet).ShowDBPanel(True);
    end;
  end;

  DoAfterClick(Sender,nbEdit,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnPostClick(Sender: TObject);
begin
  DoBeforeClick(Sender,nbPost,FIsContinue);

  if (not FIsContinue) then
    exit;

  if  IsLinkDataSourceOK  then
  begin
    ABPostDataset(TABDBNavigator(Owner).DataSource.DataSet);
  end;

  DoAfterClick(Sender,nbPost,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnCancelClick(Sender: TObject);
begin
  DoBeforeClick(Sender,nbCancel,FIsContinue);

  if (not FIsContinue) then
    exit;

  if  IsLinkDataSourceOK then
  begin
    TABDBNavigator(Owner).DataSource.DataSet.Cancel;
  end;

  DoAfterClick(Sender,nbCancel,FIsContinue);
end;

procedure TABDBNavigatorFrame.btnQueryClick(Sender: TObject);
begin
  DoBeforeClick(Sender,nbQuery,FIsContinue);

  if (not FIsContinue) then
    exit;

  if (Assigned(TABDBNavigator(Owner).DataSource)) and
     (Assigned(TABDBNavigator(Owner).DataSource.DataSet))  then
  begin
    TABDictionaryQuery(TABDBNavigator(Owner).DataSource.DataSet).OpenQuery;
  end;

  DoAfterClick(Sender,nbQuery,FIsContinue);
end;

procedure TABDBNavigatorFrame.BtnReportClick(Sender: TObject);
begin
  DoBeforeClick(Sender,nbReport,FIsContinue);

  if (not FIsContinue) then
    exit;

  TABDBNavigator(Owner).PopupMenuReport_Print(BtnReport);

  DoAfterClick(Sender,nbReport,FIsContinue);
end;

constructor TABDBNavigatorFrame.Create(AOwner: TComponent);
begin
  inherited;
  FCustomButtons[0]:=BtnCustom1;
  FCustomButtons[1]:=BtnCustom2;
  FCustomButtons[2]:=BtnCustom3;
  FCustomButtons[3]:=BtnCustom4;
  FCustomButtons[4]:=BtnCustom5;
  FCustomButtons[5]:=BtnCustom6;
  FCustomButtons[6]:=BtnCustom7;
  FCustomButtons[7]:=BtnCustom8;
  FCustomButtons[8]:=BtnCustom9;
  FCustomButtons[9]:=BtnCustom10;
end;

procedure TABDBNavigatorFrame.BtnExitClick(Sender: TObject);
begin
  DoBeforeClick(Sender,nbExit,FIsContinue);

  if (not FIsContinue) then
    exit;

  if (Assigned(Owner)) and
     (Assigned(Owner.Owner)) and
     (Owner.Owner is TForm) then
  begin
    TForm(Owner.Owner).Close;
  end;

  DoAfterClick(Sender,nbExit,FIsContinue);
end;

function TABDBNavigatorFrame.IsLinkDataSourceOK: boolean;
begin
  result:=  (Owner is TABDBNavigator) and
            (Assigned(TABDBNavigator(Owner).DataSource)) and
            (ABCheckDataSetInActive(TABDBNavigator(Owner).DataSource.DataSet));
end;

procedure TABDBNavigatorFrame.N9N10N11Click(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  TABDBNavigator(Owner).FPrintOutType:=ABIIF(N10.Checked,ptSelectPrint,ABIIF(N11.Checked,ptDirect,ptPreview));
  TABDBNavigator(Owner).SetDatabasePropertyValue('PrintOutType',ABIIF(TABDBNavigator(Owner).FPrintOutType=TABDBNavigator(Owner).FPrintOutType_Default,
                                                                emptystr,
                                                                ABEnumValueToName(TypeInfo(TABPrintOutType),ord(TABDBNavigator(Owner).FPrintOutType))));
end;

procedure TABDBNavigatorFrame.N13N14Click(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  TABDBNavigator(Owner).FPrintRange:=ABIIF(N13.Checked,prCur,prAll);
  TABDBNavigator(Owner).SetDatabasePropertyValue('PrintRange',ABIIF(TABDBNavigator(Owner).FPrintRange=TABDBNavigator(Owner).FPrintRange_Default,
                                                              emptystr,
                                                              ABEnumValueToName(TypeInfo(TABPrintRange),ord(TABDBNavigator(Owner).FPrintRange))));
end;

procedure TABDBNavigatorFrame.N17Click(Sender: TObject);
begin
  ABInputDataToDataset(TABDBNavigator(Owner).DataSource.DataSet,emptystr,emptystr,[]);
end;

procedure TABDBNavigatorFrame.N2N3Click(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
  TABDBNavigator(Owner).FAddType:=ABIIF(N2.Checked,daInsert,daAppend);
  TABDBNavigator(Owner).SetDatabasePropertyValue('AddType',ABIIF(TABDBNavigator(Owner).FAddType=TABDBNavigator(Owner).FAddType_Default,
                                                              emptystr,
                                                              ABEnumValueToName(TypeInfo(TABDatasetAddType),ord(TABDBNavigator(Owner).FAddType))));
end;

procedure TABDBNavigatorFrame.N4Click(Sender: TObject);
begin
  TABDBNavigator(Owner).FShowDBPanelInAdd:=TMenuItem(Sender).Checked;
  TABDBNavigator(Owner).SetDatabasePropertyValue('ShowDBPanelInAdd',ABIIF(TABDBNavigator(Owner).FShowDBPanelInAdd=TABDBNavigator(Owner).FShowDBPanelInAdd_Default,
                                                                    emptystr,
                                                                    ABBoolToStr(TABDBNavigator(Owner).FShowDBPanelInAdd)));
end;

procedure TABDBNavigatorFrame.N6Click(Sender: TObject);
begin
  TABDBNavigator(Owner).FShowDBPanelInEdit:=TMenuItem(Sender).Checked;
  TABDBNavigator(Owner).SetDatabasePropertyValue('ShowDBPanelInEdit',ABIIF(TABDBNavigator(Owner).FShowDBPanelInEdit=TABDBNavigator(Owner).FShowDBPanelInEdit_Default,
                                                                    emptystr,
                                                                    ABBoolToStr(TABDBNavigator(Owner).FShowDBPanelInEdit)));
end;

procedure TABDBNavigatorFrame.N8Click(Sender: TObject);
begin
  inherited;

end;

{ TABBarDBNavDataLink }

constructor TABBarDBNavDataLink.Create(aNavigator: TABDBNavigator);
begin
  inherited Create;
  FNavigator := aNavigator;
end;

procedure TABBarDBNavDataLink.ActiveChanged;
begin
  inherited;
  FNavigator.UpdateVisableButtons;
  FNavigator.UpdateEnabledButtons;
end;

procedure TABBarDBNavDataLink.EditingChanged;
begin
  inherited;
  if TABDictionaryQuery(DataSet).InitActiveOK then
    FNavigator.UpdateEnabledButtons;
end;

procedure TABBarDBNavDataLink.DataSetChanged;
begin
  inherited;
end;

procedure TABBarDBNavDataLink.DataSetScrolled(Distance: Integer);
begin
  inherited;

  if TABDictionaryQuery(DataSet).InitActiveOK then
    FNavigator.UpdateEnabledButtons;
end;

procedure TABBarDBNavDataLink.RecordChanged(Field: TField);
begin
  inherited;

  if TABDictionaryQuery(DataSet).InitActiveOK then
    FNavigator.UpdateEnabledButtons;
end;

{ TABDBNavigator }

destructor TABDBNavigator.Destroy;
begin
  FDataLink.Free;
  FDataLink := nil;

  FFrame.Free;
  FFrame := nil;

  inherited;
end;

constructor TABDBNavigator.Create(AOwner: TComponent);
begin
  inherited;
  FButtonControlType:=ctSingle;
  FApprovedCommitButton:=nbnull;
  FApprovedRollbackButton:=nbnull;

  Height:=25;
  BevelOuter:=bvNone;
  FDataLink :=TABBarDBNavDataLink.Create(self);
  FFrame:= TABDBNavigatorFrame.Create(Self);
  FFrame.Parent:=Self;
  FFrame.Align:=alClient;
  FFrame.Show;

  SetBtnEnabled([]);
  FReportDataset:=nil;
  FTemplateDataset:=nil;
  FPrintOutType:=ptPreview;
  FPrintRange:=prAll;

  ButtonRangeType:=RtMain;
  //值存于数据库中
  FAddType_Default          :=daAppend ;
  FShowDBPanelInAdd_Default :=false    ;
  FShowDBPanelInEdit_Default:=false    ;
  FPrintOutType_Default     :=ptPreview;
  FDefaultPrint_Default     :=''       ;
  FPrintRange_Default       :=prAll    ;

  FAddType          :=FAddType_Default            ;
  FShowDBPanelInAdd :=FShowDBPanelInAdd_Default   ;
  FShowDBPanelInEdit:=FShowDBPanelInEdit_Default  ;
  FPrintOutType     :=FPrintOutType_Default       ;
  FDefaultPrint     :=FDefaultPrint_Default       ;
  FPrintRange       :=FPrintRange_Default         ;
end;

procedure TABDBNavigator.SetDatabasePropertyValue(aPropertyName,aPropertyValue:string);
var
  tempParentForm:TForm;
begin
  if (Assigned(DataSource)) and
     (Assigned(DataSource.DataSet)) and
     (DataSource.DataSet is TABDictionaryQuery) and
     (TABDictionaryQuery(DataSource.DataSet).FuncGuid<>EmptyStr) then
  begin
    if Owner is TForm then
    begin
      tempParentForm:=TForm(Owner);
      if (Assigned(tempParentForm)) and
         (tempParentForm.Name<>EmptyStr) and
         (TABDictionaryQuery(DataSource.DataSet).FuncGuid<>EmptyStr) then
      begin
        ABSetFieldValue(
                        GetFuncControlPropertyDataset,
                        ['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],
                        [TABDictionaryQuery(DataSource.DataSet).FuncGuid,
                         tempParentForm.Name,Name,
                         aPropertyName],
                        ['FP_PropertyValue'],
                        [aPropertyValue]
                          );
      end;
    end;
  end;
end;

procedure TABDBNavigator.SetDataSource(const Value: TDataSource);
begin
  FTableView:=nil;
  FFuncControlPropertyDataset:=nil;
  if FDataLink.DataSource <> Value then
  begin
    FDataLink.DataSource := Value;
  end;
end;

function TABDBNavigator.GetLinkTableView: TcxGridTableView;
var
  tempFieldDef:PABFieldDef;
  i:LongInt;
  tempDataset:TABDictionaryQuery;
begin
  if not Assigned(FTableView) then
  begin
    if (Assigned(DataSource)) and
       (Assigned(DataSource.DataSet))  then
    begin
      tempDataset:=TABDictionaryQuery(DataSource.DataSet);
      for I := 0  to tempDataset.FieldCount-1 do
      begin
        tempFieldDef:=tempDataset.FieldDefItems[i];
        if (Assigned(tempFieldDef)) and
           (Assigned(tempFieldDef.Fi_GridColumn))
            then
        begin
          FTableView:= tempFieldDef.Fi_GridColumn.GridView;
          Break;
        end;
      end;
    end;
  end;
  result:= FTableView;
end;


function TABDBNavigator.GetDataSource: TDataSource;
begin
  Result := FDataLink.DataSource;
end;

function TABDBNavigator.GetBigGlyph: boolean;
begin
  if FFrame.BtnFirstRecord.OptionsImage.Images=FFrame.ImageList_32 then
    result:=true
  else
    result:=False;
end;

function TABDBNavigator.GetBtnCustomCaption(aIndex:longint): string;
begin
  result:=FFrame.FCustomButtons[aIndex-1].Caption;
end;

function TABDBNavigator.GetBtnCustomDropDownMenu(aIndex:longint): TComponent;
begin
  result:=FFrame.FCustomButtons[aIndex-1].DropDownMenu;
end;

function TABDBNavigator.GetBtnCustomImageIndex(aIndex:longint): longint;
begin
  result:=FFrame.FCustomButtons[aIndex-1].OptionsImage.ImageIndex;
end;

function TABDBNavigator.GetBtnCustomImages: TImageList;
begin
  result:=TImageList(FFrame.FCustomButtons[0].OptionsImage.Images);
end;

function TABDBNavigator.GetBtnCustomKind(aIndex:longint): TcxButtonKind;
begin
  result:=FFrame.FCustomButtons[aIndex-1].Kind;
end;

procedure TABDBNavigator.SetBtnCustomCaption(aIndex:longint;aValue:string) ;
begin
  FFrame.FCustomButtons[aIndex-1].Caption:=aValue;
  SetBtnCustomInfo;
end;

procedure TABDBNavigator.SetBtnCustomDropDownMenu(aIndex:longint;aValue:TComponent) ;
begin
  FFrame.FCustomButtons[aIndex-1].DropDownMenu:=aValue;
  SetBtnCustomInfo;
end;

procedure TABDBNavigator.SetBtnCustomImageIndex(aIndex:longint;aValue:longint)  ;
begin
  FFrame.FCustomButtons[aIndex-1].OptionsImage.ImageIndex:=aValue;
  SetBtnCustomInfo;
end;

procedure TABDBNavigator.SetBtnCustomImages(const Value: TImageList);
var
  i:LongInt;
begin
  for I := Low(FFrame.FCustomButtons) to High(FFrame.FCustomButtons) do
  begin
    FFrame.FCustomButtons[i].OptionsImage.Images:=Value;
  end;
end;

procedure TABDBNavigator.SetBtnCustomKind(aIndex:longint;aValue: TcxButtonKind);
begin
  FFrame.FCustomButtons[aIndex-1].Kind:=aValue;
  SetBtnCustomInfo;
end;

function TABDBNavigator.GetBtnCustom1Caption: string;
begin
  result:=GetBtnCustomCaption(1);
end;

function TABDBNavigator.GetBtnCustom1DropDownMenu: TComponent;
begin
  result:=GetBtnCustomDropDownMenu(1);
end;

function TABDBNavigator.GetBtnCustom1ImageIndex: longint;
begin
  result:=GetBtnCustomImageIndex(1);
end;

function TABDBNavigator.GetBtnCustom1Kind: TcxButtonKind;
begin
  result:=GetBtnCustomKind(1);
end;

procedure TABDBNavigator.SetBtnCustom1Caption(const Value: string);
begin
  SetBtnCustomCaption(1,Value);
end;

procedure TABDBNavigator.SetBtnCustom1DropDownMenu(const Value: TComponent);
begin
  SetBtnCustomDropDownMenu(1,Value);
end;

procedure TABDBNavigator.SetBtnCustom1ImageIndex(const Value: longint);
begin
  SetBtnCustomImageIndex(1,Value);
end;

procedure TABDBNavigator.SetBtnCustom1Kind(const Value: TcxButtonKind);
begin
  SetBtnCustomKind(1,Value);
end;

function TABDBNavigator.GetBtnCustom2Caption: string;
begin
  result:=GetBtnCustomCaption(2);
end;

function TABDBNavigator.GetBtnCustom2DropDownMenu: TComponent;
begin
  result:=GetBtnCustomDropDownMenu(2);
end;

function TABDBNavigator.GetBtnCustom2ImageIndex: longint;
begin
  result:=GetBtnCustomImageIndex(2);
end;

function TABDBNavigator.GetBtnCustom2Kind: TcxButtonKind;
begin
  result:=GetBtnCustomKind(2);
end;

procedure TABDBNavigator.SetBtnCustom2Caption(const Value: string);
begin
  SetBtnCustomCaption(2,Value);
end;

procedure TABDBNavigator.SetBtnCustom2DropDownMenu(const Value: TComponent);
begin
  SetBtnCustomDropDownMenu(2,Value);
end;

procedure TABDBNavigator.SetBtnCustom2ImageIndex(const Value: longint);
begin
  SetBtnCustomImageIndex(2,Value);
end;

procedure TABDBNavigator.SetBtnCustom2Kind(const Value: TcxButtonKind);
begin
  SetBtnCustomKind(2,Value);
end;

function TABDBNavigator.GetBtnCustom3Caption: string;
begin
  result:=GetBtnCustomCaption(3);
end;

function TABDBNavigator.GetBtnCustom3DropDownMenu: TComponent;
begin
  result:=GetBtnCustomDropDownMenu(3);
end;

function TABDBNavigator.GetBtnCustom3ImageIndex: longint;
begin
  result:=GetBtnCustomImageIndex(3);
end;

function TABDBNavigator.GetBtnCustom3Kind: TcxButtonKind;
begin
  result:=GetBtnCustomKind(3);
end;

procedure TABDBNavigator.SetBtnCustom3Caption(const Value: string);
begin
  SetBtnCustomCaption(3,Value);
end;

procedure TABDBNavigator.SetBtnCustom3DropDownMenu(const Value: TComponent);
begin
  SetBtnCustomDropDownMenu(3,Value);
end;

procedure TABDBNavigator.SetBtnCustom3ImageIndex(const Value: longint);
begin
  SetBtnCustomImageIndex(3,Value);
end;

procedure TABDBNavigator.SetBtnCustom3Kind(const Value: TcxButtonKind);
begin
  SetBtnCustomKind(3,Value);
end;

function TABDBNavigator.GetBtnCustom4Caption: string;
begin
  result:=GetBtnCustomCaption(4);
end;

function TABDBNavigator.GetBtnCustom4DropDownMenu: TComponent;
begin
  result:=GetBtnCustomDropDownMenu(4);
end;

function TABDBNavigator.GetBtnCustom4ImageIndex: longint;
begin
  result:=GetBtnCustomImageIndex(4);
end;

function TABDBNavigator.GetBtnCustom4Kind: TcxButtonKind;
begin
  result:=GetBtnCustomKind(4);
end;

procedure TABDBNavigator.SetBtnCustom4Caption(const Value: string);
begin
  SetBtnCustomCaption(4,Value);
end;

procedure TABDBNavigator.SetBtnCustom4DropDownMenu(const Value: TComponent);
begin
  SetBtnCustomDropDownMenu(4,Value);
end;

procedure TABDBNavigator.SetBtnCustom4ImageIndex(const Value: longint);
begin
  SetBtnCustomImageIndex(4,Value);
end;

procedure TABDBNavigator.SetBtnCustom4Kind(const Value: TcxButtonKind);
begin
  SetBtnCustomKind(4,Value);
end;

function TABDBNavigator.GetBtnCustom5Caption: string;
begin
  result:=GetBtnCustomCaption(5);
end;

function TABDBNavigator.GetBtnCustom5DropDownMenu: TComponent;
begin
  result:=GetBtnCustomDropDownMenu(5);
end;

function TABDBNavigator.GetBtnCustom5ImageIndex: longint;
begin
  result:=GetBtnCustomImageIndex(5);
end;

function TABDBNavigator.GetBtnCustom5Kind: TcxButtonKind;
begin
  result:=GetBtnCustomKind(5);
end;

procedure TABDBNavigator.SetBtnCustom5Caption(const Value: string);
begin
  SetBtnCustomCaption(5,Value);
end;

procedure TABDBNavigator.SetBtnCustom5DropDownMenu(const Value: TComponent);
begin
  SetBtnCustomDropDownMenu(5,Value);
end;

procedure TABDBNavigator.SetBtnCustom5ImageIndex(const Value: longint);
begin
  SetBtnCustomImageIndex(5,Value);
end;

procedure TABDBNavigator.SetBtnCustom5Kind(const Value: TcxButtonKind);
begin
  SetBtnCustomKind(5,Value);
end;

function TABDBNavigator.GetBtnCustom6Caption: string;
begin
  result:=GetBtnCustomCaption(6);
end;

function TABDBNavigator.GetBtnCustom6DropDownMenu: TComponent;
begin
  result:=GetBtnCustomDropDownMenu(6);
end;

function TABDBNavigator.GetBtnCustom6ImageIndex: longint;
begin
  result:=GetBtnCustomImageIndex(6);
end;

function TABDBNavigator.GetBtnCustom6Kind: TcxButtonKind;
begin
  result:=GetBtnCustomKind(6);
end;

procedure TABDBNavigator.SetBtnCustom6Caption(const Value: string);
begin
  SetBtnCustomCaption(6,Value);
end;

procedure TABDBNavigator.SetBtnCustom6DropDownMenu(const Value: TComponent);
begin
  SetBtnCustomDropDownMenu(6,Value);
end;

procedure TABDBNavigator.SetBtnCustom6ImageIndex(const Value: longint);
begin
  SetBtnCustomImageIndex(6,Value);
end;

procedure TABDBNavigator.SetBtnCustom6Kind(const Value: TcxButtonKind);
begin
  SetBtnCustomKind(6,Value);
end;

function TABDBNavigator.GetBtnCustom7Caption: string;
begin
  result:=GetBtnCustomCaption(7);
end;

function TABDBNavigator.GetBtnCustom7DropDownMenu: TComponent;
begin
  result:=GetBtnCustomDropDownMenu(7);
end;

function TABDBNavigator.GetBtnCustom7ImageIndex: longint;
begin
  result:=GetBtnCustomImageIndex(7);
end;

function TABDBNavigator.GetBtnCustom7Kind: TcxButtonKind;
begin
  result:=GetBtnCustomKind(7);
end;

procedure TABDBNavigator.SetBtnCustom7Caption(const Value: string);
begin
  SetBtnCustomCaption(7,Value);
end;

procedure TABDBNavigator.SetBtnCustom7DropDownMenu(const Value: TComponent);
begin
  SetBtnCustomDropDownMenu(7,Value);
end;

procedure TABDBNavigator.SetBtnCustom7ImageIndex(const Value: longint);
begin
  SetBtnCustomImageIndex(7,Value);
end;

procedure TABDBNavigator.SetBtnCustom7Kind(const Value: TcxButtonKind);
begin
  SetBtnCustomKind(7,Value);
end;

function TABDBNavigator.GetBtnCustom8Caption: string;
begin
  result:=GetBtnCustomCaption(8);
end;

function TABDBNavigator.GetBtnCustom8DropDownMenu: TComponent;
begin
  result:=GetBtnCustomDropDownMenu(8);
end;

function TABDBNavigator.GetBtnCustom8ImageIndex: longint;
begin
  result:=GetBtnCustomImageIndex(8);
end;

function TABDBNavigator.GetBtnCustom8Kind: TcxButtonKind;
begin
  result:=GetBtnCustomKind(8);
end;

procedure TABDBNavigator.SetBtnCustom8Caption(const Value: string);
begin
  SetBtnCustomCaption(8,Value);
end;

procedure TABDBNavigator.SetBtnCustom8DropDownMenu(const Value: TComponent);
begin
  SetBtnCustomDropDownMenu(8,Value);
end;

procedure TABDBNavigator.SetBtnCustom8ImageIndex(const Value: longint);
begin
  SetBtnCustomImageIndex(8,Value);
end;

procedure TABDBNavigator.SetBtnCustom8Kind(const Value: TcxButtonKind);
begin
  SetBtnCustomKind(8,Value);
end;

function TABDBNavigator.GetBtnCustom9Caption: string;
begin
  result:=GetBtnCustomCaption(9);
end;

function TABDBNavigator.GetBtnCustom9DropDownMenu: TComponent;
begin
  result:=GetBtnCustomDropDownMenu(9);
end;

function TABDBNavigator.GetBtnCustom9ImageIndex: longint;
begin
  result:=GetBtnCustomImageIndex(9);
end;

function TABDBNavigator.GetBtnCustom9Kind: TcxButtonKind;
begin
  result:=GetBtnCustomKind(9);
end;

procedure TABDBNavigator.SetBtnCustom9Caption(const Value: string);
begin
  SetBtnCustomCaption(9,Value);
end;

procedure TABDBNavigator.SetBtnCustom9DropDownMenu(const Value: TComponent);
begin
  SetBtnCustomDropDownMenu(9,Value);
end;

procedure TABDBNavigator.SetBtnCustom9ImageIndex(const Value: longint);
begin
  SetBtnCustomImageIndex(9,Value);
end;

procedure TABDBNavigator.SetBtnCustom9Kind(const Value: TcxButtonKind);
begin
  SetBtnCustomKind(9,Value);
end;

function TABDBNavigator.GetBtnCustom10Caption: string;
begin
  result:=GetBtnCustomCaption(10);
end;

function TABDBNavigator.GetBtnCustom10DropDownMenu: TComponent;
begin
  result:=GetBtnCustomDropDownMenu(10);
end;

function TABDBNavigator.GetBtnCustom10ImageIndex: longint;
begin
  result:=GetBtnCustomImageIndex(10);
end;

function TABDBNavigator.GetBtnCustom10Kind: TcxButtonKind;
begin
  result:=GetBtnCustomKind(10);
end;

procedure TABDBNavigator.SetBtnCustom10Caption(const Value: string);
begin
  SetBtnCustomCaption(10,Value);
end;

procedure TABDBNavigator.SetBtnCustom10DropDownMenu(const Value: TComponent);
begin
  SetBtnCustomDropDownMenu(10,Value);
end;

procedure TABDBNavigator.SetBtnCustom10ImageIndex(const Value: longint);
begin
  SetBtnCustomImageIndex(10,Value);
end;

procedure TABDBNavigator.SetBtnCustom10Kind(const Value: TcxButtonKind);
begin
  SetBtnCustomKind(10,Value);
end;


procedure TABDBNavigator.SetBtnCustomInfo;
begin
  RefreshButtonsSize(true);
end;

function TABDBNavigator.GetImageLayout: TButtonLayout;
var
  i:LongInt;
  tempControl:TControl;
begin
  result:=blGlyphLeft;
  for I := 0 to FFrame.ControlCount-1 do
  begin
    tempControl:=FFrame.Controls[i];
    if tempControl is TABcxButton  then
    begin
      result:=TABcxButton(tempControl).OptionsImage.Layout;
      Break;
    end;
  end;
end;

function TABDBNavigator.GetFuncControlPropertyDataset:Tdataset;
begin
  if not Assigned(FFuncControlPropertyDataset) then
    FFuncControlPropertyDataset:= ABGetConstSqlPubDataset('ABSys_Org_FuncControlProperty',
                                   [TABDictionaryQuery(DataSource.DataSet).FuncGuid,
                                   TForm(Owner).Name],TABInsideQuery);

  result:=FFuncControlPropertyDataset;
end;

procedure TABDBNavigator.Loaded;
begin
  inherited;
  FLoaded:=true;
  RefreshButtonsSize(False);

  RefreshSubItem;
end;

procedure TABDBNavigator.RefreshSubItem;
var
  tempParentForm:TForm;
  tempFieldValue:string;
  I: Integer;
  tempMenuItem:TMenuItem;
begin
  if (not (csDesigning in ComponentState)) and
     (Owner is TForm) and
     (Assigned(DataSource)) and
     (Assigned(DataSource.DataSet)) then
  begin
    FReportDataset:=ABGetConstSqlPubDataset('ABSys_Org_FuncReport',[TABDictionaryQuery(DataSource.DataSet).FuncGuid,Name],TABDictionaryQuery);
    FTemplateDataset:=ABGetConstSqlPubDataset('ABSys_Org_FuncTemplate',[TABDictionaryQuery(DataSource.DataSet).FuncGuid,Name],TABDictionaryQuery);

    tempParentForm:=TForm(Owner);
    if (Assigned(tempParentForm)) and
       (tempParentForm.Name<>EmptyStr) then
    begin
      if AnsiCompareText(ABGetFieldValue(GetFuncControlPropertyDataset,
                                ['FP_ControlName','FP_PropertyName'],
                                [Name,'AddType'],
                                ['FP_PropertyValue'],'' ),
                         'daInsert')=0 then
        FAddType:=daInsert;
      if AnsiCompareText(ABGetFieldValue(GetFuncControlPropertyDataset,
                                ['FP_ControlName','FP_PropertyName'],
                                [Name,'ShowDBPanelInAdd'],
                                ['FP_PropertyValue'],'' ),
                         'True')=0 then
        FShowDBPanelInAdd:=True;
      if AnsiCompareText(ABGetFieldValue(GetFuncControlPropertyDataset,
                                ['FP_ControlName','FP_PropertyName'],
                                [Name,'ShowDBPanelInEdit'],
                                ['FP_PropertyValue'],'' ),
                         'True')=0 then
        FShowDBPanelInEdit:=True;

      tempFieldValue:=ABGetFieldValue(GetFuncControlPropertyDataset,
                                ['FP_ControlName','FP_PropertyName'],
                                [Name,'PrintOutType'],
                                ['FP_PropertyValue'],'' );
      if AnsiCompareText(tempFieldValue,'ptSelectPrint')=0 then
        FPrintOutType:=ptSelectPrint
      else if AnsiCompareText(tempFieldValue,'ptDirect')=0 then
        FPrintOutType:=ptDirect;

      FDefaultPrint:=ABGetFieldValue(GetFuncControlPropertyDataset,
                                ['FP_ControlName','FP_PropertyName'],
                                [Name,'DefaultPrint'],
                                ['FP_PropertyValue'],'' );

      if AnsiCompareText(ABGetFieldValue(GetFuncControlPropertyDataset,
                                ['FP_ControlName','FP_PropertyName'],
                                [Name,'PrintRange'],
                                ['FP_PropertyValue'],'' ),
                         'prCur')=0 then
        FPrintRange:=prCur;

      if FAddType=daAppend then
      begin
        FFrame.N3.Checked:=true;
      end
      else
      begin
        FFrame.N2.Checked:=true;
      end;
      FFrame.N4.Checked:=FShowDBPanelInAdd;
      FFrame.N6.Checked:=FShowDBPanelInEdit;
      if FPrintOutType=ptPreview then
      begin
        FFrame.N9.Checked:=true;
      end
      else if FPrintOutType=ptSelectPrint then
      begin
        FFrame.N10.Checked:=true;
      end
      else if FPrintOutType=ptDirect then
      begin
        FFrame.N11.Checked:=true;
      end;
      if FPrintRange=prCur then
      begin
        FFrame.N13.Checked:=true;
      end
      else if FPrintRange=prAll then
      begin
        FFrame.N14.Checked:=true;
      end;

      FFrame.N8.Clear;
      for I := 0 to printers.printer.printers.Count-1 do
      begin
        tempMenuItem:=TMenuItem.Create(self);
        tempMenuItem.Name:='PopupMenuReport_Print'+inttostr(i);
        tempMenuItem.Caption:=printers.printer.Printers[i];
        tempMenuItem.Hint:=printers.printer.Printers[i];
        tempMenuItem.GroupIndex:=3;
        tempMenuItem.RadioItem:=true;
        tempMenuItem.AutoCheck:=true;
        tempMenuItem.OnClick:=PopupMenuReport_DefaultPrint;
        FFrame.N8.Add(tempMenuItem);
        if AnsiCompareText(FDefaultPrint,tempMenuItem.Hint)=0 then
          tempMenuItem.Checked:=true;
      end;
    end;
    RefreshPopupMenuReport;
    RefreshTemplateItem;
  end;
end;

procedure TABDBNavigator.Notification(AComponent: TComponent;Operation: TOperation);
begin
  inherited;
  if (Operation = opRemove) then
  begin
    if (Assigned(FDataLink)) and (FDataLink.DataSource =AComponent)   then
      FDataLink.DataSource:=nil;
  end;
end;

procedure TABDBNavigator.Resize;
begin
  inherited;
end;

function TABDBNavigator.IsCustomButton(aButton:TABcxButton):Boolean;
var
  i:LongInt;
begin
  result:=false;
  for I := Low(FFrame.FCustomButtons) to High(FFrame.FCustomButtons) do
  begin
    if aButton=FFrame.FCustomButtons[i] then
    begin
      result:=true;
      Break;
    end;
  end;
end;

procedure TABDBNavigator.SetBigGlyph(const Value: boolean);
var
  i:LongInt;
  tempControl:TControl;
begin
  for I := 0 to FFrame.ControlCount-1 do
  begin
    tempControl:=FFrame.Controls[i];
    if (tempControl is TABcxButton) and
       (not IsCustomButton(TABcxButton(tempControl)))  then
    begin
      if Value then
        TABcxButton(tempControl).OptionsImage.Images:=FFrame.ImageList_32
      else
        TABcxButton(tempControl).OptionsImage.Images:=FFrame.ImageList_16;
    end;
  end;
  RefreshButtonsSize(true);
end;

procedure TABDBNavigator.SetImageLayout(const Value: TButtonLayout);
var
  i:LongInt;
  tempControl:TControl;
begin
  for I := 0 to FFrame.ControlCount-1 do
  begin
    tempControl:=FFrame.Controls[i];
    if tempControl is TABcxButton  then
    begin
      TABcxButton(tempControl).OptionsImage.Layout:=Value;
    end;
  end;
  RefreshButtonsSize(true);
end;

procedure TABDBNavigator.SetVisibleButtons(const Value: TABDBNavigatorButtonSet);
begin
  FVisibleButtons := Value;
  FBackVisibleButtons:=FVisibleButtons;
  RefreshButtonsSize(False);

  if (FLoaded) and
     (FButtonRangeType<>RtCustom) then
    FButtonRangeType:=RtCustom;
end;

procedure TABDBNavigator.ShowControl(AControl: TControl);
begin
  inherited;

end;

procedure TABDBNavigator.SetBtnEnabled(aButtons: TABDBNavigatorButtonSet);
begin
  aButtons:=aButtons+[nbCustomSpacer]  ;
  aButtons:=aButtons+[nbCustom1     ]  ;
  aButtons:=aButtons+[nbCustom2     ]  ;
  aButtons:=aButtons+[nbCustom3     ]  ;
  aButtons:=aButtons+[nbCustom4     ]  ;
  aButtons:=aButtons+[nbCustom5     ]  ;
  aButtons:=aButtons+[nbCustom6     ]  ;
  aButtons:=aButtons+[nbCustom7     ]  ;
  aButtons:=aButtons+[nbCustom8     ]  ;
  aButtons:=aButtons+[nbCustom9     ]  ;
  aButtons:=aButtons+[nbCustom10    ]  ;
  aButtons:=aButtons+[nbExitSpacer  ]  ;
  aButtons:=aButtons+[nbExit        ]  ;

  if Assigned(FOnUpdateEnabledButtons) then
    FOnUpdateEnabledButtons(Self,aButtons);

  FFrame.BtnInsert               .Enabled:=nbInsert in aButtons;
  FFrame.BtnCopy                 .Enabled:=nbCopy   in aButtons;
  FFrame.BtnDelete               .Enabled:=nbDelete in aButtons;
  FFrame.BtnEdit                 .Enabled:=nbEdit   in aButtons;

  FFrame.BtnPost                 .Enabled:=nbPost   in aButtons;
  FFrame.BtnCancel               .Enabled:=nbCancel in aButtons;

  FFrame.BtnQuery                .Enabled:=nbQuery  in aButtons;
  FFrame.BtnReport               .Enabled:=nbReport in aButtons;

  FFrame.BtnCustomSpacer       .Enabled:=nbCustomSpacer in aButtons;
  FFrame.BtnCustom1            .Enabled:=nbCustom1      in aButtons;
  FFrame.BtnCustom2            .Enabled:=nbCustom2      in aButtons;
  FFrame.BtnCustom3            .Enabled:=nbCustom3      in aButtons;
  FFrame.BtnCustom4            .Enabled:=nbCustom4      in aButtons;
  FFrame.BtnCustom5            .Enabled:=nbCustom5      in aButtons;
  FFrame.BtnCustom6            .Enabled:=nbCustom6      in aButtons;
  FFrame.BtnCustom7            .Enabled:=nbCustom7      in aButtons;
  FFrame.BtnCustom8            .Enabled:=nbCustom8      in aButtons;
  FFrame.BtnCustom9            .Enabled:=nbCustom9      in aButtons;
  FFrame.BtnCustom10           .Enabled:=nbCustom10     in aButtons;
  FFrame.BtnExitSpacer         .Enabled:=nbExitSpacer   in aButtons;
  FFrame.BtnExit               .Enabled:=nbExit         in aButtons;
end;

procedure TABDBNavigator.SetButtonControlType(
  const Value: TABButtonControlType);
begin
  FButtonControlType := Value;
  UpdateVisableButtons;
end;

procedure TABDBNavigator.SetButtonRangeType(const Value: TABButtonRangeType);
begin
  if Value=RtMain  then
  begin
    VisibleButtons:=
                  [
      nbInsertSpacer          ,
      nbInsert                ,
      nbCopy                  ,
      nbDelete                ,
      nbEdit                  ,

      nbPostSpacer            ,
      nbPost                  ,
      nbCancel                ,

      nbQuerySpacer           ,
      nbQuery                 ,
      nbReport               ,

      nbExitSpacer            ,
      nbExit
                  ];
  end
  else  if Value=RtDetail then
  begin
    VisibleButtons:=
                  [
      nbInsertSpacer          ,
      nbInsert                ,
      nbCopy                  ,
      nbDelete                ,
      nbEdit                  ,

      nbPostSpacer            ,
      nbPost                  ,
      nbCancel
                  ];
  end
  else  if Value=RtReportAndExit then
  begin
    VisibleButtons:=
                  [
    nbReport             ,
    nbExitSpacer         ,
    nbExit
                  ];
  end
  else  if Value=RtQueryAndPriNext then
  begin
    VisibleButtons:=
                  [
    nbFirstRecord           ,
    nbPreviousRecord        ,
    nbNextRecord            ,
    nbLastRecord            ,

    nbQuerySpacer           ,
    nbQuery

                  ];
  end

  else if Value=RtMainPage  then
  begin
    VisibleButtons:=
                  [
      nbInsertSpacer          ,
      nbInsert                ,
      nbCopy                  ,
      nbDelete                ,
      nbEdit                  ,

      nbPostSpacer            ,
      nbPost                  ,
      nbCancel                ,

      nbQuerySpacer           ,
      nbQuery
                  ];
  end
  else  if Value=RtAll then
  begin
    VisibleButtons:=
                  [
    nbFirstRecord           ,
    nbPreviousRecord        ,
    nbNextRecord            ,
    nbLastRecord            ,

    nbInsertSpacer          ,
    nbInsert                ,
    nbCopy                  ,
    nbDelete                ,
    nbEdit                  ,

    nbPostSpacer            ,
    nbPost                  ,
    nbCancel                ,

    nbQuerySpacer           ,
    nbQuery                 ,
    nbReport               ,

    nbCustomSpacer         ,
    nbCustom1            ,
    nbCustom2            ,
    nbCustom3            ,
    nbCustom4            ,
    nbCustom5            ,
    nbCustom6            ,
    nbCustom7            ,
    nbCustom8            ,
    nbCustom9            ,
    nbCustom10           ,

    nbExitSpacer            ,
    nbExit
                  ];
  end;
  FButtonRangeType := Value;
end;

procedure TABDBNavigator.RefreshPopupMenuReport;
var
  tempTopMenuItem,
  tempMenuItem:TMenuItem;
  tempName:string;
begin
  if not (nbReport in FVisibleButtons) then
    exit;

  tempTopMenuItem:=FFrame.N16;
  tempTopMenuItem.Clear;
  //**********************增加报表**************************
  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Name:='PopupMenuReport_Add';
  tempMenuItem.Caption:='增加';
  tempMenuItem.OnClick:=PopupMenuReport_Add;
  tempTopMenuItem.Add(tempMenuItem);

  if not ABDatasetIsEmpty(FReportDataset) then
  begin
    //**********************删除报表**************************
    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.Name:='PopupMenuReport_Del';
    tempMenuItem.Caption:='删除';
    tempMenuItem.OnClick:=PopupMenuReport_Del;
    tempTopMenuItem.Add(tempMenuItem);

    //**********************设计报表**************************
    //非admin,sysuser,不能设计报表
    if ABPubUser.IsAdminOrSysuser then
    begin
      tempMenuItem:=TMenuItem.Create(self);
      tempMenuItem.Name:='PopupMenuReport_Design';
      tempMenuItem.Caption:='设计';
      tempMenuItem.OnClick:=PopupMenuReport_Design;
      tempTopMenuItem.Add(tempMenuItem);
    end;

    //**********************修改报表属性**************************
    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.Name:='PopupMenuReport_EditProperty';
    tempMenuItem.Caption:='编辑属性';
    tempMenuItem.OnClick:=PopupMenuReport_EditProperty;
    tempTopMenuItem.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.Name:='PopupMenuReport_List';
    tempMenuItem.Caption:='报表列表';
    tempTopMenuItem.Add(tempMenuItem);
    FReportDataset.First;
    while not FReportDataset.Eof do
    begin
      tempName:=FReportDataset.FindField('Fr_Name').AsString;

      tempMenuItem:=TMenuItem.Create(self);
      tempMenuItem.Name:='PopupMenuReport_List_'+inttostr(FReportDataset.RecNo);
      tempMenuItem.Caption:=tempName;
      tempMenuItem.Hint:=tempName;
      tempMenuItem.GroupIndex:=1;
      tempMenuItem.RadioItem:=true;
      tempMenuItem.AutoCheck:=true;
      tempMenuItem.OnClick:=PopupMenuReport_List;
      tempTopMenuItem[tempTopMenuItem.Count-1].Add(tempMenuItem);

      if FReportDataset.FindField('Fr_IsDefault').AsBoolean then
      begin
        tempMenuItem.Checked := true;
      end;
      FReportDataset.Next;
    end;
  end;
end;

procedure TABDBNavigator.PopupMenuReport_Add(Sender: TObject);
var
  tempName:string;
  tempIsEmpty:boolean;
begin
  tempName:='新报表';
  if (ABInPutStr('报表名称',tempName)) and
     (tempName<>EmptyStr) then
  begin
    if FReportDataset.Locate('Fr_Name',tempName,[]) then
    begin
      ABShow('['+tempName+']已存在,请检查.');
    end
    else
    begin
      tempIsEmpty:=ABDatasetIsEmpty(FReportDataset);
      FReportDataset.Append;
      try
        FReportDataset.FindField('Fr_Fu_Guid').AsString:=TABDictionaryQuery(DataSource.DataSet).FuncGuid;
        FReportDataset.FindField('Fr_GroupName').AsString:=Name;
        FReportDataset.FindField('Fr_Name').AsString:=tempName;
        FReportDataset.FindField('Fr_IsDefault').AsBoolean:=tempIsEmpty;
        FReportDataset.Post;
        RefreshPopupMenuReport;
      except
        FReportDataset.Cancel;
        raise;
      end;
    end;
  end;
end;

function TABDBNavigator.PopupMenuReport_SelectName(aCaption:string):string;
var
  tempForm:TABSelectListBoxForm;
  tempName,
  tempDefaultName:string;
begin
  tempDefaultName:=emptystr;
  result:=emptystr;
  tempForm := TABSelectListBoxForm.Create(nil);
  try
    tempForm.Caption:=aCaption;
    tempForm.ListBox1.Clear;
    FReportDataset.First;
    while not FReportDataset.Eof do
    begin
      //非admin人员,只能删除非系统报表
      if (ABPubUser.IsAdmin) or
         (not FReportDataset.FindField('Fr_SysUse').AsBoolean) then
      begin
        tempName:=FReportDataset.FindField('Fr_Name').AsString;
        tempForm.ListBox1.Items.Add(tempName);

        if FReportDataset.FindField('Fr_IsDefault').AsBoolean then
        begin
          tempDefaultName:=tempName;
        end;
      end;
      FReportDataset.Next;
    end;

    if tempDefaultName<>emptystr then
    begin
      tempForm.ListBox1.ItemIndex:=tempForm.ListBox1.Items.IndexOf(tempDefaultName)
    end;

    if (tempForm.ShowModal=mrok) and (tempForm.ListBox1.ItemIndex>=0) then
    begin
      result:=tempForm.ListBox1.Items[tempForm.ListBox1.ItemIndex];
    end;
  finally
    tempForm.Free;
  end;
end;

function TABDBNavigator.PopupMenuTemplate_SelectName(aCaption:string):string;
var
  tempForm:TABSelectListBoxForm;
  tempName:string;
  tempDefaultName:string;
begin
  tempDefaultName:=emptystr;
  result:=emptystr;
  tempForm := TABSelectListBoxForm.Create(nil);
  try
    tempForm.Caption:=aCaption;
    tempForm.ListBox1.Clear;
    FTemplateDataset.First;
    while not FTemplateDataset.Eof do
    begin
      tempName:=FTemplateDataset.FindField('Ft_Name').AsString;
      tempForm.ListBox1.Items.Add(tempName);
      if FTemplateDataset.FindField('Ft_IsDefault').AsBoolean then
      begin
        tempDefaultName:=tempName;
      end;

      FTemplateDataset.Next;
    end;

    if tempDefaultName<>emptystr then
    begin
      tempForm.ListBox1.ItemIndex:=tempForm.ListBox1.Items.IndexOf(tempDefaultName)
    end;

    if (tempForm.ShowModal=mrok) and (tempForm.ListBox1.ItemIndex>=0) then
    begin
      result:=tempForm.ListBox1.Items[tempForm.ListBox1.ItemIndex];
    end;
  finally
    tempForm.Free;
  end;
end;

procedure TABDBNavigator.PopupMenuReport_DefaultPrint(Sender: TObject);
begin
  if TMenuItem(Sender).Checked then
    FDefaultPrint:=TMenuItem(Sender).Hint
  else
    FDefaultPrint:=emptystr;

  SetDatabasePropertyValue('DefaultPrint',ABIIF((TMenuItem(Sender).Hint=FDefaultPrint_Default) or
                                                (not (TMenuItem(Sender).Checked)),
                                                emptystr,
                                                TMenuItem(Sender).Hint));
end;

procedure TABDBNavigator.PopupMenuReport_Del(Sender: TObject);
var
  tempName:string;
begin
  tempName:=PopupMenuReport_SelectName('删除报表');
  if (tempName<>EmptyStr) and
     (FReportDataset.Locate('Fr_Name',tempName,[])) then
  begin
    FReportDataset.Delete;
    RefreshPopupMenuReport;
  end;
end;

procedure TABDBNavigator.PopupMenuReport_EditProperty(Sender: TObject);
var
  tempName:string;
begin
  tempName:=PopupMenuReport_SelectName('编辑报表属性');
  if (tempName<>EmptyStr) and
     (FReportDataset.Locate('Fr_Name',tempName,[])) then
  begin
    if ABShowEditFieldValue(FReportDataset,'','FR_Name,FR_OutType,FR_DefaultPrint,FR_PrintRange,FR_SysUse,FR_Order') then
    begin
      if FReportDataset.State in [dsEdit,dsInsert] then
        FReportDataset.Post;
      if FReportDataset.FindField('Fr_IsDefault').AsBoolean then
      begin
        PopupMenu_SetDefault(tempName,'Fr_Name','Fr_IsDefault',true,FReportDataset);
      end;
      RefreshPopupMenuReport;
    end;
  end;
end;

procedure TABDBNavigator.PopupMenuReport_Design(Sender: TObject);
var
  tempName:string;
begin
  tempName:=PopupMenuReport_SelectName('设计报表');
  if (tempName<>EmptyStr) and
     (FReportDataset.Locate('Fr_Name',tempName,[])) then
  begin
    ABPubFrxReport.Init;
    ABPubFrxReport.DefaultPrint  :=ABIIF(owner is TForm,ABIsNull(FDefaultPrint,TForm(owner).Caption),FDefaultPrint);
    ABPubFrxReport.CreateOwner:= owner;
    ABPubFrxReport.DataSource:= DataSource;
    ABPubFrxReport.ReportType:= rtFunc;
    ABPubFrxReport.DesignReport(FReportDataset.FindField('Fr_Guid').AsString,FPrintRange);
  end;
end;

procedure TABDBNavigator.PopupMenuReport_Print(Sender: TObject);
begin
  if not FReportDataset.Locate('Fr_IsDefault','1',[]) then
  begin
    FReportDataset.First;
  end;

  ABPubFrxReport.Init;
  ABPubFrxReport.DefaultPrint  :=ABIIF(owner is TForm,ABIsNull(FDefaultPrint,TForm(owner).Caption),FDefaultPrint);
  ABPubFrxReport.CreateOwner:= owner;
  ABPubFrxReport.DataSource:= DataSource;
  ABPubFrxReport.ReportType:= rtFunc;
  case FPrintOutType of
    ptPreview:
    begin
      ABPubFrxReport.PreviewReport(FReportDataset.FindField('Fr_Guid').AsString,FPrintRange);
    end;
    ptSelectPrint:
    begin
      ABPubFrxReport.PrintReport(FReportDataset.FindField('Fr_Guid').AsString,FPrintRange);
    end;
    ptDirect:
    begin
      ABPubFrxReport.PrintReport(FReportDataset.FindField('Fr_Guid').AsString,FPrintRange,False);
    end;
  end;
end;

procedure TABDBNavigator.RefreshTemplateItem;
var
  tempTopMenuItem,
  tempMenuItem:TMenuItem;
  tempName:string;
begin
  if not (nbInsert in FVisibleButtons) then
    exit;
  tempTopMenuItem:=FFrame.N5;
  tempTopMenuItem.Clear;

  //**********************新模板**************************
  tempMenuItem:=TMenuItem.Create(self);
  tempMenuItem.Name:='PopupMenuTemplate_Add';
  tempMenuItem.Caption:='增加';
  tempMenuItem.OnClick:=PopupMenuTemplate_Add;
  tempTopMenuItem.Add(tempMenuItem);

  if not ABDatasetIsEmpty(FTemplateDataset) then
  begin
    //**********************删除模板**************************
    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.Name:='PopupMenuTemplate_Del';
    tempMenuItem.Caption:='删除';
    tempMenuItem.OnClick:=PopupMenuTemplate_Del;
    tempTopMenuItem.Add(tempMenuItem);
    //**********************设计模板**************************
    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.Name:='PopupMenuTemplate_Design';
    tempMenuItem.Caption:='设计';
    tempMenuItem.OnClick:=PopupMenuTemplate_Design;
    tempTopMenuItem.Add(tempMenuItem);

    //**********************修改报表属性**************************
    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.Name:='PopupMenuTemplate_EditProperty';
    tempMenuItem.Caption:='编辑属性';
    tempMenuItem.OnClick:=PopupMenuTemplate_EditProperty;
    tempTopMenuItem.Add(tempMenuItem);

    tempMenuItem:=TMenuItem.Create(self);
    tempMenuItem.Name:='PopupMenuTemplate_List';
    tempMenuItem.Caption:='模板列表';
    tempTopMenuItem.Add(tempMenuItem);

    if (Assigned(DataSource)) and
       (ABCheckDataSetInActive(DataSource.DataSet)) then
      TABDictionaryQuery(DataSource.Dataset).FieldDefaultValuesTemplate.Clear;
    FTemplateDataset.First;
    while not FTemplateDataset.Eof do
    begin
      tempName:=FTemplateDataset.FindField('Ft_Name').AsString;
      tempMenuItem:=TMenuItem.Create(self);
      tempMenuItem.Name:='PopupMenuTemplate_List_'+inttostr(FTemplateDataset.RecNo);
      tempMenuItem.Caption:=tempName;
      tempMenuItem.Hint:=tempName;
      tempMenuItem.GroupIndex:=2;
      tempMenuItem.RadioItem:=true;
      tempMenuItem.AutoCheck:=true;
      tempMenuItem.OnClick:=PopupMenuTemplate_List;
      tempTopMenuItem[tempTopMenuItem.Count-1].Add(tempMenuItem);

      if FTemplateDataset.FindField('FT_IsDefault').AsBoolean then
      begin
        tempMenuItem.Checked := true;
        if (Assigned(DataSource)) and
           (Assigned(DataSource.DataSet)) then
          TABDictionaryQuery(DataSource.DataSet).FieldDefaultValuesTemplate.Text:=
              ABDownToTextFromField('Main',
                              'ABSys_Org_FuncTemplate','Ft_FieldNameValue',
                              'Ft_Guid='+QuotedStr(FTemplateDataset.FindField('Ft_Guid').AsString));
      end;
      FTemplateDataset.Next;
    end;
  end;
end;

procedure TABDBNavigator.PopupMenuTemplate_Add(Sender: TObject);
var
  tempIsEmpty:boolean;
  tempName,tempLeftValue,tempRightValue:string;
  tempDataSet:TDataset;
  I: Integer;
  tempStrings:TStrings;
begin
  if (Assigned(DataSource)) and
     (ABCheckDataSetInActive(DataSource.DataSet)) then
  begin
    tempDataSet:=DataSource.DataSet;
    tempLeftValue:=EmptyStr;
    tempRightValue:=EmptyStr;
    for i := 0 to tempDataSet.FieldCount - 1 do
    begin
      if ABHaveChinese(tempDataSet.Fields[i].DisplayLabel) then
      begin
        ABAddstr(tempLeftValue,tempDataSet.Fields[i].DisplayLabel);
      end;
    end;

    if (ABSelectStr(tempLeftValue,tempRightValue,',','请选择新模板的字段')) then
    begin
      tempStrings:= TStringlist.Create;
      try
        tempStrings.Clear;
        tempRightValue:=ABGetFieldNamesByDisplayLabels(tempDataSet,tempRightValue);
        for I := 1 to ABGetSpaceStrCount(tempRightValue, ',') do
        begin
          tempName := ABGetSpaceStr(tempRightValue, i, ',');
          tempStrings.Add(tempName+'='+tempDataSet.FieldByName(tempName).AsString);
        end;

        tempName:='新模板';
        if (ABInPutStr('新模板名称',tempName)) and
           (tempName<>EmptyStr) then
        begin
          if FTemplateDataset.Locate('Ft_Name',tempName,[]) then
          begin
            ABShow('['+tempName+']已存在,请检查.');
          end
          else
          begin
            tempIsEmpty:=ABDatasetIsEmpty(FTemplateDataset);
            try
              FTemplateDataset.Append;
              FTemplateDataset.FindField('FT_IsDefault').AsBoolean:=tempIsEmpty;
              FTemplateDataset.FindField('Ft_Fu_Guid').AsString:=TABDictionaryQuery(DataSource.DataSet).FuncGuid;
              FTemplateDataset.FindField('Ft_GroupName').AsString:=Name;
              FTemplateDataset.FindField('Ft_Name').AsString:=tempName;
              FTemplateDataset.Post;
              ABUpTextToField('Main',
                            'ABSys_Org_FuncTemplate','Ft_FieldNameValue',
                            'Ft_Guid='+QuotedStr(FTemplateDataset.FindField('Ft_Guid').AsString),
                            tempStrings.Text);
              RefreshTemplateItem;
            except
              FTemplateDataset.Cancel;
              raise;
            end;
          end;
        end;
      finally
        tempStrings.Free;
      end;
    end;
  end;
end;

procedure TABDBNavigator.PopupMenuTemplate_Del(Sender: TObject);
var
  tempName:string;
begin
  tempName:=PopupMenuTemplate_SelectName('删除模板');
  if (tempName<>EmptyStr) and
     (FTemplateDataset.Locate('Ft_Name',tempName,[])) then
  begin
    FTemplateDataset.Delete;
    RefreshTemplateItem;
  end;
end;

procedure TABDBNavigator.PopupMenu_SetDefault(aName,aNameFieldName,aIsDefaultFieldName: string;aDefaultValue:boolean;aDataset:TDataset);
begin
  aDataset.First;
  while not aDataset.Eof do
  begin
    if AnsiCompareText(aDataset.FieldByName(aNameFieldName).AsString,aName)=0 then
    begin
      ABSetFieldValue(aDefaultValue,aDataset.FindField(aIsDefaultFieldName),False);
    end
    else
    begin
      ABSetFieldValue(false,aDataset.FindField(aIsDefaultFieldName),False);
    end;

    aDataset.Next;
  end;
end;

procedure TABDBNavigator.PopupMenuTemplate_EditProperty(Sender: TObject);
var
  tempName:string;
begin
  tempName:=PopupMenuTemplate_SelectName('编辑模板属性');
  if (tempName<>EmptyStr) and
     (FTemplateDataset.Locate('Ft_Name',tempName,[])) then
  begin
    if ABShowEditFieldValue(FTemplateDataset,'','FT_Name,FT_IsDefault,FT_SysUse,FT_Order') then
    begin
      if FTemplateDataset.State in [dsEdit,dsInsert] then
        FTemplateDataset.Post;
      if FTemplateDataset.FindField('FT_IsDefault').AsBoolean then
      begin
        PopupMenu_SetDefault(tempName,'Ft_Name','Ft_IsDefault',true,FTemplateDataset);
      end;
      RefreshTemplateItem;
    end;
  end;
end;

procedure TABDBNavigator.PopupMenuTemplate_Design(Sender: TObject);
var
  tempName:string;
  tempForm:TABMemoForm;
  tempABActiveTemplateStr,
  tempItemStr,tempLeftStr,tempRightStr:string;
  I: Integer;
  tempField:tfield;
  tempFieldDef:PABFieldDef;
begin
  tempName:=PopupMenuTemplate_SelectName('设计模板');
  if (tempName<>EmptyStr) and
     (FTemplateDataset.Locate('Ft_Name',tempName,[])) then
  begin
    tempForm := TABMemoForm.Create(nil);
    try
      tempForm.Caption:=('设计模板');
      tempForm.Memo1.Lines.Text:= ABDownToTextFromField('Main',
                                                        'ABSys_Org_FuncTemplate','Ft_FieldNameValue',
                                                        'Ft_Guid='+QuotedStr(FTemplateDataset.FindField('Ft_Guid').AsString));
      //将模板中的显示与保存不同的项翻译一下
      for I := 0 to tempForm.Memo1.Lines.Count - 1 do
      begin
        tempItemStr:=tempForm.Memo1.Lines[i];
        tempLeftStr:=ABGetLeftRightStr(tempItemStr,axdLeft);
        tempRightStr:= ABGetLeftRightStr(tempItemStr,axdRight);
        tempField:= DataSource.DataSet.FindField(tempLeftStr);
        tempFieldDef:= ABFieldDefByFieldName(DataSource.DataSet,tempLeftStr);

        if (Assigned(tempField)) then
        begin
          tempLeftStr:=tempField.DisplayLabel;
          if (Assigned(tempFieldDef)) and (Assigned(tempFieldDef.PDownDef)) and
             (AnsiCompareText(tempFieldDef.PDownDef.Fi_ViewField,tempFieldDef.PDownDef.Fi_SaveField)<>0) then
          begin
            tempRightStr:=VarToStrDef(
                            tempFieldDef.PDownDef.Fi_DataSet.Lookup(
                                                                    tempFieldDef.PDownDef.Fi_SaveField,
                                                                    tempRightStr,
                                                                    ABStringReplace(tempFieldDef.PDownDef.Fi_ViewField,',',';')
                                                                        ),
                             tempRightStr);
          end;
        end;
        tempForm.Memo1.Lines[i]:=tempLeftStr+'='+tempRightStr;
      end;
      tempABActiveTemplateStr:=tempForm.Memo1.Lines.Text;
      if tempForm.ShowModal=mrok then
      begin
        if tempForm.Memo1.Lines.Text<>tempABActiveTemplateStr then
        begin
          //将模板中的显示与保存不同的项翻译一下
          for I := 0 to tempForm.Memo1.Lines.Count - 1 do
          begin
            tempItemStr:=tempForm.Memo1.Lines[i];
            tempLeftStr:=ABGetLeftRightStr(tempItemStr,axdLeft);
            tempRightStr:= ABGetLeftRightStr(tempItemStr,axdRight);
            tempLeftStr:= ABGetFieldNamesByDisplayLabels(DataSource.DataSet,tempLeftStr);

            tempField:= DataSource.DataSet.FindField(tempLeftStr);
            tempFieldDef:= ABFieldDefByFieldName(DataSource.DataSet,tempLeftStr);

            if (Assigned(tempField)) then
            begin
              if (Assigned(tempFieldDef)) and (Assigned(tempFieldDef.PDownDef)) and
                 (AnsiCompareText(tempFieldDef.PDownDef.Fi_ViewField,tempFieldDef.PDownDef.Fi_SaveField)<>0) then
              begin
                tempRightStr:=VarToStrDef(
                                tempFieldDef.PDownDef.Fi_DataSet.Lookup(
                                                                        ABStringReplace(tempFieldDef.PDownDef.Fi_ViewField,',',';'),
                                                                        tempRightStr,
                                                                        tempFieldDef.PDownDef.Fi_SaveField
                                                                            ),
                                 tempRightStr);
              end;
            end;
            tempForm.Memo1.Lines[i]:=tempLeftStr+'='+tempRightStr;
          end;
          ABUpTextToField('Main',
                        'ABSys_Org_FuncTemplate','Ft_FieldNameValue',
                        'Ft_Guid='+QuotedStr(FTemplateDataset.FindField('Ft_Guid').AsString),
                        tempForm.Memo1.Lines.Text);

          RefreshTemplateItem;
        end;
      end;
    finally
      tempForm.Free;
    end;
  end;
end;

procedure TABDBNavigator.PopupMenuTemplate_List(Sender: TObject);
begin
  PopupMenu_SetDefault(TMenuItem(Sender).Hint,'Ft_Name','Ft_IsDefault',TMenuItem(Sender).Checked,FTemplateDataset);
  RefreshTemplateItem;
end;

procedure TABDBNavigator.PopupMenuReport_List(Sender: TObject);
begin
  PopupMenu_SetDefault(TMenuItem(Sender).Hint,'Fr_Name','Fr_IsDefault',TMenuItem(Sender).Checked,FReportDataset);
  RefreshPopupMenuReport;
end;

procedure TABDBNavigator.RefreshButtonsSize(aOnlyDesigning:Boolean);
  procedure DOSetLeft(aControl:TControl;aLeft:longint);
  begin
    if TControl(aControl).Left<>aLeft then
      aControl.Left:=aLeft;
  end;
  function IsVisibleButton(aControl:TControl):Boolean;
  begin
    result:=
        (aControl=Frame.BtnFirstRecord    ) and (nbFirstRecord    in FVisibleButtons) or
        (aControl=Frame.BtnPreviousRecord ) and (nbPreviousRecord in FVisibleButtons) or
        (aControl=Frame.BtnNextRecord     ) and (nbNextRecord     in FVisibleButtons) or
        (aControl=Frame.BtnLastRecord     ) and (nbLastRecord     in FVisibleButtons) or
        (aControl=Frame.BtnInsertSpacer   ) and (nbInsertSpacer   in FVisibleButtons) or
        (aControl=Frame.BtnInsert         ) and (nbInsert         in FVisibleButtons) or
        (aControl=Frame.BtnCopy           ) and (nbCopy           in FVisibleButtons) or
        (aControl=Frame.BtnDelete         ) and (nbDelete         in FVisibleButtons) or
        (aControl=Frame.BtnEdit           ) and (nbEdit           in FVisibleButtons) or
        (aControl=Frame.BtnPostSpacer     ) and (nbPostSpacer     in FVisibleButtons) or
        (aControl=Frame.BtnPost           ) and (nbPost           in FVisibleButtons) or
        (aControl=Frame.BtnCancel         ) and (nbCancel         in FVisibleButtons) or
        (aControl=Frame.BtnQuerySpacer    ) and (nbQuerySpacer    in FVisibleButtons) or
        (aControl=Frame.BtnQuery          ) and (nbQuery          in FVisibleButtons) or
        (aControl=Frame.BtnReport         ) and (nbReport         in FVisibleButtons) or
        (aControl=Frame.btnCustomSpacer   ) and (nbCustomSpacer   in FVisibleButtons) or
        (aControl=Frame.btnCustom1        ) and (nbCustom1        in FVisibleButtons) or
        (aControl=Frame.btnCustom2        ) and (nbCustom2        in FVisibleButtons) or
        (aControl=Frame.btnCustom3        ) and (nbCustom3        in FVisibleButtons) or
        (aControl=Frame.btnCustom4        ) and (nbCustom4        in FVisibleButtons) or
        (aControl=Frame.btnCustom5        ) and (nbCustom5        in FVisibleButtons) or
        (aControl=Frame.btnCustom6        ) and (nbCustom6        in FVisibleButtons) or
        (aControl=Frame.btnCustom7        ) and (nbCustom7        in FVisibleButtons) or
        (aControl=Frame.btnCustom8        ) and (nbCustom8        in FVisibleButtons) or
        (aControl=Frame.btnCustom9        ) and (nbCustom9        in FVisibleButtons) or
        (aControl=Frame.btnCustom10       ) and (nbCustom10       in FVisibleButtons) or
        (aControl=Frame.BtnExitSpacer     ) and (nbExitSpacer     in FVisibleButtons) or
        (aControl=Frame.BtnExit           ) and (nbExit           in FVisibleButtons);
  end;
var
  tempControl:TControl;
  i,
  tempWidth,tempWidthADD:LongInt;
begin
  if (not aOnlyDesigning) or
     (aOnlyDesigning) and (csDesigning in ComponentState)  then
  begin
    tempWidthADD:=0;
    if BigGlyph then
    begin
      tempWidthADD:=tempWidthADD+15;
    end
    else
    begin
      tempWidthADD:=tempWidthADD+0;
    end;

    if (ImageLayout=blGlyphLeft) or (ImageLayout=blGlyphRight) then
    begin
      tempWidthADD:=tempWidthADD+0;
    end
    else
    begin
      tempWidthADD:=tempWidthADD+18;
    end;

    for I := 0 to FFrame.ControlCount-1 do
    begin
      tempControl:=FFrame.Controls[i];
      if tempControl is TControl  then
      begin
        if TControl(tempControl).Height<>Height then
          TControl(tempControl).Height:=Height;
      end;
    end;

    for I := 0 to FFrame.ControlCount-1 do
    begin
      tempControl:=FFrame.Controls[i];
      if tempControl is TControl  then
      begin
        if not IsVisibleButton(tempControl) then
        begin
          tempWidth:=0;
        end
        else
        begin
          if tempControl is TABcxButton  then
          begin
            tempWidth:=ABIIF(TABcxButton(tempControl).Kind=cxbkDropDownButton,16,0)+
                                          ABIIF(TABcxButton(tempControl).OptionsImage.ImageIndex>=0,18,0)+
                                          length(TABcxButton(tempControl).Caption)*16+
                                          tempWidthADD;

            if IsCustomButton(TABcxButton(tempControl)) then
            begin
              tempWidth:= ABIIF(tempWidth<64,64,tempWidth);
            end;
          end
          else
          begin
            tempWidth:=1;
          end;
        end;

        if TControl(tempControl).Width<>tempWidth then
          TControl(tempControl).Width:=tempWidth;
      end;
    end;

    DOSetLeft(Frame.BtnFirstRecord      ,0);
    DOSetLeft(Frame.BtnPreviousRecord   ,Frame.BtnFirstRecord    .left+Frame.BtnFirstRecord   .width);
    DOSetLeft(Frame.BtnNextRecord       ,Frame.BtnPreviousRecord .left+Frame.BtnPreviousRecord.width);
    DOSetLeft(Frame.BtnLastRecord       ,Frame.BtnNextRecord     .left+Frame.BtnNextRecord    .width);
    DOSetLeft(Frame.BtnInsertSpacer     ,Frame.BtnLastRecord     .left+Frame.BtnLastRecord    .width);
    DOSetLeft(Frame.BtnInsert           ,Frame.BtnInsertSpacer   .left+Frame.BtnInsertSpacer  .width);
    DOSetLeft(Frame.BtnCopy             ,Frame.BtnInsert         .left+Frame.BtnInsert        .width);
    DOSetLeft(Frame.BtnDelete           ,Frame.BtnCopy           .left+Frame.BtnCopy          .width);
    DOSetLeft(Frame.BtnEdit             ,Frame.BtnDelete         .left+Frame.BtnDelete        .width);
    DOSetLeft(Frame.BtnPostSpacer       ,Frame.BtnEdit           .left+Frame.BtnEdit          .width);
    DOSetLeft(Frame.BtnPost             ,Frame.BtnPostSpacer     .left+Frame.BtnPostSpacer    .width);
    DOSetLeft(Frame.BtnCancel           ,Frame.BtnPost           .left+Frame.BtnPost          .width);
    DOSetLeft(Frame.BtnQuerySpacer      ,Frame.BtnCancel         .left+Frame.BtnCancel        .width);
    DOSetLeft(Frame.BtnQuery            ,Frame.BtnQuerySpacer    .left+Frame.BtnQuerySpacer   .width);
    DOSetLeft(Frame.BtnReport           ,Frame.BtnQuery          .left+Frame.BtnQuery         .width);
    DOSetLeft(Frame.BtnCustomSpacer     ,Frame.BtnReport         .left+Frame.BtnReport        .width);
    DOSetLeft(Frame.BtnCustom1          ,Frame.btnCustomSpacer   .left+Frame.btnCustomSpacer  .width);
    DOSetLeft(Frame.BtnCustom2          ,Frame.btnCustom1        .left+Frame.btnCustom1       .width);
    DOSetLeft(Frame.BtnCustom3          ,Frame.btnCustom2        .left+Frame.btnCustom2       .width);
    DOSetLeft(Frame.BtnCustom4          ,Frame.btnCustom3        .left+Frame.btnCustom3       .width);
    DOSetLeft(Frame.BtnCustom5          ,Frame.btnCustom4        .left+Frame.btnCustom4       .width);
    DOSetLeft(Frame.BtnCustom6          ,Frame.btnCustom5        .left+Frame.btnCustom5       .width);
    DOSetLeft(Frame.BtnCustom7          ,Frame.btnCustom6        .left+Frame.btnCustom6       .width);
    DOSetLeft(Frame.BtnCustom8          ,Frame.btnCustom7        .left+Frame.btnCustom7       .width);
    DOSetLeft(Frame.BtnCustom9          ,Frame.btnCustom8        .left+Frame.btnCustom8       .width);
    DOSetLeft(Frame.BtnCustom10         ,Frame.btnCustom9        .left+Frame.btnCustom9       .width);
    DOSetLeft(Frame.BtnExitSpacer       ,Frame.btnCustom10       .left+Frame.btnCustom10      .width);
    DOSetLeft(Frame.BtnExit             ,Frame.BtnExitSpacer     .left+Frame.BtnExitSpacer    .width);

  end;
end;

procedure TABDBNavigator.UpdateVisableButtons;
var
  tempDataSet:TABDictionaryQuery;
begin
  if (Assigned(FDataLink.DataSource)) and
     (Assigned(FDataLink.DataSource.DataSet)) and
     (FDataLink.DataSource.DataSet is TABDictionaryQuery) and
     (FDataLink.DataSource.DataSet.Active) then
  begin
    tempDataSet:=TABDictionaryQuery(DataSource.DataSet);

    FVisibleButtons:=FBackVisibleButtons;
    if tempDataSet.CanInsert then
    begin
    end
    else
    begin
      FVisibleButtons:=FVisibleButtons-[nbInsertSpacer,nbInsert,nbCopy];
    end;
    if TABDictionaryQuery(tempDataSet).CanEdit then
    begin
    end
    else
    begin
      FVisibleButtons:=FVisibleButtons-[nbEdit];
    end;

    if (tempDataSet.CanInsert) or (tempDataSet.CanEdit) then
    begin
    end
    else
    begin
      FVisibleButtons:=FVisibleButtons-[nbPostSpacer,nbPost,nbCancel];
    end;

    if TABDictionaryQuery(tempDataSet).CanDelete then
    begin
    end
    else
    begin
      FVisibleButtons:=FVisibleButtons-[nbDelete];
    end;

    if (TABDictionaryQuery(tempDataSet).CanPrint)  then
    begin
    end
    else
    begin
      FVisibleButtons:=FVisibleButtons-[nbReport];
    end;

    if FButtonControlType=ctFromMain then
    begin
      if (Assigned(tempDataset.DataSource)) and
         (Assigned(tempDataset.DataSource.DataSet)) then
      begin
        FVisibleButtons:=FVisibleButtons-[nbEdit,nbPostSpacer,nbPost,nbCancel];
      end;
    end;

    if Assigned(FOnUpdateVisableButtons) then
      FOnUpdateVisableButtons(Self,FVisibleButtons);

    RefreshButtonsSize(true);
  end;
end;

procedure TABDBNavigator.UpdateEnabledButtons;
var
  tempDataSet:TABDictionaryQuery;

  tempBtnInsertEnabled,
  tempBtnCopyEnabled,
  tempBtnDeleteEnabled,
  tempBtnEditEnabled,

  tempBtnPostEnabled,
  tempBtnCancelEnabled,

  tempBtnQueryEnabled,
  tempBtnReportEnabled:boolean;

  procedure UpdateEnabled;
    procedure SetEnabled(aButton:TABcxButton;aSetEnabled:Boolean);
    begin
      if aButton.Enabled<>aSetEnabled then
        aButton.Enabled:=aSetEnabled;
    end;
  var
    tempButtons: TABDBNavigatorButtonSet;
  begin
    tempButtons:=[];
    if tempBtnInsertEnabled then
      tempButtons:=tempButtons+[nbInsert] ;
    if tempBtnCopyEnabled then
      tempButtons:=tempButtons+[nbCopy] ;
    if tempBtnDeleteEnabled then
      tempButtons:=tempButtons+[nbDelete] ;
    if tempBtnEditEnabled then
      tempButtons:=tempButtons+[nbEdit] ;
    if tempBtnPostEnabled then
      tempButtons:=tempButtons+[nbPost] ;
    if tempBtnCancelEnabled then
      tempButtons:=tempButtons+[nbCancel] ;
    if tempBtnQueryEnabled then
      tempButtons:=tempButtons+[nbQuery] ;
    if tempBtnReportEnabled then
      tempButtons:=tempButtons+[nbReport] ;

    SetBtnEnabled(tempButtons);
  end;
begin
  tempBtnInsertEnabled        :=False;
  tempBtnCopyEnabled          :=False;
  tempBtnDeleteEnabled        :=False;
  tempBtnEditEnabled          :=False;

  tempBtnPostEnabled          :=False;
  tempBtnCancelEnabled        :=False;

  tempBtnQueryEnabled         :=False;
  tempBtnReportEnabled        :=False;
  if (Assigned(FDataLink.DataSource)) and
     (Assigned(FDataLink.DataSource.DataSet)) and
     (FDataLink.DataSource.DataSet is TABDictionaryQuery) and
     (FDataLink.DataSource.DataSet.Active) then
  begin
    tempDataSet:=TABDictionaryQuery(DataSource.DataSet);

    tempBtnInsertEnabled        :=True;
    tempBtnQueryEnabled:=true;
    if not ABDatasetIsEmpty(tempDataSet) then
    begin
      if (tempDataSet.State in [dsEdit,dsInsert])  then
      begin
        if (nbPost in FVisibleButtons) then tempBtnPostEnabled:=true;
        if (nbCancel in FVisibleButtons) then tempBtnCancelEnabled:=true;
      end
      else
      begin
        if (nbEdit in FVisibleButtons) then tempBtnEditEnabled:=true;
      end;

      tempBtnDeleteEnabled:=True;
      tempBtnCopyEnabled:=True;
      tempBtnReportEnabled:=True;
    end;

    if FButtonControlType=ctFromMain then
    begin
      if (Assigned(tempDataset.DataSource)) and
         (Assigned(tempDataset.DataSource.DataSet)) then
      begin
        tempBtnEditEnabled  :=False;
        tempBtnPostEnabled  :=False;
        tempBtnCancelEnabled:=False;
      end
      else
      begin
        ABSetDatasetEditState(tempDataset,(tempDataSet.State in [dsEdit,dsInsert]));
      end;
    end;

    UpdateEnabled;
  end
  else
  begin
    SetBtnEnabled([]);
  end;
end;

procedure ABSetDatasetEditState(aDataSet:TDataSet;aCanEdit:Boolean);
var
  i:LongInt;
  tempList:TList;
begin
  if (Assigned(aDataSet)) and
     (Assigned(aDataSet.Owner)) and
     (aDataSet.Owner is TForm) then
  begin
    tempList := TList.Create;
    try
      //取得设置的数据集列表
      ABGetDetailDatasetList(aDataSet,tempList,False);
      if tempList.Count>0  then
      begin
        ABPostDataset(aDataSet);
        if (not aCanEdit) then
        begin
          for I := 0 to tempList.Count - 1 do
          begin
            ABPostDataset(TDataSet(tempList[i]));
          end;
        end;

        for I := 0 to aDataSet.Owner.ComponentCount - 1 do
        begin
          if (aDataSet.Owner.Components[i] is  TABDBNavigator) and
             (Assigned(TABDBNavigator(aDataSet.Owner.Components[i]).DataSource)) and
             (Assigned(TABDBNavigator(aDataSet.Owner.Components[i]).DataSource.DataSet)) and
             (tempList.IndexOf(TABDBNavigator(aDataSet.Owner.Components[i]).DataSource.DataSet)>=0) then
          begin
            TABDBNavigator(aDataSet.Owner.Components[i]).DataSource.AutoEdit:= aCanEdit;
            if aCanEdit then
            begin
              TABDBNavigator(aDataSet.Owner.Components[i]).UpdateEnabledButtons;
            end
            else
            begin
              TABDBNavigator(aDataSet.Owner.Components[i]).SetBtnEnabled([]);
            end;
          end
          else if  (aDataSet.Owner.Components[i] is  TABcxGridDBBandedTableView) and
                   (Assigned(TABcxGridDBBandedTableView(aDataSet.Owner.Components[i]).DataController)) and
                   (Assigned(TABcxGridDBBandedTableView(aDataSet.Owner.Components[i]).DataController.DataSource)) and
                   (Assigned(TABcxGridDBBandedTableView(aDataSet.Owner.Components[i]).DataController.DataSource.DataSet)) and
                   (tempList.IndexOf(TABcxGridDBBandedTableView(aDataSet.Owner.Components[i]).DataController.DataSource.DataSet)>=0) then
          begin
            TABcxGridDBBandedTableView(aDataSet.Owner.Components[i]).DataController.DataSource.AutoEdit:= aCanEdit;
            TABcxGridDBBandedTableView(aDataSet.Owner.Components[i]).SetAutoEdit;
            if (Assigned(TABcxGridDBBandedTableView(aDataSet.Owner.Components[i]).Control)) and
               (TABcxGridDBBandedTableView(aDataSet.Owner.Components[i]).Control is TcxGrid) then
            begin
              TcxGrid(TABcxGridDBBandedTableView(aDataSet.Owner.Components[i]).Control).Enabled:=aCanEdit;
            end;
          end
          else if (aDataSet.Owner.Components[i] is  TABDBPanel) and
                  (Assigned(TABDBPanel(aDataSet.Owner.Components[i]).DataSource)) and
                  (Assigned(TABDBPanel(aDataSet.Owner.Components[i]).DataSource.DataSet)) and
                  (tempList.IndexOf(TABDBPanel(aDataSet.Owner.Components[i]).DataSource.DataSet)>=0) then
          begin
            TABDBPanel(aDataSet.Owner.Components[i]).DataSource.AutoEdit:= aCanEdit;
            TABDBPanel(aDataSet.Owner.Components[i]).Enabled:=aCanEdit;
          end
        end;
      end;
    finally
      tempList.Free;
    end;
  end;
end;



end.

