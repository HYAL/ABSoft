object ABFramkWorkBillInputForm: TABFramkWorkBillInputForm
  Left = 0
  Top = 0
  Caption = #23548#20837#21333#25454#26597#35810
  ClientHeight = 550
  ClientWidth = 800
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 510
    Width = 800
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Panel2: TPanel
      Left = 408
      Top = 0
      Width = 392
      Height = 40
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        392
        40)
      object Button1: TABcxButton
        Left = 294
        Top = 8
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #21462#28040
        LookAndFeel.Kind = lfFlat
        TabOrder = 1
        OnClick = Button1Click
        ShowProgressBar = False
        ExplicitLeft = 86
      end
      object Button2: TABcxButton
        Left = 214
        Top = 8
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #30830#23450
        Default = True
        LookAndFeel.Kind = lfFlat
        TabOrder = 0
        OnClick = Button2Click
        ShowProgressBar = False
        ExplicitLeft = 6
      end
    end
    object ABcxRadioGroup1: TABcxRadioGroup
      Left = 0
      Top = 0
      Align = alClient
      Alignment = alCenterCenter
      BiDiMode = bdLeftToRight
      Ctl3D = True
      ParentBackground = False
      ParentBiDiMode = False
      ParentCtl3D = False
      Properties.Columns = 3
      Properties.DefaultValue = 0
      Properties.Items = <
        item
          Caption = #19968#23545#19968#29983#25104#26032#21333#25454
        end
        item
          Caption = #22810#23545#19968#29983#25104#26032#21333#25454
        end
        item
          Caption = #36861#21152#21040#24403#21069#26126#32454
        end>
      ItemIndex = 0
      Style.BorderStyle = ebsNone
      Style.Shadow = False
      Style.TransparentBorder = True
      TabOrder = 1
      ExplicitWidth = 497
      Height = 40
      Width = 408
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 27
    Width = 800
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label5: TLabel
      Left = 25
      Top = 10
      Width = 65
      Height = 13
      AutoSize = False
      Caption = #26085#26399#33539#22260#20174
    end
    object Label6: TLabel
      Left = 220
      Top = 10
      Width = 12
      Height = 13
      AutoSize = False
      Caption = #21040
    end
    object PriBillName: TLabel
      Left = 376
      Top = 10
      Width = 54
      Height = 13
      AutoSize = False
      Caption = #21333#25454#32534#21495' '
    end
    object Button3: TButton
      Left = 662
      Top = 6
      Width = 75
      Height = 24
      Caption = #26597#35810
      TabOrder = 0
      OnClick = Button3Click
    end
    object ABcxDateTimeEdit1: TABcxDateTimeEdit
      Left = 88
      Top = 7
      Properties.Kind = ckDateTime
      TabOrder = 1
      Width = 128
    end
    object ABcxDateTimeEdit2: TABcxDateTimeEdit
      Left = 236
      Top = 7
      Properties.Kind = ckDateTime
      TabOrder = 2
      Width = 128
    end
    object ABcxTextEdit1: TABcxTextEdit
      Left = 428
      Top = 7
      TabOrder = 3
      Width = 189
    end
  end
  object ABDBNavigator1: TABDBNavigator
    Left = 0
    Top = 0
    Width = 800
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    ShowCaption = False
    TabOrder = 2
    BigGlyph = False
    ImageLayout = blGlyphLeft
    DataSource = ABDatasource1
    VisibleButtons = [nbFirstRecord, nbPreviousRecord, nbNextRecord, nbLastRecord, nbQuerySpacer, nbQuery, nbReport, nbExitSpacer, nbExit]
    ButtonRangeType = RtCustom
    BtnCustom1ImageIndex = -1
    BtnCustom2ImageIndex = -1
    BtnCustom3ImageIndex = -1
    BtnCustom4ImageIndex = -1
    BtnCustom5ImageIndex = -1
    BtnCustom6ImageIndex = -1
    BtnCustom7ImageIndex = -1
    BtnCustom8ImageIndex = -1
    BtnCustom9ImageIndex = -1
    BtnCustom10ImageIndex = -1
    BtnCustom1Caption = #33258#23450#20041'1'
    BtnCustom2Caption = #33258#23450#20041'2'
    BtnCustom3Caption = #33258#23450#20041'3'
    BtnCustom4Caption = #33258#23450#20041'4'
    BtnCustom5Caption = #33258#23450#20041'5'
    BtnCustom6Caption = #33258#23450#20041'6'
    BtnCustom7Caption = #33258#23450#20041'7'
    BtnCustom8Caption = #33258#23450#20041'8'
    BtnCustom9Caption = #33258#23450#20041'9'
    BtnCustom10Caption = #33258#23450#20041'10'
    BtnCustom1Kind = cxbkStandard
    BtnCustom2Kind = cxbkStandard
    BtnCustom3Kind = cxbkStandard
    BtnCustom4Kind = cxbkStandard
    BtnCustom5Kind = cxbkStandard
    BtnCustom6Kind = cxbkStandard
    BtnCustom7Kind = cxbkStandard
    BtnCustom8Kind = cxbkStandard
    BtnCustom9Kind = cxbkStandard
    BtnCustom10Kind = cxbkStandard
    ApprovedRollbackButton = nbCustom1
    ApprovedCommitButton = nbCustom1
    ButtonControlType = ctSingle
  end
  object Splitter1: TABcxSplitter
    Left = 0
    Top = 210
    Width = 800
    Height = 8
    HotZoneClassName = 'TcxMediaPlayer8Style'
    AlignSplitter = salTop
    InvertDirection = True
    Control = ABDBcxGrid1
  end
  object ABDBcxGrid1: TABcxGrid
    Left = 0
    Top = 60
    Width = 800
    Height = 150
    Align = alTop
    TabOrder = 4
    LookAndFeel.NativeStyle = False
    object ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
      PopupMenu.AutoHotkeys = maManual
      PopupMenu.CloseFootStr = False
      PopupMenu.LinkTableView = ABcxGridDBBandedTableView1
      PopupMenu.AutoApplyBestFit = True
      PopupMenu.AutoCreateAllItem = True
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ABDatasource1
      DataController.Filter.Options = [fcoCaseInsensitive]
      DataController.Filter.AutoDataSetFilter = True
      DataController.Filter.TranslateBetween = True
      DataController.Filter.TranslateIn = True
      DataController.Filter.TranslateLike = True
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.AlwaysShowEditor = True
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.DataRowSizing = True
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.MultiSelect = True
      OptionsView.CellAutoHeight = True
      OptionsView.GroupByBox = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      ExtPopupMenu.AutoHotkeys = maManual
      ExtPopupMenu.CloseFootStr = False
      ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView1
      ExtPopupMenu.AutoApplyBestFit = True
      ExtPopupMenu.AutoCreateAllItem = True
    end
    object cxGridLevel1: TcxGridLevel
      GridView = ABcxGridDBBandedTableView1
    end
  end
  object ABcxGrid1: TABcxGrid
    Left = 0
    Top = 218
    Width = 800
    Height = 292
    Align = alClient
    TabOrder = 5
    LookAndFeel.NativeStyle = False
    object ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView
      PopupMenu.AutoHotkeys = maManual
      PopupMenu.CloseFootStr = False
      PopupMenu.LinkTableView = ABcxGridDBBandedTableView2
      PopupMenu.AutoApplyBestFit = True
      PopupMenu.AutoCreateAllItem = True
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ABDatasource2
      DataController.Filter.Options = [fcoCaseInsensitive]
      DataController.Filter.AutoDataSetFilter = True
      DataController.Filter.TranslateBetween = True
      DataController.Filter.TranslateIn = True
      DataController.Filter.TranslateLike = True
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.AlwaysShowEditor = True
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.DataRowSizing = True
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.MultiSelect = True
      OptionsView.CellAutoHeight = True
      OptionsView.GroupByBox = False
      OptionsView.BandHeaders = False
      Bands = <
        item
        end>
      ExtPopupMenu.AutoHotkeys = maManual
      ExtPopupMenu.CloseFootStr = False
      ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView2
      ExtPopupMenu.AutoApplyBestFit = True
      ExtPopupMenu.AutoCreateAllItem = True
    end
    object cxGridLevel2: TcxGridLevel
      GridView = ABcxGridDBBandedTableView2
    end
  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SqlUpdateDatetime = 42255.263341967590000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'Main'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    IndexListDefs = <>
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 42
    Top = 332
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 115
    Top = 333
  end
  object ABQuery2: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    SqlUpdateDatetime = 42255.263409606480000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'Main'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    IndexListDefs = <>
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 42
    Top = 396
  end
  object ABDatasource2: TABDatasource
    AutoEdit = False
    DataSet = ABQuery2
    Left = 115
    Top = 397
  end
end
