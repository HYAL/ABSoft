{
框架变量定义
}
unit ABFramkWorkVarU;

interface
{$I ..\ABInclude.ini}

uses
  Graphics;

var
  //创建ABDBPanel控件时用的一些变量以决定创建控件的间隔及颜色信息
  //ABDBPanel中创建控件时左象素数量
  ABDBPanelParams_Leftspacing,
  //ABDBPanel中创建控件时顶象素数量
  ABDBPanelParams_Topspacing,
  //ABDBPanel中创建控件时右象素数量
  ABDBPanelParams_Righttspacing,
  //ABDBPanel中创建控件时底象素数量
  ABDBPanelParams_Bottomspacing,
  //ABDBPanel中创建控件时X方向象素数量
  ABDBPanelParams_Xspacing,
  //ABDBPanel中创建控件时Y方向象素数量
  ABDBPanelParams_Yspacing:LongInt;

  //框架数据集关联的控件中
  //非空字段标签颜色
  ABQueryParams_NotNullColor_Label               ,
  //非空字段控件颜色
  ABQueryParams_NotNullColor_Control             ,
  //只读字段标签颜色
  ABQueryParams_ReadOnlyColor_Label              :TColor;
  //只读字段控件颜色
  ABQueryParams_ReadOnlyColor_Control            :TColor;
  //非空或只读字段的颜色是在标签上还是控件上 1.在标签上 2.在控件上 3或其它.标签且控件上
  ABQueryParams_ColorInLableOrControl :longint;

  //客户端类型中每位的使用状态(在连接后设置这些共用变量值)
  ABClientTypeUse:array[1..100] of boolean;
  //ABDBPanel右键菜单的中文
  ABDBPanelPopupMenuStrs: array[0..17] of string;
  //ABDBQueryPanel右键菜单的中文
  ABDBQueryPanelPopupMenuStrs: array[0..0] of string;

implementation


procedure ABInitialization;
begin
  ABDBPanelParams_Leftspacing        :=5;
  ABDBPanelParams_Topspacing         :=5;
  ABDBPanelParams_Righttspacing      :=5;
  ABDBPanelParams_Bottomspacing      :=8;

  ABDBPanelParams_Xspacing           :=1;
  ABDBPanelParams_Yspacing           :=1;
  ABQueryParams_NotNullColor_Label        :=-16777208;
  ABQueryParams_NotNullColor_Control      :=-16777211;
  ABQueryParams_ReadOnlyColor_Label       :=-16777208;
  ABQueryParams_ReadOnlyColor_Control     :=-16777211;
  ABQueryParams_ColorInLableOrControl     :=3;

  ABDBPanelPopupMenuStrs[0]  :='************字段创建************* ';
  ABDBPanelPopupMenuStrs[1]  :='初始顺序+初始尺寸+边界自适应';
  ABDBPanelPopupMenuStrs[2]  :='初始顺序+保存尺寸+边界自适应';
  ABDBPanelPopupMenuStrs[3]  :='设定顺序+初始尺寸+边界自适应';
  ABDBPanelPopupMenuStrs[4]  :='设定顺序+保存尺寸+边界自适应';
  ABDBPanelPopupMenuStrs[5]  :='保存位置+初始尺寸+边界自适应';
  ABDBPanelPopupMenuStrs[6]  :='保存位置+保存尺寸+边界自适应';
  ABDBPanelPopupMenuStrs[7]  :='保存位置+保存尺寸(运行时默认方式)';
  ABDBPanelPopupMenuStrs[8]  :='清除字段';
  ABDBPanelPopupMenuStrs[9]  :='************字段保存************* ';
  ABDBPanelPopupMenuStrs[10]  :='保存位置+尺寸到全局位置';
  ABDBPanelPopupMenuStrs[11]  :='保存位置+尺寸到单独位置';
  ABDBPanelPopupMenuStrs[12] :='************字段设置************* ';
  ABDBPanelPopupMenuStrs[13] :='设置仅包含字段';
  ABDBPanelPopupMenuStrs[14] :='设置非包含字段';
  ABDBPanelPopupMenuStrs[15] :='设置只读字段';
  ABDBPanelPopupMenuStrs[16] :='设置只修改字段';
  ABDBPanelPopupMenuStrs[17] :='清除所有设置';

  ABDBQueryPanelPopupMenuStrs[0] :='设计查询控件';
end;

Initialization
  ABInitialization;

Finalization


end.                             
