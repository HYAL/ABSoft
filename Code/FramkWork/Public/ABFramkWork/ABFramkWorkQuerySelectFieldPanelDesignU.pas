{
查询设计面板单元
通过数据字典中指定的查询字段生成查询面板
}
unit ABFramkWorkQuerySelectFieldPanelDesignU;

interface
{$I ..\ABInclude.ini}

uses
  ABPubFuncU,

  ABPubDesignU,
  ABPubDesignAlignU,
  ABPubFormU,

  ABFramkWorkObjectInspectorU,

  ABFramkWorkControlU,
  ABFramkWorkDictionaryQueryU,

  Classes,Controls,Forms,ExtCtrls,Menus, SysUtils,Messages,
  StdCtrls, db,

  ABPubPanelU, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, cxButtons;

type
  TABQuerySelectFieldPanelDesignForm = class(TABPubForm)
    Panel2: TPanel;
    Button1: TABcxButton;
    Button2: TABcxButton;
    ABcxButton2: TABcxButton;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ABcxButton2Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
  private
    FDataSource: TDataSource;
    function GetInnerQueryControlPanel(aDoCreate:boolean=false): TABCustomPanel;
    { Private declarations }
  public
    procedure ChangeDesignControlMsg(var Msg: Tmessage); message ABChangeDesignControl_Msg;
    property DataSource: TDataSource read FDataSource write FDataSource;
    procedure   WMMOVE(var   Msg:   TMessage);   message   WM_MOVE;
    { Public declarations }
  Published
  end;


function ABQuerySelectFieldPanelDesign(aDataSource: TDataSource;var aResDFM:string):Boolean;

implementation
{$R *.dfm}

function ABQuerySelectFieldPanelDesign(aDataSource: TDataSource;var aResDFM:string):Boolean;
var
  tempForm:TABQuerySelectFieldPanelDesignForm;
  tempStrings1:TStrings;
  tempOK:boolean;
  tempName:string;
begin
  Result:=false;
  tempForm:=TABQuerySelectFieldPanelDesignForm.Create(nil);
  tempName:=tempForm.Name;
  try
    tempForm.DataSource :=aDataSource;
    if (aResDFM<>EmptyStr) then
    begin
      ABDFMPropertyToComponent(aResDFM,tempForm);
      tempForm.Name:=tempName;
    end
    else
    begin
      tempForm.ABcxButton2Click(tempForm.ABcxButton2);
    end;

    if tempForm.ShowModal=mrOk then
    begin
      aResDFM:=ABComponentToDFMProperty(tempForm);
      tempStrings1:=TStringList.Create;
      try
        tempStrings1.Text:= Trim(aResDFM);
        tempOK:=ABDFMControlDelete(tempStrings1,'  ','object Panel2: TPanel');
        if tempOK then
          tempOK:=ABDFMControlDelete(tempStrings1,'  ','object PopupMenu1: TPopupMenu');

        if tempOK then
        begin
          aResDFM:=Trim(tempStrings1.Text);
          Result:=true;
        end
        else
          aResDFM:=EmptyStr;
      finally
        tempStrings1.Free;
      end;
    end;
  finally
    tempForm.Free;
  end;
end;

procedure TABQuerySelectFieldPanelDesignForm.ABcxButton2Click(Sender: TObject);
var
  i:LongInt;
  tempCustomPanel:TABCustomPanel;
begin
  tempCustomPanel:=GetInnerQueryControlPanel;
  if Assigned(tempCustomPanel) then
  begin
    for i := ControlCount-1 downto 0 do
    begin
      if  (Controls[i].Parent= tempCustomPanel)  then
        Controls[i].Free;
    end;
  end;
  ABCreateMultiLevelQueryControls(GetInnerQueryControlPanel(True),self,TABDictionaryQuery(FDataSource.DataSet),
                                  True,
                                  0,
                                  True,

                                  0,
                                  True,

                                  true,
                                  False,
                                  False
   );
end;

function TABQuerySelectFieldPanelDesignForm.GetInnerQueryControlPanel(aDoCreate:boolean): TABCustomPanel;
var
  i:LongInt;
begin
  result:=Nil;
  for i := ControlCount-1 downto 0 do
  begin
    if Controls[i].ClassType=TABCustomPanel then
    begin
      result:=TABCustomPanel(Controls[i]);
      Break;
    end;
  end;

  if (aDoCreate) and
     (not Assigned(result))
     then
  begin
    result:= TABCustomPanel.Create(self);
    result.Name:='InnerQueryControlPanel';
    result.Align:=alClient;
    result.Parent:=self;
  end;
end;

procedure TABQuerySelectFieldPanelDesignForm.N1Click(Sender: TObject);
begin
  ABBeginDesignClientOfParent(self,GetInnerQueryControlPanel);
  ABShowObjectInspector(self);

end;

procedure TABQuerySelectFieldPanelDesignForm.N2Click(Sender: TObject);
begin
  ABCloseObjectInspector;
  ABEndDesign(self);
end;

procedure TABQuerySelectFieldPanelDesignForm.N4Click(Sender: TObject);
var
  i:LongInt;
begin
  if (Assigned(ABPubDesignerHook)) and
     (Assigned(ABPubDesignerHook.Form))  then
  begin
    for I := ABPubDesignerHook.ControlCount-1 downto 0 do
    begin
      if (ABPubDesignerHook.Controls[i].Parent=GetInnerQueryControlPanel) or
         ((Assigned(ABPubDesignerHook.Controls[i].Parent)) and  (ABPubDesignerHook.Controls[i].Parent.Parent=GetInnerQueryControlPanel))
          then
      begin
        ABPubDesignerHook.Controls[i].Free
      end;
    end;
    ABPubDesignerHook.Clear;
  end;
end;

procedure TABQuerySelectFieldPanelDesignForm.Button1Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;

procedure TABQuerySelectFieldPanelDesignForm.Button2Click(Sender: TObject);
begin
  ModalResult:=mrOk;
end;

procedure TABQuerySelectFieldPanelDesignForm.WMMOVE(var   Msg:   TMessage);
begin
  Inherited;
  if (Assigned(ABPubDesignerHook)) and
     (Assigned(ABPubDesignerHook.Form))  then
  begin
    ABRefreshDesignAlignPosition;
    ABRefreshObjectInspectorPosition(self);
  end;
end;

procedure TABQuerySelectFieldPanelDesignForm.ChangeDesignControlMsg(
  var Msg: Tmessage);
begin
  if (Assigned(ABPubDesignerHook)) and
     (Assigned(ABPubDesignerHook.Form))  then
  begin
    Sleep(10);

    if (ABPubDesignerHook.ControlCount>0) and
       (Assigned(ABPubDesignerHook.Controls[0])) then
    begin
      ABSetObjectInspectorObject(ABPubDesignerHook.Controls[0]);
      ABRefreshObjectInspectorProperty;
    end
    else
      ABSetObjectInspectorObject(nil);
  end;
end;

end.


