object ABOperatorDatasetForm: TABOperatorDatasetForm
  Left = 311
  Top = 166
  Caption = #25805#20316#25968#25454#38598#30340#25968#25454
  ClientHeight = 550
  ClientWidth = 800
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object ParentPageControl: TABcxPageControl
    Left = 0
    Top = 0
    Width = 800
    Height = 510
    Align = alClient
    TabOrder = 0
    Properties.CustomButtons.Buttons = <>
    LookAndFeel.Kind = lfFlat
    OnChange = ParentPageControlChange
    ActivePageIndex = -1
    ClientRectBottom = 509
    ClientRectLeft = 1
    ClientRectRight = 799
    ClientRectTop = 1
  end
  object Panel1: TPanel
    Left = 0
    Top = 510
    Width = 800
    Height = 40
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Panel2: TPanel
      Left = 608
      Top = 0
      Width = 192
      Height = 40
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        192
        40)
      object Button1: TABcxButton
        Left = 94
        Top = 8
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #21462#28040
        LookAndFeel.Kind = lfFlat
        TabOrder = 1
        ShowProgressBar = False
      end
      object Button2: TABcxButton
        Left = 14
        Top = 8
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #30830#23450
        Default = True
        LookAndFeel.Kind = lfFlat
        TabOrder = 0
        ShowProgressBar = False
      end
    end
    object ABcxRadioGroup1: TABcxRadioGroup
      Left = 0
      Top = 0
      Align = alClient
      Properties.Columns = 3
      Properties.DefaultValue = 0
      Properties.Items = <>
      Style.BorderStyle = ebsNone
      Style.Shadow = False
      TabOrder = 1
      OnClick = ABcxRadioGroup1Click
      Height = 40
      Width = 608
    end
  end
end
