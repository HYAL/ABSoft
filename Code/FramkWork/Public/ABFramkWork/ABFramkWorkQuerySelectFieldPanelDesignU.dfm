object ABQuerySelectFieldPanelDesignForm: TABQuerySelectFieldPanelDesignForm
  Left = 795
  Top = 246
  BorderIcons = [biMinimize, biMaximize]
  Caption = #35774#35745#26597#35810#39029#38754
  ClientHeight = 500
  ClientWidth = 700
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 462
    Width = 700
    Height = 38
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      700
      38)
    object Button1: TABcxButton
      Left = 625
      Top = 6
      Width = 65
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #21462#28040
      LookAndFeel.Kind = lfFlat
      TabOrder = 1
      OnClick = Button1Click
      ShowProgressBar = False
    end
    object Button2: TABcxButton
      Left = 554
      Top = 6
      Width = 65
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #30830#23450
      Default = True
      LookAndFeel.Kind = lfFlat
      TabOrder = 0
      OnClick = Button2Click
      ShowProgressBar = False
    end
    object ABcxButton2: TABcxButton
      Left = 8
      Top = 6
      Width = 65
      Height = 25
      Caption = #21021#22987#21270
      DropDownMenu = PopupMenu1
      Kind = cxbkDropDownButton
      LookAndFeel.Kind = lfFlat
      TabOrder = 2
      OnClick = ABcxButton2Click
      ShowProgressBar = False
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 200
    Top = 296
    object N1: TMenuItem
      Caption = #35774#35745
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #20851#38381#35774#35745
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object N4: TMenuItem
      Caption = #21024#38500#25511#20214
      OnClick = N4Click
    end
  end
end
