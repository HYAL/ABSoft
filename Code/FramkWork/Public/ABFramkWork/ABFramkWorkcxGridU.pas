{
框架字典数据集表格单元
}
unit ABFramkWorkcxGridU;

interface
{$I ..\ABInclude.ini}

uses
  abpubFuncU,
  ABPubDBU,
  ABPubSelectStrU,

  ABThirdFuncU,
  ABThird_DevExtPopupMenuU,

  ABFramkWorkDictionaryQueryU,
  ABFramkWorkFuncU,
  ABFramkWorkConstU,
  ABFramkWorkControlU,

  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxCustomData,
  cxGridDBDataDefinitions,
  cxGridDBTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxControls,
  cxClasses,
  cxGrid,
  cxFilter,
  cxDBData,
  cxEdit,

  SysUtils,Variants,Classes,Forms,Controls,DB;


type
  TABcxDBDataField  = class(TcxDBDataField    )
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    constructor Create(AFieldList: TcxCustomDataFieldList); override;
    { Public declarations }
  end;

  TABcxDBDataFilterCriteriaItem  = class(TcxDBDataFilterCriteriaItem    )
  private
    { Private declarations }
  protected
    function GetDataValue(AData: TObject): Variant; override;
    function GetFieldCaption: string; override;
    function GetFieldName: string; override;
    function GetItemLink: TObject; override;
    procedure SetItemLink(Value: TObject); override;
    function GetDisplayValue: string; override;
    { Protected declarations }
  public
    { Public declarations }
  end;

  TABcxDBDataFilterCriteria  = class(TcxDBDataFilterCriteria    )
  private
    { Private declarations }
  protected
    procedure FormatFilterTextValue(AItem: TcxFilterCriteriaItem; const AValue: Variant;
      var ADisplayValue: string); override;
    function GetItemClass: TcxFilterCriteriaItemClass; override;
    procedure Update; override;
    { Protected declarations }
  public
    constructor Create(ADataController: TcxCustomDataController); override;
    { Public declarations }
  published
    { Published declarations }
  end;

  TABcxGridDBDataController  = class(TcxGridDBDataController    )
  private
    { Private declarations }
  protected
    function GetFieldClass: TcxCustomDataFieldClass; override;
    function GetFilterCriteriaClass: TcxDataFilterCriteriaClass; override;

    procedure DoDataSourceChanged; override;
    procedure DoDataChanged; override;
    procedure LoadStorage; override;
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;
    { Public declarations }
  published
    { Published declarations }
  end;

  TABcxGridSite  = class(TcxGridSite    )
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    constructor Create(AViewInfo: TcxCustomGridViewInfo); reintroduce; virtual;
    destructor Destroy; override;
    { Public declarations }
  end;

  TABcxGridBandedTableViewInfo  = class(TcxGridBandedTableViewInfo    )
  private
    { Private declarations }
  protected
    function GetSiteClass: TcxGridSiteClass; override;
    { Protected declarations }
  public
    { Public declarations }
  end;

  TABcxGridDBBandedTableView = class(TcxGridDBBandedTableView    )
  private
    FFuncControlPropertyDataset:Tdataset;
    FExtPopupMenu:TABcxGridPopupMenu;
    FHaveField: string;
    FReadOnlyField: string;
    FNoHaveField: string;
    FEditField: string;
    FDBPanelForm: TForm;
    FGetExpProperty: Boolean;
    //加载列的字段定义
    procedure LoadColumnsFieldDef;
    function GetDBPanelPropertyValue(aPropertyName: string): string;
    procedure SetDBPanelPropertyValue(aPropertyName, aValue: string);
    procedure GetExpProperty;
    function GetFuncControlPropertyDataset: Tdataset;
    { Private declarations }
  protected
    function GetViewInfoClass: TcxCustomGridViewInfoClass; override;
    procedure DoEditKeyDown(AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit;
      var Key: Word; Shift: TShiftState); override;
    function GetDataControllerClass: TcxCustomDataControllerClass; override;
    function GetItemClass: TcxCustomGridTableItemClass; override;
    function GetSummaryItemClass: TcxDataSummaryItemClass; override;
    { Protected declarations }
  public
    //根据关联的数据集设置表格的编辑属性
    procedure SetAutoEdit;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    { Public declarations }
  published
    //根据数据集字典信息创建表格的列
    procedure CreateAllColumn;

    //清除设置字段
    procedure ClearSetupField;
    //开窗设置包含字段
    procedure SetupHaveField;
    //开窗设置不包含字段
    procedure SetupNoHaveField;
    //开窗设置只读字段
    procedure SetupReadOnlyField;
    //开窗设置可编辑字段
    procedure SetupEditField;

    //设置包含字段
    procedure SetEditField(aValue: string);
    //设置不包含字段
    procedure SetHaveField(aValue: string);
    //设置只读字段
    procedure SetNoHaveField(aValue: string);
    //设置可编辑字段
    procedure SetReadOnlyField(aValue: string);
  published
    //关联的右键菜单
    property ExtPopupMenu:TABcxGridPopupMenu read FExtPopupMenu write FExtPopupMenu;
    { Published declarations }
  end;

  //数据表格控件
  TABcxGrid            = class(TcxGrid           )
  private
    { Private declarations }
  protected
    function GetDefaultViewClass: TcxCustomGridViewClass; override;
    { Protected declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    { Public declarations }
  published
    { Published declarations }
  end;



implementation

{ TABcxGridDBDataController }


constructor TABcxGridDBDataController.Create(AOwner: TComponent);
begin
  inherited;
end;

procedure TABcxGridDBDataController.DoDataChanged;
begin
  inherited;
  ABSetAutoIDWidth(GridView);
end;

procedure TABcxGridDBDataController.DoDataSourceChanged;
begin
  inherited;
end;

function TABcxGridDBDataController.GetFieldClass: TcxCustomDataFieldClass;
begin
  //inherited;
  Result := TABcxDBDataField;
end;

function TABcxGridDBDataController.GetFilterCriteriaClass: TcxDataFilterCriteriaClass;
begin
  //inherited;
  Result := TABcxDBDataFilterCriteria;
end;

//cxGrid在逐行繪製內容時,AfterScroll代碼被执行多次的解决.
//edit by grj 2008-03-25 09:01:19
// 當然你也可以加入其他DataSet不想觸發的事件,但必須避免有其他影響
procedure TABcxGridDBDataController.LoadStorage;
var
  tempOldBeforeScroll:TDataSetNotifyEvent;
  tempOldAfterScroll:TDataSetNotifyEvent;
begin
  if Assigned(DataSet) then
  begin
    ABBackAndStopDatasetEvent(DataSet,tempOldBeforeScroll,tempOldAfterScroll);
    try
      inherited;
    finally
      ABUnBackDatasetEvent(DataSet,tempOldBeforeScroll,tempOldAfterScroll);
    end;

    if DataSet is TABDictionaryQuery then
    begin
      if (Assigned(GridView)) and
         (GridView.Name<>EmptyStr) and
         (TABDictionaryQuery(DataSet).LinkDBTableViewList.IndexOf(GridView)<0) then
      begin
        TABDictionaryQuery(DataSet).LinkDBTableViewList.Add(GridView);
      end;
    end;
  end
  else
    inherited;
end;

{ TABcxDBDataFilterCriteria }

constructor TABcxDBDataFilterCriteria.Create(ADataController: TcxCustomDataController);
begin
  inherited;


end;

procedure TABcxDBDataFilterCriteria.FormatFilterTextValue(
  AItem: TcxFilterCriteriaItem; const AValue: Variant;
  var ADisplayValue: string);
begin
  inherited;

end;

function TABcxDBDataFilterCriteria.GetItemClass: TcxFilterCriteriaItemClass;
begin
//  inherited;
  Result := TABcxDBDataFilterCriteriaItem;
end;

procedure TABcxDBDataFilterCriteria.Update;
begin
  inherited;

end;

{ TABcxDBDataFilterCriteriaItem }

function TABcxDBDataFilterCriteriaItem.GetDataValue(AData: TObject): Variant;
begin
  result:= inherited GetDataValue(AData);

end;

function TABcxDBDataFilterCriteriaItem.GetDisplayValue: string;
begin
  result:= inherited GetDisplayValue;
end;

function TABcxDBDataFilterCriteriaItem.GetFieldCaption: string;
begin
  result:= inherited GetFieldCaption;

end;

function TABcxDBDataFilterCriteriaItem.GetFieldName: string;
begin
  result:= inherited GetFieldName;

end;

function TABcxDBDataFilterCriteriaItem.GetItemLink: TObject;
begin
  result:= inherited GetItemLink;

end;

procedure TABcxDBDataFilterCriteriaItem.SetItemLink(Value: TObject);
begin
  inherited SetItemLink(Value);

end;


{ TABcxGridSite }

constructor TABcxGridSite.Create(AViewInfo: TcxCustomGridViewInfo);
begin
end;

destructor TABcxGridSite.Destroy;
begin

  inherited;
end;

{ TABcxGridBandedTableViewInfo }

function TABcxGridBandedTableViewInfo.GetSiteClass: TcxGridSiteClass;
begin
  Result := TABcxGridSite;
end;

{ TABcxGrid }

constructor TABcxGrid.Create(AOwner: TComponent);
begin
  inherited;
end;

destructor TABcxGrid.Destroy;
begin
  inherited;
end;

function TABcxGrid.GetDefaultViewClass: TcxCustomGridViewClass;
begin
  //result:=inherited GetDefaultViewClass;
  result:=TABcxGridDBBandedTableView;
end;


{ TABcxGridDBBandedTableView }

constructor TABcxGridDBBandedTableView.Create(AOwner: TComponent);
begin
  inherited;
  DataController.Filter.Options    :=DataController.Filter.Options+[fcoCaseInsensitive];
  DataController.Filter.AutoDataSetFilter:=true;
  DataController.Filter.TranslateBetween:=true;
  DataController.Filter.TranslateIn:=true;
  DataController.Filter.TranslateLike:=true;
  OptionsCustomize.ColumnsQuickCustomization:=true;
  OptionsSelection.MultiSelect:=true;
  OptionsView.GroupByBox:=false;

  OptionsBehavior.FocusCellOnTab:=True;
  OptionsBehavior.GoToNextCellOnEnter:=True;
  OptionsBehavior.AlwaysShowEditor:=True;
  OptionsCustomize.DataRowSizing:=true;

  FGetExpProperty:=false;
  FExtPopupMenu:=TABcxGridPopupMenu.Create(self); //所有者不能是AOwner,要不然就会显示在窗体中了
  FExtPopupMenu.SetSubComponent(True);
  PopupMenu:=FExtPopupMenu;
end;

procedure TABcxGridDBBandedTableView.CreateAllColumn;
begin
  FExtPopupMenu.FreeOldEVent;
  if (ColumnCount>0) then
    ClearItems;

  LoadColumnsFieldDef;
  //加载保存的INI
  FExtPopupMenu.AutoCreateAllItem:=False;
  FExtPopupMenu.Load;

  SetAutoEdit;
end;

procedure TABcxGridDBBandedTableView.SetAutoEdit;
begin
  if (Assigned(DataController)) and
     (Assigned(DataController.DataSource)) and
     (Assigned(DataController.DataSource.DataSet ))  and
     (DataController.DataSource.DataSet is TABDictionaryQuery) then
  begin
    OptionsData.Appending:= (TABDictionaryQuery(DataController.DataSource.DataSet).CanInsert) and (DataController.DataSource.AutoEdit);
    OptionsData.Inserting:= (TABDictionaryQuery(DataController.DataSource.DataSet).CanInsert) and (DataController.DataSource.AutoEdit);
    OptionsData.Deleting := (TABDictionaryQuery(DataController.DataSource.DataSet).CanDelete) and (DataController.DataSource.AutoEdit);
  end;
end;

procedure TABcxGridDBBandedTableView.LoadColumnsFieldDef;
var
  tempColumn:TcxGridDBBandedColumn;
  i:longint;
  tempFieldDef:PABFieldDef;
begin
  if (Assigned(DataController)) and
     (Assigned(DataController.DataSet)) and
     (DataController.DataSet.Active) then
  begin
    BeginUpdate;
    try
      if columncount>0 then
        ClearItems;

      GetExpProperty;

      for i := 0 to DataController.DataSet.FieldCount-1 do
      begin
        if  ((FNoHaveField=EmptyStr)or (ABPos(','+DataController.DataSet.Fields[i].FieldName+',',','+FNoHaveField  +',')<=0)) and
            ((FHaveField=EmptyStr)  or (ABPos(','+DataController.DataSet.Fields[i].FieldName+',',','+FHaveField+',')> 0)) then
        begin
          tempFieldDef:=ABFieldDefByFieldName(DataController.DataSet,
                                                 DataController.DataSet.Fields[i].FieldName
                                                                            );
          //SQL语句中的字段或数据字典中有定义的字段
          if (NOT Assigned(tempFieldDef)) or (tempFieldDef.Fi_IsGridView)  then
          begin
            tempColumn:= CreateColumn;
            tempColumn.DataBinding.FieldName := DataController.DataSet.Fields[i].FieldName;
            if Name<>EmptyStr then
              tempColumn.Name:=Name+DataController.DataSet.Fields[i].FieldName;
            ABReFreshCloumnAndControl(DataController.DataSource,DataController.DataSet.Fields[i],tempColumn);

            if (Assigned(tempFieldDef)) and (tempFieldDef.Fi_IsGridView)  then
            begin
              if ((FReadOnlyField=EmptyStr) or (ABPos(','+tempFieldDef.Fi_Name+',',','+FReadOnlyField  +',')<=0)) and
                 ((FEditField=EmptyStr)   or (ABPos(','+tempFieldDef.Fi_Name+',',','+FEditField+',')> 0)) then
              begin
              end
              else
              begin
                tempColumn.Options.Editing:=false;
                tempFieldDef.Fi_LockGridView :=true;
              end;
              tempFieldDef.Fi_GridColumn:=tempColumn;
            end
            else
            begin
              tempColumn.Options.Editing:=false;
            end;
          end;
        end;
      end;
    finally
      EndUpdate;
    end;
  end;
end;

function TABcxGridDBBandedTableView.GetFuncControlPropertyDataset:Tdataset;
begin
  if not Assigned(FFuncControlPropertyDataset) then
    FFuncControlPropertyDataset:= ABGetConstSqlPubDataset('ABSys_Org_FuncControlProperty',
                              [TABDictionaryQuery(DataController.DataSource.DataSet).FuncGuid,
                               GetParentForm(self.Control).Name],TABInsideQuery);
  result:=FFuncControlPropertyDataset;
end;


function TABcxGridDBBandedTableView.GetDBPanelPropertyValue(aPropertyName: string):string;
var
  tempParentForm:TCustomForm;
begin
  Result:=EmptyStr;
  tempParentForm:=GetParentForm(self.Control);
  if (Assigned(tempParentForm)) and (tempParentForm.Name<>EmptyStr) and
     (Assigned(DataController)) and
     (Assigned(DataController.DataSource)) and
     (Assigned(DataController.DataSource.DataSet)) and
     (DataController.DataSource.DataSet is TABDictionaryQuery)
      then
  begin
    Result:=  ABGetFieldValue(GetFuncControlPropertyDataset,
                              ['FP_ControlName','FP_PropertyName'],
                              [Name,aPropertyName],
                              ['FP_PropertyValue'],'');
  end;
end;

procedure TABcxGridDBBandedTableView.GetExpProperty;
begin
  if (not FGetExpProperty) and
     (Assigned(DataController.DataSource)) and
     (Assigned(DataController.DataSource.DataSet)) and
     (DataController.DataSource.DataSet.Active) then
  begin
    FHaveField:=GetDBPanelPropertyValue('HaveField');
    FNoHaveField:=GetDBPanelPropertyValue('NoHaveField');
    FReadOnlyField:=GetDBPanelPropertyValue('ReadOnlyField');
    FEditField:=GetDBPanelPropertyValue('EditField');
    FGetExpProperty:=true;
  end;
end;

procedure TABcxGridDBBandedTableView.SetDBPanelPropertyValue(aPropertyName: string;aValue:string);
var
  tempParentForm:TCustomForm;
begin
  tempParentForm:=GetParentForm(self.Control);
  if (Assigned(tempParentForm)) and (tempParentForm.Name<>EmptyStr) and
     (Assigned(DataController)) and
     (Assigned(DataController.DataSource)) and
     (Assigned(DataController.DataSource.DataSet)) and
     (DataController.DataSource.DataSet is TABDictionaryQuery) and
     (TABDictionaryQuery(DataController.DataSource.DataSet).FuncGuid<>EmptyStr) then
  begin
    ABSetFieldValue(
                    GetFuncControlPropertyDataset,
                    ['FP_FU_Guid','FP_FormName','FP_ControlName','FP_PropertyName'],
                    [TABDictionaryQuery(DataController.DataSource.DataSet).FuncGuid,
                     tempParentForm.Name,
                     Name,
                     aPropertyName],
                    ['FP_PropertyValue'],
                    [aValue]
                      );

  end;
end;

procedure TABcxGridDBBandedTableView.SetupHaveField;
var
  tempStr1:string;
begin
  tempStr1:=FHaveField;
  if (ABSelectFieldNames(tempStr1,DataController.DataSet)) and
     (tempStr1<>FHaveField) then
  begin
    SetHaveField(tempStr1);
  end;
end;

procedure TABcxGridDBBandedTableView.SetupNoHaveField;
var
  tempStr1:string;
begin
  tempStr1:=FNoHaveField;
  if (ABSelectFieldNames(tempStr1,DataController.DataSet))  and
     (tempStr1<>FNoHaveField) then
  begin
    SetNoHaveField(tempStr1);
  end;
end;

procedure TABcxGridDBBandedTableView.SetupReadOnlyField;
var
  tempStr1:string;
begin
  tempStr1:=FReadOnlyField;
  if (ABSelectFieldNames(tempStr1,DataController.DataSet)) and
     (tempStr1<>FReadOnlyField) then
  begin
    SetReadOnlyField(tempStr1);
  end;
end;

procedure TABcxGridDBBandedTableView.SetupEditField;
var
  tempStr1:string;
begin
  tempStr1:=FEditField;
  if (ABSelectFieldNames(tempStr1,DataController.DataSet)) and
     (tempStr1<>FEditField) then
  begin
    SetEditField(tempStr1);
  end;
end;

procedure TABcxGridDBBandedTableView.SetHaveField(aValue:string);
begin
  SetDBPanelPropertyValue('HaveField'    ,aValue);
  FHaveField := aValue;
end;

procedure TABcxGridDBBandedTableView.SetNoHaveField(aValue:string);
begin
  SetDBPanelPropertyValue('NoHaveField'    ,aValue);
  FNoHaveField := aValue;
end;

procedure TABcxGridDBBandedTableView.SetReadOnlyField(aValue:string);
begin
  SetDBPanelPropertyValue('ReadOnlyField'    ,aValue);
  FReadOnlyField := aValue;
end;

procedure TABcxGridDBBandedTableView.SetEditField(aValue:string);
begin
  SetDBPanelPropertyValue('EditField',aValue);
  FEditField := aValue;
end;

procedure TABcxGridDBBandedTableView.ClearSetupField;
begin
  SetHaveField(EmptyStr);
  SetNoHaveField(EmptyStr);
  SetReadOnlyField(EmptyStr);
  SetEditField(EmptyStr);
end;

destructor TABcxGridDBBandedTableView.Destroy;
begin
  FExtPopupMenu.Free;
  if Assigned(FDBPanelForm) then
    FreeAndNil(FDBPanelForm);
  inherited;
end;


procedure TABcxGridDBBandedTableView.DoEditKeyDown(
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Shift=[ssCtrl] then
  begin
    if  (Key=65) then
    begin
      DataController.SelectAll;
    end;
  end;
end;

function TABcxGridDBBandedTableView.GetDataControllerClass: TcxCustomDataControllerClass;
begin
  //Result := TcxGridDBDataController;
  Result := TABcxGridDBDataController;
end;

function TABcxGridDBBandedTableView.GetItemClass: TcxCustomGridTableItemClass;
begin
  Result := TcxGridDBBandedColumn;
end;

function TABcxGridDBBandedTableView.GetSummaryItemClass: TcxDataSummaryItemClass;
begin
  Result := TcxGridDBTableSummaryItem;
end;

function TABcxGridDBBandedTableView.GetViewInfoClass: TcxCustomGridViewInfoClass;
begin
  Result := TABcxGridBandedTableViewInfo;
end;

{ TABcxDBDataField }

constructor TABcxDBDataField.Create(AFieldList: TcxCustomDataFieldList);
begin
  inherited;

end;

procedure ABFinalization;
begin
  UnRegisterClass(TABcxGridDBBandedTableView);
  cxGridRegisteredViews.Unregister(TABcxGridDBBandedTableView);
end;

procedure ABInitialization;
begin
  RegisterClass(TABcxGridDBBandedTableView);
  cxGridRegisteredViews.Register(TABcxGridDBBandedTableView, 'DB ABBanded Table');
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;


end.






