{
框架常量定义
}
unit ABFramkWorkConstU;

interface
{$I ..\ABInclude.ini}

uses
  cxGridTableView,
  Controls,DB;

const
  //如果此下拉项是可增加的,则在下拉的SQL中加入此增加新项行
  ABcxLookupComboBoxAddNewItemStr='单击此处增加新项';

type
  TABUserKeyType=(ktFunc,ktReport,ktTable,ktField);
  //下拉相关的属性,仅表示有内部数据集
  PABFieldDownDef = ^TABFieldDownDef;
  TABFieldDownDef = record
    FI_ConnName           :string;           //连接名称
    Fi_DownGroup          :string;           //关联的下拉组别(当Fi_DownSQL为空时表示加载的组别,为非空时表示是源组别)
    FI_DownRefresh        :boolean;          //点击下拉控件时刷新
    FI_CacleDataset       :boolean;          //缓存数据集
    Fi_Tr_Code            :string;           //相关联的下拉表(TreeItem)中的Tr_Code字段(Fi_DownSQL为空串时有效)
    Fi_DownType           :string;           //下拉类型:1.仅下拉选择;2.可编辑;3.可增加
    Fi_CheckInDown        :boolean;          //是否检测值在列表中
    Fi_DownSQL          	:string;           //下拉的SQL语句
    Fi_Filter           	:string;           //下拉的Fiter
    Fi_IsAutoRemember	    :boolean;          //是否将字段值自动记忆式下拉
    Fi_DataSet            :TDataset;         //下拉相关联的数据集
    Fi_Datasource         :TDatasource;      //下拉相关联的Datasource
    Fi_DownFields     	  :string;           //下拉字段,可用','或';'分隔多个字段
    Fi_DownCaptions    	  :string;           //下拉字段的标签,为空时取字典表中的定义,可用','或';'分隔多个字段
    Fi_ViewField      	  :string;           //显示字段
    Fi_SaveField	        :string;           //保存字段
    Fi_ParentField      	:string;           //树结构的父字段,当此值不为空时:Fi_SaveField为KEY字段,控件为树形下拉形式
    Fi_CanSelectParent    :boolean;          //是否可以选择父结点

    Fi_RelatingFromFields	:string;           //当字段值变化时替换的源字段,可用','或';'分隔多个字段
    Fi_RelatingToFields  	:string;           //当字段值变化时替换的目的字段,可用','或';'分隔多个字段
    SingerTr_Code         :boolean;          //是否使用不加条件的下拉列项
  end;

  //元数据相关属性,只在数据字典作业中可以修改
  PABFieldMetaDef = ^TABFieldMetaDef;
  TABFieldMetaDef = record
    Fi_ID       	       :longint;           //字段序号
    Fi_IsAutoAdd	       :boolean;           //是否自增
    Fi_IsKey    	       :boolean;           //是否主键
    Fi_Type 	           :string;            //字段类型
    Fi_Byte	             :longint;           //占用字节数
    Fi_Length   	       :longint;           //长度
    Fi_EecimalDigits	   :longint;           //小数位数
    Fi_CanNull   	       :boolean;           //是否允许空
    Fi_DefaultValue      :string;            //默认值
  end;

  //字段的相关事件
  PABFieldEventDef = ^TABFieldEventDef;
  TABFieldEventDef = record
    FOnChange   : TFieldNotifyEvent  ;
    FOnGetText  : TFieldGetTextEvent ;
    FOnSetText  : TFieldSetTextEvent ;
    FOnValidate : TFieldNotifyEvent  ;
  end;

  TABDefaultValueTouchOff=(dtAfterAppend,dtBeforeAppendSave,dtBeforeEditSave);
  TABDefaultValueTouchOffs=set of TABDefaultValueTouchOff;
  PABFieldDef = ^TABFieldDef;
  TABFieldDef = record
    //基本信息相关
    Fi_Ta_Guid	          :string;           //字段所属表的GUID
    Fi_Ta_Name	          :string;           //字段所属表的名称
    Fi_Guid	              :string;           //字段GUID
    Fi_Name	              :string;           //字段名称
    Fi_Caption	          :string;           //字段标签的Caption
    Fi_Hint             	:string;           //字段提示
    Fi_DefaultValue	      :string;           //字段默认值
    Fi_DefaultValueMakeTime:TABDefaultValueTouchOffs;          //默认值触发时机
    Fi_Order              :longint;          //字段顺序
    Fi_ExtSetup           :string;           //扩展设置
    PubID     	          :string;

    //关联标签与控件的位置尺寸信息相关
    Fi_Label              :TWinControl;      //字段标签
    Fi_LabelLeft	        :longint;          //字段标签Left
    Fi_LabelTop	          :longint;          //字段标签Top
    Fi_LabelWidth	        :longint;          //字段标签Width
    Fi_LabelHeight   	    :longint;          //字段标签Height

    Fi_ControlType       	:string;           //字段控件的类型,为空时根据Fi_DataType设置相应的cx近控件类型
    Fi_Control            :TWinControl;      //字段控件
    Fi_ControlLeft	      :longint;          //字段控件Left
    Fi_ControlTop	        :longint;          //字段控件Top
    Fi_ControlWidth	      :longint;          //字段控件Width
    Fi_ControlHeight   	  :longint;          //字段控件Height
    Fi_GridColumn   	  :TcxGridColumn;      //字段关联CXGRID时的列

    Fi_LockInputPanelView:Boolean;      //在InputPanelView中锁定控制
    Fi_LockGridView      :Boolean;      //在GridView中锁定控制

    //控制相关
    Fi_IsQueryView	      :boolean;         //是否在查询窗体中显示
    Fi_BegEndQuery        :boolean;         //是否区间查询

    Fi_IsGridView	        :boolean;         //是否在数据GRID中显示
    Fi_IsInputPanelView	  :boolean;         //是否在录入PANEL中显示

    Fi_IsUpperChar        :boolean;         //是否统一大写
    Fi_IsLowerChar        :boolean;         //是否统一小写

    Fi_PassChar	          :string;           //密码字符
    Fi_Max               	:string;           //最大值
    Fi_Min               	:string;           //最小值
    Fi_Mask             	:string;           //显示输入模板
    Fi_MinLength   	      :longint;           //最小长度
    Fi_MaxLength   	      :longint;           //最大长度
    Fi_CanNull   	        :boolean;          //是否允许空
    Fi_CanEditInEdit 	    :boolean;          //编辑時能修改
    Fi_CanEditInInsert    :boolean;          //新增時能修改
    Fi_BatchEdit          :boolean;          //批量编辑
    Fi_Update             :Boolean;          //字段的值是否进行数据库更新
    Fi_Copy               :Boolean;          //在数据集复制时是否复制此字段的值

    Fi_DatasetUnique:boolean;                //数据集唯一值

    Fi_ExecSQLWhenFieldChange :string;       //字段值改变时要执行的SQL语句
    Fi_SetReadOnlyCommand     :string;       //字段设置ReadOnly的命令
    Fi_SetEditCommand         :string;       //修改时检测的命令
    Fi_GetValueSQL            :string;       //取值的SQL语句

    //非物理信息相关
    IsCheck ,                                   //在保存或修改时是否检测字段的值
    IsDown                           :Boolean;          //是否有下拉SQL(表示有内部数据集),非物理字段
    IsCanAppendTreeItemDown          :Boolean;          //是否是列项下拉,非物理字段

    Fi_SetOrder            :longint;          //设置顺序
    Fi_Edited              :boolean;          //是否修改的标志
    Fi_Lock                :boolean;
    Fi_LockValidate        :boolean;
    Fi_LockChange          :boolean;
    Fi_LockGetText         :boolean;
    Fi_LockSetText         :boolean;

    Field           :TField;
    Guid            :string;
    Flag            :string;
    PMetaDef        :PABFieldMetaDef;
    PEventDef       :PABFieldEventDef;

    //下拉框相关
    PDownDef        :PABFieldDownDef;
  end;
  TABCreateMainDetailTableType=(ctMain,ctDetail,ctMainOfHaveDetail);

  TABAfterLoadFieldBaseDefEvent = procedure(aFieldBaseDef: PABFieldDef;var aContinue:boolean) of object;
  TABAfterLoadFieldDownDefEvent = procedure(aFieldDownDef: PABFieldDownDef;var aContinue:boolean) of object;
  TABAfterLoadFieldMetaDefEvent = procedure(aFieldMetaDef: PABFieldMetaDef;var aContinue:boolean) of object;

  TABOnFieldInitPopupEvent = procedure(aFieldDef: PABFieldDef;var aContinue:boolean) of object;

  TABCustomBeforeInsertNotifyEvent= procedure(var aIsDo: boolean) of object;

  IABQuery = Interface
  ['{70F45951-DE83-4D5A-A5DB-54F0B6830F45}']
    function FieldDefByFieldName(const FieldName: String): PABFieldDef;
  end;

implementation


end.
