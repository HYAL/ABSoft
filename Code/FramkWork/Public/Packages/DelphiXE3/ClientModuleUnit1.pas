unit ClientModuleUnit1;

interface

uses


  SysUtils,Classes,ClientClassesUnit1,Dialogs,forms,DB,SqlExpr,
  DbxCompressionFilter,DBXCommon,DBXDataSnap,IndyPeerImpl;

type
  TClientModule1 = class(TDataModule)
    SQLConnection1: TSQLConnection;
    procedure DataModuleCreate(Sender: TObject);
  private
    FInstanceOwner: Boolean;
    FABServerClient: TABServerClient;
    function GetABServerClient: TABServerClient;
    { Private declarations }
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property InstanceOwner: Boolean read FInstanceOwner write FInstanceOwner;
    property ABServerClient: TABServerClient read GetABServerClient write FABServerClient;
  end;

var
  ClientModule1: TClientModule1;

implementation

{$R *.dfm}

{%CLASSGROUP 'System.Classes.TPersistent'}

constructor TClientModule1.Create(AOwner: TComponent);
begin
  inherited;
  FInstanceOwner := True;
end;

procedure TClientModule1.DataModuleCreate(Sender: TObject);
begin
  SQLConnection1.Params.Values['App_Name']:=Application.EXEName;
end;

destructor TClientModule1.Destroy;
begin
  FABServerClient.Free;
  inherited;
end;

function TClientModule1.GetABServerClient: TABServerClient;
begin
  if FABServerClient = nil then
  begin
    try
      SQLConnection1.Open;
      FABServerClient:= TABServerClient.Create(SQLConnection1.DBXConnection, FInstanceOwner);
    except
      ShowMessage('请确认服务器已启动.');
      Abort;
    end;
  end;
  Result := FABServerClient;
end;

end.

