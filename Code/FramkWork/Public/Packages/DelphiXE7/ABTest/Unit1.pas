//
// Created by the DataSnap proxy generator.
// 2015-4-24 ���� 11:06:01
//

unit ABThirdServerMethodsU;

interface

uses System.JSON, Data.DBXCommon, Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON, Datasnap.DSProxy, System.Classes, System.SysUtils, Data.DB, Data.SqlExpr, Data.DBXDBReaders, Data.DBXCDSReaders, Data.DBXJSONReflect;

type
  TABServerClient = class(TDSAdminClient)
  private
    FGetConnNamesCommand: TDBXCommand;
    FGetDatasetFileCommand: TDBXCommand;
    FGetServerTimeCommand: TDBXCommand;
    FEchoStringCommand: TDBXCommand;
    FReverseStringCommand: TDBXCommand;
    FGetConnInfoCommand: TDBXCommand;
    FExecSQLCommand: TDBXCommand;
    FLongTimeRunFuncCommand: TDBXCommand;
  public
    constructor Create(ADBXConnection: TDBXConnection); overload;
    constructor Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function GetConnNames: string;
    function GetDatasetFile(aConnName: string; aSQL: string; out aStream: TStream): Boolean;
    function GetServerTime: TDateTime;
    function EchoString(Value: string): string;
    function ReverseString(Value: string): string;
    procedure GetConnInfo(out aStream: TStream);
    function ExecSQL(aClientFlag: string; aConnName: string; aSQL: string; aParams: OleVariant): Integer;
    procedure LongTimeRunFunc(aCallback: TDBXCallback);
  end;

implementation

function TABServerClient.GetConnNames: string;
begin
  if FGetConnNamesCommand = nil then
  begin
    FGetConnNamesCommand := FDBXConnection.CreateCommand;
    FGetConnNamesCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetConnNamesCommand.Text := 'TABServer.GetConnNames';
    FGetConnNamesCommand.Prepare;
  end;
  FGetConnNamesCommand.ExecuteUpdate;
  Result := FGetConnNamesCommand.Parameters[0].Value.GetWideString;
end;

function TABServerClient.GetDatasetFile(aConnName: string; aSQL: string; out aStream: TStream): Boolean;
begin
  if FGetDatasetFileCommand = nil then
  begin
    FGetDatasetFileCommand := FDBXConnection.CreateCommand;
    FGetDatasetFileCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetDatasetFileCommand.Text := 'TABServer.GetDatasetFile';
    FGetDatasetFileCommand.Prepare;
  end;
  FGetDatasetFileCommand.Parameters[0].Value.SetWideString(aConnName);
  FGetDatasetFileCommand.Parameters[1].Value.SetWideString(aSQL);
  FGetDatasetFileCommand.ExecuteUpdate;
  aStream := FGetDatasetFileCommand.Parameters[2].Value.GetStream(FInstanceOwner);
  Result := FGetDatasetFileCommand.Parameters[3].Value.GetBoolean;
end;

function TABServerClient.GetServerTime: TDateTime;
begin
  if FGetServerTimeCommand = nil then
  begin
    FGetServerTimeCommand := FDBXConnection.CreateCommand;
    FGetServerTimeCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetServerTimeCommand.Text := 'TABServer.GetServerTime';
    FGetServerTimeCommand.Prepare;
  end;
  FGetServerTimeCommand.ExecuteUpdate;
  Result := FGetServerTimeCommand.Parameters[0].Value.AsDateTime;
end;

function TABServerClient.EchoString(Value: string): string;
begin
  if FEchoStringCommand = nil then
  begin
    FEchoStringCommand := FDBXConnection.CreateCommand;
    FEchoStringCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FEchoStringCommand.Text := 'TABServer.EchoString';
    FEchoStringCommand.Prepare;
  end;
  FEchoStringCommand.Parameters[0].Value.SetWideString(Value);
  FEchoStringCommand.ExecuteUpdate;
  Result := FEchoStringCommand.Parameters[1].Value.GetWideString;
end;

function TABServerClient.ReverseString(Value: string): string;
begin
  if FReverseStringCommand = nil then
  begin
    FReverseStringCommand := FDBXConnection.CreateCommand;
    FReverseStringCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FReverseStringCommand.Text := 'TABServer.ReverseString';
    FReverseStringCommand.Prepare;
  end;
  FReverseStringCommand.Parameters[0].Value.SetWideString(Value);
  FReverseStringCommand.ExecuteUpdate;
  Result := FReverseStringCommand.Parameters[1].Value.GetWideString;
end;

procedure TABServerClient.GetConnInfo(out aStream: TStream);
begin
  if FGetConnInfoCommand = nil then
  begin
    FGetConnInfoCommand := FDBXConnection.CreateCommand;
    FGetConnInfoCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FGetConnInfoCommand.Text := 'TABServer.GetConnInfo';
    FGetConnInfoCommand.Prepare;
  end;
  FGetConnInfoCommand.ExecuteUpdate;
  aStream := FGetConnInfoCommand.Parameters[0].Value.GetStream(FInstanceOwner);
end;

function TABServerClient.ExecSQL(aClientFlag: string; aConnName: string; aSQL: string; aParams: OleVariant): Integer;
begin
  if FExecSQLCommand = nil then
  begin
    FExecSQLCommand := FDBXConnection.CreateCommand;
    FExecSQLCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FExecSQLCommand.Text := 'TABServer.ExecSQL';
    FExecSQLCommand.Prepare;
  end;
  FExecSQLCommand.Parameters[0].Value.SetWideString(aClientFlag);
  FExecSQLCommand.Parameters[1].Value.SetWideString(aConnName);
  FExecSQLCommand.Parameters[2].Value.SetWideString(aSQL);
  FExecSQLCommand.Parameters[3].Value.AsVariant := aParams;
  FExecSQLCommand.ExecuteUpdate;
  Result := FExecSQLCommand.Parameters[4].Value.GetInt32;
end;

procedure TABServerClient.LongTimeRunFunc(aCallback: TDBXCallback);
begin
  if FLongTimeRunFuncCommand = nil then
  begin
    FLongTimeRunFuncCommand := FDBXConnection.CreateCommand;
    FLongTimeRunFuncCommand.CommandType := TDBXCommandTypes.DSServerMethod;
    FLongTimeRunFuncCommand.Text := 'TABServer.LongTimeRunFunc';
    FLongTimeRunFuncCommand.Prepare;
  end;
  FLongTimeRunFuncCommand.Parameters[0].Value.SetCallbackValue(aCallback);
  FLongTimeRunFuncCommand.ExecuteUpdate;
end;


constructor TABServerClient.Create(ADBXConnection: TDBXConnection);
begin
  inherited Create(ADBXConnection);
end;


constructor TABServerClient.Create(ADBXConnection: TDBXConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ADBXConnection, AInstanceOwner);
end;


destructor TABServerClient.Destroy;
begin
  FGetConnNamesCommand.DisposeOf;
  FGetDatasetFileCommand.DisposeOf;
  FGetServerTimeCommand.DisposeOf;
  FEchoStringCommand.DisposeOf;
  FReverseStringCommand.DisposeOf;
  FGetConnInfoCommand.DisposeOf;
  FExecSQLCommand.DisposeOf;
  FLongTimeRunFuncCommand.DisposeOf;
  inherited;
end;

end.
