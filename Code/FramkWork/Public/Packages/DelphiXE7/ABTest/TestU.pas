unit TestU;

interface

uses
  FireDAC.Stan.ExprFuncs,
  FireDAC.Stan.Expr,


  ABPubFuncU,

  cxImageComboBox, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, ABThirdCustomQueryU, ABThirdQueryU,
  ABFramkWorkDictionaryQueryU, ABFramkWorkQueryU, System.Classes, Vcl.Controls,
  Vcl.StdCtrls, ABThirdDBU,forms,ABFramkWorkUserU, FireDAC.Phys.MSSQLDef,
  FireDAC.UI.Intf, FireDAC.VCLUI.Wait, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Phys, FireDAC.Phys.MSSQL, Vcl.Grids, Vcl.DBGrids, FireDAC.Comp.UI,
  FireDAC.Phys.ODBCBase, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxButtonEdit, cxDBEdit;




type
  TABTestForm = class(TForm)
    Button1: TButton;
    FDPhysMSSQLDriverLink1: TFDPhysMSSQLDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    DataSource1: TDataSource;
    DBGrid2: TDBGrid;
    FDConnection1: TFDConnection;
    ADQuery1: TFDQuery;
    DataSource2: TDataSource;
    FDQuery1: TFDQuery;
    DBGrid1: TDBGrid;
    FDConnection2: TFDConnection;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABTestForm: TABTestForm;

implementation
{$R *.dfm}

procedure TABTestForm.Button1Click(Sender: TObject);
var
  oField: TField;
  i: Integer;
begin
  ADQuery1.close;
  ADQuery1.FieldDefs.Clear;
  ADQuery1.Fields.Clear;

  ADQuery1.FieldDefs.Updated := False;
  ADQuery1.FieldDefs.Update;
  for i := 0 to ADQuery1.FieldDefs.Count - 1 do
    ADQuery1.FieldDefs[i].CreateField(Self);

  oField := TFloatField.Create(ADQuery1);
  oField.DisplayLabel := '���';
  oField.FieldName := 'tempMoney';
  oField.FieldKind := fkInternalCalc;
  oField.DefaultExpression := 'Oi_Quantity*Oi_Price*ABIsNull(Oi_Discount,0)';
  oField.DataSet := ADQuery1;

  ADQuery1.Open;


  ADQuery1.SaveToFile('c:\aa.aa');






  FDQuery1.close;
  FDQuery1.LoadFromFile('c:\aa.aa');

{
  if ABUser.Login(ltLongin) then
  begin
    ABQuery1_1.close;



    ABQuery1_1.open;
  end;
  }
end;

function ABRound_(const AArgs: array of Variant): Variant;
begin
  Result :=ABRound(AArgs[0],AArgs[1])
end;

function ABIsNull_(const AArgs: array of Variant): Variant;
begin
  Result :=ABIsNull(AArgs[0],AArgs[1])
end;

procedure ABFinalization;
begin
end;

procedure ABInitialization;
var
  oMan: TFDExpressionManager;
begin
  oMan := FDExpressionManager();
  oMan.RemoveFunction('ABRound');
  oMan.RemoveFunction('ABIsNull');
  oMan.AddFunction('ABRound',       ckUnknown, 0, dtDouble,    -1, 2, 2, 'ii', @ABRound_);
  oMan.AddFunction('ABIsNull',       ckUnknown, 0, dtDouble,    -1, 2, 2, 'ii', @ABIsNull_);
end;

Initialization
  ABInitialization;

Finalization
  ABFinalization;

end.


