object ABSys_Org_EnterpriseForm: TABSys_Org_EnterpriseForm
  Left = 422
  Top = 130
  Caption = #20225#19994#26426#26500'-'#25805#20316#21592#31649#29702
  ClientHeight = 550
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 750
    Height = 550
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = #20026#25805#20316#21592#20998#37197#35282#33394
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Sheet1_Panel1: TPanel
        Left = 0
        Top = 0
        Width = 742
        Height = 490
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Sheet1_Splitter1: TABcxSplitter
          Left = 195
          Top = 0
          Width = 8
          Height = 490
          HotZoneClassName = 'TcxMediaPlayer8Style'
          InvertDirection = True
          Control = Panel6
        end
        object Sheet1_Panel2: TPanel
          Left = 203
          Top = 0
          Width = 539
          Height = 490
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Splitter1: TABcxSplitter
            Left = 335
            Top = 0
            Width = 8
            Height = 490
            HotZoneClassName = 'TcxMediaPlayer8Style'
            AlignSplitter = salRight
            InvertDirection = True
            Control = Panel2
          end
          object Panel2: TPanel
            Left = 343
            Top = 0
            Width = 196
            Height = 490
            Align = alRight
            BevelOuter = bvNone
            Caption = 'Panel1'
            TabOrder = 0
            object Panel4: TPanel
              Left = 0
              Top = 0
              Width = 196
              Height = 25
              Align = alTop
              BevelOuter = bvNone
              Caption = #24403#21069#25805#20316#21592#20998#37197#30340#35282#33394
              TabOrder = 0
            end
            object RzCheckTree3: TABCheckTreeView
              Left = 0
              Top = 25
              Width = 196
              Height = 429
              MoveAfirm = False
              OnStateChang = RzCheckTree3StateChang
              OnStateChanging = RzCheckTree3StateChanging
              Align = alClient
              Ctl3D = False
              Flatness = cfAlwaysFlat
              GrayedIsChecked = False
              Indent = 19
              ParentCtl3D = False
              TabOrder = 1
            end
            object Panel8: TPanel
              Left = 0
              Top = 454
              Width = 196
              Height = 36
              Align = alBottom
              TabOrder = 2
              object Button1: TABcxButton
                Left = 6
                Top = 6
                Width = 44
                Height = 25
                Caption = #20840#36873
                LookAndFeel.Kind = lfFlat
                TabOrder = 0
                OnClick = Button1Click
                ShowProgressBar = False
              end
              object Button2: TABcxButton
                Left = 52
                Top = 6
                Width = 44
                Height = 25
                Caption = #20840#19981#36873
                LookAndFeel.Kind = lfFlat
                TabOrder = 1
                OnClick = Button2Click
                ShowProgressBar = False
              end
              object Button3: TABcxButton
                Left = 98
                Top = 6
                Width = 44
                Height = 25
                Caption = #21453#36873
                LookAndFeel.Kind = lfFlat
                TabOrder = 2
                OnClick = Button3Click
                ShowProgressBar = False
              end
              object ABcxButton1: TABcxButton
                Left = 147
                Top = 6
                Width = 44
                Height = 25
                Caption = #21047#26032
                LookAndFeel.Kind = lfFlat
                TabOrder = 3
                OnClick = ABcxButton1Click
                ShowProgressBar = False
              end
            end
          end
          object Panel3: TPanel
            Left = 0
            Top = 0
            Width = 335
            Height = 490
            Align = alClient
            BevelOuter = bvNone
            Caption = 'Panel1'
            TabOrder = 1
            object ABDBcxGrid2: TABcxGrid
              Left = 0
              Top = 52
              Width = 335
              Height = 438
              Align = alClient
              TabOrder = 0
              LookAndFeel.NativeStyle = False
              ExplicitLeft = 40
              ExplicitTop = 68
              object ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView
                PopupMenu.AutoHotkeys = maManual
                PopupMenu.CloseFootStr = False
                PopupMenu.LinkTableView = ABcxGridDBBandedTableView2
                PopupMenu.AutoApplyBestFit = True
                PopupMenu.AutoCreateAllItem = True
                Navigator.Buttons.CustomButtons = <>
                DataController.DataSource = ABDatasource1_1
                DataController.Filter.Options = [fcoCaseInsensitive]
                DataController.Filter.AutoDataSetFilter = True
                DataController.Filter.TranslateBetween = True
                DataController.Filter.TranslateIn = True
                DataController.Filter.TranslateLike = True
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsBehavior.AlwaysShowEditor = True
                OptionsBehavior.FocusCellOnTab = True
                OptionsBehavior.GoToNextCellOnEnter = True
                OptionsCustomize.ColumnsQuickCustomization = True
                OptionsCustomize.DataRowSizing = True
                OptionsData.Deleting = False
                OptionsData.Inserting = False
                OptionsSelection.HideFocusRectOnExit = False
                OptionsSelection.MultiSelect = True
                OptionsView.CellAutoHeight = True
                OptionsView.GroupByBox = False
                OptionsView.BandHeaders = False
                Bands = <
                  item
                  end>
                ExtPopupMenu.AutoHotkeys = maManual
                ExtPopupMenu.CloseFootStr = False
                ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView2
                ExtPopupMenu.AutoApplyBestFit = True
                ExtPopupMenu.AutoCreateAllItem = True
              end
              object cxGridLevel2: TcxGridLevel
                GridView = ABcxGridDBBandedTableView2
              end
            end
            object Panel1: TPanel
              Left = 0
              Top = 0
              Width = 335
              Height = 25
              Align = alTop
              BevelOuter = bvNone
              Caption = #24403#21069#23703#20301#19979#25805#20316#21592#21015#34920
              TabOrder = 1
            end
            object ABDBNavigator3: TABDBNavigator
              Left = 0
              Top = 25
              Width = 335
              Height = 27
              Align = alTop
              BevelOuter = bvNone
              ParentBackground = False
              ParentColor = True
              ShowCaption = False
              TabOrder = 2
              BigGlyph = False
              ImageLayout = blGlyphLeft
              DataSource = ABDatasource1_1
              VisibleButtons = [nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel, nbQuerySpacer, nbQuery, nbReport]
              ButtonRangeType = RtCustom
              BtnCustom1ImageIndex = -1
              BtnCustom2ImageIndex = -1
              BtnCustom3ImageIndex = -1
              BtnCustom4ImageIndex = -1
              BtnCustom5ImageIndex = -1
              BtnCustom6ImageIndex = -1
              BtnCustom7ImageIndex = -1
              BtnCustom8ImageIndex = -1
              BtnCustom9ImageIndex = -1
              BtnCustom10ImageIndex = -1
              BtnCustom1Caption = #33258#23450#20041'1'
              BtnCustom2Caption = #33258#23450#20041'2'
              BtnCustom3Caption = #33258#23450#20041'3'
              BtnCustom4Caption = #33258#23450#20041'4'
              BtnCustom5Caption = #33258#23450#20041'5'
              BtnCustom6Caption = #33258#23450#20041'6'
              BtnCustom7Caption = #33258#23450#20041'7'
              BtnCustom8Caption = #33258#23450#20041'8'
              BtnCustom9Caption = #33258#23450#20041'9'
              BtnCustom10Caption = #33258#23450#20041'10'
              BtnCustom1Kind = cxbkStandard
              BtnCustom2Kind = cxbkStandard
              BtnCustom3Kind = cxbkStandard
              BtnCustom4Kind = cxbkStandard
              BtnCustom5Kind = cxbkStandard
              BtnCustom6Kind = cxbkStandard
              BtnCustom7Kind = cxbkStandard
              BtnCustom8Kind = cxbkStandard
              BtnCustom9Kind = cxbkStandard
              BtnCustom10Kind = cxbkStandard
              ApprovedRollbackButton = nbNull
              ApprovedCommitButton = nbNull
              ButtonControlType = ctSingle
            end
          end
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 195
          Height = 490
          Align = alLeft
          TabOrder = 1
          object ABcxDBTreeView1: TABcxDBTreeView
            Left = 1
            Top = 53
            Width = 193
            Height = 436
            Align = alClient
            Bands = <
              item
              end>
            DataController.DataSource = ABDatasource1
            DataController.ParentField = 'Ti_ParentGuid'
            DataController.KeyField = 'Ti_Guid'
            DragMode = dmAutomatic
            Navigator.Buttons.CustomButtons = <>
            OptionsBehavior.ExpandOnIncSearch = True
            OptionsBehavior.IncSearch = True
            OptionsData.Editing = False
            OptionsData.Deleting = False
            OptionsSelection.CellSelect = False
            OptionsSelection.HideFocusRect = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.Headers = False
            PopupMenu.DefaultParentValue = '0'
            RootValue = -1
            TabOrder = 0
            Active = True
            ExtFullExpand = False
            CanSelectParent = True
            object ABcxDBTreeView1cxDBTreeListColumn1: TcxDBTreeListColumn
              DataBinding.FieldName = 'Ti_Name'
              Position.ColIndex = 0
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object ABcxDBTreeView1cxDBTreeListColumn2: TcxDBTreeListColumn
              Visible = False
              DataBinding.FieldName = 'Ti_Order'
              Position.ColIndex = 1
              Position.RowIndex = 0
              Position.BandIndex = 0
              SortOrder = soAscending
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
          end
          object Panel7: TPanel
            Left = 1
            Top = 1
            Width = 193
            Height = 25
            Align = alTop
            BevelOuter = bvNone
            Caption = #20225#19994#26426#26500
            TabOrder = 1
          end
          object ABDBNavigator2: TABDBNavigator
            Left = 1
            Top = 26
            Width = 193
            Height = 27
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            ParentColor = True
            ShowCaption = False
            TabOrder = 2
            BigGlyph = False
            ImageLayout = blGlyphLeft
            DataSource = ABDatasource1
            VisibleButtons = [nbQuery, nbReport]
            ButtonRangeType = RtCustom
            BtnCustom1ImageIndex = -1
            BtnCustom2ImageIndex = -1
            BtnCustom3ImageIndex = -1
            BtnCustom4ImageIndex = -1
            BtnCustom5ImageIndex = -1
            BtnCustom6ImageIndex = -1
            BtnCustom7ImageIndex = -1
            BtnCustom8ImageIndex = -1
            BtnCustom9ImageIndex = -1
            BtnCustom10ImageIndex = -1
            BtnCustom1Caption = #33258#23450#20041'1'
            BtnCustom2Caption = #33258#23450#20041'2'
            BtnCustom3Caption = #33258#23450#20041'3'
            BtnCustom4Caption = #33258#23450#20041'4'
            BtnCustom5Caption = #33258#23450#20041'5'
            BtnCustom6Caption = #33258#23450#20041'6'
            BtnCustom7Caption = #33258#23450#20041'7'
            BtnCustom8Caption = #33258#23450#20041'8'
            BtnCustom9Caption = #33258#23450#20041'9'
            BtnCustom10Caption = #33258#23450#20041'10'
            BtnCustom1Kind = cxbkStandard
            BtnCustom2Kind = cxbkStandard
            BtnCustom3Kind = cxbkStandard
            BtnCustom4Kind = cxbkStandard
            BtnCustom5Kind = cxbkStandard
            BtnCustom6Kind = cxbkStandard
            BtnCustom7Kind = cxbkStandard
            BtnCustom8Kind = cxbkStandard
            BtnCustom9Kind = cxbkStandard
            BtnCustom10Kind = cxbkStandard
            ApprovedRollbackButton = nbNull
            ApprovedCommitButton = nbNull
            ButtonControlType = ctSingle
          end
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 490
        Width = 742
        Height = 32
        Align = alBottom
        TabOrder = 1
        object CheckBox1: TCheckBox
          Tag = 91
          Left = 7
          Top = 7
          Width = 102
          Height = 17
          Caption = #26174#31034#25152#26377#25805#20316#21592
          Checked = True
          State = cbChecked
          TabOrder = 0
          OnClick = CheckBox1Click
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #20026#35282#33394#20998#37197#25805#20316#21592
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 742
        Height = 522
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object ABcxSplitter1: TABcxSplitter
          Left = 576
          Top = 0
          Width = 8
          Height = 522
          HotZoneClassName = 'TcxMediaPlayer8Style'
          AlignSplitter = salRight
          InvertDirection = True
          Control = Panel10
        end
        object Panel10: TPanel
          Left = 584
          Top = 0
          Width = 158
          Height = 522
          Align = alRight
          BevelOuter = bvNone
          Caption = 'Panel1'
          TabOrder = 0
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 158
            Height = 25
            Align = alTop
            BevelOuter = bvNone
            Caption = #24403#21069#35282#33394#20998#37197#30340#25805#20316#21592
            TabOrder = 0
          end
          object RzCheckTree1: TABCheckTreeView
            Left = 0
            Top = 25
            Width = 158
            Height = 461
            MoveAfirm = False
            OnStateChang = RzCheckTree1StateChang
            OnStateChanging = RzCheckTree1StateChanging
            Align = alClient
            Ctl3D = False
            Flatness = cfAlwaysFlat
            GrayedIsChecked = False
            Indent = 19
            ParentCtl3D = False
            TabOrder = 1
          end
          object Panel12: TPanel
            Left = 0
            Top = 486
            Width = 158
            Height = 36
            Align = alBottom
            TabOrder = 2
            object ABcxButton2: TABcxButton
              Left = 6
              Top = 6
              Width = 44
              Height = 25
              Caption = #20840#36873
              LookAndFeel.Kind = lfFlat
              TabOrder = 0
              OnClick = ABcxButton2Click
              ShowProgressBar = False
            end
            object ABcxButton3: TABcxButton
              Left = 52
              Top = 6
              Width = 44
              Height = 25
              Caption = #20840#19981#36873
              LookAndFeel.Kind = lfFlat
              TabOrder = 1
              OnClick = ABcxButton3Click
              ShowProgressBar = False
            end
            object ABcxButton4: TABcxButton
              Left = 98
              Top = 6
              Width = 44
              Height = 25
              Caption = #21453#36873
              LookAndFeel.Kind = lfFlat
              TabOrder = 2
              OnClick = ABcxButton4Click
              ShowProgressBar = False
            end
          end
        end
        object Panel13: TPanel
          Left = 0
          Top = 0
          Width = 576
          Height = 522
          Align = alClient
          BevelOuter = bvNone
          Caption = 'Panel1'
          TabOrder = 1
          object ABDBcxGrid1: TABcxGrid
            Left = 0
            Top = 27
            Width = 576
            Height = 495
            Align = alClient
            TabOrder = 0
            LookAndFeel.NativeStyle = False
            object ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
              PopupMenu.AutoHotkeys = maManual
              PopupMenu.CloseFootStr = False
              PopupMenu.LinkTableView = ABcxGridDBBandedTableView1
              PopupMenu.AutoApplyBestFit = True
              PopupMenu.AutoCreateAllItem = True
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = ABDatasource_AllKey
              DataController.Filter.Options = [fcoCaseInsensitive]
              DataController.Filter.AutoDataSetFilter = True
              DataController.Filter.TranslateBetween = True
              DataController.Filter.TranslateIn = True
              DataController.Filter.TranslateLike = True
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsBehavior.AlwaysShowEditor = True
              OptionsBehavior.FocusCellOnTab = True
              OptionsBehavior.GoToNextCellOnEnter = True
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsCustomize.DataRowSizing = True
              OptionsData.Deleting = False
              OptionsData.Inserting = False
              OptionsSelection.HideFocusRectOnExit = False
              OptionsSelection.MultiSelect = True
              OptionsView.CellAutoHeight = True
              OptionsView.GroupByBox = False
              OptionsView.BandHeaders = False
              Bands = <
                item
                end>
              ExtPopupMenu.AutoHotkeys = maManual
              ExtPopupMenu.CloseFootStr = False
              ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView1
              ExtPopupMenu.AutoApplyBestFit = True
              ExtPopupMenu.AutoCreateAllItem = True
            end
            object cxGridLevel1: TcxGridLevel
              GridView = ABcxGridDBBandedTableView1
            end
          end
          object ABDBNavigator1: TABDBNavigator
            Left = 0
            Top = 0
            Width = 576
            Height = 27
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            ParentColor = True
            ShowCaption = False
            TabOrder = 1
            BigGlyph = False
            ImageLayout = blGlyphLeft
            DataSource = ABDatasource_AllKey
            VisibleButtons = [nbQuery, nbReport]
            ButtonRangeType = RtCustom
            BtnCustom1ImageIndex = -1
            BtnCustom2ImageIndex = -1
            BtnCustom3ImageIndex = -1
            BtnCustom4ImageIndex = -1
            BtnCustom5ImageIndex = -1
            BtnCustom6ImageIndex = -1
            BtnCustom7ImageIndex = -1
            BtnCustom8ImageIndex = -1
            BtnCustom9ImageIndex = -1
            BtnCustom10ImageIndex = -1
            BtnCustom1Caption = #33258#23450#20041'1'
            BtnCustom2Caption = #33258#23450#20041'2'
            BtnCustom3Caption = #33258#23450#20041'3'
            BtnCustom4Caption = #33258#23450#20041'4'
            BtnCustom5Caption = #33258#23450#20041'5'
            BtnCustom6Caption = #33258#23450#20041'6'
            BtnCustom7Caption = #33258#23450#20041'7'
            BtnCustom8Caption = #33258#23450#20041'8'
            BtnCustom9Caption = #33258#23450#20041'9'
            BtnCustom10Caption = #33258#23450#20041'10'
            BtnCustom1Kind = cxbkStandard
            BtnCustom2Kind = cxbkStandard
            BtnCustom3Kind = cxbkStandard
            BtnCustom4Kind = cxbkStandard
            BtnCustom5Kind = cxbkStandard
            BtnCustom6Kind = cxbkStandard
            BtnCustom7Kind = cxbkStandard
            BtnCustom8Kind = cxbkStandard
            BtnCustom9Kind = cxbkStandard
            BtnCustom10Kind = cxbkStandard
            ApprovedRollbackButton = nbNull
            ApprovedCommitButton = nbNull
            ButtonControlType = ctSingle
          end
        end
      end
    end
  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select *  '
      'from ABSys_Org_TreeItem '
      'where Ti_Tr_Guid=dbo.Func_GetTreeGuid('#39'Enterprise Dir'#39')'
      'order by TI_TR_Guid,TI_Group,TI_Order'
      '')
    SqlUpdateDatetime = 42095.491960462960000000
    BeforeDeleteAsk = True
    FieldDefaultValues = 'Ti_Tr_Guid=select dbo.Func_GetTreeGuid('#39'Enterprise Dir'#39')'
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_TreeItem')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_TreeItem')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 18
    Top = 149
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 119
    Top = 155
  end
  object ABQuery1_1: TABQuery
    BeforePost = ABQuery1_1BeforePost
    AfterInsert = ABQuery1_1AfterInsert
    ActiveStoredUsage = []
    ReadOnly = False
    AfterScroll = ABQuery1_1AfterScroll
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    MasterFields = 'Ti_Guid'
    DetailFields = 'Op_Ti_Guid'
    SQL.Strings = (
      'select *  from ABSys_Org_Operator '
      
        'where Op_Code<>'#39'admin'#39' and Op_Code<>'#39'sysuser'#39' and Op_Ti_Guid=:Ti' +
        '_Guid'
      'order by OP_TI_Guid ,OP_Order')
    SqlUpdateDatetime = 42090.755219340280000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Operator')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_Operator')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    OnFieldGetText = ABQuery1_1OnFieldGetText
    OnFieldSetText = ABQuery1_1OnFieldSetText
    AddTableRowQueryRight = True
    Left = 10
    Top = 230
    ParamData = <
      item
        Name = 'Ti_Guid'
        ParamType = ptInput
      end>
  end
  object ABDatasource1_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_1
    Left = 104
    Top = 230
  end
  object ABQuery1_1_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1_1
    MasterFields = 'Op_Guid'
    DetailFields = 'Ok_Op_Guid'
    SQL.Strings = (
      
        ' select *  from ABSys_Right_OperatorKey where Ok_Op_Guid=:Op_Gui' +
        'd'
      '')
    BeforeDeleteAsk = False
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Right_OperatorKey')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Right_OperatorKey')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 10
    Top = 275
    ParamData = <
      item
        Name = 'Op_Guid'
        ParamType = ptInput
      end>
  end
  object ABDatasource1_1_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_1_1
    Left = 105
    Top = 275
  end
  object ABQuery_AllKey: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    AfterScroll = ABQuery_AllKeyAfterScroll
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select * from ABSys_Org_Key'
      'order by KE_Order '
      '')
    BeforeDeleteAsk = False
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Key')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_Key')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 16
    Top = 399
  end
  object ABQuery_AllUser: TABQuery
    BeforePost = ABQuery1_1BeforePost
    AfterInsert = ABQuery1_1AfterInsert
    ActiveStoredUsage = []
    ReadOnly = False
    AfterScroll = ABQuery1_1AfterScroll
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select *  from ABSys_Org_Operator '
      'where Op_Code<>'#39'admin'#39' and Op_Code<>'#39'sysuser'#39' '
      'order by OP_TI_Guid,OP_Order'
      '')
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Operator')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_Operator')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    OnFieldGetText = ABQuery1_1OnFieldGetText
    OnFieldSetText = ABQuery1_1OnFieldSetText
    AddTableRowQueryRight = True
    Left = 122
    Top = 86
  end
  object ABDatasource_AllKey: TABDatasource
    AutoEdit = False
    DataSet = ABQuery_AllKey
    Left = 120
    Top = 366
  end
  object ABQuery4: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource_AllKey
    MasterFields = 'Ke_Guid'
    DetailFields = 'Ok_Ke_Guid'
    SQL.Strings = (
      
        ' select *  from ABSys_Right_OperatorKey where Ok_Ke_Guid=:Ke_Gui' +
        'd'
      '')
    BeforeDeleteAsk = False
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Right_OperatorKey')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Right_OperatorKey')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 26
    Top = 459
    ParamData = <
      item
        Name = 'Ke_Guid'
        ParamType = ptInput
      end>
  end
end
