unit ABSys_Org_KeyU;

interface

uses
  ABPubCheckTreeViewU,
  ABPubSelectCheckTreeViewU,
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFramkWorkcxGridU,
  ABFramkWorkDictionaryQueryU,
  ABFramkWorkDBNavigatorU,
  ABFramkWorkFuncFormU,
  ABFramkWorkControlU,
  ABFramkWorkQueryU,
  ABFramkWorkFuncU,

  cxLookAndFeels,
  cxSplitter,
  cxButtons,
  cxLookAndFeelPainters,
  cxGrid,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxClasses,
  cxGridLevel,
  cxDBData,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  cxControls,
  cxPC,
  cxLabel,
  cxEdit,
  cxContainer,
  dxdbtree,
  dxtree,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,ComCtrls,
  ExtCtrls,DB,DBClient,StdCtrls,Menus, cxNavigator,
  dxBarBuiltInMenu, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Vcl.Buttons;

type
  TABSys_Org_KeyForm = class(TABFuncForm)
    ABDBNavigator1: TABDBNavigator;
    ABDatasource1: TABDatasource;
    ABQuery1_1: TABQuery;
    Sheet1_Panel1: TPanel;
    Sheet1_Splitter1: TABcxSplitter;
    Sheet1_Panel2: TPanel;
    Sheet1_PageControl2: TABcxPageControl;
    Sheet1_Detail_Sheet1: TcxTabSheet;
    Sheet1_Detail_Sheet3: TcxTabSheet;
    Sheet1_Detail_Sheet4: TcxTabSheet;
    ABcxLabel1: TABcxLabel;
    Sheet1_Detail_Sheet5: TcxTabSheet;
    ABDBcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    RzCheckTree1: TABCheckTreeview;
    RzCheckTree3: TABCheckTreeview;
    ABQuery1_2: TABQuery;
    ABQuery1_4: TABQuery;
    ABQuery1_4_1: TABQuery;
    ABDBcxGrid2: TABcxGrid;
    ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView;
    cxGridLevel2: TcxGridLevel;
    Panel1: TPanel;
    Button1: TABcxButton;
    Button2: TABcxButton;
    Button3: TABcxButton;
    Panel2: TPanel;
    ABcxButton1: TABcxButton;
    ABcxButton2: TABcxButton;
    ABcxButton3: TABcxButton;
    ABDatasource1_4: TABDatasource;
    ABDatasource1_4_1: TABDatasource;
    ABDBcxGrid3: TABcxGrid;
    ABcxGridDBBandedTableView3: TABcxGridDBBandedTableView;
    cxGridLevel3: TcxGridLevel;
    Panel3: TPanel;
    PopupMenu1: TPopupMenu;
    ABcxButton7: TABcxButton;
    ABcxButton5: TABcxButton;
    Panel4: TPanel;
    ABcxLabel2: TABcxLabel;
    ABcxLabel3: TABcxLabel;
    ABDBNavigator2: TABDBNavigator;
    ABDBNavigator3: TABDBNavigator;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    PopupMenu2: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    MenuItem10: TMenuItem;
    ABcxButton4: TABcxButton;
    ABcxButton6: TABcxButton;
    ProgressBar1: TProgressBar;
    ProgressBar2: TProgressBar;
    N1: TMenuItem;
    N2: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    cxTabSheet1: TcxTabSheet;
    RzCheckTree4: TABCheckTreeView;
    ABQuery1_3: TABQuery;
    ABQuery1_3Base: TABQuery;
    ABQuery1: TABQuery;
    Panel5: TPanel;
    ABcxButton8: TABcxButton;
    ABcxButton9: TABcxButton;
    ABcxButton10: TABcxButton;
    ABQuery_TableDir: TABQuery;
    ABQuery1_5: TABQuery;
    ABDatasource1_5: TABDatasource;
    Panel7: TPanel;
    ABcxSplitter1: TABcxSplitter;
    Panel8: TPanel;
    ABcxSplitter2: TABcxSplitter;
    Panel9: TPanel;
    ABcxLabel5: TABcxLabel;
    ABcxLabel6: TABcxLabel;
    ABcxLabel7: TABcxLabel;
    ABcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView4: TABcxGridDBBandedTableView;
    cxGridLevel4: TcxGridLevel;
    ABcxGrid2: TABcxGrid;
    ABcxGridDBBandedTableView5: TABcxGridDBBandedTableView;
    cxGridLevel5: TcxGridLevel;
    ABQuery1_4_2: TABQuery;
    ABDatasource1_4_2: TABDatasource;
    Panel6: TPanel;
    Panel10: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure ABQuery1AfterScroll(DataSet: TDataSet);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Sheet1_PageControl2Change(Sender: TObject);
    procedure RzCheckTree1StateChang(Sender: TObject; Node: TTreeNode;
      NewState: TCheckBoxState);
    procedure RzCheckTree3StateChang(Sender: TObject; Node: TTreeNode;
      NewState: TCheckBoxState);
    procedure ABcxButton7Click(Sender: TObject);
    procedure ABcxButton4Click(Sender: TObject);
    procedure ABcxButton5Click(Sender: TObject);
    procedure ABcxButton6Click(Sender: TObject);
    procedure RzCheckTree1StateChanging(Sender: TObject; Node: TTreeNode;
      var AllowChange: Boolean);
    procedure RzCheckTree4StateChang(Sender: TObject; Node: TTreeNode;
      NewState: TCheckBoxState);
    procedure MenuItem7Click(Sender: TObject);
    procedure ABQuery1_4AfterScroll(DataSet: TDataSet);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure ABcxGridDBBandedTableView4DblClick(Sender: TObject);
    procedure ABcxGridDBBandedTableView5DblClick(Sender: TObject);
  private
    FBegCheckTreeStateChange:boolean;

    FAllFuncTree: TClientDataset;
    FAllReportTree: TClientDataset;

    procedure ABInitDatasetAndTree;
    procedure RefreshCurKey;
    procedure DoKeyList(aType, aIsDel, aIsAdd, aKF_FU_Guid, aKR_ER_Guid,aKc_Ti_Guid: string);
    { Private declarations }
  public
    { Public declarations }
  end;
var
  ABSys_Org_KeyForm: TABSys_Org_KeyForm;

implementation
uses ABFramkWorkUserU;

{$R *.dfm}


procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Org_KeyForm.ClassName;
end;

exports
   ABRegister ;

procedure TABSys_Org_KeyForm.Button1Click(Sender: TObject);
begin
  if Sheet1_PageControl2.ActivePageIndex=0 then
  begin
    ABSetCheckTreeViewState(RzCheckTree1,coCheck);
  end
  else if Sheet1_PageControl2.ActivePageIndex=1 then
  begin
    ABSetCheckTreeViewState(RzCheckTree3,coCheck);
  end
  else if Sheet1_PageControl2.ActivePageIndex=2 then
  begin
    ABSetCheckTreeViewState(RzCheckTree4,coCheck);
  end;
end;

procedure TABSys_Org_KeyForm.Button2Click(Sender: TObject);
begin
  if Sheet1_PageControl2.ActivePageIndex=0 then
  begin
    ABSetCheckTreeViewState(RzCheckTree1,coUnCheck);
  end
  else if Sheet1_PageControl2.ActivePageIndex=1 then
  begin
    ABSetCheckTreeViewState(RzCheckTree3,coUnCheck);
  end
  else if Sheet1_PageControl2.ActivePageIndex=2 then
  begin
    ABSetCheckTreeViewState(RzCheckTree4,coUnCheck);
  end;
end;

procedure TABSys_Org_KeyForm.Button3Click(Sender: TObject);
begin
  if Sheet1_PageControl2.ActivePageIndex=0 then
  begin
    ABSetCheckTreeViewState(RzCheckTree1,coReverse);
  end
  else if Sheet1_PageControl2.ActivePageIndex=1 then
  begin
    ABSetCheckTreeViewState(RzCheckTree3,coReverse);
  end
  else if Sheet1_PageControl2.ActivePageIndex=2 then
  begin
    ABSetCheckTreeViewState(RzCheckTree4,coReverse);
  end;
end;

procedure TABSys_Org_KeyForm.ABQuery1AfterScroll(DataSet: TDataSet);
begin
  RefreshCurKey;
end;

procedure TABSys_Org_KeyForm.FormCreate(Sender: TObject);
begin
  Sheet1_PageControl2.ActivePageIndex:=0;
  ABInitFormDataSet([],[ABcxGridDBBandedTableView1],ABQuery1);

  FBegCheckTreeStateChange:=true;
  RefreshCurKey;
end;

procedure TABSys_Org_KeyForm.FormDestroy(Sender: TObject);
begin
  if Assigned(FAllFuncTree) then
    FreeAndNil(FAllFuncTree);
  if Assigned(FAllReportTree) then
    FreeAndNil(FAllReportTree);
end;

procedure TABSys_Org_KeyForm.MenuItem7Click(Sender: TObject);
begin
  ABSelectMenuItemOfGroup(TMenuItem(Sender));
end;

procedure TABSys_Org_KeyForm.Sheet1_PageControl2Change(Sender: TObject);
begin
  RefreshCurKey;
end;

procedure TABSys_Org_KeyForm.DoKeyList(
                              aType,aIsDel,aIsAdd,

                              aKF_FU_Guid,
                              aKR_ER_Guid,

                              aKc_Ti_Guid:string
                             );

var
  i,j:longint;
begin
  ABSetTableViewSelect(ABcxGridDBBandedTableView1);
  ABQuery1.DisableControls;
  try
    for I := 0 to ABcxGridDBBandedTableView1.DataController.GetSelectedCount- 1 do
    begin
      if ABcxGridDBBandedTableView1.DataController.GetSelectedCount>1 then
      begin
        j:= ABcxGridDBBandedTableView1.DataController.GetSelectedRowIndex(i);
        ABQuery1.RecNo:=ABcxGridDBBandedTableView1.DataController.GetRowInfo(j).RecordIndex+1;
      end;

      ABExecSQL('Main','exec Proc_AddOrDelKeySetup '+
                             QuotedStr(aType)+','+aIsDel+','+aIsAdd+','+
                             QuotedStr(ABQuery1.FieldByName('Ke_Guid').AsString)+','+

                             QuotedStr(aKF_FU_Guid)+','+
                             QuotedStr(aKR_ER_Guid)+','+

                             QuotedStr(aKc_Ti_Guid));
    end;
  finally
    ABQuery1.EnableControls;

    if ABQuery1_1.active then
      ABReFreshQuery(ABQuery1_1,[]);
    if ABQuery1_2.active then
      ABReFreshQuery(ABQuery1_2,[]);
    if ABQuery1_3.active then
      ABReFreshQuery(ABQuery1_3,[]);
  end;
end;

procedure TABSys_Org_KeyForm.RzCheckTree1StateChang(Sender: TObject;
  Node: TTreeNode; NewState: TCheckBoxState);
begin
  if not FBegCheckTreeStateChange then
    exit;

  ABSetTableViewSelect(ABcxGridDBBandedTableView1);
  if ABcxGridDBBandedTableView1.DataController.GetSelectedCount<=1 then
  begin
    ABUpdateCheckTreeViewStateChange(FBegCheckTreeStateChange,FAllFuncTree,ABQuery1_1,
                           'Fu_Guid','Kf_Fu_Guid',
                           Node,RzCheckTree1.State[Node],'IsFunc');
    exit;
  end;

  if (not Node.HasChildren) and
     (not ABPointerIsNull(Node.Data)) then
  begin
    FBegCheckTreeStateChange:=false;
    try
      if (ABSetDatasetRecno(FAllFuncTree,LongInt(Node.Data))>0) and
         (FAllFuncTree.FieldByName('IsFunc').AsBoolean) then
      begin
        DoKeyList( 'Func',
                   '1',
                   inttostr(ABBoolToInt(RzCheckTree1.State[Node]=cbChecked)),

                   FAllFuncTree.FindField('Fu_Guid').AsString,
                   '',
                   ''
                      );
      end;
    finally
      FBegCheckTreeStateChange:=True;
    end;
  end;
end;

procedure TABSys_Org_KeyForm.RzCheckTree3StateChang(Sender: TObject;
  Node: TTreeNode; NewState: TCheckBoxState);
begin
  if not FBegCheckTreeStateChange then
    exit;

  ABSetTableViewSelect(ABcxGridDBBandedTableView1);
  if ABcxGridDBBandedTableView1.DataController.GetSelectedCount<=1 then
  begin
    ABUpdateCheckTreeViewStateChange(FBegCheckTreeStateChange,FAllReportTree,ABQuery1_2,
                           'Er_Guid','Kr_Er_Guid',
                           Node,RzCheckTree3.State[Node],'IsFunc');
    exit;
  end;

  if (not Node.HasChildren) and
     (not ABPointerIsNull(Node.Data)) then
  begin
    FBegCheckTreeStateChange:=false;
    try
      if (ABSetDatasetRecno(FAllReportTree,LongInt(Node.Data))>0) and
         (FAllReportTree.FieldByName('IsFunc').AsBoolean) then
      begin
        DoKeyList( 'ExtendReport',
                   '1',
                   inttostr(ABBoolToInt(RzCheckTree3.State[Node]=cbChecked)),

                   '',
                   FAllReportTree.FindField('Er_Guid').AsString,
                   ''
                      );
      end;
    finally
      FBegCheckTreeStateChange:=True;
    end;
  end;
end;

procedure TABSys_Org_KeyForm.RzCheckTree4StateChang(Sender: TObject;
  Node: TTreeNode; NewState: TCheckBoxState);
begin
  if not FBegCheckTreeStateChange then
    exit;

  ABSetTableViewSelect(ABcxGridDBBandedTableView1);
  if (not Node.HasChildren) and
     (not ABPointerIsNull(Node.Data)) then
  begin
    FBegCheckTreeStateChange:=false;
    try
      if (ABSetDatasetRecno(ABQuery1_3Base,LongInt(Node.Data))>0) and
         (ABQuery1_3Base.FieldByName('IsFunc').AsBoolean) then
      begin
        DoKeyList( 'CustomObject',
                   '1',
                   inttostr(ABBoolToInt(RzCheckTree3.State[Node]=cbChecked)),

                   '',
                   '',
                   ABQuery1_3Base.FindField('Ti_Guid').AsString
                   );
      end;
    finally
      FBegCheckTreeStateChange:=True;
    end;
  end;
end;

procedure TABSys_Org_KeyForm.RzCheckTree1StateChanging(Sender: TObject;
  Node: TTreeNode; var AllowChange: Boolean);
begin
  AllowChange:= ABCheckTreeViewCanChangeState(ABQuery1);
end;

procedure TABSys_Org_KeyForm.ABInitDatasetAndTree;
  procedure ABCreatTreeDataSet_CustomObject;
  begin
    ABDataSetsToTree
    (
     RzCheckTree4.Items,
     ABQuery1_3Base,
     'TI_ParentGuid' ,
     'Ti_Guid',
     'TI_Name'
     );
  end;
  procedure ABCreatTreeDataSet_Func;
  var
    tempFuncTreeDataset,
    tempFuncDataset:TDataSet;
    tempOtherWhere:string;
  begin
    FAllFuncTree:=ABCreateClientDataSet(
     ['Fu_Ti_Guid' ,'Fu_Guid','Fu_Code','Fu_Name','Fu_Order','IsFunc'],
     [],
     [ftString     ,ftString ,ftString ,ftString , ftInteger, ftBoolean],
     [100          ,100      ,100      ,100      ,100       ,100]
                                   );
    FAllFuncTree.EmptyDataSet;
    ABCloseDataset(FAllFuncTree);
    FAllFuncTree.Open;

    tempOtherWhere:=ABIIF(ABPubUser.UseFuncRight,' and Fu_FileName in (select tempFilename from dbo.Func_GetFuncFileNameList('+QuotedStr(ABPubUser.FuncRight)+'))','');
    if ABPubUser.IsAdmin then
    begin
      tempFuncDataset:= ABGetDataset('Main',
                                ' SELECT Fu_Ti_Guid,Fu_Guid,Fu_Code,Fu_Name,Fu_Order  '+
                                ' FROM ABSys_Org_Function '+
                                ' where Fu_IsView=1 ' ,[],nil,true);
    end
    else
    begin
      tempFuncDataset:= ABGetDataset('Main',
                                ' SELECT Fu_Ti_Guid,Fu_Guid,Fu_Code,Fu_Name,Fu_Order  '+
                                ' FROM ABSys_Org_Function '+
                                ' where Fu_SysUserUse=1 and Fu_IsView=1 '+tempOtherWhere,[],nil,true);
    end;
    tempFuncTreeDataset:=ABGetConstSqlPubDataset( 'ABSys_Org_TreeItem',['Function Dir']);
    absetdatasetfilter(tempFuncTreeDataset,' Ti_Code<>'+ABQuotedStr('Public')+' and '+ABGetBooleanFilter(tempFuncTreeDataset,'TI_bit1',True));
    try
      ABDataSetsToNewDataSet(
        FAllFuncTree,
        ['Fu_Ti_Guid' ,'Fu_Guid','Fu_Code','Fu_Name','Fu_Order'],
        'IsFunc',

        tempFuncTreeDataset,
        'Ti_Guid',
        ['Ti_ParentGuid','Ti_Guid','Ti_Code','Ti_Name','Ti_Order'],

        tempFuncDataset,
        'Fu_Ti_Guid',
        ['Fu_Ti_Guid' ,'Fu_Guid','Fu_Code','Fu_Name','Fu_Order']
        );

      ABSetNoParentDatasetValue(
                                FAllFuncTree,
                                'Fu_Ti_Guid',
                                'Fu_Guid',
                                '0'
                                );
      ABDeleteNoSubRecord(
                          FAllFuncTree,
                          'Fu_Ti_Guid',
                          'Fu_Guid',
                          ['IsFunc'],
                          ['False']
                          );

      ABDataSetsToTree
      (
       RzCheckTree1.Items,
       FAllFuncTree,
       'Fu_Ti_Guid' ,
       'Fu_Guid',
       'Fu_Name'
       );
    finally
      absetdatasetfilter(tempFuncTreeDataset,emptystr);
      FAllFuncTree.MergeChangeLog;
      tempFuncDataset.Free;
    end;
  end;
  procedure ABCreatTreeDataSet_Report;
  var
    tempReportTreeDataset,
    tempReportDataset:TDataSet;
  begin
    FAllReportTree:=ABCreateClientDataSet(
     ['Er_Ti_Guid' ,'Er_Guid','Er_Name','Er_Order','IsFunc'],
     [],
     [ftString     ,ftString ,ftString , ftInteger, ftBoolean],
     [100          ,100      ,100      ,100       ,100]
                                   );
    FAllReportTree.EmptyDataSet;
    ABCloseDataset(FAllReportTree);
    FAllReportTree.Open;

    tempReportDataset:=ABGetDataset('Main',' select Er_Ti_Guid,Er_Guid,Er_Name,Er_Order '+
                                                    ' from ABSys_Org_ExtendReport ' ,[],nil,true);
    tempReportTreeDataset:=ABGetConstSqlPubDataset( 'ABSys_Org_TreeItem',['Extend ReportType']);
    try
      ABDataSetsToNewDataSet(
        FAllReportTree                                    ,
        ['Er_Ti_Guid' ,'Er_Guid','Er_Name','Er_Order']    ,
        'IsFunc'                                          ,

        tempReportTreeDataset                             ,
        'Ti_Guid'                                         ,
        ['Ti_ParentGuid','Ti_Guid','Ti_Name','Ti_Order']  ,

        tempReportDataset                                 ,
        'Er_Ti_Guid'                                      ,
        ['Er_Ti_Guid','Er_Guid','Er_Name','Er_Order']
        );

        ABSetNoParentDatasetValue(
                                  FAllReportTree,
                                  'ER_Ti_Guid',
                                  'ER_Guid',
                                  '0'
                                  );
        ABDeleteNoSubRecord(
                            FAllReportTree,
                            'ER_Ti_Guid',
                            'ER_Guid',
                            ['IsFunc'],
                            ['False']
                            );

      ABDataSetsToTree
      (
       RzCheckTree3.Items,
       FAllReportTree,
       'Er_Ti_Guid' ,
       'Er_Guid',
       'Er_Name'
       );
    finally
      absetdatasetfilter(tempReportTreeDataset,emptystr);
      FAllReportTree.MergeChangeLog;
      tempReportDataset.free;
    end;
  end;
begin
  case Sheet1_PageControl2.ActivePageIndex of
    0:
    begin
      if not ABQuery1_1.Active then
      begin
        ABInitFormDataSet([],[],ABQuery1_1);
        ABCreatTreeDataSet_Func;
      end;
    end;
    1:
    begin
      if not ABQuery1_2.Active then
      begin
        ABInitFormDataSet([],[],ABQuery1_2);
        ABCreatTreeDataSet_Report;
      end;
    end;
    2:
    begin
      if not ABQuery1_3.Active then
      begin
        ABInitFormDataSet([],[],ABQuery1_3Base);
        ABInitFormDataSet([],[],ABQuery1_3);
        ABCreatTreeDataSet_CustomObject;
      end;
    end;
    3:
    begin
      if not ABQuery1_4.Active then
      begin
        ABInitFormDataSet([],[ABcxGridDBBandedTableView2],ABQuery1_4);
        ABInitFormDataSet([],[ABcxGridDBBandedTableView4],ABQuery1_4_1);
      end;
    end;
    4:
    begin
      if not ABQuery1_5.Active then
      begin
        ABInitFormDataSet([],[ABcxGridDBBandedTableView3],ABQuery1_5);
      end;
    end;
  end;
end;

procedure TABSys_Org_KeyForm.RefreshCurKey;
begin
  if not FBegCheckTreeStateChange then
    exit;

  ABInitDatasetAndTree;
  case Sheet1_PageControl2.ActivePageIndex of
    0:
    begin
      ABRefreshCheckTreeViewState(FBegCheckTreeStateChange,RzCheckTree1,FAllFuncTree,ABQuery1_1,
                         'Fu_Guid','Kf_Fu_Guid',
                         'IsFunc');
    end;
    1:
    begin
      ABRefreshCheckTreeViewState(FBegCheckTreeStateChange,RzCheckTree3,FAllReportTree,ABQuery1_2,
                         'Er_Guid','Kr_Er_Guid',
                         'IsFunc');
    end;
    2:
    begin
      ABRefreshCheckTreeViewState(FBegCheckTreeStateChange,RzCheckTree4,ABQuery1_3Base,ABQuery1_3,
                         'Ti_Guid','KC_TI_Guid',
                         'IsFunc'
                          );
    end;
  end;
end;

procedure TABSys_Org_KeyForm.ABcxButton7Click(Sender: TObject);
var
  tempDatasetTable:TDataSet;
  tempStrings:TStrings;
  I: Integer;
begin
  ABPostDataset(ABQuery1_4);
  ABQuery1_4.BeforeDeleteAsk:=false;
  ABQuery1_4.DisableControls;
  tempStrings:=TStringList.Create;
  TStringList(tempStrings).Sorted:=true;
  tempDatasetTable     := ABGetDataset('Main', ' select Ta_Guid,Ta_Ti_Guid,Ta_Caption from ABSys_Org_Table ',[]);
  try
    ABQuery1_4.First;
    while not ABQuery1_4.Eof do
    begin
      tempStrings.Add(ABQuery1_4.FieldByName('KT_TA_Guid').AsString);

      ABQuery1_4.Next;
    end;

    if ABSelectCheckTreeView(
        ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['Table Dir']),
        'Ti_ParentGuid',
        'Ti_Guid',
        'Ti_Name',
        False,

        tempDatasetTable,
        'Ta_Ti_Guid',
        'Ta_Caption',

        tempStrings,
        'Ta_Guid'
        ) then
    begin
      ProgressBar1.Visible:=true;
      ProgressBar1.Max:=ABQuery1_4.RecordCount;
      ProgressBar1.MIN:=0;
      try
        ABQuery1_4.First;
        while not ABQuery1_4.Eof do
        begin
          Application.ProcessMessages;
          ProgressBar1.Position:=ProgressBar1.Position+1;

          if tempStrings.IndexOf(ABQuery1_4.FieldByName('KT_TA_Guid').AsString)<0 then
          begin
            ABQuery1_4.Delete;
            Continue;
          end;

          ABQuery1_4.Next;
        end;
      finally
        ProgressBar1.Position:=0;
        ProgressBar1.Visible:=false;
      end;

      ProgressBar1.Visible:=true;
      ProgressBar1.Max:=tempStrings.Count;
      ProgressBar1.MIN:=0;
      try
        for I := 0 to tempStrings.Count-1 do
        begin
          Application.ProcessMessages;
          ProgressBar1.Position:=ProgressBar1.Position+1;
          if not ABQuery1_4.Locate('KT_TA_Guid',tempStrings[i],[]) then
          begin
            ABQuery1_4.Append;
            ABQuery1_4.FieldByName('KT_TA_Guid').AsString:=tempStrings[i];
            ABQuery1_4.Post;
          end;
        end;
      finally
        ProgressBar1.Position:=0;
        ProgressBar1.Visible:=false;
      end;
    end;
  finally
    ABQuery1_4.BeforeDeleteAsk:=true;
    ABQuery1_4.EnableControls;
    tempDatasetTable.free;
    tempStrings.Free;
  end;
end;

procedure TABSys_Org_KeyForm.ABcxGridDBBandedTableView4DblClick(
  Sender: TObject);
begin
  SpeedButton1Click(SpeedButton1);
end;

procedure TABSys_Org_KeyForm.ABcxGridDBBandedTableView5DblClick(
  Sender: TObject);
begin
  SpeedButton3Click(SpeedButton3);
end;

procedure TABSys_Org_KeyForm.ABcxButton5Click(Sender: TObject);
var
  tempBackStopMultiSelectUpdate:boolean;
  tempMultiSelectFieldNames:array of string;
  tempMultiSelectFieldValues:array of Variant;
  tempMultiSelectValuesIsCale:array of Boolean;
  tempMultiSelectType:TABMultiSelectType;
  tempCount:LongInt;
  I: Integer;
begin
  tempBackStopMultiSelectUpdate:=ABQuery1_4.StopMultiSelectUpdate;
  ABQuery1_4.StopMultiSelectUpdate:=true;
  try
    if N9.checked then
    begin
      tempMultiSelectType:=mtCur;
    end
    else if N10.checked then
    begin
      tempMultiSelectType:=mtSelect;
    end
    else if N11.checked then
    begin
      tempMultiSelectType:=mtCurToEnd;
    end
    else
    begin
      tempMultiSelectType:=mtAll;
    end;

    tempCount:=0;
    if N5.checked then
    begin
      tempCount:=tempCount+1;
      SetLength(tempMultiSelectFieldNames,tempCount);
      tempMultiSelectFieldNames[tempCount-1]:='Kt_CanInsert';
    end;
    if N6.checked then
    begin
      tempCount:=tempCount+1;
      SetLength(tempMultiSelectFieldNames,tempCount);
      tempMultiSelectFieldNames[tempCount-1]:='Kt_CanDelete';
    end;
    if N7.checked then
    begin
      tempCount:=tempCount+1;
      SetLength(tempMultiSelectFieldNames,tempCount);
      tempMultiSelectFieldNames[tempCount-1]:='Kt_CanEdit';
    end;
    if N8.checked then
    begin
      tempCount:=tempCount+1;
      SetLength(tempMultiSelectFieldNames,tempCount);
      tempMultiSelectFieldNames[tempCount-1]:='Kt_CanPrint';
    end;

    if (High(tempMultiSelectFieldNames)>=0) then
    begin
      SetLength(tempMultiSelectFieldValues,tempCount);
      SetLength(tempMultiSelectValuesIsCale,tempCount);
      for I := Low(tempMultiSelectFieldNames) to High(tempMultiSelectFieldNames) do
      begin
        tempMultiSelectFieldValues[i]:=ABIIF(N13.Checked,False,true);
        tempMultiSelectValuesIsCale[i]:=false;
      end;

      ABSetFieldValue_MultiSelect(ABcxGridDBBandedTableView2.DataController,
                                  tempMultiSelectType,
                                  tempMultiSelectFieldNames,
                                  tempMultiSelectFieldValues,
                                  tempMultiSelectValuesIsCale
                                  );

    end;
  finally
    ABQuery1_4.StopMultiSelectUpdate:=tempBackStopMultiSelectUpdate;
  end;
end;

procedure TABSys_Org_KeyForm.ABcxButton4Click(Sender: TObject);
var
  tempDatasetField:TDataSet;
  tempStrings:TStrings;
  I: Integer;
begin
  if not ABQuery_TableDir.Active then
    ABInitFormDataSet([],[],ABQuery_TableDir);

  ABSetDataSetLevel(ABQuery_TableDir,
                   'tempGuid',
                   'tempParentGuid',
                   'tempLevel'
                   );
  ABQuery_TableDir.IndexFieldNames:='tempLevel;Tr_Order;Ti_Order';


  ABPostDataset(ABQuery1_5);
  ABQuery1_5.BeforeDeleteAsk:=false;
  ABQuery1_5.DisableControls;
  tempStrings:=TStringList.Create;
  TStringList(tempStrings).Sorted:=true;
  tempDatasetField     := ABGetDataset('Main', ' select Fi_Ta_Guid,Fi_Caption,Fi_Ta_Guid+''=''+Fi_Guid tempGuid from   ABSys_Org_Field ',[]);
  try
    ABQuery1_5.First;
    while not ABQuery1_5.Eof do
    begin
      tempStrings.Add(ABQuery1_5.FieldByName('KF_TA_Guid').AsString+'='+ABQuery1_5.FieldByName('KF_Fi_Guid').AsString);

      ABQuery1_5.Next;
    end;

    if ABSelectCheckTreeView(
        ABQuery_TableDir,
        'tempParentGuid',
        'tempGuid',
        'tempName',
        True,

        tempDatasetField,
        'Fi_Ta_Guid',
        'Fi_Caption',

        tempStrings,
        'tempGuid'
        ) then
    begin
      ProgressBar2.Visible:=true;
      ProgressBar2.Max:=ABQuery1_5.RecordCount;
      ProgressBar2.MIN:=0;
      try
        ABQuery1_5.First;
        while not ABQuery1_5.Eof do
        begin
          Application.ProcessMessages;
          ProgressBar2.Position:=ProgressBar2.Position+1;

          if tempStrings.IndexOf(ABQuery1_5.FieldByName('KF_TA_Guid').AsString+'='+ABQuery1_5.FieldByName('KF_Fi_Guid').AsString)<0 then
          begin
            ABQuery1_5.Delete;
            Continue;
          end;

          ABQuery1_5.Next;
        end;
      finally
        ProgressBar2.Position:=0;
        ProgressBar2.Visible:=false;
      end;

      ProgressBar2.Visible:=true;
      ProgressBar2.Max:=tempStrings.Count;
      ProgressBar2.MIN:=0;
      try
        for I := 0 to tempStrings.Count-1 do
        begin
          Application.ProcessMessages;
          ProgressBar2.Position:=ProgressBar2.Position+1;
          if not ABQuery1_5.Locate('KF_FI_Guid',tempStrings.ValueFromIndex[i],[]) then
          begin
            ABQuery1_5.Append;
            ABQuery1_5.FieldByName('KF_TA_Guid').AsString:=tempStrings.Names[i];
            ABQuery1_5.FieldByName('KF_FI_Guid').AsString:=tempStrings.ValueFromIndex[i];
            ABQuery1_5.Post;
          end;
        end;
      finally
        ProgressBar2.Position:=0;
        ProgressBar2.Visible:=false;
      end;
    end;
  finally
    ABQuery1_5.EnableControls;
    ABQuery1_5.BeforeDeleteAsk:=true;
    tempDatasetField.free;
    tempStrings.Free;
  end;
end;

procedure TABSys_Org_KeyForm.ABcxButton6Click(Sender: TObject);
var
  tempBackStopMultiSelectUpdate:boolean;
  tempMultiSelectFieldNames:array of string;
  tempMultiSelectFieldValues:array of Variant;
  tempMultiSelectValuesIsCale:array of Boolean;
  tempMultiSelectType:TABMultiSelectType;
  tempCount:LongInt;
  I: Integer;
begin
  tempBackStopMultiSelectUpdate:=ABQuery1_5.StopMultiSelectUpdate;
  ABQuery1_5.StopMultiSelectUpdate:=true;
  try
    if MenuItem7.checked then
    begin
      tempMultiSelectType:=mtCur;
    end
    else if MenuItem8.checked then
    begin
      tempMultiSelectType:=mtSelect;
    end
    else if MenuItem9.checked then
    begin
      tempMultiSelectType:=mtCurToEnd;
    end
    else
    begin
      tempMultiSelectType:=mtAll;
    end;

    tempCount:=0;
    if MenuItem2.checked then
    begin
      tempCount:=tempCount+1;
      SetLength(tempMultiSelectFieldNames,tempCount);
      tempMultiSelectFieldNames[tempCount-1]:='Kf_CanEditInEdit';
    end;
    if MenuItem3.checked then
    begin
      tempCount:=tempCount+1;
      SetLength(tempMultiSelectFieldNames,tempCount);
      tempMultiSelectFieldNames[tempCount-1]:='Kf_CanEditInInsert';
    end;
    if MenuItem4.checked then
    begin
      tempCount:=tempCount+1;
      SetLength(tempMultiSelectFieldNames,tempCount);
      tempMultiSelectFieldNames[tempCount-1]:='Kf_Visible';
    end;

    if (High(tempMultiSelectFieldNames)>=0) then
    begin
      SetLength(tempMultiSelectFieldValues,tempCount);
      SetLength(tempMultiSelectValuesIsCale,tempCount);
      for I := Low(tempMultiSelectFieldNames) to High(tempMultiSelectFieldNames) do
      begin
        tempMultiSelectFieldValues[i]:=ABIIF(N15.Checked,False,true);
        tempMultiSelectValuesIsCale[i]:=false;
      end;

      ABSetFieldValue_MultiSelect(ABcxGridDBBandedTableView3.DataController,
                                  tempMultiSelectType,
                                  tempMultiSelectFieldNames,
                                  tempMultiSelectFieldValues,
                                  tempMultiSelectValuesIsCale
                                  );

    end;
  finally
    ABQuery1_5.StopMultiSelectUpdate:=tempBackStopMultiSelectUpdate;
  end;
end;

procedure TABSys_Org_KeyForm.SpeedButton1Click(Sender: TObject);
var
  i,j:longint;
  tempList:TStrings;
begin
  ABSetTableViewSelect(ABcxGridDBBandedTableView4);
  tempList:=TStringList.Create;
  ABQuery1_4_1.DisableControls;
  try
    for I := 0 to ABcxGridDBBandedTableView4.DataController.GetSelectedCount- 1 do
    begin
      j:= ABcxGridDBBandedTableView4.DataController.GetSelectedRowIndex(i);
      ABQuery1_4_1.RecNo:=ABcxGridDBBandedTableView4.DataController.GetRowInfo(j).RecordIndex+1;

      tempList.Add(ABQuery1_4_1.FieldByName('KR_Guid').AsString);
    end;

    ABQuery1_4_1.First;
    while not ABQuery1_4_1.Eof do
    begin
      if tempList.IndexOf(ABQuery1_4_1.FieldByName('KR_Guid').AsString)>=0 then
      begin
        ABQuery1_4_1.Delete;
      end
      else
      begin
        ABQuery1_4_1.Next;
      end;
    end;
  finally
    tempList.Free;
    ABQuery1_4_1.EnableControls;
  end;
end;

procedure TABSys_Org_KeyForm.SpeedButton2Click(Sender: TObject);
begin
  if not ABQuery1_4_1.IsEmpty then
  begin
    try
      ABDeleteDataset(ABQuery1_4_1);
    finally
      ABReFreshQuery(ABQuery1_4_1,[]);
    end;
  end;
end;

procedure TABSys_Org_KeyForm.ABQuery1_4AfterScroll(DataSet: TDataSet);
var
  tempDataSet: TDataSet;
  tempTA_TableRowFromTable,
  tempTA_TableRowFromTableFilterFieldNames,
  tempTA_TableRowFromTableCaptionFieldNames,
  tempSQL:string;
begin
  if not DataSet.IsEmpty then
  begin
    if DataSet.FieldByName('KT_TA_Guid').AsString<>EmptyStr then
    begin
      tempSQL:=EmptyStr;
      tempDataSet:=ABGetDataset('main',
                               ' select Cl_Name,Ta_Name,TA_TableRowFromTable,TA_TableRowFromTableFilterFieldNames,TA_TableRowFromTableCaptionFieldNames '+
                               ' from ABSys_Org_Table,ABSys_Org_ConnList '+
                               ' where Ta_CL_Guid=CL_Guid and Ta_Guid='+QuotedStr(DataSet.FieldByName('KT_TA_Guid').AsString),[]);
      try
        ABQuery1_4_2.FreeFieldExtInfos;
        ABQuery1_4_2.Close;

        tempTA_TableRowFromTable:=tempDataSet.FieldByName('TA_TableRowFromTable').AsString;
        tempTA_TableRowFromTableFilterFieldNames:=tempDataSet.FieldByName('TA_TableRowFromTableFilterFieldNames').AsString;
        tempTA_TableRowFromTableCaptionFieldNames:=tempDataSet.FieldByName('TA_TableRowFromTableCaptionFieldNames').AsString;

        if tempTA_TableRowFromTable=EmptyStr then
          tempTA_TableRowFromTable:= tempDataSet.FieldByName('Ta_Name').AsString;
        if tempTA_TableRowFromTableFilterFieldNames=EmptyStr then
          tempTA_TableRowFromTableFilterFieldNames:= ABGetTablePrimaryKeyFieldNames(tempDataSet.FieldByName('Ta_Name').AsString,tempDataSet.FieldByName('Cl_Name').AsString,'+','cast( ',' as nvarchar)');
        if tempTA_TableRowFromTableCaptionFieldNames=EmptyStr then
          tempTA_TableRowFromTableCaptionFieldNames:= ABGetTableNoTypeFieldNames(tempDataSet.FieldByName('Ta_Name').AsString,'',false,True,False,tempDataSet.FieldByName('Cl_Name').AsString,'+','cast( ',' as nvarchar)');

        if (tempTA_TableRowFromTable<>EmptyStr) and
           (tempTA_TableRowFromTableFilterFieldNames<>EmptyStr) and
           (tempTA_TableRowFromTableCaptionFieldNames<>EmptyStr) then
        begin
          tempSQL:=' SELECT '+tempTA_TableRowFromTableFilterFieldNames+' tempValue,'+
                   '        '+tempTA_TableRowFromTableCaptionFieldNames+' tempCaption'+
                   ' FROM '+tempTA_TableRowFromTable;
        end;

        if tempSQL <> emptystr then
        begin
          ABcxGridDBBandedTableView5.OptionsView.ColumnAutoWidth:=false;
          ABQuery1_4_2.ConnName:=tempDataSet.FieldByName('Cl_Name').AsString;
          ABQuery1_4_2.SQL.Text:=tempSQL;
          ABInitFormDataSet([],[ABcxGridDBBandedTableView5],ABQuery1_4_2,True,false);
          ABcxGridDBBandedTableView5.GetColumnByFieldName('tempValue').Visible:=false;
          ABcxGridDBBandedTableView5.GetColumnByFieldName('tempCaption').Visible:=True;
          ABcxGridDBBandedTableView5.GetColumnByFieldName('tempValue').Caption:='ֵ';
          ABcxGridDBBandedTableView5.GetColumnByFieldName('tempCaption').Caption:='����';
          ABcxGridDBBandedTableView5.ApplyBestFit;
          ABcxGridDBBandedTableView5.OptionsView.ColumnAutoWidth:=
            (ABcxGridDBBandedTableView5.GetColumnByFieldName('tempCaption').Width<ABcxGrid2.Width);

        end;
      finally
        tempDataSet.Free;
      end;
    end;
  end;
end;

procedure TABSys_Org_KeyForm.SpeedButton3Click(Sender: TObject);
var
  i,j:longint;
begin
  ABSetTableViewSelect(ABcxGridDBBandedTableView5);
  ABcxGridDBBandedTableView5.DataController.BeginFullUpdate;
  try
    for I := 0 to ABcxGridDBBandedTableView5.DataController.GetSelectedCount- 1 do
    begin
      j:= ABcxGridDBBandedTableView5.DataController.GetSelectedRowIndex(i);
      ABQuery1_4_2.RecNo:=ABcxGridDBBandedTableView5.DataController.GetRowInfo(j).RecordIndex+1;

      if (not ABQuery1_4_1.Locate('KR_TA_KeyFieldValue',ABQuery1_4_2.FieldByName('tempValue').AsString,[])) then
      begin
        ABExecSQL('main',' insert into ABSys_Right_KeyTableRow(KR_KT_Guid,KR_TA_KeyFieldValue,KR_TA_KeyFieldRemark,KR_Guid,PubID) '+
                         ' values('+
                           QuotedStr(ABQuery1_4.FieldByName('KT_Guid').AsString)+','+
                           QuotedStr(ABQuery1_4_2.FieldByName('tempValue').AsString)+','+
                           QuotedStr(ABQuery1_4_2.FieldByName('tempCaption').AsString)+','+
                           'newid(),'+
                           'newid())');
      end;
    end;
  finally
    ABcxGridDBBandedTableView5.DataController.EndFullUpdate;
    ABReFreshQuery(ABQuery1_4_1,[]);
  end;
end;

procedure TABSys_Org_KeyForm.SpeedButton4Click(Sender: TObject);
begin
  if not ABQuery1_4_2.IsEmpty then
  begin
    ABQuery1_4_1.DisableControls;
    ABQuery1_4_2.DisableControls;
    try
      ABQuery1_4_2.First;
      while not ABQuery1_4_2.Eof do
      begin
        if (not ABQuery1_4_1.Locate('KR_TA_KeyFieldValue',ABQuery1_4_2.FieldByName('tempValue').AsString,[])) then
        begin
          ABExecSQL('main',' insert into ABSys_Right_KeyTableRow(KR_KT_Guid,KR_TA_KeyFieldValue,KR_TA_KeyFieldRemark,KR_Guid,PubID) '+
                           ' values('+
                             QuotedStr(ABQuery1_4.FieldByName('KT_Guid').AsString)+','+
                             QuotedStr(ABQuery1_4_2.FieldByName('tempValue').AsString)+','+
                             QuotedStr(ABQuery1_4_2.FieldByName('tempCaption').AsString)+','+
                             'newid(),'+
                             'newid())');
        end;
        ABQuery1_4_2.Next;
      end;
    finally
      ABQuery1_4_1.EnableControls;
      ABQuery1_4_2.EnableControls;
      ABReFreshQuery(ABQuery1_4_1,[]);
    end;
  end;
end;

Initialization
  RegisterClass(TABSys_Org_KeyForm);
Finalization
  UnRegisterClass(TABSys_Org_KeyForm);


end.






