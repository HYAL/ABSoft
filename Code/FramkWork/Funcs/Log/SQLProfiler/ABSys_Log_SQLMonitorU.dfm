object ABSys_Log_SQLMonitorForm: TABSys_Log_SQLMonitorForm
  Left = 173
  Top = 129
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Anchors = [akLeft, akTop, akRight]
  Caption = 'SQL'#30417#25511
  ClientHeight = 550
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  ExplicitWidth = 320
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 514
    Width = 750
    Height = 36
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      750
      36)
    object Button3: TABcxButton
      Left = 579
      Top = 5
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #28165#38500#26085#24535
      LookAndFeel.Kind = lfFlat
      TabOrder = 0
      OnClick = Button3Click
      ShowProgressBar = False
    end
    object Button4: TABcxButton
      Left = 660
      Top = 5
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #36864#20986
      LookAndFeel.Kind = lfFlat
      TabOrder = 1
      OnClick = Button4Click
      ShowProgressBar = False
    end
    object Button1: TABcxButton
      Left = 196
      Top = 5
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #20174#25991#20214#20013#21152#36733
      LookAndFeel.Kind = lfFlat
      TabOrder = 2
      OnClick = Button1Click
      ShowProgressBar = False
    end
    object Button2: TABcxButton
      Left = 292
      Top = 5
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #20445#23384#21040#25991#20214#20013
      LookAndFeel.Kind = lfFlat
      TabOrder = 3
      OnClick = Button2Click
      ShowProgressBar = False
    end
    object CheckBox1: TABcxCheckBox
      Left = 106
      Top = 8
      Caption = #31383#20307#32622#39030
      Properties.Alignment = taRightJustify
      Properties.NullStyle = nssUnchecked
      Style.BorderStyle = ebsFlat
      Style.LookAndFeel.Kind = lfFlat
      StyleDisabled.LookAndFeel.Kind = lfFlat
      StyleFocused.LookAndFeel.Kind = lfFlat
      StyleHot.LookAndFeel.Kind = lfFlat
      TabOrder = 4
      Transparent = True
      OnClick = CheckBox1Click
      Width = 71
    end
    object RadioGroup1: TABcxRadioGroup
      Tag = 91
      Left = 0
      Top = 0
      Align = alLeft
      Alignment = alCenterCenter
      BiDiMode = bdLeftToRight
      ParentBiDiMode = False
      Properties.Columns = 2
      Properties.DefaultValue = 0
      Properties.Items = <
        item
          Caption = 'ON'
        end
        item
          Caption = 'OFF'
        end>
      ItemIndex = 0
      Style.BorderStyle = ebsNone
      Style.LookAndFeel.Kind = lfFlat
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfFlat
      StyleFocused.LookAndFeel.Kind = lfFlat
      StyleHot.LookAndFeel.Kind = lfFlat
      TabOrder = 5
      Height = 36
      Width = 99
    end
  end
  object ABcxPageControl1: TABcxPageControl
    Left = 0
    Top = 0
    Width = 750
    Height = 514
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = cxTabSheet1
    Properties.CustomButtons.Buttons = <>
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = False
    ActivePageIndex = 0
    ClientRectBottom = 513
    ClientRectLeft = 1
    ClientRectRight = 749
    ClientRectTop = 21
    object cxTabSheet1: TcxTabSheet
      Caption = 'SQL Monitor'
      ImageIndex = 0
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Memo1: TABcxMemo
        Left = 0
        Top = 0
        Align = alClient
        Properties.ScrollBars = ssBoth
        TabOrder = 0
        Height = 492
        Width = 748
      end
    end
  end
end
