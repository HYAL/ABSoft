unit ABMainDetailTableTemplateU;

interface

uses
  ABPubFuncU,
  ABPubPanelU,
  ABPubDBU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,

  ABFramkWorkQuerySelectFieldPanelU,
  ABFramkWorkcxGridU,
  ABFramkWorkDictionaryQueryU,
  ABFramkWorkDBNavigatorU,
  ABFramkWorkFuncFormU,
  ABFramkWorkControlU,
  ABFramkWorkQueryU,
  ABFramkWorkFuncU,
  ABFramkWorkOperatorDatasetU,

  cxDateUtils,
  cxLookAndFeels,
  cxTextEdit,
  cxContainer,
  cxLabel,
  cxCalendar,
  cxDropDownEdit,
  cxMaskEdit,
  cxButtons,
  cxLookAndFeelPainters,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGrid,
  cxGridDBTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxClasses,
  cxGridLevel,
  cxDBData,
  cxEdit,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  cxDBNavigator,
  cxNavigator,
  cxControls,
  cxPC,
  dxCore,
  dxStatusBar,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,DB,
  DBClient,ExtCtrls,ComCtrls,StdCtrls,DBCtrls,Menus,
  ABFramkWorkQueryAllFieldComboxU, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, ABFramkWorkDBPanelU, cxSplitter, dxBarBuiltInMenu ;

type
  TABMainDetailTableTemplateForm = class(TABFuncForm)
    ABDBStatusBar1: TABdxDBStatusBar;
    ABNavigator1: TABDBNavigator;
    MainPanel1: TPanel;
    Splitter1: TABcxSplitter;
    ABGrid1: TABcxGrid;
    ABTableView1: TABcxGridDBBandedTableView;
    ABLevel1: TcxGridLevel;
    DetailPanel1: TPanel;
    ABDBPanel1: TABDBPanel;
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABPageControl1: TABcxPageControl;
    procedure FormCreate(Sender: TObject);
    procedure ABPageControl1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABMainDetailTableTemplateForm: TABMainDetailTableTemplateForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABMainDetailTableTemplateForm.ClassName;
end;

exports
   ABRegister ;

procedure TABMainDetailTableTemplateForm.ABPageControl1Change(
  Sender: TObject);
begin
  ABPageControlChange(self,0,ABPageControl1.ActivePageIndex);
end;

procedure TABMainDetailTableTemplateForm.FormCreate(Sender: TObject);
begin
  ABInitFormTemplate(self,ABPageControl1,ABPageControl1Change);
end;

Initialization
  RegisterClass(TABMainDetailTableTemplateForm);

Finalization
  UnRegisterClass(TABMainDetailTableTemplateForm);

end.
