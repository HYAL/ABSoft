unit ABSys_Org_FunctionU;
                                                              
interface
                              
uses
  ABPubManualProgressBarU,
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,
  ABPubRegisterFileU,
  ABPubMessageU,
  ABPubPakandZipU,
  ABPubFormU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFramkWorkcxGridU,
  ABFramkWorkDBPanelU,
  ABFramkWorkDictionaryQueryU,
  ABFramkWorkDBNavigatorU,
  ABFramkWorkFuncFormU,
  ABFramkWorkControlU,
  ABFramkWorkQueryU,
  ABFramkWorkUpAndDownU,
  ABFramkWorkFuncU,

  cxTLdxBarBuiltInMenu,
  cxLookAndFeels,
  cxListBox,
  cxShellListView,
  cxListView,
  cxMCListBox,
  cxShellCommon,
  cxSplitter,
  cxImage,
  cxDBExtLookupComboBox,
  cxDBLookupEdit,
  cxLookupEdit,
  cxCheckBox,
  cxButtonEdit,
  cxDBEdit,
  cxMemo,
  cxTLData,
  cxDBTL,
  cxInplaceContainer,
  cxTL,
  cxGrid,
  cxGridCustomView,
  cxControls,
  cxClasses,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridLevel,
  cxPC,
  cxLabel,
  cxContainer,
  cxDropDownEdit,
  cxMaskEdit,
  cxTextEdit,
  cxButtons,
  cxLookAndFeelPainters,
  cxDBData,
  cxEdit,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  dxdbtree,
  dxtree,

  Windows,Forms,Variants,SysUtils,Graphics,Buttons,DB,Menus,DBClient,StdCtrls,
  Controls,ComCtrls,ExtCtrls,Classes,ShlObj,
  cxNavigator, dxBarBuiltInMenu, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, FileCtrl;

type
  TABSys_Org_FunctionForm = class(TABFuncForm)
    Panel2: TPanel;
    Panel5: TPanel;
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    Panel6: TPanel;
    Panel7: TPanel;
    ABDBcxGrid1: TABcxGrid;
    ABDBcxGrid1DBBandedTableView1: TABcxGridDBBandedTableView;
    ABDBcxGrid1Level1: TcxGridLevel;
    ABQuery2: TABQuery;
    ABDatasource2: TABDatasource;
    Splitter2: TABcxSplitter;
    Splitter1: TABcxSplitter;
    PopupMenu1: TPopupMenu;
    N2: TMenuItem;
    N3: TMenuItem;
    ABcxDBTreeView1: TABcxDBTreeView;
    ABcxDBTreeView1cxDBTreeListColumn1: TcxDBTreeListColumn;
    ABcxDBTreeView1cxDBTreeListColumn2: TcxDBTreeListColumn;
    N11: TMenuItem;
    N12: TMenuItem;
    Panel3: TPanel;
    Button5: TABcxButton;
    ABcxButton3: TABcxButton;
    ABcxPageControl2: TABcxPageControl;
    cxTabSheet1: TcxTabSheet;
    ABDBPanel2: TABDBPanel;
    Panel1: TPanel;
    Label1: TcxLabel;
    Label2: TcxLabel;
    cxLabel2: TcxLabel;
    ABcxComboBox2: TABcxComboBox;
    SpeedButton1: TABcxButton;
    ABcxComboBox1: TABcxComboBox;
    cxLabel1: TcxLabel;
    cxLabel3: TcxLabel;
    FileListBox1: TFileListBox;
    cxTabSheet2: TcxTabSheet;
    ABDBPanel3: TABDBPanel;
    cxTabSheet3: TcxTabSheet;
    ABDBPanel4: TABDBPanel;
    ABDBNavigator2: TABDBNavigator;
    N1: TMenuItem;
    N4: TMenuItem;
    cxTabSheet4: TcxTabSheet;
    Panel8: TPanel;
    ABcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    ABcxSplitter2: TABcxSplitter;
    Panel4: TPanel;
    ABDBPanel5: TABDBPanel;
    ABPageControl1: TABcxPageControl;
    cxTabSheet5: TcxTabSheet;
    ABcxGrid2: TABcxGrid;
    ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView;
    cxGridLevel2: TcxGridLevel;
    cxTabSheet6: TcxTabSheet;
    ABcxSplitter3: TABcxSplitter;
    ABcxGrid3: TABcxGrid;
    ABcxGridDBBandedTableView3: TABcxGridDBBandedTableView;
    cxGridLevel3: TcxGridLevel;
    Panel9: TPanel;
    ABDBPanel6: TABDBPanel;
    ABcxGrid4: TABcxGrid;
    ABcxGridDBBandedTableView4: TABcxGridDBBandedTableView;
    cxGridLevel4: TcxGridLevel;
    ABDBNavigator3: TABDBNavigator;
    ABDBNavigator4: TABDBNavigator;
    ABDBNavigator5: TABDBNavigator;
    ABDBNavigator6: TABDBNavigator;
    ABDatasource2_1: TABDatasource;
    ABQuery2_1: TABQuery;
    ABQuery2_1_1: TABQuery;
    ABDatasource2_1_1: TABDatasource;
    ABDatasource2_1_2: TABDatasource;
    ABDatasource2_1_2_1: TABDatasource;
    ABQuery2_1_2_1: TABQuery;
    ABQuery2_1_2: TABQuery;
    ABDBNavigator1: TABDBNavigator;
    procedure FormCreate(Sender: TObject);
    procedure ABcxComboBox1PropertiesChange(Sender: TObject);
    procedure ABQuery2AfterScroll(DataSet: TDataSet);
    procedure FileListBox1Change(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure ABDBcxGrid1Enter(Sender: TObject);
    procedure ABcxComboBox2PropertiesChange(Sender: TObject);
    procedure ABQuery2AfterInsert(DataSet: TDataSet);
    procedure FileListBox1DblClick(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ABcxButton3Click(Sender: TObject);
    procedure ABQuery2AfterPost(DataSet: TDataSet);
    procedure N1Click(Sender: TObject);
    procedure ABQuery2BeforePost(DataSet: TDataSet);
    { Private declarations }
  private                             
    FisLocateInFileListBox1,
    FisLocateInABQuery2,
    FMultiSelectUpdate:Boolean;

    procedure ViewVersionSize(aFileName:string);
    procedure ThanVersion(aFileName:string);
    function RegsterCurFile(aUpdate:boolean;aPkgName:string='';aShowMsg:boolean=false): boolean;
    { Private declarations }
  public
    { Public declarations }
  end;
  

var
  ABSys_Org_FunctionForm: TABSys_Org_FunctionForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Org_FunctionForm.ClassName;
end;

exports
   ABRegister ;


//显示当前文件的大小和版本号
procedure TABSys_Org_FunctionForm.ViewVersionSize(aFileName:string);
var
  tempLocalFileVersion,tempLocalFileSize:string;
begin
  tempLocalFileVersion:=ABRegisterFile.GetVersion(aFileName);
  if tempLocalFileVersion <> emptystr then
    Label2.Caption:=tempLocalFileVersion
  else
    Label2.Caption:=('未取到');

  tempLocalFileSize:=ABSpaceStr(FloatToStr(ABTrunc(ABGetFileSize(aFileName)/1024)))+'KB';
  if tempLocalFileSize<>emptystr then
    Label1.Caption:=tempLocalFileSize
  else
    Label1.Caption:= ('未取到');
end;

//比较当前文件和服务器的版本
procedure TABSys_Org_FunctionForm.ThanVersion(aFileName:string);
var
  tempMaxVersion,tempLocalFileVersion:string;
begin
  Label2.Style.Font.Color:=clWindowText;
  Label2.Style.Font.Style:=Label2.Style.Font.Style-[fsbold];
  Label2.Style.Font.Style:=Label2.Style.Font.Style-[fsStrikeOut];
  tempLocalFileVersion:=ABRegisterFile.GetVersion(aFileName);
  if tempLocalFileVersion=emptystr then
    exit;

  tempMaxVersion:=ABGetMaxVersion(ABQuery2.FindField('Fu_Version').AsString,tempLocalFileVersion);
  if AnsiCompareText(tempMaxVersion,tempLocalFileVersion)=0  then
  begin
    Label2.Style.Font.Color:=clRed;
    Label2.Style.Font.Style:=Label2.Style.Font.Style+[fsbold];
    Label2.Style.Font.Style:=Label2.Style.Font.Style-[fsStrikeOut];
  end
  else if AnsiCompareText(tempMaxVersion,ABQuery2.FindField('Fu_Version').AsString)=0  then
  begin
    Label2.Style.Font.Color:=clActiveCaption;
    Label2.Style.Font.Style:=Label2.Style.Font.Style-[fsbold];
    Label2.Style.Font.Style:=Label2.Style.Font.Style+[fsStrikeOut];
  end;
end;

procedure TABSys_Org_FunctionForm.FormCreate(Sender: TObject);
begin
  ABInitFormDataSet([],[],ABQuery1);
  ABInitFormDataSet([ABDBPanel2,ABDBPanel3,ABDBPanel4],[ABDBcxGrid1DBBandedTableView1],ABQuery2);

  ABInitFormDataSet([ABDBPanel5],[ABcxGridDBBandedTableView1],ABQuery2_1);
  ABInitFormDataSet([]          ,[ABcxGridDBBandedTableView2],ABQuery2_1_1);
  ABInitFormDataSet([ABDBPanel6],[ABcxGridDBBandedTableView3],ABQuery2_1_2);
  ABInitFormDataSet([]          ,[ABcxGridDBBandedTableView4],ABQuery2_1_2_1);

  if ABCheckDirExists(ABcxComboBox2.Text) then
  begin
    FileListBox1.Directory:=ABcxComboBox2.Text;
  end;
  FileListBox1.Mask:=ABcxComboBox1.Text;
end;

procedure TABSys_Org_FunctionForm.FormShow(Sender: TObject);
begin
  FMultiSelectUpdate:=true;
  FisLocateInFileListBox1:=true;
  FisLocateInABQuery2 :=true;

  if ABDBcxGrid1.CanFocus then
    ABDBcxGrid1.SetFocus;

  if FileListBox1.CanFocus then
    FileListBox1.SetFocus;

  if FileListBox1.Count>0 then
  begin
    FileListBox1.ItemIndex:=0;
    FileListBox1Change(FileListBox1);
  end;
end;

procedure TABSys_Org_FunctionForm.ABQuery2AfterInsert(
  DataSet: TDataSet);
begin
  DataSet.FindField('Fu_IsView').AsBoolean:=true;
  DataSet.FindField('FU_IsUpdate').AsBoolean:=true;
end;

procedure TABSys_Org_FunctionForm.ABQuery2AfterPost(DataSet: TDataSet);
var
  tempDataset:TDataSet;
  tempDatetime:TDatetime;
  i,j,tempCount1,tempCount2:LongInt;
  tempStrings: TStrings;
  procedure AddSQL(aQueryName,aConnName,aQuerySQL: string);
  begin
    if not (tempDataset.Locate('FS_Name',aQueryName,[])) then
    begin
      ABSetFieldValue(tempDataset,
                      ['FS_FU_Guid','FS_FormName','FS_Name'],
                      [ABQuery2.FindField('FU_Guid').AsString,
                       ABStrToName(ABQuery2.FindField('FU_FileName').AsString),
                       aQueryName],
                      ['FS_ConnName','FS_Sql','FS_UpdateDatetime'],
                      [ABGetConnGuidByConnName(aConnName),aQuerySQL,tempDatetime]
                        );
    end;
  end;
begin
  tempDatetime:=now;
  if ABQuery2.FindField('FU_TemplateName').AsString<>emptystr then
  begin
    tempDataset:=ABGetConstSqlPubDataset('ABSys_Org_FuncSQL',[ABStrToName(ABQuery2.FindField('FU_FileName').AsString)],TABInsideQuery);
    if (Assigned(tempDataset))    then
    begin
      tempStrings:= TStringList.Create;
      try
        tempStrings.Text:=ABQuery2.FindField('FU_TemplateReMark').AsString;
        if AnsiCompareText(ABQuery2.FindField('FU_TemplateName').AsString,'ABBaseTableTemplateG.bpl')=0 then
        begin
          AddSQL('ABQuery1',ABReadIniInStrings('Setup','ConnName',tempStrings),ABReadIniInStrings('Setup','SQL',tempStrings));
        end
        else if AnsiCompareText(ABQuery2.FindField('FU_TemplateName').AsString,'ABMainDetailTableTemplateG.bpl')=0 then
        begin
          AddSQL('ABQuery1'  ,ABReadIniInStrings('Setup','ConnName',tempStrings),ABReadIniInStrings('Setup','SQL',tempStrings));
          tempCount1:=StrToIntDef(ABReadIniInStrings('Setup','DetailCount',tempStrings),1);
          for I := 1 to tempCount1 do
          begin
            AddSQL('ABQuery1_'+inttostr(i),ABReadIniInStrings('Detail'+inttostr(i),'ConnName',tempStrings),ABReadIniInStrings('Detail'+inttostr(i),'SQL',tempStrings));
          end;
        end
        else if AnsiCompareText(ABQuery2.FindField('FU_TemplateName').AsString,'ABMultiMainDetailTableTemplateG.bpl')=0 then
        begin
          tempCount1:=StrToIntDef(ABReadIniInStrings('Setup','MainCount',tempStrings),1);
          for I := 1 to tempCount1 do
          begin
            AddSQL('ABQuery'+inttostr(i),ABReadIniInStrings('Main'+inttostr(i),'ConnName',tempStrings),ABReadIniInStrings('Main'+inttostr(i),'SQL',tempStrings));
            tempCount2:=StrToIntDef(ABReadIniInStrings('Main'+inttostr(i),'DetailCount',tempStrings),1);
            for j := 1 to tempCount2 do
            begin
              AddSQL('ABQuery'+inttostr(i)+'_'+inttostr(j),ABReadIniInStrings('Main'+inttostr(i)+'Detail'+inttostr(j),'ConnName',tempStrings),ABReadIniInStrings('Main'+inttostr(i)+'Detail'+inttostr(j),'SQL',tempStrings));
            end;
          end;
        end;
      finally
        tempStrings.Free;
      end;
    end;
  end;

  ABExecSQL('Main','  update ABSys_Org_Parameter set Pa_Value=isnull(Pa_Value,0)+1 where Pa_Name=''FuncUpdateCount''');
end;

procedure TABSys_Org_FunctionForm.ABQuery2AfterScroll(
  DataSet: TDataSet);
var
  I,k: Integer;
begin
  if not FisLocateInFileListBox1 then
    exit;

  if DataSet.FindField('Fu_FileName').AsString<>EmptyStr then
  begin
    FisLocateInFileListBox1:=false;
    FisLocateInABQuery2:=false;
    try
      i:=FileListBox1.Items.IndexOf(DataSet.FindField('Fu_FileName').AsString) ;
      if i<0 then
      begin
        for k := 0 to ABcxComboBox2.Properties.Items.Count - 1 do
        begin
          if ABCheckFileExists(ABGetpath(ABcxComboBox2.Properties.Items[k])+DataSet.FindField('Fu_FileName').AsString) then
          begin
            ABcxComboBox1.text:='*.*';
            ABcxComboBox2.ItemIndex:=k;
            FileListBox1.Mask:=ABcxComboBox1.text;
            FileListBox1.Directory:=ABcxComboBox2.text;
            i:=FileListBox1.Items.IndexOf(DataSet.FindField('Fu_FileName').AsString) ;
              Break;
          end;
        end;
      end;

      if (i>=0)  then
      begin
        FileListBox1.Selected[FileListBox1.ItemIndex]:=false;
        FileListBox1.ItemIndex:=i;
        FileListBox1.Selected[i]:=True;

        ViewVersionSize(ABGetpath(FileListBox1.Directory)+DataSet.FindField('Fu_FileName').AsString);
        ThanVersion(ABGetpath(FileListBox1.Directory)+DataSet.FindField('Fu_FileName').AsString);
      end;
    finally
      FisLocateInFileListBox1:=true;
      FisLocateInABQuery2:=true;
    end;
  end;
end;

procedure TABSys_Org_FunctionForm.ABQuery2BeforePost(DataSet: TDataSet);
begin
  if ABQuery2.FindField('FU_TemplateName').AsString<>emptystr then
  begin
    ABQuery2.FindField('Fu_FileName').AsString:=ABQuery2.FindField('FU_Code').AsString+'.bpl';
  end;
end;

procedure TABSys_Org_FunctionForm.ABcxButton3Click(Sender: TObject);
var
  I: Integer;
  tempStrings:TStrings;
  tempFileName,
  tempPath:string;
begin
  if FileListBox1.SelCount<2 then
    exit;

  tempPath:=ABGetpath(FileListBox1.Directory);
  tempFileName:= ABSaveFile(tempPath,ABGetLastDirName(tempPath),'PAK FILES|*.pak');
  if tempFileName<>EmptyStr  then
  begin
    tempStrings:=TStringList.Create;
    try
      for I := 0 to FileListBox1.Count - 1 do
      begin
        if FileListBox1.Selected[i] then
        begin
          tempStrings.Add(tempPath+FileListBox1.Items[i]);
        end;
      end;

      if ABDoPak(tempStrings,tempFileName) then
      begin
        if (ABCheckDirExists(ABGetFilepath(tempFileName))) and
           (ABQuery1.Locate('Ti_Code','Public',[])) then
        begin
          FileListBox1.Directory:=ABGetFilepath(tempFileName);
          ABcxComboBox1.text:='*.Pak';
          if FileListBox1.Items.IndexOf(ABGetFilename(tempFileName,false))>=0 then
          begin
            FileListBox1.ItemIndex:=FileListBox1.Items.IndexOf(ABGetFilename(tempFileName,false));
            FileListBox1DblClick(FileListBox1);
          end;
        end;
      end;
    finally
      tempStrings.Free;
    end;
  end;
end;

procedure TABSys_Org_FunctionForm.ABcxComboBox1PropertiesChange(
  Sender: TObject);
begin
  FileListBox1.Mask:=ABcxComboBox1.Text;
end;

procedure TABSys_Org_FunctionForm.ABcxComboBox2PropertiesChange(
  Sender: TObject);
begin
  if ABCheckDirExists(ABcxComboBox2.Text) then
  begin
    FileListBox1.Directory:=ABcxComboBox2.Text;
  end;
end;

procedure TABSys_Org_FunctionForm.ABDBcxGrid1Enter(Sender: TObject);
begin

end;

//定位FileListBox1时,定位数据集与比较版本号
procedure TABSys_Org_FunctionForm.FileListBox1Change(Sender: TObject);
var
  tempParentGuid:string;
  tempNotifyEvent:TDataSetNotifyEvent;
begin
  if not FisLocateInABQuery2 then
    exit;

  FisLocateInFileListBox1:=false;
  FisLocateInABQuery2:=false;
  try
    if FileListBox1.ItemIndex>=0 then
    begin
      tempNotifyEvent:=ABQuery2.AfterScroll;
      ABQuery2.AfterScroll:=nil;
      try
        tempParentGuid:=ABGetSQLValue('Main',
                        ' select Fu_Ti_Guid '+
                        ' from  ABSys_Org_Function'+
                        ' where Fu_FileName='+QuotedStr(FileListBox1.Items[FileListBox1.ItemIndex]),[],'');

        if (tempParentGuid<>EmptyStr) and
           (AnsiCompareText(ABQuery1.FindField('Ti_Guid').AsString,tempParentGuid)<>0) then
          ABQuery1.Locate('Ti_Guid',tempParentGuid,[]);

        ViewVersionSize(ABGetpath(FileListBox1.Directory)+FileListBox1.Items[FileListBox1.ItemIndex]);
        if (AnsiCompareText(ABQuery2.FindField('Fu_FileName').AsString,
                            FileListBox1.Items[FileListBox1.ItemIndex])<>0) then
        begin
          if ABQuery2.Locate('Fu_FileName',FileListBox1.Items[FileListBox1.ItemIndex],[]) then
            ThanVersion(ABGetpath(FileListBox1.Directory)+FileListBox1.Items[FileListBox1.ItemIndex]);
        end
        else
        begin
          ThanVersion(ABGetpath(FileListBox1.Directory)+FileListBox1.Items[FileListBox1.ItemIndex]);
        end;
      finally
        ABQuery2.AfterScroll:=tempNotifyEvent;
      end;
    end;
  finally
    FisLocateInFileListBox1:=true;
    FisLocateInABQuery2:=true;
  end;
end;

procedure TABSys_Org_FunctionForm.SpeedButton1Click(Sender: TObject);
var
  tempstr1:string;
begin
  tempstr1:= trim(ABSelectDirectory(ABcxComboBox2.Text));
  if tempstr1<>emptystr then
  begin
    ABcxComboBox2.Text:=tempstr1;
    if ABcxComboBox2.Properties.Items.IndexOf(tempstr1)<0 then
      ABcxComboBox2.Properties.Items.Add(tempstr1);
      
    FileListBox1.Directory:=tempstr1;
  end;
end;

//注册模块
procedure TABSys_Org_FunctionForm.N12Click(Sender: TObject);
var
  tempBoolean:boolean;
  I: Integer;
  tempDataset:TDataSet;
  tempBaseTi_Guid:String;
begin
  ABGetPubManualProgressBarForm.Title:='更新或注册选择文件中';
  ABGetPubManualProgressBarForm.MainMin:=0;
  ABGetPubManualProgressBarForm.MainPosition:=0;
  ABGetPubManualProgressBarForm.MainMax:=FileListBox1.Count;
  ABGetPubManualProgressBarForm.Run(true);
  FisLocateInFileListBox1:=False;
  FisLocateInABQuery2:=False;
  FMultiSelectUpdate:=false;
  tempDataset:=ABGetDataset('Main','select Fu_Ti_Guid,Fu_FileName from ABSys_Org_Function',[]);
  ABQuery2.DisableControls;
  tempBaseTi_Guid:=ABQuery1.FieldByName('Ti_Guid').AsString;
  try
    for I := 0 to FileListBox1.Count - 1 do
    begin
      if ABGetPubManualProgressBarForm.ExitRun then
        break;
      ABGetPubManualProgressBarForm.NextMainIndex;

      if FileListBox1.Selected[i] then
      begin
        if tempDataset.Locate('Fu_FileName',FileListBox1.Items[i],[]) then
        begin
          ABQuery1.Locate('Ti_Guid',tempDataset.FieldByName('Fu_Ti_Guid').AsString,[]);
          ABQuery2.Locate('Fu_FileName',FileListBox1.Items[i],[]);
          tempBoolean:=true;
        end
        else
        begin
          if tempBaseTi_Guid<>EmptyStr then
            ABQuery1.Locate('Ti_Guid',tempBaseTi_Guid,[]);

          tempBoolean:=false;
        end;

        RegsterCurFile(tempBoolean,FileListBox1.Items[i]);
      end;
    end;
    ABGetPubManualProgressBarForm.Stop;
    ABShow('更新或注册选择文件完成.')
  finally
    tempDataset.Free;
    ABQuery2.EnableControls;
    FMultiSelectUpdate:=true;
    FisLocateInFileListBox1:=true;
    FisLocateInABQuery2:=true;
    ABGetPubManualProgressBarForm.Stop;
  end;
end;

procedure TABSys_Org_FunctionForm.N1Click(Sender: TObject);
begin
  if not ABQuery2.IsEmpty then
  begin
    ABUpStreamToField('Main',
                    'ABSys_Org_Function','Fu_Pkg',
                    'Fu_Guid='+ABQuotedStr(ABQuery2.FindField('Fu_Guid').AsString),
                    nil);
    ABShow('清除完成.')
  end;
end;

//更新当前分类下程序包
procedure TABSys_Org_FunctionForm.N2Click(Sender: TObject);
begin
  ABcxComboBox1.Text:='*.*';

  ABGetPubManualProgressBarForm.Title:='更新当前分类菜单中';
  ABGetPubManualProgressBarForm.MainMin:=0;
  ABGetPubManualProgressBarForm.MainPosition:=0;
  ABGetPubManualProgressBarForm.MainMax:=ABQuery2.RecordCount;
  ABGetPubManualProgressBarForm.Run(true);
  ABQuery2.DisableControls;
  FMultiSelectUpdate:=false;
  try
    ABQuery2.First;
    while not ABQuery2.Eof do
    begin
      if ABGetPubManualProgressBarForm.ExitRun then
        break;
      ABGetPubManualProgressBarForm.NextMainIndex;

      RegsterCurFile(true);

      ABQuery2.Next;
    end;
    ABGetPubManualProgressBarForm.Stop;
    ABShow('更新当前分类菜单完成.')
  finally
    FMultiSelectUpdate:=true;
    ABQuery2.EnableControls;
    ABGetPubManualProgressBarForm.Stop;
  end;
end;

//更新所有程序包
procedure TABSys_Org_FunctionForm.N3Click(Sender: TObject);
begin
  ABcxComboBox1.Text:='*.*';

  ABGetPubManualProgressBarForm.Title:='更新所有分类菜单中';
  ABGetPubManualProgressBarForm.MainMin:=0;
  ABGetPubManualProgressBarForm.MainPosition:=0;
  ABGetPubManualProgressBarForm.MainMax:=ABQuery1.RecordCount;
  ABGetPubManualProgressBarForm.DetailVisible:=true;
  ABGetPubManualProgressBarForm.DetailMin:=0;
  ABGetPubManualProgressBarForm.DetailPosition:=0;
  ABGetPubManualProgressBarForm.Run(true);
  ABQuery2.DisableControls;
  FMultiSelectUpdate:=False;
  try
    ABQuery1.First;
    while not ABQuery1.Eof do
    begin
      ABGetPubManualProgressBarForm.NextMainIndex;
      ABGetPubManualProgressBarForm.DetailMax:=ABQuery2.RecordCount;
      ABGetPubManualProgressBarForm.DetailPosition:=0;
      ABQuery2.First;
      while not ABQuery2.Eof do
      begin
        if ABGetPubManualProgressBarForm.ExitRun then
          break;
        ABGetPubManualProgressBarForm.NextDetailIndex;

        RegsterCurFile(true);

        ABQuery2.Next;
      end;
      ABQuery1.Next;
    end;
    ABGetPubManualProgressBarForm.Stop;
    ABShow('更新所有分类菜单完成.')
  finally
    FMultiSelectUpdate:=true;
    ABQuery2.EnableControls;
    ABGetPubManualProgressBarForm.DetailVisible:=false;
    ABGetPubManualProgressBarForm.Stop;
  end;
end;

procedure TABSys_Org_FunctionForm.FileListBox1DblClick(Sender: TObject);
var
  tempBoolean:boolean;
begin
  if FileListBox1.ItemIndex>=0 then
  begin
    if (not ABDatasetIsEmpty(ABQuery2)) and
       (AnsiCompareText(ABQuery2.FindField('Fu_FileName').AsString,
                        FileListBox1.Items[FileListBox1.ItemIndex])=0) then
    begin
      tempBoolean:=true;
    end
    else
      tempBoolean:=false;

    RegsterCurFile(tempBoolean,'',true);
  end;
end;

function TABSys_Org_FunctionForm.RegsterCurFile(aUpdate:boolean;aPkgName:string;aShowMsg:boolean): boolean;
var
  tempFormClassName:string;
  tempLocalFile,tempVersion:string;
  tempNotifyEvent:TDataSetNotifyEvent;
  tempBpl: HModule;
  tempFormClass: TPersistentClass;
  tempForm: TForm;
begin
  result:=false;

  if aPkgName=EmptyStr then
    aPkgName:=FileListBox1.Items[FileListBox1.ItemIndex];
    
  tempLocalFile:=ABGetpath(FileListBox1.Directory)+aPkgName;
  tempVersion:=ABRegisterFile.GetVersion(tempLocalFile);

  tempNotifyEvent:=ABQuery2.AfterScroll;
  ABQuery2.AfterScroll:=nil;
  try
    //如果是更新 菜单的程序包
    if (aUpdate) then
    begin               
      if AnsiCompareText(ABQuery2.FindField('Fu_FileName').AsString,aPkgName)=0  then
      begin
        if  (ABRegisterFile.IsUp(tempLocalFile,ABQuery2.FindField('Fu_Version').AsString,true)) then
        begin
          ABQuery2.Edit;
          if (ABPos('.',tempVersion)<=0) and
             (ABPos('.',ABQuery2.FindField('Fu_Version').AsString)<=0) then
          begin
            ABQuery2.FindField('Fu_Version').AsInteger:=StrToIntDef(ABQuery2.FindField('Fu_Version').AsString,0)+1;
            ABWriteInI('Version',aPkgName,ABQuery2.FindField('Fu_Version').AsString,ABLocalVersionsFile);
          end
          else
          begin
            ABQuery2.FindField('Fu_Version').AsString:=ABIIF(trim(tempVersion)=EmptyStr,'0',tempVersion);
          end;
          ABQuery2.Post;

          ABUpFileToField('Main',
                          'ABSys_Org_Function','Fu_Pkg',
                          'Fu_Guid='+ABQuotedStr(ABQuery2.FindField('Fu_Guid').AsString),
                          tempLocalFile);
          result:=true;
        end
        else if aShowMsg then
        begin
          ABShow('本地版本号[%s]太低,不能更新,请检查.',[tempVersion]);
        end;
      end;
    end
    //如果是注册 菜单的程序包
    else
    begin
      if AnsiCompareText(ABQuery2.FindField('Fu_FileName').AsString,aPkgName)<>0  then
      begin
        //增加新注册的程序包
        if (not ABDatasetIsEmpty(ABQuery2)) and
           (ABQuery2.FindField('Fu_FileName').AsString=EmptyStr)  then
        begin
          //注册到当前菜单
          ABQuery2.Edit;
        end
        else
        begin
          //注册到新菜单
          ABQuery2.Append;
          ABQuery2.FindField('Fu_Code').AsString:=aPkgName;
          ABQuery2.FindField('Fu_Name').AsString:=aPkgName;
          ABQuery2.FindField('Fu_IsView').AsBoolean:=not (AnsiCompareText(ABQuery1.FindField('Ti_Code').AsString,'Public')=0);
          ABQuery2.FindField('Fu_Public').AsBoolean:=(AnsiCompareText(ABQuery1.FindField('Ti_Code').AsString,'Public')=0);
          if (AnsiCompareText(ExtractFileExt(aPkgName), '.bpl') = 0) then
          begin
            tempForm := ABRegisterFile.GetFormByFileName(aPkgName);
            if (Assigned(tempForm)) then
            begin
              ABQuery2.FindField('Fu_Name').AsString:=tempForm.Caption;
            end
            else
            begin
              tempBpl := loadpackage(tempLocalFile);
              try
                if tempBpl <> null then
                begin
                  @ABRegisterPro := GetProcAddress(tempBpl, 'ABRegister');
                  if Assigned(@ABRegisterPro) then
                  begin
                    ABRegisterPro(tempFormClassName);
                    tempFormClass := GetClass(tempFormClassName);
                    if Assigned(tempFormClass) then
                    begin
                      ABCloseFormEvent:=true;
                      try
                        tempForm := TFormClass(tempFormClass).create(Application);
                        try
                          ABQuery2.FindField('Fu_Name').AsString:=tempForm.Caption;
                        finally
                          tempForm.Free;
                        end;
                      finally
                        ABCloseFormEvent:=False;
                      end;
                    end;
                  end;
                end;
              finally
                UnRegisterModuleClasses(tempBpl);
                UnLoadPackage(tempBpl);
              end;
            end;
          end;
        end;
        ABQuery2.FindField('Fu_FileName').AsString:=aPkgName;
        ABQuery2.FindField('Fu_Version').AsString:=ABIIF(trim(tempVersion)=EmptyStr,'0',tempVersion);
        ABQuery2.Post;

        ABUpFileToField('Main',
                      'ABSys_Org_Function','Fu_Pkg',
                      'Fu_Guid='+ABQuotedStr(ABQuery2.FindField('Fu_Guid').AsString),
                      tempLocalFile);

        result:=true;
      end;
    end;
    ThanVersion(tempLocalFile);
  finally
    ABQuery2.AfterScroll:=tempNotifyEvent;
  end;
end;

Initialization
  RegisterClass(TABSys_Org_FunctionForm);

Finalization
  UnRegisterClass(TABSys_Org_FunctionForm);


end.



