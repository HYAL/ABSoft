unit ABMultiMainDetailTableTemplateU;

interface

uses
  ABPubFuncU,
  ABPubPanelU,
  ABPubDBU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,

  ABFramkWorkQuerySelectFieldPanelU,
  ABFramkWorkcxGridU,
  ABFramkWorkDictionaryQueryU,
  ABFramkWorkDBNavigatorU,
  ABFramkWorkFuncFormU,
  ABFramkWorkControlU,
  ABFramkWorkQueryU,
  ABFramkWorkFuncU,
  ABFramkWorkOperatorDatasetU,

  cxDateUtils,
  cxLookAndFeels,
  cxTextEdit,
  cxContainer,
  cxLabel,
  cxCalendar,
  cxDropDownEdit,
  cxMaskEdit,
  cxButtons,
  cxLookAndFeelPainters,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGrid,
  cxGridDBTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxClasses,
  cxGridLevel,
  cxDBData,
  cxEdit,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  cxDBNavigator,
  cxNavigator,
  cxControls,
  cxPC,
  dxCore,
  dxStatusBar,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,DB,
  DBClient,ExtCtrls,ComCtrls,StdCtrls,DBCtrls,Menus,
  ABFramkWorkQueryAllFieldComboxU, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, dxBarBuiltInMenu ;

type
  TABMultiMainDetailTableTemplateForm = class(TABFuncForm)
    ParentPageControl: TABcxPageControl;
    procedure FormCreate(Sender: TObject);
    procedure ParentPageControlChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABMultiMainDetailTableTemplateForm: TABMultiMainDetailTableTemplateForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABMultiMainDetailTableTemplateForm.ClassName;
end;

exports
   ABRegister ;

procedure TABMultiMainDetailTableTemplateForm.ParentPageControlChange(
  Sender: TObject);
var
  tempPageControl: TABcxPageControl;
begin
  tempPageControl:=TABcxPageControl(FindComponent('ABPageControl'+inttostr(ParentPageControl.ActivePageIndex+1)));
  ABPageControlChange(self,ParentPageControl.ActivePageIndex,ABIIF(Assigned(tempPageControl),tempPageControl.ActivePageIndex,0));
end;

procedure TABMultiMainDetailTableTemplateForm.FormCreate(Sender: TObject);
begin
  ABInitFormTemplate(self,ParentPageControl,ParentPageControlChange);
end;

Initialization
  RegisterClass(TABMultiMainDetailTableTemplateForm);

Finalization
  UnRegisterClass(TABMultiMainDetailTableTemplateForm);

end.


