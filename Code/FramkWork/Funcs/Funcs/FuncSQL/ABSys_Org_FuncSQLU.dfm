object ABSys_Org_FuncSQLForm: TABSys_Org_FuncSQLForm
  Left = 173
  Top = 129
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = #21152#36733'SQL'#31649#29702
  ClientHeight = 550
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxPageControl1: TABcxPageControl
    Left = 174
    Top = 27
    Width = 576
    Height = 523
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = cxTabSheet1
    Properties.CustomButtons.Buttons = <>
    Properties.ShowFrame = True
    LookAndFeel.Kind = lfFlat
    ActivePageIndex = 0
    ClientRectBottom = 522
    ClientRectLeft = 1
    ClientRectRight = 575
    ClientRectTop = 21
    object cxTabSheet1: TcxTabSheet
      Caption = 'SQL'#21015#34920
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object ABDBPanel1: TABDBPanel
        Left = 0
        Top = 0
        Width = 574
        Height = 501
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        ShowCaption = False
        TabOrder = 0
        ReadOnly = False
        DataSource = ABDatasource3
        AddAnchors_akRight = True
        AddAnchors_akBottom = True
        AutoHeight = False
        AutoWidth = True
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'SQL'#25805#20316
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object ABDBcxGrid2: TABcxGrid
        Left = 0
        Top = 165
        Width = 574
        Height = 194
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
        object ABDBcxGrid2ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
          PopupMenu.AutoHotkeys = maManual
          PopupMenu.CloseFootStr = False
          PopupMenu.LinkTableView = ABDBcxGrid2ABcxGridDBBandedTableView1
          PopupMenu.AutoApplyBestFit = True
          PopupMenu.AutoCreateAllItem = True
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ABDatasource_Do
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.AutoDataSetFilter = True
          DataController.Filter.TranslateBetween = True
          DataController.Filter.TranslateIn = True
          DataController.Filter.TranslateLike = True
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.AlwaysShowEditor = True
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.DataRowSizing = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.MultiSelect = True
          OptionsView.CellAutoHeight = True
          OptionsView.GroupByBox = False
          OptionsView.BandHeaders = False
          Bands = <
            item
            end>
          ExtPopupMenu.AutoHotkeys = maManual
          ExtPopupMenu.CloseFootStr = False
          ExtPopupMenu.LinkTableView = ABDBcxGrid2ABcxGridDBBandedTableView1
          ExtPopupMenu.AutoApplyBestFit = True
          ExtPopupMenu.AutoCreateAllItem = True
        end
        object ABDBcxGrid2Level1: TcxGridLevel
          GridView = ABDBcxGrid2ABcxGridDBBandedTableView1
        end
      end
      object ABcxSplitter1: TABcxSplitter
        Left = 0
        Top = 359
        Width = 574
        Height = 8
        HotZoneClassName = 'TcxMediaPlayer8Style'
        AlignSplitter = salBottom
        InvertDirection = True
        Control = ABcxMemo2
      end
      object ABcxMemo2: TABcxMemo
        Left = 0
        Top = 367
        Align = alBottom
        Properties.ScrollBars = ssVertical
        TabOrder = 2
        Height = 134
        Width = 574
      end
      object ABcxMemo1: TABcxMemo
        Left = 0
        Top = 0
        Align = alTop
        Properties.ScrollBars = ssVertical
        TabOrder = 3
        Height = 128
        Width = 574
      end
      object Panel3: TPanel
        Left = 0
        Top = 128
        Width = 574
        Height = 37
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 4
        object Label2: TLabel
          Left = 302
          Top = -2
          Width = 355
          Height = 13
          AutoSize = False
          Transparent = True
          Visible = False
        end
        object ABcxButton1: TABcxButton
          Left = 12
          Top = 6
          Width = 85
          Height = 25
          Caption = #26597#35810
          LookAndFeel.Kind = lfFlat
          LookAndFeel.NativeStyle = False
          TabOrder = 0
          OnClick = ABcxButton1Click
          ShowProgressBar = False
        end
        object ABcxButton2: TABcxButton
          Left = 103
          Top = 6
          Width = 98
          Height = 25
          Caption = #25191#34892
          LookAndFeel.Kind = lfFlat
          LookAndFeel.NativeStyle = False
          TabOrder = 1
          OnClick = ABcxButton2Click
          ShowProgressBar = False
        end
      end
    end
  end
  object ABDBNavigator1: TABDBNavigator
    Left = 0
    Top = 0
    Width = 750
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    ShowCaption = False
    TabOrder = 0
    BigGlyph = False
    ImageLayout = blGlyphLeft
    DataSource = ABDatasource1
    VisibleButtons = [nbQuery, nbReport, nbExitSpacer, nbExit]
    ButtonRangeType = RtCustom
    BtnCustom1ImageIndex = -1
    BtnCustom2ImageIndex = -1
    BtnCustom3ImageIndex = -1
    BtnCustom4ImageIndex = -1
    BtnCustom5ImageIndex = -1
    BtnCustom6ImageIndex = -1
    BtnCustom7ImageIndex = -1
    BtnCustom8ImageIndex = -1
    BtnCustom9ImageIndex = -1
    BtnCustom10ImageIndex = -1
    BtnCustom1Caption = #33258#23450#20041'1'
    BtnCustom2Caption = #33258#23450#20041'2'
    BtnCustom3Caption = #33258#23450#20041'3'
    BtnCustom4Caption = #33258#23450#20041'4'
    BtnCustom5Caption = #33258#23450#20041'5'
    BtnCustom6Caption = #33258#23450#20041'6'
    BtnCustom7Caption = #33258#23450#20041'7'
    BtnCustom8Caption = #33258#23450#20041'8'
    BtnCustom9Caption = #33258#23450#20041'9'
    BtnCustom10Caption = #33258#23450#20041'10'
    BtnCustom1Kind = cxbkStandard
    BtnCustom2Kind = cxbkStandard
    BtnCustom3Kind = cxbkStandard
    BtnCustom4Kind = cxbkStandard
    BtnCustom5Kind = cxbkStandard
    BtnCustom6Kind = cxbkStandard
    BtnCustom7Kind = cxbkStandard
    BtnCustom8Kind = cxbkStandard
    BtnCustom9Kind = cxbkStandard
    BtnCustom10Kind = cxbkStandard
    ApprovedRollbackButton = nbNull
    ApprovedCommitButton = nbNull
    ButtonControlType = ctSingle
  end
  object Panel5: TPanel
    Left = 0
    Top = 27
    Width = 166
    Height = 523
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 2
    object ABcxSplitter3: TABcxSplitter
      Left = 0
      Top = 153
      Width = 166
      Height = 8
      Cursor = crVSplit
      HotZoneClassName = 'TcxMediaPlayer8Style'
      AlignSplitter = salTop
      InvertDirection = True
      Control = ABcxDBTreeView1
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 166
      Height = 24
      Align = alTop
      BevelOuter = bvNone
      Caption = #33756#21333#20998#31867
      TabOrder = 0
    end
    object Panel7: TPanel
      Left = 0
      Top = 161
      Width = 166
      Height = 24
      Align = alTop
      BevelOuter = bvNone
      Caption = #33756#21333#21015#34920
      TabOrder = 2
    end
    object ABcxGrid1: TABcxGrid
      Left = 0
      Top = 185
      Width = 166
      Height = 122
      Align = alTop
      TabOrder = 3
      LookAndFeel.NativeStyle = False
      object ABDBcxGrid1DBBandedTableView1: TABcxGridDBBandedTableView
        PopupMenu.AutoHotkeys = maManual
        PopupMenu.CloseFootStr = False
        PopupMenu.LinkTableView = ABDBcxGrid1DBBandedTableView1
        PopupMenu.AutoApplyBestFit = True
        PopupMenu.AutoCreateAllItem = True
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = ABDatasource2
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Filter.AutoDataSetFilter = True
        DataController.Filter.TranslateBetween = True
        DataController.Filter.TranslateIn = True
        DataController.Filter.TranslateLike = True
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.AlwaysShowEditor = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.DataRowSizing = True
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.MultiSelect = True
        OptionsView.CellAutoHeight = True
        OptionsView.ExpandButtonsForEmptyDetails = False
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        OptionsView.IndicatorWidth = 26
        OptionsView.BandHeaders = False
        Bands = <
          item
          end>
        ExtPopupMenu.AutoHotkeys = maManual
        ExtPopupMenu.CloseFootStr = False
        ExtPopupMenu.LinkTableView = ABDBcxGrid1DBBandedTableView1
        ExtPopupMenu.AutoApplyBestFit = True
        ExtPopupMenu.AutoCreateAllItem = True
      end
      object ABDBcxGrid1Level1: TcxGridLevel
        GridView = ABDBcxGrid1DBBandedTableView1
      end
    end
    object ABDBcxGrid1: TABcxGrid
      Left = 0
      Top = 339
      Width = 166
      Height = 152
      Align = alClient
      TabOrder = 4
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
      object ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
        PopupMenu.AutoHotkeys = maManual
        PopupMenu.CloseFootStr = False
        PopupMenu.LinkTableView = ABcxGridDBBandedTableView1
        PopupMenu.AutoApplyBestFit = True
        PopupMenu.AutoCreateAllItem = True
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = ABDatasource3
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Filter.AutoDataSetFilter = True
        DataController.Filter.TranslateBetween = True
        DataController.Filter.TranslateIn = True
        DataController.Filter.TranslateLike = True
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.AlwaysShowEditor = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.DataRowSizing = True
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.MultiSelect = True
        OptionsView.CellAutoHeight = True
        OptionsView.GroupByBox = False
        OptionsView.BandHeaders = False
        Bands = <
          item
          end>
        ExtPopupMenu.AutoHotkeys = maManual
        ExtPopupMenu.CloseFootStr = False
        ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView1
        ExtPopupMenu.AutoApplyBestFit = True
        ExtPopupMenu.AutoCreateAllItem = True
      end
      object cxGridLevel1: TcxGridLevel
        GridView = ABcxGridDBBandedTableView1
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 315
      Width = 166
      Height = 24
      Align = alTop
      BevelOuter = bvNone
      Caption = 'SQL'#21015#34920
      TabOrder = 5
    end
    object ABcxDBTreeView1: TABcxDBTreeView
      Left = 0
      Top = 24
      Width = 166
      Height = 129
      Align = alTop
      Bands = <
        item
        end>
      DataController.DataSource = ABDatasource1
      DataController.ParentField = 'Ti_ParentGuid'
      DataController.KeyField = 'Ti_Guid'
      DragMode = dmAutomatic
      Navigator.Buttons.CustomButtons = <>
      OptionsBehavior.ExpandOnIncSearch = True
      OptionsBehavior.IncSearch = True
      OptionsBehavior.IncSearchItem = ABcxDBTreeView1cxDBTreeListColumn1
      OptionsData.Editing = False
      OptionsData.Deleting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRect = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.Headers = False
      PopupMenu.DefaultParentValue = '0'
      RootValue = -1
      TabOrder = 6
      Active = True
      ExtFullExpand = False
      CanSelectParent = True
      object ABcxDBTreeView1cxDBTreeListColumn1: TcxDBTreeListColumn
        DataBinding.FieldName = 'Ti_Name'
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
      object ABcxDBTreeView1cxDBTreeListColumn2: TcxDBTreeListColumn
        Visible = False
        DataBinding.FieldName = 'Ti_Order'
        Position.ColIndex = 1
        Position.RowIndex = 0
        Position.BandIndex = 0
        SortOrder = soAscending
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
    object ABcxSplitter2: TABcxSplitter
      Left = 0
      Top = 307
      Width = 166
      Height = 8
      Cursor = crVSplit
      HotZoneClassName = 'TcxMediaPlayer8Style'
      AlignSplitter = salTop
      InvertDirection = True
      Control = ABcxGrid1
    end
    object ABDBNavigator2: TABDBNavigator
      Left = 0
      Top = 491
      Width = 166
      Height = 32
      Align = alBottom
      BevelOuter = bvNone
      ShowCaption = False
      TabOrder = 8
      BigGlyph = False
      ImageLayout = blGlyphLeft
      DataSource = ABDatasource3
      VisibleButtons = [nbInsert, nbCopy, nbDelete]
      ButtonRangeType = RtCustom
      BtnCustom1ImageIndex = -1
      BtnCustom2ImageIndex = -1
      BtnCustom3ImageIndex = -1
      BtnCustom4ImageIndex = -1
      BtnCustom5ImageIndex = -1
      BtnCustom6ImageIndex = -1
      BtnCustom7ImageIndex = -1
      BtnCustom8ImageIndex = -1
      BtnCustom9ImageIndex = -1
      BtnCustom10ImageIndex = -1
      BtnCustom1Caption = #33258#23450#20041'1'
      BtnCustom2Caption = #33258#23450#20041'2'
      BtnCustom3Caption = #33258#23450#20041'3'
      BtnCustom4Caption = #33258#23450#20041'4'
      BtnCustom5Caption = #33258#23450#20041'5'
      BtnCustom6Caption = #33258#23450#20041'6'
      BtnCustom7Caption = #33258#23450#20041'7'
      BtnCustom8Caption = #33258#23450#20041'8'
      BtnCustom9Caption = #33258#23450#20041'9'
      BtnCustom10Caption = #33258#23450#20041'10'
      BtnCustom1Kind = cxbkStandard
      BtnCustom2Kind = cxbkStandard
      BtnCustom3Kind = cxbkStandard
      BtnCustom4Kind = cxbkStandard
      BtnCustom5Kind = cxbkStandard
      BtnCustom6Kind = cxbkStandard
      BtnCustom7Kind = cxbkStandard
      BtnCustom8Kind = cxbkStandard
      BtnCustom9Kind = cxbkStandard
      BtnCustom10Kind = cxbkStandard
      ApprovedRollbackButton = nbNull
      ApprovedCommitButton = nbNull
      ButtonControlType = ctSingle
    end
  end
  object Splitter1: TABcxSplitter
    Left = 166
    Top = 27
    Width = 8
    Height = 523
    HotZoneClassName = 'TcxMediaPlayer8Style'
    InvertDirection = True
    Control = Panel5
  end
  object ABDatasource3: TABDatasource
    DataSet = ABQuery3
    Left = 291
    Top = 408
  end
  object ABQuery3: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource2
    MasterFields = 'FU_Guid'
    DetailFields = 'FS_FU_Guid'
    SQL.Strings = (
      'select * '
      'from ABSys_Org_FuncSQL'
      'where FS_FU_Guid=:FU_Guid')
    SqlUpdateDatetime = 42099.593210289350000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'Main'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_FuncSQL')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_FuncSQL')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 199
    Top = 410
    ParamData = <
      item
        Name = 'FU_GUID'
        ParamType = ptInput
      end>
  end
  object ABQuery_Do: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    IndexListDefs = <>
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 471
    Top = 435
  end
  object ABDatasource_Do: TABDatasource
    AutoEdit = False
    DataSet = ABQuery_Do
    Left = 562
    Top = 439
  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select * '
      'from ABSys_Org_TreeItem '
      'where Ti_Tr_Guid=dbo.Func_GetTreeGuid('#39'Function Dir'#39') '
      'order by TI_TR_Guid,TI_Group,TI_Order')
    SqlUpdateDatetime = 42099.600687326390000000
    BeforeDeleteAsk = True
    FieldDefaultValues = 'Ti_Tr_Guid=select dbo.Func_GetTreeGuid('#39'Function Dir'#39')'
    MaxRecordCount = 0
    ConnName = 'Main'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_TreeItem')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_TreeItem')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 199
    Top = 194
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 283
    Top = 195
  end
  object ABDatasource2: TABDatasource
    DataSet = ABQuery2
    Left = 288
    Top = 341
  end
  object ABQuery2: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    MasterFields = 'Ti_Guid'
    DetailFields = 'Fu_Ti_Guid'
    SQL.Strings = (
      'select                                   '
      ' FU_TI_Guid,                            '
      ' FU_Guid,                              '
      ' FU_Code,                               '
      ' FU_Name,                           '
      ' FU_FileName                      '
      'from ABSys_Org_Function '
      'where Fu_IsView=1 and  Fu_Ti_Guid  =:Ti_Guid            '
      'order by FU_TI_Guid,FU_Order  '
      '')
    SqlUpdateDatetime = 42127.392697152770000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'Main'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Function')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_Function')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 201
    Top = 335
    ParamData = <
      item
        Name = 'Ti_Guid'
        ParamType = ptInput
      end>
  end
end
