object ABSys_Org_ExtendReportPrintForm: TABSys_Org_ExtendReportPrintForm
  Left = 211
  Top = 89
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = #25193#23637#25253#34920#25171#21360
  ClientHeight = 550
  ClientWidth = 900
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 513
    Width = 900
    Height = 37
    Align = alBottom
    TabOrder = 0
    object ABcxButton2: TABcxButton
      Left = 106
      Top = 6
      Width = 80
      Height = 25
      Caption = #25171#21360
      LookAndFeel.Kind = lfFlat
      TabOrder = 0
      OnClick = ABcxButton2Click
      ShowProgressBar = False
    end
    object ABcxButton1: TABcxButton
      Left = 7
      Top = 6
      Width = 95
      Height = 25
      Caption = #26597#35810'/'#39044#35272
      Default = True
      DropDownMenu = PopupMenu1
      Kind = cxbkDropDownButton
      LookAndFeel.Kind = lfFlat
      TabOrder = 1
      OnClick = ABcxButton1Click
      ShowProgressBar = False
    end
    object ABcxButton120: TABcxButton
      Left = 192
      Top = 6
      Width = 105
      Height = 25
      Caption = #25253#34920#26174#31034#26041#24335
      Default = True
      DropDownMenu = PopupMenu2
      Kind = cxbkDropDownButton
      LookAndFeel.Kind = lfFlat
      TabOrder = 2
      ShowProgressBar = False
    end
    object ABcxButton29: TABcxButton
      Left = 303
      Top = 6
      Width = 100
      Height = 25
      Caption = #27979#35797#25152#26377#25253#34920
      LookAndFeel.Kind = lfFlat
      TabOrder = 3
      OnClick = ABcxButton29Click
      ShowProgressBar = False
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 900
    Height = 513
    Align = alClient
    TabOrder = 1
    object ABcxPageControl1: TABcxPageControl
      Left = 412
      Top = 1
      Width = 487
      Height = 511
      Align = alClient
      TabOrder = 0
      Properties.ActivePage = cxTabSheet1
      Properties.CustomButtons.Buttons = <>
      Properties.HideTabs = True
      LookAndFeel.Kind = lfFlat
      ActivePageIndex = 0
      ClientRectBottom = 510
      ClientRectLeft = 1
      ClientRectRight = 486
      ClientRectTop = 1
      object cxTabSheet1: TcxTabSheet
        Caption = 'cxTabSheet1'
        ImageIndex = 0
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 485
          Height = 509
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object frxPreview1: TfrxPreview
            Left = 0
            Top = 26
            Width = 485
            Height = 483
            Align = alClient
            OutlineVisible = False
            OutlineWidth = 120
            ThumbnailVisible = False
            OnPageChanged = frxPreview1PageChanged
            UseReportHints = True
          end
          object dxBarDockControl1: TdxBarDockControl
            Left = 0
            Top = 0
            Width = 485
            Height = 26
            Align = dalTop
            BarManager = dxBarManager1
          end
        end
      end
      object cxTabSheet2: TcxTabSheet
        Caption = 'cxTabSheet2'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object ABcxPageControl2: TABcxPageControl
          Left = 0
          Top = 0
          Width = 485
          Height = 509
          Align = alClient
          TabOrder = 0
          Properties.ActivePage = cxTabSheet3
          Properties.CustomButtons.Buttons = <>
          LookAndFeel.Kind = lfFlat
          ActivePageIndex = 0
          ClientRectBottom = 508
          ClientRectLeft = 1
          ClientRectRight = 484
          ClientRectTop = 21
          object cxTabSheet3: TcxTabSheet
            Caption = #34920#26684#21015#34920
            ImageIndex = 0
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object ABcxPageControl3: TABcxPageControl
              Left = 0
              Top = 0
              Width = 483
              Height = 487
              Align = alClient
              TabOrder = 0
              Properties.ActivePage = cxTabSheet5
              Properties.CustomButtons.Buttons = <>
              Properties.HideTabs = True
              LookAndFeel.Kind = lfFlat
              ActivePageIndex = 0
              ClientRectBottom = 486
              ClientRectLeft = 1
              ClientRectRight = 482
              ClientRectTop = 1
              object cxTabSheet5: TcxTabSheet
                Caption = #31532#19968#25968#25454#38598
                ImageIndex = 0
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object ABDBcxGrid1: TABcxGrid
                  Left = 0
                  Top = 0
                  Width = 481
                  Height = 485
                  Align = alClient
                  TabOrder = 0
                  LookAndFeel.NativeStyle = False
                  object ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
                    PopupMenu.AutoHotkeys = maManual
                    PopupMenu.CloseFootStr = False
                    PopupMenu.LinkTableView = ABcxGridDBBandedTableView1
                    PopupMenu.AutoApplyBestFit = True
                    PopupMenu.AutoCreateAllItem = True
                    Navigator.Buttons.CustomButtons = <>
                    DataController.Filter.Options = [fcoCaseInsensitive]
                    DataController.Filter.AutoDataSetFilter = True
                    DataController.Filter.TranslateBetween = True
                    DataController.Filter.TranslateIn = True
                    DataController.Filter.TranslateLike = True
                    DataController.Summary.DefaultGroupSummaryItems = <>
                    DataController.Summary.FooterSummaryItems = <>
                    DataController.Summary.SummaryGroups = <>
                    OptionsBehavior.AlwaysShowEditor = True
                    OptionsBehavior.FocusCellOnTab = True
                    OptionsBehavior.GoToNextCellOnEnter = True
                    OptionsCustomize.ColumnsQuickCustomization = True
                    OptionsCustomize.DataRowSizing = True
                    OptionsData.Deleting = False
                    OptionsData.Inserting = False
                    OptionsSelection.HideFocusRectOnExit = False
                    OptionsSelection.MultiSelect = True
                    OptionsView.CellAutoHeight = True
                    OptionsView.GroupByBox = False
                    OptionsView.BandHeaders = False
                    Bands = <
                      item
                      end>
                    ExtPopupMenu.AutoHotkeys = maManual
                    ExtPopupMenu.CloseFootStr = False
                    ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView1
                    ExtPopupMenu.AutoApplyBestFit = True
                    ExtPopupMenu.AutoCreateAllItem = True
                  end
                  object cxGridLevel1: TcxGridLevel
                    GridView = ABcxGridDBBandedTableView1
                  end
                end
              end
              object cxTabSheet6: TcxTabSheet
                Caption = #31532#20108#25968#25454#38598
                ImageIndex = 1
                TabVisible = False
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object ABDBcxGrid2: TABcxGrid
                  Left = 0
                  Top = 0
                  Width = 481
                  Height = 485
                  Align = alClient
                  TabOrder = 0
                  LookAndFeel.NativeStyle = False
                  object ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView
                    PopupMenu.AutoHotkeys = maManual
                    PopupMenu.CloseFootStr = False
                    PopupMenu.LinkTableView = ABcxGridDBBandedTableView2
                    PopupMenu.AutoApplyBestFit = True
                    PopupMenu.AutoCreateAllItem = True
                    Navigator.Buttons.CustomButtons = <>
                    DataController.Filter.Options = [fcoCaseInsensitive]
                    DataController.Filter.AutoDataSetFilter = True
                    DataController.Filter.TranslateBetween = True
                    DataController.Filter.TranslateIn = True
                    DataController.Filter.TranslateLike = True
                    DataController.Summary.DefaultGroupSummaryItems = <>
                    DataController.Summary.FooterSummaryItems = <>
                    DataController.Summary.SummaryGroups = <>
                    OptionsBehavior.AlwaysShowEditor = True
                    OptionsBehavior.FocusCellOnTab = True
                    OptionsBehavior.GoToNextCellOnEnter = True
                    OptionsCustomize.ColumnsQuickCustomization = True
                    OptionsCustomize.DataRowSizing = True
                    OptionsData.Deleting = False
                    OptionsData.Inserting = False
                    OptionsSelection.HideFocusRectOnExit = False
                    OptionsSelection.MultiSelect = True
                    OptionsView.CellAutoHeight = True
                    OptionsView.GroupByBox = False
                    OptionsView.BandHeaders = False
                    Bands = <
                      item
                      end>
                    ExtPopupMenu.AutoHotkeys = maManual
                    ExtPopupMenu.CloseFootStr = False
                    ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView2
                    ExtPopupMenu.AutoApplyBestFit = True
                    ExtPopupMenu.AutoCreateAllItem = True
                  end
                  object cxGridLevel2: TcxGridLevel
                    GridView = ABcxGridDBBandedTableView2
                  end
                end
              end
              object cxTabSheet7: TcxTabSheet
                Caption = #31532#19977#25968#25454#38598
                ImageIndex = 2
                TabVisible = False
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object ABDBcxGrid3: TABcxGrid
                  Left = 0
                  Top = 0
                  Width = 481
                  Height = 485
                  Align = alClient
                  TabOrder = 0
                  LookAndFeel.NativeStyle = False
                  object ABcxGridDBBandedTableView3: TABcxGridDBBandedTableView
                    PopupMenu.AutoHotkeys = maManual
                    PopupMenu.CloseFootStr = False
                    PopupMenu.LinkTableView = ABcxGridDBBandedTableView3
                    PopupMenu.AutoApplyBestFit = True
                    PopupMenu.AutoCreateAllItem = True
                    Navigator.Buttons.CustomButtons = <>
                    DataController.Filter.Options = [fcoCaseInsensitive]
                    DataController.Filter.AutoDataSetFilter = True
                    DataController.Filter.TranslateBetween = True
                    DataController.Filter.TranslateIn = True
                    DataController.Filter.TranslateLike = True
                    DataController.Summary.DefaultGroupSummaryItems = <>
                    DataController.Summary.FooterSummaryItems = <>
                    DataController.Summary.SummaryGroups = <>
                    OptionsBehavior.AlwaysShowEditor = True
                    OptionsBehavior.FocusCellOnTab = True
                    OptionsBehavior.GoToNextCellOnEnter = True
                    OptionsCustomize.ColumnsQuickCustomization = True
                    OptionsCustomize.DataRowSizing = True
                    OptionsData.Deleting = False
                    OptionsData.Inserting = False
                    OptionsSelection.HideFocusRectOnExit = False
                    OptionsSelection.MultiSelect = True
                    OptionsView.CellAutoHeight = True
                    OptionsView.GroupByBox = False
                    OptionsView.BandHeaders = False
                    Bands = <
                      item
                      end>
                    ExtPopupMenu.AutoHotkeys = maManual
                    ExtPopupMenu.CloseFootStr = False
                    ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView3
                    ExtPopupMenu.AutoApplyBestFit = True
                    ExtPopupMenu.AutoCreateAllItem = True
                  end
                  object cxGridLevel3: TcxGridLevel
                    GridView = ABcxGridDBBandedTableView3
                  end
                end
              end
              object cxTabSheet8: TcxTabSheet
                Caption = #31532#22235#25968#25454#38598
                ImageIndex = 3
                TabVisible = False
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object ABDBcxGrid4: TABcxGrid
                  Left = 0
                  Top = 0
                  Width = 481
                  Height = 485
                  Align = alClient
                  TabOrder = 0
                  LookAndFeel.NativeStyle = False
                  object ABcxGridDBBandedTableView4: TABcxGridDBBandedTableView
                    PopupMenu.AutoHotkeys = maManual
                    PopupMenu.CloseFootStr = False
                    PopupMenu.LinkTableView = ABcxGridDBBandedTableView4
                    PopupMenu.AutoApplyBestFit = True
                    PopupMenu.AutoCreateAllItem = True
                    Navigator.Buttons.CustomButtons = <>
                    DataController.Filter.Options = [fcoCaseInsensitive]
                    DataController.Filter.AutoDataSetFilter = True
                    DataController.Filter.TranslateBetween = True
                    DataController.Filter.TranslateIn = True
                    DataController.Filter.TranslateLike = True
                    DataController.Summary.DefaultGroupSummaryItems = <>
                    DataController.Summary.FooterSummaryItems = <>
                    DataController.Summary.SummaryGroups = <>
                    OptionsBehavior.AlwaysShowEditor = True
                    OptionsBehavior.FocusCellOnTab = True
                    OptionsBehavior.GoToNextCellOnEnter = True
                    OptionsCustomize.ColumnsQuickCustomization = True
                    OptionsCustomize.DataRowSizing = True
                    OptionsData.Deleting = False
                    OptionsData.Inserting = False
                    OptionsSelection.HideFocusRectOnExit = False
                    OptionsSelection.MultiSelect = True
                    OptionsView.CellAutoHeight = True
                    OptionsView.GroupByBox = False
                    OptionsView.BandHeaders = False
                    Bands = <
                      item
                      end>
                    ExtPopupMenu.AutoHotkeys = maManual
                    ExtPopupMenu.CloseFootStr = False
                    ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView4
                    ExtPopupMenu.AutoApplyBestFit = True
                    ExtPopupMenu.AutoCreateAllItem = True
                  end
                  object cxGridLevel4: TcxGridLevel
                    GridView = ABcxGridDBBandedTableView4
                  end
                end
              end
              object cxTabSheet9: TcxTabSheet
                Caption = #31532#20116#25968#25454#38598
                ImageIndex = 4
                TabVisible = False
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object ABDBcxGrid5: TABcxGrid
                  Left = 0
                  Top = 0
                  Width = 481
                  Height = 485
                  Align = alClient
                  TabOrder = 0
                  LookAndFeel.NativeStyle = False
                  object ABcxGridDBBandedTableView5: TABcxGridDBBandedTableView
                    PopupMenu.AutoHotkeys = maManual
                    PopupMenu.CloseFootStr = False
                    PopupMenu.LinkTableView = ABcxGridDBBandedTableView5
                    PopupMenu.AutoApplyBestFit = True
                    PopupMenu.AutoCreateAllItem = True
                    Navigator.Buttons.CustomButtons = <>
                    DataController.Filter.Options = [fcoCaseInsensitive]
                    DataController.Filter.AutoDataSetFilter = True
                    DataController.Filter.TranslateBetween = True
                    DataController.Filter.TranslateIn = True
                    DataController.Filter.TranslateLike = True
                    DataController.Summary.DefaultGroupSummaryItems = <>
                    DataController.Summary.FooterSummaryItems = <>
                    DataController.Summary.SummaryGroups = <>
                    OptionsBehavior.AlwaysShowEditor = True
                    OptionsBehavior.FocusCellOnTab = True
                    OptionsBehavior.GoToNextCellOnEnter = True
                    OptionsCustomize.ColumnsQuickCustomization = True
                    OptionsCustomize.DataRowSizing = True
                    OptionsData.Deleting = False
                    OptionsData.Inserting = False
                    OptionsSelection.HideFocusRectOnExit = False
                    OptionsSelection.MultiSelect = True
                    OptionsView.CellAutoHeight = True
                    OptionsView.GroupByBox = False
                    OptionsView.BandHeaders = False
                    Bands = <
                      item
                      end>
                    ExtPopupMenu.AutoHotkeys = maManual
                    ExtPopupMenu.CloseFootStr = False
                    ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView5
                    ExtPopupMenu.AutoApplyBestFit = True
                    ExtPopupMenu.AutoCreateAllItem = True
                  end
                  object cxGridLevel5: TcxGridLevel
                    GridView = ABcxGridDBBandedTableView5
                  end
                end
              end
              object cxTabSheet10: TcxTabSheet
                Caption = #31532#20845#25968#25454#38598
                ImageIndex = 5
                TabVisible = False
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object ABDBcxGrid6: TABcxGrid
                  Left = 0
                  Top = 0
                  Width = 481
                  Height = 485
                  Align = alClient
                  TabOrder = 0
                  LookAndFeel.NativeStyle = False
                  object ABcxGridDBBandedTableView6: TABcxGridDBBandedTableView
                    PopupMenu.AutoHotkeys = maManual
                    PopupMenu.CloseFootStr = False
                    PopupMenu.LinkTableView = ABcxGridDBBandedTableView6
                    PopupMenu.AutoApplyBestFit = True
                    PopupMenu.AutoCreateAllItem = True
                    Navigator.Buttons.CustomButtons = <>
                    DataController.Filter.Options = [fcoCaseInsensitive]
                    DataController.Filter.AutoDataSetFilter = True
                    DataController.Filter.TranslateBetween = True
                    DataController.Filter.TranslateIn = True
                    DataController.Filter.TranslateLike = True
                    DataController.Summary.DefaultGroupSummaryItems = <>
                    DataController.Summary.FooterSummaryItems = <>
                    DataController.Summary.SummaryGroups = <>
                    OptionsBehavior.AlwaysShowEditor = True
                    OptionsBehavior.FocusCellOnTab = True
                    OptionsBehavior.GoToNextCellOnEnter = True
                    OptionsCustomize.ColumnsQuickCustomization = True
                    OptionsCustomize.DataRowSizing = True
                    OptionsData.Deleting = False
                    OptionsData.Inserting = False
                    OptionsSelection.HideFocusRectOnExit = False
                    OptionsSelection.MultiSelect = True
                    OptionsView.CellAutoHeight = True
                    OptionsView.GroupByBox = False
                    OptionsView.BandHeaders = False
                    Bands = <
                      item
                      end>
                    ExtPopupMenu.AutoHotkeys = maManual
                    ExtPopupMenu.CloseFootStr = False
                    ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView6
                    ExtPopupMenu.AutoApplyBestFit = True
                    ExtPopupMenu.AutoCreateAllItem = True
                  end
                  object cxGridLevel6: TcxGridLevel
                    GridView = ABcxGridDBBandedTableView6
                  end
                end
              end
            end
          end
          object cxTabSheet4: TcxTabSheet
            Caption = #20132#21449#34920
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object ABcxDBPivotGrid1: TABcxDBPivotGrid
              Left = 0
              Top = 0
              Width = 483
              Height = 487
              Align = alClient
              Groups = <>
              OptionsView.ColumnFields = False
              OptionsView.ColumnGrandTotalText = #24635#35745
              OptionsView.DataFields = False
              OptionsView.FilterFields = False
              OptionsView.RowGrandTotalText = #24635#35745
              PopupMenu.LinkcxDBPivotGrid = ABcxDBPivotGrid1
              TabOrder = 0
            end
          end
        end
      end
    end
    object Panel811: TPanel
      Left = 194
      Top = 1
      Width = 210
      Height = 511
      Align = alLeft
      ParentBackground = False
      TabOrder = 1
      object Panel8: TPanel
        Left = 1
        Top = 1
        Width = 208
        Height = 509
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
      end
    end
    object Splitter6: TABcxSplitter
      Left = 186
      Top = 1
      Width = 8
      Height = 511
      Cursor = crVSplit
      HotZoneClassName = 'TcxMediaPlayer8Style'
      InvertDirection = True
      Control = Panel3
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 185
      Height = 511
      Align = alLeft
      TabOrder = 3
      object TreeView1: TABcxTreeView
        Left = 1
        Top = 1
        Width = 183
        Height = 509
        Align = alClient
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = []
        Style.LookAndFeel.Kind = lfFlat
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.Kind = lfFlat
        StyleFocused.LookAndFeel.Kind = lfFlat
        StyleHot.LookAndFeel.Kind = lfFlat
        TabOrder = 0
        OnDblClick = TreeView1DblClick
        HideSelection = False
        ReadOnly = True
        RowSelect = True
        ShowLines = False
        OnChange = TreeView1Change
        ExtFullExpand = False
      end
      object ScrollBox1: TScrollBox
        Left = 1
        Top = 1
        Width = 183
        Height = 509
        Align = alClient
        TabOrder = 1
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 179
          Height = 25
          Align = alTop
          AutoSize = True
          BevelOuter = bvNone
          TabOrder = 0
        end
      end
    end
    object ABcxSplitter1: TABcxSplitter
      Left = 404
      Top = 1
      Width = 8
      Height = 511
      Cursor = crVSplit
      HotZoneClassName = 'TcxMediaPlayer8Style'
      InvertDirection = True
      Control = Panel811
    end
  end
  object Rpt_ImageList: TImageList
    Left = 687
    Top = 88
    Bitmap = {
      494C01010F002300040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000004000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CE9A9C009C300000CE9A9C0000000000CE656300CE656300CE65
      6300CE656300CE656300CE656300CE656300CE656300CE656300CE656300CE65
      6300CE656300CE656300CE656300CE6563000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9A6300CE9A6300CE9A
      6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A
      6300CE9A6300CE9A6300CE9A6300CE9A630000000000CE9A6300CE9A6300CE9A
      6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A
      6300CE9A9C009C3000009C3000009C30000000000000CE656300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFCFCE00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE6563000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9A6300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE9A630000000000CE9A6300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008482
      84009C3000009C3000009C300000CE9A9C0000000000CE656300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFCFCE00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000009C00FFFFFF00CE6563000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9A6300FFFFFF009C30
      00009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      00009C3000009C300000FFFFFF00CE9A630000000000CE9A6300FFFFFF00E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7008482840084828400848284009C30
      00009C3000009C300000CE9A9C000000000000000000CE656300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFCFCE00FFCFCE00FFCFCE00FFCF
      CE00FFCFCE00FFCFCE00FFCFCE00CE6563000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9A6300FFFFFF009C30
      0000CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A
      6300CE9A63009C300000FFFFFF00CE9A630000000000CE9A6300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00E7E7E7009C9A6300FFFFCE00FFFFCE00FFFFFF00CECF
      9C009C300000CE9A9C00000000000000000000000000CE656300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFCFCE00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE6563000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9A6300FFFFFF009C30
      00009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      00009C3000009C300000FFFFFF00CE9A630000000000CE9A6300FFFFFF00E7E7
      E700E7E7E700E7E7E7009C9A9C00F7EBBD00FFFFCE00FFFFCE00FFFFCE00FFFF
      FF006365630000000000000000000000000000000000CE656300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000009C00FFFFFF00FFCFCE00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000009C00FFFFFF00CE6563000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9A6300FFFFFF009C30
      0000CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A
      6300CE9A63009C300000FFFFFF00CE9A630000000000CE9A6300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF009C9A9C00F7EBBD00FFFFFF00F7EBBD00FFFFCE00FFFF
      CE006365630000000000000000000000000000000000CE656300FFCFCE00FFCF
      CE00FFCFCE00FFCFCE00FFCFCE00FFCFCE00FFCFCE00FFCFCE00FFCFCE00FFCF
      CE00FFCFCE00FFCFCE00FFCFCE00CE6563000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9A6300FFFFFF009C30
      00009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      00009C3000009C300000FFFFFF00CE9A630000000000CE9A6300FFFFFF00E7E7
      E700E7E7E700E7E7E7009C9A9C00F7EBBD00FFFFFF00FFFFFF00F7EBBD00FFFF
      CE006365630000000000000000000000000000000000CE656300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFCFCE00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE6563000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9A6300FFFFFF009C30
      0000CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A
      6300CE9A63009C300000FFFFFF00CE9A630000000000CE9A6300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF009C9A9C00F7EBBD00F7EBBD00F7EBBD009C9A
      63008482840000000000000000000000000000000000CE656300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFCFCE00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE6563000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9A6300FFFFFF009C30
      00009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      00009C3000009C300000FFFFFF00CE9A630000000000CE9A6300FFFFFF00E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E7009C9A9C009C9A9C009C9A9C00E7E7
      E700CE9A630000000000000000000000000000000000CE656300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000009C00FFFFFF00FFCFCE00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000009C00FFFFFF00CE6563000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9A6300FFFFFF009C30
      0000CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A
      6300CE9A63009C300000FFFFFF00CE9A630000000000CE9A6300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CE9A630000000000000000000000000000000000CE656300FFCFCE00FFCF
      CE00FFCFCE00FFCFCE00FFCFCE00FFCFCE00FFCFCE00FFCFCE00FFCFCE00FFCF
      CE00FFCFCE00FFCFCE00FFCFCE00CE6563000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9A6300FFFFFF009C30
      00009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      00009C3000009C300000FFFFFF00CE9A630000000000CE9A6300FFFFFF00E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700FFFFFF00CE9A6300CE9A6300CE9A
      6300CE9A630000000000000000000000000000000000CE656300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFCFCE00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE6563000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9A6300FFFFFF009C30
      0000CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A
      6300CE9A63009C300000FFFFFF00CE9A630000000000CE9A6300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9A6300E7E7E700CE9A
      63000000000000000000000000000000000000000000CE656300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFCFCE00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE6563000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9A6300FFFFFF009C30
      00009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      00009C3000009C300000FFFFFF00CE9A630000000000CE9A6300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9A6300CE9A63000000
      00000000000000000000000000000000000000000000CE656300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFCFCE00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE6563000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9A6300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE9A630000000000CE9A6300CE9A6300CE9A
      6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300000000000000
      00000000000000000000000000000000000000000000CE656300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000009C00FFFFFF00FFCFCE00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000009C00FFFFFF00CE6563000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE9A6300CE9A6300CE9A
      6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A
      6300CE9A6300CE9A6300CE9A6300CE9A63000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE656300CE656300CE65
      6300CE656300CE656300CE656300CE656300CE656300CE656300CE656300CE65
      6300CE656300CE656300CE656300CE6563000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C3000009C3000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C3000009C300000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009C30
      00009C30000000000000000000009C3000009C30000000000000000000000000
      00000000000000000000000000000000000000000000CE9A630000000000CE9A
      630000000000CE9A630000000000CE9A630000000000CE9A630000000000CE9A
      630000000000CE9A630000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C300000CE6500009C3000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C300000CE6500009C3000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009C30
      0000CE6500009C300000000000009C300000CE6500009C300000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C300000CE650000CE6500009C3000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C300000CE650000CE6500009C30000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009C30
      0000CE650000CE6500009C3000009C300000CE650000CE6500009C3000000000
      00000000000000000000000000000000000000000000CE9A6300000000009C30
      00009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      000000000000CE9A630000000000000000000000000000000000000000000000
      000000000000000000009C300000CE650000CE650000CE6500009C3000009C30
      00009C3000000000000000000000000000000000000000000000000000009C30
      00009C3000009C300000CE650000CE650000CE6500009C300000000000000000
      000000000000000000000000000000000000000000009C3000009C3000009C30
      0000CE650000CE650000CE6500009C300000CE650000CE650000CE6500009C30
      0000000000000000000000000000000000000000000000000000000000009C30
      0000CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A63009C30
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C300000CE650000CE650000CE650000CE650000CE650000CE65
      00009C3000000000000000000000000000000000000000000000000000009C30
      0000CE650000CE650000CE650000CE650000CE650000CE6500009C3000000000
      000000000000000000000000000000000000000000009C300000CE650000CE65
      0000CE650000CE650000CE650000CE6500009C300000CE650000CE650000CE65
      00009C30000000000000000000000000000000000000CE9A6300000000009C30
      0000CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A63009C30
      000000000000CE9A630000000000000000000000000000000000000000000000
      00009C300000CE650000CE650000CE650000CE650000CE650000CE650000CE65
      00009C3000000000000000000000000000000000000000000000000000009C30
      0000CE650000CE650000CE650000CE650000CE650000CE650000CE6500009C30
      000000000000000000000000000000000000000000009C300000CE650000CE65
      0000CE650000CE650000CE650000CE650000CE6500009C300000CE650000CE65
      0000CE6500009C30000000000000000000000000000000000000000000009C30
      0000CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A63009C30
      0000000000000000000000000000000000000000000000000000000000009C30
      0000CE650000CE650000CE650000CE650000CE650000CE650000CE650000CE65
      00009C3000000000000000000000000000000000000000000000000000009C30
      0000CE650000CE650000CE650000CE650000CE650000CE650000CE650000CE65
      00009C300000000000000000000000000000000000009C300000CE650000CE65
      0000CE650000CE650000CE650000CE650000CE650000CE6500009C300000CE65
      0000CE650000CE6500009C3000000000000000000000CE9A6300000000009C30
      0000CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A63009C30
      000000000000CE9A630000000000000000000000000000000000000000000000
      00009C300000CE650000CE650000CE650000CE650000CE650000CE650000CE65
      00009C3000000000000000000000000000000000000000000000000000009C30
      0000CE650000CE650000CE650000CE650000CE650000CE650000CE6500009C30
      000000000000000000000000000000000000000000009C300000CE650000CE65
      0000CE650000CE650000CE650000CE650000CE6500009C300000CE650000CE65
      0000CE6500009C30000000000000000000000000000000000000000000009C30
      0000CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A63009C30
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C300000CE650000CE650000CE650000CE650000CE650000CE65
      00009C3000000000000000000000000000000000000000000000000000009C30
      0000CE650000CE650000CE650000CE650000CE650000CE6500009C3000000000
      000000000000000000000000000000000000000000009C300000CE650000CE65
      0000CE650000CE650000CE650000CE6500009C300000CE650000CE650000CE65
      00009C30000000000000000000000000000000000000CE9A6300000000009C30
      0000CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A63009C30
      000000000000CE9A630000000000000000000000000000000000000000000000
      000000000000000000009C300000CE650000CE650000CE6500009C3000009C30
      00009C3000000000000000000000000000000000000000000000000000009C30
      00009C3000009C300000CE650000CE650000CE6500009C300000000000000000
      000000000000000000000000000000000000000000009C3000009C3000009C30
      0000CE650000CE650000CE6500009C300000CE650000CE650000CE6500009C30
      0000000000000000000000000000000000000000000000000000000000009C30
      0000CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A63009C30
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C300000CE650000CE6500009C3000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C300000CE650000CE6500009C30000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009C30
      0000CE650000CE6500009C3000009C300000CE650000CE6500009C3000000000
      00000000000000000000000000000000000000000000CE9A6300000000009C30
      00009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      000000000000CE9A630000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C300000CE6500009C3000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C300000CE6500009C3000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009C30
      0000CE6500009C300000000000009C300000CE6500009C300000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C3000009C3000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C3000009C300000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009C30
      00009C30000000000000000000009C3000009C30000000000000000000000000
      00000000000000000000000000000000000000000000CE9A630000000000CE9A
      630000000000CE9A630000000000CE9A630000000000CE9A630000000000CE9A
      630000000000CE9A630000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009ACE0000659C000065
      9C0000659C0000659C0000659C0000659C0000659C0000659C0000659C000065
      9C0000659C0000659C0000659C0000659C000000000000000000000000000000
      0000000000009C3000009C300000CE6500009C300000CE6500009C3000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009ACE009CFFFF00009A
      CE009CFFFF00009ACE009CFFFF00009ACE009CFFFF00009ACE009CFFFF00009A
      CE009CFFFF00009ACE009CFFFF00009ACE000000000000000000000000000000
      0000000000009C300000CE6500009C300000CE6500009C3000009C3000000000
      00000000000000000000000000000000000000000000000000009C3000009C30
      00009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      00009C3000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000009ACE009CFFFF009CFF
      FF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFF
      FF009CFFFF009CFFFF009CFFFF009CFFFF000000000000000000000000000000
      0000000000009C3000009C300000CE6500009C300000CE6500009C3000000000
      00000000000000000000000000000000000000000000CE9A6300FFCF9C00FFCF
      9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00CE9A
      6300CE9A63009C30000000000000000000000000000000000000000000000000
      00000000000000000000000000009C3000009C30000000000000000000009C30
      00009C30000000000000000000000000000000000000009ACE009CFFFF009CFF
      FF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFF
      FF009CFFFF009CFFFF009CFFFF009CFFFF000000000000000000000000000000
      0000000000009C9A9C009C3000009C3000009C3000009C3000009C9A9C000000
      000000000000000000000000000000000000CE9A6300CE9A6300CE9A6300CE9A
      6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A
      63009C300000CE9A63009C300000000000000000000000000000000000000000
      000000000000000000009C300000CE6500009C300000000000009C300000CE65
      00009C30000000000000000000000000000000000000009ACE009CFFFF009CFF
      FF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF00009ACE009CFFFF009CFF
      FF009CFFFF009CFFFF009CFFFF009CFFFF000000000000000000000000000000
      000000000000000000009C9A9C00FFFFFF00FFCFCE0063656300000000000000
      000000000000000000000000000000000000CE9A6300FFFFFF00FFCF9C00FFCF
      9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF
      9C00CE9A63009C3000009C300000000000000000000000000000000000000000
      0000000000009C300000CE650000CE6500009C3000009C300000CE650000CE65
      00009C30000000000000000000000000000000000000009ACE009CFFFF009CFF
      FF009CFFFF00009ACE009CFFFF009CFFFF009CFFFF00009ACE009CFFFF009CFF
      FF009CFFFF00009ACE009CFFFF009CFFFF000000000000000000000000000000
      000000000000000000009C9A9C00FFFFFF00FFCFCE0063656300000000000000
      000000000000000000000000000000000000CE9A6300FFFFFF00FFCF9C00FFCF
      9C00FFCF9C00FFCF9C0000CF0000009A0000FFCF9C000000FF000000CE00FFCF
      9C00CE9A6300CE9A63009C300000000000000000000000000000000000000000
      00009C300000CE650000CE650000CE6500009C300000CE650000CE650000CE65
      00009C3000009C3000009C3000000000000000000000009ACE009CFFFF00009A
      CE009CFFFF00009ACE009CFFFF00009ACE009CFFFF00009ACE009CFFFF00009A
      CE009CFFFF00009ACE009CFFFF00009ACE000000000000000000000000000000
      000000000000000000009C9A9C00FFFFFF00FFCFCE0063656300000000000000
      000000000000000000000000000000000000CE9A6300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CE9A6300CE9A6300CE9A63009C3000000000000000000000000000009C30
      0000CE650000CE650000CE6500009C300000CE650000CE650000CE650000CE65
      0000CE650000CE6500009C3000000000000000000000009ACE00009ACE00009A
      CE00009ACE00009ACE00009ACE00009ACE00009ACE00009ACE00009ACE00009A
      CE00009ACE00009ACE00009ACE00009ACE000000000000000000000000000000
      000000000000000000009C9A9C00FFFFFF00FFCFCE0063656300000000000000
      000000000000000000000000000000000000CE9A6300FFFFFF00FFCF9C00FFCF
      9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF9C00FFCF
      9C00CE9A6300CE9A6300CE9A63009C30000000000000000000009C300000CE65
      0000CE650000CE6500009C300000CE650000CE650000CE650000CE650000CE65
      0000CE650000CE6500009C300000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CE65000000000000000000000000000000000000000000000000
      000000000000000000009C9A9C00FFFFFF00FFCFCE0063656300000000000000
      00000000000000000000000000000000000000000000CE9A6300CE9A6300CE9A
      6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300FFCF
      9C00FFCF9C00CE9A6300CE9A63009C300000000000009C300000CE650000CE65
      0000CE6500009C300000CE650000CE650000CE650000CE650000CE650000CE65
      0000CE650000CE6500009C300000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C3000000000
      000000000000CE65000000000000000000000000000000000000000000000000
      0000000000000000000063656300636563006365630063656300000000000000
      0000000000000000000000000000636563000000000000000000CE9A6300FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CE9A
      6300FFCF9C00FFCF9C00CE9A63009C30000000000000000000009C300000CE65
      0000CE650000CE6500009C300000CE650000CE650000CE650000CE650000CE65
      0000CE650000CE6500009C300000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C3000009C30
      000000000000CE65000000000000000000000000000063656300636563000000
      000000000000636563009C9A9C009C9A9C009C9A9C009C9A9C00636563000000
      000000000000000000006365630063656300000000000000000000000000CE9A
      6300FFFFFF00E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700FFFFFF00CE9A
      6300CE9A6300CE9A63009C300000000000000000000000000000000000009C30
      0000CE650000CE650000CE6500009C300000CE650000CE650000CE650000CE65
      0000CE650000CE6500009C300000000000000000000000000000000000000000
      00009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      00009C300000CE65000000000000000000009C9A9C009C9A9C009C9A9C006365
      6300636563009C9A9C00CECFCE00CECFCE00CECFCE009C9A9C00636563006365
      63006365630063656300CECFCE0063656300000000000000000000000000CE9A
      6300FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CE9A63000000000000000000000000000000000000000000000000000000
      00009C300000CE650000CE650000CE6500009C300000CE650000CE650000CE65
      00009C3000009C3000009C300000000000000000000000000000000000000000
      00009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      00009C300000CE65000000000000000000009C9A9C00FFFFFF00CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE009C9A9C009C9A
      9C009C9A9C00CECFCE0063656300000000000000000000000000000000000000
      0000CE9A6300FFFFFF00E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700FFFF
      FF00CE9A63000000000000000000000000000000000000000000000000000000
      0000000000009C300000CE650000CE6500009C3000009C300000CE650000CE65
      00009C3000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C3000009C30
      000000000000CE65000000000000000000009C9A9C00FFFFFF00CECFCE00CECF
      CE00FFFFFF00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECF
      CE00CECFCE009C9A9C0000000000000000000000000000000000000000000000
      0000CE9A6300FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CE9A630000000000000000000000000000000000000000000000
      000000000000000000009C300000CE6500009C300000000000009C300000CE65
      00009C3000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C3000000000
      000000000000CE65000000000000000000009C9A9C00FFFFFF00FFFFFF009C9A
      9C009C9A9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C9A
      9C009C9A9C000000000000000000000000000000000000000000000000000000
      000000000000CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A6300CE9A
      6300CE9A6300CE9A630000000000000000000000000000000000000000000000
      00000000000000000000000000009C3000009C30000000000000000000009C30
      00009C3000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CE6500000000000000000000000000009C9A9C009C9A9C000000
      0000000000009C9A9C009C9A9C009C9A9C009C9A9C009C9A9C009C9A9C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009C30
      00009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      00009C3000009C3000009C3000009C3000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C3000009C30
      00009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      00009C3000009C3000009C3000000000000000000000000000009C300000CE65
      0000CE6500009C300000E7E7E700CE6500009C300000E7E7E700E7E7E700E7E7
      E7009C300000CE650000CE6500009C30000000000000319ACE0000659C000065
      9C0000659C0000659C0000659C0000659C0000659C0000659C0000659C000065
      9C0063CFCE0000000000000000000000000000000000319ACE0000659C000065
      9C0000659C0000659C0000659C0000659C0000659C0000659C0000659C000065
      9C0063CFCE00000000000000000000000000000000009C300000CE650000CE65
      00009C300000E7E7E700CE6500009C300000E7E7E700E7E7E700E7E7E7009C30
      0000CE650000CE6500009C30000000000000000000009C3000009C300000CE65
      0000CE6500009C300000E7E7E700CE6500009C300000E7E7E700E7E7E700E7E7
      E7009C300000CE650000CE6500009C300000319ACE00319ACE009CFFFF0063CF
      FF0063CFFF0063CFFF0063CFFF0063CFFF0063CFFF0063CFFF0063CFFF00319A
      CE0000659C00000000000000000000000000319ACE00319ACE009CFFFF0063CF
      FF0063CFFF0063CFFF0063CFFF0063CFFF0063CFFF0063CFFF0063CFFF00319A
      CE0000659C00000000000000000000000000000000009C300000CE650000CE65
      00009C300000E7E7E700CE6500009C300000E7E7E700E7E7E700E7E7E7009C30
      0000CE650000CE6500009C300000000000009C300000CE6500009C300000CE65
      0000CE6500009C300000E7E7E700CE6500009C300000E7E7E700E7E7E700E7E7
      E7009C300000CE650000CE6500009C300000319ACE00319ACE0063CFFF009CFF
      FF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF0063CF
      FF0000659C00319ACE000000000000000000319ACE00319ACE0063CFFF009CFF
      FF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF0063CF
      FF0000659C00319ACE000000000000000000000000009C300000CE650000CE65
      00009C300000E7E7E700CE6500009C300000E7E7E700E7E7E700E7E7E7009C30
      0000CE650000CE6500009C300000000000009C300000CE6500009C300000CE65
      0000CE6500009C300000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E7009C300000CE650000CE6500009C300000319ACE00319ACE0063CFFF009CFF
      FF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF0063CF
      FF0063CFCE0000659C000000000000000000319ACE00319ACE0063CFFF009CFF
      FF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF0063CF
      FF0063CFCE0000659C000000000000000000000000009C300000CE650000CE65
      00009C300000E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E7009C30
      0000CE650000CE6500009C300000000000009C300000CE6500009C300000CE65
      0000CE650000CE6500009C3000009C3000009C3000009C3000009C3000009C30
      0000CE650000CE650000CE6500009C300000319ACE0063CFFF00319ACE009CFF
      FF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF0063CF
      FF009CFFFF0000659C00319ACE0000000000319ACE0063CFFF00319ACE009CFF
      FF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF0063CF
      FF009CFFFF0000659C00319ACE0000000000000000009C300000CE650000CE65
      0000CE6500009C3000009C3000009C3000009C3000009C3000009C300000CE65
      0000CE650000CE6500009C300000000000009C300000CE6500009C300000CE65
      0000CE650000CE650000CE650000CE650000CE650000CE650000CE650000CE65
      0000CE650000CE650000CE6500009C300000319ACE0063CFFF0063CFCE0063CF
      CE009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF0063CF
      FF009CFFFF0063CFCE0000659C0000000000319ACE0063CFFF0063CFCE0063CF
      CE009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF009CFFFF0063CF
      FF009CFFFF0063CFCE0000659C0000000000000000009C300000CE650000CE65
      0000CE650000CE650000CE650000CE650000CE650000CE650000CE650000CE65
      0000CE650000CE6500009C300000000000009C300000CE6500009C300000CE65
      0000CE6500009C3000009C3000009C3000009C3000009C3000009C3000009C30
      00009C300000CE650000CE6500009C300000319ACE009CFFFF0063CFFF00319A
      CE00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF009CFF
      FF00CEFFFF00CEFFFF0000659C0000000000319ACE009CFFFF0063CFFF00319A
      CE00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF009CFF
      FF00CEFFFF00CEFFFF0000659C0000000000000000009C300000CE650000CE65
      00009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      0000CE650000CE6500009C300000000000009C300000CE6500009C300000CE65
      00009C300000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF009C300000CE6500009C300000319ACE009CFFFF009CFFFF0063CF
      FF00319ACE00319ACE00319ACE00319ACE00319ACE00319ACE00319ACE00319A
      CE00319ACE00319ACE0063CFFF0000000000319ACE009CFFFF009CFFFF0063CF
      FF00319ACE00319ACE00319ACE00319ACE00319ACE00319ACE00319ACE00319A
      CE00319ACE00319ACE0063CFFF0000000000000000009C300000CE6500009C30
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF009C300000CE6500009C300000000000009C300000CE6500009C300000CE65
      00009C300000FFFFFF009C3000009C3000009C3000009C3000009C3000009C30
      0000FFFFFF009C300000CE6500009C300000319ACE00CEFFFF009CFFFF009CFF
      FF009CFFFF009CFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF000065
      9C0000000000000000000000000000000000319ACE00CEFFFF009CFFFF009CFF
      FF009CFFFF009CFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF00CEFFFF000065
      9C0000000000000000000000000000000000000000009C300000CE6500009C30
      0000FFFFFF009C3000009C3000009C3000009C3000009C3000009C300000FFFF
      FF009C300000CE6500009C300000000000009C300000CE6500009C300000CE65
      00009C300000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF009C300000CE6500009C30000000000000319ACE00CEFFFF00CEFF
      FF00CEFFFF00CEFFFF00319ACE00319ACE00319ACE00319ACE00319ACE000000
      00000000000000000000000000000000000000000000319ACE00CEFFFF00CEFF
      FF00CEFFFF00CEFFFF00319ACE00319ACE00319ACE00319ACE00319ACE000000
      000000000000000000000000000000000000000000009C300000CE6500009C30
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF009C300000CE6500009C300000000000009C300000CE6500009C300000E7E7
      E7009C300000FFFFFF009C3000009C3000009C3000009C3000009C3000009C30
      0000FFFFFF009C3000009C3000009C3000000000000000000000319ACE00319A
      CE00319ACE00319ACE0000000000000000000000000000000000000000000000
      0000000000009C3000009C3000009C3000000000000000000000319ACE00319A
      CE00319ACE00319ACE0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C300000E7E7E7009C30
      0000FFFFFF009C3000009C3000009C3000009C3000009C3000009C300000FFFF
      FF009C3000009C3000009C300000000000009C300000CE6500009C300000CE65
      00009C300000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF009C300000CE6500009C3000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C3000009C3000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C300000CE6500009C30
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF009C300000CE6500009C300000000000009C300000E7E7E7009C3000009C30
      00009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      00009C3000009C3000009C3000009C3000000000000000000000000000000000
      000000000000000000000000000000000000000000009C300000000000000000
      0000000000009C300000000000009C3000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C3000009C3000009C30
      00009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      00009C3000009C3000009C300000000000009C300000CE6500009C300000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C30
      0000CE6500009C30000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C3000009C30
      00009C3000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C3000009C3000009C3000009C30
      00009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      00009C3000009C3000000000000000000000424D3E000000000000003E000000
      2800000040000000400000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF00FFFFFFF8800000008000800080000000
      8000800080000000800080018000000080008003800000008000800780000000
      8000800780000000800080078000000080008007800000008000800780000000
      800080078000000080008007800000008000800F800000008000801F80000000
      8000803F800000008000FFFF80000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF9FF9FFE67FAAABFF1FF8FFE23FFFFFFE1FF87FE01FA00BFC07E03F800FE00F
      F807E01F8007A00BF007E00F8003E00FE007E0078001A00BF007E00F8003E00F
      F807E01F8007A00BFC07E03F800FE00FFE1FF87FE01FA00BFF1FF8FFE23FFFFF
      FF9FF9FFE67FAAABFFFFFFFFFFFFFFFF8000F81FFFFFFFFF8000F81FC007FFFF
      8000F81F8003FE678000F81F0001FC478000FC3F0001F8078000FC3F0001F001
      8000FC3F0000E0018000FC3F0000C001FFFBFC3F80008001FFDBFC3EC000C001
      FFCB981CE001E001F0030000E007F001F0030001F007F807FFCB0003F003FC47
      FFDB0007F803FE67FFFB981FFFFFFFFFFFFFFFFFFFFFE000FFFFFFFFC001C000
      8007800780018000000700078001000000030003800100000003000380010000
      0001000180010000000100018001000000010001800100000001000180010000
      000F000F80010000801F801F80010000C3F8C3FF80010000FFFCFFFF80010000
      FFBAFFFF80010003FFC7FFFFFFFF000300000000000000000000000000000000
      000000000000}
  end
  object PopupMenu1: TPopupMenu
    Left = 156
    Top = 454
    object Preview1: TMenuItem
      Tag = 91
      AutoCheck = True
      Caption = #22312#26412#31383#20307#20013#39044#35272
      GroupIndex = 11
      RadioItem = True
      OnClick = Preview1Click
    end
    object Preview2: TMenuItem
      Tag = 91
      AutoCheck = True
      Caption = #22312#26032#31383#20307#20013#39044#35272
      GroupIndex = 11
      RadioItem = True
      OnClick = Preview2Click
    end
    object N1: TMenuItem
      Caption = '-'
      GroupIndex = 11
    end
    object Preview3: TMenuItem
      Tag = 91
      AutoCheck = True
      Caption = #22312#34920#26684#20013#26174#31034
      Checked = True
      Default = True
      GroupIndex = 11
      RadioItem = True
      OnClick = Preview3Click
    end
    object N2: TMenuItem
      Caption = #34920#26684#20013#26174#31034
      GroupIndex = 11
      object N3: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #34920#26684#21015#34920
        Checked = True
        OnClick = N3Click
      end
      object N4: TMenuItem
        Tag = 91
        AutoCheck = True
        Caption = #20132#21449#34920
        OnClick = N4Click
      end
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #23435#20307
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = Rpt_ImageList
    PopupMenuLinks = <>
    Style = bmsUseLookAndFeel
    UseSystemFont = True
    Left = 648
    Top = 112
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockControl = dxBarDockControl1
      DockedDockControl = dxBarDockControl1
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 256
      FloatTop = 215
      FloatClientWidth = 51
      FloatClientHeight = 22
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 69
          Visible = True
          ItemName = 'Rpt_ToPageEdit'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarButton11'
        end
        item
          Visible = True
          ItemName = 'Rpt_CustScaleCbx'
        end
        item
          Visible = True
          ItemName = 'dxBarButton10'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = True
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
      ImageIndex = 5
      OnClick = dxBarButton1Click
    end
    object dxBarButton2: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
      ImageIndex = 9
      OnClick = dxBarButton2Click
    end
    object dxBarButton3: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
      ImageIndex = 8
      OnClick = dxBarButton3Click
    end
    object dxBarButton4: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
      ImageIndex = 6
      OnClick = dxBarButton4Click
    end
    object dxBarButton5: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
      ImageIndex = 7
      OnClick = dxBarButton5Click
    end
    object dxBarButton6: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
      ImageIndex = 10
      OnClick = dxBarButton6Click
    end
    object dxBarButton7: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
      ImageIndex = 11
      OnClick = dxBarButton7Click
    end
    object dxBarButton8: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton9: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton10: TdxBarButton
      Caption = '100%'
      Category = 0
      Hint = '100%'
      Visible = ivAlways
      ImageIndex = 13
      PaintStyle = psCaptionGlyph
      OnClick = dxBarButton10Click
    end
    object dxBarButton11: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
      ImageIndex = 12
      OnClick = dxBarButton11Click
    end
    object Rpt_ToPageEdit: TdxBarEdit
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      OnKeyUp = Rpt_ToPageEditKeyUp
      Text = '0'
    end
    object Rpt_CustScaleCbx: TdxBarCombo
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      OnChange = Rpt_CustScaleCbxChange
      Text = '100'
      Items.Strings = (
        '25'
        '50'
        '75'
        '80'
        '90'
        '100'
        '150'
        '200')
      ItemIndex = 5
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 228
    Top = 454
    object N5: TMenuItem
      Tag = 91
      AutoCheck = True
      Caption = #26641#24418#30446#24405#26174#31034#25253#34920
      Checked = True
      Default = True
      GroupIndex = 113
      RadioItem = True
      OnClick = N5Click
    end
    object N6: TMenuItem
      Tag = 91
      AutoCheck = True
      Caption = #25353#38062#21015#34920#26174#31034#25253#34920
      GroupIndex = 113
      RadioItem = True
      OnClick = N6Click
    end
  end
end
