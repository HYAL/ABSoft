object ABSys_Org_UpdateForm: TABSys_Org_UpdateForm
  Left = 173
  Top = 129
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = #19979#36733#26356#26032
  ClientHeight = 350
  ClientWidth = 550
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 284
    Width = 550
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      550
      41)
    object Button1: TABcxButton
      Left = 306
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #24320#22987#26356#26032
      LookAndFeel.Kind = lfFlat
      TabOrder = 0
      OnClick = Button1Click
      ShowProgressBar = False
    end
    object Button2: TABcxButton
      Left = 458
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #36864#20986
      LookAndFeel.Kind = lfFlat
      TabOrder = 1
      OnClick = Button2Click
      ShowProgressBar = False
    end
    object Button3: TABcxButton
      Left = 8
      Top = 6
      Width = 75
      Height = 25
      Caption = #20840#36873
      LookAndFeel.Kind = lfFlat
      TabOrder = 2
      OnClick = Button3Click
      ShowProgressBar = False
    end
    object Button4: TABcxButton
      Left = 84
      Top = 6
      Width = 75
      Height = 25
      Caption = #20840#19981#36873
      LookAndFeel.Kind = lfFlat
      TabOrder = 3
      OnClick = Button4Click
      ShowProgressBar = False
    end
    object ABcxButton1: TABcxButton
      Left = 382
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #20572#27490
      LookAndFeel.Kind = lfFlat
      TabOrder = 4
      OnClick = ABcxButton1Click
      ShowProgressBar = False
    end
  end
  object cxCheckListBox1: TcxCheckListBox
    Left = 0
    Top = 0
    Width = 550
    Height = 284
    Align = alClient
    EditValueFormat = cvfCaptions
    ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
    Items = <
      item
      end>
    TabOrder = 1
  end
  object cxCheckListBox2: TcxCheckListBox
    Left = 358
    Top = 152
    Width = 67
    Height = 43
    EditValueFormat = cvfCaptions
    ImeName = #20013#25991' ('#31616#20307') - '#25628#29399#25340#38899#36755#20837#27861
    Items = <
      item
      end>
    TabOrder = 2
    Visible = False
  end
  object ABdxStatusBar1: TABdxStatusBar
    Left = 0
    Top = 325
    Width = 550
    Height = 25
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = #36895#24230
        Width = 40
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 100
      end
      item
        PanelStyleClassName = 'TdxStatusBarContainerPanelStyle'
        PanelStyle.Container = ABdxStatusBar1Container2
      end>
    PaintStyle = stpsUseLookAndFeel
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    object ABdxStatusBar1Container2: TdxStatusBarContainerControl
      Left = 149
      Top = 3
      Width = 384
      Height = 21
      object ABcxProgressBar1: TABcxProgressBar
        Left = 0
        Top = 0
        Align = alClient
        Style.BorderStyle = ebsFlat
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 0
        Width = 384
      end
    end
  end
  object FTPClient: TIdFTP
    OnWorkEnd = FTPClientWorkEnd
    IPVersion = Id_IPv4
    NATKeepAlive.UseKeepAlive = False
    NATKeepAlive.IdleTimeMS = 0
    NATKeepAlive.IntervalMS = 0
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    ReadTimeout = 0
    Left = 248
    Top = 95
  end
end
