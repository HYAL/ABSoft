object ABSys_Org_RegisteredForm: TABSys_Org_RegisteredForm
  Left = 173
  Top = 129
  Caption = #36719#20214#27880#20876
  ClientHeight = 450
  ClientWidth = 350
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  DesignSize = (
    350
    450)
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 350
    Height = 81
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      350
      81)
    object ABcxTextEdit2: TABcxTextEdit
      Tag = 91
      Left = 70
      Top = 41
      Anchors = [akLeft, akTop, akRight]
      Enabled = False
      TabOrder = 1
      Width = 268
    end
    object ABcxLabel2: TABcxLabel
      Left = 11
      Top = 43
      AutoSize = False
      Caption = #27880#20876#30721'  '
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -14
      Style.Font.Name = #23435#20307
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Properties.Alignment.Vert = taVCenter
      Transparent = True
      Height = 17
      Width = 59
      AnchorY = 52
    end
    object ABcxLabel1: TABcxLabel
      Left = 11
      Top = 16
      AutoSize = False
      Caption = #27880#20876#21517#31216'  '
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -14
      Style.Font.Name = #23435#20307
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Properties.Alignment.Vert = taVCenter
      Transparent = True
      Height = 17
      Width = 60
      AnchorY = 25
    end
    object ABcxTextEdit1: TABcxTextEdit
      Tag = 91
      Left = 70
      Top = 14
      Anchors = [akLeft, akTop, akRight]
      TabOrder = 0
      Width = 268
    end
  end
  object Memo1: TABcxMemo
    Left = 11
    Top = 80
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      #38750#24120#24863#35874#24744#30340#20351#29992)
    ParentFont = False
    Properties.ReadOnly = True
    Properties.ScrollBars = ssVertical
    TabOrder = 1
    Height = 319
    Width = 328
  end
  object Panel2: TPanel
    Left = 0
    Top = 409
    Width = 350
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    DesignSize = (
      350
      41)
    object Button2: TABcxButton
      Left = 127
      Top = 6
      Width = 107
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #21152#36733#25480#26435#25991#20214
      LookAndFeel.Kind = lfFlat
      TabOrder = 0
      OnClick = Button2Click
      ShowProgressBar = False
    end
    object ABcxLabel3: TABcxLabel
      Left = 12
      Top = 8
      AutoSize = False
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      Properties.Alignment.Vert = taVCenter
      Transparent = True
      Height = 17
      Width = 85
      AnchorY = 17
    end
    object ABcxButton1: TABcxButton
      Left = 256
      Top = 6
      Width = 83
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #36864#20986
      LookAndFeel.Kind = lfFlat
      TabOrder = 2
      OnClick = ABcxButton1Click
      ShowProgressBar = False
    end
  end
end
