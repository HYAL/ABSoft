unit ABSys_Org_RegisteredU;
                                         
interface
                              
uses
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,
  ABPubPassU,
  ABPubMessageU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFramkWorkControlU,
  ABFramkWorkQueryU,
  ABFramkWorkFuncU,
  ABFramkWorkFuncFormU,

  cxLookAndFeels,
  cxGraphics,
  cxLabel,
  cxMemo,
  cxTextEdit,
  cxEdit,
  cxContainer,
  cxControls,
  cxButtons,
  cxLookAndFeelPainters,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,
  StdCtrls,Buttons,ExtCtrls,CheckLst,ComCtrls,Menus;
                                                     
type                                                             
  TABSys_Org_RegisteredForm = class(TABFuncForm)
    Panel1: TPanel;
    Memo1: TABcxMemo;
    Panel2: TPanel;
    Button2: TABcxButton;
    ABcxTextEdit2: TABcxTextEdit;
    ABcxLabel2: TABcxLabel;
    ABcxLabel1: TABcxLabel;
    ABcxTextEdit1: TABcxTextEdit;
    ABcxLabel3: TABcxLabel;
    ABcxButton1: TABcxButton;
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ABcxButton1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Org_RegisteredForm: TABSys_Org_RegisteredForm;

implementation

{$R *.dfm}

//框架注册代码
procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Org_RegisteredForm.ClassName;
end;

exports
   ABRegister ;
//框架注册代码

procedure TABSys_Org_RegisteredForm.ABcxButton1Click(Sender: TObject);
begin
  if (ABcxTextEdit1.Enabled) and
     (ABGetFieldValue(ABGetConstSqlPubDataset('ABSys_Org_Parameter',[]),['Pa_Name'],['RegName'],['Pa_Value'],'')<>ABcxTextEdit1.Text) then
  begin
    ABShow('注册名称已成功修改,请重启系统.');
  end;
  close;
end;

procedure TABSys_Org_RegisteredForm.Button2Click(Sender: TObject);
var
  tempFieldName,
  tempRegName,
  tempRegKey:string;
  tempStrings:Tstrings;
begin
  tempFieldName:=ABSelectFile(ABPublicSetPath,'','Key Files|*.Key');
  if ABCheckFileExists(tempFieldName)  then
  begin
    tempStrings:=TStringList.Create;
    try
      tempStrings.LoadFromFile(tempFieldName);
      tempStrings.Text:=ABUnDOPassWord(tempStrings.Text);

      tempRegName:=ABReadIniInStrings('MainSetup','RegName',tempStrings);
      tempRegKey :=ABReadIniInStrings('MainSetup','RegKey',tempStrings);
      if ABDoRegisterKey(tempRegName)=tempRegKey then
      begin
        //将注册文件更新到服务器，且将版本号加1
        ABExecSQL('Main','exec Proc_SetPublicPkgNewVersion  ''License.key'',''ABClientP_Set\'' ');
        ABUpFileToField('Main','ABSys_Org_Function','FU_Pkg',
                        ' where FU_FileName='+ABQuotedStr('License.key'),
                        tempFieldName);

        ABcxTextEdit1.Text:=tempRegName;
        ABcxTextEdit2.Text:=tempRegKey;
        ABCopyFile(tempFieldName,ABPublicSetPath +'License.key');

        ABcxLabel3.Caption:='已注册';
        ABShow('注册信息已成功写入,请重启系统.');
      end
      else
      begin
        ABShow('授权文件[%s]已损坏,请联系软件供应商.',['License.key']);
      end;
    finally
      tempStrings.Free;
    end;
  end;
end;

procedure TABSys_Org_RegisteredForm.FormCreate(Sender: TObject);
var
  tempEditRegNameType:string;
  tempStrings:Tstrings;
begin
  tempStrings:=TStringList.Create;
  try
    if ABCheckFileExists(ABPublicSetPath+'License.key')  then
      tempStrings.LoadFromFile(ABPublicSetPath+'License.key');
    tempStrings.Text:=ABUnDOPassWord(tempStrings.Text);

    ABcxTextEdit1.Text:=ABGetFieldValue(ABGetConstSqlPubDataset('ABSys_Org_Parameter',[]),['Pa_Name'],['RegName'],['Pa_Value'],'');
    ABcxTextEdit2.Text:=ABReadIniInStrings('MainSetup','RegKey',tempStrings);

    tempEditRegNameType:=ABReadIniInStrings('MainSetup','EditRegNameType',tempStrings);
               
    Button2.Visible:=ABPubUser.IsAdminOrSysuser;
    ABcxTextEdit1.Enabled:= (Button2.Visible) and (AnsiCompareText(tempEditRegNameType,'CanEditRegName')=0);

    if ABDoRegisterKey(ABReadIniInStrings('MainSetup','RegName',tempStrings))=ABcxTextEdit2.Text then
    begin
      ABcxLabel3.Caption:='已注册';
    end
    else
    begin
      ABcxLabel3.Caption:='未注册';
    end;

  finally
    tempStrings.Free;
  end;
end;

//框架注册代码
Initialization
  RegisterClass(TABSys_Org_RegisteredForm);
Finalization
  UnRegisterClass(TABSys_Org_RegisteredForm);


end.
