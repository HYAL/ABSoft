object ABSys_Org_DownListForm: TABSys_Org_DownListForm
  Left = 549
  Top = 127
  Caption = #19979#25289#21015#39033#31649#29702
  ClientHeight = 550
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ABDBStatusBar1: TABdxDBStatusBar
    Left = 0
    Top = 531
    Width = 750
    Height = 19
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 50
      end>
    PaintStyle = stpsUseLookAndFeel
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    StopUpdate = False
    DataSource = ABDatasource1
  end
  object Sheet1_Navigator1: TABDBNavigator
    Left = 0
    Top = 0
    Width = 750
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    ShowCaption = False
    TabOrder = 1
    BigGlyph = False
    ImageLayout = blGlyphLeft
    DataSource = ABDatasource1
    VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel, nbQuerySpacer, nbQuery, nbReport, nbExitSpacer, nbExit]
    ButtonRangeType = RtMain
    BtnCustom1ImageIndex = -1
    BtnCustom2ImageIndex = -1
    BtnCustom3ImageIndex = -1
    BtnCustom4ImageIndex = -1
    BtnCustom5ImageIndex = -1
    BtnCustom6ImageIndex = -1
    BtnCustom7ImageIndex = -1
    BtnCustom8ImageIndex = -1
    BtnCustom9ImageIndex = -1
    BtnCustom10ImageIndex = -1
    BtnCustom1Caption = #33258#23450#20041'1'
    BtnCustom2Caption = #33258#23450#20041'2'
    BtnCustom3Caption = #33258#23450#20041'3'
    BtnCustom4Caption = #33258#23450#20041'4'
    BtnCustom5Caption = #33258#23450#20041'5'
    BtnCustom6Caption = #33258#23450#20041'6'
    BtnCustom7Caption = #33258#23450#20041'7'
    BtnCustom8Caption = #33258#23450#20041'8'
    BtnCustom9Caption = #33258#23450#20041'9'
    BtnCustom10Caption = #33258#23450#20041'10'
    BtnCustom1Kind = cxbkStandard
    BtnCustom2Kind = cxbkStandard
    BtnCustom3Kind = cxbkStandard
    BtnCustom4Kind = cxbkStandard
    BtnCustom5Kind = cxbkStandard
    BtnCustom6Kind = cxbkStandard
    BtnCustom7Kind = cxbkStandard
    BtnCustom8Kind = cxbkStandard
    BtnCustom9Kind = cxbkStandard
    BtnCustom10Kind = cxbkStandard
    ApprovedRollbackButton = nbNull
    ApprovedCommitButton = nbNull
    ButtonControlType = ctSingle
  end
  object Sheet1_Panel1: TPanel
    Left = 0
    Top = 27
    Width = 750
    Height = 504
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Sheet1_Splitter1: TABcxSplitter
      Left = 167
      Top = 0
      Width = 8
      Height = 504
      HotZoneClassName = 'TcxMediaPlayer8Style'
      InvertDirection = True
      Control = Sheet1_Grid1
    end
    object Sheet1_Grid1: TABcxGrid
      Left = 0
      Top = 0
      Width = 167
      Height = 504
      Align = alLeft
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
      object Sheet1_TableView1: TABcxGridDBBandedTableView
        PopupMenu.AutoHotkeys = maManual
        PopupMenu.CloseFootStr = False
        PopupMenu.LinkTableView = Sheet1_TableView1
        PopupMenu.AutoApplyBestFit = True
        PopupMenu.AutoCreateAllItem = True
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = ABDatasource1
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Filter.AutoDataSetFilter = True
        DataController.Filter.TranslateBetween = True
        DataController.Filter.TranslateIn = True
        DataController.Filter.TranslateLike = True
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.AlwaysShowEditor = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.DataRowSizing = True
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.MultiSelect = True
        OptionsView.CellAutoHeight = True
        OptionsView.GroupByBox = False
        OptionsView.BandHeaders = False
        Bands = <
          item
          end>
        ExtPopupMenu.AutoHotkeys = maManual
        ExtPopupMenu.CloseFootStr = False
        ExtPopupMenu.LinkTableView = Sheet1_TableView1
        ExtPopupMenu.AutoApplyBestFit = True
        ExtPopupMenu.AutoCreateAllItem = True
      end
      object Sheet1_Level1: TcxGridLevel
        GridView = Sheet1_TableView1
      end
    end
    object Sheet1_Panel2: TPanel
      Left = 175
      Top = 0
      Width = 575
      Height = 504
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Sheet1_PageControl2: TABcxPageControl
        Left = 0
        Top = 188
        Width = 575
        Height = 316
        Align = alClient
        TabOrder = 0
        Properties.ActivePage = Sheet1_Detail_Sheet1
        Properties.CustomButtons.Buttons = <>
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
        ActivePageIndex = 0
        ClientRectBottom = 315
        ClientRectLeft = 1
        ClientRectRight = 574
        ClientRectTop = 21
        object Sheet1_Detail_Sheet1: TcxTabSheet
          Caption = 'Grid '#21015#34920
          ImageIndex = 0
          object Sheet1_Detail_Grid1: TABcxGrid
            Left = 0
            Top = 0
            Width = 573
            Height = 294
            Align = alClient
            TabOrder = 0
            LookAndFeel.Kind = lfFlat
            LookAndFeel.NativeStyle = False
            object Sheet1_Detail_TableView1: TABcxGridDBBandedTableView
              PopupMenu.AutoHotkeys = maManual
              PopupMenu.CloseFootStr = False
              PopupMenu.LinkTableView = Sheet1_Detail_TableView1
              PopupMenu.AutoApplyBestFit = True
              PopupMenu.AutoCreateAllItem = True
              Navigator.Buttons.CustomButtons = <>
              DataController.DataSource = ABDatasource1_1
              DataController.Filter.Options = [fcoCaseInsensitive]
              DataController.Filter.AutoDataSetFilter = True
              DataController.Filter.TranslateBetween = True
              DataController.Filter.TranslateIn = True
              DataController.Filter.TranslateLike = True
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsBehavior.AlwaysShowEditor = True
              OptionsBehavior.FocusCellOnTab = True
              OptionsBehavior.GoToNextCellOnEnter = True
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsCustomize.DataRowSizing = True
              OptionsData.Deleting = False
              OptionsData.Inserting = False
              OptionsSelection.HideFocusRectOnExit = False
              OptionsSelection.MultiSelect = True
              OptionsView.CellAutoHeight = True
              OptionsView.GroupByBox = False
              OptionsView.BandHeaders = False
              Bands = <
                item
                end>
              ExtPopupMenu.AutoHotkeys = maManual
              ExtPopupMenu.CloseFootStr = False
              ExtPopupMenu.LinkTableView = Sheet1_Detail_TableView1
              ExtPopupMenu.AutoApplyBestFit = True
              ExtPopupMenu.AutoCreateAllItem = True
            end
            object Sheet1_Detail_Level1: TcxGridLevel
              GridView = Sheet1_Detail_TableView1
            end
          end
        end
        object cxTabSheet1: TcxTabSheet
          Caption = #26641#21015#21015#34920
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object ABcxDBTreeView2: TABcxDBTreeView
            Left = 0
            Top = 0
            Width = 573
            Height = 294
            Align = alClient
            Bands = <
              item
                Options.OnlyOwnColumns = True
              end>
            DataController.DataSource = ABDatasource1_1
            DataController.ParentField = 'Ti_ParentGuid'
            DataController.KeyField = 'Ti_Guid'
            DragMode = dmAutomatic
            LookAndFeel.Kind = lfFlat
            LookAndFeel.NativeStyle = False
            Navigator.Buttons.CustomButtons = <>
            OptionsBehavior.ExpandOnIncSearch = True
            OptionsBehavior.IncSearch = True
            OptionsBehavior.IncSearchItem = ABcxDBTreeView2cxDBTreeListColumn2
            OptionsData.Deleting = False
            OptionsSelection.CellSelect = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.Headers = False
            PopupMenu.DefaultParentValue = '0'
            RootValue = -1
            TabOrder = 0
            Active = False
            ExtFullExpand = False
            CanSelectParent = True
            object ABcxDBTreeView2cxDBTreeListColumn2: TcxDBTreeListColumn
              DataBinding.FieldName = 'Ti_Name'
              Width = 457
              Position.ColIndex = 0
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object ABcxDBTreeView2cxDBTreeListColumn1: TcxDBTreeListColumn
              Visible = False
              DataBinding.FieldName = 'Ti_Order'
              Position.ColIndex = 1
              Position.RowIndex = 0
              Position.BandIndex = 0
              SortOrder = soAscending
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
          end
        end
      end
      object Sheet1_Main_Panel1: TABDBPanel
        Left = 0
        Top = 0
        Width = 575
        Height = 161
        Align = alTop
        ShowCaption = False
        TabOrder = 1
        ReadOnly = False
        DataSource = ABDatasource1
        AddAnchors_akRight = True
        AddAnchors_akBottom = True
        AutoHeight = True
        AutoWidth = True
      end
      object Sheet1_Detail_Navigator1: TABDBNavigator
        Left = 0
        Top = 161
        Width = 575
        Height = 27
        Align = alTop
        BevelOuter = bvNone
        ShowCaption = False
        TabOrder = 2
        BigGlyph = False
        ImageLayout = blGlyphLeft
        DataSource = ABDatasource1_1
        VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
        ButtonRangeType = RtDetail
        BtnCustom1ImageIndex = -1
        BtnCustom2ImageIndex = -1
        BtnCustom3ImageIndex = -1
        BtnCustom4ImageIndex = -1
        BtnCustom5ImageIndex = -1
        BtnCustom6ImageIndex = -1
        BtnCustom7ImageIndex = -1
        BtnCustom8ImageIndex = -1
        BtnCustom9ImageIndex = -1
        BtnCustom10ImageIndex = -1
        BtnCustom1Caption = #33258#23450#20041'1'
        BtnCustom2Caption = #33258#23450#20041'2'
        BtnCustom3Caption = #33258#23450#20041'3'
        BtnCustom4Caption = #33258#23450#20041'4'
        BtnCustom5Caption = #33258#23450#20041'5'
        BtnCustom6Caption = #33258#23450#20041'6'
        BtnCustom7Caption = #33258#23450#20041'7'
        BtnCustom8Caption = #33258#23450#20041'8'
        BtnCustom9Caption = #33258#23450#20041'9'
        BtnCustom10Caption = #33258#23450#20041'10'
        BtnCustom1Kind = cxbkStandard
        BtnCustom2Kind = cxbkStandard
        BtnCustom3Kind = cxbkStandard
        BtnCustom4Kind = cxbkStandard
        BtnCustom5Kind = cxbkStandard
        BtnCustom6Kind = cxbkStandard
        BtnCustom7Kind = cxbkStandard
        BtnCustom8Kind = cxbkStandard
        BtnCustom9Kind = cxbkStandard
        BtnCustom10Kind = cxbkStandard
        ApprovedRollbackButton = nbNull
        ApprovedCommitButton = nbNull
        ButtonControlType = ctSingle
      end
    end
  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    AfterScroll = ABQuery1AfterScroll
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select *  from ABSys_Org_Tree '
      'order by Tr_Order'
      ''
      '')
    SqlUpdateDatetime = 42074.709679004630000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'Main'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Tree')
    IndexListDefs = <
      item
        Name = 'IX_ABSys_Org_Tree'
        Fields = 'TR_Order'
      end
      item
        Name = 'IX_ABSys_Org_Tree_1'
        Fields = 'TR_Name'
        Options = [ixUnique]
      end
      item
        Name = 'IX_ABSys_Org_Tree_2'
        Fields = 'TR_Code'
        Options = [ixUnique]
      end
      item
        Name = 'PK_ABSys_Org_Tree'
        Fields = 'TR_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ABSys_Org_Tree')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 15
    Top = 190
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 51
    Top = 190
  end
  object ABQuery1_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    MasterFields = 'TR_Guid'
    DetailFields = 'Ti_TR_Guid'
    SQL.Strings = (
      'select *  from ABSys_Org_TreeItem '
      'where Ti_Tr_Guid=:Tr_Guid'
      'order by Ti_Order'
      ''
      '')
    SqlUpdateDatetime = 42074.709580810190000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_TreeItem')
    IndexListDefs = <
      item
        Name = 'IX_ABSys_Org_TreeItem'
        Fields = 'TI_Name;TI_TR_Guid'
        Options = [ixUnique]
      end
      item
        Name = 'IX_ABSys_Org_TreeItem_1'
        Fields = 'TI_Order;TI_ParentGuid;TI_TR_Guid'
      end
      item
        Name = 'IX_ABSys_Org_TreeItem_2'
        Fields = 'TI_Code;TI_TR_Guid'
        Options = [ixUnique]
      end
      item
        Name = 'PK_ABSys_Org_TreeItem'
        Fields = 'TI_Guid'
        Options = [ixPrimary, ixUnique]
      end>
    LoadTables.Strings = (
      'ABSys_Org_TreeItem')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 17
    Top = 226
    ParamData = <
      item
        Name = 'Tr_Guid'
        DataType = ftWideString
        ParamType = ptInput
        Size = 100
        Value = '{0010FE68-22E2-47EA-8239-DB5BF04E9E18}'
      end>
  end
  object ABDatasource1_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_1
    Left = 49
    Top = 226
  end
  object EditRepository: TcxEditRepository
    Left = 568
    Top = 304
  end
end
