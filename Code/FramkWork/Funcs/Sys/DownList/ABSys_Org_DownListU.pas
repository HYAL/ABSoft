unit ABSys_Org_DownListU;

interface

uses
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFramkWorkcxGridU,
  ABFramkWorkDBPanelU,
  ABFramkWorkDictionaryQueryU,
  ABFramkWorkDBNavigatorU,
  ABFramkWorkFuncFormU,
  ABFramkWorkControlU,
  ABFramkWorkQueryU,
  ABFramkWorkFuncU,
  ABFramkWorkTreeItemEditU,

  cxTLdxBarBuiltInMenu,
  cxLookAndFeels,
  cxEditRepositoryItems,
  cxButtons,
  cxLookAndFeelPainters,
  cxDBExtLookupComboBox,
  cxDBLookupEdit,
  cxLookupEdit,
  cxTextEdit,
  cxContainer,
  cxLabel,
  cxCalc,
  cxDropDownEdit,
  cxMemo,
  cxDBEdit,
  cxButtonEdit,
  cxTLData,
  cxDBTL,
  cxInplaceContainer,
  cxMaskEdit,
  cxTL,
  cxSplitter,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGrid,
  cxGridDBTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxClasses,
  cxGridLevel,
  cxDBData,
  cxEdit,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  cxDBNavigator,
  cxNavigator,
  cxControls,
  cxPC,
  dxBar,
  dxStatusBar,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,DB,
  DBClient,ExtCtrls,ComCtrls,StdCtrls,DBCtrls,Buttons,Menus, dxBarBuiltInMenu,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  cxCheckListBox, cxDBCheckListBox, Vcl.Grids, Vcl.DBGrids;

type
  TABSys_Org_DownListForm = class(TABFuncForm)
    ABDBStatusBar1: TABdxDBStatusBar;
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    Sheet1_Navigator1: TABDBNavigator;
    Sheet1_Panel1: TPanel;
    Sheet1_Grid1: TABcxGrid;
    Sheet1_Level1: TcxGridLevel;
    Sheet1_TableView1: TABcxGridDBBandedTableView;
    Sheet1_Splitter1: TABcxSplitter;
    Sheet1_Panel2: TPanel;
    Sheet1_PageControl2: TABcxPageControl;
    Sheet1_Main_Panel1: TABDBPanel;
    ABQuery1_1: TABQuery;
    Sheet1_Detail_Sheet1: TcxTabSheet;
    Sheet1_Detail_Grid1: TABcxGrid;
    Sheet1_Detail_TableView1: TABcxGridDBBandedTableView;
    Sheet1_Detail_Level1: TcxGridLevel;
    ABDatasource1_1: TABDatasource;
    cxTabSheet1: TcxTabSheet;
    ABcxDBTreeView2: TABcxDBTreeView;
    ABcxDBTreeView2cxDBTreeListColumn2: TcxDBTreeListColumn;
    ABcxDBTreeView2cxDBTreeListColumn1: TcxDBTreeListColumn;
    EditRepository: TcxEditRepository;
    Sheet1_Detail_Navigator1: TABDBNavigator;
    procedure FormCreate(Sender: TObject);
    procedure ABQuery1AfterScroll(DataSet: TDataSet);
  private
    FTi_bit1Visibled,FTi_bit2Visibled,FTi_Varchar1Visibled,FTi_Varchar2Visibled:boolean;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Org_DownListForm: TABSys_Org_DownListForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Org_DownListForm.ClassName;
end;

exports
   ABRegister ;                     

procedure TABSys_Org_DownListForm.ABQuery1AfterScroll(
  DataSet: TDataSet);
begin
  ABViewDownItemByMain(ABQuery1.FieldByName('Tr_Code').AsString,ABQuery1_1,
                         FTi_Varchar1Visibled,FTi_Varchar2Visibled,FTi_bit1Visibled,FTi_bit2Visibled,
                         EditRepository,
                         Sheet1_Detail_TableView1);
end;

procedure TABSys_Org_DownListForm.FormCreate(Sender: TObject);
begin
  Sheet1_PageControl2.ActivePageIndex:=0;
  ABInitFormDataSet([Sheet1_Main_Panel1],[Sheet1_TableView1],ABQuery1);
  ABInitFormDataSet([],[Sheet1_Detail_TableView1],ABQuery1_1);

  FTi_Varchar1Visibled:=ABStrToBool(VarToStrDef(ABGetColumnProperty(Sheet1_Detail_TableView1,'Ti_Varchar1','Visible'),'False'));
  FTi_Varchar2Visibled:=ABStrToBool(VarToStrDef(ABGetColumnProperty(Sheet1_Detail_TableView1,'Ti_Varchar2','Visible'),'False'));
  FTi_bit1Visibled    :=ABStrToBool(VarToStrDef(ABGetColumnProperty(Sheet1_Detail_TableView1,'Ti_bit1'    ,'Visible'),'False'));
  FTi_bit2Visibled    :=ABStrToBool(VarToStrDef(ABGetColumnProperty(Sheet1_Detail_TableView1,'Ti_bit2'    ,'Visible'),'False'));

  ABQuery1AfterScroll(ABQuery1);
end;

Initialization
  RegisterClass(TABSys_Org_DownListForm);

Finalization
  UnRegisterClass(TABSys_Org_DownListForm);

end.
