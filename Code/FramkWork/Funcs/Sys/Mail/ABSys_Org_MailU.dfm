object ABSys_Org_MailForm: TABSys_Org_MailForm
  Left = 469
  Top = 134
  Caption = #37038#20214#31649#29702
  ClientHeight = 550
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TABcxSplitter
    Left = 185
    Top = 26
    Width = 8
    Height = 505
    HotZoneClassName = 'TcxMediaPlayer8Style'
    InvertDirection = True
    Control = mydxNavBar
  end
  object ABDBStatusBar1: TABdxDBStatusBar
    Left = 0
    Top = 531
    Width = 750
    Height = 19
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 50
      end>
    PaintStyle = stpsUseLookAndFeel
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    StopUpdate = False
    DataSource = ABDatasource1
  end
  object Panel1: TPanel
    Left = 193
    Top = 26
    Width = 557
    Height = 505
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Splitter2: TABcxSplitter
      Left = 0
      Top = 266
      Width = 557
      Height = 8
      Cursor = crVSplit
      HotZoneClassName = 'TcxMediaPlayer8Style'
      AlignSplitter = salTop
      InvertDirection = True
      Control = ABDBcxGrid1
    end
    object ABDBcxGrid1: TABcxGrid
      Left = 0
      Top = 0
      Width = 557
      Height = 266
      Align = alTop
      TabOrder = 0
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
      object ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
        PopupMenu.AutoHotkeys = maManual
        PopupMenu.CloseFootStr = False
        PopupMenu.LinkTableView = ABcxGridDBBandedTableView1
        PopupMenu.AutoApplyBestFit = True
        PopupMenu.AutoCreateAllItem = True
        OnDblClick = ABcxGridDBBandedTableView1DblClick
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = ABDatasource1
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Filter.AutoDataSetFilter = True
        DataController.Filter.TranslateBetween = True
        DataController.Filter.TranslateIn = True
        DataController.Filter.TranslateLike = True
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.AlwaysShowEditor = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.DataRowSizing = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.MultiSelect = True
        OptionsView.CellAutoHeight = True
        OptionsView.GroupByBox = False
        OptionsView.BandHeaders = False
        Bands = <
          item
          end>
        ExtPopupMenu.AutoHotkeys = maManual
        ExtPopupMenu.CloseFootStr = False
        ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView1
        ExtPopupMenu.AutoApplyBestFit = True
        ExtPopupMenu.AutoCreateAllItem = True
      end
      object cxGridLevel1: TcxGridLevel
        GridView = ABcxGridDBBandedTableView1
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 274
      Width = 557
      Height = 231
      Align = alClient
      BevelOuter = bvNone
      Locked = True
      TabOrder = 1
      object Splitter3: TABcxSplitter
        Left = 422
        Top = 0
        Width = 8
        Height = 231
        HotZoneClassName = 'TcxMediaPlayer8Style'
        AlignSplitter = salRight
        InvertDirection = True
        Control = Panel3
      end
      object Panel3: TPanel
        Left = 430
        Top = 0
        Width = 127
        Height = 231
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 0
          Top = 6
          Width = 27
          Height = 13
          Align = alTop
          Caption = ' '#38468#20214
          Transparent = True
        end
        object Label2: TLabel
          Left = 0
          Top = 0
          Width = 127
          Height = 6
          Align = alTop
          AutoSize = False
          Transparent = True
        end
        object ABDBcxGrid2: TABcxGrid
          Left = 0
          Top = 19
          Width = 127
          Height = 187
          Align = alClient
          TabOrder = 0
          LookAndFeel.Kind = lfFlat
          LookAndFeel.NativeStyle = False
          object ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView
            PopupMenu.AutoHotkeys = maManual
            PopupMenu.CloseFootStr = False
            PopupMenu.LinkTableView = ABcxGridDBBandedTableView2
            PopupMenu.AutoApplyBestFit = True
            PopupMenu.AutoCreateAllItem = True
            Navigator.Buttons.CustomButtons = <>
            OnCellDblClick = ABcxGridDBBandedTableView2CellDblClick
            DataController.DataSource = ABDatasource1_1
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Filter.AutoDataSetFilter = True
            DataController.Filter.TranslateBetween = True
            DataController.Filter.TranslateIn = True
            DataController.Filter.TranslateLike = True
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.AlwaysShowEditor = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.DataRowSizing = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsSelection.MultiSelect = True
            OptionsView.CellAutoHeight = True
            OptionsView.GroupByBox = False
            OptionsView.BandHeaders = False
            Bands = <
              item
              end>
            ExtPopupMenu.AutoHotkeys = maManual
            ExtPopupMenu.CloseFootStr = False
            ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView2
            ExtPopupMenu.AutoApplyBestFit = True
            ExtPopupMenu.AutoCreateAllItem = True
          end
          object cxGridLevel2: TcxGridLevel
            GridView = ABcxGridDBBandedTableView2
          end
        end
        object Panel4: TPanel
          Left = 0
          Top = 206
          Width = 127
          Height = 25
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object SpeedButton1: TABcxButton
            Left = 4
            Top = 1
            Width = 60
            Height = 22
            Caption = #22686#21152#38468#20214
            LookAndFeel.Kind = lfFlat
            TabOrder = 0
            OnClick = SpeedButton1Click
            ShowProgressBar = False
          end
          object SpeedButton2: TABcxButton
            Left = 65
            Top = 1
            Width = 60
            Height = 22
            Caption = #21024#38500#38468#20214
            LookAndFeel.Kind = lfFlat
            TabOrder = 1
            OnClick = SpeedButton2Click
            ShowProgressBar = False
          end
        end
      end
      object ABcxRichEdit1: TABcxRichEdit
        Left = 0
        Top = 0
        Align = alClient
        Lines.Strings = (
          'RichEdit1')
        TabOrder = 2
        OnExit = ABcxRichEdit1Exit
        Height = 231
        Width = 422
      end
    end
  end
  object mydxNavBar: TdxNavBar
    Left = 0
    Top = 26
    Width = 185
    Height = 505
    Align = alLeft
    ActiveGroupIndex = 0
    TabOrder = 7
    LookAndFeelSchemes.Flat = 8
    LookAndFeelSchemes.Standard = 8
    LookAndFeelSchemes.UltraFlat = 9
    LookAndFeelSchemes.Native = 18
    LookAndFeelSchemes.Office11 = 16
    LookAndFeelSchemes.Skin = 14
    View = 10
    OptionsBehavior.Common.DragDropFlags = []
    OptionsBehavior.Common.ShowGroupsHint = True
    OptionsBehavior.Common.ShowLinksHint = True
    OptionsStyle.CustomStyles.Background = stGroup3Background
    OptionsView.NavigationPane.MaxVisibleGroups = 0
    object Group1: TdxNavBarGroup
      Caption = #37038#20214#30446#24405
      LargeImageIndex = 1
      SelectedLinkIndex = -1
      TopVisibleLinkIndex = 0
      Links = <>
    end
    object stBackground: TdxNavBarStyleItem
      Style.AlphaBlending = 120
      Style.AlphaBlending2 = 120
      Style.BackColor = clNavy
      Style.BackColor2 = clNavy
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.Image.Data = {
        0A544A504547496D6167652E0B0000FFD8FFE000104A46494600010101012C01
        2C0000FFDB004300030202020202030202020303030304060404040404080606
        050609080A0A090809090A0C0F0C0A0B0E0B09090D110D0E0F101011100A0C12
        131210130F101010FFDB00430103030304030408040408100B090B1010101010
        1010101010101010101010101010101010101010101010101010101010101010
        10101010101010101010101010FFC00011080060006003012200021101031101
        FFC4001F0000010501010101010100000000000000000102030405060708090A
        0BFFC400B5100002010303020403050504040000017D01020300041105122131
        410613516107227114328191A1082342B1C11552D1F02433627282090A161718
        191A25262728292A3435363738393A434445464748494A535455565758595A63
        6465666768696A737475767778797A838485868788898A92939495969798999A
        A2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6
        D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301
        01010101010101010000000000000102030405060708090A0BFFC400B5110002
        0102040403040705040400010277000102031104052131061241510761711322
        328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728
        292A35363738393A434445464748494A535455565758595A636465666768696A
        737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7
        A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3
        E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00FA2D
        E101B8EC4F248FF3EA291622080463D455D540FD4679E4FA73D734EF23728206
        7A0C1FC7007E5EFDEBF6EF6DD19F0C5781363A904F519C752339FE86B4ED6405
        7CB6180C71903DBFFAD555632400013CF6F73E9DCD4F126D6C00719E80F5E9FE
        73EBE9D6B0AB2531ADCCFD6F4B5909914707DBE99FF3ED5C56ABA5B2B19514F0
        7E6079AF4F389E2DBFC43079FF003DEB9DD4B4F19394200FCC7EB5D982C5CA9B
        E562944E434E9A5B4937213D72403FE73DBF315E85A2EAE935B2AB1E83039FAF
        3FCBF4AE0EF2D1ADD8C9180320678C73D377F8FF009CDAD2EFDE17C2B75ED9C0
        3FFD7FF3F5EDC6518E2A1CC888BE567A235E26321BB67A71D2982FD795208603
        A63935CD0D4D08E5CE7A671CFF009FF0A6BDF28E03F55E3D0F26BC95827D4D39
        8DF9AE5589DADDB8E7EBFF00D6FF003C8A13246C7383C9CE0F4C7D7F0FF3D0D0
        8350490FCCDDF182727BF356E3989390DCE32491C743D7FCF6AB545D2D82F719
        6B77130E4E403D7D7EBDEAFC53A38C3375C6EFA73CF1D7FC6B8C99751D314198
        33460F3220E3EA7B8EB4B16BCE464BE4F2320633D7DABAE78273F7A0C9E6B6E7
        72855B9638C64127A83EF9FC7F2A9D630C37281C9CF27A73D3F9FF002AE62CF5
        B561B84A307DFAF07FA56C41A9472261BAE49E9D0E727F97F9EDE7D5C3CE1D0A
        4D1A4AAEBC6338E873FE7FCFAD54BF852505CF71FDD1DBBFEB5616E11F956C8E
        464631FE7A5366191BD4820F5078FA7F5FCF9E2B08DE32B8DD8E4F53B50771C7
        7C13FCAB03C89525C463F32074E3FCE3DFDABB2BF8F78DAC4F2319C7E9FA5739
        B3CBBD96323D0FD3FCF3FAD7B986AADC0CDAD445B5BD75C46548230724F39FC2
        A39E1D4E142CF6AE40EA54EE00FE1F526BA0B389485407232383C77E7FCFD6B5
        ED6D1480CA382324671F9E2B39E2FD93D55C394F37FED092294E410C0E769183
        FA8FF39AD9D3B598E550ACD9F98E09E33FE735D55FF876CEF9017811B2464150
        703BFF00915CDDE783C40E5ED649212483B73B9476FAF7AD638BC3E2159E8C39
        5A3A2D4AC9543653A64118E31FE73FAD701E20D31F4F91AEACD4F947964CFDDF
        715EAB7917989F2FDE1823FC2B94D66D54C6C00CAF6CF71E95C997629C24932A
        48E22C35421830624672464679E3FCFAE2BA4D3B520CB8C8393D33DFDAB85D46
        33A65F18D73E539DC9EC71D3A71D456AE9F780B67771C8F5CFA7D7B1FA57BF88
        C346A479E3D4CF6D4F43B7BB38CEFCAB77AD149CB719CE411C73D7A7F9FF000A
        E534EBD2CA0B36431EA7D2B7ADE42C36B3E4F606BE7B114795DCD13D09AE53CC
        527764E4F41FA7D3FC2B9AD4D4DBDE2CC4604836F4EE39FF001FD6BAB8E12E40
        5270480707B647FF00AEB2B56D3D6E6DCA1E013B81FEE900629616AA8CB96427
        7B0CD3E65644218E0601AE86C5C346A31EA4633C7B7E79AF3FB1BE92D6536D71
        B95E3F9483D78FC3A1AEAF4CD44100EF1C738FA76FD0FF0091578CC3C96A8716
        7488028CE73E94D9A159136B0CFB7E9490CAAE9B81E3D0F1529E2BC6778B34D1
        A2B4C4056C8E3A74CD616AAA8C84AB70411C9EC467D7EB57A3BD127258673907
        B719FF003FE4550BC62C021CE36E3EB5DD422E33D487A9E6DE2EB43244EC83E7
        8FE703BFBFE86B1749BC66C066EADEBF5FEBFD2BADD7E30EA496E7A7046071FF
        00D6AE06D8B5BDD3C44636B95C7E38C7E55F69837ED2872B32F53D0749B924AE
        E396EF8EFEFF00AFE95D8E9C4C9805B3C678C75C807FC8AF3ED1A70480727249
        CE4F2304FF005AF40D0E4C95C9CF183EDCE39FC0D78598C392ED151EC741042C
        501279E0E739A6DF590914B04E718391FE3F43F9D6A58DB87018007838078AB5
        756398C3A633D86735F30F11CB3D0E8E4D0F2BF1068AD3FEFA0CACEB82A7D476
        CD6158EB72DACA61B805248F86078F5F6FF23F0AF4DD56CB20B2FF00224E7BFF
        009FA5709E23D060BB52E54248A70AEB8DCBEBF5E95F4981C542B4153ABB7E47
        3CA36773A1D1B5D8DF1F3E49C039E33D6BA68AE1255CAB647435E1706A57DA35
        C9B6BBDDD72ACB8C3F53C71EFF008715DE683E245986D77EF83CFA673CFE1FE7
        8A8C7E56E1FBC86C38CFB94F42F10C77310566F980CA903AF538FAFF009F5C6C
        4D701E2254E72B8007E95E776F6325A4DE7D9C840CEE287A03EA0F6AE96CEE6E
        9D156484E7BF4C71FE7F4AEDC4E1611973C1E84A9742AEAEE0E769EE0151DFFC
        E2BCFF0051510EA4CFB4624C13EF838FF3F5F6AEF3548E700978E4C63246DC8E
        99CFEBFE715C4EB56D234C2608C0F7C8E79E475FA7FF00ABA57A997D92B08DBD
        1AE03004B0C1E73D47BFF9FF00EBE3D17C3F3EEDA33D0E33DC7F9C5792E8D738
        60AC707823FC9FAFF9E95E8FE1AB9C14DD93C74FF3F4AE3CD68FBAD82DCF59D0
        DD4B0C9F7C74FA7E35BD2401E320B13C6187E5FE7F1AE6B4198EF42C4F404907
        FC3F1AEA9092B923B57E718BBC6A6877D3D558E6B53B2DA49553D3E6006715C7
        6B16214300060803AF3FFD7AF46BF4570632C7D87A1E2B93D5AD8156720E073C
        1AF43015DC5AB99548F53CA35ED3229D1A3963CA927EAB8F4FA57330DC5E68B3
        8472CD1871B64033D73819EC6BD1756B5DC72A06E1C71ED5CADDDA2306465CA9
        E0823B7F87FF005ABEE70788528724B5472B566692DA1046011D4673D3A7F4C5
        6859C601546EE718FF003F5AB26D887201FC3BD4D141B58301820919ED9E6BCE
        9D5E65A9562692C96EA1DAC012381EBEBFD0D72BAE68380C5572189C704F5E3D
        7FCE4D76F66E55C6148E7D339F6FD3F4A9751D3E2BAB732C4A58119619C91FE4
        7E95CF4B172C3D4B741B573C4A7B192CAE77C7C8CE78F4E3FC7F95753E1BD417
        721CFA123BE7AFA7B559D7746DC1860F39C77E3A67DFE87B7E35816AB359DCAE
        D5C632063FCFF9E7F1FA075238BA36EA46C7B87876F559108704AF1C576D05D2
        3A2FCF9E307BF6AF1DF0B6ADB590963EE077EBC57A15ADE1F2C10F8381FE7DEB
        E0B33C23854D4EAA5356366EE52492873D0E33DBF97FFAEB1EFE30E37A13D381
        8E6A6378A40504E07400F1FE71FCEAB4D2A9195200C6303D2B8A941C19A4ACD5
        8E3756B4C36C3C1EC481FE7A5735776792485F981C9C751CFF008FF2AEF75484
        480BA9CE70A47626B9EB9B60E72060F4E7BFB57D3612BBE5472B47FFD9}
      Style.AssignedValues = [savAlphaBlending, savAlphaBlending2, savBackColor, savBackColor2, savImage]
    end
    object stGroup1Background: TdxNavBarStyleItem
      Style.AlphaBlending = 10
      Style.AlphaBlending2 = 10
      Style.BackColor = clAqua
      Style.BackColor2 = clAqua
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.Image.Data = {
        0A544A504547496D6167653E090000FFD8FFE000104A46494600010202000000
        000000FFE100BC45786966000049492A00080000000600120103000100000001
        0000001A01050001000000560000001B010500010000005E0000002801030001
        0000000200000013020300010000000100000069870400010000006600000000
        0000004800000001000000480000000100000006000090070004000000303230
        3001910700040000000102030000A00700040000003031303101A00300010000
        000100000002A00400010000006C00000003A00400010000006C000000000000
        00FFFE001E4143442053797374656D73204469676974616C20496D6167696E67
        00FFDB00840007040506050407060506070707080A110B0A09090A150F100C11
        19161A1A181618181C1F28221C1D261E1818232F2326292A2D2D2D1B21313431
        2B34282C2D2B010B0B0B0F0D0F1E11111E402B242B4040404040404040404040
        4040404040404040404040404040404040404040404040404040404040404040
        40404040404040FFC4007D000002030101000000000000000000000003040102
        0500061000020103030303020505010100000000010203001121041231134151
        22617132812342A1C1D105335291B1F0E1010101010101000000000000000000
        0000010203000511000202020202030100000000000000000001021112213141
        03511322F0E1FFC0001108006C006C03012200021101031101FFDA000C030100
        02110311003F00F78AF7528C7838A674D282AA49B038F8349EB6E84C83041B35
        062D46C90A1202BE7E0D7BD8DA3C9BA34D9F6CAD1F17C836A3C532BAD89167FD
        2B2B513978438FEE29BFDE829AEC900E185ED47C76872A66BCCC191D18640B8C
        D29A798C52291F9707E2957D71963DEA6D2266C7BF9FD29787569249B51B1202
        57CDFB8A578DD6C1CD1B3AD002861C03716ED4177DF17554E579B78AAE9A51A8
        D2B46C72314B6867D93B4328E0DA851D1CDA1E8E6BA2C8BC5EC689225D883C36
        7FF948C1F873B42C7D2DC1F7EC69E86ED1588F52D0D50AD8AE952D2BC327D0E3
        04F63DAA610D1497719076B7C79A6248C37AFB0C9A249182C1CF0D86B773E6BB
        21A234EE158C6786C0A5E7D3FE29B10078F1559818D82116653834D4417529BF
        17E0E3BD1C6CE42D2AF5E3DE461C6D75F06B28C4FB1A31F5A936FB715E8E1840
        73191F57B77A4B59A5E96A377F962F5509ABA265132A390B023370323CD0A68B
        1BD4DD79F8AD0D5697A1389907A5B9038F7ABA40A2428C2E8F91EE2B5CD724E2
        60CACFA3D5ACA386B5FC1FFDFCD44B0BC1A93D2FEDB012C249EC7915B3A8FE9E
        2584C6E2E5706AB068C4BA25561EB84DD33DBB8ABF9519BF16FF007EFE96D2B7
        E224CA0ED71916E3CD13FA8695B7A6A1317B5F3FAD120D3EC42A07A5F23E69F8
        2312C263B03B7FE56129D3B46CA368CE6533224807A948BFC53F002CA1AD9AE4
        D3F49AD6C1CD35A68B6E00BED37B79159CA48A4802820588E39A374B7465706D
        C0A625D38570E32A706AF046549161E9C7C8ACDCCB4BA32B5D1178BA8065706B
        39925BDD18807915E926802B71E96C1A41E008E558641F15A43C84CA2C7058A9
        B1CAF36F15DAD884DA7EA0FA80BFF341493649EABE2EA41A3C2FB642848CF1FB
        1AC9A6B652F4670512C0D1919E47CD0611B936F0D19C7C78A6E788C32DD45876
        F6AA98C09F70FCD5AA64D14717DAC05EE2D530C61640CBF9B9A318894B8E79B5
        AAC8A31EFDE8C8EA29D01EA41F22A619363820641C8F34CB27A51C0E0D8FCD02
        78ED2061C30BD4A77A1A1878D587A73F994F915D01D8770BE391ED54D33978F6
        F75E3DC5577EC9083C1CD4D7426928529B4F1DAF500016FF0054945A828FD3BF
        1C51BAC59481C8FB5463455A0A4AB5E337CD28E232C448016183715DA892EBBC
        115C153520484ADF83C734A540C0B80DB5CF7166FD8D54B116173B862E3C55A3
        3752A4D526378B7822E306B40187B4D186C6707E683B6D75E08CD0F4BA904EC6
        38716BDFBD4CF285B1382B86FE6BA9A741639A7652012059858FB1AA18B6395B
        5BBADBB1A5A39C262F86FF00B4679838B939E6F538B4C53185716CE7773F3409
        872BF707F6AA75AE2D623C553ADD58CDBEA4CF1DA951A3AC9898A306071FFAF5
        DAE5DB675FA6F42864572541C1E3E6998C2CF0989ADC5A97A761C8B4E4BC3D45
        C95A9867255645383823C1A0C0FD390C5276C1F715D10E9CB2444DD4E455D058
        E31BDC0E0E452ACCF19B2DEDCE28B7361B8F15D7B773F6142D0BD95864B5C137
        2BCFB8A991C2B9C828C2D404BA905B91E93F145281C18DBB70696810848C6295
        92E6EB9BD32D27574FD41CB0B11EF42962258330B11E96A3E961319284E0F154
        DAE495E84E19BA88509F50CA9ABE9F5659587E64E47B544FA56D36AEC07A4E45
        46A34DD1D42CF1DECD83FBD56982B1CEA82AB22E4707E296794C1A8575C8C0A3
        41104664394617FB1A8780B26D61722A743B2B25D25210FA5BD687F6A6639ACC
        1C1C3679EF41084C0A7BA1B8FE2AB21E91C0BAB0B8B763455E843EBD01612273
        8341770CAAE2F71827DA8904BD584A7E65C8BF714BCA446E42E55F22B92E818E
        03BAE2F922FF006A109802430C836355D3497502F94FD45125D3A4ADBB3C79AE
        E0A0A1431B8E78AE78C90ACBDAABA79376D71C1C1F9A682AEE2A00DAE2E3E7B8
        A86E841491075128B7F8B7B783468610C9C650DBED5DA5203BC4DF4B77F07B51
        50F4DB238C30F6A86DF0276AF4825D3EF51764C8F34A88049118D80BF22FE6B4
        A19806D848B1F2697D4811C8586339A98C9F02D09C5102B63820F8AE91369E3B
        77A36E5EA7B371F35D38DD15C1F50AD2F6005105FE7FED0258C32321191EA5FE
        28A1F38EF9F835139BD980C8E40AA5C93D19514E6298124DD4907DC531AA50C8
        429E3D687DBC503571033EE53E971FAD33A506483A6DF52F07DAB57EC85E80C4
        F60241DB91ED4FC2C4C7E960076CD21A7B26A0A3707B511FA9139456C03E2892
        B390487F0A6688F0D81FB1A7124BA589C838A4A6CA46E79BDA9A4CD8F9C54491
        6831607D42E08E7E28AD25D949C86C1A52324AADE8DCC19F6A868A456598A395
        273D8D19B502540C72186D61E293D7922147BE76F355D3312645271B6F6F7A71
        4D58592642095E197BD1A2981CE73823DE959B12A11F9867FDD56E5242171DEA
        B1B26C2BFA703826E3DAA0C9B1AE786E6FE6AC4EEC1EC7140D401B0FB6294730
        7A90036C1C1F521FDA890595D645186FD0F7A1487768779E55B157D37D130F1E
        A1F355D016FEA70ED759D0E0E18F8F14549159012336F145203E89830B8CD66E
        E28001C5AA63F6542F5B3FFFD9}
      Style.AssignedValues = [savAlphaBlending, savAlphaBlending2, savBackColor, savBackColor2, savImage]
    end
    object stGroup2Background: TdxNavBarStyleItem
      Style.AlphaBlending = 90
      Style.AlphaBlending2 = 90
      Style.BackColor = clLime
      Style.BackColor2 = clLime
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.Image.Data = {
        0A544A504547496D6167653D050000FFD8FFE000104A46494600010201004800
        480000FFED01B850686F746F73686F7020332E30003842494D03E90000000000
        78000C0000004800480000000002F80248FFF6FFF2030E02562C02052803FC01
        00000001680168000000000ED80B680001010000640001000000010002000000
        01270F00010000000000000000000000000000300000000010000000000000FF
        CEFFBA0F460BAE0020010000000000000000000000000000003842494D03ED00
        0000000010004800000001000100480000000100013842494D03F30000000000
        0800000000000000003842494D271000000000000A0001000000000000000238
        42494D03F5000000000048002F66660001006C66660006000000000001002F66
        66000100A1999A0006000000000001003200000001005A000000060000000000
        01003500000001002D000000060000000000013842494D03F800000000007000
        00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF03E800000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF03E800000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF03E800000000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF03E800003842494D04060000000000020000FFEE00
        0E41646F626500648000000001FFDB008400120E0E0E100E151010151E131113
        1E231A15151A2322171717171722110C0C0C0C0C0C110C0C0C0C0C0C0C0C0C0C
        0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C011413131619161B17171B140E0E
        0E14140E0E0E0E14110C0C0C0C0C11110C0C0C0C0C0C110C0C0C0C0C0C0C0C0C
        0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0CFFC00011080060006003012200
        021101031101FFC4013F00000105010101010101000000000000000300010204
        05060708090A0B01000105010101010101000000000000000100020304050607
        08090A0B1000010401030204020507060805030C330100021103042112310541
        51611322718132061491A1B14223241552C16233347282D14307259253F0E1F1
        63733516A2B283264493546445C2A3743617D255E265F2B384C3D375E3F34627
        94A485B495C4D4E4F4A5B5C5D5E5F55666768696A6B6C6D6E6F6374757677787
        97A7B7C7D7E7F711000202010204040304050607070605350100021103213112
        044151617122130532819114A1B14223C152D1F0332462E17282924353156373
        34F1250616A2B283072635C2D2449354A317644555367465E2F2B384C3D375E3
        F34694A485B495C4D4E4F4A5B5C5D5E5F55666768696A6B6C6D6E6F627374757
        67778797A7B7C7FFDD00040006FFDA000C03010002110311003F00DC2DDA7553
        6952B0680A8B4A6B024807E098B14C28BC9949484851E0CF0A64A705253212A6
        13049C1252CE8950923848A8A4A6533CA5B014ED088929FFD0E86C3A4283549E
        2545A9AC095A9DE34949A148C110925AE7C53029DE20A88496A66953E4421351
        35849288F8270D4DDD273925249012050654DA5253FFD1E82674526361411818
        6CA6B02890D0825E65273894394956C9CF94C028F7536849091A11070A2D5271
        8092509E540952712864A485D4DA4A1E888D494FFFD2E89A9DFC260533D35811
        3892A29CA41242C022B428008AD092590D06AA0F7CA9BC80D40494B12A254882
        9005242804568516B7BA281A2497FFD3DF6B948FB821351014D6060414B6A240
        E531494C07288DE10E754FBF4494AB1D27C90E52265209219028A02806A9F012
        4B2D02839E99CEEE844CA4A7FFD4DA0511A50A14834A6B5D38437730A4D91CA8
        38EA925895194E547B2285C44C29B538ABCD4856E412C9A149DC288D148EADD1
        25213E686511C0A1A487FFD9}
      Style.AssignedValues = [savAlphaBlending, savAlphaBlending2, savBackColor, savBackColor2, savImage]
    end
    object stGroup3Background: TdxNavBarStyleItem
      Style.AlphaBlending = 255
      Style.AlphaBlending2 = 255
      Style.BackColor = clSilver
      Style.BackColor2 = clSilver
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'MS Sans Serif'
      Style.Font.Style = []
      Style.AssignedValues = [savAlphaBlending, savAlphaBlending2, savBackColor, savBackColor2]
    end
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 40
    Top = 190
  end
  object ABDatasource1_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_1
    Left = 40
    Top = 228
  end
  object ABQuery1_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    MasterFields = 'Ma_Guid'
    DetailFields = 'Mf_Ma_Guid'
    SQL.Strings = (
      'select  '
      'GetNoBigDateFieldNames('#39'ABSys_Org_MailFile'#39') '
      'from ABSys_Org_MailFile '
      'where Mf_Ma_Guid=:Ma_Guid'
      'order by MF_MA_Guid,MF_Order'
      '')
    SqlUpdateDatetime = 42218.441652349540000000
    BeforeDeleteAsk = False
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_MailFile')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_MailFile')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 10
    Top = 228
    ParamData = <
      item
        Name = 'Ma_Guid'
        DataType = ftString
        ParamType = ptInput
      end>
  end
  object ABQuery2_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource2
    MasterFields = 'Ma_Guid'
    DetailFields = 'Mf_Ma_Guid'
    SQL.Strings = (
      'select  '
      'GetNoBigDateFieldNames('#39'ABSys_Org_MailFile'#39') '
      'from ABSys_Org_MailFile '
      'where Mf_Ma_Guid=:Ma_Guid')
    SqlUpdateDatetime = 42218.442088182870000000
    BeforeDeleteAsk = False
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_MailFile')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_MailFile')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 8
    Top = 344
    ParamData = <
      item
        Name = 'Ma_Guid'
        DataType = ftString
        ParamType = ptInput
      end>
  end
  object ABQuery2: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select *  from ABSys_Org_Mail '#13#10'where  1=2'#13#10)
    BeforeDeleteAsk = False
    BeforeDeleteAskMsg = #30830#23450#21024#38500#37038#20214#21527'?'
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Mail')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_Mail')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 8
    Top = 305
  end
  object ABDatasource2: TABDatasource
    AutoEdit = False
    DataSet = ABQuery2
    Left = 41
    Top = 306
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #23435#20307
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    PopupMenuLinks = <>
    Style = bmsUseLookAndFeel
    UseSystemFont = True
    Left = 632
    Top = 112
    DockControlHeights = (
      0
      0
      26
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 23
      FloatTop = 154
      FloatClientWidth = 100
      FloatClientHeight = 169
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton6'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton4'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarStatic1'
        end
        item
          Visible = True
          ItemName = 'dxbarComboBox1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton10'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = True
    end
    object dxBarButton1: TdxBarButton
      Caption = #25910#37038#20214
      Category = 0
      Hint = #25910#37038#20214
      Visible = ivAlways
      ImageIndex = 5
      OnClick = dxBarButton1Click
    end
    object dxBarButton2: TdxBarButton
      Caption = #21024#38500
      Category = 0
      Hint = #21024#38500
      Visible = ivAlways
      ImageIndex = 9
      OnClick = dxBarButton2Click
    end
    object dxBarButton3: TdxBarButton
      Caption = #21457#36865
      Category = 0
      Hint = #21457#36865
      Visible = ivAlways
      ImageIndex = 8
      OnClick = dxBarButton3Click
    end
    object dxBarButton4: TdxBarButton
      Caption = #20889#37038#20214
      Category = 0
      Hint = #20889#37038#20214
      Visible = ivAlways
      ImageIndex = 6
      OnClick = dxBarButton4Click
    end
    object dxBarButton5: TdxBarButton
      Caption = #20462#25913
      Category = 0
      Hint = #20462#25913
      Visible = ivAlways
      ImageIndex = 7
      OnClick = dxBarButton5Click
    end
    object dxBarButton6: TdxBarButton
      Caption = #22238#22797
      Category = 0
      Hint = #22238#22797
      Visible = ivAlways
      ImageIndex = 10
      OnClick = dxBarButton6Click
    end
    object dxBarButton7: TdxBarButton
      Caption = #36716#21457
      Category = 0
      Hint = #36716#21457
      Visible = ivAlways
      ImageIndex = 11
      OnClick = dxBarButton7Click
    end
    object dxBarButton8: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton9: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton10: TdxBarButton
      Caption = #36864#20986
      Category = 0
      Hint = #36864#20986
      Visible = ivAlways
      OnClick = dxBarButton10Click
    end
    object dxBarButton11: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
      ImageIndex = 12
    end
    object Rpt_ToPageEdit: TdxBarEdit
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Text = '0'
    end
    object dxbarComboBox1: TdxBarCombo
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      OnChange = dxbarComboBox1Change
      OnCurChange = dxbarComboBox1CurChange
      Text = '100'
      Items.Strings = (
        '25'
        '50'
        '75'
        '80'
        '90'
        '100'
        '150'
        '200')
      ItemIndex = 5
    end
    object dxBarStatic1: TdxBarStatic
      Caption = #36716#31227#21040
      Category = 0
      Hint = #36716#31227#21040
      Visible = ivAlways
    end
  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    AfterScroll = ABQuery1AfterScroll
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      'select '
      'GetNoBigDateFieldNames('#39'ABSys_Org_Mail'#39') '
      'from ABSys_Org_Mail '
      'where Ma_OwnerOperator = :ABUser_Code'
      'order by MA_SendOperator,MA_State,MA_SendDatetime')
    SqlUpdateDatetime = 42218.441178356480000000
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    ConnName = 'Main'
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Org_Mail')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Org_Mail')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 8
    Top = 192
    ParamData = <
      item
        Name = 'ABUser_Code'
        DataType = ftString
        ParamType = ptInput
        Value = 'admin'
      end>
  end
end
