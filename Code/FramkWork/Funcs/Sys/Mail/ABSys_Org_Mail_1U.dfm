object ABSys_Org_Mail_1Form: TABSys_Org_Mail_1Form
  Left = 248
  Top = 159
  Anchors = [akLeft, akTop, akRight]
  Caption = #32534#36753#37038#20214
  ClientHeight = 550
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  ExplicitWidth = 320
  PixelsPerInch = 96
  TextHeight = 13
  object dxStatusBar: TABdxStatusBar
    Left = 0
    Top = 531
    Width = 750
    Height = 19
    Images = ilStatusBarImages
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 130
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Fixed = False
        Width = 435
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Width = 130
      end>
    PaintStyle = stpsXP
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 750
    Height = 80
    Align = alTop
    TabOrder = 1
    DesignSize = (
      750
      80)
    object cxLabel1: TcxLabel
      Left = 8
      Top = 7
      Caption = #25910#20214#20154
      Transparent = True
    end
    object cxLabel3: TcxLabel
      Left = 8
      Top = 27
      Caption = #26631#39064
      Transparent = True
    end
    object cxMemo1: TcxMemo
      Left = 54
      Top = 27
      Anchors = [akLeft, akTop, akRight]
      Properties.OnChange = cxMemo1PropertiesChange
      TabOrder = 1
      Height = 49
      Width = 693
    end
    object ABcxButtonEdit1: TABcxButtonEdit
      Left = 54
      Top = 5
      Anchors = [akLeft, akTop, akRight]
      Properties.Buttons = <
        item
          Caption = #36873#25321
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = ABcxButtonEdit1PropertiesButtonClick
      Properties.OnChange = ABcxButtonEdit1PropertiesChange
      TabOrder = 0
      OnDblClick = ABcxButtonEdit1DblClick
      Width = 587
    end
    object ABcxButton1: TABcxButton
      Left = 647
      Top = 5
      Width = 99
      Height = 21
      Anchors = [akTop, akRight]
      Caption = #21152#20837#31995#32479#31649#29702#21592
      LookAndFeel.Kind = lfFlat
      TabOrder = 4
      OnClick = ABcxButton1Click
      ShowProgressBar = False
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 490
    Width = 750
    Height = 41
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      750
      41)
    object btn1: TABcxButton
      Left = 666
      Top = 6
      Width = 75
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #21462#28040
      LookAndFeel.Kind = lfFlat
      TabOrder = 0
      OnClick = btn1Click
      ShowProgressBar = False
    end
    object btn2: TABcxButton
      Left = 560
      Top = 6
      Width = 100
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #31435#21363#21457#36865
      LookAndFeel.Kind = lfFlat
      TabOrder = 1
      OnClick = btn2Click
      ShowProgressBar = False
    end
  end
  object Editor: TRichEdit
    Left = 0
    Top = 132
    Width = 750
    Height = 358
    Align = alClient
    Font.Charset = GB2312_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ImeName = #24555#20048#20116#31508
    ParentFont = False
    TabOrder = 7
    Zoom = 100
    OnChange = EditorChange
    OnMouseDown = EditorMouseDown
    OnSelectionChange = EditorSelectionChange
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #23435#20307
    Font.Style = []
    Categories.Strings = (
      'File'
      'Edit'
      'Format'
      'Style')
    Categories.ItemsVisibles = (
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      False)
    ImageOptions.DisabledLargeImages = ilDisabledImages
    ImageOptions.HotImages = ilHotImages
    ImageOptions.Images = Images
    ImageOptions.LargeImages = Images
    PopupMenuLinks = <>
    Style = bmsXP
    UseSystemFont = True
    Left = 14
    Top = 148
    DockControlHeights = (
      0
      0
      52
      0)
    object BarManagerBar2: TdxBar
      Caption = 'Standard'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 122
      FloatTop = 175
      FloatClientWidth = 150
      FloatClientHeight = 50
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButtonSave'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButtonPrint'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButtonCut'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonCopy'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonPaste'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButtonUndo'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonClear'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonFind'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonReplace'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 102
          Visible = True
          ItemName = 'dxBarCombo1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object BarManagerBar3: TdxBar
      Caption = 'Format'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 427
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 225
      FloatTop = 262
      FloatClientWidth = 248
      FloatClientHeight = 47
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButtonBold'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonItalic'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonUnderline'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButtonAlignLeft'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonCenter'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonAlignRight'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButtonBullets'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonProtected'
        end>
      OldName = 'Format'
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object BarManagerBar4: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      Caption = 'Bars Style'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 296
      DockedTop = 26
      DockingStyle = dsTop
      FloatLeft = 646
      FloatTop = 370
      FloatClientWidth = 123
      FloatClientHeight = 72
      Hidden = True
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButtonStdStyle'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonEnhancedStyle'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonFlatStyle'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonXPStyle'
        end
        item
          Visible = True
          ItemName = 'dxBarButtonOffice11Style'
        end>
      OldName = 'Style'
      OneOnRow = False
      Row = 1
      UseOwnFont = False
      UseRecentItems = False
      Visible = True
      WholeRow = False
    end
    object BarManagerBar5: TdxBar
      Caption = 'Font and Colors'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 26
      DockingStyle = dsTop
      FloatLeft = 585
      FloatTop = 329
      FloatClientWidth = 160
      FloatClientHeight = 60
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButtonFont'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 97
          Visible = True
          ItemName = 'dxBarComboFontName'
        end
        item
          Visible = True
          ItemName = 'dxBarComboFontSize'
        end
        item
          Visible = True
          ItemName = 'dxBarComboFontColor'
        end>
      OldName = 'Font and Colors'
      OneOnRow = False
      Row = 1
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarButtonPrint: TdxBarLargeButton
      Caption = #25171#21360'...'
      Category = 0
      Hint = #25171#21360
      Visible = ivAlways
      LargeImageIndex = 6
      OnClick = dxBarButtonPrintClick
      AutoGrayScale = False
      HotImageIndex = 6
      ShowCaption = False
    end
    object dxBarButtonExit: TdxBarLargeButton
      Caption = #36864#20986
      Category = 0
      Hint = #36864#20986
      Visible = ivAlways
      LargeImageIndex = 28
      ShortCut = 32856
      AutoGrayScale = False
      HotImageIndex = 28
    end
    object dxBarMRUFiles: TdxBarMRUListItem
      Caption = 'List of Recently Used Files'
      Category = 0
      Visible = ivAlways
      RemoveItemOnClick = True
    end
    object dxBarCombo1: TdxBarCombo
      Caption = #20248#20808#32423
      Category = 0
      Hint = #20248#20808#32423
      Visible = ivAlways
      ShowCaption = True
      Text = #19968#33324
      ItemIndex = -1
    end
    object dxBarButtonSave: TdxBarLargeButton
      Caption = #20445#23384
      Category = 0
      Hint = #20445#23384
      Visible = ivAlways
      LargeImageIndex = 5
      OnClick = dxBarButtonSaveClick
      AutoGrayScale = False
      HotImageIndex = 5
      ShowCaption = False
      SyncImageIndex = False
      ImageIndex = 5
    end
    object dxBarButtonUndo: TdxBarLargeButton
      Caption = #25764#28040
      Category = 1
      Hint = #25764#28040
      Visible = ivAlways
      LargeImageIndex = 7
      ShortCut = 16474
      OnClick = dxBarButtonUndoClick
      AutoGrayScale = False
      HotImageIndex = 7
      ShowCaption = False
    end
    object dxBarButtonCut: TdxBarLargeButton
      Caption = #21098#20999
      Category = 1
      Hint = #21098#20999
      Visible = ivAlways
      LargeImageIndex = 8
      ShortCut = 16472
      OnClick = dxBarButtonCutClick
      AutoGrayScale = False
      HotImageIndex = 8
      ShowCaption = False
    end
    object dxBarButtonCopy: TdxBarLargeButton
      Caption = #25335#36125
      Category = 1
      Hint = #25335#36125
      Visible = ivAlways
      LargeImageIndex = 9
      OnClick = dxBarButtonCopyClick
      AutoGrayScale = False
      HotImageIndex = 9
      ShowCaption = False
    end
    object dxBarButtonPaste: TdxBarLargeButton
      Caption = #31896#36148
      Category = 1
      Hint = #31896#36148
      Visible = ivAlways
      LargeImageIndex = 10
      OnClick = dxBarButtonPasteClick
      AutoGrayScale = False
      HotImageIndex = 10
      ShowCaption = False
    end
    object dxBarButtonClear: TdxBarLargeButton
      Caption = #28165#38500
      Category = 1
      Hint = #28165#38500
      Visible = ivAlways
      LargeImageIndex = 34
      OnClick = dxBarButtonClearClick
      AutoGrayScale = False
      HotImageIndex = 34
      ShowCaption = False
    end
    object dxBarButtonFind: TdxBarLargeButton
      Caption = #26597#25214
      Category = 1
      Hint = #26597#25214
      Visible = ivAlways
      LargeImageIndex = 12
      ShortCut = 16454
      OnClick = dxBarButtonFindClick
      AutoGrayScale = False
      HotImageIndex = 12
      ShowCaption = False
    end
    object dxBarButtonReplace: TdxBarLargeButton
      Caption = #26367#25442
      Category = 1
      Hint = #26367#25442
      Visible = ivAlways
      LargeImageIndex = 13
      ShortCut = 16456
      OnClick = dxBarButtonReplaceClick
      AutoGrayScale = False
      HotImageIndex = 13
      ShowCaption = False
    end
    object dxBarButtonBold: TdxBarLargeButton
      Caption = #31895#20307
      Category = 2
      Hint = #31895#20307
      Visible = ivAlways
      ButtonStyle = bsChecked
      LargeImageIndex = 14
      OnClick = dxBarButtonBoldClick
      AutoGrayScale = False
      HotImageIndex = 14
      ShowCaption = False
    end
    object dxBarButtonItalic: TdxBarLargeButton
      Caption = #26012#20307
      Category = 2
      Hint = #26012#20307
      Visible = ivAlways
      ButtonStyle = bsChecked
      LargeImageIndex = 15
      OnClick = dxBarButtonItalicClick
      AutoGrayScale = False
      HotImageIndex = 15
      ShowCaption = False
    end
    object dxBarButtonUnderline: TdxBarLargeButton
      Caption = #19979#21010#32447
      Category = 2
      Hint = #19979#21010#32447
      Visible = ivAlways
      ButtonStyle = bsChecked
      LargeImageIndex = 16
      OnClick = dxBarButtonUnderlineClick
      AutoGrayScale = False
      HotImageIndex = 16
      ShowCaption = False
    end
    object dxBarButtonBullets: TdxBarLargeButton
      Caption = #32534#21495
      Category = 2
      Hint = #32534#21495
      Visible = ivAlways
      ButtonStyle = bsChecked
      LargeImageIndex = 17
      OnClick = dxBarButtonBulletsClick
      AutoGrayScale = False
      HotImageIndex = 17
      ShowCaption = False
    end
    object dxBarButtonAlignLeft: TdxBarLargeButton
      Caption = #24038#23545#40784
      Category = 2
      Hint = #24038#23545#40784
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 1
      LargeImageIndex = 18
      ShortCut = 16460
      OnClick = dxBarButtonAlignLeftClick
      AutoGrayScale = False
      HotImageIndex = 18
      ShowCaption = False
    end
    object dxBarButtonCenter: TdxBarLargeButton
      Tag = 2
      Caption = #23621#20013
      Category = 2
      Hint = #23621#20013
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 1
      LargeImageIndex = 19
      ShortCut = 16453
      OnClick = dxBarButtonCenterClick
      AutoGrayScale = False
      HotImageIndex = 19
      ShowCaption = False
    end
    object dxBarButtonAlignRight: TdxBarLargeButton
      Tag = 1
      Caption = #21491#23545#40784
      Category = 2
      Hint = #21491#23545#40784
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 1
      LargeImageIndex = 20
      ShortCut = 16466
      OnClick = dxBarButtonAlignRightClick
      AutoGrayScale = False
      HotImageIndex = 20
      ShowCaption = False
    end
    object dxBarButtonProtected: TdxBarLargeButton
      Caption = #38145#23450
      Category = 2
      Hint = #38145#23450
      Visible = ivAlways
      ButtonStyle = bsChecked
      LargeImageIndex = 32
      OnClick = dxBarButtonProtectedClick
      AutoGrayScale = False
      HotImageIndex = 32
      ShowCaption = False
    end
    object dxBarButtonFont: TdxBarLargeButton
      Category = 2
      Visible = ivAlways
      LargeImageIndex = 21
      OnClick = dxBarButtonFontClick
      AutoGrayScale = False
      HotImageIndex = 21
      ShowCaption = False
    end
    object dxBarComboFontSize: TdxBarCombo
      Caption = #22823#23567
      Category = 2
      Hint = 'Font Size'
      Visible = ivAlways
      OnChange = dxBarComboFontSizeChange
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777777777777777777777777777777777770000777700000707780777777008
        7000770077778007770777800000008777077770077800777707777808700877
        7707777700800777770777778000877777077777700077777000777778087777
        7707777777808080808777777777777777777777777777777777}
      Width = 50
      DropDownCount = 12
      Items.Strings = (
        '8'
        '9'
        '10'
        '11'
        '12'
        '14'
        '16'
        '18'
        '20'
        '22'
        '24'
        '26'
        '28'
        '36'
        '48'
        '72')
      ItemIndex = -1
    end
    object dxBarComboFontColor: TdxBarColorCombo
      Caption = #39068#33394
      Category = 2
      Hint = #39068#33394
      Visible = ivAlways
      OnChange = dxBarComboFontColorChange
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        0800000000000001000000000000000000000001000000010000000000000000
        80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
        A6000020400000206000002080000020A0000020C0000020E000004000000040
        20000040400000406000004080000040A0000040C0000040E000006000000060
        20000060400000606000006080000060A0000060C0000060E000008000000080
        20000080400000806000008080000080A0000080C0000080E00000A0000000A0
        200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
        200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
        200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
        20004000400040006000400080004000A0004000C0004000E000402000004020
        20004020400040206000402080004020A0004020C0004020E000404000004040
        20004040400040406000404080004040A0004040C0004040E000406000004060
        20004060400040606000406080004060A0004060C0004060E000408000004080
        20004080400040806000408080004080A0004080C0004080E00040A0000040A0
        200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
        200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
        200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
        20008000400080006000800080008000A0008000C0008000E000802000008020
        20008020400080206000802080008020A0008020C0008020E000804000008040
        20008040400080406000804080008040A0008040C0008040E000806000008060
        20008060400080606000806080008060A0008060C0008060E000808000008080
        20008080400080806000808080008080A0008080C0008080E00080A0000080A0
        200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
        200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
        200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
        2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
        2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
        2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
        2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
        2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
        2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
        2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFF000101A6
        000202B4000404E4FFFFFFFF000101A6000202B4000404E4FFFFFFFF000EF9A7
        0030FABC00C0FCE4FFFFFFFF0000F9A70000FABC0000FCE4FFFFFFFF000001A7
        000002BC000004E4FFFFFFFF000001A7000002BC000004E4FFFFFFFF000001A7
        000002BC000004E4FFFFFFFF000001A7000002BC000004E4FFFFFFFF000001A7
        000002BC000004E4FFFFFFFF0000F9A70000FABC0000FCE4FFFFFFFF000EF9A7
        0030FABC00C0FCE4FFFFFFFF000101A6000202B4000404E4FFFFFFFF000101A6
        000202B4000404E4FFFFFFFF5201F9075202FA075204FC07FFFFFFFF070056FF
        070072FF0700D2FFFFFFFFFFFF5207FFFF5207FFFF5207FFFFFF}
      ShowEditor = True
      Color = clBlack
      ShowCustomColorButton = True
    end
    object dxBarComboFontName: TdxBarFontNameCombo
      Caption = #23383#27573
      Category = 2
      Hint = #23383#27573
      Visible = ivAlways
      OnChange = dxBarComboFontNameChange
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888444488844444488887488888744
        7888888448888744888888874888844788888888444444488888888874884478
        8888888884484488888888888744478888888888884448888888888888747888
        8888888888848888888888888888888888888888888888888888}
      Width = 160
      DropDownCount = 12
    end
    object dxBarButtonEnhancedStyle: TdxBarLargeButton
      Caption = 'Enhanced'
      Category = 3
      Hint = 'Enhanced Style'
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 2
      LargeImageIndex = 38
      PaintStyle = psCaption
      UnclickAfterDoing = False
      OnClick = dxBarButtonEnhancedStyleClick
      AutoGrayScale = False
      GlyphLayout = glLeft
      HotImageIndex = 38
    end
    object dxBarButtonFlatStyle: TdxBarLargeButton
      Caption = 'Flat'
      Category = 3
      Hint = 'Flat Style'
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 2
      LargeImageIndex = 39
      PaintStyle = psCaption
      UnclickAfterDoing = False
      OnClick = dxBarButtonFlatStyleClick
      AutoGrayScale = False
      GlyphLayout = glLeft
      HotImageIndex = 39
    end
    object dxBarButtonStdStyle: TdxBarLargeButton
      Caption = 'Standard'
      Category = 3
      Hint = 'Standard Style'
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 2
      LargeImageIndex = 37
      UnclickAfterDoing = False
      OnClick = dxBarButtonStdStyleClick
      AutoGrayScale = False
      GlyphLayout = glLeft
      HotImageIndex = 37
    end
    object dxBarButtonXPStyle: TdxBarLargeButton
      Caption = 'XP'
      Category = 3
      Hint = 'XP'
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 2
      Down = True
      LargeImageIndex = 40
      PaintStyle = psCaption
      UnclickAfterDoing = False
      OnClick = dxBarButtonXPStyleClick
      AutoGrayScale = False
      GlyphLayout = glRight
      HotImageIndex = 40
    end
    object dxBarButtonOffice11Style: TdxBarLargeButton
      Caption = 'Office11'
      Category = 3
      Hint = 'Office11'
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 2
      LargeImageIndex = 41
      PaintStyle = psCaption
      UnclickAfterDoing = False
      OnClick = dxBarButtonOffice11StyleClick
      AutoGrayScale = False
      GlyphLayout = glLeft
      HotImageIndex = 41
    end
    object dxBarGroup1: TdxBarGroup
      Items = (
        'dxBarButtonPrint'
        'dxBarButtonUndo'
        'dxBarButtonCut'
        'dxBarButtonCopy'
        'dxBarButtonPaste'
        'dxBarButtonClear'
        'dxBarButtonFind'
        'dxBarButtonReplace'
        'dxBarComboFontSize'
        'dxBarButtonBold'
        'dxBarButtonItalic'
        'dxBarButtonUnderline'
        'dxBarButtonBullets'
        'dxBarButtonAlignLeft'
        'dxBarButtonCenter'
        'dxBarButtonAlignRight'
        'dxBarButtonProtected'
        'dxBarButtonFont'
        'dxBarComboFontColor'
        'dxBarComboFontName')
    end
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'RTF'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist]
    Left = 48
    Top = 148
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'RTF'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist]
    Left = 84
    Top = 148
  end
  object PrintDialog: TPrintDialog
    Left = 116
    Top = 148
  end
  object FontDialog: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Left = 148
    Top = 149
  end
  object dxBarPopupMenu: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <
      item
        Visible = True
        ItemName = 'dxBarButtonCut'
      end
      item
        Visible = True
        ItemName = 'dxBarButtonCopy'
      end
      item
        Visible = True
        ItemName = 'dxBarButtonPaste'
      end
      item
        BeginGroup = True
        Visible = True
        ItemName = 'dxBarButtonFont'
      end
      item
        Visible = True
        ItemName = 'dxBarButtonBullets'
      end>
    UseOwnFont = False
    Left = 14
    Top = 179
  end
  object Images: TImageList
    Left = 264
    Top = 152
    Bitmap = {
      494C01012A002C00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000B0000000010020000000000000B0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000181818000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000313031000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000181818001818180000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000313031003130310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001818180018181800181818001818
      1800181818001818180018181800181818001818180018181800181818001818
      18001818180018181800181818001818180000000000C6C7C6006B696B006B69
      6B006B696B006B696B006B696B00313031003130310031303100525152005A59
      5A00636163006B696B006B696B006B696B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00DEDFDE00DEDFDE00DEDF
      DE00DEDFDE00D6D7D600D6D7D60018181800A5A6A500A5A6A500181818009496
      9400B5B6B500CECFCE00D6D7D600D6D7D600C6C7C600B5B6B500B5B6B500B5B6
      B500B5B6B500B5B6B500B5B6B50031303100ADAEAD00ADAEAD00313031008486
      84009C9E9C00ADAEAD00B5B6B500B5B6B5000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00A5A6A50018181800DEDF
      DE00DEDFDE00D6D7D6008486840084868400A5A6A500A5A6A500181818004241
      42004A494A004A494A005251520052515200BDBEBD00FFFFFF00FFFFFF00BDBE
      BD00BDBEBD0042414200424142008C8E8C00ADAEAD00ADAEAD00313031003938
      3900393839004241420042414200424142000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00CECFCE00A5A6A500DEDF
      DE00DEDFDE0094969400A5A6A500ADAEAD0084868400A5A6A500A5A6A5001818
      18007B797B009C9E9C00ADAEAD00B5B6B500BDBEBD004A494A00FFFFFF00BDBE
      BD00BDBEBD0042414200ADAEAD00A5A6A5008C8E8C00ADAEAD00ADAEAD003130
      31007B797B0094969400A5A6A500ADAEAD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00DEDFDE00DEDFDE00DEDF
      DE00DEDFDE0094969400BDBEBD00C6C7C60084868400A5A6A500A5A6A5001818
      18008486840094969400B5B6B500C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C60042414200ADAEAD00ADAEAD008C8E8C00ADAEAD00ADAEAD003130
      31007B797B008C8E8C009C9E9C00ADAEAD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00A5A6A50018181800DEDF
      DE00DEDFDE009C9E9C00DEDFDE00DEDFDE00D6D7D60084868400A5A6A500A5A6
      A5001818180094969400B5B6B500CECFCE00CECFCE00FFFFFF00FFFFFF00CECF
      CE00CECFCE0042414200B5B6B500B5B6B500B5B6B5008C8E8C00ADAEAD00ADAE
      AD0031303100848684009C9E9C00ADAEAD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00CECFCE00A5A6A500DEDF
      DE00DEDFDE009C9E9C00DEDFDE00E7E7E700E7E7E70084868400A5A6A500A5A6
      A500181818008C8E8C00ADAEAD00CECFCE00D6D7D6004A494A00FFFFFF00D6D7
      D600D6D7D60042414200BDBEBD00BDBEBD00BDBEBD008C8E8C00ADAEAD00ADAE
      AD00313031008486840094969400ADAEAD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00DEDFDE00DEDFDE00DEDF
      DE00DEDFDE00A5A6A500DEDFDE00E7E7E700EFEFEF00E7E7E70084868400A5A6
      A500A5A6A500181818009C9E9C00C6C7C600DEDFDE00DEDFDE00DEDFDE00DEDF
      DE00DEDFDE0042414200C6C7C600C6C7C600BDBEBD00BDBEBD008C8E8C00ADAE
      AD00ADAEAD00313031008C8E8C00A5A6A5000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00A5A6A50018181800DEDF
      DE00DEDFDE00A5A6A500D6D7D600E7E7E700EFEFEF00F7F7F70084868400A5A6
      A500A5A6A500181818009C9E9C00B5B6B500E7E7E700FFFFFF00FFFFFF00E7E7
      E700E7E7E70042414200C6C7C600C6C7C600C6C7C600C6C7C6008C8E8C00ADAE
      AD00ADAEAD00313031008C8E8C009C9E9C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00CECFCE00A5A6A500DEDF
      DE00DEDFDE00DEDFDE00A5A6A500A5A6A500A5A6A500A5A6A5009C9E9C008486
      8400A5A6A500A5A6A500181818006B696B00E7E7E7004A494A00FFFFFF00E7E7
      E700E7E7E7004241420042414200424142004241420042414200424142008C8E
      8C00ADAEAD00ADAEAD0031303100393839000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00DEDFDE00DEDFDE00DEDF
      DE00DEDFDE00DEDFDE00DEDFDE00DEDFDE00DEDFDE00D6D7D600D6D7D6008486
      8400A5A6A500181818001818180084868400C6C7C600EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF008C8E
      8C00ADAEAD0031303100313031009C9E9C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009496940094969400949694009496
      9400949694009496940094969400949694009496940094969400949694009496
      940084868400E7E7E700E7E7E7001818180000000000C6C7C600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7
      F7008C8E8C00DEDFDE00DEDFDE00313031000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084868400E7E7E700E7E7E700181818000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008C8E8C00DEDFDE00DEDFDE00313031000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008C8E8C008C8E8C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006B4129006B41
      29006B4129006B4129006B4129006B4129006B4129006B4129006B4129006B41
      29006B4129006B4129006B4129006B4129000000000000000000000000000000
      0000000000000000000000000000181818000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000181818000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000181818000000000000000000000000000000
      00000000000000000000000000000000000000000000CE716B00D6969400CE8E
      8C0073594A00C6B2AD00C6BEBD00CEC3BD00CEC3BD00CEC3BD00CEC3BD008C75
      63009441420094414200944142006B4129000000000000000000000000000000
      0000000000000000000000000000181818001818180000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000181818001818180000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000181818001818180000000000000000000000
      00000000000000000000000000000000000000000000CE716B00D69E9C00D696
      940073594A00291C18005A514200FFFFFF00FFFFFF00FFFFFF00F7E7DE008C75
      63009445420094414200944142006B4129001818180018181800181818001818
      1800181818001818180018181800181818001818180018181800181818001818
      1800181818001818180018181800181818001818180018181800181818001818
      1800181818001818180018181800181818001818180018181800181818001818
      1800181818001818180018181800181818001818180018181800181818001818
      1800181818001818180018181800181818001818180018181800181818001818
      18001818180018181800181818001818180000000000CE716B00DEAAA500D69E
      9C0073594A000000000031241800FFFFFF00F7F3EF00F7E7DE00E7CBBD008C75
      63009C514A0094454200944142006B412900FFFFFF00D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D600CECFCE00181818009C9E9C009C9E9C00181818008C8E
      8C00ADAEAD00C6C7C600D6D7D600D6D7D600FFFFFF00D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D600CECFCE00181818009C9E9C009C9E9C00181818008C8E
      8C00ADAEAD00C6C7C600D6D7D600D6D7D60094969400EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00181818009C9E9C009C9E9C0018181800A5A6
      A500C6C7C600DEDFDE00EFEFEF00EFEFEF0000000000CE716B00E7B2AD00DEAA
      A50073594A0073594A0073594A0073594A0073594A0073594A0073594A007359
      4A00A55952009C514A00944542006B412900FFFFFF00D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D600D6D7D600848684009C9E9C009C9E9C00181818001818
      180018181800181818001818180018181800FFFFFF00D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D60018181800848684009C9E9C009C9E9C00181818001818
      18001818180018181800181818001818180094969400EFEFEF00181818001818
      180018181800EFEFEF0018181800848684009C9E9C009C9E9C00181818001818
      18001818180018181800181818001818180000000000CE716B00EFBEBD00E7B6
      B500E7AEA500DEA69C00D69A9400CE8E8C00CE615A00B59294009C514A00B569
      6B00AD615A00A55952009C514A006B412900FFFFFF00D6D7D600181818001818
      18001818180018181800D6D7D600FFFFFF00848684009C9E9C009C9E9C001818
      18008C8E8C00ADAEAD00C6C7C600D6D7D600FFFFFF00D6D7D600181818001818
      180018181800D6D7D600FFFFFF00CECFCE00848684009C9E9C009C9E9C001818
      18008C8E8C00ADAEAD00C6C7C600D6D7D60094969400EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF008C8E8C00E7E7E700848684009C9E9C009C9E9C001818
      1800A5A6A500C6C7C600DEDFDE00EFEFEF0000000000CE716B00F7C3BD00EFBE
      BD00E7B6B500E7AEA500DEA69C00D69A9400CE615A00D6B6A5009C514A00BD79
      7300B5696B00AD615A00A55952006B412900FFFFFF00D6D7D600FFFFFF001818
      1800FFFFFF0018181800D6D7D600FFFFFF00848684009C9E9C009C9E9C001818
      1800848684009C9E9C00BDBEBD00CECFCE00FFFFFF00D6D7D600FFFFFF00D6D7
      D60018181800D6D7D600FFFFFF00D6D7D600848684009C9E9C009C9E9C001818
      1800848684009C9E9C00BDBEBD00CECFCE0094969400EFEFEF00181818001818
      180018181800EFEFEF008C8E8C00EFEFEF00848684009C9E9C009C9E9C001818
      180094969400B5B6B500D6D7D600E7E7E70000000000CE716B00F7C3BD00F7C3
      BD00D69A9400CE615A00CE615A00CE615A00B58E7B00D6BAAD00D6B6A5009C51
      4A009C514A00B5716B00AD6563006B412900FFFFFF00D6D7D600FFFFFF001818
      1800FFFFFF0018181800D6D7D600FFFFFF00CECFCE00848684009C9E9C009C9E
      9C00181818008C8E8C00ADAEAD00C6C7C600FFFFFF00D6D7D600FFFFFF00D6D7
      D60018181800D6D7D600FFFFFF00D6D7D600CECFCE00848684009C9E9C009C9E
      9C00181818008C8E8C00ADAEAD00C6C7C60094969400EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF0094969400EFEFEF00E7E7E700848684009C9E9C009C9E
      9C0018181800A5A6A500C6C7C600DEDFDE0000000000CE716B00F7C3BD00CEBA
      B500C68A7B00DEBEAD00E7CFBD00E7D3BD004A59DE009C96C600D6BAAD00D6B6
      A500A57163009C514A00B5716B006B412900FFFFFF00D6D7D600FFFFFF001818
      1800FFFFFF0018181800D6D7D600FFFFFF00D6D7D600848684009C9E9C009C9E
      9C0018181800848684009C9E9C00BDBEBD00FFFFFF00D6D7D600FFFFFF00D6D7
      D60018181800D6D7D600FFFFFF00D6D7D600D6D7D600848684009C9E9C009C9E
      9C0018181800848684009C9E9C00BDBEBD0094969400EFEFEF00181818001818
      180018181800EFEFEF0094969400EFEFEF00EFEFEF00848684009C9E9C009C9E
      9C001818180094969400B5B6B500D6D7D60000000000CE716B00F7C3BD00C68A
      7B00EFDFC600EFE3CE00EFE3CE00EFDBC600B5AEC600CEBABD00DEBEAD00D6B6
      A500CEA294009C6963009C514A006B412900FFFFFF00D6D7D600FFFFFF001818
      1800FFFFFF0018181800D6D7D600FFFFFF00D6D7D600CECFCE00848684009C9E
      9C009C9E9C00181818008C8E8C00ADAEAD00FFFFFF00D6D7D600FFFFFF00D6D7
      D60018181800D6D7D600FFFFFF00D6D7D600D6D7D600CECFCE00848684009C9E
      9C009C9E9C00181818008C8E8C00ADAEAD0094969400EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF0094969400EFEFEF00EFEFEF00E7E7E700848684009C9E
      9C009C9E9C0018181800A5A6A500C6C7C60000000000CE716B00C68A7B00E7CF
      BD00EFEBCE00F7EFD600EFEBCE00EFDFC6009496D6008C8ECE00DEBEAD00D6BA
      A500D6AE9C00BD8A84009C514A006B412900FFFFFF00D6D7D600FFFFFF001818
      1800FFFFFF0018181800D6D7D600FFFFFF00D6D7D600D6D7D600848684009C9E
      9C009C9E9C0018181800848684009C9E9C00FFFFFF00D6D7D600FFFFFF00FFFF
      FF0018181800D6D7D600FFFFFF00D6D7D600D6D7D600D6D7D600848684009C9E
      9C009C9E9C0018181800848684009C9E9C0094969400EFEFEF00181818001818
      180018181800EFEFEF0094969400EFEFEF00EFEFEF00EFEFEF00848684009C9E
      9C009C9E9C001818180094969400ADAEAD0000000000CE716B00C68A7B00E7D3
      BD00F7EBD600F7F3D600EFEBCE00EFDFC600CEC7C6003149E700B5A6BD00D6BA
      A500D6AE9C00BD8A84009C514A006B412900FFFFFF00D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D600D6D7D600FFFFFF00FFFFFF00FFFFFF00FFFFFF008486
      84009C9E9C009C9E9C0018181800ADAEAD00FFFFFF00D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008486
      84009C9E9C009C9E9C0018181800ADAEAD0094969400EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF009496940094969400949694008C8E8C008C8E8C008486
      84009C9E9C009C9E9C00181818006361630000000000CE716B00F7C3BD00C68A
      7B00EFE3C600EFE7CE00CECFCE001838EF00CEC3C6001834EF009C96BD00D6B6
      A500CEA29400AD716B009C514A006B412900FFFFFF00D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D600D6D7D600D6D7D600D6D7D600D6D7D600D6D7D6008486
      84009C9E9C00181818001818180084868400FFFFFF00D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D600D6D7D600D6D7D600D6D7D600D6D7D600D6D7D6008486
      84009C9E9C0018181800181818008486840094969400EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF008486
      84009C9E9C0018181800181818009496940000000000CE716B00F7C3BD00E7DB
      D600C68A7B00DEC3B500DECBBD007B82D6005A65DE006B6DCE00C6A6A500CE9E
      9400B5796B009C514A00000000006B412900FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0084868400E7E7E700E7E7E70018181800FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0084868400DEDFDE00DEDFDE00181818009496940094969400949694009496
      94009496940094969400949694009496940094969400949694008C8E8C008C8E
      8C0084868400E7E7E700E7E7E7001818180000000000CE716B00F7C3BD00CE61
      5A00E7CBC600C68A7B00C69A8C00C69A8C00C6968400C6928400C6928400CEA6
      9C009C514A00CE615A00CE8A84006B4129000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084868400E7E7E700E7E7E700181818000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084868400DEDFDE00DEDFDE00181818000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084868400E7E7E700E7E7E7001818180000000000CE716B00CE716B00CE71
      6B00CE716B00CE716B00CE716B00CE716B00CE716B00CE716B00CE716B00CE71
      6B00CE716B00CE716B00CE716B00CE716B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006B4531006B45
      31006B4531006B4531006B4531006B4531006B4531006B4531006B4531006B45
      31006B4531006B4531006B453100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000006194000061
      9400006194000061940000619400006194000061940000619400006194000061
      94000061940000000000000000000000000000000000C6AE9C00F7F3EF00B5A2
      9400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A2
      9400B5A29400000CB500B5A294006B4531000000000000000000000000000000
      0000A58E7B00846D5A00846D5A00633031006330310063303100633031006330
      3100633031006330310063303100000000000000000084929C00006194000061
      9400006194000061940000619400006194000061940000619400006194000061
      940000619400006194000061940000000000000000000092CE009CDBFF000092
      CE000092CE000092CE000092CE000092CE000092CE000092CE000092CE000092
      CE000092CE0000619400000000000000000000000000C6AE9C00F7F7F700F7F3
      EF00F7EFEF00EFEBE700EFE7DE00EFE3DE00E7DBD600E7D7D600E7D3CE00DECF
      C600DECBC6000018CE00B5A294006B4531000000000000000000A58E7B00A58E
      7B00B5A29400B5A29400B5A29400A58E7B006330310000000000000000000000
      0000000000000000000000000000000000000000000084929C008CF3FF000092
      CE000092CE000092CE000092CE000092CE000092CE000092CE000092CE000092
      CE000092CE000092CE000061940000000000000000000092CE00D6EFFF007BCF
      F70073CFF7006BCBF7005296AD004241420042869C004ABAF70042B6EF0042B6
      EF000092CE0000619400000000000000000000000000C6AE9C00FFFBF7009C30
      00009C300000F7EFEF009C3000009C300000EFE3DE009C3000009C3000000030
      FF00002CF7000020DE000018CE000014C60000000000A58E7B00B5A29400B5A2
      9400DED3CE00DED3CE00FFFBF700B5A29400A58E7B0063303100000000000000
      0000000000000000000000000000000000000000000084929C0094F7FF008CF3
      FF0084EFF70084EBF7007BE7F70073E3F7006BE3F7006BDFF70063DBF7005AD7
      F70052D3F7000092CE000061940000000000000000000092CE00D6EFFF0084D3
      FF007BD3F70073CFF70073CBF7004241420063C7F7005AC3F70052BEF7004ABA
      F7000092CE0000619400000000000000000000000000C6AE9C00FFFFFF00FFFB
      F700F7F7F700F7F3EF00F7EFEF00EFEBE700EFE7DE00EFE3DE00E7DBD600E7D7
      D6009C300000002CF700B5A294006B45310000000000B5A29400DED3CE00F7EF
      EF00EFE3DE00E7DBD600DED3CE00FFFFFF00B5A29400A58E7B00633031000000
      0000000000000000000000000000000000000000000084929C0094F7FF0094F7
      FF008CF3FF0084EFF70084EBF7007BE7F70073E3F7006BE3F7006BDFF70063DB
      F7005AD7F7000092CE000061940000000000000000000092CE00D6EFFF008CD7
      FF0084D3FF007BD3F70042414200424142004241420063C7F7005AC3F70052BE
      F7000092CE0000619400000000000000000000000000C6AE9C00FFFFFF009C30
      0000FFFBFF00E7CBBD00A54518009C300000B56542009C3000009C300000E7DB
      D600E7D7D6000030FF00B5A294006B45310000000000B5A29400FFFFFF00FFF7
      F700F7EFEF00EFE3DE00E7DBD600DED3CE00FFFFFF00B5A29400A58E7B006330
      3100000000000000000000000000000000000000000084929C0094F7FF0094F7
      FF0094F7FF008CF3FF0084EFF70084EBF7007BE7F70073E3F7006BE3F7006BDF
      F70063DBF7000092CE000061940000000000000000000092CE00D6EFFF008CDB
      FF008CD7FF0084D3FF0063AABD004241420063AABD006BCBF70063C7F7005AC3
      F7000092CE0000619400000000000000000000000000C6AE9C00FFFFFF009C30
      0000FFFFFF00B56D4A009C300000EFDBD600E7CFC6009C3000009C300000EFE3
      DE009C300000E7D7D600B5A294006B4531000000000000000000B5A29400FFFF
      FF00FFF7F700F7EFEF00EFE3DE00E7DBD600DED3CE00FFFFFF00B5A29400A58E
      7B00633031000000000000000000000000000000000084929C0094F7FF0094F7
      FF0094F7FF0094F7FF008CF3FF0084EFF70084EBF7007BE7F70073E3F7006BE3
      F7006BDFF7000092CE000061940000000000000000000092CE00D6EFFF0094DB
      FF008CDBFF008CD7FF0084D3FF007BD3F70073CFF70073CBF7006BCBF70063C7
      F7000092CE0000619400000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00BD795A009C300000DEB2A500F7E7E7009C3000009C300000EFE7
      E7009C300000EFDFDE00B5A294006B453100000000000000000000000000B5A2
      9400FFFFFF00FFF7F700F7EFEF00EFE3DE00E7DBD600DED3CE00FFFFFF00B5A2
      9400A58E7B006330310000000000000000000000000084929C0094F7FF0094F7
      FF0094F7FF0094F7FF0094F7FF008CF3FF008CF3FF0084EBF7007BE7F70073E3
      F7006BE3F7000092CE000061940000000000000000000092CE009CDBFF00D6EF
      FF00D6EFFF00D6EFFF00D6EFFF00D6EFFF00D6EFFF00D6EFFF00D6EFFF00D6EF
      FF009CDBFF0000619400000000000000000000000000C6AE9C00FFFFFF009C30
      0000FFFFFF00F7EFEF00C6866300AD5121009C3408009C3000009C300000F7EB
      E700EFE7E700EFE3DE00B5A294006B4531000000000000000000000000000000
      0000B5A29400FFFFFF00FFF7F700F7EFEF00EFE3DE00E7DBD600DED3CE00FFFF
      FF00B5A29400A58E7B006B453100000000000000000084929C0094F7FF0094F7
      FF0094F7FF0094F7FF0094F7FF0094F7FF008CF3FF008CF3FF0084EFF7007BEB
      F70073E7F7000092CE00006194000000000000000000000000000092CE000092
      CE000092CE000092CE000092CE000092CE000092CE000092CE000092CE000092
      CE000092CE0000000000000000000000000000000000C6AE9C00FFFFFF009C30
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFD7CE009C3000009C3C1000F7EF
      EF009C300000EFE7E700B5A294006B4531000000000000000000000000000000
      000000000000B5A29400FFFFFF00FFF7F700F7EFEF00EFE3DE00E7DBD600DED3
      CE00FFFFFF00B5A29400846D5A00000000000000000084929C0094F7FF0094F7
      FF0094F7FF0094F7FF0094F7FF0094F7FF0094F7FF008CF3FF008CF3FF0084EF
      F7007BEBF70073E7F70000619400000000000000000000000000000000000092
      CE00D6EFFF00006194000000000000000000000000000092CE0063CBFF000061
      94000000000000000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00A54518009C3000009C300000AD512100DEBAAD00F7F3
      EF009C300000F7EBE700B5A294006B4531000000000000000000000000000000
      00000000000000000000B5A29400FFFFFF00FFF7F700F7EFEF00EFE3DE00E7DB
      D600DED3CE00FFFFFF00846D5A00000000000000000084929C0084929C008492
      9C0084929C0084929C0084929C0084929C0084929C009C3000009C3000009C30
      00009C30000084929C0084929C00000000000000000000000000000000000092
      CE00D6EFFF00006194000000000000000000000000000092CE0063CBFF000061
      94000000000000000000000000000000000000000000C6AE9C00FFFFFF009C30
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFF7
      F700F7F3EF00F7EFEF00B5A294006B4531000000000000000000000000000000
      0000000000000000000000000000B5A29400FFFFFF00FFFBFF00F7F3EF00EFE7
      E700FFFBFF00DED3CE00A58E7B00000000000000000084929C0094F7FF0094F7
      FF0094F7FF0094F7FF0084929C0000000000000000009C300000E7A684009C30
      0000000000000000000000000000000000000000000000000000000000000092
      CE00D6EFFF009CDBFF0000619400006194000061940063CBFF0063CBFF000061
      94000000000000000000000000000000000000000000C6AE9C00FFFFFF009C30
      0000FFFFFF009C3000009C300000FFFFFF009C3000009C300000FFFFFF009C30
      00009C300000F7F7F700B5A294006B4531000000000000000000000000000000
      000000000000000000000000000000000000B5A29400FFFFFF00FFFBFF00FFFB
      FF00DED3CE00A58E7B000000000000000000000000000000000084929C008492
      9C0084929C0084929C000000000000000000000000009C3000009C300000BD71
      3900000000000000000000000000000000000000000000000000000000000000
      00000092CE00D6EFFF00D6EFFF00D6EFFF00D6EFFF0063CBFF00006194000000
      00000000000000000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFBF700F7F7F7006B4531000000000000000000000000000000
      00000000000000000000000000000000000000000000B5A29400B5A29400B5A2
      9400B5A294000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C300000000000009C30
      0000BD71390000000000BD7139009C3000000000000000000000000000000000
      0000000000000092CE000092CE000092CE000092CE000092CE00000000000000
      0000000000000000000000000000000000000000000000000000C6AE9C00C6AE
      9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE
      9C00C6AE9C00C6AE9C00C6AE9C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009C3000009C3000009C300000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C615A006B3031006B3031006B30310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000006B4531006B45310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C66510008C4118008C4118008C4118008C4118008C4118008C41
      18008C4118008C4118008C4118008C4118000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000946163006B30
      31006B303100A5555200AD595200A55952006B3031006B3031006B3031006B30
      31006B3031006B3031006B3031006B3031000000000000000000000000000000
      000000000000000000006B453100F7EFEF00F7EFEF006B453100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6651000FFFFFF00B5A29400B5A29400B5A29400B5A29400B5A2
      9400B5A29400B5A29400B5A294008C41180000000000C66510008C4118008C41
      18008C4118008C4118008C4118008C4118008C4118008C4118008C4118008C41
      18008C4118008C4118008C4118008C4118000000000094616300A5555200BD65
      5A00BD655A00B5615A00B5615A00A55D52006B303100F7929400F7929400F792
      9400F7929400F7929400F79294006B3031000000000000000000000000000000
      0000000000006B453100F7EFEF009C3000009C300000F7EFEF006B4531000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6651000FFFFFF00FFFFFF00FFFFFF00FFF7F700F7F3EF00EFEB
      E700EFE3DE00E7DBD600B5A294008C41180000000000C6651000FFF7F700B5A2
      9400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A2
      9400B5A29400B5A29400B5A294008C4118000000000094616300C6696300C669
      6300BD656300BD655A00B5615A00AD5D52006B30310000490000004900000049
      0000004D0000295D1000F79294006B3031000000000000000000000000000000
      00006B453100F7EFEF009C300000F7865200F77D42009C300000F7EFEF006B45
      310000000000000000000000000000000000000000000000000000000000C665
      10008C411800C6651000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7F700F7F3
      EF00EFEBE700EFE3DE00B5A294008C41180000000000C6651000FFFBFF00FFF7
      F700F7F3EF00F7EBEF00EFE7E700EFE3DE00EFDFD600E7DBD600E7D7CE00DED3
      C600DED3C600DECBC600B5A294008C4118000000000094616300CE696300C669
      6300C6696300B5615A00B55D5A00AD5D52006B30310000510000004900000049
      000000650800296D2100F79294006B3031000000000000000000000000006B45
      3100F7EFEF009C300000C6927300C6927300C6927300C69273009C300000F7EF
      EF006B453100000000000000000000000000000000000000000000000000C665
      1000FFFFFF00C6651000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7
      F700F7F3EF00EFEBE700B5A294008C41180000000000C6651000FFFFFF00FFFB
      FF00FFF7F700F7F3EF00F7EBEF00EFE7E700EFE3DE00EFDFD600E7DBD600E7D7
      CE00E7D7CE00DED3C600B5A294008C4118000000000094616300CE6D6B00CE6D
      6B00C6716B00DEB6B5009C5D5A00A55952006B3031000059000000510000005D
      08000882180029752900F79294006B30310000000000000000006B453100F7EF
      EF009C300000F79A6B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00AD6542009C30
      0000F7EFEF006B4531000000000000000000000000000000000000000000C665
      1000FFFFFF00C6651000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFF7F700F7F3EF00EFEBE7008C41180000000000C6651000FFFFFF00FFFF
      FF00FFFBFF00FFF7F700F7F3EF00F7EBEF00EFE7E700EFE3DE00EFDFD600E7DB
      D600E7DBD600E7D7CE00B5A294008C4118000000000094616300D66D6B00CE6D
      6B00DEB6B500FFF3F700DEB6B500A55952006B30310000650800105900002971
      1000108A210031712100F79294006B303100000000006B453100F7EFEF009C30
      0000FFB28400FFAE8400F79A6B00FFFFFF00FFFFFF00AD654200F7865200F77D
      42009C300000F7EFEF006B4531000000000000000000C66510008C411800C665
      1000FFFFFF00C66510008C4118008C4118008C4118008C4118008C4118008C41
      18008C4118008C4118008C4118008C41180000000000C6651000FFFFFF00FFFF
      FF00FFFFFF00FFFBFF00FFF7F700F7F3EF00F7EBEF00EFE7E700EFE3DE00EFDF
      D600EFDFD600E7DBD600B5A294008C4118000000000094616300D6716B00D671
      6B00D6716B00DEB6B500C6716B00AD615A006B303100106908008C822100D6A6
      420031751000D6A64200F79294006B3031009C300000FFF7F700CE610000FFC3
      9C00FFBA9400FFB28400FFAE8400FFFFFF00FFFFFF00AD654200F78E5A00F786
      5200F77D42009C300000F7EFEF006B45310000000000C6651000FFFFFF00C665
      1000FFFFFF00CE610000EF963100EF963100EF963100EF963100F7CB9C00EF96
      3100F7CB9C00EF963100316DFF008C41180000000000C6651000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFBF700F7F3F700F7EBEF00EFE7E700EFE3
      DE00EFE3DE00EFDFD600B5A294008C4118000000000094616300DE757300D671
      6B00D6716B00D66D6B00CE6D6B00B5615A006B30310073822900FFC38400FFC3
      8400FFC38400FFC38400F79294006B3031009C300000FFFFFF00CE610000FFC3
      9C00FFC39C00FFBA9400FFB28400FFFFFF00FFFFFF00AD654200F7926300F78E
      5A00F78652009C300000F7EFEF006B45310000000000C6651000FFFFFF00C665
      1000FFFFFF00FFFFFF00C6651000C6651000C6651000C6651000C6651000C665
      1000C6651000C6651000C66510000000000000000000C6651000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBF700F7F3F700F7EFEF00F7EB
      E700F7EBE700EFE7DE00B5A294008C4118000000000094616300DE757300DE75
      7300DE716B00D6716B00D66D6B00B5655A006B303100FFC38400FFC38400FFC3
      8400FFC38400FFC38400F79294006B303100000000009C300000FFFBFF00CE61
      0000FFC39C00FFC39C00FFFFFF00FFFFFF00FFFFFF00AD654200F79A6B00F792
      63009C300000F7EFEF006B4531000000000000000000C6651000FFFFFF00C665
      10008C4118008C4118008C4118008C4118008C4118008C4118008C4118008C41
      18008C4118008C411800000000000000000000000000C6651000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBF700F7F3F700F7EF
      EF00F7EFEF00F7EBE700B5A294008C4118000000000094616300E7797300DE75
      7300DE757300DE716B00D6716B00B5655A006B303100FFC38400FFC38400FFC3
      8400B5B2A50029A6FF00F79294006B30310000000000000000009C300000FFFF
      FF00CE610000FFC39C00FFC39C00C6927300C6927300FFAE8400F79A6B009C30
      0000F7EFEF006B453100000000000000000000000000C6651000FFFFFF00CE61
      0000EF963100EF963100EF963100EF963100F7CB9C00EF963100F7CB9C00EF96
      3100316DFF008C411800000000000000000000000000C6651000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBF700F7F3
      F700F7F3F700F7EFEF00B5A294008C4118000000000094616300E7797B00E779
      7300E7757300DE757300DE757300BD6563006B303100FFC38400FFC38400B5B2
      A50021BAF70029A6FF00F79294006B3031000000000000000000000000009C30
      0000FFFFFF00CE610000FFC39C00FFFFFF00FFFFFF00AD6542009C300000F7EF
      EF006B45310000000000000000000000000000000000C6651000FFFFFF00FFFF
      FF00C6651000C6651000C6651000C6651000C6651000C6651000C6651000C665
      1000C66510000000000000000000000000000030FF009CEBFF000030FF009CEB
      FF000030FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFB
      F700FFFBF700F7F3F700F7EFEF008C4118000000000094616300E7797300E779
      7B00E7797300E7757300DE757300BD6563006B303100FFC38400B5B2A50018C7
      FF0018C7FF0021BAF700F79294006B3031000000000000000000000000000000
      00009C300000FFFFFF00CE610000FFFFFF00FFFFFF009C300000F7EFEF006B45
      31000000000000000000000000000000000000000000C66510008C4118008C41
      18008C4118008C4118008C4118008C4118008C4118008C4118008C4118008C41
      1800000000000000000000000000000000009CEBFF004A79FF0000FBFF004A79
      FF009CEBFF008C4118008C4118008C4118008C4118008C4118008C4118008C41
      18008C4118008C4118008C4118008C4118000000000000000000B5716B009461
      6300C66D6B00C66D6B00D6717300BD6563006B30310094616300946163009461
      6300946163009461630094616300946163000000000000000000000000000000
      0000000000009C300000FFFBFF00CE610000CE610000F7EFEF006B4531000000
      00000000000000000000000000000000000000000000CE610000EF963100EF96
      3100EF963100EF963100F7CB9C00EF963100F7CB9C00EF963100316DFF008C41
      1800000000000000000000000000000000000030FF0000FBFF00FFFFFF0000FB
      FF000030FF00EF963100EF963100EF963100EF963100F7CB9C00EF963100F7CB
      9C00EF963100316DFF00EF963100C66510000000000000000000000000000000
      0000B5716B0094616300C66D6B00C66D6B006B30310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C300000FFFFFF00FFF7F7006B453100000000000000
      0000000000000000000000000000000000000000000000000000C6651000C665
      1000C6651000C6651000C6651000C6651000C6651000C6651000C66510000000
      0000000000000000000000000000000000009CEBFF004A79FF0000FBFF004A79
      FF009CEBFF00C6651000C6651000C6651000C6651000C6651000C6651000C665
      1000C6651000C6651000C6651000000000000000000000000000000000000000
      00000000000000000000B5716B00946163009461630000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C3000009C30000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000030FF009CEBFF000030FF009CEB
      FF000030FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5A294006B45
      31006B4531006B4531006B4531006B4531006B4531006B4531006B4531006B45
      31006B4531006B4531006B453100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5A29400EFE7
      E700B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A2
      9400B5A29400B5A294006B45310000000000000000000000000084828400C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600000000000000000084929C0000619400006194000061
      9400006194000061940000619400006194000061940000619400006194000061
      9400000000000000000000000000000000000000000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000B5A29400F7EB
      E700EFE7E700EFE3DE00EFDFDE00E7DBD600E7D7CE00E7D3CE00DECFC600DECB
      BD00DECBBD00B5A294006B45310000000000000000000000000084828400FFFF
      FF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFF
      FF00FFFFFF00C6C3C600000000000000000084929C0084929C0021A2D6000092
      CE000092CE000092CE000092CE000092CE000092CE000092CE000092CE000061
      9400000000000000000000000000000000000000000084828400FFFFFF008482
      840084828400848284008482840084828400FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000B5A29400F7EF
      EF00F7EBE700EFE7E700EFE3DE00EFDFDE00E7DBD600E7D7CE00E7D3CE00DECF
      C600DECBBD00B5A294006B45310000000000000000000000000084828400FFFF
      FF0000FFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFF
      FF0000FFFF00C6C3C600000000000000000084929C0084929C007BBED60063DB
      F7005AD7F70052D3F7004ACBF70042C7EF0039C3EF0031BEEF0031BAEF000092
      CE00006194000000000000000000000000000000000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000B5A29400F7F3
      F700F7EFEF00F7EBE700EFE7E700EFE3DE00EFDFDE00E7DBD600E7D7CE00E7D3
      CE00DECFC600B5A294006B45310000000000000000000000000084828400FFFF
      FF00FFFFFF00FFFFFF00C6C3C600000000000000000000000000C6C3C600FFFF
      FF00FFFFFF00C6C3C600000000000000000084929C0063DBFF0084929C006BD7
      EF0063DBF7005AD7F70052D3F7004ACFF70042CBEF0039C7EF0031BEEF0029B6
      E700006194000000000000000000000000000000000084828400FFFFFF008482
      840084828400848284008482840084828400FFFFFF0000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600000000000000000000000000B5A29400FFF7
      F700F7F3F700F7EFEF00F7EBE700EFE7E700EFE3DE00EFDFDE00E7DBD600E7D7
      CE00E7D3CE00B5A294006B45310000000000000000000000000084828400FFFF
      FF0000FFFF00C6C3C60084000000FF000000840000008400000000000000C6C3
      C60000FFFF00C6C3C600000000000000000084929C007BEBFF0084929C0073C3
      D60063DFFF0063DFFF005AD7F70052D3F7004ACFF70042CBEF0039C7EF0031BE
      EF000092CE000061940000000000000000000000000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600000000000000000000000000B5A29400FFFB
      FF00FFF7F700F7F3F700F7EFEF00F7EBE700EFE7E700EFE3DE00EFDFDE00E7DB
      D600E7D7CE00B5A294006B45310000000000000000000000000084828400FFFF
      FF00FFFFFF0084828400FF000000FF00000000820000FF000000840000008400
      0000FFFFFF00C6C3C600000000000000000084929C0084EFFF0063DBFF008492
      9C006BD7EF0063DFFF0063DBFF005AD7F70052D3F7004ACFF70042CBEF0039C7
      EF0031B6E7000061940000000000000000000000000084828400FFFFFF008482
      840084828400FFFFFF000000000000000000000000000000000084000000FFFF
      FF00FFFFFF00FFFFFF00C6C3C600000000000000000000000000B5A29400FFFF
      FF00FFFBFF00FFF7F700F7F3F700F7EFEF00F7EBE700EFE7E700EFE3DE00EFDF
      DE00E7DBD600B5A294006B45310000000000000000000000000084828400FFFF
      FF0000FFFF0084820000C6C3C600848284000082000084820000FF0000008400
      000000FFFF00C6C3C600000000000000000084929C0094F7FF0084EFFF008492
      9C0073C7DE0063DFFF0063DFFF0063DFFF005AD7F70052D3F7004ACFF70042CB
      EF0039C3EF000092CE0000619400000000000000000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084828400FFFFFF00FFFFFF0084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600000000000000000000000000B5A29400FFFF
      FF00FFFFFF00FFFFFF00FFFBFF00FFF7F700F7F3EF00F7EFEF00EFEBE700EFE7
      DE00EFE3DE00B5A294006B45310000000000000000000000000084828400FFFF
      FF00FFFFFF0084820000FFFFFF00C6C3C6000082000084000000FF0000008400
      0000FFFFFF00C6C3C600000000000000000084929C0094F7FF0094F7FF0063DB
      FF0084929C0084929C0084929C0084929C0084929C0084929C0084929C008492
      9C0084929C0084929C0084929C00000000000000000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084828400FFFFFF00848284008400000084000000FFFF
      FF000000840000008400C6C3C600000000000000000000000000B5A29400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFF7F700F7F3EF00F7EFEF00EFEB
      E700EFE7DE00B5A294006B45310000000000000000000000000084828400FFFF
      FF0000FFFF0084820000C6C3C60084820000FF00000084820000008200008482
      840000FFFF00C6C3C600000000000000000084929C0094F7FF0094F7FF0094F7
      FF0063DBFF0063DBFF0063DBFF0063DBFF0063DBFF0063DBFF0063DBFF0063DB
      FF0084929C000000000000000000000000000000000084828400848284008482
      840084828400848284008482840084828400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000840000008400C6C3C600000000000000000000000000B5A29400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFF7F700F7F3EF00F7EF
      EF00B5A29400B5A294006B45310000000000000000000000000084828400FFFF
      FF00FFFFFF00C6C3C6008482000084828400848200000082000084820000C6C3
      C600FFFFFF00C6C3C600000000000000000084929C0094F7FF0094F7FF0094F7
      FF0094F7FF0063DBFF0084929C0084929C0084929C0084929C0084929C008492
      9C009C3000009C3000009C3000009C3000000000000000000000000000000000
      000084828400C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600000000000000000000000000B5A29400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFF7F700B5A2
      94006B4531006B4531006B45310000000000000000000000000084828400FFFF
      FF0000FFFF00FFFFFF00C6C3C600848200008482000084820000C6C3C600FFFF
      FF0000FFFF00C6C3C600000000000000000084929C0094F7FF0094F7FF0094F7
      FF0094F7FF0084929C0000000000000000000000000000000000000000000000
      0000000000009C300000E7A684009C3000000000000000000000000000000000
      00000000000084828400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000B5A29400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00B5A2
      9400F7EBE700DECBBD006B45310000000000000000000000000084828400FFFF
      FF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF000000
      0000000000000000000000000000000000000000000084929C0084929C008492
      9C0084929C000000000000000000000000000000000000000000000000000000
      000000000000BD7139009C3000009C3000000000000000000000000000000000
      0000000000000000000084828400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000B5A29400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B5A2
      9400DECBBD006B4531000000000000000000000000000000000084828400FFFF
      FF0000FFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFFFF00C6C3
      C600FFFFFF008482840000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C300000BD7139000000
      0000BD7139009C300000000000009C3000000000000000000000000000000000
      000000000000000000000000000084828400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000B5A29400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B5A2
      94006B453100000000000000000000000000000000000000000084828400FFFF
      FF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF00C6C3
      C600848284000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C3000009C30
      00009C3000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008482840084828400848284008482
      8400000000000000000000000000000000000000000000000000B5A29400B5A2
      9400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A2
      9400000000000000000000000000000000000000000000000000848284008482
      8400848284008482840084828400848284008482840084828400848284008482
      84000000000000000000000000000000000000000000000000006B4531006B45
      31006B4531006B4531006B4531006B4531006B4531006B4531006B4531006B45
      31006B4531006B4531006B4531000000000000000000000000006B4531006B45
      31006B4531006B4531006B4531006B4531006B4531006B4531006B4531006B45
      31006B4531006B4531006B453100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6AE9C00F7F3EF00B5A2
      9400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A2
      9400B5A29400B5A29400B5A294006B45310000000000C6AE9C00F7F3EF00B5A2
      9400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A2
      9400B5A29400B5A29400B5A294006B4531000000000000000000000000000000
      0000000000000000000084000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6AE9C00F7F7F700F7F3
      EF00F7EFEF00EFEBE700EFE7DE00EFE3DE00E7DBD600E7D7D600E7D3CE00DECF
      C600DECBC600DECBBD00B5A294006B45310000000000C6AE9C00F7F7F700F7F3
      EF00F7EFEF00EFEBE700EFE7DE00EFE3DE00E7DBD600E7D7D600E7D3CE00DECF
      C600DECBC600DECBBD00B5A294006B4531000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008400000084000000000000000000000000000000C6AE9C00FFFBF700F7F7
      F700F7F3EF00F7EFEF00EFEBE700EFE7DE00EFE3DE00E7DBD600E7D7D600E7D3
      CE00DECFC600DECBC600B5A294006B45310000000000C6AE9C00FFFBF700F7F7
      F700F7F3EF009C3000009C3000009C300000EFE3DE00E7DBD6009C3000009C30
      00009C300000DECBC600B5A294006B4531000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000C6AE9C00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DECFC600B5A294006B45310000000000C6AE9C00FFFFFF00FFFB
      F700F7F7F700F7F3EF009C300000EFEBE700EFE7DE00EFE3DE00E7DBD6009C30
      0000E7D3CE00DECFC600B5A294006B4531000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFBFF00FFF7F700F7F3EF00F7EFEF00EFEBE700EFE7DE00EFE3DE00E7DB
      D600E7D7D600E7D3CE00B5A294006B45310000000000C6AE9C00219E21000092
      0000219E1800FFF7F7005AB65A009C3000009C3000009C3000009C3000009C30
      0000E7D7D600E7D3CE00B5A294006B4531008482000084820000848200008482
      0000848200008482000084000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000E7D7D600B5A294006B45310000000000C6AE9C00D6EFD6000892
      0800EFFBEF00FFFBFF00FFF7F700189A18009C300000B5D3AD00EFE7E7009C30
      0000E7DBD600E7D7D600B5A294006B45310084820000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000FFFFFF008400000084000000840000008400
      0000840000008400000084000000840000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008400000084000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFBFF00FFF7F700F7F3EF00F7EFEF00F7EBE700EFE7
      E700EFE3DE00EFDFDE00B5A294006B45310000000000C6AE9C00FFFFFF0073C3
      7300A5D7A500FFFFFF00C6E3BD000092000042AA42009C300000F7EBE7009C30
      0000EFE3DE00EFDFDE00B5A294006B45310084820000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000084000000FFFFFF008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008400000084000000000000000000000000000000C6AE9C00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EFE3DE00B5A294006B45310000000000C6AE9C00FFFFFF00DEEF
      DE0000920000009200000092000000920000A5D39C00F7F3EF009C3000009C30
      0000EFE7E700EFE3DE00B5A294006B45310084820000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084820000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008400000084000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFF7F700F7F3EF00F7EF
      EF00F7EBE700EFE7E700B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF0042AE4200D6EBD6001096100008960800F7F7EF00FFF7F700F7F3EF009C30
      0000F7EBE700EFE7E700B5A294006B4531008482000084820000848200008482
      0000848200008482000084820000848200008482000084820000848200008482
      0000848200008482000084820000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000F7EBE700B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00ADDBAD0031A631000092000063BA6300FFFFFF00FFFBFF00FFF7F700F7F3
      EF00F7EFEF00F7EBE700B5A294006B45310084820000FFFFFF00848200008482
      0000848200008482000084820000848200008482000084820000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084820000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFF7
      F700F7F3EF00F7EFEF00B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00189A180000920000C6E7C600FFFFFF00FFFFFF00FFFBFF00FFF7
      F700F7F3EF00F7EFEF00B5A294006B4531008482000084820000848200008482
      0000848200008482000084820000848200008482000084820000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084820000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBF700F7F7F700B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF007BC77B00189E1800FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBF700F7F7F700B5A294006B4531000000000000000000000000000000
      00000000000084820000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084820000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008400000084000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFBF700F7F7F7006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFBF700F7F7F7006B4531000000000000000000000000000000
      0000000000008482000084820000848200008482000084820000848200008482
      0000848200008482000084820000000000000000000084000000FFFFFF008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008400000000000000000000000000000000000000C6AE9C00C6AE
      9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE
      9C00C6AE9C00C6AE9C00C6AE9C00000000000000000000000000C6AE9C00C6AE
      9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE
      9C00C6AE9C00C6AE9C00C6AE9C00000000000000000000000000000000000000
      00000000000084820000FFFFFF00848200008482000084820000848200008482
      0000848200008482000084820000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008482000084820000848200008482000084820000848200008482
      0000848200008482000084820000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006B4531006B45
      31006B4531006B4531006B4531006B4531006B4531006B4531006B4531006B45
      31006B4531006B4531006B453100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006B4531006B45
      31006B4531006B4531006B4531006B4531006B4531006B4531006B4531006B45
      31006B4531006B4531006B4531000000000000000000000000006B4531006B45
      31006B4531006B4531006B4531006B4531006B4531006B4531006B4531006B45
      31006B4531006B4531006B4531000000000000000000C6AE9C00F7F3EF00B5A2
      9400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A2
      9400B5A29400B5A29400B5A294006B453100000000009C300000633031000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6AE9C00F7F3EF00B5A2
      9400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A2
      9400B5A29400B5A29400B5A294006B45310000000000C6AE9C00F7F3EF00B5A2
      9400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A2
      9400B5A29400B5A29400B5A294006B45310000000000C6AE9C00F7F7F700F7F3
      EF00F7EFEF00EFEBE700EFE7DE00EFE3DE00E7DBD600E7D7D600E7D3CE00DECF
      C600DECBC600DECBBD00B5A294006B453100CE610000FF9A0000D68200006330
      31000000000000000000000000009475630094756300947563008C6D5A007B5D
      4A006B45310000000000000000000000000000000000C6AE9C00F7F7F700F7F3
      EF00F7EFEF00EFEBE700EFE7DE00EFE3DE00E7DBD600E7D7D600E7D3CE00DECF
      C600DECBC600DECBBD00B5A294006B45310000000000C6AE9C00F7F7F700F7F3
      EF00F7EFEF00EFEBE700EFE7DE00EFE3DE00E7DBD600E7D7D600E7D3CE00DECF
      C600DECBC600DECBBD00B5A294006B45310000000000C6AE9C00FFFBF700F7F7
      F7009C3000009C3000009C3000009C3000009C3000009C3000009C3000009C30
      00009C300000DECBC600B5A294006B453100BD713900FFCB9400FF9A00009C30
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6AE9C00FFFBF700F7F7
      F700F7F3EF00F7EFEF00EFEBE700EFE7DE00EFE3DE00E7DBD600E7D7D600E7D3
      CE00DECFC600DECBC600B5A294006B45310000000000C6AE9C00FFFBF700F7F7
      F700F7F3EF00F7EFEF00EFEBE700EFE7DE00EFE3DE00E7DBD600E7D7D600E7D3
      CE00DECFC600DECBC600B5A294006B45310000000000C6AE9C00FFFFFF00FFFB
      F700F7F7F700F7F3EF00F7EFEF00EFEBE700EFE7DE00EFE3DE00E7DBD600E7D7
      D600E7D3CE00DECFC600B5A294006B45310000000000BD713900CE6100000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6AE9C00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DECFC600B5A294006B45310000000000C6AE9C00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DECFC600B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFBFF00EFDBD600B5714A009C3C08009C3000009C380800B5715200E7CF
      C600E7D7D600E7D3CE00B5A294006B4531000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFBFF00FFF7F700F7F3EF00F7EFEF00EFEBE700EFE7DE00EFE3DE00E7DB
      D600E7D7D600E7D3CE00B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFBFF00FFF7F700F7F3EF00F7EFEF00EFEBE700EFE7DE00EFE3DE00E7DB
      D600E7D7D600E7D3CE00B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00AD5121009C300000C6927B00EFE7E700EFE3DE00BD826300AD5D
      3900E7DBD600E7D7D600B5A294006B453100000000009C300000633031000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6AE9C00FFFFFF000000
      000000000000000000000000000000000000000000000000000000000000EFE3
      DE00E7DBD600E7D7D600B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000000000000EFE3
      DE00E7DBD600E7D7D600B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF009C3000009C300000F7F3EF00F7F3EF00F7EFEF00EFDFDE009C30
      0000EFE3DE00EFDFDE00B5A294006B453100CE610000FF9A0000D68200006330
      31000000000000000000000000009475630094756300947563008C6D5A007B5D
      4A006B45310000000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFBFF00FFF7F700F7F3EF00F7EFEF00F7EBE700EFE7
      E700EFE3DE00EFDFDE00B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFBFF00FFF7F700F7F3EF00F7EFEF00F7EBE700EFE7
      E700EFE3DE00EFDFDE00B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF009C3000009C300000FFFBFF00FFF7F700F7F3EF00F7EFEF009C30
      0000EFE7E700EFE3DE00B5A294006B453100BD713900FFCB9400FF9A00009C30
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6AE9C00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EFE3DE00B5A294006B45310000000000C6AE9C00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EFE3DE00B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF009C3000009C300000FFFFFF00FFFBFF00FFF7F700F7F3EF009C30
      0000F7EBE700EFE7E700B5A294006B45310000000000BD713900CE6100000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFF7F700F7F3EF00F7EF
      EF00F7EBE700EFE7E700B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFF7F700F7F3EF00F7EF
      EF00F7EBE700EFE7E700B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF009C3000009C300000FFFFFF00FFFFFF00FFFBFF00FFF7F7009C30
      0000F7EFEF00F7EBE700B5A294006B4531000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6AE9C00FFFFFF000000
      000000000000000000000000000000000000000000000000000000000000F7F3
      EF00F7EFEF00F7EBE700B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000000000000F7F3
      EF00F7EFEF00F7EBE700B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFF3F7009C3000009C300000F7F3EF00FFFFFF00FFFFFF00F7EFEF009C30
      0000EFE7DE00F7EFEF00B5A294006B453100000000009C300000633031000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFF7
      F700F7F3EF00F7EFEF00B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFF7
      F700F7F3EF00F7EFEF00B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF009C3000009C3000009C3000009C300000FFFFFF00FFFFFF009C3000009C30
      00009C300000F7F7F700B5A294006B453100CE610000FF9A0000D68200006330
      31000000000000000000000000009475630094756300947563008C6D5A007B5D
      4A006B45310000000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBF700F7F7F700B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBF700F7F7F700B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFBF700F7F7F7006B453100BD713900FFCB9400FF9A00009C30
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFBF700F7F7F7006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFBF700F7F7F7006B4531000000000000000000C6AE9C00C6AE
      9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE
      9C00C6AE9C00C6AE9C00C6AE9C000000000000000000BD713900CE6100000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6AE9C00C6AE
      9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE
      9C00C6AE9C00C6AE9C00C6AE9C00000000000000000000000000C6AE9C00C6AE
      9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE
      9C00C6AE9C00C6AE9C00C6AE9C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006B4531006B45
      31006B4531006B4531006B4531006B4531006B4531006B4531006B4531006B45
      31006B4531006B4531006B4531000000000000000000000000006B4531006B45
      31006B4531006B4531006B4531006B4531006B4531006B4531006B4531006B45
      31006B4531006B4531006B45310000000000AD7D6300A5755A00945D39008C55
      39007B4121007B412100000000000000000000000000AD7D6300A5755A00945D
      39008C5539007B4121007B412100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000006D0000006D00000000000000000000C6AE9C00F7F3EF00B5A2
      9400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A2
      9400B5A29400B5A29400B5A294006B45310000000000C6AE9C00F7F3EF00B5A2
      9400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A2
      9400B5A29400B5A29400B5A294006B453100AD7D6300FFFBFF00E7CBBD00CEA2
      8C00AD7552007B412100000000000000000000000000AD7D6300FFFBFF00E7CB
      BD00CEA28C00AD7552007B412100000000000000000000000000000000000000
      00009C3000000000000000000000000000000000000000000000000000000000
      0000006D00000000000000000000006D000000000000C6AE9C00F7F7F700F7F3
      EF00F7EFEF00EFEBE700EFE7DE00EFE3DE00E7DBD600E7D7D600E7D3CE00DECF
      C600DECBC600DECBBD00B5A294006B45310000000000C6AE9C00F7F7F700F7F3
      EF00F7EFEF00EFEBE700EFE7DE00EFE3DE00E7DBD600E7D7D600E7D3CE00DECF
      C600DECBC600DECBBD00B5A294006B453100AD7D6300FFFBFF00E7CBBD00CEA2
      8C00BD825A007B412100000000000000000000000000AD7D6300FFFBFF00E7CB
      BD00CEA28C00BD825A007B4121000000000000000000000000009C3000009C30
      00009C3000009C30000000000000000000000000000000000000000000000000
      0000006D000000000000000000000000000000000000C6AE9C00FFFBF700F7F7
      F7009C3000009C3000009C3000009C3000009C3000009C300000B56D4A00DEC7
      BD00DECFC600DECBC600B5A294006B45310000000000C6AE9C00FFFBF700F7F7
      F700E7C7BD009C3000009C3000009C3000009C300000DEC3B500E7D7D600E7D3
      CE00DECFC600DECBC600B5A294006B453100AD7D6300FFFBFF00E7CBBD00CEA2
      8C00BD825A007B412100000000000000000000000000AD7D6300FFFBFF00E7CB
      BD00CEA28C00BD825A007B41210000000000000000009C300000D66531000000
      00009C3000000000000000000000000000000000000000000000000000000000
      0000006D00000000000000000000006D000000000000C6AE9C00FFFFFF00FFFB
      F700F7F7F7009C3000009C300000EFDFD600EFE7DE00CEA694009C3000009C34
      0000E7D3CE00DECFC600B5A294006B45310000000000C6AE9C00FFFFFF00FFFB
      F700F7F7F700F7E7DE00AD5529009C300000DEBEAD00EFE3DE00E7DBD600E7D7
      D600E7D3CE00DECFC600B5A294006B453100AD7D6300FFFBFF00E7CBBD00CEA2
      8C00BD825A007B412100000000000000000000000000AD7D6300FFFBFF00E7CB
      BD00CEA28C00BD825A007B41210000000000000000009C300000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000006D0000006D00000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFBFF009C3000009C300000F7EFEF00EFEBE700EFE7DE009C3000009C30
      0000E7D7D600E7D3CE00B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFBFF00FFF7F700C68A6B009C300000CEA28C00EFE7DE00EFE3DE00E7DB
      D600E7D7D600E7D3CE00B5A294006B453100AD7D63009C694A009C694A009451
      3100945131007B4121007B4121006B412100AD7D63009C694A009C694A009451
      3100945131007B4121007B41210000000000000000009C300000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF009C3000009C300000F7F3EF00F7EFEF00DEBAAD009C300000B565
      4200E7DBD600E7D7D600B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFBFF00D6AE9C009C300000BD795A00F7EBE700EFE7E700EFE3
      DE00E7DBD600E7D7D600B5A294006B453100DEC7AD00AD7D6300EFDBCE00EFDB
      CE00CEA28C00AD7552007B4121009C694A00AD7D6300EFDBCE00EFDBCE00CEA2
      8C00AD7552007B41210094616300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF009C3000009C3000009C3000009C3000009C300000BD795A00EFDB
      D600EFE3DE00EFDFDE00B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00EFD7CE009C300000AD512900F7EFEF00F7EBE700EFE7
      E700EFE3DE00EFDFDE00B5A294006B45310000000000AD7D6300FFFFFF00F7EB
      E700EFD3C600AD7552007B412100CE9A9400AD7D6300FFFFFF00F7EBE700EFD3
      C600AD7552009461630000000000000000000000000000000000000000000000
      0000000000000000000042454200636563006365630063656300000000000000
      00000000000000000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF009C3000009C300000FFFBFF00EFE3DE00BD7D63009C340000CE9E
      8C00EFE7E700EFE3DE00B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFBF7009C3000009C300000F7EBE700F7EFEF00F7EB
      E700EFE7E700EFE3DE00B5A294006B4531000000000000000000AD7D63009C69
      4A00945D39007B4121007B4121009C694A00AD7D63009C694A00945D39007B41
      21007B4121000000000000000000000000000000000000000000000000000000
      0000000000000000000042454200000000000000000000000000636563000000
      00000000000000000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF009C3000009C300000FFFFFF00FFFBFF00FFF7F7009C3000009C34
      0000F7EBE700EFE7E700B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00AD5529009C300000E7C3B500F7F3EF00F7EF
      EF00F7EBE700EFE7E700B5A294006B4531000000000000000000AD7D6300FFFB
      FF00DEC3B500AD7552007B41210000000000AD7D6300FFFBFF00DEC3B500AD75
      52007B4121000000000000000000000000000000000000000000000000000000
      0000000000000000000042454200000000000000000000000000636563000000
      00000000000000000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF009C3000009C300000FFFFFF00FFFFFF00DEBAAD009C3000009C34
      0000F7EFEF00F7EBE700B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CE9A84009C300000BD7D5A00FFF7F700F7F3
      EF00F7EFEF00F7EBE700B5A294006B4531000000000000000000AD7D63009C69
      4A00945D39007B4121009C694A0000000000AD7D63009C694A00945D39007B41
      21009C694A000000000000000000000000000000000000000000000000000000
      0000000000000000000042454200000000000000000000000000636563000000
      00000000000000000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF009C3000009C3000009C3000009C3000009C3000009C300000BD7D5A00F7E7
      DE00F7F3EF00F7EFEF00B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00DEB2A5009C3000009C3000009C300000A5491800FFF7
      F700F7F3EF00F7EFEF00B5A294006B453100000000000000000000000000AD7D
      6300FFFBFF007B412100000000000000000000000000AD7D6300FFFBFF007B41
      2100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000042454200636563006365630063656300000000000000
      00000000000000000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBF700F7F7F700B5A294006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBF700F7F7F700B5A294006B453100000000000000000000000000AD7D
      6300AD7D63009C694A00000000000000000000000000AD7D6300AD7D63009C69
      4A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000042454200000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFBF700F7F7F7006B45310000000000C6AE9C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFBF700F7F7F7006B4531000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000042454200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6AE9C00C6AE
      9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE
      9C00C6AE9C00C6AE9C00C6AE9C00000000000000000000000000C6AE9C00C6AE
      9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE9C00C6AE
      9C00C6AE9C00C6AE9C00C6AE9C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004245420042454200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CE5110009C3000009C3000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7AA8C009445100094451000944510009445
      1000944510009445100094451000944510000000000000000000000000000000
      00000000000000000000D6AEA5006B4531006B4531006B4531006B4531006B45
      31006B4531006B4531006B4531006B4531000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EF824200CE511000CE511000CE5110009C30000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7AA8C00F7E7DE00F7E3D600F7DBCE00F7D3
      BD00EFCFB500EFC7AD00EFC7AD00944510000000000000000000000000000000
      00000000000000000000D6AEA500EFE7E700C6AE9C00C6AE9C00C6AE9C00C6AE
      9C00C6AE9C00C6AE9C00C6AE9C006B4531000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EF824200CE511000EF824200F7A273009C3000000000000000000000CE51
      1000CE511000A549100000000000000000000000000000000000000000000000
      0000000000000000000000000000E7AA8C00FFEFE700F7E7DE00F7E3D600F7DB
      CE00F7D7C600F7D3BD00EFCBB500944510000000000000416B0000416B000041
      6B0000416B0000416B00D6AEA500F7EFEF00EFE7E700EFE3DE00E7DBD600E7DB
      D600E7D3CE00DECFC600C6AE9C006B4531000000000084828400000000000000
      0000000000000000000000000000000000008482840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EF824200CE51100000000000F7A273009C30000000000000CE511000CE51
      1000A5491000A5491000A5491000000000000000000000000000000000000000
      0000000000000000000000000000E7AA8C00FFF7F700D6825200D6825200D682
      5200D6825200D6825200F7D3BD00944510000092CE007BDBEF000092CE000092
      CE000092CE000092CE00D6AEA500FFF7F700DE9A4200DE9A4200DE9A4200DE9A
      4200DE9A4200E7D3CE00C6AE9C006B4531000000000000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EF824200F7A27300A5491000A54910009C30000000000000CE511000EF82
      4200F7A27300A54910009C300000000000000000000000000000000000000000
      0000000000000000000000000000E7AA8C00FFFFFF00FFFBF700FFF3EF00FFEB
      DE00F7E3D600F7DFCE00F7D7C600944510000092CE0084DFF7007BDBEF0073D3
      EF006BCFEF0063CBE700D6AEA500FFFFFF00FFFBF700F7F3EF00F7EBE700EFE7
      DE00EFDFDE00E7DBD600C6AE9C006B4531000000000000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C6000000000084828400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EF824200F7A27300A54910009C3000009C300000CE5110000000
      0000EF824200A54910009C30000000000000CEBAAD007B5942007B5942007B59
      42007B5942007B5942007B594200E7AA8C00FFFFFF00D6825200D6825200D682
      5200D6825200D6825200F7DFCE00944510000092CE008CE3F70084DFF7007BDB
      EF0073D3EF006BCFEF00D6AEA500FFFFFF00DE9A4200DE9A4200DE9A4200DE9A
      4200DE9A4200EFDFDE00C6AE9C006B453100000000008482840000000000C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C6000000000084828400848284000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EF8242009C410800A54D21009C300000CE511000A549
      10009C3000009C3000000000000000000000CEBAAD00EFE7E700EFE3DE00E7DB
      D600E7D7CE00DECFC600DECBBD00E7AA8C00FFFFFF00FFFFFF00FFFFFF00FFFB
      F700FFF3EF00FFEBDE00F7E3D600944510000092CE0094EBFF008CE3F70084DF
      F7007BDBEF0073D3EF00D6AEA500FFFFFF00FFFFFF00FFFFFF00FFFBF700F7F3
      EF00F7EBE700EFE7DE00C6AE9C006B453100000000000000000000000000C6C3
      C600FFFFFF00C6C3C600FFFFFF00C6C3C600FFFFFF0000000000848284008482
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000073615200B5A29400A55D39009C3000009C30
      0000CE511000000000000000000000000000CEBAAD00F7EFEF00EFE7E700EFE3
      DE00E7DBD600E7DBD600E7D3CE00E7AA8C00FFFFFF00D6825200D6825200FFFF
      FF00E7AA8C00AD552900AD552900AD5529000092CE0094EBFF0094EBFF008CE3
      F70084DFF7007BDBEF00D6AEA500FFFFFF00DE9A4200DE9A4200DE9A4200FFFB
      F700F7F3EF00CEAAA5006B4531006B4531000000000000000000000000000000
      0000C6C3C600FFFFFF00C6C3C600FFFFFF00C6C3C600FFFFFF00000000008482
      8400848284000000000000000000000000000000000000000000000000000000
      0000000000000000000073615200D6C7BD00CEB2A50073615200000000000000
      000000000000000000000000000000000000CEBAAD00FFF7F700DEAA8400DEAA
      8400DEAA8400DEAA8400DEAA8400E7AA8C00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E7AA8C00F7EBE700AD552900000000000092CE0094EBFF0094EBFF0094EB
      FF008CE3F70084DFF700D6AEA500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBF700CEAAA500FFFFFF006B4531000000000000000000000000000000
      000000000000C6C3C600FFFFFF00C6C3C600FFFFFF00C6C3C600FFFFFF000000
      0000848284008482840084828400000000000000000000000000000000000000
      00000000000073615200DED3CE0073615200DECBBD0073615200000000000000
      000000000000000000000000000000000000CEBAAD00FFFFFF00FFFBF700F7F3
      EF00F7EBE700EFE7DE00EFDFDE00E7AA8C00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00EFAA8C00C669310000000000000000000092CE0094EBFF0094EBFF0094EB
      FF0094EBFF008CE3F700D6AEA500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CEAAA5006B453100000000000000000000000000000000000000
      00000000000000000000C6C3C600FFFFFF00C6C3C600FFFFFF00C6C3C600FFFF
      FF00000000008482840000000000000000000000000000000000000000000000
      000073615200EFE7DE0073615200AD928400EFDBD60073615200000000000000
      000000000000000000000000000000000000CEBAAD00FFFFFF00DEAA8400DEAA
      8400DEAA8400DEAA8400DEAA8400E7AA8C00E7AA8C00E7AA8C00E7AA8C00E7AA
      8C00E7AA8C000000000000000000000000000092CE0094EBFF0094EBFF0094EB
      FF0094EBFF0094EBFF00D6AEA500D6AEA500D6AEA500D6AEA500D6AEA500D6AE
      A500D6AEA500D6AEA50000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C3C600FFFFFF00C6C3C600FFFFFF00C6C3
      C600FFFFFF00848284000000000000000000000000000000000000000000AD92
      8400FFFFFF007361520000000000AD928400EFE7DE0073615200000000000000
      000000000000000000000000000000000000CEBAAD00FFFFFF00FFFFFF00FFFF
      FF00FFFBF700F7F3EF00F7EBE700EFE7DE006B45310000000000000000000000
      0000000000000000000000000000000000000092CE0094EBFF0094EBFF000061
      9C0000416B0000416B0000416B0000416B0000416B0000416B0000416B000092
      CE0000416B000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C3C600FFFFFF00C6C3C600FFFF
      FF00C6C3C600C6C3C6000000000000000000000000000000000000000000AD92
      8400736152000000000000000000AD928400F7EFEF0073615200000000000000
      000000000000000000000000000000000000CEBAAD00FFFFFF00DEAA8400DEAA
      8400FFFFFF00CEBAAD006B4531006B4531006B45310000000000000000000000
      0000000000000000000000000000000000000092CE0094EBFF0094EBFF000061
      9C0094EBFF006BD3EF0000416B006BD3EF004ABEE7000896CE0000416B000092
      CE0000416B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008482840000000000000000000000
      000000000000848284008482840000000000000000000000000000000000AD92
      8400000000000000000000000000AD928400FFFFFF0073615200000000000000
      000000000000000000000000000000000000CEBAAD00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CEBAAD00F7EBE7006B4531000000000000000000000000000000
      0000000000000000000000000000000000000092CE0094EBFF0094EBFF0094EB
      FF0000619C0094EBFF0000416B0000416B006BD3EF0000416B0063CBEF0063CB
      EF0000416B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD9284007361520000000000000000000000
      000000000000000000000000000000000000CEBAAD00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CEBAAD006B453100000000000000000000000000000000000000
      000000000000000000000000000000000000000000000092CE000092CE000092
      CE000092CE0000619C0094EBFF0094EBFF0000416B000092CE000092CE000092
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD9284000000000000000000000000000000
      000000000000000000000000000000000000CEBAAD00CEBAAD00CEBAAD00CEBA
      AD00CEBAAD00CEBAAD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000619C0000619C0000619C0000619C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006B4129006B41
      29006B4129006B4129006B4129006B4129006B4129006B4129006B4129006B41
      29006B4129006B4129006B4129006B4129000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE716B00D6969400CE8E
      8C0073594A00C6B2AD00C6BEBD00CEC3BD00CEC3BD00CEC3BD00CEC3BD008C75
      63009441420094414200944142006B4129000000000084614A0084614A008461
      4A0084614A0084614A0084614A0084614A0084614A0084614A0084614A008461
      4A00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B5552900CE410000000000000000
      0000000000000000000000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000CE716B00D69E9C00D696
      940073594A00291C18005A514200FFFFFF00FFFFFF00FFFFFF00F7E7DE008C75
      63009445420094414200944142006B412900A58E7B00FFFFFF00BDA69400BDA6
      9400BDA69400BDA69400BDA69400BDA69400BDA69400BDA69400BDA69400BDA6
      940084614A000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AD5D3100D6653100CE4100000000
      0000000000000000000000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000CE716B00DEAAA500D69E
      9C0073594A000000000031241800FFFFFF00F7F3EF00F7E7DE00E7CBBD008C75
      63009C514A0094454200944142006B412900A58E7B00FFFFFF00FFFFFF00FFFF
      FF00FFF7F700FFEFE700F7E7DE00F7DFCE00F7D7C600213C5A00EFCBAD00BDA6
      940084614A0084614A0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD5D3100D6653100CE41
      0000000000000000000000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000CE716B00E7B2AD00DEAA
      A50073594A0073594A0073594A0073594A0073594A0073594A0073594A007359
      4A00A55952009C514A00944542006B412900A58E7B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFF7F700FFEFE700F7E7DE00F7DFCE0042AE4A00EFCFB500BDA6
      9400A58E7B00A57D630084614A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000AD5D3100D665
      3100CE4100000000000000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000CE716B00EFBEBD00E7B6
      B500E7AEA500DEA69C00D69A9400CE8E8C00C6868400BD7D7B00BD757300B569
      6B00AD615A00A55952009C514A006B412900A58E7B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFF7F700FFEFE700F7E7DE00F7DFCE00F7D7C600EFCF
      B500A58E7B00BD92730084614A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE754200EF8A
      5A00CE5D2900CE5D290000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000CE716B00F7C3BD00EFBE
      BD00E7B6B500E7AEA500DEA69C00D69A9400CE928C00CE8A8400C6827B00BD79
      7300B5696B00AD615A00A55952006B412900A58E7B00A58E7B00A58E7B00A58E
      7B00A58E7B00A58E7B00A58E7B00A58E7B00A58E7B00A58E7B0084614A008461
      4A00A58E7B00C69E840084614A00000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000AD5D
      3100EF8A5A00CE41000000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000CE716B00F7C3BD00F7C3
      BD00CE615A00CE615A00CE615A00CE615A00CE615A00CE615A00CE615A00CE61
      5A00CE615A00B5716B00AD6563006B412900A58E7B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7F700FFEFE700F7E7DE00F7DF
      CE00A58E7B00D6AE940084614A000000000000000000C66931009C3000009C30
      00009C3000009C3000009C3000009C300000000000000000000000000000AD5D
      3100EF8A5A00DE754200D6714200000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000CE716B00F7C3BD00CE61
      5A00FFF7F700F7EFEF00F7E7E700EFE3DE00EFDFD600E7D7CE00E7D3C600E7CB
      BD00E7CBBD00CE615A00B5716B006B41290000000000A58E7B00A58E7B00A58E
      7B00A58E7B00A58E7B00A58E7B00A58E7B00A58E7B00A58E7B00A58E7B00A58E
      7B00DEB69C009482730084614A000000000000000000C6693100EF8A5A00EF8A
      5A00EF8A5A00EF8A5A009C30000000000000000000000000000000000000AD5D
      3100EF8A5A00DE754200CE410000000000000000000084828400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084828400000000000000000000000000CE716B00F7C3BD00CE61
      5A00FFFFFF00FFFBF700FFF3EF00F7EBE700F7E7DE00EFDFD600EFDBCE00E7D3
      CE00E7CFC600CE615A00BD7973006B41290000000000D6AAAD00D6AAAD00FFFF
      FF00FFFFFF00FFFFFF00FFFBF700F7F7F700F7F3EF00F7EFEF00F7EBE7008461
      4A00AD928400DEB69C0084614A000000000000000000C6693100EF8A5A00E78E
      6300E78A5A00E78652009C30000000000000000000000000000000000000AD5D
      3100EF8A5A00DE754200CE41000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000008400000000000000CE716B00F7C3BD00CE61
      5A00FFFFFF00FFFFFF00FFFBF700FFF3EF00F7EBE700F7E7DE00EFDFD600EFDB
      CE00E7D3CE00CE615A00C6827B006B412900000000000000000000000000D6AA
      AD00FFFFFF00D6BAAD00D6BAAD00D6BAAD00D6BAAD00D6BAAD00F7EFEF008461
      4A0084614A0084614A0084614A000000000000000000C6693100EF8A5A00F7B2
      9400E78E6300E78A5A00D67142009C30000000000000000000009C300000F7B2
      9400EF8A5A00DE754200CE5D2900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008400000000000000CE716B00F7C3BD00CE61
      5A00FFFFFF00FFFFFF00FFFFFF00FFFBF700FFF3EF00F7EBE700F7E7DE00EFDF
      D600EFDBCE00CE615A00CE8A84006B412900000000000000000000000000D6AA
      AD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBF700F7F7F700F7F3EF00F7EF
      EF0084614A0000000000000000000000000000000000C6693100F7B29400C669
      3100F7B29400E78E6300E78E5A00D67142009C3000009C300000F7B29400EF8A
      5A00EF8A5A00DE754200D6714200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008400000000000000840000000000000000000000CE716B00F7C3BD00CE61
      5A00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBF700FFF3EF00F7EBE700F7E7
      DE00EFDFD600CE615A00D69694006B4129000000000000000000000000000000
      0000D6AAAD00FFFFFF00D6BAAD00D6BAAD00D6BAAD00D6BAAD00D6BAAD00F7F3
      EF0084614A0084614A00000000000000000000000000C6693100C66931000000
      0000C6693100F7B29400E7926300E78E5A00E7865A00E7825200E7825200EF8A
      5A00DE754200CE5D290000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008400000084000000000000000000000000000000CE716B00F7C3BD00CE61
      5A00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBF700FFF3EF00F7EB
      E700F7E7DE00CE615A00000000006B4129000000000000000000000000000000
      000000000000D6AAAD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBF700F7F7
      F700F7F3EF0084614A00000000000000000000000000C6693100000000000000
      000000000000C6693100F7B29400F7B29400F7B29400F7B29400EF8A5A00CE5D
      2900D67142000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008400000084000000840000000000000000000000CE716B00F7C3BD00CE61
      5A00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBF700FFF3
      EF00F7EBE700CE615A00CE8A84006B4129000000000000000000000000000000
      000000000000D6AAAD00D6AAAD00D6AAAD00D6AAAD00D6AAAD00D6AAAD00D6AA
      AD00D6AAAD00D6AAAD0000000000000000000000000000000000000000000000
      00000000000000000000F76D2900C6693100C6693100C6693100F76D29000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE716B00CE716B00CE71
      6B00CE716B00CE716B00CE716B00CE716B00CE716B00CE716B00CE716B00CE71
      6B00CE716B00CE716B00CE716B00CE716B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008482
      8400848284008482840084828400848284008482840084828400848284008482
      8400848284008482840084828400848284000000000000000000000000000000
      0000000000000000000084828400848284008482840084828400848284008482
      8400848284008482840084828400848284000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF000000000000000000000000000000000000000000C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600848284000000000000000000000000000000
      00000000000084828400C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600848284000000000000000000000000000000
      0000000000000000000000000000840084008400840084828400000000000000
      00000000000000000000000000000000000000000000FFFFFF000000000000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
      FF00FFFFFF0000000000FFFFFF0000000000000000000000000000000000C6C3
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600848284000000000000000000000000008482
      840084828400FF000000C6C3C600FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFF
      FF0000FFFF00FFFFFF00C6C3C600848284000000000000000000000000000000
      0000000000008400840084008400FFFFFF00FFFFFF00C6C3C600848284000000
      0000000000000000000000000000000000000000000000FFFF00FFFFFF000000
      000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
      FF0000000000FFFFFF0000FFFF00000000000000000000000000FF000000C6C3
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C60084828400000000000000000084828400FF00
      0000FF000000FF000000C6C3C600FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600848284000000000000000000000000008400
      840084008400FFFFFF00FFFFFF000000000000000000C6C3C600C6C3C6008482
      84000000000000000000000000000000000000000000FFFFFF0000FFFF00FFFF
      FF000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF000000
      0000FFFFFF0000FFFF00FFFFFF000000000000000000FF000000848284008482
      8400848284008482840084828400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600848284000000000084828400FF000000FF00
      0000FF000000FF000000C6C3C600FFFFFF0000FFFF0084828400848284008482
      8400C6C3C600FFFFFF00C6C3C60084828400848284008400840084008400FFFF
      FF00FFFFFF000000000000000000840084008400840000000000C6C3C600C6C3
      C600848284000000000000000000000000000000000000FFFF00FFFFFF000000
      0000FFFFFF000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000FFFF000000000084828400FF000000848284008482
      8400FF000000FF000000FF000000FF00000084828400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600848284000000000084828400FF000000FF00
      0000FF000000FF000000C6C3C600FFFFFF0084828400FF000000FF000000FF00
      000084828400FFFFFF00C6C3C600848284008482840084008400FFFFFF000000
      000000000000840084008400840084008400840084008400840000000000C6C3
      C600C6C3C60084828400000000000000000000000000FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF0000000000848284008482840084828400FF00
      0000FF000000C6C3C600C6C3C60084828400FF00000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840084828400FF000000FF000000FF00
      0000FF000000FF000000C6C3C600FFFFFF0084828400C6C3C60084828400FF00
      000084828400FFFFFF00C6C3C600848284008482840000000000000000008400
      840084008400840084000082840000FFFF008400840084008400840084000000
      0000C6C3C600C6C3C60084828400000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00000000000000000084828400C6C3C60084828400FF00
      000084828400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840084828400FF000000FF000000FF00
      00008482840084828400C6C3C600FFFFFF0084828400FFFFFF00C6C3C6008482
      840084828400FFFFFF00C6C3C600848284008482840084008400840084008400
      8400840084008400840084008400008284008400840084008400840084008400
      840000000000C6C3C600000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FF000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000008482840000FFFF00FF00
      0000FF000000FF000000FF000000FF000000FF00000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840084828400FF000000FF0000008482
      84008482840084828400C6C3C600FFFFFF0000FFFF0084828400848284008482
      8400C6C3C600FFFFFF00C6C3C600848284000000000084008400FFFFFF008400
      84008400840084008400840084008400840000FFFF0000FFFF00840084008400
      84008400840000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FF000000FF000000FF00
      0000FFFFFF00FFFFFF00FFFFFF000000000000000000000000008482840000FF
      FF00FF000000FFFFFF00FFFFFF0084828400FF00000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840084828400FF000000FF0000008482
      84008482840084828400C6C3C600FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFF
      FF0084828400848284008482840084828400000000000000000084008400FFFF
      FF0084008400840084008400840084008400840084000082840000FFFF0000FF
      FF00840084008400840000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FF000000FF000000FFFFFF00FF00
      0000FF000000FFFFFF0000000000000000000000000000000000000000008482
      840000FFFF00FF000000FF000000FF00000084828400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840084828400FF000000FF000000C6C3
      C6008482840084828400C6C3C600FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFF
      FF00C6C3C600FFFFFF00C6C3C600000000000000000000000000000000008400
      8400FFFFFF00840084008400840084008400008284008400840000FFFF0000FF
      FF0084008400840084008400840000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FF000000FF000000FFFFFF00FFFFFF00FFFF
      FF00FF000000FF0000000000000000000000000000000000000000000000C6C3
      C600FF000000FF000000FF00000084828400FFFFFF00FF000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840000000000C6C3C600FF000000FF00
      0000FFFFFF00C6C3C600C6C3C600FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFF
      FF00C6C3C600C6C3C60000000000000000000000000000000000000000000000
      000084008400FFFFFF00840084008400840000FFFF0000FFFF0000FFFF008400
      8400840084008400840000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FF000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FF000000FF00000000000000000000000000000000000000C6C3
      C600FFFFFF00FFFFFF00FFFFFF00FF000000FF000000FFFFFF00FFFFFF00FFFF
      FF008482840084828400848284008482840000000000C6C3C600FF000000FFFF
      FF00C6C3C600FFFFFF00C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C6008482840000000000000000000000000000000000000000000000
      00000000000084008400FFFFFF00840084008400840084008400840084008400
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000FF000000FF000000000000000000000000000000C6C3
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00C6C3C600FFFFFF00C6C3C600000000000000000000000000C6C3C600FF00
      0000FF000000C6C3C600FFFFFF00C6C3C6008482840084828400848284008482
      8400848284000000000000000000000000000000000000000000000000000000
      0000000000000000000084008400FFFFFF008400840084008400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000FF000000000000000000000000000000C6C3
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00C6C3C600C6C3C6000000000000000000000000000000000000000000C6C3
      C600C6C3C600FF000000FF000000FFFFFF00C6C3C60084828400848284008482
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840084008400840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C6000000000000000000000000000000000000000000000000000000
      000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000B00000000100010000000000800500000000000000000000
      000000000000000000000000FFFFFF00FEFFFEFF00000000FE7FFE7F00000000
      0000800000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000800000000000
      FFF0FFF000000000FFF9FFF900000000C000FEFFFEFFFEFF8000FE7FFE7FFE7F
      8000000000000000800000000000000080000000000000008000000000000000
      8000000000000000800000000000000080000000000000008000000000000000
      8000000000000000800000000000000080000000000000008000000000000000
      8000FFF0FFF0FFF08000FFF9FFF9FFF9FFFFC001FFFFFFFFC0078000F0018001
      80038000C07F800180038000803F800180038000801F800180038000800F8001
      80038000C007800180038000E003800180038000F0018001C0078000F8018001
      E38F8000FC018001E38F8000FE01818FE00F8000FF03C38FF01F8000FF87FFA4
      F83FC001FFFFFFF1FFFFFFFFFFFFFFFFF87FFE7FF800FFFFC000FC3FF8008000
      8000F81FF80080008000F00FE00080008000E007E00080008000C003E0008000
      8000800180008000800000008000800080000000800180008000800180038000
      8000C003800380008000E007800700008000F00F800F0000C000F81F800F0000
      F07FFC3FC01F0001FC7FFE7FFFFF07FFFFFFFFFFC001C001FFFF803FC001C001
      000F803FC001C001000F803FC001C00100078000C001C00100078000C001C001
      00038000C001C00100038000C001C00100018000C001C00100018000C001C001
      00078000C001C0010000F000C001C00103F8F801C001C00187F8FC03C003C003
      FF92FE07C007C007FFC7FF0FC00FC00FC001C001FFFFFFFF80008000FC00FFFF
      80008000FC00800380008000FC00800380008000FC0080038000800000008003
      8000800000008003800080000000800380008000003F80038000800000018003
      8000800000018003800080000001800380008000F801800380008000F8018003
      C001C001F8018003FFFFFFFFF801FFFFC001FFFFC001C00180009C0380008000
      80000C038000800080000C038000800080009FFF800080008000FFFF80008000
      80009C038000800080000C038000800080000C038000800080009FFF80008000
      8000FFFF8000800080009C038000800080000C038000800080000C0380008000
      C0019FFFC001C001FFFFFFFFFFFFFFFFFFFFFFFFC001C0010381FE0980008000
      0381F6D6800080000381C31780008000038196D6800080000381BF1980008000
      0001BFFF800080000001FFFF800080008003803F80008000C007B5DF80008000
      C107C5DF80008000C107B5DF80008000E38FC43F80008000E38FFDFF80008000
      FFFFFDFFC001C001FFFFF9FFFFFFFFFFF8FFFE00FC00FFFFF07FFE00FC00FFFF
      F063FE008000807FF241FE000000803FF041FE000000801FF81100000000800F
      FC0300000000C007FE0700000000E003FC3F00010000F001F83F00030001F801
      F03F00070003FC01E23F007F0007FE01E63F007F0007FF01EE3F00FF0007FFFF
      FE7F01FF800FFFFFFEFF03FFF87FFFFFFFFFC000FFFFFFFF80038000800FFF3F
      800380000007FF1F800380000003FF8F800380000001FFC7800380000001FFC3
      800380000001FFE380038000000180E180038000800181E180038000800181E1
      C1FE8000E00180C1E3FE8000E0078001FFF58000F0039003FFF38000F803B807
      FFF18000F803FC1FFFFF8000FFFFFFFFFFFF8001E000FC00FE3F0000E000F800
      F81F0000E000E000E00F0000C000C00080070000800080000003000000008000
      00010000000000000000000000000000000100008000000080010000C0000000
      C0018001E0000001E000C003E0008003F000E001E0008003F803F00CE001C007
      FC0FF81EE003E00FFE3FFC3FE007F83F00000000000000000000000000000000
      000000000000}
  end
  object ilHotImages: TImageList
    Left = 328
    Top = 152
    Bitmap = {
      494C01012A002C00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000B0000000010020000000000000B0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000311000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000004A3018000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000311000003110000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000004A3018004A30180000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003110000031100000311000003110
      0000311000003110000031100000311000003110000031100000311000003110
      00003110000031100000311000003110000000000000EFBE94009C6139009C61
      39009C6139009C6139009C6139004A3018004A3018004A3018007B4931008451
      3100945939009C6139009C6139009C6139000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7D7CE00E7D7CE00E7D7CE00E7D7
      CE00E7D7CE00E7D7CE00E7CFC6003110000042FFFF0042FFFF0031100000A58E
      7B00C6AE9C00DEC7BD00E7D7C600E7D7CE00EFBE9400E7AE8400E7AE8400E7AE
      8400E7AE8400E7AE8400DEAE84004A3018005AEFFF005AEFFF004A301800AD79
      6300C6967300D6A67B00DEAE8400E7AE84000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7D7CE00C69E840031100000E7D7
      CE00E7D7CE00E7D7CE009C796B00A5866B0042FFFF0042FFFF00311000006338
      2900634129006B4129006B4931006B493100E7B68C00FFFFFF00FFFFFF00E7B6
      8C00E7B68C007B0000007B000000A58E7B005AEFFF005AEFFF004A3018006B10
      0800731008007B0800007B000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7D7CE00DEC7BD00C69E8C00E7D7
      CE00E7D7CE00A58E7B00BDA68C00BDA69400A5866B0042FFFF0042FFFF003110
      000094796300AD968400C6AE9C00CEB6A500E7BE9C0073412900FFFFFF00E7BE
      9C00E7BE9C00840000005A9EFF005A96F700A58E7B005AEFFF005AEFFF004A30
      18005271AD005286CE005A96EF005A96FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7D7CE00E7D7CE00E7D7CE00E7D7
      CE00E7D7CE00AD968400D6BEAD00D6BEB500A5866B0042FFFF0042FFFF003110
      000094796B00AD968400C6AEA500D6BEB500EFC7A500EFC7A500EFC7A500EFC7
      A500EFC7A5008400000063A6FF0063A6FF00A58E7B005AEFFF005AEFFF004A30
      18005A719C005A86BD006396DE0063A6F7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7D7CE00C69E840031100000E7D7
      CE00E7D7CE00B5968400EFD7CE00EFD7CE00E7D7C600A5866B0042FFFF0042FF
      FF0031100000AD968400C6B6A500DECFBD00EFCFAD00FFFFFF00FFFFFF00EFCF
      AD00EFCFAD00840000006BB6FF006BB6FF006BAEF700A58E7B005AEFFF005AEF
      FF004A3018006386AD006B96CE006BA6EF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7D7CE00DEC7BD00C69E8C00E7D7
      CE00E7D7CE00B59E8C00EFDFD600EFE7DE00EFDFD600A5866B0042FFFF0042FF
      FF0031100000A58E7B00BDA69400D6C7BD00F7D7BD0073412900FFFFFF00F7D7
      BD00F7D7BD00840000007BBEFF007BBEFF007BBEFF00A58E7B005AEFFF005AEF
      FF004A3018006B869C006B96BD0073AEDE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7D7CE00E7D7CE00E7D7CE00E7D7
      CE00E7D7CE00B59E8C00E7DFCE00F7E7DE00F7E7E700EFE7DE00A5866B0042FF
      FF0042FFFF0031100000B59E8C00CEBEB500F7DFC600F7DFC600F7DFC600F7DF
      C600F7DFC6008400000084CFFF0084C7FF0084C7FF0084C7F700A58E7B005AEF
      FF005AEFFF004A3018007396AD007BAECE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7D7CE00C69E840031100000E7D7
      CE00E7D7CE00B59E8C00E7D7CE00EFDFDE00F7EFE700FFEFEF00A5866B0042FF
      FF0042FFFF0031100000AD968C00BDAEA500F7DFCE00FFFFFF00FFFFFF00F7DF
      CE00F7DFCE00840000008CCFFF008CCFFF008CCFFF008CCFFF00A58E7B005AEF
      FF005AEFFF004A301800738E9C007B9EBD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7D7CE00DEC7BD00C69E8C00E7D7
      CE00E7D7CE00E7D7CE00B59E8C00B59E8C00B59E8C00B59E8C00B59E8C00A586
      6B0042FFFF0042FFFF00311000008C695200FFE7D60073412900FFFFFF00FFE7
      D600FFE7D60084000000840000008400000084000000840000007B000000A58E
      7B005AEFFF005AEFFF004A3018006B1008000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7D7CE00E7D7CE00E7D7CE00E7D7
      CE00E7D7CE00E7D7CE00E7D7CE00E7D7CE00E7D7CE00E7D7CE00E7D7C600A586
      6B0042FFFF0031100000311000009C797300EFBE9400FFEFDE00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00FFEFDE00FFEFDE00FFEFDE00FFE7DE00A58E
      7B005AEFFF004A3018004A301800B59E8C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AD968400AD968400AD968400AD96
      8400AD968400AD968400AD968400AD968400AD968400AD968400AD968400AD96
      8400A5866B00EFE7DE00EFE7DE003110000000000000EFBE9400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7
      F700A58E7B00E7D7D600E7D7D6004A3018000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A5866B00EFE7DE00EFE7DE00311000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A58E7B00E7D7D600E7D7D6004A3018000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A5866B00A5866B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A58E7B00A58E7B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000522818005228
      1800522818005228180052281800522818005228180052281800522818005228
      1800522818005228180052281800522818000000000000000000000000000000
      0000000000000000000000000000310800000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000310800000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000310800000000000000000000000000000000
      00000000000000000000000000000000000000000000C6555200C67D7B00BD75
      730052412900AD9E9400B5AAA500BDB2AD00BDB2AD00BDB2AD00BDB2AD007359
      4A007B2829007B2829007B282900522818000000000000000000000000000000
      0000000000000000000000000000310800003108000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000310800003108000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000310800003108000000000000000000000000
      00000000000000000000000000000000000000000000C6555200CE8A8400C67D
      7B0052412900180C080042342900FFFFFF00FFFFFF00FFFFFF00EFDFD6007359
      4A007B2C29007B2829007B282900522818003108000031080000310800003108
      0000310800003108000031080000310800003108000031080000310800003108
      0000310800003108000031080000310800003108000031080000310800003108
      0000310800003108000031080000310800003108000031080000310800003108
      0000310800003108000031080000310800003108000031080000310800003108
      0000310800003108000031080000310800003108000031080000310800003108
      00003108000031080000310800003108000000000000C6555200D6928C00CE8A
      8400524129000000000021100800FFFFFF00F7EFEF00EFDFD600DEBAAD007359
      4A00843431007B2C29007B28290052281800FFFFFF00E7D7C600E7D7C600E7D7
      C600E7D7C600E7CFC600DECFBD003108000042F7FF0042F7FF0031080000A58E
      7B00BDA69C00D6C7B500E7CFC600E7CFC600FFFFFF00E7CFC600E7CFC600E7CF
      C600E7CFC600E7CFC600DECFBD003108000042F7FF0042F7FF0031080000A58E
      7B00BDA69C00D6C7B500DECFC600E7CFC600A58E7B00FFEFE700FFEFE700FFEF
      E700FFEFE700FFEFE700F7E7DE003108000042F7FF0042F7FF0031080000B59E
      8C00D6BEB500EFDFD600F7E7E700FFEFE70000000000C6555200DE9E9400D692
      8C00524129005241290052412900524129005241290052412900524129005241
      29008C3C3900843431007B2C290052281800FFFFFF00E7D7C600E7D7C600E7D7
      C600E7D7C600E7CFC600E7CFC6009C796B0042F7FF0042F7FF00310800003108
      000031080000310800003108000031080000FFFFFF00E7CFC600E7CFC600E7CF
      C600E7CFC600E7CFC600310800009C796B0042F7FF0042F7FF00310800003108
      000031080000310800003108000031080000A58E7B00FFEFE700310800003108
      000031080000FFEFE700310800009C866B0042F7FF0042F7FF00310800003108
      00003108000031080000310800003108000000000000C6555200E7AAA500E7A2
      9C00D6969400CE8E8C00C6827B00BD757300BD4942009C797B00843431009C51
      4A00944542008C3C39008434310052281800FFFFFF00E7D7C600310800003108
      00003108000031080000E7CFC600FFFFFF009C796B0042F7FF0042F7FF003108
      0000A58E7B00BDA69C00D6C7B500E7CFC600FFFFFF00E7CFC600310800003108
      000031080000E7CFC600FFFFFF00DEC7BD009C796B0042F7FF0042F7FF003108
      0000A58E7B00BDA69C00D6C7B500DECFC600A58E7B00FFEFE700FFEFE700FFEF
      E700FFEFE700FFEFE700A58E7B00F7E7DE009C866B0042F7FF0042F7FF003108
      0000B59E8C00D6BEB500EFDFD600F7E7E70000000000C6555200EFB2AD00E7AA
      A500E7A29C00D6969400CE8E8C00C6827B00BD494200CEA28C0084343100AD5D
      5A009C514A00944542008C3C390052281800FFFFFF00E7D7C600FFFFFF003108
      0000FFFFFF0031080000E7CFC600FFFFFF009C796B0042F7FF0042F7FF003108
      00009C796B00B5968C00CEB6AD00DECFBD00FFFFFF00E7CFC600FFFFFF00E7CF
      C60031080000E7CFC600FFFFFF00DECFC6009C796B0042F7FF0042F7FF003108
      000094796B00AD968C00CEB6AD00DEC7BD00A58E7B00FFEFE700310800003108
      000031080000FFEFE700A58E7B00F7E7E7009C866B0042F7FF0042F7FF003108
      0000A58E8400C6AE9C00DECFC600F7E7DE0000000000C6555200EFB2AD00EFB2
      AD00C6827B00BD494200BD494200BD494200A5716300CEA69400CEA28C008434
      310084343100A55552009C4D4A0052281800FFFFFF00E7D7C600FFFFFF003108
      0000FFFFFF0031080000E7D7C600FFFFFF00DECFBD009C796B0042F7FF0042F7
      FF0031080000A58E7B00BDA69C00D6C7B500FFFFFF00E7CFC600FFFFFF00E7CF
      C60031080000E7CFC600FFFFFF00E7CFC600DEC7BD009C796B0042F7FF0042F7
      FF0031080000A58E7B00BDA69C00D6C7B500A58E7B00FFEFE700FFEFE700FFEF
      E700FFEFE700FFEFE700A58E7B00FFEFE700F7E7DE009C866B0042F7FF0042F7
      FF0031080000B59E8C00D6BEB500EFDFD60000000000C6555200EFB2AD00BDA6
      A500B56D6300CEAE9400DEC3A500DEC7A5002941D600847DB500CEA69400CEA2
      8C008C554A0084343100A555520052281800FFFFFF00E7D7C600FFFFFF003108
      0000FFFFFF0031080000E7D7C600FFFFFF00E7CFC6009C796B0042F7FF0042F7
      FF00310800009C796B00B5968C00CEB6AD00FFFFFF00E7CFC600FFFFFF00E7CF
      C60031080000E7CFC600FFFFFF00E7CFC600DECFC6009C796B0042F7FF0042F7
      FF003108000094796B00AD968C00CEB6AD00A58E7B00FFEFE700310800003108
      000031080000FFEFE700A58E7B00FFEFE700F7E7E7009C866B0042F7FF0042F7
      FF0031080000A58E8400C6AE9C00DECFC60000000000C6555200EFB2AD00B56D
      6300E7D3B500E7DBBD00E7DBBD00E7CFB5009C96B500BDA6A500CEAE9400CEA2
      8C00BD8A7B0084514A008434310052281800FFFFFF00E7D7C600FFFFFF003108
      0000FFFFFF0031080000E7D7C600FFFFFF00E7CFC600DECFBD009C796B0042F7
      FF0042F7FF0031080000A58E7B00BDA69C00FFFFFF00E7CFC600FFFFFF00E7CF
      C60031080000E7CFC600FFFFFF00E7CFC600E7CFC600DEC7BD009C796B0042F7
      FF0042F7FF0031080000A58E7B00BDA69C00A58E7B00FFEFE700FFEFE700FFEF
      E700FFEFE700FFEFE700A58E7B00FFEFE700FFEFE700F7E7DE009C866B0042F7
      FF0042F7FF0031080000B59E8C00D6BEB50000000000C6555200B56D6300DEC3
      A500EFE3C600EFE7C600EFE3C600E7D7B5007B7DC6007371BD00CEAE9C00CEA6
      9400C69A8400AD7163008434310052281800FFFFFF00E7D7C600FFFFFF003108
      0000FFFFFF0031080000E7D7C600FFFFFF00E7CFC600E7CFC6009C796B0042F7
      FF0042F7FF00310800009C796B00AD968C00FFFFFF00E7CFC600FFFFFF00FFFF
      FF0031080000E7CFC600FFFFFF00E7CFC600E7CFC600DECFC6009C796B0042F7
      FF0042F7FF00310800009C796B00AD968C00A58E7B00FFEFE700310800003108
      000031080000FFEFE700A58E7B00FFEFE700FFEFE700F7E7E7009C866B0042F7
      FF0042F7FF0031080000AD8E8400BDAE9C0000000000C6555200B56D6300DEC7
      AD00EFE7C600EFEBC600EFE3C600E7D7B500C6B6B500182CDE00A592A500CEA6
      9400C69A8400AD7163008434310052281800FFFFFF00E7D7C600E7D7C600E7D7
      C600E7D7C600E7D7C600E7D7C600FFFFFF00FFFFFF00FFFFFF00FFFFFF009C79
      6B0042F7FF0042F7FF0031080000BDAEA500FFFFFF00E7CFC600E7CFC600E7CF
      C600E7CFC600E7CFC600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009C79
      6B0042F7FF0042F7FF0031080000BDAEA500A58E7B00FFEFE700FFEFE700FFEF
      E700FFEFE700FFEFE700A58E7B00A58E7B00A58E7B00A58E7B00A58673009C86
      6B0042F7FF0042F7FF00310800007B594A0000000000C6555200EFB2AD00B56D
      6300E7DBB500EFDFBD00C6BEC6000824E700BDB2B5000820E7008C79AD00CEA6
      8C00BD8A7B00945552008434310052281800FFFFFF00E7D7C600E7D7C600E7D7
      C600E7D7C600E7D7C600E7D7C600E7D7C600E7D7C600E7CFC600E7CFC6009C79
      6B0042F7FF00310800003108000094796B00FFFFFF00E7CFC600E7CFC600E7CF
      C600E7CFC600E7CFC600E7CFC600E7CFC600E7CFC600E7CFC600DECFC6009C79
      6B0042F7FF00310800003108000094796B00A58E7B00FFEFE700FFEFE700FFEF
      E700FFEFE700FFEFE700FFEFE700FFEFE700FFEFE700FFEFE700F7E7E7009C86
      6B0042F7FF003108000031080000A58E7B0000000000C6555200EFB2AD00E7CF
      C600B56D6300D6B29C00CEBAAD006369C6003949CE004A55BD00B5928C00BD8A
      7B009C5D4A00843431000000000052281800FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF009C796B00EFDFD600EFDFD60031080000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF009C796B00EFDFD600EFDFD60031080000A58E7B00A58E7B00A58E7B00A58E
      7B00A58E7B00A58E7B00A58E7B00A58E7B00A58E7B00A58E7B00A58E7B00A586
      73009C866B00EFDFDE00EFDFDE003108000000000000C6555200EFB2AD00BD49
      4200DEBAAD00B56D6300B5826B00B5826B00B57D6B00B5796B00B5796B00BD8E
      840084343100BD494200BD716B00522818000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009C796B00EFDFD600EFDFD600310800000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009C796B00EFDFD600EFDFD600310800000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009C866B00EFDFDE00EFDFDE003108000000000000C6555200C6555200C655
      5200C6555200C6555200C6555200C6555200C6555200C6555200C6555200C655
      5200C6555200C6555200C6555200C65552000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C796B009C796B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C796B009C796B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C866B009C866B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004A2C18004A2C
      18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C
      18004A2C18004A2C18004A2C1800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000004573000045
      7300004573000045730000457300004573000045730000457300004573000045
      73000045730000000000000000000000000000000000B59A8C00F7EBE700A58A
      7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A
      7B00A58A7B000004A500A58A7B004A2C18000000000000000000000000000000
      0000947563006B5139006B513900421C1800421C1800421C1800421C1800421C
      1800421C1800421C1800421C180000000000000000006B798400004573000045
      7300004573000045730000457300004573000045730000457300004573000045
      730000457300004573000045730000000000000000000075BD0084CFF7000075
      BD000075BD000075BD000075BD000075BD000075BD000075BD000075BD000075
      BD000075BD0000457300000000000000000000000000B59A8C00F7F3EF00F7EB
      E700EFE7E700EFE3DE00E7DFD600E7D7CE00E7D3CE00DECBC600DEC7BD00D6C3
      B500D6BEB5000008BD00A58A7B004A2C18000000000000000000947563009475
      6300A58A7B00A58A7B00A58A7B0094756300421C180000000000000000000000
      000000000000000000000000000000000000000000006B79840073EFF7000079
      BD000079BD000079BD000079BD000079BD000079BD000079BD000079BD000079
      BD000079BD000079BD000045730000000000000000000075BD00CEEBFF005AC3
      F7005ABEF70052BAF7003979940029282900296D840031AAEF0029A6EF0029A2
      EF000075BD0000457300000000000000000000000000B59A8C00FFF7F700841C
      0000841C0000EFE7E700841C0000841C0000E7D7CE00841C0000841C0000001C
      F7000018EF000010D6000008BD000008AD000000000094756300A58A7B00A58A
      7B00D6C3BD00D6C3BD00FFF7F700A58A7B0094756300421C1800000000000000
      000000000000000000000000000000000000000000006B7984007BF3F70073EF
      F7006BEBF70063E3F7005AE3F7005ADBF70052D7F7004AD3F70042CFF70042CB
      EF0039C7EF000079BD000045730000000000000000000075BD00CEEBFF006BC7
      F70063C3F7005ABEF70052BAF700292829004AB6F70042B2EF0039AAEF0031A6
      EF000075BD0000457300000000000000000000000000B59A8C00FFFBFF00FFF7
      F700F7F3EF00F7EBE700EFE7E700EFE3DE00E7DFD600E7D7CE00E7D3CE00DECB
      C600841C00000018EF00A58A7B004A2C180000000000A58A7B00D6C3BD00EFE7
      E700E7DBD600DECFC600D6C3BD00FFFFFF00A58A7B0094756300421C18000000
      000000000000000000000000000000000000000000006B7984007BF3F7007BF3
      F70073EFF7006BEBF70063E3F7005AE3F7005ADBF70052D7F7004AD3F70042CF
      F70042CBEF000079BD000045730000000000000000000075BD00CEEBFF006BCB
      F7006BC7F70063C3F7002928290029282900292829004AB6F70042B2EF0039AA
      EF000075BD0000457300000000000000000000000000B59A8C00FFFFFF00841C
      0000FFFBF700DEBEAD008C2C0800841C00009C492900841C0000841C0000E7D3
      CE00DECBC600001CF700A58A7B004A2C180000000000A58A7B00FFFFFF00F7F3
      F700EFE7E700E7DBD600DECFC600D6C3BD00FFFFFF00A58A7B0094756300421C
      180000000000000000000000000000000000000000006B7984007BF3F7007BF3
      F7007BF3F70073EFF7006BEBF70063E3F7005AE3F7005ADBF70052D7F7004AD3
      F70042CFF7000079BD000045730000000000000000000075BD00CEEBFF0073CF
      FF006BCBF7006BC7F7004296AD00292829004296AD0052BAF7004AB6F70042B2
      EF000075BD0000457300000000000000000000000000B59A8C00FFFFFF00841C
      0000FFFFFF00A5513100841C0000E7CFC600DEC3B500841C0000841C0000E7DB
      D600841C0000DECBC600A58A7B004A2C18000000000000000000A58A7B00FFFF
      FF00F7F3F700EFE7E700E7DBD600DECFC600D6C3BD00FFFFFF00A58A7B009475
      6300421C1800000000000000000000000000000000006B7984007BF3F7007BF3
      F7007BF3F7007BF3F70073EFF7006BEBF70063E3F7005AE3F7005ADBF70052D7
      F7004AD3F7000079BD000045730000000000000000000075BD00CEEBFF007BD3
      FF0073CFFF006BCBF7006BC7F70063C3F7005ABEF70052BAF70052BAF7004AB6
      F7000075BD0000457300000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00AD5D4200841C0000CE9E8C00EFE3DE00841C0000841C0000EFDF
      DE00841C0000E7D7CE00A58A7B004A2C1800000000000000000000000000A58A
      7B00FFFFFF00F7F3F700EFE7E700E7DBD600DECFC600D6C3BD00FFFFFF00A58A
      7B0094756300421C18000000000000000000000000006B7984007BF3F7007BF3
      F7007BF3F7007BF3F7007BF3F70073EFF7006BEBF70063E3F7005AE3F7005ADB
      F70052D7F7000079BD000045730000000000000000000075BD0084CFF700CEEB
      FF00CEEBFF00CEEBFF00CEEBFF00CEEBFF00CEEBFF00CEEBFF00CEEBFF00CEEB
      FF0084CFF70000457300000000000000000000000000B59A8C00FFFFFF00841C
      0000FFFFFF00F7E7E700B5694A009434100084200000841C0000841C0000EFE3
      DE00EFDFDE00E7DBD600A58A7B004A2C18000000000000000000000000000000
      0000A58A7B00FFFFFF00F7F3F700EFE7E700E7DBD600DECFC600D6C3BD00FFFF
      FF00A58A7B00947563004A2C180000000000000000006B7984007BF3F7007BF3
      F7007BF3F7007BF3F7007BF3F7007BF3F70073EFF7006BEBF7006BE7F70063E3
      F7005ADFF7000079BD00004573000000000000000000000000000075BD000075
      BD000075BD000075BD000075BD000075BD000075BD000075BD000075BD000075
      BD000075BD0000000000000000000000000000000000B59A8C00FFFFFF00841C
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7CBBD00841C00008C280800EFEB
      E700841C0000EFDFDE00A58A7B004A2C18000000000000000000000000000000
      000000000000A58A7B00FFFFFF00F7F3F700EFE7E700E7DBD600DECFC600D6C3
      BD00FFFFFF00A58A7B006B51390000000000000000006B7984007BF3F7007BF3
      F7007BF3F7007BF3F7007BF3F7007BF3F7007BF3F70073EFF7006BEBF7006BE7
      F70063E3F7005ADFF70000457300000000000000000000000000000000000075
      BD00CEEBFF00004573000000000000000000000000000075BD0042BAF7000045
      73000000000000000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF008C2C0800841C0000841C000094341000D6A69400F7EF
      EF00841C0000EFE3DE00A58A7B004A2C18000000000000000000000000000000
      00000000000000000000A58A7B00FFFFFF00F7F3F700EFE7E700E7DBD600DECF
      C600D6C3BD00FFFFFF006B51390000000000000000006B7984006B7984006B79
      84006B7984006B7984006B7984006B7984006B798400841C0000841C0000841C
      0000841C00006B7984006B798400000000000000000000000000000000000075
      BD00CEEBFF00004573000000000000000000000000000075BD0042BAF7000045
      73000000000000000000000000000000000000000000B59A8C00FFFFFF00841C
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBF700F7F3
      F700F7EFEF00EFEBE700A58A7B004A2C18000000000000000000000000000000
      0000000000000000000000000000A58A7B00FFFFFF00FFFBF700F7EFEF00E7E3
      DE00FFFBFF00D6C3BD009475630000000000000000006B7984007BF3F7007BF3
      F7007BF3F7007BF3F7006B7984000000000000000000841C0000DE8E6B00841C
      0000000000000000000000000000000000000000000000000000000000000075
      BD00CEEBFF0084CFF70000457300004573000045730042BAF70042BAF7000045
      73000000000000000000000000000000000000000000B59A8C00FFFFFF00841C
      0000FFFFFF00841C0000841C0000FFFFFF00841C0000841C0000FFFFFF00841C
      0000841C0000F7F3EF00A58A7B004A2C18000000000000000000000000000000
      000000000000000000000000000000000000A58A7B00FFFFFF00FFFBF700FFFB
      FF00D6C3BD0094756300000000000000000000000000000000006B7984006B79
      84006B7984006B798400000000000000000000000000841C0000841C0000AD55
      2100000000000000000000000000000000000000000000000000000000000000
      00000075BD00CEEBFF00CEEBFF00CEEBFF00CEEBFF0042BAF700004573000000
      00000000000000000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00FFF7F700F7F3EF004A2C18000000000000000000000000000000
      00000000000000000000000000000000000000000000A58A7B00A58A7B00A58A
      7B00A58A7B000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000841C000000000000841C
      0000AD55210000000000AD552100841C00000000000000000000000000000000
      0000000000000075BD000075BD000075BD000075BD000075BD00000000000000
      0000000000000000000000000000000000000000000000000000B59A8C00B59A
      8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A
      8C00B59A8C00B59A8C00B59A8C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000841C0000841C0000841C0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000844542004A1C18004A1C18004A1C180000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000004A2C18004A2C180000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AD49080073280800732808007328080073280800732808007328
      0800732808007328080073280800732808000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000734542004A1C
      18004A1C18008C3C3100944139008C4139004A1C18004A1C18004A1C18004A1C
      18004A1C18004A1C18004A1C18004A1C18000000000000000000000000000000
      000000000000000000004A2C1800EFE7E700EFE7E7004A2C1800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AD490800FFFFFF00A58A7B00A58A7B00A58A7B00A58A7B00A58A
      7B00A58A7B00A58A7B00A58A7B007328080000000000AD490800732808007328
      0800732808007328080073280800732808007328080073280800732808007328
      08007328080073280800732808007328080000000000734542008C383100AD49
      4200A5494200A54542009C454200944139004A1C1800EF797300EF797300EF79
      7300EF797300EF797300EF7973004A1C18000000000000000000000000000000
      0000000000004A2C1800EFE7E700841C0000841C0000EFE7E7004A2C18000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AD490800FFFFFF00FFFFFF00FFFFFF00FFF7F700F7EBE700EFE3
      DE00E7D7D600DECFC600A58A7B007328080000000000AD490800F7F3F700A58A
      7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A
      7B00A58A7B00A58A7B00A58A7B00732808000000000073454200B54D4A00B54D
      4A00AD494200AD494200A5454200944139004A1C180000300000003000000030
      00000034000010410800EF7973004A1C18000000000000000000000000000000
      00004A2C1800EFE7E700841C0000F76D3100F7612900841C0000EFE7E7004A2C
      180000000000000000000000000000000000000000000000000000000000AD49
      080073280800AD490800FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7F700F7EB
      E700EFE3DE00E7D7D600A58A7B007328080000000000AD490800FFFBFF00F7F3
      F700F7EBEF00EFE7E700EFDFDE00E7DBD600E7D7CE00DECFC600DEC7BD00D6C3
      B500D6C3B500D6BEB500A58A7B00732808000000000073454200BD514A00B54D
      4A00B54D4A00A54542009C413900944139004A1C180000340000003000000030
      00000049000018511000EF7973004A1C18000000000000000000000000004A2C
      1800EFE7E700841C0000B5795A00B5795A00B5795A00B5795A00841C0000EFE7
      E7004A2C1800000000000000000000000000000000000000000000000000AD49
      0800FFFFFF00AD490800FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7
      F700F7EBE700EFE3DE00A58A7B007328080000000000AD490800FFFFFF00FFFB
      FF00F7F3F700F7EBEF00EFE7E700EFDFDE00E7DBD600E7D7CE00DECFC600DEC7
      BD00DEC7BD00D6C3B500A58A7B00732808000000000073454200BD514A00BD51
      4A00B5555200D6A29C00844542008C3C39004A1C180000410000003400000045
      000000650800185D1000EF7973004A1C180000000000000000004A2C1800EFEB
      E700841C0000F7825200FFFFFF00FFFFFF00FFFFFF00FFFFFF0094492900841C
      0000EFE7E7004A2C18000000000000000000000000000000000000000000AD49
      0800FFFFFF00AD490800FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFF7F700F7EBE700EFE3DE007328080000000000AD490800FFFFFF00FFFF
      FF00FFFBFF00F7F3F700F7EBEF00EFE7E700EFDFDE00E7DBD600E7D7CE00DECF
      C600DECFC600DEC7BD00A58A7B00732808000000000073454200C6555200C651
      4A00D6A29C00FFEFEF00D6A29C008C4139004A1C180000490000004100001055
      08000071100018551000EF7973004A1C1800000000004A2C1800EFE7E700841C
      0000F79E6B00F7966300F7825200FFFFFF00FFFFFF0094492900F76D3100F761
      2900841C0000EFE7E7004A2C18000000000000000000AD49080073280800AD49
      0800FFFFFF00AD49080073280800732808007328080073280800732808007328
      08007328080073280800732808007328080000000000AD490800FFFFFF00FFFF
      FF00FFFFFF00FFFBFF00F7F3F700F7EBEF00EFE7E700EFDFDE00E7DBD600E7D7
      CE00E7D7CE00DECFC600A58A7B00732808000000000073454200CE555200C655
      5200C6595200D6A29C00B55552009C4542004A1C1800004D000073651000C68E
      290018590800C68E2900EF7973004A1C1800841C0000F7F3F700BD450000FFB2
      8C00FFA67B00F79E6B00F7966300FFFFFF00FFFFFF0094492900F7753900F76D
      3100F7612900841C0000EFE7E7004A2C180000000000AD490800FFFFFF00AD49
      0800FFFFFF00C6490000E77D1800E77D1800E77D1800E77D1800F7BA8400E77D
      1800F7BA8400E77D18001851F7007328080000000000AD490800FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFF7F700F7EFEF00EFE7E700EFDFDE00E7DB
      D600E7DBD600E7D7CE00A58A7B00732808000000000073454200CE595200CE59
      5200C6555200C6555200BD514A009C4942004A1C180052651000F7AE6300F7AE
      6300F7AE6300F7AE6300EF7973004A1C1800841C0000FFFFFF00BD450000FFB2
      8C00FFB28C00FFA67B00F79E6B00FFFFFF00FFFFFF0094492900F7794200F775
      3900F76D3100841C0000EFE7E7004A2C180000000000AD490800FFFFFF00AD49
      0800FFFFFF00FFFFFF00AD490800AD490800AD490800AD490800AD490800AD49
      0800AD490800AD490800AD4908000000000000000000AD490800FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7F700F7EFEF00F7EBE700EFE3
      DE00EFE3DE00E7DFD600A58A7B00732808000000000073454200D6595A00D659
      5200CE595200C6555200C6555200A54942004A1C1800F7AE6300F7AE6300F7AE
      6300F7AE6300F7AE6300EF7973004A1C180000000000841C0000FFFBF700BD45
      0000FFB28C00FFB28C00FFFFFF00FFFFFF00FFFFFF0094492900F7825200F779
      4200841C0000EFE7E7004A2C18000000000000000000AD490800FFFFFF00AD49
      0800732808007328080073280800732808007328080073280800732808007328
      08007328080073280800000000000000000000000000AD490800FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7F700F7EFEF00F7EB
      E700F7EBE700EFE3DE00A58A7B00732808000000000073454200DE5D5A00D659
      5A00D6595200CE595200C6555200A54942004A1C1800F7AE6300F7AE6300F7AE
      63009C9E8C00108EF700EF7973004A1C18000000000000000000841C0000FFFF
      FF00BD450000FFB28C00FFB28C00B5795A00B5795A00F7966300F7825200841C
      0000EFE7E7004A2C1800000000000000000000000000AD490800FFFFFF00C649
      0000E77D1800E77D1800E77D1800E77D1800F7BA8400E77D1800F7BA8400E77D
      18001851F70073280800000000000000000000000000AD490800FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7F700F7EF
      EF00F7EFEF00F7EBE700A58A7B00732808000000000073454200DE5D5A00DE5D
      5A00D65D5A00D6595A00CE595200A54942004A1C1800F7AE6300F7AE63009C9E
      8C0010A6F700108EF700EF7973004A1C1800000000000000000000000000841C
      0000FFFFFF00BD450000FFB28C00FFFFFF00FFFFFF0094492900841C0000EFE7
      E7004A2C180000000000000000000000000000000000AD490800FFFFFF00FFFF
      FF00AD490800AD490800AD490800AD490800AD490800AD490800AD490800AD49
      0800AD490800000000000000000000000000001CF70084E7F700001CF70084E7
      F700001CF700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7
      F700FFF7F700F7EFEF00F7EBE700732808000000000073454200D65D5A00DE5D
      5A00DE5D5A00D65D5A00D6595A00AD4D42004A1C1800F7AE63009C9E8C0008B6
      F70008B6F70010A6F700EF7973004A1C18000000000000000000000000000000
      0000841C0000FFFFFF00BD450000FFFFFF00FFFFFF00841C0000EFE7E7004A2C
      18000000000000000000000000000000000000000000AD490800732808007328
      0800732808007328080073280800732808007328080073280800732808007328
      08000000000000000000000000000000000084E7F700295DF70000F7F700295D
      F70084E7F7007328080073280800732808007328080073280800732808007328
      08007328080073280800732808007328080000000000000000009C554A007345
      4200B5514A00B5514A00C6555200A54942004A1C180073454200734542007345
      4200734542007345420073454200734542000000000000000000000000000000
      000000000000841C0000FFFBF700BD450000BD450000EFE7E7004A2C18000000
      00000000000000000000000000000000000000000000C6490000E77D1800E77D
      1800E77D1800E77D1800F7BA8400E77D1800F7BA8400E77D18001851F7007328
      080000000000000000000000000000000000001CF70000F7F700FFFFFF0000F7
      F700001CF700E77D1800E77D1800E77D1800E77D1800F7BA8400E77D1800F7BA
      8400E77D18001851F700E77D1800AD4908000000000000000000000000000000
      00009C554A0073454200B5514A00B5514A004A1C180000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000841C0000FFFFFF00F7F3F7004A2C1800000000000000
      0000000000000000000000000000000000000000000000000000AD490800AD49
      0800AD490800AD490800AD490800AD490800AD490800AD490800AD4908000000
      00000000000000000000000000000000000084E7F700295DF70000F7F700295D
      F70084E7F700AD490800AD490800AD490800AD490800AD490800AD490800AD49
      0800AD490800AD490800AD490800000000000000000000000000000000000000
      000000000000000000009C554A00734542007345420000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000841C0000841C000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000001CF70084E7F700001CF70084E7
      F700001CF7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A58A7B004A2C
      18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C
      18004A2C18004A2C18004A2C1800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A58A7B00EFDF
      DE00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A
      7B00A58A7B00A58A7B004A2C180000000000000000000000000084828400C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C60000000000000000006B79840000457300004573000045
      7300004573000045730000457300004573000045730000457300004573000045
      7300000000000000000000000000000000000000000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000A58A7B00EFE3
      DE00EFDFDE00E7DBD600E7D7CE00DECFC600DECBC600D6C7BD00D6BEB500D6BA
      AD00D6BAAD00A58A7B004A2C180000000000000000000000000084828400FFFF
      FF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFF
      FF00FFFFFF00C6C3C60000000000000000006B7984006B798400108AC6000079
      BD000079BD000079BD000079BD000079BD000079BD000079BD000079BD000045
      7300000000000000000000000000000000000000000084828400FFFFFF008482
      840084828400848284008482840084828400FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000A58A7B00F7EB
      E700EFE3DE00EFDFDE00E7DBD600E7D7CE00DECFC600DECBC600D6C7BD00D6BE
      B500D6BAAD00A58A7B004A2C180000000000000000000000000084828400FFFF
      FF0000FFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFF
      FF0000FFFF00C6C3C60000000000000000006B7984006B7984005AAEC60042CF
      F70042CBF70039C7EF0031BEEF0029B6EF0021B2E70021AEE70018A6E7000079
      BD00004573000000000000000000000000000000000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000A58A7B00F7EF
      EF00F7EBE700EFE3DE00EFDFDE00E7DBD600E7D7CE00DECFC600DECBC600D6C7
      BD00D6BEB500A58A7B004A2C180000000000000000000000000084828400FFFF
      FF00FFFFFF00FFFFFF00C6C3C600000000000000000000000000C6C3C600FFFF
      FF00FFFFFF00C6C3C60000000000000000006B7984004AD3F7006B79840052C7
      E7004ACFF70042CBF70039C7EF0031BEEF0029BAEF0029B6E70021AEE70010A2
      E700004573000000000000000000000000000000000084828400FFFFFF008482
      840084828400848284008482840084828400FFFFFF0000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600000000000000000000000000A58A7B00F7F3
      F700F7EFEF00F7EBE700EFE3DE00EFDFDE00E7DBD600E7D7CE00DECFC600DECB
      C600D6C7BD00A58A7B004A2C180000000000000000000000000084828400FFFF
      FF0000FFFF00C6C3C60084000000FF000000840000008400000000000000C6C3
      C60000FFFF00C6C3C60000000000000000006B79840063E3F7006B7984005AB2
      CE004AD3F7004AD3F70042CBF70039C7EF0031BEEF0029BAEF0029B6E70021AE
      E7000079BD000045730000000000000000000000000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600000000000000000000000000A58A7B00FFFB
      FF00F7F3F700F7EFEF00F7EBE700EFE3DE00EFDFDE00E7DBD600E7D7CE00DECF
      C600DECBC600A58A7B004A2C180000000000000000000000000084828400FFFF
      FF00FFFFFF0084828400FF000000FF00000000820000FF000000840000008400
      0000FFFFFF00C6C3C60000000000000000006B7984006BEBF7004AD3F7006B79
      840052C7E7004AD3F7004AD3F70042CBF70039C7EF0031BEEF0029BAEF0029B6
      E70018A2DE000045730000000000000000000000000084828400FFFFFF008482
      840084828400FFFFFF000000000000000000000000000000000084000000FFFF
      FF00FFFFFF00FFFFFF00C6C3C600000000000000000000000000A58A7B00FFFF
      FF00FFFBFF00F7F3F700F7EFEF00F7EBE700EFE3DE00EFDFDE00E7DBD600E7D7
      CE00DECFC600A58A7B004A2C180000000000000000000000000084828400FFFF
      FF0000FFFF0084820000C6C3C600848284000082000084820000FF0000008400
      000000FFFF00C6C3C60000000000000000006B79840073F3F7006BEBF7006B79
      84005AB6CE004AD3F7004AD3F7004AD3F70042CBF70039C7F70031BEEF0029BA
      EF0021B2E7000079BD0000457300000000000000000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084828400FFFFFF00FFFFFF0084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600000000000000000000000000A58A7B00FFFF
      FF00FFFFFF00FFFFFF00FFF7F700F7F3EF00F7EBEF00EFE7E700EFE3DE00E7DF
      D600E7D7CE00A58A7B004A2C180000000000000000000000000084828400FFFF
      FF00FFFFFF0084820000FFFFFF00C6C3C6000082000084000000FF0000008400
      0000FFFFFF00C6C3C60000000000000000006B7984007BF3F70073F3F7004AD3
      F7006B7984006B7984006B7984006B7984006B7984006B7984006B7984006B79
      84006B7984006B7984006B798400000000000000000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084828400FFFFFF00848284008400000084000000FFFF
      FF000000840000008400C6C3C600000000000000000000000000A58A7B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFF7F700F7F3EF00F7EBEF00EFE7E700EFE3
      DE00E7DFD600A58A7B004A2C180000000000000000000000000084828400FFFF
      FF0000FFFF0084820000C6C3C60084820000FF00000084820000008200008482
      840000FFFF00C6C3C60000000000000000006B7984007BF3F7007BF3F70073F3
      F7004AD3F7004AD3F7004AD3F7004AD3F7004AD3F7004AD3F7004AD3F7004AD3
      F7006B7984000000000000000000000000000000000084828400848284008482
      840084828400848284008482840084828400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000840000008400C6C3C600000000000000000000000000A58A7B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7F700F7F3EF00F7EBEF00EFE7
      E700A58A7B00A58A7B004A2C180000000000000000000000000084828400FFFF
      FF00FFFFFF00C6C3C6008482000084828400848200000082000084820000C6C3
      C600FFFFFF00C6C3C60000000000000000006B7984007BF3F7007BF3F7007BF3
      F70073F3F7004AD3F7006B7984006B7984006B7984006B7984006B7984006B79
      8400841C0000841C0000841C0000841C00000000000000000000000000000000
      000084828400C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600000000000000000000000000A58A7B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7F700F7F3EF00A58A
      7B004A2C18004A2C18004A2C180000000000000000000000000084828400FFFF
      FF0000FFFF00FFFFFF00C6C3C600848200008482000084820000C6C3C600FFFF
      FF0000FFFF00C6C3C60000000000000000006B7984007BF3F7007BF3F7007BF3
      F7007BF3F7006B79840000000000000000000000000000000000000000000000
      000000000000841C0000DE8E6B00841C00000000000000000000000000000000
      00000000000084828400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000A58A7B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7F700A58A
      7B00EFE7DE00D6BAAD004A2C180000000000000000000000000084828400FFFF
      FF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF000000
      000000000000000000000000000000000000000000006B7984006B7984006B79
      84006B7984000000000000000000000000000000000000000000000000000000
      000000000000AD552100841C0000841C00000000000000000000000000000000
      0000000000000000000084828400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000A58A7B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A58A
      7B00D6BAAD004A2C18000000000000000000000000000000000084828400FFFF
      FF0000FFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFFFF00C6C3
      C600FFFFFF008482840000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000841C0000AD5521000000
      0000AD552100841C000000000000841C00000000000000000000000000000000
      000000000000000000000000000084828400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000A58A7B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A58A
      7B004A2C1800000000000000000000000000000000000000000084828400FFFF
      FF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF00C6C3
      C600848284000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000841C0000841C
      0000841C00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008482840084828400848284008482
      8400000000000000000000000000000000000000000000000000A58A7B00A58A
      7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A
      7B00000000000000000000000000000000000000000000000000848284008482
      8400848284008482840084828400848284008482840084828400848284008482
      84000000000000000000000000000000000000000000000000004A2C18004A2C
      18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C
      18004A2C18004A2C18004A2C18000000000000000000000000004A2C18004A2C
      18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C
      18004A2C18004A2C18004A2C1800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B59A8C00F7EBE700A58A
      7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A
      7B00A58A7B00A58A7B00A58A7B004A2C180000000000B59A8C00F7EBE700A58A
      7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A
      7B00A58A7B00A58A7B00A58A7B004A2C18000000000000000000000000000000
      0000000000000000000084000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B59A8C00F7F3EF00F7EB
      E700EFE7E700EFE3DE00E7DFD600E7D7CE00E7D3CE00DECBC600DEC7BD00D6C3
      B500D6BEB500D6BAAD00A58A7B004A2C180000000000B59A8C00F7F3EF00F7EB
      E700EFE7E700EFE3DE00E7DFD600E7D7CE00E7D3CE00DECBC600DEC7BD00D6C3
      B500D6BEB500D6BAAD00A58A7B004A2C18000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008400000084000000000000000000000000000000B59A8C00FFF7F700F7F3
      EF00F7EBE700EFE7E700EFE3DE00E7DFD600E7D7CE00E7D3CE00DECBC600DEC7
      BD00D6C3B500D6BEB500A58A7B004A2C180000000000B59A8C00FFF7F700F7F3
      EF00F7EBE700841C0000841C0000841C0000E7D7CE00E7D3CE00841C0000841C
      0000841C0000D6BEB500A58A7B004A2C18000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000B59A8C00FFFBFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6C3B500A58A7B004A2C180000000000B59A8C00FFFBFF00FFF7
      F700F7F3EF00F7EBE700841C0000EFE3DE00E7DFD600E7D7CE00E7D3CE00841C
      0000DEC7BD00D6C3B500A58A7B004A2C18000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFBF700F7F3F700F7EFEF00EFE7E700EFE3DE00E7DFD600E7D7CE00E7D3
      CE00DECBC600DEC7BD00A58A7B004A2C180000000000B59A8C00108610000075
      000010860800F7F3F70042A24200841C0000841C0000841C0000841C0000841C
      0000DECBC600DEC7BD00A58A7B004A2C18008482000084820000848200008482
      0000848200008482000084000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000DECBC600A58A7B004A2C180000000000B59A8C00C6E7C6000079
      0000EFF7EF00FFFBF700F7F3F70008820800841C00009CC79400EFDFDE00841C
      0000E7D3CE00DECBC600A58A7B004A2C180084820000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000FFFFFF008400000084000000840000008400
      0000840000008400000084000000840000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008400000084000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFBF700F7F3F700F7EFEF00EFEBE700EFE3DE00EFDF
      DE00E7DBD600E7D7CE00A58A7B004A2C180000000000B59A8C00FFFFFF0052B2
      52008CCB8C00FFFFFF00B5DBAD000075000029962900841C0000EFE3DE00841C
      0000E7DBD600E7D7CE00A58A7B004A2C180084820000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000084000000FFFFFF008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008400000084000000000000000000000000000000B59A8C00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7DBD600A58A7B004A2C180000000000B59A8C00FFFFFF00CEEB
      CE00007500000075000000750000007500008CC78400F7EFEF00841C0000841C
      0000EFDFDE00E7DBD600A58A7B004A2C180084820000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084820000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008400000084000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBF700F7F3F700F7EFEF00EFEB
      E700EFE3DE00EFDFDE00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF0029962900C6E7C600007D0000007D0000EFF3EF00F7F3F700F7EFEF00841C
      0000EFE3DE00EFDFDE00A58A7B004A2C18008482000084820000848200008482
      0000848200008482000084820000848200008482000084820000848200008482
      0000848200008482000084820000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000EFE3DE00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF0094D39400188E18000075000042A64200FFFFFF00FFFBF700F7F3F700F7EF
      EF00EFEBE700EFE3DE00A58A7B004A2C180084820000FFFFFF00848200008482
      0000848200008482000084820000848200008482000084820000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084820000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBF700F7F3
      F700F7EFEF00EFEBE700A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF000882080000750000ADDFAD00FFFFFF00FFFFFF00FFFBF700F7F3
      F700F7EFEF00EFEBE700A58A7B004A2C18008482000084820000848200008482
      0000848200008482000084820000848200008482000084820000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084820000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFB
      FF00FFF7F700F7F3EF00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF005AB65A0008860800FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFB
      FF00FFF7F700F7F3EF00A58A7B004A2C18000000000000000000000000000000
      00000000000084820000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084820000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008400000084000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00FFF7F700F7F3EF004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00FFF7F700F7F3EF004A2C18000000000000000000000000000000
      0000000000008482000084820000848200008482000084820000848200008482
      0000848200008482000084820000000000000000000084000000FFFFFF008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008400000000000000000000000000000000000000B59A8C00B59A
      8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A
      8C00B59A8C00B59A8C00B59A8C00000000000000000000000000B59A8C00B59A
      8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A
      8C00B59A8C00B59A8C00B59A8C00000000000000000000000000000000000000
      00000000000084820000FFFFFF00848200008482000084820000848200008482
      0000848200008482000084820000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008482000084820000848200008482000084820000848200008482
      0000848200008482000084820000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004A2C18004A2C
      18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C
      18004A2C18004A2C18004A2C1800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004A2C18004A2C
      18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C
      18004A2C18004A2C18004A2C18000000000000000000000000004A2C18004A2C
      18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C
      18004A2C18004A2C18004A2C18000000000000000000B59A8C00F7EBE700A58A
      7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A
      7B00A58A7B00A58A7B00A58A7B004A2C180000000000841C0000421C18000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B59A8C00F7EBE700A58A
      7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A
      7B00A58A7B00A58A7B00A58A7B004A2C180000000000B59A8C00F7EBE700A58A
      7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A
      7B00A58A7B00A58A7B00A58A7B004A2C180000000000B59A8C00F7F3EF00F7EB
      E700EFE7E700EFE3DE00E7DFD600E7D7CE00E7D3CE00DECBC600DEC7BD00D6C3
      B500D6BEB500D6BAAD00A58A7B004A2C1800BD450000F7820000C6650000421C
      18000000000000000000000000007B594A007B594A007B594A00735142006341
      31004A2C180000000000000000000000000000000000B59A8C00F7F3EF00F7EB
      E700EFE7E700EFE3DE00E7DFD600E7D7CE00E7D3CE00DECBC600DEC7BD00D6C3
      B500D6BEB500D6BAAD00A58A7B004A2C180000000000B59A8C00F7F3EF00F7EB
      E700EFE7E700EFE3DE00E7DFD600E7D7CE00E7D3CE00DECBC600DEC7BD00D6C3
      B500D6BEB500D6BAAD00A58A7B004A2C180000000000B59A8C00FFF7F700F7F3
      EF00841C0000841C0000841C0000841C0000841C0000841C0000841C0000841C
      0000841C0000D6BEB500A58A7B004A2C1800AD552100F7BA7300F7820000841C
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B59A8C00FFF7F700F7F3
      EF00F7EBE700EFE7E700EFE3DE00E7DFD600E7D7CE00E7D3CE00DECBC600DEC7
      BD00D6C3B500D6BEB500A58A7B004A2C180000000000B59A8C00FFF7F700F7F3
      EF00F7EBE700EFE7E700EFE3DE00E7DFD600E7D7CE00E7D3CE00DECBC600DEC7
      BD00D6C3B500D6BEB500A58A7B004A2C180000000000B59A8C00FFFBFF00FFF7
      F700F7F3EF00F7EBE700EFE7E700EFE3DE00E7DFD600E7D7CE00E7D3CE00DECB
      C600DEC7BD00D6C3B500A58A7B004A2C180000000000AD552100BD4500000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B59A8C00FFFBFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6C3B500A58A7B004A2C180000000000B59A8C00FFFBFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6C3B500A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFBF700E7D3C600A555310084240000841C000084240000A5553900D6BE
      B500DECBC600DEC7BD00A58A7B004A2C18000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFBF700F7F3F700F7EFEF00EFE7E700EFE3DE00E7DFD600E7D7CE00E7D3
      CE00DECBC600DEC7BD00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFBF700F7F3F700F7EFEF00EFE7E700EFE3DE00E7DFD600E7D7CE00E7D3
      CE00DECBC600DEC7BD00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF0094341000841C0000B5795A00EFDFD600E7DBD600AD694A009C45
      2100E7D3CE00DECBC600A58A7B004A2C180000000000841C0000421C18000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B59A8C00FFFFFF000000
      000000000000000000000000000000000000000000000000000000000000E7DB
      D600E7D3CE00DECBC600A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000000000000E7DB
      D600E7D3CE00DECBC600A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00841C0000841C0000F7EFE700F7EFEF00EFEBE700E7D7CE00841C
      0000E7DBD600E7D7CE00A58A7B004A2C1800BD450000F7820000C6650000421C
      18000000000000000000000000007B594A007B594A007B594A00735142006341
      31004A2C180000000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFBF700F7F3F700F7EFEF00EFEBE700EFE3DE00EFDF
      DE00E7DBD600E7D7CE00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFBF700F7F3F700F7EFEF00EFEBE700EFE3DE00EFDF
      DE00E7DBD600E7D7CE00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00841C0000841C0000FFFBF700F7F3F700F7EFEF00EFEBE700841C
      0000EFDFDE00E7DBD600A58A7B004A2C1800AD552100F7BA7300F7820000841C
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B59A8C00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7DBD600A58A7B004A2C180000000000B59A8C00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7DBD600A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00841C0000841C0000FFFFFF00FFFBF700F7F3F700F7EFEF00841C
      0000EFE3DE00EFDFDE00A58A7B004A2C180000000000AD552100BD4500000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBF700F7F3F700F7EFEF00EFEB
      E700EFE3DE00EFDFDE00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBF700F7F3F700F7EFEF00EFEB
      E700EFE3DE00EFDFDE00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00841C0000841C0000FFFFFF00FFFFFF00FFFBF700F7F3F700841C
      0000EFEBE700EFE3DE00A58A7B004A2C18000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B59A8C00FFFFFF000000
      000000000000000000000000000000000000000000000000000000000000F7EF
      EF00EFEBE700EFE3DE00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000000000000F7EF
      EF00EFEBE700EFE3DE00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00F7EFEF00841C0000841C0000F7EBE700FFFFFF00FFFFFF00F7EBE700841C
      0000EFDFD600EFEBE700A58A7B004A2C180000000000841C0000421C18000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBF700F7F3
      F700F7EFEF00EFEBE700A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBF700F7F3
      F700F7EFEF00EFEBE700A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00841C0000841C0000841C0000841C0000FFFFFF00FFFFFF00841C0000841C
      0000841C0000F7F3EF00A58A7B004A2C1800BD450000F7820000C6650000421C
      18000000000000000000000000007B594A007B594A007B594A00735142006341
      31004A2C180000000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFB
      FF00FFF7F700F7F3EF00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFB
      FF00FFF7F700F7F3EF00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00FFF7F700F7F3EF004A2C1800AD552100F7BA7300F7820000841C
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00FFF7F700F7F3EF004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00FFF7F700F7F3EF004A2C18000000000000000000B59A8C00B59A
      8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A
      8C00B59A8C00B59A8C00B59A8C000000000000000000AD552100BD4500000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B59A8C00B59A
      8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A
      8C00B59A8C00B59A8C00B59A8C00000000000000000000000000B59A8C00B59A
      8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A
      8C00B59A8C00B59A8C00B59A8C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004A2C18004A2C
      18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C
      18004A2C18004A2C18004A2C18000000000000000000000000004A2C18004A2C
      18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C18004A2C
      18004A2C18004A2C18004A2C180000000000946542008C5D42007B452900733C
      21006328100063281000000000000000000000000000946542008C5D42007B45
      2900733C21006328100063281000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000510000005100000000000000000000B59A8C00F7EBE700A58A
      7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A
      7B00A58A7B00A58A7B00A58A7B004A2C180000000000B59A8C00F7EBE700A58A
      7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A7B00A58A
      7B00A58A7B00A58A7B00A58A7B004A2C180094654200FFFBFF00DEBAA500BD8E
      7300945939006328100000000000000000000000000094654200FFFBFF00DEBA
      A500BD8E73009459390063281000000000000000000000000000000000000000
      0000841C00000000000000000000000000000000000000000000000000000000
      00000051000000000000000000000051000000000000B59A8C00F7F3EF00F7EB
      E700EFE7E700EFE3DE00E7DFD600E7D7CE00E7D3CE00DECBC600DEC7BD00D6C3
      B500D6BEB500D6BAAD00A58A7B004A2C180000000000B59A8C00F7F3EF00F7EB
      E700EFE7E700EFE3DE00E7DFD600E7D7CE00E7D3CE00DECBC600DEC7BD00D6C3
      B500D6BEB500D6BAAD00A58A7B004A2C180094654200FFFBFF00DEBAA500BD8E
      7300A56542006328100000000000000000000000000094654200FFFBFF00DEBA
      A500BD8E7300A565420063281000000000000000000000000000841C0000841C
      0000841C0000841C000000000000000000000000000000000000000000000000
      00000051000000000000000000000000000000000000B59A8C00FFF7F700F7F3
      EF00841C0000841C0000841C0000841C0000841C0000841C0000A5553100D6B6
      A500D6C3B500D6BEB500A58A7B004A2C180000000000B59A8C00FFF7F700F7F3
      EF00DEB6A500841C0000841C0000841C0000841C0000D6B2A500DECBC600DEC7
      BD00D6C3B500D6BEB500A58A7B004A2C180094654200FFFBFF00DEBAA500BD8E
      7300A56542006328100000000000000000000000000094654200FFFBFF00DEBA
      A500BD8E7300A5654200632810000000000000000000841C0000CE4D18000000
      0000841C00000000000000000000000000000000000000000000000000000000
      00000051000000000000000000000051000000000000B59A8C00FFFBFF00FFF7
      F700F7F3EF00841C0000841C0000E7D3CE00E7DFD600C6927B00841C0000841C
      0000DEC7BD00D6C3B500A58A7B004A2C180000000000B59A8C00FFFBFF00FFF7
      F700F7F3EF00EFDFD600943C1800841C0000D6AA9C00E7D7CE00E7D3CE00DECB
      C600DEC7BD00D6C3B500A58A7B004A2C180094654200FFFBFF00DEBAA500BD8E
      7300A56542006328100000000000000000000000000094654200FFFBFF00DEBA
      A500BD8E7300A5654200632810000000000000000000841C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000510000005100000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFBF700841C0000841C0000EFE7E700EFE3DE00E7DFD600841C0000841C
      0000DECBC600DEC7BD00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFBF700F7F3F700B56D5200841C0000BD8A7300E7DFD600E7D7CE00E7D3
      CE00DECBC600DEC7BD00A58A7B004A2C1800946542008C5131008C5131007B38
      18007B381800632810006328100052281000946542008C5131008C5131007B38
      18007B38180063281000632810000000000000000000841C0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00841C0000841C0000F7EFEF00EFEBE700CEA69400841C00009C49
      2900E7D3CE00DECBC600A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFBF700CE9A8400841C0000AD614200EFE3DE00EFDFDE00E7DB
      D600E7D3CE00DECBC600A58A7B004A2C1800D6B6940094654200E7CFBD00E7CF
      BD00BD8A6B0094593900632810008C51310094654200E7CFBD00E7CFBD00BD8A
      6B00945939006328100073454200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00841C0000841C0000841C0000841C0000841C0000AD5D4200E7CF
      C600E7DBD600E7D7CE00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00E7CBBD00841C000094341000EFEBE700EFE3DE00EFDF
      DE00E7DBD600E7D7CE00A58A7B004A2C18000000000094654200FFFFFF00EFE3
      DE00EFC7B5009459390063281000BD82730094654200FFFFFF00EFE3DE00EFC7
      B500945939007345420000000000000000000000000000000000000000000000
      00000000000000000000292C29004A494A004A494A004A494A00000000000000
      00000000000000000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00841C0000841C0000FFFBF700EFDBCE00AD65420084200000BD8A
      6B00EFDFDE00E7DBD600A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFF7F700841C0000841C0000EFE3DE00EFEBE700EFE3
      DE00EFDFDE00E7DBD600A58A7B004A2C18000000000000000000946542008C51
      31007B45290063281000632810008C513100946542008C5131007B4529006328
      1000632810000000000000000000000000000000000000000000000000000000
      00000000000000000000292C29000000000000000000000000004A494A000000
      00000000000000000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00841C0000841C0000FFFFFF00FFFBF700F7F3EF00841C00008420
      0000EFE3DE00EFDFDE00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0094381800841C0000D6B2A500F7EFEF00EFEB
      E700EFE3DE00EFDFDE00A58A7B004A2C1800000000000000000094654200FFFB
      FF00D6B29C0094593900632810000000000094654200FFFBFF00D6B29C009459
      3900632810000000000000000000000000000000000000000000000000000000
      00000000000000000000292C29000000000000000000000000004A494A000000
      00000000000000000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00841C0000841C0000FFFFFF00FFFFFF00D6A69400841C00008420
      0000EFEBE700EFE3DE00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00BD826300841C0000AD614200F7F3EF00F7EF
      EF00EFEBE700EFE3DE00A58A7B004A2C18000000000000000000946542008C51
      31007B452900632810008C51310000000000946542008C5131007B4529006328
      10008C5131000000000000000000000000000000000000000000000000000000
      00000000000000000000292C29000000000000000000000000004A494A000000
      00000000000000000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00841C0000841C0000841C0000841C0000841C0000841C0000AD614200EFDF
      D600F7EFEF00EFEBE700A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00CE9E8C00841C0000841C0000841C00008C300800F7F3
      F700F7EFEF00EFEBE700A58A7B004A2C18000000000000000000000000009465
      4200FFFBFF006328100000000000000000000000000094654200FFFBFF006328
      1000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000292C29004A494A004A494A004A494A00000000000000
      00000000000000000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFB
      FF00FFF7F700F7F3EF00A58A7B004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFB
      FF00FFF7F700F7F3EF00A58A7B004A2C18000000000000000000000000009465
      4200946542008C51310000000000000000000000000094654200946542008C51
      3100000000000000000000000000000000000000000000000000000000000000
      00000000000000000000292C2900000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00FFF7F700F7F3EF004A2C180000000000B59A8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00FFF7F700F7F3EF004A2C18000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000292C2900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B59A8C00B59A
      8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A
      8C00B59A8C00B59A8C00B59A8C00000000000000000000000000B59A8C00B59A
      8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A8C00B59A
      8C00B59A8C00B59A8C00B59A8C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000292C2900292C2900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BD380800841C0000841C00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D69273007B2C08007B2C08007B2C08007B2C
      08007B2C08007B2C08007B2C08007B2C08000000000000000000000000000000
      00000000000000000000C69694004A2C18004A2C18004A2C18004A2C18004A2C
      18004A2C18004A2C18004A2C18004A2C18000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7652900BD380800BD380800BD380800841C000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D6927300F7DFCE00F7D7C600EFCFBD00EFC7
      AD00EFBEA500E7B69400E7B694007B2C08000000000000000000000000000000
      00000000000000000000C6969400EFE3DE00B5968400B5968400B5968400B596
      8400B5968400B5968400B59684004A2C18000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7652900BD380800E7652900EF8A5A00841C00000000000000000000BD38
      0800BD3808008C30000000000000000000000000000000000000000000000000
      0000000000000000000000000000D6927300F7E7DE00F7DFCE00F7D7C600EFCF
      BD00EFCBB500EFC3A500EFBA9C007B2C08000000000000285200002852000028
      52000028520000285200C6969400F7EBE700EFE3DE00E7DBD600E7D3CE00DECF
      C600DEC7BD00D6BEB500B59684004A2C18000000000084828400000000000000
      0000000000000000000000000000000000008482840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7652900BD38080000000000EF8A5A00841C000000000000BD380800BD38
      08008C3000008C3000008C300000000000000000000000000000000000000000
      0000000000000000000000000000D6927300FFF3EF00C6693900C6693900C669
      3900C6693900C6693900EFC3A5007B2C08000075BD005ACFEF000075BD000075
      BD000075BD000075BD00C6969400F7F3EF00CE822900CE822900CE822900CE82
      2900CE822900DEC7BD00B59684004A2C18000000000000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7652900EF8A5A008C3000008C300000841C000000000000BD380800E765
      2900EF8A5A008C300000841C0000000000000000000000000000000000000000
      0000000000000000000000000000D6927300FFFFFF00FFF7F700FFEFE700F7E3
      D600F7DBCE00F7D3BD00EFCBB5007B2C08000075BD006BD7EF005ACFEF0052C7
      E7004ABEE70042BAE700C6969400FFFFFF00FFF7F700F7EFEF00EFE3E700E7DF
      D600E7D7CE00DECFC600B59684004A2C18000000000000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C6000000000084828400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7652900EF8A5A008C300000841C0000841C0000BD3808000000
      0000E76529008C300000841C000000000000C6A694005A4129005A4129005A41
      29005A4129005A4129005A412900D6927300FFFFFF00C6693900C6693900C669
      3900C6693900C6693900F7D3BD007B2C08000075BD0073DBF7006BD7EF005ACF
      EF0052C7E7004ABEE700C6969400FFFFFF00CE822900CE822900CE822900CE82
      2900CE822900E7D7CE00B59684004A2C1800000000008482840000000000C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C6000000000084828400848284000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E7652900842800008C341000841C0000BD3808008C30
      0000841C0000841C00000000000000000000C6A69400EFE3DE00E7DBD600E7D3
      CE00DECBBD00D6C3B500D6BAAD00D6927300FFFFFF00FFFFFF00FFFFFF00FFF7
      F700FFEFE700F7E3D600F7DBCE007B2C08000075BD007BE3F70073DBF7006BD7
      EF005ACFEF0052C7E700C6969400FFFFFF00FFFFFF00FFFFFF00FFF7F700F7EF
      EF00EFE3E700E7DFD600B59684004A2C1800000000000000000000000000C6C3
      C600FFFFFF00C6C3C600FFFFFF00C6C3C600FFFFFF0000000000848284008482
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000052453900A58A7B008C412100841C0000841C
      0000BD380800000000000000000000000000C6A69400F7EBE700EFE3DE00E7DB
      D600E7D3CE00DECFC600DEC7BD00D6927300FFFFFF00C6693900C6693900FFFF
      FF00D69273009438100094381000943810000075BD007BE3F7007BE3F70073DB
      F7006BD7EF005ACFEF00C6969400FFFFFF00CE822900CE822900CE822900FFF7
      F700F7EFEF00C69294004A2C18004A2C18000000000000000000000000000000
      0000C6C3C600FFFFFF00C6C3C600FFFFFF00C6C3C600FFFFFF00000000008482
      8400848284000000000000000000000000000000000000000000000000000000
      0000000000000000000052453900C6B6AD00BD9E8C0052453900000000000000
      000000000000000000000000000000000000C6A69400F7F3EF00D6926B00D692
      6B00D6926B00D6926B00D6926B00D6927300FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00D6927300EFE7DE0094381000000000000075BD007BE3F7007BE3F7007BE3
      F70073DBF7006BD7EF00C6969400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFF7F700C6929400FFFFFF004A2C18000000000000000000000000000000
      000000000000C6C3C600FFFFFF00C6C3C600FFFFFF00C6C3C600FFFFFF000000
      0000848284008482840084828400000000000000000000000000000000000000
      00000000000052453900D6C3BD0052453900D6BAAD0052453900000000000000
      000000000000000000000000000000000000C6A69400FFFFFF00FFF7F700F7EF
      EF00EFE3E700E7DFD600E7D7CE00D6927300FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E7927300B54D180000000000000000000075BD007BE3F7007BE3F7007BE3
      F7007BE3F70073DBF700C6969400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00C69294004A2C1800000000000000000000000000000000000000
      00000000000000000000C6C3C600FFFFFF00C6C3C600FFFFFF00C6C3C600FFFF
      FF00000000008482840000000000000000000000000000000000000000000000
      000052453900E7DFD6005245390094796300E7D3CE0052453900000000000000
      000000000000000000000000000000000000C6A69400FFFFFF00D6926B00D692
      6B00D6926B00D6926B00D6926B00D6927300D6927300D6927300D6927300D692
      7300D69273000000000000000000000000000075BD007BE3F7007BE3F7007BE3
      F7007BE3F7007BE3F700C6969400C6969400C6969400C6969400C6969400C696
      9400C6969400C696940000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C3C600FFFFFF00C6C3C600FFFFFF00C6C3
      C600FFFFFF008482840000000000000000000000000000000000000000009479
      6300FFFFFF00524539000000000094796300E7DFD60052453900000000000000
      000000000000000000000000000000000000C6A69400FFFFFF00FFFFFF00FFFF
      FF00FFF7F700F7EFEF00EFE3E700E7DFD6004A2C180000000000000000000000
      0000000000000000000000000000000000000075BD007BE3F7007BE3F7000045
      8400002852000028520000285200002852000028520000285200002852000075
      BD00002852000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C3C600FFFFFF00C6C3C600FFFF
      FF00C6C3C600C6C3C60000000000000000000000000000000000000000009479
      630052453900000000000000000094796300F7EBE70052453900000000000000
      000000000000000000000000000000000000C6A69400FFFFFF00D6926B00D692
      6B00FFFFFF00C6A694004A2C18004A2C18004A2C180000000000000000000000
      0000000000000000000000000000000000000075BD007BE3F7007BE3F7000045
      84007BE3F70052C3E7000028520052C3E70031AADE00007DBD00002852000075
      BD00002852000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008482840000000000000000000000
      0000000000008482840084828400000000000000000000000000000000009479
      630000000000000000000000000094796300FFFFFF0052453900000000000000
      000000000000000000000000000000000000C6A69400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00C6A69400EFE7DE004A2C18000000000000000000000000000000
      0000000000000000000000000000000000000075BD007BE3F7007BE3F7007BE3
      F700004584007BE3F700002852000028520052C3E700002852004ABEE7004ABE
      E700002852000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000947963005245390000000000000000000000
      000000000000000000000000000000000000C6A69400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00C6A694004A2C1800000000000000000000000000000000000000
      000000000000000000000000000000000000000000000075BD000075BD000075
      BD000075BD00004584007BE3F7007BE3F700002852000075BD000075BD000075
      BD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000947963000000000000000000000000000000
      000000000000000000000000000000000000C6A69400C6A69400C6A69400C6A6
      9400C6A69400C6A6940000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000045840000458400004584000045840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000522818005228
      1800522818005228180052281800522818005228180052281800522818005228
      1800522818005228180052281800522818000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6555200C67D7B00BD75
      730052412900AD9E9400B5AAA500BDB2AD00BDB2AD00BDB2AD00BDB2AD007359
      4A007B2829007B2829007B28290052281800000000006B4531006B4531006B45
      31006B4531006B4531006B4531006B4531006B4531006B4531006B4531006B45
      3100000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A5381000BD280000000000000000
      0000000000000000000000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000C6555200CE8A8400C67D
      7B0052412900180C080042342900FFFFFF00FFFFFF00FFFFFF00EFDFD6007359
      4A007B2C29007B2829007B282900522818008C716300FFFFFF00A58E7B00A58E
      7B00A58E7B00A58E7B00A58E7B00A58E7B00A58E7B00A58E7B00A58E7B00A58E
      7B006B4531000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C411800CE491800BD2800000000
      0000000000000000000000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000C6555200D6928C00CE8A
      8400524129000000000021100800FFFFFF00F7EFEF00EFDFD600DEBAAD007359
      4A00843431007B2C29007B282900522818008C716300FFFFFF00FFFFFF00FFFF
      FF00FFF7EF00F7EBDE00F7DFCE00F7D7BD00EFCBB50010244200E7BA9400A58E
      7B006B4531006B45310000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009C411800CE4D1800BD28
      0000000000000000000000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000C6555200DE9E9400D692
      8C00524129005241290052412900524129005241290052412900524129005241
      29008C3C3900843431007B2C2900522818008C716300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFF7EF00F7EBDE00F7DFCE00F7D7BD00299A3100EFC3A500A58E
      7B008C716300946142006B453100000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C411800CE49
      1800BD2800000000000000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000C6555200E7AAA500E7A2
      9C00D6969400CE8E8C00C6827B00BD757300B56D6B00AD655A00A55952009C51
      4A00944542008C3C390084343100522818008C716300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFF7EF00F7EBDE00F7DFCE00F7D7BD00EFCBB500EFC3
      A5008C716300A5795A006B453100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D6592900EF71
      3900C6451800C645180000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000C6555200EFB2AD00E7AA
      A500E7A29C00D6969400CE8E8C00C6827B00C6797300BD716B00B5696300AD5D
      5A009C514A00944542008C3C3900522818008C7163008C7163008C7163008C71
      63008C7163008C7163008C7163008C7163008C7163008C7163006B4531006B45
      31008C716300B5866B006B453100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009C41
      1800EF713900BD28000000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000C6555200EFB2AD00EFB2
      AD00BD494200BD494200BD494200BD494200BD494200BD494200BD494200BD49
      4200BD494200A55552009C4D4A00522818008C716300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7EF00F7EBDE00F7DFCE00F7D7
      BD008C716300C69A7B006B4531000000000000000000B54D1800841C0000841C
      0000841C0000841C0000841C0000841C00000000000000000000000000009C41
      1800EF713900D6592900C6552900000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000C6555200EFB2AD00BD49
      4200FFF3EF00F7EBE700EFE3DE00EFDBCE00E7D3C600E7CBBD00DEC3B500DEBA
      AD00DEBAAD00BD494200A555520052281800000000008C7163008C7163008C71
      63008C7163008C7163008C7163008C7163008C7163008C7163008C7163008C71
      6300CEA284007B695A006B4531000000000000000000B54D1800EF713900EF71
      3900EF713900EF713900841C0000000000000000000000000000000000009C41
      1800EF713900D6592900BD280000000000000000000084828400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084828400000000000000000000000000C6555200EFB2AD00BD49
      4200FFFFFF00FFF7F700F7EFEF00F7E3DE00EFDFD600E7D7CE00E7CFC600DEC7
      BD00DEBEAD00BD494200AD5D5A005228180000000000C6929400C6929400FFFF
      FF00FFFFFF00FFFBFF00FFF7F700F7F3EF00F7EFEF00EFEBE700EFE3DE006B45
      310094796300CEA284006B4531000000000000000000B54D1800EF713900DE75
      4A00DE714200DE693900841C0000000000000000000000000000000000009C41
      1800EF713900D6592900BD28000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000008400000000000000C6555200EFB2AD00BD49
      4200FFFFFF00FFFFFF00FFF7F700F7EFEF00F7E3DE00EFDFD600E7D7CE00E7CF
      C600DEC7BD00BD494200B569630052281800000000000000000000000000C692
      9400FFFFFF00C6A69400C6A69400C6A69400C6A69400C6A69400EFEBE7006B45
      31006B4531006B4531006B4531000000000000000000B54D1800EF713900F79E
      7300DE754A00DE714200C6552900841C00000000000000000000841C0000F79E
      7300EF713900D6592900C6451800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008400000000000000C6555200EFB2AD00BD49
      4200FFFFFF00FFFFFF00FFFFFF00FFF7F700F7EFEF00F7E3DE00EFDFD600E7D7
      CE00E7CFC600BD494200BD716B0052281800000000000000000000000000C692
      9400FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFF7F700F7F3EF00F7EFEF00EFEB
      E7006B45310000000000000000000000000000000000B54D1800F79E7300B54D
      1800F79E7300DE754A00DE714200C6552900841C0000841C0000F79E7300EF71
      3900EF713900D6592900C6552900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008400000000000000840000000000000000000000C6555200EFB2AD00BD49
      4200FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7F700F7EFEF00F7E3DE00EFDF
      D600E7D7CE00BD494200C67D7B00522818000000000000000000000000000000
      0000C6929400FFFFFF00C6A69400C6A69400C6A69400C6A69400C6A69400F7EF
      EF006B4531006B453100000000000000000000000000B54D1800B54D18000000
      0000B54D1800F79E7300DE754A00DE714200DE6D4200DE693900DE653100EF71
      3900D6592900C645180000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008400000084000000000000000000000000000000C6555200EFB2AD00BD49
      4200FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7F700F7EFEF00F7E3
      DE00EFDFD600BD49420000000000522818000000000000000000000000000000
      000000000000C6929400FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFF7F700F7F3
      EF00F7EFEF006B453100000000000000000000000000B54D1800000000000000
      000000000000B54D1800F79E7300F79E7300F79E7300F79E7300EF713900C645
      1800C65529000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008400000084000000840000000000000000000000C6555200EFB2AD00BD49
      4200FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFF7F700F7EF
      EF00F7E3DE00BD494200BD716B00522818000000000000000000000000000000
      000000000000C6929400C6929400C6929400C6929400C6929400C6929400C692
      9400C6929400C692940000000000000000000000000000000000000000000000
      00000000000000000000F7551800B54D1800B54D1800B54D1800F75518000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6555200C6555200C655
      5200C6555200C6555200C6555200C6555200C6555200C6555200C6555200C655
      5200C6555200C6555200C6555200C65552000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008482
      8400848284008482840084828400848284008482840084828400848284008482
      8400848284008482840084828400848284000000000000000000000000000000
      0000000000000000000084828400848284008482840084828400848284008482
      8400848284008482840084828400848284000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF000000000000000000000000000000000000000000C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600848284000000000000000000000000000000
      00000000000084828400C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600848284000000000000000000000000000000
      0000000000000000000000000000840084008400840084828400000000000000
      00000000000000000000000000000000000000000000FFFFFF000000000000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
      FF00FFFFFF0000000000FFFFFF0000000000000000000000000000000000C6C3
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600848284000000000000000000000000008482
      840084828400FF000000C6C3C600FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFF
      FF0000FFFF00FFFFFF00C6C3C600848284000000000000000000000000000000
      0000000000008400840084008400FFFFFF00FFFFFF00C6C3C600848284000000
      0000000000000000000000000000000000000000000000FFFF00FFFFFF000000
      000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
      FF0000000000FFFFFF0000FFFF00000000000000000000000000FF000000C6C3
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C60084828400000000000000000084828400FF00
      0000FF000000FF000000C6C3C600FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600848284000000000000000000000000008400
      840084008400FFFFFF00FFFFFF000000000000000000C6C3C600C6C3C6008482
      84000000000000000000000000000000000000000000FFFFFF0000FFFF00FFFF
      FF000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF000000
      0000FFFFFF0000FFFF00FFFFFF000000000000000000FF000000848284008482
      8400848284008482840084828400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600848284000000000084828400FF000000FF00
      0000FF000000FF000000C6C3C600FFFFFF0000FFFF0084828400848284008482
      8400C6C3C600FFFFFF00C6C3C60084828400848284008400840084008400FFFF
      FF00FFFFFF000000000000000000840084008400840000000000C6C3C600C6C3
      C600848284000000000000000000000000000000000000FFFF00FFFFFF000000
      0000FFFFFF000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000FFFF000000000084828400FF000000848284008482
      8400FF000000FF000000FF000000FF00000084828400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600848284000000000084828400FF000000FF00
      0000FF000000FF000000C6C3C600FFFFFF0084828400FF000000FF000000FF00
      000084828400FFFFFF00C6C3C600848284008482840084008400FFFFFF000000
      000000000000840084008400840084008400840084008400840000000000C6C3
      C600C6C3C60084828400000000000000000000000000FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF0000000000848284008482840084828400FF00
      0000FF000000C6C3C600C6C3C60084828400FF00000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840084828400FF000000FF000000FF00
      0000FF000000FF000000C6C3C600FFFFFF0084828400C6C3C60084828400FF00
      000084828400FFFFFF00C6C3C600848284008482840000000000000000008400
      840084008400840084000082840000FFFF008400840084008400840084000000
      0000C6C3C600C6C3C60084828400000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00000000000000000084828400C6C3C60084828400FF00
      000084828400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840084828400FF000000FF000000FF00
      00008482840084828400C6C3C600FFFFFF0084828400FFFFFF00C6C3C6008482
      840084828400FFFFFF00C6C3C600848284008482840084008400840084008400
      8400840084008400840084008400008284008400840084008400840084008400
      840000000000C6C3C600000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FF000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000008482840000FFFF00FF00
      0000FF000000FF000000FF000000FF000000FF00000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840084828400FF000000FF0000008482
      84008482840084828400C6C3C600FFFFFF0000FFFF0084828400848284008482
      8400C6C3C600FFFFFF00C6C3C600848284000000000084008400FFFFFF008400
      84008400840084008400840084008400840000FFFF0000FFFF00840084008400
      84008400840000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FF000000FF000000FF00
      0000FFFFFF00FFFFFF00FFFFFF000000000000000000000000008482840000FF
      FF00FF000000FFFFFF00FFFFFF0084828400FF00000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840084828400FF000000FF0000008482
      84008482840084828400C6C3C600FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFF
      FF0084828400848284008482840084828400000000000000000084008400FFFF
      FF0084008400840084008400840084008400840084000082840000FFFF0000FF
      FF00840084008400840000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FF000000FF000000FFFFFF00FF00
      0000FF000000FFFFFF0000000000000000000000000000000000000000008482
      840000FFFF00FF000000FF000000FF00000084828400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840084828400FF000000FF000000C6C3
      C6008482840084828400C6C3C600FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFF
      FF00C6C3C600FFFFFF00C6C3C600000000000000000000000000000000008400
      8400FFFFFF00840084008400840084008400008284008400840000FFFF0000FF
      FF0084008400840084008400840000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FF000000FF000000FFFFFF00FFFFFF00FFFF
      FF00FF000000FF0000000000000000000000000000000000000000000000C6C3
      C600FF000000FF000000FF00000084828400FFFFFF00FF000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840000000000C6C3C600FF000000FF00
      0000FFFFFF00C6C3C600C6C3C600FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFF
      FF00C6C3C600C6C3C60000000000000000000000000000000000000000000000
      000084008400FFFFFF00840084008400840000FFFF0000FFFF0000FFFF008400
      8400840084008400840000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FF000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FF000000FF00000000000000000000000000000000000000C6C3
      C600FFFFFF00FFFFFF00FFFFFF00FF000000FF000000FFFFFF00FFFFFF00FFFF
      FF008482840084828400848284008482840000000000C6C3C600FF000000FFFF
      FF00C6C3C600FFFFFF00C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C6008482840000000000000000000000000000000000000000000000
      00000000000084008400FFFFFF00840084008400840084008400840084008400
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000FF000000FF000000000000000000000000000000C6C3
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00C6C3C600FFFFFF00C6C3C600000000000000000000000000C6C3C600FF00
      0000FF000000C6C3C600FFFFFF00C6C3C6008482840084828400848284008482
      8400848284000000000000000000000000000000000000000000000000000000
      0000000000000000000084008400FFFFFF008400840084008400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000FF000000000000000000000000000000C6C3
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00C6C3C600C6C3C6000000000000000000000000000000000000000000C6C3
      C600C6C3C600FF000000FF000000FFFFFF00C6C3C60084828400848284008482
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840084008400840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C6000000000000000000000000000000000000000000000000000000
      000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000B00000000100010000000000800500000000000000000000
      000000000000000000000000FFFFFF00FEFFFEFF00000000FE7FFE7F00000000
      0000800000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000800000000000
      FFF0FFF000000000FFF9FFF900000000C000FEFFFEFFFEFF8000FE7FFE7FFE7F
      8000000000000000800000000000000080000000000000008000000000000000
      8000000000000000800000000000000080000000000000008000000000000000
      8000000000000000800000000000000080000000000000008000000000000000
      8000FFF0FFF0FFF08000FFF9FFF9FFF9FFFFC001FFFFFFFFC0078000F0018001
      80038000C07F800180038000803F800180038000801F800180038000800F8001
      80038000C007800180038000E003800180038000F0018001C0078000F8018001
      E38F8000FC018001E38F8000FE01818FE00F8000FF03C38FF01F8000FF87FFA4
      F83FC001FFFFFFF1FFFFFFFFFFFFFFFFF87FFE7FF800FFFFC000FC3FF8008000
      8000F81FF80080008000F00FE00080008000E007E00080008000C003E0008000
      8000800180008000800000008000800080000000800180008000800180038000
      8000C003800380008000E007800700008000F00F800F0000C000F81F800F0000
      F07FFC3FC01F0001FC7FFE7FFFFF07FFFFFFFFFFC001C001FFFF803FC001C001
      000F803FC001C001000F803FC001C00100078000C001C00100078000C001C001
      00038000C001C00100038000C001C00100018000C001C00100018000C001C001
      00078000C001C0010000F000C001C00103F8F801C001C00187F8FC03C003C003
      FF92FE07C007C007FFC7FF0FC00FC00FC001C001FFFFFFFF80008000FC00FFFF
      80008000FC00800380008000FC00800380008000FC0080038000800000008003
      8000800000008003800080000000800380008000003F80038000800000018003
      8000800000018003800080000001800380008000F801800380008000F8018003
      C001C001F8018003FFFFFFFFF801FFFFC001FFFFC001C00180009C0380008000
      80000C038000800080000C038000800080009FFF800080008000FFFF80008000
      80009C038000800080000C038000800080000C038000800080009FFF80008000
      8000FFFF8000800080009C038000800080000C038000800080000C0380008000
      C0019FFFC001C001FFFFFFFFFFFFFFFFFFFFFFFFC001C0010381FE0980008000
      0381F6D6800080000381C31780008000038196D6800080000381BF1980008000
      0001BFFF800080000001FFFF800080008003803F80008000C007B5DF80008000
      C107C5DF80008000C107B5DF80008000E38FC43F80008000E38FFDFF80008000
      FFFFFDFFC001C001FFFFF9FFFFFFFFFFF8FFFE00FC00FFFFF07FFE00FC00FFFF
      F063FE008000807FF241FE000000803FF041FE000000801FF81100000000800F
      FC0300000000C007FE0700000000E003FC3F00010000F001F83F00030001F801
      F03F00070003FC01E23F007F0007FE01E63F007F0007FF01EE3F00FF0007FFFF
      FE7F01FF800FFFFFFEFF03FFF87FFFFFFFFFC000FFFFFFFF80038000800FFF3F
      800380000007FF1F800380000003FF8F800380000001FFC7800380000001FFC3
      800380000001FFE380038000000180E180038000800181E180038000800181E1
      C1FE8000E00180C1E3FE8000E0078001FFF58000F0039003FFF38000F803B807
      FFF18000F803FC1FFFFF8000FFFFFFFFFFFF8001E000FC00FE3F0000E000F800
      F81F0000E000E000E00F0000C000C00080070000800080000003000000008000
      00010000000000000000000000000000000100008000000080010000C0000000
      C0018001E0000001E000C003E0008003F000E001E0008003F803F00CE001C007
      FC0FF81EE003E00FFE3FFC3FE007F83F00000000000000000000000000000000
      000000000000}
  end
  object ilDisabledImages: TImageList
    Left = 299
    Top = 152
    Bitmap = {
      494C01012A002C00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000B0000000010020000000000000B0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000181818000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000313031000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000181818001818180000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000313031003130310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001818180018181800181818001818
      1800181818001818180018181800181818001818180018181800181818001818
      18001818180018181800181818001818180000000000C6C7C6006B696B006B69
      6B006B696B006B696B006B696B00313031003130310031303100525152005A59
      5A00636163006B696B006B696B006B696B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00DEDFDE00DEDFDE00DEDF
      DE00DEDFDE00D6D7D600D6D7D60018181800A5A6A500A5A6A500181818009496
      9400B5B6B500CECFCE00D6D7D600D6D7D600C6C7C600B5B6B500B5B6B500B5B6
      B500B5B6B500B5B6B500B5B6B50031303100ADAEAD00ADAEAD00313031008486
      84009C9E9C00ADAEAD00B5B6B500B5B6B5000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00A5A6A50018181800DEDF
      DE00DEDFDE00D6D7D6008486840084868400A5A6A500A5A6A500181818004241
      42004A494A004A494A005251520052515200BDBEBD00FFFFFF00FFFFFF00BDBE
      BD00BDBEBD0042414200424142008C8E8C00ADAEAD00ADAEAD00313031003938
      3900393839004241420042414200424142000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00CECFCE00A5A6A500DEDF
      DE00DEDFDE0094969400A5A6A500ADAEAD0084868400A5A6A500A5A6A5001818
      18007B797B009C9E9C00ADAEAD00B5B6B500BDBEBD004A494A00FFFFFF00BDBE
      BD00BDBEBD0042414200ADAEAD00A5A6A5008C8E8C00ADAEAD00ADAEAD003130
      31007B797B0094969400A5A6A500ADAEAD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00DEDFDE00DEDFDE00DEDF
      DE00DEDFDE0094969400BDBEBD00C6C7C60084868400A5A6A500A5A6A5001818
      18008486840094969400B5B6B500C6C7C600C6C7C600C6C7C600C6C7C600C6C7
      C600C6C7C60042414200ADAEAD00ADAEAD008C8E8C00ADAEAD00ADAEAD003130
      31007B797B008C8E8C009C9E9C00ADAEAD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00A5A6A50018181800DEDF
      DE00DEDFDE009C9E9C00DEDFDE00DEDFDE00D6D7D60084868400A5A6A500A5A6
      A5001818180094969400B5B6B500CECFCE00CECFCE00FFFFFF00FFFFFF00CECF
      CE00CECFCE0042414200B5B6B500B5B6B500B5B6B5008C8E8C00ADAEAD00ADAE
      AD0031303100848684009C9E9C00ADAEAD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00CECFCE00A5A6A500DEDF
      DE00DEDFDE009C9E9C00DEDFDE00E7E7E700E7E7E70084868400A5A6A500A5A6
      A500181818008C8E8C00ADAEAD00CECFCE00D6D7D6004A494A00FFFFFF00D6D7
      D600D6D7D60042414200BDBEBD00BDBEBD00BDBEBD008C8E8C00ADAEAD00ADAE
      AD00313031008486840094969400ADAEAD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00DEDFDE00DEDFDE00DEDF
      DE00DEDFDE00A5A6A500DEDFDE00E7E7E700EFEFEF00E7E7E70084868400A5A6
      A500A5A6A500181818009C9E9C00C6C7C600DEDFDE00DEDFDE00DEDFDE00DEDF
      DE00DEDFDE0042414200C6C7C600C6C7C600BDBEBD00BDBEBD008C8E8C00ADAE
      AD00ADAEAD00313031008C8E8C00A5A6A5000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00A5A6A50018181800DEDF
      DE00DEDFDE00A5A6A500D6D7D600E7E7E700EFEFEF00F7F7F70084868400A5A6
      A500A5A6A500181818009C9E9C00B5B6B500E7E7E700FFFFFF00FFFFFF00E7E7
      E700E7E7E70042414200C6C7C600C6C7C600C6C7C600C6C7C6008C8E8C00ADAE
      AD00ADAEAD00313031008C8E8C009C9E9C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00CECFCE00A5A6A500DEDF
      DE00DEDFDE00DEDFDE00A5A6A500A5A6A500A5A6A500A5A6A5009C9E9C008486
      8400A5A6A500A5A6A500181818006B696B00E7E7E7004A494A00FFFFFF00E7E7
      E700E7E7E7004241420042414200424142004241420042414200424142008C8E
      8C00ADAEAD00ADAEAD0031303100393839000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEDFDE00DEDFDE00DEDFDE00DEDF
      DE00DEDFDE00DEDFDE00DEDFDE00DEDFDE00DEDFDE00D6D7D600D6D7D6008486
      8400A5A6A500181818001818180084868400C6C7C600EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF008C8E
      8C00ADAEAD0031303100313031009C9E9C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009496940094969400949694009496
      9400949694009496940094969400949694009496940094969400949694009496
      940084868400E7E7E700E7E7E7001818180000000000C6C7C600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7
      F7008C8E8C00DEDFDE00DEDFDE00313031000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084868400E7E7E700E7E7E700181818000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008C8E8C00DEDFDE00DEDFDE00313031000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008C8E8C008C8E8C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007B7D7B007B7D
      7B007B7D7B007B7D7B007B7D7B007B7D7B007B7D7B007B7D7B007B7D7B007B7D
      7B007B7D7B007B7D7B007B7D7B007B7D7B000000000000000000000000000000
      0000000000000000000000000000181818000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000181818000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000181818000000000000000000000000000000
      00000000000000000000000000000000000000000000ADAEAD00C6C7C600C6C3
      C60094929400D6D3D600DEDBDE00DEDFDE00DEDFDE00DEDFDE00DEDFDE00ADAA
      AD008486840084868400848684007B7D7B000000000000000000000000000000
      0000000000000000000000000000181818001818180000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000181818001818180000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000181818001818180000000000000000000000
      00000000000000000000000000000000000000000000ADAEAD00CECBCE00C6C7
      C60094929400525152008C8A8C00FFFFFF00FFFFFF00FFFFFF00F7F3F700ADAA
      AD008C8A8C0084868400848684007B7D7B001818180018181800181818001818
      1800181818001818180018181800181818001818180018181800181818001818
      1800181818001818180018181800181818001818180018181800181818001818
      1800181818001818180018181800181818001818180018181800181818001818
      1800181818001818180018181800181818001818180018181800181818001818
      1800181818001818180018181800181818001818180018181800181818001818
      18001818180018181800181818001818180000000000ADAEAD00CECFCE00CECB
      CE0094929400000000005A595A00FFFFFF00FFFBFF00F7F3F700E7E3E700ADAA
      AD00949294008C8A8C00848684007B7D7B00FFFFFF00D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D600CECFCE00181818009C9E9C009C9E9C00181818008C8E
      8C00ADAEAD00C6C7C600D6D7D600D6D7D600FFFFFF00D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D600CECFCE00181818009C9E9C009C9E9C00181818008C8E
      8C00ADAEAD00C6C7C600D6D7D600D6D7D60094969400EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00181818009C9E9C009C9E9C0018181800A5A6
      A500C6C7C600DEDFDE00EFEFEF00EFEFEF0000000000ADAEAD00D6D7D600CECF
      CE00949294009492940094929400949294009492940094929400949294009492
      94009C9A9C00949294008C8A8C007B7D7B00FFFFFF00D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D600D6D7D600848684009C9E9C009C9E9C00181818001818
      180018181800181818001818180018181800FFFFFF00D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D60018181800848684009C9E9C009C9E9C00181818001818
      18001818180018181800181818001818180094969400EFEFEF00181818001818
      180018181800EFEFEF0018181800848684009C9E9C009C9E9C00181818001818
      18001818180018181800181818001818180000000000ADAEAD00DEDFDE00D6D7
      D600D6D3D600CECFCE00CECBCE00C6C3C600A5A2A500C6C3C60094929400A5A6
      A5009C9E9C009C9A9C00949294007B7D7B00FFFFFF00D6D7D600181818001818
      18001818180018181800D6D7D600FFFFFF00848684009C9E9C009C9E9C001818
      18008C8E8C00ADAEAD00C6C7C600D6D7D600FFFFFF00D6D7D600181818001818
      180018181800D6D7D600FFFFFF00CECFCE00848684009C9E9C009C9E9C001818
      18008C8E8C00ADAEAD00C6C7C600D6D7D60094969400EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF008C8E8C00E7E7E700848684009C9E9C009C9E9C001818
      1800A5A6A500C6C7C600DEDFDE00EFEFEF0000000000ADAEAD00DEDFDE00DEDF
      DE00D6D7D600D6D3D600CECFCE00CECBCE00A5A2A500D6D7D60094929400B5B2
      B500A5A6A5009C9E9C009C9A9C007B7D7B00FFFFFF00D6D7D600FFFFFF001818
      1800FFFFFF0018181800D6D7D600FFFFFF00848684009C9E9C009C9E9C001818
      1800848684009C9E9C00BDBEBD00CECFCE00FFFFFF00D6D7D600FFFFFF00D6D7
      D60018181800D6D7D600FFFFFF00D6D7D600848684009C9E9C009C9E9C001818
      1800848684009C9E9C00BDBEBD00CECFCE0094969400EFEFEF00181818001818
      180018181800EFEFEF008C8E8C00EFEFEF00848684009C9E9C009C9E9C001818
      180094969400B5B6B500D6D7D600E7E7E70000000000ADAEAD00DEDFDE00DEDF
      DE00CECBCE00A5A2A500A5A2A500A5A2A500BDBABD00D6D7D600D6D7D6009492
      940094929400ADAAAD00A5A6A5007B7D7B00FFFFFF00D6D7D600FFFFFF001818
      1800FFFFFF0018181800D6D7D600FFFFFF00CECFCE00848684009C9E9C009C9E
      9C00181818008C8E8C00ADAEAD00C6C7C600FFFFFF00D6D7D600FFFFFF00D6D7
      D60018181800D6D7D600FFFFFF00D6D7D600CECFCE00848684009C9E9C009C9E
      9C00181818008C8E8C00ADAEAD00C6C7C60094969400EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF0094969400EFEFEF00E7E7E700848684009C9E9C009C9E
      9C0018181800A5A6A500C6C7C600DEDFDE0000000000ADAEAD00DEDFDE00D6D7
      D600BDBABD00DEDBDE00E7E3E700E7E7E700ADAAAD00CECBCE00D6D7D600D6D7
      D600ADAAAD0094929400ADAAAD007B7D7B00FFFFFF00D6D7D600FFFFFF001818
      1800FFFFFF0018181800D6D7D600FFFFFF00D6D7D600848684009C9E9C009C9E
      9C0018181800848684009C9E9C00BDBEBD00FFFFFF00D6D7D600FFFFFF00D6D7
      D60018181800D6D7D600FFFFFF00D6D7D600D6D7D600848684009C9E9C009C9E
      9C0018181800848684009C9E9C00BDBEBD0094969400EFEFEF00181818001818
      180018181800EFEFEF0094969400EFEFEF00EFEFEF00848684009C9E9C009C9E
      9C001818180094969400B5B6B500D6D7D60000000000ADAEAD00DEDFDE00BDBA
      BD00EFEBEF00EFEFEF00EFEFEF00EFEBEF00D6D7D600DEDBDE00DEDBDE00D6D7
      D600CECBCE00A5A6A500949294007B7D7B00FFFFFF00D6D7D600FFFFFF001818
      1800FFFFFF0018181800D6D7D600FFFFFF00D6D7D600CECFCE00848684009C9E
      9C009C9E9C00181818008C8E8C00ADAEAD00FFFFFF00D6D7D600FFFFFF00D6D7
      D60018181800D6D7D600FFFFFF00D6D7D600D6D7D600CECFCE00848684009C9E
      9C009C9E9C00181818008C8E8C00ADAEAD0094969400EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF0094969400EFEFEF00EFEFEF00E7E7E700848684009C9E
      9C009C9E9C0018181800A5A6A500C6C7C60000000000ADAEAD00BDBABD00E7E3
      E700EFEFEF00F7F3F700EFEFEF00EFEBEF00CECBCE00C6C3C600DEDBDE00D6D7
      D600CECFCE00BDBEBD00949294007B7D7B00FFFFFF00D6D7D600FFFFFF001818
      1800FFFFFF0018181800D6D7D600FFFFFF00D6D7D600D6D7D600848684009C9E
      9C009C9E9C0018181800848684009C9E9C00FFFFFF00D6D7D600FFFFFF00FFFF
      FF0018181800D6D7D600FFFFFF00D6D7D600D6D7D600D6D7D600848684009C9E
      9C009C9E9C0018181800848684009C9E9C0094969400EFEFEF00181818001818
      180018181800EFEFEF0094969400EFEFEF00EFEFEF00EFEFEF00848684009C9E
      9C009C9E9C001818180094969400ADAEAD0000000000ADAEAD00BDBABD00E7E7
      E700F7F3F700F7F3F700EFEFEF00EFEBEF00DEDFDE00A5A2A500CECFCE00D6D7
      D600CECFCE00BDBEBD00949294007B7D7B00FFFFFF00D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D600D6D7D600FFFFFF00FFFFFF00FFFFFF00FFFFFF008486
      84009C9E9C009C9E9C0018181800ADAEAD00FFFFFF00D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008486
      84009C9E9C009C9E9C0018181800ADAEAD0094969400EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF009496940094969400949694008C8E8C008C8E8C008486
      84009C9E9C009C9E9C00181818006361630000000000ADAEAD00DEDFDE00BDBA
      BD00EFEFEF00EFEFEF00E7E3E7009C9A9C00DEDFDE009C9A9C00C6C7C600D6D7
      D600CECBCE00ADAAAD00949294007B7D7B00FFFFFF00D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D600D6D7D600D6D7D600D6D7D600D6D7D600D6D7D6008486
      84009C9E9C00181818001818180084868400FFFFFF00D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D600D6D7D600D6D7D600D6D7D600D6D7D600D6D7D6008486
      84009C9E9C0018181800181818008486840094969400EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF00EFEFEF008486
      84009C9E9C0018181800181818009496940000000000ADAEAD00DEDFDE00EFEB
      EF00BDBABD00DEDFDE00E7E3E700C6C3C600B5B2B500B5B6B500CECFCE00CECB
      CE00ADAEAD0094929400000000007B7D7B00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0084868400E7E7E700E7E7E70018181800FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0084868400DEDFDE00DEDFDE00181818009496940094969400949694009496
      94009496940094969400949694009496940094969400949694008C8E8C008C8E
      8C0084868400E7E7E700E7E7E7001818180000000000ADAEAD00DEDFDE00A5A2
      A500E7E3E700BDBABD00C6C3C600C6C3C600C6C3C600BDBEBD00C6C3C600CECB
      CE0094929400A5A2A500BDBEBD007B7D7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084868400E7E7E700E7E7E700181818000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084868400DEDFDE00DEDFDE00181818000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000084868400E7E7E700E7E7E7001818180000000000ADAEAD00ADAEAD00ADAE
      AD00ADAEAD00ADAEAD00ADAEAD00ADAEAD00ADAEAD00ADAEAD00ADAEAD00ADAE
      AD00ADAEAD00ADAEAD00ADAEAD00ADAEAD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008486840084868400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848284008482
      8400848284008482840084828400848284008482840084828400848284008482
      8400848284008482840084828400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9A9C009C9A
      9C009C9A9C009C9A9C009C9A9C009C9A9C009C9A9C009C9A9C009C9A9C009C9A
      9C009C9A9C0000000000000000000000000000000000CECFCE00F7F7F700CECB
      CE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECB
      CE00CECBCE0073717300CECBCE00848284000000000000000000000000000000
      0000BDBABD009C9E9C009C9E9C00737173007371730073717300737173007371
      73007371730073717300737173000000000000000000BDBEBD009C9A9C009C9A
      9C009C9A9C009C9A9C009C9A9C009C9A9C009C9A9C009C9A9C009C9A9C009C9A
      9C009C9A9C009C9A9C009C9A9C000000000000000000BDBABD00EFEBEF00BDBA
      BD00BDBABD00BDBABD00BDBABD00BDBABD00BDBABD00BDBABD00BDBABD00BDBA
      BD00BDBABD009C9A9C00000000000000000000000000CECFCE00FFFBFF00F7F7
      F700F7F7F700F7F3F700F7F3F700EFEFEF00EFEBEF00EFEBEF00E7E7E700E7E7
      E700E7E3E7007B797B00CECBCE00848284000000000000000000BDBABD00BDBA
      BD00CECBCE00CECBCE00CECBCE00BDBABD007371730000000000000000000000
      00000000000000000000000000000000000000000000BDBEBD00F7F3F700BDBE
      BD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBE
      BD00BDBEBD00BDBEBD009C9A9C000000000000000000BDBABD00F7F7F700E7E7
      E700E7E3E700E7E3E700BDBEBD007B797B00B5B6B500DEDBDE00D6D7D600D6D7
      D600BDBABD009C9A9C00000000000000000000000000CECFCE00FFFBFF006B69
      6B006B696B00F7F7F7006B696B006B696B00EFEFEF006B696B006B696B009496
      940094929400848684007B797B007375730000000000BDBABD00CECBCE00CECB
      CE00E7E7E700E7E7E700FFFFFF00CECBCE00BDBABD0073717300000000000000
      00000000000000000000000000000000000000000000BDBEBD00F7F3F700F7F3
      F700EFEFEF00EFEFEF00EFEFEF00EFEBEF00EFEBEF00E7E7E700E7E7E700E7E3
      E700E7E3E700BDBEBD009C9A9C000000000000000000BDBABD00F7F7F700E7E7
      E700E7E7E700E7E3E700E7E3E7007B797B00DEDFDE00DEDFDE00DEDBDE00D6D7
      D600BDBABD009C9A9C00000000000000000000000000CECFCE00FFFFFF00FFFB
      FF00FFFBFF00F7F7F700F7F7F700F7F3F700F7F3F700EFEFEF00EFEBEF00EFEB
      EF006B696B0094929400CECBCE008482840000000000CECBCE00E7E7E700F7F7
      F700EFEFEF00EFEBEF00E7E7E700FFFFFF00CECBCE00BDBABD00737173000000
      00000000000000000000000000000000000000000000BDBEBD00F7F3F700F7F3
      F700F7F3F700EFEFEF00EFEFEF00EFEFEF00EFEBEF00EFEBEF00E7E7E700E7E7
      E700E7E3E700BDBEBD009C9A9C000000000000000000BDBABD00F7F7F700EFEB
      EF00E7E7E700E7E7E7007B797B007B797B007B797B00DEDFDE00DEDFDE00DEDB
      DE00BDBABD009C9A9C00000000000000000000000000CECFCE00FFFFFF006B69
      6B00FFFFFF00E7E3E700848284006B696B009C9E9C006B696B006B696B00EFEB
      EF00EFEBEF0094969400CECBCE008482840000000000CECBCE00FFFFFF00FFFB
      FF00F7F7F700EFEFEF00EFEBEF00E7E7E700FFFFFF00CECBCE00BDBABD007371
      73000000000000000000000000000000000000000000BDBEBD00F7F3F700F7F3
      F700F7F3F700F7F3F700EFEFEF00EFEFEF00EFEFEF00EFEBEF00EFEBEF00E7E7
      E700E7E7E700BDBEBD009C9A9C000000000000000000BDBABD00F7F7F700EFEB
      EF00EFEBEF00E7E7E700CECBCE007B797B00CECBCE00E7E3E700DEDFDE00DEDF
      DE00BDBABD009C9A9C00000000000000000000000000CECFCE00FFFFFF006B69
      6B00FFFFFF00A5A2A5006B696B00EFEBEF00E7E7E7006B696B006B696B00EFEF
      EF006B696B00EFEBEF00CECBCE00848284000000000000000000CECBCE00FFFF
      FF00FFFBFF00F7F7F700EFEFEF00EFEBEF00E7E7E700FFFFFF00CECBCE00BDBA
      BD007371730000000000000000000000000000000000BDBEBD00F7F3F700F7F3
      F700F7F3F700F7F3F700F7F3F700EFEFEF00EFEFEF00EFEFEF00EFEBEF00EFEB
      EF00E7E7E700BDBEBD009C9A9C000000000000000000BDBABD00F7F7F700EFEB
      EF00EFEBEF00EFEBEF00E7E7E700E7E7E700E7E3E700E7E3E700E7E3E700DEDF
      DE00BDBABD009C9A9C00000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00ADAEAD006B696B00D6D3D600F7F3F7006B696B006B696B00F7F3
      F7006B696B00EFEFEF00CECBCE0084828400000000000000000000000000CECB
      CE00FFFFFF00FFFBFF00F7F7F700EFEFEF00EFEBEF00E7E7E700FFFFFF00CECB
      CE00BDBABD0073717300000000000000000000000000BDBEBD00F7F3F700F7F3
      F700F7F3F700F7F3F700F7F3F700F7F3F700F7F3F700EFEFEF00EFEFEF00EFEB
      EF00EFEBEF00BDBEBD009C9A9C000000000000000000BDBABD00EFEBEF00F7F7
      F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7F700F7F7
      F700EFEBEF009C9A9C00000000000000000000000000CECFCE00FFFFFF006B69
      6B00FFFFFF00F7F7F700B5B6B5008C8A8C00737173006B696B006B696B00F7F7
      F700F7F3F700EFEFEF00CECBCE00848284000000000000000000000000000000
      0000CECBCE00FFFFFF00FFFBFF00F7F7F700EFEFEF00EFEBEF00E7E7E700FFFF
      FF00CECBCE00BDBABD00848284000000000000000000BDBEBD00F7F3F700F7F3
      F700F7F3F700F7F3F700F7F3F700F7F3F700F7F3F700F7F3F700EFEFEF00EFEF
      EF00EFEBEF00BDBEBD009C9A9C00000000000000000000000000BDBABD00BDBA
      BD00BDBABD00BDBABD00BDBABD00BDBABD00BDBABD00BDBABD00BDBABD00BDBA
      BD00BDBABD0000000000000000000000000000000000CECFCE00FFFFFF006B69
      6B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFEBEF006B696B007B797B00F7F7
      F7006B696B00F7F3F700CECBCE00848284000000000000000000000000000000
      000000000000CECBCE00FFFFFF00FFFBFF00F7F7F700EFEFEF00EFEBEF00E7E7
      E700FFFFFF00CECBCE009C9E9C000000000000000000BDBEBD00F7F3F700F7F3
      F700F7F3F700F7F3F700F7F3F700F7F3F700F7F3F700F7F3F700F7F3F700EFEF
      EF00EFEFEF00EFEBEF009C9A9C0000000000000000000000000000000000BDBA
      BD00F7F7F7009C9A9C00000000000000000000000000BDBABD00DEDFDE009C9A
      9C000000000000000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00848284006B696B006B696B008C8A8C00D6D7D600FFFB
      FF006B696B00F7F7F700CECBCE00848284000000000000000000000000000000
      00000000000000000000CECBCE00FFFFFF00FFFBFF00F7F7F700EFEFEF00EFEB
      EF00E7E7E700FFFFFF009C9E9C000000000000000000BDBEBD00BDBEBD00BDBE
      BD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBEBD006B696B006B696B006B69
      6B006B696B00BDBEBD00BDBEBD0000000000000000000000000000000000BDBA
      BD00F7F7F7009C9A9C00000000000000000000000000BDBABD00DEDFDE009C9A
      9C000000000000000000000000000000000000000000CECFCE00FFFFFF006B69
      6B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFB
      FF00FFFBFF00F7F7F700CECBCE00848284000000000000000000000000000000
      0000000000000000000000000000CECBCE00FFFFFF00FFFFFF00FFFBFF00F7F3
      F700FFFFFF00E7E7E700BDBABD000000000000000000BDBEBD00F7F7F700F7F7
      F700F7F7F700F7F7F700BDBEBD0000000000000000006B696B00CECBCE006B69
      6B0000000000000000000000000000000000000000000000000000000000BDBA
      BD00F7F7F700EFEBEF009C9A9C009C9A9C009C9A9C00DEDFDE00DEDFDE009C9A
      9C000000000000000000000000000000000000000000CECFCE00FFFFFF006B69
      6B00FFFFFF006B696B006B696B00FFFFFF006B696B006B696B00FFFFFF006B69
      6B006B696B00FFFBFF00CECBCE00848284000000000000000000000000000000
      000000000000000000000000000000000000CECBCE00FFFFFF00FFFFFF00FFFF
      FF00E7E7E700BDBABD0000000000000000000000000000000000BDBEBD00BDBE
      BD00BDBEBD00BDBEBD000000000000000000000000006B696B006B696B00A5A2
      A500000000000000000000000000000000000000000000000000000000000000
      0000BDBABD00F7F7F700F7F7F700F7F7F700F7F7F700DEDFDE009C9A9C000000
      00000000000000000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFBFF00FFFBFF00848284000000000000000000000000000000
      00000000000000000000000000000000000000000000CECBCE00CECBCE00CECB
      CE00CECBCE000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000006B696B00000000006B69
      6B00A5A2A50000000000A5A2A5006B696B000000000000000000000000000000
      000000000000BDBABD00BDBABD00BDBABD00BDBABD00BDBABD00000000000000
      0000000000000000000000000000000000000000000000000000CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006B696B006B696B006B696B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C9E9C0073717300737173007371730000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000848284008482840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000949694007B797B007B797B007B797B007B797B007B797B007B79
      7B007B797B007B797B007B797B007B797B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9E9C007371
      730073717300949694009C9A9C009C9A9C007371730073717300737173007371
      7300737173007371730073717300737173000000000000000000000000000000
      0000000000000000000084828400F7F7F700F7F7F70084828400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000094969400FFFFFF00CECBCE00CECBCE00CECBCE00CECBCE00CECB
      CE00CECBCE00CECBCE00CECBCE007B797B0000000000949694007B797B007B79
      7B007B797B007B797B007B797B007B797B007B797B007B797B007B797B007B79
      7B007B797B007B797B007B797B007B797B00000000009C9E9C0094969400A5A2
      A500A5A2A500A5A2A5009C9E9C009C9A9C0073717300C6C7C600C6C7C600C6C7
      C600C6C7C600C6C7C600C6C7C600737173000000000000000000000000000000
      00000000000084828400F7F7F7006B696B006B696B00F7F7F700848284000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000094969400FFFFFF00FFFFFF00FFFFFF00FFFBFF00F7F7F700F7F3
      F700EFEFEF00EFEBEF00CECBCE007B797B000000000094969400FFFBFF00CECB
      CE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECB
      CE00CECBCE00CECBCE00CECBCE007B797B00000000009C9E9C00ADAAAD00A5A6
      A500A5A6A500A5A2A500A5A2A5009C9E9C00737173006B696B006B696B006B69
      6B006B696B007B7D7B00C6C7C600737173000000000000000000000000000000
      000084828400F7F7F7006B696B00B5B6B500ADAEAD006B696B00F7F7F7008482
      8400000000000000000000000000000000000000000000000000000000009496
      94007B797B0094969400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00F7F7
      F700F7F3F700EFEFEF00CECBCE007B797B000000000094969400FFFFFF00FFFB
      FF00F7F7F700F7F7F700F7F3F700EFEFEF00EFEFEF00EFEBEF00E7E7E700E7E7
      E700E7E7E700E7E3E700CECBCE007B797B00000000009C9E9C00ADAAAD00ADAA
      AD00A5A6A5009C9E9C009C9E9C009C9E9C00737173006B6D6B006B696B006B69
      6B007B7D7B008C8A8C00C6C7C600737173000000000000000000000000008482
      8400F7F7F7006B696B00BDBEBD00BDBEBD00BDBEBD00BDBEBD006B696B00F7F7
      F700848284000000000000000000000000000000000000000000000000009496
      9400FFFFFF0094969400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFB
      FF00F7F7F700F7F3F700CECBCE007B797B000000000094969400FFFFFF00FFFF
      FF00FFFBFF00F7F7F700F7F7F700F7F3F700EFEFEF00EFEFEF00EFEBEF00E7E7
      E700E7E7E700E7E7E700CECBCE007B797B00000000009C9E9C00ADAAAD00ADAA
      AD00ADAEAD00D6D7D6009C9E9C009C9A9C0073717300737573006B6D6B007B79
      7B009492940094929400C6C7C60073717300000000000000000084828400F7F7
      F7006B696B00C6C3C600FFFFFF00FFFFFF00FFFFFF00FFFFFF009C9E9C006B69
      6B00F7F7F7008482840000000000000000000000000000000000000000009496
      9400FFFFFF0094969400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00F7F7F700F7F3F7007B797B000000000094969400FFFFFF00FFFF
      FF00FFFFFF00FFFBFF00F7F7F700F7F7F700F7F3F700EFEFEF00EFEFEF00EFEB
      EF00EFEBEF00E7E7E700CECBCE007B797B00000000009C9E9C00ADAEAD00ADAA
      AD00D6D7D600FFFBFF00D6D7D6009C9A9C00737173007B7D7B00737573008C8A
      8C009C9A9C0094929400C6C7C600737173000000000084828400F7F7F7006B69
      6B00D6D3D600CECFCE00C6C3C600FFFFFF00FFFFFF009C9E9C00B5B6B500ADAE
      AD006B696B00F7F7F700848284000000000000000000949694007B797B009496
      9400FFFFFF00949694007B797B007B797B007B797B007B797B007B797B007B79
      7B007B797B007B797B007B797B007B797B000000000094969400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFBFF00F7F7F700F7F7F700F7F3F700EFEFEF00EFEF
      EF00EFEFEF00EFEBEF00CECBCE007B797B00000000009C9E9C00ADAEAD00ADAE
      AD00ADAEAD00D6D7D600ADAEAD009C9E9C007371730084828400A5A2A500BDBE
      BD008C8E8C00BDBEBD00C6C7C600737173006B696B00FFFBFF008C8E8C00DEDF
      DE00D6D7D600D6D3D600CECFCE00FFFFFF00FFFFFF009C9E9C00BDBABD00B5B6
      B500ADAEAD006B696B00F7F7F700848284000000000094969400FFFFFF009496
      9400FFFFFF0094929400B5B6B500B5B6B500B5B6B500B5B6B500DEDFDE00B5B6
      B500DEDFDE00B5B6B500B5B6B5007B797B000000000094969400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFBFF00F7F7F700F7F3F700EFEF
      EF00EFEFEF00EFEFEF00CECBCE007B797B00000000009C9E9C00B5B2B500B5B2
      B500ADAEAD00ADAEAD00ADAAAD00A5A2A500737173009C9E9C00D6D7D600D6D7
      D600D6D7D600D6D7D600C6C7C600737173006B696B00FFFFFF008C8E8C00DEDF
      DE00DEDFDE00D6D7D600D6D3D600FFFFFF00FFFFFF009C9E9C00BDBEBD00BDBA
      BD00B5B6B5006B696B00F7F7F700848284000000000094969400FFFFFF009496
      9400FFFFFF00FFFFFF0094969400949694009496940094969400949694009496
      9400949694009496940094969400000000000000000094969400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFBFF00F7F7F700F7F3
      F700F7F3F700F7F3F700CECBCE007B797B00000000009C9E9C00B5B2B500B5B2
      B500B5B2B500ADAEAD00ADAEAD00A5A2A50073717300D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D600C6C7C60073717300000000006B696B00FFFFFF008C8E
      8C00DEDFDE00DEDFDE00FFFFFF00FFFFFF00FFFFFF009C9E9C00C6C3C600BDBE
      BD006B696B00F7F7F70084828400000000000000000094969400FFFFFF009496
      94007B797B007B797B007B797B007B797B007B797B007B797B007B797B007B79
      7B007B797B007B797B0000000000000000000000000094969400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFBFF00F7F7
      F700F7F7F700F7F3F700CECBCE007B797B00000000009C9E9C00B5B6B500B5B2
      B500B5B2B500B5B2B500ADAEAD00A5A2A50073717300D6D7D600D6D7D600D6D7
      D600CECFCE00CECFCE00C6C7C6007371730000000000000000006B696B00FFFF
      FF008C8E8C00DEDFDE00DEDFDE00BDBEBD00BDBEBD00CECFCE00C6C3C6006B69
      6B00F7F7F7008482840000000000000000000000000094969400FFFFFF009492
      9400B5B6B500B5B6B500B5B6B500B5B6B500DEDFDE00B5B6B500DEDFDE00B5B6
      B500B5B6B5007B797B0000000000000000000000000094969400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFB
      FF00FFFBFF00F7F7F700CECBCE007B797B00000000009C9E9C00B5B6B500B5B6
      B500B5B6B500B5B2B500B5B2B500A5A6A50073717300D6D7D600D6D7D600CECF
      CE00D6D7D600CECFCE00C6C7C600737173000000000000000000000000006B69
      6B00FFFFFF008C8E8C00DEDFDE00FFFFFF00FFFFFF009C9E9C006B696B00F7F7
      F700848284000000000000000000000000000000000094969400FFFFFF00FFFF
      FF00949694009496940094969400949694009496940094969400949694009496
      94009496940000000000000000000000000094969400F7F3F70094969400F7F3
      F70094969400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFB
      FF00FFFBFF00FFFBFF00F7F7F7007B797B00000000009C9E9C00B5B6B500B5B6
      B500B5B6B500B5B6B500B5B2B500A5A6A50073717300D6D7D600CECFCE00DEDB
      DE00DEDBDE00D6D7D600C6C7C600737173000000000000000000000000000000
      00006B696B00FFFFFF008C8E8C00FFFFFF00FFFFFF006B696B00F7F7F7008482
      84000000000000000000000000000000000000000000949694007B797B007B79
      7B007B797B007B797B007B797B007B797B007B797B007B797B007B797B007B79
      7B0000000000000000000000000000000000F7F3F700BDBEBD00EFEBEF00BDBE
      BD00F7F3F7007B797B007B797B007B797B007B797B007B797B007B797B007B79
      7B007B797B007B797B007B797B007B797B000000000000000000ADAAAD009C9E
      9C00ADAAAD00ADAAAD00ADAEAD00A5A6A500737173009C9E9C009C9E9C009C9E
      9C009C9E9C009C9E9C009C9E9C009C9E9C000000000000000000000000000000
      0000000000006B696B00FFFFFF008C8E8C008C8E8C00F7F7F700848284000000
      0000000000000000000000000000000000000000000094929400B5B6B500B5B6
      B500B5B6B500B5B6B500DEDFDE00B5B6B500DEDFDE00B5B6B500B5B6B5007B79
      7B000000000000000000000000000000000094969400EFEBEF00FFFFFF00EFEB
      EF0094969400B5B6B500B5B6B500B5B6B500B5B6B500DEDFDE00B5B6B500DEDF
      DE00B5B6B500B5B6B500B5B6B500949694000000000000000000000000000000
      0000ADAAAD009C9E9C00ADAAAD00ADAAAD007371730000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B696B00FFFFFF00FFFBFF0084828400000000000000
      0000000000000000000000000000000000000000000000000000949694009496
      9400949694009496940094969400949694009496940094969400949694000000
      000000000000000000000000000000000000F7F3F700BDBEBD00EFEBEF00BDBE
      BD00F7F3F7009496940094969400949694009496940094969400949694009496
      9400949694009496940094969400000000000000000000000000000000000000
      00000000000000000000ADAAAD009C9E9C009C9E9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000006B696B006B696B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000094969400F7F3F70094969400F7F3
      F700949694000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CECBCE008482
      8400848284008482840084828400848284008482840084828400848284008482
      8400848284008482840084828400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CECBCE00F7F3
      F700CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECB
      CE00CECBCE00CECBCE008482840000000000000000000000000084828400C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C6000000000000000000BDBEBD009C9A9C009C9A9C009C9A
      9C009C9A9C009C9A9C009C9A9C009C9A9C009C9A9C009C9A9C009C9A9C009C9A
      9C00000000000000000000000000000000000000000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000CECBCE00F7F7
      F700F7F3F700EFEFEF00EFEFEF00EFEBEF00EFEBEF00E7E7E700E7E3E700E7E3
      E700E7E3E700CECBCE008482840000000000000000000000000084828400FFFF
      FF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFF
      FF00FFFFFF00C6C3C6000000000000000000BDBEBD00BDBEBD00C6C7C600BDBE
      BD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBEBD009C9A
      9C00000000000000000000000000000000000000000084828400FFFFFF008482
      840084828400848284008482840084828400FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000CECBCE00F7F7
      F700F7F7F700F7F3F700EFEFEF00EFEFEF00EFEBEF00EFEBEF00E7E7E700E7E3
      E700E7E3E700CECBCE008482840000000000000000000000000084828400FFFF
      FF0000FFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFF
      FF0000FFFF00C6C3C6000000000000000000BDBEBD00BDBEBD00DEDBDE00E7E7
      E700E7E7E700E7E3E700DEDFDE00DEDBDE00DEDBDE00D6D7D600D6D7D600BDBE
      BD009C9A9C000000000000000000000000000000000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000CECBCE00FFFB
      FF00F7F7F700F7F7F700F7F3F700EFEFEF00EFEFEF00EFEBEF00EFEBEF00E7E7
      E700E7E3E700CECBCE008482840000000000000000000000000084828400FFFF
      FF00FFFFFF00FFFFFF00C6C3C600000000000000000000000000C6C3C600FFFF
      FF00FFFFFF00C6C3C6000000000000000000BDBEBD00E7E7E700BDBEBD00E7E7
      E700E7E7E700E7E7E700E7E3E700DEDFDE00DEDFDE00DEDBDE00D6D7D600D6D3
      D6009C9A9C000000000000000000000000000000000084828400FFFFFF008482
      840084828400848284008482840084828400FFFFFF0000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600000000000000000000000000CECBCE00FFFB
      FF00FFFBFF00F7F7F700F7F7F700F7F3F700EFEFEF00EFEFEF00EFEBEF00EFEB
      EF00E7E7E700CECBCE008482840000000000000000000000000084828400FFFF
      FF0000FFFF00C6C3C60084000000FF000000840000008400000000000000C6C3
      C60000FFFF00C6C3C6000000000000000000BDBEBD00EFEFEF00BDBEBD00DEDB
      DE00EFEBEF00E7E7E700E7E7E700E7E3E700DEDFDE00DEDFDE00DEDBDE00D6D7
      D600BDBEBD009C9A9C0000000000000000000000000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600000000000000000000000000CECBCE00FFFF
      FF00FFFBFF00FFFBFF00F7F7F700F7F7F700F7F3F700EFEFEF00EFEFEF00EFEB
      EF00EFEBEF00CECBCE008482840000000000000000000000000084828400FFFF
      FF00FFFFFF0084828400FF000000FF00000000820000FF000000840000008400
      0000FFFFFF00C6C3C6000000000000000000BDBEBD00F7F3F700E7E7E700BDBE
      BD00E7E7E700EFEBEF00E7E7E700E7E7E700E7E3E700DEDFDE00DEDFDE00DEDB
      DE00D6D3D6009C9A9C0000000000000000000000000084828400FFFFFF008482
      840084828400FFFFFF000000000000000000000000000000000084000000FFFF
      FF00FFFFFF00FFFFFF00C6C3C600000000000000000000000000CECBCE00FFFF
      FF00FFFFFF00FFFBFF00FFFBFF00F7F7F700F7F7F700F7F3F700EFEFEF00EFEF
      EF00EFEBEF00CECBCE008482840000000000000000000000000084828400FFFF
      FF0000FFFF0084820000C6C3C600848284000082000084820000FF0000008400
      000000FFFF00C6C3C6000000000000000000BDBEBD00F7F3F700F7F3F700BDBE
      BD00DEDBDE00E7E7E700EFEBEF00E7E7E700E7E7E700E7E3E700DEDFDE00DEDF
      DE00DEDBDE00BDBEBD009C9A9C00000000000000000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084828400FFFFFF00FFFFFF0084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600000000000000000000000000CECBCE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00F7F7F700F7F7F700F7F3F700F7F3
      F700EFEFEF00CECBCE008482840000000000000000000000000084828400FFFF
      FF00FFFFFF0084820000FFFFFF00C6C3C6000082000084000000FF0000008400
      0000FFFFFF00C6C3C6000000000000000000BDBEBD00F7F7F700F7F3F700E7E7
      E700BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBE
      BD00BDBEBD00BDBEBD00BDBEBD00000000000000000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084828400FFFFFF00848284008400000084000000FFFF
      FF000000840000008400C6C3C600000000000000000000000000CECBCE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00F7F7F700F7F7F700F7F3
      F700F7F3F700CECBCE008482840000000000000000000000000084828400FFFF
      FF0000FFFF0084820000C6C3C60084820000FF00000084820000008200008482
      840000FFFF00C6C3C6000000000000000000BDBEBD00F7F7F700F7F7F700F7F3
      F700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7
      E700BDBEBD000000000000000000000000000000000084828400848284008482
      840084828400848284008482840084828400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000840000008400C6C3C600000000000000000000000000CECBCE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00F7F7F700F7F7
      F700CECBCE00CECBCE008482840000000000000000000000000084828400FFFF
      FF00FFFFFF00C6C3C6008482000084828400848200000082000084820000C6C3
      C600FFFFFF00C6C3C6000000000000000000BDBEBD00F7F7F700F7F7F700F7F7
      F700F7F3F700E7E7E700BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBE
      BD006B696B006B696B006B696B006B696B000000000000000000000000000000
      000084828400C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600000000000000000000000000CECBCE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00CECB
      CE0084828400848284008482840000000000000000000000000084828400FFFF
      FF0000FFFF00FFFFFF00C6C3C600848200008482000084820000C6C3C600FFFF
      FF0000FFFF00C6C3C6000000000000000000BDBEBD00F7F7F700F7F7F700F7F7
      F700F7F7F700BDBEBD0000000000000000000000000000000000000000000000
      0000000000006B696B00CECBCE006B696B000000000000000000000000000000
      00000000000084828400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000CECBCE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CECB
      CE00F7F7F700E7E3E7008482840000000000000000000000000084828400FFFF
      FF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF000000
      00000000000000000000000000000000000000000000BDBEBD00BDBEBD00BDBE
      BD00BDBEBD000000000000000000000000000000000000000000000000000000
      000000000000A5A2A5006B696B006B696B000000000000000000000000000000
      0000000000000000000084828400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000CECBCE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CECB
      CE00E7E3E700848284000000000000000000000000000000000084828400FFFF
      FF0000FFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFFFF00C6C3
      C600FFFFFF008482840000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000006B696B00A5A2A5000000
      0000A5A2A5006B696B00000000006B696B000000000000000000000000000000
      000000000000000000000000000084828400FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000CECBCE00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CECB
      CE0084828400000000000000000000000000000000000000000084828400FFFF
      FF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFFFF0000FFFF00C6C3
      C600848284000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006B696B006B69
      6B006B696B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008482840084828400848284008482
      8400000000000000000000000000000000000000000000000000CECBCE00CECB
      CE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECB
      CE00000000000000000000000000000000000000000000000000848284008482
      8400848284008482840084828400848284008482840084828400848284008482
      8400000000000000000000000000000000000000000000000000848284008482
      8400848284008482840084828400848284008482840084828400848284008482
      8400848284008482840084828400000000000000000000000000848284008482
      8400848284008482840084828400848284008482840084828400848284008482
      8400848284008482840084828400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECFCE00F7F7F700CECB
      CE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECB
      CE00CECBCE00CECBCE00CECBCE008482840000000000CECFCE00F7F7F700CECB
      CE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECB
      CE00CECBCE00CECBCE00CECBCE00848284000000000000000000000000000000
      0000000000000000000084000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECFCE00FFFBFF00F7F7
      F700F7F7F700F7F3F700F7F3F700EFEFEF00EFEBEF00EFEBEF00E7E7E700E7E7
      E700E7E3E700E7E3E700CECBCE008482840000000000CECFCE00FFFBFF00F7F7
      F700F7F7F700F7F3F700F7F3F700EFEFEF00EFEBEF00EFEBEF00E7E7E700E7E7
      E700E7E3E700E7E3E700CECBCE00848284000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008400000084000000000000000000000000000000CECFCE00FFFBFF00FFFB
      FF00F7F7F700F7F7F700F7F3F700F7F3F700EFEFEF00EFEBEF00EFEBEF00E7E7
      E700E7E7E700E7E3E700CECBCE008482840000000000CECFCE00C6C7C6006B69
      6B006B696B00B5B2B500F7F3F700F7F3F7006B696B006B696B006B696B00E7E7
      E7006B696B00E7E3E700CECBCE00848284000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000CECFCE00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700CECBCE008482840000000000CECFCE00FFFFFF00D6D7
      D6008C8E8C00EFEFEF00F7F7F700F7F3F700E7E7E7006B696B00EFEBEF006B69
      6B006B696B006B696B00CECBCE00848284000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFBFF00FFFBFF00F7F7F700F7F3F700F7F3F700EFEFEF00EFEB
      EF00EFEBEF00E7E7E700CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00E7E3E7009C9A9C00F7F7F700F7F7F700F7F3F7006B696B00EFEFEF00EFEB
      EF006B696B00E7E7E700CECBCE00848284008482000084820000848200008482
      0000848200008482000084000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000EFEBEF00CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00CECBCE006B696B006B696B006B696B006B696B00F7F3F700EFEF
      EF006B696B00EFEBEF00CECBCE008482840084820000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000FFFFFF008400000084000000840000008400
      0000840000008400000084000000840000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008400000084000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFBFF00F7F7F700F7F7F700F7F3
      F700EFEFEF00EFEFEF00CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00ADAEAD00CECFCE00FFFBFF006B696B00F7F7F700F7F3
      F7006B696B00EFEFEF00CECBCE008482840084820000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000084000000FFFFFF008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008400000084000000000000000000000000000000CECFCE00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EFEFEF00CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFBFF0094969400E7E3E7006B696B00F7F7F700F7F7
      F7006B696B00EFEFEF00CECBCE008482840084820000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084820000000000000000
      0000000000000000000000000000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008400000084000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFBFF00F7F7
      F700F7F7F700F7F3F700CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F7F7F700949294006B696B00FFFBFF00F7F7
      F7006B696B00F7F3F700CECBCE00848284008482000084820000848200008482
      0000848200008482000084820000848200008482000084820000848200008482
      0000848200008482000084820000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000F7F7F700CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7E7E7006B6D6B00FFFBFF006B69
      6B006B696B006B696B00CECBCE008482840084820000FFFFFF00848200008482
      0000848200008482000084820000848200008482000084820000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084820000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFB
      FF00FFFBFF00F7F7F700CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00D6D3D600FFFFFF00FFFB
      FF006B696B00F7F7F700CECBCE00848284008482000084820000848200008482
      0000848200008482000084820000848200008482000084820000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084820000000000000000000084000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0084000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00FFFBFF00CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00FFFBFF00CECBCE00848284000000000000000000000000000000
      00000000000084820000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0084820000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      00008400000084000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFBFF00FFFBFF008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFBFF00FFFBFF00848284000000000000000000000000000000
      0000000000008482000084820000848200008482000084820000848200008482
      0000848200008482000084820000000000000000000084000000FFFFFF008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008400000000000000000000000000000000000000CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE00000000000000000000000000CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE00000000000000000000000000000000000000
      00000000000084820000FFFFFF00848200008482000084820000848200008482
      0000848200008482000084820000000000000000000084000000840000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008482000084820000848200008482000084820000848200008482
      0000848200008482000084820000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848284008482
      8400848284008482840084828400848284008482840084828400848284008482
      8400848284008482840084828400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848284008482
      8400848284008482840084828400848284008482840084828400848284008482
      8400848284008482840084828400000000000000000000000000848284008482
      8400848284008482840084828400848284008482840084828400848284008482
      84008482840084828400848284000000000000000000CECFCE00F7F7F700CECB
      CE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECB
      CE00CECBCE00CECBCE00CECBCE0084828400000000006B696B00737173000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECFCE00F7F7F700CECB
      CE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECB
      CE00CECBCE00CECBCE00CECBCE008482840000000000CECFCE00F7F7F700CECB
      CE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECB
      CE00CECBCE00CECBCE00CECBCE008482840000000000CECFCE00FFFBFF00F7F7
      F700F7F7F700F7F3F700F7F3F700EFEFEF00EFEBEF00EFEBEF00E7E7E700E7E7
      E700E7E3E700E7E3E700CECBCE00848284008C8E8C00ADAEAD009C9E9C007371
      7300000000000000000000000000ADAAAD00ADAAAD00ADAAAD00A5A2A5009496
      94008482840000000000000000000000000000000000CECFCE00FFFBFF00F7F7
      F700F7F7F700F7F3F700F7F3F700EFEFEF00EFEBEF00EFEBEF00E7E7E700E7E7
      E700E7E3E700E7E3E700CECBCE008482840000000000CECFCE00FFFBFF00F7F7
      F700F7F7F700F7F3F700F7F3F700EFEFEF00EFEBEF00EFEBEF00E7E7E700E7E7
      E700E7E3E700E7E3E700CECBCE008482840000000000CECFCE00FFFBFF00FFFB
      FF006B696B006B696B006B696B006B696B006B696B006B696B006B696B006B69
      6B006B696B00E7E3E700CECBCE0084828400A5A2A500DEDBDE00ADAEAD006B69
      6B00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECFCE00FFFBFF00FFFB
      FF00F7F7F700F7F7F700F7F3F700F7F3F700EFEFEF00EFEBEF00EFEBEF00E7E7
      E700E7E7E700E7E3E700CECBCE008482840000000000CECFCE00FFFBFF00FFFB
      FF00F7F7F700F7F7F700F7F3F700F7F3F700EFEFEF00EFEBEF00EFEBEF00E7E7
      E700E7E7E700E7E3E700CECBCE008482840000000000CECFCE00FFFFFF00FFFB
      FF00FFFBFF00F7F7F700F7F7F700F7F3F700F7F3F700EFEFEF00EFEBEF00EFEB
      EF00E7E7E700E7E7E700CECBCE008482840000000000A5A2A5008C8E8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECFCE00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700CECBCE008482840000000000CECFCE00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7E7E700CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00EFEBEF00A5A6A500737573006B696B0073757300A5A6A500E7E3
      E700EFEBEF00E7E7E700CECBCE00848284000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFBFF00FFFBFF00F7F7F700F7F3F700F7F3F700EFEFEF00EFEB
      EF00EFEBEF00E7E7E700CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFBFF00FFFBFF00F7F7F700F7F3F700F7F3F700EFEFEF00EFEB
      EF00EFEBEF00E7E7E700CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF008C8A8C006B696B00BDBEBD00F7F3F700EFEFEF00B5B2B5009C9A
      9C00EFEBEF00EFEBEF00CECBCE0084828400000000006B696B00737173000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECFCE00FFFFFF000000
      000000000000000000000000000000000000000000000000000000000000EFEF
      EF00EFEBEF00EFEBEF00CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000000000000EFEF
      EF00EFEBEF00EFEBEF00CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF006B696B006B696B00FFFBFF00FFFBFF00F7F7F700EFEFEF006B69
      6B00EFEFEF00EFEFEF00CECBCE00848284008C8E8C00ADAEAD009C9E9C007371
      7300000000000000000000000000ADAAAD00ADAAAD00ADAAAD00A5A2A5009496
      94008482840000000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFBFF00F7F7F700F7F7F700F7F3
      F700EFEFEF00EFEFEF00CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFBFF00F7F7F700F7F7F700F7F3
      F700EFEFEF00EFEFEF00CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF006B696B006B696B00FFFFFF00FFFBFF00FFFBFF00F7F7F7006B69
      6B00F7F3F700EFEFEF00CECBCE0084828400A5A2A500DEDBDE00ADAEAD006B69
      6B00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECFCE00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EFEFEF00CECBCE008482840000000000CECFCE00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EFEFEF00CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF006B696B006B696B00FFFFFF00FFFFFF00FFFBFF00FFFBFF006B69
      6B00F7F7F700F7F3F700CECBCE008482840000000000A5A2A5008C8E8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFBFF00F7F7
      F700F7F7F700F7F3F700CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFBFF00F7F7
      F700F7F7F700F7F3F700CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF006B696B006B696B00FFFFFF00FFFFFF00FFFFFF00FFFBFF006B69
      6B00F7F7F700F7F7F700CECBCE00848284000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECFCE00FFFFFF000000
      000000000000000000000000000000000000000000000000000000000000FFFB
      FF00F7F7F700F7F7F700CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000000000000000000000000000FFFB
      FF00F7F7F700F7F7F700CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFBFF006B696B006B696B00F7F7F700FFFFFF00FFFFFF00F7F7F7006B69
      6B00F7F3F700F7F7F700CECBCE0084828400000000006B696B00737173000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFB
      FF00FFFBFF00F7F7F700CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFB
      FF00FFFBFF00F7F7F700CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF006B696B006B696B006B696B006B696B00FFFFFF00FFFFFF006B696B006B69
      6B006B696B00FFFBFF00CECBCE00848284008C8E8C00ADAEAD009C9E9C007371
      7300000000000000000000000000ADAAAD00ADAAAD00ADAAAD00A5A2A5009496
      94008482840000000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00FFFBFF00CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00FFFBFF00CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFBFF00FFFBFF0084828400A5A2A500DEDBDE00ADAEAD006B69
      6B00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFBFF00FFFBFF008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFBFF00FFFBFF00848284000000000000000000CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE000000000000000000A5A2A5008C8E8C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE00000000000000000000000000CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848284008482
      8400848284008482840084828400848284008482840084828400848284008482
      8400848284008482840084828400000000000000000000000000848284008482
      8400848284008482840084828400848284008482840084828400848284008482
      840084828400848284008482840000000000ADAEAD00ADAAAD00949694008C8E
      8C007B797B007B797B00000000000000000000000000ADAEAD00ADAAAD009496
      94008C8E8C007B797B007B797B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B7D7B007B7D7B000000000000000000CECFCE00F7F7F700CECB
      CE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECB
      CE00CECBCE00CECBCE00CECBCE008482840000000000CECFCE00F7F7F700CECB
      CE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECB
      CE00CECBCE00CECBCE00CECBCE0084828400ADAEAD00FFFFFF00DEDFDE00CECB
      CE00ADAAAD007B797B00000000000000000000000000ADAEAD00FFFFFF00DEDF
      DE00CECBCE00ADAAAD007B797B00000000000000000000000000000000000000
      00006B696B000000000000000000000000000000000000000000000000000000
      00007B7D7B0000000000000000007B7D7B0000000000CECFCE00FFFBFF00F7F7
      F700F7F7F700F7F3F700F7F3F700EFEFEF00EFEBEF00EFEBEF00E7E7E700E7E7
      E700E7E3E700E7E3E700CECBCE008482840000000000CECFCE00FFFBFF00F7F7
      F700F7F7F700F7F3F700F7F3F700EFEFEF00EFEBEF00EFEBEF00E7E7E700E7E7
      E700E7E3E700E7E3E700CECBCE0084828400ADAEAD00FFFFFF00DEDFDE00CECB
      CE00B5B2B5007B797B00000000000000000000000000ADAEAD00FFFFFF00DEDF
      DE00CECBCE00B5B2B5007B797B000000000000000000000000006B696B006B69
      6B006B696B006B696B0000000000000000000000000000000000000000000000
      00007B7D7B0000000000000000000000000000000000CECFCE00FFFBFF00FFFB
      FF006B696B006B696B006B696B006B696B006B696B006B6D6B00A5A6A500DEDF
      DE00E7E7E700E7E3E700CECBCE008482840000000000CECFCE00FFFBFF00FFFB
      FF00DEDFDE006B696B006B696B006B696B006B696B00DEDFDE00EFEBEF00E7E7
      E700E7E7E700E7E3E700CECBCE0084828400ADAEAD00FFFFFF00DEDFDE00CECB
      CE00B5B2B5007B797B00000000000000000000000000ADAEAD00FFFFFF00DEDF
      DE00CECBCE00B5B2B5007B797B0000000000000000006B696B009C9E9C000000
      00006B696B000000000000000000000000000000000000000000000000000000
      00007B7D7B0000000000000000007B7D7B0000000000CECFCE00FFFFFF00FFFB
      FF00FFFBFF006B696B006B696B00EFEFEF00F7F3F700CECFCE006B696B007371
      7300E7E7E700E7E7E700CECBCE008482840000000000CECFCE00FFFFFF00FFFB
      FF00FFFBFF00F7F3F700949294006B696B00DEDBDE00EFEFEF00EFEBEF00EFEB
      EF00E7E7E700E7E7E700CECBCE0084828400ADAEAD00FFFFFF00DEDFDE00CECB
      CE00B5B2B5007B797B00000000000000000000000000ADAEAD00FFFFFF00DEDF
      DE00CECBCE00B5B2B5007B797B0000000000000000006B696B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B7D7B007B7D7B000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF006B696B006B696B00F7F7F700F7F3F700F7F3F7006B696B006B69
      6B00EFEBEF00E7E7E700CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFBFF00B5B6B5006B696B00CECBCE00F7F3F700EFEFEF00EFEB
      EF00EFEBEF00E7E7E700CECBCE0084828400ADAEAD009C9E9C009C9E9C008C8A
      8C008C8A8C007B797B007B797B007B797B00ADAEAD009C9E9C009C9E9C008C8A
      8C008C8A8C007B797B007B797B0000000000000000006B696B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF006B696B006B696B00FFFBFF00F7F7F700D6D7D6006B696B009C9E
      9C00EFEBEF00EFEBEF00CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00D6D3D6006B696B00ADAEAD00F7F7F700F7F3F700EFEF
      EF00EFEBEF00EFEBEF00CECBCE0084828400DEDFDE00ADAEAD00EFEBEF00EFEB
      EF00C6C7C600ADAAAD007B797B009C9E9C00ADAEAD00EFEBEF00EFEBEF00C6C7
      C600ADAAAD007B797B009C9E9C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF006B696B006B696B006B696B006B696B006B696B00ADAEAD00EFEB
      EF00EFEFEF00EFEFEF00CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00EFEBEF006B696B008C8A8C00F7F7F700F7F7F700F7F3
      F700EFEFEF00EFEFEF00CECBCE008482840000000000ADAEAD00FFFFFF00F7F3
      F700E7E7E700ADAAAD007B797B00C6C7C600ADAEAD00FFFFFF00F7F3F700E7E7
      E700ADAAAD009C9E9C0000000000000000000000000000000000000000000000
      000000000000000000007B7D7B009C9A9C009C9A9C009C9A9C00000000000000
      00000000000000000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF006B696B006B696B00FFFFFF00EFEFEF00B5B2B50073717300C6C7
      C600F7F3F700EFEFEF00CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFBFF006B696B006B696B00F7F7F700F7F7F700F7F7
      F700F7F3F700EFEFEF00CECBCE00848284000000000000000000ADAEAD009C9E
      9C00949694007B797B007B797B009C9E9C00ADAEAD009C9E9C00949694007B79
      7B007B797B000000000000000000000000000000000000000000000000000000
      000000000000000000007B7D7B000000000000000000000000009C9A9C000000
      00000000000000000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF006B696B006B696B00FFFFFF00FFFFFF00FFFBFF006B696B007371
      7300F7F7F700F7F3F700CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF008C8E8C006B696B00DEDFDE00FFFBFF00F7F7
      F700F7F7F700F7F3F700CECBCE00848284000000000000000000ADAEAD00FFFF
      FF00DEDFDE00ADAAAD007B797B0000000000ADAEAD00FFFFFF00DEDFDE00ADAA
      AD007B797B000000000000000000000000000000000000000000000000000000
      000000000000000000007B7D7B000000000000000000000000009C9A9C000000
      00000000000000000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF006B696B006B696B00FFFFFF00FFFFFF00D6D7D6006B696B007371
      7300F7F7F700F7F7F700CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00C6C3C6006B696B00ADAEAD00FFFBFF00FFFB
      FF00F7F7F700F7F7F700CECBCE00848284000000000000000000ADAEAD009C9E
      9C00949694007B797B009C9E9C0000000000ADAEAD009C9E9C00949694007B79
      7B009C9E9C000000000000000000000000000000000000000000000000000000
      000000000000000000007B7D7B000000000000000000000000009C9A9C000000
      00000000000000000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF006B696B006B696B006B696B006B696B006B696B006B6D6B00ADAEAD00F7F3
      F700FFFBFF00F7F7F700CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00D6D3D6006B696B006B696B006B696B0084868400FFFB
      FF00FFFBFF00F7F7F700CECBCE0084828400000000000000000000000000ADAE
      AD00FFFFFF007B797B00000000000000000000000000ADAEAD00FFFFFF007B79
      7B00000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7D7B009C9A9C009C9A9C009C9A9C00000000000000
      00000000000000000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00FFFBFF00CECBCE008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00FFFBFF00CECBCE0084828400000000000000000000000000ADAE
      AD00ADAEAD009C9E9C00000000000000000000000000ADAEAD00ADAEAD009C9E
      9C00000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7D7B00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFBFF00FFFBFF008482840000000000CECFCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFBFF00FFFBFF00848284000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7D7B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE00000000000000000000000000CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007B7D7B007B7D7B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008C8A8C006B696B006B696B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CECFCE007B797B007B797B007B797B007B79
      7B007B797B007B797B007B797B007B797B000000000000000000000000000000
      00000000000000000000D6D3D600848284008482840084828400848284008482
      8400848284008482840084828400848284000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000ADAEAD008C8A8C008C8A8C008C8A8C006B696B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CECFCE00F7F3F700EFEFEF00EFEBEF00E7E7
      E700E7E3E700DEDFDE00DEDFDE007B797B000000000000000000000000000000
      00000000000000000000D6D3D600F7F3F700CECFCE00CECFCE00CECFCE00CECF
      CE00CECFCE00CECFCE00CECFCE00848284000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000ADAEAD008C8A8C00ADAEAD00C6C7C6006B696B0000000000000000008C8A
      8C008C8A8C008482840000000000000000000000000000000000000000000000
      0000000000000000000000000000CECFCE00F7F7F700F7F3F700EFEFEF00EFEB
      EF00EFEBEF00E7E7E700E7E3E7007B797B00000000007B7D7B007B7D7B007B7D
      7B007B7D7B007B7D7B00D6D3D600F7F7F700F7F3F700EFEFEF00EFEBEF00EFEB
      EF00E7E7E700E7E3E700CECFCE00848284000000000084828400000000000000
      0000000000000000000000000000000000008482840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000ADAEAD008C8A8C0000000000C6C7C6006B696B00000000008C8A8C008C8A
      8C00848284008482840084828400000000000000000000000000000000000000
      0000000000000000000000000000CECFCE00FFFBFF00B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500E7E7E7007B797B00BDBABD00E7E7E700BDBABD00BDBA
      BD00BDBABD00BDBABD00D6D3D600FFFBFF00BDBABD00BDBABD00BDBABD00BDBA
      BD00BDBABD00E7E7E700CECFCE00848284000000000000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000ADAEAD00C6C7C60084828400848284006B696B00000000008C8A8C00ADAE
      AD00C6C7C600848284006B696B00000000000000000000000000000000000000
      0000000000000000000000000000CECFCE00FFFFFF00FFFBFF00F7F7F700F7F3
      F700EFEFEF00EFEFEF00EFEBEF007B797B00BDBABD00EFEBEF00E7E7E700E7E3
      E700E7E3E700DEDFDE00D6D3D600FFFFFF00FFFBFF00FFFBFF00F7F7F700F7F3
      F700EFEFEF00EFEBEF00CECFCE00848284000000000000000000C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C6000000000084828400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000ADAEAD00C6C7C600848284006B696B006B696B008C8A8C000000
      0000ADAEAD00848284006B696B0000000000D6D7D60094929400949294009492
      9400949294009492940094929400CECFCE00FFFFFF00B5B2B500B5B2B500B5B2
      B500B5B2B500B5B2B500EFEFEF007B797B00BDBABD00EFEFEF00EFEBEF00E7E7
      E700E7E3E700E7E3E700D6D3D600FFFFFF00BDBABD00BDBABD00BDBABD00BDBA
      BD00BDBABD00EFEFEF00CECFCE0084828400000000008482840000000000C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C6000000000084828400848284000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000ADAEAD007B797B00848684006B696B008C8A8C008482
      84006B696B006B696B000000000000000000D6D7D600F7F3F700EFEFEF00EFEB
      EF00EFEBEF00E7E7E700E7E3E700CECFCE00FFFFFF00FFFFFF00FFFFFF00FFFB
      FF00F7F7F700F7F3F700EFEFEF007B797B00BDBABD00EFEFEF00EFEFEF00EFEB
      EF00E7E7E700E7E3E700D6D3D600FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFB
      FF00F7F7F700F7F3F700CECFCE0084828400000000000000000000000000C6C3
      C600FFFFFF00C6C3C600FFFFFF00C6C3C600FFFFFF0000000000848284008482
      8400000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C9A9C00CECBCE00949694006B696B006B69
      6B008C8A8C00000000000000000000000000D6D7D600F7F7F700F7F3F700EFEF
      EF00EFEBEF00EFEBEF00E7E7E700CECFCE00FFFFFF00B5B2B500B5B2B500FFFF
      FF00CECFCE008C8E8C008C8E8C008C8E8C00BDBABD00EFEFEF00EFEFEF00EFEF
      EF00EFEBEF00E7E7E700D6D3D600FFFFFF00BDBABD00BDBABD00BDBABD00FFFB
      FF00FFFBFF00CECFCE0084828400848284000000000000000000000000000000
      0000C6C3C600FFFFFF00C6C3C600FFFFFF00C6C3C600FFFFFF00000000008482
      8400848284000000000000000000000000000000000000000000000000000000
      000000000000000000009C9A9C00DEDFDE00D6D3D6009C9A9C00000000000000
      000000000000000000000000000000000000D6D7D600FFFBFF00CECBCE00CECB
      CE00CECBCE00CECBCE00CECBCE00CECFCE00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CECFCE00F7F7F7008C8E8C0000000000BDBABD00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEBEF00D6D3D600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00CECFCE00FFFFFF00848284000000000000000000000000000000
      000000000000C6C3C600FFFFFF00C6C3C600FFFFFF00C6C3C600FFFFFF000000
      0000848284008482840084828400000000000000000000000000000000000000
      0000000000009C9A9C00E7E7E7009C9A9C00E7E3E7009C9A9C00000000000000
      000000000000000000000000000000000000D6D7D600FFFFFF00FFFBFF00FFFB
      FF00F7F7F700F7F3F700EFEFEF00CECFCE00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CECFCE009C9E9C000000000000000000BDBABD00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00D6D3D600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CECFCE0084828400000000000000000000000000000000000000
      00000000000000000000C6C3C600FFFFFF00C6C3C600FFFFFF00C6C3C600FFFF
      FF00000000008482840000000000000000000000000000000000000000000000
      00009C9A9C00F7F3F7009C9A9C00BDBEBD00EFEBEF009C9A9C00000000000000
      000000000000000000000000000000000000D6D7D600FFFFFF00CECBCE00CECB
      CE00CECBCE00CECBCE00CECBCE00CECFCE00CECFCE00CECFCE00CECFCE00CECF
      CE00CECFCE00000000000000000000000000BDBABD00EFEFEF00EFEFEF00EFEF
      EF00EFEFEF00EFEFEF00D6D3D600D6D3D600D6D3D600D6D3D600D6D3D600D6D3
      D600D6D3D600D6D3D60000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C3C600FFFFFF00C6C3C600FFFFFF00C6C3
      C600FFFFFF00848284000000000000000000000000000000000000000000BDBE
      BD00FFFFFF009C9A9C0000000000BDBEBD00F7F3F7009C9A9C00000000000000
      000000000000000000000000000000000000D6D7D600FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00FFFBFF00F7F7F700F7F3F7008482840000000000000000000000
      000000000000000000000000000000000000BDBABD00EFEFEF00EFEFEF009C9A
      9C007B7D7B007B7D7B007B7D7B007B7D7B007B7D7B007B7D7B007B7D7B00BDBA
      BD007B7D7B000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C3C600FFFFFF00C6C3C600FFFF
      FF00C6C3C600C6C3C6000000000000000000000000000000000000000000BDBE
      BD009C9A9C000000000000000000BDBEBD00F7F7F7009C9A9C00000000000000
      000000000000000000000000000000000000D6D7D600FFFFFF00CECBCE00CECB
      CE00FFFFFF00D6D7D60084828400848284008482840000000000000000000000
      000000000000000000000000000000000000BDBABD00EFEFEF00EFEFEF009C9A
      9C00EFEFEF00E7E3E7007B7D7B00E7E3E700D6D7D600BDBEBD007B7D7B00BDBA
      BD007B7D7B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008482840000000000000000000000
      000000000000848284008482840000000000000000000000000000000000BDBE
      BD00000000000000000000000000BDBEBD00FFFFFF009C9A9C00000000000000
      000000000000000000000000000000000000D6D7D600FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00D6D7D600F7F7F700848284000000000000000000000000000000
      000000000000000000000000000000000000BDBABD00EFEFEF00EFEFEF00EFEF
      EF009C9A9C00EFEFEF007B7D7B007B7D7B00E7E3E7007B7D7B00DEDFDE00DEDF
      DE007B7D7B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BDBEBD009C9A9C0000000000000000000000
      000000000000000000000000000000000000D6D7D600FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00D6D7D60084828400000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BDBABD00BDBABD00BDBA
      BD00BDBABD009C9A9C00EFEFEF00EFEFEF007B7D7B00BDBABD00BDBABD00BDBA
      BD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BDBEBD000000000000000000000000000000
      000000000000000000000000000000000000D6D7D600D6D7D600D6D7D600D6D7
      D600D6D7D600D6D7D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C9A9C009C9A9C009C9A9C009C9A9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007B7D7B007B7D
      7B007B7D7B007B7D7B007B7D7B007B7D7B007B7D7B007B7D7B007B7D7B007B7D
      7B007B7D7B007B7D7B007B7D7B007B7D7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ADAEAD00C6C7C600C6C3
      C60094929400D6D3D600DEDBDE00DEDFDE00DEDFDE00DEDFDE00DEDFDE00ADAA
      AD008486840084868400848684007B7D7B00000000009C9A9C009C9A9C009C9A
      9C009C9A9C009C9A9C009C9A9C009C9A9C009C9A9C009C9A9C009C9A9C009C9A
      9C00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000949294007B7D7B00000000000000
      0000000000000000000000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000ADAEAD00CECBCE00C6C7
      C60094929400525152008C8A8C00FFFFFF00FFFFFF00FFFFFF00F7F3F700ADAA
      AD008C8A8C0084868400848684007B7D7B00BDBABD00FFFFFF00CECBCE00CECB
      CE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECB
      CE009C9A9C000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000949694009C9E9C007B7D7B000000
      0000000000000000000000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000ADAEAD00CECFCE00CECB
      CE0094929400000000005A595A00FFFFFF00FFFBFF00F7F3F700E7E3E700ADAA
      AD00949294008C8A8C00848684007B7D7B00BDBABD00FFFFFF00FFFFFF00FFFF
      FF00FFFBFF00F7F7F700F7F3F700EFEFEF00EFEBEF007B797B00DEDFDE00CECB
      CE009C9A9C009C9A9C0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000949694009C9E9C007B7D
      7B00000000000000000000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000ADAEAD00D6D7D600CECF
      CE00949294009492940094929400949294009492940094929400949294009492
      94009C9A9C00949294008C8A8C007B7D7B00BDBABD00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFBFF00F7F7F700F7F3F700EFEFEF00B5B6B500E7E3E700CECB
      CE00BDBABD00ADAEAD009C9A9C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000949694009C9E
      9C007B7D7B000000000000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000ADAEAD00DEDFDE00D6D7
      D600D6D3D600CECFCE00CECBCE00C6C3C600BDBABD00B5B6B500ADAEAD00A5A6
      A5009C9E9C009C9A9C00949294007B7D7B00BDBABD00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFBFF00F7F7F700F7F3F700EFEFEF00EFEBEF00E7E3
      E700BDBABD00BDBEBD009C9A9C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000ADAAAD00BDBA
      BD009C9A9C009C9A9C0000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000ADAEAD00DEDFDE00DEDF
      DE00D6D7D600D6D3D600CECFCE00CECBCE00C6C3C600BDBEBD00B5B6B500B5B2
      B500A5A6A5009C9E9C009C9A9C007B7D7B00BDBABD00BDBABD00BDBABD00BDBA
      BD00BDBABD00BDBABD00BDBABD00BDBABD00BDBABD00BDBABD009C9A9C009C9A
      9C00BDBABD00C6C7C6009C9A9C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009496
      9400BDBABD007B7D7B0000000000000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000ADAEAD00DEDFDE00DEDF
      DE00A5A2A500A5A2A500A5A2A500A5A2A500A5A2A500A5A2A500A5A2A500A5A2
      A500A5A2A500ADAAAD00A5A6A5007B7D7B00BDBABD00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00F7F7F700F7F3F700EFEF
      EF00BDBABD00CECFCE009C9A9C0000000000000000009C9E9C006B696B006B69
      6B006B696B006B696B006B696B006B696B000000000000000000000000009496
      9400BDBABD00ADAAAD00A5A6A500000000000000000000000000008284000082
      8400008284000082840000828400008284000082840000828400008284000082
      84000082840000000000000000000000000000000000ADAEAD00DEDFDE00A5A2
      A500FFFBFF00F7F7F700F7F3F700EFEFEF00EFEFEF00EFEBEF00E7E7E700E7E3
      E700E7E3E700A5A2A500ADAAAD007B7D7B0000000000BDBABD00BDBABD00BDBA
      BD00BDBABD00BDBABD00BDBABD00BDBABD00BDBABD00BDBABD00BDBABD00BDBA
      BD00D6D7D600B5B2B5009C9A9C0000000000000000009C9E9C00BDBABD00BDBA
      BD00BDBABD00BDBABD006B696B00000000000000000000000000000000009496
      9400BDBABD00ADAAAD007B7D7B00000000000000000084828400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000084828400000000000000000000000000ADAEAD00DEDFDE00A5A2
      A500FFFFFF00FFFBFF00FFFBFF00F7F7F700F7F3F700EFEFEF00EFEBEF00E7E7
      E700E7E3E700A5A2A500B5B2B5007B7D7B0000000000CECFCE00CECFCE00FFFF
      FF00FFFFFF00FFFFFF00FFFBFF00FFFBFF00FFFBFF00F7F7F700F7F7F7009C9A
      9C00BDBEBD00D6D7D6009C9A9C0000000000000000009C9E9C00BDBABD00BDBE
      BD00BDBABD00B5B6B5006B696B00000000000000000000000000000000009496
      9400BDBABD00ADAAAD007B7D7B0000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000008400000000000000ADAEAD00DEDFDE00A5A2
      A500FFFFFF00FFFFFF00FFFBFF00FFFBFF00F7F7F700F7F3F700EFEFEF00EFEB
      EF00E7E7E700A5A2A500B5B6B5007B7D7B00000000000000000000000000CECF
      CE00FFFFFF00D6D7D600D6D7D600D6D7D600D6D7D600D6D7D600F7F7F7009C9A
      9C009C9A9C009C9A9C009C9A9C0000000000000000009C9E9C00BDBABD00D6D3
      D600BDBEBD00BDBABD00A5A6A5006B696B0000000000000000006B696B00D6D3
      D600BDBABD00ADAAAD009C9A9C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008400000000000000ADAEAD00DEDFDE00A5A2
      A500FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFBFF00F7F7F700F7F3F700EFEF
      EF00EFEBEF00A5A2A500BDBEBD007B7D7B00000000000000000000000000CECF
      CE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFBFF00FFFBFF00F7F7
      F7009C9A9C00000000000000000000000000000000009C9E9C00D6D3D6009C9E
      9C00D6D3D600BDBEBD00BDBABD00A5A6A5006B696B006B696B00D6D3D600BDBA
      BD00BDBABD00ADAAAD00A5A6A500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008400000000000000840000000000000000000000ADAEAD00DEDFDE00A5A2
      A500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFBFF00F7F7F700F7F3
      F700EFEFEF00A5A2A500C6C7C6007B7D7B000000000000000000000000000000
      0000CECFCE00FFFFFF00D6D7D600D6D7D600D6D7D600D6D7D600D6D7D600FFFB
      FF009C9A9C009C9A9C000000000000000000000000009C9E9C009C9E9C000000
      00009C9E9C00D6D3D600BDBEBD00BDBABD00B5B6B500B5B2B500B5B2B500BDBA
      BD00ADAAAD009C9A9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008400000084000000000000000000000000000000ADAEAD00DEDFDE00A5A2
      A500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFBFF00F7F7
      F700F7F3F700A5A2A500000000007B7D7B000000000000000000000000000000
      000000000000CECFCE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFB
      FF00FFFBFF009C9A9C000000000000000000000000009C9E9C00000000000000
      0000000000009C9E9C00D6D3D600D6D3D600D6D3D600D6D3D600BDBABD009C9A
      9C00A5A6A5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008400000084000000840000000000000000000000ADAEAD00DEDFDE00A5A2
      A500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFB
      FF00F7F7F700A5A2A500BDBEBD007B7D7B000000000000000000000000000000
      000000000000CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECFCE00CECF
      CE00CECFCE00CECFCE0000000000000000000000000000000000000000000000
      00000000000000000000A5A6A5009C9E9C009C9E9C009C9E9C00A5A6A5000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ADAEAD00ADAEAD00ADAE
      AD00ADAEAD00ADAEAD00ADAEAD00ADAEAD00ADAEAD00ADAEAD00ADAEAD00ADAE
      AD00ADAEAD00ADAEAD00ADAEAD00ADAEAD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008482
      8400848284008482840084828400848284008482840084828400848284008482
      8400848284008482840084828400848284000000000000000000000000000000
      0000000000000000000084828400848284008482840084828400848284008482
      8400848284008482840084828400848284000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF000000000000000000000000000000000000000000C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600848284000000000000000000000000000000
      00000000000084828400C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C600C6C3C600C6C3C600848284000000000000000000000000000000
      0000000000000000000000000000840084008400840084828400000000000000
      00000000000000000000000000000000000000000000FFFFFF000000000000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
      FF00FFFFFF0000000000FFFFFF0000000000000000000000000000000000C6C3
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600848284000000000000000000000000008482
      840084828400FF000000C6C3C600FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFF
      FF0000FFFF00FFFFFF00C6C3C600848284000000000000000000000000000000
      0000000000008400840084008400FFFFFF00FFFFFF00C6C3C600848284000000
      0000000000000000000000000000000000000000000000FFFF00FFFFFF000000
      000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
      FF0000000000FFFFFF0000FFFF00000000000000000000000000FF000000C6C3
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C60084828400000000000000000084828400FF00
      0000FF000000FF000000C6C3C600FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600848284000000000000000000000000008400
      840084008400FFFFFF00FFFFFF000000000000000000C6C3C600C6C3C6008482
      84000000000000000000000000000000000000000000FFFFFF0000FFFF00FFFF
      FF000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF000000
      0000FFFFFF0000FFFF00FFFFFF000000000000000000FF000000848284008482
      8400848284008482840084828400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600848284000000000084828400FF000000FF00
      0000FF000000FF000000C6C3C600FFFFFF0000FFFF0084828400848284008482
      8400C6C3C600FFFFFF00C6C3C60084828400848284008400840084008400FFFF
      FF00FFFFFF000000000000000000840084008400840000000000C6C3C600C6C3
      C600848284000000000000000000000000000000000000FFFF00FFFFFF000000
      0000FFFFFF000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFF
      FF0000000000FFFFFF0000FFFF000000000084828400FF000000848284008482
      8400FF000000FF000000FF000000FF00000084828400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C600848284000000000084828400FF000000FF00
      0000FF000000FF000000C6C3C600FFFFFF0084828400FF000000FF000000FF00
      000084828400FFFFFF00C6C3C600848284008482840084008400FFFFFF000000
      000000000000840084008400840084008400840084008400840000000000C6C3
      C600C6C3C60084828400000000000000000000000000FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF0000000000848284008482840084828400FF00
      0000FF000000C6C3C600C6C3C60084828400FF00000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840084828400FF000000FF000000FF00
      0000FF000000FF000000C6C3C600FFFFFF0084828400C6C3C60084828400FF00
      000084828400FFFFFF00C6C3C600848284008482840000000000000000008400
      840084008400840084000082840000FFFF008400840084008400840084000000
      0000C6C3C600C6C3C60084828400000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00000000000000000084828400C6C3C60084828400FF00
      000084828400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840084828400FF000000FF000000FF00
      00008482840084828400C6C3C600FFFFFF0084828400FFFFFF00C6C3C6008482
      840084828400FFFFFF00C6C3C600848284008482840084008400840084008400
      8400840084008400840084008400008284008400840084008400840084008400
      840000000000C6C3C600000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FF000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000008482840000FFFF00FF00
      0000FF000000FF000000FF000000FF000000FF00000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840084828400FF000000FF0000008482
      84008482840084828400C6C3C600FFFFFF0000FFFF0084828400848284008482
      8400C6C3C600FFFFFF00C6C3C600848284000000000084008400FFFFFF008400
      84008400840084008400840084008400840000FFFF0000FFFF00840084008400
      84008400840000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FF000000FF000000FF00
      0000FFFFFF00FFFFFF00FFFFFF000000000000000000000000008482840000FF
      FF00FF000000FFFFFF00FFFFFF0084828400FF00000084828400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840084828400FF000000FF0000008482
      84008482840084828400C6C3C600FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFF
      FF0084828400848284008482840084828400000000000000000084008400FFFF
      FF0084008400840084008400840084008400840084000082840000FFFF0000FF
      FF00840084008400840000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FF000000FF000000FFFFFF00FF00
      0000FF000000FFFFFF0000000000000000000000000000000000000000008482
      840000FFFF00FF000000FF000000FF00000084828400FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840084828400FF000000FF000000C6C3
      C6008482840084828400C6C3C600FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFF
      FF00C6C3C600FFFFFF00C6C3C600000000000000000000000000000000008400
      8400FFFFFF00840084008400840084008400008284008400840000FFFF0000FF
      FF0084008400840084008400840000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FF000000FF000000FFFFFF00FFFFFF00FFFF
      FF00FF000000FF0000000000000000000000000000000000000000000000C6C3
      C600FF000000FF000000FF00000084828400FFFFFF00FF000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6C3C6008482840000000000C6C3C600FF000000FF00
      0000FFFFFF00C6C3C600C6C3C600FFFFFF00FFFFFF00FFFFFF0000FFFF00FFFF
      FF00C6C3C600C6C3C60000000000000000000000000000000000000000000000
      000084008400FFFFFF00840084008400840000FFFF0000FFFF0000FFFF008400
      8400840084008400840000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FF000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FF000000FF00000000000000000000000000000000000000C6C3
      C600FFFFFF00FFFFFF00FFFFFF00FF000000FF000000FFFFFF00FFFFFF00FFFF
      FF008482840084828400848284008482840000000000C6C3C600FF000000FFFF
      FF00C6C3C600FFFFFF00C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C6008482840000000000000000000000000000000000000000000000
      00000000000084008400FFFFFF00840084008400840084008400840084008400
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      00000000000000000000FF000000FF000000000000000000000000000000C6C3
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00C6C3C600FFFFFF00C6C3C600000000000000000000000000C6C3C600FF00
      0000FF000000C6C3C600FFFFFF00C6C3C6008482840084828400848284008482
      8400848284000000000000000000000000000000000000000000000000000000
      0000000000000000000084008400FFFFFF008400840084008400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000FF000000000000000000000000000000C6C3
      C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00C6C3C600C6C3C6000000000000000000000000000000000000000000C6C3
      C600C6C3C600FF000000FF000000FFFFFF00C6C3C60084828400848284008482
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840084008400840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C6C3
      C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600C6C3
      C600C6C3C6000000000000000000000000000000000000000000000000000000
      000000000000C6C3C600C6C3C600C6C3C600C6C3C600C6C3C600000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000B00000000100010000000000800500000000000000000000
      000000000000000000000000FFFFFF00FEFFFEFF00000000FE7FFE7F00000000
      0000800000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000800000000000
      FFF0FFF000000000FFF9FFF900000000C000FEFFFEFFFEFF8000FE7FFE7FFE7F
      8000000000000000800000000000000080000000000000008000000000000000
      8000000000000000800000000000000080000000000000008000000000000000
      8000000000000000800000000000000080000000000000008000000000000000
      8000FFF0FFF0FFF08000FFF9FFF9FFF9FFFFC001FFFFFFFFC0078000F0018001
      80038000C07F800180038000803F800180038000801F800180038000800F8001
      80038000C007800180038000E003800180038000F0018001C0078000F8018001
      E38F8000FC018001E38F8000FE01818FE00F8000FF03C38FF01F8000FF87FFA4
      F83FC001FFFFFFF1FFFFFFFFFFFFFFFFF87FFE7FF800FFFFC000FC3FF8008000
      8000F81FF80080008000F00FE00080008000E007E00080008000C003E0008000
      8000800180008000800000008000800080000000800180008000800180038000
      8000C003800380008000E007800700008000F00F800F0000C000F81F800F0000
      F07FFC3FC01F0001FC7FFE7FFFFF07FFFFFFFFFFC001C001FFFF803FC001C001
      000F803FC001C001000F803FC001C00100078000C001C00100078000C001C001
      00038000C001C00100038000C001C00100018000C001C00100018000C001C001
      00078000C001C0010000F000C001C00103F8F801C001C00187F8FC03C003C003
      FF92FE07C007C007FFC7FF0FC00FC00FC001C001FFFFFFFF80008000FC00FFFF
      80008000FC00800380008000FC00800380008000FC0080038000800000008003
      8000800000008003800080000000800380008000003F80038000800000018003
      8000800000018003800080000001800380008000F801800380008000F8018003
      C001C001F8018003FFFFFFFFF801FFFFC001FFFFC001C00180009C0380008000
      80000C038000800080000C038000800080009FFF800080008000FFFF80008000
      80009C038000800080000C038000800080000C038000800080009FFF80008000
      8000FFFF8000800080009C038000800080000C038000800080000C0380008000
      C0019FFFC001C001FFFFFFFFFFFFFFFFFFFFFFFFC001C0010381FE0980008000
      0381F6D6800080000381C31780008000038196D6800080000381BF1980008000
      0001BFFF800080000001FFFF800080008003803F80008000C007B5DF80008000
      C107C5DF80008000C107B5DF80008000E38FC43F80008000E38FFDFF80008000
      FFFFFDFFC001C001FFFFF9FFFFFFFFFFF8FFFE00FC00FFFFF07FFE00FC00FFFF
      F063FE008000807FF241FE000000803FF041FE000000801FF81100000000800F
      FC0300000000C007FE0700000000E003FC3F00010000F001F83F00030001F801
      F03F00070003FC01E23F007F0007FE01E63F007F0007FF01EE3F00FF0007FFFF
      FE7F01FF800FFFFFFEFF03FFF87FFFFFFFFFC000FFFFFFFF80038000800FFF3F
      800380000007FF1F800380000003FF8F800380000001FFC7800380000001FFC3
      800380000001FFE380038000000180E180038000800181E180038000800181E1
      C1FE8000E00180C1E3FE8000E0078001FFF58000F0039003FFF38000F803B807
      FFF18000F803FC1FFFFF8000FFFFFFFFFFFF8001E000FC00FE3F0000E000F800
      F81F0000E000E000E00F0000C000C00080070000800080000003000000008000
      00010000000000000000000000000000000100008000000080010000C0000000
      C0018001E0000001E000C003E0008003F000E001E0008003F803F00CE001C007
      FC0FF81EE003E00FFE3FFC3FE007F83F00000000000000000000000000000000
      000000000000}
  end
  object ilStatusBarImages: TImageList
    Left = 264
    Top = 184
    Bitmap = {
      494C010106000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000002000000001002000000000000020
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000BDBABD00848284008482840084828400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B696B006B696B006B696B006B696B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BDBABD00E7E3E7009C9E9C009C9E9C0084828400848284000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B696B008C8E8C00ADAEAD006B696B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BDBABD00FFFFFF00DEDFDE00CECFCE009C9E9C00848284000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B696B0094929400ADAEAD006B696B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BDBABD00FFFFFF00E7E3E700CECFCE009C9E9C00848284000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B696B0094969400ADAEAD006B696B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BDBABD00FFFFFF00E7E3E700CECFCE009C9E9C00848284000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B696B009C9A9C00ADAEAD006B696B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BDBABD00C6C7C600C6C7C600C6C7C6009C9E9C00848284000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B696B009C9E9C00ADAEAD006B696B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BDBABD00D6D3D600DEDBDE00E7E3E700CECFCE00C6C3C600BDBABD000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B696B00A5A6A500ADAEAD006B696B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BDBA
      BD00E7E7E700EFEFEF00DEDBDE00EFEBEF00DEDBDE00EFEBEF00E7E7E700BDBA
      BD00000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B696B006B696B008C8E8C006B696B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CECBCE00DEDF
      DE00EFEFEF00EFEFEF00DEDFDE00EFEFEF00E7E7E700E7E3E700EFEBEF00DEDF
      DE00CECBCE000000000000000000000000000000000000000000000000000000
      000000000000CECBCE009C9A9C009C9A9C009C9A9C009C9A9C009C9A9C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BDBEBD00F7F3
      F700F7F3F700F7F3F700DEDFDE00EFEFEF00E7E7E700DEDFDE00EFEBEF00EFEB
      EF00BDBEBD00000000000000000000000000000000006B6D6B006B6D6B006B6D
      6B0000000000CECBCE00EFEFEF00EFEFEF00EFEFEF00EFEFEF009C9A9C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BDBABD00F7F7
      F700F7F3F700F7F3F700EFEFEF00EFEBEF00EFEFEF00EFEBEF00EFEBEF00EFEB
      EF00BDBABD000000000000000000000000000000000000000000000000000000
      000000000000CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BDBEBD00F7F7
      F700FFFFFF00FFFFFF00FFFFFF00F7F7F700EFEFEF00EFEFEF00EFEFEF00EFEB
      EF00BDBEBD000000000000000000000000000000000000000000000000000000
      000000000000000000006B696B00A5A6A500ADAEAD006B696B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CECBCE00EFEB
      EF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFEFEF00EFEFEF00EFEFEF00E7E3
      E700CECBCE000000000000000000000000000000000000000000000000000000
      000000000000000000006B696B00A5A6A500ADAEAD006B696B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BDBA
      BD00F7F7F700FFFFFF00FFFFFF00FFFBFF00F7F3F700EFEFEF00E7E3E700BDBA
      BD00000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B696B00A5A6A500ADAEAD006B696B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BDBABD00EFEBEF00F7F7F700F7F7F700F7F3F700EFEBEF00BDBABD000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B696B00A5A6A500ADAEAD006B696B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CECBCE00BDBEBD00BDBABD00BDBEBD00CECBCE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006B696B006B696B006B696B006B696B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000010141000101410001014
      1000101410001014100010141000101410001014100010141000101410001014
      1000101410001014100010141000000000000000000000000000000000000000
      00000000000000000000A58E7B006B4531006B4531006B453100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C3000009C3000009C3000009C300000000000000000
      0000000000000000000000000000000000000000000042414200424142004241
      4200424142004241420042414200424142004241420042414200424142004241
      420042414200424142004241420000000000529252009CD7AD00396D3900396D
      3900396D3900396D3900396D3900396D3900396D3900396D3900396D3900396D
      3900396D3900396D3900396D3900101410000000000000000000000000000000
      000000000000A58E7B00D6CFCE0084655200846552006B4531006B4531000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C300000CE610000FF9A00009C300000000000000000
      000000000000000000000000000000000000ADAEAD00DEDFDE00949694009496
      9400949694009496940094969400949694009496940094969400949694009496
      94009496940094969400949694004241420052925200A5DBAD009CD7AD0094D3
      A50084D39C007BCF940073CB8C006BCB840063C77B005AC37B004ABE73004ABE
      6B0042BA630039BA6300396D3900101410000000000000000000000000000000
      000000000000A58E7B00FFFFFF00D6C3C600C6A6A500846552006B4531000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C300000CE650000FF9A00009C300000000000000000
      000000000000000000000000000000000000ADAEAD00DEDFDE00DEDFDE00DEDB
      DE00D6D7D600D6D7D600D6D3D600CECFCE00CECFCE00CECBCE00C6C7C600C6C3
      C600C6C3C600C6C3C600949694004241420052925200ADDFB500A5DBAD00B5A2
      94006B4531006B4531006B4531006B4531006B4531006B4531006B4531006B45
      31004ABE6B0042BA6300396D3900101410000000000000000000000000000000
      000000000000A58E7B00FFFFFF00DEC7C600C6AAAD00846552006B4531000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C300000D66D0000FF9A00009C300000000000000000
      000000000000000000000000000000000000ADAEAD00E7E3E700DEDFDE00CECB
      CE00848284008482840084828400848284008482840084828400848284008482
      8400C6C3C600C6C3C600949694004241420052925200B5DFBD00ADDFB500B5A2
      9400FFF3EF00FFEFE700FFEBDE00FFE7DE00FFE3D600FFDFCE00FFDFCE006B45
      310052C373004ABE6B00396D3900101410000000000000000000000000000000
      000000000000A58E7B00FFFFFF00DEC7C600C6AAAD00846552006B4531000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C300000DE710000FF9A00009C300000000000000000
      000000000000000000000000000000000000ADAEAD00E7E7E700E7E3E700CECB
      CE00F7F7F700F7F7F700F7F3F700F7F3F700EFEFEF00EFEFEF00EFEFEF008482
      8400C6C7C600C6C3C600949694004241420052925200BDE3C600BDE3BD00B5A2
      9400FFF7EF00BD927B00BD927B00BD927B00BD927B00BD927B00FFDFCE006B45
      31005AC77B0052C37300396D3900101410000000000000000000000000000000
      000000000000A58E7B00BD9A9C00BD9A9C00BD9A9C00846552006B4531000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C300000DE790000FF9A00009C300000000000000000
      000000000000000000000000000000000000ADAEAD00EFEBEF00E7E7E700CECB
      CE00FFFBFF00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00EFEFEF008482
      8400CECBCE00C6C7C600949694004241420052925200C6E3C600C6E3C600B5A2
      9400FFFBF700FFF7EF00FFF3EF00FFEFE700FFEBE700FFE7DE00FFE3D6006B45
      310063C784005AC77B00396D3900101410000000000000000000000000000000
      0000AD868400C6B2A500D6BEB500DEC7C600C6AAA500B5968C00AD8684000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C300000E7820000FF9A00009C300000000000000000
      000000000000000000000000000000000000ADAEAD00EFEBEF00EFEBEF00CECB
      CE00FFFFFF00FFFBFF00FFFBFF00F7F7F700F7F7F700F7F3F700EFEFEF008482
      8400CECFCE00CECBCE00949694004241420052925200C6E3C600C6E3C600B5A2
      9400FFFFFF00BD927B00BD927B00BD927B00BD927B00BD927B00FFE7DE006B45
      310073CB8C0063C78400396D390010141000000000000000000000000000AD86
      8400E7D3C600F7DFCE00D6BEAD00F7DFCE00D6BEAD00F7D7C600EFD3BD00AD86
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C3000009C300000CE6100009C300000000000000000
      000000000000000000000000000000000000ADAEAD00EFEBEF00EFEBEF00CECB
      CE00FFFFFF00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00F7F3F7008482
      8400D6D3D600CECFCE00949694004241420052925200C6E3C600C6E3C600B5A2
      9400FFFFFF00FFFFFF00FFFBFF00FFF7F700FFF3EF00FFEFE700FFEBE7006B45
      31007BCF940073CB8C00396D3900101410000000000000000000A5A6A500D6C7
      BD00F7E3D600F7E3D600DEC7BD00F7DFCE00EFD3C600E7CBBD00F7D7C600DEC3
      B500A5A6A5000000000000000000000000000000000000000000000000000000
      000000000000319AFF0000619400006194000061940000619400006194000000
      000000000000000000000000000000000000ADAEAD00EFEBEF00EFEBEF00CECB
      CE00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFBFF00F7F7F700F7F7F7008482
      8400D6D3D600D6D3D600949694004241420052925200C6E3C600C6E3C600B5A2
      9400FFFFFF00BD927B00BD927B00BD927B00BD927B00BD927B00FFEFE7006B45
      310084CF94007BCF9400396D39001014100000000000000000009C928C00F7E7
      DE00F7E7DE00F7E7DE00DECBBD00EFDFD600E7D7CE00DEC7BD00F7DBC600F7DB
      C6009C928C000000000000000000000000000000000031303100313031003130
      310000000000319AFF0000FFFF0000FFFF0000FFFF0000FFFF00006194000000
      000000000000000000000000000000000000ADAEAD00EFEBEF00EFEBEF00CECB
      CE00FFFFFF00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00F7F7F7008482
      8400D6D7D600D6D3D600949694004241420052925200C6E3C600C6E3C600B5A2
      9400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFF7F700FFF3EF006B45
      31008CD39C0084CF9400396D3900101410000000000000000000AD868400FFEF
      E700F7EBDE00F7EBDE00F7E3DE00E7D7CE00EFDFD600EFDBCE00F7DBCE00F7D7
      C600AD8684000000000000000000000000000000000000000000000000000000
      000000000000319AFF00319AFF00319AFF00319AFF00319AFF00319AFF000000
      000000000000000000000000000000000000ADAEAD00EFEBEF00EFEBEF00CECB
      CE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF00FFFBFF008482
      8400DEDBDE00D6D7D600949694004241420052925200C6E3C600C6E3C600B5A2
      9400FFFFFF00BD927B00BD927B00BD927B00BD927B00BD927B00FFF7F7006B45
      31009CD7AD0094D3A500396D39001014100000000000000000009C928C00FFEF
      EF00FFFBF700FFFFFF00FFFFFF00FFEFE700F7E3D600F7E3D600F7DFCE00F7DB
      CE009C928C000000000000000000000000000000000000000000000000000000
      000000000000000000009C300000EF860000FF9A00009C300000000000000000
      000000000000000000000000000000000000ADAEAD00EFEBEF00EFEBEF00CECB
      CE00FFFFFF00BDBEBD00BDBEBD00BDBEBD00BDBEBD00BDBEBD00FFFBFF008482
      8400DEDFDE00DEDBDE00949694004241420052925200C6E3C600C6E3C600B5A2
      9400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFBFF006B45
      3100A5DBAD009CD7AD00396D3900101410000000000000000000A5A6A500F7DB
      CE00FFFFFF00FFFFFF00FFFFFF00FFFBFF00F7E7DE00F7E3D600F7DFCE00E7CF
      BD00A5A6A5000000000000000000000000000000000000000000000000000000
      000000000000000000009C300000EF860000FF9A00009C300000000000000000
      000000000000000000000000000000000000ADAEAD00EFEBEF00EFEBEF00CECB
      CE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008482
      8400DEDFDE00DEDFDE00949694004241420052925200C6E3C600C6E3C600B5A2
      9400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A29400B5A2
      9400ADDFB500A5DBAD00396D390010141000000000000000000000000000AD86
      8400FFEFE700FFFFFF00FFFFFF00FFF3EF00F7E7DE00F7E3D600E7CFBD00AD86
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C300000EF860000FF9A00009C300000000000000000
      000000000000000000000000000000000000ADAEAD00EFEBEF00EFEBEF00CECB
      CE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECBCE00CECB
      CE00E7E3E700DEDFDE00949694004241420052925200C6E3C600C6E3C600C6E3
      C600C6E3C600C6E3C600C6E3C600C6E3C600C6E3C600C6E3C600C6E3C600BDE3
      C600B5DFBD00ADDFB500A5DBAD00101410000000000000000000000000000000
      0000AD868400F7DBCE00FFF3EF00F7EBE700F7EBDE00F7DBCE00AD8684000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C300000EF860000FF9A00009C300000000000000000
      000000000000000000000000000000000000ADAEAD00EFEBEF00EFEBEF00EFEB
      EF00EFEBEF00EFEBEF00EFEBEF00EFEBEF00EFEBEF00EFEBEF00EFEBEF00EFEB
      EF00E7E7E700E7E3E700DEDFDE00424142000000000052925200529252005292
      5200529252005292520052925200529252005292520052925200529252005292
      5200529252005292520052925200000000000000000000000000000000000000
      000000000000A5A6A5009C928C00AD8684009C928C00A5A6A500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C3000009C3000009C3000009C300000000000000000
      00000000000000000000000000000000000000000000ADAEAD00ADAEAD00ADAE
      AD00ADAEAD00ADAEAD00ADAEAD00ADAEAD00ADAEAD00ADAEAD00ADAEAD00ADAE
      AD00ADAEAD00ADAEAD00ADAEAD0000000000424D3E000000000000003E000000
      2800000040000000200000000100010000000000000100000000000000000000
      000000000000000000000000FFFFFF00FC3FFC3F00000000F81FEC3700000000
      F81FFC3F00000000F81FFC3F00000000F81FFC3F00000000F81FCC3300000000
      F01FFC3F00000000E00FFC3F00000000C007F81F00000000C007881100000000
      C007F81F00000000C007FC3F00000000C007FC3F00000000E00F0C3000000000
      F01FFC3F00000000F83FFC3F000000008001FC3FFC3F80010000F81FEC370000
      0000F81FFC3F00000000F81FFC3F00000000F81FFC3F00000000F81FCC330000
      0000F01FFC3F00000000E00FFC3F00000000C007F81F00000000C00788110000
      0000C007F81F00000000C007FC3F00000000C007FC3F00000000E00F0C300000
      0000F01FFC3F00008001F83FFC3F800100000000000000000000000000000000
      000000000000}
  end
  object FindDialog: TFindDialog
    OnFind = FindOne
    Left = 88
    Top = 184
  end
  object ReplaceDialog: TReplaceDialog
    OnFind = FindOne
    OnReplace = ReplaceOne
    Left = 48
    Top = 184
  end
end
