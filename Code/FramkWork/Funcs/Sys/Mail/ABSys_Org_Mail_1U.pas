unit ABSys_Org_Mail_1U;

interface

uses
  ABPubSelectCheckTreeViewU,
  ABPubManualProgressBarU,
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,
  ABPubShowEditDatasetU,
  ABPubMessageU,

  ABThirdCacheDatasetU,
  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFramkWorkControlU,
  ABFramkWorkQueryU,
  ABFramkWorkFuncU,
  ABFramkWorkDictionaryQueryU,

  cxLookAndFeels,
  cxButtons,
  cxLookAndFeelPainters,
  cxControls,
  cxLabel,
  cxEdit,
  cxContainer,
  cxMemo,
  cxTextEdit,
  cxButtonEdit,
  cxMaskEdit,
  cxClasses,
  cxGraphics,
  dxStatusBar,
  dxBar,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,DB,
  DBClient,ImgList,StdCtrls,ComCtrls,DBCtrls,ExtCtrls,RichEdit,Menus, ABPubFormU,
  dxBarExtItems;

type
  TABSys_Org_Mail_1Form = class(TABPubForm)
    dxStatusBar: TABdxStatusBar;
    BarManager: TdxBarManager;
    BarManagerBar2: TdxBar;         
    BarManagerBar3: TdxBar;
    BarManagerBar4: TdxBar;
    BarManagerBar5: TdxBar;
    dxBarButtonPrint: TdxBarLargeButton;
    dxBarButtonExit: TdxBarLargeButton;
    dxBarButtonUndo: TdxBarLargeButton;
    dxBarButtonCut: TdxBarLargeButton;
    dxBarButtonCopy: TdxBarLargeButton;
    dxBarButtonPaste: TdxBarLargeButton;
    dxBarButtonClear: TdxBarLargeButton;
    dxBarButtonFind: TdxBarLargeButton;
    dxBarButtonReplace: TdxBarLargeButton;
    dxBarComboFontSize: TdxBarCombo;
    dxBarButtonBold: TdxBarLargeButton;
    dxBarButtonItalic: TdxBarLargeButton;
    dxBarButtonUnderline: TdxBarLargeButton;
    dxBarButtonBullets: TdxBarLargeButton;
    dxBarButtonAlignLeft: TdxBarLargeButton;
    dxBarButtonCenter: TdxBarLargeButton;
    dxBarButtonAlignRight: TdxBarLargeButton;
    dxBarButtonProtected: TdxBarLargeButton;
    dxBarButtonFont: TdxBarLargeButton;
    dxBarButtonEnhancedStyle: TdxBarLargeButton;
    dxBarButtonFlatStyle: TdxBarLargeButton;
    dxBarButtonStdStyle: TdxBarLargeButton;
    dxBarMRUFiles: TdxBarMRUListItem;
    dxBarComboFontColor: TdxBarColorCombo;
    dxBarButtonXPStyle: TdxBarLargeButton;
    dxBarButtonOffice11Style: TdxBarLargeButton;
    dxBarComboFontName: TdxBarFontNameCombo;
    dxBarGroup1: TdxBarGroup;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    PrintDialog: TPrintDialog;
    FontDialog: TFontDialog;
    dxBarPopupMenu: TdxBarPopupMenu;
    Images: TImageList;
    ilHotImages: TImageList;
    ilDisabledImages: TImageList;
    ilStatusBarImages: TImageList;
    FindDialog: TFindDialog;
    ReplaceDialog: TReplaceDialog;
    Panel1: TPanel;
    cxLabel1: TcxLabel;
    cxLabel3: TcxLabel;
    cxMemo1: TcxMemo;
    ABcxButtonEdit1: TABcxButtonEdit;
    pnl1: TPanel;
    btn1: TABcxButton;
    btn2: TABcxButton;
    dxBarCombo1: TdxBarCombo;
    dxBarButtonSave: TdxBarLargeButton;
    Editor: TRichEdit;
    ABcxButton1: TABcxButton;
    procedure ABcxButtonEdit1DblClick(Sender: TObject);
    procedure ABcxButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure btn2Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure dxBarButtonSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxBarButtonPrintClick(Sender: TObject);
    procedure dxBarButtonCutClick(Sender: TObject);
    procedure dxBarButtonCopyClick(Sender: TObject);
    procedure dxBarButtonPasteClick(Sender: TObject);
    procedure dxBarButtonUndoClick(Sender: TObject);
    procedure dxBarButtonClearClick(Sender: TObject);
    procedure dxBarButtonFindClick(Sender: TObject);
    procedure dxBarButtonReplaceClick(Sender: TObject);
    procedure dxBarButtonBoldClick(Sender: TObject);
    procedure dxBarButtonItalicClick(Sender: TObject);
    procedure dxBarButtonUnderlineClick(Sender: TObject);
    procedure dxBarButtonAlignLeftClick(Sender: TObject);
    procedure dxBarButtonCenterClick(Sender: TObject);
    procedure dxBarButtonAlignRightClick(Sender: TObject);
    procedure dxBarButtonBulletsClick(Sender: TObject);
    procedure dxBarButtonProtectedClick(Sender: TObject);
    procedure dxBarButtonFontClick(Sender: TObject);
    procedure dxBarComboFontNameChange(Sender: TObject);
    procedure dxBarComboFontSizeChange(Sender: TObject);
    procedure dxBarComboFontColorChange(Sender: TObject);
    procedure dxBarButtonStdStyleClick(Sender: TObject);
    procedure dxBarButtonEnhancedStyleClick(Sender: TObject);
    procedure dxBarButtonFlatStyleClick(Sender: TObject);
    procedure dxBarButtonXPStyleClick(Sender: TObject);
    procedure dxBarButtonOffice11StyleClick(Sender: TObject);
    procedure EditorChange(Sender: TObject);
    procedure EditorSelectionChange(Sender: TObject);
    procedure EditorMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FindOne(Sender: TObject);
    procedure ReplaceOne(Sender: TObject);
    procedure ABcxButton1Click(Sender: TObject);
    procedure ABcxButtonEdit1PropertiesChange(Sender: TObject);
    procedure cxMemo1PropertiesChange(Sender: TObject);
  private
    FUpdating: Boolean;
    procedure SetButtonEdit1Value;
    function GetEditorCol: Integer;
    function GetEditorRow: Integer;
    procedure SetModified(Value: Boolean);
    function CheckInput: Boolean;
    { Private declarations }
  public
    Ma_Guid:string;
    property EditorCol: Integer read GetEditorCol;
    property EditorRow: Integer read GetEditorRow;
    { Public declarations }        
  end;

var
  ABSys_Org_Mail_1Form: TABSys_Org_Mail_1Form;

implementation

{$R *.dfm}
uses
  ABSys_Org_MailU;

const
  sRichEditFoundResultCaption = 'Information';
  sRichEditTextNotFound = 'The search text is not found.';
  sRichEditReplaceAllResult = 'Replaced %d occurances.';
  
procedure TABSys_Org_Mail_1Form.ABcxButton1Click(Sender: TObject);
begin
  if ABcxButtonEdit1.Text=EmptyStr then
  begin
    ABcxButtonEdit1.Text:= 'admin,sysuser';
  end
  else
  begin
    if abpos(',admin,',','+ABcxButtonEdit1.Text+',')<=0 then
      ABcxButtonEdit1.Text:=ABcxButtonEdit1.Text+ ',admin';
    if abpos(',sysuser,',','+ABcxButtonEdit1.Text+',')<=0 then
      ABcxButtonEdit1.Text:=ABcxButtonEdit1.Text+ ',sysuser';
  end;
end;

procedure TABSys_Org_Mail_1Form.ABcxButtonEdit1DblClick(Sender: TObject);
begin
  SetButtonEdit1Value;
end;

procedure TABSys_Org_Mail_1Form.ABcxButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  SetButtonEdit1Value;
end;

procedure TABSys_Org_Mail_1Form.ABcxButtonEdit1PropertiesChange(
  Sender: TObject);
begin
  dxBarButtonSave.Enabled := true;
end;

procedure TABSys_Org_Mail_1Form.btn1Click(Sender: TObject);
begin
  ModalResult:=mrCancel;
end;                          

procedure TABSys_Org_Mail_1Form.btn2Click(Sender: TObject);
begin
  if CheckInput then
    ModalResult:=mrOk;
end;

function TABSys_Org_Mail_1Form.CheckInput:Boolean;
var
  i: LongInt;
  tempstr: string;
  tempDataset:TDataSet;
begin
  Result:=true;

  if (ABcxButtonEdit1.Text)=EmptyStr then
  begin
    Result:=false;
    ABShow('收件人不能为空，请检查。');
    exit;
  end;

  if (cxMemo1.Text)=EmptyStr then
  begin
    Result:=false;
    ABShow('标题不能为空，请检查。');
    exit;
  end;

  if (dxBarCombo1.Text)=EmptyStr then
  begin
    Result:=false;
    ABShow('优先级不能为空，请检查。');
    exit;
  end;

  tempDataset:=ABGetDataset('Main',
                            ' SELECT  Op_Code '+
                            ' FROM ABSys_Org_Operator ',[],nil,true);
  try
    for I := 1 to ABGetSpaceStrCount(ABcxButtonEdit1.Text, ',') do
    begin
      tempstr := ABGetSpaceStr(ABcxButtonEdit1.Text, i, ',');
      if not tempDataset.Locate('Op_Code',tempstr,[]) then
      begin
        Result:=false;
        ABShow('[%s]不是合法的收件人,请检查.',[tempstr]);
      end;
    end;
  finally
    tempDataset.Free;
  end;
end;

procedure TABSys_Org_Mail_1Form.cxMemo1PropertiesChange(Sender: TObject);
begin
  dxBarButtonSave.Enabled := true;
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonAlignLeftClick(Sender: TObject);
begin
  if TdxBarLargeButton(Sender).Down then
    Editor.Paragraph.Alignment := TAlignment(TdxBarLargeButton(Sender).Tag)
  else
    Editor.Paragraph.Alignment := taLeftJustify;
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonAlignRightClick(Sender: TObject);
begin
  if TdxBarLargeButton(Sender).Down then
    Editor.Paragraph.Alignment := TAlignment(TdxBarLargeButton(Sender).Tag)
  else
    Editor.Paragraph.Alignment := taLeftJustify;
  if TdxBarLargeButton(Sender).Down then
    Editor.Paragraph.Alignment := TAlignment(TdxBarLargeButton(Sender).Tag)
  else
    Editor.Paragraph.Alignment := taLeftJustify;
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonBoldClick(Sender: TObject);
begin
  if dxBarButtonBold.Down then
    Editor.SelAttributes.Style := Editor.SelAttributes.Style + [fsBold]
  else
    Editor.SelAttributes.Style := Editor.SelAttributes.Style - [fsBold];
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonBulletsClick(Sender: TObject);
begin
  Editor.Paragraph.Numbering := TNumberingStyle(dxBarButtonBullets.Down);
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonCenterClick(Sender: TObject);
begin
  if TdxBarLargeButton(Sender).Down then
    Editor.Paragraph.Alignment := TAlignment(TdxBarLargeButton(Sender).Tag)
  else
    Editor.Paragraph.Alignment := taLeftJustify;
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonClearClick(Sender: TObject);
begin
  Editor.ClearSelection;
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonCopyClick(Sender: TObject);
begin
  Editor.CopyToClipboard;
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonCutClick(Sender: TObject);
begin
  Editor.CutToClipboard;
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonEnhancedStyleClick(Sender: TObject);
begin
  BarManager.Style := bmsEnhanced;
  dxStatusBar.PaintStyle := stpsStandard;
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonFindClick(Sender: TObject);
begin
  Editor.SelLength := 0;
  FindDialog.Execute;
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonFlatStyleClick(Sender: TObject);
begin
  BarManager.Style := bmsFlat;
  dxStatusBar.PaintStyle := stpsFlat;
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonFontClick(Sender: TObject);
begin
  FontDialog.Font.Assign(Editor.SelAttributes);
  if FontDialog.Execute then
    Editor.SelAttributes.Assign(FontDialog.Font);
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonItalicClick(Sender: TObject);
begin
  if dxBarButtonItalic.Down then
    Editor.SelAttributes.Style := Editor.SelAttributes.Style + [fsItalic]
  else
    Editor.SelAttributes.Style := Editor.SelAttributes.Style - [fsItalic];
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonOffice11StyleClick(Sender: TObject);
begin
  BarManager.Style := bmsOffice11;
  dxStatusBar.PaintStyle := stpsOffice11;
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonPasteClick(Sender: TObject);
begin
  Editor.PasteFromClipboard;
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonPrintClick(Sender: TObject);
begin
  if PrintDialog.Execute then Editor.Print(('打印'));
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonProtectedClick(Sender: TObject);
begin
  Editor.SelAttributes.Protected := not Editor.SelAttributes.Protected;
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonReplaceClick(Sender: TObject);
begin
  Editor.SelLength := 0;
  ReplaceDialog.Execute;
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonStdStyleClick(Sender: TObject);
begin
  BarManager.Style := bmsStandard;
  dxStatusBar.PaintStyle := stpsStandard;
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonUnderlineClick(Sender: TObject);
begin
  if dxBarButtonUnderline.Down then
    Editor.SelAttributes.Style := Editor.SelAttributes.Style + [fsUnderline]
  else
    Editor.SelAttributes.Style := Editor.SelAttributes.Style - [fsUnderline];
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonUndoClick(Sender: TObject);
begin
  SendMessage(Editor.Handle, EM_UNDO, 0, 0);
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonXPStyleClick(Sender: TObject);
begin
  BarManager.Style := bmsXP;
  dxStatusBar.PaintStyle := stpsXP;
end;

procedure TABSys_Org_Mail_1Form.dxBarComboFontColorChange(Sender: TObject);
begin
  Editor.SelAttributes.Color := dxBarComboFontColor.Color;
end;

procedure TABSys_Org_Mail_1Form.dxBarComboFontNameChange(Sender: TObject);
begin
  if not FUpdating then
    Editor.SelAttributes.Name := dxBarComboFontName.Text;
end;

procedure TABSys_Org_Mail_1Form.dxBarComboFontSizeChange(Sender: TObject);
begin
  if not FUpdating then
    Editor.SelAttributes.Size := StrToInt(dxBarComboFontSize.Text);
end;

procedure TABSys_Org_Mail_1Form.dxBarButtonSaveClick(Sender: TObject);
begin
  if CheckInput then
  begin
    if (Owner is TABSys_Org_MailForm) then
    begin
      TABSys_Org_MailForm(Owner).save;
    end;
  end;
end;

procedure TABSys_Org_Mail_1Form.EditorChange(Sender: TObject);
begin
  if Editor = nil then Exit;

  Editor.OnSelectionChange(Editor);
  SetModified(Editor.Modified);
  dxStatusBar.Panels[1].Text := ActiveMDIChild.Caption;
  TdxStatusBarTextPanelStyle(dxStatusBar.Panels[1].PanelStyle).ImageIndex := 0;
  TdxStatusBarTextPanelStyle(dxStatusBar.Panels[0].PanelStyle).ImageIndex := 2;
  dxBarButtonUndo.Enabled := SendMessage(Editor.Handle, EM_CANUNDO, 0, 0) <> 0;

end;

procedure TABSys_Org_Mail_1Form.SetModified(Value: Boolean);
begin
  Editor.Modified := Value;
  if Value then
  begin
    dxStatusBar.Panels[2].Text := 'Modified';
    TdxStatusBarTextPanelStyle(dxStatusBar.Panels[2].PanelStyle).ImageIndex := 1;
  end
  else
  begin
    dxStatusBar.Panels[2].Text := '';
    TdxStatusBarTextPanelStyle(dxStatusBar.Panels[2].PanelStyle).ImageIndex := 4;
  end;
  dxBarButtonSave.Enabled := Value;
end;

procedure TABSys_Org_Mail_1Form.EditorMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    dxBarPopupMenu.PopupFromCursorPos;
end;

procedure TABSys_Org_Mail_1Form.EditorSelectionChange(Sender: TObject);
begin
  FUpdating := True;
  dxBarComboFontSize.OnChange := nil;
  dxBarComboFontName.OnChange := nil;
  dxBarComboFontColor.OnChange := nil;
  try
     dxStatusBar.Panels[0].Text := Format('Line: %3d   Col: %3d', [1 + EditorRow, 1 + EditorCol]);

     dxBarButtonCopy.Enabled := Editor.SelLength > 0;
     dxBarButtonCut.Enabled := dxBarButtonCopy.Enabled;
     dxBarButtonPaste.Enabled := SendMessage(Editor.Handle, EM_CANPASTE, 0, 0) <> 0;
     dxBarButtonClear.Enabled := dxBarButtonCopy.Enabled;

     dxBarComboFontSize.Text := IntToStr(Editor.SelAttributes.Size);
     dxBarComboFontName.Text := Name;
     dxBarComboFontColor.Color := Color;

     dxBarButtonBold.Down := fsBold in Editor.SelAttributes.Style;
     dxBarButtonItalic.Down := fsItalic in Editor.SelAttributes.Style;
     dxBarButtonUnderline.Down := fsUnderline in Editor.SelAttributes.Style;

     dxBarButtonBullets.Down := Boolean(Editor.Paragraph.Numbering);
     case Ord(Editor.Paragraph.Alignment) of
       0: dxBarButtonAlignLeft.Down := True;
       1: dxBarButtonAlignRight.Down := True;
       2: dxBarButtonCenter.Down := True;
     end;
     dxBarButtonProtected.Down := Editor.SelAttributes.Protected;
  finally
    FUpdating := False;
    dxBarComboFontSize.OnChange := dxBarComboFontSizeChange;
    dxBarComboFontName.OnChange := dxBarComboFontNameChange;
    dxBarComboFontColor.OnChange := dxBarComboFontColorChange;
  end;
end;
                               
procedure TABSys_Org_Mail_1Form.FormCreate(Sender: TObject);
begin
  ABFillTreeItemByTr_Code('PRI',['Ti_Name'],dxBarCombo1.Items);
  Editor.Modified;
  Editor.OnChange(Editor);
end;

function TABSys_Org_Mail_1Form.GetEditorCol: Integer;
begin
  Result := Editor.SelStart - SendMessage(Handle, EM_LINEINDEX, EditorRow, 0);
end;

function TABSys_Org_Mail_1Form.GetEditorRow: Integer;
begin
  Result := SendMessage(Handle, EM_LINEFROMCHAR, Editor.SelStart, 0);
end;

procedure TABSys_Org_Mail_1Form.FindOne(Sender: TObject);
var
  StartPos, FindLength, FoundAt: Integer;
  Flags: TSearchTypes;
  P: TPoint;
  CaretR, R, IntersectR: TRect;
begin
  if frDown in TFindDialog(Sender).Options then
  begin
    if Editor.SelLength = 0 then StartPos := Editor.SelStart
    else StartPos := Editor.SelStart + Editor.SelLength;
    FindLength := Length(Text) - StartPos;
  end
  else
  begin
    StartPos := Editor.SelStart;
    FindLength := -StartPos;
  end;
  Flags := [];
  if frMatchCase in TFindDialog(Sender).Options then Include(Flags, stMatchCase);
  if frWholeWord in TFindDialog(Sender).Options then Include(Flags, stWholeWord);
  Screen.Cursor := crHourglass;
  FoundAt := Editor.FindText(TFindDialog(Sender).FindText, StartPos, FindLength, Flags);
  if not (frReplaceAll in TFindDialog(Sender).Options) then Screen.Cursor := crDefault;
  if FoundAt > -1 then
    if frReplaceAll in TFindDialog(Sender).Options then
    begin
      Editor.SelStart := FoundAt;
      Editor.SelLength := Length(TFindDialog(Sender).FindText);
    end
    else
    begin
      SetFocus;
      Editor.SelStart := FoundAt;
      Editor.SelLength := Length(TFindDialog(Sender).FindText);

      GetCaretPos(P);
      P := ClientToScreen(P);
      CaretR := Rect(P.X, P.Y, P.X + 2, P.Y + 20);
      GetWindowRect(Handle, R);
      if IntersectRect(IntersectR, CaretR, R) then
        if P.Y < Screen.Height div 2 then
          Top := P.Y + 40
        else
          Top := P.Y - (R.Bottom - R.Top + 20);
    end
  else
    if not (frReplaceAll in TFindDialog(Sender).Options) then
      Application.MessageBox(sRichEditTextNotFound,
        sRichEditFoundResultCaption, MB_ICONINFORMATION);
end;

procedure TABSys_Org_Mail_1Form.ReplaceOne(Sender: TObject);
var
  ReplacedCount, OldSelStart, PrevSelStart: Integer;
begin
  ReplacedCount := 0;
  OldSelStart := Editor.SelStart;
  if frReplaceAll in TReplaceDialog(Sender).Options then
    Screen.Cursor := crHourglass;
  repeat
    if (Editor.SelLength > 0) and ((Editor.SelText = TFindDialog(Sender).FindText) or
      (not (frMatchCase in TReplaceDialog(Sender).Options) and
       (AnsiUpperCase(Editor.SelText) = AnsiUpperCase(TFindDialog(Sender).FindText)))) then
    begin
      Editor.SelText := TReplaceDialog(Sender).ReplaceText;
      Inc(ReplacedCount);
    end;
    PrevSelStart := Editor.SelStart;
    FindOne(Sender);
  until not (frReplaceAll in TReplaceDialog(Sender).Options) or (Editor.SelStart = PrevSelStart);
  if frReplaceAll in TReplaceDialog(Sender).Options then
  begin
    Screen.Cursor := crDefault;
    if ReplacedCount = 0 then
    begin
      ABShow(sRichEditTextNotFound);
    end
    else
    begin
      Editor.SelStart := OldSelStart;
      ABShow(sRichEditReplaceAllResult, [ReplacedCount]);
    end;
  end;
end;

procedure TABSys_Org_Mail_1Form.SetButtonEdit1Value;
var
  tempMainDataset,tempDetailDataset:TDataSet;
  tempStrings:TStrings;
begin
  tempMainDataset:=ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['Enterprise Dir']);
  tempDetailDataset:=ABGetDataset('Main',
                              ' SELECT  Op_Ti_Guid,Op_Code,Op_Name '+
                              ' FROM ABSys_Org_Operator ',[],nil,true);
  tempStrings:=Tstringlist.create;
  try
    ABStrsToStrings(ABcxButtonEdit1.Text,',',tempStrings);
    if ABSelectCheckTreeView(
        tempMainDataset,
        'Ti_ParentGuid',
        'Ti_Guid',
        'Ti_Name',
        False,

        tempDetailDataset,
        'Op_Ti_Guid',
        'Op_Name',

        tempStrings,
        'Op_Code'
        ) then
    begin
      ABcxButtonEdit1.Text:= ABStringsToStrs(tempStrings);
    end;
  finally
    tempDetailDataset.Free;
    tempStrings.free;
  end;
end;

end.



