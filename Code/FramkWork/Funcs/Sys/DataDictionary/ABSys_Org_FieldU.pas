unit ABSys_Org_FieldU;

interface                         
                              
uses
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,
  ABPubShowEditDatasetU,
  ABPubDesignU,
  ABPubMessageU,

  ABThirdCacheDatasetU,
  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFramkWorkcxGridU,
  ABFramkWorkDBPanelU,
  ABFramkWorkDictionaryQueryU,
  ABFramkWorkFuncFormU,
  ABFramkWorkControlU,
  ABFramkWorkQueryU,
  ABFramkWorkFuncU,
  ABFramkWorkOperatorDatasetU,

  cxTLdxBarBuiltInMenu,
  cxLookAndFeels,
  cxCheckBox,
  cxDBLookupComboBox,
  cxTLData,
  cxDBTL,
  cxInplaceContainer,
  cxMaskEdit,
  cxTL,
  cxGridDBTableView,
  cxButtons,
  cxLookAndFeelPainters,
  cxGrid,
  cxGridCustomView,
  cxControls,
  cxClasses,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridLevel,
  cxSplitter,
  cxPC,
  cxGroupBox,
  cxContainer,
  cxDBData,
  cxEdit,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  dxdbtree,
  dxtree,


  Variants,
  Forms,ShellAPI,Windows,DB,DBClient,StdCtrls,ComCtrls,Classes,Controls,ExtCtrls,
  SysUtils,Menus,Grids,DBGrids, cxNavigator,
  dxBarBuiltInMenu, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  cxTextEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBExtLookupComboBox, Vcl.Buttons, cxLabel;

type                                              
  TABSys_Org_FieldForm = class(TABFuncForm)
    Panel1: TPanel;
    Splitter1: TABcxSplitter;
    Splitter2: TABcxSplitter;
    PopupMenu1: TPopupMenu;
    N2: TMenuItem;
    N3: TMenuItem;
    ABcxDBTreeView1: TABcxDBTreeView;
    ABcxDBTreeView1cxDBTreeListColumn1: TcxDBTreeListColumn;
    N4: TMenuItem;
    N5: TMenuItem;
    N7: TMenuItem;
    ABcxDBTreeView1cxDBTreeListColumn2: TcxDBTreeListColumn;
    Panel5: TPanel;
    ABcxPageControl1: TABcxPageControl;
    tempTable: TcxTabSheet;
    ABDBPanel2: TABDBPanel;
    tempField: TcxTabSheet;
    ABcxPageControl2: TABcxPageControl;
    cxTabSheet2: TcxTabSheet;
    ABDBPanel5: TABDBPanel;
    cxTabSheet1: TcxTabSheet;
    ABDBPanel4: TABDBPanel;
    cxTabSheet3: TcxTabSheet;
    ABDBPanel6: TABDBPanel;
    cxTabSheet4: TcxTabSheet;
    ABDBPanel7: TABDBPanel;
    ABcxGroupBox1: TABcxGroupBox;
    ABDBPanel3: TABDBPanel;
    ABQuery_Field: TABQuery;
    ABQuery_Table: TABQuery;
    ABDatasource_Table: TABDatasource;
    ABDatasource_Field: TABDatasource;
    ABQuery_GetRepeatedFieldCaption: TABQuery;
    ABQuery_TreeItem: TABQuery;
    ABDatasource_TreeItem: TABDatasource;
    ABcxSplitter1: TABcxSplitter;
    Panel2: TPanel;
    Button3: TABcxButton;
    ABcxButton1: TABcxButton;
    ABcxButton2: TABcxButton;
    Button1: TABcxButton;
    ABcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel2: TcxGridLevel;
    ABcxGrid2: TABcxGrid;
    ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    cxTabSheet5: TcxTabSheet;
    N1: TMenuItem;
    N6: TMenuItem;
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ABcxButton1Click(Sender: TObject);
    procedure ABcxButton2Click(Sender: TObject);
    procedure ABcxGrid1Enter(Sender: TObject);
    procedure ABcxGrid2Enter(Sender: TObject);
    procedure ABcxDBTreeView1Enter(Sender: TObject);
    procedure ABQuery_TreeItemAfterScroll(DataSet: TDataSet);
    procedure N1Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
  private
    procedure ReFreshFromDatabase;
    procedure SetActivePageIndex(Sender: TObject);
    procedure ShowRepeatedFieldCaption;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Org_FieldForm: TABSys_Org_FieldForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Org_FieldForm.ClassName;
end;

exports
   ABRegister ;


procedure TABSys_Org_FieldForm.ReFreshFromDatabase;
begin
  ABReFreshQuery(ABQuery_Table,[]);
  ABReFreshQuery(ABQuery_Field,[]);
end;

procedure TABSys_Org_FieldForm.ABcxButton1Click(Sender: TObject);
begin
  ABGetDownManagerForm.ShowModal;
end;

procedure TABSys_Org_FieldForm.ABcxButton2Click(Sender: TObject);
begin
  ABRefreshPubDataset;
end;

procedure TABSys_Org_FieldForm.ABcxDBTreeView1Enter(Sender: TObject);
begin
  SetActivePageIndex(ABcxDBTreeView1);
end;

procedure TABSys_Org_FieldForm.ABcxGrid1Enter(Sender: TObject);
begin
  SetActivePageIndex(ABcxGrid1);
end;

procedure TABSys_Org_FieldForm.ABcxGrid2Enter(Sender: TObject);
begin
  SetActivePageIndex(ABcxGrid2);
end;

procedure TABSys_Org_FieldForm.ABQuery_TreeItemAfterScroll(DataSet: TDataSet);
begin
  ABcxGridDBBandedTableView1.DataController.Groups.FullExpand;
end;

procedure TABSys_Org_FieldForm.ShowRepeatedFieldCaption;
begin
  ABReFreshQuery(ABQuery_GetRepeatedFieldCaption,[]);
  try
    if not ABQuery_GetRepeatedFieldCaption.IsEmpty then
      ABShowEditDataset(ABQuery_GetRepeatedFieldCaption,false,False,false);
  finally
    ABQuery_GetRepeatedFieldCaption.Close;
  end;
end;

procedure TABSys_Org_FieldForm.Button1Click(Sender: TObject);
begin
  try
    ABExecSQL('Main','exec Proc_ParseDictionary '+QuotedStr(''));
    ReFreshFromDatabase;
  except
    ShowRepeatedFieldCaption;
    raise;
  end;
end;

procedure TABSys_Org_FieldForm.N1Click(Sender: TObject);
var
  tempStr1,tempStr2: string;
begin
  tempStr1:=ABGetDatasetValue(ABQuery_Table,['CL_DatabaseName'],[],[],',',',',false);
  tempStr2:=ABGetDatasetValue(ABQuery_Table,['Ta_Name'],[],[],',',',',false);
  ABExecSQL('Main','exec Proc_UpdateTableAndFieldCaption_FramkworkToDatabase '+
                        QuotedStr(tempStr1)+','+
                        QuotedStr(tempStr2),
                        []);
end;

procedure TABSys_Org_FieldForm.N2Click(Sender: TObject);
begin
  ABExecSQL('Main','exec Proc_UpdateTableAndFieldCaption_FramkworkToDatabase '+
                        QuotedStr(ABQuery_Table.FieldByName('CL_DatabaseName').AsString)+','+
                        QuotedStr(ABQuery_Table.FindField('Ta_Name').AsString),
                        []);
end;

procedure TABSys_Org_FieldForm.N4Click(Sender: TObject);
begin
  ABExecSQL('Main','exec Proc_UpdateTableAndFieldCaption_FramkworkToDatabase '+
                        QuotedStr(ABQuery_Table.FieldByName('CL_DatabaseName').AsString)+','+
                        QuotedStr('') );
end;

procedure TABSys_Org_FieldForm.N3Click(Sender: TObject);
begin
  try
    ABExecSQL('Main','exec Proc_UpdateTableAndFieldCaption_DatabaseToFramkwork '+
              QuotedStr(ABQuery_Table.FieldByName('CL_DatabaseName').AsString)+','+
              QuotedStr(ABQuery_Table.FindField('Ta_Name').AsString) );
    ReFreshFromDatabase;
  except
    ShowRepeatedFieldCaption;
    raise;
  end;
end;

procedure TABSys_Org_FieldForm.N5Click(Sender: TObject);
begin
  try
    ABExecSQL('Main','exec Proc_UpdateTableAndFieldCaption_DatabaseToFramkwork '+
              QuotedStr(ABQuery_Table.FieldByName('CL_DatabaseName').AsString)+','+
              QuotedStr('') );
    ReFreshFromDatabase;
  except
    ShowRepeatedFieldCaption;
    raise;
  end;
end;

procedure TABSys_Org_FieldForm.N6Click(Sender: TObject);
var
  tempStr1,tempStr2: string;
begin
  tempStr1:=ABGetDatasetValue(ABQuery_Table,['CL_DatabaseName'],[],[],',',',',false);
  tempStr2:=ABGetDatasetValue(ABQuery_Table,['Ta_Name'],[],[],',',',',false);
  try
    ABExecSQL('Main','exec Proc_UpdateTableAndFieldCaption_DatabaseToFramkwork '+
                        QuotedStr(tempStr1)+','+
                        QuotedStr(tempStr2)
                         );
    ReFreshFromDatabase;
  except
    ShowRepeatedFieldCaption;
    raise;
  end;
end;

procedure TABSys_Org_FieldForm.Button3Click(Sender: TObject);
begin
  close;
end;

procedure TABSys_Org_FieldForm.SetActivePageIndex(Sender: TObject);
var
  tempActivePageIndex:LongInt;
begin
  tempActivePageIndex:=0;
  if Sender=ABcxGrid1 then
  begin
    tempActivePageIndex:=1;
  end
  else if Sender=ABcxGrid2 then
  begin
    tempActivePageIndex:=2;
  end
  else if Sender=ABcxDBTreeView1 then
  begin
    tempActivePageIndex:=0;
  end;

  if tempActivePageIndex>=0 then
    ABcxPageControl1.ActivePageIndex:=tempActivePageIndex;
end;

procedure TABSys_Org_FieldForm.FormCreate(Sender: TObject);
begin
  ABInitFormDataSet([],[],ABQuery_TreeItem);
  ABInitFormDataSet([ABDBPanel2],[ABcxGridDBBandedTableView1],ABQuery_Table);
  ABQuery_Table.CanInsert:=false;
  ABInitFormDataSet([ABDBPanel3,ABDBPanel4,ABDBPanel5,ABDBPanel6,ABDBPanel7],[ABcxGridDBBandedTableView2],ABQuery_Field);

  ABcxPageControl1.ActivePageIndex:=0;
  ABcxPageControl2.ActivePageIndex:=0;
end;


Initialization
  RegisterClass(TABSys_Org_FieldForm);
Finalization
  UnRegisterClass(TABSys_Org_FieldForm);




end.






