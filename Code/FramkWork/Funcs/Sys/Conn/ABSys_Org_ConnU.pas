unit ABSys_Org_ConnU;

interface

uses
  ABPubMessageU,
  ABPubConstU,
  ABPubVarU,
  ABPubLocalParamsU,
  ABPubAutoProgressBar_ThreadU,
  ABPubFuncU,

  ABThirdConnServerU,
  ABThirdConnDatabaseU,

  ABFramkWorkFuncFormU,

  ShellAPI,Windows,Messages,SysUtils,Variants,Classes,DB,DBClient, ABThirdFormU;

type
  TABSys_Org_ConnForm = class(TABThirdForm)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TABConnDatabaseForm_ = class(TABConnDatabaseForm)
  private
    { Private declarations }
  public
    constructor Create(AOwner: TComponent);override;
    { Public declarations }
  end;

  TABConnServerForm_ = class(TABConnServerForm)
  private
    { Private declarations }
  public
    constructor Create(AOwner: TComponent);override;
    { Public declarations }
  end;

var
  ABSys_Org_ConnForm: TABSys_Org_ConnForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  if ABLocalParams.LoginType=ltCS_Two then
  begin
    aFormClassName  :=TABConnDatabaseForm_.ClassName;
  end
  else if ABLocalParams.LoginType=ltCS_Three then
  begin
    aFormClassName  :=TABConnServerForm_.ClassName;
  end;
end;

exports
   ABRegister;

{ TABConnDatabaseForm_ }

constructor TABConnDatabaseForm_.Create(AOwner: TComponent);
begin
  inherited;
  OkClose:=True;
end;

{ TABConnServerForm_ }

constructor TABConnServerForm_.Create(AOwner: TComponent);
begin
  inherited;
  OkClose:=True;
end;

Initialization
  RegisterClass(TABConnDatabaseForm_);
  RegisterClass(TABConnServerForm_);

Finalization
  UnRegisterClass(TABConnDatabaseForm_);
  UnRegisterClass(TABConnServerForm_);

end.
