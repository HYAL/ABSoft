unit ABSys_Org_HistoryDataU;

interface

uses
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,
  ABPubMessageU,
  ABPubLogU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFramkWorkDBPanelU,
  ABFramkWorkcxGridU,
  ABFramkWorkDictionaryQueryU,
  ABFramkWorkDBNavigatorU,
  ABFramkWorkFuncFormU,
  ABFramkWorkControlU,
  ABFramkWorkQueryU,
  ABFramkWorkFuncU,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, DBClient, cxPC, cxControls, cxNavigator, cxDBNavigator, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, ExtCtrls, ComCtrls,
  StdCtrls, DBCtrls, cxGridBandedTableView,
  cxGridDBBandedTableView, cxLookAndFeels, dxStatusBar, cxDBLookupComboBox,
  cxCalc, cxButtonEdit, cxLabel, cxTextEdit, Menus, cxLookAndFeelPainters,
  cxButtons, dxBarBuiltInMenu, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, cxGrid;

type
  TABSys_Org_HistoryDataForm = class(TABFuncForm)
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABcxPageControl1: TABcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    ABDBNavigator1: TABDBNavigator;
    Panel1: TPanel;
    ABcxButton1: TABcxButton;
    ProgressBar1: TProgressBar;
    ABDBcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1TableView1: TcxGridTableView;
    cxGrid1TableView1Column0: TcxGridColumn;
    cxGrid1TableView1Column1: TcxGridColumn;
    cxGrid1TableView1Column2: TcxGridColumn;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1TableView1Column3: TcxGridColumn;
    procedure FormCreate(Sender: TObject);
    procedure ABcxPageControl1Change(Sender: TObject);
    procedure ABcxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Org_HistoryDataForm: TABSys_Org_HistoryDataForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Org_HistoryDataForm.ClassName;
end;

exports
   ABRegister ;

procedure TABSys_Org_HistoryDataForm.ABcxButton1Click(Sender: TObject);
var
  i:LongInt;
  tempConnName:string;
  tempSQL:string;
begin
  cxGrid1TableView1.DataController.Post;
  if cxGrid1TableView1.Controller.SelectedRowCount<=0 then
  begin
    cxGrid1TableView1.Controller.SelectAll;
  end;

  if cxGrid1TableView1.Controller.SelectedRowCount>0 then
  begin
    ProgressBar1.Visible:=true;
    ProgressBar1.Min:=0;
    ProgressBar1.Max:=cxGrid1TableView1.Controller.SelectedRowCount;
    ProgressBar1.Position:=0;
    try
      for I := 0 to cxGrid1TableView1.Controller.SelectedRowCount- 1 do
      begin
        ProgressBar1.Position:=ProgressBar1.Position+1;
        tempConnName:=cxGrid1TableView1.Controller.SelectedRecords[i].Values[cxGrid1TableView1Column3.Index];
        tempSQL:=cxGrid1TableView1.Controller.SelectedRecords[i].Values[cxGrid1TableView1Column2.Index];
        ABWriteLog('TABSys_Org_HistoryDataForm ['+tempConnName+']['+tempSQL+']');
        ABExecSQL(tempConnName,tempSQL);
      end;
      ABShow('删除成功');
    finally
      ProgressBar1.Position:=0;
      ProgressBar1.Visible:=False;
    end;
  end
  else
  begin
    ABShow('请选择要删除历史数据的记录.');
  end;
end;

procedure TABSys_Org_HistoryDataForm.ABcxPageControl1Change(Sender: TObject);
var
  i:LongInt;
begin
  if not ABQuery1.IsEmpty then
  begin
    i:=0;
    cxGrid1TableView1.DataController.RecordCount:=0;
    ABQuery1.First;
    while not ABQuery1.Eof do
    begin
      cxGrid1TableView1.DataController.Append;

      i:=i+1;
      cxGrid1TableView1Column0.EditValue:=inttostr(i);
      cxGrid1TableView1Column1.EditValue:=
         '删除'+
         '['+ABQuery1.FieldDefByFieldName('HD_Ta_Guid').PDownDef.Fi_DataSet.Lookup('Ta_Guid',ABQuery1.FieldByName('HD_Ta_Guid').AsString,'Ta_Caption')+']中'+
         '['+ABQuery1.FieldDefByFieldName('HD_Fi_Guid').PDownDef.Fi_DataSet.Lookup('Fi_Guid',ABQuery1.FieldByName('HD_Fi_Guid').AsString,'FI_Caption')+']超过['+
         inttostr(ABQuery1.FieldByName('HD_HoldDayCount').AsInteger)+']天的数据';

      cxGrid1TableView1Column2.EditValue:=
               ' delete '+ABQuery1.FieldDefByFieldName('HD_Ta_Guid').PDownDef.Fi_DataSet.Lookup('Ta_Guid',ABQuery1.FieldByName('HD_Ta_Guid').AsString,'Ta_Name')+
               ' where  '+ABQuery1.FieldDefByFieldName('HD_Fi_Guid').PDownDef.Fi_DataSet.Lookup('Fi_Guid',ABQuery1.FieldByName('HD_Fi_Guid').AsString,'FI_Name')+
               '       < getdate()-'+inttostr(ABQuery1.FieldByName('HD_HoldDayCount').AsInteger);

      cxGrid1TableView1Column3.EditValue:=ABGetConnNameByConnGuid(ABQuery1.FieldDefByFieldName('HD_Ta_Guid').PDownDef.Fi_DataSet.Lookup('Ta_Guid',ABQuery1.FieldByName('HD_Ta_Guid').AsString,'TA_CL_Guid'));
      ABQuery1.Next;
    end;
  end;
end;

procedure TABSys_Org_HistoryDataForm.FormCreate(Sender: TObject);
begin
  ABInitFormDataSet([],[ABcxGridDBBandedTableView1],ABQuery1);
  ABcxPageControl1.HideTabs:=not ABPubUser.IsAdmin;
  if ABcxPageControl1.ActivePageIndex<>ABIIF(ABPubUser.IsAdmin,0,1) then
    ABcxPageControl1.ActivePageIndex:=ABIIF(ABPubUser.IsAdmin,0,1)
  else
    ABcxPageControl1Change(ABcxPageControl1);
end;

Initialization
  RegisterClass(TABSys_Org_HistoryDataForm);

Finalization
  UnRegisterClass(TABSys_Org_HistoryDataForm);

end.
