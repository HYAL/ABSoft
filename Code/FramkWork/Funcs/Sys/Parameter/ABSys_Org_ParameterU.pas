unit ABSys_Org_ParameterU;

interface

uses
  ABPubAutoProgressBar_ThreadU,
  ABPubInPutPassWordU,
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFramkWorkcxGridU,
  ABFramkWorkDictionaryQueryU,
  ABFramkWorkDBNavigatorU,
  ABFramkWorkFuncFormU,
  ABFramkWorkControlU,
  ABFramkWorkQueryU,
  ABFramkWorkFuncU,

  cxCheckBox,
  cxLookAndFeelPainters,
  cxCalc,
  cxDropDownEdit,
  cxMaskEdit,
  cxTextEdit,
  cxContainer,
  cxEditRepositoryItems,
  cxExtEditRepositoryItems,
  cxLookAndFeels,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGrid,
  cxGridDBTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxClasses,
  cxGridLevel,
  cxDBData,
  cxEdit,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  cxDBNavigator,
  cxNavigator,
  cxControls,
  cxPC,
  dxStatusBar,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,DB,
  DBClient,ExtCtrls,ComCtrls,StdCtrls,DBCtrls,
  ABPubItemListFrameU, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TABSys_Org_ParameterForm = class(TABFuncForm)
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABDBStatusBar1: TABdxDBStatusBar;
    ABDBNavigator1: TABDBNavigator;
    EditRepository: TcxEditRepository;
    EditRepositoryDateItem1: TcxEditRepositoryDateItem;
    EditRepositoryCalcItem_Int: TcxEditRepositoryCalcItem;
    EditRepositoryCheckBoxItem1: TcxEditRepositoryCheckBoxItem;
    EditRepositoryComboBoxItem_NumberListMond: TcxEditRepositoryComboBoxItem;
    EditRepositoryColorComboBox1: TcxEditRepositoryColorComboBox;
    EditRepositoryComboBoxItem_InputMondLabelYAlignment: TcxEditRepositoryComboBoxItem;
    EditRepositoryComboBoxItem_InputMondLabelXAlignment: TcxEditRepositoryComboBoxItem;
    EditRepositoryCalcItem_Float: TcxEditRepositoryCalcItem;
    ABDBcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    EditRepositoryComboBoxItem_VersionNo: TcxEditRepositoryComboBoxItem;
    EditRepositoryCheckComboBox_CustomCustName: TcxEditRepositoryCheckComboBox;
    EditRepositoryCheckComboBox_ClientType: TcxEditRepositoryCheckComboBox;
    procedure FormCreate(Sender: TObject);
    procedure EditRepositoryCheckComboBox_ClientTypePropertiesEditValueToStates(
      Sender: TObject; const AValue: Variant; var ACheckStates: TcxCheckStates);
    procedure EditRepositoryCheckComboBox_ClientTypePropertiesStatesToEditValue(
      Sender: TObject; const ACheckStates: TcxCheckStates; out AValue: Variant);
    procedure EditRepositoryCheckComboBox_CustomCustNamePropertiesEditValueToStates(
      Sender: TObject; const AValue: Variant; var ACheckStates: TcxCheckStates);
    procedure EditRepositoryCheckComboBox_CustomCustNamePropertiesStatesToEditValue(
      Sender: TObject; const ACheckStates: TcxCheckStates; out AValue: Variant);
  private
    procedure PamesValueGetProperties(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Org_ParameterForm: TABSys_Org_ParameterForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Org_ParameterForm.ClassName;
end;

procedure ABRunBeforeCreateForm(var aCreateForm:Boolean);stdcall;
begin
  aCreateForm:=(ABPubUser.IsAdmin) or (ABInputPassWord);
end;

exports
   ABRunBeforeCreateForm;
exports
   ABRegister ;

procedure TABSys_Org_ParameterForm.EditRepositoryCheckComboBox_ClientTypePropertiesEditValueToStates(
  Sender: TObject; const AValue: Variant; var ACheckStates: TcxCheckStates);
begin
  ABCheckComboBoxEditValueToStates(EditRepositoryCheckComboBox_ClientType.Properties,AValue,ACheckStates);
end;

procedure TABSys_Org_ParameterForm.EditRepositoryCheckComboBox_ClientTypePropertiesStatesToEditValue(
  Sender: TObject; const ACheckStates: TcxCheckStates; out AValue: Variant);
begin
  ABCheckComboBoxStatesToEditValue(EditRepositoryCheckComboBox_ClientType.Properties,ACheckStates,AValue);
end;

procedure TABSys_Org_ParameterForm.EditRepositoryCheckComboBox_CustomCustNamePropertiesEditValueToStates(
  Sender: TObject; const AValue: Variant; var ACheckStates: TcxCheckStates);
begin
  ABCheckComboBoxEditValueToStates(EditRepositoryCheckComboBox_CustomCustName.Properties,AValue,ACheckStates);
end;

procedure TABSys_Org_ParameterForm.EditRepositoryCheckComboBox_CustomCustNamePropertiesStatesToEditValue(
  Sender: TObject; const ACheckStates: TcxCheckStates; out AValue: Variant);
begin
  ABCheckComboBoxStatesToEditValue(EditRepositoryCheckComboBox_CustomCustName.Properties,ACheckStates,AValue);
end;

procedure TABSys_Org_ParameterForm.FormCreate(Sender: TObject);
begin
  ABInitFormDataSet([],[ABcxGridDBBandedTableView1],ABQuery1);
  if not ABPubUser.IsAdmin then
  begin
    ABSetColumnProperty(ABcxGridDBBandedTableView1,'PA_SysUse','Visible',False);
    ABSetColumnProperty(ABcxGridDBBandedTableView1,'PA_SysUse','VisibleForCustomization',False);
  end;

  ABFillDatasetToControl(nil,
                         EditRepositoryCheckComboBox_ClientType.Properties,
                         0,
                         ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['ClientType']),
                         'Ti_Name');
  ABFillDatasetToControl(nil,
                         EditRepositoryCheckComboBox_CustomCustName.Properties,
                         0,
                         ABGetConstSqlPubDataset('ABSys_Org_TreeItem',['CustomCustName']),
                         'Ti_Name');

  ABcxGridDBBandedTableView1.GetColumnByFieldName('Pa_Value').OnGetProperties:=PamesValueGetProperties;
end;

procedure TABSys_Org_ParameterForm.PamesValueGetProperties(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
var
  tempParamName:string;
begin
  tempParamName:=VarToStrDef(ABcxGridDBBandedTableView1.DataController.Values[ARecord.RecordIndex,ABcxGridDBBandedTableView1.GetColumnByFieldName('Pa_Name').Index],'');
  //布尔型
  if (AnsiCompareText(tempParamName,trim('CheckSoftDog'))=0)  then
  begin
    AProperties:=EditRepositoryCheckBoxItem1.Properties;
  end
  //下拉枚举型
  //ABDBPanel中输入标签的X靠向
  //ABDBPanel中输入标签的Y靠向
  else if (AnsiCompareText(tempParamName,trim('InputMondLabelXAlignment'))=0) then
  begin
    AProperties:=EditRepositoryComboBoxItem_InputMondLabelXAlignment.Properties;
  end
  else if (AnsiCompareText(tempParamName,trim('InputMondLabelYAlignment'))=0) then
  begin
    AProperties:=EditRepositoryComboBoxItem_InputMondLabelYAlignment.Properties;
  end
  else if (AnsiCompareText(tempParamName,trim('VersionNo'))=0) then
  begin
    AProperties:=EditRepositoryComboBoxItem_VersionNo.Properties;
  end
  //下拉数字列表型
  else if (AnsiCompareText(tempParamName,trim('ColorInLableOrControl'))=0)   then
  begin
    AProperties:=EditRepositoryComboBoxItem_NumberListMond.Properties;
  end
  //整型数字型
  else if  (AnsiCompareText(tempParamName,trim('InputMondLeftspacing'))=0) or
           (AnsiCompareText(tempParamName,trim('InputMondTopspacing'))=0) or
           (AnsiCompareText(tempParamName,trim('InputMondRightspacing'))=0) or
           (AnsiCompareText(tempParamName,trim('InputMondBottomspacing'))=0) or
           (AnsiCompareText(tempParamName,trim('InputMondXspacing'))=0) or
           (AnsiCompareText(tempParamName,trim('InputMondYspacing'))=0) or
           (AnsiCompareText(tempParamName,trim('Heartbeat'))=0) or
           (AnsiCompareText(tempParamName,trim('FuncUpdateCount'))=0)   then
  begin
    AProperties:=EditRepositoryCalcItem_Int.Properties;
  end
  //整型浮点型
  else if (AnsiCompareText(tempParamName,trim('ReadOnlyColor_Label'))=0) or
          (AnsiCompareText(tempParamName,trim('NotNullColor_Label'))=0) or
          (AnsiCompareText(tempParamName,trim('ReadOnlyColor_Control'))=0) or
          (AnsiCompareText(tempParamName,trim('NotNullColor_Control'))=0) then
  begin
    AProperties:=EditRepositoryColorComboBox1.Properties;
  end
  //日期型
  //开始使用日期
  else if (AnsiCompareText(tempParamName,trim('BegUseDate'))=0) then
  begin
    AProperties:=EditRepositoryDateItem1.Properties;
  end
  else if (AnsiCompareText(tempParamName,trim('ClientType'))=0) then
  begin
    AProperties:=EditRepositoryCheckComboBox_ClientType.Properties;
  end
  else if (AnsiCompareText(tempParamName,trim('CustomCustName'))=0) then
  begin
    AProperties:=EditRepositoryCheckComboBox_CustomCustName.Properties;
  end
  //其它无需处理的类型
  else
  begin

  end;
end;


Initialization
  RegisterClass(TABSys_Org_ParameterForm);

Finalization
  UnRegisterClass(TABSys_Org_ParameterForm);

end.

