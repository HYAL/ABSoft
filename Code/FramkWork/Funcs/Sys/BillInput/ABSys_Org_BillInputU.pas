unit ABSys_Org_BillInputU;

interface

uses
  ABPubFuncU,
  ABPubPanelU,
  ABPubDBU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,

  ABFramkWorkcxGridU,
  ABFramkWorkQuerySelectFieldPanelU,
  ABFramkWorkDictionaryQueryU,
  ABFramkWorkDBNavigatorU,
  ABFramkWorkFuncFormU,
  ABFramkWorkControlU,
  ABFramkWorkQueryU,
  ABFramkWorkFuncU,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,DB,
  DBClient,ExtCtrls,ComCtrls,StdCtrls,DBCtrls,FMTBcd,Provider,SqlExpr,Grids,
  DBGrids,Menus, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxCustomData, cxStyles, cxTL, cxMaskEdit,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxGrid,
  cxInplaceContainer, cxDBTL, cxTLData, dxStatusBar, cxSplitter,
  cxTLdxBarBuiltInMenu, ABFramkWorkDBPanelU, dxBarBuiltInMenu, cxPC;

type
  TABSys_Org_BillInputForm = class(TABFuncForm)
    ABDBStatusBar1: TABdxDBStatusBar;
    ABDBNavigator1: TABDBNavigator;
    Panel2: TPanel;
    ABDBcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABDBPanel1: TABDBPanel;
    Splitter1: TABcxSplitter;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Org_BillInputForm: TABSys_Org_BillInputForm;

implementation

{$R *.dfm}

procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Org_BillInputForm.ClassName;
end;

exports
   ABRegister ;

procedure TABSys_Org_BillInputForm.FormCreate(Sender: TObject);
begin
  ABInitFormDataSet([ABDBPanel1],[ABcxGridDBBandedTableView1],ABQuery1);
end;

Initialization
  RegisterClass(TABSys_Org_BillInputForm);

Finalization
  UnRegisterClass(TABSys_Org_BillInputForm);

end.

