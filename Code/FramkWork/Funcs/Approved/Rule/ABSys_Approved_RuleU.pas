unit ABSys_Approved_RuleU;
                   
interface
                              
uses
  ABPubVarU,
  ABPubUserU,
  ABPubFuncU,
  ABPubConstU,
  ABPubDBU,
  ABPubPanelU,
  ABPubDBLabelsU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,
  ABThirdFuncU,

  ABFramkWorkDictionaryQueryU,
  ABFramkWorkcxGridU,
  ABFramkWorkFuncU,
  ABFramkWorkDBNavigatorU,
  ABFramkWorkControlU,
  ABFramkWorkFuncFormU,
  ABFramkWorkQueryU,

  cxLookAndFeelPainters,
  cxLookAndFeels,
  cxSplitter,
  cxPC,
  cxGrid,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxControls,
  cxClasses,
  cxGridLevel,
  cxDBData,
  cxEdit,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  dxStatusBar,

  Dialogs,SqlExpr,StdCtrls,ComCtrls,Controls,Classes,ExtCtrls,DB,DBCtrls,DBClient,
  dxBarBuiltInMenu, cxNavigator, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TABSys_Approved_RuleForm = class(TABFuncForm)
    Splitter3: TABcxSplitter;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    ABDBNavigator1: TABDBNavigator;
    Panel2: TPanel;
    ABDBLabels1: TABDBLabels;
    ABDBcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    ABDBNavigator2: TABDBNavigator;
    ABDBNavigator3: TABDBNavigator;
    ABDBNavigator4: TABDBNavigator;
    ABDBNavigator5: TABDBNavigator;
    ABDBcxGrid2: TABcxGrid;
    ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView;
    cxGridLevel2: TcxGridLevel;
    ABDBcxGrid3: TABcxGrid;
    ABcxGridDBBandedTableView3: TABcxGridDBBandedTableView;
    cxGridLevel3: TcxGridLevel;
    ABDBcxGrid4: TABcxGrid;
    ABcxGridDBBandedTableView4: TABcxGridDBBandedTableView;
    cxGridLevel4: TcxGridLevel;
    ABDBcxGrid5: TABcxGrid;
    ABcxGridDBBandedTableView5: TABcxGridDBBandedTableView;
    cxGridLevel5: TcxGridLevel;
    ABDBStatusBar1: TABdxDBStatusBar;
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABQuery1_1: TABQuery;
    ABDatasource1_1: TABDatasource;
    ABQuery1_2: TABQuery;
    ABDatasource1_2: TABDatasource;
    ABQuery1_3: TABQuery;
    ABDatasource1_3: TABDatasource;
    ABDatasource1_3_1: TABDatasource;
    ABQuery1_3_1: TABQuery;
    ABcxPageControl1: TABcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    cxTabSheet4: TcxTabSheet;
    procedure FormCreate(Sender: TObject);
    procedure ABDBcxGrid4Enter(Sender: TObject);
    procedure ABDBcxGrid5Enter(Sender: TObject);
    procedure ABcxPageControl1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Approved_RuleForm: TABSys_Approved_RuleForm;

implementation

uses SysUtils, Forms;


{$R *.dfm}


procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Approved_RuleForm.ClassName;
end;

exports
   ABRegister ;


procedure TABSys_Approved_RuleForm.ABcxPageControl1Change(Sender: TObject);
begin
  case ABcxPageControl1.ActivePageIndex+1 of
    1:
    begin
      ABDBStatusBar1.DataSource:=ABDatasource1;
    end;
    2:
    begin
      ABDBStatusBar1.DataSource:=ABDatasource1_1;
    end;
    3:
    begin
      ABDBStatusBar1.DataSource:=ABDatasource1_2;
    end;
  end;
end;

procedure TABSys_Approved_RuleForm.ABDBcxGrid4Enter(Sender: TObject);
begin
  if ABcxPageControl1.ActivePageIndex+1=4 then
  begin
    ABDBStatusBar1.DataSource:=ABDatasource1_3;
  end;
end;

procedure TABSys_Approved_RuleForm.ABDBcxGrid5Enter(Sender: TObject);
begin
  if ABcxPageControl1.ActivePageIndex+1=4 then
  begin
    ABDBStatusBar1.DataSource:=ABDatasource1_3_1;
  end;
end;

procedure TABSys_Approved_RuleForm.FormCreate(Sender: TObject);
begin
  ABInitFormDataSet([],[ABcxGridDBBandedTableView1],ABQuery1);
  ABInitFormDataSet([],[ABcxGridDBBandedTableView2],ABQuery1_1);
  ABInitFormDataSet([],[ABcxGridDBBandedTableView3],ABQuery1_2);
  ABInitFormDataSet([],[ABcxGridDBBandedTableView4],ABQuery1_3);
  ABInitFormDataSet([],[ABcxGridDBBandedTableView5],ABQuery1_3_1);

  ABcxPageControl1.ActivePageIndex:=0;
  ABcxPageControl1Change(ABcxPageControl1);
end;

Initialization
  RegisterClass(TABSys_Approved_RuleForm);
Finalization
  UnRegisterClass(TABSys_Approved_RuleForm);


end.







