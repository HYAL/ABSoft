object ABSys_Approved_RuleForm: TABSys_Approved_RuleForm
  Left = 173
  Top = 129
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Anchors = [akLeft, akTop, akRight]
  Caption = #23457#25209#35268#21017#31649#29702
  ClientHeight = 550
  ClientWidth = 750
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  ExplicitWidth = 320
  PixelsPerInch = 96
  TextHeight = 13
  object ABDBNavigator1: TABDBNavigator
    Left = 0
    Top = 0
    Width = 750
    Height = 27
    Align = alTop
    BevelOuter = bvNone
    ShowCaption = False
    TabOrder = 0
    BigGlyph = False
    ImageLayout = blGlyphLeft
    DataSource = ABDatasource1
    VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel, nbQuerySpacer, nbQuery, nbReport, nbExitSpacer, nbExit]
    ButtonRangeType = RtMain
    BtnCustom1ImageIndex = -1
    BtnCustom2ImageIndex = -1
    BtnCustom3ImageIndex = -1
    BtnCustom4ImageIndex = -1
    BtnCustom5ImageIndex = -1
    BtnCustom6ImageIndex = -1
    BtnCustom7ImageIndex = -1
    BtnCustom8ImageIndex = -1
    BtnCustom9ImageIndex = -1
    BtnCustom10ImageIndex = -1
    BtnCustom1Caption = #33258#23450#20041'1'
    BtnCustom2Caption = #33258#23450#20041'2'
    BtnCustom3Caption = #33258#23450#20041'3'
    BtnCustom4Caption = #33258#23450#20041'4'
    BtnCustom5Caption = #33258#23450#20041'5'
    BtnCustom6Caption = #33258#23450#20041'6'
    BtnCustom7Caption = #33258#23450#20041'7'
    BtnCustom8Caption = #33258#23450#20041'8'
    BtnCustom9Caption = #33258#23450#20041'9'
    BtnCustom10Caption = #33258#23450#20041'10'
    BtnCustom1Kind = cxbkStandard
    BtnCustom2Kind = cxbkStandard
    BtnCustom3Kind = cxbkStandard
    BtnCustom4Kind = cxbkStandard
    BtnCustom5Kind = cxbkStandard
    BtnCustom6Kind = cxbkStandard
    BtnCustom7Kind = cxbkStandard
    BtnCustom8Kind = cxbkStandard
    BtnCustom9Kind = cxbkStandard
    BtnCustom10Kind = cxbkStandard
    ApprovedRollbackButton = nbNull
    ApprovedCommitButton = nbNull
    ButtonControlType = ctSingle
  end
  object Panel2: TPanel
    Left = 0
    Top = 27
    Width = 750
    Height = 35
    Align = alTop
    ParentBackground = False
    TabOrder = 1
    DesignSize = (
      750
      35)
    object ABDBLabels1: TABDBLabels
      Left = 7
      Top = 12
      Width = 736
      Height = 17
      Anchors = [akLeft, akTop, akRight]
      AutoSize = False
      BiDiMode = bdRightToLeft
      ParentBiDiMode = False
      Transparent = True
      DataSource = ABDatasource1
      DataFields = 'RU_Name'
    end
  end
  object ABDBStatusBar1: TABdxDBStatusBar
    Left = 0
    Top = 531
    Width = 750
    Height = 19
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = #20849#26377#35760#24405':2'#26465','#24403#21069#20301#32622':1/2'
        Width = 50
      end>
    PaintStyle = stpsUseLookAndFeel
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    StopUpdate = False
    DataSource = ABDatasource1
  end
  object ABcxPageControl1: TABcxPageControl
    Left = 0
    Top = 62
    Width = 750
    Height = 469
    Align = alClient
    TabOrder = 3
    Properties.ActivePage = cxTabSheet1
    Properties.CustomButtons.Buttons = <>
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = False
    OnChange = ABcxPageControl1Change
    ActivePageIndex = 0
    ClientRectBottom = 468
    ClientRectLeft = 1
    ClientRectRight = 749
    ClientRectTop = 21
    object cxTabSheet1: TcxTabSheet
      Caption = #23457#25209#35268#21017#21015#34920
      ImageIndex = 0
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object ABDBcxGrid1: TABcxGrid
        Left = 0
        Top = 0
        Width = 748
        Height = 447
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
        ExplicitLeft = 32
        ExplicitTop = 24
        object ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView
          PopupMenu.AutoHotkeys = maManual
          PopupMenu.CloseFootStr = False
          PopupMenu.LinkTableView = ABcxGridDBBandedTableView1
          PopupMenu.AutoApplyBestFit = True
          PopupMenu.AutoCreateAllItem = True
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ABDatasource1
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.AutoDataSetFilter = True
          DataController.Filter.TranslateBetween = True
          DataController.Filter.TranslateIn = True
          DataController.Filter.TranslateLike = True
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.AlwaysShowEditor = True
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.DataRowSizing = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.MultiSelect = True
          OptionsView.CellAutoHeight = True
          OptionsView.GroupByBox = False
          OptionsView.BandHeaders = False
          Bands = <
            item
            end>
          ExtPopupMenu.AutoHotkeys = maManual
          ExtPopupMenu.CloseFootStr = False
          ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView1
          ExtPopupMenu.AutoApplyBestFit = True
          ExtPopupMenu.AutoCreateAllItem = True
        end
        object cxGridLevel1: TcxGridLevel
          GridView = ABcxGridDBBandedTableView1
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = #19981#38656#23457#25209#30340#24405#20837#20154#21592
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object ABDBNavigator2: TABDBNavigator
        Left = 0
        Top = 0
        Width = 748
        Height = 27
        Align = alTop
        BevelOuter = bvNone
        ShowCaption = False
        TabOrder = 0
        BigGlyph = False
        ImageLayout = blGlyphLeft
        DataSource = ABDatasource1_1
        VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
        ButtonRangeType = RtDetail
        BtnCustom1ImageIndex = -1
        BtnCustom2ImageIndex = -1
        BtnCustom3ImageIndex = -1
        BtnCustom4ImageIndex = -1
        BtnCustom5ImageIndex = -1
        BtnCustom6ImageIndex = -1
        BtnCustom7ImageIndex = -1
        BtnCustom8ImageIndex = -1
        BtnCustom9ImageIndex = -1
        BtnCustom10ImageIndex = -1
        BtnCustom1Caption = #33258#23450#20041'1'
        BtnCustom2Caption = #33258#23450#20041'2'
        BtnCustom3Caption = #33258#23450#20041'3'
        BtnCustom4Caption = #33258#23450#20041'4'
        BtnCustom5Caption = #33258#23450#20041'5'
        BtnCustom6Caption = #33258#23450#20041'6'
        BtnCustom7Caption = #33258#23450#20041'7'
        BtnCustom8Caption = #33258#23450#20041'8'
        BtnCustom9Caption = #33258#23450#20041'9'
        BtnCustom10Caption = #33258#23450#20041'10'
        BtnCustom1Kind = cxbkStandard
        BtnCustom2Kind = cxbkStandard
        BtnCustom3Kind = cxbkStandard
        BtnCustom4Kind = cxbkStandard
        BtnCustom5Kind = cxbkStandard
        BtnCustom6Kind = cxbkStandard
        BtnCustom7Kind = cxbkStandard
        BtnCustom8Kind = cxbkStandard
        BtnCustom9Kind = cxbkStandard
        BtnCustom10Kind = cxbkStandard
        ApprovedRollbackButton = nbNull
        ApprovedCommitButton = nbNull
        ButtonControlType = ctSingle
      end
      object ABDBcxGrid2: TABcxGrid
        Left = 0
        Top = 27
        Width = 748
        Height = 420
        Align = alClient
        TabOrder = 1
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
        object ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView
          PopupMenu.AutoHotkeys = maManual
          PopupMenu.CloseFootStr = False
          PopupMenu.LinkTableView = ABcxGridDBBandedTableView2
          PopupMenu.AutoApplyBestFit = True
          PopupMenu.AutoCreateAllItem = True
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ABDatasource1_1
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.AutoDataSetFilter = True
          DataController.Filter.TranslateBetween = True
          DataController.Filter.TranslateIn = True
          DataController.Filter.TranslateLike = True
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.AlwaysShowEditor = True
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.DataRowSizing = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.MultiSelect = True
          OptionsView.CellAutoHeight = True
          OptionsView.GroupByBox = False
          OptionsView.BandHeaders = False
          Bands = <
            item
            end>
          ExtPopupMenu.AutoHotkeys = maManual
          ExtPopupMenu.CloseFootStr = False
          ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView2
          ExtPopupMenu.AutoApplyBestFit = True
          ExtPopupMenu.AutoCreateAllItem = True
        end
        object cxGridLevel2: TcxGridLevel
          GridView = ABcxGridDBBandedTableView2
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = #23457#25209#30340#21333#25454#31867#22411#19982#26465#20214
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object ABDBNavigator3: TABDBNavigator
        Left = 0
        Top = 0
        Width = 748
        Height = 27
        Align = alTop
        BevelOuter = bvNone
        ShowCaption = False
        TabOrder = 0
        BigGlyph = False
        ImageLayout = blGlyphLeft
        DataSource = ABDatasource1_2
        VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
        ButtonRangeType = RtDetail
        BtnCustom1ImageIndex = -1
        BtnCustom2ImageIndex = -1
        BtnCustom3ImageIndex = -1
        BtnCustom4ImageIndex = -1
        BtnCustom5ImageIndex = -1
        BtnCustom6ImageIndex = -1
        BtnCustom7ImageIndex = -1
        BtnCustom8ImageIndex = -1
        BtnCustom9ImageIndex = -1
        BtnCustom10ImageIndex = -1
        BtnCustom1Caption = #33258#23450#20041'1'
        BtnCustom2Caption = #33258#23450#20041'2'
        BtnCustom3Caption = #33258#23450#20041'3'
        BtnCustom4Caption = #33258#23450#20041'4'
        BtnCustom5Caption = #33258#23450#20041'5'
        BtnCustom6Caption = #33258#23450#20041'6'
        BtnCustom7Caption = #33258#23450#20041'7'
        BtnCustom8Caption = #33258#23450#20041'8'
        BtnCustom9Caption = #33258#23450#20041'9'
        BtnCustom10Caption = #33258#23450#20041'10'
        BtnCustom1Kind = cxbkStandard
        BtnCustom2Kind = cxbkStandard
        BtnCustom3Kind = cxbkStandard
        BtnCustom4Kind = cxbkStandard
        BtnCustom5Kind = cxbkStandard
        BtnCustom6Kind = cxbkStandard
        BtnCustom7Kind = cxbkStandard
        BtnCustom8Kind = cxbkStandard
        BtnCustom9Kind = cxbkStandard
        BtnCustom10Kind = cxbkStandard
        ApprovedRollbackButton = nbNull
        ApprovedCommitButton = nbNull
        ButtonControlType = ctSingle
      end
      object ABDBcxGrid3: TABcxGrid
        Left = 0
        Top = 27
        Width = 748
        Height = 420
        Align = alClient
        TabOrder = 1
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
        object ABcxGridDBBandedTableView3: TABcxGridDBBandedTableView
          PopupMenu.AutoHotkeys = maManual
          PopupMenu.CloseFootStr = False
          PopupMenu.LinkTableView = ABcxGridDBBandedTableView3
          PopupMenu.AutoApplyBestFit = True
          PopupMenu.AutoCreateAllItem = True
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ABDatasource1_2
          DataController.Filter.Options = [fcoCaseInsensitive]
          DataController.Filter.AutoDataSetFilter = True
          DataController.Filter.TranslateBetween = True
          DataController.Filter.TranslateIn = True
          DataController.Filter.TranslateLike = True
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.AlwaysShowEditor = True
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.DataRowSizing = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.MultiSelect = True
          OptionsView.CellAutoHeight = True
          OptionsView.GroupByBox = False
          OptionsView.BandHeaders = False
          Bands = <
            item
            end>
          ExtPopupMenu.AutoHotkeys = maManual
          ExtPopupMenu.CloseFootStr = False
          ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView3
          ExtPopupMenu.AutoApplyBestFit = True
          ExtPopupMenu.AutoCreateAllItem = True
        end
        object cxGridLevel3: TcxGridLevel
          GridView = ABcxGridDBBandedTableView3
        end
      end
    end
    object cxTabSheet4: TcxTabSheet
      Caption = #23457#25209#38454#27573#19982#23457#25209#20154#21592
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Splitter3: TABcxSplitter
        Left = 372
        Top = 0
        Width = 8
        Height = 447
        HotZoneClassName = 'TcxMediaPlayer8Style'
        InvertDirection = True
        Control = Panel6
      end
      object Panel4: TPanel
        Left = 380
        Top = 0
        Width = 368
        Height = 447
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 1
          Top = 1
          Width = 366
          Height = 24
          Align = alTop
          BevelOuter = bvNone
          Caption = #24403#21069#38454#27573#19979#30340#23457#25209#20154#21592
          Color = clMedGray
          ParentBackground = False
          TabOrder = 0
        end
        object ABDBNavigator5: TABDBNavigator
          Left = 1
          Top = 25
          Width = 366
          Height = 27
          Align = alTop
          BevelOuter = bvNone
          ShowCaption = False
          TabOrder = 1
          BigGlyph = False
          ImageLayout = blGlyphLeft
          DataSource = ABDatasource1_3_1
          VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
          ButtonRangeType = RtDetail
          BtnCustom1ImageIndex = -1
          BtnCustom2ImageIndex = -1
          BtnCustom3ImageIndex = -1
          BtnCustom4ImageIndex = -1
          BtnCustom5ImageIndex = -1
          BtnCustom6ImageIndex = -1
          BtnCustom7ImageIndex = -1
          BtnCustom8ImageIndex = -1
          BtnCustom9ImageIndex = -1
          BtnCustom10ImageIndex = -1
          BtnCustom1Caption = #33258#23450#20041'1'
          BtnCustom2Caption = #33258#23450#20041'2'
          BtnCustom3Caption = #33258#23450#20041'3'
          BtnCustom4Caption = #33258#23450#20041'4'
          BtnCustom5Caption = #33258#23450#20041'5'
          BtnCustom6Caption = #33258#23450#20041'6'
          BtnCustom7Caption = #33258#23450#20041'7'
          BtnCustom8Caption = #33258#23450#20041'8'
          BtnCustom9Caption = #33258#23450#20041'9'
          BtnCustom10Caption = #33258#23450#20041'10'
          BtnCustom1Kind = cxbkStandard
          BtnCustom2Kind = cxbkStandard
          BtnCustom3Kind = cxbkStandard
          BtnCustom4Kind = cxbkStandard
          BtnCustom5Kind = cxbkStandard
          BtnCustom6Kind = cxbkStandard
          BtnCustom7Kind = cxbkStandard
          BtnCustom8Kind = cxbkStandard
          BtnCustom9Kind = cxbkStandard
          BtnCustom10Kind = cxbkStandard
          ApprovedRollbackButton = nbNull
          ApprovedCommitButton = nbNull
          ButtonControlType = ctSingle
        end
        object ABDBcxGrid5: TABcxGrid
          Left = 1
          Top = 52
          Width = 366
          Height = 394
          Align = alClient
          TabOrder = 2
          OnEnter = ABDBcxGrid5Enter
          LookAndFeel.Kind = lfFlat
          LookAndFeel.NativeStyle = False
          object ABcxGridDBBandedTableView5: TABcxGridDBBandedTableView
            PopupMenu.AutoHotkeys = maManual
            PopupMenu.CloseFootStr = False
            PopupMenu.LinkTableView = ABcxGridDBBandedTableView5
            PopupMenu.AutoApplyBestFit = True
            PopupMenu.AutoCreateAllItem = True
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = ABDatasource1_3_1
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Filter.AutoDataSetFilter = True
            DataController.Filter.TranslateBetween = True
            DataController.Filter.TranslateIn = True
            DataController.Filter.TranslateLike = True
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.AlwaysShowEditor = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.DataRowSizing = True
            OptionsData.Deleting = False
            OptionsData.Inserting = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsSelection.MultiSelect = True
            OptionsView.CellAutoHeight = True
            OptionsView.GroupByBox = False
            OptionsView.BandHeaders = False
            Bands = <
              item
              end>
            ExtPopupMenu.AutoHotkeys = maManual
            ExtPopupMenu.CloseFootStr = False
            ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView5
            ExtPopupMenu.AutoApplyBestFit = True
            ExtPopupMenu.AutoCreateAllItem = True
          end
          object cxGridLevel5: TcxGridLevel
            GridView = ABcxGridDBBandedTableView5
          end
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 372
        Height = 447
        Align = alLeft
        TabOrder = 1
        object Panel7: TPanel
          Left = 1
          Top = 1
          Width = 370
          Height = 24
          Align = alTop
          BevelOuter = bvNone
          Caption = #23457#25209#38454#27573
          Color = clMedGray
          ParentBackground = False
          TabOrder = 0
        end
        object ABDBNavigator4: TABDBNavigator
          Left = 1
          Top = 25
          Width = 370
          Height = 27
          Align = alTop
          BevelOuter = bvNone
          ShowCaption = False
          TabOrder = 1
          BigGlyph = False
          ImageLayout = blGlyphLeft
          DataSource = ABDatasource1_3
          VisibleButtons = [nbInsertSpacer, nbInsert, nbCopy, nbDelete, nbEdit, nbPostSpacer, nbPost, nbCancel]
          ButtonRangeType = RtDetail
          BtnCustom1ImageIndex = -1
          BtnCustom2ImageIndex = -1
          BtnCustom3ImageIndex = -1
          BtnCustom4ImageIndex = -1
          BtnCustom5ImageIndex = -1
          BtnCustom6ImageIndex = -1
          BtnCustom7ImageIndex = -1
          BtnCustom8ImageIndex = -1
          BtnCustom9ImageIndex = -1
          BtnCustom10ImageIndex = -1
          BtnCustom1Caption = #33258#23450#20041'1'
          BtnCustom2Caption = #33258#23450#20041'2'
          BtnCustom3Caption = #33258#23450#20041'3'
          BtnCustom4Caption = #33258#23450#20041'4'
          BtnCustom5Caption = #33258#23450#20041'5'
          BtnCustom6Caption = #33258#23450#20041'6'
          BtnCustom7Caption = #33258#23450#20041'7'
          BtnCustom8Caption = #33258#23450#20041'8'
          BtnCustom9Caption = #33258#23450#20041'9'
          BtnCustom10Caption = #33258#23450#20041'10'
          BtnCustom1Kind = cxbkStandard
          BtnCustom2Kind = cxbkStandard
          BtnCustom3Kind = cxbkStandard
          BtnCustom4Kind = cxbkStandard
          BtnCustom5Kind = cxbkStandard
          BtnCustom6Kind = cxbkStandard
          BtnCustom7Kind = cxbkStandard
          BtnCustom8Kind = cxbkStandard
          BtnCustom9Kind = cxbkStandard
          BtnCustom10Kind = cxbkStandard
          ApprovedRollbackButton = nbNull
          ApprovedCommitButton = nbNull
          ButtonControlType = ctSingle
        end
        object ABDBcxGrid4: TABcxGrid
          Left = 1
          Top = 52
          Width = 370
          Height = 394
          Align = alClient
          TabOrder = 2
          OnEnter = ABDBcxGrid4Enter
          LookAndFeel.Kind = lfFlat
          LookAndFeel.NativeStyle = False
          object ABcxGridDBBandedTableView4: TABcxGridDBBandedTableView
            PopupMenu.AutoHotkeys = maManual
            PopupMenu.CloseFootStr = False
            PopupMenu.LinkTableView = ABcxGridDBBandedTableView4
            PopupMenu.AutoApplyBestFit = True
            PopupMenu.AutoCreateAllItem = True
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = ABDatasource1_3
            DataController.Filter.Options = [fcoCaseInsensitive]
            DataController.Filter.AutoDataSetFilter = True
            DataController.Filter.TranslateBetween = True
            DataController.Filter.TranslateIn = True
            DataController.Filter.TranslateLike = True
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.AlwaysShowEditor = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.DataRowSizing = True
            OptionsData.Deleting = False
            OptionsData.Inserting = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsSelection.MultiSelect = True
            OptionsView.CellAutoHeight = True
            OptionsView.GroupByBox = False
            OptionsView.BandHeaders = False
            Bands = <
              item
              end>
            ExtPopupMenu.AutoHotkeys = maManual
            ExtPopupMenu.CloseFootStr = False
            ExtPopupMenu.LinkTableView = ABcxGridDBBandedTableView4
            ExtPopupMenu.AutoApplyBestFit = True
            ExtPopupMenu.AutoCreateAllItem = True
          end
          object cxGridLevel4: TcxGridLevel
            GridView = ABcxGridDBBandedTableView4
          end
        end
      end
    end
  end
  object ABQuery1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    SQL.Strings = (
      ' select *  from ABSys_Approved_Rule order by RU_Order'#13#10)
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Approved_Rule')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Approved_Rule')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 10
    Top = 190
  end
  object ABDatasource1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1
    Left = 43
    Top = 190
  end
  object ABQuery1_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    MasterFields = 'Ru_Guid'
    DetailFields = 'Ro_Ru_Guid'
    SQL.Strings = (
      
        ' select *  from ABSys_Approved_PassRuleOperator'#13#10'where Ro_Ru_Gui' +
        'd=:Ru_Guid'#13#10' order by RO_RU_Guid,RO_Order'#13#10)
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Approved_PassRuleOperator')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Approved_PassRuleOperator')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 10
    Top = 225
    ParamData = <
      item
        Name = 'Ru_Guid'
      end>
  end
  object ABDatasource1_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_1
    Left = 43
    Top = 225
  end
  object ABQuery1_2: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    MasterFields = 'Ru_Guid'
    DetailFields = 'CP_Ru_Guid'
    SQL.Strings = (
      
        ' select *  from ABSys_Approved_ConditionParam  '#13#10'where CP_Ru_Gui' +
        'd=:Ru_Guid'#13#10'order by CP_RU_Guid,CP_Order'#13#10)
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Approved_ConditionParam')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Approved_ConditionParam')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 9
    Top = 258
    ParamData = <
      item
        Name = 'Ru_Guid'
      end>
  end
  object ABDatasource1_2: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_2
    Left = 42
    Top = 258
  end
  object ABQuery1_3: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1
    MasterFields = 'Ru_Guid'
    DetailFields = 'ST_Ru_Guid'
    SQL.Strings = (
      
        ' select *  from ABSys_Approved_Step    '#13#10'where ST_Ru_Guid=:Ru_Gu' +
        'id'#13#10'order by ST_RU_Guid,ST_Order'#13#10)
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Approved_Step')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Approved_Step')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 8
    Top = 293
    ParamData = <
      item
        Name = 'Ru_Guid'
      end>
  end
  object ABDatasource1_3: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_3
    Left = 41
    Top = 293
  end
  object ABDatasource1_3_1: TABDatasource
    AutoEdit = False
    DataSet = ABQuery1_3_1
    Left = 116
    Top = 292
  end
  object ABQuery1_3_1: TABQuery
    ActiveStoredUsage = []
    ReadOnly = False
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    MasterSource = ABDatasource1_3
    MasterFields = 'ST_Guid'
    DetailFields = 'SO_ST_Guid'
    SQL.Strings = (
      
        ' select *  from ABSys_Approved_StepOperator     '#13#10'where SO_ST_Gu' +
        'id=:ST_Guid'#13#10'order by SO_ST_Guid,SO_Order'#13#10)
    BeforeDeleteAsk = True
    MaxRecordCount = 0
    UseTrans = False
    UpdateDatabase = True
    CacheUpdate = False
    UpdateMode = upWhereKeyOnly
    UpdateTables.Strings = (
      'ABSys_Approved_StepOperator')
    IndexListDefs = <>
    LoadTables.Strings = (
      'ABSys_Approved_StepOperator')
    InitActiveOK = False
    BlobFieldDefs = <>
    MultiLevelQuery = True
    MultiLevelOrder = 0
    MultiLevelAlign = alTop
    CanCheckFieldValue = True
    AddTableRowQueryRight = True
    Left = 77
    Top = 292
    ParamData = <
      item
        Name = 'ST_Guid'
      end>
  end
end
