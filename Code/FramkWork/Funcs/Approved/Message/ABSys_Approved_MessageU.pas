unit ABSys_Approved_MessageU;

interface

uses
  ABPubFuncU,
  ABPubPanelU,
  ABPubDBU,
  ABPubMessageU,
  ABPubInPutStrU,

  ABThirdQueryU,
  ABThirdConnU,
  ABThirdDBU,
  ABThirdCustomQueryU,

  ABFramkWorkQueryU,
  ABFramkWorkcxGridU,
  ABFramkWorkFuncU,
  ABFramkWorkDBNavigatorU,
  ABFramkWorkControlU,
  ABFramkWorkUpAndDownU,
  ABFramkWorkFuncFormU,
  ABFramkWorkDictionaryQueryU,
  ABFramkWorkOperatorDatasetU,

  cxLookAndFeels,
  cxSplitter,
  cxButtons,
  cxLookAndFeelPainters,
  cxCheckBox,
  cxContainer,
  cxPC,
  cxGrid,
  cxGridDBBandedTableView,
  cxGridBandedTableView,
  cxGridTableView,
  cxGridCustomTableView,
  cxGridCustomView,
  cxControls,
  cxClasses,
  cxGridLevel,
  cxDBData,
  cxEdit,
  cxDataStorage,
  cxData,
  cxFilter,
  cxGraphics,
  cxCustomData,
  cxStyles,
  dxStatusBar,

  Dialogs,SqlExpr,StdCtrls,ComCtrls,Controls,Classes,ExtCtrls,DB,DBCtrls,DBClient,
  Menus,dxBarBuiltInMenu, cxNavigator, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, ABFramkWorkDBPanelU;

type
  TABSys_Approved_MessageForm = class(TABFuncForm)
    ABDBcxGrid1: TABcxGrid;
    ABcxGridDBBandedTableView1: TABcxGridDBBandedTableView;
    cxGridLevel1: TcxGridLevel;
    ABDBcxGrid2: TABcxGrid;
    ABcxGridDBBandedTableView2: TABcxGridDBBandedTableView;
    cxGridLevel2: TcxGridLevel;
    ABDBStatusBar1: TABdxDBStatusBar;
    ABQuery1: TABQuery;
    ABDatasource1: TABDatasource;
    ABQuery2: TABQuery;
    ABDatasource2: TABDatasource;
    ABcxPageControl1: TABcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    ABDBNavigator2: TABDBNavigator;
    Panel2: TPanel;
    Button1: TABcxButton;
    Button2: TABcxButton;
    procedure FormCreate(Sender: TObject);
    procedure ABcxPageControl1Change(Sender: TObject);
    procedure ABcxGridDBBandedTableView1DblClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FABSys_Approved_Bill:TDataset;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ABSys_Approved_MessageForm: TABSys_Approved_MessageForm;

implementation

uses SysUtils, Forms;


{$R *.dfm}


procedure ABRegister(var aFormClassName:string);stdcall;
begin
  aFormClassName  :=TABSys_Approved_MessageForm.ClassName;
end;

exports
   ABRegister ;


procedure TABSys_Approved_MessageForm.ABcxGridDBBandedTableView1DblClick(
  Sender: TObject);
var
  tempDataset:TDataset;
begin
  if FABSys_Approved_Bill.Locate('Bi_Guid',ABQuery1.FieldByName('ME_Bi_Guid').AsString,[]) then
  begin
    tempDataset:= ABGetConstSqlPubDataset('ABSys_Approved_Bill',[FABSys_Approved_Bill.FieldByName('Bi_Code').AsString]);
    if (Assigned(tempDataset)) and
       (tempDataset.FieldByName('Bi_MainTableName').AsString<>emptystr) and
       (tempDataset.FieldByName('Bi_MainTableLinkField').AsString<>emptystr) then
    begin
      ABShowDataset(     tempDataset.FieldByName('Bi_Name').AsString,
                        [tempDataset.FieldByName('Bi_Name').AsString+'主'],
                        [tempDataset.FieldByName('Bi_MainConnName').AsString],
                        [' select * '+
                         ' from '+tempDataset.FieldByName('Bi_MainTableName').AsString+
                         ' where '+tempDataset.FieldByName('Bi_MainTableLinkField').AsString+'='+QuotedStr(ABQuery1.FieldByName('ME_FromMainGuid').AsString)],
                        [''],

                        [[ABIIF(tempDataset.FieldByName('Bi_DetailTableName').AsString=emptystr,emptystr,tempDataset.FieldByName('Bi_Name').AsString+'明细')]],
                        [[tempDataset.FieldByName('Bi_DetailConnName').AsString]],
                        [[ABIIF(tempDataset.FieldByName('Bi_DetailTableName').AsString=emptystr,emptystr,
                         ' select * '+
                         ' from '+tempDataset.FieldByName('Bi_DetailTableName').AsString+
                         ' where '+tempDataset.FieldByName('Bi_DetailTableLinkField').AsString+'=:'+tempDataset.FieldByName('Bi_MainTableLinkField').AsString)]],
                        [['']],
                        [[tempDataset.FieldByName('Bi_MainTableLinkField').AsString]],
                        [[tempDataset.FieldByName('Bi_DetailTableLinkField').AsString]],

                        [],[]

                        );
    end;
  end;
end;

procedure TABSys_Approved_MessageForm.ABcxPageControl1Change(Sender: TObject);
begin
  ABInitFormPageControl(
                        ABcxPageControl1,

                        ABDBNavigator2,
                        ABDBStatusBar1,

                        [ABDatasource1,ABDatasource2],
                        [[],[]],
                        [[ABcxGridDBBandedTableView1],[ABcxGridDBBandedTableView2]],
                        [true,True],
                        [true,True],
                        [true,True],

                        [],
                        [],
                        [],
                        [],
                        [],
                        [],

                        [true,True]
                        );
end;

procedure TABSys_Approved_MessageForm.Button1Click(Sender: TObject);
var
  tempWhy:string;
begin
  if not ABDatasetIsEmpty(ABQuery1) then
  begin
    tempWhy:=ABQuery1.FieldByName('ME_NoAgreeWhy').AsString;
    if (ABInPutStr('原因',tempWhy,'','请先输入不同意原因')) and
       (tempWhy<>EmptyStr) then
    begin
      ABQuery1.edit;
      ABQuery1.FieldByName('ME_NoAgreeWhy').AsString:=tempWhy;
      ABQuery1.FieldByName('ME_State').AsString:='Dissent';
      ABQuery1.Post;
      ABQuery1.OnlyDatasetDelete;
    end;
  end;
end;

procedure TABSys_Approved_MessageForm.Button2Click(Sender: TObject);
begin
  if not ABDatasetIsEmpty(ABQuery1) then
  begin
    ABSetFieldValue('Approve',ABQuery1.FieldByName('ME_State'),true);
    ABQuery1.OnlyDatasetDelete;
  end;
end;

procedure TABSys_Approved_MessageForm.FormCreate(Sender: TObject);
begin
  ABcxPageControl1Change(ABcxPageControl1);
  FABSys_Approved_Bill:=ABGetDataset('Main','select Bi_Guid,Bi_Code from ABSys_Approved_Bill',[]);
end;

procedure TABSys_Approved_MessageForm.FormDestroy(Sender: TObject);
begin
  FABSys_Approved_Bill.Free;
end;

Initialization
  RegisterClass(TABSys_Approved_MessageForm);
Finalization
  UnRegisterClass(TABSys_Approved_MessageForm);


end.



