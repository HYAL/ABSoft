unit Main;

interface                                

uses
  ABPubVarU,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, DBCtrls, Grids, DBGrids, ADODB, ComCtrls, StdCtrls;

type
  TABADOProgressBaForm = class(TForm)
    ADOConnection1: TADOConnection;
    ADOQuery1: TADOQuery;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    DataSource1: TDataSource;
    ProgressBar1: TProgressBar;
    Button1: TButton;
    Label1: TLabel;
    Label2: TLabel;
    procedure ADOQuery1FetchProgress(DataSet: TCustomADODataSet; Progress,
      MaxProgress: Integer; var EventStatus: TEventStatus);
    procedure ADOQuery1FetchComplete(DataSet: TCustomADODataSet;
      const Error: Error; var EventStatus: TEventStatus);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations 
  published}
    property TabOrder;
  end;

var
  ABADOProgressBaForm: TABADOProgressBaForm;

implementation

{$R *.dfm}

var
  beginTime,EndTime:longint;


procedure TABADOProgressBaForm.ADOQuery1FetchProgress(DataSet: TCustomADODataSet;
  Progress, MaxProgress: Integer; var EventStatus: TEventStatus);
begin
  ProgressBar1.Position := Progress;
end;

procedure TABADOProgressBaForm.ADOQuery1FetchComplete(DataSet: TCustomADODataSet;
  const Error: Error; var EventStatus: TEventStatus);
begin
  ProgressBar1.Position := 0;
  EndTime := GetTickCount;
  DataSource1.DataSet:=ADOQuery1;
  Button1.Enabled:=True;
end;

procedure TABADOProgressBaForm.Button1Click(Sender: TObject);
begin
  Button1.Enabled:=false;
  DataSource1.DataSet:=nil;
  beginTime:=GetTickCount;

  ADOQuery1.Close;
  ADOQuery1.ExecuteOptions :=ADOQuery1.ExecuteOptions- [eoAsyncFetchNonBlocking];
  ADOQuery1.SQL.Text:='select count(*) from table1 ';
  ADOQuery1.Open;

  ProgressBar1.Max :=ADOQuery1.Fields[0].AsInteger;

  ADOQuery1.Close;
  ADOQuery1.ExecuteOptions := [eoAsyncFetchNonBlocking];
  ADOQuery1.SQL.Text:='select * from table1 ';
  ADOQuery1.Open;
end;

procedure TABADOProgressBaForm.FormCreate(Sender: TObject);
begin
  ADOConnection1.Connected:=false;
  ADOConnection1.ConnectionString:=
    'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+ABSoftSetPath+'Data.mdb;Persist Security Info=False';
  ADOConnection1.Connected:=true;

end;

end.

