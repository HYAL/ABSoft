object ABRowRefreshForm: TABRowRefreshForm
  Left = 225
  Top = 230
  Caption = #34892#21047#26032
  ClientHeight = 446
  ClientWidth = 722
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 240
    Top = 10
    Width = 232
    Height = 13
    AutoSize = False
  end
  object Label2: TLabel
    Left = 48
    Top = 8
    Width = 139
    Height = 13
    AutoSize = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object DBGrid1: TDBGrid
    Left = 24
    Top = 96
    Width = 641
    Height = 345
    DataSource = DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DBNavigator1: TDBNavigator
    Left = 24
    Top = 65
    Width = 240
    Height = 25
    DataSource = DataSource1
    TabOrder = 1
  end
  object Button2: TButton
    Left = 192
    Top = 32
    Width = 75
    Height = 25
    Caption = #34892#21047#26032
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 112
    Top = 32
    Width = 75
    Height = 25
    Caption = #34920#21047#26032
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnClick = Button3Click
  end
  object Edit1: TEdit
    Tag = 91
    Left = 272
    Top = 32
    Width = 305
    Height = 21
    TabOrder = 4
    Text = 'update table1 set a=a+'#39'1'#39' where id=10'
  end
  object Button1: TButton
    Left = 592
    Top = 32
    Width = 75
    Height = 25
    Caption = #25191#34892
    TabOrder = 5
    OnClick = Button1Click
  end
  object ADOConnection1: TADOConnection
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 48
    Top = 128
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from table1')
    Left = 80
    Top = 128
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 112
    Top = 128
  end
end
