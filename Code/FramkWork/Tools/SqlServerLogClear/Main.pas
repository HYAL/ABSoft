unit Main;

interface

uses


  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,
  ComCtrls,StdCtrls,AdoDb,Grids,ValEdit;
const EnKey='12553649764327656689569879822442242';
      conn='Provider=SQLOLEDB.1;Persist Security Info=True;Data Source=%s;Initial Catalog=%s;User ID=%s;Password=%s';
type
  TMainForm = class(TForm)
    Label1: TLabel;
    EditServerName: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    EditUserName: TEdit;
    EditPassWord: TEdit;
    ConnectBtn: TButton;
    RefreshBtn: TButton;
    ClearBtn: TButton;
    MainSg: TStringGrid;
    StatusBar1: TStatusBar;
    procedure ConnectBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RefreshBtnClick(Sender: TObject);
    procedure ClearBtnClick(Sender: TObject);
  private
    { Private declarations }
    ConnStr:string;
    procedure DataBaseList;
    procedure SetConnStr;
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation
uses inifiles;
{$R *.dfm}

procedure TMainForm.ConnectBtnClick(Sender: TObject);
begin
  StatusBar1.Panels[0].Text:='Connecting...';
  SetConnStr;
  //ConnStr:=Format(conn,[servername,'master',username,password]);
  MainSg.Rows[1].Clear;
  DataBaseList;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin

  MainSg.ColWidths[0]:=100;
  MainSg.ColWidths[1]:=100;
  MainSg.Cells[0,0]:=('数据库名');
  MainSg.Cells[1,0]:=('大小(MB)');
end;

procedure TMainForm.RefreshBtnClick(Sender: TObject);
begin
  StatusBar1.Panels[0].Text:='Refreshing databases list...';
  SetConnStr;
  DataBaseList;
  StatusBar1.Panels[0].Text:='Refreshed databases list';
end;

procedure TMainForm.ClearBtnClick(Sender: TObject);
var
  Query:TAdoQuery;
  dbname:string;
begin
  SetConnStr;
  dbname:=Trim(MainSg.Cells[0,MainSg.Selection.TopLeft.Y]);
  if dbname='' then
  begin
    MessageBox(Handle,'Please select a database!','Errors',MB_OK+MB_ICONWARNING);
    exit;
  end;
  //ConnStr:=Format(conn,[servername,'master',username,password]);
  Query:=TAdoQuery.Create(nil);
  StatusBar1.Panels[0].Text:='Clearing '+dbname+'''s log....';
  try
    Query.ConnectionString:=ConnStr;
    Query.SQL.Clear;
    Query.SQL.Add('backup log ['+dbname+'] with no_log ');
    Query.SQL.Add('dbcc shrinkdatabase(['+dbname+'])');
    //showmessage(query.SQL.GetText);
    //exit;
    Query.ExecSQL;
    Query.Close;
    Query.Free;
    DataBAseList;
  except
    Query.Close;
    Query.Free;
  end;
end;

procedure TMainForm.DataBaseList;
var
  Query:TAdoQuery;
  i:integer;
begin
  Query:=TAdoQuery.Create(nil);
  try
    Query.ConnectionString:=ConnStr;
    Query.SQL.Clear;
    Query.SQL.Add('exec sp_databases');
    Query.Open;
    if Query.RecordCount<1 then
    begin
      MainSg.RowCount:=2;
      MainSg.Rows[1].Clear;
    end
    else
    begin
      MainSG.RowCount:=Query.RecordCount+1;
      for i:=0 to Query.RecordCount-1 do
      begin
        StatusBar1.Panels[0].Text:='Getting databases list....';
        MainSg.Cells[0,i+1]:=Query.FieldByName('DATABASE_NAME').AsString;
        MainSg.Cells[1,i+1]:=Format('%.2f MB',[Query.FieldByName('DATABASE_SIZE').AsFloat/1024]);
        Query.Next;
      end;
      Query.close;
      Query.free;
    end;
    StatusBar1.Panels[0].Text:='Get databases list finish!';
  except
    Query.Close;
    Query.Free;
    StatusBar1.Panels[0].Text:='Can not connect server';
    MessageBox(Handle,'Can not connect server','Errors',MB_OK+MB_ICONWARNING);

  end;
end;

procedure TMainForm.SetConnStr;
var
  servername,username,password:string;
begin
  servername:=Trim(EditServerName.Text);
  username:=Trim(EditUsername.Text);
  password:=Trim(EditPassWord.Text);
  if (servername='') or (username='') then
  begin
    MessageBox(Handle,'Please input server and username!','Errors',MB_OK+MB_ICONWARNING);
    exit;
  end;
  ConnStr:=Format(conn,[servername,'master',username,password]);

end;

end.

