program CompareText;

uses
  Forms,
  Detail in '..\..\CompareText\Detail.PAS' {Dateivergleich},
  Main in '..\..\CompareText\Main.pas' {MainForm};

{$R *.RES}

begin
  Application.Title := 'Dateivergleich';
  Application.CreateForm(TMainForm, MainForm);
  if ParamCount>0 then
    MainForm.SetFileName(ParamStr(1));
  Application.CreateForm(TDateivergleich, Dateivergleich);
  Application.Run;
end.
