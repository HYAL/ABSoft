program DelphiManager;

uses
  Forms,
  MainU in '..\..\DelphiManager\MainU.pas' {MainForm},
  PathSetU in '..\..\DelphiManager\PathSetU.pas' {PathSetForm};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TPathSetForm, PathSetForm);
  Application.Run;
end.
