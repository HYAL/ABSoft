program Setup;

uses
  Forms,
  Registry,
  Windows,
  SysUtils,
  ABPubMessageU,
  BackGroupFormU in '..\..\Setup\Setup\BackGroupFormU.pas' {BackGroundForm},
  MainFormU in '..\..\Setup\Setup\MainFormU.pas';

{$R *.res}

var
  myreg:tregistry;
begin
  Application.Initialize;
  Application.Title := 'setup';

  myreg:=tregistry.Create;
  try
    myreg.RootKey:=hkey_local_machine;
    myreg.OpenKey('software\Armous',true);
    if Trim(myreg.ReadString('SetupPath'))<>EmptyStr then
    begin
      abshow('一卡通系统已安装,如需重新安装请先卸载');
    end
    else
    begin
      ShowSetup;
    end;
    myreg.CloseKey;
  finally
    myreg.Free;
  end;

  Application.Run;
end.
