program CalePractise;

uses
  Forms,
  Main in '..\..\CalePractise\Main.pas' {CaleForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TCaleForm, CaleForm);
  Application.Run;
end.
