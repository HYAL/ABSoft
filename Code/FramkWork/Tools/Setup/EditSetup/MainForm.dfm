object frmEditSetup: TfrmEditSetup
  Left = 276
  Top = 149
  BorderStyle = bsDialog
  Caption = #21152#23494#25991#20214#32534#36753
  ClientHeight = 488
  ClientWidth = 555
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 0
    Top = 0
    Width = 555
    Height = 447
    Align = alClient
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 447
    Width = 555
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      555
      41)
    object btnFinish: TButton
      Left = 444
      Top = 6
      Width = 97
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #20445#23384
      TabOrder = 0
      OnClick = btnFinishClick
    end
    object Button1: TButton
      Left = 332
      Top = 6
      Width = 97
      Height = 25
      Anchors = [akTop, akRight]
      Caption = #21152#36733
      TabOrder = 1
      OnClick = SpeedButton1Click
    end
  end
end
