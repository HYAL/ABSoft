unit Main;

interface

uses
  ABPubCharacterCodingU,
  ABPubInPutStrU,
  ABPubMessageU,
  ABPubUserU,
  ABPubVarU,
  ABPubFuncU,
  ABPubFormU,
  ABPubConstU,

  ABThirdDBU,

  cxDropDownEdit,
  cxMaskEdit,
  cxTextEdit,
  cxEdit,
  cxContainer,
  cxLookAndFeelPainters,
  cxLookAndFeels,
  cxControls,
  cxGraphics,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,DB,Dialogs,
  StdCtrls,Buttons,ComCtrls,ExtCtrls,ToolWin,ImgList,StdConvs,ValEdit,Grids,
  ShellAPI;

type
  TMainForm = class(TABPubForm)
    Panel1: TPanel;
    btnFind: TBitBtn;
    LabelFind: TLabel;
    LabelFile: TLabel;
    LabelDir: TLabel;
    ImageListNormal: TImageList;
    Memo2: TMemo;
    btnStop: TBitBtn;
    Memo3: TMemo;
    BitBtn1: TBitBtn;
    Panel2: TPanel;
    ProgressBar1: TProgressBar;
    ProgressBar2: TProgressBar;
    rg1: TRadioGroup;
    Edit1: TEdit;
    SpeedButton2: TSpeedButton;
    Panel3: TPanel;
    PageControlWork: TPageControl;
    tstResult: TTabSheet;
    RichEdit1: TRichEdit;
    tstEdit: TTabSheet;
    ToolBar: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton6: TToolButton;
    ToolButton5: TToolButton;
    ToolButton4: TToolButton;
    RichEdit2: TRichEdit;
    Panel4: TPanel;
    Label4: TLabel;
    GroupBox1: TGroupBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    GroupBox2: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox3: TCheckBox;
    Label1: TLabel;
    GroupBox3: TGroupBox;
    CheckBox4: TCheckBox;
    CheckBox2: TCheckBox;
    Edit3: TEdit;
    Label3: TLabel;
    Label5: TLabel;
    Edit4: TEdit;
    Edit5: TEdit;
    Label8: TLabel;
    Label9: TLabel;
    Label6: TLabel;
    Edit6: TEdit;
    CheckBox8: TCheckBox;
    Edit7: TEdit;
    Label7: TLabel;
    Edit8: TEdit;
    ComboBox2: TComboBox;
    CheckBox9: TCheckBox;
    CheckBox10: TCheckBox;
    CheckBox7: TCheckBox;
    Label2: TLabel;
    procedure btnFindClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnStopClick(Sender: TObject);
    procedure RichEdit1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure rg1Click(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure RichEdit2MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    FFindFileCount: LongInt;
    FFindCount: LongInt;
    procedure FindFileName(aOnlyFind: Boolean = true);
    procedure FindPathName(aOnlyFind: Boolean = true);
    procedure FindStrInFile(aOnlyFind: Boolean = true);
    function Replace: LongInt;

    procedure AddFindFileCount(aAddNum: LongInt = 1);
    procedure AddFindCount(aAddNum: LongInt = 1);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

var
  tempFileName: string;
  tempBegDown: TDateTime;

procedure TMainForm.AddFindCount(aAddNum: LongInt);
begin
  FFindCount := FFindCount + aAddNum;

  Label2.Caption := ('共查找到') + inttostr(FFindCount) + ('个查找项.');
end;

procedure TMainForm.AddFindFileCount(aAddNum: LongInt);
begin
  FFindFileCount := FFindFileCount + aAddNum;

  if rg1.ItemIndex = 0 then
    Label4.Caption := ('共查找到') + inttostr(FFindFileCount) + ('个目录.')
  else
    Label4.Caption := ('共查找到') + inttostr(FFindFileCount) + ('个文件.');
end;

procedure TMainForm.btnFindClick(Sender: TObject);
begin
  FFindFileCount := 0;
  FFindCount     := 0;
  RichEdit1.clear;
  ProgressBar1.Min := 0;
  ProgressBar1.Position := 0;
  ProgressBar2.Min := 0;
  ProgressBar2.Position := 0;
  btnFind.Enabled := false;
  BitBtn1.Enabled := false;
  btnStop.Enabled := true;
  RichEdit2.Lines.BeginUpdate;
  RichEdit1.Lines.BeginUpdate;
  Label4.Caption := emptystr;
  try
    case rg1.ItemIndex of
      0:
        begin
          FindPathName(Sender = btnFind);
        end;
      1:
        begin
          FindFileName(Sender = btnFind);
        end;
      2:
        begin
          FindStrInFile(Sender = btnFind);

        end;
    end;
  finally
    RichEdit2.SelStart := 0;
    RichEdit2.SelLength := 0;
    RichEdit2.Perform(EM_LINESCROLL, 0, 0);

    RichEdit2.Lines.EndUpdate;
    RichEdit1.Lines.EndUpdate;
    ProgressBar1.Position := 0;
    ProgressBar2.Position := 0;
    btnFind.Enabled := true;
    BitBtn1.Enabled := true;
    btnStop.Enabled := false;
    PageControlWork.ActivePageIndex := 0;
  end;
end;

procedure TMainForm.BitBtn1Click(Sender: TObject);
var
  tempStr1: string;
begin
  if trim(Memo3.Text) = EmptyStr then
  begin
    tempStr1 := ('你确认要将') + Memo2.Text + ('删除吗？');
  end
  else
    tempStr1 := ('你确认要替换') + Memo2.Text + ('到') + Memo3.Text + ('吗？');

  if ABShow(tempStr1, [], ['是', '否'], 1) = 1 then
  begin
    if ABShow('此操作将无法还原,请确认.', [], ['是', '否'], 1) = 1 then
      btnFindClick(BitBtn1);
  end;
end;

procedure TMainForm.FindPathName(aOnlyFind: Boolean = true);
var
  i: LongInt;
  tempStr1, tempStr2: string;
  tempList1: TStringList;
begin
  tempList1 := TStringList.Create;
  ProgressBar1.Max := ABGetDirCount(ComboBox2.Text, ABStrToStringArray(Memo2.Text),StrToIntDef(edit3.text, -1));
  ABGetDirList(tempList1,ComboBox2.Text, ABStrToStringArray(Memo2.Text), StrToIntDef(edit3.text, -1),  ProgressBar1, not CheckBox4.Checked);
  ProgressBar1.Position := 0;
  try
    ProgressBar1.Max := tempList1.Count;
    for I := 0 to tempList1.Count - 1 do
    begin
      if not btnStop.Enabled then Break;

      if (not CheckBox1.Checked) or (ABHaveChinese(tempList1.Strings[i])) then
      begin
        AddFindFileCount;
        RichEdit1.Lines.Add(tempList1.Strings[i]);
        if (not aOnlyFind) and (not CheckBox4.Checked) then
        begin
          tempStr2 := tempList1.Strings[i];
          tempStr1 := ABGetParentDir(tempStr2);
          tempStr1 := ABLeftStr(tempStr2, length(tempStr2) - length(tempStr1)) +
            StringReplace(tempStr1, Memo2.Text, Memo3.Text, [rfReplaceAll, rfIgnoreCase]);
          if tempStr1 <> tempStr2 then
          begin
            if (ABGetFileAttr(tempStr2)) and (SysUtils.faReadOnly) = SysUtils.faReadOnly then
              ABSetFileAttr(tempStr2, SysUtils.faReadOnly, ftDelete);

            RenameFile(tempStr2, tempStr1);
          end;
        end;
      end;
      ProgressBar1.Position := ProgressBar1.Position + 1;
      Application.ProcessMessages;
    end;
  finally
    tempList1.Free;
  end;
end;

procedure TMainForm.FindFileName(aOnlyFind: Boolean = true);
var
  i: LongInt;
  tempStr1, tempStr2: string;
  tempList1: TStringList;
begin
  tempList1 := TStringList.Create;
  ProgressBar1.Max := ABGetFileCount(ComboBox2.Text, ABStrToStringArray(Memo2.Text),StrToIntDef(edit3.text, -1));
  ABGetFileList(tempList1, ComboBox2.Text, ABStrToStringArray(Memo2.Text), StrToIntDef(edit3.text, -1), false, ProgressBar1, CheckBox4.Checked);
  ProgressBar1.Position := 0;
  try
    ProgressBar1.Max := tempList1.Count;
    for I := 0 to tempList1.Count - 1 do
    begin
      if not btnStop.Enabled then Break;

      if ((not CheckBox1.Checked) or (ABHaveChinese(tempList1.Strings[i]))) then
      begin
        AddFindFileCount;
        RichEdit1.Lines.Add(tempList1.Strings[i]);
        if (not aOnlyFind) and (not CheckBox4.Checked) then
        begin
          tempStr2 := tempList1.Strings[i];
          tempStr1 := ABGetFilepath(tempStr2) + StringReplace(ABGetFilename(tempStr2, false), Memo2.Text, Memo3.Text, [rfReplaceAll, rfIgnoreCase]);
          if tempStr1 <> tempStr2 then
          begin
            if ABGetFileAttr(tempStr2) and SysUtils.faReadOnly = SysUtils.faReadOnly then
              ABSetFileAttr(tempStr2, SysUtils.faReadOnly, ftDelete);

            RenameFile(tempStr2, tempStr1);
          end;
        end;
      end;

      ProgressBar1.Position := ProgressBar1.Position + 1;
      Application.ProcessMessages;
    end;
  finally
    tempList1.Free;
  end;
end;

procedure TMainForm.SpeedButton2Click(Sender: TObject);
begin
  ComboBox2.Text:=ABSelectDirectory(ComboBox2.Text);
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  PageControlWork.ActivePageIndex := 0;
  tempBegDown := now;
  rg1Click(nil);
end;

procedure TMainForm.btnStopClick(Sender: TObject);
begin
  btnStop.Enabled := false;
end;

procedure TMainForm.rg1Click(Sender: TObject);
begin
  GroupBox3.Enabled := (rg1.ItemIndex = 0) or (rg1.ItemIndex = 1);
  GroupBox1.Enabled := not GroupBox3.Enabled;

  edit1.Enabled := not GroupBox3.Enabled;

  if (rg1.ItemIndex = 0) then
    CheckBox2.Caption := ('前N层目录')
  else if (rg1.ItemIndex = 1) then
    CheckBox2.Caption := ('前N层文件');
end;

procedure TMainForm.FindStrInFile(aOnlyFind: Boolean = true);
var
  tempFindStart, tempLastLine: integer;
  tempList1: TStringList;
  tempFirst, tempPass: boolean;
  i, j, l, m, tempLine, tempFindCount: LongInt;
  tempStr1, tempStr2, tempCurLineStr, tempLastInStr, tempLastNotINStr, tempLastStr: string;
begin
  if (Memo2.Text = '') and (not CheckBox1.Checked) then
    exit;
  if (not CheckBox5.Checked) and (not CheckBox6.Checked) then
    exit;

  tempLastLine := 0;
  if CheckBox8.Checked then
  begin
    tempLastLine := StrToInt(Edit7.Text);
    tempLastINStr := Edit6.Text;
    tempLastNotINStr := Edit8.Text;
  end;

  tempList1 := TStringList.Create;
  ProgressBar1.Max := ABGetFileCount(ComboBox2.Text, ABStrToStringArray(Edit1.Text));
  ABGetFileList(tempList1,ComboBox2.Text, ABStrToStringArray(Edit1.Text),-1, false, ProgressBar1);
  ProgressBar1.Position := 0;
  ProgressBar1.Max := tempList1.Count;
  try
    for I := 0 to tempList1.Count - 1 do
    begin
      if not btnStop.Enabled then Break;
      tempFileName := tempList1.Strings[i];
      tempFindCount := 0;

      tempFirst := true;

      RichEdit2.Clear;
//      if (AnsiCompareText(ABGetFileExt(tempFileName), 'dproj') = 0) then
//      begin
//        RichEdit2.Text :=ABReadUTF8Txt(tempFileName);
//      end
//      else
      begin
        RichEdit2.Text := ABReadTxt(tempFileName);
      end;

      ProgressBar2.Max := RichEdit2.Lines.Count;
      if CheckBox1.Checked then
      begin
        for j := 0 to RichEdit2.Lines.Count - 1 do
        begin
          if not btnStop.Enabled then Break;
          ProgressBar2.Position := RichEdit2.CaretPos.y;
          Application.ProcessMessages;
          tempCurLineStr := StringReplace(RichEdit2.Lines[j], #10, '', []);

          if (ABHaveChinese(tempCurLineStr)) and
            ((edit4.Text = EmptyStr) or (ABArrayInStr(ABStrToStringArray(edit4.Text),tempCurLineStr,[loPartialKey, loCaseInsensitive]))) and
            ((edit5.Text = EmptyStr) or (not ABArrayInStr(ABStrToStringArray(edit5.Text),tempCurLineStr,[loPartialKey, loCaseInsensitive]))) then
          begin
            tempPass := true;
            with RichEdit2 do
            begin
              if CheckBox8.Checked then
              begin
                tempLastStr := '';
                m := tempLastLine + 1;
                for l := j to RichEdit2.Lines.Count - 1 do
                begin
                  tempLastStr := tempLastStr + RichEdit2.Lines[l];

                  m := m - 1;
                  if m <= 0 then
                    break;
                end;
                tempPass := ((tempLastINStr = EmptyStr) or (ABArrayInStr(ABStrToStringArray(edit6.Text),tempLastStr,[loPartialKey, loCaseInsensitive]))) and
                  ((tempLastNotINStr = EmptyStr) or (not ABArrayInStr(ABStrToStringArray(edit8.Text),tempLastStr,[loPartialKey, loCaseInsensitive])));
              end;

            end;

            if tempPass then
            begin
              if tempFirst then
              begin
                AddFindFileCount;
                RichEdit1.SelAttributes.Color := clBlack;
                RichEdit1.Lines.Add(tempFileName);
                tempFirst := false;
              end;
              AddFindCount;
              RichEdit1.SelAttributes.Color := clRed;
              tempStr1 := ('    第') + IntToStr(j + 1) + ('行    ') + tempCurLineStr;
              try
                tempFindCount := tempFindCount + 1;
                RichEdit1.Lines.Add(tempStr1 + ('    第') + inttostr(tempFindCount) + ('处'));
              except

              end;
            end;
          end;
        end;
        ProgressBar2.Position := 0;
      end
      else
      begin
        tempFindStart := 0;
        while tempFindStart <> -1 do
        begin
          if not btnStop.Enabled then Break;
          ProgressBar2.Position := RichEdit2.CaretPos.y;
          Application.ProcessMessages;

          if CheckBox3.Checked then
            tempFindStart := RichEdit2.FindText(Memo2.Text, tempFindStart, RichEdit2.GetTextLen, [stWholeWord])
          else
            tempFindStart := RichEdit2.FindText(Memo2.Text, tempFindStart, RichEdit2.GetTextLen, []);

          if (tempFindStart <> -1) then
          begin
            if ((edit4.Text = EmptyStr) or (ABArrayInStr(ABStrToStringArray(edit4.Text),RichEdit2.Lines[RichEdit2.CaretPos.y],[loPartialKey, loCaseInsensitive]))) and
              ((edit5.Text = EmptyStr) or (not ABArrayInStr(ABStrToStringArray(edit5.Text),RichEdit2.Lines[RichEdit2.CaretPos.y],[loPartialKey, loCaseInsensitive]))) then
            begin

              with RichEdit2 do
              begin
                SelStart := tempFindStart;
                SelLength := Length(Memo2.Text);
                tempPass := true;
                if (CheckBox5.Checked) and (not CheckBox6.Checked) then
                begin
                  if CaretPos.x = Length(StringReplace(Lines[CaretPos.y], ABWrapStr, '', [])) then
                  begin
                    tempPass := false;
                  end;
                end
                else if (not CheckBox5.Checked) and (CheckBox6.Checked) then
                begin

                  if CaretPos.x <> Length(StringReplace(Lines[CaretPos.y], ABWrapStr, '', [])) then

                  begin

                    tempPass := false;
                  end;
                end;

                if (tempPass) then
                begin
                  if CheckBox8.Checked then
                  begin
                    tempLastStr := '';
                    m := tempLastLine + 1;
                    for l := CaretPos.y to RichEdit2.Lines.Count - 1 do
                    begin
                      tempLastStr := tempLastStr + RichEdit2.Lines[l];

                      m := m - 1;
                      if m <= 0 then
                        break;
                    end;
                    tempPass := ((tempLastINStr = EmptyStr) or (ABArrayInStr(ABStrToStringArray(edit6.Text),tempLastStr,[loPartialKey, loCaseInsensitive]))) and
                      ((tempLastNotINStr = EmptyStr) or (not ABArrayInStr(ABStrToStringArray(edit8.Text),tempLastStr,[loPartialKey, loCaseInsensitive])));
                  end;

                end;

                if tempPass then
                begin
                  if tempFirst then
                  begin
                    AddFindFileCount;
                    RichEdit1.SelAttributes.Color := clBlack;
                    RichEdit1.Lines.Add(tempFileName);
                    tempFirst := false;
                  end;
                  AddFindCount;

                  if not aOnlyFind then
                    SelText := Memo3.Text;

                  tempLine := CaretPos.y;
                  RichEdit1.SelAttributes.Color := clRed;
                  tempStr2 := ('    第') + IntToStr(tempLine + 1) + ('行    ') + Lines[tempLine];
                  tempStr2 := StringReplace(tempStr2, #10, '', [rfReplaceAll, rfIgnoreCase]);
                  tempFindCount := tempFindCount + 1;
                  RichEdit1.Lines.Add(tempStr2 + ('    第') + inttostr(tempFindCount) + ('处'));
                end;
              end;
            end;
            tempFindStart := tempFindStart + Length(Memo2.Text);
          end;
        end;

        if (not aOnlyFind) and (not tempFirst) then
        begin
          if ABGetFileAttr(tempFileName) and SysUtils.faReadOnly = SysUtils.faReadOnly then
            ABSetFileAttr(tempFileName, SysUtils.faReadOnly, ftDelete);

          if CheckBox7.Checked then
            ABCopyFile(tempFileName, ABBackPath + ABGetFilename(tempFileName, false));

          RichEdit2.PlainText := True;
          try
            RichEdit2.Lines.SaveToFile(tempFileName);
          finally
            RichEdit2.PlainText := false;
          end;
        end;
        ProgressBar2.Position := 0;
      end;

      ProgressBar1.Position := ProgressBar1.Position + 1;
      Application.ProcessMessages;
    end;
  finally
    tempList1.Free;
    ProgressBar1.Position := 0;
    ProgressBar2.Position := 0;
  end;
end;

//结果列表中双击

procedure TMainForm.RichEdit1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  tempLine, i: LongInt;
  tempStr1, tempStr2: string;
begin
  if (button = mbLeft) then
  begin
    if ABGetDateTimeSpan_Float(now, tempBegDown, tuMilliSeconds) < 500 then
    begin
      tempStr1 := RichEdit1.lines[RichEdit1.CaretPos.y];
      i := RichEdit1.CaretPos.y;
      while ABLeftStr(trim(RichEdit1.Lines[i]), 2) = ('第') do
      begin
        i := i - 1;
      end;
      tempFileName := RichEdit1.Lines[i];
      if ABCheckFileExists(tempFileName) then
      begin
        RichEdit2.Clear;
        RichEdit2.Text := ABReadTxt(RichEdit1.Lines[i]);
        //RichEdit2.Lines.LoadFromFile(RichEdit1.Lines[i]);
        tempLine := StrToIntDef(ABGetItemValue(tempStr1, ('第'), ('行')), 1) - 1;
        RichEdit2.SelStart := RichEdit2.Perform(EM_LINEINDEX, tempLine, 0);
        RichEdit2.Perform(EM_LINESCROLL, 0, tempLine - RichEdit2.Perform(EM_GETFIRSTVISIBLELINE, 0, 0));
        PageControlWork.ActivePageIndex := 1;
        if not CheckBox1.checked then
          ToolButton6Click(nil);

        if CheckBox9.checked then
        begin
          //ABOpenWith(tempFileName);
          if (CheckBox10.checked) and
            (AnsiCompareText(ABGetFileExt(tempFileName), 'dfm') = 0) then
            tempStr2 := ABGetFilepath(tempFileName) + ABGetFilename(tempFileName) + '.pas'
          else
            tempStr2 := tempFileName;

          ShellExecute(0, 'open', pchar(tempStr2), nil, nil, sw_shownormal);
        end;
      end;
    end
    else
    begin
      tempBegDown := Now;
    end;
  end;
end;

procedure TMainForm.RichEdit2MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  Label8.Caption := ('行') + ' ' + IntToStr(TRichEdit(Sender).CaretPos.Y + 1);
  Label9.Caption := ('列') + ' ' + IntToStr(TRichEdit(Sender).CaretPos.X + 1);
end;

//查找下一个

procedure TMainForm.ToolButton6Click(Sender: TObject);
var
  tempFindStart: LongInt;
  tempStr1: string;
begin
  if Memo2.Text <> '' then
    tempStr1 := Memo2.Text;

  if tempStr1 <> EmptyStr then
  begin
    if RichEdit2.SelText <> '' then
      RichEdit2.SelStart := RichEdit2.SelStart + Length(RichEdit2.SelText);

    if CheckBox3.Checked then
      tempFindStart := RichEdit2.FindText(tempStr1, RichEdit2.SelStart, RichEdit2.GetTextLen, [stWholeWord])
    else
      tempFindStart := RichEdit2.FindText(tempStr1, RichEdit2.SelStart, RichEdit2.GetTextLen, []);
    if tempFindStart <> -1 then
    begin
      with RichEdit2 do
      begin
        SetFocus;
        SelStart := tempFindStart;
        SelLength := Length(tempStr1);
      end;
    end
    else
      RichEdit2.SelStart := 0;
  end;
end;

//剪切

procedure TMainForm.ToolButton3Click(Sender: TObject);
begin
  if RichEdit2.SelText <> EmptyStr then
    RichEdit2.SelText := EmptyStr;
end;

//保存

procedure TMainForm.ToolButton2Click(Sender: TObject);
begin
  if ABCheckFileExists(tempFileName) then
  begin
    if ABGetFileAttr(tempFileName) and SysUtils.faReadOnly = SysUtils.faReadOnly then
      ABSetFileAttr(tempFileName, SysUtils.faReadOnly, ftDelete);

    if CheckBox7.Checked then
      ABCopyFile(tempFileName, ABBackPath + ABGetFilename(tempFileName, false));

    RichEdit2.PlainText := True;
    try
      RichEdit2.Lines.SaveToFile(tempFileName);
    finally
      RichEdit2.PlainText := false;
    end;
  end;
end;

//打开

procedure TMainForm.ToolButton1Click(Sender: TObject);
var
  tempStr1: string;
begin
  if ABCheckFileExists(tempFileName) then
  begin
    tempStr1 := ABSelectFile(ABGetFilepath(tempFileName), ABGetFilename(tempFileName, false));
    RichEdit2.Text := ABReadTxt(tempStr1);
  end;
end;

//替换下一个

procedure TMainForm.ToolButton5Click(Sender: TObject);
begin
  Replace;
end;

function TMainForm.Replace: LongInt;
var
  tempFindStart: LongInt;
  tempPass: boolean;
  tempStr1: string;
begin
  Result := 0;
  if (not CheckBox5.Checked) and (not CheckBox6.Checked) then
    exit;
  if Memo2.Text <> '' then
    tempStr1 := Memo2.Text;

  if tempStr1 <> EmptyStr then
  begin
    if CheckBox3.Checked then
      tempFindStart := RichEdit2.FindText(tempStr1, RichEdit2.SelStart, RichEdit2.GetTextLen, [stWholeWord])
    else
      tempFindStart := RichEdit2.FindText(tempStr1, RichEdit2.SelStart, RichEdit2.GetTextLen, []);
    if tempFindStart <> -1 then
    begin
      with RichEdit2 do
      begin
        SetFocus;
        SelStart := tempFindStart;
        SelLength := Length(tempStr1);

        tempPass := true;
        if (CheckBox5.Checked) and (not CheckBox6.Checked) then
        begin
          tempPass := CaretPos.x <> Length(StringReplace(Lines[CaretPos.y], ABWrapStr, '', []));
        end
        else if (not CheckBox5.Checked) and (CheckBox6.Checked) then
        begin

          tempPass := CaretPos.x = Length(StringReplace(Lines[CaretPos.y], ABWrapStr, '', []));

        end;

        if tempPass then
          SelText := Memo3.Text
        else
          SelStart := SelStart + Length(tempStr1);
      end;
    end
    else
      RichEdit2.SelStart := 0;

    result := RichEdit2.SelStart;
  end;
end;


initialization

end.

