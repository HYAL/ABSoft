unit Main;

interface

uses
  ABPubCharacterCodingU,
  ABPubInPutStrU,
  ABPubMessageU,
  ABPubUserU,
  ABPubDBU,
  ABPubVarU,
  ABPubFuncU,
  ABPubFormU,
  ABPubConstU,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,
  StdCtrls,ExtCtrls,CheckLst,DB,ADODB,Grids,DBGrids;

type
  TMainForm = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    RadioGroup1: TRadioGroup;
    CheckBox1: TCheckBox;
    Label1: TLabel;
    Memo2: TMemo;
    Memo1: TMemo;
    ADOConnection1: TADOConnection;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Memo1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.Button1Click(Sender: TObject);
var
  tempStr:string;
begin
  tempStr:=Trim(Memo1.Text);
  if tempStr=EmptyStr  then
    Exit;

  if not ABHaveChinese(tempStr) then
    RadioGroup1.ItemIndex:=1
  else if RadioGroup1.ItemIndex=1 then
    RadioGroup1.ItemIndex:=0;

  case RadioGroup1.ItemIndex of
    0:
    begin
      DBGrid1.Visible:=true;
      Memo2.Visible:=false;
      ADOQuery1.Filter:='explain  like '+ABQuotedStr( '*'+tempStr+'*');
      if ABDatasetIsEmpty(ADOQuery1) then
      begin
        ShowMessage('在字典中未查到');
      end;
    end;
    1:
    begin
      DBGrid1.Visible:=true;
      Memo2.Visible:=false;
      ADOQuery1.Filter:='word='+ABQuotedStr(tempStr);
      if ABDatasetIsEmpty(ADOQuery1) then
      begin
        ShowMessage('在字典中未查到');
      end;
    end;
  end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  ADOConnection1.Connected:=false;
  ADOConnection1.ConnectionString:=
    'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+ABSoftSetPath + 'Data.mdb;Persist Security Info=False';
  ADOConnection1.Connected:=true;
  ADOQuery1.Active:=false;
  ADOQuery1.Active:=true;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  ADOConnection1.Connected:=false;
  ADOQuery1.Active:=false;
end;

procedure TMainForm.Memo1KeyPress(Sender: TObject; var Key: Char);
begin
  if Key=#13 then
  begin
    Button1Click(nil);
    key:=#0;
  end;
end;

end.
