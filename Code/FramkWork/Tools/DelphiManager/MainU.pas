unit MainU;

interface

uses
  ABPubDownButtonU,
  abpubmessageU,
  ABPubMemoU,
  ABPubFormU,
  ABPubInPutStrU,
  ABPubVarU,
  ABPubConstU,
  ABPubFuncU,

  ABThirdFuncU,

  PathSetU,clipbrd,unitResFile,unitResourceVersionInfo,types,TypInfo,
  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,Menus,
  StdCtrls,ComCtrls,ExtCtrls,CheckLst,Buttons,Spin,NativeXml;

type
  TMainForm = class(TABPubForm)
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    pnl1: TPanel;
    lbl2: TLabel;
    lbl3: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Button11: TButton;
    Edit3: TEdit;
    Edit4: TEdit;
    pnl4: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    pnl6: TPanel;
    pnl2: TPanel;
    Button16: TButton;
    PopupMenu2: TPopupMenu;
    N4: TMenuItem;
    dpk1: TMenuItem;
    dpr1: TMenuItem;
    dproj1: TMenuItem;
    N5: TMenuItem;
    Splitter1: TSplitter;
    N1: TMenuItem;
    RadioGroup1: TRadioGroup;
    ABDownButton3: TABDownButton;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    CheckBox1: TCheckBox;
    Memo1: TMemo;
    ListBox1: TListBox;
    StatusBar1: TStatusBar;
    ABDownButton1: TABDownButton;
    ABDownButton2: TABDownButton;
    PopupMenu3: TPopupMenu;
    N2: TMenuItem;
    N3: TMenuItem;
    Panel3: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    SpinEdit3: TSpinEdit;
    SpinEdit4: TSpinEdit;
    ABDownButton4: TABDownButton;
    ABDownButton5: TABDownButton;
    ABDownButton6: TABDownButton;
    ABDownButton7: TABDownButton;
    ABDownButton8: TABDownButton;
    PopupMenu5: TPopupMenu;
    N11: TMenuItem;
    ABDownButton9: TABDownButton;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    ProgressBar1: TProgressBar;
    GroupBox3: TGroupBox;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    ABDownButton10: TABDownButton;
    Memo2: TMemo;
    PopupMenu6: TPopupMenu;
    MenuItem1: TMenuItem;
    GroupBox5: TGroupBox;
    GroupBox4: TGroupBox;
    ListBox2: TListBox;
    Splitter2: TSplitter;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit5: TEdit;
    Label4: TLabel;
    TabSheet3: TTabSheet;
    Label9: TLabel;
    Memo3: TMemo;
    Panel1: TPanel;
    Button1: TButton;
    Button4: TButton;
    RadioGroup2: TRadioGroup;
    pas1: TMenuItem;
    N6: TMenuItem;
    Panel5: TPanel;
    ToolButton4: TSpeedButton;
    ToolButton3: TSpeedButton;
    ToolButton2: TSpeedButton;
    ToolButton1: TSpeedButton;
    TabSheet4: TTabSheet;
    Panel6: TPanel;
    Button2: TButton;
    Button3: TButton;
    Label10: TLabel;
    Memo4: TMemo;
    RadioGroup3: TRadioGroup;
    Button5: TButton;
    TabSheet5: TTabSheet;
    Label12: TLabel;
    Panel7: TPanel;
    Button6: TButton;
    Button7: TButton;
    RadioGroup4: TRadioGroup;
    Button8: TButton;
    Label13: TLabel;
    RadioGroup5: TRadioGroup;
    PageControl2: TPageControl;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    TabSheet9: TTabSheet;
    TabSheet10: TTabSheet;
    Memo6: TMemo;
    Panel8: TPanel;
    CheckBox5: TCheckBox;
    Memo7: TMemo;
    Memo8: TMemo;
    Memo9: TMemo;
    Memo10: TMemo;
    Panel9: TPanel;
    CheckBox6: TCheckBox;
    Panel10: TPanel;
    CheckBox7: TCheckBox;
    Panel11: TPanel;
    CheckBox8: TCheckBox;
    Panel12: TPanel;
    CheckBox9: TCheckBox;
    Panel13: TPanel;
    Memo5: TMemo;
    Panel14: TPanel;
    Memo11: TMemo;
    Label14: TLabel;
    ABDownButton11: TABDownButton;
    ABDownButton12: TABDownButton;
    ABDownButton13: TABDownButton;
    ABDownButton14: TABDownButton;
    ABDownButton15: TABDownButton;
    ABDownButton16: TABDownButton;
    ABDownButton17: TABDownButton;
    TabSheet11: TTabSheet;
    Button9: TButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure dpk1Click(Sender: TObject);
    procedure dpr1Click(Sender: TObject);
    procedure dproj1Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ABDownButton5Click(Sender: TObject);
    procedure ABDownButton6Click(Sender: TObject);
    procedure ABDownButton7Click(Sender: TObject);
    procedure ABDownButton8Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure ABDownButton9Click(Sender: TObject);
    procedure ABDownButton4Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure Button16Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure pas1Click(Sender: TObject);
    procedure Memo3KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure N6Click(Sender: TObject);
    procedure ListBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure ABDownButton10Click(Sender: TObject);
    procedure ABDownButton12Click(Sender: TObject);
    procedure ABDownButton11Click(Sender: TObject);
    procedure ABDownButton13Click(Sender: TObject);
    procedure ABDownButton16Click(Sender: TObject);
    procedure ABDownButton14Click(Sender: TObject);
    procedure ABDownButton15Click(Sender: TObject);
    procedure ABDownButton1Click(Sender: TObject);
    procedure ABDownButton17Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
  private
    procedure AddProject(aFileType: array of string;aFilterExt:boolean);
    procedure RefreshSum;
    procedure Down(aStep: Integer);
    procedure Up(aStep: Integer);
    procedure ClearNamedian(aOnlySelect: boolean);
    procedure TrimUseName(aOnlySelect: boolean);
    procedure buildPorjects(aString: Tstrings);

    procedure AfterCompilerNotifyEvent(Sender: TObject;var aContinue:Boolean);
    { Private declarations }
  public
    { Public declarations }
  end;

var         
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.Button11Click(Sender: TObject);
begin
  if not ABLoadGroupItemToStrings(Edit4.Text,ListBox1.Items) then
  begin
    if ListBox1.Items.IndexOf(Edit4.Text)<0 then
    begin
      ListBox1.Items.Add(Edit4.Text);
    end;
  end;


  RefreshSum;
end;

procedure TMainForm.RefreshSum;
begin
  StatusBar1.Panels[0].Text:='共有文件:'+inttostr(ListBox1.Items.Count)+('个');
end;

procedure TMainForm.AddProject(aFileType:array of string;aFilterExt:boolean);
var
  i: Integer;
  tempList:TStrings;
begin
  if ABCheckDirExists(Edit3.Text) then
  begin
    tempList := TStringList.Create;
    try
      ABGetFileList(tempList,Edit3.Text,aFileType,-1,aFilterExt,ProgressBar1);
      ProgressBar1.Min:=0;
      ProgressBar1.Max:=tempList.Count;
      for i:=0 to tempList.Count-1 do
      begin
        ProgressBar1.Position:=ProgressBar1.Position+1;
        Application.ProcessMessages;
        if ListBox1.Items.IndexOf(tempList.Strings[i])<0 then
        begin
          ListBox1.Items.Add(tempList.Strings[i]);
        end;
      end;
      RefreshSum;
    finally
      tempList.Free;
      ProgressBar1.Position:=0;
    end;
  end;
end;

procedure TMainForm.AfterCompilerNotifyEvent(Sender: TObject;
  var aContinue: Boolean);
begin
  aContinue:=ABDownButton17.Enabled;
end;

procedure TMainForm.dpk1Click(Sender: TObject);
begin
  AddProject(['dpk'],true);
end;

procedure TMainForm.dpr1Click(Sender: TObject);
begin
  AddProject(['dpr'],true);
end;

procedure TMainForm.dproj1Click(Sender: TObject);
begin
  AddProject(['dproj'],true);
end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose:=not Button4.Enabled;
  if not CanClose then
  begin
    ShowMessage('请先停止检测');
  end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  RefreshSum;
  SendMessage(ListBox1.Handle,LB_SetHorizontalExtent,5000, longint(0));
end;

procedure TMainForm.N11Click(Sender: TObject);
var
  tempStrings:TStrings;
begin
  if ListBox1.ItemIndex>=0 then
  begin
    tempStrings := TStringList.Create;
    try
      ABGetDelphiProjectinfoToStrings(ListBox1.Items[ListBox1.ItemIndex],tempStrings);

      CheckBox2.Checked:=ABStrToBool(tempStrings.Values['bstIncludeVerInfo']);
      CheckBox3.Checked:=ABStrToBool(tempStrings.Values['bstBuildIncVersion']);
      SpinEdit1.Text:=tempStrings.Values['bstVersion0'];
      SpinEdit2.Text:=tempStrings.Values['bstVersion1'];
      SpinEdit3.Text:=tempStrings.Values['bstVersion2'];
      SpinEdit4.Text:=tempStrings.Values['bstVersion3'];
      Edit6.Text:=tempStrings.Values['bstBplPath'];
      Edit7.Text:=tempStrings.Values['bstDcpPath'];
      Edit8.Text:=tempStrings.Values['bstExePath'];
      CheckBox1.Checked:=ABStrToBool(tempStrings.Values['bstBuildWithPackage']);
      Memo1.Text:=tempStrings.Values['bstBuildWithPackageList'];

    finally
      tempStrings.Free;
    end;
  end;
end;

procedure TMainForm.N6Click(Sender: TObject);
var
  tempStrings:TStrings;
  i:LongInt;
  tempMsg,
  tempSumMsg:string;
begin
  tempStrings := TStringList.Create;
  try
    for I := 0 to ListBox1.Count-1 do
    begin
      ABGetDelphiProjectinfoToStrings(ListBox1.Items[i],tempStrings);

      tempMsg:=ABFillStr(ABGetFilename(ListBox1.Items[i])+'=',40,' ',axdRight);

      ABAddstr(tempMsg,ABFillStr('IncludeVerInfo='+tempStrings.Values['bstIncludeVerInfo'],25,' ',axdRight));
      ABAddstr(tempMsg,ABFillStr('BuildIncVersion='+tempStrings.Values['bstBuildIncVersion'],25,' ',axdRight));
      ABAddstr(tempMsg,ABFillStr('Version0='+tempStrings.Values['bstVersion0'],25,' ',axdRight));
      ABAddstr(tempMsg,ABFillStr('Version1='+tempStrings.Values['bstVersion1'],25,' ',axdRight));
      ABAddstr(tempMsg,ABFillStr('Version2='+tempStrings.Values['bstVersion2'],25,' ',axdRight));
      ABAddstr(tempMsg,ABFillStr('Version3='+tempStrings.Values['bstVersion3'],25,' ',axdRight));
      ABAddstr(tempMsg,ABFillStr('BplPath='+tempStrings.Values['bstBplPath'],100,' ',axdRight));
      ABAddstr(tempMsg,ABFillStr('DcpPath='+tempStrings.Values['bstDcpPath'],100,' ',axdRight));
      ABAddstr(tempMsg,ABFillStr('ExePath='+tempStrings.Values['bstExePath'],100,' ',axdRight));
      ABAddstr(tempMsg,ABFillStr('BuildWithPackage='+tempStrings.Values['bstBuildWithPackage'],25,' ',axdRight));

      //ABAddstr(tempMsg,'bstIncludeVerInfo=',tempStrings.Values['bstBuildWithPackageList']);
      ABAddstr(tempSumMsg,tempMsg,ABEnterWrapStr);
    end;

    ABShowEditMemo(tempSumMsg,false,'文件配制');
  finally
    tempStrings.Free;
  end;
end;

procedure TMainForm.ABDownButton10Click(Sender: TObject);
var
  tempStrings:TStrings;
begin
  tempStrings := TStringList.Create;
  try
    tempStrings.Add(ListBox1.Items[ListBox1.ItemIndex]);
    buildPorjects(tempStrings);
  finally
    tempStrings.Free;
  end;
end;

procedure TMainForm.ABDownButton11Click(Sender: TObject);
var
  tempStrings:TStrings;
begin
  if ListBox1.ItemIndex>=0 then
  begin
    tempStrings := TStringList.Create;
    try
      ABListBoxCopyStrings(ListBox1,ListBox1.ItemIndex,ListBox1.count-ListBox1.ItemIndex,tempStrings);
      buildPorjects(tempStrings);
    finally
      tempStrings.Free;
    end;
  end;
end;

procedure TMainForm.ABDownButton12Click(Sender: TObject);
var
  tempStrings:TStrings;
begin
  tempStrings := TStringList.Create;
  try
    ABListBoxSelectsCopyStrings(ListBox1,tempStrings);
    buildPorjects(tempStrings);
  finally
    tempStrings.Free;
  end;
end;

procedure TMainForm.ABDownButton13Click(Sender: TObject);
begin
  buildPorjects(ListBox1.Items);
end;

procedure TMainForm.ABDownButton14Click(Sender: TObject);
var
  i:LongInt;
begin
  for i:=ListBox1.Count-1 downto 0 do
  begin
    if ListBox1.Selected[i] then
    begin
      ListBox1.Items.Delete(i);
    end;
  end;
  RefreshSum;

end;

procedure TMainForm.ABDownButton15Click(Sender: TObject);
begin
  ListBox1.Clear;
  RefreshSum;
end;

procedure TMainForm.ABDownButton16Click(Sender: TObject);
var
  i:LongInt;
begin
  for i:=ListBox1.Items.Count-1 downto 0 do
  begin
    if not ABCheckFileExists(ListBox1.Items[i]) then
    begin
      ListBox1.Items.Delete(i);
    end;
  end;
  RefreshSum;
end;

procedure TMainForm.ABDownButton17Click(Sender: TObject);
begin
  ABDownButton17.Enabled:=false;
end;

procedure TMainForm.ABDownButton1Click(Sender: TObject);
begin
  if ListBox1.ItemIndex>=0 then
  begin
    ListBox1.Items.Delete(ListBox1.ItemIndex);
  end;
end;

procedure TMainForm.ABDownButton4Click(Sender: TObject);
begin
  ABSetDelphiProjectinfos(ListBox1.Items,
                          bstIncludeVerInfo,[IntToStr(ABBoolToInt(CheckBox2.Checked))]);
  ABSetDelphiProjectinfos(ListBox1.Items,
                          bstBuildIncVersion,[IntToStr(ABBoolToInt(CheckBox3.Checked))]);
  ABSetDelphiProjectinfos(ListBox1.Items,
                          bstVersion,
                          [IntToStr(SpinEdit1.Value),IntToStr(SpinEdit2.Value),
                          IntToStr(SpinEdit3.Value),IntToStr(SpinEdit4.Value)]);
end;

procedure TMainForm.ABDownButton5Click(Sender: TObject);
begin
  ABSetDelphiProjectinfos(ListBox1.Items,
                          bstBplPath,
                          [Edit6.Text],
                          ProgressBar1);
end;

procedure TMainForm.ABDownButton6Click(Sender: TObject);
begin
  ABSetDelphiProjectinfos(ListBox1.Items,
                          bstDcpPath,
                          [Edit7.Text],
                          ProgressBar1);
end;

procedure TMainForm.ABDownButton7Click(Sender: TObject);
begin
  ABSetDelphiProjectinfos(ListBox1.Items,
                          bstExePath,
                          [Edit8.Text],
                          ProgressBar1);
end;

procedure TMainForm.ABDownButton8Click(Sender: TObject);
begin
  ABSetDelphiProjectinfos(ListBox1.Items,
                          bstBuildWithPackage,
                          [ABIIF(CheckBox1.Checked,'1','0')],
                          ProgressBar1);
end;

procedure TMainForm.ABDownButton9Click(Sender: TObject);
begin
  ABSetDelphiProjectinfos(ListBox1.Items,
                          bstBuildWithPackageList,
                          [Memo1.Text],
                          ProgressBar1);
end;

procedure TMainForm.N1Click(Sender: TObject);
var
  tempStr:string;
begin
  if ABInPutStr('过滤条件',tempStr,'请输入过滤条件(如*XE2.dpk),多个时逗号分隔') then
  begin
    if tempStr<>EmptyStr then
      AddProject(ABStrToStringArray(tempStr),false);
  end;
end;

procedure TMainForm.N2Click(Sender: TObject);
var
  tempStrings:TStrings;
begin
  tempStrings := TStringList.Create;
  try
    ABListBoxSelectsCopyStrings(ListBox1,tempStrings);
    ABSaveToD7Group(tempStrings,'');
  finally
    tempStrings.Free;
  end;
end;

procedure TMainForm.N3Click(Sender: TObject);
begin
  ABSaveToD7Group(ListBox1.Items,'');
end;

procedure TMainForm.N4Click(Sender: TObject);
begin
  AddProject(['pas','dpk','dpr','dproj'],true);
end;

procedure TMainForm.N5Click(Sender: TObject);
var
  tempStr:string;
begin
  if ABInPutStr('扩展名',tempStr,'请输入扩展名(如dpk,dpr),多个时逗号分隔') then
  begin
    if tempStr<>EmptyStr then
      AddProject(ABStrToStringArray(tempStr),true);
  end;
end;

procedure TMainForm.pas1Click(Sender: TObject);
begin
  AddProject(['pas'],true);
end;

procedure TMainForm.SpeedButton1Click(Sender: TObject);
var
  tempStr:string;
begin
  tempStr:= ABSelectDirectory(Edit3.Text);
  if tempStr<>EmptyStr then
  begin
    Edit3.Text:=tempStr;
  end;
end;

procedure TMainForm.SpeedButton2Click(Sender: TObject);
var
  tempStr:string;
begin
  tempStr := ABSelectFile('',Edit4.Text,'Delphi Group|*.bpg;*.groupproj|(Delphi7) bpg|*.bpg|(D2009~DXE3) groupproj|*.groupproj|All|*.*');
  if tempStr<>EmptyStr then
  begin
    Edit4.Text:=tempStr;
  end;
end;

procedure TMainForm.SpeedButton3Click(Sender: TObject);
var
  tempStr:string;
begin
  tempStr:= ABSelectDirectory(Edit1.Text);
  if tempStr<>EmptyStr then
  begin
    Edit1.Text:=tempStr;
  end;
end;

procedure TMainForm.SpeedButton4Click(Sender: TObject);
var
  tempStr:string;
begin
  tempStr:= ABSelectDirectory(Edit2.Text);
  if tempStr<>EmptyStr then
  begin
    Edit2.Text:=tempStr;
  end;
end;

procedure TMainForm.SpeedButton5Click(Sender: TObject);
var
  tempStr:string;
begin
  tempStr:= ABSelectDirectory(Edit5.Text);
  if tempStr<>EmptyStr then
  begin
    Edit5.Text:=tempStr;
  end;
end;

procedure TMainForm.SpeedButton6Click(Sender: TObject);
var
  tempStr:string;
begin
  tempStr:= ABSelectDirectory(Edit6.Text);
  if tempStr<>EmptyStr then
  begin
    Edit6.Text:=tempStr;
  end;
end;

procedure TMainForm.SpeedButton7Click(Sender: TObject);
var
  tempStr:string;
begin
  tempStr:= ABSelectDirectory(Edit7.Text);
  if tempStr<>EmptyStr then
  begin
    Edit7.Text:=tempStr;
  end;
end;

procedure TMainForm.SpeedButton8Click(Sender: TObject);
var
  tempStr:string;
begin
  tempStr:= ABSelectDirectory(Edit8.Text);
  if tempStr<>EmptyStr then
  begin
    Edit8.Text:=tempStr;
  end;
end;

procedure TMainForm.Down(aStep:LongInt);
var
  i:longint;
begin
  if ListBox1.MultiSelect then
  begin
    for I :=ListBox1.Count - 1 downto 0 do
    begin
      if ListBox1.Selected[i] then
      begin
        ABListBoxDown(ListBox1,i,aStep);
      end;
    end;
  end
  else
  begin
    ABListBoxDown(ListBox1,ListBox1.ItemIndex,aStep);
  end;
end;

procedure TMainForm.Up(aStep:LongInt);
var
  i:longint;
begin
  if ListBox1.MultiSelect then
  begin
    for I :=0 to ListBox1.Count - 1  do
    begin
      if ListBox1.Selected[i] then
      begin
        ABListBoxUp(ListBox1,i,aStep);
      end;
    end;
  end
  else
  begin
    ABListBoxUp(ListBox1,ListBox1.ItemIndex,aStep);
  end;
end;

procedure TMainForm.ToolButton1Click(Sender: TObject);
begin
  Up(1);
end;

procedure TMainForm.ToolButton2Click(Sender: TObject);
begin
  Down(1);
end;

procedure TMainForm.ToolButton3Click(Sender: TObject);
begin
  Up(10);
end;

procedure TMainForm.ToolButton4Click(Sender: TObject);
begin
  Down(10);
end;

procedure TMainForm.ListBox1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Shift=[ssCtrl]) then
  begin
    if (Key=VK_DOWN) then
    begin
      ToolButton2Click(nil);
    end
    else if (Key=VK_UP) then
    begin
      ToolButton1Click(nil);
    end
    else if (Key=67) then
    begin
      clipboard.AsText := ListBox1.Items.Text;
    end;
    Key:=0;
  end;
end;

procedure TMainForm.buildPorjects(aString:Tstrings);
var
  tempVersion:TABDelphiVersion;
  i:LongInt;
begin
  tempVersion:=TABDelphiVersion(RadioGroup1.ItemIndex+1);
  if Edit1.text=emptystr then
  begin
    Edit1.text:=ABReplaceEnvironment(ABGetDelphiBplPath(tempVersion));
  end;
  if Edit2.text=emptystr then
  begin
    Edit2.text:=ABReplaceEnvironment(ABGetDelphiDcpPath(tempVersion));
  end;

  Button16.Enabled:=false;
  ABDownButton10.Enabled:=false;
  ABDownButton11.Enabled:=false;
  ABDownButton12.Enabled:=false;
  ABDownButton13.Enabled:=false;
  ABDownButton17.Enabled:=true;
  try
    for I := 0 to aString.count-1 do
    begin
      ABCreateEmptyRes(aString[i]);
    end;

    ABBuildProjects(tempVersion,
                    ABSoftSetPath+'Compilers\'+GetEnumName(Typeinfo(TABDelphiVersion),ord(tempVersion))+'\DCC32.EXE',
                    aString,
                    Memo2.Lines,
                    ListBox2.Items,

                    '',
                    Edit2.text,
                    Edit1.text,
                    Edit5.text,
                    PathSetForm.lst1.Items,
                    False,
                    False,

                    AfterCompilerNotifyEvent,
                    ProgressBar1);
  finally
    Button16.Enabled:=true;
    ABDownButton10.Enabled:=true;
    ABDownButton11.Enabled:=true;
    ABDownButton12.Enabled:=true;
    ABDownButton13.Enabled:=true;
    ABDownButton17.Enabled:=false;
  end;
end;

procedure TMainForm.Memo3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Shift=[ssCtrl]) then
  begin
    if  (Key=65) then
    begin
      TMemo(Sender).SelectAll;
    end
  end;
end;

procedure TMainForm.MenuItem1Click(Sender: TObject);
var
  tempStrings:TStrings;
begin
  tempStrings := TStringList.Create;
  try
    tempStrings.AddStrings(ListBox2.Items);
    buildPorjects(tempStrings);
  finally
    tempStrings.Free;
  end;
end;

procedure TMainForm.Button16Click(Sender: TObject);
begin
  PathSetForm.FDelphiVersion:=TABDelphiVersion(RadioGroup1.ItemIndex+1);
  PathSetForm.ShowModal;
end;

//检测单元循环引用
procedure TMainForm.Button1Click(Sender: TObject);

var
  tempStringsFileNames,
  tempStrings:TStrings;
  tempStrings_DO:TStrings;
  i:longint;
  tempCycleRemark:string;
  tempAllRemark:string;
  procedure CheckCycle(aTopUnitName,aCurUnitName,aPasFileName:string;var aCycleRemark:string;var aAllRemark:string);
  var
    tempusesStr,
    tempusesStrs:string;
    j,k,l:longint;
  begin
    if tempStrings_DO.IndexOf(aCurUnitName)>=0 then
      exit;
    if ABPos(','+aCurUnitName+',',','+aAllRemark+',')<=0 then
    begin
      if aTopUnitName<>aCurUnitName then
        aAllRemark:=aAllRemark+','+aCurUnitName;
    end
    else
    begin
      exit;
    end;

    l:=tempStringsFileNames.IndexOfName(aCurUnitName);
    if l>=0 then
    begin
      tempusesStrs:=trim(ABGetItemValue(TStrings(tempStringsFileNames.Objects[l]).Text,'uses','',';'));
      tempusesStrs:=ABStringReplace(tempusesStrs,ABEnterWrapStr,'');
      tempusesStrs:=ABStringReplace(tempusesStrs,ABEnterStr,'');
    end
    else
    begin
      tempusesStrs:=EmptyStr;
    end;

    for j := 1 to ABGetSpaceStrCount(tempusesStrs, ',') do
    begin
      if not Button4.Enabled then
        break;

      Application.ProcessMessages;
      tempusesStr := trim(ABGetSpaceStr(tempusesStrs, j, ','));
      k:=tempStringsFileNames.IndexOfName(tempusesStr);
      if k>=0 then
      begin
        if AnsiCompareText(aTopUnitName,tempusesStr)=0 then
        begin
          aCycleRemark:='['+aTopUnitName+']单元与['+aCurUnitName+']中的['+tempusesStr+']循环引用';
          Break;
        end
        else
        begin
          CheckCycle(aTopUnitName,tempusesStr,tempStringsFileNames.ValueFromIndex[k],aCycleRemark,aAllRemark);
        end;
      end;
    end;
  end;
begin
  Memo3.Clear;
  tempStringsFileNames := TStringList.Create;
  tempStrings_DO       := TStringList.Create;
  ProgressBar1.Max:=ListBox1.Items.Count;
  ProgressBar1.MIN:=0;
  Button1.Enabled:=false;
  Button4.Enabled:=true;
  try
    //取出所有的文件名称与内容存入列表tempStringsFileNames中
    for i := 0 to ListBox1.Items.Count - 1 do
    begin
      if not abcheckFileExists(ListBox1.Items[i] ) then
        Continue;

      tempStrings:=TStringList.Create;
      tempStrings.LoadFromFile(ListBox1.Items[i]);
      tempStringsFileNames.AddObject(ABGetFilename(ListBox1.Items[i])+'='+ListBox1.Items[i],tempStrings);
    end;

    for i := 0 to tempStringsFileNames.Count - 1 do
    begin
      Application.ProcessMessages;
      ProgressBar1.Position:=i+1;
      if not Button4.Enabled then
        break;

      tempCycleRemark:=EmptyStr;
      tempAllRemark :=EmptyStr;
      //对每个文件进行检测
      CheckCycle(tempStringsFileNames.Names[i],tempStringsFileNames.Names[i],tempStringsFileNames.ValueFromIndex[i],tempCycleRemark,tempAllRemark);
      if tempCycleRemark<>EmptyStr then
      begin
        Memo3.Lines.Add(tempCycleRemark);
      end
      else
      begin
        tempStrings_DO.Add(tempStringsFileNames.Names[i]);
      end;
      if RadioGroup2.ItemIndex=1 then
      begin
        Memo3.Lines.Add('['+tempStringsFileNames.Names[i]+']引用到的单元:'+ABEnterWrapStr+tempAllRemark);
        Memo3.Lines.Add('-------------------------');
      end;
    end;

    for i := 0 to tempStringsFileNames.Count - 1 do
    begin
      tempStringsFileNames.Objects[i].Create;
    end;
  finally
    Button1.Enabled:=true;
    Button4.Enabled:=false;
    tempStringsFileNames.Free;
    tempStrings_DO.Free;
    ProgressBar1.Position:=0;
  end;
end;


procedure TMainForm.Button3Click(Sender: TObject);

begin
  Button3.Enabled:=false;
end;

procedure TMainForm.Button4Click(Sender: TObject);

begin
  Button4.Enabled:=false;
end;

//检测清除XE下单元的空间名
procedure TMainForm.ClearNamedian(aOnlySelect:boolean);
var
  i,j:longint;
  tempStrings:TStrings;
  tempClearRemark:string;

  tempusesStr,
  tempusesStrs_old,
  tempusesStrs,
  tempusesStrs_New:string;

  tempLineCount:longint;
begin
  Memo4.Clear;
  tempStrings := TStringList.Create;
  ProgressBar1.Max:=ListBox1.Items.Count;
  ProgressBar1.MIN:=0;
  Button2.Enabled:=false;
  Button3.Enabled:=true;
  try
    for i := 0 to ListBox1.Items.Count - 1 do
    begin
      Application.ProcessMessages;
      ProgressBar1.Position:=i+1;
      if not Button3.Enabled then
        break;

      if not abcheckFileExists(ListBox1.Items[i] ) then
        Continue;

      if (aOnlySelect) and (not ListBox1.Selected[i]) then
        Continue;

      tempLineCount:=1;
      tempStrings.LoadFromFile(ListBox1.Items[i]);
      tempusesStrs_old:=ABGetItemValue(tempStrings.Text,'uses','',';');
      tempusesStrs:=trim(tempusesStrs_old);
      tempusesStrs:=ABStringReplace(tempusesStrs,' ','');
      tempusesStrs:=ABStringReplace(tempusesStrs,ABEnterWrapStr,'');
      tempusesStrs:=ABStringReplace(tempusesStrs,ABEnterStr,'');
      tempClearRemark:=emptystr;
      tempusesStrs_New:=emptystr;

      if (pos('{',tempusesStrs)>0) or (pos('}',tempusesStrs)>0) or
         (pos('''',tempusesStrs)>0) or (pos('uses',tempusesStrs)>0) or (pos('//',tempusesStrs)>0) then
      Continue;

      for j := 1 to ABGetSpaceStrCount(tempusesStrs, ',') do
      begin
        Application.ProcessMessages;
        if not Button3.Enabled then
          break;

        tempusesStr:=ABGetSpaceStr(tempusesStrs, j, ',');
        tempusesStr:=trim(tempusesStr);
        tempusesStr:=ABStringReplace(tempusesStr,' ','');
        tempusesStr:=ABStringReplace(tempusesStr,ABEnterWrapStr,'');
        tempusesStr:=ABStringReplace(tempusesStr,ABEnterStr,'');

        if (tempusesStr<>emptystr)  then
        begin
          if (pos('.',tempusesStr)>0)  then
          begin
            abaddstr(tempClearRemark,tempusesStr+'->'+ABGetLeftRightStr(tempusesStr,axdright,'.',''));
            tempusesStr:=ABGetLeftRightStr(tempusesStr,axdright,'.','');
          end;
        end;
        if length(tempusesStrs_New)>=tempLineCount*80 then
        begin
          tempLineCount:=tempLineCount+1;
          tempusesStrs_New:=tempusesStrs_New+','+ABEnterWrapStr+tempusesStr;
        end
        else
          abaddstr(tempusesStrs_New,tempusesStr);
      end;

      if tempClearRemark<>emptystr then
      begin
        if RadioGroup3.ItemIndex=1 then
        begin
          tempStrings.Text:=Abstringreplace(tempStrings.Text,tempusesStrs_old,ABEnterWrapStr+'  '+tempusesStrs_New);
          tempStrings.SaveToFile(ListBox1.Items[i]);
          Memo4.Lines.Add('单元['+ListBox1.Items[i]+']空间名'+tempClearRemark+' 已清除.');
        end
        else
        begin
          Memo4.Lines.Add('单元['+ListBox1.Items[i]+']空间名'+tempClearRemark+' 已检测.');
        end;
      end;
    end;
  finally
    Button2.Enabled:=true;
    Button3.Enabled:=false;
    tempStrings.Free;
    ProgressBar1.Position:=0;
  end;
end;

procedure TMainForm.Button2Click(Sender: TObject);
begin
  if (RadioGroup3.ItemIndex=1) and
     (ABShow('确认清除所有文件中的单元空间名称吗?',[],['是','否'],1)=2) then
  begin
    exit;
  end;

  ClearNamedian(false);
end;

procedure TMainForm.Button5Click(Sender: TObject);
begin
  ClearNamedian(true);
end;

//引用单元整理
procedure TMainForm.TrimUseName(aOnlySelect:boolean);
var
  i:longint;
  tempStrings,
  tempStrings_Use:TStrings;
  tempStrings_Style:array[0..4] of TStrings;
  tempStrings_Row:array[0..4] of boolean;

  tempusesStrs_old,
  tempusesStrs,
  tempusesStrs_New,
  tempusesStrs_New1,
  tempusesStrs_New2:string;

  procedure DoTrimUse;
  var
    ii,i,j:LongInt;
    tempLevelStr:string;
  begin
    for ii := 0 to 4 do
    begin
      tempLevelStr:=emptystr;
      for i := 0 to tempStrings_Style[ii].Count-1  do
      begin
        for j := tempStrings_Use.Count-1 downto 0 do
        begin
          Application.ProcessMessages;
          if not Button7.Enabled then
            break;

          if (AnsiCompareText(tempStrings_Style[ii][i],tempStrings_Use[j])=0) or
             (((Pos('*',tempStrings_Style[ii][i])>0) or (Pos('?',tempStrings_Style[ii][i])>0)) and
               (ABLike(tempStrings_Use[j],tempStrings_Style[ii][i]))
               ) then
          begin
            if tempStrings_Row[ii] then
            begin
              abaddstr(tempLevelStr,'  '+tempStrings_Use[j]+',',ABEnterWrapStr);
            end
            else
            begin
              abaddstr(tempLevelStr,tempStrings_Use[j]+',','');
            end;
            tempStrings_Use.Delete(j);
          end;
        end;
      end;
      if tempLevelStr<>emptystr then
      begin
        if not tempStrings_Row[ii] then
          tempLevelStr:=ABCompartTrimStr(tempLevelStr,',',80,'  ');

        abaddstr(tempusesStrs_New1,tempLevelStr,ABEnterWrapStr+ABEnterWrapStr);
      end;
    end;
  end;
begin
  Memo5.Clear;
  Memo11.Clear;
  for i := 0 to 4 do
  begin
    tempStrings_Style[i]:=Tmemo(FindComponent('Memo'+inttostr(6+i))).Lines;
    tempStrings_Row[i]:= TCheckBox(FindComponent('CheckBox'+inttostr(5+i))).Checked;
  end;
  tempStrings := TStringList.Create;
  tempStrings_Use:= TStringList.Create;
  ProgressBar1.Max:=ListBox1.Items.Count;
  ProgressBar1.MIN:=0;
  Button6.Enabled:=false;
  Button7.Enabled:=true;
  try
    for i := 0 to ListBox1.Items.Count - 1 do
    begin
      Application.ProcessMessages;
      ProgressBar1.Position:=i+1;
      if not Button7.Enabled then
        break;

      if not abcheckFileExists(ListBox1.Items[i] ) then
        Continue;

      if (aOnlySelect) and (not ListBox1.Selected[i]) then
        Continue;

      tempStrings.LoadFromFile(ListBox1.Items[i]);
      tempusesStrs_old:=ABGetItemValue(tempStrings.Text,'uses','',';');
      tempusesStrs:=trim(tempusesStrs_old);
      tempusesStrs:=ABStringReplace(tempusesStrs,' ','');
      tempusesStrs:=ABStringReplace(tempusesStrs,ABEnterWrapStr,'');
      tempusesStrs:=ABStringReplace(tempusesStrs,ABEnterStr,'');
      tempusesStrs_New:=emptystr;
      tempusesStrs_New1:=emptystr;
      tempusesStrs_New2:=emptystr;

      if (pos('{',tempusesStrs)>0) or (pos('}',tempusesStrs)>0) or
         (pos('''',tempusesStrs)>0) or (pos('uses',tempusesStrs)>0) or (pos('//',tempusesStrs)>0) then
      begin
        Memo11.Lines.Add(ListBox1.Items[i]);
        Continue;
      end;

      ABStrsToStrings(tempusesStrs,',',tempStrings_Use);
      DoTrimUse;
      tempusesStrs_New2:=ABStringsToStrs(tempStrings_Use);
      tempusesStrs_New2:=ABCompartTrimStr(tempusesStrs_New2,',',80,'  ');

      if tempusesStrs_New2<>EmptyStr  then
      begin
        tempusesStrs_New:=tempusesStrs_New1+ABEnterWrapStr+ABEnterWrapStr+tempusesStrs_New2;
      end
      else
      begin
        tempusesStrs_New:=copy(tempusesStrs_New1,1,length(tempusesStrs_New1)-1);
      end;

      if (tempusesStrs_New<>EmptyStr) and (tempusesStrs_old<>tempusesStrs_New) then
      begin
        if RadioGroup4.ItemIndex=1 then
        begin
          tempStrings.Text:=Abstringreplace(tempStrings.Text,tempusesStrs_old,ABEnterWrapStr+tempusesStrs_New);
          tempStrings.SaveToFile(ListBox1.Items[i]);
          Memo5.Lines.Add('单元['+ListBox1.Items[i]+'] *******'+ABEnterWrapStr+
                          '整理前:'+ABEnterWrapStr+tempusesStrs_old+ABEnterWrapStr+
                          '整理后:'+ABEnterWrapStr+tempusesStrs_New
                          );
        end
        else
        begin
          Memo5.Lines.Add('单元['+ListBox1.Items[i]+'] *******'+ABEnterWrapStr+
                          '整理前:'+ABEnterWrapStr+tempusesStrs_old+ABEnterWrapStr+
                          '将整理为:'+ABEnterWrapStr+tempusesStrs_New
                          );
        end;
      end;
    end;
  finally
    Button6.Enabled:=true;
    Button7.Enabled:=false;
    tempStrings.Free;
    tempStrings_Use.Free;
    ProgressBar1.Position:=0;
  end;
end;

procedure TMainForm.Button6Click(Sender: TObject);
begin
  if (RadioGroup3.ItemIndex=1) and
     (ABShow('确认清除所有文件中的单元空间名称吗?',[],['是','否'],1)=2) then
  begin
    exit;
  end;

  TrimUseName(false);
end;

procedure TMainForm.Button8Click(Sender: TObject);
begin
  TrimUseName(true);
end;

procedure TMainForm.Button9Click(Sender: TObject);
var
  tempSumStr,
  tempFileName,
  tempFileExt,
  tempFileStr,
  tempItems,
  tempItem:string;
  j:LongInt;
begin
  tempFileName := ABSelectFile('',Edit4.Text,'dpr file|*.dpr|dpk file|*.dpk');
  if tempFileName<>EmptyStr then
  begin
    tempFileExt:=ABGetFileExt(tempFileName);
    if (AnsiCompareText(tempFileExt, 'dpr')=0) then
    begin
      tempFileStr:= ABGetItemValue(ABReadTxt(tempFileName),'uses','','begin');
      for j := 1 to ABGetSpaceStrCount(tempFileStr, ',') do
      begin
        tempItems := trim(ABGetSpaceStr(tempFileStr, j, ','));
        if tempItems <> EmptyStr then
        begin
          tempItem:= ABGetLeftRightStr(tempItems);
          if tempItem <> EmptyStr then
          begin
            ABAddstr(tempSumStr,tempItem,ABEnterWrapStr);
          end;
        end;
      end;
    end
    else if (AnsiCompareText(tempFileExt, 'dpk')=0) then
    begin
      tempFileStr:= ABGetItemValue(ABReadTxt(tempFileName),'contains','','end.');
      for j := 1 to ABGetSpaceStrCount(tempFileStr, ',') do
      begin
        tempItems := trim(ABGetSpaceStr(tempFileStr, j, ','));
        if tempItems <> EmptyStr then
        begin
          tempItem:= ABGetLeftRightStr(tempItems);
          if tempItem <> EmptyStr then
          begin
            ABAddstr(tempSumStr,tempItem,ABEnterWrapStr);
          end;
        end;
      end;
    end;
    if tempSumStr<>EmptyStr then
    begin
      ABShow('单元名如下：',tempSumStr);
    end;
  end;
end;

procedure TMainForm.Button7Click(Sender: TObject);
begin
  Button7.Enabled:=false;
end;

end.

