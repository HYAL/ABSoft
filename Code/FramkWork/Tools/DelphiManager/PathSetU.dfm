object PathSetForm: TPathSetForm
  Left = 283
  Top = 201
  Caption = #32534#35793#22120#26597#25214#36335#24452#37197#21046
  ClientHeight = 467
  ClientWidth = 727
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 16
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 727
    Height = 467
    Align = alClient
    Caption = 'Library Path'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    DesignSize = (
      727
      467)
    object lst1: TListBox
      Tag = 910
      Left = 3
      Top = 27
      Width = 718
      Height = 402
      Anchors = [akLeft, akTop, akRight, akBottom]
      MultiSelect = True
      TabOrder = 0
      OnClick = lst1Click
    end
    object Panel2: TPanel
      Left = 2
      Top = 430
      Width = 723
      Height = 35
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        723
        35)
      object edtLibraryDir: TEdit
        Left = 87
        Top = 6
        Width = 322
        Height = 24
        Anchors = [akLeft, akTop, akRight]
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ImeName = 'Chinese (Simplified) - Microsoft Pinyin'
        ParentFont = False
        TabOrder = 0
      end
      object Button3: TButton
        Left = 438
        Top = 6
        Width = 45
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #22686#21152
        TabOrder = 1
        OnClick = Button3Click
      end
      object Button4: TButton
        Left = 530
        Top = 6
        Width = 45
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #20462#25913
        TabOrder = 2
        OnClick = Button4Click
      end
      object Button5: TButton
        Left = 411
        Top = 6
        Width = 26
        Height = 25
        Anchors = [akTop, akRight]
        Caption = '...'
        TabOrder = 3
        OnClick = Button5Click
      end
      object Button1: TButton
        Left = 484
        Top = 6
        Width = 45
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #21024#38500
        TabOrder = 4
        OnClick = Button1Click
      end
      object Button22: TButton
        Left = 1
        Top = 6
        Width = 80
        Height = 25
        Caption = #23548#20837#40664#35748
        TabOrder = 5
        OnClick = Button22Click
      end
      object Button2: TButton
        Left = 577
        Top = 6
        Width = 142
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #21024#38500#19981#23384#22312#30340#30446#24405
        TabOrder = 6
        OnClick = Button2Click
      end
    end
  end
end
