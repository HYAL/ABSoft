object MainForm: TMainForm
  Left = 389
  Top = 153
  Caption = 'DELPHI '#24037#31243#25991#20214#31649#29702#24037#20855'('#25903#25345' D7'#12289'D2007'#12289'D2009'#12289'D2010'#12289'DXE1'#12289'DXE2'#12289'DXE3)'
  ClientHeight = 500
  ClientWidth = 937
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 937
    Height = 480
    Align = alClient
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 433
      Top = 1
      Height = 478
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 432
      Height = 478
      Align = alLeft
      Caption = #25991#20214#21015#34920
      TabOrder = 0
      object pnl1: TPanel
        Left = 2
        Top = 389
        Width = 428
        Height = 87
        Align = alBottom
        TabOrder = 0
        DesignSize = (
          428
          87)
        object lbl2: TLabel
          Left = 4
          Top = 11
          Width = 39
          Height = 16
          AutoSize = False
          Caption = #30446#24405':'
        end
        object lbl3: TLabel
          Left = 4
          Top = 37
          Width = 39
          Height = 16
          AutoSize = False
          Caption = #25991#20214':'
        end
        object SpeedButton1: TSpeedButton
          Left = 292
          Top = 8
          Width = 27
          Height = 22
          Anchors = [akTop, akRight]
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object SpeedButton2: TSpeedButton
          Left = 292
          Top = 34
          Width = 27
          Height = 22
          Anchors = [akTop, akRight]
          Caption = '...'
          OnClick = SpeedButton2Click
        end
        object Button11: TButton
          Left = 319
          Top = 33
          Width = 105
          Height = 24
          Anchors = [akTop, akRight]
          BiDiMode = bdLeftToRight
          Caption = #28155#21152#21333#20010#25991#20214
          ParentBiDiMode = False
          TabOrder = 0
          OnClick = Button11Click
        end
        object Edit3: TEdit
          Tag = 91
          Left = 52
          Top = 8
          Width = 237
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 1
        end
        object Edit4: TEdit
          Tag = 91
          Left = 52
          Top = 34
          Width = 237
          Height = 21
          Anchors = [akLeft, akTop, akRight]
          TabOrder = 2
        end
        object ABDownButton3: TABDownButton
          Left = 320
          Top = 7
          Width = 104
          Height = 24
          Anchors = [akTop, akRight]
          Caption = #28155#21152#25991#20214#21040#21015#34920
          DropDownMenu = PopupMenu2
          TabOrder = 3
        end
        object ABDownButton1: TABDownButton
          Left = 4
          Top = 59
          Width = 89
          Height = 24
          Anchors = [akLeft, akTop, akRight]
          Caption = #28165#38500#24403#21069#25991#20214
          TabOrder = 4
          OnClick = ABDownButton1Click
        end
        object ABDownButton2: TABDownButton
          Left = 320
          Top = 59
          Width = 104
          Height = 24
          Anchors = [akTop, akRight]
          Caption = #21015#34920#21478#23384#20026'D7'#32452
          DropDownMenu = PopupMenu3
          TabOrder = 5
        end
        object ABDownButton14: TABDownButton
          Left = 96
          Top = 59
          Width = 62
          Height = 24
          Anchors = [akLeft, akTop, akRight]
          Caption = #28165#38500#36873#25321
          TabOrder = 6
          OnClick = ABDownButton14Click
        end
        object ABDownButton15: TABDownButton
          Left = 252
          Top = 59
          Width = 67
          Height = 24
          Anchors = [akLeft, akTop, akRight]
          Caption = #28165#38500#25152#26377
          TabOrder = 7
          OnClick = ABDownButton15Click
        end
        object ABDownButton16: TABDownButton
          Left = 162
          Top = 59
          Width = 88
          Height = 24
          Anchors = [akLeft, akTop, akRight]
          Caption = #28165#38500#19981#23384#22312#30340
          TabOrder = 8
          OnClick = ABDownButton16Click
        end
      end
      object pnl4: TPanel
        Left = 2
        Top = 15
        Width = 428
        Height = 374
        Align = alClient
        TabOrder = 1
        object ListBox1: TListBox
          Tag = 910
          Left = 1
          Top = 1
          Width = 426
          Height = 331
          Align = alClient
          ItemHeight = 13
          MultiSelect = True
          PopupMenu = PopupMenu5
          TabOrder = 0
          OnKeyDown = ListBox1KeyDown
        end
        object ProgressBar1: TProgressBar
          Left = 1
          Top = 355
          Width = 426
          Height = 18
          Align = alBottom
          TabOrder = 1
        end
        object Panel5: TPanel
          Left = 1
          Top = 332
          Width = 426
          Height = 23
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object ToolButton4: TSpeedButton
            Left = 69
            Top = 1
            Width = 23
            Height = 22
            Flat = True
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE6100009C300000FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE6100009C30
              0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE61
              00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF009C300000CE610000CE610000CE6100009C300000CE610000CE61
              0000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF009C300000CE610000CE610000CE6100009C300000CE6100009C300000CE61
              0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF009C30
              0000CE610000CE610000CE6100009C300000CE610000CE610000CE6100009C30
              0000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF009C30
              00009C3000009C3000009C300000CE610000CE610000CE610000CE610000CE61
              00009C3000009C3000009C3000009C300000FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE610000CE61
              0000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF009C300000CE610000CE610000CE610000CE610000CE610000CE610000CE61
              0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF009C30
              0000CE610000CE610000CE610000CE610000CE610000CE610000CE610000CE61
              0000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF009C30
              00009C3000009C3000009C300000CE610000CE610000CE610000CE610000CE61
              00009C3000009C3000009C3000009C300000FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE61
              00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF009C3000009C3000009C3000009C3000009C3000009C30
              00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            OnClick = ToolButton4Click
          end
          object ToolButton3: TSpeedButton
            Left = 46
            Top = 1
            Width = 23
            Height = 22
            Flat = True
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF009C3000009C3000009C3000009C3000009C30
              00009C3000009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE61
              0000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF009C3000009C3000009C3000009C300000CE610000CE610000CE610000CE61
              0000CE6100009C3000009C3000009C3000009C300000FF00FF00FF00FF00FF00
              FF009C300000CE610000CE610000CE610000CE610000CE610000CE610000CE61
              0000CE610000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00
              FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE610000CE61
              0000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE61
              0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF009C3000009C3000009C3000009C300000CE610000CE610000CE610000CE61
              0000CE6100009C3000009C3000009C3000009C300000FF00FF00FF00FF00FF00
              FF009C300000CE610000CE610000CE6100009C300000CE610000CE610000CE61
              00009C300000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00
              FF00FF00FF009C300000CE610000CE610000CE6100009C300000CE6100009C30
              0000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF009C300000CE610000CE610000CE6100009C300000CE61
              0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE61
              0000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE61
              00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE6100009C30
              0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            OnClick = ToolButton3Click
          end
          object ToolButton2: TSpeedButton
            Left = 23
            Top = 1
            Width = 23
            Height = 22
            Flat = True
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE6100009C30
              0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE61
              00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE61
              0000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE61
              0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE610000CE61
              0000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00
              FF009C300000CE610000CE610000CE610000CE610000CE610000CE610000CE61
              0000CE610000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00
              FF009C3000009C3000009C3000009C300000CE610000CE610000CE610000CE61
              0000CE6100009C3000009C3000009C3000009C300000FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE61
              0000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF009C3000009C3000009C3000009C3000009C30
              00009C3000009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            OnClick = ToolButton2Click
          end
          object ToolButton1: TSpeedButton
            Left = 0
            Top = 1
            Width = 23
            Height = 22
            Flat = True
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF009C3000009C3000009C3000009C3000009C3000009C30
              00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE61
              00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C30
              00009C3000009C3000009C300000CE610000CE610000CE610000CE610000CE61
              00009C3000009C3000009C3000009C300000FF00FF00FF00FF00FF00FF009C30
              0000CE610000CE610000CE610000CE610000CE610000CE610000CE610000CE61
              0000CE610000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00
              FF009C300000CE610000CE610000CE610000CE610000CE610000CE610000CE61
              0000CE610000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE610000CE61
              0000CE6100009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF009C300000CE610000CE610000CE610000CE610000CE61
              00009C300000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF009C300000CE610000CE610000CE6100009C30
              0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000CE6100009C300000FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009C300000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            OnClick = ToolButton1Click
          end
        end
      end
    end
    object PageControl1: TPageControl
      Tag = 91
      Left = 436
      Top = 1
      Width = 500
      Height = 478
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = #25209#37327#25805#20316
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GroupBox2: TGroupBox
          Left = 0
          Top = 129
          Width = 492
          Height = 321
          Align = alClient
          Caption = 'dpr '#39033#30446#25209#37327#25805#20316
          TabOrder = 0
          DesignSize = (
            492
            321)
          object Label11: TLabel
            Left = 8
            Top = 47
            Width = 73
            Height = 13
            AutoSize = False
            Caption = #21253#21015#34920':'
          end
          object CheckBox1: TCheckBox
            Tag = 91
            Left = 8
            Top = 25
            Width = 289
            Height = 17
            Caption = #35774#32622'dpr'#24102#21253#32534#35793'('#25171#21246#21518#36816#34892'EXE'#38656#35201#30456#24212#30340'bpl'#21253')'
            TabOrder = 0
          end
          object Memo1: TMemo
            Tag = 910
            Left = 14
            Top = 66
            Width = 384
            Height = 246
            Anchors = [akLeft, akTop, akRight, akBottom]
            TabOrder = 1
            OnKeyDown = Memo3KeyDown
          end
          object ABDownButton8: TABDownButton
            Left = 408
            Top = 22
            Width = 75
            Height = 24
            Anchors = [akTop, akRight]
            Caption = #35774#32622
            TabOrder = 2
            OnClick = ABDownButton8Click
          end
          object ABDownButton9: TABDownButton
            Left = 408
            Top = 65
            Width = 75
            Height = 24
            Anchors = [akTop, akRight]
            Caption = #35774#32622
            TabOrder = 3
            OnClick = ABDownButton9Click
          end
        end
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 492
          Height = 129
          Align = alTop
          TabOrder = 1
          DesignSize = (
            492
            129)
          object Label5: TLabel
            Left = 8
            Top = 21
            Width = 75
            Height = 16
            AutoSize = False
            Caption = #29256#26412#21495':'
          end
          object Label6: TLabel
            Left = 8
            Top = 48
            Width = 75
            Height = 13
            AutoSize = False
            Caption = 'BPL'#36755#20986#36335#24452
          end
          object Label7: TLabel
            Left = 8
            Top = 74
            Width = 75
            Height = 13
            AutoSize = False
            Caption = 'DCP'#36755#20986#36335#24452
          end
          object Label8: TLabel
            Left = 8
            Top = 100
            Width = 75
            Height = 13
            AutoSize = False
            Caption = 'EXE'#36755#20986#36335#24452
          end
          object SpeedButton6: TSpeedButton
            Left = 375
            Top = 43
            Width = 27
            Height = 24
            Anchors = [akTop, akRight]
            Caption = '...'
            OnClick = SpeedButton6Click
          end
          object SpeedButton7: TSpeedButton
            Left = 375
            Top = 69
            Width = 27
            Height = 24
            Anchors = [akTop, akRight]
            Caption = '...'
            OnClick = SpeedButton7Click
          end
          object SpeedButton8: TSpeedButton
            Left = 375
            Top = 95
            Width = 27
            Height = 24
            Anchors = [akTop, akRight]
            Caption = '...'
            OnClick = SpeedButton8Click
          end
          object Edit6: TEdit
            Tag = 91
            Left = 82
            Top = 43
            Width = 289
            Height = 24
            Anchors = [akLeft, akTop, akRight]
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ImeName = 'Chinese (Simplified) - Microsoft Pinyin'
            ParentFont = False
            TabOrder = 0
          end
          object Edit7: TEdit
            Tag = 91
            Left = 82
            Top = 69
            Width = 289
            Height = 24
            Anchors = [akLeft, akTop, akRight]
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ImeName = 'Chinese (Simplified) - Microsoft Pinyin'
            ParentFont = False
            TabOrder = 1
          end
          object Edit8: TEdit
            Tag = 91
            Left = 82
            Top = 95
            Width = 289
            Height = 24
            Anchors = [akLeft, akTop, akRight]
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Arial'
            Font.Style = []
            ImeName = 'Chinese (Simplified) - Microsoft Pinyin'
            ParentFont = False
            TabOrder = 2
          end
          object SpinEdit1: TSpinEdit
            Tag = 91
            Left = 82
            Top = 17
            Width = 46
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 3
            Value = 0
          end
          object SpinEdit2: TSpinEdit
            Tag = 91
            Left = 129
            Top = 17
            Width = 46
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 4
            Value = 0
          end
          object SpinEdit3: TSpinEdit
            Tag = 91
            Left = 176
            Top = 17
            Width = 46
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 5
            Value = 0
          end
          object SpinEdit4: TSpinEdit
            Tag = 91
            Left = 223
            Top = 17
            Width = 46
            Height = 22
            MaxValue = 0
            MinValue = 0
            TabOrder = 6
            Value = 0
          end
          object ABDownButton4: TABDownButton
            Left = 408
            Top = 14
            Width = 75
            Height = 24
            Anchors = [akTop, akRight]
            Caption = #35774#32622
            TabOrder = 7
            OnClick = ABDownButton4Click
          end
          object ABDownButton5: TABDownButton
            Left = 408
            Top = 43
            Width = 75
            Height = 24
            Anchors = [akTop, akRight]
            Caption = #35774#32622
            TabOrder = 8
            OnClick = ABDownButton5Click
          end
          object ABDownButton6: TABDownButton
            Left = 408
            Top = 69
            Width = 75
            Height = 24
            Anchors = [akTop, akRight]
            Caption = #35774#32622
            TabOrder = 9
            OnClick = ABDownButton6Click
          end
          object ABDownButton7: TABDownButton
            Left = 408
            Top = 95
            Width = 75
            Height = 24
            Anchors = [akTop, akRight]
            Caption = #35774#32622
            TabOrder = 10
            OnClick = ABDownButton7Click
          end
          object CheckBox2: TCheckBox
            Tag = 91
            Left = 274
            Top = 20
            Width = 67
            Height = 17
            Caption = #21253#21547#29256#26412
            TabOrder = 11
          end
          object CheckBox3: TCheckBox
            Tag = 91
            Left = 345
            Top = 20
            Width = 53
            Height = 17
            Caption = #33258#22686#38271
            TabOrder = 12
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = #32534#35793
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label4: TLabel
          Left = 0
          Top = 0
          Width = 492
          Height = 25
          Align = alTop
          Alignment = taCenter
          AutoSize = False
          Caption = #20165#25903#25345'Dcc32'#32534#35793'dpk/dpr,'#35831#23433#35013#30456#24212#30340'DELPHI,'#26242#19981#25903#25345#33073#31163'DELPHI'#32534#35793
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          Layout = tlCenter
        end
        object pnl6: TPanel
          Left = 0
          Top = 25
          Width = 492
          Height = 390
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Splitter2: TSplitter
            Left = 0
            Top = 282
            Width = 492
            Height = 4
            Cursor = crVSplit
            Align = alBottom
          end
          object RadioGroup1: TRadioGroup
            Tag = 91
            Left = 0
            Top = 0
            Width = 492
            Height = 41
            Align = alTop
            BiDiMode = bdLeftToRight
            Caption = #32534#35793#29256#26412
            Columns = 7
            Ctl3D = True
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Courier New'
            Font.Style = []
            ItemIndex = 0
            Items.Strings = (
              'D7'
              'D2007'
              'D2009'
              'D2010'
              'DXE1'
              'DXE2'
              'DXE3')
            ParentBiDiMode = False
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
          end
          object GroupBox3: TGroupBox
            Left = 0
            Top = 146
            Width = 492
            Height = 136
            Align = alClient
            Caption = #32534#35793#26085#24535
            TabOrder = 1
            object Memo2: TMemo
              Tag = 910
              Left = 2
              Top = 15
              Width = 488
              Height = 119
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 0
              OnKeyDown = Memo3KeyDown
            end
          end
          object GroupBox5: TGroupBox
            Left = 0
            Top = 41
            Width = 492
            Height = 105
            Align = alTop
            Caption = #36755#20986#36335#24452
            TabOrder = 2
            DesignSize = (
              492
              105)
            object Label1: TLabel
              Left = 8
              Top = 24
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'BPL'#36755#20986#36335#24452
            end
            object Label2: TLabel
              Left = 8
              Top = 50
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'DCP'#36755#20986#36335#24452
            end
            object Label3: TLabel
              Left = 8
              Top = 76
              Width = 74
              Height = 13
              AutoSize = False
              Caption = 'EXE'#36755#20986#36335#24452
            end
            object SpeedButton3: TSpeedButton
              Left = 462
              Top = 19
              Width = 27
              Height = 24
              Anchors = [akTop, akRight]
              Caption = '...'
              ParentShowHint = False
              ShowHint = True
              OnClick = SpeedButton3Click
            end
            object SpeedButton4: TSpeedButton
              Left = 462
              Top = 45
              Width = 27
              Height = 24
              Anchors = [akTop, akRight]
              Caption = '...'
              OnClick = SpeedButton4Click
            end
            object SpeedButton5: TSpeedButton
              Left = 462
              Top = 72
              Width = 27
              Height = 24
              Anchors = [akTop, akRight]
              Caption = '...'
              OnClick = SpeedButton5Click
            end
            object Edit1: TEdit
              Tag = 91
              Left = 82
              Top = 19
              Width = 377
              Height = 24
              Anchors = [akLeft, akTop, akRight]
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = []
              ImeName = 'Chinese (Simplified) - Microsoft Pinyin'
              ParentFont = False
              TabOrder = 0
            end
            object Edit2: TEdit
              Tag = 91
              Left = 82
              Top = 45
              Width = 377
              Height = 24
              Anchors = [akLeft, akTop, akRight]
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = []
              ImeName = 'Chinese (Simplified) - Microsoft Pinyin'
              ParentFont = False
              TabOrder = 1
            end
            object Edit5: TEdit
              Tag = 91
              Left = 82
              Top = 71
              Width = 377
              Height = 24
              Anchors = [akLeft, akTop, akRight]
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = []
              ImeName = 'Chinese (Simplified) - Microsoft Pinyin'
              ParentFont = False
              TabOrder = 2
            end
          end
          object GroupBox4: TGroupBox
            Left = 0
            Top = 286
            Width = 492
            Height = 104
            Align = alBottom
            Caption = #32534#35793#22833#36133#30340#24037#31243
            TabOrder = 3
            object ListBox2: TListBox
              Tag = 910
              Left = 2
              Top = 15
              Width = 488
              Height = 87
              Align = alClient
              ItemHeight = 13
              MultiSelect = True
              PopupMenu = PopupMenu6
              TabOrder = 0
            end
          end
        end
        object pnl2: TPanel
          Left = 0
          Top = 415
          Width = 492
          Height = 35
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            492
            35)
          object Button16: TButton
            Left = 2
            Top = 6
            Width = 80
            Height = 24
            BiDiMode = bdLeftToRight
            Caption = #36335#24452#35774#32622
            ParentBiDiMode = False
            TabOrder = 0
            OnClick = Button16Click
          end
          object ABDownButton10: TABDownButton
            Left = 104
            Top = 6
            Width = 88
            Height = 24
            Anchors = [akTop, akRight]
            Caption = #32534#35793#24403#21069#24037#31243
            TabOrder = 1
            OnClick = ABDownButton10Click
          end
          object ABDownButton11: TABDownButton
            Left = 262
            Top = 6
            Width = 74
            Height = 24
            Anchors = [akTop, akRight]
            Caption = #20174#24403#21069#24320#22987
            TabOrder = 2
            OnClick = ABDownButton11Click
          end
          object ABDownButton12: TABDownButton
            Left = 195
            Top = 6
            Width = 64
            Height = 24
            Anchors = [akTop, akRight]
            Caption = #32534#35793#36873#25321
            TabOrder = 3
            OnClick = ABDownButton12Click
          end
          object ABDownButton13: TABDownButton
            Left = 339
            Top = 6
            Width = 65
            Height = 24
            Anchors = [akTop, akRight]
            Caption = #32534#35793#25152#26377
            TabOrder = 4
            OnClick = ABDownButton13Click
          end
          object ABDownButton17: TABDownButton
            Left = 410
            Top = 6
            Width = 71
            Height = 24
            Anchors = [akTop, akRight]
            Caption = #20572#27490#32534#35793
            Enabled = False
            TabOrder = 5
            OnClick = ABDownButton17Click
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = #26816#27979#21333#20803#24490#29615#24341#29992
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label9: TLabel
          Left = 0
          Top = 0
          Width = 492
          Height = 18
          Align = alTop
          Alignment = taCenter
          AutoSize = False
          Caption = #26816#27979#26085#24535
          Layout = tlCenter
        end
        object Memo3: TMemo
          Tag = 910
          Left = 0
          Top = 18
          Width = 492
          Height = 393
          Align = alClient
          ScrollBars = ssBoth
          TabOrder = 0
          OnKeyDown = Memo3KeyDown
        end
        object Panel1: TPanel
          Left = 0
          Top = 411
          Width = 492
          Height = 39
          Align = alBottom
          TabOrder = 1
          object Button1: TButton
            Left = 3
            Top = 5
            Width = 80
            Height = 25
            Caption = #24320#22987#26816#27979
            TabOrder = 0
            OnClick = Button1Click
          end
          object Button4: TButton
            Left = 85
            Top = 5
            Width = 80
            Height = 25
            Caption = #20572#27490#26816#27979
            Enabled = False
            TabOrder = 1
            OnClick = Button4Click
          end
          object RadioGroup2: TRadioGroup
            Tag = 91
            Left = 320
            Top = 1
            Width = 171
            Height = 37
            Align = alRight
            BiDiMode = bdLeftToRight
            Columns = 2
            Ctl3D = True
            ItemIndex = 0
            Items.Strings = (
              #26174#31034#24490#29615
              #26174#31034#25152#26377)
            ParentBiDiMode = False
            ParentCtl3D = False
            TabOrder = 2
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = #26816#27979#28165#38500'XE'#19979#21333#20803#30340#31354#38388#21517
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label10: TLabel
          Left = 0
          Top = 0
          Width = 492
          Height = 18
          Align = alTop
          Alignment = taCenter
          AutoSize = False
          Caption = #26816#27979#28165#38500#26085#24535
          Layout = tlCenter
        end
        object Panel6: TPanel
          Left = 0
          Top = 411
          Width = 492
          Height = 39
          Align = alBottom
          TabOrder = 0
          object Button2: TButton
            Left = 3
            Top = 6
            Width = 86
            Height = 25
            Caption = #22788#29702#25152#26377#25991#20214
            TabOrder = 0
            OnClick = Button2Click
          end
          object Button3: TButton
            Left = 189
            Top = 6
            Width = 80
            Height = 25
            Caption = #20572#27490
            Enabled = False
            TabOrder = 1
            OnClick = Button3Click
          end
          object RadioGroup3: TRadioGroup
            Tag = 91
            Left = 320
            Top = 1
            Width = 171
            Height = 37
            Align = alRight
            BiDiMode = bdLeftToRight
            Columns = 2
            Ctl3D = True
            ItemIndex = 0
            Items.Strings = (
              #20165#26816#27979
              #26816#27979'+'#28165#38500)
            ParentBiDiMode = False
            ParentCtl3D = False
            TabOrder = 2
          end
          object Button5: TButton
            Left = 95
            Top = 6
            Width = 86
            Height = 25
            Caption = #22788#29702#36873#25321#25991#20214
            TabOrder = 3
            OnClick = Button5Click
          end
        end
        object Memo4: TMemo
          Tag = 910
          Left = 0
          Top = 18
          Width = 492
          Height = 393
          Align = alClient
          ScrollBars = ssBoth
          TabOrder = 1
          OnKeyDown = Memo3KeyDown
        end
      end
      object TabSheet5: TTabSheet
        Caption = #24341#29992#21333#20803#25972#29702
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label12: TLabel
          Left = 0
          Top = 0
          Width = 492
          Height = 18
          Align = alTop
          Alignment = taCenter
          AutoSize = False
          Caption = #25972#29702#26085#24535
          Layout = tlCenter
        end
        object Label13: TLabel
          Left = 0
          Top = 214
          Width = 492
          Height = 18
          Align = alBottom
          Alignment = taCenter
          AutoSize = False
          Caption = #27169#26495
          Layout = tlCenter
          ExplicitTop = 0
        end
        object Panel7: TPanel
          Left = 0
          Top = 411
          Width = 492
          Height = 39
          Align = alBottom
          TabOrder = 0
          object Button6: TButton
            Left = 3
            Top = 6
            Width = 86
            Height = 25
            Caption = #25972#29702#25152#26377#25991#20214
            TabOrder = 0
            OnClick = Button6Click
          end
          object Button7: TButton
            Left = 189
            Top = 6
            Width = 80
            Height = 25
            Caption = #20572#27490
            Enabled = False
            TabOrder = 1
            OnClick = Button7Click
          end
          object RadioGroup4: TRadioGroup
            Tag = 91
            Left = 320
            Top = 1
            Width = 171
            Height = 37
            Align = alRight
            BiDiMode = bdLeftToRight
            Columns = 2
            Ctl3D = True
            ItemIndex = 0
            Items.Strings = (
              #20165#26816#27979
              #26816#27979'+'#25972#29702)
            ParentBiDiMode = False
            ParentCtl3D = False
            TabOrder = 2
          end
          object Button8: TButton
            Left = 95
            Top = 6
            Width = 86
            Height = 25
            Caption = #25972#29702#36873#25321#25991#20214
            TabOrder = 3
            OnClick = Button8Click
          end
        end
        object RadioGroup5: TRadioGroup
          Left = 328
          Top = 304
          Width = 185
          Height = 105
          Caption = 'RadioGroup5'
          TabOrder = 1
        end
        object PageControl2: TPageControl
          Tag = 91
          Left = 0
          Top = 232
          Width = 492
          Height = 179
          ActivePage = TabSheet6
          Align = alBottom
          TabOrder = 2
          object TabSheet6: TTabSheet
            Caption = #27169#26495'1'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Memo6: TMemo
              Tag = 910
              Left = 0
              Top = 0
              Width = 484
              Height = 122
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 0
              OnKeyDown = Memo3KeyDown
            end
            object Panel8: TPanel
              Left = 0
              Top = 122
              Width = 484
              Height = 29
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 1
              object CheckBox5: TCheckBox
                Tag = 91
                Left = 4
                Top = 6
                Width = 150
                Height = 17
                Caption = #27599#20010#21333#20803#21517#31216#21333#34892#26174#31034
                Checked = True
                State = cbChecked
                TabOrder = 0
              end
            end
          end
          object TabSheet7: TTabSheet
            Caption = #27169#26495'2'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Memo7: TMemo
              Tag = 910
              Left = 0
              Top = 0
              Width = 484
              Height = 122
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 0
              OnKeyDown = Memo3KeyDown
            end
            object Panel9: TPanel
              Left = 0
              Top = 122
              Width = 484
              Height = 29
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 1
              object CheckBox6: TCheckBox
                Tag = 91
                Left = 4
                Top = 6
                Width = 150
                Height = 17
                Caption = #27599#20010#21333#20803#21517#31216#21333#34892#26174#31034
                Checked = True
                State = cbChecked
                TabOrder = 0
              end
            end
          end
          object TabSheet8: TTabSheet
            Caption = #27169#26495'3'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Memo8: TMemo
              Tag = 910
              Left = 0
              Top = 0
              Width = 484
              Height = 122
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 0
              OnKeyDown = Memo3KeyDown
            end
            object Panel10: TPanel
              Left = 0
              Top = 122
              Width = 484
              Height = 29
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 1
              object CheckBox7: TCheckBox
                Tag = 91
                Left = 4
                Top = 6
                Width = 150
                Height = 17
                Caption = #27599#20010#21333#20803#21517#31216#21333#34892#26174#31034
                Checked = True
                State = cbChecked
                TabOrder = 0
              end
            end
          end
          object TabSheet9: TTabSheet
            Caption = #27169#26495'4'
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Memo9: TMemo
              Tag = 910
              Left = 0
              Top = 0
              Width = 484
              Height = 122
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 0
              OnKeyDown = Memo3KeyDown
            end
            object Panel11: TPanel
              Left = 0
              Top = 122
              Width = 484
              Height = 29
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 1
              object CheckBox8: TCheckBox
                Tag = 91
                Left = 4
                Top = 6
                Width = 150
                Height = 17
                Caption = #27599#20010#21333#20803#21517#31216#21333#34892#26174#31034
                Checked = True
                State = cbChecked
                TabOrder = 0
              end
            end
          end
          object TabSheet10: TTabSheet
            Caption = #27169#26495'5'
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Memo10: TMemo
              Tag = 910
              Left = 0
              Top = 0
              Width = 484
              Height = 122
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 0
              OnKeyDown = Memo3KeyDown
            end
            object Panel12: TPanel
              Left = 0
              Top = 122
              Width = 484
              Height = 29
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 1
              object CheckBox9: TCheckBox
                Tag = 91
                Left = 4
                Top = 6
                Width = 150
                Height = 17
                Caption = #27599#20010#21333#20803#21517#31216#21333#34892#26174#31034
                Checked = True
                State = cbChecked
                TabOrder = 0
              end
            end
          end
        end
        object Panel13: TPanel
          Left = 0
          Top = 18
          Width = 492
          Height = 196
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 3
          object Memo5: TMemo
            Tag = 910
            Left = 0
            Top = 0
            Width = 307
            Height = 196
            Align = alClient
            ScrollBars = ssBoth
            TabOrder = 0
            OnKeyDown = Memo3KeyDown
          end
          object Panel14: TPanel
            Left = 307
            Top = 0
            Width = 185
            Height = 196
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object Label14: TLabel
              Left = 0
              Top = 0
              Width = 185
              Height = 18
              Align = alTop
              Alignment = taCenter
              AutoSize = False
              Caption = #26410#25972#29702#21333#20803
              Layout = tlCenter
              ExplicitWidth = 492
            end
            object Memo11: TMemo
              Tag = 910
              Left = 0
              Top = 18
              Width = 185
              Height = 178
              Align = alClient
              ScrollBars = ssBoth
              TabOrder = 0
              OnKeyDown = Memo3KeyDown
            end
          end
        end
      end
      object TabSheet11: TTabSheet
        Caption = #20854#23427
        ImageIndex = 5
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Button9: TButton
          Left = 3
          Top = 3
          Width = 214
          Height = 24
          BiDiMode = bdLeftToRight
          Caption = #21462'dpr/dpk'#21253#30340#25152#26377#21333#20803#21517
          ParentBiDiMode = False
          TabOrder = 0
          OnClick = Button9Click
        end
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 480
    Width = 937
    Height = 20
    Panels = <
      item
        Width = 50
      end>
  end
  object PopupMenu2: TPopupMenu
    Left = 352
    Top = 296
    object N4: TMenuItem
      Caption = #28155#21152'pas/dpk/dpr/dproj'#25991#20214
      OnClick = N4Click
    end
    object pas1: TMenuItem
      Caption = #28155#21152'pas'#25991#20214
      OnClick = pas1Click
    end
    object dpk1: TMenuItem
      Caption = #28155#21152'dpk'#25991#20214
      OnClick = dpk1Click
    end
    object dpr1: TMenuItem
      Caption = #28155#21152'dpr'#25991#20214
      OnClick = dpr1Click
    end
    object dproj1: TMenuItem
      Caption = #28155#21152'dproj'#25991#20214
      OnClick = dproj1Click
    end
    object N5: TMenuItem
      Caption = #28155#21152#25991#20214#25193#23637#21517#31526#21512#36807#28388#26465#20214#30340#25991#20214
      OnClick = N5Click
    end
    object N1: TMenuItem
      Caption = #28155#21152#25991#20214#21517#31526#21512#36807#28388#26465#20214#30340#25991#20214'('#25903#25345'?*)'
      OnClick = N1Click
    end
  end
  object PopupMenu3: TPopupMenu
    Left = 360
    Top = 360
    object N2: TMenuItem
      Caption = #21478#23384#36873#25321#25991#20214#21040#24037#31243#32452
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #21478#23384#25152#26377#25991#20214#21040#24037#31243#32452
      OnClick = N3Click
    end
  end
  object PopupMenu5: TPopupMenu
    Left = 304
    Top = 296
    object N11: TMenuItem
      Caption = #35835#21462#21333#20010#25991#20214#25209#37327#25805#20316#20013#20869#23481
      OnClick = N11Click
    end
    object N6: TMenuItem
      Caption = #35835#21462#25152#26377#25991#20214#25209#37327#25805#20316#20013#20869#23481
      OnClick = N6Click
    end
  end
  object PopupMenu6: TPopupMenu
    Left = 872
    Top = 368
    object MenuItem1: TMenuItem
      Caption = #37325#32534#35793#22833#36133#30340#24037#31243
      OnClick = MenuItem1Click
    end
  end
end
