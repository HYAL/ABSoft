unit Main;

interface               
                                        
uses
  ABPubVarU,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,  StdCtrls, ExtCtrls, DBCtrls, Grids, DBGrids, DB,
  ADODB, ComCtrls,adoint;

type
  TABUpdateADOQuery = class;
  TABUpdateADOQueryForm = class(TForm)
    ADOConnection1: TADOConnection;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FUpdateADOQuery: TABUpdateADOQuery;
    { Private declarations }
  public
    { Public declarations }
  end;


  TABUpdateADOQuery = class(TADOQuery)
  private
    FUpdateMode: TUpdateMode;
    FModifySQL: TStrings;
    FInsertSQL: TStrings;
    FDeleteSQL: TStrings;
    procedure SetUpdateMode(const Value: TUpdateMode);
    function GetDeleteSQL: TStrings;
    function GetInsertSQL: TStrings;
    function GetModifySQL: TStrings;
    procedure SetDeleteSQL(const Value: TStrings);
    procedure SetInsertSQL(const Value: TStrings);
    procedure SetModifySQL(const Value: TStrings);

    { Private declarations }
  protected
    procedure DoAfterOpen; override;
    //放在FetchCompletea中解决第一条记录看不到的情况
    //UpdateCursorPos;
    //Resync([]);
    { Protected declarations }
  public
    //行刷新,只对单表在即时更新时有效
    function RowRefresh:boolean;
    procedure InternalPost; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure  InternalDelete; override;
    procedure Requery(Options: TExecuteOptions = []);
    property ABUpdateMode: TUpdateMode read FUpdateMode write SetUpdateMode default upWhereAll;

    { Public declarations }
  published
    //更新模式,只对单表在即时更新时有效
    property ABModifySQL: TStrings read GetModifySQL write SetModifySQL;
    property ABInsertSQL: TStrings read GetInsertSQL write SetInsertSQL;
    property ABDeleteSQL: TStrings read GetDeleteSQL write SetDeleteSQL;
    { Published declarations }
  end;
var
  ABUpdateADOQueryForm: TABUpdateADOQueryForm;

implementation
{$R *.dfm}

type
  PRecInfo = ^TRecInfo;
  TRecInfo = packed record
    Bookmark: OleVariant;
    BookmarkFlag: TBookmarkFlag;
    RecordStatus: Integer;
    RecordNumber: Integer;
  end;

{ TABUpdateADOQuery }

constructor TABUpdateADOQuery.Create(AOwner: TComponent);
begin
  inherited;
  FModifySQL := TStringList.Create;
  FInsertSQL := TStringList.Create;
  FDeleteSQL := TStringList.Create;
end;

destructor TABUpdateADOQuery.Destroy;
begin
  FreeAndNil(FModifySQL);
  FreeAndNil(FInsertSQL);
  FreeAndNil(FDeleteSQL);
  inherited;
end;

function TABUpdateADOQuery.GetDeleteSQL: TStrings;
begin
  Result := FDeleteSQL;
end;

function TABUpdateADOQuery.GetInsertSQL: TStrings;
begin
  Result := FInsertSQL;
end;

function TABUpdateADOQuery.GetModifySQL: TStrings;
begin
  Result := FModifySQL;
end;

procedure TABUpdateADOQuery.InternalDelete;
var
  TempADOCommand:TADOCommand;
  TempStr1,TempStr2:string;
  i:longint;
begin
  inherited InternalDelete ;
  TempStr1:=trim(FDeleteSQL.Text);
  if TempStr1<>'' then
  begin
    TempADOCommand:=TADOCommand.Create(nil);
    try
      TempADOCommand.Connection:=Connection;
      TempADOCommand.CommandText:=TempStr1;
      for i := 1 to TempADOCommand.Parameters.Count do
      begin
        TempStr2:=TempADOCommand.Parameters[i-1].Name;
        TempADOCommand.Parameters[i-1].Value:=fieldbyname(TempStr2).Value;
      end;
      TempADOCommand.Execute;
    finally
      TempADOCommand.Free;
    end;
  end;
end;

procedure TABUpdateADOQuery.InternalPost;
var
  TempADOCommand:TADOCommand;
  TempStr1,TempStr2:string;
  i:longint;
begin
  inherited InternalPost;

  if State in [dsedit] then
    TempStr1:=trim(FModifySQL.Text)
  else
  if State in [dsinsert] then
    TempStr1:=trim(FInsertSQL.Text);
  if TempStr1<>'' then
  begin
    TempADOCommand:=TADOCommand.Create(nil);
    try
      TempADOCommand.Connection:=Connection;
      TempADOCommand.CommandText:=TempStr1;
      for i := 1 to TempADOCommand.Parameters.Count do
      begin
        TempStr2:=TempADOCommand.Parameters[i-1].Name;
        TempADOCommand.Parameters[i-1].Value:=fieldbyname(TempStr2).Value;
      end;
      TempADOCommand.Execute;
    finally
      TempADOCommand.Free;
    end;
  end;
end;


function TABUpdateADOQuery.RowRefresh: boolean;
begin
  result:=true;
  try
    UpdateCursorPos;
    Recordset.Resync(AdAffectCurrent, AdResyncAllValues);
    Resync([]);

    //Requery([]);

  except
    result:=false;
  end;
end;

procedure TABUpdateADOQuery.SetDeleteSQL(const Value: TStrings);
begin
  FDeleteSQL.Assign(Value);
end;

procedure TABUpdateADOQuery.SetInsertSQL(const Value: TStrings);
begin
  FInsertSQL.Assign(Value);
end;

procedure TABUpdateADOQuery.SetModifySQL(const Value: TStrings);
begin
  FModifySQL.Assign(Value);
end;

procedure TABUpdateADOQuery.SetUpdateMode(const Value: TUpdateMode);
begin
  FUpdateMode := Value;
  if Active then
  begin
    if FUpdateMode = upWhereAll then
      Recordset.Properties['Update Criteria'].Value:=adCriteriaAllCols
    else if FUpdateMode = upWhereChanged then
      Recordset.Properties['Update Criteria'].Value:=adCriteriaUpdCols
    else if FUpdateMode = upWhereKeyOnly then
      Recordset.Properties['Update Criteria'].Value:=adCriteriaKey;
  end;
end;

procedure TABUpdateADOQuery.Requery(Options: TExecuteOptions);
begin
  CheckBrowseMode;
  InternalRequery(Options);
end;

procedure TABUpdateADOQuery.DoAfterOpen;
begin
  inherited;
  SetUpdateMode(FUpdateMode);
end;


procedure TABUpdateADOQueryForm.Button1Click(Sender: TObject);
begin
  DataSource1.DataSet.Close;
  DataSource1.DataSet.Open;
end;

procedure TABUpdateADOQueryForm.FormCreate(Sender: TObject);
begin
  ADOConnection1.Connected:=false;
  ADOConnection1.ConnectionString:=
    'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+ABSoftSetPath+'Data.mdb;Persist Security Info=False';
  ADOConnection1.Connected:=true;

  FUpdateADOQuery:= TABUpdateADOQuery.create(nil);
  FUpdateADOQuery.Connection := ADOConnection1;
  FUpdateADOQuery.CursorType := ctStatic;
  FUpdateADOQuery.ExecuteOptions := [eoAsyncFetchNonBlocking];
  FUpdateADOQuery.LockType := ltBatchOptimistic;
  FUpdateADOQuery.SQL.text:=
        'select  b.name,a.* from orders  a left join custs b on a.custcode=b.code ';
  FUpdateADOQuery.ABModifySQL.text:=
      'update orders set Custcode=:Custcode,SumMoney=:SumMoney where code =:code';
  FUpdateADOQuery.ABInsertSQL.text:=
        'insert into orders(code,Custcode,SumMoney) values(:code,:Custcode,:SumMoney)';
  FUpdateADOQuery.ABDeleteSQL.text:=
        'delete from orders where code =:code and Custcode=:Custcode and  SumMoney=:SumMoney';

  DataSource1.DataSet:=FUpdateADOQuery;
  Button1Click(nil);
end;

procedure TABUpdateADOQueryForm.FormDestroy(Sender: TObject);
begin
  FUpdateADOQuery.free;
end;

end.

