unit Main;

interface

uses
  ABPubFormU,

  Windows,Messages,SysUtils,Variants,Classes,Graphics,Controls,Forms,Dialogs,
  StdCtrls,IdBaseComponent,IdComponent,IdRawBase,IdRawClient,IdIcmpClient,Mask,
  Menus,ExtCtrls;

type                                                
  TIPvalues = array[0..3] of Byte;
  TMainForm = class(TABPubForm)
    IdIcmp: TIdIcmpClient;
    Panel1: TPanel;
    Label5: TLabel;
    Panel2: TPanel;
    label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Edit1: TEdit;
    Button1: TButton;
    Button2: TButton;
    CheckBox1: TCheckBox;
    Button3: TButton;
    Edit2: TEdit;
    Edit3: TEdit;
    Memo1: TMemo;
    Memo2: TMemo;
    procedure Button1Click(Sender: TObject);
    function IsIPString(IPStr: string): Boolean;
    function iptoValues(ip: string): TIPvalues;
    procedure Button2Click(Sender: TObject);
    procedure IdIcmpReply(ASender: TComponent;
      const AReplyStatus: TReplyStatus);
    procedure Button3Click(Sender: TObject);
    procedure Memo2KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}
var stop: Boolean;

function TMainForm.IsIPString(IPStr: string): Boolean;
var
  i, DotNum: integer;
  Tmpstr: string;
begin
  Result := true;
  DotNum := 0;
  for i := 1 to Length(IPStr) do
    case IPStr[i] of
      '0'..'9', '.':
        begin
          if IPStr[i] <> '.' then
          begin
            Tmpstr := Tmpstr + IPStr[i];
            if StrtoInt(tmpstr) > 255 then
            begin
              Result := False;
              exit;
            end;
          end
          else
          begin
            if (TmpStr = '') and (DotNum > 0) then //如果连续2个点的情况
            begin
              Result := False;
              Exit;
            end;
            Tmpstr := '';
            DotNum := DotNum + 1;
          end;
        end;
    else
      begin Result := false; exit; end;
    end;
  if DotNum <> 3 then Result := False;
end;


procedure TMainForm.Memo2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Shift=[ssCtrl]) then
  begin
    if  (Key=65) then
    begin
      TMemo(Sender).SelectAll;
    end
  end;
end;

procedure TMainForm.Button2Click(Sender: TObject);
var
  i: Integer;
begin
  Button2.Enabled := false;
  Button1.Enabled := false;
  stop := False;
  IdIcmp.ReceiveTimeout := Trunc(strtoint(Edit3.text));
  for i := 0 to Memo1.Lines.Count - 1 do
  begin
    Application.ProcessMessages;
    IdIcmp.Host := Memo1.Lines[i];
    IdIcmp.Ping;
    if stop then Break;
  end;
  Button2.Enabled := true;
  Button1.Enabled := true;
end;

procedure TMainForm.Button3Click(Sender: TObject);
begin
  stop := true;
end;

procedure TMainForm.IdIcmpReply(ASender: TComponent;
  const AReplyStatus: TReplyStatus);
begin
  case AReplyStatus.ReplyStatusType of
    rsEcho: Memo2.Lines.Add(AReplyStatus.FromIpAddress + '  成功  耗时(毫秒)：' + inttostr(areplystatus.MsRoundTripTime));
    rsError: Memo2.Lines.Add(IdIcmp.Host + '  错误  ');
    rsTimeOut: Memo2.Lines.Add(IdIcmp.Host + '  超时  ');
    rsErrorUnreachable: Memo2.Lines.Add(IdIcmp.Host + '  地址错误  ');
  else
    Memo2.Lines.Add(IdIcmp.Host + '  其他未知错误  ');
  end;
end;

function TMainForm.iptoValues(ip: string): TIPvalues;
var tmp: string;
  i, x: Byte;
begin
  x := 0;
  tmp := '';
  if IsIPString(ip) then
  begin
    for i := 1 to Length(ip) do
    begin
      if ip[i] in ['0'..'9'] then
        tmp := tmp + ip[i];
      if ip[i] = '.' then
      begin
        Result[x] := StrToInt(tmp);
        tmp := '';
        x := x + 1;
      end;
    end;
    Result[x] := StrToInt(tmp);
  end else raise Exception.Create('非法IP');
end;

procedure TMainForm.Button1Click(Sender: TObject);
var ipvalue: TIPvalues;
  i: Integer;
begin
  if IsIPString(Edit1.Text) then
  begin
    Memo1.Lines.Add(Edit1.Text);
    if CheckBox1.Checked and (strtoint(Edit2.text) > 0) then
    begin
      ipvalue := iptoValues(Edit1.Text);
      if ipvalue[3] + strtoint(Edit2.text) > 255 then  Edit2.text  :=inttostr( 255 - ipvalue[3]);
      Memo1.Lines.BeginUpdate;
      try
        for I := ipvalue[3] + 1 to ipvalue[3] + trunc(strtoint(Edit2.text)) do
          Memo1.Lines.Add(Format('%d.%d.%d.%d', [ipvalue[0], ipvalue[1], ipvalue[2], i]));
      finally
        Memo1.Lines.EndUpdate;
      end;
    end;
  end else Application.MessageBox('你输入的IP不合法。', '错误', MB_OK + MB_ICONSTOP);
end;

end.

