unit Main;

interface

uses
  ABPubVarU,
  ABPubFuncU,
  ABPubDBU,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Spin, ExtCtrls,db, DBClient;

type
  TCaleForm = class(TForm)
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    Label1: TLabel;
    SpinEdit1: TSpinEdit;
    Label2: TLabel;
    Label3: TLabel;
    SpinEdit2: TSpinEdit;
    Label4: TLabel;
    SpinEdit3: TSpinEdit;
    Label5: TLabel;
    SpinEdit4: TSpinEdit;
    Label6: TLabel;
    SpinEdit5: TSpinEdit;
    Label7: TLabel;
    SpinEdit6: TSpinEdit;
    Label8: TLabel;
    SpinEdit7: TSpinEdit;
    Panel1: TPanel;
    Button1: TButton;
    Label9: TLabel;
    SpinEdit8: TSpinEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    FDataSet:TClientdataset;
    procedure MakeData;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CaleForm: TCaleForm;

implementation

{$R *.dfm}
procedure TCaleForm.MakeData;
var
  i,j:LongInt;
  tempSubject:string;
  tempDecArray:array[0..4] of LongInt;
  tempDecArray_Max:array[0..4] of LongInt;
  tempDecArray_Flag:array of string;
  tempFlag:string;
  tempBoolean:Boolean;
  tempDouble:Double;
begin
  FDataSet.MergeChangeLog;
  FDataSet.EmptyDataSet;
  tempDecArray_Max[0]:= SpinEdit2.Value;
  tempDecArray_Max[1]:= SpinEdit3.Value;
  tempDecArray_Max[2]:= SpinEdit4.Value;
  tempDecArray_Max[3]:= SpinEdit5.Value;
  tempDecArray_Max[4]:= SpinEdit6.Value;

  if CheckBox1.Checked then
  begin
    SetLength(tempDecArray_Flag,High(tempDecArray_Flag)+2);
    tempDecArray_Flag[High(tempDecArray_Flag)]:='+';
  end;
  if CheckBox2.Checked then
  begin
    SetLength(tempDecArray_Flag,High(tempDecArray_Flag)+2);
    tempDecArray_Flag[High(tempDecArray_Flag)]:='-';
  end;
  if CheckBox3.Checked then
  begin
    SetLength(tempDecArray_Flag,High(tempDecArray_Flag)+2);
    tempDecArray_Flag[High(tempDecArray_Flag)]:='*';
  end;
  if CheckBox4.Checked then
  begin
    SetLength(tempDecArray_Flag,High(tempDecArray_Flag)+2);
    tempDecArray_Flag[High(tempDecArray_Flag)]:='/';
  end;

  Randomize;
  for I := 0 to SpinEdit8.Value - 1 do
  begin
    repeat
      tempSubject:=EmptyStr;
      for j := 0 to SpinEdit1.Value - 1 do
      begin
        repeat
          tempFlag:=tempDecArray_Flag[ABTrunc(abs(Random(ABTicker) mod (High(tempDecArray_Flag)+1)))];
          tempDecArray[j]:=ABTrunc(abs(Random(ABTicker) mod (tempDecArray_Max[j]+1)));
        until (tempFlag <> '/') or (tempDecArray[j]<>0);

        ABAddstr(tempSubject,IntToStr(tempDecArray[j]),tempFlag);
      end;
      tempDouble:=ABExpressionEval(tempSubject,tempBoolean);
    until (tempDouble>=0) and (tempDouble<=SpinEdit7.Value);

    FDataSet.Append;
    FDataSet.FieldByName('N').AsString:=tempSubject+'=';
    FDataSet.post;
  end;
end;

procedure TCaleForm.Button1Click(Sender: TObject);
var
  tempMsg:string;
begin
  MakeData;

  ABQueryOutWord_OLE(ABSoftSetPath+'ģ��.dot',
                 ABSoftSetPath+'���.doc',

                 FDataSet,

                 False,
                 0,
                 0,
                 '',

                 true,
                 '',
                 '',

                 tempMsg,
                 true);
end;

procedure TCaleForm.FormCreate(Sender: TObject);
begin
  FDataSet := ABCreateClientDataSet(
    ['N'],
    [],
    [ftString],
    [100]
    );
end;

procedure TCaleForm.FormDestroy(Sender: TObject);
begin
  FDataSet.free;
end;

end.
