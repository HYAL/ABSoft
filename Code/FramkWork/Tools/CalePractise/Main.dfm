object CaleForm: TCaleForm
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = #21152#20943#20056#38500#32451#20064
  ClientHeight = 318
  ClientWidth = 318
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 318
    Height = 277
    Align = alClient
    Caption = #36816#31639#39033#30446#35774#32622
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 49
      Width = 30
      Height = 13
      AutoSize = False
      Caption = #21253#21547
      ParentShowHint = False
      ShowHint = False
    end
    object Label2: TLabel
      Left = 144
      Top = 49
      Width = 145
      Height = 41
      AutoSize = False
      Caption = 
        #20010#25968#25454'.'#22914'2'#21487#20026' 3+2'#13#10'                  3'#21487#20026' 11+2+5'#13#10'                  4'#21487 +
        #20026' 5+2-6*1'#13#10
      ParentShowHint = False
      ShowHint = False
    end
    object Label3: TLabel
      Left = 8
      Top = 77
      Width = 66
      Height = 19
      AutoSize = False
      Caption = #31532'1'#20010#26368#22823#20026
      ParentShowHint = False
      ShowHint = False
    end
    object Label4: TLabel
      Left = 8
      Top = 106
      Width = 66
      Height = 19
      AutoSize = False
      Caption = #31532'2'#20010#26368#22823#20026
      ParentShowHint = False
      ShowHint = False
    end
    object Label5: TLabel
      Left = 8
      Top = 135
      Width = 66
      Height = 19
      AutoSize = False
      Caption = #31532'3'#20010#26368#22823#20026
      ParentShowHint = False
      ShowHint = False
    end
    object Label6: TLabel
      Left = 8
      Top = 164
      Width = 66
      Height = 19
      AutoSize = False
      Caption = #31532'4'#20010#26368#22823#20026
      ParentShowHint = False
      ShowHint = False
    end
    object Label7: TLabel
      Left = 8
      Top = 193
      Width = 66
      Height = 19
      AutoSize = False
      Caption = #31532'5'#20010#26368#22823#20026
      ParentShowHint = False
      ShowHint = False
    end
    object Label8: TLabel
      Left = 8
      Top = 222
      Width = 66
      Height = 19
      AutoSize = False
      Caption = #32467#26524#26368#22823#20026
      ParentShowHint = False
      ShowHint = False
    end
    object Label9: TLabel
      Left = 8
      Top = 250
      Width = 66
      Height = 19
      AutoSize = False
      Caption = #39064#30446#25968#37327#20026
      ParentShowHint = False
      ShowHint = False
    end
    object CheckBox1: TCheckBox
      Tag = 91
      Left = 8
      Top = 20
      Width = 73
      Height = 17
      Caption = #21253#21547#21152#27861
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object CheckBox2: TCheckBox
      Tag = 91
      Left = 76
      Top = 20
      Width = 73
      Height = 17
      Caption = #21253#21547#20943#27861
      TabOrder = 1
    end
    object CheckBox3: TCheckBox
      Tag = 91
      Left = 144
      Top = 20
      Width = 73
      Height = 17
      Caption = #21253#21547#20056#27861
      TabOrder = 2
    end
    object CheckBox4: TCheckBox
      Tag = 91
      Left = 216
      Top = 20
      Width = 73
      Height = 17
      Caption = #21253#21547#38500#27861
      TabOrder = 3
    end
    object SpinEdit1: TSpinEdit
      Tag = 91
      Left = 76
      Top = 46
      Width = 62
      Height = 22
      MaxValue = 5
      MinValue = 2
      TabOrder = 4
      Value = 2
    end
    object SpinEdit2: TSpinEdit
      Tag = 91
      Left = 76
      Top = 74
      Width = 62
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 5
      Value = 5
    end
    object SpinEdit3: TSpinEdit
      Tag = 91
      Left = 76
      Top = 103
      Width = 62
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 6
      Value = 5
    end
    object SpinEdit4: TSpinEdit
      Tag = 91
      Left = 76
      Top = 132
      Width = 62
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 7
      Value = 0
    end
    object SpinEdit5: TSpinEdit
      Tag = 91
      Left = 76
      Top = 161
      Width = 62
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 8
      Value = 0
    end
    object SpinEdit6: TSpinEdit
      Tag = 91
      Left = 76
      Top = 190
      Width = 62
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 9
      Value = 0
    end
    object SpinEdit7: TSpinEdit
      Tag = 91
      Left = 76
      Top = 219
      Width = 62
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 10
      Value = 10
    end
    object SpinEdit8: TSpinEdit
      Tag = 91
      Left = 76
      Top = 247
      Width = 62
      Height = 22
      MaxValue = 0
      MinValue = 0
      TabOrder = 11
      Value = 40
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 277
    Width = 318
    Height = 41
    Align = alBottom
    TabOrder = 1
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 281
      Height = 25
      Caption = #20197#27169#26495#29983#25104#32451#20064#24182#25171#24320#32467#26524
      TabOrder = 0
      OnClick = Button1Click
    end
  end
end
