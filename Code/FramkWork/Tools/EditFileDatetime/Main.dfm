object Form53: TForm53
  Left = 0
  Top = 910
  Caption = #25991#20214#26102#38388#25209#37327#20462#25913#24037#20855#65288#21487#25913#25293#29031#26102#38388#65292#21516#19968#32452#30340#25991#20214#35774#32622#21518#30340#26102#38388#19968#26679#65289
  ClientHeight = 508
  ClientWidth = 963
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 482
    Width = 963
    Height = 26
    Align = alBottom
    TabOrder = 0
    object Button1: TButton
      Left = 1
      Top = 1
      Width = 167
      Height = 24
      Align = alLeft
      Caption = #20462#25913#25152#26377#25991#20214#25152#26377#26085#26399
      TabOrder = 0
      OnClick = Button1Click
    end
    object ProgressBar1: TProgressBar
      Left = 168
      Top = 1
      Width = 794
      Height = 24
      Align = alClient
      TabOrder = 1
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 0
    Width = 321
    Height = 482
    Align = alLeft
    TabOrder = 1
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 319
      Height = 120
      Align = alTop
      Caption = #31532#19968#32452
      TabOrder = 0
      object Panel1: TPanel
        Left = 2
        Top = 87
        Width = 315
        Height = 31
        Align = alBottom
        TabOrder = 0
        object Button2: TButton
          Left = 5
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox1'
          Caption = #22686#21152#25991#20214
          TabOrder = 0
          OnClick = Button2Click
        end
        object cxDateEdit1: TcxDateEdit
          Tag = 91
          Left = 171
          Top = 6
          Properties.DateButtons = [btnClear, btnNow, btnToday]
          Properties.Kind = ckDateTime
          Style.LookAndFeel.Kind = lfFlat
          StyleDisabled.LookAndFeel.Kind = lfFlat
          StyleFocused.LookAndFeel.Kind = lfFlat
          StyleHot.LookAndFeel.Kind = lfFlat
          TabOrder = 1
          Width = 134
        end
        object Button3: TButton
          Left = 85
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox1'
          Caption = #21024#38500#25991#20214
          TabOrder = 2
          OnClick = Button3Click
        end
      end
      object ListBox1: TListBox
        Tag = 910
        Left = 2
        Top = 15
        Width = 315
        Height = 72
        Align = alClient
        ItemHeight = 13
        TabOrder = 1
      end
    end
    object GroupBox2: TGroupBox
      Left = 1
      Top = 121
      Width = 319
      Height = 120
      Align = alTop
      Caption = #31532#20108#32452
      TabOrder = 1
      object Panel2: TPanel
        Left = 2
        Top = 87
        Width = 315
        Height = 31
        Align = alBottom
        TabOrder = 0
        object Button4: TButton
          Left = 5
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox2'
          Caption = #22686#21152#25991#20214
          TabOrder = 0
          OnClick = Button2Click
        end
        object cxDateEdit2: TcxDateEdit
          Tag = 91
          Left = 171
          Top = 6
          Properties.DateButtons = [btnClear, btnNow, btnToday]
          Properties.Kind = ckDateTime
          Style.LookAndFeel.Kind = lfFlat
          StyleDisabled.LookAndFeel.Kind = lfFlat
          StyleFocused.LookAndFeel.Kind = lfFlat
          StyleHot.LookAndFeel.Kind = lfFlat
          TabOrder = 1
          Width = 134
        end
        object Button5: TButton
          Left = 85
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox2'
          Caption = #21024#38500#25991#20214
          TabOrder = 2
          OnClick = Button3Click
        end
      end
      object ListBox2: TListBox
        Tag = 910
        Left = 2
        Top = 15
        Width = 315
        Height = 72
        Align = alClient
        ItemHeight = 13
        TabOrder = 1
      end
    end
    object GroupBox3: TGroupBox
      Left = 1
      Top = 241
      Width = 319
      Height = 120
      Align = alTop
      Caption = #31532#19977#32452
      TabOrder = 2
      object Panel6: TPanel
        Left = 2
        Top = 87
        Width = 315
        Height = 31
        Align = alBottom
        TabOrder = 0
        object Button6: TButton
          Left = 5
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox3'
          Caption = #22686#21152#25991#20214
          TabOrder = 0
          OnClick = Button2Click
        end
        object cxDateEdit3: TcxDateEdit
          Tag = 91
          Left = 171
          Top = 6
          Properties.DateButtons = [btnClear, btnNow, btnToday]
          Properties.Kind = ckDateTime
          Style.LookAndFeel.Kind = lfFlat
          StyleDisabled.LookAndFeel.Kind = lfFlat
          StyleFocused.LookAndFeel.Kind = lfFlat
          StyleHot.LookAndFeel.Kind = lfFlat
          TabOrder = 1
          Width = 134
        end
        object Button7: TButton
          Left = 85
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox3'
          Caption = #21024#38500#25991#20214
          TabOrder = 2
          OnClick = Button3Click
        end
      end
      object ListBox3: TListBox
        Tag = 910
        Left = 2
        Top = 15
        Width = 315
        Height = 72
        Align = alClient
        ItemHeight = 13
        TabOrder = 1
      end
    end
    object GroupBox4: TGroupBox
      Left = 1
      Top = 361
      Width = 319
      Height = 120
      Align = alClient
      Caption = #31532#22235#32452
      TabOrder = 3
      object Panel7: TPanel
        Left = 2
        Top = 87
        Width = 315
        Height = 31
        Align = alBottom
        TabOrder = 0
        object Button8: TButton
          Left = 5
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox4'
          Caption = #22686#21152#25991#20214
          TabOrder = 0
          OnClick = Button2Click
        end
        object cxDateEdit4: TcxDateEdit
          Tag = 91
          Left = 171
          Top = 6
          Properties.DateButtons = [btnClear, btnNow, btnToday]
          Properties.Kind = ckDateTime
          Style.LookAndFeel.Kind = lfFlat
          StyleDisabled.LookAndFeel.Kind = lfFlat
          StyleFocused.LookAndFeel.Kind = lfFlat
          StyleHot.LookAndFeel.Kind = lfFlat
          TabOrder = 1
          Width = 134
        end
        object Button9: TButton
          Left = 85
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox4'
          Caption = #21024#38500#25991#20214
          TabOrder = 2
          OnClick = Button3Click
        end
      end
      object ListBox4: TListBox
        Tag = 910
        Left = 2
        Top = 15
        Width = 315
        Height = 72
        Align = alClient
        ItemHeight = 13
        TabOrder = 1
      end
    end
  end
  object Panel5: TPanel
    Left = 321
    Top = 0
    Width = 321
    Height = 482
    Align = alLeft
    TabOrder = 2
    object GroupBox5: TGroupBox
      Left = 1
      Top = 1
      Width = 319
      Height = 120
      Align = alTop
      Caption = #31532#20116#32452
      TabOrder = 0
      object Panel9: TPanel
        Left = 2
        Top = 87
        Width = 315
        Height = 31
        Align = alBottom
        TabOrder = 0
        object Button10: TButton
          Left = 5
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox5'
          Caption = #22686#21152#25991#20214
          TabOrder = 0
          OnClick = Button2Click
        end
        object cxDateEdit5: TcxDateEdit
          Tag = 91
          Left = 171
          Top = 6
          Properties.DateButtons = [btnClear, btnNow, btnToday]
          Properties.Kind = ckDateTime
          Style.LookAndFeel.Kind = lfFlat
          StyleDisabled.LookAndFeel.Kind = lfFlat
          StyleFocused.LookAndFeel.Kind = lfFlat
          StyleHot.LookAndFeel.Kind = lfFlat
          TabOrder = 1
          Width = 134
        end
        object Button11: TButton
          Left = 85
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox5'
          Caption = #21024#38500#25991#20214
          TabOrder = 2
          OnClick = Button3Click
        end
      end
      object ListBox5: TListBox
        Tag = 910
        Left = 2
        Top = 15
        Width = 315
        Height = 72
        Align = alClient
        ItemHeight = 13
        TabOrder = 1
      end
    end
    object GroupBox6: TGroupBox
      Left = 1
      Top = 121
      Width = 319
      Height = 120
      Align = alTop
      Caption = #31532#20845#32452
      TabOrder = 1
      object Panel10: TPanel
        Left = 2
        Top = 87
        Width = 315
        Height = 31
        Align = alBottom
        TabOrder = 0
        object Button12: TButton
          Left = 5
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox6'
          Caption = #22686#21152#25991#20214
          TabOrder = 0
          OnClick = Button2Click
        end
        object cxDateEdit6: TcxDateEdit
          Tag = 91
          Left = 171
          Top = 6
          Properties.DateButtons = [btnClear, btnNow, btnToday]
          Properties.Kind = ckDateTime
          Style.LookAndFeel.Kind = lfFlat
          StyleDisabled.LookAndFeel.Kind = lfFlat
          StyleFocused.LookAndFeel.Kind = lfFlat
          StyleHot.LookAndFeel.Kind = lfFlat
          TabOrder = 1
          Width = 134
        end
        object Button13: TButton
          Left = 85
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox6'
          Caption = #21024#38500#25991#20214
          TabOrder = 2
          OnClick = Button3Click
        end
      end
      object ListBox6: TListBox
        Tag = 910
        Left = 2
        Top = 15
        Width = 315
        Height = 72
        Align = alClient
        ItemHeight = 13
        TabOrder = 1
      end
    end
    object GroupBox7: TGroupBox
      Left = 1
      Top = 241
      Width = 319
      Height = 120
      Align = alTop
      Caption = #31532#19971#32452
      TabOrder = 2
      object Panel8: TPanel
        Left = 2
        Top = 87
        Width = 315
        Height = 31
        Align = alBottom
        TabOrder = 0
        object Button14: TButton
          Left = 5
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox7'
          Caption = #22686#21152#25991#20214
          TabOrder = 0
          OnClick = Button2Click
        end
        object cxDateEdit7: TcxDateEdit
          Tag = 91
          Left = 171
          Top = 6
          Properties.DateButtons = [btnClear, btnNow, btnToday]
          Properties.Kind = ckDateTime
          Style.LookAndFeel.Kind = lfFlat
          StyleDisabled.LookAndFeel.Kind = lfFlat
          StyleFocused.LookAndFeel.Kind = lfFlat
          StyleHot.LookAndFeel.Kind = lfFlat
          TabOrder = 1
          Width = 134
        end
        object Button15: TButton
          Left = 85
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox7'
          Caption = #21024#38500#25991#20214
          TabOrder = 2
          OnClick = Button3Click
        end
      end
      object ListBox7: TListBox
        Tag = 910
        Left = 2
        Top = 15
        Width = 315
        Height = 72
        Align = alClient
        ItemHeight = 13
        TabOrder = 1
      end
    end
    object GroupBox8: TGroupBox
      Left = 1
      Top = 361
      Width = 319
      Height = 120
      Align = alClient
      Caption = #31532#20843#32452
      TabOrder = 3
      object Panel11: TPanel
        Left = 2
        Top = 87
        Width = 315
        Height = 31
        Align = alBottom
        TabOrder = 0
        object Button16: TButton
          Left = 5
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox8'
          Caption = #22686#21152#25991#20214
          TabOrder = 0
          OnClick = Button2Click
        end
        object cxDateEdit8: TcxDateEdit
          Tag = 91
          Left = 171
          Top = 6
          Properties.DateButtons = [btnClear, btnNow, btnToday]
          Properties.Kind = ckDateTime
          Style.LookAndFeel.Kind = lfFlat
          StyleDisabled.LookAndFeel.Kind = lfFlat
          StyleFocused.LookAndFeel.Kind = lfFlat
          StyleHot.LookAndFeel.Kind = lfFlat
          TabOrder = 1
          Width = 134
        end
        object Button17: TButton
          Left = 85
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox8'
          Caption = #21024#38500#25991#20214
          TabOrder = 2
          OnClick = Button3Click
        end
      end
      object ListBox8: TListBox
        Tag = 910
        Left = 2
        Top = 15
        Width = 315
        Height = 72
        Align = alClient
        ItemHeight = 13
        TabOrder = 1
      end
    end
  end
  object Panel12: TPanel
    Left = 642
    Top = 0
    Width = 321
    Height = 482
    Align = alLeft
    TabOrder = 3
    object GroupBox9: TGroupBox
      Left = 1
      Top = 1
      Width = 319
      Height = 120
      Align = alTop
      Caption = #31532#20061#32452
      TabOrder = 0
      object Panel13: TPanel
        Left = 2
        Top = 87
        Width = 315
        Height = 31
        Align = alBottom
        TabOrder = 0
        object Button18: TButton
          Left = 5
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox9'
          Caption = #22686#21152#25991#20214
          TabOrder = 0
          OnClick = Button2Click
        end
        object cxDateEdit9: TcxDateEdit
          Tag = 91
          Left = 171
          Top = 6
          Properties.DateButtons = [btnClear, btnNow, btnToday]
          Properties.Kind = ckDateTime
          Style.LookAndFeel.Kind = lfFlat
          StyleDisabled.LookAndFeel.Kind = lfFlat
          StyleFocused.LookAndFeel.Kind = lfFlat
          StyleHot.LookAndFeel.Kind = lfFlat
          TabOrder = 1
          Width = 134
        end
        object Button19: TButton
          Left = 85
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox9'
          Caption = #21024#38500#25991#20214
          TabOrder = 2
          OnClick = Button3Click
        end
      end
      object ListBox9: TListBox
        Tag = 910
        Left = 2
        Top = 15
        Width = 315
        Height = 72
        Align = alClient
        ItemHeight = 13
        TabOrder = 1
      end
    end
    object GroupBox10: TGroupBox
      Left = 1
      Top = 121
      Width = 319
      Height = 120
      Align = alTop
      Caption = #31532#21313#32452
      TabOrder = 1
      object Panel14: TPanel
        Left = 2
        Top = 87
        Width = 315
        Height = 31
        Align = alBottom
        TabOrder = 0
        object Button20: TButton
          Left = 5
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox10'
          Caption = #22686#21152#25991#20214
          TabOrder = 0
          OnClick = Button2Click
        end
        object cxDateEdit10: TcxDateEdit
          Tag = 91
          Left = 171
          Top = 6
          Properties.DateButtons = [btnClear, btnNow, btnToday]
          Properties.Kind = ckDateTime
          Style.LookAndFeel.Kind = lfFlat
          StyleDisabled.LookAndFeel.Kind = lfFlat
          StyleFocused.LookAndFeel.Kind = lfFlat
          StyleHot.LookAndFeel.Kind = lfFlat
          TabOrder = 1
          Width = 134
        end
        object Button21: TButton
          Left = 85
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox10'
          Caption = #21024#38500#25991#20214
          TabOrder = 2
          OnClick = Button3Click
        end
      end
      object ListBox10: TListBox
        Tag = 910
        Left = 2
        Top = 15
        Width = 315
        Height = 72
        Align = alClient
        ItemHeight = 13
        TabOrder = 1
      end
    end
    object GroupBox11: TGroupBox
      Left = 1
      Top = 241
      Width = 319
      Height = 120
      Align = alTop
      Caption = #31532#21313#19968#32452
      TabOrder = 2
      object Panel15: TPanel
        Left = 2
        Top = 87
        Width = 315
        Height = 31
        Align = alBottom
        TabOrder = 0
        object Button22: TButton
          Left = 5
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox11'
          Caption = #22686#21152#25991#20214
          TabOrder = 0
          OnClick = Button2Click
        end
        object cxDateEdit11: TcxDateEdit
          Tag = 91
          Left = 171
          Top = 6
          Properties.DateButtons = [btnClear, btnNow, btnToday]
          Properties.Kind = ckDateTime
          Style.LookAndFeel.Kind = lfFlat
          StyleDisabled.LookAndFeel.Kind = lfFlat
          StyleFocused.LookAndFeel.Kind = lfFlat
          StyleHot.LookAndFeel.Kind = lfFlat
          TabOrder = 1
          Width = 134
        end
        object Button23: TButton
          Left = 85
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox11'
          Caption = #21024#38500#25991#20214
          TabOrder = 2
          OnClick = Button3Click
        end
      end
      object ListBox11: TListBox
        Tag = 910
        Left = 2
        Top = 15
        Width = 315
        Height = 72
        Align = alClient
        ItemHeight = 13
        TabOrder = 1
      end
    end
    object GroupBox12: TGroupBox
      Left = 1
      Top = 361
      Width = 319
      Height = 120
      Align = alClient
      Caption = #31532#21313#20108#32452
      TabOrder = 3
      object Panel16: TPanel
        Left = 2
        Top = 87
        Width = 315
        Height = 31
        Align = alBottom
        TabOrder = 0
        object Button24: TButton
          Left = 5
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox12'
          Caption = #22686#21152#25991#20214
          TabOrder = 0
          OnClick = Button2Click
        end
        object cxDateEdit12: TcxDateEdit
          Tag = 91
          Left = 171
          Top = 6
          Properties.DateButtons = [btnClear, btnNow, btnToday]
          Properties.Kind = ckDateTime
          Style.LookAndFeel.Kind = lfFlat
          StyleDisabled.LookAndFeel.Kind = lfFlat
          StyleFocused.LookAndFeel.Kind = lfFlat
          StyleHot.LookAndFeel.Kind = lfFlat
          TabOrder = 1
          Width = 134
        end
        object Button25: TButton
          Left = 85
          Top = 4
          Width = 80
          Height = 25
          Hint = 'ListBox12'
          Caption = #21024#38500#25991#20214
          TabOrder = 2
          OnClick = Button3Click
        end
      end
      object ListBox12: TListBox
        Tag = 910
        Left = 2
        Top = 15
        Width = 315
        Height = 72
        Align = alClient
        ItemHeight = 13
        TabOrder = 1
      end
    end
  end
end
