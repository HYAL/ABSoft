unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls,ABPubFormU,ABPubFuncU, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxCore, cxDateUtils, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar;

type
  TForm53 = class(TABPubForm)
    Panel3: TPanel;
    Button1: TButton;
    ProgressBar1: TProgressBar;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Button2: TButton;
    cxDateEdit1: TcxDateEdit;
    Button3: TButton;
    ListBox1: TListBox;
    GroupBox2: TGroupBox;
    Panel2: TPanel;
    Button4: TButton;
    cxDateEdit2: TcxDateEdit;
    Button5: TButton;
    ListBox2: TListBox;
    Panel5: TPanel;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    Button6: TButton;
    cxDateEdit3: TcxDateEdit;
    Button7: TButton;
    ListBox3: TListBox;
    GroupBox5: TGroupBox;
    Panel9: TPanel;
    Button10: TButton;
    cxDateEdit5: TcxDateEdit;
    Button11: TButton;
    ListBox5: TListBox;
    GroupBox6: TGroupBox;
    Panel10: TPanel;
    Button12: TButton;
    cxDateEdit6: TcxDateEdit;
    Button13: TButton;
    ListBox6: TListBox;
    GroupBox4: TGroupBox;
    Panel7: TPanel;
    Button8: TButton;
    cxDateEdit4: TcxDateEdit;
    Button9: TButton;
    ListBox4: TListBox;
    GroupBox7: TGroupBox;
    Panel8: TPanel;
    Button14: TButton;
    cxDateEdit7: TcxDateEdit;
    Button15: TButton;
    ListBox7: TListBox;
    GroupBox8: TGroupBox;
    Panel11: TPanel;
    Button16: TButton;
    cxDateEdit8: TcxDateEdit;
    Button17: TButton;
    ListBox8: TListBox;
    Panel12: TPanel;
    GroupBox9: TGroupBox;
    Panel13: TPanel;
    Button18: TButton;
    cxDateEdit9: TcxDateEdit;
    Button19: TButton;
    ListBox9: TListBox;
    GroupBox10: TGroupBox;
    Panel14: TPanel;
    Button20: TButton;
    cxDateEdit10: TcxDateEdit;
    Button21: TButton;
    ListBox10: TListBox;
    GroupBox11: TGroupBox;
    Panel15: TPanel;
    Button22: TButton;
    cxDateEdit11: TcxDateEdit;
    Button23: TButton;
    ListBox11: TListBox;
    GroupBox12: TGroupBox;
    Panel16: TPanel;
    Button24: TButton;
    cxDateEdit12: TcxDateEdit;
    Button25: TButton;
    ListBox12: TListBox;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    procedure ListBoxAdd(aListBox: TListBox);
    procedure ListBoxDel(aListBox: TListBox);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form53: TForm53;

implementation

{$R *.dfm}

procedure TForm53.Button1Click(Sender: TObject);
var
  tempListBox:TListBox;
  tempDatetime:TDatetime;
  I,j: Integer;
begin
  ProgressBar1.Position:=0;
  ProgressBar1.Max:=ListBox1.Count+ListBox2.Count+ListBox3.Count+
                    ListBox4.Count+ListBox5.Count+ListBox6.Count+
                    ListBox7.Count+ListBox8.Count+ListBox9.Count+
                    ListBox10.Count+ListBox11.Count+ListBox12.Count;
  try
    for I := 1 to 12 do
    begin
      tempListBox:=TListBox(FindComponent('ListBox'+inttostr(i)));
      tempDatetime:=TcxDateEdit(FindComponent('cxDateEdit'+inttostr(i))).Date;
      if tempDatetime>0 then
      begin
        for j := 0 to tempListBox.Items.Count-1 do
        begin
          ProgressBar1.Position:=ProgressBar1.Position+1;
          if (ABCheckFileExists(tempListBox.Items[j])) then
          begin
            ABReadOrEditFileAllDatetime(tempListBox.Items[j],tempDatetime);
            ABSetFileDateTime(tempListBox.Items[j],tempDatetime,tempDatetime,tempDatetime);
          end;
        end;
      end;
    end;
    ShowMessage('�������');
  finally
    ProgressBar1.Position:=0;
  end;
end;

procedure TForm53.Button2Click(Sender: TObject);
var
  tempListBox:TListBox;
begin
  tempListBox:=TListBox(FindComponent(TButton(Sender).hint));
  ListBoxAdd(tempListBox);
end;

procedure TForm53.Button3Click(Sender: TObject);
var
  tempListBox:TListBox;
begin
  tempListBox:=TListBox(FindComponent(TButton(Sender).hint));
  ListBoxDel(tempListBox);
end;

procedure TForm53.ListBoxAdd(aListBox: TListBox);
var
  tempFileName:string;
  tempMultiFiles:TStrings;
  I: Integer;
begin
  tempMultiFiles := TStringList.Create;
  try
    tempMultiFiles.Clear;
    ABSelectFile('','','jpg file|*.jpg|jpeg file|*.jpeg|all file|*.*',True,tempMultiFiles);
    if tempMultiFiles.Count>0 then
    begin
      for I := 0 to tempMultiFiles.Count-1 do
      begin
        tempFileName:=tempMultiFiles.Strings[i];

        if (ABCheckFileExists(tempFileName)) and
           (aListBox.Items.IndexOf(tempFileName)<0) then
        begin
          aListBox.Items.Add(tempFileName);
        end;
      end;
    end;
  finally
    tempMultiFiles.Free;
  end;
end;

procedure TForm53.ListBoxDel(aListBox: TListBox);
var
  i:LongInt;
begin
  i:=aListBox.ItemIndex;
  if i>=0 then
  begin
    aListBox.Items.Delete(i);
    if i<aListBox.count then
      aListBox.ItemIndex:=i;
  end;
end;

end.
