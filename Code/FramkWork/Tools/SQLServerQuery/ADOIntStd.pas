unit ADOIntStd;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 2007-04-14 22:12:00 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Program Files\Common Files\system\ado\msado20.tlb (1)
// LIBID: {00000200-0000-0010-8000-00AA006D2EA4}
// LCID: 0
// Helpfile: 
// HelpString: Microsoft ActiveX Data Objects 2.0 Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// Errors:
//   Hint: TypeInfo 'Property' changed to 'Property_'
//   Hint: Parameter 'Object' of _DynaCollection.Append changed to 'Object_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Parameter 'Type' of _Command15.CreateParameter changed to 'Type_'
//   Hint: Parameter 'Type' of Fields.Append changed to 'Type_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: TypeInfo 'Property' changed to 'Property_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Hint: Symbol 'Type' renamed to 'type_'
//   Error creating palette bitmap of (TConnection) : Server C:\Program Files\Common Files\system\ado\msado15.dll contains no icons
//   Error creating palette bitmap of (TCommand) : Server C:\Program Files\Common Files\system\ado\msado15.dll contains no icons
//   Error creating palette bitmap of (TRecordset) : Server C:\Program Files\Common Files\system\ado\msado15.dll contains no icons
//   Error creating palette bitmap of (TParameter) : Server C:\Program Files\Common Files\system\ado\msado15.dll contains no icons
// ************************************************************************ //
// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses


  Windows,ActiveX,Classes,Graphics,OleServer,StdVCL,Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ADODBMajorVersion = 2;
  ADODBMinorVersion = 0;

  LIBID_ADODB: TGUID = '{00000200-0000-0010-8000-00AA006D2EA4}';

  IID__Collection: TGUID = '{00000512-0000-0010-8000-00AA006D2EA4}';
  IID__DynaCollection: TGUID = '{00000513-0000-0010-8000-00AA006D2EA4}';
  IID__ADO: TGUID = '{00000534-0000-0010-8000-00AA006D2EA4}';
  IID_Properties: TGUID = '{00000504-0000-0010-8000-00AA006D2EA4}';
  IID_Property_: TGUID = '{00000503-0000-0010-8000-00AA006D2EA4}';
  IID_Error: TGUID = '{00000500-0000-0010-8000-00AA006D2EA4}';
  IID_Errors: TGUID = '{00000501-0000-0010-8000-00AA006D2EA4}';
  IID__Command15: TGUID = '{00000508-0000-0010-8000-00AA006D2EA4}';
  IID__Connection15: TGUID = '{00000515-0000-0010-8000-00AA006D2EA4}';
  IID__Connection: TGUID = '{00000550-0000-0010-8000-00AA006D2EA4}';
  IID__Recordset15: TGUID = '{0000050E-0000-0010-8000-00AA006D2EA4}';
  IID__Recordset: TGUID = '{0000054F-0000-0010-8000-00AA006D2EA4}';
  IID_Fields15: TGUID = '{00000506-0000-0010-8000-00AA006D2EA4}';
  IID_Fields: TGUID = '{0000054D-0000-0010-8000-00AA006D2EA4}';
  IID_Field: TGUID = '{0000054C-0000-0010-8000-00AA006D2EA4}';
  IID__Parameter: TGUID = '{0000050C-0000-0010-8000-00AA006D2EA4}';
  IID_Parameters: TGUID = '{0000050D-0000-0010-8000-00AA006D2EA4}';
  IID__Command: TGUID = '{0000054E-0000-0010-8000-00AA006D2EA4}';
  IID_ConnectionEventsVt: TGUID = '{00000402-0000-0010-8000-00AA006D2EA4}';
  IID_RecordsetEventsVt: TGUID = '{00000403-0000-0010-8000-00AA006D2EA4}';
  DIID_ConnectionEvents: TGUID = '{00000400-0000-0010-8000-00AA006D2EA4}';
  DIID_RecordsetEvents: TGUID = '{00000266-0000-0010-8000-00AA006D2EA4}';
  IID_ADOConnectionConstruction15: TGUID = '{00000516-0000-0010-8000-00AA006D2EA4}';
  IID_ADOConnectionConstruction: TGUID = '{00000551-0000-0010-8000-00AA006D2EA4}';
  CLASS_Connection: TGUID = '{00000514-0000-0010-8000-00AA006D2EA4}';
  IID_ADOCommandConstruction: TGUID = '{00000517-0000-0010-8000-00AA006D2EA4}';
  CLASS_Command: TGUID = '{00000507-0000-0010-8000-00AA006D2EA4}';
  CLASS_Recordset: TGUID = '{00000535-0000-0010-8000-00AA006D2EA4}';
  IID_ADORecordsetConstruction: TGUID = '{00000283-0000-0010-8000-00AA006D2EA4}';
  IID_Field15: TGUID = '{00000505-0000-0010-8000-00AA006D2EA4}';
  CLASS_Parameter: TGUID = '{0000050B-0000-0010-8000-00AA006D2EA4}';

// *********************************************************************//
// Declaration of Enumerations defined in Type Library                    
// *********************************************************************//
// Constants for enum CursorTypeEnum
type
  CursorTypeEnum = TOleEnum;
const
  adOpenUnspecified = $FFFFFFFF;
  adOpenForwardOnly = $00000000;
  adOpenKeyset = $00000001;
  adOpenDynamic = $00000002;
  adOpenStatic = $00000003;

// Constants for enum CursorOptionEnum
type
  CursorOptionEnum = TOleEnum;
const
  adHoldRecords = $00000100;
  adMovePrevious = $00000200;
  adAddNew = $01000400;
  adDelete = $01000800;
  adUpdate = $01008000;
  adBookmark = $00002000;
  adApproxPosition = $00004000;
  adUpdateBatch = $00010000;
  adResync = $00020000;
  adNotify = $00040000;
  adFind = $00080000;

// Constants for enum LockTypeEnum
type
  LockTypeEnum = TOleEnum;
const
  adLockUnspecified = $FFFFFFFF;
  adLockReadOnly = $00000001;
  adLockPessimistic = $00000002;
  adLockOptimistic = $00000003;
  adLockBatchOptimistic = $00000004;

// Constants for enum ExecuteOptionEnum
type
  ExecuteOptionEnum = TOleEnum;
const
  adOptionUnspecified = $FFFFFFFF;
  adAsyncExecute = $00000010;
  adAsyncFetch = $00000020;
  adAsyncFetchNonBlocking = $00000040;
  adExecuteNoRecords = $00000080;

// Constants for enum ConnectOptionEnum
type
  ConnectOptionEnum = TOleEnum;
const
  adConnectUnspecified = $FFFFFFFF;
  adAsyncConnect = $00000010;

// Constants for enum ObjectStateEnum
type
  ObjectStateEnum = TOleEnum;
const
  adStateClosed = $00000000;
  adStateOpen = $00000001;
  adStateConnecting = $00000002;
  adStateExecuting = $00000004;
  adStateFetching = $00000008;

// Constants for enum CursorLocationEnum
type
  CursorLocationEnum = TOleEnum;
const
  adUseNone = $00000001;
  adUseServer = $00000002;
  adUseClient = $00000003;
  adUseClientBatch = $00000003;

// Constants for enum DataTypeEnum
type
  DataTypeEnum = TOleEnum;
const
  adEmpty = $00000000;
  adTinyInt = $00000010;
  adSmallInt = $00000002;
  adInteger = $00000003;
  adBigInt = $00000014;
  adUnsignedTinyInt = $00000011;
  adUnsignedSmallInt = $00000012;
  adUnsignedInt = $00000013;
  adUnsignedBigInt = $00000015;
  adSingle = $00000004;
  adDouble = $00000005;
  adCurrency = $00000006;
  adDecimal = $0000000E;
  adNumeric = $00000083;
  adBoolean = $0000000B;
  adError = $0000000A;
  adUserDefined = $00000084;
  adVariant = $0000000C;
  adIDispatch = $00000009;
  adIUnknown = $0000000D;
  adGUID = $00000048;
  adDate = $00000007;
  adDBDate = $00000085;
  adDBTime = $00000086;
  adDBTimeStamp = $00000087;
  adBSTR = $00000008;
  adChar = $00000081;
  adVarChar = $000000C8;
  adLongVarChar = $000000C9;
  adWChar = $00000082;
  adVarWChar = $000000CA;
  adLongVarWChar = $000000CB;
  adBinary = $00000080;
  adVarBinary = $000000CC;
  adLongVarBinary = $000000CD;
  adChapter = $00000088;
  adFileTime = $00000040;
  adDBFileTime = $00000089;
  adPropVariant = $0000008A;
  adVarNumeric = $0000008B;

// Constants for enum FieldAttributeEnum
type
  FieldAttributeEnum = TOleEnum;
const
  adFldUnspecified = $FFFFFFFF;
  adFldMayDefer = $00000002;
  adFldUpdatable = $00000004;
  adFldUnknownUpdatable = $00000008;
  adFldFixed = $00000010;
  adFldIsNullable = $00000020;
  adFldMayBeNull = $00000040;
  adFldLong = $00000080;
  adFldRowID = $00000100;
  adFldRowVersion = $00000200;
  adFldCacheDeferred = $00001000;
  adFldNegativeScale = $00004000;
  adFldKeyColumn = $00008000;

// Constants for enum EditModeEnum
type
  EditModeEnum = TOleEnum;
const
  adEditNone = $00000000;
  adEditInProgress = $00000001;
  adEditAdd = $00000002;
  adEditDelete = $00000004;

// Constants for enum RecordStatusEnum
type
  RecordStatusEnum = TOleEnum;
const
  adRecOK = $00000000;
  adRecNew = $00000001;
  adRecModified = $00000002;
  adRecDeleted = $00000004;
  adRecUnmodified = $00000008;
  adRecInvalid = $00000010;
  adRecMultipleChanges = $00000040;
  adRecPendingChanges = $00000080;
  adRecCanceled = $00000100;
  adRecCantRelease = $00000400;
  adRecConcurrencyViolation = $00000800;
  adRecIntegrityViolation = $00001000;
  adRecMaxChangesExceeded = $00002000;
  adRecObjectOpen = $00004000;
  adRecOutOfMemory = $00008000;
  adRecPermissionDenied = $00010000;
  adRecSchemaViolation = $00020000;
  adRecDBDeleted = $00040000;

// Constants for enum GetRowsOptionEnum
type
  GetRowsOptionEnum = TOleEnum;
const
  adGetRowsRest = $FFFFFFFF;

// Constants for enum PositionEnum
type
  PositionEnum = TOleEnum;
const
  adPosUnknown = $FFFFFFFF;
  adPosBOF = $FFFFFFFE;
  adPosEOF = $FFFFFFFD;

// Constants for enum BookmarkEnum
type
  BookmarkEnum = TOleEnum;
const
  adBookmarkCurrent = $00000000;
  adBookmarkFirst = $00000001;
  adBookmarkLast = $00000002;

// Constants for enum MarshalOptionsEnum
type
  MarshalOptionsEnum = TOleEnum;
const
  adMarshalAll = $00000000;
  adMarshalModifiedOnly = $00000001;

// Constants for enum AffectEnum
type
  AffectEnum = TOleEnum;
const
  adAffectCurrent = $00000001;
  adAffectGroup = $00000002;
  adAffectAll = $00000003;
  adAffectAllChapters = $00000004;

// Constants for enum ResyncEnum
type
  ResyncEnum = TOleEnum;
const
  adResyncUnderlyingValues = $00000001;
  adResyncAllValues = $00000002;

// Constants for enum CompareEnum
type
  CompareEnum = TOleEnum;
const
  adCompareLessThan = $00000000;
  adCompareEqual = $00000001;
  adCompareGreaterThan = $00000002;
  adCompareNotEqual = $00000003;
  adCompareNotComparable = $00000004;

// Constants for enum FilterGroupEnum
type
  FilterGroupEnum = TOleEnum;
const
  adFilterNone = $00000000;
  adFilterPendingRecords = $00000001;
  adFilterAffectedRecords = $00000002;
  adFilterFetchedRecords = $00000003;
  adFilterPredicate = $00000004;
  adFilterConflictingRecords = $00000005;

// Constants for enum SearchDirectionEnum
type
  SearchDirectionEnum = TOleEnum;
const
  adSearchForward = $00000001;
  adSearchBackward = $FFFFFFFF;

// Constants for enum PersistFormatEnum
type
  PersistFormatEnum = TOleEnum;
const
  adPersistADTG = $00000000;
  adPersistXML = $00000001;

// Constants for enum StringFormatEnum
type
  StringFormatEnum = TOleEnum;
const
  adClipString = $00000002;

// Constants for enum ADCPROP_UPDATECRITERIA_ENUM
type
  ADCPROP_UPDATECRITERIA_ENUM = TOleEnum;
const
  adCriteriaKey = $00000000;
  adCriteriaAllCols = $00000001;
  adCriteriaUpdCols = $00000002;
  adCriteriaTimeStamp = $00000003;

// Constants for enum ADCPROP_ASYNCTHREADPRIORITY_ENUM
type
  ADCPROP_ASYNCTHREADPRIORITY_ENUM = TOleEnum;
const
  adPriorityLowest = $00000001;
  adPriorityBelowNormal = $00000002;
  adPriorityNormal = $00000003;
  adPriorityAboveNormal = $00000004;
  adPriorityHighest = $00000005;

// Constants for enum ConnectPromptEnum
type
  ConnectPromptEnum = TOleEnum;
const
  adPromptAlways = $00000001;
  adPromptComplete = $00000002;
  adPromptCompleteRequired = $00000003;
  adPromptNever = $00000004;

// Constants for enum ConnectModeEnum
type
  ConnectModeEnum = TOleEnum;
const
  adModeUnknown = $00000000;
  adModeRead = $00000001;
  adModeWrite = $00000002;
  adModeReadWrite = $00000003;
  adModeShareDenyRead = $00000004;
  adModeShareDenyWrite = $00000008;
  adModeShareExclusive = $0000000C;
  adModeShareDenyNone = $00000010;

// Constants for enum IsolationLevelEnum
type
  IsolationLevelEnum = TOleEnum;
const
  adXactUnspecified = $FFFFFFFF;
  adXactChaos = $00000010;
  adXactReadUncommitted = $00000100;
  adXactBrowse = $00000100;
  adXactCursorStability = $00001000;
  adXactReadCommitted = $00001000;
  adXactRepeatableRead = $00010000;
  adXactSerializable = $00100000;
  adXactIsolated = $00100000;

// Constants for enum XactAttributeEnum
type
  XactAttributeEnum = TOleEnum;
const
  adXactCommitRetaining = $00020000;
  adXactAbortRetaining = $00040000;
  adXactAsyncPhaseOne = $00080000;
  adXactSyncPhaseOne = $00100000;

// Constants for enum PropertyAttributesEnum
type
  PropertyAttributesEnum = TOleEnum;
const
  adPropNotSupported = $00000000;
  adPropRequired = $00000001;
  adPropOptional = $00000002;
  adPropRead = $00000200;
  adPropWrite = $00000400;

// Constants for enum ErrorValueEnum
type
  ErrorValueEnum = TOleEnum;
const
  adErrInvalidArgument = $00000BB9;
  adErrNoCurrentRecord = $00000BCD;
  adErrIllegalOperation = $00000C93;
  adErrInTransaction = $00000CAE;
  adErrFeatureNotAvailable = $00000CB3;
  adErrItemNotFound = $00000CC1;
  adErrObjectInCollection = $00000D27;
  adErrObjectNotSet = $00000D5C;
  adErrDataConversion = $00000D5D;
  adErrObjectClosed = $00000E78;
  adErrObjectOpen = $00000E79;
  adErrProviderNotFound = $00000E7A;
  adErrBoundToCommand = $00000E7B;
  adErrInvalidParamInfo = $00000E7C;
  adErrInvalidConnection = $00000E7D;
  adErrNotReentrant = $00000E7E;
  adErrStillExecuting = $00000E7F;
  adErrOperationCancelled = $00000E80;
  adErrStillConnecting = $00000E81;
  adErrNotExecuting = $00000E83;
  adErrUnsafeOperation = $00000E84;

// Constants for enum ParameterAttributesEnum
type
  ParameterAttributesEnum = TOleEnum;
const
  adParamSigned = $00000010;
  adParamNullable = $00000040;
  adParamLong = $00000080;

// Constants for enum ParameterDirectionEnum
type
  ParameterDirectionEnum = TOleEnum;
const
  adParamUnknown = $00000000;
  adParamInput = $00000001;
  adParamOutput = $00000002;
  adParamInputOutput = $00000003;
  adParamReturnValue = $00000004;

// Constants for enum CommandTypeEnum
type
  CommandTypeEnum = TOleEnum;
const
  adCmdUnspecified = $FFFFFFFF;
  adCmdUnknown = $00000008;
  adCmdText = $00000001;
  adCmdTable = $00000002;
  adCmdStoredProc = $00000004;
  adCmdFile = $00000100;
  adCmdTableDirect = $00000200;

// Constants for enum EventStatusEnum
type
  EventStatusEnum = TOleEnum;
const
  adStatusOK = $00000001;
  adStatusErrorsOccurred = $00000002;
  adStatusCantDeny = $00000003;
  adStatusCancel = $00000004;
  adStatusUnwantedEvent = $00000005;

// Constants for enum EventReasonEnum
type
  EventReasonEnum = TOleEnum;
const
  adRsnAddNew = $00000001;
  adRsnDelete = $00000002;
  adRsnUpdate = $00000003;
  adRsnUndoUpdate = $00000004;
  adRsnUndoAddNew = $00000005;
  adRsnUndoDelete = $00000006;
  adRsnRequery = $00000007;
  adRsnResynch = $00000008;
  adRsnClose = $00000009;
  adRsnMove = $0000000A;
  adRsnFirstChange = $0000000B;
  adRsnMoveFirst = $0000000C;
  adRsnMoveNext = $0000000D;
  adRsnMovePrevious = $0000000E;
  adRsnMoveLast = $0000000F;

// Constants for enum SchemaEnum
type
  SchemaEnum = TOleEnum;
const
  adSchemaProviderSpecific = $FFFFFFFF;
  adSchemaAsserts = $00000000;
  adSchemaCatalogs = $00000001;
  adSchemaCharacterSets = $00000002;
  adSchemaCollations = $00000003;
  adSchemaColumns = $00000004;
  adSchemaCheckConstraints = $00000005;
  adSchemaConstraintColumnUsage = $00000006;
  adSchemaConstraintTableUsage = $00000007;
  adSchemaKeyColumnUsage = $00000008;
  adSchemaReferentialContraints = $00000009;
  adSchemaTableConstraints = $0000000A;
  adSchemaColumnsDomainUsage = $0000000B;
  adSchemaIndexes = $0000000C;
  adSchemaColumnPrivileges = $0000000D;
  adSchemaTablePrivileges = $0000000E;
  adSchemaUsagePrivileges = $0000000F;
  adSchemaProcedures = $00000010;
  adSchemaSchemata = $00000011;
  adSchemaSQLLanguages = $00000012;
  adSchemaStatistics = $00000013;
  adSchemaTables = $00000014;
  adSchemaTranslations = $00000015;
  adSchemaProviderTypes = $00000016;
  adSchemaViews = $00000017;
  adSchemaViewColumnUsage = $00000018;
  adSchemaViewTableUsage = $00000019;
  adSchemaProcedureParameters = $0000001A;
  adSchemaForeignKeys = $0000001B;
  adSchemaPrimaryKeys = $0000001C;
  adSchemaProcedureColumns = $0000001D;
  adSchemaDBInfoKeywords = $0000001E;
  adSchemaDBInfoLiterals = $0000001F;
  adSchemaCubes = $00000020;
  adSchemaDimensions = $00000021;
  adSchemaHierarchies = $00000022;
  adSchemaLevels = $00000023;
  adSchemaMeasures = $00000024;
  adSchemaProperties = $00000025;
  adSchemaMembers = $00000026;

type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  _Collection = interface;
  _CollectionDisp = dispinterface;
  _DynaCollection = interface;
  _DynaCollectionDisp = dispinterface;
  _ADO = interface;
  _ADODisp = dispinterface;
  Properties = interface;
  PropertiesDisp = dispinterface;
  Property_ = interface;
  Property_Disp = dispinterface;
  Error = interface;
  ErrorDisp = dispinterface;
  Errors = interface;
  ErrorsDisp = dispinterface;
  _Command15 = interface;
  _Command15Disp = dispinterface;
  _Connection15 = interface;
  _Connection15Disp = dispinterface;
  _Connection = interface;
  _ConnectionDisp = dispinterface;
  _Recordset15 = interface;
  _Recordset15Disp = dispinterface;
  _Recordset = interface;
  _RecordsetDisp = dispinterface;
  Fields15 = interface;
  Fields15Disp = dispinterface;
  Fields = interface;
  FieldsDisp = dispinterface;
  Field = interface;
  FieldDisp = dispinterface;
  _Parameter = interface;
  _ParameterDisp = dispinterface;
  Parameters = interface;
  ParametersDisp = dispinterface;
  _Command = interface;
  _CommandDisp = dispinterface;
  ConnectionEventsVt = interface;
  RecordsetEventsVt = interface;
  ConnectionEvents = dispinterface;
  RecordsetEvents = dispinterface;
  ADOConnectionConstruction15 = interface;
  ADOConnectionConstruction = interface;
  ADOCommandConstruction = interface;
  ADORecordsetConstruction = interface;
  Field15 = interface;
  Field15Disp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Connection = _Connection;
  Command = _Command;
  Recordset = _Recordset;
  Parameter = _Parameter;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  POleVariant1 = ^OleVariant; {*}

  SearchDirection = SearchDirectionEnum; 

// *********************************************************************//
// Interface: _Collection
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000512-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _Collection = interface(IDispatch)
    ['{00000512-0000-0010-8000-00AA006D2EA4}']
    function Get_Count(out c: Integer): HResult; stdcall;
    function _NewEnum(out ppvObject: IUnknown): HResult; stdcall;
    function Refresh: HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  _CollectionDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000512-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _CollectionDisp = dispinterface
    ['{00000512-0000-0010-8000-00AA006D2EA4}']
    property Count: Integer readonly dispid 1610743808;
    function _NewEnum: IUnknown; dispid -4;
    procedure Refresh; dispid 1610743810;
  end;

// *********************************************************************//
// Interface: _DynaCollection
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000513-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _DynaCollection = interface(_Collection)
    ['{00000513-0000-0010-8000-00AA006D2EA4}']
    function Append(const Object_: IDispatch): HResult; stdcall;
    function Delete(Index: OleVariant): HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  _DynaCollectionDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000513-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _DynaCollectionDisp = dispinterface
    ['{00000513-0000-0010-8000-00AA006D2EA4}']
    procedure Append(const Object_: IDispatch); dispid 1610809344;
    procedure Delete(Index: OleVariant); dispid 1610809345;
    property Count: Integer readonly dispid 1610743808;
    function _NewEnum: IUnknown; dispid -4;
    procedure Refresh; dispid 1610743810;
  end;

// *********************************************************************//
// Interface: _ADO
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000534-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _ADO = interface(IDispatch)
    ['{00000534-0000-0010-8000-00AA006D2EA4}']
    function Get_Properties(out ppvObject: Properties): HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  _ADODisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000534-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _ADODisp = dispinterface
    ['{00000534-0000-0010-8000-00AA006D2EA4}']
    property Properties: Properties readonly dispid 500;
  end;

// *********************************************************************//
// Interface: Properties
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000504-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  Properties = interface(_Collection)
    ['{00000504-0000-0010-8000-00AA006D2EA4}']
    function Get_Item(Index: OleVariant; out ppvObject: Property_): HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  PropertiesDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000504-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  PropertiesDisp = dispinterface
    ['{00000504-0000-0010-8000-00AA006D2EA4}']
    property Item[Index: OleVariant]: Property_ readonly dispid 0; default;
    property Count: Integer readonly dispid 1610743808;
    function _NewEnum: IUnknown; dispid -4;
    procedure Refresh; dispid 1610743810;
  end;

// *********************************************************************//
// Interface: Property_
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000503-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  Property_ = interface(IDispatch)
    ['{00000503-0000-0010-8000-00AA006D2EA4}']
    function Get_Value(out pval: OleVariant): HResult; stdcall;
    function Set_Value(pval: OleVariant): HResult; stdcall;
    function Get_Name(out pbstr: WideString): HResult; stdcall;
    function Get_type_(out ptype: DataTypeEnum): HResult; stdcall;
    function Get_Attributes(out plAttributes: Integer): HResult; stdcall;
    function Set_Attributes(plAttributes: Integer): HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  Property_Disp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000503-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  Property_Disp = dispinterface
    ['{00000503-0000-0010-8000-00AA006D2EA4}']
    function Value: OleVariant; dispid 0;
    property Name: WideString readonly dispid 1610743810;
    property type_: DataTypeEnum readonly dispid 1610743811;
    function Attributes: Integer; dispid 1610743812;
  end;

// *********************************************************************//
// Interface: Error
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000500-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  Error = interface(IDispatch)
    ['{00000500-0000-0010-8000-00AA006D2EA4}']
    function Get_Number(out pl: Integer): HResult; stdcall;
    function Get_Source(out pbstr: WideString): HResult; stdcall;
    function Get_Description(out pbstr: WideString): HResult; stdcall;
    function Get_HelpFile(out pbstr: WideString): HResult; stdcall;
    function Get_HelpContext(out pl: Integer): HResult; stdcall;
    function Get_SQLState(out pbstr: WideString): HResult; stdcall;
    function Get_NativeError(out pl: Integer): HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  ErrorDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000500-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  ErrorDisp = dispinterface
    ['{00000500-0000-0010-8000-00AA006D2EA4}']
    property Number: Integer readonly dispid 1610743808;
    property Source: WideString readonly dispid 1610743809;
    property Description: WideString readonly dispid 0;
    property HelpFile: WideString readonly dispid 1610743811;
    property HelpContext: Integer readonly dispid 1610743812;
    property SQLState: WideString readonly dispid 1610743813;
    property NativeError: Integer readonly dispid 1610743814;
  end;

// *********************************************************************//
// Interface: Errors
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000501-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  Errors = interface(_Collection)
    ['{00000501-0000-0010-8000-00AA006D2EA4}']
    function Get_Item(Index: OleVariant; out ppvObject: Error): HResult; stdcall;
    function Clear: HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  ErrorsDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000501-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  ErrorsDisp = dispinterface
    ['{00000501-0000-0010-8000-00AA006D2EA4}']
    property Item[Index: OleVariant]: Error readonly dispid 0; default;
    procedure Clear; dispid 1610809345;
    property Count: Integer readonly dispid 1610743808;
    function _NewEnum: IUnknown; dispid -4;
    procedure Refresh; dispid 1610743810;
  end;

// *********************************************************************//
// Interface: _Command15
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000508-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _Command15 = interface(_ADO)
    ['{00000508-0000-0010-8000-00AA006D2EA4}']
    function Get_ActiveConnection(out ppvObject: _Connection): HResult; stdcall;
    function _Set_ActiveConnection(const ppvObject: _Connection): HResult; stdcall;
    function Set_ActiveConnection(ppvObject: OleVariant): HResult; stdcall;
    function Get_CommandText(out pbstr: WideString): HResult; stdcall;
    function Set_CommandText(const pbstr: WideString): HResult; stdcall;
    function Get_CommandTimeout(out pl: Integer): HResult; stdcall;
    function Set_CommandTimeout(pl: Integer): HResult; stdcall;
    function Get_Prepared(out pfPrepared: WordBool): HResult; stdcall;
    function Set_Prepared(pfPrepared: WordBool): HResult; stdcall;
    function Execute(out RecordsAffected: OleVariant; var Parameters: OleVariant; Options: Integer; 
                     out ppiRs: _Recordset): HResult; stdcall;
    function CreateParameter(const Name: WideString; Type_: DataTypeEnum; 
                             Direction: ParameterDirectionEnum; Size: Integer; Value: OleVariant; 
                             out ppiprm: _Parameter): HResult; stdcall;
    function Get_Parameters(out ppvObject: Parameters): HResult; stdcall;
    function Set_CommandType(plCmdType: CommandTypeEnum): HResult; stdcall;
    function Get_CommandType(out plCmdType: CommandTypeEnum): HResult; stdcall;
    function Get_Name(out pbstrName: WideString): HResult; stdcall;
    function Set_Name(const pbstrName: WideString): HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  _Command15Disp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000508-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _Command15Disp = dispinterface
    ['{00000508-0000-0010-8000-00AA006D2EA4}']
    function ActiveConnection: _Connection; dispid 1610809344;
    function CommandText: WideString; dispid 1610809347;
    function CommandTimeout: Integer; dispid 1610809349;
    function Prepared: WordBool; dispid 1610809351;
    function Execute(out RecordsAffected: OleVariant; var Parameters: OleVariant; Options: Integer): _Recordset; dispid 1610809353;
    function CreateParameter(const Name: WideString; Type_: DataTypeEnum; 
                             Direction: ParameterDirectionEnum; Size: Integer; Value: OleVariant): _Parameter; dispid 1610809354;
    property Parameters: Parameters readonly dispid 0;
    property CommandType: CommandTypeEnum dispid 1610809356;
    function Name: WideString; dispid 1610809358;
    property Properties: Properties readonly dispid 500;
  end;

// *********************************************************************//
// Interface: _Connection15
// Flags:     (4432) Hidden Dual OleAutomation Dispatchable
// GUID:      {00000515-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _Connection15 = interface(_ADO)
    ['{00000515-0000-0010-8000-00AA006D2EA4}']
    function Get_ConnectionString(out pbstr: WideString): HResult; stdcall;
    function Set_ConnectionString(const pbstr: WideString): HResult; stdcall;
    function Get_CommandTimeout(out plTimeout: Integer): HResult; stdcall;
    function Set_CommandTimeout(plTimeout: Integer): HResult; stdcall;
    function Get_ConnectionTimeout(out plTimeout: Integer): HResult; stdcall;
    function Set_ConnectionTimeout(plTimeout: Integer): HResult; stdcall;
    function Get_Version(out pbstr: WideString): HResult; stdcall;
    function Close: HResult; stdcall;
    function Execute(const CommandText: WideString; out RecordsAffected: OleVariant; 
                     Options: Integer; out ppiRset: _Recordset): HResult; stdcall;
    function BeginTrans(out TransactionLevel: Integer): HResult; stdcall;
    function CommitTrans: HResult; stdcall;
    function RollbackTrans: HResult; stdcall;
    function Open(const ConnectionString: WideString; const UserID: WideString; 
                  const Password: WideString; Options: Integer): HResult; stdcall;
    function Get_Errors(out ppvObject: Errors): HResult; stdcall;
    function Get_DefaultDatabase(out pbstr: WideString): HResult; stdcall;
    function Set_DefaultDatabase(const pbstr: WideString): HResult; stdcall;
    function Get_IsolationLevel(out Level: IsolationLevelEnum): HResult; stdcall;
    function Set_IsolationLevel(Level: IsolationLevelEnum): HResult; stdcall;
    function Get_Attributes(out plAttr: Integer): HResult; stdcall;
    function Set_Attributes(plAttr: Integer): HResult; stdcall;
    function Get_CursorLocation(out plCursorLoc: CursorLocationEnum): HResult; stdcall;
    function Set_CursorLocation(plCursorLoc: CursorLocationEnum): HResult; stdcall;
    function Get_Mode(out plMode: ConnectModeEnum): HResult; stdcall;
    function Set_Mode(plMode: ConnectModeEnum): HResult; stdcall;
    function Get_Provider(out pbstr: WideString): HResult; stdcall;
    function Set_Provider(const pbstr: WideString): HResult; stdcall;
    function Get_State(out plObjState: Integer): HResult; stdcall;
    function OpenSchema(Schema: SchemaEnum; Restrictions: OleVariant; SchemaID: OleVariant; 
                        out pprset: _Recordset): HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  _Connection15Disp
// Flags:     (4432) Hidden Dual OleAutomation Dispatchable
// GUID:      {00000515-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _Connection15Disp = dispinterface
    ['{00000515-0000-0010-8000-00AA006D2EA4}']
    function ConnectionString: WideString; dispid 0;
    function CommandTimeout: Integer; dispid 2;
    function ConnectionTimeout: Integer; dispid 3;
    property Version: WideString readonly dispid 4;
    procedure Close; dispid 5;
    function Execute(const CommandText: WideString; out RecordsAffected: OleVariant; 
                     Options: Integer): _Recordset; dispid 6;
    function BeginTrans: Integer; dispid 7;
    procedure CommitTrans; dispid 8;
    procedure RollbackTrans; dispid 9;
    procedure Open(const ConnectionString: WideString; const UserID: WideString; 
                   const Password: WideString; Options: Integer); dispid 10;
    property Errors: Errors readonly dispid 11;
    function DefaultDatabase: WideString; dispid 12;
    function IsolationLevel: IsolationLevelEnum; dispid 13;
    function Attributes: Integer; dispid 14;
    function CursorLocation: CursorLocationEnum; dispid 15;
    function Mode: ConnectModeEnum; dispid 16;
    function Provider: WideString; dispid 17;
    property State: Integer readonly dispid 18;
    function OpenSchema(Schema: SchemaEnum; Restrictions: OleVariant; SchemaID: OleVariant): _Recordset; dispid 19;
    property Properties: Properties readonly dispid 500;
  end;

// *********************************************************************//
// Interface: _Connection
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {00000550-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _Connection = interface(_Connection15)
    ['{00000550-0000-0010-8000-00AA006D2EA4}']
    function Cancel: HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  _ConnectionDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {00000550-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _ConnectionDisp = dispinterface
    ['{00000550-0000-0010-8000-00AA006D2EA4}']
    procedure Cancel; dispid 21;
    function ConnectionString: WideString; dispid 0;
    function CommandTimeout: Integer; dispid 2;
    function ConnectionTimeout: Integer; dispid 3;
    property Version: WideString readonly dispid 4;
    procedure Close; dispid 5;
    function Execute(const CommandText: WideString; out RecordsAffected: OleVariant; 
                     Options: Integer): _Recordset; dispid 6;
    function BeginTrans: Integer; dispid 7;
    procedure CommitTrans; dispid 8;
    procedure RollbackTrans; dispid 9;
    procedure Open(const ConnectionString: WideString; const UserID: WideString; 
                   const Password: WideString; Options: Integer); dispid 10;
    property Errors: Errors readonly dispid 11;
    function DefaultDatabase: WideString; dispid 12;
    function IsolationLevel: IsolationLevelEnum; dispid 13;
    function Attributes: Integer; dispid 14;
    function CursorLocation: CursorLocationEnum; dispid 15;
    function Mode: ConnectModeEnum; dispid 16;
    function Provider: WideString; dispid 17;
    property State: Integer readonly dispid 18;
    function OpenSchema(Schema: SchemaEnum; Restrictions: OleVariant; SchemaID: OleVariant): _Recordset; dispid 19;
    property Properties: Properties readonly dispid 500;
  end;

// *********************************************************************//
// Interface: _Recordset15
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {0000050E-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _Recordset15 = interface(_ADO)
    ['{0000050E-0000-0010-8000-00AA006D2EA4}']
    function Get_AbsolutePosition(out pl: PositionEnum): HResult; stdcall;
    function Set_AbsolutePosition(pl: PositionEnum): HResult; stdcall;
    function _Set_ActiveConnection(const pvar: IDispatch): HResult; stdcall;
    function Set_ActiveConnection(pvar: OleVariant): HResult; stdcall;
    function Get_ActiveConnection(out pvar: OleVariant): HResult; stdcall;
    function Get_BOF(out pb: WordBool): HResult; stdcall;
    function Get_Bookmark(out pvBookmark: OleVariant): HResult; stdcall;
    function Set_Bookmark(pvBookmark: OleVariant): HResult; stdcall;
    function Get_CacheSize(out pl: Integer): HResult; stdcall;
    function Set_CacheSize(pl: Integer): HResult; stdcall;
    function Get_CursorType(out plCursorType: CursorTypeEnum): HResult; stdcall;
    function Set_CursorType(plCursorType: CursorTypeEnum): HResult; stdcall;
    function Get_EOF(out pb: WordBool): HResult; stdcall;
    function Get_Fields(out ppvObject: Fields): HResult; stdcall;
    function Get_LockType(out plLockType: LockTypeEnum): HResult; stdcall;
    function Set_LockType(plLockType: LockTypeEnum): HResult; stdcall;
    function Get_MaxRecords(out plMaxRecords: Integer): HResult; stdcall;
    function Set_MaxRecords(plMaxRecords: Integer): HResult; stdcall;
    function Get_RecordCount(out pl: Integer): HResult; stdcall;
    function _Set_Source(const pvSource: IDispatch): HResult; stdcall;
    function Set_Source(const pvSource: WideString): HResult; stdcall;
    function Get_Source(out pvSource: OleVariant): HResult; stdcall;
    function AddNew(FieldList: OleVariant; Values: OleVariant): HResult; stdcall;
    function CancelUpdate: HResult; stdcall;
    function Close: HResult; stdcall;
    function Delete(AffectRecords: AffectEnum): HResult; stdcall;
    function GetRows(Rows: Integer; Start: OleVariant; Fields: OleVariant; out pvar: OleVariant): HResult; stdcall;
    function Move(NumRecords: Integer; Start: OleVariant): HResult; stdcall;
    function MoveNext: HResult; stdcall;
    function MovePrevious: HResult; stdcall;
    function MoveFirst: HResult; stdcall;
    function MoveLast: HResult; stdcall;
    function Open(Source: OleVariant; ActiveConnection: OleVariant; CursorType: CursorTypeEnum; 
                  LockType: LockTypeEnum; Options: Integer): HResult; stdcall;
    function Requery(Options: Integer): HResult; stdcall;
    function _xResync(AffectRecords: AffectEnum): HResult; stdcall;
    function Update(Fields: OleVariant; Values: OleVariant): HResult; stdcall;
    function Get_AbsolutePage(out pl: PositionEnum): HResult; stdcall;
    function Set_AbsolutePage(pl: PositionEnum): HResult; stdcall;
    function Get_EditMode(out pl: EditModeEnum): HResult; stdcall;
    function Get_Filter(out Criteria: OleVariant): HResult; stdcall;
    function Set_Filter(Criteria: OleVariant): HResult; stdcall;
    function Get_PageCount(out pl: Integer): HResult; stdcall;
    function Get_PageSize(out pl: Integer): HResult; stdcall;
    function Set_PageSize(pl: Integer): HResult; stdcall;
    function Get_Sort(out Criteria: WideString): HResult; stdcall;
    function Set_Sort(const Criteria: WideString): HResult; stdcall;
    function Get_Status(out pl: Integer): HResult; stdcall;
    function Get_State(out plObjState: Integer): HResult; stdcall;
    function _xClone(out ppvObject: _Recordset): HResult; stdcall;
    function UpdateBatch(AffectRecords: AffectEnum): HResult; stdcall;
    function CancelBatch(AffectRecords: AffectEnum): HResult; stdcall;
    function Get_CursorLocation(out plCursorLoc: CursorLocationEnum): HResult; stdcall;
    function Set_CursorLocation(plCursorLoc: CursorLocationEnum): HResult; stdcall;
    function NextRecordset(out RecordsAffected: OleVariant; out ppiRs: _Recordset): HResult; stdcall;
    function Supports(CursorOptions: CursorOptionEnum; out pb: WordBool): HResult; stdcall;
    function Get_Collect(Index: OleVariant; out pvar: OleVariant): HResult; stdcall;
    function Set_Collect(Index: OleVariant; pvar: OleVariant): HResult; stdcall;
    function Get_MarshalOptions(out peMarshal: MarshalOptionsEnum): HResult; stdcall;
    function Set_MarshalOptions(peMarshal: MarshalOptionsEnum): HResult; stdcall;
    function Find(const Criteria: WideString; SkipRecords: Integer; 
                  SearchDirection: SearchDirectionEnum; Start: OleVariant): HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  _Recordset15Disp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {0000050E-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _Recordset15Disp = dispinterface
    ['{0000050E-0000-0010-8000-00AA006D2EA4}']
    function AbsolutePosition: PositionEnum; dispid 1000;
    function ActiveConnection: IDispatch; dispid 1001;
    property BOF: WordBool readonly dispid 1002;
    function Bookmark: OleVariant; dispid 1003;
    function CacheSize: Integer; dispid 1004;
    function CursorType: CursorTypeEnum; dispid 1005;
    property EOF: WordBool readonly dispid 1006;
    property Fields: Fields readonly dispid 0;
    function LockType: LockTypeEnum; dispid 1008;
    function MaxRecords: Integer; dispid 1009;
    property RecordCount: Integer readonly dispid 1010;
    function Source: IDispatch; dispid 1011;
    procedure AddNew(FieldList: OleVariant; Values: OleVariant); dispid 1012;
    procedure CancelUpdate; dispid 1013;
    procedure Close; dispid 1014;
    procedure Delete(AffectRecords: AffectEnum); dispid 1015;
    function GetRows(Rows: Integer; Start: OleVariant; Fields: OleVariant): OleVariant; dispid 1016;
    procedure Move(NumRecords: Integer; Start: OleVariant); dispid 1017;
    procedure MoveNext; dispid 1018;
    procedure MovePrevious; dispid 1019;
    procedure MoveFirst; dispid 1020;
    procedure MoveLast; dispid 1021;
    procedure Open(Source: OleVariant; ActiveConnection: OleVariant; CursorType: CursorTypeEnum; 
                   LockType: LockTypeEnum; Options: Integer); dispid 1022;
    procedure Requery(Options: Integer); dispid 1023;
    procedure _xResync(AffectRecords: AffectEnum); dispid 1610809378;
    procedure Update(Fields: OleVariant; Values: OleVariant); dispid 1025;
    function AbsolutePage: PositionEnum; dispid 1047;
    property EditMode: EditModeEnum readonly dispid 1026;
    function Filter: OleVariant; dispid 1030;
    property PageCount: Integer readonly dispid 1050;
    function PageSize: Integer; dispid 1048;
    function Sort: WideString; dispid 1031;
    property Status: Integer readonly dispid 1029;
    property State: Integer readonly dispid 1054;
    function _xClone: _Recordset; dispid 1610809392;
    procedure UpdateBatch(AffectRecords: AffectEnum); dispid 1035;
    procedure CancelBatch(AffectRecords: AffectEnum); dispid 1049;
    function CursorLocation: CursorLocationEnum; dispid 1051;
    function NextRecordset(out RecordsAffected: OleVariant): _Recordset; dispid 1052;
    function Supports(CursorOptions: CursorOptionEnum): WordBool; dispid 1036;
    function Collect(Index: OleVariant): OleVariant; dispid -8;
    function MarshalOptions: MarshalOptionsEnum; dispid 1053;
    procedure Find(const Criteria: WideString; SkipRecords: Integer; 
                   SearchDirection: SearchDirectionEnum; Start: OleVariant); dispid 1058;
    property Properties: Properties readonly dispid 500;
  end;

// *********************************************************************//
// Interface: _Recordset
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {0000054F-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _Recordset = interface(_Recordset15)
    ['{0000054F-0000-0010-8000-00AA006D2EA4}']
    function Cancel: HResult; stdcall;
    function Get_DataSource(out ppunkDataSource: IUnknown): HResult; stdcall;
    function _Set_DataSource(const ppunkDataSource: IUnknown): HResult; stdcall;
    function Save(const FileName: WideString; PersistFormat: PersistFormatEnum): HResult; stdcall;
    function Get_ActiveCommand(out ppCmd: IDispatch): HResult; stdcall;
    function Set_StayInSync(pbStayInSync: WordBool): HResult; stdcall;
    function Get_StayInSync(out pbStayInSync: WordBool): HResult; stdcall;
    function GetString(StringFormat: StringFormatEnum; NumRows: Integer; 
                       const ColumnDelimeter: WideString; const RowDelimeter: WideString; 
                       const NullExpr: WideString; out pRetString: WideString): HResult; stdcall;
    function Get_DataMember(out pbstrDataMember: WideString): HResult; stdcall;
    function Set_DataMember(const pbstrDataMember: WideString): HResult; stdcall;
    function CompareBookmarks(Bookmark1: OleVariant; Bookmark2: OleVariant; 
                              out pCompare: CompareEnum): HResult; stdcall;
    function Clone(LockType: LockTypeEnum; out ppvObject: _Recordset): HResult; stdcall;
    function Resync(AffectRecords: AffectEnum; ResyncValues: ResyncEnum): HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  _RecordsetDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {0000054F-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _RecordsetDisp = dispinterface
    ['{0000054F-0000-0010-8000-00AA006D2EA4}']
    procedure Cancel; dispid 1055;
    function DataSource: IUnknown; dispid 1056;
    procedure Save(const FileName: WideString; PersistFormat: PersistFormatEnum); dispid 1057;
    property ActiveCommand: IDispatch readonly dispid 1061;
    property StayInSync: WordBool dispid 1063;
    function GetString(StringFormat: StringFormatEnum; NumRows: Integer; 
                       const ColumnDelimeter: WideString; const RowDelimeter: WideString; 
                       const NullExpr: WideString): WideString; dispid 1062;
    function DataMember: WideString; dispid 1064;
    function CompareBookmarks(Bookmark1: OleVariant; Bookmark2: OleVariant): CompareEnum; dispid 1065;
    function Clone(LockType: LockTypeEnum): _Recordset; dispid 1034;
    procedure Resync(AffectRecords: AffectEnum; ResyncValues: ResyncEnum); dispid 1024;
    function AbsolutePosition: PositionEnum; dispid 1000;
    function ActiveConnection: IDispatch; dispid 1001;
    property BOF: WordBool readonly dispid 1002;
    function Bookmark: OleVariant; dispid 1003;
    function CacheSize: Integer; dispid 1004;
    function CursorType: CursorTypeEnum; dispid 1005;
    property EOF: WordBool readonly dispid 1006;
    property Fields: Fields readonly dispid 0;
    function LockType: LockTypeEnum; dispid 1008;
    function MaxRecords: Integer; dispid 1009;
    property RecordCount: Integer readonly dispid 1010;
    function Source: IDispatch; dispid 1011;
    procedure AddNew(FieldList: OleVariant; Values: OleVariant); dispid 1012;
    procedure CancelUpdate; dispid 1013;
    procedure Close; dispid 1014;
    procedure Delete(AffectRecords: AffectEnum); dispid 1015;
    function GetRows(Rows: Integer; Start: OleVariant; Fields: OleVariant): OleVariant; dispid 1016;
    procedure Move(NumRecords: Integer; Start: OleVariant); dispid 1017;
    procedure MoveNext; dispid 1018;
    procedure MovePrevious; dispid 1019;
    procedure MoveFirst; dispid 1020;
    procedure MoveLast; dispid 1021;
    procedure Open(Source: OleVariant; ActiveConnection: OleVariant; CursorType: CursorTypeEnum; 
                   LockType: LockTypeEnum; Options: Integer); dispid 1022;
    procedure Requery(Options: Integer); dispid 1023;
    procedure _xResync(AffectRecords: AffectEnum); dispid 1610809378;
    procedure Update(Fields: OleVariant; Values: OleVariant); dispid 1025;
    function AbsolutePage: PositionEnum; dispid 1047;
    property EditMode: EditModeEnum readonly dispid 1026;
    function Filter: OleVariant; dispid 1030;
    property PageCount: Integer readonly dispid 1050;
    function PageSize: Integer; dispid 1048;
    function Sort: WideString; dispid 1031;
    property Status: Integer readonly dispid 1029;
    property State: Integer readonly dispid 1054;
    function _xClone: _Recordset; dispid 1610809392;
    procedure UpdateBatch(AffectRecords: AffectEnum); dispid 1035;
    procedure CancelBatch(AffectRecords: AffectEnum); dispid 1049;
    function CursorLocation: CursorLocationEnum; dispid 1051;
    function NextRecordset(out RecordsAffected: OleVariant): _Recordset; dispid 1052;
    function Supports(CursorOptions: CursorOptionEnum): WordBool; dispid 1036;
    function Collect(Index: OleVariant): OleVariant; dispid -8;
    function MarshalOptions: MarshalOptionsEnum; dispid 1053;
    procedure Find(const Criteria: WideString; SkipRecords: Integer; 
                   SearchDirection: SearchDirectionEnum; Start: OleVariant); dispid 1058;
    property Properties: Properties readonly dispid 500;
  end;

// *********************************************************************//
// Interface: Fields15
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000506-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  Fields15 = interface(_Collection)
    ['{00000506-0000-0010-8000-00AA006D2EA4}']
    function Get_Item(Index: OleVariant; out ppvObject: Field): HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  Fields15Disp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000506-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  Fields15Disp = dispinterface
    ['{00000506-0000-0010-8000-00AA006D2EA4}']
    property Item[Index: OleVariant]: Field readonly dispid 0; default;
    property Count: Integer readonly dispid 1610743808;
    function _NewEnum: IUnknown; dispid -4;
    procedure Refresh; dispid 1610743810;
  end;

// *********************************************************************//
// Interface: Fields
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {0000054D-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  Fields = interface(Fields15)
    ['{0000054D-0000-0010-8000-00AA006D2EA4}']
    function Append(const Name: WideString; Type_: DataTypeEnum; DefinedSize: Integer; 
                    Attrib: FieldAttributeEnum): HResult; stdcall;
    function Delete(Index: OleVariant): HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  FieldsDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {0000054D-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  FieldsDisp = dispinterface
    ['{0000054D-0000-0010-8000-00AA006D2EA4}']
    procedure Append(const Name: WideString; Type_: DataTypeEnum; DefinedSize: Integer; 
                     Attrib: FieldAttributeEnum); dispid 1610874880;
    procedure Delete(Index: OleVariant); dispid 1610874881;
    property Item[Index: OleVariant]: Field readonly dispid 0; default;
    property Count: Integer readonly dispid 1610743808;
    function _NewEnum: IUnknown; dispid -4;
    procedure Refresh; dispid 1610743810;
  end;

// *********************************************************************//
// Interface: Field
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {0000054C-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  Field = interface(_ADO)
    ['{0000054C-0000-0010-8000-00AA006D2EA4}']
    function Get_ActualSize(out pl: Integer): HResult; stdcall;
    function Get_Attributes(out pl: Integer): HResult; stdcall;
    function Get_DefinedSize(out pl: Integer): HResult; stdcall;
    function Get_Name(out pbstr: WideString): HResult; stdcall;
    function Get_type_(out pDataType: DataTypeEnum): HResult; stdcall;
    function Get_Value(out pvar: OleVariant): HResult; stdcall;
    function Set_Value(pvar: OleVariant): HResult; stdcall;
    function Get_Precision(out pbPrecision: Byte): HResult; stdcall;
    function Get_NumericScale(out pbNumericScale: Byte): HResult; stdcall;
    function AppendChunk(Data: OleVariant): HResult; stdcall;
    function GetChunk(Length: Integer; out pvar: OleVariant): HResult; stdcall;
    function Get_OriginalValue(out pvar: OleVariant): HResult; stdcall;
    function Get_UnderlyingValue(out pvar: OleVariant): HResult; stdcall;
    function Get_DataFormat(out ppiDF: IUnknown): HResult; stdcall;
    function _Set_DataFormat(const ppiDF: IUnknown): HResult; stdcall;
    function Set_Precision(pbPrecision: Byte): HResult; stdcall;
    function Set_NumericScale(pbNumericScale: Byte): HResult; stdcall;
    function Set_type_(pDataType: DataTypeEnum): HResult; stdcall;
    function Set_DefinedSize(pl: Integer): HResult; stdcall;
    function Set_Attributes(pl: Integer): HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  FieldDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {0000054C-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  FieldDisp = dispinterface
    ['{0000054C-0000-0010-8000-00AA006D2EA4}']
    property ActualSize: Integer readonly dispid 1109;
    function Attributes: Integer; dispid 1036;
    function DefinedSize: Integer; dispid 1103;
    property Name: WideString readonly dispid 1100;
    function type_: DataTypeEnum; dispid 1102;
    function Value: OleVariant; dispid 0;
    function Precision: Byte; dispid 1610809351;
    function NumericScale: Byte; dispid 1610809352;
    procedure AppendChunk(Data: OleVariant); dispid 1107;
    function GetChunk(Length: Integer): OleVariant; dispid 1108;
    property OriginalValue: OleVariant readonly dispid 1104;
    property UnderlyingValue: OleVariant readonly dispid 1105;
    function DataFormat: IUnknown; dispid 1610809357;
    property Properties: Properties readonly dispid 500;
  end;

// *********************************************************************//
// Interface: _Parameter
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {0000050C-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _Parameter = interface(_ADO)
    ['{0000050C-0000-0010-8000-00AA006D2EA4}']
    function Get_Name(out pbstr: WideString): HResult; stdcall;
    function Set_Name(const pbstr: WideString): HResult; stdcall;
    function Get_Value(out pvar: OleVariant): HResult; stdcall;
    function Set_Value(pvar: OleVariant): HResult; stdcall;
    function Get_type_(out psDataType: DataTypeEnum): HResult; stdcall;
    function Set_type_(psDataType: DataTypeEnum): HResult; stdcall;
    function Set_Direction(plParmDirection: ParameterDirectionEnum): HResult; stdcall;
    function Get_Direction(out plParmDirection: ParameterDirectionEnum): HResult; stdcall;
    function Set_Precision(pbPrecision: Byte): HResult; stdcall;
    function Get_Precision(out pbPrecision: Byte): HResult; stdcall;
    function Set_NumericScale(pbScale: Byte): HResult; stdcall;
    function Get_NumericScale(out pbScale: Byte): HResult; stdcall;
    function Set_Size(pl: Integer): HResult; stdcall;
    function Get_Size(out pl: Integer): HResult; stdcall;
    function AppendChunk(Val: OleVariant): HResult; stdcall;
    function Get_Attributes(out plParmAttribs: Integer): HResult; stdcall;
    function Set_Attributes(plParmAttribs: Integer): HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  _ParameterDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {0000050C-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _ParameterDisp = dispinterface
    ['{0000050C-0000-0010-8000-00AA006D2EA4}']
    function Name: WideString; dispid 1610809344;
    function Value: OleVariant; dispid 0;
    function type_: DataTypeEnum; dispid 1610809348;
    property Direction: ParameterDirectionEnum dispid 1610809350;
    property Precision: Byte dispid 1610809352;
    property NumericScale: Byte dispid 1610809354;
    property Size: Integer dispid 1610809356;
    procedure AppendChunk(Val: OleVariant); dispid 1610809358;
    function Attributes: Integer; dispid 1610809359;
    property Properties: Properties readonly dispid 500;
  end;

// *********************************************************************//
// Interface: Parameters
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {0000050D-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  Parameters = interface(_DynaCollection)
    ['{0000050D-0000-0010-8000-00AA006D2EA4}']
    function Get_Item(Index: OleVariant; out ppvObject: _Parameter): HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  ParametersDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {0000050D-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  ParametersDisp = dispinterface
    ['{0000050D-0000-0010-8000-00AA006D2EA4}']
    property Item[Index: OleVariant]: _Parameter readonly dispid 0; default;
    procedure Append(const Object_: IDispatch); dispid 1610809344;
    procedure Delete(Index: OleVariant); dispid 1610809345;
    property Count: Integer readonly dispid 1610743808;
    function _NewEnum: IUnknown; dispid -4;
    procedure Refresh; dispid 1610743810;
  end;

// *********************************************************************//
// Interface: _Command
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {0000054E-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _Command = interface(_Command15)
    ['{0000054E-0000-0010-8000-00AA006D2EA4}']
    function Get_State(out plObjState: Integer): HResult; stdcall;
    function Cancel: HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  _CommandDisp
// Flags:     (4544) Dual NonExtensible OleAutomation Dispatchable
// GUID:      {0000054E-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  _CommandDisp = dispinterface
    ['{0000054E-0000-0010-8000-00AA006D2EA4}']
    property State: Integer readonly dispid 1610874880;
    procedure Cancel; dispid 1610874881;
    function ActiveConnection: _Connection; dispid 1610809344;
    function CommandText: WideString; dispid 1610809347;
    function CommandTimeout: Integer; dispid 1610809349;
    function Prepared: WordBool; dispid 1610809351;
    function Execute(out RecordsAffected: OleVariant; var Parameters: OleVariant; Options: Integer): _Recordset; dispid 1610809353;
    function CreateParameter(const Name: WideString; Type_: DataTypeEnum; 
                             Direction: ParameterDirectionEnum; Size: Integer; Value: OleVariant): _Parameter; dispid 1610809354;
    property Parameters: Parameters readonly dispid 0;
    property CommandType: CommandTypeEnum dispid 1610809356;
    function Name: WideString; dispid 1610809358;
    property Properties: Properties readonly dispid 500;
  end;

// *********************************************************************//
// Interface: ConnectionEventsVt
// Flags:     (16) Hidden
// GUID:      {00000402-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  ConnectionEventsVt = interface(IUnknown)
    ['{00000402-0000-0010-8000-00AA006D2EA4}']
    function InfoMessage(const pError: Error; var adStatus: EventStatusEnum; 
                         const pConnection: _Connection): HResult; stdcall;
    function BeginTransComplete(TransactionLevel: Integer; const pError: Error; 
                                var adStatus: EventStatusEnum; const pConnection: _Connection): HResult; stdcall;
    function CommitTransComplete(const pError: Error; var adStatus: EventStatusEnum; 
                                 const pConnection: _Connection): HResult; stdcall;
    function RollbackTransComplete(const pError: Error; var adStatus: EventStatusEnum; 
                                   const pConnection: _Connection): HResult; stdcall;
    function WillExecute(var Source: WideString; var CursorType: CursorTypeEnum; 
                         var LockType: LockTypeEnum; var Options: Integer; 
                         var adStatus: EventStatusEnum; const pCommand: _Command; 
                         const pRecordset: _Recordset; const pConnection: _Connection): HResult; stdcall;
    function ExecuteComplete(RecordsAffected: Integer; const pError: Error; 
                             var adStatus: EventStatusEnum; const pCommand: _Command; 
                             const pRecordset: _Recordset; const pConnection: _Connection): HResult; stdcall;
    function WillConnect(var ConnectionString: WideString; var UserID: WideString; 
                         var Password: WideString; var Options: Integer; 
                         var adStatus: EventStatusEnum; const pConnection: _Connection): HResult; stdcall;
    function ConnectComplete(const pError: Error; var adStatus: EventStatusEnum; 
                             const pConnection: _Connection): HResult; stdcall;
    function Disconnect(var adStatus: EventStatusEnum; const pConnection: _Connection): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: RecordsetEventsVt
// Flags:     (16) Hidden
// GUID:      {00000403-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  RecordsetEventsVt = interface(IUnknown)
    ['{00000403-0000-0010-8000-00AA006D2EA4}']
    function WillChangeField(cFields: Integer; Fields: OleVariant; var adStatus: EventStatusEnum; 
                             const pRecordset: _Recordset): HResult; stdcall;
    function FieldChangeComplete(cFields: Integer; Fields: OleVariant; const pError: Error; 
                                 var adStatus: EventStatusEnum; const pRecordset: _Recordset): HResult; stdcall;
    function WillChangeRecord(adReason: EventReasonEnum; cRecords: Integer; 
                              var adStatus: EventStatusEnum; const pRecordset: _Recordset): HResult; stdcall;
    function RecordChangeComplete(adReason: EventReasonEnum; cRecords: Integer; 
                                  const pError: Error; var adStatus: EventStatusEnum; 
                                  const pRecordset: _Recordset): HResult; stdcall;
    function WillChangeRecordset(adReason: EventReasonEnum; var adStatus: EventStatusEnum; 
                                 const pRecordset: _Recordset): HResult; stdcall;
    function RecordsetChangeComplete(adReason: EventReasonEnum; const pError: Error; 
                                     var adStatus: EventStatusEnum; const pRecordset: _Recordset): HResult; stdcall;
    function WillMove(adReason: EventReasonEnum; var adStatus: EventStatusEnum; 
                      const pRecordset: _Recordset): HResult; stdcall;
    function MoveComplete(adReason: EventReasonEnum; const pError: Error; 
                          var adStatus: EventStatusEnum; const pRecordset: _Recordset): HResult; stdcall;
    function EndOfRecordset(var fMoreData: WordBool; var adStatus: EventStatusEnum; 
                            const pRecordset: _Recordset): HResult; stdcall;
    function FetchProgress(Progress: Integer; MaxProgress: Integer; var adStatus: EventStatusEnum; 
                           const pRecordset: _Recordset): HResult; stdcall;
    function FetchComplete(const pError: Error; var adStatus: EventStatusEnum; 
                           const pRecordset: _Recordset): HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  ConnectionEvents
// Flags:     (4096) Dispatchable
// GUID:      {00000400-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  ConnectionEvents = dispinterface
    ['{00000400-0000-0010-8000-00AA006D2EA4}']
    procedure InfoMessage(const pError: Error; var adStatus: EventStatusEnum; 
                          const pConnection: _Connection); dispid 0;
    procedure BeginTransComplete(TransactionLevel: Integer; const pError: Error; 
                                 var adStatus: EventStatusEnum; const pConnection: _Connection); dispid 1;
    procedure CommitTransComplete(const pError: Error; var adStatus: EventStatusEnum; 
                                  const pConnection: _Connection); dispid 3;
    procedure RollbackTransComplete(const pError: Error; var adStatus: EventStatusEnum; 
                                    const pConnection: _Connection); dispid 2;
    procedure WillExecute(var Source: WideString; var CursorType: CursorTypeEnum; 
                          var LockType: LockTypeEnum; var Options: Integer; 
                          var adStatus: EventStatusEnum; const pCommand: _Command; 
                          const pRecordset: _Recordset; const pConnection: _Connection); dispid 4;
    procedure ExecuteComplete(RecordsAffected: Integer; const pError: Error; 
                              var adStatus: EventStatusEnum; const pCommand: _Command; 
                              const pRecordset: _Recordset; const pConnection: _Connection); dispid 5;
    procedure WillConnect(var ConnectionString: WideString; var UserID: WideString; 
                          var Password: WideString; var Options: Integer; 
                          var adStatus: EventStatusEnum; const pConnection: _Connection); dispid 6;
    procedure ConnectComplete(const pError: Error; var adStatus: EventStatusEnum; 
                              const pConnection: _Connection); dispid 7;
    procedure Disconnect(var adStatus: EventStatusEnum; const pConnection: _Connection); dispid 8;
  end;

// *********************************************************************//
// DispIntf:  RecordsetEvents
// Flags:     (4096) Dispatchable
// GUID:      {00000266-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  RecordsetEvents = dispinterface
    ['{00000266-0000-0010-8000-00AA006D2EA4}']
    procedure WillChangeField(cFields: Integer; Fields: OleVariant; var adStatus: EventStatusEnum; 
                              const pRecordset: _Recordset); dispid 9;
    procedure FieldChangeComplete(cFields: Integer; Fields: OleVariant; const pError: Error; 
                                  var adStatus: EventStatusEnum; const pRecordset: _Recordset); dispid 10;
    procedure WillChangeRecord(adReason: EventReasonEnum; cRecords: Integer; 
                               var adStatus: EventStatusEnum; const pRecordset: _Recordset); dispid 11;
    procedure RecordChangeComplete(adReason: EventReasonEnum; cRecords: Integer; 
                                   const pError: Error; var adStatus: EventStatusEnum; 
                                   const pRecordset: _Recordset); dispid 12;
    procedure WillChangeRecordset(adReason: EventReasonEnum; var adStatus: EventStatusEnum; 
                                  const pRecordset: _Recordset); dispid 13;
    procedure RecordsetChangeComplete(adReason: EventReasonEnum; const pError: Error; 
                                      var adStatus: EventStatusEnum; const pRecordset: _Recordset); dispid 14;
    procedure WillMove(adReason: EventReasonEnum; var adStatus: EventStatusEnum; 
                       const pRecordset: _Recordset); dispid 15;
    procedure MoveComplete(adReason: EventReasonEnum; const pError: Error; 
                           var adStatus: EventStatusEnum; const pRecordset: _Recordset); dispid 16;
    procedure EndOfRecordset(var fMoreData: WordBool; var adStatus: EventStatusEnum; 
                             const pRecordset: _Recordset); dispid 17;
    procedure FetchProgress(Progress: Integer; MaxProgress: Integer; var adStatus: EventStatusEnum; 
                            const pRecordset: _Recordset); dispid 18;
    procedure FetchComplete(const pError: Error; var adStatus: EventStatusEnum; 
                            const pRecordset: _Recordset); dispid 19;
  end;

// *********************************************************************//
// Interface: ADOConnectionConstruction15
// Flags:     (512) Restricted
// GUID:      {00000516-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  ADOConnectionConstruction15 = interface(IUnknown)
    ['{00000516-0000-0010-8000-00AA006D2EA4}']
    function Get_DSO(out ppDSO: IUnknown): HResult; stdcall;
    function Get_Session(out ppSession: IUnknown): HResult; stdcall;
    function WrapDSOandSession(const pDSO: IUnknown; const pSession: IUnknown): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: ADOConnectionConstruction
// Flags:     (512) Restricted
// GUID:      {00000551-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  ADOConnectionConstruction = interface(ADOConnectionConstruction15)
    ['{00000551-0000-0010-8000-00AA006D2EA4}']
  end;

// *********************************************************************//
// Interface: ADOCommandConstruction
// Flags:     (512) Restricted
// GUID:      {00000517-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  ADOCommandConstruction = interface(IUnknown)
    ['{00000517-0000-0010-8000-00AA006D2EA4}']
    function Get_OLEDBCommand(out ppOLEDBCommand: IUnknown): HResult; stdcall;
    function Set_OLEDBCommand(const ppOLEDBCommand: IUnknown): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: ADORecordsetConstruction
// Flags:     (4608) Restricted Dispatchable
// GUID:      {00000283-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  ADORecordsetConstruction = interface(IDispatch)
    ['{00000283-0000-0010-8000-00AA006D2EA4}']
    function Get_Rowset(out ppRowset: IUnknown): HResult; stdcall;
    function Set_Rowset(const ppRowset: IUnknown): HResult; stdcall;
    function Get_Chapter(out plChapter: Integer): HResult; stdcall;
    function Set_Chapter(plChapter: Integer): HResult; stdcall;
    function Get_RowPosition(out ppRowPos: IUnknown): HResult; stdcall;
    function Set_RowPosition(const ppRowPos: IUnknown): HResult; stdcall;
  end;

// *********************************************************************//
// Interface: Field15
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000505-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  Field15 = interface(_ADO)
    ['{00000505-0000-0010-8000-00AA006D2EA4}']
    function Get_ActualSize(out pl: Integer): HResult; stdcall;
    function Get_Attributes(out pl: Integer): HResult; stdcall;
    function Get_DefinedSize(out pl: Integer): HResult; stdcall;
    function Get_Name(out pbstr: WideString): HResult; stdcall;
    function Get_type_(out pDataType: DataTypeEnum): HResult; stdcall;
    function Get_Value(out pvar: OleVariant): HResult; stdcall;
    function Set_Value(pvar: OleVariant): HResult; stdcall;
    function Get_Precision(out pbPrecision: Byte): HResult; stdcall;
    function Get_NumericScale(out pbNumericScale: Byte): HResult; stdcall;
    function AppendChunk(Data: OleVariant): HResult; stdcall;
    function GetChunk(Length: Integer; out pvar: OleVariant): HResult; stdcall;
    function Get_OriginalValue(out pvar: OleVariant): HResult; stdcall;
    function Get_UnderlyingValue(out pvar: OleVariant): HResult; stdcall;
  end;

// *********************************************************************//
// DispIntf:  Field15Disp
// Flags:     (4560) Hidden Dual NonExtensible OleAutomation Dispatchable
// GUID:      {00000505-0000-0010-8000-00AA006D2EA4}
// *********************************************************************//
  Field15Disp = dispinterface
    ['{00000505-0000-0010-8000-00AA006D2EA4}']
    property ActualSize: Integer readonly dispid 1109;
    property Attributes: Integer readonly dispid 1036;
    property DefinedSize: Integer readonly dispid 1103;
    property Name: WideString readonly dispid 1100;
    property type_: DataTypeEnum readonly dispid 1102;
    function Value: OleVariant; dispid 0;
    property Precision: Byte readonly dispid 1610809351;
    property NumericScale: Byte readonly dispid 1610809352;
    procedure AppendChunk(Data: OleVariant); dispid 1107;
    function GetChunk(Length: Integer): OleVariant; dispid 1108;
    property OriginalValue: OleVariant readonly dispid 1104;
    property UnderlyingValue: OleVariant readonly dispid 1105;
    property Properties: Properties readonly dispid 500;
  end;

// *********************************************************************//
// The Class CoConnection provides a Create and CreateRemote method to          
// create instances of the default interface _Connection exposed by              
// the CoClass Connection. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoConnection = class
    class function Create: _Connection;
    class function CreateRemote(const MachineName: string): _Connection;
  end;

  TConnectionInfoMessage = procedure(ASender: TObject; const pError: Error; 
                                                       var adStatus: EventStatusEnum; 
                                                       const pConnection: _Connection) of object;
  TConnectionBeginTransComplete = procedure(ASender: TObject; TransactionLevel: Integer; 
                                                              const pError: Error; 
                                                              var adStatus: EventStatusEnum; 
                                                              const pConnection: _Connection) of object;
  TConnectionCommitTransComplete = procedure(ASender: TObject; const pError: Error; 
                                                               var adStatus: EventStatusEnum; 
                                                               const pConnection: _Connection) of object;
  TConnectionRollbackTransComplete = procedure(ASender: TObject; const pError: Error; 
                                                                 var adStatus: EventStatusEnum; 
                                                                 const pConnection: _Connection) of object;
  TConnectionWillExecute = procedure(ASender: TObject; var Source: WideString; 
                                                       var CursorType: CursorTypeEnum; 
                                                       var LockType: LockTypeEnum; 
                                                       var Options: Integer; 
                                                       var adStatus: EventStatusEnum; 
                                                       const pCommand: _Command; 
                                                       const pRecordset: _Recordset; 
                                                       const pConnection: _Connection) of object;
  TConnectionExecuteComplete = procedure(ASender: TObject; RecordsAffected: Integer; 
                                                           const pError: Error; 
                                                           var adStatus: EventStatusEnum; 
                                                           const pCommand: _Command; 
                                                           const pRecordset: _Recordset; 
                                                           const pConnection: _Connection) of object;
  TConnectionWillConnect = procedure(ASender: TObject; var ConnectionString: WideString; 
                                                       var UserID: WideString; 
                                                       var Password: WideString; 
                                                       var Options: Integer; 
                                                       var adStatus: EventStatusEnum; 
                                                       const pConnection: _Connection) of object;
  TConnectionConnectComplete = procedure(ASender: TObject; const pError: Error; 
                                                           var adStatus: EventStatusEnum; 
                                                           const pConnection: _Connection) of object;
  TConnectionDisconnect = procedure(ASender: TObject; var adStatus: EventStatusEnum; 
                                                      const pConnection: _Connection) of object;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TConnection
// Help String      : 
// Default Interface: _Connection
// Def. Intf. DISP? : No
// Event   Interface: ConnectionEvents
// TypeFlags        : (6) CanCreate Licensed
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TConnectionProperties= class;
{$ENDIF}
  TConnection = class(TOleServer)
  private
    FOnInfoMessage: TConnectionInfoMessage;
    FOnBeginTransComplete: TConnectionBeginTransComplete;
    FOnCommitTransComplete: TConnectionCommitTransComplete;
    FOnRollbackTransComplete: TConnectionRollbackTransComplete;
    FOnWillExecute: TConnectionWillExecute;
    FOnExecuteComplete: TConnectionExecuteComplete;
    FOnWillConnect: TConnectionWillConnect;
    FOnConnectComplete: TConnectionConnectComplete;
    FOnDisconnect: TConnectionDisconnect;
    FIntf:        _Connection;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TConnectionProperties;
    function      GetServerProperties: TConnectionProperties;
{$ENDIF}
    function      GetDefaultInterface: _Connection;
  protected
    procedure InitServerData; override;
    procedure InvokeEvent(DispID: TDispID; var Params: TVariantArray); override;
    function Get_ConnectionString(out pbstr: WideString): HResult;
    function Set_ConnectionString(const pbstr: WideString): HResult;
    function Get_CommandTimeout(out plTimeout: Integer): HResult;
    function Set_CommandTimeout(plTimeout: Integer): HResult;
    function Get_ConnectionTimeout(out plTimeout: Integer): HResult;
    function Set_ConnectionTimeout(plTimeout: Integer): HResult;
    function Get_Version(out pbstr: WideString): HResult;
    function Get_Errors(out ppvObject: Errors): HResult;
    function Get_DefaultDatabase(out pbstr: WideString): HResult;
    function Set_DefaultDatabase(const pbstr: WideString): HResult;
    function Get_IsolationLevel(out Level: IsolationLevelEnum): HResult;
    function Set_IsolationLevel(Level: IsolationLevelEnum): HResult;
    function Get_Attributes(out plAttr: Integer): HResult;
    function Set_Attributes(plAttr: Integer): HResult;
    function Get_CursorLocation(out plCursorLoc: CursorLocationEnum): HResult;
    function Set_CursorLocation(plCursorLoc: CursorLocationEnum): HResult;
    function Get_Mode(out plMode: ConnectModeEnum): HResult;
    function Set_Mode(plMode: ConnectModeEnum): HResult;
    function Get_Provider(out pbstr: WideString): HResult;
    function Set_Provider(const pbstr: WideString): HResult;
    function Get_State(out plObjState: Integer): HResult;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _Connection);
    procedure Disconnect; override;
    function Close: HResult;
    function Execute(const CommandText: WideString; out RecordsAffected: OleVariant; 
                     Options: Integer; out ppiRset: _Recordset): HResult;
    function BeginTrans(out TransactionLevel: Integer): HResult;
    function CommitTrans: HResult;
    function RollbackTrans: HResult;
    function Open(const ConnectionString: WideString; const UserID: WideString; 
                  const Password: WideString; Options: Integer): HResult;
    function OpenSchema(Schema: SchemaEnum; Restrictions: OleVariant; SchemaID: OleVariant; 
                        out pprset: _Recordset): HResult;
    function Cancel: HResult;
    property DefaultInterface: _Connection read GetDefaultInterface;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TConnectionProperties read GetServerProperties;
{$ENDIF}
    property OnInfoMessage: TConnectionInfoMessage read FOnInfoMessage write FOnInfoMessage;
    property OnBeginTransComplete: TConnectionBeginTransComplete read FOnBeginTransComplete write FOnBeginTransComplete;
    property OnCommitTransComplete: TConnectionCommitTransComplete read FOnCommitTransComplete write FOnCommitTransComplete;
    property OnRollbackTransComplete: TConnectionRollbackTransComplete read FOnRollbackTransComplete write FOnRollbackTransComplete;
    property OnWillExecute: TConnectionWillExecute read FOnWillExecute write FOnWillExecute;
    property OnExecuteComplete: TConnectionExecuteComplete read FOnExecuteComplete write FOnExecuteComplete;
    property OnWillConnect: TConnectionWillConnect read FOnWillConnect write FOnWillConnect;
    property OnConnectComplete: TConnectionConnectComplete read FOnConnectComplete write FOnConnectComplete;
    property OnDisconnect: TConnectionDisconnect read FOnDisconnect write FOnDisconnect;
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TConnection
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TConnectionProperties = class(TPersistent)
  private
    FServer:    TConnection;
    function    GetDefaultInterface: _Connection;
    constructor Create(AServer: TConnection);
  protected
    function Get_ConnectionString(out pbstr: WideString): HResult;
    function Set_ConnectionString(const pbstr: WideString): HResult;
    function Get_CommandTimeout(out plTimeout: Integer): HResult;
    function Set_CommandTimeout(plTimeout: Integer): HResult;
    function Get_ConnectionTimeout(out plTimeout: Integer): HResult;
    function Set_ConnectionTimeout(plTimeout: Integer): HResult;
    function Get_Version(out pbstr: WideString): HResult;
    function Get_Errors(out ppvObject: Errors): HResult;
    function Get_DefaultDatabase(out pbstr: WideString): HResult;
    function Set_DefaultDatabase(const pbstr: WideString): HResult;
    function Get_IsolationLevel(out Level: IsolationLevelEnum): HResult;
    function Set_IsolationLevel(Level: IsolationLevelEnum): HResult;
    function Get_Attributes(out plAttr: Integer): HResult;
    function Set_Attributes(plAttr: Integer): HResult;
    function Get_CursorLocation(out plCursorLoc: CursorLocationEnum): HResult;
    function Set_CursorLocation(plCursorLoc: CursorLocationEnum): HResult;
    function Get_Mode(out plMode: ConnectModeEnum): HResult;
    function Set_Mode(plMode: ConnectModeEnum): HResult;
    function Get_Provider(out pbstr: WideString): HResult;
    function Set_Provider(const pbstr: WideString): HResult;
    function Get_State(out plObjState: Integer): HResult;
  public
    property DefaultInterface: _Connection read GetDefaultInterface;
  published
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoCommand provides a Create and CreateRemote method to          
// create instances of the default interface _Command exposed by              
// the CoClass Command. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCommand = class
    class function Create: _Command;
    class function CreateRemote(const MachineName: string): _Command;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TCommand
// Help String      : 
// Default Interface: _Command
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (6) CanCreate Licensed
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TCommandProperties= class;
{$ENDIF}
  TCommand = class(TOleServer)
  private
    FIntf:        _Command;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TCommandProperties;
    function      GetServerProperties: TCommandProperties;
{$ENDIF}
    function      GetDefaultInterface: _Command;
  protected
    procedure InitServerData; override;
    function Get_State(out plObjState: Integer): HResult;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _Command);
    procedure Disconnect; override;
    function Cancel: HResult;
    property DefaultInterface: _Command read GetDefaultInterface;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TCommandProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TCommand
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TCommandProperties = class(TPersistent)
  private
    FServer:    TCommand;
    function    GetDefaultInterface: _Command;
    constructor Create(AServer: TCommand);
  protected
    function Get_State(out plObjState: Integer): HResult;
  public
    property DefaultInterface: _Command read GetDefaultInterface;
  published
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoRecordset provides a Create and CreateRemote method to          
// create instances of the default interface _Recordset exposed by              
// the CoClass Recordset. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoRecordset = class
    class function Create: _Recordset;
    class function CreateRemote(const MachineName: string): _Recordset;
  end;

  TRecordsetWillChangeField = procedure(ASender: TObject; cFields: Integer; Fields: OleVariant; 
                                                          var adStatus: EventStatusEnum; 
                                                          const pRecordset: _Recordset) of object;
  TRecordsetFieldChangeComplete = procedure(ASender: TObject; cFields: Integer; Fields: OleVariant; 
                                                              const pError: Error; 
                                                              var adStatus: EventStatusEnum; 
                                                              const pRecordset: _Recordset) of object;
  TRecordsetWillChangeRecord = procedure(ASender: TObject; adReason: EventReasonEnum; 
                                                           cRecords: Integer; 
                                                           var adStatus: EventStatusEnum; 
                                                           const pRecordset: _Recordset) of object;
  TRecordsetRecordChangeComplete = procedure(ASender: TObject; adReason: EventReasonEnum; 
                                                               cRecords: Integer; 
                                                               const pError: Error; 
                                                               var adStatus: EventStatusEnum; 
                                                               const pRecordset: _Recordset) of object;
  TRecordsetWillChangeRecordset = procedure(ASender: TObject; adReason: EventReasonEnum; 
                                                              var adStatus: EventStatusEnum; 
                                                              const pRecordset: _Recordset) of object;
  TRecordsetRecordsetChangeComplete = procedure(ASender: TObject; adReason: EventReasonEnum; 
                                                                  const pError: Error; 
                                                                  var adStatus: EventStatusEnum; 
                                                                  const pRecordset: _Recordset) of object;
  TRecordsetWillMove = procedure(ASender: TObject; adReason: EventReasonEnum; 
                                                   var adStatus: EventStatusEnum; 
                                                   const pRecordset: _Recordset) of object;
  TRecordsetMoveComplete = procedure(ASender: TObject; adReason: EventReasonEnum; 
                                                       const pError: Error; 
                                                       var adStatus: EventStatusEnum; 
                                                       const pRecordset: _Recordset) of object;
  TRecordsetEndOfRecordset = procedure(ASender: TObject; var fMoreData: WordBool; 
                                                         var adStatus: EventStatusEnum; 
                                                         const pRecordset: _Recordset) of object;
  TRecordsetFetchProgress = procedure(ASender: TObject; Progress: Integer; MaxProgress: Integer; 
                                                        var adStatus: EventStatusEnum; 
                                                        const pRecordset: _Recordset) of object;
  TRecordsetFetchComplete = procedure(ASender: TObject; const pError: Error; 
                                                        var adStatus: EventStatusEnum; 
                                                        const pRecordset: _Recordset) of object;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TRecordset
// Help String      : 
// Default Interface: _Recordset
// Def. Intf. DISP? : No
// Event   Interface: RecordsetEvents
// TypeFlags        : (6) CanCreate Licensed
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TRecordsetProperties= class;
{$ENDIF}
  TRecordset = class(TOleServer)
  private
    FOnWillChangeField: TRecordsetWillChangeField;
    FOnFieldChangeComplete: TRecordsetFieldChangeComplete;
    FOnWillChangeRecord: TRecordsetWillChangeRecord;
    FOnRecordChangeComplete: TRecordsetRecordChangeComplete;
    FOnWillChangeRecordset: TRecordsetWillChangeRecordset;
    FOnRecordsetChangeComplete: TRecordsetRecordsetChangeComplete;
    FOnWillMove: TRecordsetWillMove;
    FOnMoveComplete: TRecordsetMoveComplete;
    FOnEndOfRecordset: TRecordsetEndOfRecordset;
    FOnFetchProgress: TRecordsetFetchProgress;
    FOnFetchComplete: TRecordsetFetchComplete;
    FIntf:        _Recordset;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TRecordsetProperties;
    function      GetServerProperties: TRecordsetProperties;
{$ENDIF}
    function      GetDefaultInterface: _Recordset;
  protected
    procedure InitServerData; override;
    procedure InvokeEvent(DispID: TDispID; var Params: TVariantArray); override;
    function Get_DataSource(out ppunkDataSource: IUnknown): HResult;
    function _Set_DataSource(const ppunkDataSource: IUnknown): HResult;
    function Get_ActiveCommand(out ppCmd: IDispatch): HResult;
    function Set_StayInSync(pbStayInSync: WordBool): HResult;
    function Get_StayInSync(out pbStayInSync: WordBool): HResult;
    function Get_DataMember(out pbstrDataMember: WideString): HResult;
    function Set_DataMember(const pbstrDataMember: WideString): HResult;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _Recordset);
    procedure Disconnect; override;
    function Cancel: HResult;
    function Save(const FileName: WideString; PersistFormat: PersistFormatEnum): HResult;
    function GetString(StringFormat: StringFormatEnum; NumRows: Integer; 
                       const ColumnDelimeter: WideString; const RowDelimeter: WideString; 
                       const NullExpr: WideString; out pRetString: WideString): HResult;
    function CompareBookmarks(Bookmark1: OleVariant; Bookmark2: OleVariant; 
                              out pCompare: CompareEnum): HResult;
    function Clone(LockType: LockTypeEnum; out ppvObject: _Recordset): HResult;
    function Resync(AffectRecords: AffectEnum; ResyncValues: ResyncEnum): HResult;
    property DefaultInterface: _Recordset read GetDefaultInterface;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TRecordsetProperties read GetServerProperties;
{$ENDIF}
    property OnWillChangeField: TRecordsetWillChangeField read FOnWillChangeField write FOnWillChangeField;
    property OnFieldChangeComplete: TRecordsetFieldChangeComplete read FOnFieldChangeComplete write FOnFieldChangeComplete;
    property OnWillChangeRecord: TRecordsetWillChangeRecord read FOnWillChangeRecord write FOnWillChangeRecord;
    property OnRecordChangeComplete: TRecordsetRecordChangeComplete read FOnRecordChangeComplete write FOnRecordChangeComplete;
    property OnWillChangeRecordset: TRecordsetWillChangeRecordset read FOnWillChangeRecordset write FOnWillChangeRecordset;
    property OnRecordsetChangeComplete: TRecordsetRecordsetChangeComplete read FOnRecordsetChangeComplete write FOnRecordsetChangeComplete;
    property OnWillMove: TRecordsetWillMove read FOnWillMove write FOnWillMove;
    property OnMoveComplete: TRecordsetMoveComplete read FOnMoveComplete write FOnMoveComplete;
    property OnEndOfRecordset: TRecordsetEndOfRecordset read FOnEndOfRecordset write FOnEndOfRecordset;
    property OnFetchProgress: TRecordsetFetchProgress read FOnFetchProgress write FOnFetchProgress;
    property OnFetchComplete: TRecordsetFetchComplete read FOnFetchComplete write FOnFetchComplete;
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TRecordset
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TRecordsetProperties = class(TPersistent)
  private
    FServer:    TRecordset;
    function    GetDefaultInterface: _Recordset;
    constructor Create(AServer: TRecordset);
  protected
    function Get_DataSource(out ppunkDataSource: IUnknown): HResult;
    function _Set_DataSource(const ppunkDataSource: IUnknown): HResult;
    function Get_ActiveCommand(out ppCmd: IDispatch): HResult;
    function Set_StayInSync(pbStayInSync: WordBool): HResult;
    function Get_StayInSync(out pbStayInSync: WordBool): HResult;
    function Get_DataMember(out pbstrDataMember: WideString): HResult;
    function Set_DataMember(const pbstrDataMember: WideString): HResult;
  public
    property DefaultInterface: _Recordset read GetDefaultInterface;
  published
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoParameter provides a Create and CreateRemote method to          
// create instances of the default interface _Parameter exposed by              
// the CoClass Parameter. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoParameter = class
    class function Create: _Parameter;
    class function CreateRemote(const MachineName: string): _Parameter;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TParameter
// Help String      : 
// Default Interface: _Parameter
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (6) CanCreate Licensed
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TParameterProperties= class;
{$ENDIF}
  TParameter = class(TOleServer)
  private
    FIntf:        _Parameter;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TParameterProperties;
    function      GetServerProperties: TParameterProperties;
{$ENDIF}
    function      GetDefaultInterface: _Parameter;
  protected
    procedure InitServerData; override;
    function Get_Name(out pbstr: WideString): HResult;
    function Set_Name(const pbstr: WideString): HResult;
    function Get_Value(out pvar: OleVariant): HResult;
    function Set_Value(pvar: OleVariant): HResult;
    function Get_type_(out psDataType: DataTypeEnum): HResult;
    function Set_type_(psDataType: DataTypeEnum): HResult;
    function Set_Direction(plParmDirection: ParameterDirectionEnum): HResult;
    function Get_Direction(out plParmDirection: ParameterDirectionEnum): HResult;
    function Set_Precision(pbPrecision: Byte): HResult;
    function Get_Precision(out pbPrecision: Byte): HResult;
    function Set_NumericScale(pbScale: Byte): HResult;
    function Get_NumericScale(out pbScale: Byte): HResult;
    function Set_Size(pl: Integer): HResult;
    function Get_Size(out pl: Integer): HResult;
    function Get_Attributes(out plParmAttribs: Integer): HResult;
    function Set_Attributes(plParmAttribs: Integer): HResult;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: _Parameter);
    procedure Disconnect; override;
    function AppendChunk(Val: OleVariant): HResult;
    property DefaultInterface: _Parameter read GetDefaultInterface;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TParameterProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TParameter
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TParameterProperties = class(TPersistent)
  private
    FServer:    TParameter;
    function    GetDefaultInterface: _Parameter;
    constructor Create(AServer: TParameter);
  protected
    function Get_Name(out pbstr: WideString): HResult;
    function Set_Name(const pbstr: WideString): HResult;
    function Get_Value(out pvar: OleVariant): HResult;
    function Set_Value(pvar: OleVariant): HResult;
    function Get_type_(out psDataType: DataTypeEnum): HResult;
    function Set_type_(psDataType: DataTypeEnum): HResult;
    function Set_Direction(plParmDirection: ParameterDirectionEnum): HResult;
    function Get_Direction(out plParmDirection: ParameterDirectionEnum): HResult;
    function Set_Precision(pbPrecision: Byte): HResult;
    function Get_Precision(out pbPrecision: Byte): HResult;
    function Set_NumericScale(pbScale: Byte): HResult;
    function Get_NumericScale(out pbScale: Byte): HResult;
    function Set_Size(pl: Integer): HResult;
    function Get_Size(out pl: Integer): HResult;
    function Get_Attributes(out plParmAttribs: Integer): HResult;
    function Set_Attributes(plParmAttribs: Integer): HResult;
  public
    property DefaultInterface: _Parameter read GetDefaultInterface;
  published
  end;
{$ENDIF}


procedure Register;

resourcestring
  dtlServerPage = 'Servers';

  dtlOcxPage = 'ActiveX';

implementation

uses ComObj;

class function CoConnection.Create: _Connection;
begin
  Result := CreateComObject(CLASS_Connection) as _Connection;
end;

class function CoConnection.CreateRemote(const MachineName: string): _Connection;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Connection) as _Connection;
end;

procedure TConnection.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{00000514-0000-0010-8000-00AA006D2EA4}';
    IntfIID:   '{00000550-0000-0010-8000-00AA006D2EA4}';
    EventIID:  '{00000400-0000-0010-8000-00AA006D2EA4}';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TConnection.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    ConnectEvents(punk);
    Fintf:= punk as _Connection;
  end;
end;

procedure TConnection.ConnectTo(svrIntf: _Connection);
begin
  Disconnect;
  FIntf := svrIntf;
  ConnectEvents(FIntf);
end;

procedure TConnection.DisConnect;
begin
  if Fintf <> nil then
  begin
    DisconnectEvents(FIntf);
    FIntf := nil;
  end;
end;

function TConnection.GetDefaultInterface: _Connection;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TConnection.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TConnectionProperties.Create(Self);
{$ENDIF}
end;

destructor TConnection.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TConnection.GetServerProperties: TConnectionProperties;
begin
  Result := FProps;
end;
{$ENDIF}

procedure TConnection.InvokeEvent(DispID: TDispID; var Params: TVariantArray);
begin
  case DispID of
    -1: Exit;  // DISPID_UNKNOWN
    0: if Assigned(FOnInfoMessage) then
         FOnInfoMessage(Self,
                        IUnknown(TVarData(Params[0]).VPointer) as Error {const Error},
                        EventStatusEnum((TVarData(Params[1]).VPointer)^) {var EventStatusEnum},
                        IUnknown(TVarData(Params[2]).VPointer) as _Connection {const _Connection});
    1: if Assigned(FOnBeginTransComplete) then
         FOnBeginTransComplete(Self,
                               Params[0] {Integer},
                               IUnknown(TVarData(Params[1]).VPointer) as Error {const Error},
                               EventStatusEnum((TVarData(Params[2]).VPointer)^) {var EventStatusEnum},
                               IUnknown(TVarData(Params[3]).VPointer) as _Connection {const _Connection});
    3: if Assigned(FOnCommitTransComplete) then
         FOnCommitTransComplete(Self,
                                IUnknown(TVarData(Params[0]).VPointer) as Error {const Error},
                                EventStatusEnum((TVarData(Params[1]).VPointer)^) {var EventStatusEnum},
                                IUnknown(TVarData(Params[2]).VPointer) as _Connection {const _Connection});
    2: if Assigned(FOnRollbackTransComplete) then
         FOnRollbackTransComplete(Self,
                                  IUnknown(TVarData(Params[0]).VPointer) as Error {const Error},
                                  EventStatusEnum((TVarData(Params[1]).VPointer)^) {var EventStatusEnum},
                                  IUnknown(TVarData(Params[2]).VPointer) as _Connection {const _Connection});
    4: if Assigned(FOnWillExecute) then
         FOnWillExecute(Self,
                        WideString((TVarData(Params[0]).VPointer)^) {var WideString},
                        CursorTypeEnum((TVarData(Params[1]).VPointer)^) {var CursorTypeEnum},
                        LockTypeEnum((TVarData(Params[2]).VPointer)^) {var LockTypeEnum},
                        Integer((TVarData(Params[3]).VPointer)^) {var Integer},
                        EventStatusEnum((TVarData(Params[4]).VPointer)^) {var EventStatusEnum},
                        IUnknown(TVarData(Params[5]).VPointer) as _Command {const _Command},
                        IUnknown(TVarData(Params[6]).VPointer) as _Recordset {const _Recordset},
                        IUnknown(TVarData(Params[7]).VPointer) as _Connection {const _Connection});
    5: if Assigned(FOnExecuteComplete) then
         FOnExecuteComplete(Self,
                            Params[0] {Integer},
                            IUnknown(TVarData(Params[1]).VPointer) as Error {const Error},
                            EventStatusEnum((TVarData(Params[2]).VPointer)^) {var EventStatusEnum},
                            IUnknown(TVarData(Params[3]).VPointer) as _Command {const _Command},
                            IUnknown(TVarData(Params[4]).VPointer) as _Recordset {const _Recordset},
                            IUnknown(TVarData(Params[5]).VPointer) as _Connection {const _Connection});
    6: if Assigned(FOnWillConnect) then
         FOnWillConnect(Self,
                        WideString((TVarData(Params[0]).VPointer)^) {var WideString},
                        WideString((TVarData(Params[1]).VPointer)^) {var WideString},
                        WideString((TVarData(Params[2]).VPointer)^) {var WideString},
                        Integer((TVarData(Params[3]).VPointer)^) {var Integer},
                        EventStatusEnum((TVarData(Params[4]).VPointer)^) {var EventStatusEnum},
                        IUnknown(TVarData(Params[5]).VPointer) as _Connection {const _Connection});
    7: if Assigned(FOnConnectComplete) then
         FOnConnectComplete(Self,
                            IUnknown(TVarData(Params[0]).VPointer) as Error {const Error},
                            EventStatusEnum((TVarData(Params[1]).VPointer)^) {var EventStatusEnum},
                            IUnknown(TVarData(Params[2]).VPointer) as _Connection {const _Connection});
    8: if Assigned(FOnDisconnect) then
         FOnDisconnect(Self,
                       EventStatusEnum((TVarData(Params[0]).VPointer)^) {var EventStatusEnum},
                       IUnknown(TVarData(Params[1]).VPointer) as _Connection {const _Connection});
  end; {case DispID}
end;

function TConnection.Get_ConnectionString(out pbstr: WideString): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.ConnectionString;
end;

function TConnection.Set_ConnectionString(const pbstr: WideString): HResult;
  { Warning: The property ConnectionString has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ConnectionString := pbstr;
  Result := S_OK;
end;

function TConnection.Get_CommandTimeout(out plTimeout: Integer): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.CommandTimeout;
end;

function TConnection.Set_CommandTimeout(plTimeout: Integer): HResult;
  { Warning: The property CommandTimeout has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.CommandTimeout := plTimeout;
  Result := S_OK;
end;

function TConnection.Get_ConnectionTimeout(out plTimeout: Integer): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.ConnectionTimeout;
end;

function TConnection.Set_ConnectionTimeout(plTimeout: Integer): HResult;
  { Warning: The property ConnectionTimeout has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ConnectionTimeout := plTimeout;
  Result := S_OK;
end;

function TConnection.Get_Version(out pbstr: WideString): HResult;
begin
    Result := DefaultInterface.Get_Version(pbstr);
end;

function TConnection.Get_Errors(out ppvObject: Errors): HResult;
begin
    Result := DefaultInterface.Get_Errors(ppvObject);
end;

function TConnection.Get_DefaultDatabase(out pbstr: WideString): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.DefaultDatabase;
end;

function TConnection.Set_DefaultDatabase(const pbstr: WideString): HResult;
  { Warning: The property DefaultDatabase has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.DefaultDatabase := pbstr;
  Result := S_OK;
end;

function TConnection.Get_IsolationLevel(out Level: IsolationLevelEnum): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.IsolationLevel;
end;

function TConnection.Set_IsolationLevel(Level: IsolationLevelEnum): HResult;
  { Warning: The property IsolationLevel has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.IsolationLevel := Level;
  Result := S_OK;
end;

function TConnection.Get_Attributes(out plAttr: Integer): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Attributes;
end;

function TConnection.Set_Attributes(plAttr: Integer): HResult;
  { Warning: The property Attributes has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Attributes := plAttr;
  Result := S_OK;
end;

function TConnection.Get_CursorLocation(out plCursorLoc: CursorLocationEnum): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.CursorLocation;
end;

function TConnection.Set_CursorLocation(plCursorLoc: CursorLocationEnum): HResult;
  { Warning: The property CursorLocation has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.CursorLocation := plCursorLoc;
  Result := S_OK;
end;

function TConnection.Get_Mode(out plMode: ConnectModeEnum): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Mode;
end;

function TConnection.Set_Mode(plMode: ConnectModeEnum): HResult;
  { Warning: The property Mode has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Mode := plMode;
  Result := S_OK;
end;

function TConnection.Get_Provider(out pbstr: WideString): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Provider;
end;

function TConnection.Set_Provider(const pbstr: WideString): HResult;
  { Warning: The property Provider has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Provider := pbstr;
  Result := S_OK;
end;

function TConnection.Get_State(out plObjState: Integer): HResult;
begin
    Result := DefaultInterface.Get_State(plObjState);
end;

function TConnection.Close: HResult;
begin
  Result := DefaultInterface.Close;
end;

function TConnection.Execute(const CommandText: WideString; out RecordsAffected: OleVariant; 
                             Options: Integer; out ppiRset: _Recordset): HResult;
begin
  Result := DefaultInterface.Execute(CommandText, RecordsAffected, Options, ppiRset);
end;

function TConnection.BeginTrans(out TransactionLevel: Integer): HResult;
begin
  Result := DefaultInterface.BeginTrans(TransactionLevel);
end;

function TConnection.CommitTrans: HResult;
begin
  Result := DefaultInterface.CommitTrans;
end;

function TConnection.RollbackTrans: HResult;
begin
  Result := DefaultInterface.RollbackTrans;
end;

function TConnection.Open(const ConnectionString: WideString; const UserID: WideString; 
                          const Password: WideString; Options: Integer): HResult;
begin
  Result := DefaultInterface.Open(ConnectionString, UserID, Password, Options);
end;

function TConnection.OpenSchema(Schema: SchemaEnum; Restrictions: OleVariant; SchemaID: OleVariant; 
                                out pprset: _Recordset): HResult;
begin
  Result := DefaultInterface.OpenSchema(Schema, Restrictions, SchemaID, pprset);
end;

function TConnection.Cancel: HResult;
begin
  Result := DefaultInterface.Cancel;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TConnectionProperties.Create(AServer: TConnection);
begin
  inherited Create;
  FServer := AServer;
end;

function TConnectionProperties.GetDefaultInterface: _Connection;
begin
  Result := FServer.DefaultInterface;
end;

function TConnectionProperties.Get_ConnectionString(out pbstr: WideString): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.ConnectionString;
end;

function TConnectionProperties.Set_ConnectionString(const pbstr: WideString): HResult;
  { Warning: The property ConnectionString has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ConnectionString := pbstr;
  Result := S_OK;
end;

function TConnectionProperties.Get_CommandTimeout(out plTimeout: Integer): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.CommandTimeout;
end;

function TConnectionProperties.Set_CommandTimeout(plTimeout: Integer): HResult;
  { Warning: The property CommandTimeout has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.CommandTimeout := plTimeout;
  Result := S_OK;
end;

function TConnectionProperties.Get_ConnectionTimeout(out plTimeout: Integer): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.ConnectionTimeout;
end;

function TConnectionProperties.Set_ConnectionTimeout(plTimeout: Integer): HResult;
  { Warning: The property ConnectionTimeout has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.ConnectionTimeout := plTimeout;
  Result := S_OK;
end;

function TConnectionProperties.Get_Version(out pbstr: WideString): HResult;
begin
    Result := DefaultInterface.Get_Version(pbstr);
end;

function TConnectionProperties.Get_Errors(out ppvObject: Errors): HResult;
begin
    Result := DefaultInterface.Get_Errors(ppvObject);
end;

function TConnectionProperties.Get_DefaultDatabase(out pbstr: WideString): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.DefaultDatabase;
end;

function TConnectionProperties.Set_DefaultDatabase(const pbstr: WideString): HResult;
  { Warning: The property DefaultDatabase has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.DefaultDatabase := pbstr;
  Result := S_OK;
end;

function TConnectionProperties.Get_IsolationLevel(out Level: IsolationLevelEnum): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.IsolationLevel;
end;

function TConnectionProperties.Set_IsolationLevel(Level: IsolationLevelEnum): HResult;
  { Warning: The property IsolationLevel has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.IsolationLevel := Level;
  Result := S_OK;
end;

function TConnectionProperties.Get_Attributes(out plAttr: Integer): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Attributes;
end;

function TConnectionProperties.Set_Attributes(plAttr: Integer): HResult;
  { Warning: The property Attributes has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Attributes := plAttr;
  Result := S_OK;
end;

function TConnectionProperties.Get_CursorLocation(out plCursorLoc: CursorLocationEnum): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.CursorLocation;
end;

function TConnectionProperties.Set_CursorLocation(plCursorLoc: CursorLocationEnum): HResult;
  { Warning: The property CursorLocation has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.CursorLocation := plCursorLoc;
  Result := S_OK;
end;

function TConnectionProperties.Get_Mode(out plMode: ConnectModeEnum): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Mode;
end;

function TConnectionProperties.Set_Mode(plMode: ConnectModeEnum): HResult;
  { Warning: The property Mode has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Mode := plMode;
  Result := S_OK;
end;

function TConnectionProperties.Get_Provider(out pbstr: WideString): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Provider;
end;

function TConnectionProperties.Set_Provider(const pbstr: WideString): HResult;
  { Warning: The property Provider has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Provider := pbstr;
  Result := S_OK;
end;

function TConnectionProperties.Get_State(out plObjState: Integer): HResult;
begin
    Result := DefaultInterface.Get_State(plObjState);
end;

{$ENDIF}

class function CoCommand.Create: _Command;
begin
  Result := CreateComObject(CLASS_Command) as _Command;
end;

class function CoCommand.CreateRemote(const MachineName: string): _Command;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Command) as _Command;
end;

procedure TCommand.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{00000507-0000-0010-8000-00AA006D2EA4}';
    IntfIID:   '{0000054E-0000-0010-8000-00AA006D2EA4}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TCommand.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _Command;
  end;
end;

procedure TCommand.ConnectTo(svrIntf: _Command);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TCommand.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TCommand.GetDefaultInterface: _Command;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TCommand.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TCommandProperties.Create(Self);
{$ENDIF}
end;

destructor TCommand.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TCommand.GetServerProperties: TCommandProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TCommand.Get_State(out plObjState: Integer): HResult;
begin
    Result := DefaultInterface.Get_State(plObjState);
end;

function TCommand.Cancel: HResult;
begin
  Result := DefaultInterface.Cancel;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TCommandProperties.Create(AServer: TCommand);
begin
  inherited Create;
  FServer := AServer;
end;

function TCommandProperties.GetDefaultInterface: _Command;
begin
  Result := FServer.DefaultInterface;
end;

function TCommandProperties.Get_State(out plObjState: Integer): HResult;
begin
    Result := DefaultInterface.Get_State(plObjState);
end;

{$ENDIF}

class function CoRecordset.Create: _Recordset;
begin
  Result := CreateComObject(CLASS_Recordset) as _Recordset;
end;

class function CoRecordset.CreateRemote(const MachineName: string): _Recordset;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Recordset) as _Recordset;
end;

procedure TRecordset.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{00000535-0000-0010-8000-00AA006D2EA4}';
    IntfIID:   '{0000054F-0000-0010-8000-00AA006D2EA4}';
    EventIID:  '{00000266-0000-0010-8000-00AA006D2EA4}';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TRecordset.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    ConnectEvents(punk);
    Fintf:= punk as _Recordset;
  end;
end;

procedure TRecordset.ConnectTo(svrIntf: _Recordset);
begin
  Disconnect;
  FIntf := svrIntf;
  ConnectEvents(FIntf);
end;

procedure TRecordset.DisConnect;
begin
  if Fintf <> nil then
  begin
    DisconnectEvents(FIntf);
    FIntf := nil;
  end;
end;

function TRecordset.GetDefaultInterface: _Recordset;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TRecordset.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TRecordsetProperties.Create(Self);
{$ENDIF}
end;

destructor TRecordset.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TRecordset.GetServerProperties: TRecordsetProperties;
begin
  Result := FProps;
end;
{$ENDIF}

procedure TRecordset.InvokeEvent(DispID: TDispID; var Params: TVariantArray);
begin
  case DispID of
    -1: Exit;  // DISPID_UNKNOWN
    9: if Assigned(FOnWillChangeField) then
         FOnWillChangeField(Self,
                            Params[0] {Integer},
                            Params[1] {OleVariant},
                            EventStatusEnum((TVarData(Params[2]).VPointer)^) {var EventStatusEnum},
                            IUnknown(TVarData(Params[3]).VPointer) as _Recordset {const _Recordset});
    10: if Assigned(FOnFieldChangeComplete) then
         FOnFieldChangeComplete(Self,
                                Params[0] {Integer},
                                Params[1] {OleVariant},
                                IUnknown(TVarData(Params[2]).VPointer) as Error {const Error},
                                EventStatusEnum((TVarData(Params[3]).VPointer)^) {var EventStatusEnum},
                                IUnknown(TVarData(Params[4]).VPointer) as _Recordset {const _Recordset});
    11: if Assigned(FOnWillChangeRecord) then
         FOnWillChangeRecord(Self,
                             Params[0] {EventReasonEnum},
                             Params[1] {Integer},
                             EventStatusEnum((TVarData(Params[2]).VPointer)^) {var EventStatusEnum},
                             IUnknown(TVarData(Params[3]).VPointer) as _Recordset {const _Recordset});
    12: if Assigned(FOnRecordChangeComplete) then
         FOnRecordChangeComplete(Self,
                                 Params[0] {EventReasonEnum},
                                 Params[1] {Integer},
                                 IUnknown(TVarData(Params[2]).VPointer) as Error {const Error},
                                 EventStatusEnum((TVarData(Params[3]).VPointer)^) {var EventStatusEnum},
                                 IUnknown(TVarData(Params[4]).VPointer) as _Recordset {const _Recordset});
    13: if Assigned(FOnWillChangeRecordset) then
         FOnWillChangeRecordset(Self,
                                Params[0] {EventReasonEnum},
                                EventStatusEnum((TVarData(Params[1]).VPointer)^) {var EventStatusEnum},
                                IUnknown(TVarData(Params[2]).VPointer) as _Recordset {const _Recordset});
    14: if Assigned(FOnRecordsetChangeComplete) then
         FOnRecordsetChangeComplete(Self,
                                    Params[0] {EventReasonEnum},
                                    IUnknown(TVarData(Params[1]).VPointer) as Error {const Error},
                                    EventStatusEnum((TVarData(Params[2]).VPointer)^) {var EventStatusEnum},
                                    IUnknown(TVarData(Params[3]).VPointer) as _Recordset {const _Recordset});
    15: if Assigned(FOnWillMove) then
         FOnWillMove(Self,
                     Params[0] {EventReasonEnum},
                     EventStatusEnum((TVarData(Params[1]).VPointer)^) {var EventStatusEnum},
                     IUnknown(TVarData(Params[2]).VPointer) as _Recordset {const _Recordset});
    16: if Assigned(FOnMoveComplete) then
         FOnMoveComplete(Self,
                         Params[0] {EventReasonEnum},
                         IUnknown(TVarData(Params[1]).VPointer) as Error {const Error},
                         EventStatusEnum((TVarData(Params[2]).VPointer)^) {var EventStatusEnum},
                         IUnknown(TVarData(Params[3]).VPointer) as _Recordset {const _Recordset});
    17: if Assigned(FOnEndOfRecordset) then
         FOnEndOfRecordset(Self,
                           WordBool((TVarData(Params[0]).VPointer)^) {var WordBool},
                           EventStatusEnum((TVarData(Params[1]).VPointer)^) {var EventStatusEnum},
                           IUnknown(TVarData(Params[2]).VPointer) as _Recordset {const _Recordset});
    18: if Assigned(FOnFetchProgress) then
         FOnFetchProgress(Self,
                          Params[0] {Integer},
                          Params[1] {Integer},
                          EventStatusEnum((TVarData(Params[2]).VPointer)^) {var EventStatusEnum},
                          IUnknown(TVarData(Params[3]).VPointer) as _Recordset {const _Recordset});
    19: if Assigned(FOnFetchComplete) then
         FOnFetchComplete(Self,
                          IUnknown(TVarData(Params[0]).VPointer) as Error {const Error},
                          EventStatusEnum((TVarData(Params[1]).VPointer)^) {var EventStatusEnum},
                          IUnknown(TVarData(Params[2]).VPointer) as _Recordset {const _Recordset});
  end; {case DispID}
end;

function TRecordset.Get_DataSource(out ppunkDataSource: IUnknown): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.DataSource;
end;

function TRecordset._Set_DataSource(const ppunkDataSource: IUnknown): HResult;
  { Warning: The property DataSource has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.DataSource := ppunkDataSource;
  Result := S_OK;
end;

function TRecordset.Get_ActiveCommand(out ppCmd: IDispatch): HResult;
begin
    Result := DefaultInterface.Get_ActiveCommand(ppCmd);
end;

function TRecordset.Set_StayInSync(pbStayInSync: WordBool): HResult;
  { Warning: The property StayInSync has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.StayInSync := pbStayInSync;
  Result := S_OK;
end;

function TRecordset.Get_StayInSync(out pbStayInSync: WordBool): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.StayInSync;
end;

function TRecordset.Get_DataMember(out pbstrDataMember: WideString): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.DataMember;
end;

function TRecordset.Set_DataMember(const pbstrDataMember: WideString): HResult;
  { Warning: The property DataMember has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.DataMember := pbstrDataMember;
  Result := S_OK;
end;

function TRecordset.Cancel: HResult;
begin
  Result := DefaultInterface.Cancel;
end;

function TRecordset.Save(const FileName: WideString; PersistFormat: PersistFormatEnum): HResult;
begin
  Result := DefaultInterface.Save(FileName, PersistFormat);
end;

function TRecordset.GetString(StringFormat: StringFormatEnum; NumRows: Integer; 
                              const ColumnDelimeter: WideString; const RowDelimeter: WideString; 
                              const NullExpr: WideString; out pRetString: WideString): HResult;
begin
  Result := DefaultInterface.GetString(StringFormat, NumRows, ColumnDelimeter, RowDelimeter, 
                                       NullExpr, pRetString);
end;

function TRecordset.CompareBookmarks(Bookmark1: OleVariant; Bookmark2: OleVariant; 
                                     out pCompare: CompareEnum): HResult;
begin
  Result := DefaultInterface.CompareBookmarks(Bookmark1, Bookmark2, pCompare);
end;

function TRecordset.Clone(LockType: LockTypeEnum; out ppvObject: _Recordset): HResult;
begin
  Result := DefaultInterface.Clone(LockType, ppvObject);
end;

function TRecordset.Resync(AffectRecords: AffectEnum; ResyncValues: ResyncEnum): HResult;
begin
  Result := DefaultInterface.Resync(AffectRecords, ResyncValues);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TRecordsetProperties.Create(AServer: TRecordset);
begin
  inherited Create;
  FServer := AServer;
end;

function TRecordsetProperties.GetDefaultInterface: _Recordset;
begin
  Result := FServer.DefaultInterface;
end;

function TRecordsetProperties.Get_DataSource(out ppunkDataSource: IUnknown): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.DataSource;
end;

function TRecordsetProperties._Set_DataSource(const ppunkDataSource: IUnknown): HResult;
  { Warning: The property DataSource has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.DataSource := ppunkDataSource;
  Result := S_OK;
end;

function TRecordsetProperties.Get_ActiveCommand(out ppCmd: IDispatch): HResult;
begin
    Result := DefaultInterface.Get_ActiveCommand(ppCmd);
end;

function TRecordsetProperties.Set_StayInSync(pbStayInSync: WordBool): HResult;
  { Warning: The property StayInSync has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.StayInSync := pbStayInSync;
  Result := S_OK;
end;

function TRecordsetProperties.Get_StayInSync(out pbStayInSync: WordBool): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.StayInSync;
end;

function TRecordsetProperties.Get_DataMember(out pbstrDataMember: WideString): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.DataMember;
end;

function TRecordsetProperties.Set_DataMember(const pbstrDataMember: WideString): HResult;
  { Warning: The property DataMember has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.DataMember := pbstrDataMember;
  Result := S_OK;
end;

{$ENDIF}

class function CoParameter.Create: _Parameter;
begin
  Result := CreateComObject(CLASS_Parameter) as _Parameter;
end;

class function CoParameter.CreateRemote(const MachineName: string): _Parameter;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Parameter) as _Parameter;
end;

procedure TParameter.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{0000050B-0000-0010-8000-00AA006D2EA4}';
    IntfIID:   '{0000050C-0000-0010-8000-00AA006D2EA4}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TParameter.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as _Parameter;
  end;
end;

procedure TParameter.ConnectTo(svrIntf: _Parameter);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TParameter.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TParameter.GetDefaultInterface: _Parameter;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TParameter.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TParameterProperties.Create(Self);
{$ENDIF}
end;

destructor TParameter.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TParameter.GetServerProperties: TParameterProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TParameter.Get_Name(out pbstr: WideString): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Name;
end;

function TParameter.Set_Name(const pbstr: WideString): HResult;
  { Warning: The property Name has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Name := pbstr;
  Result := S_OK;
end;

function TParameter.Get_Value(out pvar: OleVariant): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Value;
end;

function TParameter.Set_Value(pvar: OleVariant): HResult;
  { Warning: The property Value has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Value := pvar;
  Result := S_OK;
end;

function TParameter.Get_type_(out psDataType: DataTypeEnum): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.type_;
end;

function TParameter.Set_type_(psDataType: DataTypeEnum): HResult;
  { Warning: The property type_ has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.type_ := psDataType;
  Result := S_OK;
end;

function TParameter.Set_Direction(plParmDirection: ParameterDirectionEnum): HResult;
  { Warning: The property Direction has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Direction := plParmDirection;
  Result := S_OK;
end;

function TParameter.Get_Direction(out plParmDirection: ParameterDirectionEnum): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Direction;
end;

function TParameter.Set_Precision(pbPrecision: Byte): HResult;
  { Warning: The property Precision has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Precision := pbPrecision;
  Result := S_OK;
end;

function TParameter.Get_Precision(out pbPrecision: Byte): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Precision;
end;

function TParameter.Set_NumericScale(pbScale: Byte): HResult;
  { Warning: The property NumericScale has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.NumericScale := pbScale;
  Result := S_OK;
end;

function TParameter.Get_NumericScale(out pbScale: Byte): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.NumericScale;
end;

function TParameter.Set_Size(pl: Integer): HResult;
  { Warning: The property Size has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Size := pl;
  Result := S_OK;
end;

function TParameter.Get_Size(out pl: Integer): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Size;
end;

function TParameter.Get_Attributes(out plParmAttribs: Integer): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Attributes;
end;

function TParameter.Set_Attributes(plParmAttribs: Integer): HResult;
  { Warning: The property Attributes has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Attributes := plParmAttribs;
  Result := S_OK;
end;

function TParameter.AppendChunk(Val: OleVariant): HResult;
begin
  Result := DefaultInterface.AppendChunk(Val);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TParameterProperties.Create(AServer: TParameter);
begin
  inherited Create;
  FServer := AServer;
end;

function TParameterProperties.GetDefaultInterface: _Parameter;
begin
  Result := FServer.DefaultInterface;
end;

function TParameterProperties.Get_Name(out pbstr: WideString): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Name;
end;

function TParameterProperties.Set_Name(const pbstr: WideString): HResult;
  { Warning: The property Name has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Name := pbstr;
  Result := S_OK;
end;

function TParameterProperties.Get_Value(out pvar: OleVariant): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Value;
end;

function TParameterProperties.Set_Value(pvar: OleVariant): HResult;
  { Warning: The property Value has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Value := pvar;
  Result := S_OK;
end;

function TParameterProperties.Get_type_(out psDataType: DataTypeEnum): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.type_;
end;

function TParameterProperties.Set_type_(psDataType: DataTypeEnum): HResult;
  { Warning: The property type_ has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.type_ := psDataType;
  Result := S_OK;
end;

function TParameterProperties.Set_Direction(plParmDirection: ParameterDirectionEnum): HResult;
  { Warning: The property Direction has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Direction := plParmDirection;
  Result := S_OK;
end;

function TParameterProperties.Get_Direction(out plParmDirection: ParameterDirectionEnum): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Direction;
end;

function TParameterProperties.Set_Precision(pbPrecision: Byte): HResult;
  { Warning: The property Precision has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Precision := pbPrecision;
  Result := S_OK;
end;

function TParameterProperties.Get_Precision(out pbPrecision: Byte): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Precision;
end;

function TParameterProperties.Set_NumericScale(pbScale: Byte): HResult;
  { Warning: The property NumericScale has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.NumericScale := pbScale;
  Result := S_OK;
end;

function TParameterProperties.Get_NumericScale(out pbScale: Byte): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.NumericScale;
end;

function TParameterProperties.Set_Size(pl: Integer): HResult;
  { Warning: The property Size has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Size := pl;
  Result := S_OK;
end;

function TParameterProperties.Get_Size(out pl: Integer): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Size;
end;

function TParameterProperties.Get_Attributes(out plParmAttribs: Integer): HResult;
var
  InterfaceVariant : OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  Result := InterfaceVariant.Attributes;
end;

function TParameterProperties.Set_Attributes(plParmAttribs: Integer): HResult;
  { Warning: The property Attributes has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.Attributes := plParmAttribs;
  Result := S_OK;
end;

{$ENDIF}

procedure Register;
begin
  RegisterComponents(dtlServerPage, [TConnection, TCommand, TRecordset, TParameter]);
end;

end.
